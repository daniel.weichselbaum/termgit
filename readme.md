> **Für die deutsche Version bitte [hier](arch_and_setup_de.html) klicken.**

### The ELGA & Austrian eHealth terminologies.

ELGA and Austrian eHealth systems use a variety of different terminologies with different structures from different international, national and local sources. The current terminologies and governance can be found at [termgit.elga.gv.at](https://termgit.elga.gv.at). Technologically, the Austrian e-Health terminology browser is based on TerminoloGit.

In the spirit of the open source idea, the developers would be very happy about the use of the terminology server in other projects, feedback and even more about active participation. Suggestions for improvements or bug reports detected on TerminoloGit can be submitted via the GitLab ticket system at [https://gitlab.com/elga-gmbh/termgit/-/issues/new](https://gitlab.com/elga-gmbh/termgit/-/issues/new).

### Usage and Web Services

The following shows the different uses of the Terminology Browser and WebServices.

#### Browsing/Research
*Actor:* Student

The browser is ideal for research purposes or simply for browsing, as all lists are presented in a well-structured HTML page. The user has the possibility to call up different code systems and value sets individually in a structured form or to search for specific concepts with the help of the search function. There is always a link between the code systems and value sets to make it easier to find lists that belong together.

#### Automatic import of catalogues
*Actor:* System

To ensure that your systems are always up to date, there are different options of automatically importing catalogues. There are several ways to ensure that you always have the latest lists in your system. The variants presented here focus on updating a specific terminology.

Basically, the following URL template can always be used to retrieve the latest version of a terminology:

    https://gitlab.com/elga-gmbh/termgit/-/raw/main/terminologies/<NAME_OF_TERMINOLOGY>/<NAME_OF_TERMINOLOGY><FORMAT>

Here the
- `<NAME_OF_TERMINOLOGY>` corresponds to the type of the terminology (`CodeSystem` or `ValueSet`) and the name of the terminology (e.g. `iso-3166-1-alpha-3`). The type and name are linked with `-` resulting in `CodeSystem-iso-3166-1-alpha-3`.
- `<FORMAT>` corresponds to the file extension of one of the offered formats (see [Download Formats](#download)), e.g. `.4.fhir.xml`

The result then looks like this:

    https://gitlab.com/elga-gmbh/termgit/-/raw/main/terminologies/CodeSystem-iso-3166-1-alpha-3/CodeSystem-iso-3166-1-alpha-3.4.fhir.xml

Based on a particular format of a terminology, different types of retrieval are presented in the next points:
1. As a stable https link to the file, without versioning details:
   * Python:

         response = urllib2.urlopen('https://gitlab.com/elga-gmbh/termgit/-/raw/main/terminologies/CodeSystem-iso-3166-1-alpha-3/CodeSystem-iso-3166-1-alpha-3.2.claml.xml?inline=false')
   * Java:

         URL url = new URL("https://gitlab.com/elga-gmbh/termgit/-/raw/main/terminologies/CodeSystem-iso-3166-1-alpha-3/CodeSystem-iso-3166-1-alpha-3.2.claml.xml?inline=false");
         URLConnection connection = url.openConnection();
         InputStream is = connection.getInputStream();
         // .. then download the file
2. With git via ssh within a local repository and the Git-tags for the corresponding terminology:
   * First clone of the repository:

         git clone git@gitlab.com:elga-gmbh/termgit.git

   * Update of the local Git repository (incl. Git-tags):

         git fetch --tags -f

   * Checking the difference between current local directory content and that of the Git-tag of a terminology once the repository has been updated:

         git log HEAD..tags/CodeSytem-iso-3166-1-alpha-3

     or

         git log HEAD..tags/1.0.3166.1.2.3

   * Checkout the latest version of a terminology with the corresponding Git-tag. **Note:** This will set the entire repository to the state of the Git-tag. Other terminologies might have a more recent state.

         git checkout tags/CodeSystem-iso-3166-1-alpha-3

     or

         git checkout tags/1.0.3166.1.2.3

3. With git via ssh without a local repository and the Git-tags for the corresponding terminology-folder (no support for receiving specific files):

       git archive -o C:/tmp/CS-ISO-3166-1-alpha-3.zip --remote git@gitlab.com:elga-gmbh/termgit.git CodeSystem-iso-3166-1-alpha-3:terminologies/CodeSystem-iso-3166-1-alpha-3

   or

       git archive -o C:/tmp/CS-ISO-3166-1-alpha-3.zip --remote git@gitlab.com:elga-gmbh/termgit.git 1.0.3166.1.2.3:terminologies/CodeSystem-iso-3166-1-alpha-3

4. With GitLab API via REST for a specific format (here .2.claml.xml; '/' needs to be escaped through '%2f')

       curl https://gitlab.com/api/v4/projects/33179072/repository/files/terminologies%2fCodeSystem-iso-3166-1-alpha-3%2fCodeSystem-iso-3166-1-alpha-3.2.claml.xml?ref=main

5. With FHIR via REST (FHIR Server comes with alpha release in Q3 2022):
   * curl:

         curl https://<FHIR SERVER NAME>.at/CodeSystem/CodeSystem-iso-3166-1-alpha-3

   * for more details see [http://www.hl7.org/fhir/overview-dev.html#Interactions](http://www.hl7.org/fhir/overview-dev.html#Interactions)
   * for testing purposes you can use, e.g.:

         curl http://hapi.fhir.org/baseR4/CodeSystem/2559241

##### Retrieving meta information of a Git-tag.

The [GitLab API for Git Tags](https://docs.gitlab.com/ee/api/tags.html) can be used for automated checking whether a particular terminology has been updated. The following command can be used to retrieve the metadata of a Git-tag:

    curl https://gitlab.com/api/v4/projects/33179072/repository/tags/CodeSystem-iso-3166-1-alpha-3

Among other things, the metadata includes the date the Git-tag was created (`created_at`).

#### Manual updating of terminologies
*Actor:* Master data/system admin

In addition to the automatic import of catalogues, it is also possible to manually update all the terminologies. For this, we recommend activating the tag news in GitLab. You will then receive an automatic notification for each change.

[![tag-abonnieren](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/Tag-abonnieren.png "tag-Neuigkeiten-abonnieren"){: style="width: 100%"}](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/Tag-abonnieren.png)

[![manuelles-mapping](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/automatische-benachrichtigung.png "Manuelles-Mapping"){: style="width: 100%"}](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/automatische-benachrichtigung.png)

#### Validation of codes
*Actor:* System

The system can also use the new Austrian e-Health Terminology Server to check whether a particular code is part of a code system or value set. This works with the FHIR operation [`$validate-code` for Code Systems](http://www.hl7.org/fhir/codesystem-operation-validate-code.html) and [`$validate-code` for Value Sets](http://www.hl7.org/fhir/valueset-operation-validate-code.html).

[![validate-code](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/validate-code.png){: style="width: 100%"}](https://gitlab.com/elga-gmbh/termgit/-/raw/dev/input/images/validate-code.png)

### Download tab with all download formats

The following download formats are currently offered:

| Name | File extension | Note |
| ----------- | ----------- | ----------- |
| FHIR R4 xml | `.4.fhir.xml` | |
| FHIR R4 json | `.4.fhir.json` | |
| fsh v1 | `.1.fsh` | |
| fsh v2 | `.2.fsh` | |
| ClaML v2 | `.2.claml.xml` | |
| ClaML v3 | `.3.claml.xml` | |
| propCSV v1 | `.1.propcsv.csv` | |
| SVSextELGA v1 | `.1.svsextelga.xml` | **Warning! DEPRECATED!**<br/>This format can technically not contain all the information<br/>that is available in the other formats.<br/>This format is only available for legacy purposes. |
| SVSextELGA v2 | `.2.svsextelga.xml`   | **Warning! DEPRECATED!**<br/>This format can technically not contain all the information<br/>that is available in the other formats.<br/>This format is only available for legacy purposes. <br/>However, all concept properties are available. |
| outdatedCSV v1 | `.1.outdatedcsv.csv` | **Warning! DEPRECATED!**<br/>This format can technically not contain all the information<br/>that is available in the other formats.<br/>This format is only available for legacy purposes. |
| outdatedCSV v2 | `.2.outdatedcsv.csv`  | **Warning! DEPRECATED!**<br/>This format can technically not contain all the information<br/>that is available in the other formats.<br/>This format is only available for legacy purposes. <br/>However, all concept properties are available. |


#### FHIR R4 XML
The XML representation for a resource is described using this format [^6]:

     <name xmlns="http://hl7.org/fhir" (attrA="value")>
       <!-- from Resource: id, meta, implicitRules, and language -->
       <nameA><!--  1..1 type description of content  --><nameA>
       <nameB[x]><!-- 0..1 type1|type1 description  --></nameB[x]>
       <nameC> <!--  1..* -->
         <nameD><!-- 1..1 type>Relevant elements  --></nameD>
       </nameC>
     <name>

#### FHIR R4 JSON
The JSON representation for a resource is based on the [JSON format described in STD 90 (RFC 8259)](https://www.rfc-editor.org/info/std90) , and is described using this format [^7]:

    {
      "resourceType" : "[Resource Type]",
      // from Source: property0
      "property1" : "<[primitive]>", // short description
      "property2" : { [Data Type] }, // short description
      "property3" : { // Short Description
        "propertyA" : { CodeableConcept }, // Short Description (Example)
      },
      "property4" : [{ // Short Description
        "propertyB" : { Reference(ResourceType) } // R!  Short Description
      }]
    }

#### fsh v1 and v2
FHIR Shorthand (FSH) is a domain-specific language for defining the contents of FHIR Implementation Guides (IG). The language is specifically designed for this purpose, simple and compact, and allows the author to express their intent with fewer concerns about underlying FHIR mechanics. FSH can be created and updated using any text editor, and because it is text, it enables distributed, team-based development using source code control tools such as GitHub [^5].

#### Proprietary CSV
The proprietary CSV is used by terminology administrators to easily edit terminologies. This format is a set of FHIR metadata elements and concept elements. The FHIR specification defines the mandatory elements for code systems (http://hl7.org/fhir/codesystem.html#resource) and value sets (http://hl7.org/fhir/valueset.html#resource) - the designations are also predefined and may not be changed.

The value of the designation is converted into the respective FHIR data types (uri, identifier, string, code, ...) and stored if the conversion was successful. No other metadata elements or concept elements can be quoted unless a custom extension is created for CodeSystem/ValueSet.

The metadata elements and concept elements can be specified in any order. According to the key-value-principle always the key is read, which gives the meaning to the value standing under it.

The following metadata elements are desired for the **ELGA code systems**:

| Name | Cardinality | Descpirtion |
| --- | --- | --- |
| resource | **1..1** | declares the existence of and describes a code system or code system supplement and its key properties, and optionally defines a part or all of its content. Also known as Ontology, Terminology, or Enumeration |
| url | 0..1 | Canonical identifier for this code system, represented as a URI (globally unique) (Coding.system) |
| identifier | 0..* | Additional identifier for the code system (business identifier) all attributes are combined by a pipe in the import file e.g.\|\|urn:oid:1.25.256.25.8.6\|\|\| |
| * use | 0..1 | usual, official, temp, secondary, old (If known) |
| * type | 0..1 | Description of identifier |
| * system | 0..1 | The namespace for the identifier value |
| * value | 0..1 | The value that is unique |
| * period | 0..1 | Time period when id is/was valid for use |
| * assigner | 0..1 | Organization that issued id (may be just text) |
| date | 0..1 | Date last changed. The format is YYYY, YYYY-MM, YYYY-MM-DD or YYYY-MM-DDThh:mm:ss+zz:zz, e.g. 2018, 1973-06, 1905-08-23, 2015-02-07T13:28:17-05:00 or 2017-01-01T00:00:00.000Z. |
| version | 0..1 | Business version of the code system (Coding.version) |
| name | 0..1 | Name for this code system (computer friendly) |
| title | 0..1 | Name for this code system (human friendly) |
| status | **1..1** | draft, active, retired, unknown |
| description | 0..1 | Natural language description of the code system |
| content | **1..1** | not-present, example, fragment, complete, supplement |
| copyright | 0..1 | Use and/or publishing restrictions |

The following concept elements are desired for the **ELGA code systems**:

| Name | Cardinality | Descpirtion |
| --- | --- | --- |
| code | **1..1** | Code that identifies concept |
| display | 0..1 | Text to display to the user |
| designation | 0..* | Additional representations for the concept - all three attributes are combined by a pipe in the import file e.g. de\|Fully specified name\|Bronchitis |
| * language | 0..1 | Human language of the designation - Common Languages (Preferred: de) |
| * use | 0..1 | Details how this designation would be used - Designation Use (Fully specified name or Synonym) |
| * value | **1..1** | The text value for this designation |
| definition | 0..1 | Formal definition |
| property | 0..* | Property value for the concept - both attributes are combined by a pipe in the import file e.g. child\|101 |
| * code | 1..1 | Reference to CodeSystem.property.code |
| * value | 1..1 | Value of the property for this concept |

The following metadata elements are desired for the **ELGA value sets**:

| Name | Cardinality | Descpirtion |
| --- | --- | --- |
| resource | **1..1** | declares the existence of and describes a code system or code system supplement and its key properties, and optionally defines a part or all of its content. Also known as Ontology, Terminology, or Enumeration |
| url | 0..1 | Canonical identifier for this value set, represented as a URI (globally unique) |
| identifier | 0..* | Additional identifier for the value set (business identifier) all attributes are combined by a pipe in the import file e.g.\|\|urn:oid:1.25.256.25.8.6\|\|\| |
| * use | 0..1 | usual, official, temp, secondary, old (If known) |
| * type | 0..1 | Description of identifier |
| * system | 0..1 | The namespace for the identifier value |
| * value | 0..1 | The value that is unique |
| * period | 0..1 | Time period when id is/was valid for use |
| * assigner | 0..1 | Organization that issued id (may be just text) |
| date | 0..1 | Date last changed. The format is YYYY, YYYY-MM, YYYY-MM-DD or YYYY-MM-DDThh:mm:ss+zz:zz, e.g. 2018, 1973-06, 1905-08-23, 2015-02-07T13:28:17-05:00 or 2017-01-01T00:00:00.000Z. |
| version | 0..1 | Business version of the code system (Coding.version) |
| name | 0..1 | Name for this code system (computer friendly) |
| title | 0..1 | Name for this code system (human friendly) |
| status | **1..1** | draft, active, retired, unknown |
| description | 0..1 | Natural language description of the code system |
| copyright | 0..1 | Use and/or publishing restrictions |

The following concept elements are desired for the **ELGA value sets**:

| Name | Cardinality | Descpirtion |
| --- | --- | --- |
| system | 0..1 | The system the codes come from e.g. urn:oid:1.34.2.0.12.56.35 |
| code | **1..1** | Code or expression from system |
| display | 0..1 | Text to display for this code for this value set in this valueset |
| designation | 0..* | Additional representations for the concept - all three attributes are combined by a pipe in the import file e.g. de\|Fully specified name\|Bronchitis |
| * language | 0..1 | Human language of the designation - Common Languages (Preferred: de) |
| * use | 0..1 | Details how this designation would be used - Designation Use (Fully specified name or Synonym) |
| * value | 1..1 | The text value for this designation |
| filter | 0..* | Select codes/concepts by their properties (including relationships) - all three attributes are combined by a pipe in the import file e.g. child\|not-in\|101 |
| * property | 1..1 | A property/filter defined by the code system |
| * op | 1..1 | =, is-a, descendent-of, is-not-a, regex, in, not-in, generalizes, exists |
| * value | 1..1 | Code from the system, or regex criteria, or boolean value for exists |
| exclude | 0..1 | Explicitly exclude codes from a code system or other value sets - means that this concept should be excluded (false/0 oder true/1) |


#### ClaML v2 and v3
ClaML (Classification Markup Language) is a special XML data format for classifications. The ClaML export is only available for code lists. Information on terminology or versions is contained in the first elements. The OID is found in the "uid" attribute of the Identifier element. Further information on the terminology can be found in the first "meta" elements as a name-value pair:
Description: German description of the terminology.
* Description_eng: English description of the terminology.
* Website: Link to source etc.
* version_description: description of the version
* insert_ts: date of import/creation on terminology server
* status_date: "status changed on" date
* expiration_date: "valid until" date
* last_change_date: "changed on" date
* gueltigkeitsbereich
* statusCode: 0=>Suggestion; 1=>Public; 2=>Obsolet
* unvollständig (for code lists): flag to indicate code lists that are only published in part on the terminology server
* verantw_Org (for code lists): responsible organisation

All other version information is in the title element:
* tite@date: "valid-from" date
* tite@name: name of the terminology
* tite@version: name or number of the version.

The individual concepts are mapped as class elements, each with name-value
pairs:
* class/@code: Code
* \<Rubric kind='preferred'>/Label: term
* \<Rubric kind='note'>/Label: more detailed description (application description) of the concept
* TS_ATTRIBUTE_HINTS: hints on the application of the concept
* TS_ATTRIBUTE_MEANING: German language variant, meaning
* TS_ATTRIBUTE_STATUS/ TS_ATTRIBUTE_STATUSUPDATE: Information on the status of the individual concept.
* Level/Type: mapping of the hierarchy
* Relationships: any connections to other concepts.

Note here that the elements are not present in the export file if they are not populated on the terminology server.

#### SVSextELGA
The SVS offered is an extended IHE Sharing Value Set format. In this XML format, code lists and value sets can be exported.
This contains attributes for the terminology or the version (element "valueSet"):
* Name: Designation of the terminology
* Description of the terminology
* effectiveDate: "valid-from" date of the version
* Id: OID of the version
* statusCode (currently not supported)
* website: Link to source etc.
* version: number/designation of the version
* version-description (for code lists): description of the version
* validity range
* statusCode: 0=>Suggestion: 1=>Public; 2=>Obsolet
* last_change_date: date of last change

All concepts are present as "concept" elements in the "conceptList" and contain the following attributes:
* code
* codeSystem: OID of the source code system of the concept
* displayName: term
* concept_beschreibung: more detailed description (application description) of the concept
* deutsch: German language variant, meaning
* einheit_codiert, einheit_print: only relevant for laboratory parameters
* hinweis: Notes on the application of the concept
* level/type: mapping of the hierarchy
* relationships: possible connections to other concepts
* orderNumber (only for value sets): index to fix the order
* conceptStatus: 0=>Suggestion; 1=>Public; 2=>Obsolet
* unvollständig (for code lists): flag to indicate code lists that are only published in excerpts on the terminology server
* verantw_Org (for code lists): responsible organisation

####outdatedcsv
The "outdatedcsv" format is the csv export format of the previous terminology server (termpub.gv.at). The format does not contain any metadata on code systems or value sets and cannot represent all the information required for FHIR that is available in other formats and is therefore not suitable for FHIR.
For each concept, only the following information can be represented in "outdatedcsv":
* code
* codeSystem
* displayName
* parentCodeSystemName
* concept_Beschreibung
* meaning
* hints
* orderNumber
* level
* type
* relationships
* einheit print
* einheit codiert

**This format is intended to facilitate the changeover to the new terminology server (termgit.gv.at). Further use is explicitly discouraged, as this format is no longer being developed.**


### Current state of development

The following two open source projects serve to further develop the processes and software required to provide TerminoloGit.

| Project Name | Description | Repository URL | GitLab Project ID | GitLab Pages URL |
| --- | --- | --- | --- | --- |
| TerminoloGit Dev | [https://gitlab.com/elga-gmbh/termgit-dev](https://gitlab.com/elga-gmbh/termgit-dev) | Repository for TerminoloGit technical development.<br/><br/>*Note:* The contents of the last successful pipeline of any Git branch of this repository are displayed under the specified GitLab Pages URL. | 21743825 | [https://elga-gmbh.gitlab.io/termgit-dev/](https://elga-gmbh.gitlab.io/termgit-dev/) |
| TerminoloGit Dev HTML | [https://gitlab.com/elga-gmbh/terminologit-dev-html](https://gitlab.com/elga-gmbh/terminologit-dev-html) | Repository for the static HTML pages created by the HL7® FHIR® IG Publisher based on the `master` branch of [https://gitlab.com/elga-gmbh/termgit-dev](https://gitlab.com/elga-gmbh/termgit-dev). | 28239847 | [https://dev.termgit.elga.gv.at](https://dev.termgit.elga.gv.at) |


### Architecture

Basic knowledge of Git and GitLab is assumed. Specifically important for TerminoloGit are:
* Git as a distributed version control system (each developer has a complete copy of the repository).
	* Branching/Merging (parallel, independent development on different components)
	* Tags
	* Logs
* GitLab as UI
	* Merge Requests
	* CI/CD with GitLab Runner
	* User/rights management

Basic knowledge of FHIR and its tools is assumed. Specifically important for TerminoloGit are:
* FHIR combines the advantages of the established HL7 standard product lines version 2, version 3 and CDA with current web standards.
	* data formats and elements as so-called "resources".
	* strong focus on ease of implementation
	* Web-based API technologies, such as the HTTP-based programming paradigm REST, HTML, TLS and OAUTH2
	* representation of data as JSON as well as XML
	* direct access to individual information fields as a service
* FHIR IG Publisher as a tool for creating static HTML pages representing the entire content of an implementation guide
	* User-defined content as well as FHIR resources can be processed.
* FSH (FHIR Shorthand) + SUSHI (SUSHI Unshortens Short Hands Inputs)
	* FSH allows simplified definition of FHIR resources, primarily for definition of FHIR IGs.
	* SUSHI interprets/compiles FSH files and creates FHIR resources out of them.
* FHIR Server, when used for terminologies only, this is called FHIR tx (Terminology Exchange).
	* REST API
	* Operations (e.g. $validate-code)
	* Versioning of resources

#### Fully dressed use case diagram

The entire concept is shown in a fully dressed use case diagram, below you will find the written use cases at subfunction/fish level. Please note that this is not fully equivalent to the Cockburn diagram for fully dressed use case diagrams. A few changes have been made to show all the needed information in one diagram.

[![fully dressed use case diagram](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/FullyDressedUseCaseDiagramm.png){: style="width: 100%"}](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/FullyDressedUseCaseDiagramm.png)

##### Parameters for all use cases:

1. __Scope:__
    * a terminology like a codesystem or valueset

2. __Level:__
    * Subfunction/Fish

##### 1. Create Account, Authenticate and Authorisate

1. __Actors:__
    * Terminology user (human)
    * GitLab Service (machine)

1. __Brief:__ The terminology user needs an account for special interactions like subscribing to a tag of a terminology via GitLab and creates one via GitLab.

1. __Postconditions:__
    1. __Minimal Guarantees:__
        * if the user authorisation by ELGA GmbH is not done
            * waiting till the authorisation process is complete
    2. __Success Guarantees:__
        * A user account with individual access rights has been created.

1. __Preconditions:__
    * The GitLab projects needed for TerminoloGit have been created

1. __Triggers:__
    * A terminology user wants to perform a special interaction

1. __Basic flow:__
    1. Creating an account on GitLab and if needed requesting individual permissions
    1. Login in GitLab

1. __Extensions:__ none

1. __Software:__
    * Any browser

1. __Hardware:__
    * Any client with a browser

1. __Services:__
    * GitLab.com or own GitLab CE hosting

##### 2. Subscription

1. __Actors:__
    * GitLab Service (machine)

1. __Brief:__ The subscribers are being informed per mail, because a new or edited terminology-file has been pushed.

1. __Postconditions:__
    1. __Minimal Guarantees:__ none
    2. __Success Guarantees:__
        * A subscriber is being informed of a new terminology

1. __Preconditions:__
    * The GitLab project TerminoloGit has been subscribed by GitLab users

1. __Triggers:__
    * A terminology has been added or updated

1. __Basic flow:__
    1. GitLab sends the change to all subscribers

1. __Extensions:__ none

1. __Software:__
    * Any mail program

1. __Hardware:__
    * Any client with a mail program

1. __Services:__
    * GitLab for sending the change to subscribers

##### 3. Using FHIR IG and FHIR-tx-at

1. __Actors:__
   * Terminology user (human or machine)
   * GitLab Service (machine)

1. __Brief:__ A terminology like a codesystem or valueset is being retrieved via the GitLab GUI or TerminoloGit GUI or GitLab REST interface or FHIR TX REST interface

1. __Postconditions:__
    1. __Minimal Guarantees:__
        * receiving any feedback
    2. __Success Guarantees:__
        * if the terminology the user is looking for exists
            * receiving a terminology

1. __Preconditions:__
    * The TerminoloGit GitLab project exists
    * The FHIR IG is published
    * The FHIR-tx-at is up to date

1. __Triggers:__
    * A terminology needs to be retrieved

1. __Basic flow:__
    1. By requesting the FHIR IG web side you get a browsable format of all terminologies, with the possibility to download them in any format
	1. By requesting the GitLab web side you get a list of all formats of all terminologies, with the possibility to download them
    1. By operating with worldwide standardized FHIR ValueSet or CodeSystems REST operations the terminologies are retrieved by a machine
	1. By operating with the GitLab REST API terminologies are retrieved by a machine

1. __Extensions:__
    * A validation of concepts in ressources ValueSet or CodeSystem is possible by $validate-code.
    * A hierarchy status between two concepts in a CodeSystem is possible by $subsumes.
    * A detailed view of a concept in a CodeSystem is possible by $lookup.

1. __Software:__
    * A browser or rest-client

1. __Hardware:__
    * Client for browsing or sending rest-operations

1. __Services:__
    * GitLab for publishing terminologies
    * a FHIR server for rest-operations

##### 4. CRUD and push a terminology to GitLab

1. __Actors:__
    * Terminology Manager (human)
    * GitLab Service (machine)

1. __Brief:__ A new terminology-file is being created or edited and pushed to the GitLab project

1. __Postconditions:__
    1. __Minimal Guarantees:__
        * If the commit succeeds (e.g. no merge conflicts):
            * The new, edited or as retired flagged terminology-file is published in the TerminoloGit GitLab project. It's versioned by git and by the own IGVer programm
            * The continuous delivery is starting and triggering the next steps
    2. __Success Guarantees:__
        * By pushing a feedback is returning from the git-client

1. __Preconditions:__
    * The TerminoloGit GitLab project exists
    * A local git client and an editor is installed on a client or a WebIDE of GitLab is used

1. __Triggers:__
    * A terminology needs to be created or updated

1. __Basic flow:__
    1. With an editor like Notepad++ or Visual Studio Code or a WebIDE a terminology-file in the local git repository is being updated or created or a deleted by setting the status to retired.
    1. In your local git client a new commit is being created and pushed to the remote repository TerminoloGit

1. __Extensions:__
    * A branch beside of master could indicate that a new commit is pushed for testing or approval purposes. In this case the Continuous Delivery of GitLab is the same as for master, with the only difference that the result is only visible on an own site

1. __Software:__
    * a local git client i.e. SourceTree (free git GUI) or a browser
    * an editor i.e. Notepad++ or Visual Studio Code (free code editors) or a browser

1. __Hardware:__
    * Client for editing text files and git pushing or browsing

1. __Services:__
    * GitLab for publishing terminologies

##### 5. Continuous Delivery to FHIR IG and FHIR-tx-at

1. __Actors:__
    * GitLab Service (machine)

1. __Brief:__ A new or edited terminology-file is being processed to other terminology formats from the GitLab service by a runner

1. __Postconditions:__
    1. __Minimal Guarantees:__
        * If there is no continuous delivery error:
            * The new or edited terminology-files are being converted by MaLaC-CT into all the other formats
            * The FHIR IG is being built with links to all formats
            * The FHIR-tx-at is being updated with the new terminologies
    2. __Success Guarantees:__
        * A message is returning from the GitLab runner

1. __Preconditions:__
    * The TerminoloGit GitLab CI is configured

1. __Triggers:__
    * A terminology has been added or updated

1. __Basic flow:__
    1. a runner for the continuous delivery is being started because a new commit has been pushed
    1. the runner is converting the terminology to all other formats i.e. fsh, FHIR, ClaMl, SVS, the predefined CSV
    1. if the conversion passed, the IGVer and afterwards the IG Publisher is started
    1. if the IG Publisher passed, the upload to FHIR-tx-at is starting

1. __Extensions:__ none

1. __Software:__ none

1. __Hardware:__ none

1. __Services:__
    * GitLab for running the runner

##### 6. Push to branches, approve and merge to master

1. __Actors:__
    * Terminology Manager (human)
    * GitLab Service (machine)

1. __Brief:__ A new or edited terminology-files are being committed to a branch and requested for a merge to master.

1. __Postconditions:__
    1. __Minimal Guarantees:__
        * If a correction is needed:
            * waiting that the terminology manager corrects with a new commit
    2. __Success Guarantees:__
        * a branch has been merged to master

1. __Preconditions:__
    * The TerminoloGit project is configured for branching

1. __Triggers:__
    * A terminology has been added or updated to a branch

1. __Basic flow:__
    1. the terminology manager creates a branch for his commits
    1. the terminology manager creates a commit with his changes
    1. the terminology manager requests a merge to master or another protected branch
    1. a terminology administrator comments the request and demands changes or accepts this branch and merges it into master

1. __Extensions:__ none

1. __Software:__
    * a browser

1. __Hardware:__
    * a machine with a browser

1. __Services:__
    * GitLab for running the runner

#### BigPicture

Knowing the rough architecture through the fully dressed use case diagram, the following BigPicture shows the different ways to use TerminoloGit, including the possible cascading system dependencies.

[![big-picture](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/BigPicture.png){: style="width: 100%"}](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/BigPicture.png)

#### CI/CD flow

The following is a visualization of the three most common flows of the CI/CD pipeline. For more details, refer to the .gitlab-ci.yml.

##### standard-case terminology-maintenance

[![CI-CD-normal-case](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/CICD_norm.png){: style="width: 100%"}](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/CICD_norm.png)

##### integration-test

[![CI-CD-integration-test](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/CICD_inttest.png){: style="width: 100%"}](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/CICD_inttest.png)

##### SKIP_MALAC

[![CI-CD-skip-malac](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/CICD_skip_malac.png){: style="width: 100%"}](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/CICD_skip_malac.png)

### Setup/Installation

0. Create a GitLab.com User
1. Fork from a exisiting TerminoloGit Project
1.1. If you need a fresh Repository with only two reference Terminologies, fork from
https://gitlab.com/elga-gmbh/termgit-dev & https://gitlab.com/elga-gmbh/termgit-dev-html
1.2. If you need all published terminologies, fork from   https://gitlab.com/elga-gmbh/termgit & https://gitlab.com/elga-gmbh/termgit-html
2. Configure CI/CD
2.1. Enable under settings - CI/CD - runner the public runners
2.1. Confirm your Gitlab.com-User with a credit card (there will not be anything debited, this is only because of crypto-miner using free gitlab-runner with their bots)
2.3. Create under settings - CI/CD - variables following
 * [username]_GITLAB_CI_TOKEN = [create under User - preferences - access tokens a token with the name: GITLAB_CI and the scope: api & read_api and use that token]
 * TERMGIT_CANONICAL = https://[username].gitlab.io/[Projectname]
 * TERMGIT_HTML_PROJECT = [username]/[HTML-Projectname]
 * TERMGIT_HTML_PROJECT_DEFAULT_BRANCH = main
 * UPLOAD_ARTIFACTS = true
3. Delete all unneeded terminologies and add your needed terminologies
observe your Pipeline run and enjoy your first published TerminoloGit under https://[username].gitlab.io/[Projectname]


### "Previous Versions" tab with the display of old versions
Old versions of a terminology can be accessed via the "Previous Versions" tab. The table shown contains the following columns:

- `Version Number`: This column contains the version number of the respective version of the terminology.
- `Current Version vs. Outdated Version`: By means of the [W3C HTML Diff Tool](https://services.w3.org/htmldiff) the differences between the old and current version of the terminology can be displayed.

If there are no old versions for a terminology, the table is empty.

In the display an old version is recognizable by the following banner. This banner can also be used to switch directly to the current version.

[![old-version-banner](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/old-version-banner.png){: style="width: 100%"}](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/old-version-banner.png)

### Cascading synchronization of own instances

Cascading synchronization is possible manually with the Git function Submodule or Subtree. Furthermore, this is possible automatically via CI/CD in your own system, if you do not perform any adaptations/extensions to the terminologies yourself.

For the TerGi implementation in Q3 2022, further ways for cascading synchronization between TerminoloGit and TerGi instances will be investigated and implemented. This documentation will be updated according to the results.

Currently, a dedicated FHIR 4.0.1 (R4) server https://hub.docker.com/r/ibmcom/ibm-fhir-server exists for internal testing and conversion support, based on the open source project https://github.com/LinuxForHealth/FHIR. For TerGi instances, a free choice of FHIR server will be possible.

### References

[^1]: FHIR Short Hand, described in [http://hl7.org/fhir/uv/shorthand/2020May/](http://hl7.org/fhir/uv/shorthand/2020May/)
[^2]: FHIR XML, described in [http://www.hl7.org/fhir/xml.html](http://www.hl7.org/fhir/xml.html)
[^3]: FHIR JSON, described in [http://www.hl7.org/fhir/json.html](http://www.hl7.org/fhir/json.html)
