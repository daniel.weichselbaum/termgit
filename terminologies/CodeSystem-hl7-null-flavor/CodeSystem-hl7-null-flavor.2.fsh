Instance: hl7-null-flavor 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-null-flavor" 
* name = "hl7-null-flavor" 
* title = "HL7 Null Flavor" 
* status = #active 
* content = #complete 
* version = "202006" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.5.1008" 
* date = "2020-06-08" 
* count = 14 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #NI "NoInformation"
* #NI ^property[0].code = #child 
* #NI ^property[0].valueCode = #INV 
* #NI ^property[1].code = #child 
* #NI ^property[1].valueCode = #MSK 
* #NI ^property[2].code = #child 
* #NI ^property[2].valueCode = #NA 
* #NI ^property[3].code = #child 
* #NI ^property[3].valueCode = #UNK 
* #INV "invalid"
* #INV ^property[0].code = #parent 
* #INV ^property[0].valueCode = #NI 
* #INV ^property[1].code = #child 
* #INV ^property[1].valueCode = #DER 
* #INV ^property[2].code = #child 
* #INV ^property[2].valueCode = #OTH 
* #DER "derived"
* #DER ^property[0].code = #parent 
* #DER ^property[0].valueCode = #INV 
* #OTH "Other"
* #OTH ^property[0].code = #parent 
* #OTH ^property[0].valueCode = #INV 
* #OTH ^property[1].code = #child 
* #OTH ^property[1].valueCode = #NINF 
* #OTH ^property[2].code = #child 
* #OTH ^property[2].valueCode = #PINF 
* #NINF "Negative infinity"
* #NINF ^property[0].code = #parent 
* #NINF ^property[0].valueCode = #OTH 
* #PINF "Positive infinity"
* #PINF ^property[0].code = #parent 
* #PINF ^property[0].valueCode = #OTH 
* #MSK "Masked"
* #MSK ^property[0].code = #parent 
* #MSK ^property[0].valueCode = #NI 
* #NA "Not applicable"
* #NA ^property[0].code = #parent 
* #NA ^property[0].valueCode = #NI 
* #UNK "Unknown"
* #UNK ^property[0].code = #parent 
* #UNK ^property[0].valueCode = #NI 
* #UNK ^property[1].code = #child 
* #UNK ^property[1].valueCode = #ASKU 
* #UNK ^property[2].code = #child 
* #UNK ^property[2].valueCode = #NASK 
* #UNK ^property[3].code = #child 
* #UNK ^property[3].valueCode = #QS 
* #UNK ^property[4].code = #child 
* #UNK ^property[4].valueCode = #TRC 
* #ASKU "Asked, but unknown"
* #ASKU ^property[0].code = #parent 
* #ASKU ^property[0].valueCode = #UNK 
* #ASKU ^property[1].code = #child 
* #ASKU ^property[1].valueCode = #NAV 
* #NAV "Temporarily unavailable"
* #NAV ^property[0].code = #parent 
* #NAV ^property[0].valueCode = #ASKU 
* #NASK "Not asked"
* #NASK ^property[0].code = #parent 
* #NASK ^property[0].valueCode = #UNK 
* #QS "Sufficient Quantity"
* #QS ^property[0].code = #parent 
* #QS ^property[0].valueCode = #UNK 
* #TRC "Trace"
* #TRC ^property[0].code = #parent 
* #TRC ^property[0].valueCode = #UNK 
