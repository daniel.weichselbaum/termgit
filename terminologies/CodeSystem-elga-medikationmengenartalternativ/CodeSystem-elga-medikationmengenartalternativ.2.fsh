Instance: elga-medikationmengenartalternativ 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-medikationmengenartalternativ" 
* name = "elga-medikationmengenartalternativ" 
* title = "ELGA_MedikationMengenartAlternativ" 
* status = #active 
* content = #complete 
* version = "202009" 
* description = "**Description:** Codelist of alternative units of measure (for countable units).

**Beschreibung:** Codeliste für alternative Mengenarten (zählbare Einheiten)." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.10.1.4.3.4.3.2" 
* date = "2020-09-18" 
* count = 2 
* #{HUB} "Hub"
* #{TAB} "Tablette"
