Instance: elga-laborparameterergaenzung 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-laborparameterergaenzung" 
* name = "elga-laborparameterergaenzung" 
* title = "ELGA_LaborparameterErgaenzung" 
* status = #active 
* content = #complete 
* version = "202209" 
* description = "**Description:** Codes for structuring and laboratory parameters which are used in Austria, but where no official LOINC-Codes are existent. This codes are used within the Value Sets ELGA_Laborparameter and ELGA_Laborstruktur

**Beschreibung:** Codes für die Gliederung und Laborparameter, die in Österreich verwendet werden und für die es keinen LOINC gibt. Verwendung im ValueSet ELGA_Laborparameter und  ELGA_Laborstruktur" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.11" 
* date = "2022-09-06" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* count = 769 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* property[2].code = #status 
* property[2].type = #code 
* property[3].code = #Relationships 
* property[3].type = #string 
* property[4].code = #hints 
* property[4].type = #string 
* #1 "Allgemeiner Laborbefund"
* #1 ^property[0].code = #child 
* #1 ^property[0].valueCode = #100 
* #1 ^property[1].code = #child 
* #1 ^property[1].valueCode = #1000 
* #1 ^property[2].code = #child 
* #1 ^property[2].valueCode = #1100 
* #1 ^property[3].code = #child 
* #1 ^property[3].valueCode = #1300 
* #1 ^property[4].code = #child 
* #1 ^property[4].valueCode = #1400 
* #1 ^property[5].code = #child 
* #1 ^property[5].valueCode = #1500 
* #1 ^property[6].code = #child 
* #1 ^property[6].valueCode = #1600 
* #1 ^property[7].code = #child 
* #1 ^property[7].valueCode = #1800 
* #1 ^property[8].code = #child 
* #1 ^property[8].valueCode = #200 
* #1 ^property[9].code = #child 
* #1 ^property[9].valueCode = #2300 
* #1 ^property[10].code = #child 
* #1 ^property[10].valueCode = #2500 
* #1 ^property[11].code = #child 
* #1 ^property[11].valueCode = #300 
* #1 ^property[12].code = #child 
* #1 ^property[12].valueCode = #400 
* #1 ^property[13].code = #child 
* #1 ^property[13].valueCode = #500 
* #1 ^property[14].code = #child 
* #1 ^property[14].valueCode = #600 
* #1 ^property[15].code = #child 
* #1 ^property[15].valueCode = #900 
* #100 "Blutgruppenserologie"
* #100 ^property[0].code = #parent 
* #100 ^property[0].valueCode = #1 
* #100 ^property[1].code = #child 
* #100 ^property[1].valueCode = #01850 
* #100 ^property[2].code = #child 
* #100 ^property[2].valueCode = #01860 
* #100 ^property[3].code = #child 
* #100 ^property[3].valueCode = #01870 
* #01850 "Blutgruppenserologie"
* #01850 ^property[0].code = #parent 
* #01850 ^property[0].valueCode = #100 
* #01850 ^property[1].code = #child 
* #01850 ^property[1].valueCode = #V00432-7 
* #01850 ^property[2].code = #child 
* #01850 ^property[2].valueCode = #V00433-5 
* #01850 ^property[3].code = #child 
* #01850 ^property[3].valueCode = #V00434-3 
* #01850 ^property[4].code = #child 
* #01850 ^property[4].valueCode = #V00435-0 
* #V00432-7 "Blutgruppe AB0 Kontr"
* #V00432-7 ^definition = Blutgruppe AB0 Kontr
* #V00432-7 ^designation[0].language = #de-AT 
* #V00432-7 ^designation[0].value = "Blutgruppe AB0 Kontrolle" 
* #V00432-7 ^property[0].code = #parent 
* #V00432-7 ^property[0].valueCode = #01850 
* #V00433-5 "Blutgruppe AB0+Rh Bt"
* #V00433-5 ^definition = Blutgruppe AB0+Rh Bt
* #V00433-5 ^designation[0].language = #de-AT 
* #V00433-5 ^designation[0].value = "Blutgruppe AB0 + Rhesusfaktor Bestätigung" 
* #V00433-5 ^property[0].code = #parent 
* #V00433-5 ^property[0].valueCode = #01850 
* #V00434-3 "Blutgr. AB0+Rh Kontr"
* #V00434-3 ^definition = Blutgr. AB0+Rh Kontr
* #V00434-3 ^designation[0].language = #de-AT 
* #V00434-3 ^designation[0].value = "Blutgruppe AB0 + Rhesusfaktor Kontrolle" 
* #V00434-3 ^property[0].code = #parent 
* #V00434-3 ^property[0].valueCode = #01850 
* #V00435-0 "Rhesusfaktor Kontr."
* #V00435-0 ^definition = Rhesusfaktor Kontr.
* #V00435-0 ^designation[0].language = #de-AT 
* #V00435-0 ^designation[0].value = "Rhesusfaktor Kontrolle" 
* #V00435-0 ^property[0].code = #parent 
* #V00435-0 ^property[0].valueCode = #01850 
* #01860 "HLA-Diagnostik"
* #01860 ^property[0].code = #parent 
* #01860 ^property[0].valueCode = #100 
* #01870 "HPA-Diagnostik"
* #01870 ^property[0].code = #parent 
* #01870 ^property[0].valueCode = #100 
* #1000 "Medikamente"
* #1000 ^property[0].code = #parent 
* #1000 ^property[0].valueCode = #1 
* #1000 ^property[1].code = #child 
* #1000 ^property[1].valueCode = #08260 
* #1000 ^property[2].code = #child 
* #1000 ^property[2].valueCode = #08270 
* #1000 ^property[3].code = #child 
* #1000 ^property[3].valueCode = #08280 
* #1000 ^property[4].code = #child 
* #1000 ^property[4].valueCode = #08290 
* #1000 ^property[5].code = #child 
* #1000 ^property[5].valueCode = #08300 
* #1000 ^property[6].code = #child 
* #1000 ^property[6].valueCode = #08310 
* #1000 ^property[7].code = #child 
* #1000 ^property[7].valueCode = #08320 
* #08260 "Antibiotika"
* #08260 ^property[0].code = #parent 
* #08260 ^property[0].valueCode = #1000 
* #08270 "Virostatika"
* #08270 ^property[0].code = #parent 
* #08270 ^property[0].valueCode = #1000 
* #08270 ^property[1].code = #child 
* #08270 ^property[1].valueCode = #V00545-6 
* #08270 ^property[2].code = #child 
* #08270 ^property[2].valueCode = #V00546-4 
* #V00545-6 "Elvitegravir"
* #V00545-6 ^definition = Elvitegravir
* #V00545-6 ^designation[0].language = #de-AT 
* #V00545-6 ^designation[0].value = "Elvitegravir" 
* #V00545-6 ^property[0].code = #parent 
* #V00545-6 ^property[0].valueCode = #08270 
* #V00545-6 ^property[1].code = #status 
* #V00545-6 ^property[1].valueCode = #retired 
* #V00545-6 ^property[2].code = #Relationships 
* #V00545-6 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00545-6 ^property[3].code = #hints 
* #V00545-6 ^property[3].valueString = "DEPRECATED" 
* #V00546-4 "Maraviroc"
* #V00546-4 ^definition = Maraviroc
* #V00546-4 ^designation[0].language = #de-AT 
* #V00546-4 ^designation[0].value = "Maraviroc" 
* #V00546-4 ^property[0].code = #parent 
* #V00546-4 ^property[0].valueCode = #08270 
* #V00546-4 ^property[1].code = #status 
* #V00546-4 ^property[1].valueCode = #retired 
* #V00546-4 ^property[2].code = #Relationships 
* #V00546-4 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00546-4 ^property[3].code = #hints 
* #V00546-4 ^property[3].valueString = "DEPRECATED" 
* #08280 "Antiepileptika"
* #08280 ^property[0].code = #parent 
* #08280 ^property[0].valueCode = #1000 
* #08280 ^property[1].code = #child 
* #08280 ^property[1].valueCode = #V00582-9 
* #08280 ^property[2].code = #child 
* #08280 ^property[2].valueCode = #V00673-6 
* #08280 ^property[3].code = #child 
* #08280 ^property[3].valueCode = #V00674-4 
* #V00582-9 "Eslicarbazepin"
* #V00582-9 ^definition = Eslicarbazepin
* #V00582-9 ^designation[0].language = #de-AT 
* #V00582-9 ^designation[0].value = "Eslicarbazepin" 
* #V00582-9 ^property[0].code = #parent 
* #V00582-9 ^property[0].valueCode = #08280 
* #V00673-6 "Brivaracetam"
* #V00673-6 ^definition = Brivaracetam
* #V00673-6 ^designation[0].language = #de-AT 
* #V00673-6 ^designation[0].value = "Brivaracetam" 
* #V00673-6 ^property[0].code = #parent 
* #V00673-6 ^property[0].valueCode = #08280 
* #V00673-6 ^property[1].code = #status 
* #V00673-6 ^property[1].valueCode = #retired 
* #V00673-6 ^property[2].code = #Relationships 
* #V00673-6 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00673-6 ^property[3].code = #hints 
* #V00673-6 ^property[3].valueString = "DEPRECATED" 
* #V00674-4 "Perampanel"
* #V00674-4 ^definition = Perampanel
* #V00674-4 ^designation[0].language = #de-AT 
* #V00674-4 ^designation[0].value = "Perampanel" 
* #V00674-4 ^property[0].code = #parent 
* #V00674-4 ^property[0].valueCode = #08280 
* #V00674-4 ^property[1].code = #status 
* #V00674-4 ^property[1].valueCode = #retired 
* #V00674-4 ^property[2].code = #Relationships 
* #V00674-4 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00674-4 ^property[3].code = #hints 
* #V00674-4 ^property[3].valueString = "DEPRECATED" 
* #08290 "Psychopharmaka"
* #08290 ^property[0].code = #parent 
* #08290 ^property[0].valueCode = #1000 
* #08290 ^property[1].code = #child 
* #08290 ^property[1].valueCode = #V00544-9 
* #08290 ^property[2].code = #child 
* #08290 ^property[2].valueCode = #V00547-2 
* #08290 ^property[3].code = #child 
* #08290 ^property[3].valueCode = #V00548-0 
* #08290 ^property[4].code = #child 
* #08290 ^property[4].valueCode = #V00549-8 
* #08290 ^property[5].code = #child 
* #08290 ^property[5].valueCode = #V00550-6 
* #08290 ^property[6].code = #child 
* #08290 ^property[6].valueCode = #V00551-4 
* #08290 ^property[7].code = #child 
* #08290 ^property[7].valueCode = #V00637-1 
* #08290 ^property[8].code = #child 
* #08290 ^property[8].valueCode = #V00638-9 
* #08290 ^property[9].code = #child 
* #08290 ^property[9].valueCode = #V00704-9 
* #08290 ^property[10].code = #child 
* #08290 ^property[10].valueCode = #V00705-6 
* #08290 ^property[11].code = #child 
* #08290 ^property[11].valueCode = #V00706-4 
* #08290 ^property[12].code = #child 
* #08290 ^property[12].valueCode = #V00707-2 
* #08290 ^property[13].code = #child 
* #08290 ^property[13].valueCode = #V00708-0 
* #08290 ^property[14].code = #child 
* #08290 ^property[14].valueCode = #V00710-6 
* #08290 ^property[15].code = #child 
* #08290 ^property[15].valueCode = #V00711-4 
* #08290 ^property[16].code = #child 
* #08290 ^property[16].valueCode = #V00761-9 
* #08290 ^property[17].code = #child 
* #08290 ^property[17].valueCode = #V00762-7 
* #08290 ^property[18].code = #child 
* #08290 ^property[18].valueCode = #V00764-3 
* #V00544-9 "Amisulprid"
* #V00544-9 ^definition = Amisulprid
* #V00544-9 ^designation[0].language = #de-AT 
* #V00544-9 ^designation[0].value = "Amisulprid" 
* #V00544-9 ^property[0].code = #parent 
* #V00544-9 ^property[0].valueCode = #08290 
* #V00544-9 ^property[1].code = #status 
* #V00544-9 ^property[1].valueCode = #retired 
* #V00544-9 ^property[2].code = #Relationships 
* #V00544-9 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00544-9 ^property[3].code = #hints 
* #V00544-9 ^property[3].valueString = "DEPRECATED" 
* #V00547-2 "Melperon"
* #V00547-2 ^definition = Melperon
* #V00547-2 ^designation[0].language = #de-AT 
* #V00547-2 ^designation[0].value = "Melperon" 
* #V00547-2 ^property[0].code = #parent 
* #V00547-2 ^property[0].valueCode = #08290 
* #V00547-2 ^property[1].code = #status 
* #V00547-2 ^property[1].valueCode = #retired 
* #V00547-2 ^property[2].code = #Relationships 
* #V00547-2 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00547-2 ^property[3].code = #hints 
* #V00547-2 ^property[3].valueString = "DEPRECATED" 
* #V00548-0 "Perazin"
* #V00548-0 ^definition = Perazin
* #V00548-0 ^designation[0].language = #de-AT 
* #V00548-0 ^designation[0].value = "Perazin" 
* #V00548-0 ^property[0].code = #parent 
* #V00548-0 ^property[0].valueCode = #08290 
* #V00548-0 ^property[1].code = #status 
* #V00548-0 ^property[1].valueCode = #retired 
* #V00548-0 ^property[2].code = #Relationships 
* #V00548-0 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00548-0 ^property[3].code = #hints 
* #V00548-0 ^property[3].valueString = "DEPRECATED" 
* #V00549-8 "Reboxetin"
* #V00549-8 ^definition = Reboxetin
* #V00549-8 ^designation[0].language = #de-AT 
* #V00549-8 ^designation[0].value = "Reboxetin" 
* #V00549-8 ^property[0].code = #parent 
* #V00549-8 ^property[0].valueCode = #08290 
* #V00549-8 ^property[1].code = #status 
* #V00549-8 ^property[1].valueCode = #retired 
* #V00549-8 ^property[2].code = #Relationships 
* #V00549-8 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00549-8 ^property[3].code = #hints 
* #V00549-8 ^property[3].valueString = "DEPRECATED" 
* #V00550-6 "Sertindol"
* #V00550-6 ^definition = Sertindol
* #V00550-6 ^designation[0].language = #de-AT 
* #V00550-6 ^designation[0].value = "Sertindol" 
* #V00550-6 ^property[0].code = #parent 
* #V00550-6 ^property[0].valueCode = #08290 
* #V00550-6 ^property[1].code = #status 
* #V00550-6 ^property[1].valueCode = #retired 
* #V00550-6 ^property[2].code = #Relationships 
* #V00550-6 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00550-6 ^property[3].code = #hints 
* #V00550-6 ^property[3].valueString = "DEPRECATED" 
* #V00551-4 "Zotepin"
* #V00551-4 ^definition = Zotepin
* #V00551-4 ^designation[0].language = #de-AT 
* #V00551-4 ^designation[0].value = "Zotepin" 
* #V00551-4 ^property[0].code = #parent 
* #V00551-4 ^property[0].valueCode = #08290 
* #V00551-4 ^property[1].code = #status 
* #V00551-4 ^property[1].valueCode = #retired 
* #V00551-4 ^property[2].code = #Relationships 
* #V00551-4 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00551-4 ^property[3].code = #hints 
* #V00551-4 ^property[3].valueString = "DEPRECATED" 
* #V00637-1 "Dehydroaripiprazol"
* #V00637-1 ^definition = Dehydroaripiprazol
* #V00637-1 ^designation[0].language = #de-AT 
* #V00637-1 ^designation[0].value = "Dehydroaripiprazol" 
* #V00637-1 ^property[0].code = #parent 
* #V00637-1 ^property[0].valueCode = #08290 
* #V00637-1 ^property[1].code = #status 
* #V00637-1 ^property[1].valueCode = #retired 
* #V00637-1 ^property[2].code = #Relationships 
* #V00637-1 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00637-1 ^property[3].code = #hints 
* #V00637-1 ^property[3].valueString = "DEPRECATED" 
* #V00638-9 "Norquetiapin"
* #V00638-9 ^definition = Norquetiapin
* #V00638-9 ^designation[0].language = #de-AT 
* #V00638-9 ^designation[0].value = "Norquetiapin" 
* #V00638-9 ^property[0].code = #parent 
* #V00638-9 ^property[0].valueCode = #08290 
* #V00638-9 ^property[1].code = #status 
* #V00638-9 ^property[1].valueCode = #retired 
* #V00638-9 ^property[2].code = #Relationships 
* #V00638-9 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00638-9 ^property[3].code = #hints 
* #V00638-9 ^property[3].valueString = "DEPRECATED" 
* #V00704-9 "Clomethiazol"
* #V00704-9 ^definition = Clomethiazol
* #V00704-9 ^designation[0].language = #de-AT 
* #V00704-9 ^designation[0].value = "Clomethiazol" 
* #V00704-9 ^property[0].code = #parent 
* #V00704-9 ^property[0].valueCode = #08290 
* #V00704-9 ^property[1].code = #status 
* #V00704-9 ^property[1].valueCode = #retired 
* #V00704-9 ^property[2].code = #Relationships 
* #V00704-9 ^property[2].valueString = "verwendbar bis 30.06.2019" 
* #V00704-9 ^property[3].code = #hints 
* #V00704-9 ^property[3].valueString = "DEPRECATED" 
* #V00705-6 "Opipramol"
* #V00705-6 ^definition = Opipramol
* #V00705-6 ^designation[0].language = #de-AT 
* #V00705-6 ^designation[0].value = "Opipramol" 
* #V00705-6 ^property[0].code = #parent 
* #V00705-6 ^property[0].valueCode = #08290 
* #V00705-6 ^property[1].code = #status 
* #V00705-6 ^property[1].valueCode = #retired 
* #V00705-6 ^property[2].code = #Relationships 
* #V00705-6 ^property[2].valueString = "verwendbar bis 30.06.2019" 
* #V00705-6 ^property[3].code = #hints 
* #V00705-6 ^property[3].valueString = "DEPRECATED" 
* #V00706-4 "Tianeptin"
* #V00706-4 ^definition = Tianeptin
* #V00706-4 ^designation[0].language = #de-AT 
* #V00706-4 ^designation[0].value = "Tianeptin" 
* #V00706-4 ^property[0].code = #parent 
* #V00706-4 ^property[0].valueCode = #08290 
* #V00706-4 ^property[1].code = #status 
* #V00706-4 ^property[1].valueCode = #retired 
* #V00706-4 ^property[2].code = #Relationships 
* #V00706-4 ^property[2].valueString = "verwendbar bis 30.06.2019" 
* #V00706-4 ^property[3].code = #hints 
* #V00706-4 ^property[3].valueString = "DEPRECATED" 
* #V00707-2 "Vilazodon"
* #V00707-2 ^definition = Vilazodon
* #V00707-2 ^designation[0].language = #de-AT 
* #V00707-2 ^designation[0].value = "Vilazodon" 
* #V00707-2 ^property[0].code = #parent 
* #V00707-2 ^property[0].valueCode = #08290 
* #V00707-2 ^property[1].code = #status 
* #V00707-2 ^property[1].valueCode = #retired 
* #V00707-2 ^property[2].code = #Relationships 
* #V00707-2 ^property[2].valueString = "verwendbar bis 30.06.2019" 
* #V00707-2 ^property[3].code = #hints 
* #V00707-2 ^property[3].valueString = "DEPRECATED" 
* #V00708-0 "Vortioxetin"
* #V00708-0 ^definition = Vortioxetin
* #V00708-0 ^designation[0].language = #de-AT 
* #V00708-0 ^designation[0].value = "Vortioxetin" 
* #V00708-0 ^property[0].code = #parent 
* #V00708-0 ^property[0].valueCode = #08290 
* #V00708-0 ^property[1].code = #status 
* #V00708-0 ^property[1].valueCode = #retired 
* #V00708-0 ^property[2].code = #Relationships 
* #V00708-0 ^property[2].valueString = "verwendbar bis 30.06.2019" 
* #V00708-0 ^property[3].code = #hints 
* #V00708-0 ^property[3].valueString = "DEPRECATED" 
* #V00710-6 "Erythro-Dihydrobupropion"
* #V00710-6 ^definition = Erythro-DH-Bupropion
* #V00710-6 ^designation[0].language = #de-AT 
* #V00710-6 ^designation[0].value = "Erythro-Dihydrobupropion" 
* #V00710-6 ^property[0].code = #parent 
* #V00710-6 ^property[0].valueCode = #08290 
* #V00710-6 ^property[1].code = #status 
* #V00710-6 ^property[1].valueCode = #retired 
* #V00710-6 ^property[2].code = #Relationships 
* #V00710-6 ^property[2].valueString = "verwendbar bis 31.12.2020" 
* #V00710-6 ^property[3].code = #hints 
* #V00710-6 ^property[3].valueString = "DEPRECATED" 
* #V00711-4 "Threo-Dihydrobupropion"
* #V00711-4 ^definition = Threo-DH-Bupropion
* #V00711-4 ^designation[0].language = #de-AT 
* #V00711-4 ^designation[0].value = "Threo-Dihydrobupropion" 
* #V00711-4 ^property[0].code = #parent 
* #V00711-4 ^property[0].valueCode = #08290 
* #V00711-4 ^property[1].code = #status 
* #V00711-4 ^property[1].valueCode = #retired 
* #V00711-4 ^property[2].code = #Relationships 
* #V00711-4 ^property[2].valueString = "verwendbar bis 31.12.2020" 
* #V00711-4 ^property[3].code = #hints 
* #V00711-4 ^property[3].valueString = "DEPRECATED" 
* #V00761-9 "Brexpiprazol"
* #V00761-9 ^definition = Brexpiprazol
* #V00761-9 ^designation[0].language = #de-AT 
* #V00761-9 ^designation[0].value = "Brexpiprazol" 
* #V00761-9 ^property[0].code = #parent 
* #V00761-9 ^property[0].valueCode = #08290 
* #V00761-9 ^property[1].code = #status 
* #V00761-9 ^property[1].valueCode = #retired 
* #V00761-9 ^property[2].code = #Relationships 
* #V00761-9 ^property[2].valueString = "verwendbar bis 30.06.2023" 
* #V00761-9 ^property[3].code = #hints 
* #V00761-9 ^property[3].valueString = "DEPRECATED" 
* #V00762-7 "Cariprazin"
* #V00762-7 ^definition = Cariprazin
* #V00762-7 ^designation[0].language = #de-AT 
* #V00762-7 ^designation[0].value = "Cariprazin" 
* #V00762-7 ^property[0].code = #parent 
* #V00762-7 ^property[0].valueCode = #08290 
* #V00762-7 ^property[1].code = #status 
* #V00762-7 ^property[1].valueCode = #retired 
* #V00762-7 ^property[2].code = #Relationships 
* #V00762-7 ^property[2].valueString = "verwendbar bis 30.06.2023" 
* #V00762-7 ^property[3].code = #hints 
* #V00762-7 ^property[3].valueString = "DEPRECATED" 
* #V00764-3 "Prothipendyl"
* #V00764-3 ^definition = Prothipendyl
* #V00764-3 ^designation[0].language = #de-AT 
* #V00764-3 ^designation[0].value = "Prothipendyl" 
* #V00764-3 ^property[0].code = #parent 
* #V00764-3 ^property[0].valueCode = #08290 
* #V00764-3 ^property[1].code = #status 
* #V00764-3 ^property[1].valueCode = #retired 
* #V00764-3 ^property[2].code = #Relationships 
* #V00764-3 ^property[2].valueString = "verwendbar bis 30.06.2023" 
* #V00764-3 ^property[3].code = #hints 
* #V00764-3 ^property[3].valueString = "DEPRECATED" 
* #08300 "Kardiaka"
* #08300 ^property[0].code = #parent 
* #08300 ^property[0].valueCode = #1000 
* #08300 ^property[1].code = #child 
* #08300 ^property[1].valueCode = #V00763-5 
* #V00763-5 "Guanfacin"
* #V00763-5 ^definition = Guanfacin
* #V00763-5 ^designation[0].language = #de-AT 
* #V00763-5 ^designation[0].value = "Guanfacin" 
* #V00763-5 ^property[0].code = #parent 
* #V00763-5 ^property[0].valueCode = #08300 
* #V00763-5 ^property[1].code = #status 
* #V00763-5 ^property[1].valueCode = #retired 
* #V00763-5 ^property[2].code = #Relationships 
* #V00763-5 ^property[2].valueString = "verwendbar bis 30.06.2023" 
* #V00763-5 ^property[3].code = #hints 
* #V00763-5 ^property[3].valueString = "DEPRECATED" 
* #08310 "Immunsuppressiva"
* #08310 ^property[0].code = #parent 
* #08310 ^property[0].valueCode = #1000 
* #08320 "Medikamente Sonstiges"
* #08320 ^property[0].code = #parent 
* #08320 ^property[0].valueCode = #1000 
* #08320 ^property[1].code = #child 
* #08320 ^property[1].valueCode = #V00768-4 
* #V00768-4 "Emicizumab"
* #V00768-4 ^definition = Emicizumab
* #V00768-4 ^designation[0].language = #de-AT 
* #V00768-4 ^designation[0].value = "Emicizumab" 
* #V00768-4 ^property[0].code = #parent 
* #V00768-4 ^property[0].valueCode = #08320 
* #V00768-4 ^property[1].code = #status 
* #V00768-4 ^property[1].valueCode = #retired 
* #V00768-4 ^property[2].code = #Relationships 
* #V00768-4 ^property[2].valueString = "verwendbar bis 31.12.2022" 
* #V00768-4 ^property[3].code = #hints 
* #V00768-4 ^property[3].valueString = "DEPRECATED" 
* #1100 "Infektionsdiagnostik"
* #1100 ^property[0].code = #parent 
* #1100 ^property[0].valueCode = #1 
* #1100 ^property[1].code = #child 
* #1100 ^property[1].valueCode = #10780 
* #1100 ^property[2].code = #child 
* #1100 ^property[2].valueCode = #10790 
* #1100 ^property[3].code = #child 
* #1100 ^property[3].valueCode = #10800 
* #1100 ^property[4].code = #child 
* #1100 ^property[4].valueCode = #10810 
* #1100 ^property[5].code = #child 
* #1100 ^property[5].valueCode = #10820 
* #10780 "Virologie"
* #10780 ^property[0].code = #parent 
* #10780 ^property[0].valueCode = #1100 
* #10780 ^property[1].code = #child 
* #10780 ^property[1].valueCode = #V00017 
* #10780 ^property[2].code = #child 
* #10780 ^property[2].valueCode = #V00018 
* #10780 ^property[3].code = #child 
* #10780 ^property[3].valueCode = #V00169 
* #10780 ^property[4].code = #child 
* #10780 ^property[4].valueCode = #V00170 
* #10780 ^property[5].code = #child 
* #10780 ^property[5].valueCode = #V00250 
* #10780 ^property[6].code = #child 
* #10780 ^property[6].valueCode = #V00251 
* #10780 ^property[7].code = #child 
* #10780 ^property[7].valueCode = #V00274 
* #10780 ^property[8].code = #child 
* #10780 ^property[8].valueCode = #V00275 
* #10780 ^property[9].code = #child 
* #10780 ^property[9].valueCode = #V00280 
* #10780 ^property[10].code = #child 
* #10780 ^property[10].valueCode = #V00305-5 
* #10780 ^property[11].code = #child 
* #10780 ^property[11].valueCode = #V00442-6 
* #10780 ^property[12].code = #child 
* #10780 ^property[12].valueCode = #V00466-5 
* #10780 ^property[13].code = #child 
* #10780 ^property[13].valueCode = #V00489-7 
* #10780 ^property[14].code = #child 
* #10780 ^property[14].valueCode = #V00563-9 
* #10780 ^property[15].code = #child 
* #10780 ^property[15].valueCode = #V00564-7 
* #10780 ^property[16].code = #child 
* #10780 ^property[16].valueCode = #V00566-2 
* #10780 ^property[17].code = #child 
* #10780 ^property[17].valueCode = #V00567-0 
* #10780 ^property[18].code = #child 
* #10780 ^property[18].valueCode = #V00568-8 
* #10780 ^property[19].code = #child 
* #10780 ^property[19].valueCode = #V00592-8 
* #10780 ^property[20].code = #child 
* #10780 ^property[20].valueCode = #V00593-6 
* #10780 ^property[21].code = #child 
* #10780 ^property[21].valueCode = #V00594-4 
* #10780 ^property[22].code = #child 
* #10780 ^property[22].valueCode = #V00595-1 
* #10780 ^property[23].code = #child 
* #10780 ^property[23].valueCode = #V00596-9 
* #10780 ^property[24].code = #child 
* #10780 ^property[24].valueCode = #V00597-7 
* #10780 ^property[25].code = #child 
* #10780 ^property[25].valueCode = #V00598-5 
* #10780 ^property[26].code = #child 
* #10780 ^property[26].valueCode = #V00599-3 
* #10780 ^property[27].code = #child 
* #10780 ^property[27].valueCode = #V00600-9 
* #10780 ^property[28].code = #child 
* #10780 ^property[28].valueCode = #V00601-7 
* #10780 ^property[29].code = #child 
* #10780 ^property[29].valueCode = #V00602-5 
* #10780 ^property[30].code = #child 
* #10780 ^property[30].valueCode = #V00603-3 
* #10780 ^property[31].code = #child 
* #10780 ^property[31].valueCode = #V00715-5 
* #10780 ^property[32].code = #child 
* #10780 ^property[32].valueCode = #V00716-3 
* #10780 ^property[33].code = #child 
* #10780 ^property[33].valueCode = #V00717-1 
* #10780 ^property[34].code = #child 
* #10780 ^property[34].valueCode = #V00718-9 
* #10780 ^property[35].code = #child 
* #10780 ^property[35].valueCode = #V00721-3 
* #10780 ^property[36].code = #child 
* #10780 ^property[36].valueCode = #V00722-1 
* #10780 ^property[37].code = #child 
* #10780 ^property[37].valueCode = #V00723-9 
* #10780 ^property[38].code = #child 
* #10780 ^property[38].valueCode = #V00724-7 
* #10780 ^property[39].code = #child 
* #10780 ^property[39].valueCode = #V00725-4 
* #10780 ^property[40].code = #child 
* #10780 ^property[40].valueCode = #V00726-2 
* #10780 ^property[41].code = #child 
* #10780 ^property[41].valueCode = #V00727-0 
* #10780 ^property[42].code = #child 
* #10780 ^property[42].valueCode = #V00728-8 
* #10780 ^property[43].code = #child 
* #10780 ^property[43].valueCode = #V00731-2 
* #10780 ^property[44].code = #child 
* #10780 ^property[44].valueCode = #V00735-3 
* #V00017 "FSME-AK IgG qn./Liquor"
* #V00017 ^definition = FSME-V.-AK IgG/L qn.
* #V00017 ^designation[0].language = #de-AT 
* #V00017 ^designation[0].value = "FSME-Virus-Antikörper IgG /Liquor quant." 
* #V00017 ^property[0].code = #parent 
* #V00017 ^property[0].valueCode = #10780 
* #V00018 "FSME-AK IgM qn./Liquor"
* #V00018 ^definition = FSME-V.-AK IgM/L qn.
* #V00018 ^designation[0].language = #de-AT 
* #V00018 ^designation[0].value = "FSME-Virus-Antikörper IgM /Liquor quant." 
* #V00018 ^property[0].code = #parent 
* #V00018 ^property[0].valueCode = #10780 
* #V00169 "HPV E6/E7 Onkogen mRNA"
* #V00169 ^definition = HPV E6/E7  mRNA
* #V00169 ^designation[0].language = #de-AT 
* #V00169 ^designation[0].value = "Humanes Papilloma-Virus E6/E7 Onkogen mRNA" 
* #V00169 ^property[0].code = #parent 
* #V00169 ^property[0].valueCode = #10780 
* #V00170 "HPV sonstige Hochrisiko Genotypen DNA"
* #V00170 ^definition = HPV H.Risiko andere
* #V00170 ^designation[0].language = #de-AT 
* #V00170 ^designation[0].value = "HPV sonstige Hochrisiko Genotypen DNA" 
* #V00170 ^property[0].code = #parent 
* #V00170 ^property[0].valueCode = #10780 
* #V00250 "Neurotrope Viren /L."
* #V00250 ^definition = Neurotrope Viren /L.
* #V00250 ^designation[0].language = #de-AT 
* #V00250 ^designation[0].value = "Neurotrope Viren /Liquor" 
* #V00250 ^property[0].code = #parent 
* #V00250 ^property[0].valueCode = #10780 
* #V00251 "Virusb. Organtrans."
* #V00251 ^definition = Virusb. Organtrans.
* #V00251 ^designation[0].language = #de-AT 
* #V00251 ^designation[0].value = "Virusblock Organtransplantation" 
* #V00251 ^property[0].code = #parent 
* #V00251 ^property[0].valueCode = #10780 
* #V00274 "HDV RNA PCR qn."
* #V00274 ^definition = HDV-RNA PCR qn.
* #V00274 ^designation[0].language = #de-AT 
* #V00274 ^designation[0].value = "HDV-RNA PCR qn." 
* #V00274 ^property[0].code = #parent 
* #V00274 ^property[0].valueCode = #10780 
* #V00274 ^property[1].code = #status 
* #V00274 ^property[1].valueCode = #retired 
* #V00274 ^property[2].code = #Relationships 
* #V00274 ^property[2].valueString = "verwendbar bis 30.06.2017" 
* #V00274 ^property[3].code = #hints 
* #V00274 ^property[3].valueString = "DEPRECATED" 
* #V00275 "HEV RNA PCR qn."
* #V00275 ^definition = HEV-RNA PCR qn.
* #V00275 ^designation[0].language = #de-AT 
* #V00275 ^designation[0].value = "HEV-RNA PCR qn." 
* #V00275 ^property[0].code = #parent 
* #V00275 ^property[0].valueCode = #10780 
* #V00275 ^property[1].code = #status 
* #V00275 ^property[1].valueCode = #retired 
* #V00275 ^property[2].code = #Relationships 
* #V00275 ^property[2].valueString = "verwendbar bis 30.06.2018" 
* #V00275 ^property[3].code = #hints 
* #V00275 ^property[3].valueString = "DEPRECATED" 
* #V00280 "Kommentar Virologie"
* #V00280 ^definition = Komm.Virologie
* #V00280 ^designation[0].language = #de-AT 
* #V00280 ^designation[0].value = "Kommentar Virologie" 
* #V00280 ^property[0].code = #parent 
* #V00280 ^property[0].valueCode = #10780 
* #V00305-5 "Influenza H1N1 RNA/SM PCR"
* #V00305-5 ^definition = Infl.H1N1-RNA/SM PCR
* #V00305-5 ^designation[0].language = #de-AT 
* #V00305-5 ^designation[0].value = "Infl.H1N1-RNA/SM PCR" 
* #V00305-5 ^property[0].code = #parent 
* #V00305-5 ^property[0].valueCode = #10780 
* #V00442-6 "FSME RNA /Blut PCR"
* #V00442-6 ^definition = FSME-RNA /Blut PCR
* #V00442-6 ^designation[0].language = #de-AT 
* #V00442-6 ^designation[0].value = "FSME-RNA /Blut PCR" 
* #V00442-6 ^property[0].code = #parent 
* #V00442-6 ^property[0].valueCode = #10780 
* #V00466-5 "Humanes Parechovirus RNA qn. /PCR SM"
* #V00466-5 ^definition = HPeV RNA PCR qn. /SM
* #V00466-5 ^designation[0].language = #de-AT 
* #V00466-5 ^designation[0].value = "Humanes Parechovirus RNA PCR qn. /Sondermaterial" 
* #V00466-5 ^property[0].code = #parent 
* #V00466-5 ^property[0].valueCode = #10780 
* #V00489-7 "Parvo Virus B19 DNA AC qn./SM PCR"
* #V00489-7 ^definition = PV B19-DNA AC qn/SM
* #V00489-7 ^designation[0].language = #de-AT 
* #V00489-7 ^designation[0].value = "Parvo-Virus B19-DNA AC quant./Sondermaterial  PCR" 
* #V00489-7 ^property[0].code = #parent 
* #V00489-7 ^property[0].valueCode = #10780 
* #V00563-9 "Human-Herpes-Virus 8-DNA /B ql. PCR"
* #V00563-9 ^definition = HHV8-DNA /B ql. PCR
* #V00563-9 ^designation[0].language = #de-AT 
* #V00563-9 ^designation[0].value = "Human-Herpes-Virus 8-DNA /Blut qualitativ PCR" 
* #V00563-9 ^property[0].code = #parent 
* #V00563-9 ^property[0].valueCode = #10780 
* #V00564-7 "EBV-AK IgM ql. /L"
* #V00564-7 ^definition = EBV-AK IgM ql. /L
* #V00564-7 ^designation[0].language = #de-AT 
* #V00564-7 ^designation[0].value = "EBV-AK IgM qualitativ /Liquor" 
* #V00564-7 ^property[0].code = #parent 
* #V00564-7 ^property[0].valueCode = #10780 
* #V00566-2 "HSV IgG AK-Index"
* #V00566-2 ^definition = HSV IgG AK-Ind.
* #V00566-2 ^designation[0].language = #de-AT 
* #V00566-2 ^designation[0].value = "HSV IgG AK-Index" 
* #V00566-2 ^property[0].code = #parent 
* #V00566-2 ^property[0].valueCode = #10780 
* #V00567-0 "Varizella-Zoster-Virus IgG AK-Index"
* #V00567-0 ^definition = VZV IgG AK-Ind.
* #V00567-0 ^designation[0].language = #de-AT 
* #V00567-0 ^designation[0].value = "Varizella-Zoster-Virus IgG AK-Index" 
* #V00567-0 ^property[0].code = #parent 
* #V00567-0 ^property[0].valueCode = #10780 
* #V00567-0 ^property[1].code = #status 
* #V00567-0 ^property[1].valueCode = #retired 
* #V00567-0 ^property[2].code = #Relationships 
* #V00567-0 ^property[2].valueString = "verwendbar bis 31.12.2021" 
* #V00567-0 ^property[3].code = #hints 
* #V00567-0 ^property[3].valueString = "DEPRECATED" 
* #V00568-8 "EBV IgG AK-Index"
* #V00568-8 ^definition = EBV IgG AK-Ind.
* #V00568-8 ^designation[0].language = #de-AT 
* #V00568-8 ^designation[0].value = "EBV IgG AK-Index" 
* #V00568-8 ^property[0].code = #parent 
* #V00568-8 ^property[0].valueCode = #10780 
* #V00592-8 "Adenovirus-AK IgA"
* #V00592-8 ^definition = Adenovirus-AK IgA
* #V00592-8 ^designation[0].language = #de-AT 
* #V00592-8 ^designation[0].value = "Adenovirus-Antikörper IgA" 
* #V00592-8 ^property[0].code = #parent 
* #V00592-8 ^property[0].valueCode = #10780 
* #V00593-6 "Adenovirus-AK IgA /L"
* #V00593-6 ^definition = Adenovirus-AK IgA /L
* #V00593-6 ^designation[0].language = #de-AT 
* #V00593-6 ^designation[0].value = "Adenovirus-Antikörper IgA /Liquor" 
* #V00593-6 ^property[0].code = #parent 
* #V00593-6 ^property[0].valueCode = #10780 
* #V00594-4 "Adenovirus-AK IgG /L"
* #V00594-4 ^definition = Adenovirus-AK IgG /L
* #V00594-4 ^designation[0].language = #de-AT 
* #V00594-4 ^designation[0].value = "Adenovirus-Antikörper IgG /Liquor" 
* #V00594-4 ^property[0].code = #parent 
* #V00594-4 ^property[0].valueCode = #10780 
* #V00595-1 "Coxsackievirus-AK IgG"
* #V00595-1 ^definition = Coxsackiev.-AK IgG
* #V00595-1 ^designation[0].language = #de-AT 
* #V00595-1 ^designation[0].value = "Coxsackievirus-Antikörper IgG" 
* #V00595-1 ^property[0].code = #parent 
* #V00595-1 ^property[0].valueCode = #10780 
* #V00596-9 "Coxsackievirus-AK IgM"
* #V00596-9 ^definition = Coxsackiev.-AK IgM
* #V00596-9 ^designation[0].language = #de-AT 
* #V00596-9 ^designation[0].value = "Coxsackievirus-Antikörper IgM" 
* #V00596-9 ^property[0].code = #parent 
* #V00596-9 ^property[0].valueCode = #10780 
* #V00597-7 "Coxsackievirus-AK IgG /L"
* #V00597-7 ^definition = Coxsackiev-AK IgG /L
* #V00597-7 ^designation[0].language = #de-AT 
* #V00597-7 ^designation[0].value = "Coxsackievirus-Antikörper IgG /Liquor" 
* #V00597-7 ^property[0].code = #parent 
* #V00597-7 ^property[0].valueCode = #10780 
* #V00598-5 "Coxsackievirus-AK IgM /L"
* #V00598-5 ^definition = Coxsackiev-AK IgM /L
* #V00598-5 ^designation[0].language = #de-AT 
* #V00598-5 ^designation[0].value = "Coxsackievirus-Antikörper IgM /Liquor" 
* #V00598-5 ^property[0].code = #parent 
* #V00598-5 ^property[0].valueCode = #10780 
* #V00599-3 "Parainfluenzavirus(1,2,3)-AK IgG"
* #V00599-3 ^definition = P.-Inf(1,2,3)-AK IgG
* #V00599-3 ^designation[0].language = #de-AT 
* #V00599-3 ^designation[0].value = "Parainfluenzavirus(1,2,3)-Antikörper IgG" 
* #V00599-3 ^property[0].code = #parent 
* #V00599-3 ^property[0].valueCode = #10780 
* #V00600-9 "Parainfluenzavirus(1,2,3)-AK IgA"
* #V00600-9 ^definition = P.-Inf(1,2,3)-AK IgA
* #V00600-9 ^designation[0].language = #de-AT 
* #V00600-9 ^designation[0].value = "Parainfluenzavirus(1,2,3)-Antikörper IgA" 
* #V00600-9 ^property[0].code = #parent 
* #V00600-9 ^property[0].valueCode = #10780 
* #V00601-7 "HSV-1/2-AK IgG Titer /L"
* #V00601-7 ^definition = HSV-1/2-AK IgG Ti /L
* #V00601-7 ^designation[0].language = #de-AT 
* #V00601-7 ^designation[0].value = "HSV-1/2-AK IgG Titer /Liquor" 
* #V00601-7 ^property[0].code = #parent 
* #V00601-7 ^property[0].valueCode = #10780 
* #V00602-5 "Masern AK-Index"
* #V00602-5 ^definition = Masern AK-Index
* #V00602-5 ^designation[0].language = #de-AT 
* #V00602-5 ^designation[0].value = "Masern AK-Index" 
* #V00602-5 ^property[0].code = #parent 
* #V00602-5 ^property[0].valueCode = #10780 
* #V00603-3 "Mumps AK-Index"
* #V00603-3 ^definition = Mumps AK-Index
* #V00603-3 ^designation[0].language = #de-AT 
* #V00603-3 ^designation[0].value = "Mumps AK-Index" 
* #V00603-3 ^property[0].code = #parent 
* #V00603-3 ^property[0].valueCode = #10780 
* #V00715-5 "Coronavirus HKU1 RNA ql. /NP PCR"
* #V00715-5 ^definition = CoV HKU1 RNA ql/NP P
* #V00715-5 ^designation[0].language = #de-AT 
* #V00715-5 ^designation[0].value = "Coronavirus HKU1 RNA ql. /NP PCR" 
* #V00715-5 ^property[0].code = #parent 
* #V00715-5 ^property[0].valueCode = #10780 
* #V00715-5 ^property[1].code = #status 
* #V00715-5 ^property[1].valueCode = #retired 
* #V00715-5 ^property[2].code = #Relationships 
* #V00715-5 ^property[2].valueString = "verwendbar bis 30.06.2021" 
* #V00715-5 ^property[3].code = #hints 
* #V00715-5 ^property[3].valueString = "DEPRECATED" 
* #V00716-3 "Coronavirus NL63 RNA ql. /NP PCR"
* #V00716-3 ^definition = CoV NL63 RNA ql/NP P
* #V00716-3 ^designation[0].language = #de-AT 
* #V00716-3 ^designation[0].value = "Coronavirus NL63 RNA ql. /NP PCR" 
* #V00716-3 ^property[0].code = #parent 
* #V00716-3 ^property[0].valueCode = #10780 
* #V00716-3 ^property[1].code = #status 
* #V00716-3 ^property[1].valueCode = #retired 
* #V00716-3 ^property[2].code = #Relationships 
* #V00716-3 ^property[2].valueString = "verwendbar bis 30.06.2021" 
* #V00716-3 ^property[3].code = #hints 
* #V00716-3 ^property[3].valueString = "DEPRECATED" 
* #V00717-1 "Coronavirus 229E RNA ql. /NP PCR"
* #V00717-1 ^definition = CoV 229E RNA ql/NP P
* #V00717-1 ^designation[0].language = #de-AT 
* #V00717-1 ^designation[0].value = "Coronavirus 229E RNA ql. /NP PCR" 
* #V00717-1 ^property[0].code = #parent 
* #V00717-1 ^property[0].valueCode = #10780 
* #V00717-1 ^property[1].code = #status 
* #V00717-1 ^property[1].valueCode = #retired 
* #V00717-1 ^property[2].code = #Relationships 
* #V00717-1 ^property[2].valueString = "verwendbar bis 30.06.2021" 
* #V00717-1 ^property[3].code = #hints 
* #V00717-1 ^property[3].valueString = "DEPRECATED" 
* #V00718-9 "Coronavirus OC43 RNA ql. /NP PCR"
* #V00718-9 ^definition = CoV OC43 RNA ql/NP P
* #V00718-9 ^designation[0].language = #de-AT 
* #V00718-9 ^designation[0].value = "Coronavirus OC43 RNA ql. /NP PCR" 
* #V00718-9 ^property[0].code = #parent 
* #V00718-9 ^property[0].valueCode = #10780 
* #V00718-9 ^property[1].code = #status 
* #V00718-9 ^property[1].valueCode = #retired 
* #V00718-9 ^property[2].code = #Relationships 
* #V00718-9 ^property[2].valueString = "verwendbar bis 30.06.2021" 
* #V00718-9 ^property[3].code = #hints 
* #V00718-9 ^property[3].valueString = "DEPRECATED" 
* #V00721-3 "SARS-CoV-2 E-Gen ql. /NP PCR"
* #V00721-3 ^definition = SCV2 E-G. ql/NP P
* #V00721-3 ^designation[0].language = #de-AT 
* #V00721-3 ^designation[0].value = "SARS-CoV-2 E-Gen ql. /NP PCR" 
* #V00721-3 ^property[0].code = #parent 
* #V00721-3 ^property[0].valueCode = #10780 
* #V00721-3 ^property[1].code = #status 
* #V00721-3 ^property[1].valueCode = #retired 
* #V00721-3 ^property[2].code = #Relationships 
* #V00721-3 ^property[2].valueString = "verwendbar bis 30.06.2021" 
* #V00721-3 ^property[3].code = #hints 
* #V00721-3 ^property[3].valueString = "DEPRECATED" 
* #V00722-1 "SARS-CoV-2 RdRP-Gen ql. /NP PCR"
* #V00722-1 ^definition = SCV2 RdRP-G. ql/NP P
* #V00722-1 ^designation[0].language = #de-AT 
* #V00722-1 ^designation[0].value = "SARS-CoV-2 RdRP-Gen ql. /NP PCR" 
* #V00722-1 ^property[0].code = #parent 
* #V00722-1 ^property[0].valueCode = #10780 
* #V00722-1 ^property[1].code = #status 
* #V00722-1 ^property[1].valueCode = #retired 
* #V00722-1 ^property[2].code = #Relationships 
* #V00722-1 ^property[2].valueString = "verwendbar bis 30.06.2021" 
* #V00722-1 ^property[3].code = #hints 
* #V00722-1 ^property[3].valueString = "DEPRECATED" 
* #V00723-9 "SARS-CoV-2 E-Gen ql. /SP PCR"
* #V00723-9 ^definition = SCV2 E-G. ql/SP P
* #V00723-9 ^designation[0].language = #de-AT 
* #V00723-9 ^designation[0].value = "SARS-CoV-2 E-Gen ql. /Sputum PCR" 
* #V00723-9 ^property[0].code = #parent 
* #V00723-9 ^property[0].valueCode = #10780 
* #V00723-9 ^property[1].code = #status 
* #V00723-9 ^property[1].valueCode = #retired 
* #V00723-9 ^property[2].code = #Relationships 
* #V00723-9 ^property[2].valueString = "verwendbar bis 30.06.2021" 
* #V00723-9 ^property[3].code = #hints 
* #V00723-9 ^property[3].valueString = "DEPRECATED" 
* #V00724-7 "SARS-CoV-2 RdRP-Gen ql. /SP PCR"
* #V00724-7 ^definition = SCV2 RdRP-G. ql/SP P
* #V00724-7 ^designation[0].language = #de-AT 
* #V00724-7 ^designation[0].value = "SARS-CoV-2 RdRP-Gen ql. /Sputum PCR" 
* #V00724-7 ^property[0].code = #parent 
* #V00724-7 ^property[0].valueCode = #10780 
* #V00724-7 ^property[1].code = #status 
* #V00724-7 ^property[1].valueCode = #retired 
* #V00724-7 ^property[2].code = #Relationships 
* #V00724-7 ^property[2].valueString = "verwendbar bis 30.06.2021" 
* #V00724-7 ^property[3].code = #hints 
* #V00724-7 ^property[3].valueString = "DEPRECATED" 
* #V00725-4 "SARS-CoV-2 E-Gen ql. /BAL PCR"
* #V00725-4 ^definition = SCV2 E-G. ql/BAL P
* #V00725-4 ^designation[0].language = #de-AT 
* #V00725-4 ^designation[0].value = "SARS-CoV-2 E-Gen ql. /BAL PCR" 
* #V00725-4 ^property[0].code = #parent 
* #V00725-4 ^property[0].valueCode = #10780 
* #V00725-4 ^property[1].code = #status 
* #V00725-4 ^property[1].valueCode = #retired 
* #V00725-4 ^property[2].code = #Relationships 
* #V00725-4 ^property[2].valueString = "verwendbar bis 30.06.2021" 
* #V00725-4 ^property[3].code = #hints 
* #V00725-4 ^property[3].valueString = "DEPRECATED" 
* #V00726-2 "SARS-CoV-2 RdRP-Gen ql. /BAL PCR"
* #V00726-2 ^definition = SCV2 RdRP-G.ql/BAL P
* #V00726-2 ^designation[0].language = #de-AT 
* #V00726-2 ^designation[0].value = "SARS-CoV-2 RdRP-Gen ql. /BAL PCR" 
* #V00726-2 ^property[0].code = #parent 
* #V00726-2 ^property[0].valueCode = #10780 
* #V00726-2 ^property[1].code = #status 
* #V00726-2 ^property[1].valueCode = #retired 
* #V00726-2 ^property[2].code = #Relationships 
* #V00726-2 ^property[2].valueString = "verwendbar bis 30.06.2021" 
* #V00726-2 ^property[3].code = #hints 
* #V00726-2 ^property[3].valueString = "DEPRECATED" 
* #V00727-0 "SARS-CoV-2-AK ql."
* #V00727-0 ^definition = SARS-CoV-2-AK ql
* #V00727-0 ^designation[0].language = #de-AT 
* #V00727-0 ^designation[0].value = "SARS-CoV-2 Antikörper qualitativ" 
* #V00727-0 ^property[0].code = #parent 
* #V00727-0 ^property[0].valueCode = #10780 
* #V00727-0 ^property[1].code = #status 
* #V00727-0 ^property[1].valueCode = #retired 
* #V00727-0 ^property[2].code = #Relationships 
* #V00727-0 ^property[2].valueString = "verwendbar bis 30.06.2021" 
* #V00727-0 ^property[3].code = #hints 
* #V00727-0 ^property[3].valueString = "DEPRECATED" 
* #V00728-8 "SARS-CoV-2-AK IgA ql."
* #V00728-8 ^definition = SARS-CoV-2-AK IgA ql
* #V00728-8 ^designation[0].language = #de-AT 
* #V00728-8 ^designation[0].value = "SARS-CoV-2 Antikörper IgA qualitativ" 
* #V00728-8 ^property[0].code = #parent 
* #V00728-8 ^property[0].valueCode = #10780 
* #V00728-8 ^property[1].code = #status 
* #V00728-8 ^property[1].valueCode = #retired 
* #V00728-8 ^property[2].code = #Relationships 
* #V00728-8 ^property[2].valueString = "verwendbar bis 31.12.2020" 
* #V00728-8 ^property[3].code = #hints 
* #V00728-8 ^property[3].valueString = "DEPRECATED" 
* #V00731-2 "Varizella-Zoster-Virus IgM AK-Index"
* #V00731-2 ^definition = VZV IgM AK-Ind.
* #V00731-2 ^designation[0].language = #de-AT 
* #V00731-2 ^designation[0].value = "Varizella-Zoster-Virus IgM AK-Index" 
* #V00731-2 ^property[0].code = #parent 
* #V00731-2 ^property[0].valueCode = #10780 
* #V00731-2 ^property[1].code = #status 
* #V00731-2 ^property[1].valueCode = #retired 
* #V00731-2 ^property[2].code = #Relationships 
* #V00731-2 ^property[2].valueString = "verwendbar bis 31.12.2021" 
* #V00731-2 ^property[3].code = #hints 
* #V00731-2 ^property[3].valueString = "DEPRECATED" 
* #V00735-3 "Rötelnvirus AK-Index"
* #V00735-3 ^definition = Rötelnv.-AK-Ind.
* #V00735-3 ^designation[0].language = #de-AT 
* #V00735-3 ^designation[0].value = "Rötelnvirus Antikörper-Index" 
* #V00735-3 ^property[0].code = #parent 
* #V00735-3 ^property[0].valueCode = #10780 
* #V00735-3 ^property[1].code = #status 
* #V00735-3 ^property[1].valueCode = #retired 
* #V00735-3 ^property[2].code = #Relationships 
* #V00735-3 ^property[2].valueString = "verwendbar bis 31.12.2022" 
* #V00735-3 ^property[3].code = #hints 
* #V00735-3 ^property[3].valueString = "DEPRECATED" 
* #10790 "Bakteriologie"
* #10790 ^property[0].code = #parent 
* #10790 ^property[0].valueCode = #1100 
* #10790 ^property[1].code = #child 
* #10790 ^property[1].valueCode = #V00014 
* #10790 ^property[2].code = #child 
* #10790 ^property[2].valueCode = #V00064 
* #10790 ^property[3].code = #child 
* #10790 ^property[3].valueCode = #V00109 
* #10790 ^property[4].code = #child 
* #10790 ^property[4].valueCode = #V00110 
* #10790 ^property[5].code = #child 
* #10790 ^property[5].valueCode = #V00111 
* #10790 ^property[6].code = #child 
* #10790 ^property[6].valueCode = #V00281 
* #10790 ^property[7].code = #child 
* #10790 ^property[7].valueCode = #V00282 
* #10790 ^property[8].code = #child 
* #10790 ^property[8].valueCode = #V00291 
* #10790 ^property[9].code = #child 
* #10790 ^property[9].valueCode = #V00486-3 
* #10790 ^property[10].code = #child 
* #10790 ^property[10].valueCode = #V00529-0 
* #10790 ^property[11].code = #child 
* #10790 ^property[11].valueCode = #V00530-8 
* #10790 ^property[12].code = #child 
* #10790 ^property[12].valueCode = #V00531-6 
* #10790 ^property[13].code = #child 
* #10790 ^property[13].valueCode = #V00532-4 
* #10790 ^property[14].code = #child 
* #10790 ^property[14].valueCode = #V00533-2 
* #10790 ^property[15].code = #child 
* #10790 ^property[15].valueCode = #V00535-7 
* #10790 ^property[16].code = #child 
* #10790 ^property[16].valueCode = #V00536-5 
* #10790 ^property[17].code = #child 
* #10790 ^property[17].valueCode = #V00537-3 
* #10790 ^property[18].code = #child 
* #10790 ^property[18].valueCode = #V00538-1 
* #10790 ^property[19].code = #child 
* #10790 ^property[19].valueCode = #V00539-9 
* #10790 ^property[20].code = #child 
* #10790 ^property[20].valueCode = #V00540-7 
* #10790 ^property[21].code = #child 
* #10790 ^property[21].valueCode = #V00541-5 
* #10790 ^property[22].code = #child 
* #10790 ^property[22].valueCode = #V00542-3 
* #10790 ^property[23].code = #child 
* #10790 ^property[23].valueCode = #V00562-1 
* #10790 ^property[24].code = #child 
* #10790 ^property[24].valueCode = #V00604-1 
* #10790 ^property[25].code = #child 
* #10790 ^property[25].valueCode = #V00605-8 
* #10790 ^property[26].code = #child 
* #10790 ^property[26].valueCode = #V00606-6 
* #10790 ^property[27].code = #child 
* #10790 ^property[27].valueCode = #V00607-4 
* #10790 ^property[28].code = #child 
* #10790 ^property[28].valueCode = #V00676-9 
* #10790 ^property[29].code = #child 
* #10790 ^property[29].valueCode = #V00719-7 
* #10790 ^property[30].code = #child 
* #10790 ^property[30].valueCode = #V00720-5 
* #10790 ^property[31].code = #child 
* #10790 ^property[31].valueCode = #V00730-4 
* #10790 ^property[32].code = #child 
* #10790 ^property[32].valueCode = #V00732-0 
* #10790 ^property[33].code = #child 
* #10790 ^property[33].valueCode = #V00733-8 
* #10790 ^property[34].code = #child 
* #10790 ^property[34].valueCode = #V00734-6 
* #10790 ^property[35].code = #child 
* #10790 ^property[35].valueCode = #V00736-1 
* #10790 ^property[36].code = #child 
* #10790 ^property[36].valueCode = #V00737-9 
* #10790 ^property[37].code = #child 
* #10790 ^property[37].valueCode = #V00738-7 
* #10790 ^property[38].code = #child 
* #10790 ^property[38].valueCode = #V00739-5 
* #10790 ^property[39].code = #child 
* #10790 ^property[39].valueCode = #V00740-3 
* #10790 ^property[40].code = #child 
* #10790 ^property[40].valueCode = #V00741-1 
* #10790 ^property[41].code = #child 
* #10790 ^property[41].valueCode = #V00742-9 
* #10790 ^property[42].code = #child 
* #10790 ^property[42].valueCode = #V00743-7 
* #10790 ^property[43].code = #child 
* #10790 ^property[43].valueCode = #V00744-5 
* #10790 ^property[44].code = #child 
* #10790 ^property[44].valueCode = #V00745-2 
* #10790 ^property[45].code = #child 
* #10790 ^property[45].valueCode = #V00746-0 
* #10790 ^property[46].code = #child 
* #10790 ^property[46].valueCode = #V00747-8 
* #10790 ^property[47].code = #child 
* #10790 ^property[47].valueCode = #V00748-6 
* #10790 ^property[48].code = #child 
* #10790 ^property[48].valueCode = #V00749-4 
* #10790 ^property[49].code = #child 
* #10790 ^property[49].valueCode = #V00750-2 
* #10790 ^property[50].code = #child 
* #10790 ^property[50].valueCode = #V00751-0 
* #10790 ^property[51].code = #child 
* #10790 ^property[51].valueCode = #V00752-8 
* #10790 ^property[52].code = #child 
* #10790 ^property[52].valueCode = #V00753-6 
* #10790 ^property[53].code = #child 
* #10790 ^property[53].valueCode = #V00754-4 
* #10790 ^property[54].code = #child 
* #10790 ^property[54].valueCode = #V00755-1 
* #10790 ^property[55].code = #child 
* #10790 ^property[55].valueCode = #V00756-9 
* #10790 ^property[56].code = #child 
* #10790 ^property[56].valueCode = #V00757-7 
* #10790 ^property[57].code = #child 
* #10790 ^property[57].valueCode = #V00758-5 
* #10790 ^property[58].code = #child 
* #10790 ^property[58].valueCode = #V00759-3 
* #10790 ^property[59].code = #child 
* #10790 ^property[59].valueCode = #V00760-1 
* #10790 ^property[60].code = #child 
* #10790 ^property[60].valueCode = #V00771-8 
* #V00014 "TPHA-Index Liquor/Serum"
* #V00014 ^definition = TPHA-Index /L
* #V00014 ^designation[0].language = #de-AT 
* #V00014 ^designation[0].value = "TPHA-Index /L" 
* #V00014 ^property[0].code = #parent 
* #V00014 ^property[0].valueCode = #10790 
* #V00064 "ITpA-Index Liquor/Serum"
* #V00064 ^definition = ITpA-Index
* #V00064 ^designation[0].language = #de-AT 
* #V00064 ^designation[0].value = "ITpA-Index" 
* #V00064 ^property[0].code = #parent 
* #V00064 ^property[0].valueCode = #10790 
* #V00109 "Pseudomonas Aerug.Exotoxin A AK"
* #V00109 ^definition = Pseudom. Aer.Exo.AK
* #V00109 ^designation[0].language = #de-AT 
* #V00109 ^designation[0].value = "Pseudomonas Aerug.Exotoxin A AK" 
* #V00109 ^property[0].code = #parent 
* #V00109 ^property[0].valueCode = #10790 
* #V00109 ^property[1].code = #status 
* #V00109 ^property[1].valueCode = #retired 
* #V00109 ^property[2].code = #Relationships 
* #V00109 ^property[2].valueString = "verwendbar bis 30.06.2017" 
* #V00109 ^property[3].code = #hints 
* #V00109 ^property[3].valueString = "DEPRECATED" 
* #V00110 "Pseudomonas alkalische Prot. AK"
* #V00110 ^definition = Pseudom. Aer.AP.AK
* #V00110 ^designation[0].language = #de-AT 
* #V00110 ^designation[0].value = "Pseudomonas alkalische Prot. AK" 
* #V00110 ^property[0].code = #parent 
* #V00110 ^property[0].valueCode = #10790 
* #V00110 ^property[1].code = #status 
* #V00110 ^property[1].valueCode = #retired 
* #V00110 ^property[2].code = #Relationships 
* #V00110 ^property[2].valueString = "verwendbar bis 30.06.2017" 
* #V00110 ^property[3].code = #hints 
* #V00110 ^property[3].valueString = "DEPRECATED" 
* #V00111 "Pseudomonas Elastase AK"
* #V00111 ^definition = Pseudom. Aer.Ela.AK
* #V00111 ^designation[0].language = #de-AT 
* #V00111 ^designation[0].value = "Pseudomonas Elastase AK" 
* #V00111 ^property[0].code = #parent 
* #V00111 ^property[0].valueCode = #10790 
* #V00111 ^property[1].code = #status 
* #V00111 ^property[1].valueCode = #retired 
* #V00111 ^property[2].code = #Relationships 
* #V00111 ^property[2].valueString = "verwendbar bis 30.06.2017" 
* #V00111 ^property[3].code = #hints 
* #V00111 ^property[3].valueString = "DEPRECATED" 
* #V00281 "Kommentar Bakteriologie"
* #V00281 ^definition = Komm.Bakteriologie
* #V00281 ^designation[0].language = #de-AT 
* #V00281 ^designation[0].value = "Kommentar Bakteriologie" 
* #V00281 ^property[0].code = #parent 
* #V00281 ^property[0].valueCode = #10790 
* #V00281 ^property[1].code = #status 
* #V00281 ^property[1].valueCode = #retired 
* #V00281 ^property[2].code = #Relationships 
* #V00281 ^property[2].valueString = "verwendbar bis 30.06.2017" 
* #V00281 ^property[3].code = #hints 
* #V00281 ^property[3].valueString = "DEPRECATED" 
* #V00282 "Kommentar sonst.Erregerdiag."
* #V00282 ^definition = Komm.sonst.Erreger.
* #V00282 ^designation[0].language = #de-AT 
* #V00282 ^designation[0].value = "Kommentar sonstige Erregerdiagnostik" 
* #V00282 ^property[0].code = #parent 
* #V00282 ^property[0].valueCode = #10790 
* #V00291 "Trep. pall. TPPA Ti"
* #V00291 ^definition = TPPA Ti.
* #V00291 ^designation[0].language = #de-AT 
* #V00291 ^designation[0].value = "Treponema pallidum-Antikörper Titer PA (TPPA)" 
* #V00291 ^property[0].code = #parent 
* #V00291 ^property[0].valueCode = #10790 
* #V00486-3 "M.tuberculosis stim. IFN-G Spot-Test /SM"
* #V00486-3 ^definition = MTB-i. IFN-G Sp-T/SM
* #V00486-3 ^designation[0].language = #de-AT 
* #V00486-3 ^designation[0].value = "M.tuberculosis stim. IFN-G Spot-Test /SM" 
* #V00486-3 ^property[0].code = #parent 
* #V00486-3 ^property[0].valueCode = #10790 
* #V00529-0 "E. coli K1 /Liquor PCR"
* #V00529-0 ^definition = E. coli K1 /L PCR
* #V00529-0 ^designation[0].language = #de-AT 
* #V00529-0 ^designation[0].value = "E. coli K1 /Liquor PCR" 
* #V00529-0 ^property[0].code = #parent 
* #V00529-0 ^property[0].valueCode = #10790 
* #V00530-8 "Haemophilus influenzae /Liquor PCR"
* #V00530-8 ^definition = Haem. Influ. /L PCR
* #V00530-8 ^designation[0].language = #de-AT 
* #V00530-8 ^designation[0].value = "Haemophilus influenzae /Liquor PCR" 
* #V00530-8 ^property[0].code = #parent 
* #V00530-8 ^property[0].valueCode = #10790 
* #V00531-6 "Listeria monocytogenes DNA /Liquor PCR"
* #V00531-6 ^definition = L. monoc. DNA /L PCR
* #V00531-6 ^designation[0].language = #de-AT 
* #V00531-6 ^designation[0].value = "Listeria monocytogenes DNA /Liquor PCR" 
* #V00531-6 ^property[0].code = #parent 
* #V00531-6 ^property[0].valueCode = #10790 
* #V00532-4 "Streptococcus agalactiae DNA /Liquor PCR"
* #V00532-4 ^definition = Gp.B.Strp.DNA /L PCR
* #V00532-4 ^designation[0].language = #de-AT 
* #V00532-4 ^designation[0].value = "Streptococcus agalactiae DNA /Liquor PCR" 
* #V00532-4 ^property[0].code = #parent 
* #V00532-4 ^property[0].valueCode = #10790 
* #V00532-4 ^property[1].code = #status 
* #V00532-4 ^property[1].valueCode = #retired 
* #V00532-4 ^property[2].code = #Relationships 
* #V00532-4 ^property[2].valueString = "verwendbar bis 30.06.2017" 
* #V00532-4 ^property[3].code = #hints 
* #V00532-4 ^property[3].valueString = "DEPRECATED" 
* #V00533-2 "Streptococcus pneumoniae DNA /Liquor PCR"
* #V00533-2 ^definition = S. pneum. DNA /L PCR
* #V00533-2 ^designation[0].language = #de-AT 
* #V00533-2 ^designation[0].value = "Streptococcus pneumoniae DNA /Liquor PCR" 
* #V00533-2 ^property[0].code = #parent 
* #V00533-2 ^property[0].valueCode = #10790 
* #V00535-7 "Trep.pall. p15-AK IgG ql."
* #V00535-7 ^definition = TP p15-AK IgG ql.
* #V00535-7 ^designation[0].language = #de-AT 
* #V00535-7 ^designation[0].value = "Treponema pallidum p15-AK IgG qualitativ" 
* #V00535-7 ^property[0].code = #parent 
* #V00535-7 ^property[0].valueCode = #10790 
* #V00536-5 "Trep.pall. p15-AK IgM ql."
* #V00536-5 ^definition = TP p15-AK IgM ql.
* #V00536-5 ^designation[0].language = #de-AT 
* #V00536-5 ^designation[0].value = "Treponema pallidum p15-AK IgM qualitativ" 
* #V00536-5 ^property[0].code = #parent 
* #V00536-5 ^property[0].valueCode = #10790 
* #V00537-3 "Trep.pall. p17-AK IgG ql."
* #V00537-3 ^definition = TP p17-AK IgG ql.
* #V00537-3 ^designation[0].language = #de-AT 
* #V00537-3 ^designation[0].value = "Treponema pallidum p17-AK IgG qualitativ" 
* #V00537-3 ^property[0].code = #parent 
* #V00537-3 ^property[0].valueCode = #10790 
* #V00538-1 "Trep.pall. p17-AK IgM ql."
* #V00538-1 ^definition = TP p17-AK IgM ql.
* #V00538-1 ^designation[0].language = #de-AT 
* #V00538-1 ^designation[0].value = "Treponema pallidum p17-AK IgM qualitativ" 
* #V00538-1 ^property[0].code = #parent 
* #V00538-1 ^property[0].valueCode = #10790 
* #V00539-9 "Trep.pall. p45-AK IgG ql."
* #V00539-9 ^definition = TP p14-AK IgG ql.
* #V00539-9 ^designation[0].language = #de-AT 
* #V00539-9 ^designation[0].value = "Treponema pallidum p45-AK IgG qualitativ" 
* #V00539-9 ^property[0].code = #parent 
* #V00539-9 ^property[0].valueCode = #10790 
* #V00540-7 "Trep.pall. p45-AK IgM ql."
* #V00540-7 ^definition = TP p14-AK IgM ql.
* #V00540-7 ^designation[0].language = #de-AT 
* #V00540-7 ^designation[0].value = "Treponema pallidum p45-AK IgM qualitativ" 
* #V00540-7 ^property[0].code = #parent 
* #V00540-7 ^property[0].valueCode = #10790 
* #V00541-5 "Trep.pall. p47-AK IgG ql."
* #V00541-5 ^definition = TP p47-AK IgG ql.
* #V00541-5 ^designation[0].language = #de-AT 
* #V00541-5 ^designation[0].value = "Treponema pallidum p47-AK IgG qualitativ" 
* #V00541-5 ^property[0].code = #parent 
* #V00541-5 ^property[0].valueCode = #10790 
* #V00542-3 "Trep.pall. p47-AK IgM ql."
* #V00542-3 ^definition = TP p47-AK IgM ql.
* #V00542-3 ^designation[0].language = #de-AT 
* #V00542-3 ^designation[0].value = "Treponema pallidum p47-AK IgM qualitativ" 
* #V00542-3 ^property[0].code = #parent 
* #V00542-3 ^property[0].valueCode = #10790 
* #V00562-1 "Listeria monocytogenes DNA /B ql. PCR"
* #V00562-1 ^definition = L. m. DNA /B ql. PCR
* #V00562-1 ^designation[0].language = #de-AT 
* #V00562-1 ^designation[0].value = "Listeria monocytogenes DNA /Blut qualitativ PCR" 
* #V00562-1 ^property[0].code = #parent 
* #V00562-1 ^property[0].valueCode = #10790 
* #V00604-1 "Campylobacter jejuni AK IgG"
* #V00604-1 ^definition = C. jejuni AK IgG
* #V00604-1 ^designation[0].language = #de-AT 
* #V00604-1 ^designation[0].value = "Campylobacter jejuni Antikörper IgG" 
* #V00604-1 ^property[0].code = #parent 
* #V00604-1 ^property[0].valueCode = #10790 
* #V00605-8 "Campylobacter jejuni AK IgA"
* #V00605-8 ^definition = C. jejuni AK IgA
* #V00605-8 ^designation[0].language = #de-AT 
* #V00605-8 ^designation[0].value = "Campylobacter jejuni Antikörper IgA" 
* #V00605-8 ^property[0].code = #parent 
* #V00605-8 ^property[0].valueCode = #10790 
* #V00606-6 "Yersinia enterocolitica AK IgG IB"
* #V00606-6 ^definition = Y.entero.-AK IgG IB
* #V00606-6 ^designation[0].language = #de-AT 
* #V00606-6 ^designation[0].value = "Yersinia enterocolitica Antikörper IgG Immunoblot" 
* #V00606-6 ^property[0].code = #parent 
* #V00606-6 ^property[0].valueCode = #10790 
* #V00607-4 "Yersinia enterocolitica AK IgA IB"
* #V00607-4 ^definition = Y.entero.-AK IgA IB
* #V00607-4 ^designation[0].language = #de-AT 
* #V00607-4 ^designation[0].value = "Yersinia enterocolitica Antikörper IgA Immunoblot" 
* #V00607-4 ^property[0].code = #parent 
* #V00607-4 ^property[0].valueCode = #10790 
* #V00676-9 "MRSA-DNA /N PCR"
* #V00676-9 ^definition = MRSA-DNA /N PCR
* #V00676-9 ^designation[0].language = #de-AT 
* #V00676-9 ^designation[0].value = "MRSA-DNA /Nasenabstrich PCR" 
* #V00676-9 ^property[0].code = #parent 
* #V00676-9 ^property[0].valueCode = #10790 
* #V00676-9 ^property[1].code = #status 
* #V00676-9 ^property[1].valueCode = #retired 
* #V00676-9 ^property[2].code = #Relationships 
* #V00676-9 ^property[2].valueString = "verwendbar bis 30.06.2019" 
* #V00676-9 ^property[3].code = #hints 
* #V00676-9 ^property[3].valueString = "DEPRECATED" 
* #V00719-7 "Bordetella parapertussis IS1001 DNA ql. /NP PCR"
* #V00719-7 ^definition = BppIS1001DNA ql/NP P
* #V00719-7 ^designation[0].language = #de-AT 
* #V00719-7 ^designation[0].value = "Bordetella parapertussis IS1001 DNA ql. /NP PCR" 
* #V00719-7 ^property[0].code = #parent 
* #V00719-7 ^property[0].valueCode = #10790 
* #V00719-7 ^property[1].code = #status 
* #V00719-7 ^property[1].valueCode = #retired 
* #V00719-7 ^property[2].code = #Relationships 
* #V00719-7 ^property[2].valueString = "verwendbar bis 30.06.2021" 
* #V00719-7 ^property[3].code = #hints 
* #V00719-7 ^property[3].valueString = "DEPRECATED" 
* #V00720-5 "Neisseria meningitidis DNA ql. /L PCR"
* #V00720-5 ^definition = N.m.-DNA ql. /L P
* #V00720-5 ^designation[0].language = #de-AT 
* #V00720-5 ^designation[0].value = "Neisseria meningitidis DNA ql. /Liquor PCR" 
* #V00720-5 ^property[0].code = #parent 
* #V00720-5 ^property[0].valueCode = #10790 
* #V00730-4 "Bordetella pertussis Toxin Prom. Reg. ql. /RT PCR"
* #V00730-4 ^definition = Bord.p. TPR ql./RT P
* #V00730-4 ^designation[0].language = #de-AT 
* #V00730-4 ^designation[0].value = "Bordetella pertussis Toxin Prom. Reg. ql. /RT PCR" 
* #V00730-4 ^property[0].code = #parent 
* #V00730-4 ^property[0].valueCode = #10790 
* #V00730-4 ^property[1].code = #status 
* #V00730-4 ^property[1].valueCode = #retired 
* #V00730-4 ^property[2].code = #Relationships 
* #V00730-4 ^property[2].valueString = "verwendbar bis 31.12.2021" 
* #V00730-4 ^property[3].code = #hints 
* #V00730-4 ^property[3].valueString = "DEPRECATED" 
* #V00732-0 "Bakterienkultur angereichert /SM"
* #V00732-0 ^definition = Bakt.-Klt. anger./SM
* #V00732-0 ^designation[0].language = #de-AT 
* #V00732-0 ^designation[0].value = "Bakterienkultur angereichert /Sondermaterial" 
* #V00732-0 ^property[0].code = #parent 
* #V00732-0 ^property[0].valueCode = #10790 
* #V00733-8 "Mikroorganismus Kultur angereichert /SM"
* #V00733-8 ^definition = MO Klt. anger. /SM
* #V00733-8 ^designation[0].language = #de-AT 
* #V00733-8 ^designation[0].value = "Mikroorganismus Kultur angereichert/Sondermaterial" 
* #V00733-8 ^property[0].code = #parent 
* #V00733-8 ^property[0].valueCode = #10790 
* #V00734-6 "Diphtherietoxin-Gen ql. /SM PCR"
* #V00734-6 ^definition = C.diph.Tox.G ql/SM P
* #V00734-6 ^designation[0].language = #de-AT 
* #V00734-6 ^designation[0].value = "Diphtherietoxin-Gen qualitativ /Sondermaterial PCR" 
* #V00734-6 ^property[0].code = #parent 
* #V00734-6 ^property[0].valueCode = #10790 
* #V00736-1 "MRSA spa-Typisierung /SM"
* #V00736-1 ^definition = MRSA spa-Typ. /SM
* #V00736-1 ^designation[0].language = #de-AT 
* #V00736-1 ^designation[0].value = "MRSA spa-Typisierung /Sondermaterial" 
* #V00736-1 ^property[0].code = #parent 
* #V00736-1 ^property[0].valueCode = #10790 
* #V00736-1 ^property[1].code = #status 
* #V00736-1 ^property[1].valueCode = #retired 
* #V00736-1 ^property[2].code = #Relationships 
* #V00736-1 ^property[2].valueString = "verwendbar bis 31.12.2022" 
* #V00736-1 ^property[3].code = #hints 
* #V00736-1 ^property[3].valueString = "DEPRECATED" 
* #V00737-9 "Aminoglycosid-Empfindlichkeit"
* #V00737-9 ^definition = Aminoglycosid-Empf.
* #V00737-9 ^designation[0].language = #de-AT 
* #V00737-9 ^designation[0].value = "Aminoglycosid-Empfindlichkeit" 
* #V00737-9 ^property[0].code = #parent 
* #V00737-9 ^property[0].valueCode = #10790 
* #V00737-9 ^property[1].code = #status 
* #V00737-9 ^property[1].valueCode = #retired 
* #V00737-9 ^property[2].code = #Relationships 
* #V00737-9 ^property[2].valueString = "verwendbar bis 31.12.2022" 
* #V00737-9 ^property[3].code = #hints 
* #V00737-9 ^property[3].valueString = "DEPRECATED" 
* #V00738-7 "Atovaquon-Empfindlichkeit"
* #V00738-7 ^definition = Atovaquon-Empf.
* #V00738-7 ^designation[0].language = #de-AT 
* #V00738-7 ^designation[0].value = "Atovaquon-Empfindlichkeit" 
* #V00738-7 ^property[0].code = #parent 
* #V00738-7 ^property[0].valueCode = #10790 
* #V00738-7 ^property[1].code = #status 
* #V00738-7 ^property[1].valueCode = #retired 
* #V00738-7 ^property[2].code = #Relationships 
* #V00738-7 ^property[2].valueString = "verwendbar bis 31.12.2022" 
* #V00738-7 ^property[3].code = #hints 
* #V00738-7 ^property[3].valueString = "DEPRECATED" 
* #V00739-5 "Azithromycin/Ethambutol-Empfindlichkeit"
* #V00739-5 ^definition = Azithr./Etham.-Empf.
* #V00739-5 ^designation[0].language = #de-AT 
* #V00739-5 ^designation[0].value = "Azithromycin/Ethambutol-Empfindlichkeit" 
* #V00739-5 ^property[0].code = #parent 
* #V00739-5 ^property[0].valueCode = #10790 
* #V00739-5 ^property[1].code = #status 
* #V00739-5 ^property[1].valueCode = #retired 
* #V00739-5 ^property[2].code = #Relationships 
* #V00739-5 ^property[2].valueString = "verwendbar bis 31.12.2022" 
* #V00739-5 ^property[3].code = #hints 
* #V00739-5 ^property[3].valueString = "DEPRECATED" 
* #V00740-3 "Cefcapene-Empfindlichkeit"
* #V00740-3 ^definition = Cefcapene-Empf.
* #V00740-3 ^designation[0].language = #de-AT 
* #V00740-3 ^designation[0].value = "Cefcapene-Empfindlichkeit" 
* #V00740-3 ^property[0].code = #parent 
* #V00740-3 ^property[0].valueCode = #10790 
* #V00740-3 ^property[1].code = #status 
* #V00740-3 ^property[1].valueCode = #retired 
* #V00740-3 ^property[2].code = #Relationships 
* #V00740-3 ^property[2].valueString = "verwendbar bis 31.12.2022" 
* #V00740-3 ^property[3].code = #hints 
* #V00740-3 ^property[3].valueString = "DEPRECATED" 
* #V00741-1 "Cefiderocol-Empfindlichkeit"
* #V00741-1 ^definition = Cefiderocol-Empf.
* #V00741-1 ^designation[0].language = #de-AT 
* #V00741-1 ^designation[0].value = "Cefiderocol-Empfindlichkeit" 
* #V00741-1 ^property[0].code = #parent 
* #V00741-1 ^property[0].valueCode = #10790 
* #V00741-1 ^property[1].code = #status 
* #V00741-1 ^property[1].valueCode = #retired 
* #V00741-1 ^property[2].code = #Relationships 
* #V00741-1 ^property[2].valueString = "verwendbar bis 31.12.2022" 
* #V00741-1 ^property[3].code = #hints 
* #V00741-1 ^property[3].valueString = "DEPRECATED" 
* #V00742-9 "Cefozopran-Empfindlichkeit"
* #V00742-9 ^definition = Cefozopran-Empf.
* #V00742-9 ^designation[0].language = #de-AT 
* #V00742-9 ^designation[0].value = "Cefozopran-Empfindlichkeit" 
* #V00742-9 ^property[0].code = #parent 
* #V00742-9 ^property[0].valueCode = #10790 
* #V00742-9 ^property[1].code = #status 
* #V00742-9 ^property[1].valueCode = #retired 
* #V00742-9 ^property[2].code = #Relationships 
* #V00742-9 ^property[2].valueString = "verwendbar bis 31.12.2022" 
* #V00742-9 ^property[3].code = #hints 
* #V00742-9 ^property[3].valueString = "DEPRECATED" 
* #V00743-7 "Cefquinom-Empfindlichkeit"
* #V00743-7 ^definition = Cefquinom-Empf.
* #V00743-7 ^designation[0].language = #de-AT 
* #V00743-7 ^designation[0].value = "Cefquinom-Empfindlichkeit" 
* #V00743-7 ^property[0].code = #parent 
* #V00743-7 ^property[0].valueCode = #10790 
* #V00743-7 ^property[1].code = #status 
* #V00743-7 ^property[1].valueCode = #retired 
* #V00743-7 ^property[2].code = #Relationships 
* #V00743-7 ^property[2].valueString = "verwendbar bis 31.12.2022" 
* #V00743-7 ^property[3].code = #hints 
* #V00743-7 ^property[3].valueString = "DEPRECATED" 
* #V00744-5 "Cefteram-Empfindlichkeit"
* #V00744-5 ^definition = Cefteram-Empf.
* #V00744-5 ^designation[0].language = #de-AT 
* #V00744-5 ^designation[0].value = "Cefteram-Empfindlichkeit" 
* #V00744-5 ^property[0].code = #parent 
* #V00744-5 ^property[0].valueCode = #10790 
* #V00744-5 ^property[1].code = #status 
* #V00744-5 ^property[1].valueCode = #retired 
* #V00744-5 ^property[2].code = #Relationships 
* #V00744-5 ^property[2].valueString = "verwendbar bis 31.12.2022" 
* #V00744-5 ^property[3].code = #hints 
* #V00744-5 ^property[3].valueString = "DEPRECATED" 
* #V00745-2 "Clarithromycin/Ethambutol-Empfindlichkeit"
* #V00745-2 ^definition = Clarithr/Etham-Empf.
* #V00745-2 ^designation[0].language = #de-AT 
* #V00745-2 ^designation[0].value = "Clarithromycin/Ethambutol-Empfindlichkeit" 
* #V00745-2 ^property[0].code = #parent 
* #V00745-2 ^property[0].valueCode = #10790 
* #V00745-2 ^property[1].code = #status 
* #V00745-2 ^property[1].valueCode = #retired 
* #V00745-2 ^property[2].code = #Relationships 
* #V00745-2 ^property[2].valueString = "verwendbar bis 31.12.2022" 
* #V00745-2 ^property[3].code = #hints 
* #V00745-2 ^property[3].valueString = "DEPRECATED" 
* #V00746-0 "Eravacycline-Empfindlichkeit"
* #V00746-0 ^definition = Eravacycline-Empf.
* #V00746-0 ^designation[0].language = #de-AT 
* #V00746-0 ^designation[0].value = "Eravacycline-Empfindlichkeit" 
* #V00746-0 ^property[0].code = #parent 
* #V00746-0 ^property[0].valueCode = #10790 
* #V00746-0 ^property[1].code = #status 
* #V00746-0 ^property[1].valueCode = #retired 
* #V00746-0 ^property[2].code = #Relationships 
* #V00746-0 ^property[2].valueString = "verwendbar bis 31.12.2022" 
* #V00746-0 ^property[3].code = #hints 
* #V00746-0 ^property[3].valueString = "DEPRECATED" 
* #V00747-8 "Erythromycin/Ethambutol-Empfindlichkeit"
* #V00747-8 ^definition = Erythr./Etham.-Empf.
* #V00747-8 ^designation[0].language = #de-AT 
* #V00747-8 ^designation[0].value = "Erythromycin/Ethambutol-Empfindlichkeit" 
* #V00747-8 ^property[0].code = #parent 
* #V00747-8 ^property[0].valueCode = #10790 
* #V00747-8 ^property[1].code = #status 
* #V00747-8 ^property[1].valueCode = #retired 
* #V00747-8 ^property[2].code = #Relationships 
* #V00747-8 ^property[2].valueString = "verwendbar bis 31.12.2022" 
* #V00747-8 ^property[3].code = #hints 
* #V00747-8 ^property[3].valueString = "DEPRECATED" 
* #V00748-6 "Ethambutol/rifAMPin-Empfindlichkeit"
* #V00748-6 ^definition = Ethamb/rifAMP.-Empf.
* #V00748-6 ^designation[0].language = #de-AT 
* #V00748-6 ^designation[0].value = "Ethambutol/rifAMPin-Empfindlichkeit" 
* #V00748-6 ^property[0].code = #parent 
* #V00748-6 ^property[0].valueCode = #10790 
* #V00748-6 ^property[1].code = #status 
* #V00748-6 ^property[1].valueCode = #retired 
* #V00748-6 ^property[2].code = #Relationships 
* #V00748-6 ^property[2].valueString = "verwendbar bis 31.12.2022" 
* #V00748-6 ^property[3].code = #hints 
* #V00748-6 ^property[3].valueString = "DEPRECATED" 
* #V00749-4 "Flomoxef-Empfindlichkeit"
* #V00749-4 ^definition = Flomoxef-Empf.
* #V00749-4 ^designation[0].language = #de-AT 
* #V00749-4 ^designation[0].value = "Flomoxef-Empfindlichkeit" 
* #V00749-4 ^property[0].code = #parent 
* #V00749-4 ^property[0].valueCode = #10790 
* #V00749-4 ^property[1].code = #status 
* #V00749-4 ^property[1].valueCode = #retired 
* #V00749-4 ^property[2].code = #Relationships 
* #V00749-4 ^property[2].valueString = "verwendbar bis 31.12.2022" 
* #V00749-4 ^property[3].code = #hints 
* #V00749-4 ^property[3].valueString = "DEPRECATED" 
* #V00750-2 "Fluoroquinolon-Empfindlichkeit"
* #V00750-2 ^definition = Fluoroquinolon-Empf.
* #V00750-2 ^designation[0].language = #de-AT 
* #V00750-2 ^designation[0].value = "Fluoroquinolon-Empfindlichkeit" 
* #V00750-2 ^property[0].code = #parent 
* #V00750-2 ^property[0].valueCode = #10790 
* #V00750-2 ^property[1].code = #status 
* #V00750-2 ^property[1].valueCode = #retired 
* #V00750-2 ^property[2].code = #Relationships 
* #V00750-2 ^property[2].valueString = "verwendbar bis 31.12.2022" 
* #V00750-2 ^property[3].code = #hints 
* #V00750-2 ^property[3].valueString = "DEPRECATED" 
* #V00751-0 "Gamithromycin-Empfindlichkeit"
* #V00751-0 ^definition = Gamithromycin-Empf.
* #V00751-0 ^designation[0].language = #de-AT 
* #V00751-0 ^designation[0].value = "Gamithromycin-Empfindlichkeit" 
* #V00751-0 ^property[0].code = #parent 
* #V00751-0 ^property[0].valueCode = #10790 
* #V00751-0 ^property[1].code = #status 
* #V00751-0 ^property[1].valueCode = #retired 
* #V00751-0 ^property[2].code = #Relationships 
* #V00751-0 ^property[2].valueString = "verwendbar bis 31.12.2022" 
* #V00751-0 ^property[3].code = #hints 
* #V00751-0 ^property[3].valueString = "DEPRECATED" 
* #V00752-8 "Lefamulin-Empfindlichkeit"
* #V00752-8 ^definition = Lefamulin-Empf.
* #V00752-8 ^designation[0].language = #de-AT 
* #V00752-8 ^designation[0].value = "Lefamulin-Empfindlichkeit" 
* #V00752-8 ^property[0].code = #parent 
* #V00752-8 ^property[0].valueCode = #10790 
* #V00752-8 ^property[1].code = #status 
* #V00752-8 ^property[1].valueCode = #retired 
* #V00752-8 ^property[2].code = #Relationships 
* #V00752-8 ^property[2].valueString = "verwendbar bis 31.12.2022" 
* #V00752-8 ^property[3].code = #hints 
* #V00752-8 ^property[3].valueString = "DEPRECATED" 
* #V00753-6 "Optochin-Empfindlichkeit"
* #V00753-6 ^definition = Optochin-Empf.
* #V00753-6 ^designation[0].language = #de-AT 
* #V00753-6 ^designation[0].value = "Optochin-Empfindlichkeit" 
* #V00753-6 ^property[0].code = #parent 
* #V00753-6 ^property[0].valueCode = #10790 
* #V00753-6 ^property[1].code = #status 
* #V00753-6 ^property[1].valueCode = #retired 
* #V00753-6 ^property[2].code = #Relationships 
* #V00753-6 ^property[2].valueString = "verwendbar bis 31.12.2022" 
* #V00753-6 ^property[3].code = #hints 
* #V00753-6 ^property[3].valueString = "DEPRECATED" 
* #V00754-4 "Panipenem-Empfindlichkeit"
* #V00754-4 ^definition = Panipenem-Empf.
* #V00754-4 ^designation[0].language = #de-AT 
* #V00754-4 ^designation[0].value = "Panipenem-Empfindlichkeit" 
* #V00754-4 ^property[0].code = #parent 
* #V00754-4 ^property[0].valueCode = #10790 
* #V00754-4 ^property[1].code = #status 
* #V00754-4 ^property[1].valueCode = #retired 
* #V00754-4 ^property[2].code = #Relationships 
* #V00754-4 ^property[2].valueString = "verwendbar bis 31.12.2022" 
* #V00754-4 ^property[3].code = #hints 
* #V00754-4 ^property[3].valueString = "DEPRECATED" 
* #V00755-1 "Prothionamid-Empfindlichkeit"
* #V00755-1 ^definition = Prothionamid-Empf.
* #V00755-1 ^designation[0].language = #de-AT 
* #V00755-1 ^designation[0].value = "Prothionamid-Empfindlichkeit" 
* #V00755-1 ^property[0].code = #parent 
* #V00755-1 ^property[0].valueCode = #10790 
* #V00755-1 ^property[1].code = #status 
* #V00755-1 ^property[1].valueCode = #retired 
* #V00755-1 ^property[2].code = #Relationships 
* #V00755-1 ^property[2].valueString = "verwendbar bis 31.12.2022" 
* #V00755-1 ^property[3].code = #hints 
* #V00755-1 ^property[3].valueString = "DEPRECATED" 
* #V00756-9 "Prulifloxacin-Empfindlichkeit"
* #V00756-9 ^definition = Prulifloxacin-Empf.
* #V00756-9 ^designation[0].language = #de-AT 
* #V00756-9 ^designation[0].value = "Prulifloxacin-Empfindlichkeit" 
* #V00756-9 ^property[0].code = #parent 
* #V00756-9 ^property[0].valueCode = #10790 
* #V00756-9 ^property[1].code = #status 
* #V00756-9 ^property[1].valueCode = #retired 
* #V00756-9 ^property[2].code = #Relationships 
* #V00756-9 ^property[2].valueString = "verwendbar bis 31.12.2022" 
* #V00756-9 ^property[3].code = #hints 
* #V00756-9 ^property[3].valueString = "DEPRECATED" 
* #V00757-7 "Rifabutin/Ethambutol-Empfindlichkeit"
* #V00757-7 ^definition = Rifabu./Etham.-Empf.
* #V00757-7 ^designation[0].language = #de-AT 
* #V00757-7 ^designation[0].value = "Rifabutin/Ethambutol-Empfindlichkeit" 
* #V00757-7 ^property[0].code = #parent 
* #V00757-7 ^property[0].valueCode = #10790 
* #V00757-7 ^property[1].code = #status 
* #V00757-7 ^property[1].valueCode = #retired 
* #V00757-7 ^property[2].code = #Relationships 
* #V00757-7 ^property[2].valueString = "verwendbar bis 30.06.2023" 
* #V00757-7 ^property[3].code = #hints 
* #V00757-7 ^property[3].valueString = "DEPRECATED" 
* #V00758-5 "Rifapentin-Empfindlichkeit"
* #V00758-5 ^definition = Rifapentin-Empf.
* #V00758-5 ^designation[0].language = #de-AT 
* #V00758-5 ^designation[0].value = "Rifapentin-Empfindlichkeit" 
* #V00758-5 ^property[0].code = #parent 
* #V00758-5 ^property[0].valueCode = #10790 
* #V00758-5 ^property[1].code = #status 
* #V00758-5 ^property[1].valueCode = #retired 
* #V00758-5 ^property[2].code = #Relationships 
* #V00758-5 ^property[2].valueString = "verwendbar bis 31.12.2022" 
* #V00758-5 ^property[3].code = #hints 
* #V00758-5 ^property[3].valueString = "DEPRECATED" 
* #V00759-3 "Tildipirosin-Empfindlichkeit"
* #V00759-3 ^definition = Tildipirosin-Empf.
* #V00759-3 ^designation[0].language = #de-AT 
* #V00759-3 ^designation[0].value = "Tildipirosin-Empfindlichkeit" 
* #V00759-3 ^property[0].code = #parent 
* #V00759-3 ^property[0].valueCode = #10790 
* #V00759-3 ^property[1].code = #status 
* #V00759-3 ^property[1].valueCode = #retired 
* #V00759-3 ^property[2].code = #Relationships 
* #V00759-3 ^property[2].valueString = "verwendbar bis 31.12.2022" 
* #V00759-3 ^property[3].code = #hints 
* #V00759-3 ^property[3].valueString = "DEPRECATED" 
* #V00760-1 "Tosufloxacin-Empfindlichkeit"
* #V00760-1 ^definition = Tosufloxacin-Empf.
* #V00760-1 ^designation[0].language = #de-AT 
* #V00760-1 ^designation[0].value = "Tosufloxacin-Empfindlichkeit" 
* #V00760-1 ^property[0].code = #parent 
* #V00760-1 ^property[0].valueCode = #10790 
* #V00760-1 ^property[1].code = #status 
* #V00760-1 ^property[1].valueCode = #retired 
* #V00760-1 ^property[2].code = #Relationships 
* #V00760-1 ^property[2].valueString = "verwendbar bis 31.12.2022" 
* #V00760-1 ^property[3].code = #hints 
* #V00760-1 ^property[3].valueString = "DEPRECATED" 
* #V00771-8 "Borrelien-Antikörper IgM Index"
* #V00771-8 ^definition = Borrelien IgM Index
* #V00771-8 ^designation[0].language = #de-AT 
* #V00771-8 ^designation[0].value = "Borrelien-Antikörper IgM Index" 
* #V00771-8 ^property[0].code = #parent 
* #V00771-8 ^property[0].valueCode = #10790 
* #10800 "Mykologie"
* #10800 ^property[0].code = #parent 
* #10800 ^property[0].valueCode = #1100 
* #10800 ^property[1].code = #child 
* #10800 ^property[1].valueCode = #V00293 
* #10800 ^property[2].code = #child 
* #10800 ^property[2].valueCode = #V00528-2 
* #V00293 "Kommentar Mykologie"
* #V00293 ^definition = Komm.Mykologie
* #V00293 ^designation[0].language = #de-AT 
* #V00293 ^designation[0].value = "Kommentar Mykologie" 
* #V00293 ^property[0].code = #parent 
* #V00293 ^property[0].valueCode = #10800 
* #V00528-2 "Cryptococcus neoformans rRNA /Liquor PCR"
* #V00528-2 ^definition = C. neof. rRNA /L PCR
* #V00528-2 ^designation[0].language = #de-AT 
* #V00528-2 ^designation[0].value = "Cryptococcus neoformans rRNA /Liquor PCR" 
* #V00528-2 ^property[0].code = #parent 
* #V00528-2 ^property[0].valueCode = #10800 
* #10810 "Parasitologie"
* #10810 ^property[0].code = #parent 
* #10810 ^property[0].valueCode = #1100 
* #10810 ^property[1].code = #child 
* #10810 ^property[1].valueCode = #V00283 
* #V00283 "Kommentar Parasitologie"
* #V00283 ^definition = Komm.Parasitologie
* #V00283 ^designation[0].language = #de-AT 
* #V00283 ^designation[0].value = "Kommentar Parasitologie" 
* #V00283 ^property[0].code = #parent 
* #V00283 ^property[0].valueCode = #10810 
* #10820 "Makroskopie/Mikroskopie"
* #10820 ^property[0].code = #parent 
* #10820 ^property[0].valueCode = #1100 
* #10820 ^property[1].code = #child 
* #10820 ^property[1].valueCode = #V00690-0 
* #10820 ^property[2].code = #child 
* #10820 ^property[2].valueCode = #V00692-6 
* #10820 ^property[3].code = #child 
* #10820 ^property[3].valueCode = #V00693-4 
* #10820 ^property[4].code = #child 
* #10820 ^property[4].valueCode = #V00694-2 
* #10820 ^property[5].code = #child 
* #10820 ^property[5].valueCode = #V00695-9 
* #10820 ^property[6].code = #child 
* #10820 ^property[6].valueCode = #V00696-7 
* #10820 ^property[7].code = #child 
* #10820 ^property[7].valueCode = #V00697-5 
* #10820 ^property[8].code = #child 
* #10820 ^property[8].valueCode = #V00698-3 
* #10820 ^property[9].code = #child 
* #10820 ^property[9].valueCode = #V00699-1 
* #10820 ^property[10].code = #child 
* #10820 ^property[10].valueCode = #V00700-7 
* #10820 ^property[11].code = #child 
* #10820 ^property[11].valueCode = #V00701-5 
* #10820 ^property[12].code = #child 
* #10820 ^property[12].valueCode = #V00702-3 
* #10820 ^property[13].code = #child 
* #10820 ^property[13].valueCode = #V00703-1 
* #V00690-0 "Mikroorganismen mikr. /SM"
* #V00690-0 ^definition = Mikroorganismen m/SM
* #V00690-0 ^designation[0].language = #de-AT 
* #V00690-0 ^designation[0].value = "Mikroorganismen mikr. /Sondermaterial" 
* #V00690-0 ^property[0].code = #parent 
* #V00690-0 ^property[0].valueCode = #10820 
* #V00692-6 "Gramlabile Kokken mikr. /SM"
* #V00692-6 ^definition = Gramlab. Kokken m/SM
* #V00692-6 ^designation[0].language = #de-AT 
* #V00692-6 ^designation[0].value = "Gramlabile Kokken mikr. /Sondermaterial" 
* #V00692-6 ^property[0].code = #parent 
* #V00692-6 ^property[0].valueCode = #10820 
* #V00693-4 "Gramlabile Stäbchen mikr. /SM"
* #V00693-4 ^definition = Graml. Stäbchen m/SM
* #V00693-4 ^designation[0].language = #de-AT 
* #V00693-4 ^designation[0].value = "Gramlabile Stäbchen mikr. /Sondermaterial" 
* #V00693-4 ^property[0].code = #parent 
* #V00693-4 ^property[0].valueCode = #10820 
* #V00694-2 "Gramnegative Kokken mikr. /SM"
* #V00694-2 ^definition = Gramn. Kokken m./SM
* #V00694-2 ^designation[0].language = #de-AT 
* #V00694-2 ^designation[0].value = "Gramnegative Kokken mikr. /Sondermaterial" 
* #V00694-2 ^property[0].code = #parent 
* #V00694-2 ^property[0].valueCode = #10820 
* #V00695-9 "Gramnegative Stäbchen mikr. /SM"
* #V00695-9 ^definition = Gramn. Stäbchen m/SM
* #V00695-9 ^designation[0].language = #de-AT 
* #V00695-9 ^designation[0].value = "Gramnegative Stäbchen mikr. /Sondermaterial" 
* #V00695-9 ^property[0].code = #parent 
* #V00695-9 ^property[0].valueCode = #10820 
* #V00696-7 "Grampositive Kokken mikr. /SM"
* #V00696-7 ^definition = Gramp. Kokken m./SM
* #V00696-7 ^designation[0].language = #de-AT 
* #V00696-7 ^designation[0].value = "Grampositive Kokken mikr. /Sondermaterial" 
* #V00696-7 ^property[0].code = #parent 
* #V00696-7 ^property[0].valueCode = #10820 
* #V00697-5 "Grampositive Kokken (i. Haufen) mikr. /SM"
* #V00697-5 ^definition = Gramp Kokken (H)m/SM
* #V00697-5 ^designation[0].language = #de-AT 
* #V00697-5 ^designation[0].value = "Grampositive Kokken (i. Haufen) mikr. /SM" 
* #V00697-5 ^property[0].code = #parent 
* #V00697-5 ^property[0].valueCode = #10820 
* #V00698-3 "Grampositive Kokken (i. Ketten) mikr. /SM"
* #V00698-3 ^definition = Gramp Kokken (K)m/SM
* #V00698-3 ^designation[0].language = #de-AT 
* #V00698-3 ^designation[0].value = "Grampositive Kokken (i. Ketten) mikr. /SM" 
* #V00698-3 ^property[0].code = #parent 
* #V00698-3 ^property[0].valueCode = #10820 
* #V00699-1 "Grampositive Kokken (Lanzettform) mikr. /SM"
* #V00699-1 ^definition = Gramp Kokken(LF)m/SM
* #V00699-1 ^designation[0].language = #de-AT 
* #V00699-1 ^designation[0].value = "Grampositive Kokken (Lanzettform) mikr./SM" 
* #V00699-1 ^property[0].code = #parent 
* #V00699-1 ^property[0].valueCode = #10820 
* #V00700-7 "Grampositive Stäbchen mikr. /SM"
* #V00700-7 ^definition = Gramp.Stäbch. m./SM
* #V00700-7 ^designation[0].language = #de-AT 
* #V00700-7 ^designation[0].value = "Grampositive Stäbchen mikr. /Sondermaterial" 
* #V00700-7 ^property[0].code = #parent 
* #V00700-7 ^property[0].valueCode = #10820 
* #V00701-5 "Kokken mikr. /SM"
* #V00701-5 ^definition = Kokken m./SM
* #V00701-5 ^designation[0].language = #de-AT 
* #V00701-5 ^designation[0].value = "Kokken mikr. /Sondermaterial" 
* #V00701-5 ^property[0].code = #parent 
* #V00701-5 ^property[0].valueCode = #10820 
* #V00702-3 "Stäbchen mikr. /SM"
* #V00702-3 ^definition = Stäbchen m./SM
* #V00702-3 ^designation[0].language = #de-AT 
* #V00702-3 ^designation[0].value = "Stäbchen mikr. /Sondermaterial" 
* #V00702-3 ^property[0].code = #parent 
* #V00702-3 ^property[0].valueCode = #10820 
* #V00703-1 "Spirochäten mikr. /SM"
* #V00703-1 ^definition = Spirochäten m./SM
* #V00703-1 ^designation[0].language = #de-AT 
* #V00703-1 ^designation[0].value = "Spirochäten mikr. /Sondermaterial" 
* #V00703-1 ^property[0].code = #parent 
* #V00703-1 ^property[0].valueCode = #10820 
* #1300 "Autoimmundiagnostik"
* #1300 ^property[0].code = #parent 
* #1300 ^property[0].valueCode = #1 
* #1300 ^property[1].code = #child 
* #1300 ^property[1].valueCode = #11360 
* #1300 ^property[2].code = #child 
* #1300 ^property[2].valueCode = #11370 
* #1300 ^property[3].code = #child 
* #1300 ^property[3].valueCode = #11380 
* #1300 ^property[4].code = #child 
* #1300 ^property[4].valueCode = #11390 
* #1300 ^property[5].code = #child 
* #1300 ^property[5].valueCode = #11400 
* #1300 ^property[6].code = #child 
* #1300 ^property[6].valueCode = #11410 
* #1300 ^property[7].code = #child 
* #1300 ^property[7].valueCode = #11420 
* #1300 ^property[8].code = #child 
* #1300 ^property[8].valueCode = #11430 
* #1300 ^property[9].code = #child 
* #1300 ^property[9].valueCode = #11440 
* #1300 ^property[10].code = #child 
* #1300 ^property[10].valueCode = #11450 
* #1300 ^property[11].code = #child 
* #1300 ^property[11].valueCode = #11460 
* #1300 ^property[12].code = #child 
* #1300 ^property[12].valueCode = #11470 
* #11360 "Rheumatoide Arthritis-assoziierte Autoantikörper"
* #11360 ^property[0].code = #parent 
* #11360 ^property[0].valueCode = #1300 
* #11360 ^property[1].code = #child 
* #11360 ^property[1].valueCode = #V00248 
* #11360 ^property[2].code = #child 
* #11360 ^property[2].valueCode = #V00249 
* #11360 ^property[3].code = #child 
* #11360 ^property[3].valueCode = #V00458-2 
* #11360 ^property[4].code = #child 
* #11360 ^property[4].valueCode = #V00559-7 
* #V00248 "RA assoz. Autoak."
* #V00248 ^definition = RA assoz. Autoak.
* #V00248 ^designation[0].language = #de-AT 
* #V00248 ^designation[0].value = "Rheumatoide Arthritis-assoziierte Autoantikörper" 
* #V00248 ^property[0].code = #parent 
* #V00248 ^property[0].valueCode = #11360 
* #V00249 "RF IGG, IGA, IGM"
* #V00249 ^definition = RF IGG, IGA, IGM
* #V00249 ^designation[0].language = #de-AT 
* #V00249 ^designation[0].value = "Rheumafaktor IG Subtypen (IGG,IGA,IGM)" 
* #V00249 ^property[0].code = #parent 
* #V00249 ^property[0].valueCode = #11360 
* #V00458-2 "Heterogenes nukleäres Ribonukleoprotein A2"
* #V00458-2 ^definition = RA33
* #V00458-2 ^designation[0].language = #de-AT 
* #V00458-2 ^designation[0].value = "Heterogene nukleäres Ribonukleoprotein A2" 
* #V00458-2 ^property[0].code = #parent 
* #V00458-2 ^property[0].valueCode = #11360 
* #V00559-7 "Sa(ACPA) AK"
* #V00559-7 ^definition = Sa(ACPA) AK
* #V00559-7 ^designation[0].language = #de-AT 
* #V00559-7 ^designation[0].value = "Sa(ACPA) Antikörper" 
* #V00559-7 ^property[0].code = #parent 
* #V00559-7 ^property[0].valueCode = #11360 
* #11370 "Kollagenose-assoziierte Autoantikörper"
* #11370 ^property[0].code = #parent 
* #11370 ^property[0].valueCode = #1300 
* #11370 ^property[1].code = #child 
* #11370 ^property[1].valueCode = #V00126 
* #11370 ^property[2].code = #child 
* #11370 ^property[2].valueCode = #V00436-8 
* #11370 ^property[3].code = #child 
* #11370 ^property[3].valueCode = #V00490-5 
* #11370 ^property[4].code = #child 
* #11370 ^property[4].valueCode = #V00525-8 
* #11370 ^property[5].code = #child 
* #11370 ^property[5].valueCode = #V00552-2 
* #11370 ^property[6].code = #child 
* #11370 ^property[6].valueCode = #V00553-0 
* #11370 ^property[7].code = #child 
* #11370 ^property[7].valueCode = #V00569-6 
* #11370 ^property[8].code = #child 
* #11370 ^property[8].valueCode = #V00648-8 
* #11370 ^property[9].code = #child 
* #11370 ^property[9].valueCode = #V00649-6 
* #11370 ^property[10].code = #child 
* #11370 ^property[10].valueCode = #V00650-4 
* #11370 ^property[11].code = #child 
* #11370 ^property[11].valueCode = #V00651-2 
* #11370 ^property[12].code = #child 
* #11370 ^property[12].valueCode = #V00652-0 
* #11370 ^property[13].code = #child 
* #11370 ^property[13].valueCode = #V00714-8 
* #V00126 "Kollagenose assoz.AK"
* #V00126 ^definition = Kollagenose assoz.AK
* #V00126 ^designation[0].language = #de-AT 
* #V00126 ^designation[0].value = "Verdacht auf Kollagenosen" 
* #V00126 ^property[0].code = #parent 
* #V00126 ^property[0].valueCode = #11370 
* #V00436-8 "PCNA-AK"
* #V00436-8 ^definition = PCNA-AK
* #V00436-8 ^designation[0].language = #de-AT 
* #V00436-8 ^designation[0].value = "PCNA-Antikörper" 
* #V00436-8 ^property[0].code = #parent 
* #V00436-8 ^property[0].valueCode = #11370 
* #V00490-5 "Fibrillarin"
* #V00490-5 ^definition = Fibrillarin
* #V00490-5 ^designation[0].language = #de-AT 
* #V00490-5 ^designation[0].value = "Fibrillarin" 
* #V00490-5 ^property[0].code = #parent 
* #V00490-5 ^property[0].valueCode = #11370 
* #V00490-5 ^property[1].code = #status 
* #V00490-5 ^property[1].valueCode = #retired 
* #V00490-5 ^property[2].code = #Relationships 
* #V00490-5 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00490-5 ^property[3].code = #hints 
* #V00490-5 ^property[3].valueString = "DEPRECATED" 
* #V00525-8 "DFS70-AK ql."
* #V00525-8 ^definition = DFS70-AK ql.
* #V00525-8 ^designation[0].language = #de-AT 
* #V00525-8 ^designation[0].value = "DFS70-Antikörper qualitativ" 
* #V00525-8 ^property[0].code = #parent 
* #V00525-8 ^property[0].valueCode = #11370 
* #V00525-8 ^property[1].code = #status 
* #V00525-8 ^property[1].valueCode = #retired 
* #V00525-8 ^property[2].code = #Relationships 
* #V00525-8 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00525-8 ^property[3].code = #hints 
* #V00525-8 ^property[3].valueString = "DEPRECATED" 
* #V00552-2 "Alpha-Fodrin AK IgG"
* #V00552-2 ^definition = Alpha-Fodrin AK IgG
* #V00552-2 ^designation[0].language = #de-AT 
* #V00552-2 ^designation[0].value = "Alpha-Fodrin Antikörper IgG" 
* #V00552-2 ^property[0].code = #parent 
* #V00552-2 ^property[0].valueCode = #11370 
* #V00553-0 "Alpha-Fodrin AK IgA"
* #V00553-0 ^definition = Alpha-Fodrin AK IgA
* #V00553-0 ^designation[0].language = #de-AT 
* #V00553-0 ^designation[0].value = "Alpha-Fodrin Antikörper IgA" 
* #V00553-0 ^property[0].code = #parent 
* #V00553-0 ^property[0].valueCode = #11370 
* #V00569-6 "SmD-AK"
* #V00569-6 ^definition = SmD-AK
* #V00569-6 ^designation[0].language = #de-AT 
* #V00569-6 ^designation[0].value = "SmD-Antikörper" 
* #V00569-6 ^property[0].code = #parent 
* #V00569-6 ^property[0].valueCode = #11370 
* #V00648-8 "SmB-AK qual. IB"
* #V00648-8 ^definition = SmB-AK qual. IB
* #V00648-8 ^designation[0].language = #de-AT 
* #V00648-8 ^designation[0].value = "SmB-Antikörper qual. IB" 
* #V00648-8 ^property[0].code = #parent 
* #V00648-8 ^property[0].valueCode = #11370 
* #V00648-8 ^property[1].code = #status 
* #V00648-8 ^property[1].valueCode = #retired 
* #V00648-8 ^property[2].code = #Relationships 
* #V00648-8 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00648-8 ^property[3].code = #hints 
* #V00648-8 ^property[3].valueString = "DEPRECATED" 
* #V00649-6 "SmD-AK qual. IB"
* #V00649-6 ^definition = SmD-AK qual. IB
* #V00649-6 ^designation[0].language = #de-AT 
* #V00649-6 ^designation[0].value = "SmD-Antikörper qual. IB" 
* #V00649-6 ^property[0].code = #parent 
* #V00649-6 ^property[0].valueCode = #11370 
* #V00649-6 ^property[1].code = #status 
* #V00649-6 ^property[1].valueCode = #retired 
* #V00649-6 ^property[2].code = #Relationships 
* #V00649-6 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00649-6 ^property[3].code = #hints 
* #V00649-6 ^property[3].valueString = "DEPRECATED" 
* #V00650-4 "U1-snRNP-70-AK qual. IB"
* #V00650-4 ^definition = U1-snRNP-70-AK ql IB
* #V00650-4 ^designation[0].language = #de-AT 
* #V00650-4 ^designation[0].value = "U1-snRNP-70-Antikörper qual. IB" 
* #V00650-4 ^property[0].code = #parent 
* #V00650-4 ^property[0].valueCode = #11370 
* #V00650-4 ^property[1].code = #status 
* #V00650-4 ^property[1].valueCode = #retired 
* #V00650-4 ^property[2].code = #Relationships 
* #V00650-4 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00650-4 ^property[3].code = #hints 
* #V00650-4 ^property[3].valueString = "DEPRECATED" 
* #V00651-2 "U1-snRNP-A-AK ql. IB"
* #V00651-2 ^definition = U1-snRNP-A-AK ql. IB
* #V00651-2 ^designation[0].language = #de-AT 
* #V00651-2 ^designation[0].value = "U1-snRNP-A-Antikörper qual. IB" 
* #V00651-2 ^property[0].code = #parent 
* #V00651-2 ^property[0].valueCode = #11370 
* #V00651-2 ^property[1].code = #status 
* #V00651-2 ^property[1].valueCode = #retired 
* #V00651-2 ^property[2].code = #Relationships 
* #V00651-2 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00651-2 ^property[3].code = #hints 
* #V00651-2 ^property[3].valueString = "DEPRECATED" 
* #V00652-0 "U1-snRNP-C-AK ql. IB"
* #V00652-0 ^definition = U1-snRNP-C-AK ql. IB
* #V00652-0 ^designation[0].language = #de-AT 
* #V00652-0 ^designation[0].value = "U1-snRNP-C-Antikörper qual. IB" 
* #V00652-0 ^property[0].code = #parent 
* #V00652-0 ^property[0].valueCode = #11370 
* #V00652-0 ^property[1].code = #status 
* #V00652-0 ^property[1].valueCode = #retired 
* #V00652-0 ^property[2].code = #Relationships 
* #V00652-0 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00652-0 ^property[3].code = #hints 
* #V00652-0 ^property[3].valueString = "DEPRECATED" 
* #V00714-8 "DFS70-AK"
* #V00714-8 ^definition = DFS70-AK
* #V00714-8 ^designation[0].language = #de-AT 
* #V00714-8 ^designation[0].value = "DFS70-Antikörper" 
* #V00714-8 ^property[0].code = #parent 
* #V00714-8 ^property[0].valueCode = #11370 
* #V00714-8 ^property[1].code = #status 
* #V00714-8 ^property[1].valueCode = #retired 
* #V00714-8 ^property[2].code = #Relationships 
* #V00714-8 ^property[2].valueString = "verwendbar bis 31.12.2020" 
* #V00714-8 ^property[3].code = #hints 
* #V00714-8 ^property[3].valueString = "DEPRECATED" 
* #11380 "Myositis-assoziierte Autoantikörper"
* #11380 ^property[0].code = #parent 
* #11380 ^property[0].valueCode = #1300 
* #11380 ^property[1].code = #child 
* #11380 ^property[1].valueCode = #V00127 
* #11380 ^property[2].code = #child 
* #11380 ^property[2].valueCode = #V00554-8 
* #11380 ^property[3].code = #child 
* #11380 ^property[3].valueCode = #V00555-5 
* #11380 ^property[4].code = #child 
* #11380 ^property[4].valueCode = #V00556-3 
* #11380 ^property[5].code = #child 
* #11380 ^property[5].valueCode = #V00557-1 
* #11380 ^property[6].code = #child 
* #11380 ^property[6].valueCode = #V00558-9 
* #V00127 "Myositis assoz.AK"
* #V00127 ^definition = Myositis assoz.AK
* #V00127 ^designation[0].language = #de-AT 
* #V00127 ^designation[0].value = "Verdacht auf Myositis" 
* #V00127 ^property[0].code = #parent 
* #V00127 ^property[0].valueCode = #11380 
* #V00554-8 "MDA5 AK ql."
* #V00554-8 ^definition = MDA5 AK ql.
* #V00554-8 ^designation[0].language = #de-AT 
* #V00554-8 ^designation[0].value = "MDA5 Antikörper qualitativ" 
* #V00554-8 ^property[0].code = #parent 
* #V00554-8 ^property[0].valueCode = #11380 
* #V00555-5 "TIF1-gamma AK ql."
* #V00555-5 ^definition = TIF1-gamma AK ql.
* #V00555-5 ^designation[0].language = #de-AT 
* #V00555-5 ^designation[0].value = "TIF1-gamma Antikörper qualitativ" 
* #V00555-5 ^property[0].code = #parent 
* #V00555-5 ^property[0].valueCode = #11380 
* #V00556-3 "HMGCR AK ql."
* #V00556-3 ^definition = HMGCR AK ql.
* #V00556-3 ^designation[0].language = #de-AT 
* #V00556-3 ^designation[0].value = "HMGCR Antikörper qualitativ" 
* #V00556-3 ^property[0].code = #parent 
* #V00556-3 ^property[0].valueCode = #11380 
* #V00557-1 "SAE-1 AK ql."
* #V00557-1 ^definition = SAE-1 AK ql.
* #V00557-1 ^designation[0].language = #de-AT 
* #V00557-1 ^designation[0].value = "SAE-1 Antikörper qualitativ" 
* #V00557-1 ^property[0].code = #parent 
* #V00557-1 ^property[0].valueCode = #11380 
* #V00557-1 ^property[1].code = #status 
* #V00557-1 ^property[1].valueCode = #retired 
* #V00557-1 ^property[2].code = #Relationships 
* #V00557-1 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00557-1 ^property[3].code = #hints 
* #V00557-1 ^property[3].valueString = "DEPRECATED" 
* #V00558-9 "NXP2 AK ql."
* #V00558-9 ^definition = NXP2 AK ql.
* #V00558-9 ^designation[0].language = #de-AT 
* #V00558-9 ^designation[0].value = "NXP2 Antikörper qualitativ" 
* #V00558-9 ^property[0].code = #parent 
* #V00558-9 ^property[0].valueCode = #11380 
* #11390 "Lebererkrankungen-assoziierte Autoantikörper"
* #11390 ^property[0].code = #parent 
* #11390 ^property[0].valueCode = #1300 
* #11390 ^property[1].code = #child 
* #11390 ^property[1].valueCode = #V00128 
* #11390 ^property[2].code = #child 
* #11390 ^property[2].valueCode = #V00129 
* #11390 ^property[3].code = #child 
* #11390 ^property[3].valueCode = #V00130 
* #11390 ^property[4].code = #child 
* #11390 ^property[4].valueCode = #V00454-1 
* #V00128 "Lebererkr.assoz.AK"
* #V00128 ^definition = Lebererkr.assoz.AK
* #V00128 ^designation[0].language = #de-AT 
* #V00128 ^designation[0].value = "Verdacht auf Autoimmunhepatitis/PBC" 
* #V00128 ^property[0].code = #parent 
* #V00128 ^property[0].valueCode = #11390 
* #V00129 "SMA(ASMA)IF Interpr."
* #V00129 ^definition = SMA(ASMA)IF Interpr.
* #V00129 ^designation[0].language = #de-AT 
* #V00129 ^designation[0].value = "SMA (ASMA) IF Interpretation" 
* #V00129 ^property[0].code = #parent 
* #V00129 ^property[0].valueCode = #11390 
* #V00130 "SMA(ASMA) Interpret."
* #V00130 ^definition = SMA(ASMA) Interpret.
* #V00130 ^designation[0].language = #de-AT 
* #V00130 ^designation[0].value = "SMA (ASMA) Interpretation" 
* #V00130 ^property[0].code = #parent 
* #V00130 ^property[0].valueCode = #11390 
* #V00454-1 "GP210-AK"
* #V00454-1 ^definition = GP210-AK
* #V00454-1 ^designation[0].language = #de-AT 
* #V00454-1 ^designation[0].value = "GP210-Antikörper" 
* #V00454-1 ^property[0].code = #parent 
* #V00454-1 ^property[0].valueCode = #11390 
* #11400 "Perniziöse Anämie assoziierte Autoantikörper"
* #11400 ^property[0].code = #parent 
* #11400 ^property[0].valueCode = #1300 
* #11400 ^property[1].code = #child 
* #11400 ^property[1].valueCode = #V00131 
* #11400 ^property[2].code = #child 
* #11400 ^property[2].valueCode = #V00653-8 
* #V00131 "perniz.Anämie.ass.AK"
* #V00131 ^definition = perniz.Anämie.ass.AK
* #V00131 ^designation[0].language = #de-AT 
* #V00131 ^designation[0].value = "Verdacht auf perniziöse Anämie" 
* #V00131 ^property[0].code = #parent 
* #V00131 ^property[0].valueCode = #11400 
* #V00653-8 "Intrinsic-Faktor-AK ql. IB"
* #V00653-8 ^definition = IF-AK ql. IB
* #V00653-8 ^designation[0].language = #de-AT 
* #V00653-8 ^designation[0].value = "Intrinsic-Faktor-Antikörper qual. IB" 
* #V00653-8 ^property[0].code = #parent 
* #V00653-8 ^property[0].valueCode = #11400 
* #V00653-8 ^property[1].code = #status 
* #V00653-8 ^property[1].valueCode = #retired 
* #V00653-8 ^property[2].code = #Relationships 
* #V00653-8 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00653-8 ^property[3].code = #hints 
* #V00653-8 ^property[3].valueString = "DEPRECATED" 
* #11410 "Vasculitis-assoziierte Antikörper"
* #11410 ^property[0].code = #parent 
* #11410 ^property[0].valueCode = #1300 
* #11410 ^property[1].code = #child 
* #11410 ^property[1].valueCode = #V00058 
* #11410 ^property[2].code = #child 
* #11410 ^property[2].valueCode = #V00059 
* #V00058 "MPO-AK cELISA"
* #V00058 ^definition = MPO-AK cELISA
* #V00058 ^designation[0].language = #de-AT 
* #V00058 ^designation[0].value = "MPO-Antikörper cELISA" 
* #V00058 ^property[0].code = #parent 
* #V00058 ^property[0].valueCode = #11410 
* #V00059 "PR3-AK cELISA"
* #V00059 ^definition = PR3-AK cELISA
* #V00059 ^designation[0].language = #de-AT 
* #V00059 ^designation[0].value = "PR3-Antikörper cELISA" 
* #V00059 ^property[0].code = #parent 
* #V00059 ^property[0].valueCode = #11410 
* #11420 "Goodpasture Syndrom assoziierte Autoantikörper"
* #11420 ^property[0].code = #parent 
* #11420 ^property[0].valueCode = #1300 
* #11420 ^property[1].code = #child 
* #11420 ^property[1].valueCode = #V00132 
* #V00132 "GBM assoz.AK"
* #V00132 ^definition = GBM assoz.AK
* #V00132 ^designation[0].language = #de-AT 
* #V00132 ^designation[0].value = "Verdacht auf Goodpasture Syndrom" 
* #V00132 ^property[0].code = #parent 
* #V00132 ^property[0].valueCode = #11420 
* #11430 "IBD assoziierte Autoantikörper"
* #11430 ^property[0].code = #parent 
* #11430 ^property[0].valueCode = #1300 
* #11430 ^property[1].code = #child 
* #11430 ^property[1].valueCode = #V00133 
* #V00133 "IBD assoz. AK"
* #V00133 ^definition = IBD assoz. AK
* #V00133 ^designation[0].language = #de-AT 
* #V00133 ^designation[0].value = "Verdacht auf M. Crohn/Colitis Ulcerosa" 
* #V00133 ^property[0].code = #parent 
* #V00133 ^property[0].valueCode = #11430 
* #11440 "Zöliakie assoziierte Autoantikörper"
* #11440 ^property[0].code = #parent 
* #11440 ^property[0].valueCode = #1300 
* #11440 ^property[1].code = #child 
* #11440 ^property[1].valueCode = #V00134 
* #11440 ^property[2].code = #child 
* #11440 ^property[2].valueCode = #V00135 
* #V00134 "Zöliakie assoz. AK"
* #V00134 ^definition = Zöliakie assoz. AK
* #V00134 ^designation[0].language = #de-AT 
* #V00134 ^designation[0].value = "Verdacht auf Zöliakie" 
* #V00134 ^property[0].code = #parent 
* #V00134 ^property[0].valueCode = #11440 
* #V00135 "Zöliakie Interprt."
* #V00135 ^definition = Zöliakie Interprt.
* #V00135 ^designation[0].language = #de-AT 
* #V00135 ^designation[0].value = "Zöliakie Interpretation" 
* #V00135 ^property[0].code = #parent 
* #V00135 ^property[0].valueCode = #11440 
* #11450 "Diabetes Mellitus assoziierte Autoantikörper"
* #11450 ^property[0].code = #parent 
* #11450 ^property[0].valueCode = #1300 
* #11450 ^property[1].code = #child 
* #11450 ^property[1].valueCode = #V00245 
* #V00245 "DM I assoz. AutoAK."
* #V00245 ^definition = DM I assoz. AutoAK.
* #V00245 ^designation[0].language = #de-AT 
* #V00245 ^designation[0].value = "Diabetes Mellitus assoziierte Autoantikörper" 
* #V00245 ^property[0].code = #parent 
* #V00245 ^property[0].valueCode = #11450 
* #11460 "Paraneoplasie assoziierte Autoantikörper"
* #11460 ^property[0].code = #parent 
* #11460 ^property[0].valueCode = #1300 
* #11460 ^property[1].code = #child 
* #11460 ^property[1].valueCode = #V00487-1 
* #11460 ^property[2].code = #child 
* #11460 ^property[2].valueCode = #V00488-9 
* #11460 ^property[3].code = #child 
* #11460 ^property[3].valueCode = #V00580-3 
* #11460 ^property[4].code = #child 
* #11460 ^property[4].valueCode = #V00581-1 
* #11460 ^property[5].code = #child 
* #11460 ^property[5].valueCode = #V00586-0 
* #11460 ^property[6].code = #child 
* #11460 ^property[6].valueCode = #V00587-8 
* #11460 ^property[7].code = #child 
* #11460 ^property[7].valueCode = #V00588-6 
* #V00487-1 "Anti-Recoverin"
* #V00487-1 ^definition = Anti-Recoverin
* #V00487-1 ^designation[0].language = #de-AT 
* #V00487-1 ^designation[0].value = "Anti-Recoverin" 
* #V00487-1 ^property[0].code = #parent 
* #V00487-1 ^property[0].valueCode = #11460 
* #V00488-9 "Anti-Titin"
* #V00488-9 ^definition = Anti-Titin
* #V00488-9 ^designation[0].language = #de-AT 
* #V00488-9 ^designation[0].value = "Anti-Titin" 
* #V00488-9 ^property[0].code = #parent 
* #V00488-9 ^property[0].valueCode = #11460 
* #V00580-3 "Anti-GAD65 ql. /L"
* #V00580-3 ^definition = Anti-GAD65 ql. /L
* #V00580-3 ^designation[0].language = #de-AT 
* #V00580-3 ^designation[0].value = "Anti-GAD65 qualitativ /Liquor" 
* #V00580-3 ^property[0].code = #parent 
* #V00580-3 ^property[0].valueCode = #11460 
* #V00581-1 "MOG-IgG AK ql. IF /L"
* #V00581-1 ^definition = MOG-IgG AK ql. IF /L
* #V00581-1 ^designation[0].language = #de-AT 
* #V00581-1 ^designation[0].value = "MOG-IgG Anikörper qualitativ IF /Liquor" 
* #V00581-1 ^property[0].code = #parent 
* #V00581-1 ^property[0].valueCode = #11460 
* #V00586-0 "Titin-AK /L"
* #V00586-0 ^definition = Titin-AK /L
* #V00586-0 ^designation[0].language = #de-AT 
* #V00586-0 ^designation[0].value = "Titin-Antikörper /Liquor" 
* #V00586-0 ^property[0].code = #parent 
* #V00586-0 ^property[0].valueCode = #11460 
* #V00587-8 "Recoverin-AK /L"
* #V00587-8 ^definition = Recoverin-AK /L
* #V00587-8 ^designation[0].language = #de-AT 
* #V00587-8 ^designation[0].value = "Recoverin-Antikörper /Liquor" 
* #V00587-8 ^property[0].code = #parent 
* #V00587-8 ^property[0].valueCode = #11460 
* #V00588-6 "Zic4-AK /L"
* #V00588-6 ^definition = Zic4-AK /L
* #V00588-6 ^designation[0].language = #de-AT 
* #V00588-6 ^designation[0].value = "Zic4-Antikörper /Liquor" 
* #V00588-6 ^property[0].code = #parent 
* #V00588-6 ^property[0].valueCode = #11460 
* #11470 "Sonstige Autoantikörper"
* #11470 ^property[0].code = #parent 
* #11470 ^property[0].valueCode = #1300 
* #11470 ^property[1].code = #child 
* #11470 ^property[1].valueCode = #V00450-9 
* #11470 ^property[2].code = #child 
* #11470 ^property[2].valueCode = #V00534-0 
* #11470 ^property[3].code = #child 
* #11470 ^property[3].valueCode = #V00560-5 
* #11470 ^property[4].code = #child 
* #11470 ^property[4].valueCode = #V00583-7 
* #11470 ^property[5].code = #child 
* #11470 ^property[5].valueCode = #V00584-5 
* #11470 ^property[6].code = #child 
* #11470 ^property[6].valueCode = #V00585-2 
* #V00450-9 "KSL IF ql."
* #V00450-9 ^definition = KSL IF ql.
* #V00450-9 ^designation[0].language = #de-AT 
* #V00450-9 ^designation[0].value = "Kidney-Stomach-Liver IF qualitativ" 
* #V00450-9 ^property[0].code = #parent 
* #V00450-9 ^property[0].valueCode = #11470 
* #V00534-0 "Phospholipase-A2-Rezeptor-AK IgG IF ql."
* #V00534-0 ^definition = PL-A2R-AK IgG IF ql.
* #V00534-0 ^designation[0].language = #de-AT 
* #V00534-0 ^designation[0].value = "Phospholipase-A2-Rezeptor-AK IgG IF qualitativ" 
* #V00534-0 ^property[0].code = #parent 
* #V00534-0 ^property[0].valueCode = #11470 
* #V00560-5 "Herzmuskel AK IF ql."
* #V00560-5 ^definition = Herzmuskel AK IF ql.
* #V00560-5 ^designation[0].language = #de-AT 
* #V00560-5 ^designation[0].value = "Herzmuskel Antikörper IF qualitativ" 
* #V00560-5 ^property[0].code = #parent 
* #V00560-5 ^property[0].valueCode = #11470 
* #V00560-5 ^property[1].code = #status 
* #V00560-5 ^property[1].valueCode = #retired 
* #V00560-5 ^property[2].code = #Relationships 
* #V00560-5 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00560-5 ^property[3].code = #hints 
* #V00560-5 ^property[3].valueString = "DEPRECATED" 
* #V00583-7 "Humane-Epidermale-Transglutaminase-AK IgA"
* #V00583-7 ^definition = HETG-AK IgA
* #V00583-7 ^designation[0].language = #de-AT 
* #V00583-7 ^designation[0].value = "Humane-Epidermale-Transglutaminase-Antikörper IgA" 
* #V00583-7 ^property[0].code = #parent 
* #V00583-7 ^property[0].valueCode = #11470 
* #V00584-5 "Kollagen-Typ-VII-AK ql."
* #V00584-5 ^definition = Kollagen-7-AK ql.
* #V00584-5 ^designation[0].language = #de-AT 
* #V00584-5 ^designation[0].value = "Kollagen-Typ-VII-Antikörper qualitativ" 
* #V00584-5 ^property[0].code = #parent 
* #V00584-5 ^property[0].valueCode = #11470 
* #V00585-2 "Envoplakin-AK ql."
* #V00585-2 ^definition = Envoplakin-AK ql.
* #V00585-2 ^designation[0].language = #de-AT 
* #V00585-2 ^designation[0].value = "Envoplakin-Antikörper qualitativ" 
* #V00585-2 ^property[0].code = #parent 
* #V00585-2 ^property[0].valueCode = #11470 
* #1400 "Urindiagnostik"
* #1400 ^property[0].code = #parent 
* #1400 ^property[0].valueCode = #1 
* #1400 ^property[1].code = #child 
* #1400 ^property[1].valueCode = #13730 
* #1400 ^property[2].code = #child 
* #1400 ^property[2].valueCode = #13740 
* #1400 ^property[3].code = #child 
* #1400 ^property[3].valueCode = #13750 
* #13730 "Urinstreifen"
* #13730 ^property[0].code = #parent 
* #13730 ^property[0].valueCode = #1400 
* #13740 "Urinsediment"
* #13740 ^property[0].code = #parent 
* #13740 ^property[0].valueCode = #1400 
* #13740 ^property[1].code = #child 
* #13740 ^property[1].valueCode = #V00022 
* #13740 ^property[2].code = #child 
* #13740 ^property[2].valueCode = #V00038 
* #13740 ^property[3].code = #child 
* #13740 ^property[3].valueCode = #V00039 
* #13740 ^property[4].code = #child 
* #13740 ^property[4].valueCode = #V00040 
* #13740 ^property[5].code = #child 
* #13740 ^property[5].valueCode = #V00572-0 
* #13740 ^property[6].code = #child 
* #13740 ^property[6].valueCode = #V00665-2 
* #13740 ^property[7].code = #child 
* #13740 ^property[7].valueCode = #V00666-0 
* #13740 ^property[8].code = #child 
* #13740 ^property[8].valueCode = #V00677-7 
* #V00022 "Rundepithelien /US"
* #V00022 ^definition = Rundepithelien /US
* #V00022 ^designation[0].language = #de-AT 
* #V00022 ^designation[0].value = "Rundepithelien /Urinsediment" 
* #V00022 ^property[0].code = #parent 
* #V00022 ^property[0].valueCode = #13740 
* #V00022 ^property[1].code = #status 
* #V00022 ^property[1].valueCode = #retired 
* #V00022 ^property[2].code = #Relationships 
* #V00022 ^property[2].valueString = "verwendbar bis 30.06.2019" 
* #V00022 ^property[3].code = #hints 
* #V00022 ^property[3].valueString = "DEPRECATED" 
* #V00038 "Pilze /US"
* #V00038 ^definition = Pilze /US
* #V00038 ^designation[0].language = #de-AT 
* #V00038 ^designation[0].value = "Pilze /Urinsediment mikr." 
* #V00038 ^property[0].code = #parent 
* #V00038 ^property[0].valueCode = #13740 
* #V00039 "Rundepithelien /U"
* #V00039 ^definition = Rundepithelien /U
* #V00039 ^designation[0].language = #de-AT 
* #V00039 ^designation[0].value = "Rundepithelien /U" 
* #V00039 ^property[0].code = #parent 
* #V00039 ^property[0].valueCode = #13740 
* #V00039 ^property[1].code = #status 
* #V00039 ^property[1].valueCode = #retired 
* #V00039 ^property[2].code = #Relationships 
* #V00039 ^property[2].valueString = "verwendbar bis 30.06.2019" 
* #V00039 ^property[3].code = #hints 
* #V00039 ^property[3].valueString = "DEPRECATED" 
* #V00040 "Pilze /U"
* #V00040 ^definition = Pilze /U
* #V00040 ^designation[0].language = #de-AT 
* #V00040 ^designation[0].value = "Pilze /U" 
* #V00040 ^property[0].code = #parent 
* #V00040 ^property[0].valueCode = #13740 
* #V00572-0 "Makrophagen /US"
* #V00572-0 ^definition = Makrophagen /US
* #V00572-0 ^designation[0].language = #de-AT 
* #V00572-0 ^designation[0].value = "Makrophagen /Urinsediment" 
* #V00572-0 ^property[0].code = #parent 
* #V00572-0 ^property[0].valueCode = #13740 
* #V00665-2 "Rundepithelien /U"
* #V00665-2 ^definition = Rundepithelien /U
* #V00665-2 ^designation[0].language = #de-AT 
* #V00665-2 ^designation[0].value = "Rundepithelien /Urin" 
* #V00665-2 ^property[0].code = #parent 
* #V00665-2 ^property[0].valueCode = #13740 
* #V00665-2 ^property[1].code = #status 
* #V00665-2 ^property[1].valueCode = #retired 
* #V00665-2 ^property[2].code = #Relationships 
* #V00665-2 ^property[2].valueString = "verwendbar bis 30.06.2019" 
* #V00665-2 ^property[3].code = #hints 
* #V00665-2 ^property[3].valueString = "DEPRECATED" 
* #V00666-0 "Pseudozylinder /U"
* #V00666-0 ^definition = Pseudozylinder /U
* #V00666-0 ^designation[0].language = #de-AT 
* #V00666-0 ^designation[0].value = "Pseudozylinder /Urin" 
* #V00666-0 ^property[0].code = #parent 
* #V00666-0 ^property[0].valueCode = #13740 
* #V00666-0 ^property[1].code = #status 
* #V00666-0 ^property[1].valueCode = #retired 
* #V00666-0 ^property[2].code = #Relationships 
* #V00666-0 ^property[2].valueString = "verwendbar bis 30.06.2019" 
* #V00666-0 ^property[3].code = #hints 
* #V00666-0 ^property[3].valueString = "DEPRECATED" 
* #V00677-7 "Akanthozyten /US"
* #V00677-7 ^definition = Akanthozyten /US
* #V00677-7 ^designation[0].language = #de-AT 
* #V00677-7 ^designation[0].value = "Akanthozyten /Urinsediment" 
* #V00677-7 ^property[0].code = #parent 
* #V00677-7 ^property[0].valueCode = #13740 
* #V00677-7 ^property[1].code = #status 
* #V00677-7 ^property[1].valueCode = #retired 
* #V00677-7 ^property[2].code = #Relationships 
* #V00677-7 ^property[2].valueString = "verwendbar bis 30.06.2019" 
* #V00677-7 ^property[3].code = #hints 
* #V00677-7 ^property[3].valueString = "DEPRECATED" 
* #13750 "Urinchemie"
* #13750 ^property[0].code = #parent 
* #13750 ^property[0].valueCode = #1400 
* #13750 ^property[1].code = #child 
* #13750 ^property[1].valueCode = #V00009 
* #13750 ^property[2].code = #child 
* #13750 ^property[2].valueCode = #V00166 
* #13750 ^property[3].code = #child 
* #13750 ^property[3].valueCode = #V00167 
* #13750 ^property[4].code = #child 
* #13750 ^property[4].valueCode = #V00168 
* #13750 ^property[5].code = #child 
* #13750 ^property[5].valueCode = #V00298 
* #13750 ^property[6].code = #child 
* #13750 ^property[6].valueCode = #V00299 
* #13750 ^property[7].code = #child 
* #13750 ^property[7].valueCode = #V00300 
* #13750 ^property[8].code = #child 
* #13750 ^property[8].valueCode = #V00301 
* #13750 ^property[9].code = #child 
* #13750 ^property[9].valueCode = #V00451-7 
* #13750 ^property[10].code = #child 
* #13750 ^property[10].valueCode = #V00452-5 
* #13750 ^property[11].code = #child 
* #13750 ^property[11].valueCode = #V00576-1 
* #13750 ^property[12].code = #child 
* #13750 ^property[12].valueCode = #V00577-9 
* #13750 ^property[13].code = #child 
* #13750 ^property[13].valueCode = #V00609-0 
* #V00009 "Proteine /U SDS-Elp."
* #V00009 ^definition = Proteine /U SDS-Elp.
* #V00009 ^designation[0].language = #de-AT 
* #V00009 ^designation[0].value = "Proteine /Urin SDS-Elektrophorese" 
* #V00009 ^property[0].code = #parent 
* #V00009 ^property[0].valueCode = #13750 
* #V00166 "3-OH-Butters.U/Krea"
* #V00166 ^definition = 3-OH-Butters.U/Krea
* #V00166 ^designation[0].language = #de-AT 
* #V00166 ^designation[0].value = "3-OH-Butters.U/Krea" 
* #V00166 ^property[0].code = #parent 
* #V00166 ^property[0].valueCode = #13750 
* #V00167 "3OHButters.24hU/Krea"
* #V00167 ^definition = 3OHButters.24hU/Krea
* #V00167 ^designation[0].language = #de-AT 
* #V00167 ^designation[0].value = "3-OH-Butters.24 h Sammelurin/Krea" 
* #V00167 ^property[0].code = #parent 
* #V00167 ^property[0].valueCode = #13750 
* #V00168 "4OH-Phenylac.24hU/Kr"
* #V00168 ^definition = 4OH-Phenylac.24hU/Kr
* #V00168 ^designation[0].language = #de-AT 
* #V00168 ^designation[0].value = "4-OH-Phenylac.24 h Sammelurin/Krea" 
* #V00168 ^property[0].code = #parent 
* #V00168 ^property[0].valueCode = #13750 
* #V00298 "Urinpr.Entnahmeztp."
* #V00298 ^definition = Urinpr.Entnahmeztp.
* #V00298 ^designation[0].language = #de-AT 
* #V00298 ^designation[0].value = "Urinprobe Entnahmezeitpunkt" 
* #V00298 ^property[0].code = #parent 
* #V00298 ^property[0].valueCode = #13750 
* #V00298 ^property[1].code = #status 
* #V00298 ^property[1].valueCode = #retired 
* #V00298 ^property[2].code = #Relationships 
* #V00298 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00298 ^property[3].code = #hints 
* #V00298 ^property[3].valueString = "DEPRECATED" 
* #V00299 "Urinprobe"
* #V00299 ^definition = Urinprobe
* #V00299 ^designation[0].language = #de-AT 
* #V00299 ^designation[0].value = "Urinprobe" 
* #V00299 ^property[0].code = #parent 
* #V00299 ^property[0].valueCode = #13750 
* #V00300 "Urinprobe 2"
* #V00300 ^definition = Urinprobe 2
* #V00300 ^designation[0].language = #de-AT 
* #V00300 ^designation[0].value = "Urinprobe 2" 
* #V00300 ^property[0].code = #parent 
* #V00300 ^property[0].valueCode = #13750 
* #V00301 "Urinprobe 3"
* #V00301 ^definition = Urinprobe 3
* #V00301 ^designation[0].language = #de-AT 
* #V00301 ^designation[0].value = "Urinprobe 3" 
* #V00301 ^property[0].code = #parent 
* #V00301 ^property[0].valueCode = #13750 
* #V00451-7 "Albumin Teststr. (Mikroalbumin) /Urin"
* #V00451-7 ^definition = Albumin Teststrf. /U
* #V00451-7 ^designation[0].language = #de-AT 
* #V00451-7 ^designation[0].value = "Albumin Streifentest (Mikroalbumin) /Urin" 
* #V00451-7 ^property[0].code = #parent 
* #V00451-7 ^property[0].valueCode = #13750 
* #V00452-5 "Sulfit /U"
* #V00452-5 ^definition = Sulfit /U
* #V00452-5 ^designation[0].language = #de-AT 
* #V00452-5 ^designation[0].value = "Sulfit /Urin" 
* #V00452-5 ^property[0].code = #parent 
* #V00452-5 ^property[0].valueCode = #13750 
* #V00576-1 "IgG/Albumin-Ratio /U"
* #V00576-1 ^definition = IgG/Albumin-Ratio /U
* #V00576-1 ^designation[0].language = #de-AT 
* #V00576-1 ^designation[0].value = "IgG/Albumin-Ratio /Urin" 
* #V00576-1 ^property[0].code = #parent 
* #V00576-1 ^property[0].valueCode = #13750 
* #V00577-9 "Alpha-2-Makroglobulin/Albumin-Ratio /U"
* #V00577-9 ^definition = A2M/Albumin-Ratio /U
* #V00577-9 ^designation[0].language = #de-AT 
* #V00577-9 ^designation[0].value = "Alpha-2-Makroglobulin/Albumin-Ratio /Urin" 
* #V00577-9 ^property[0].code = #parent 
* #V00577-9 ^property[0].valueCode = #13750 
* #V00609-0 "A1-Mikrog./Alb.-Rto /U"
* #V00609-0 ^definition = A1-Mikrog./Alb-Rto/U
* #V00609-0 ^designation[0].language = #de-AT 
* #V00609-0 ^designation[0].value = "Alpha-1-Mikroglobulin/Albumin-Ratio /Urin" 
* #V00609-0 ^property[0].code = #parent 
* #V00609-0 ^property[0].valueCode = #13750 
* #1500 "Stuhldiagnostik"
* #1500 ^property[0].code = #parent 
* #1500 ^property[0].valueCode = #1 
* #1500 ^property[1].code = #child 
* #1500 ^property[1].valueCode = #14760 
* #14760 "Stuhldiagnostik"
* #14760 ^property[0].code = #parent 
* #14760 ^property[0].valueCode = #1500 
* #14760 ^property[1].code = #child 
* #14760 ^property[1].valueCode = #V00286 
* #14760 ^property[2].code = #child 
* #14760 ^property[2].valueCode = #V00765-0 
* #V00286 "Komm.Stuhldiagnostik"
* #V00286 ^definition = Komm.Stuhldiagnostik
* #V00286 ^designation[0].language = #de-AT 
* #V00286 ^designation[0].value = "Kommentar Stuhldiagnostik" 
* #V00286 ^property[0].code = #parent 
* #V00286 ^property[0].valueCode = #14760 
* #V00765-0 "M2-PK /ST"
* #V00765-0 ^definition = M2-PK /ST
* #V00765-0 ^designation[0].language = #de-AT 
* #V00765-0 ^designation[0].value = "M2-PK (Pyruvatkinase) /Stuhl" 
* #V00765-0 ^property[0].code = #parent 
* #V00765-0 ^property[0].valueCode = #14760 
* #V00765-0 ^property[1].code = #status 
* #V00765-0 ^property[1].valueCode = #retired 
* #V00765-0 ^property[2].code = #Relationships 
* #V00765-0 ^property[2].valueString = "verwendbar bis 30.06.2023" 
* #V00765-0 ^property[3].code = #hints 
* #V00765-0 ^property[3].valueString = "DEPRECATED" 
* #1600 "Liquordiagnostik"
* #1600 ^property[0].code = #parent 
* #1600 ^property[0].valueCode = #1 
* #1600 ^property[1].code = #child 
* #1600 ^property[1].valueCode = #15770 
* #15770 "Liquordiagnostik"
* #15770 ^property[0].code = #parent 
* #15770 ^property[0].valueCode = #1600 
* #15770 ^property[1].code = #child 
* #15770 ^property[1].valueCode = #V00010 
* #15770 ^property[2].code = #child 
* #15770 ^property[2].valueCode = #V00136 
* #15770 ^property[3].code = #child 
* #15770 ^property[3].valueCode = #V00287 
* #15770 ^property[4].code = #child 
* #15770 ^property[4].valueCode = #V00303-0 
* #V00010 "IgG SW(Alb-Qu x0.43)"
* #V00010 ^definition = IgG SW(Alb-Qu x0.43)
* #V00010 ^designation[0].language = #de-AT 
* #V00010 ^designation[0].value = "IgG SW (Alb-Qu. X 0.43)" 
* #V00010 ^property[0].code = #parent 
* #V00010 ^property[0].valueCode = #15770 
* #V00136 "Proteinpanel Liquor"
* #V00136 ^definition = Proteinpanel Liquor
* #V00136 ^designation[0].language = #de-AT 
* #V00136 ^designation[0].value = "Proteinpanel Liquor" 
* #V00136 ^property[0].code = #parent 
* #V00136 ^property[0].valueCode = #15770 
* #V00287 "Komm.Liquor"
* #V00287 ^definition = Komm.Liquor
* #V00287 ^designation[0].language = #de-AT 
* #V00287 ^designation[0].value = "Kommentar Liquor" 
* #V00287 ^property[0].code = #parent 
* #V00287 ^property[0].valueCode = #15770 
* #V00303-0 "Fr. Kappa LK-Index"
* #V00303-0 ^definition = Fr. Kappa LK-Index
* #V00303-0 ^designation[0].language = #de-AT 
* #V00303-0 ^designation[0].value = "Freie Kappa Leichtketten-Index" 
* #V00303-0 ^property[0].code = #parent 
* #V00303-0 ^property[0].valueCode = #15770 
* #1800 "Allergiediagnostik"
* #1800 ^property[0].code = #parent 
* #1800 ^property[0].valueCode = #1 
* #1800 ^property[1].code = #child 
* #1800 ^property[1].valueCode = #12010 
* #1800 ^property[2].code = #child 
* #1800 ^property[2].valueCode = #12020 
* #1800 ^property[3].code = #child 
* #1800 ^property[3].valueCode = #12030 
* #1800 ^property[4].code = #child 
* #1800 ^property[4].valueCode = #12040 
* #1800 ^property[5].code = #child 
* #1800 ^property[5].valueCode = #12050 
* #1800 ^property[6].code = #child 
* #1800 ^property[6].valueCode = #12060 
* #1800 ^property[7].code = #child 
* #1800 ^property[7].valueCode = #12070 
* #1800 ^property[8].code = #child 
* #1800 ^property[8].valueCode = #12080 
* #1800 ^property[9].code = #child 
* #1800 ^property[9].valueCode = #12090 
* #12010 "Globalmarker"
* #12010 ^property[0].code = #parent 
* #12010 ^property[0].valueCode = #1800 
* #12020 "Inhalationsallergene IgE"
* #12020 ^property[0].code = #parent 
* #12020 ^property[0].valueCode = #1800 
* #12020 ^property[1].code = #child 
* #12020 ^property[1].valueCode = #V00015 
* #12020 ^property[2].code = #child 
* #12020 ^property[2].valueCode = #V00062 
* #12020 ^property[3].code = #child 
* #12020 ^property[3].valueCode = #V00149 
* #12020 ^property[4].code = #child 
* #12020 ^property[4].valueCode = #V00219 
* #12020 ^property[5].code = #child 
* #12020 ^property[5].valueCode = #V00224 
* #12020 ^property[6].code = #child 
* #12020 ^property[6].valueCode = #V00437-6 
* #12020 ^property[7].code = #child 
* #12020 ^property[7].valueCode = #V00440-0 
* #12020 ^property[8].code = #child 
* #12020 ^property[8].valueCode = #V00441-8 
* #12020 ^property[9].code = #child 
* #12020 ^property[9].valueCode = #V00471-5 
* #12020 ^property[10].code = #child 
* #12020 ^property[10].valueCode = #V00474-9 
* #12020 ^property[11].code = #child 
* #12020 ^property[11].valueCode = #V00475-6 
* #12020 ^property[12].code = #child 
* #12020 ^property[12].valueCode = #V00476-4 
* #12020 ^property[13].code = #child 
* #12020 ^property[13].valueCode = #V00477-2 
* #12020 ^property[14].code = #child 
* #12020 ^property[14].valueCode = #V00478-0 
* #12020 ^property[15].code = #child 
* #12020 ^property[15].valueCode = #V00481-4 
* #12020 ^property[16].code = #child 
* #12020 ^property[16].valueCode = #V00482-2 
* #12020 ^property[17].code = #child 
* #12020 ^property[17].valueCode = #V00483-0 
* #12020 ^property[18].code = #child 
* #12020 ^property[18].valueCode = #V00491-3 
* #12020 ^property[19].code = #child 
* #12020 ^property[19].valueCode = #V00492-1 
* #12020 ^property[20].code = #child 
* #12020 ^property[20].valueCode = #V00493-9 
* #12020 ^property[21].code = #child 
* #12020 ^property[21].valueCode = #V00511-8 
* #12020 ^property[22].code = #child 
* #12020 ^property[22].valueCode = #V00512-6 
* #12020 ^property[23].code = #child 
* #12020 ^property[23].valueCode = #V00513-4 
* #12020 ^property[24].code = #child 
* #12020 ^property[24].valueCode = #V00514-2 
* #12020 ^property[25].code = #child 
* #12020 ^property[25].valueCode = #V00515-9 
* #12020 ^property[26].code = #child 
* #12020 ^property[26].valueCode = #V00516-7 
* #12020 ^property[27].code = #child 
* #12020 ^property[27].valueCode = #V00672-8 
* #V00015 "IgE Inhalat.scr ip8"
* #V00015 ^definition = Inhalat.scr ip8 IgE
* #V00015 ^designation[0].language = #de-AT 
* #V00015 ^designation[0].value = "Inhalationsscreen ip8 IgE" 
* #V00015 ^property[0].code = #parent 
* #V00015 ^property[0].valueCode = #12020 
* #V00062 "IgE t25 Europäische Esche RAST"
* #V00062 ^definition = Europ.Esche IGE R
* #V00062 ^designation[0].language = #de-AT 
* #V00062 ^designation[0].value = "Europäische Esche IGE Rast Klasse" 
* #V00062 ^property[0].code = #parent 
* #V00062 ^property[0].valueCode = #12020 
* #V00149 "IgE Vorratsm.Eurm2"
* #V00149 ^definition = Vorratsm.Eurm2 IgE
* #V00149 ^designation[0].language = #de-AT 
* #V00149 ^designation[0].value = "Vorratsmilbe Eurm2 IgE" 
* #V00149 ^property[0].code = #parent 
* #V00149 ^property[0].valueCode = #12020 
* #V00219 "IgE Lieschgras Phlp7"
* #V00219 ^definition = Lieschgras Phlp7 IgE
* #V00219 ^designation[0].language = #de-AT 
* #V00219 ^designation[0].value = "Lieschgras Phl p 7  IgE Qn" 
* #V00219 ^property[0].code = #parent 
* #V00219 ^property[0].valueCode = #12020 
* #V00224 "IgE Lieschgras Phl p12"
* #V00224 ^definition = Lieschgr.Phlp12 IgE
* #V00224 ^designation[0].language = #de-AT 
* #V00224 ^designation[0].value = "Lieschgras Phl p 12  IgE Qn" 
* #V00224 ^property[0].code = #parent 
* #V00224 ^property[0].valueCode = #12020 
* #V00437-6 "IgE mx4 Aspergillusmix qualitativ RAST"
* #V00437-6 ^definition = IgE mx4 Ag.-Mix-R
* #V00437-6 ^designation[0].language = #de-AT 
* #V00437-6 ^designation[0].value = "Aspergillusmix RAST" 
* #V00437-6 ^property[0].code = #parent 
* #V00437-6 ^property[0].valueCode = #12020 
* #V00440-0 "Inhalatives Scrn. Qn"
* #V00440-0 ^definition = Inhalatives Scrn. Qn
* #V00440-0 ^designation[0].language = #de-AT 
* #V00440-0 ^designation[0].value = "Inhalatives Screening quant" 
* #V00440-0 ^property[0].code = #parent 
* #V00440-0 ^property[0].valueCode = #12020 
* #V00441-8 "IgE mx2 Schimmelpilze RAST"
* #V00441-8 ^definition = IgE mx2 Ag.-Mix-R
* #V00441-8 ^designation[0].language = #de-AT 
* #V00441-8 ^designation[0].value = "Schimmelpilzmix RAST" 
* #V00441-8 ^property[0].code = #parent 
* #V00441-8 ^property[0].valueCode = #12020 
* #V00441-8 ^property[1].code = #hints 
* #V00441-8 ^property[1].valueString = "(Penicillium notatum, Cladosporium herbarum, Aspergillus fumigatus, Candida albicans, Alternaria alternata, Helminthosporium halodes)" 
* #V00471-5 "IgE g214 Lieschgraskomponenten RAST"
* #V00471-5 ^definition = Lieschgraskomp. R
* #V00471-5 ^designation[0].language = #de-AT 
* #V00471-5 ^designation[0].value = "Lieschgraskomponenten (rPhl p7, rPhl p12) RAST" 
* #V00471-5 ^property[0].code = #parent 
* #V00471-5 ^property[0].valueCode = #12020 
* #V00471-5 ^property[1].code = #status 
* #V00471-5 ^property[1].valueCode = #retired 
* #V00471-5 ^property[2].code = #Relationships 
* #V00471-5 ^property[2].valueString = "verwendbar bis 31.12.2021" 
* #V00471-5 ^property[3].code = #hints 
* #V00471-5 ^property[3].valueString = "DEPRECATED, (rPhlp7,rPhlp12)" 
* #V00474-9 "IgE t221E Birkenkomponenten RAST"
* #V00474-9 ^definition = Birkenkomponenten R
* #V00474-9 ^designation[0].language = #de-AT 
* #V00474-9 ^designation[0].value = "Birkenkomponenten (Bet v2, Bet v4) RAST" 
* #V00474-9 ^property[0].code = #parent 
* #V00474-9 ^property[0].valueCode = #12020 
* #V00474-9 ^property[1].code = #status 
* #V00474-9 ^property[1].valueCode = #retired 
* #V00474-9 ^property[2].code = #Relationships 
* #V00474-9 ^property[2].valueString = "verwendbar bis 31.12.2021" 
* #V00474-9 ^property[3].code = #hints 
* #V00474-9 ^property[3].valueString = "DEPRECATED, (rBet v2, rBet v4)" 
* #V00475-6 "IgE t224 Olivenkomponente (nOle e1) RAST"
* #V00475-6 ^definition = Olivenb.P.Ole1 IgE R
* #V00475-6 ^designation[0].language = #de-AT 
* #V00475-6 ^designation[0].value = "Olivenbaum nOle e1 IgE RAST" 
* #V00475-6 ^property[0].code = #parent 
* #V00475-6 ^property[0].valueCode = #12020 
* #V00475-6 ^property[1].code = #status 
* #V00475-6 ^property[1].valueCode = #retired 
* #V00475-6 ^property[2].code = #Relationships 
* #V00475-6 ^property[2].valueString = "verwendbar bis 31.12.2021" 
* #V00475-6 ^property[3].code = #hints 
* #V00475-6 ^property[3].valueString = "DEPRECATED" 
* #V00476-4 "IgE w211 Glaskraukomponente, LTP (rPar j2) RAST"
* #V00476-4 ^definition = Glaskraut Parj2 IgER
* #V00476-4 ^designation[0].language = #de-AT 
* #V00476-4 ^designation[0].value = "Ausgebreitetes Glaskraut rPar j 2  IgE RAST" 
* #V00476-4 ^property[0].code = #parent 
* #V00476-4 ^property[0].valueCode = #12020 
* #V00477-2 "IgE w230 Ambrosienkomponente (nAmb a1) RAST"
* #V00477-2 ^definition = Traubenkrt (B) IgE R
* #V00477-2 ^designation[0].language = #de-AT 
* #V00477-2 ^designation[0].value = "Beifußblättriges Traubenkraut nAmb a1 IgE RAST" 
* #V00477-2 ^property[0].code = #parent 
* #V00477-2 ^property[0].valueCode = #12020 
* #V00477-2 ^property[1].code = #status 
* #V00477-2 ^property[1].valueCode = #retired 
* #V00477-2 ^property[2].code = #Relationships 
* #V00477-2 ^property[2].valueString = "verwendbar bis 31.12.2021" 
* #V00477-2 ^property[3].code = #hints 
* #V00477-2 ^property[3].valueString = "DEPRECATED" 
* #V00478-0 "IgE w231 nArt v1, Beifuß, Majorallergen RAST"
* #V00478-0 ^definition = Gem.Beifuß rv1 IgE R
* #V00478-0 ^designation[0].language = #de-AT 
* #V00478-0 ^designation[0].value = "Gemeiner Beifuß nArt v1  IgE RAST" 
* #V00478-0 ^property[0].code = #parent 
* #V00478-0 ^property[0].valueCode = #12020 
* #V00478-0 ^property[1].code = #status 
* #V00478-0 ^property[1].valueCode = #retired 
* #V00478-0 ^property[2].code = #Relationships 
* #V00478-0 ^property[2].valueString = "verwendbar bis 31.12.2021" 
* #V00478-0 ^property[3].code = #hints 
* #V00478-0 ^property[3].valueString = "DEPRECATED" 
* #V00481-4 "IgE d202 Milbenkomponente (nDer p1) RAST"
* #V00481-4 ^definition = Hausstaubm.Dp1 IgE R
* #V00481-4 ^designation[0].language = #de-AT 
* #V00481-4 ^designation[0].value = "Hausstaubmilbe nDer p 1 IgE RAST" 
* #V00481-4 ^property[0].code = #parent 
* #V00481-4 ^property[0].valueCode = #12020 
* #V00481-4 ^property[1].code = #status 
* #V00481-4 ^property[1].valueCode = #retired 
* #V00481-4 ^property[2].code = #Relationships 
* #V00481-4 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00481-4 ^property[3].code = #hints 
* #V00481-4 ^property[3].valueString = "DEPRECATED" 
* #V00482-2 "IgE d203 Milbenkomponente (rDer p2) RAST"
* #V00482-2 ^definition = Hausstaubm.Dp2 IgE R
* #V00482-2 ^designation[0].language = #de-AT 
* #V00482-2 ^designation[0].value = "Hausstaubmilbe rDer p 2  IgE RAST" 
* #V00482-2 ^property[0].code = #parent 
* #V00482-2 ^property[0].valueCode = #12020 
* #V00482-2 ^property[1].code = #status 
* #V00482-2 ^property[1].valueCode = #retired 
* #V00482-2 ^property[2].code = #Relationships 
* #V00482-2 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00482-2 ^property[3].code = #hints 
* #V00482-2 ^property[3].valueString = "DEPRECATED" 
* #V00483-0 "IgE d205 Milbenkomponente, Tropomyosin RAST"
* #V00483-0 ^definition = Hausstaubm.Dp10 IgER
* #V00483-0 ^designation[0].language = #de-AT 
* #V00483-0 ^designation[0].value = "Hausstaubmilbe Der10 IgE RAST" 
* #V00483-0 ^property[0].code = #parent 
* #V00483-0 ^property[0].valueCode = #12020 
* #V00483-0 ^property[1].code = #status 
* #V00483-0 ^property[1].valueCode = #retired 
* #V00483-0 ^property[2].code = #Relationships 
* #V00483-0 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00483-0 ^property[3].code = #hints 
* #V00483-0 ^property[3].valueString = "DEPRECATED, (rDer p10)" 
* #V00491-3 "IgE t225 Birkenkomponente (rBet v6)"
* #V00491-3 ^definition = IgE t225
* #V00491-3 ^designation[0].language = #de-AT 
* #V00491-3 ^designation[0].value = "IgE t225 Birkenkomponente (rBet v6)" 
* #V00491-3 ^property[0].code = #parent 
* #V00491-3 ^property[0].valueCode = #12020 
* #V00491-3 ^property[1].code = #status 
* #V00491-3 ^property[1].valueCode = #retired 
* #V00491-3 ^property[2].code = #Relationships 
* #V00491-3 ^property[2].valueString = "verwendbar bis 30.06.2018" 
* #V00491-3 ^property[3].code = #hints 
* #V00491-3 ^property[3].valueString = "DEPRECATED" 
* #V00492-1 "IgE f435 Apflelkomponente, LTP (rMal d3)"
* #V00492-1 ^definition = IgE f435
* #V00492-1 ^designation[0].language = #de-AT 
* #V00492-1 ^designation[0].value = "IgE f435 Apflelkomponente, LTP (rMal d3)" 
* #V00492-1 ^property[0].code = #parent 
* #V00492-1 ^property[0].valueCode = #12020 
* #V00492-1 ^property[1].code = #status 
* #V00492-1 ^property[1].valueCode = #retired 
* #V00492-1 ^property[2].code = #Relationships 
* #V00492-1 ^property[2].valueString = "verwendbar bis 30.06.2018" 
* #V00492-1 ^property[3].code = #hints 
* #V00492-1 ^property[3].valueString = "DEPRECATED" 
* #V00493-9 "IgE f439 Haselnusskomponente"
* #V00493-9 ^definition = IgE f439
* #V00493-9 ^designation[0].language = #de-AT 
* #V00493-9 ^designation[0].value = "IgE f439 Haselnusskomponente" 
* #V00493-9 ^property[0].code = #parent 
* #V00493-9 ^property[0].valueCode = #12020 
* #V00493-9 ^property[1].code = #status 
* #V00493-9 ^property[1].valueCode = #retired 
* #V00493-9 ^property[2].code = #Relationships 
* #V00493-9 ^property[2].valueString = "verwendbar bis 30.06.2018" 
* #V00493-9 ^property[3].code = #hints 
* #V00493-9 ^property[3].valueString = "DEPRECATED, 2S Albumin(rCor a14)" 
* #V00511-8 "IgE f428 Haselnusskomponente RAST"
* #V00511-8 ^definition = IgE f428 R
* #V00511-8 ^designation[0].language = #de-AT 
* #V00511-8 ^designation[0].value = "IgE f428 Haselnusskomponente RAST" 
* #V00511-8 ^property[0].code = #parent 
* #V00511-8 ^property[0].valueCode = #12020 
* #V00511-8 ^property[1].code = #hints 
* #V00511-8 ^property[1].valueString = "PR 10-Protein(rCor a1)" 
* #V00512-6 "IgE k215 Latexkomponente (rHev b1) RAST"
* #V00512-6 ^definition = Latex rHevb1 IgE R
* #V00512-6 ^designation[0].language = #de-AT 
* #V00512-6 ^designation[0].value = "Latex rHev b 1  IgE RAST" 
* #V00512-6 ^property[0].code = #parent 
* #V00512-6 ^property[0].valueCode = #12020 
* #V00512-6 ^property[1].code = #status 
* #V00512-6 ^property[1].valueCode = #retired 
* #V00512-6 ^property[2].code = #Relationships 
* #V00512-6 ^property[2].valueString = "verwendbar bis 31.12.2021" 
* #V00512-6 ^property[3].code = #hints 
* #V00512-6 ^property[3].valueString = "DEPRECATED" 
* #V00513-4 "IgE k217 Latexkomponente (rHev b3) RAST"
* #V00513-4 ^definition = Latex rHevb3 IgE R
* #V00513-4 ^designation[0].language = #de-AT 
* #V00513-4 ^designation[0].value = "Latex rHev b 3  IgE RAST" 
* #V00513-4 ^property[0].code = #parent 
* #V00513-4 ^property[0].valueCode = #12020 
* #V00513-4 ^property[1].code = #status 
* #V00513-4 ^property[1].valueCode = #retired 
* #V00513-4 ^property[2].code = #Relationships 
* #V00513-4 ^property[2].valueString = "verwendbar bis 31.12.2021" 
* #V00513-4 ^property[3].code = #hints 
* #V00513-4 ^property[3].valueString = "DEPRECATED" 
* #V00514-2 "IgE k219 Latexkomponente (rHev b6.01) RAST"
* #V00514-2 ^definition = Latex rHevb6 IgE R
* #V00514-2 ^designation[0].language = #de-AT 
* #V00514-2 ^designation[0].value = "Latex rHev b 6  IgE RAST" 
* #V00514-2 ^property[0].code = #parent 
* #V00514-2 ^property[0].valueCode = #12020 
* #V00515-9 "IgE k220 Latexkomponente (rHev b6.02) RAST"
* #V00515-9 ^definition = IgE k220 R
* #V00515-9 ^designation[0].language = #de-AT 
* #V00515-9 ^designation[0].value = "IgE k220 Latexkomponente (rHev b6.02) RAST" 
* #V00515-9 ^property[0].code = #parent 
* #V00515-9 ^property[0].valueCode = #12020 
* #V00515-9 ^property[1].code = #status 
* #V00515-9 ^property[1].valueCode = #retired 
* #V00515-9 ^property[2].code = #Relationships 
* #V00515-9 ^property[2].valueString = "verwendbar bis 31.12.2021" 
* #V00515-9 ^property[3].code = #hints 
* #V00515-9 ^property[3].valueString = "DEPRECATED" 
* #V00516-7 "IgE f431 Sojabohnenkomponente RAST"
* #V00516-7 ^definition = Soja nGlym5 IgE R
* #V00516-7 ^designation[0].language = #de-AT 
* #V00516-7 ^designation[0].value = "Sojabohne nGly m 5  IgE RAST" 
* #V00516-7 ^property[0].code = #parent 
* #V00516-7 ^property[0].valueCode = #12020 
* #V00516-7 ^property[1].code = #status 
* #V00516-7 ^property[1].valueCode = #retired 
* #V00516-7 ^property[2].code = #Relationships 
* #V00516-7 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00516-7 ^property[3].code = #hints 
* #V00516-7 ^property[3].valueString = "DEPRECATED, 7S Globulin (Gly m5)" 
* #V00672-8 "IgE d202 Milbenkomponente (rDer p1)"
* #V00672-8 ^definition = Hausstaubm.Derp1 IgE
* #V00672-8 ^designation[0].language = #de-AT 
* #V00672-8 ^designation[0].value = "Hausstaubmilbe rDer p 1  IgE Qn" 
* #V00672-8 ^property[0].code = #parent 
* #V00672-8 ^property[0].valueCode = #12020 
* #V00672-8 ^property[1].code = #status 
* #V00672-8 ^property[1].valueCode = #retired 
* #V00672-8 ^property[2].code = #Relationships 
* #V00672-8 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00672-8 ^property[3].code = #hints 
* #V00672-8 ^property[3].valueString = "DEPRECATED" 
* #12030 "Nahrungsmittelallergene IgE"
* #12030 ^property[0].code = #parent 
* #12030 ^property[0].valueCode = #1800 
* #12030 ^property[1].code = #child 
* #12030 ^property[1].valueCode = #V00158 
* #12030 ^property[2].code = #child 
* #12030 ^property[2].valueCode = #V00159 
* #12030 ^property[3].code = #child 
* #12030 ^property[3].valueCode = #V00161 
* #12030 ^property[4].code = #child 
* #12030 ^property[4].valueCode = #V00226 
* #12030 ^property[5].code = #child 
* #12030 ^property[5].valueCode = #V00232 
* #12030 ^property[6].code = #child 
* #12030 ^property[6].valueCode = #V00236 
* #12030 ^property[7].code = #child 
* #12030 ^property[7].valueCode = #V00269 
* #12030 ^property[8].code = #child 
* #12030 ^property[8].valueCode = #V00469-9 
* #12030 ^property[9].code = #child 
* #12030 ^property[9].valueCode = #V00479-8 
* #12030 ^property[10].code = #child 
* #12030 ^property[10].valueCode = #V00480-6 
* #12030 ^property[11].code = #child 
* #12030 ^property[11].valueCode = #V00494-7 
* #12030 ^property[12].code = #child 
* #12030 ^property[12].valueCode = #V00495-4 
* #12030 ^property[13].code = #child 
* #12030 ^property[13].valueCode = #V00510-0 
* #12030 ^property[14].code = #child 
* #12030 ^property[14].valueCode = #V00517-5 
* #12030 ^property[15].code = #child 
* #12030 ^property[15].valueCode = #V00518-3 
* #12030 ^property[16].code = #child 
* #12030 ^property[16].valueCode = #V00519-1 
* #12030 ^property[17].code = #child 
* #12030 ^property[17].valueCode = #V00520-9 
* #12030 ^property[18].code = #child 
* #12030 ^property[18].valueCode = #V00521-7 
* #12030 ^property[19].code = #child 
* #12030 ^property[19].valueCode = #V00522-5 
* #12030 ^property[20].code = #child 
* #12030 ^property[20].valueCode = #V00523-3 
* #V00158 "IgE Shrimp nPeni1"
* #V00158 ^definition = Shrimp nPeni1
* #V00158 ^designation[0].language = #de-AT 
* #V00158 ^designation[0].value = "Shrimp nPen i 1 IgE Qn" 
* #V00158 ^property[0].code = #parent 
* #V00158 ^property[0].valueCode = #12030 
* #V00159 "IgE Sesam nSesi1"
* #V00159 ^definition = Sesam nSesi1 IgE
* #V00159 ^designation[0].language = #de-AT 
* #V00159 ^designation[0].value = "Sesam nSes i 1  IgE Qn" 
* #V00159 ^property[0].code = #parent 
* #V00159 ^property[0].valueCode = #12030 
* #V00161 "IgE Weizen Gliadin"
* #V00161 ^definition = Weizen Gliadin IgE
* #V00161 ^designation[0].language = #de-AT 
* #V00161 ^designation[0].value = "Weizen Gliadin  IgE Qn" 
* #V00161 ^property[0].code = #parent 
* #V00161 ^property[0].valueCode = #12030 
* #V00226 "IgE Kuhmilch Bod5"
* #V00226 ^definition = Kuhmilch Bod5 IgE
* #V00226 ^designation[0].language = #de-AT 
* #V00226 ^designation[0].value = "Kuhmilch nBos d 5  IgE Qn" 
* #V00226 ^property[0].code = #parent 
* #V00226 ^property[0].valueCode = #12030 
* #V00232 "IgE Kuhmilch Bosd8"
* #V00232 ^definition = Kuhmilch Bosd8 IgE
* #V00232 ^designation[0].language = #de-AT 
* #V00232 ^designation[0].value = "Kuhmilch nBos d 8  IgE Qn" 
* #V00232 ^property[0].code = #parent 
* #V00232 ^property[0].valueCode = #12030 
* #V00236 "IgE Kuhmilch Bod4"
* #V00236 ^definition = Kuhmilch Bod4 IgE
* #V00236 ^designation[0].language = #de-AT 
* #V00236 ^designation[0].value = "Kuhmilch nBos d 4  IgE Qn" 
* #V00236 ^property[0].code = #parent 
* #V00236 ^property[0].valueCode = #12030 
* #V00269 "IgE f447 Erdnusskomponente"
* #V00269 ^definition = IgE f447
* #V00269 ^designation[0].language = #de-AT 
* #V00269 ^designation[0].value = "IgE f447 Erdnusskomponente (rAra h6)" 
* #V00269 ^property[0].code = #parent 
* #V00269 ^property[0].valueCode = #12030 
* #V00269 ^property[1].code = #status 
* #V00269 ^property[1].valueCode = #retired 
* #V00269 ^property[2].code = #Relationships 
* #V00269 ^property[2].valueString = "verwendbar bis 30.06.2020" 
* #V00269 ^property[3].code = #hints 
* #V00269 ^property[3].valueString = "DEPRECATED, Speicherprotein (rAra h6)" 
* #V00469-9 "IgE f299 Maroni RAST"
* #V00469-9 ^definition = Esskastanie IgE R
* #V00469-9 ^designation[0].language = #de-AT 
* #V00469-9 ^designation[0].value = "Esskastanie (Maroni) IgE RAST" 
* #V00469-9 ^property[0].code = #parent 
* #V00469-9 ^property[0].valueCode = #12030 
* #V00479-8 "IgE f352 Erdnusskomponente RAST"
* #V00479-8 ^definition = IgE f352 R
* #V00479-8 ^designation[0].language = #de-AT 
* #V00479-8 ^designation[0].value = "IgE f352 Erdnusskomponente (rAra h8) RAST" 
* #V00479-8 ^property[0].code = #parent 
* #V00479-8 ^property[0].valueCode = #12030 
* #V00479-8 ^property[1].code = #status 
* #V00479-8 ^property[1].valueCode = #retired 
* #V00479-8 ^property[2].code = #Relationships 
* #V00479-8 ^property[2].valueString = "verwendbar bis 30.06.2020" 
* #V00479-8 ^property[3].code = #hints 
* #V00479-8 ^property[3].valueString = "DEPRECATED, PR 10-Protein (rAra h8)" 
* #V00480-6 "IgE Erdnuss Arah2 RAST"
* #V00480-6 ^definition = Erdnuss nArah2 IgE R
* #V00480-6 ^designation[0].language = #de-AT 
* #V00480-6 ^designation[0].value = "Erdnuss nAra h 2  IgE RAST" 
* #V00480-6 ^property[0].code = #parent 
* #V00480-6 ^property[0].valueCode = #12030 
* #V00494-7 "IgE f319 Rote Rübe (Beta vulgaris)"
* #V00494-7 ^definition = IgE f319
* #V00494-7 ^designation[0].language = #de-AT 
* #V00494-7 ^designation[0].value = "IgE f319 Rote Rübe (Beta vulgaris)" 
* #V00494-7 ^property[0].code = #parent 
* #V00494-7 ^property[0].valueCode = #12030 
* #V00494-7 ^property[1].code = #status 
* #V00494-7 ^property[1].valueCode = #retired 
* #V00494-7 ^property[2].code = #Relationships 
* #V00494-7 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00494-7 ^property[3].code = #hints 
* #V00494-7 ^property[3].valueString = "DEPRECATED" 
* #V00495-4 "IgE f443 Cashewnusskomponente"
* #V00495-4 ^definition = IgE f443
* #V00495-4 ^designation[0].language = #de-AT 
* #V00495-4 ^designation[0].value = "IgE f443 Cashewnusskomponente," 
* #V00495-4 ^property[0].code = #parent 
* #V00495-4 ^property[0].valueCode = #12030 
* #V00495-4 ^property[1].code = #status 
* #V00495-4 ^property[1].valueCode = #retired 
* #V00495-4 ^property[2].code = #Relationships 
* #V00495-4 ^property[2].valueString = "verwendbar bis 30.06.2018" 
* #V00495-4 ^property[3].code = #hints 
* #V00495-4 ^property[3].valueString = "DEPRECATED, 2S Albumin(rAna o3)" 
* #V00510-0 "IgE f427 Erdnusskomponente RAST"
* #V00510-0 ^definition = IgE f427 R
* #V00510-0 ^designation[0].language = #de-AT 
* #V00510-0 ^designation[0].value = "IgE f427 Erdnusskomponente (rAra h9) RAST" 
* #V00510-0 ^property[0].code = #parent 
* #V00510-0 ^property[0].valueCode = #12030 
* #V00510-0 ^property[1].code = #status 
* #V00510-0 ^property[1].valueCode = #retired 
* #V00510-0 ^property[2].code = #Relationships 
* #V00510-0 ^property[2].valueString = "verwendbar bis 30.06.2020" 
* #V00510-0 ^property[3].code = #hints 
* #V00510-0 ^property[3].valueString = "DEPRECATED, LTP (rAra h9)" 
* #V00517-5 "IgE f353 Sojabohnenkomponente RAST"
* #V00517-5 ^definition = Soja Glym4  IgE R
* #V00517-5 ^designation[0].language = #de-AT 
* #V00517-5 ^designation[0].value = "Sojabohne rGly m 4  IgE RAST" 
* #V00517-5 ^property[0].code = #parent 
* #V00517-5 ^property[0].valueCode = #12030 
* #V00517-5 ^property[1].code = #status 
* #V00517-5 ^property[1].valueCode = #retired 
* #V00517-5 ^property[2].code = #Relationships 
* #V00517-5 ^property[2].valueString = "verwendbar bis 31.12.2021" 
* #V00517-5 ^property[3].code = #hints 
* #V00517-5 ^property[3].valueString = "DEPRECATED, PR 10-Protein (rGly m4)" 
* #V00518-3 "IgE f419 Pfirsichkomponente RAST"
* #V00518-3 ^definition = Pfirsich Prup1 IgE R
* #V00518-3 ^designation[0].language = #de-AT 
* #V00518-3 ^designation[0].value = "Pfirsich rPru p 1  IgE RAST" 
* #V00518-3 ^property[0].code = #parent 
* #V00518-3 ^property[0].valueCode = #12030 
* #V00518-3 ^property[1].code = #hints 
* #V00518-3 ^property[1].valueString = ", PR 10-Protein (rPru p1)" 
* #V00519-1 "IgE f420 Pfirsichkomponente, LTP (rPru p3) RAST"
* #V00519-1 ^definition = Pfirsich Prup3 IgE R
* #V00519-1 ^designation[0].language = #de-AT 
* #V00519-1 ^designation[0].value = "Pfirsich rPru p 3  IgE RAST" 
* #V00519-1 ^property[0].code = #parent 
* #V00519-1 ^property[0].valueCode = #12030 
* #V00519-1 ^property[1].code = #status 
* #V00519-1 ^property[1].valueCode = #retired 
* #V00519-1 ^property[2].code = #Relationships 
* #V00519-1 ^property[2].valueString = "verwendbar bis 31.12.2021" 
* #V00519-1 ^property[3].code = #hints 
* #V00519-1 ^property[3].valueString = "DEPRECATED" 
* #V00520-9 "IgE f422 Erdnusskomponente RAST"
* #V00520-9 ^definition = IgE f422 R
* #V00520-9 ^designation[0].language = #de-AT 
* #V00520-9 ^designation[0].value = "IgE f422 Erdnusskomponente (rAra h1) RAST" 
* #V00520-9 ^property[0].code = #parent 
* #V00520-9 ^property[0].valueCode = #12030 
* #V00520-9 ^property[1].code = #status 
* #V00520-9 ^property[1].valueCode = #retired 
* #V00520-9 ^property[2].code = #Relationships 
* #V00520-9 ^property[2].valueString = "verwendbar bis 30.06.2020" 
* #V00520-9 ^property[3].code = #hints 
* #V00520-9 ^property[3].valueString = "DEPRECATED, Speicherprotein(rAra h1)" 
* #V00521-7 "IgE f423 Erdnusskomponente RAST"
* #V00521-7 ^definition = IgE f423 R
* #V00521-7 ^designation[0].language = #de-AT 
* #V00521-7 ^designation[0].value = "IgE f423 Erdnusskomponente (rAra h2) RAST" 
* #V00521-7 ^property[0].code = #parent 
* #V00521-7 ^property[0].valueCode = #12030 
* #V00521-7 ^property[1].code = #status 
* #V00521-7 ^property[1].valueCode = #retired 
* #V00521-7 ^property[2].code = #Relationships 
* #V00521-7 ^property[2].valueString = "verwendbar bis 30.06.2020" 
* #V00521-7 ^property[3].code = #hints 
* #V00521-7 ^property[3].valueString = "DEPRECATED, Speicherprotein (rAra h2)" 
* #V00522-5 "IgE f424 Erdnusskomponente RAST"
* #V00522-5 ^definition = IgE f424 R
* #V00522-5 ^designation[0].language = #de-AT 
* #V00522-5 ^designation[0].value = "IgE f424 Erdnusskomponente (rAra h3) RAST" 
* #V00522-5 ^property[0].code = #parent 
* #V00522-5 ^property[0].valueCode = #12030 
* #V00522-5 ^property[1].code = #status 
* #V00522-5 ^property[1].valueCode = #retired 
* #V00522-5 ^property[2].code = #Relationships 
* #V00522-5 ^property[2].valueString = "verwendbar bis 30.06.2020" 
* #V00522-5 ^property[3].code = #hints 
* #V00522-5 ^property[3].valueString = "DEPRECATED, Speicherprotein (rAra h3)" 
* #V00523-3 "IgE f425 Haselnusskomponente, LTP (rCor a8) RAST"
* #V00523-3 ^definition = Haseln. Cora8 IgE R
* #V00523-3 ^designation[0].language = #de-AT 
* #V00523-3 ^designation[0].value = "Haselnuss rCor a 8  IgE Qn" 
* #V00523-3 ^property[0].code = #parent 
* #V00523-3 ^property[0].valueCode = #12030 
* #12040 "Umwelt- und Berufsallergene IgE"
* #12040 ^property[0].code = #parent 
* #12040 ^property[0].valueCode = #1800 
* #12040 ^property[1].code = #child 
* #12040 ^property[1].valueCode = #V00438-4 
* #12040 ^property[2].code = #child 
* #12040 ^property[2].valueCode = #V00496-2 
* #V00438-4 "IgE PAX5 Chemikalien RAST"
* #V00438-4 ^definition = IgE PAX5 Ag.-Mix-R
* #V00438-4 ^designation[0].language = #de-AT 
* #V00438-4 ^designation[0].value = "Chemikalienmix RAST" 
* #V00438-4 ^property[0].code = #parent 
* #V00438-4 ^property[0].valueCode = #12040 
* #V00438-4 ^property[1].code = #hints 
* #V00438-4 ^property[1].valueString = "(Isocyanat TDI/MDI/HDI, Phthalsäure Anhydrid)" 
* #V00496-2 "IgE Ro213 MBP Maltose bindendes Fusionsprotein"
* #V00496-2 ^definition = IgE Ro213 MBP
* #V00496-2 ^designation[0].language = #de-AT 
* #V00496-2 ^designation[0].value = "IgE Ro213 MBP Maltose bindendes Fusionsprotein" 
* #V00496-2 ^property[0].code = #parent 
* #V00496-2 ^property[0].valueCode = #12040 
* #12050 "Medikamente IgE"
* #12050 ^property[0].code = #parent 
* #12050 ^property[0].valueCode = #1800 
* #12050 ^property[1].code = #child 
* #12050 ^property[1].valueCode = #V00143 
* #12050 ^property[2].code = #child 
* #12050 ^property[2].valueCode = #V00497-0 
* #V00143 "IgE C8 Chlorhexidin RAST"
* #V00143 ^definition = Chlorhexidin IGE R
* #V00143 ^designation[0].language = #de-AT 
* #V00143 ^designation[0].value = "Chlorhexidin IGE RAST" 
* #V00143 ^property[0].code = #parent 
* #V00143 ^property[0].valueCode = #12050 
* #V00497-0 "IgE c260 Morphin (Muskelrelaxans)"
* #V00497-0 ^definition = IgE c260 Morphin
* #V00497-0 ^designation[0].language = #de-AT 
* #V00497-0 ^designation[0].value = "IgE c260 Morphin (Muskelrelaxans)" 
* #V00497-0 ^property[0].code = #parent 
* #V00497-0 ^property[0].valueCode = #12050 
* #V00497-0 ^property[1].code = #hints 
* #V00497-0 ^property[1].valueString = "Quaternäres Ammonium" 
* #12060 "Insekten und Insektengifte IgE"
* #12060 ^property[0].code = #parent 
* #12060 ^property[0].valueCode = #1800 
* #12060 ^property[1].code = #child 
* #12060 ^property[1].valueCode = #V00098 
* #12060 ^property[2].code = #child 
* #12060 ^property[2].valueCode = #V00142 
* #12060 ^property[3].code = #child 
* #12060 ^property[3].valueCode = #V00153 
* #12060 ^property[4].code = #child 
* #12060 ^property[4].valueCode = #V00231 
* #12060 ^property[5].code = #child 
* #12060 ^property[5].valueCode = #V00498-8 
* #12060 ^property[6].code = #child 
* #12060 ^property[6].valueCode = #V00524-1 
* #12060 ^property[7].code = #child 
* #12060 ^property[7].valueCode = #V00573-8 
* #12060 ^property[8].code = #child 
* #12060 ^property[8].valueCode = #V00574-6 
* #12060 ^property[9].code = #child 
* #12060 ^property[9].valueCode = #V00608-2 
* #12060 ^property[10].code = #child 
* #12060 ^property[10].valueCode = #V00679-3 
* #V00098 "IgE i210 Feldwespengiftkomponente (rPol d5) RAST"
* #V00098 ^definition = Feldwes.rPold5 IgE R
* #V00098 ^designation[0].language = #de-AT 
* #V00098 ^designation[0].value = "Feldwespe (Polistes spp) rek. (rPol d) 5 IgE Rast" 
* #V00098 ^property[0].code = #parent 
* #V00098 ^property[0].valueCode = #12060 
* #V00142 "IgE i211 Wespengiftkomponente (rVes v1) RAST"
* #V00142 ^definition = Wespe rVesv1 IGE R
* #V00142 ^designation[0].language = #de-AT 
* #V00142 ^designation[0].value = "Gemeine Wespe rekombinant (rVes v) 1 IgE.RAST" 
* #V00142 ^property[0].code = #parent 
* #V00142 ^property[0].valueCode = #12060 
* #V00153 "IgE D.Küchensch.Blg4"
* #V00153 ^definition = D.Küchensch.Blg4 IgE
* #V00153 ^designation[0].language = #de-AT 
* #V00153 ^designation[0].value = "Deutsche Küchenschabe rBla g 4  IgE Qn" 
* #V00153 ^property[0].code = #parent 
* #V00153 ^property[0].valueCode = #12060 
* #V00231 "IgE Gem.Wespe Vesv5"
* #V00231 ^definition = Gem.Wespe Vesv5 IgE
* #V00231 ^designation[0].language = #de-AT 
* #V00231 ^designation[0].value = "Gemeine Wespe Vesv5 IgE Qn" 
* #V00231 ^property[0].code = #parent 
* #V00231 ^property[0].valueCode = #12060 
* #V00498-8 "IgE i212 Wespengiftkomponente (rVes v2)"
* #V00498-8 ^definition = IgE i212
* #V00498-8 ^designation[0].language = #de-AT 
* #V00498-8 ^designation[0].value = "IgE i212 Wespengiftkomponente (rVes v2)" 
* #V00498-8 ^property[0].code = #parent 
* #V00498-8 ^property[0].valueCode = #12060 
* #V00524-1 "IgE i217 Bienengiftkomponente Icarapin Variant 2"
* #V00524-1 ^definition = Biene rApi m10 IgE
* #V00524-1 ^designation[0].language = #de-AT 
* #V00524-1 ^designation[0].value = "Biene rApi m10 IgE" 
* #V00524-1 ^property[0].code = #parent 
* #V00524-1 ^property[0].valueCode = #12060 
* #V00524-1 ^property[1].code = #status 
* #V00524-1 ^property[1].valueCode = #retired 
* #V00524-1 ^property[2].code = #Relationships 
* #V00524-1 ^property[2].valueString = "verwendbar bis 30.06.2017" 
* #V00524-1 ^property[3].code = #hints 
* #V00524-1 ^property[3].valueString = "DEPRECATED, Carbohydrate-rich Protein (rApi m10)" 
* #V00573-8 "IgE i215 Bienengiftkomponente"
* #V00573-8 ^definition = Biene rApi m3 IgE
* #V00573-8 ^designation[0].language = #de-AT 
* #V00573-8 ^designation[0].value = "Biene rApi m3 IgE" 
* #V00573-8 ^property[0].code = #parent 
* #V00573-8 ^property[0].valueCode = #12060 
* #V00573-8 ^property[1].code = #hints 
* #V00573-8 ^property[1].valueString = ", Majorallergen saure Phospatase (rApi m3)" 
* #V00574-6 "IgE i215 Bienengiftkomponente RAST"
* #V00574-6 ^definition = Biene rApi m3 IgE R
* #V00574-6 ^designation[0].language = #de-AT 
* #V00574-6 ^designation[0].value = "Biene rApi m3 IgE R" 
* #V00574-6 ^property[0].code = #parent 
* #V00574-6 ^property[0].valueCode = #12060 
* #V00574-6 ^property[1].code = #hints 
* #V00574-6 ^property[1].valueString = ", Majorallergen saure Phospatase (rApi m3)" 
* #V00608-2 "IgE i216 Bienengiftkomponente Peptidylpeptidase IV"
* #V00608-2 ^definition = Biene i216 IgE
* #V00608-2 ^designation[0].language = #de-AT 
* #V00608-2 ^designation[0].value = "IgE i216 Bienengiftkomponente Peptidylpeptidase IV" 
* #V00608-2 ^property[0].code = #parent 
* #V00608-2 ^property[0].valueCode = #12060 
* #V00608-2 ^property[1].code = #hints 
* #V00608-2 ^property[1].valueString = ", (rApi m5)" 
* #V00679-3 "IgE i214 Bienengiftkomponente Hyaluronidase RAST"
* #V00679-3 ^definition = Biene rApi m2 IgE R
* #V00679-3 ^designation[0].language = #de-AT 
* #V00679-3 ^designation[0].value = "Biene rApi m2 IgE RAST" 
* #V00679-3 ^property[0].code = #parent 
* #V00679-3 ^property[0].valueCode = #12060 
* #V00679-3 ^property[1].code = #status 
* #V00679-3 ^property[1].valueCode = #retired 
* #V00679-3 ^property[2].code = #Relationships 
* #V00679-3 ^property[2].valueString = "verwendbar bis 31.12.2019" 
* #V00679-3 ^property[3].code = #hints 
* #V00679-3 ^property[3].valueString = "DEPRECATED, (rApi m2)" 
* #12070 "Sonstige IgE"
* #12070 ^property[0].code = #parent 
* #12070 ^property[0].valueCode = #1800 
* #12080 "Allergiechip IgE"
* #12080 ^property[0].code = #parent 
* #12080 ^property[0].valueCode = #1800 
* #12080 ^property[1].code = #child 
* #12080 ^property[1].valueCode = #V00270 
* #12080 ^property[2].code = #child 
* #12080 ^property[2].valueCode = #V00307-1 
* #12080 ^property[3].code = #child 
* #12080 ^property[3].valueCode = #V00308-9 
* #12080 ^property[4].code = #child 
* #12080 ^property[4].valueCode = #V00309-7 
* #12080 ^property[5].code = #child 
* #12080 ^property[5].valueCode = #V00310-5 
* #12080 ^property[6].code = #child 
* #12080 ^property[6].valueCode = #V00311-3 
* #12080 ^property[7].code = #child 
* #12080 ^property[7].valueCode = #V00312-1 
* #12080 ^property[8].code = #child 
* #12080 ^property[8].valueCode = #V00313-9 
* #12080 ^property[9].code = #child 
* #12080 ^property[9].valueCode = #V00314-7 
* #12080 ^property[10].code = #child 
* #12080 ^property[10].valueCode = #V00315-4 
* #12080 ^property[11].code = #child 
* #12080 ^property[11].valueCode = #V00316-2 
* #12080 ^property[12].code = #child 
* #12080 ^property[12].valueCode = #V00317-0 
* #12080 ^property[13].code = #child 
* #12080 ^property[13].valueCode = #V00318-8 
* #12080 ^property[14].code = #child 
* #12080 ^property[14].valueCode = #V00319-6 
* #12080 ^property[15].code = #child 
* #12080 ^property[15].valueCode = #V00320-4 
* #12080 ^property[16].code = #child 
* #12080 ^property[16].valueCode = #V00321-2 
* #12080 ^property[17].code = #child 
* #12080 ^property[17].valueCode = #V00322-0 
* #12080 ^property[18].code = #child 
* #12080 ^property[18].valueCode = #V00323-8 
* #12080 ^property[19].code = #child 
* #12080 ^property[19].valueCode = #V00324-6 
* #12080 ^property[20].code = #child 
* #12080 ^property[20].valueCode = #V00325-3 
* #12080 ^property[21].code = #child 
* #12080 ^property[21].valueCode = #V00326-1 
* #12080 ^property[22].code = #child 
* #12080 ^property[22].valueCode = #V00327-9 
* #12080 ^property[23].code = #child 
* #12080 ^property[23].valueCode = #V00328-7 
* #12080 ^property[24].code = #child 
* #12080 ^property[24].valueCode = #V00329-5 
* #12080 ^property[25].code = #child 
* #12080 ^property[25].valueCode = #V00330-3 
* #12080 ^property[26].code = #child 
* #12080 ^property[26].valueCode = #V00331-1 
* #12080 ^property[27].code = #child 
* #12080 ^property[27].valueCode = #V00332-9 
* #12080 ^property[28].code = #child 
* #12080 ^property[28].valueCode = #V00333-7 
* #12080 ^property[29].code = #child 
* #12080 ^property[29].valueCode = #V00334-5 
* #12080 ^property[30].code = #child 
* #12080 ^property[30].valueCode = #V00335-2 
* #12080 ^property[31].code = #child 
* #12080 ^property[31].valueCode = #V00336-0 
* #12080 ^property[32].code = #child 
* #12080 ^property[32].valueCode = #V00337-8 
* #12080 ^property[33].code = #child 
* #12080 ^property[33].valueCode = #V00338-6 
* #12080 ^property[34].code = #child 
* #12080 ^property[34].valueCode = #V00339-4 
* #12080 ^property[35].code = #child 
* #12080 ^property[35].valueCode = #V00340-2 
* #12080 ^property[36].code = #child 
* #12080 ^property[36].valueCode = #V00341-0 
* #12080 ^property[37].code = #child 
* #12080 ^property[37].valueCode = #V00342-8 
* #12080 ^property[38].code = #child 
* #12080 ^property[38].valueCode = #V00343-6 
* #12080 ^property[39].code = #child 
* #12080 ^property[39].valueCode = #V00344-4 
* #12080 ^property[40].code = #child 
* #12080 ^property[40].valueCode = #V00345-1 
* #12080 ^property[41].code = #child 
* #12080 ^property[41].valueCode = #V00346-9 
* #12080 ^property[42].code = #child 
* #12080 ^property[42].valueCode = #V00347-7 
* #12080 ^property[43].code = #child 
* #12080 ^property[43].valueCode = #V00348-5 
* #12080 ^property[44].code = #child 
* #12080 ^property[44].valueCode = #V00349-3 
* #12080 ^property[45].code = #child 
* #12080 ^property[45].valueCode = #V00350-1 
* #12080 ^property[46].code = #child 
* #12080 ^property[46].valueCode = #V00351-9 
* #12080 ^property[47].code = #child 
* #12080 ^property[47].valueCode = #V00352-7 
* #12080 ^property[48].code = #child 
* #12080 ^property[48].valueCode = #V00353-5 
* #12080 ^property[49].code = #child 
* #12080 ^property[49].valueCode = #V00354-3 
* #12080 ^property[50].code = #child 
* #12080 ^property[50].valueCode = #V00355-0 
* #12080 ^property[51].code = #child 
* #12080 ^property[51].valueCode = #V00356-8 
* #12080 ^property[52].code = #child 
* #12080 ^property[52].valueCode = #V00357-6 
* #12080 ^property[53].code = #child 
* #12080 ^property[53].valueCode = #V00358-4 
* #12080 ^property[54].code = #child 
* #12080 ^property[54].valueCode = #V00359-2 
* #12080 ^property[55].code = #child 
* #12080 ^property[55].valueCode = #V00360-0 
* #12080 ^property[56].code = #child 
* #12080 ^property[56].valueCode = #V00361-8 
* #12080 ^property[57].code = #child 
* #12080 ^property[57].valueCode = #V00362-6 
* #12080 ^property[58].code = #child 
* #12080 ^property[58].valueCode = #V00363-4 
* #12080 ^property[59].code = #child 
* #12080 ^property[59].valueCode = #V00364-2 
* #12080 ^property[60].code = #child 
* #12080 ^property[60].valueCode = #V00365-9 
* #12080 ^property[61].code = #child 
* #12080 ^property[61].valueCode = #V00366-7 
* #12080 ^property[62].code = #child 
* #12080 ^property[62].valueCode = #V00367-5 
* #12080 ^property[63].code = #child 
* #12080 ^property[63].valueCode = #V00368-3 
* #12080 ^property[64].code = #child 
* #12080 ^property[64].valueCode = #V00369-1 
* #12080 ^property[65].code = #child 
* #12080 ^property[65].valueCode = #V00370-9 
* #12080 ^property[66].code = #child 
* #12080 ^property[66].valueCode = #V00371-7 
* #12080 ^property[67].code = #child 
* #12080 ^property[67].valueCode = #V00372-5 
* #12080 ^property[68].code = #child 
* #12080 ^property[68].valueCode = #V00373-3 
* #12080 ^property[69].code = #child 
* #12080 ^property[69].valueCode = #V00374-1 
* #12080 ^property[70].code = #child 
* #12080 ^property[70].valueCode = #V00375-8 
* #12080 ^property[71].code = #child 
* #12080 ^property[71].valueCode = #V00376-6 
* #12080 ^property[72].code = #child 
* #12080 ^property[72].valueCode = #V00377-4 
* #12080 ^property[73].code = #child 
* #12080 ^property[73].valueCode = #V00378-2 
* #12080 ^property[74].code = #child 
* #12080 ^property[74].valueCode = #V00379-0 
* #12080 ^property[75].code = #child 
* #12080 ^property[75].valueCode = #V00380-8 
* #12080 ^property[76].code = #child 
* #12080 ^property[76].valueCode = #V00381-6 
* #12080 ^property[77].code = #child 
* #12080 ^property[77].valueCode = #V00382-4 
* #12080 ^property[78].code = #child 
* #12080 ^property[78].valueCode = #V00383-2 
* #12080 ^property[79].code = #child 
* #12080 ^property[79].valueCode = #V00384-0 
* #12080 ^property[80].code = #child 
* #12080 ^property[80].valueCode = #V00385-7 
* #12080 ^property[81].code = #child 
* #12080 ^property[81].valueCode = #V00386-5 
* #12080 ^property[82].code = #child 
* #12080 ^property[82].valueCode = #V00387-3 
* #12080 ^property[83].code = #child 
* #12080 ^property[83].valueCode = #V00388-1 
* #12080 ^property[84].code = #child 
* #12080 ^property[84].valueCode = #V00389-9 
* #12080 ^property[85].code = #child 
* #12080 ^property[85].valueCode = #V00390-7 
* #12080 ^property[86].code = #child 
* #12080 ^property[86].valueCode = #V00391-5 
* #12080 ^property[87].code = #child 
* #12080 ^property[87].valueCode = #V00392-3 
* #12080 ^property[88].code = #child 
* #12080 ^property[88].valueCode = #V00393-1 
* #12080 ^property[89].code = #child 
* #12080 ^property[89].valueCode = #V00394-9 
* #12080 ^property[90].code = #child 
* #12080 ^property[90].valueCode = #V00395-6 
* #12080 ^property[91].code = #child 
* #12080 ^property[91].valueCode = #V00396-4 
* #12080 ^property[92].code = #child 
* #12080 ^property[92].valueCode = #V00397-2 
* #12080 ^property[93].code = #child 
* #12080 ^property[93].valueCode = #V00398-0 
* #12080 ^property[94].code = #child 
* #12080 ^property[94].valueCode = #V00399-8 
* #12080 ^property[95].code = #child 
* #12080 ^property[95].valueCode = #V00400-4 
* #12080 ^property[96].code = #child 
* #12080 ^property[96].valueCode = #V00401-2 
* #12080 ^property[97].code = #child 
* #12080 ^property[97].valueCode = #V00402-0 
* #12080 ^property[98].code = #child 
* #12080 ^property[98].valueCode = #V00403-8 
* #12080 ^property[99].code = #child 
* #12080 ^property[99].valueCode = #V00404-6 
* #12080 ^property[100].code = #child 
* #12080 ^property[100].valueCode = #V00405-3 
* #12080 ^property[101].code = #child 
* #12080 ^property[101].valueCode = #V00406-1 
* #12080 ^property[102].code = #child 
* #12080 ^property[102].valueCode = #V00407-9 
* #12080 ^property[103].code = #child 
* #12080 ^property[103].valueCode = #V00408-7 
* #12080 ^property[104].code = #child 
* #12080 ^property[104].valueCode = #V00409-5 
* #12080 ^property[105].code = #child 
* #12080 ^property[105].valueCode = #V00410-3 
* #12080 ^property[106].code = #child 
* #12080 ^property[106].valueCode = #V00411-1 
* #12080 ^property[107].code = #child 
* #12080 ^property[107].valueCode = #V00412-9 
* #12080 ^property[108].code = #child 
* #12080 ^property[108].valueCode = #V00413-7 
* #12080 ^property[109].code = #child 
* #12080 ^property[109].valueCode = #V00414-5 
* #12080 ^property[110].code = #child 
* #12080 ^property[110].valueCode = #V00415-2 
* #12080 ^property[111].code = #child 
* #12080 ^property[111].valueCode = #V00416-0 
* #12080 ^property[112].code = #child 
* #12080 ^property[112].valueCode = #V00417-8 
* #12080 ^property[113].code = #child 
* #12080 ^property[113].valueCode = #V00418-6 
* #12080 ^property[114].code = #child 
* #12080 ^property[114].valueCode = #V00419-4 
* #12080 ^property[115].code = #child 
* #12080 ^property[115].valueCode = #V00420-2 
* #12080 ^property[116].code = #child 
* #12080 ^property[116].valueCode = #V00421-0 
* #12080 ^property[117].code = #child 
* #12080 ^property[117].valueCode = #V00422-8 
* #12080 ^property[118].code = #child 
* #12080 ^property[118].valueCode = #V00423-6 
* #12080 ^property[119].code = #child 
* #12080 ^property[119].valueCode = #V00424-4 
* #12080 ^property[120].code = #child 
* #12080 ^property[120].valueCode = #V00425-1 
* #12080 ^property[121].code = #child 
* #12080 ^property[121].valueCode = #V00426-9 
* #12080 ^property[122].code = #child 
* #12080 ^property[122].valueCode = #V00427-7 
* #12080 ^property[123].code = #child 
* #12080 ^property[123].valueCode = #V00428-5 
* #12080 ^property[124].code = #child 
* #12080 ^property[124].valueCode = #V00429-3 
* #12080 ^property[125].code = #child 
* #12080 ^property[125].valueCode = #V00430-1 
* #12080 ^property[126].code = #child 
* #12080 ^property[126].valueCode = #V00431-9 
* #V00270 "Allergie Befundint."
* #V00270 ^definition = Allergie Befundint.
* #V00270 ^designation[0].language = #de-AT 
* #V00270 ^designation[0].value = "Allergiediagnostik Befundinterpretation" 
* #V00270 ^property[0].code = #parent 
* #V00270 ^property[0].valueCode = #12080 
* #V00307-1 "IgE Kiwi nActd1 M"
* #V00307-1 ^definition = Kiwi nActd1 IgE M
* #V00307-1 ^designation[0].language = #de-AT 
* #V00307-1 ^designation[0].value = "Kiwi nActd1 IgE MA" 
* #V00307-1 ^property[0].code = #parent 
* #V00307-1 ^property[0].valueCode = #12080 
* #V00308-9 "IgE Kiwi nActd2 M"
* #V00308-9 ^definition = Kiwi nActd2 IgE M
* #V00308-9 ^designation[0].language = #de-AT 
* #V00308-9 ^designation[0].value = "Kiwi nActd2 IgE MA" 
* #V00308-9 ^property[0].code = #parent 
* #V00308-9 ^property[0].valueCode = #12080 
* #V00309-7 "IgE Kiwi nActd5 M"
* #V00309-7 ^definition = Kiwi nActd5 IgE M
* #V00309-7 ^designation[0].language = #de-AT 
* #V00309-7 ^designation[0].value = "Kiwi nActd5 IgE MA" 
* #V00309-7 ^property[0].code = #parent 
* #V00309-7 ^property[0].valueCode = #12080 
* #V00310-5 "IgE Kiwi rActd8 M"
* #V00310-5 ^definition = Kiwi rActd8 IgE M
* #V00310-5 ^designation[0].language = #de-AT 
* #V00310-5 ^designation[0].value = "Kiwi rActd8 IgE MA" 
* #V00310-5 ^property[0].code = #parent 
* #V00310-5 ^property[0].valueCode = #12080 
* #V00311-3 "Allergie Befundint.M"
* #V00311-3 ^definition = Allergie Befundint.M
* #V00311-3 ^designation[0].language = #de-AT 
* #V00311-3 ^designation[0].value = "Allergie Befundint. MA" 
* #V00311-3 ^property[0].code = #parent 
* #V00311-3 ^property[0].valueCode = #12080 
* #V00312-1 "IgE Erle rAlng1 M"
* #V00312-1 ^definition = Erle rAlng1 IgE M
* #V00312-1 ^designation[0].language = #de-AT 
* #V00312-1 ^designation[0].value = "Erle rAlng1 IgE MA" 
* #V00312-1 ^property[0].code = #parent 
* #V00312-1 ^property[0].valueCode = #12080 
* #V00313-9 "IgE Altern. Alta1 M"
* #V00313-9 ^definition = Altern. Alta1 IgE M
* #V00313-9 ^designation[0].language = #de-AT 
* #V00313-9 ^designation[0].value = "Schimmelpilz Alternaria rAlta1 IgE MA" 
* #V00313-9 ^property[0].code = #parent 
* #V00313-9 ^property[0].valueCode = #12080 
* #V00314-7 "IgE Altern. Alta6 M"
* #V00314-7 ^definition = Altern. Alta6 IgE M
* #V00314-7 ^designation[0].language = #de-AT 
* #V00314-7 ^designation[0].value = "Schimmelpilz Alternaria rAlta6 IgE MA" 
* #V00314-7 ^property[0].code = #parent 
* #V00314-7 ^property[0].valueCode = #12080 
* #V00315-4 "IgE BeifbTr.kr.A1 M"
* #V00315-4 ^definition = BeifbTr.kr.A1 IgE M
* #V00315-4 ^designation[0].language = #de-AT 
* #V00315-4 ^designation[0].value = "Beifüßblättriges Traubenkraut nAmba1 IgE MA" 
* #V00315-4 ^property[0].code = #parent 
* #V00315-4 ^property[0].valueCode = #12080 
* #V00316-2 "IgE Bromel. nAnac2 M"
* #V00316-2 ^definition = Bromel. nAnac2 IgE M
* #V00316-2 ^designation[0].language = #de-AT 
* #V00316-2 ^designation[0].value = "Bromelin nAnac2 IgE MA" 
* #V00316-2 ^property[0].code = #parent 
* #V00316-2 ^property[0].valueCode = #12080 
* #V00317-0 "IgE Cashewnu.Anao2 M"
* #V00317-0 ^definition = Cashewnu.Anao2 IgE M
* #V00317-0 ^designation[0].language = #de-AT 
* #V00317-0 ^designation[0].value = "Cashewnuss rAnao2 IgE MA" 
* #V00317-0 ^property[0].code = #parent 
* #V00317-0 ^property[0].valueCode = #12080 
* #V00318-8 "IgE Heringswu.Ans1 M"
* #V00318-8 ^definition = Heringswu.Ans1 IgE M
* #V00318-8 ^designation[0].language = #de-AT 
* #V00318-8 ^designation[0].value = "Heringswurm rAnis1 IgE MA" 
* #V00318-8 ^property[0].code = #parent 
* #V00318-8 ^property[0].valueCode = #12080 
* #V00319-6 "IgE Heringswu.Ans3 M"
* #V00319-6 ^definition = Heringswu.Ans3 IgE M
* #V00319-6 ^designation[0].language = #de-AT 
* #V00319-6 ^designation[0].value = "Heringswurm rAnis3 IgE MA" 
* #V00319-6 ^property[0].code = #parent 
* #V00319-6 ^property[0].valueCode = #12080 
* #V00320-4 "IgE Seller.rApig1 M"
* #V00320-4 ^definition = Seller.rApig1 IgE M
* #V00320-4 ^designation[0].language = #de-AT 
* #V00320-4 ^designation[0].value = "Sellerie rApig1 IgE MA" 
* #V00320-4 ^property[0].code = #parent 
* #V00320-4 ^property[0].valueCode = #12080 
* #V00321-2 "IgE Honigbien.pim1 M"
* #V00321-2 ^definition = Honigbien.pim1 IgE M
* #V00321-2 ^designation[0].language = #de-AT 
* #V00321-2 ^designation[0].value = "Honigbiene rApim1 IgE MA" 
* #V00321-2 ^property[0].code = #parent 
* #V00321-2 ^property[0].valueCode = #12080 
* #V00322-0 "IgE Honigbie.Apim4 M"
* #V00322-0 ^definition = Honigbie.Apim4 IgE M
* #V00322-0 ^designation[0].language = #de-AT 
* #V00322-0 ^designation[0].value = "Honigbiene nApim4 IgE MA" 
* #V00322-0 ^property[0].code = #parent 
* #V00322-0 ^property[0].valueCode = #12080 
* #V00323-8 "IgE Erdnuss rArah1 M"
* #V00323-8 ^definition = Erdnuss rArah1 IgE M
* #V00323-8 ^designation[0].language = #de-AT 
* #V00323-8 ^designation[0].value = "Erdnuss rArah1 IgE MA" 
* #V00323-8 ^property[0].code = #parent 
* #V00323-8 ^property[0].valueCode = #12080 
* #V00324-6 "IgE Erdnuss rArah2 M"
* #V00324-6 ^definition = Erdnuss rArah2 IgE M
* #V00324-6 ^designation[0].language = #de-AT 
* #V00324-6 ^designation[0].value = "Erdnuss rArah2 IgE MA" 
* #V00324-6 ^property[0].code = #parent 
* #V00324-6 ^property[0].valueCode = #12080 
* #V00325-3 "IgE Erdnuss rArah3 M"
* #V00325-3 ^definition = Erdnuss rArah3 IgE M
* #V00325-3 ^designation[0].language = #de-AT 
* #V00325-3 ^designation[0].value = "Erdnuss rArah3 IgE MA" 
* #V00325-3 ^property[0].code = #parent 
* #V00325-3 ^property[0].valueCode = #12080 
* #V00326-1 "IgE Erdnuss nArah6 M"
* #V00326-1 ^definition = Erdnuss nArah6 IgE M
* #V00326-1 ^designation[0].language = #de-AT 
* #V00326-1 ^designation[0].value = "Erdnuss nArah6 IgE MA" 
* #V00326-1 ^property[0].code = #parent 
* #V00326-1 ^property[0].valueCode = #12080 
* #V00327-9 "IgE Erdnuss rArah8 M"
* #V00327-9 ^definition = Erdnuss rArah8 IgE M
* #V00327-9 ^designation[0].language = #de-AT 
* #V00327-9 ^designation[0].value = "Erdnuss rArah8 IgE MA" 
* #V00327-9 ^property[0].code = #parent 
* #V00327-9 ^property[0].valueCode = #12080 
* #V00328-7 "IgE Erdnuss rArah9 M"
* #V00328-7 ^definition = Erdnuss rArah9 IgE M
* #V00328-7 ^designation[0].language = #de-AT 
* #V00328-7 ^designation[0].value = "Erdnuss rArah9 IgE MA" 
* #V00328-7 ^property[0].code = #parent 
* #V00328-7 ^property[0].valueCode = #12080 
* #V00329-5 "IgE Gem.Beif.Artv1 M"
* #V00329-5 ^definition = Gem.Beif.Artv1 IgE M
* #V00329-5 ^designation[0].language = #de-AT 
* #V00329-5 ^designation[0].value = "Gemeiner Beifuß nArtv1 IgE MA" 
* #V00329-5 ^property[0].code = #parent 
* #V00329-5 ^property[0].valueCode = #12080 
* #V00330-3 "IgE Gem.Beif.Artv3 M"
* #V00330-3 ^definition = Gem.Beif.Artv3 IgE M
* #V00330-3 ^designation[0].language = #de-AT 
* #V00330-3 ^designation[0].value = "Gemeiner Beifuß nArtv3 IgE MA" 
* #V00330-3 ^property[0].code = #parent 
* #V00330-3 ^property[0].valueCode = #12080 
* #V00331-1 "IgE Aspergill.Asf1 M"
* #V00331-1 ^definition = Aspergill.Asf1 IgE M
* #V00331-1 ^designation[0].language = #de-AT 
* #V00331-1 ^designation[0].value = "Aspergillus rAspf1 IgE MA" 
* #V00331-1 ^property[0].code = #parent 
* #V00331-1 ^property[0].valueCode = #12080 
* #V00332-9 "IgE Aspergill.Asf2 M"
* #V00332-9 ^definition = Aspergill.Asf2 IgE M
* #V00332-9 ^designation[0].language = #de-AT 
* #V00332-9 ^designation[0].value = "Aspergillus rAspf2 IgE MA" 
* #V00332-9 ^property[0].code = #parent 
* #V00332-9 ^property[0].valueCode = #12080 
* #V00333-7 "IgE Aspergill.Asf3 M"
* #V00333-7 ^definition = Aspergill.Asf3 IgE M
* #V00333-7 ^designation[0].language = #de-AT 
* #V00333-7 ^designation[0].value = "Aspergillus rAspf3 IgE MA" 
* #V00333-7 ^property[0].code = #parent 
* #V00333-7 ^property[0].valueCode = #12080 
* #V00334-5 "IgE Aspergill.Asf4 M"
* #V00334-5 ^definition = Aspergill.Asf4 IgE M
* #V00334-5 ^designation[0].language = #de-AT 
* #V00334-5 ^designation[0].value = "Aspergillus rAspf4 IgE MA" 
* #V00334-5 ^property[0].code = #parent 
* #V00334-5 ^property[0].valueCode = #12080 
* #V00335-2 "IgE Aspergill.Asf6 M"
* #V00335-2 ^definition = Aspergill.Asf6 IgE M
* #V00335-2 ^designation[0].language = #de-AT 
* #V00335-2 ^designation[0].value = "Aspergillus rAspf6 IgE MA" 
* #V00335-2 ^property[0].code = #parent 
* #V00335-2 ^property[0].valueCode = #12080 
* #V00336-0 "IgE Paranu.rBere1 M"
* #V00336-0 ^definition = Paranu.rBere1 IgE M
* #V00336-0 ^designation[0].language = #de-AT 
* #V00336-0 ^designation[0].value = "Paranuss rBere1 IgE MA" 
* #V00336-0 ^property[0].code = #parent 
* #V00336-0 ^property[0].valueCode = #12080 
* #V00337-8 "IgE Birke rBetv1 M"
* #V00337-8 ^definition = Birke rBetv1 IgE M
* #V00337-8 ^designation[0].language = #de-AT 
* #V00337-8 ^designation[0].value = "Birke rBetv1 IgE MA" 
* #V00337-8 ^property[0].code = #parent 
* #V00337-8 ^property[0].valueCode = #12080 
* #V00338-6 "IgE Birke rBetv2 M"
* #V00338-6 ^definition = Birke rBetv2 IgE M
* #V00338-6 ^designation[0].language = #de-AT 
* #V00338-6 ^designation[0].value = "Birke rBetv2 IgE MA" 
* #V00338-6 ^property[0].code = #parent 
* #V00338-6 ^property[0].valueCode = #12080 
* #V00339-4 "IgE Birke rBetv4 M"
* #V00339-4 ^definition = Birke rBetv4 IgE M
* #V00339-4 ^designation[0].language = #de-AT 
* #V00339-4 ^designation[0].value = "Birke rBetv4 IgE MA" 
* #V00339-4 ^property[0].code = #parent 
* #V00339-4 ^property[0].valueCode = #12080 
* #V00340-2 "IgE D.Küchens.Blg1 M"
* #V00340-2 ^definition = D.Küchens.Blg1 IgE M
* #V00340-2 ^designation[0].language = #de-AT 
* #V00340-2 ^designation[0].value = "Deutsche Küchenschabe rBlag1 IgE MA" 
* #V00340-2 ^property[0].code = #parent 
* #V00340-2 ^property[0].valueCode = #12080 
* #V00341-0 "IgE D.Küchens.Blg2 M"
* #V00341-0 ^definition = D.Küchens.Blg2 IgE M
* #V00341-0 ^designation[0].language = #de-AT 
* #V00341-0 ^designation[0].value = "Deutsche Küchenschabe rBlag2 IgE MA" 
* #V00341-0 ^property[0].code = #parent 
* #V00341-0 ^property[0].valueCode = #12080 
* #V00342-8 "IgE D.Küchens.Blg4 M"
* #V00342-8 ^definition = D.Küchens.Blg4 IgE M
* #V00342-8 ^designation[0].language = #de-AT 
* #V00342-8 ^designation[0].value = "Deutsche Küchenschabe rBlag4 IgE MA" 
* #V00342-8 ^property[0].code = #parent 
* #V00342-8 ^property[0].valueCode = #12080 
* #V00343-6 "IgE D.Küchens.Blg5 M"
* #V00343-6 ^definition = D.Küchens.Blg5 IgE M
* #V00343-6 ^designation[0].language = #de-AT 
* #V00343-6 ^designation[0].value = "Deutsche Küchenschabe rBlag5 IgE MA" 
* #V00343-6 ^property[0].code = #parent 
* #V00343-6 ^property[0].valueCode = #12080 
* #V00344-4 "IgE D.Küchens.Blg7 M"
* #V00344-4 ^definition = D.Küchens.Blg7 IgE M
* #V00344-4 ^designation[0].language = #de-AT 
* #V00344-4 ^designation[0].value = "Deutsche Küchenschabe rBlag7 IgE MA" 
* #V00344-4 ^property[0].code = #parent 
* #V00344-4 ^property[0].valueCode = #12080 
* #V00345-1 "IgE Vorratsmil.Bl5 M"
* #V00345-1 ^definition = Vorratsmil.Bl5 IgE M
* #V00345-1 ^designation[0].language = #de-AT 
* #V00345-1 ^designation[0].value = "Vorratsmilbe rBlot5 IgE MA" 
* #V00345-1 ^property[0].code = #parent 
* #V00345-1 ^property[0].valueCode = #12080 
* #V00346-9 "IgE Kuhmilc.nBosd4 M"
* #V00346-9 ^definition = Kuhmilc.nBosd4 IgE M
* #V00346-9 ^designation[0].language = #de-AT 
* #V00346-9 ^designation[0].value = "Kuhmilch nBosd4 IgE MA" 
* #V00346-9 ^property[0].code = #parent 
* #V00346-9 ^property[0].valueCode = #12080 
* #V00347-7 "IgE Kuhmilc.nBosd5 M"
* #V00347-7 ^definition = Kuhmilc.nBosd5 IgE M
* #V00347-7 ^designation[0].language = #de-AT 
* #V00347-7 ^designation[0].value = "Kuhmilch nBosd5 IgE MA" 
* #V00347-7 ^property[0].code = #parent 
* #V00347-7 ^property[0].valueCode = #12080 
* #V00348-5 "IgE Kuhmilc.nBosd6 M"
* #V00348-5 ^definition = Kuhmilc.nBosd6 IgE M
* #V00348-5 ^designation[0].language = #de-AT 
* #V00348-5 ^designation[0].value = "Kuhmilch nBosd6 IgE MA" 
* #V00348-5 ^property[0].code = #parent 
* #V00348-5 ^property[0].valueCode = #12080 
* #V00349-3 "IgE Kuhmilc.nBosd8 M"
* #V00349-3 ^definition = Kuhmilc.nBosd8 IgE M
* #V00349-3 ^designation[0].language = #de-AT 
* #V00349-3 ^designation[0].value = "Kuhmilch nBosd8 IgE MA" 
* #V00349-3 ^property[0].code = #parent 
* #V00349-3 ^property[0].valueCode = #12080 
* #V00350-1 "IgE Kuhm.nBosd lac. M"
* #V00350-1 ^definition = Kuhm.nBosd lac.IgE M
* #V00350-1 ^designation[0].language = #de-AT 
* #V00350-1 ^designation[0].value = "Kuhmilch nBosd lactoferrin IgE MA" 
* #V00350-1 ^property[0].code = #parent 
* #V00350-1 ^property[0].valueCode = #12080 
* #V00351-9 "IgE Haselpol.Co101 M"
* #V00351-9 ^definition = Haselpol.Co101 IgE M
* #V00351-9 ^designation[0].language = #de-AT 
* #V00351-9 ^designation[0].value = "Haselpollen rCora1.0101 IgE MA" 
* #V00351-9 ^property[0].code = #parent 
* #V00351-9 ^property[0].valueCode = #12080 
* #V00352-7 "IgE Hund rCanf 1 M"
* #V00352-7 ^definition = Hund rCanf 1 IgE M
* #V00352-7 ^designation[0].language = #de-AT 
* #V00352-7 ^designation[0].value = "Hund rCanf 1 IgE MA" 
* #V00352-7 ^property[0].code = #parent 
* #V00352-7 ^property[0].valueCode = #12080 
* #V00353-5 "IgE Hund rCanf 2 M"
* #V00353-5 ^definition = Hund rCanf 2 IgE M
* #V00353-5 ^designation[0].language = #de-AT 
* #V00353-5 ^designation[0].value = "Hund rCanf 2 IgE MA" 
* #V00353-5 ^property[0].code = #parent 
* #V00353-5 ^property[0].valueCode = #12080 
* #V00354-3 "IgE Hund nCanf 3 M"
* #V00354-3 ^definition = Hund nCanf 3 IgE M
* #V00354-3 ^designation[0].language = #de-AT 
* #V00354-3 ^designation[0].value = "Hund nCanf 3 IgE MA" 
* #V00354-3 ^property[0].code = #parent 
* #V00354-3 ^property[0].valueCode = #12080 
* #V00355-0 "IgE Hund rCanf 5 M"
* #V00355-0 ^definition = Hund rCanf 5 IgE M
* #V00355-0 ^designation[0].language = #de-AT 
* #V00355-0 ^designation[0].value = "Hund rCanf 5 IgE MA" 
* #V00355-0 ^property[0].code = #parent 
* #V00355-0 ^property[0].valueCode = #12080 
* #V00356-8 "IgE W.Gänsef.Chea1 M"
* #V00356-8 ^definition = W.Gänsef.Chea1 IgE M
* #V00356-8 ^designation[0].language = #de-AT 
* #V00356-8 ^designation[0].value = "Weißer Gänsefuß rChea1 IgE MA" 
* #V00356-8 ^property[0].code = #parent 
* #V00356-8 ^property[0].valueCode = #12080 
* #V00357-6 "IgE Cladosp.rClah8 M"
* #V00357-6 ^definition = Cladosp.rClah8 IgE M
* #V00357-6 ^designation[0].language = #de-AT 
* #V00357-6 ^designation[0].value = "Schimmelpilz Cladosporium rClah8 IgE MA" 
* #V00357-6 ^property[0].code = #parent 
* #V00357-6 ^property[0].valueCode = #12080 
* #V00358-4 "IgE Haselnu.Co104 M"
* #V00358-4 ^definition = Haselnu.Co104 IgE M
* #V00358-4 ^designation[0].language = #de-AT 
* #V00358-4 ^designation[0].value = "Haselnuss rCora1.0401 IgE MA" 
* #V00358-4 ^property[0].code = #parent 
* #V00358-4 ^property[0].valueCode = #12080 
* #V00359-2 "IgE Haselnu.rCora8 M"
* #V00359-2 ^definition = Haselnu.rCora8 IgE M
* #V00359-2 ^designation[0].language = #de-AT 
* #V00359-2 ^designation[0].value = "Haselnuss rCora8 IgE MA" 
* #V00359-2 ^property[0].code = #parent 
* #V00359-2 ^property[0].valueCode = #12080 
* #V00360-0 "IgE Haselnu.nCora9 M"
* #V00360-0 ^definition = Haselnu.nCora9 IgE M
* #V00360-0 ^designation[0].language = #de-AT 
* #V00360-0 ^designation[0].value = "Haselnuss nCora9 IgE MA" 
* #V00360-0 ^property[0].code = #parent 
* #V00360-0 ^property[0].valueCode = #12080 
* #V00361-8 "IgE Japan.Zed.Cry1 M"
* #V00361-8 ^definition = Japan.Zed.Cry1 IgE M
* #V00361-8 ^designation[0].language = #de-AT 
* #V00361-8 ^designation[0].value = "Japanische .Zeder nCryj1 IgE MA" 
* #V00361-8 ^property[0].code = #parent 
* #V00361-8 ^property[0].valueCode = #12080 
* #V00362-6 "IgE Ariz.Zypr.Cua1 M"
* #V00362-6 ^definition = Ariz.Zypr.Cua1 IgE M
* #V00362-6 ^designation[0].language = #de-AT 
* #V00362-6 ^designation[0].value = "Arizona Zypresse nCupa1 IgE MA" 
* #V00362-6 ^property[0].code = #parent 
* #V00362-6 ^property[0].valueCode = #12080 
* #V00363-4 "IgE Hundszahngr.C1 M"
* #V00363-4 ^definition = Hundszahngr.C1 IgE M
* #V00363-4 ^designation[0].language = #de-AT 
* #V00363-4 ^designation[0].value = "Hundszahngras nCynd1 IgE MA" 
* #V00363-4 ^property[0].code = #parent 
* #V00363-4 ^property[0].valueCode = #12080 
* #V00364-2 "IgE Karpf.Par.Cyc1 M"
* #V00364-2 ^definition = Karpf.Par.Cyc1 IgE M
* #V00364-2 ^designation[0].language = #de-AT 
* #V00364-2 ^designation[0].value = "Karpfen Par.Cyc1 IgE MA" 
* #V00364-2 ^property[0].code = #parent 
* #V00364-2 ^property[0].valueCode = #12080 
* #V00365-9 "IgE Karott.Dau c 1 M"
* #V00365-9 ^definition = Karott.Dau c 1 IgE M
* #V00365-9 ^designation[0].language = #de-AT 
* #V00365-9 ^designation[0].value = "Karotte Dau c 1 IgE MA" 
* #V00365-9 ^property[0].code = #parent 
* #V00365-9 ^property[0].valueCode = #12080 
* #V00366-7 "IgE Hausst.m.Drp10 M"
* #V00366-7 ^definition = Hausst.m.Drp10 IgE M
* #V00366-7 ^designation[0].language = #de-AT 
* #V00366-7 ^designation[0].value = "Hausstaubmilbe rDerp10 IgE MA" 
* #V00366-7 ^property[0].code = #parent 
* #V00366-7 ^property[0].valueCode = #12080 
* #V00367-5 "IgE Hausst.m.Derf1 M"
* #V00367-5 ^definition = Hausst.m.Derf1 IgE M
* #V00367-5 ^designation[0].language = #de-AT 
* #V00367-5 ^designation[0].value = "Hausstaubmilbe nDerf1 IgE MA" 
* #V00367-5 ^property[0].code = #parent 
* #V00367-5 ^property[0].valueCode = #12080 
* #V00368-3 "IgE Hausst.m.Derf2 M"
* #V00368-3 ^definition = Hausst.m.Derf2 IgE M
* #V00368-3 ^designation[0].language = #de-AT 
* #V00368-3 ^designation[0].value = "Hausstaubmilbe rDerf2 IgE MA" 
* #V00368-3 ^property[0].code = #parent 
* #V00368-3 ^property[0].valueCode = #12080 
* #V00369-1 "IgE Hausst.m.Derp1 M"
* #V00369-1 ^definition = Hausst.m.Derp1 IgE M
* #V00369-1 ^designation[0].language = #de-AT 
* #V00369-1 ^designation[0].value = "Hausstaubmilbe nDerp1 IgE MA" 
* #V00369-1 ^property[0].code = #parent 
* #V00369-1 ^property[0].valueCode = #12080 
* #V00370-9 "IgE Hausst.m.Derp2 M"
* #V00370-9 ^definition = Hausst.m.Derp2 IgE M
* #V00370-9 ^designation[0].language = #de-AT 
* #V00370-9 ^designation[0].value = "Hausstaubmilbe rDerp2 IgE MA" 
* #V00370-9 ^property[0].code = #parent 
* #V00370-9 ^property[0].valueCode = #12080 
* #V00371-7 "IgE Pferd rEquc1 M"
* #V00371-7 ^definition = Pferd rEquc1 IgE M
* #V00371-7 ^designation[0].language = #de-AT 
* #V00371-7 ^designation[0].value = "Pferd rEquc1 IgE MA" 
* #V00371-7 ^property[0].code = #parent 
* #V00371-7 ^property[0].valueCode = #12080 
* #V00372-5 "IgE Pferd nEquc3 M"
* #V00372-5 ^definition = Pferd nEquc3 IgE M
* #V00372-5 ^designation[0].language = #de-AT 
* #V00372-5 ^designation[0].value = "Pferd nEquc3 IgE MA" 
* #V00372-5 ^property[0].code = #parent 
* #V00372-5 ^property[0].valueCode = #12080 
* #V00373-3 "IgE Vorratsm.Eurm2 M"
* #V00373-3 ^definition = Vorratsm.Eurm2 IgE M
* #V00373-3 ^designation[0].language = #de-AT 
* #V00373-3 ^designation[0].value = "Vorratsm.Eurm2 IgE MA" 
* #V00373-3 ^property[0].code = #parent 
* #V00373-3 ^property[0].valueCode = #12080 
* #V00374-1 "IgE Buchweiz.Fage2 M"
* #V00374-1 ^definition = Buchweiz.Fage2 IgE M
* #V00374-1 ^designation[0].language = #de-AT 
* #V00374-1 ^designation[0].value = "Buchweizen nFage2 IgE MA" 
* #V00374-1 ^property[0].code = #parent 
* #V00374-1 ^property[0].valueCode = #12080 
* #V00375-8 "IgE Katze rFeld1 M"
* #V00375-8 ^definition = Katze rFeld1 IgE M
* #V00375-8 ^designation[0].language = #de-AT 
* #V00375-8 ^designation[0].value = "Katze rFeld1 IgE MA" 
* #V00375-8 ^property[0].code = #parent 
* #V00375-8 ^property[0].valueCode = #12080 
* #V00376-6 "IgE Katze nFeld2 M"
* #V00376-6 ^definition = Katze nFeld2 IgE M
* #V00376-6 ^designation[0].language = #de-AT 
* #V00376-6 ^designation[0].value = "Katze nFeld2 IgE MA" 
* #V00376-6 ^property[0].code = #parent 
* #V00376-6 ^property[0].valueCode = #12080 
* #V00377-4 "IgE Katze rFeld4 M"
* #V00377-4 ^definition = Katze rFeld4 IgE M
* #V00377-4 ^designation[0].language = #de-AT 
* #V00377-4 ^designation[0].value = "Katze rFeld4 IgE MA" 
* #V00377-4 ^property[0].code = #parent 
* #V00377-4 ^property[0].valueCode = #12080 
* #V00378-2 "IgE Dorsch rGadc1 M"
* #V00378-2 ^definition = Dorsch rGadc1 IgE M
* #V00378-2 ^designation[0].language = #de-AT 
* #V00378-2 ^designation[0].value = "Dorsch rGadc1 IgE MA" 
* #V00378-2 ^property[0].code = #parent 
* #V00378-2 ^property[0].valueCode = #12080 
* #V00379-0 "IgE Hühn.ei nGald1 M"
* #V00379-0 ^definition = Hühn.ei nGald1 IgE M
* #V00379-0 ^designation[0].language = #de-AT 
* #V00379-0 ^designation[0].value = "Hühnerei nGald1 IgE MA" 
* #V00379-0 ^property[0].code = #parent 
* #V00379-0 ^property[0].valueCode = #12080 
* #V00380-8 "IgE Hühn.ei nGald2 M"
* #V00380-8 ^definition = Hühn.ei nGald2 IgE M
* #V00380-8 ^designation[0].language = #de-AT 
* #V00380-8 ^designation[0].value = "Hühnerei nGald2 IgE MA" 
* #V00380-8 ^property[0].code = #parent 
* #V00380-8 ^property[0].valueCode = #12080 
* #V00381-6 "IgE Hühn.ei nGald3 M"
* #V00381-6 ^definition = Hühn.ei nGald3 IgE M
* #V00381-6 ^designation[0].language = #de-AT 
* #V00381-6 ^designation[0].value = "Hühnerei nGald3 IgE MA" 
* #V00381-6 ^property[0].code = #parent 
* #V00381-6 ^property[0].valueCode = #12080 
* #V00382-4 "IgE Hühn.ei nGald5 M"
* #V00382-4 ^definition = Hühn.ei nGald5 IgE M
* #V00382-4 ^designation[0].language = #de-AT 
* #V00382-4 ^designation[0].value = "Hühnerei nGald5 IgE MA" 
* #V00382-4 ^property[0].code = #parent 
* #V00382-4 ^property[0].valueCode = #12080 
* #V00383-2 "IgE Weizen Gliadin M"
* #V00383-2 ^definition = Weizen Gliadin IgE M
* #V00383-2 ^designation[0].language = #de-AT 
* #V00383-2 ^designation[0].value = "Weizen Gliadin IgE MA" 
* #V00383-2 ^property[0].code = #parent 
* #V00383-2 ^property[0].valueCode = #12080 
* #V00384-0 "IgE Soja rGlym4 M"
* #V00384-0 ^definition = Soja rGlym4  IgE M
* #V00384-0 ^designation[0].language = #de-AT 
* #V00384-0 ^designation[0].value = "Soja rGlym4  IgE MA" 
* #V00384-0 ^property[0].code = #parent 
* #V00384-0 ^property[0].valueCode = #12080 
* #V00385-7 "IgE Soja nGlym5 M"
* #V00385-7 ^definition = Soja nGlym5 IgE M
* #V00385-7 ^designation[0].language = #de-AT 
* #V00385-7 ^designation[0].value = "Soja nGlym5 IgE MA" 
* #V00385-7 ^property[0].code = #parent 
* #V00385-7 ^property[0].valueCode = #12080 
* #V00386-5 "IgE Soja nGlym6 M"
* #V00386-5 ^definition = Soja nGlym6 IgE M
* #V00386-5 ^designation[0].language = #de-AT 
* #V00386-5 ^designation[0].value = "Soja nGlym6 IgE MA" 
* #V00386-5 ^property[0].code = #parent 
* #V00386-5 ^property[0].valueCode = #12080 
* #V00387-3 "IgE Latex rHevb1 M"
* #V00387-3 ^definition = Latex rHevb1 IgE M
* #V00387-3 ^designation[0].language = #de-AT 
* #V00387-3 ^designation[0].value = "Latex rHevb1 IgE MA" 
* #V00387-3 ^property[0].code = #parent 
* #V00387-3 ^property[0].valueCode = #12080 
* #V00388-1 "IgE Latex rHevb3 M"
* #V00388-1 ^definition = Latex rHevb3 IgE M
* #V00388-1 ^designation[0].language = #de-AT 
* #V00388-1 ^designation[0].value = "Latex rHevb3 IgE MA" 
* #V00388-1 ^property[0].code = #parent 
* #V00388-1 ^property[0].valueCode = #12080 
* #V00389-9 "IgE Latex rHevb5 M"
* #V00389-9 ^definition = Latex rHevb5 IgE M
* #V00389-9 ^designation[0].language = #de-AT 
* #V00389-9 ^designation[0].value = "Latex rHevb5 IgE MA" 
* #V00389-9 ^property[0].code = #parent 
* #V00389-9 ^property[0].valueCode = #12080 
* #V00390-7 "IgE Late.rHevb6.01 M"
* #V00390-7 ^definition = Late.rHevb6.01 IgE M
* #V00390-7 ^designation[0].language = #de-AT 
* #V00390-7 ^designation[0].value = "Latex rHevb6.01 IgE MA" 
* #V00390-7 ^property[0].code = #parent 
* #V00390-7 ^property[0].valueCode = #12080 
* #V00391-5 "IgE Latex rHevb8 M"
* #V00391-5 ^definition = Latex rHevb8 IgE M
* #V00391-5 ^designation[0].language = #de-AT 
* #V00391-5 ^designation[0].value = "Latex rHevb8 IgE MA" 
* #V00391-5 ^property[0].code = #parent 
* #V00391-5 ^property[0].valueCode = #12080 
* #V00392-3 "IgE Walnuss nJugr1 M"
* #V00392-3 ^definition = Walnuss nJugr1 IgE M
* #V00392-3 ^designation[0].language = #de-AT 
* #V00392-3 ^designation[0].value = "Walnuss nJugr1 IgE MA" 
* #V00392-3 ^property[0].code = #parent 
* #V00392-3 ^property[0].valueCode = #12080 
* #V00393-1 "IgE Walnuss nJugr2 M"
* #V00393-1 ^definition = Walnuss nJugr2 IgE M
* #V00393-1 ^designation[0].language = #de-AT 
* #V00393-1 ^designation[0].value = "Walnuss nJugr2 IgE MA" 
* #V00393-1 ^property[0].code = #parent 
* #V00393-1 ^property[0].valueCode = #12080 
* #V00394-9 "IgE Walnuss nJugr3 M"
* #V00394-9 ^definition = Walnuss nJugr3 IgE M
* #V00394-9 ^designation[0].language = #de-AT 
* #V00394-9 ^designation[0].value = "Walnuss nJugr3 IgE MA" 
* #V00394-9 ^property[0].code = #parent 
* #V00394-9 ^property[0].valueCode = #12080 
* #V00395-6 "IgE Vorratsm.rLepd2 M"
* #V00395-6 ^definition = Vorratsm.rLepd2 Ig M
* #V00395-6 ^designation[0].language = #de-AT 
* #V00395-6 ^designation[0].value = "Vorratsmilbe rLepd2 IgE MA" 
* #V00395-6 ^property[0].code = #parent 
* #V00395-6 ^property[0].valueCode = #12080 
* #V00396-4 "IgE Apfel rMal d1 M"
* #V00396-4 ^definition = Apfel rMal d1 IgE M
* #V00396-4 ^designation[0].language = #de-AT 
* #V00396-4 ^designation[0].value = "Apfel rMal d1 IgE MA" 
* #V00396-4 ^property[0].code = #parent 
* #V00396-4 ^property[0].valueCode = #12080 
* #V00397-2 "IgE Bingelk.rMera1 M"
* #V00397-2 ^definition = Bingelk.rMera1 IgE M
* #V00397-2 ^designation[0].language = #de-AT 
* #V00397-2 ^designation[0].value = "Bingelkraut Einjährig rMera1 IgE MA" 
* #V00397-2 ^property[0].code = #parent 
* #V00397-2 ^property[0].valueCode = #12080 
* #V00398-0 "IgE Maus nMusm1 M"
* #V00398-0 ^definition = Maus nMusm1 IgE M
* #V00398-0 ^designation[0].language = #de-AT 
* #V00398-0 ^designation[0].value = "Maus nMusm1 IgE MA" 
* #V00398-0 ^property[0].code = #parent 
* #V00398-0 ^property[0].valueCode = #12080 
* #V00399-8 "IgE Bromel. nMuxf3 M"
* #V00399-8 ^definition = Bromel. nMuxf3 IgE M
* #V00399-8 ^designation[0].language = #de-AT 
* #V00399-8 ^designation[0].value = "Bromelin nMuxf3 IgE CCD-Marker MA" 
* #V00399-8 ^property[0].code = #parent 
* #V00399-8 ^property[0].valueCode = #12080 
* #V00400-4 "IgE Olivenb.nOlee1 M"
* #V00400-4 ^definition = Olivenb.nOlee1 IgE M
* #V00400-4 ^designation[0].language = #de-AT 
* #V00400-4 ^designation[0].value = "Olivenbaum nOlee1 IgE MA" 
* #V00400-4 ^property[0].code = #parent 
* #V00400-4 ^property[0].valueCode = #12080 
* #V00401-2 "IgE Olivenb.nOlee2 M"
* #V00401-2 ^definition = Olivenb.nOlee2 IgE M
* #V00401-2 ^designation[0].language = #de-AT 
* #V00401-2 ^designation[0].value = "Olivenbaum nOlee2 IgE MA" 
* #V00401-2 ^property[0].code = #parent 
* #V00401-2 ^property[0].valueCode = #12080 
* #V00402-0 "IgE Olivenb.nOlee7 M"
* #V00402-0 ^definition = Olivenb.nOlee7 IgE M
* #V00402-0 ^designation[0].language = #de-AT 
* #V00402-0 ^designation[0].value = "Olivenbaum nOlee7 IgE MA" 
* #V00402-0 ^property[0].code = #parent 
* #V00402-0 ^property[0].valueCode = #12080 
* #V00403-8 "IgE Olivenb.rOlee9 M"
* #V00403-8 ^definition = Olivenb.rOlee9 IgE M
* #V00403-8 ^designation[0].language = #de-AT 
* #V00403-8 ^designation[0].value = "Olivenbaum rOlee9 IgE MA" 
* #V00403-8 ^property[0].code = #parent 
* #V00403-8 ^property[0].valueCode = #12080 
* #V00404-6 "IgE Glaskr. rParj2 M"
* #V00404-6 ^definition = Glaskr. rParj2 IgE M
* #V00404-6 ^designation[0].language = #de-AT 
* #V00404-6 ^designation[0].value = "Glaskraut rParj2 IgE MA" 
* #V00404-6 ^property[0].code = #parent 
* #V00404-6 ^property[0].valueCode = #12080 
* #V00405-3 "IgE ShrimpTropom.Pena1 M"
* #V00405-3 ^definition = ShrimpTropom.Pena1 M
* #V00405-3 ^designation[0].language = #de-AT 
* #V00405-3 ^designation[0].value = "Shrimp Tropom.Pena1 MA" 
* #V00405-3 ^property[0].code = #parent 
* #V00405-3 ^property[0].valueCode = #12080 
* #V00406-1 "IgE Shrimp nPeni1 M"
* #V00406-1 ^definition = Shrimp nPeni1 M
* #V00406-1 ^designation[0].language = #de-AT 
* #V00406-1 ^designation[0].value = "Shrimp nPeni1 MA" 
* #V00406-1 ^property[0].code = #parent 
* #V00406-1 ^property[0].valueCode = #12080 
* #V00407-9 "IgE Shrimp nPenm1 M"
* #V00407-9 ^definition = Shrimp nPenm1 IgE M
* #V00407-9 ^designation[0].language = #de-AT 
* #V00407-9 ^designation[0].value = "Shrimp nPenm1 IgE MA" 
* #V00407-9 ^property[0].code = #parent 
* #V00407-9 ^property[0].valueCode = #12080 
* #V00408-7 "IgE Shrimp nPenm2 M"
* #V00408-7 ^definition = Shrimp nPenm2 IgE M
* #V00408-7 ^designation[0].language = #de-AT 
* #V00408-7 ^designation[0].value = "Shrimp nPenm2 IgE MA" 
* #V00408-7 ^property[0].code = #parent 
* #V00408-7 ^property[0].valueCode = #12080 
* #V00409-5 "IgE Shrimp nPenm4 M"
* #V00409-5 ^definition = Shrimp nPenm4 IgE M
* #V00409-5 ^designation[0].language = #de-AT 
* #V00409-5 ^designation[0].value = "Shrimp nPenm4 IgE MA" 
* #V00409-5 ^property[0].code = #parent 
* #V00409-5 ^property[0].valueCode = #12080 
* #V00410-3 "IgE Lieschgr.Phlp1 M"
* #V00410-3 ^definition = Lieschgr.Phlp1 IgE M
* #V00410-3 ^designation[0].language = #de-AT 
* #V00410-3 ^designation[0].value = "Lieschgras rPhlp1 IgE MA" 
* #V00410-3 ^property[0].code = #parent 
* #V00410-3 ^property[0].valueCode = #12080 
* #V00411-1 "IgE Lieschgr.Phlp2 M"
* #V00411-1 ^definition = Lieschgr.Phlp2 IgE M
* #V00411-1 ^designation[0].language = #de-AT 
* #V00411-1 ^designation[0].value = "Lieschgras rPhlp2 IgE MA" 
* #V00411-1 ^property[0].code = #parent 
* #V00411-1 ^property[0].valueCode = #12080 
* #V00412-9 "IgE Lieschgr.Phlp4 M"
* #V00412-9 ^definition = Lieschgr.Phlp4 IgE M
* #V00412-9 ^designation[0].language = #de-AT 
* #V00412-9 ^designation[0].value = "Lieschgras nPhlp4 IgE MA" 
* #V00412-9 ^property[0].code = #parent 
* #V00412-9 ^property[0].valueCode = #12080 
* #V00413-7 "IgE Lieschgr.Phlp5 M"
* #V00413-7 ^definition = Lieschgr.Phlp5 IgE M
* #V00413-7 ^designation[0].language = #de-AT 
* #V00413-7 ^designation[0].value = "Lieschgras rPhlp5 IgE MA" 
* #V00413-7 ^property[0].code = #parent 
* #V00413-7 ^property[0].valueCode = #12080 
* #V00414-5 "IgE Lieschgr.Phlp6 M"
* #V00414-5 ^definition = Lieschgr.Phlp6 IgE M
* #V00414-5 ^designation[0].language = #de-AT 
* #V00414-5 ^designation[0].value = "Lieschgras rPhlp6 IgE MA" 
* #V00414-5 ^property[0].code = #parent 
* #V00414-5 ^property[0].valueCode = #12080 
* #V00415-2 "IgE Lieschgr.Phlp7 M"
* #V00415-2 ^definition = Lieschgr.Phlp7 IgE M
* #V00415-2 ^designation[0].language = #de-AT 
* #V00415-2 ^designation[0].value = "Lieschgras rPhlp7 IgE MA" 
* #V00415-2 ^property[0].code = #parent 
* #V00415-2 ^property[0].valueCode = #12080 
* #V00416-0 "IgE Lieschg.Phlp11 M"
* #V00416-0 ^definition = Lieschg.Phlp11 IgE M
* #V00416-0 ^designation[0].language = #de-AT 
* #V00416-0 ^designation[0].value = "Lieschgras rPhlp11 IgE MA" 
* #V00416-0 ^property[0].code = #parent 
* #V00416-0 ^property[0].valueCode = #12080 
* #V00417-8 "IgE Lieschg.Phlp12 M"
* #V00417-8 ^definition = Lieschg.Phlp12 IgE M
* #V00417-8 ^designation[0].language = #de-AT 
* #V00417-8 ^designation[0].value = "Lieschgras rPhlp12 IgE MA" 
* #V00417-8 ^property[0].code = #parent 
* #V00417-8 ^property[0].valueCode = #12080 
* #V00418-6 "IgE Ahornbl.Pl.Pl1 M"
* #V00418-6 ^definition = Ahornbl.Pl.Pl1 IgE M
* #V00418-6 ^designation[0].language = #de-AT 
* #V00418-6 ^designation[0].value = "Ahornblättrige Platane rPlaa1 IgE MA" 
* #V00418-6 ^property[0].code = #parent 
* #V00418-6 ^property[0].valueCode = #12080 
* #V00419-4 "IgE Ahornbl.Pl.Pl2 M"
* #V00419-4 ^definition = Ahornbl.Pl.Pl2 IgE M
* #V00419-4 ^designation[0].language = #de-AT 
* #V00419-4 ^designation[0].value = "Ahornblättrige Platane rPlaa2 IgE MA" 
* #V00419-4 ^property[0].code = #parent 
* #V00419-4 ^property[0].valueCode = #12080 
* #V00420-2 "IgE Ahornbl.Pl.Pl3 M"
* #V00420-2 ^definition = Ahornbl.Pl.Pl3 IgE M
* #V00420-2 ^designation[0].language = #de-AT 
* #V00420-2 ^designation[0].value = "Ahornblättrige Platane rPlaa3 IgE MA" 
* #V00420-2 ^property[0].code = #parent 
* #V00420-2 ^property[0].valueCode = #12080 
* #V00421-0 "IgE Spitzweg.Plal1 M"
* #V00421-0 ^definition = Spitzweg.Plal1 IgE M
* #V00421-0 ^designation[0].language = #de-AT 
* #V00421-0 ^designation[0].value = "Spitzwegerich rPlal1 IgE MA" 
* #V00421-0 ^property[0].code = #parent 
* #V00421-0 ^property[0].valueCode = #12080 
* #V00422-8 "IgE F.Feldwes.Pld5 M"
* #V00422-8 ^definition = F.Feldwes.Pld5 IgE M
* #V00422-8 ^designation[0].language = #de-AT 
* #V00422-8 ^designation[0].value = "Französische Feldwespe rPold5 IgE MA" 
* #V00422-8 ^property[0].code = #parent 
* #V00422-8 ^property[0].valueCode = #12080 
* #V00423-6 "IgE Pfirsic.rPrup1 M"
* #V00423-6 ^definition = Pfirsic.rPrup1 IgE M
* #V00423-6 ^designation[0].language = #de-AT 
* #V00423-6 ^designation[0].value = "Pfirsich rPrup1 IgE MA" 
* #V00423-6 ^property[0].code = #parent 
* #V00423-6 ^property[0].valueCode = #12080 
* #V00424-4 "IgE Pfirsic.rPrup3 M"
* #V00424-4 ^definition = Pfirsic.rPrup3 IgE M
* #V00424-4 ^designation[0].language = #de-AT 
* #V00424-4 ^designation[0].value = "Pfirsich rPrup3 IgE MA" 
* #V00424-4 ^property[0].code = #parent 
* #V00424-4 ^property[0].valueCode = #12080 
* #V00425-1 "IgE Salzkr. nSalk1 M"
* #V00425-1 ^definition = Salzkr. nSalk1 IgE M
* #V00425-1 ^designation[0].language = #de-AT 
* #V00425-1 ^designation[0].value = "Salzkraut nSalk1 IgE MA" 
* #V00425-1 ^property[0].code = #parent 
* #V00425-1 ^property[0].valueCode = #12080 
* #V00426-9 "IgE Sesam nSesi1 M"
* #V00426-9 ^definition = Sesam nSesi1 IgE M
* #V00426-9 ^designation[0].language = #de-AT 
* #V00426-9 ^designation[0].value = "Sesam nSesi1 IgE MA" 
* #V00426-9 ^property[0].code = #parent 
* #V00426-9 ^property[0].valueCode = #12080 
* #V00427-7 "IgE Weizen nTria18 M"
* #V00427-7 ^definition = Weizen nTria18 IgE M
* #V00427-7 ^designation[0].language = #de-AT 
* #V00427-7 ^designation[0].value = "Weizen nTria18 IgE MA" 
* #V00427-7 ^property[0].code = #parent 
* #V00427-7 ^property[0].valueCode = #12080 
* #V00428-5 "IgE Weizen rTria14 M"
* #V00428-5 ^definition = Weizen rTria14 IgE M
* #V00428-5 ^designation[0].language = #de-AT 
* #V00428-5 ^designation[0].value = "Weizen rTria14 IgE MA" 
* #V00428-5 ^property[0].code = #parent 
* #V00428-5 ^property[0].valueCode = #12080 
* #V00429-3 "IgE Weizen nTria19 M"
* #V00429-3 ^definition = Weizen nTria19 IgE M
* #V00429-3 ^designation[0].language = #de-AT 
* #V00429-3 ^designation[0].value = "Weizen nTria19 IgE MA" 
* #V00429-3 ^property[0].code = #parent 
* #V00429-3 ^property[0].valueCode = #12080 
* #V00430-1 "IgE Weiz.nTriaaATI M"
* #V00430-1 ^definition = Weiz.nTriaaATI IgE M
* #V00430-1 ^designation[0].language = #de-AT 
* #V00430-1 ^designation[0].value = "Weizen nTriaaA_TI IgE MA" 
* #V00430-1 ^property[0].code = #parent 
* #V00430-1 ^property[0].valueCode = #12080 
* #V00431-9 "IgE Gem.Wesp.Vesv5 M"
* #V00431-9 ^definition = Gem.Wesp.Vesv5 IgE M
* #V00431-9 ^designation[0].language = #de-AT 
* #V00431-9 ^designation[0].value = "Gemeine Wespe rVesv5 IgE MA" 
* #V00431-9 ^property[0].code = #parent 
* #V00431-9 ^property[0].valueCode = #12080 
* #12090 "IgG Antigene"
* #12090 ^property[0].code = #parent 
* #12090 ^property[0].valueCode = #1800 
* #12090 ^property[1].code = #child 
* #12090 ^property[1].valueCode = #V00306-3 
* #12090 ^property[2].code = #child 
* #12090 ^property[2].valueCode = #V00499-6 
* #12090 ^property[3].code = #child 
* #12090 ^property[3].valueCode = #V00500-1 
* #12090 ^property[4].code = #child 
* #12090 ^property[4].valueCode = #V00501-9 
* #12090 ^property[5].code = #child 
* #12090 ^property[5].valueCode = #V00502-7 
* #12090 ^property[6].code = #child 
* #12090 ^property[6].valueCode = #V00503-5 
* #12090 ^property[7].code = #child 
* #12090 ^property[7].valueCode = #V00504-3 
* #12090 ^property[8].code = #child 
* #12090 ^property[8].valueCode = #V00505-0 
* #12090 ^property[9].code = #child 
* #12090 ^property[9].valueCode = #V00506-8 
* #12090 ^property[10].code = #child 
* #12090 ^property[10].valueCode = #V00507-6 
* #12090 ^property[11].code = #child 
* #12090 ^property[11].valueCode = #V00508-4 
* #12090 ^property[12].code = #child 
* #12090 ^property[12].valueCode = #V00509-2 
* #V00306-3 "IgG Penic. Chrys. qualitativ"
* #V00306-3 ^definition = Penic. Chrys. IgG ql
* #V00306-3 ^designation[0].language = #de-AT 
* #V00306-3 ^designation[0].value = "Penicillium chrysogenum IgG ql." 
* #V00306-3 ^property[0].code = #parent 
* #V00306-3 ^property[0].valueCode = #12090 
* #V00499-6 "IgG rPhl p1"
* #V00499-6 ^definition = IgG rPhl p1
* #V00499-6 ^designation[0].language = #de-AT 
* #V00499-6 ^designation[0].value = "IgG rPhl p1" 
* #V00499-6 ^property[0].code = #parent 
* #V00499-6 ^property[0].valueCode = #12090 
* #V00500-1 "IgG rPhl p2 (Lieschgras)"
* #V00500-1 ^definition = IgG rPhl p2
* #V00500-1 ^designation[0].language = #de-AT 
* #V00500-1 ^designation[0].value = "IgG rPhl p2 (Lieschgras)" 
* #V00500-1 ^property[0].code = #parent 
* #V00500-1 ^property[0].valueCode = #12090 
* #V00501-9 "IgG rPhl p5 (Lieschgras)"
* #V00501-9 ^definition = IgG rPhl p5
* #V00501-9 ^designation[0].language = #de-AT 
* #V00501-9 ^designation[0].value = "IgG rPhl p5 (Lieschgras)" 
* #V00501-9 ^property[0].code = #parent 
* #V00501-9 ^property[0].valueCode = #12090 
* #V00502-7 "IgG rPhl p4 (Lieschgras, nativ)"
* #V00502-7 ^definition = IgG rPhl p4
* #V00502-7 ^designation[0].language = #de-AT 
* #V00502-7 ^designation[0].value = "IgG rPhl p4 (Lieschgras, nativ)" 
* #V00502-7 ^property[0].code = #parent 
* #V00502-7 ^property[0].valueCode = #12090 
* #V00503-5 "IgG rPhl p6 (Lieschgras)"
* #V00503-5 ^definition = IgG rPhl p6
* #V00503-5 ^designation[0].language = #de-AT 
* #V00503-5 ^designation[0].value = "IgG rPhl p6 (Lieschgras)" 
* #V00503-5 ^property[0].code = #parent 
* #V00503-5 ^property[0].valueCode = #12090 
* #V00504-3 "IgG rPhl p7 (Lieschgras)"
* #V00504-3 ^definition = IgG rPhl p7
* #V00504-3 ^designation[0].language = #de-AT 
* #V00504-3 ^designation[0].value = "IgG rPhl p7 (Lieschgras)" 
* #V00504-3 ^property[0].code = #parent 
* #V00504-3 ^property[0].valueCode = #12090 
* #V00505-0 "IgG rPhl p11 (Lieschgras)"
* #V00505-0 ^definition = IgG rPhl p11
* #V00505-0 ^designation[0].language = #de-AT 
* #V00505-0 ^designation[0].value = "IgG rPhl p11 (Lieschgras)" 
* #V00505-0 ^property[0].code = #parent 
* #V00505-0 ^property[0].valueCode = #12090 
* #V00506-8 "IgG rPhl p12 (Lieschgras)"
* #V00506-8 ^definition = IgG rPhl p12
* #V00506-8 ^designation[0].language = #de-AT 
* #V00506-8 ^designation[0].value = "IgG rPhl p12 (Lieschgras)" 
* #V00506-8 ^property[0].code = #parent 
* #V00506-8 ^property[0].valueCode = #12090 
* #V00507-6 "IgG rBet v1 (Birke)"
* #V00507-6 ^definition = IgG rBet v1 (Birke)
* #V00507-6 ^designation[0].language = #de-AT 
* #V00507-6 ^designation[0].value = "IgG rBet v1 (Birke)" 
* #V00507-6 ^property[0].code = #parent 
* #V00507-6 ^property[0].valueCode = #12090 
* #V00508-4 "IgG rBet v2 (Birke)"
* #V00508-4 ^definition = IgG rBet v2 (Birke)
* #V00508-4 ^designation[0].language = #de-AT 
* #V00508-4 ^designation[0].value = "IgG rBet v2 (Birke)" 
* #V00508-4 ^property[0].code = #parent 
* #V00508-4 ^property[0].valueCode = #12090 
* #V00509-2 "IgG rBet v4 (Birke)"
* #V00509-2 ^definition = IgG rBet v4 (Birke)
* #V00509-2 ^designation[0].language = #de-AT 
* #V00509-2 ^designation[0].value = "IgG rBet v4 (Birke)" 
* #V00509-2 ^property[0].code = #parent 
* #V00509-2 ^property[0].valueCode = #12090 
* #200 "Blutgasanalytik"
* #200 ^property[0].code = #parent 
* #200 ^property[0].valueCode = #1 
* #200 ^property[1].code = #child 
* #200 ^property[1].valueCode = #02060 
* #200 ^property[2].code = #child 
* #200 ^property[2].valueCode = #02070 
* #200 ^property[3].code = #child 
* #200 ^property[3].valueCode = #02080 
* #200 ^property[4].code = #child 
* #200 ^property[4].valueCode = #02090 
* #200 ^property[5].code = #child 
* #200 ^property[5].valueCode = #02100 
* #200 ^property[6].code = #child 
* #200 ^property[6].valueCode = #02110 
* #200 ^property[7].code = #child 
* #200 ^property[7].valueCode = #02120 
* #200 ^property[8].code = #child 
* #200 ^property[8].valueCode = #02130 
* #02060 "Blutgasanalyse arteriell"
* #02060 ^property[0].code = #parent 
* #02060 ^property[0].valueCode = #200 
* #02070 "Hb-Derivate arteriell"
* #02070 ^property[0].code = #parent 
* #02070 ^property[0].valueCode = #200 
* #02070 ^property[1].code = #child 
* #02070 ^property[1].valueCode = #V00230 
* #V00230 "Sulfhämoglobin art."
* #V00230 ^definition = Sulfhämoglobin art.
* #V00230 ^designation[0].language = #de-AT 
* #V00230 ^designation[0].value = "Sulfhämoglobin arteriell" 
* #V00230 ^property[0].code = #parent 
* #V00230 ^property[0].valueCode = #02070 
* #02080 "Blutgasanalyse venös"
* #02080 ^property[0].code = #parent 
* #02080 ^property[0].valueCode = #200 
* #02090 "Hb-Derivate venös"
* #02090 ^property[0].code = #parent 
* #02090 ^property[0].valueCode = #200 
* #02090 ^property[1].code = #child 
* #02090 ^property[1].valueCode = #V00229 
* #V00229 "Sulfhämoglobin ven."
* #V00229 ^definition = Sulfhämoglobin ven
* #V00229 ^designation[0].language = #de-AT 
* #V00229 ^designation[0].value = "Sulfhämoglobin venös" 
* #V00229 ^property[0].code = #parent 
* #V00229 ^property[0].valueCode = #02090 
* #02100 "Blutgasanalyse kapillär"
* #02100 ^property[0].code = #parent 
* #02100 ^property[0].valueCode = #200 
* #02100 ^property[1].code = #child 
* #02100 ^property[1].valueCode = #V00443-4 
* #02100 ^property[2].code = #child 
* #02100 ^property[2].valueCode = #V00453-3 
* #V00443-4 "Kreatinin Kap."
* #V00443-4 ^definition = Kreatinin BK
* #V00443-4 ^designation[0].language = #de-AT 
* #V00443-4 ^designation[0].value = "Kreatinin Kapillarblut" 
* #V00443-4 ^property[0].code = #parent 
* #V00443-4 ^property[0].valueCode = #02100 
* #V00453-3 "Bilirubin Kap."
* #V00453-3 ^definition = Bilirubin BK
* #V00453-3 ^designation[0].language = #de-AT 
* #V00453-3 ^designation[0].value = "Bilirubin Kapillarblut" 
* #V00453-3 ^property[0].code = #parent 
* #V00453-3 ^property[0].valueCode = #02100 
* #02110 "Hb-Derivate kapillär"
* #02110 ^property[0].code = #parent 
* #02110 ^property[0].valueCode = #200 
* #02110 ^property[1].code = #child 
* #02110 ^property[1].valueCode = #V00228 
* #V00228 "Sulfhämoglobin Kap."
* #V00228 ^definition = Sulfhämoglobin kap
* #V00228 ^designation[0].language = #de-AT 
* #V00228 ^designation[0].value = "Sulfhämoglobin kapillär" 
* #V00228 ^property[0].code = #parent 
* #V00228 ^property[0].valueCode = #02110 
* #02120 "Hb-Derivate, gemischtvenös (Rechtsherzkatheter)"
* #02120 ^property[0].code = #parent 
* #02120 ^property[0].valueCode = #200 
* #02120 ^property[1].code = #child 
* #02120 ^property[1].valueCode = #V00227 
* #02120 ^property[2].code = #child 
* #02120 ^property[2].valueCode = #V00712-2 
* #02120 ^property[3].code = #child 
* #02120 ^property[3].valueCode = #V00713-0 
* #V00227 "Sulfhämoglobin gev."
* #V00227 ^definition = Sulfhämoglobin gev
* #V00227 ^designation[0].language = #de-AT 
* #V00227 ^designation[0].value = "Sulfhämoglobin gemischt venös" 
* #V00227 ^property[0].code = #parent 
* #V00227 ^property[0].valueCode = #02120 
* #V00712-2 "Chlorid gev."
* #V00712-2 ^definition = Chlorid gev.
* #V00712-2 ^designation[0].language = #de-AT 
* #V00712-2 ^designation[0].value = "Chlorid gemischt venös" 
* #V00712-2 ^property[0].code = #parent 
* #V00712-2 ^property[0].valueCode = #02120 
* #V00713-0 "Glucose gev."
* #V00713-0 ^definition = Glucose gev.
* #V00713-0 ^designation[0].language = #de-AT 
* #V00713-0 ^designation[0].value = "Glucose gemischt venös" 
* #V00713-0 ^property[0].code = #parent 
* #V00713-0 ^property[0].valueCode = #02120 
* #02130 "BGA Sonstiges"
* #02130 ^property[0].code = #parent 
* #02130 ^property[0].valueCode = #200 
* #02130 ^property[1].code = #child 
* #02130 ^property[1].valueCode = #V00276 
* #V00276 "Kommentar BGA"
* #V00276 ^definition = Kommentar BGA
* #V00276 ^designation[0].language = #de-AT 
* #V00276 ^designation[0].value = "Kommentar Blutgasanalyse" 
* #V00276 ^property[0].code = #parent 
* #V00276 ^property[0].valueCode = #02130 
* #2300 "Genetische Diagnostik"
* #2300 ^property[0].code = #parent 
* #2300 ^property[0].valueCode = #1 
* #2300 ^property[1].code = #child 
* #2300 ^property[1].valueCode = #16830 
* #2300 ^property[2].code = #child 
* #2300 ^property[2].valueCode = #16840 
* #2300 ^property[3].code = #child 
* #2300 ^property[3].valueCode = #16841 
* #16830 "Pharmakogenetik"
* #16830 ^property[0].code = #parent 
* #16830 ^property[0].valueCode = #2300 
* #16830 ^property[1].code = #child 
* #16830 ^property[1].valueCode = #V00171 
* #16830 ^property[2].code = #child 
* #16830 ^property[2].valueCode = #V00172 
* #V00171 "DYPD I560S Mut."
* #V00171 ^definition = DYPD I560S Mut.
* #V00171 ^designation[0].language = #de-AT 
* #V00171 ^designation[0].value = "Dihydropyrimidin-Dehydrogenase I560S Mut.-Analy." 
* #V00171 ^property[0].code = #parent 
* #V00171 ^property[0].valueCode = #16830 
* #V00172 "DYPD D949V Mut."
* #V00172 ^definition = DYPD D949V Mut.
* #V00172 ^designation[0].language = #de-AT 
* #V00172 ^designation[0].value = "Dihydropyrimidin-Dehydrogenase D949V Mut.-Analy." 
* #V00172 ^property[0].code = #parent 
* #V00172 ^property[0].valueCode = #16830 
* #16840 "Humangenetik"
* #16840 ^property[0].code = #parent 
* #16840 ^property[0].valueCode = #2300 
* #16840 ^property[1].code = #child 
* #16840 ^property[1].valueCode = #V00063 
* #16840 ^property[2].code = #child 
* #16840 ^property[2].valueCode = #V00225 
* #16840 ^property[3].code = #child 
* #16840 ^property[3].valueCode = #V00260 
* #16840 ^property[4].code = #child 
* #16840 ^property[4].valueCode = #V00261 
* #16840 ^property[5].code = #child 
* #16840 ^property[5].valueCode = #V00262 
* #16840 ^property[6].code = #child 
* #16840 ^property[6].valueCode = #V00263 
* #16840 ^property[7].code = #child 
* #16840 ^property[7].valueCode = #V00264 
* #16840 ^property[8].code = #child 
* #16840 ^property[8].valueCode = #V00265 
* #16840 ^property[9].code = #child 
* #16840 ^property[9].valueCode = #V00266 
* #16840 ^property[10].code = #child 
* #16840 ^property[10].valueCode = #V00465-7 
* #V00063 "Lactoseintoleranz Genanalyse"
* #V00063 ^definition = Lact.Int.Gen-Anal.
* #V00063 ^designation[0].language = #de-AT 
* #V00063 ^designation[0].value = "Lactoseintoleranz Genanalyse" 
* #V00063 ^property[0].code = #parent 
* #V00063 ^property[0].valueCode = #16840 
* #V00225 "DYPD*2A Mut."
* #V00225 ^definition = DYPD*2A Mut.
* #V00225 ^designation[0].language = #de-AT 
* #V00225 ^designation[0].value = "Dihydropyrimidin-Dehydrogenase *2A Mutationsanaly." 
* #V00225 ^property[0].code = #parent 
* #V00225 ^property[0].valueCode = #16840 
* #V00260 "Genotyp. Colontu."
* #V00260 ^definition = Genotyp. Colontu.
* #V00260 ^designation[0].language = #de-AT 
* #V00260 ^designation[0].value = "Genotypisierung bei Verdacht auf Colontumor" 
* #V00260 ^property[0].code = #parent 
* #V00260 ^property[0].valueCode = #16840 
* #V00261 "Genotyp. Mammatu."
* #V00261 ^definition = Genotyp. Mammatu.
* #V00261 ^designation[0].language = #de-AT 
* #V00261 ^designation[0].value = "Genotypisierung bei Verdacht auf Mammatumor" 
* #V00261 ^property[0].code = #parent 
* #V00261 ^property[0].valueCode = #16840 
* #V00262 "Genotyp. Prostatatu."
* #V00262 ^definition = Genotyp. Prostatatu.
* #V00262 ^designation[0].language = #de-AT 
* #V00262 ^designation[0].value = "Genotypisierung bei Verdacht auf Prostatatumor" 
* #V00262 ^property[0].code = #parent 
* #V00262 ^property[0].valueCode = #16840 
* #V00263 "Genotyp. Lungentu."
* #V00263 ^definition = Genotyp. Lungentu.
* #V00263 ^designation[0].language = #de-AT 
* #V00263 ^designation[0].value = "Genotypisierung bei Verdacht auf Lungentumor" 
* #V00263 ^property[0].code = #parent 
* #V00263 ^property[0].valueCode = #16840 
* #V00264 "Genotyp. Magentu."
* #V00264 ^definition = Genotyp. Magentu.
* #V00264 ^designation[0].language = #de-AT 
* #V00264 ^designation[0].value = "Genotypisierung bei Verdacht auf Magentumor" 
* #V00264 ^property[0].code = #parent 
* #V00264 ^property[0].valueCode = #16840 
* #V00265 "Genotyp. Pancreastu."
* #V00265 ^definition = Genotyp. Pancreastu.
* #V00265 ^designation[0].language = #de-AT 
* #V00265 ^designation[0].value = "Genotypisierung bei Verdacht auf Pancreastumor" 
* #V00265 ^property[0].code = #parent 
* #V00265 ^property[0].valueCode = #16840 
* #V00266 "Genotyp. Nierentu."
* #V00266 ^definition = Genotyp. Nierentu.
* #V00266 ^designation[0].language = #de-AT 
* #V00266 ^designation[0].value = "Genotypisierung bei Verdacht auf Nierentumor" 
* #V00266 ^property[0].code = #parent 
* #V00266 ^property[0].valueCode = #16840 
* #V00465-7 "A1AT Genotypisierung"
* #V00465-7 ^definition = A1AT Genotypisierung
* #V00465-7 ^designation[0].language = #de-AT 
* #V00465-7 ^designation[0].value = "Alpha 1 Antitrypsin Genotypisierung" 
* #V00465-7 ^property[0].code = #parent 
* #V00465-7 ^property[0].valueCode = #16840 
* #16841 "Hämatologische Genetik"
* #16841 ^property[0].code = #parent 
* #16841 ^property[0].valueCode = #2300 
* #16841 ^property[1].code = #child 
* #16841 ^property[1].valueCode = #V00252 
* #16841 ^property[2].code = #child 
* #16841 ^property[2].valueCode = #V00253 
* #16841 ^property[3].code = #child 
* #16841 ^property[3].valueCode = #V00254 
* #16841 ^property[4].code = #child 
* #16841 ^property[4].valueCode = #V00255 
* #16841 ^property[5].code = #child 
* #16841 ^property[5].valueCode = #V00256 
* #16841 ^property[6].code = #child 
* #16841 ^property[6].valueCode = #V00257 
* #16841 ^property[7].code = #child 
* #16841 ^property[7].valueCode = #V00258 
* #16841 ^property[8].code = #child 
* #16841 ^property[8].valueCode = #V00259 
* #16841 ^property[9].code = #child 
* #16841 ^property[9].valueCode = #V00295 
* #16841 ^property[10].code = #child 
* #16841 ^property[10].valueCode = #V00296 
* #16841 ^property[11].code = #child 
* #16841 ^property[11].valueCode = #V00297 
* #16841 ^property[12].code = #child 
* #16841 ^property[12].valueCode = #V00302 
* #16841 ^property[13].code = #child 
* #16841 ^property[13].valueCode = #V00589-4 
* #16841 ^property[14].code = #child 
* #16841 ^property[14].valueCode = #V00590-2 
* #16841 ^property[15].code = #child 
* #16841 ^property[15].valueCode = #V00591-0 
* #V00252 "Genotyp. bei AML"
* #V00252 ^definition = Genotyp. bei AML
* #V00252 ^designation[0].language = #de-AT 
* #V00252 ^designation[0].value = "Genotypisierung bei Verdacht auf AML" 
* #V00252 ^property[0].code = #parent 
* #V00252 ^property[0].valueCode = #16841 
* #V00253 "Genotyp. bei CML"
* #V00253 ^definition = Genotyp. bei CML
* #V00253 ^designation[0].language = #de-AT 
* #V00253 ^designation[0].value = "Genotypisierung bei Verdacht auf CML" 
* #V00253 ^property[0].code = #parent 
* #V00253 ^property[0].valueCode = #16841 
* #V00254 "Genotyp. bei ALL"
* #V00254 ^definition = Genotyp. bei ALL
* #V00254 ^designation[0].language = #de-AT 
* #V00254 ^designation[0].value = "Genotypisierung bei Verdacht auf ALL" 
* #V00254 ^property[0].code = #parent 
* #V00254 ^property[0].valueCode = #16841 
* #V00255 "Genotyp. bei Lymphom"
* #V00255 ^definition = Genotyp. bei Lymphom
* #V00255 ^designation[0].language = #de-AT 
* #V00255 ^designation[0].value = "Genotypisierung bei Verdacht auf Lymphom" 
* #V00255 ^property[0].code = #parent 
* #V00255 ^property[0].valueCode = #16841 
* #V00256 "Genotyp. bei CLL"
* #V00256 ^definition = Genotyp. bei CLL
* #V00256 ^designation[0].language = #de-AT 
* #V00256 ^designation[0].value = "Genotypisierung bei Verdacht auf CLL" 
* #V00256 ^property[0].code = #parent 
* #V00256 ^property[0].valueCode = #16841 
* #V00257 "Gentyp. PlasmazellKH"
* #V00257 ^definition = Gentyp. PlasmazellKH
* #V00257 ^designation[0].language = #de-AT 
* #V00257 ^designation[0].value = "Genotypisierung bei Vd. a. Plasmazellerkrankung" 
* #V00257 ^property[0].code = #parent 
* #V00257 ^property[0].valueCode = #16841 
* #V00258 "Genotyp. bei MDS"
* #V00258 ^definition = Genotyp. bei MDS
* #V00258 ^designation[0].language = #de-AT 
* #V00258 ^designation[0].value = "Genotypisierung bei Verdacht auf MDS" 
* #V00258 ^property[0].code = #parent 
* #V00258 ^property[0].valueCode = #16841 
* #V00259 "Genotyp. bei MPN"
* #V00259 ^definition = Genotyp. bei MPN
* #V00259 ^designation[0].language = #de-AT 
* #V00259 ^designation[0].value = "Genotypisierung bei Verdacht auf MPN" 
* #V00259 ^property[0].code = #parent 
* #V00259 ^property[0].valueCode = #16841 
* #V00295 "Befundint.mol.Häm."
* #V00295 ^definition = Befundint.mol.Häm.
* #V00295 ^designation[0].language = #de-AT 
* #V00295 ^designation[0].value = "Befundinterpretation molekulare Hämatologie" 
* #V00295 ^property[0].code = #parent 
* #V00295 ^property[0].valueCode = #16841 
* #V00296 "JAK2 Mut.V617F KM"
* #V00296 ^definition = JAK2 Mut.V617F KM
* #V00296 ^designation[0].language = #de-AT 
* #V00296 ^designation[0].value = "JAK2 Genmutation V617F Knochenmark" 
* #V00296 ^property[0].code = #parent 
* #V00296 ^property[0].valueCode = #16841 
* #V00297 "JAK2 Mut.V617F Blut"
* #V00297 ^definition = JAK2 Mut.V617F Blut
* #V00297 ^designation[0].language = #de-AT 
* #V00297 ^designation[0].value = "JAK2 Genmutation V617F Blut" 
* #V00297 ^property[0].code = #parent 
* #V00297 ^property[0].valueCode = #16841 
* #V00302 "Inversion 16 qn."
* #V00302 ^definition = Inversion 16 qn.
* #V00302 ^designation[0].language = #de-AT 
* #V00302 ^designation[0].value = "Inversion 16 quantitativ" 
* #V00302 ^property[0].code = #parent 
* #V00302 ^property[0].valueCode = #16841 
* #V00589-4 "TP53 Genmutation /KM"
* #V00589-4 ^definition = TP53 Genmutation /KM
* #V00589-4 ^designation[0].language = #de-AT 
* #V00589-4 ^designation[0].value = "TP53 Genmutation /Knochenmark" 
* #V00589-4 ^property[0].code = #parent 
* #V00589-4 ^property[0].valueCode = #16841 
* #V00590-2 "FLT3 Genmutation (Asp835+Ile836) /KM"
* #V00590-2 ^definition = FLT3 M A835+I836 /KM
* #V00590-2 ^designation[0].language = #de-AT 
* #V00590-2 ^designation[0].value = "FLT3 Genmutation (Asp835+Ile836) /Knochenmark" 
* #V00590-2 ^property[0].code = #parent 
* #V00590-2 ^property[0].valueCode = #16841 
* #V00591-0 "MYD88 Genmutation L265P /KM"
* #V00591-0 ^definition = MYD88 Gm. L265P /KM
* #V00591-0 ^designation[0].language = #de-AT 
* #V00591-0 ^designation[0].value = "MYD88 Genmutation L265P /Knochenmark" 
* #V00591-0 ^property[0].code = #parent 
* #V00591-0 ^property[0].valueCode = #16841 
* #2500 "Sonstige"
* #2500 ^property[0].code = #parent 
* #2500 ^property[0].valueCode = #1 
* #2500 ^property[1].code = #child 
* #2500 ^property[1].valueCode = #17880 
* #2500 ^property[2].code = #child 
* #2500 ^property[2].valueCode = #17890 
* #17880 "Zytologie"
* #17880 ^property[0].code = #parent 
* #17880 ^property[0].valueCode = #2500 
* #17880 ^property[1].code = #child 
* #17880 ^property[1].valueCode = #V00680-1 
* #17880 ^property[2].code = #child 
* #17880 ^property[2].valueCode = #V00681-9 
* #17880 ^property[3].code = #child 
* #17880 ^property[3].valueCode = #V00682-7 
* #17880 ^property[4].code = #child 
* #17880 ^property[4].valueCode = #V00683-5 
* #17880 ^property[5].code = #child 
* #17880 ^property[5].valueCode = #V00688-4 
* #17880 ^property[6].code = #child 
* #17880 ^property[6].valueCode = #V00691-8 
* #V00680-1 "Alveolarmakrophagen mikr. /SM"
* #V00680-1 ^definition = AM m./SM
* #V00680-1 ^designation[0].language = #de-AT 
* #V00680-1 ^designation[0].value = "Alveolarmakrophagen mikr. /Sondermaterial" 
* #V00680-1 ^property[0].code = #parent 
* #V00680-1 ^property[0].valueCode = #17880 
* #V00681-9 "Epithelzellen mikr. /SM"
* #V00681-9 ^definition = Epithelzellen m./SM
* #V00681-9 ^designation[0].language = #de-AT 
* #V00681-9 ^designation[0].value = "Epithelzellen mikr. /Sondermaterial" 
* #V00681-9 ^property[0].code = #parent 
* #V00681-9 ^property[0].valueCode = #17880 
* #V00682-7 "Plattenepithelzellen mikr. /SM"
* #V00682-7 ^definition = PE m./SM
* #V00682-7 ^designation[0].language = #de-AT 
* #V00682-7 ^designation[0].value = "Plattenepithelzellen mikr. /Sondermaterial" 
* #V00682-7 ^property[0].code = #parent 
* #V00682-7 ^property[0].valueCode = #17880 
* #V00683-5 "Flimmerepithelzellen mikr. /SM"
* #V00683-5 ^definition = FZ m./SM
* #V00683-5 ^designation[0].language = #de-AT 
* #V00683-5 ^designation[0].value = "Flimmerepithelzellen mikr. /Sondermaterial" 
* #V00683-5 ^property[0].code = #parent 
* #V00683-5 ^property[0].valueCode = #17880 
* #V00688-4 "Zelldetritus mikr. /SM"
* #V00688-4 ^definition = Zelldetritus m./SM
* #V00688-4 ^designation[0].language = #de-AT 
* #V00688-4 ^designation[0].value = "Zelldetritus mikr. /Sondermaterial" 
* #V00688-4 ^property[0].code = #parent 
* #V00688-4 ^property[0].valueCode = #17880 
* #V00691-8 "Zellen mikr. /SM"
* #V00691-8 ^definition = Zellen m./SM
* #V00691-8 ^designation[0].language = #de-AT 
* #V00691-8 ^designation[0].value = "Zellen mikr. /Sondermaterial" 
* #V00691-8 ^property[0].code = #parent 
* #V00691-8 ^property[0].valueCode = #17880 
* #17890 "Sonstige"
* #17890 ^property[0].code = #parent 
* #17890 ^property[0].valueCode = #2500 
* #17890 ^property[1].code = #child 
* #17890 ^property[1].valueCode = #V00463-2 
* #17890 ^property[2].code = #child 
* #17890 ^property[2].valueCode = #V00565-4 
* #V00463-2 "Osmolale Clearance"
* #V00463-2 ^definition = Osmolale Clearance
* #V00463-2 ^designation[0].language = #de-AT 
* #V00463-2 ^designation[0].value = "Osmolale Clearance" 
* #V00463-2 ^property[0].code = #parent 
* #V00463-2 ^property[0].valueCode = #17890 
* #V00565-4 "Kommentar Demenz"
* #V00565-4 ^definition = Kommentar Demenz
* #V00565-4 ^designation[0].language = #de-AT 
* #V00565-4 ^designation[0].value = "Kommentar Demenz" 
* #V00565-4 ^property[0].code = #parent 
* #V00565-4 ^property[0].valueCode = #17890 
* #300 "Hämatologie"
* #300 ^property[0].code = #parent 
* #300 ^property[0].valueCode = #1 
* #300 ^property[1].code = #child 
* #300 ^property[1].valueCode = #03010 
* #300 ^property[2].code = #child 
* #300 ^property[2].valueCode = #03020 
* #300 ^property[3].code = #child 
* #300 ^property[3].valueCode = #03030 
* #300 ^property[4].code = #child 
* #300 ^property[4].valueCode = #03050 
* #03010 "Blutbild"
* #03010 ^property[0].code = #parent 
* #03010 ^property[0].valueCode = #300 
* #03010 ^property[1].code = #child 
* #03010 ^property[1].valueCode = #V00042 
* #03010 ^property[2].code = #child 
* #03010 ^property[2].valueCode = #V00043 
* #03010 ^property[3].code = #child 
* #03010 ^property[3].valueCode = #V00197 
* #03010 ^property[4].code = #child 
* #03010 ^property[4].valueCode = #V00654-6 
* #03010 ^property[5].code = #child 
* #03010 ^property[5].valueCode = #V00655-3 
* #V00042 "Akt.Lymphoz.rel.mi."
* #V00042 ^definition = Akt.Lymphoz.rel.mi.
* #V00042 ^designation[0].language = #de-AT 
* #V00042 ^designation[0].value = "Aktivierte Lymphozyten rel. mikr." 
* #V00042 ^property[0].code = #parent 
* #V00042 ^property[0].valueCode = #03010 
* #V00043 "Akt.Lymphoz.abs.mi."
* #V00043 ^definition = Akt.Lymphoz.abs.mi.
* #V00043 ^designation[0].language = #de-AT 
* #V00043 ^designation[0].value = "Aktivierte Lymphozyten abs. mikr." 
* #V00043 ^property[0].code = #parent 
* #V00043 ^property[0].valueCode = #03010 
* #V00197 "Blutbild 37 Grad"
* #V00197 ^definition = Blutbild 37 Grad
* #V00197 ^designation[0].language = #de-AT 
* #V00197 ^designation[0].value = "Blutbild 37 Grad" 
* #V00197 ^property[0].code = #parent 
* #V00197 ^property[0].valueCode = #03010 
* #V00654-6 "Dysgranulopoese qual. mikr."
* #V00654-6 ^definition = Dysgranulop. ql.mi.
* #V00654-6 ^designation[0].language = #de-AT 
* #V00654-6 ^designation[0].value = "Dysgranulopoese qual. mikr." 
* #V00654-6 ^property[0].code = #parent 
* #V00654-6 ^property[0].valueCode = #03010 
* #V00655-3 "Dysmonopoese qual. mikr."
* #V00655-3 ^definition = Dysmonop. ql.mi.
* #V00655-3 ^designation[0].language = #de-AT 
* #V00655-3 ^designation[0].value = "Dysmonopoese qual. mikr." 
* #V00655-3 ^property[0].code = #parent 
* #V00655-3 ^property[0].valueCode = #03010 
* #03020 "Knochenmark Morphologie"
* #03020 ^property[0].code = #parent 
* #03020 ^property[0].valueCode = #300 
* #03030 "Immunphänotypisierung"
* #03030 ^property[0].code = #parent 
* #03030 ^property[0].valueCode = #300 
* #03030 ^property[1].code = #child 
* #03030 ^property[1].valueCode = #V00057 
* #03030 ^property[2].code = #child 
* #03030 ^property[2].valueCode = #V00065 
* #03030 ^property[3].code = #child 
* #03030 ^property[3].valueCode = #V00066 
* #03030 ^property[4].code = #child 
* #03030 ^property[4].valueCode = #V00067 
* #03030 ^property[5].code = #child 
* #03030 ^property[5].valueCode = #V00068 
* #03030 ^property[6].code = #child 
* #03030 ^property[6].valueCode = #V00069 
* #03030 ^property[7].code = #child 
* #03030 ^property[7].valueCode = #V00070 
* #03030 ^property[8].code = #child 
* #03030 ^property[8].valueCode = #V00071 
* #03030 ^property[9].code = #child 
* #03030 ^property[9].valueCode = #V00072 
* #03030 ^property[10].code = #child 
* #03030 ^property[10].valueCode = #V00073 
* #03030 ^property[11].code = #child 
* #03030 ^property[11].valueCode = #V00074 
* #03030 ^property[12].code = #child 
* #03030 ^property[12].valueCode = #V00075 
* #03030 ^property[13].code = #child 
* #03030 ^property[13].valueCode = #V00076 
* #03030 ^property[14].code = #child 
* #03030 ^property[14].valueCode = #V00077 
* #03030 ^property[15].code = #child 
* #03030 ^property[15].valueCode = #V00078 
* #03030 ^property[16].code = #child 
* #03030 ^property[16].valueCode = #V00079 
* #03030 ^property[17].code = #child 
* #03030 ^property[17].valueCode = #V00080 
* #03030 ^property[18].code = #child 
* #03030 ^property[18].valueCode = #V00081 
* #03030 ^property[19].code = #child 
* #03030 ^property[19].valueCode = #V00082 
* #03030 ^property[20].code = #child 
* #03030 ^property[20].valueCode = #V00083 
* #03030 ^property[21].code = #child 
* #03030 ^property[21].valueCode = #V00084 
* #03030 ^property[22].code = #child 
* #03030 ^property[22].valueCode = #V00085 
* #03030 ^property[23].code = #child 
* #03030 ^property[23].valueCode = #V00086 
* #03030 ^property[24].code = #child 
* #03030 ^property[24].valueCode = #V00087 
* #03030 ^property[25].code = #child 
* #03030 ^property[25].valueCode = #V00088 
* #03030 ^property[26].code = #child 
* #03030 ^property[26].valueCode = #V00089 
* #03030 ^property[27].code = #child 
* #03030 ^property[27].valueCode = #V00090 
* #03030 ^property[28].code = #child 
* #03030 ^property[28].valueCode = #V00091 
* #03030 ^property[29].code = #child 
* #03030 ^property[29].valueCode = #V00092 
* #03030 ^property[30].code = #child 
* #03030 ^property[30].valueCode = #V00093 
* #03030 ^property[31].code = #child 
* #03030 ^property[31].valueCode = #V00116 
* #03030 ^property[32].code = #child 
* #03030 ^property[32].valueCode = #V00117 
* #03030 ^property[33].code = #child 
* #03030 ^property[33].valueCode = #V00118 
* #03030 ^property[34].code = #child 
* #03030 ^property[34].valueCode = #V00119 
* #03030 ^property[35].code = #child 
* #03030 ^property[35].valueCode = #V00120 
* #03030 ^property[36].code = #child 
* #03030 ^property[36].valueCode = #V00201 
* #03030 ^property[37].code = #child 
* #03030 ^property[37].valueCode = #V00202 
* #03030 ^property[38].code = #child 
* #03030 ^property[38].valueCode = #V00203 
* #03030 ^property[39].code = #child 
* #03030 ^property[39].valueCode = #V00204 
* #03030 ^property[40].code = #child 
* #03030 ^property[40].valueCode = #V00205 
* #03030 ^property[41].code = #child 
* #03030 ^property[41].valueCode = #V00206 
* #03030 ^property[42].code = #child 
* #03030 ^property[42].valueCode = #V00207 
* #03030 ^property[43].code = #child 
* #03030 ^property[43].valueCode = #V00208 
* #03030 ^property[44].code = #child 
* #03030 ^property[44].valueCode = #V00209 
* #03030 ^property[45].code = #child 
* #03030 ^property[45].valueCode = #V00210 
* #03030 ^property[46].code = #child 
* #03030 ^property[46].valueCode = #V00211 
* #03030 ^property[47].code = #child 
* #03030 ^property[47].valueCode = #V00212 
* #03030 ^property[48].code = #child 
* #03030 ^property[48].valueCode = #V00213 
* #03030 ^property[49].code = #child 
* #03030 ^property[49].valueCode = #V00214 
* #03030 ^property[50].code = #child 
* #03030 ^property[50].valueCode = #V00215 
* #03030 ^property[51].code = #child 
* #03030 ^property[51].valueCode = #V00237 
* #03030 ^property[52].code = #child 
* #03030 ^property[52].valueCode = #V00238 
* #03030 ^property[53].code = #child 
* #03030 ^property[53].valueCode = #V00239 
* #03030 ^property[54].code = #child 
* #03030 ^property[54].valueCode = #V00240 
* #03030 ^property[55].code = #child 
* #03030 ^property[55].valueCode = #V00241 
* #03030 ^property[56].code = #child 
* #03030 ^property[56].valueCode = #V00242 
* #03030 ^property[57].code = #child 
* #03030 ^property[57].valueCode = #V00444-2 
* #03030 ^property[58].code = #child 
* #03030 ^property[58].valueCode = #V00445-9 
* #03030 ^property[59].code = #child 
* #03030 ^property[59].valueCode = #V00446-7 
* #03030 ^property[60].code = #child 
* #03030 ^property[60].valueCode = #V00447-5 
* #03030 ^property[61].code = #child 
* #03030 ^property[61].valueCode = #V00448-3 
* #03030 ^property[62].code = #child 
* #03030 ^property[62].valueCode = #V00449-1 
* #03030 ^property[63].code = #child 
* #03030 ^property[63].valueCode = #V00561-3 
* #03030 ^property[64].code = #child 
* #03030 ^property[64].valueCode = #V00678-5 
* #V00057 "CD64 AG-Zahl/Gran."
* #V00057 ^definition = CD64 AG-Zahl/Gran.
* #V00057 ^designation[0].language = #de-AT 
* #V00057 ^designation[0].value = "CD64 Antigenzahl /Granulozyt" 
* #V00057 ^property[0].code = #parent 
* #V00057 ^property[0].valueCode = #03030 
* #V00065 "CD64 AG-D.[Ne.Gran.]"
* #V00065 ^definition = CD64 AG-D.[Ne.Gran.]
* #V00065 ^designation[0].language = #de-AT 
* #V00065 ^designation[0].value = "CD64 Antigen-Dichte [Neutrophile Granulozyten]" 
* #V00065 ^property[0].code = #parent 
* #V00065 ^property[0].valueCode = #03030 
* #V00066 "HLA-DR AG-D.[Monoz.]"
* #V00066 ^definition = HLA-DR AG-D.[Monoz.]
* #V00066 ^designation[0].language = #de-AT 
* #V00066 ^designation[0].value = "HLA-DR Antigen-Dichte [Monozyten]" 
* #V00066 ^property[0].code = #parent 
* #V00066 ^property[0].valueCode = #03030 
* #V00067 "CD52+ Zellen abs."
* #V00067 ^definition = CD52+ Zellen abs.
* #V00067 ^designation[0].language = #de-AT 
* #V00067 ^designation[0].value = "CD52+ Zellen abs." 
* #V00067 ^property[0].code = #parent 
* #V00067 ^property[0].valueCode = #03030 
* #V00068 "CD52 AG-Ex.[path.P.]"
* #V00068 ^definition = CD52 AG-Ex.[path.P.]
* #V00068 ^designation[0].language = #de-AT 
* #V00068 ^designation[0].value = "CD52 AG-Expression [pathol. Population]" 
* #V00068 ^property[0].code = #parent 
* #V00068 ^property[0].valueCode = #03030 
* #V00069 "CD41 AG-Expr.[Thro.]"
* #V00069 ^definition = CD41 AG-Expr.[Thro.]
* #V00069 ^designation[0].language = #de-AT 
* #V00069 ^designation[0].value = "CD41 AG-Expression [Thrombozyten]" 
* #V00069 ^property[0].code = #parent 
* #V00069 ^property[0].valueCode = #03030 
* #V00070 "PAC-1+[Thrombozyten]"
* #V00070 ^definition = PAC-1+[Thrombozyten]
* #V00070 ^designation[0].language = #de-AT 
* #V00070 ^designation[0].value = "PAC-1+ [Thrombozyten]" 
* #V00070 ^property[0].code = #parent 
* #V00070 ^property[0].valueCode = #03030 
* #V00071 "CD19+23+ abs."
* #V00071 ^definition = CD19+23+ abs.
* #V00071 ^designation[0].language = #de-AT 
* #V00071 ^designation[0].value = "CD19+23+ abs." 
* #V00071 ^property[0].code = #parent 
* #V00071 ^property[0].valueCode = #03030 
* #V00072 "Klonale Popul. [Leu]"
* #V00072 ^definition = Klonale Popul. [Leu]
* #V00072 ^designation[0].language = #de-AT 
* #V00072 ^designation[0].value = "Klonale Population [Leu]" 
* #V00072 ^property[0].code = #parent 
* #V00072 ^property[0].valueCode = #03030 
* #V00073 "Klonale Popul. abs."
* #V00073 ^definition = Klonale Popul. abs.
* #V00073 ^designation[0].language = #de-AT 
* #V00073 ^designation[0].value = "Klonale Population abs." 
* #V00073 ^property[0].code = #parent 
* #V00073 ^property[0].valueCode = #03030 
* #V00074 "Klonale Pop.[Leu]/KM"
* #V00074 ^definition = Klonale Pop.[Leu]/KM
* #V00074 ^designation[0].language = #de-AT 
* #V00074 ^designation[0].value = "Klonale Population [Leu] /KM" 
* #V00074 ^property[0].code = #parent 
* #V00074 ^property[0].valueCode = #03030 
* #V00075 "Helf-T-Z.(4+)[Ly]/SM"
* #V00075 ^definition = Helf-T-Z.(4+)[Ly]/SM
* #V00075 ^designation[0].language = #de-AT 
* #V00075 ^designation[0].value = "Helfer-T-Z. (CD4+3+) [Lymphozyten] /SM" 
* #V00075 ^property[0].code = #parent 
* #V00075 ^property[0].valueCode = #03030 
* #V00076 "Zytot.T-Z(8+)[Ly]/SM"
* #V00076 ^definition = Zytot.T-Z(8+)[Ly]/SM
* #V00076 ^designation[0].language = #de-AT 
* #V00076 ^designation[0].value = "Zytotox.T-Z. [Lymphozyten] /SM" 
* #V00076 ^property[0].code = #parent 
* #V00076 ^property[0].valueCode = #03030 
* #V00077 "NK-Zellen [Ly]/SM"
* #V00077 ^definition = NK-Zellen [Ly]/SM
* #V00077 ^designation[0].language = #de-AT 
* #V00077 ^designation[0].value = "NK-Zellen (CD56.16+CD3-) [Lymphozyten] /SM" 
* #V00077 ^property[0].code = #parent 
* #V00077 ^property[0].valueCode = #03030 
* #V00078 "NK-like T-Z. [Ly]/SM"
* #V00078 ^definition = NK-like T-Z. [Ly]/SM
* #V00078 ^designation[0].language = #de-AT 
* #V00078 ^designation[0].value = "NK-like T-Zellen (CD56.16+CD3+) [Lymphozyten] /SM" 
* #V00078 ^property[0].code = #parent 
* #V00078 ^property[0].valueCode = #03030 
* #V00079 "B-Zellen(19+)[Ly]/BL"
* #V00079 ^definition = B-Zellen(19+)[Ly]/BL
* #V00079 ^designation[0].language = #de-AT 
* #V00079 ^designation[0].value = "B-Zellen (CD19+) [Lymphozyten] /BAL" 
* #V00079 ^property[0].code = #parent 
* #V00079 ^property[0].valueCode = #03030 
* #V00080 "T-Zellen(3+)[Ly]/BL"
* #V00080 ^definition = T-Zellen(3+)[Ly]/BL
* #V00080 ^designation[0].language = #de-AT 
* #V00080 ^designation[0].value = "T-Zellen (3+) [Lymphozyten] /BAL" 
* #V00080 ^property[0].code = #parent 
* #V00080 ^property[0].valueCode = #03030 
* #V00081 "Helf-T-Z.(4+)[Ly]/BL"
* #V00081 ^definition = Helf-T-Z.(4+)[Ly]/BL
* #V00081 ^designation[0].language = #de-AT 
* #V00081 ^designation[0].value = "Helfer-T-Z. (CD4+3+) [Lymphozyten] /BAL" 
* #V00081 ^property[0].code = #parent 
* #V00081 ^property[0].valueCode = #03030 
* #V00082 "Zytot.T-Z(8+)[Ly]/BL"
* #V00082 ^definition = Zytot.T-Z(8+)[Ly]/BL
* #V00082 ^designation[0].language = #de-AT 
* #V00082 ^designation[0].value = "Zytotox.T-Z. [Lymphozyten] /BAL" 
* #V00082 ^property[0].code = #parent 
* #V00082 ^property[0].valueCode = #03030 
* #V00083 "CD4/CD8-Ratio /BL"
* #V00083 ^definition = CD4/CD8-Ratio /BL
* #V00083 ^designation[0].language = #de-AT 
* #V00083 ^designation[0].value = "CD4+3+/CD8+3+-Ratio /BAL" 
* #V00083 ^property[0].code = #parent 
* #V00083 ^property[0].valueCode = #03030 
* #V00084 "akt.T-Z.(DR+)[Ly]/BL"
* #V00084 ^definition = akt.T-Z.(DR+)[Ly]/BL
* #V00084 ^designation[0].language = #de-AT 
* #V00084 ^designation[0].value = "aktiv.T-Z. (DR+CD3+) [Lymphozyten] /BAL" 
* #V00084 ^property[0].code = #parent 
* #V00084 ^property[0].valueCode = #03030 
* #V00085 "NK-Zellen [Ly]/BL"
* #V00085 ^definition = NK-Zellen [Ly]/BL
* #V00085 ^designation[0].language = #de-AT 
* #V00085 ^designation[0].value = "NK-Zellen (CD56.16+CD3-) [Lymphozyten] /BAL" 
* #V00085 ^property[0].code = #parent 
* #V00085 ^property[0].valueCode = #03030 
* #V00086 "NK-like T-Z. [Ly]/BL"
* #V00086 ^definition = NK-like T-Z. [Ly]/BL
* #V00086 ^designation[0].language = #de-AT 
* #V00086 ^designation[0].value = "NK-like T-Zellen (CD56.16+CD3+) [Lymphozyten] /BAL" 
* #V00086 ^property[0].code = #parent 
* #V00086 ^property[0].valueCode = #03030 
* #V00087 "B-Zellen(19+)abs./BL"
* #V00087 ^definition = B-Zellen(19+)abs./BL
* #V00087 ^designation[0].language = #de-AT 
* #V00087 ^designation[0].value = "B-Zellen (CD19+) abs. /BAL" 
* #V00087 ^property[0].code = #parent 
* #V00087 ^property[0].valueCode = #03030 
* #V00088 "T-Zellen(3+)abs./BL"
* #V00088 ^definition = T-Zellen(3+)abs./BL
* #V00088 ^designation[0].language = #de-AT 
* #V00088 ^designation[0].value = "T-Zellen (3+) abs. /BAL" 
* #V00088 ^property[0].code = #parent 
* #V00088 ^property[0].valueCode = #03030 
* #V00089 "Helf-T-Z.(4+)abs./BL"
* #V00089 ^definition = Helf-T-Z.(4+)abs./BL
* #V00089 ^designation[0].language = #de-AT 
* #V00089 ^designation[0].value = "Helfer-T-Z. (CD4+3+) abs. /BAL" 
* #V00089 ^property[0].code = #parent 
* #V00089 ^property[0].valueCode = #03030 
* #V00090 "Zytot.T-Z(8+)abs./BL"
* #V00090 ^definition = Zytot.T-Z(8+)abs./BL
* #V00090 ^designation[0].language = #de-AT 
* #V00090 ^designation[0].value = "Zytotox.T-Z. abs. /BAL" 
* #V00090 ^property[0].code = #parent 
* #V00090 ^property[0].valueCode = #03030 
* #V00091 "akt.T-Z.(DR+)abs./BL"
* #V00091 ^definition = akt.T-Z.(DR+)abs./BL
* #V00091 ^designation[0].language = #de-AT 
* #V00091 ^designation[0].value = "aktiv.T-Z. (DR+CD3+) abs. /BAL" 
* #V00091 ^property[0].code = #parent 
* #V00091 ^property[0].valueCode = #03030 
* #V00092 "NK-Zellen abs./BL"
* #V00092 ^definition = NK-Zellen abs./BL
* #V00092 ^designation[0].language = #de-AT 
* #V00092 ^designation[0].value = "NK-Zellen (CD56.16+CD3-) abs. /BAL" 
* #V00092 ^property[0].code = #parent 
* #V00092 ^property[0].valueCode = #03030 
* #V00093 "NK-like T-Z. abs./BL"
* #V00093 ^definition = NK-like T-Z. abs./BL
* #V00093 ^designation[0].language = #de-AT 
* #V00093 ^designation[0].value = "NK-like T-Zellen (CD56.16+CD3+) abs. /BAL" 
* #V00093 ^property[0].code = #parent 
* #V00093 ^property[0].valueCode = #03030 
* #V00116 "Flowzytometrie Blut"
* #V00116 ^definition = Flowzytometrie Blut
* #V00116 ^designation[0].language = #de-AT 
* #V00116 ^designation[0].value = "Flowzytometrie Unt./ Peripheres Blut" 
* #V00116 ^property[0].code = #parent 
* #V00116 ^property[0].valueCode = #03030 
* #V00117 "Flowzytometrie BAL"
* #V00117 ^definition = Flowzytometrie BAL
* #V00117 ^designation[0].language = #de-AT 
* #V00117 ^designation[0].value = "Flowzytometrie Unt. /Bronchoalveoläre Lavage" 
* #V00117 ^property[0].code = #parent 
* #V00117 ^property[0].valueCode = #03030 
* #V00118 "Flowzytometrie /L"
* #V00118 ^definition = Flowzytometrie /L
* #V00118 ^designation[0].language = #de-AT 
* #V00118 ^designation[0].value = "Flowzytometrie Unt. / L (Liquor)" 
* #V00118 ^property[0].code = #parent 
* #V00118 ^property[0].valueCode = #03030 
* #V00119 "Flowzytometrie KM"
* #V00119 ^definition = Flowzytometrie KM
* #V00119 ^designation[0].language = #de-AT 
* #V00119 ^designation[0].value = "Flowzytometrie Unt. / Knochenmark" 
* #V00119 ^property[0].code = #parent 
* #V00119 ^property[0].valueCode = #03030 
* #V00120 "Flowzytometrie SM"
* #V00120 ^definition = Flowzytometrie SM
* #V00120 ^designation[0].language = #de-AT 
* #V00120 ^designation[0].value = "Flowzytometrie Unt. / Sondermaterial" 
* #V00120 ^property[0].code = #parent 
* #V00120 ^property[0].valueCode = #03030 
* #V00201 "B-Zellen (19+) [Ly]"
* #V00201 ^definition = B-Zellen (19+) [Ly]
* #V00201 ^designation[0].language = #de-AT 
* #V00201 ^designation[0].value = "B-Zellen (CD19+) [Lymphozyten]" 
* #V00201 ^property[0].code = #parent 
* #V00201 ^property[0].valueCode = #03030 
* #V00202 "T-Zellen (3+) [Ly]"
* #V00202 ^definition = T-Zellen (3+) [Ly]
* #V00202 ^designation[0].language = #de-AT 
* #V00202 ^designation[0].value = "T-Zellen (3+) [Lymphozyten]" 
* #V00202 ^property[0].code = #parent 
* #V00202 ^property[0].valueCode = #03030 
* #V00203 "Helfer-T-Z.(4+) [Ly]"
* #V00203 ^definition = Helfer-T-Z.(4+) [Ly]
* #V00203 ^designation[0].language = #de-AT 
* #V00203 ^designation[0].value = "Helfer-T-Z. (CD4+3+) T-Zellen [Lymphozyten]" 
* #V00203 ^property[0].code = #parent 
* #V00203 ^property[0].valueCode = #03030 
* #V00204 "Zytotox.T-Z.(8+)[Ly]"
* #V00204 ^definition = Zytotox.T-Z.(8+)[Ly]
* #V00204 ^designation[0].language = #de-AT 
* #V00204 ^designation[0].value = "Zytotox.T-Z. (CD8+3+) T-Zellen [Lymphozyten]" 
* #V00204 ^property[0].code = #parent 
* #V00204 ^property[0].valueCode = #03030 
* #V00205 "aktiv.T-Z.(DR+)[Ly]"
* #V00205 ^definition = aktiv.T-Z.(DR+)[Ly]
* #V00205 ^designation[0].language = #de-AT 
* #V00205 ^designation[0].value = "aktiv.T-Z. (DR+CD3+) [Lymphozyten]" 
* #V00205 ^property[0].code = #parent 
* #V00205 ^property[0].valueCode = #03030 
* #V00206 "NK-Zellen [Ly]"
* #V00206 ^definition = NK-Zellen [Ly]
* #V00206 ^designation[0].language = #de-AT 
* #V00206 ^designation[0].value = "NK-Zellen (CD56.16+CD3-) [Lymphozyten]" 
* #V00206 ^property[0].code = #parent 
* #V00206 ^property[0].valueCode = #03030 
* #V00207 "NK-like T-Zellen[Ly]"
* #V00207 ^definition = NK-like T-Zellen[Ly]
* #V00207 ^designation[0].language = #de-AT 
* #V00207 ^designation[0].value = "NK-like T-Zellen (CD56.16+CD3+) [Lymphozyten]" 
* #V00207 ^property[0].code = #parent 
* #V00207 ^property[0].valueCode = #03030 
* #V00207 ^property[1].code = #status 
* #V00207 ^property[1].valueCode = #retired 
* #V00207 ^property[2].code = #Relationships 
* #V00207 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00207 ^property[3].code = #hints 
* #V00207 ^property[3].valueString = "DEPRECATED" 
* #V00208 "B-Zellen(19+)[Ly]/SM"
* #V00208 ^definition = B-Zellen(19+)[Ly]/SM
* #V00208 ^designation[0].language = #de-AT 
* #V00208 ^designation[0].value = "B-Zellen (CD19+) [Lymphozyten] /SM" 
* #V00208 ^property[0].code = #parent 
* #V00208 ^property[0].valueCode = #03030 
* #V00209 "T-Zellen(3+)[Ly]/SM"
* #V00209 ^definition = T-Zellen(3+)[Ly]/SM
* #V00209 ^designation[0].language = #de-AT 
* #V00209 ^designation[0].value = "T-Zellen (3+) [Lymphozyten] /SM" 
* #V00209 ^property[0].code = #parent 
* #V00209 ^property[0].valueCode = #03030 
* #V00210 "akt.T-Z.(DR+)[Ly]/SM"
* #V00210 ^definition = akt.T-Z.(DR+)[Ly]/SM
* #V00210 ^designation[0].language = #de-AT 
* #V00210 ^designation[0].value = "aktiv.T-Z. (DR+CD3+) [Lymphozyten] /SM" 
* #V00210 ^property[0].code = #parent 
* #V00210 ^property[0].valueCode = #03030 
* #V00211 "Stammzell.(34+)[Leu]"
* #V00211 ^definition = Stammzell.(34+)[Leu]
* #V00211 ^designation[0].language = #de-AT 
* #V00211 ^designation[0].value = "Stammzellen (CD34+) [Leu]" 
* #V00211 ^property[0].code = #parent 
* #V00211 ^property[0].valueCode = #03030 
* #V00212 "B-Zellen (19+) [Leu]"
* #V00212 ^definition = B-Zellen (19+) [Leu]
* #V00212 ^designation[0].language = #de-AT 
* #V00212 ^designation[0].value = "B-Zellen (CD19+) [Leukozyten]" 
* #V00212 ^property[0].code = #parent 
* #V00212 ^property[0].valueCode = #03030 
* #V00213 "CD20+ Zellen [Leu]"
* #V00213 ^definition = CD20+ Zellen [Leu]
* #V00213 ^designation[0].language = #de-AT 
* #V00213 ^designation[0].value = "CD20+ Zellen [Leukozyten]" 
* #V00213 ^property[0].code = #parent 
* #V00213 ^property[0].valueCode = #03030 
* #V00214 "CD52+ Zellen [Leu]"
* #V00214 ^definition = CD52+ Zellen [Leu]
* #V00214 ^designation[0].language = #de-AT 
* #V00214 ^designation[0].value = "CD52+ Zellen [Leukozyten]" 
* #V00214 ^property[0].code = #parent 
* #V00214 ^property[0].valueCode = #03030 
* #V00215 "Monozyten rel. FC"
* #V00215 ^definition = Monozyten rel. FC
* #V00215 ^designation[0].language = #de-AT 
* #V00215 ^designation[0].value = "Monozyten rel. FC" 
* #V00215 ^property[0].code = #parent 
* #V00215 ^property[0].valueCode = #03030 
* #V00237 "Monozyten abs. FC"
* #V00237 ^definition = Monozyten abs. FC
* #V00237 ^designation[0].language = #de-AT 
* #V00237 ^designation[0].value = "Monozyten abs. FC" 
* #V00237 ^property[0].code = #parent 
* #V00237 ^property[0].valueCode = #03030 
* #V00238 "CD19+5+ [BZ]"
* #V00238 ^definition = CD19+5+ [BZ]
* #V00238 ^designation[0].language = #de-AT 
* #V00238 ^designation[0].value = "CD19+5+ [B-Zellen]" 
* #V00238 ^property[0].code = #parent 
* #V00238 ^property[0].valueCode = #03030 
* #V00239 "CD19+23+ [BZ]"
* #V00239 ^definition = CD19+23+ [BZ]
* #V00239 ^designation[0].language = #de-AT 
* #V00239 ^designation[0].value = "CD19+23+ [B-Zellen]" 
* #V00239 ^property[0].code = #parent 
* #V00239 ^property[0].valueCode = #03030 
* #V00240 "CD19+kappa+ [BZ]"
* #V00240 ^definition = CD19+kappa+ [BZ]
* #V00240 ^designation[0].language = #de-AT 
* #V00240 ^designation[0].value = "CD19+kappa+ [B-Zellen]" 
* #V00240 ^property[0].code = #parent 
* #V00240 ^property[0].valueCode = #03030 
* #V00241 "CD19+lambda+ [BZ]"
* #V00241 ^definition = CD19+lambda+ [BZ]
* #V00241 ^designation[0].language = #de-AT 
* #V00241 ^designation[0].value = "CD19+lambda+ [B-Zellen]" 
* #V00241 ^property[0].code = #parent 
* #V00241 ^property[0].valueCode = #03030 
* #V00242 "K/L-Ratio [B-Zellen]"
* #V00242 ^definition = K/L-Ratio [B-Zellen]
* #V00242 ^designation[0].language = #de-AT 
* #V00242 ^designation[0].value = "Kappa/Lambda-Ratio[B-Zellen]" 
* #V00242 ^property[0].code = #parent 
* #V00242 ^property[0].valueCode = #03030 
* #V00242 ^property[1].code = #status 
* #V00242 ^property[1].valueCode = #retired 
* #V00242 ^property[2].code = #Relationships 
* #V00242 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00242 ^property[3].code = #hints 
* #V00242 ^property[3].valueString = "DEPRECATED" 
* #V00444-2 "T-Zellen(3+)[Ly] /L"
* #V00444-2 ^definition = T-Zellen(3+)[Ly] /L
* #V00444-2 ^designation[0].language = #de-AT 
* #V00444-2 ^designation[0].value = "T-Zellen (3+) [Lymphozyten] /Liquor" 
* #V00444-2 ^property[0].code = #parent 
* #V00444-2 ^property[0].valueCode = #03030 
* #V00445-9 "B-Zellen(19+)[Ly] /L"
* #V00445-9 ^definition = B-Zellen(19+)[Ly] /L
* #V00445-9 ^designation[0].language = #de-AT 
* #V00445-9 ^designation[0].value = "B-Zellen (CD19+) [Lymphozyten] /Liquor" 
* #V00445-9 ^property[0].code = #parent 
* #V00445-9 ^property[0].valueCode = #03030 
* #V00446-7 "NK-Zellen [Ly] /L"
* #V00446-7 ^definition = NK-Zellen [Ly] /L
* #V00446-7 ^designation[0].language = #de-AT 
* #V00446-7 ^designation[0].value = "NK-Zellen (CD56.16+CD3-) [Lymphozyten] /Liquor" 
* #V00446-7 ^property[0].code = #parent 
* #V00446-7 ^property[0].valueCode = #03030 
* #V00447-5 "Helf-T-Z.(4+)[Ly] /L"
* #V00447-5 ^definition = Helf-T-Z.(4+)[Ly] /L
* #V00447-5 ^designation[0].language = #de-AT 
* #V00447-5 ^designation[0].value = "Helfer-T-Z. (CD4+3+) [Lymphozyten] /Liquor" 
* #V00447-5 ^property[0].code = #parent 
* #V00447-5 ^property[0].valueCode = #03030 
* #V00447-5 ^property[1].code = #status 
* #V00447-5 ^property[1].valueCode = #retired 
* #V00447-5 ^property[2].code = #Relationships 
* #V00447-5 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00447-5 ^property[3].code = #hints 
* #V00447-5 ^property[3].valueString = "DEPRECATED" 
* #V00448-3 "Zytot.T-Z(8+)[Ly] /L"
* #V00448-3 ^definition = Zytot.T-Z(8+)[Ly] /L
* #V00448-3 ^designation[0].language = #de-AT 
* #V00448-3 ^designation[0].value = "Zytotox.T-Z. [Lymphozyten] /Liquor" 
* #V00448-3 ^property[0].code = #parent 
* #V00448-3 ^property[0].valueCode = #03030 
* #V00448-3 ^property[1].code = #status 
* #V00448-3 ^property[1].valueCode = #retired 
* #V00448-3 ^property[2].code = #Relationships 
* #V00448-3 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00448-3 ^property[3].code = #hints 
* #V00448-3 ^property[3].valueString = "DEPRECATED" 
* #V00449-1 "CD4/CD8-Ratio /L"
* #V00449-1 ^definition = CD4/CD8-Ratio /L
* #V00449-1 ^designation[0].language = #de-AT 
* #V00449-1 ^designation[0].value = "CD4+3+/CD8+3+-Ratio /Liquor" 
* #V00449-1 ^property[0].code = #parent 
* #V00449-1 ^property[0].valueCode = #03030 
* #V00561-3 "Neutrophile Chemotaktische Aktivität"
* #V00561-3 ^definition = Neutroph. Chemotaxis
* #V00561-3 ^designation[0].language = #de-AT 
* #V00561-3 ^designation[0].value = "Neutrophile Chemotaktische Aktivität" 
* #V00561-3 ^property[0].code = #parent 
* #V00561-3 ^property[0].valueCode = #03030 
* #V00678-5 "NK-like T-Zellen[Ly] /KM"
* #V00678-5 ^definition = NK-like T-Zellen /KM
* #V00678-5 ^designation[0].language = #de-AT 
* #V00678-5 ^designation[0].value = "NK-like T-Zellen (CD56.16+CD3+) [Lymphozyten] /KM" 
* #V00678-5 ^property[0].code = #parent 
* #V00678-5 ^property[0].valueCode = #03030 
* #V00678-5 ^property[1].code = #status 
* #V00678-5 ^property[1].valueCode = #retired 
* #V00678-5 ^property[2].code = #Relationships 
* #V00678-5 ^property[2].valueString = "verwendbar bis 31.12.2019" 
* #V00678-5 ^property[3].code = #hints 
* #V00678-5 ^property[3].valueString = "DEPRECATED" 
* #03050 "Hämatologie Sonstiges"
* #03050 ^property[0].code = #parent 
* #03050 ^property[0].valueCode = #300 
* #03050 ^property[1].code = #child 
* #03050 ^property[1].valueCode = #V00137 
* #03050 ^property[2].code = #child 
* #03050 ^property[2].valueCode = #V00138 
* #03050 ^property[3].code = #child 
* #03050 ^property[3].valueCode = #V00139 
* #03050 ^property[4].code = #child 
* #03050 ^property[4].valueCode = #V00457-4 
* #03050 ^property[5].code = #child 
* #03050 ^property[5].valueCode = #V00526-6 
* #03050 ^property[6].code = #child 
* #03050 ^property[6].valueCode = #V00527-4 
* #03050 ^property[7].code = #child 
* #03050 ^property[7].valueCode = #V00684-3 
* #03050 ^property[8].code = #child 
* #03050 ^property[8].valueCode = #V00685-0 
* #03050 ^property[9].code = #child 
* #03050 ^property[9].valueCode = #V00686-8 
* #03050 ^property[10].code = #child 
* #03050 ^property[10].valueCode = #V00687-6 
* #03050 ^property[11].code = #child 
* #03050 ^property[11].valueCode = #V00689-2 
* #V00137 "Unreife Gran.abs./SM"
* #V00137 ^definition = Unreife Gran.abs./SM
* #V00137 ^designation[0].language = #de-AT 
* #V00137 ^designation[0].value = "Unreife Granulozyten abs. /Sondermaterial" 
* #V00137 ^property[0].code = #parent 
* #V00137 ^property[0].valueCode = #03050 
* #V00138 "Unreife Gran.rel./SM"
* #V00138 ^definition = Unreife Gran.rel./SM
* #V00138 ^designation[0].language = #de-AT 
* #V00138 ^designation[0].value = "Unreife Granulozyten rel.  /Sondermaterial" 
* #V00138 ^property[0].code = #parent 
* #V00138 ^property[0].valueCode = #03050 
* #V00139 "Hämoglobin /PD"
* #V00139 ^definition = Hämoglobin /PD
* #V00139 ^designation[0].language = #de-AT 
* #V00139 ^designation[0].value = "Hämoglobin /Peritonealdialysat" 
* #V00139 ^property[0].code = #parent 
* #V00139 ^property[0].valueCode = #03050 
* #V00457-4 "Granulozyten rel./PG"
* #V00457-4 ^definition = Granulozyten rel./PG
* #V00457-4 ^designation[0].language = #de-AT 
* #V00457-4 ^designation[0].value = "Granulozyten rel. /Gelenkspunktat" 
* #V00457-4 ^property[0].code = #parent 
* #V00457-4 ^property[0].valueCode = #03050 
* #V00526-6 "Thrombozyten /PD"
* #V00526-6 ^definition = Thrombozyten /PD
* #V00526-6 ^designation[0].language = #de-AT 
* #V00526-6 ^designation[0].value = "Thrombozyten /Peritonealdialysat" 
* #V00526-6 ^property[0].code = #parent 
* #V00526-6 ^property[0].valueCode = #03050 
* #V00527-4 "Hämatokrit /PD"
* #V00527-4 ^definition = Hämatokrit /PD
* #V00527-4 ^designation[0].language = #de-AT 
* #V00527-4 ^designation[0].value = "Hämatokrit /Peritonealdialysat" 
* #V00527-4 ^property[0].code = #parent 
* #V00527-4 ^property[0].valueCode = #03050 
* #V00684-3 "Granulozyten mikr. /SM"
* #V00684-3 ^definition = Granulozyten m. /SM
* #V00684-3 ^designation[0].language = #de-AT 
* #V00684-3 ^designation[0].value = "Granulozyten mikr. /Sondermaterial" 
* #V00684-3 ^property[0].code = #parent 
* #V00684-3 ^property[0].valueCode = #03050 
* #V00685-0 "Leukozyten mikr. /SM"
* #V00685-0 ^definition = Leukozyten m. /SM
* #V00685-0 ^designation[0].language = #de-AT 
* #V00685-0 ^designation[0].value = "Leukozyten mikr. /Sondermaterial" 
* #V00685-0 ^property[0].code = #parent 
* #V00685-0 ^property[0].valueCode = #03050 
* #V00686-8 "Lymphozyten mikr. /SM"
* #V00686-8 ^definition = Lymphozyten m. /SM
* #V00686-8 ^designation[0].language = #de-AT 
* #V00686-8 ^designation[0].value = "Lymphozyten mikr. /Sondermaterial" 
* #V00686-8 ^property[0].code = #parent 
* #V00686-8 ^property[0].valueCode = #03050 
* #V00687-6 "Erythrozyten mikr. /SM"
* #V00687-6 ^definition = Erythrozyten m. /SM
* #V00687-6 ^designation[0].language = #de-AT 
* #V00687-6 ^designation[0].value = "Erythrozyten mikr. /Sondermaterial" 
* #V00687-6 ^property[0].code = #parent 
* #V00687-6 ^property[0].valueCode = #03050 
* #V00689-2 "Entzündungszellen mikr. /SM"
* #V00689-2 ^definition = Entzündungsz. m. /SM
* #V00689-2 ^designation[0].language = #de-AT 
* #V00689-2 ^designation[0].value = "Entzündungszellen mikr. /Sondermaterial" 
* #V00689-2 ^property[0].code = #parent 
* #V00689-2 ^property[0].valueCode = #03050 
* #400 "Gerinnung/Hämostaseologie"
* #400 ^property[0].code = #parent 
* #400 ^property[0].valueCode = #1 
* #400 ^property[1].code = #child 
* #400 ^property[1].valueCode = #04140 
* #400 ^property[2].code = #child 
* #400 ^property[2].valueCode = #04150 
* #400 ^property[3].code = #child 
* #400 ^property[3].valueCode = #04160 
* #400 ^property[4].code = #child 
* #400 ^property[4].valueCode = #04170 
* #04140 "Hämostaseologie Globaltests"
* #04140 ^property[0].code = #parent 
* #04140 ^property[0].valueCode = #400 
* #04140 ^property[1].code = #child 
* #04140 ^property[1].valueCode = #V00019 
* #04140 ^property[2].code = #child 
* #04140 ^property[2].valueCode = #V00041 
* #04140 ^property[3].code = #child 
* #04140 ^property[3].valueCode = #V00051 
* #04140 ^property[4].code = #child 
* #04140 ^property[4].valueCode = #V00052 
* #04140 ^property[5].code = #child 
* #04140 ^property[5].valueCode = #V00055 
* #04140 ^property[6].code = #child 
* #04140 ^property[6].valueCode = #V00233 
* #04140 ^property[7].code = #child 
* #04140 ^property[7].valueCode = #V00243 
* #04140 ^property[8].code = #child 
* #04140 ^property[8].valueCode = #V00247 
* #04140 ^property[9].code = #child 
* #04140 ^property[9].valueCode = #V00467-3 
* #04140 ^property[10].code = #child 
* #04140 ^property[10].valueCode = #V00656-1 
* #04140 ^property[11].code = #child 
* #04140 ^property[11].valueCode = #V00657-9 
* #04140 ^property[12].code = #child 
* #04140 ^property[12].valueCode = #V00658-7 
* #04140 ^property[13].code = #child 
* #04140 ^property[13].valueCode = #V00675-1 
* #04140 ^property[14].code = #child 
* #04140 ^property[14].valueCode = #V00709-8 
* #04140 ^property[15].code = #child 
* #04140 ^property[15].valueCode = #V00766-8 
* #04140 ^property[16].code = #child 
* #04140 ^property[16].valueCode = #V00767-6 
* #V00019 "INR High"
* #V00019 ^definition = INR High
* #V00019 ^designation[0].language = #de-AT 
* #V00019 ^designation[0].value = "INR High" 
* #V00019 ^property[0].code = #parent 
* #V00019 ^property[0].valueCode = #04140 
* #V00041 "Blutgerinn.Bef.int."
* #V00041 ^definition = Blutgerinn.Bef.int.
* #V00041 ^designation[0].language = #de-AT 
* #V00041 ^designation[0].value = "Blutgerinnung Befundinterpretation" 
* #V00041 ^property[0].code = #parent 
* #V00041 ^property[0].valueCode = #04140 
* #V00051 "VASP-Inhib.ADP-med."
* #V00051 ^definition = VASP-Inhib.ADP-med.
* #V00051 ^designation[0].language = #de-AT 
* #V00051 ^designation[0].value = "VASP-Inhibition ADP-mediiert" 
* #V00051 ^property[0].code = #parent 
* #V00051 ^property[0].valueCode = #04140 
* #V00052 "Thr.aggr.TRAP-ind./B"
* #V00052 ^definition = Thr.aggr.TRAP-ind./B
* #V00052 ^designation[0].language = #de-AT 
* #V00052 ^designation[0].value = "Thrombozytenaggregation TRAP-ind./Blut" 
* #V00052 ^property[0].code = #parent 
* #V00052 ^property[0].valueCode = #04140 
* #V00052 ^property[1].code = #status 
* #V00052 ^property[1].valueCode = #retired 
* #V00052 ^property[2].code = #Relationships 
* #V00052 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00052 ^property[3].code = #hints 
* #V00052 ^property[3].valueString = "DEPRECATED" 
* #V00055 "Thr.aggr.Ristoc./B"
* #V00055 ^definition = Thr.aggr.Ristoc./B
* #V00055 ^designation[0].language = #de-AT 
* #V00055 ^designation[0].value = "Thrombozytenaggregation Ristocetin-ind./Blut" 
* #V00055 ^property[0].code = #parent 
* #V00055 ^property[0].valueCode = #04140 
* #V00055 ^property[1].code = #status 
* #V00055 ^property[1].valueCode = #retired 
* #V00055 ^property[2].code = #Relationships 
* #V00055 ^property[2].valueString = "verwendbar bis 30.06.2018" 
* #V00055 ^property[3].code = #hints 
* #V00055 ^property[3].valueString = "DEPRECATED" 
* #V00233 "PTZ n. Owren INR"
* #V00233 ^definition = PTZ n. Owren INR
* #V00233 ^designation[0].language = #de-AT 
* #V00233 ^designation[0].value = "PTZ nach Owren INR" 
* #V00233 ^property[0].code = #parent 
* #V00233 ^property[0].valueCode = #04140 
* #V00243 "Fondaparinux (a-Fxa Akt.)"
* #V00243 ^definition = Fondapar.(a-Fxa Ak)
* #V00243 ^designation[0].language = #de-AT 
* #V00243 ^designation[0].value = "Fondaparinux (Arixtra) Anti FaktorFa Aktivität" 
* #V00243 ^property[0].code = #parent 
* #V00243 ^property[0].valueCode = #04140 
* #V00247 "PFA 100 / P2Y"
* #V00247 ^definition = PFA 100 / P2Y
* #V00247 ^designation[0].language = #de-AT 
* #V00247 ^designation[0].value = "PFA 100 / P2Y Rezeptor (Clopidogrel Resistenz)" 
* #V00247 ^property[0].code = #parent 
* #V00247 ^property[0].valueCode = #04140 
* #V00467-3 "D-Dimer ql./B"
* #V00467-3 ^definition = D-Dimer ql./B
* #V00467-3 ^designation[0].language = #de-AT 
* #V00467-3 ^designation[0].value = "D-Dimer qalitativ/B" 
* #V00467-3 ^property[0].code = #parent 
* #V00467-3 ^property[0].valueCode = #04140 
* #V00467-3 ^property[1].code = #status 
* #V00467-3 ^property[1].valueCode = #retired 
* #V00467-3 ^property[2].code = #Relationships 
* #V00467-3 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00467-3 ^property[3].code = #hints 
* #V00467-3 ^property[3].valueString = "DEPRECATED" 
* #V00656-1 "AT III Aktivität progressiv"
* #V00656-1 ^definition = AT III Akt. progr.
* #V00656-1 ^designation[0].language = #de-AT 
* #V00656-1 ^designation[0].value = "AT III Aktivität progressiv" 
* #V00656-1 ^property[0].code = #parent 
* #V00656-1 ^property[0].valueCode = #04140 
* #V00656-1 ^property[1].code = #status 
* #V00656-1 ^property[1].valueCode = #retired 
* #V00656-1 ^property[2].code = #Relationships 
* #V00656-1 ^property[2].valueString = "verwendbar bis 30.06.2019" 
* #V00656-1 ^property[3].code = #hints 
* #V00656-1 ^property[3].valueString = "DEPRECATED" 
* #V00657-9 "Heparin-PF4-induzierte IgG AK ql."
* #V00657-9 ^definition = Hep-PF4-i. IgG AK ql
* #V00657-9 ^designation[0].language = #de-AT 
* #V00657-9 ^designation[0].value = "Heparin-PF4-induzierte IgG Antikörper qualitativ" 
* #V00657-9 ^property[0].code = #parent 
* #V00657-9 ^property[0].valueCode = #04140 
* #V00657-9 ^property[1].code = #status 
* #V00657-9 ^property[1].valueCode = #retired 
* #V00657-9 ^property[2].code = #Relationships 
* #V00657-9 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00657-9 ^property[3].code = #hints 
* #V00657-9 ^property[3].valueString = "DEPRECATED" 
* #V00658-7 "Heparin-PF4-induzierte IgG AK"
* #V00658-7 ^definition = Hep.-PF4-ind. IgG AK
* #V00658-7 ^designation[0].language = #de-AT 
* #V00658-7 ^designation[0].value = "Heparin-PF4-induzierte IgG Antikörper" 
* #V00658-7 ^property[0].code = #parent 
* #V00658-7 ^property[0].valueCode = #04140 
* #V00658-7 ^property[1].code = #status 
* #V00658-7 ^property[1].valueCode = #retired 
* #V00658-7 ^property[2].code = #Relationships 
* #V00658-7 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00658-7 ^property[3].code = #hints 
* #V00658-7 ^property[3].valueString = "DEPRECATED" 
* #V00675-1 "PTZ n. Owren"
* #V00675-1 ^definition = PTZ n. Owren
* #V00675-1 ^designation[0].language = #de-AT 
* #V00675-1 ^designation[0].value = "PTZ nach Owren" 
* #V00675-1 ^property[0].code = #parent 
* #V00675-1 ^property[0].valueCode = #04140 
* #V00675-1 ^property[1].code = #status 
* #V00675-1 ^property[1].valueCode = #retired 
* #V00675-1 ^property[2].code = #Relationships 
* #V00675-1 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00675-1 ^property[3].code = #hints 
* #V00675-1 ^property[3].valueString = "DEPRECATED" 
* #V00709-8 "Edoxaban (aFXa Akt.)"
* #V00709-8 ^definition = Edoxaban (aFXa Akt.)
* #V00709-8 ^designation[0].language = #de-AT 
* #V00709-8 ^designation[0].value = "Edoxaban Anti Faktor Fxa Aktivität" 
* #V00709-8 ^property[0].code = #parent 
* #V00709-8 ^property[0].valueCode = #04140 
* #V00709-8 ^property[1].code = #status 
* #V00709-8 ^property[1].valueCode = #retired 
* #V00709-8 ^property[2].code = #Relationships 
* #V00709-8 ^property[2].valueString = "verwendbar bis 31.12.2020" 
* #V00709-8 ^property[3].code = #hints 
* #V00709-8 ^property[3].valueString = "DEPRECATED" 
* #V00766-8 "Clotting Time FIBTEM"
* #V00766-8 ^definition = Clotting Time FIBTEM
* #V00766-8 ^designation[0].language = #de-AT 
* #V00766-8 ^designation[0].value = "Clotting Time FIBTEM" 
* #V00766-8 ^property[0].code = #parent 
* #V00766-8 ^property[0].valueCode = #04140 
* #V00766-8 ^property[1].code = #status 
* #V00766-8 ^property[1].valueCode = #retired 
* #V00766-8 ^property[2].code = #Relationships 
* #V00766-8 ^property[2].valueString = "verwendbar bis 31.12.2022" 
* #V00766-8 ^property[3].code = #hints 
* #V00766-8 ^property[3].valueString = "DEPRECATED" 
* #V00767-6 "Clot Formation Time FIBTEM"
* #V00767-6 ^definition = Clot Form. T. FIBTEM
* #V00767-6 ^designation[0].language = #de-AT 
* #V00767-6 ^designation[0].value = "Clot Formation Time FIBTEM" 
* #V00767-6 ^property[0].code = #parent 
* #V00767-6 ^property[0].valueCode = #04140 
* #V00767-6 ^property[1].code = #status 
* #V00767-6 ^property[1].valueCode = #retired 
* #V00767-6 ^property[2].code = #Relationships 
* #V00767-6 ^property[2].valueString = "verwendbar bis 31.12.2022" 
* #V00767-6 ^property[3].code = #hints 
* #V00767-6 ^property[3].valueString = "DEPRECATED" 
* #04150 "Einzelfaktoranalysen"
* #04150 ^property[0].code = #parent 
* #04150 ^property[0].valueCode = #400 
* #04150 ^property[1].code = #child 
* #04150 ^property[1].valueCode = #V00006 
* #04150 ^property[2].code = #child 
* #04150 ^property[2].valueCode = #V00016 
* #04150 ^property[3].code = #child 
* #04150 ^property[3].valueCode = #V00636-3 
* #04150 ^property[4].code = #child 
* #04150 ^property[4].valueCode = #V00659-5 
* #04150 ^property[5].code = #child 
* #04150 ^property[5].valueCode = #V00660-3 
* #04150 ^property[6].code = #child 
* #04150 ^property[6].valueCode = #V00661-1 
* #04150 ^property[7].code = #child 
* #04150 ^property[7].valueCode = #V00662-9 
* #04150 ^property[8].code = #child 
* #04150 ^property[8].valueCode = #V00663-7 
* #04150 ^property[9].code = #child 
* #04150 ^property[9].valueCode = #V00664-5 
* #V00006 "FXIII Akt.Clot-Lysis"
* #V00006 ^definition = FXIII Akt.Clot-Lysis
* #V00006 ^designation[0].language = #de-AT 
* #V00006 ^designation[0].value = "Faktor XIII Aktivität (Clot-Lysis)" 
* #V00006 ^property[0].code = #parent 
* #V00006 ^property[0].valueCode = #04150 
* #V00016 "vWF Aktiv. (GPIb-R)"
* #V00016 ^definition = vWF Aktiv. (GPIb-R)
* #V00016 ^designation[0].language = #de-AT 
* #V00016 ^designation[0].value = "von Willebrand Faktor Aktivität (GPIb-R)" 
* #V00016 ^property[0].code = #parent 
* #V00016 ^property[0].valueCode = #04150 
* #V00636-3 "Faktor IX Akt. chr"
* #V00636-3 ^definition = Faktor IX Akt. chr.
* #V00636-3 ^designation[0].language = #de-AT 
* #V00636-3 ^designation[0].value = "Faktor IX (Antihaemophiler Faktor B) Akt. chr." 
* #V00636-3 ^property[0].code = #parent 
* #V00636-3 ^property[0].valueCode = #04150 
* #V00636-3 ^property[1].code = #status 
* #V00636-3 ^property[1].valueCode = #retired 
* #V00636-3 ^property[2].code = #Relationships 
* #V00636-3 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00636-3 ^property[3].code = #hints 
* #V00636-3 ^property[3].valueString = "DEPRECATED" 
* #V00659-5 "vWF-Ristocetin Kofaktor Aktivität"
* #V00659-5 ^definition = vWF-Ristoc.Kof.Akt.
* #V00659-5 ^designation[0].language = #de-AT 
* #V00659-5 ^designation[0].value = "vWF-Ristocetin Kofaktor Aktivität" 
* #V00659-5 ^property[0].code = #parent 
* #V00659-5 ^property[0].valueCode = #04150 
* #V00659-5 ^property[1].code = #status 
* #V00659-5 ^property[1].valueCode = #retired 
* #V00659-5 ^property[2].code = #Relationships 
* #V00659-5 ^property[2].valueString = "verwendbar bis 30.06.2019" 
* #V00659-5 ^property[3].code = #hints 
* #V00659-5 ^property[3].valueString = "DEPRECATED" 
* #V00660-3 "vWF:RiCoF Inhibitor"
* #V00660-3 ^definition = vWF:RiCoF Inhibitor
* #V00660-3 ^designation[0].language = #de-AT 
* #V00660-3 ^designation[0].value = "vWF:RiCoF Inhibitor" 
* #V00660-3 ^property[0].code = #parent 
* #V00660-3 ^property[0].valueCode = #04150 
* #V00660-3 ^property[1].code = #status 
* #V00660-3 ^property[1].valueCode = #retired 
* #V00660-3 ^property[2].code = #Relationships 
* #V00660-3 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00660-3 ^property[3].code = #hints 
* #V00660-3 ^property[3].valueString = "DEPRECATED" 
* #V00661-1 "vWF:CBA Inhibitor"
* #V00661-1 ^definition = vWF:CBA Inhibitor
* #V00661-1 ^designation[0].language = #de-AT 
* #V00661-1 ^designation[0].value = "vWF:CBA Inhibitor" 
* #V00661-1 ^property[0].code = #parent 
* #V00661-1 ^property[0].valueCode = #04150 
* #V00661-1 ^property[1].code = #status 
* #V00661-1 ^property[1].valueCode = #retired 
* #V00661-1 ^property[2].code = #Relationships 
* #V00661-1 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00661-1 ^property[3].code = #hints 
* #V00661-1 ^property[3].valueString = "DEPRECATED" 
* #V00662-9 "Plasmatauschversuch aPTT 50:50"
* #V00662-9 ^definition = PTV aPTT 50:50
* #V00662-9 ^designation[0].language = #de-AT 
* #V00662-9 ^designation[0].value = "Plasmatauschversuch aPTT 50:50" 
* #V00662-9 ^property[0].code = #parent 
* #V00662-9 ^property[0].valueCode = #04150 
* #V00662-9 ^property[1].code = #status 
* #V00662-9 ^property[1].valueCode = #retired 
* #V00662-9 ^property[2].code = #Relationships 
* #V00662-9 ^property[2].valueString = "verwendbar bis 30.06.2019" 
* #V00662-9 ^property[3].code = #hints 
* #V00662-9 ^property[3].valueString = "DEPRECATED" 
* #V00663-7 "Plasmatauschversuch aPTT-Fakt.-sens.Normalpool"
* #V00663-7 ^definition = PTV aPTT-Fakt.-s.Np.
* #V00663-7 ^designation[0].language = #de-AT 
* #V00663-7 ^designation[0].value = "Plasmatauschversuch  aPTT-Fakt.-sens.Normalpool" 
* #V00663-7 ^property[0].code = #parent 
* #V00663-7 ^property[0].valueCode = #04150 
* #V00664-5 "Plasmatauschversuch aPTT-Fakt.-sens. 50:50"
* #V00664-5 ^definition = PTV aPTT-Fakt.-s.1:1
* #V00664-5 ^designation[0].language = #de-AT 
* #V00664-5 ^designation[0].value = "Plasmatauschversuch  aPTT-Fakt.-sens. 50:50" 
* #V00664-5 ^property[0].code = #parent 
* #V00664-5 ^property[0].valueCode = #04150 
* #V00664-5 ^property[1].code = #status 
* #V00664-5 ^property[1].valueCode = #retired 
* #V00664-5 ^property[2].code = #Relationships 
* #V00664-5 ^property[2].valueString = "verwendbar bis 30.06.2019" 
* #V00664-5 ^property[3].code = #hints 
* #V00664-5 ^property[3].valueString = "DEPRECATED" 
* #04160 "Thrombophilie Tests"
* #04160 ^property[0].code = #parent 
* #04160 ^property[0].valueCode = #400 
* #04160 ^property[1].code = #child 
* #04160 ^property[1].valueCode = #V00050 
* #04160 ^property[2].code = #child 
* #04160 ^property[2].valueCode = #V00095 
* #04160 ^property[3].code = #child 
* #04160 ^property[3].valueCode = #V00096 
* #04160 ^property[4].code = #child 
* #04160 ^property[4].valueCode = #V00121 
* #04160 ^property[5].code = #child 
* #04160 ^property[5].valueCode = #V00122 
* #04160 ^property[6].code = #child 
* #04160 ^property[6].valueCode = #V00123 
* #04160 ^property[7].code = #child 
* #04160 ^property[7].valueCode = #V00124 
* #04160 ^property[8].code = #child 
* #04160 ^property[8].valueCode = #V00292 
* #V00050 "Prot.C Pathway DRVVT"
* #V00050 ^definition = Prot.C Pathway DRVVT
* #V00050 ^designation[0].language = #de-AT 
* #V00050 ^designation[0].value = "Protein C Pathway DRVVT" 
* #V00050 ^property[0].code = #parent 
* #V00050 ^property[0].valueCode = #04160 
* #V00095 "LA-Bestät. dRVVT /NP"
* #V00095 ^definition = LA-Bestät. dRVVT /NP
* #V00095 ^designation[0].language = #de-AT 
* #V00095 ^designation[0].value = "Lupus-Anticoagulans Bestät. dRVVT /Normalplasma" 
* #V00095 ^property[0].code = #parent 
* #V00095 ^property[0].valueCode = #04160 
* #V00096 "LA-Best.aPTT Lupus-s"
* #V00096 ^definition = LA-Best.aPTT Lupus-s
* #V00096 ^designation[0].language = #de-AT 
* #V00096 ^designation[0].value = "Lupus-Anticoagulans Bestätigungstest aPTT Lupus-s." 
* #V00096 ^property[0].code = #parent 
* #V00096 ^property[0].valueCode = #04160 
* #V00121 "LA Bst.dRVVT-R.1+2NP"
* #V00121 ^definition = LA Bst.dRVVT-R.1+2NP
* #V00121 ^designation[0].language = #de-AT 
* #V00121 ^designation[0].value = "Lupus-Anticoag. Bestät. dRVVT-Rto 1+2 Normalplasma" 
* #V00121 ^property[0].code = #parent 
* #V00121 ^property[0].valueCode = #04160 
* #V00122 "LA Best.dRVVT 1+2 NP"
* #V00122 ^definition = LA Best.dRVVT 1+2 NP
* #V00122 ^designation[0].language = #de-AT 
* #V00122 ^designation[0].value = "Lupus-Anticoagulans Bestät. dRVVT 1+2 Normalplasma" 
* #V00122 ^property[0].code = #parent 
* #V00122 ^property[0].valueCode = #04160 
* #V00123 "LA Bst.dRVVT-R.1+1NP"
* #V00123 ^definition = LA Bst.dRVVT-R.1+1NP
* #V00123 ^designation[0].language = #de-AT 
* #V00123 ^designation[0].value = "Lupus-Anticoag. Bestät. dRVVT-Rto 1+1 Normalplasma" 
* #V00123 ^property[0].code = #parent 
* #V00123 ^property[0].valueCode = #04160 
* #V00124 "APS assoz. AK"
* #V00124 ^definition = APS assoz. AK
* #V00124 ^designation[0].language = #de-AT 
* #V00124 ^designation[0].value = "Verdacht auf Antiphospholipidsyndrom" 
* #V00124 ^property[0].code = #parent 
* #V00124 ^property[0].valueCode = #04160 
* #V00292 "aPTT FSL"
* #V00292 ^definition = aPTT FSL
* #V00292 ^designation[0].language = #de-AT 
* #V00292 ^designation[0].value = "aPTT Faktor & Lupus-sensitiv" 
* #V00292 ^property[0].code = #parent 
* #V00292 ^property[0].valueCode = #04160 
* #04170 "Gerinnung Sonstiges"
* #04170 ^property[0].code = #parent 
* #04170 ^property[0].valueCode = #400 
* #500 "Klinische Chemie/Proteindiagnostik"
* #500 ^property[0].code = #parent 
* #500 ^property[0].valueCode = #1 
* #500 ^property[1].code = #child 
* #500 ^property[1].valueCode = #05180 
* #500 ^property[2].code = #child 
* #500 ^property[2].valueCode = #05190 
* #500 ^property[3].code = #child 
* #500 ^property[3].valueCode = #05200 
* #500 ^property[4].code = #child 
* #500 ^property[4].valueCode = #05210 
* #500 ^property[5].code = #child 
* #500 ^property[5].valueCode = #05220 
* #05180 "Klinische Chemie"
* #05180 ^property[0].code = #parent 
* #05180 ^property[0].valueCode = #500 
* #05180 ^property[1].code = #child 
* #05180 ^property[1].valueCode = #V00007 
* #05180 ^property[2].code = #child 
* #05180 ^property[2].valueCode = #V00056 
* #05180 ^property[3].code = #child 
* #05180 ^property[3].valueCode = #V00125 
* #05180 ^property[4].code = #child 
* #05180 ^property[4].valueCode = #V00173 
* #05180 ^property[5].code = #child 
* #05180 ^property[5].valueCode = #V00174 
* #05180 ^property[6].code = #child 
* #05180 ^property[6].valueCode = #V00175 
* #05180 ^property[7].code = #child 
* #05180 ^property[7].valueCode = #V00176 
* #05180 ^property[8].code = #child 
* #05180 ^property[8].valueCode = #V00177 
* #05180 ^property[9].code = #child 
* #05180 ^property[9].valueCode = #V00178 
* #05180 ^property[10].code = #child 
* #05180 ^property[10].valueCode = #V00179 
* #05180 ^property[11].code = #child 
* #05180 ^property[11].valueCode = #V00180 
* #05180 ^property[12].code = #child 
* #05180 ^property[12].valueCode = #V00181 
* #05180 ^property[13].code = #child 
* #05180 ^property[13].valueCode = #V00183 
* #05180 ^property[14].code = #child 
* #05180 ^property[14].valueCode = #V00184 
* #05180 ^property[15].code = #child 
* #05180 ^property[15].valueCode = #V00185 
* #05180 ^property[16].code = #child 
* #05180 ^property[16].valueCode = #V00186 
* #05180 ^property[17].code = #child 
* #05180 ^property[17].valueCode = #V00187 
* #05180 ^property[18].code = #child 
* #05180 ^property[18].valueCode = #V00188 
* #05180 ^property[19].code = #child 
* #05180 ^property[19].valueCode = #V00189 
* #05180 ^property[20].code = #child 
* #05180 ^property[20].valueCode = #V00190 
* #05180 ^property[21].code = #child 
* #05180 ^property[21].valueCode = #V00191 
* #05180 ^property[22].code = #child 
* #05180 ^property[22].valueCode = #V00192 
* #05180 ^property[23].code = #child 
* #05180 ^property[23].valueCode = #V00193 
* #05180 ^property[24].code = #child 
* #05180 ^property[24].valueCode = #V00194 
* #05180 ^property[25].code = #child 
* #05180 ^property[25].valueCode = #V00195 
* #05180 ^property[26].code = #child 
* #05180 ^property[26].valueCode = #V00196 
* #05180 ^property[27].code = #child 
* #05180 ^property[27].valueCode = #V00220 
* #05180 ^property[28].code = #child 
* #05180 ^property[28].valueCode = #V00221 
* #05180 ^property[29].code = #child 
* #05180 ^property[29].valueCode = #V00267 
* #05180 ^property[30].code = #child 
* #05180 ^property[30].valueCode = #V00268 
* #05180 ^property[31].code = #child 
* #05180 ^property[31].valueCode = #V00461-6 
* #05180 ^property[32].code = #child 
* #05180 ^property[32].valueCode = #V00579-5 
* #05180 ^property[33].code = #child 
* #05180 ^property[33].valueCode = #V00610-8 
* #05180 ^property[34].code = #child 
* #05180 ^property[34].valueCode = #V00611-6 
* #05180 ^property[35].code = #child 
* #05180 ^property[35].valueCode = #V00639-7 
* #05180 ^property[36].code = #child 
* #05180 ^property[36].valueCode = #V00640-5 
* #05180 ^property[37].code = #child 
* #05180 ^property[37].valueCode = #V00641-3 
* #05180 ^property[38].code = #child 
* #05180 ^property[38].valueCode = #V00642-1 
* #05180 ^property[39].code = #child 
* #05180 ^property[39].valueCode = #V00643-9 
* #05180 ^property[40].code = #child 
* #05180 ^property[40].valueCode = #V00644-7 
* #05180 ^property[41].code = #child 
* #05180 ^property[41].valueCode = #V00645-4 
* #05180 ^property[42].code = #child 
* #05180 ^property[42].valueCode = #V00646-2 
* #05180 ^property[43].code = #child 
* #05180 ^property[43].valueCode = #V00647-0 
* #05180 ^property[44].code = #child 
* #05180 ^property[44].valueCode = #V00729-6 
* #05180 ^property[45].code = #child 
* #05180 ^property[45].valueCode = #V00769-2 
* #V00007 "Blutsenkung Interpr."
* #V00007 ^definition = Blutsenkung Interpr.
* #V00007 ^designation[0].language = #de-AT 
* #V00007 ^designation[0].value = "Blutsenkung Befundinterpretation" 
* #V00007 ^property[0].code = #parent 
* #V00007 ^property[0].valueCode = #05180 
* #V00056 "Lösl.Trf-R/Ferr.-Rto"
* #V00056 ^definition = Lösl.Trf-R/Ferr.-Rto
* #V00056 ^designation[0].language = #de-AT 
* #V00056 ^designation[0].value = "Löslicher Transferrin-Rezeptor / Ferritin-Ratio" 
* #V00056 ^property[0].code = #parent 
* #V00056 ^property[0].valueCode = #05180 
* #V00056 ^property[1].code = #status 
* #V00056 ^property[1].valueCode = #retired 
* #V00056 ^property[2].code = #Relationships 
* #V00056 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00056 ^property[3].code = #hints 
* #V00056 ^property[3].valueString = "DEPRECATED" 
* #V00125 "BUN Removal Rate"
* #V00125 ^definition = BUN Removal Rate
* #V00125 ^designation[0].language = #de-AT 
* #V00125 ^designation[0].value = "BUN Removal Rate" 
* #V00125 ^property[0].code = #parent 
* #V00125 ^property[0].valueCode = #05180 
* #V00125 ^property[1].code = #status 
* #V00125 ^property[1].valueCode = #retired 
* #V00125 ^property[2].code = #Relationships 
* #V00125 ^property[2].valueString = "verwendbar bis 30.06.2017" 
* #V00125 ^property[3].code = #hints 
* #V00125 ^property[3].valueString = "DEPRECATED" 
* #V00173 "IL-1RA"
* #V00173 ^definition = IL-1RA
* #V00173 ^designation[0].language = #de-AT 
* #V00173 ^designation[0].value = "Interleukin-1Rezeptor Antagonist" 
* #V00173 ^property[0].code = #parent 
* #V00173 ^property[0].valueCode = #05180 
* #V00174 "High sensitive IL6"
* #V00174 ^definition = High sensitive IL6
* #V00174 ^designation[0].language = #de-AT 
* #V00174 ^designation[0].value = "High sensitive IL6" 
* #V00174 ^property[0].code = #parent 
* #V00174 ^property[0].valueCode = #05180 
* #V00175 "Interleukin-7"
* #V00175 ^definition = Interleukin-7
* #V00175 ^designation[0].language = #de-AT 
* #V00175 ^designation[0].value = "Interleukin-7" 
* #V00175 ^property[0].code = #parent 
* #V00175 ^property[0].valueCode = #05180 
* #V00175 ^property[1].code = #status 
* #V00175 ^property[1].valueCode = #retired 
* #V00175 ^property[2].code = #Relationships 
* #V00175 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00175 ^property[3].code = #hints 
* #V00175 ^property[3].valueString = "DEPRECATED" 
* #V00176 "sCD8"
* #V00176 ^definition = sCD8
* #V00176 ^designation[0].language = #de-AT 
* #V00176 ^designation[0].value = "sCD8" 
* #V00176 ^property[0].code = #parent 
* #V00176 ^property[0].valueCode = #05180 
* #V00177 "sCD14"
* #V00177 ^definition = sCD14
* #V00177 ^designation[0].language = #de-AT 
* #V00177 ^designation[0].value = "sCD14 (lösl.Endotoxin-Rezeptor)" 
* #V00177 ^property[0].code = #parent 
* #V00177 ^property[0].valueCode = #05180 
* #V00178 "sCD23"
* #V00178 ^definition = sCD23
* #V00178 ^designation[0].language = #de-AT 
* #V00178 ^designation[0].value = "sCD23" 
* #V00178 ^property[0].code = #parent 
* #V00178 ^property[0].valueCode = #05180 
* #V00179 "sCD30"
* #V00179 ^definition = sCD30
* #V00179 ^designation[0].language = #de-AT 
* #V00179 ^designation[0].value = "sCD30" 
* #V00179 ^property[0].code = #parent 
* #V00179 ^property[0].valueCode = #05180 
* #V00180 "sCD40 Ligand"
* #V00180 ^definition = sCD40 Ligand
* #V00180 ^designation[0].language = #de-AT 
* #V00180 ^designation[0].value = "sCD40 Ligand" 
* #V00180 ^property[0].code = #parent 
* #V00180 ^property[0].valueCode = #05180 
* #V00181 "EGF-Rezeptor"
* #V00181 ^definition = EGF-Rezeptor
* #V00181 ^designation[0].language = #de-AT 
* #V00181 ^designation[0].value = "EGF-Rezeptor" 
* #V00181 ^property[0].code = #parent 
* #V00181 ^property[0].valueCode = #05180 
* #V00183 "Endothelin (1-21)"
* #V00183 ^definition = Endothelin (1-21)
* #V00183 ^designation[0].language = #de-AT 
* #V00183 ^designation[0].value = "Endothelin (1-21)" 
* #V00183 ^property[0].code = #parent 
* #V00183 ^property[0].valueCode = #05180 
* #V00183 ^property[1].code = #status 
* #V00183 ^property[1].valueCode = #retired 
* #V00183 ^property[2].code = #Relationships 
* #V00183 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00183 ^property[3].code = #hints 
* #V00183 ^property[3].valueString = "DEPRECATED" 
* #V00184 "GM-CSF"
* #V00184 ^definition = GM-CSF
* #V00184 ^designation[0].language = #de-AT 
* #V00184 ^designation[0].value = "Gran.-Macrophage col.stim.fact" 
* #V00184 ^property[0].code = #parent 
* #V00184 ^property[0].valueCode = #05180 
* #V00185 "PDGF-Antikörper"
* #V00185 ^definition = PDGF-Antikörper
* #V00185 ^designation[0].language = #de-AT 
* #V00185 ^designation[0].value = "Platelet-derived GF-AK" 
* #V00185 ^property[0].code = #parent 
* #V00185 ^property[0].valueCode = #05180 
* #V00186 "RANTES (CCL5)"
* #V00186 ^definition = RANTES (CCL5)
* #V00186 ^designation[0].language = #de-AT 
* #V00186 ^designation[0].value = "RANTES (CCL5)" 
* #V00186 ^property[0].code = #parent 
* #V00186 ^property[0].valueCode = #05180 
* #V00187 "TGF-ß2"
* #V00187 ^definition = TGF-ß2
* #V00187 ^designation[0].language = #de-AT 
* #V00187 ^designation[0].value = "Transforming growth factor-ß2" 
* #V00187 ^property[0].code = #parent 
* #V00187 ^property[0].valueCode = #05180 
* #V00188 "VEGF-A"
* #V00188 ^definition = VEGF-A
* #V00188 ^designation[0].language = #de-AT 
* #V00188 ^designation[0].value = "Vasc.endoth.growth factor A" 
* #V00188 ^property[0].code = #parent 
* #V00188 ^property[0].valueCode = #05180 
* #V00189 "VEGF-C"
* #V00189 ^definition = VEGF-C
* #V00189 ^designation[0].language = #de-AT 
* #V00189 ^designation[0].value = "Vasc.endoth.growth factor C" 
* #V00189 ^property[0].code = #parent 
* #V00189 ^property[0].valueCode = #05180 
* #V00190 "VEGF-R1"
* #V00190 ^definition = VEGF-R1
* #V00190 ^designation[0].language = #de-AT 
* #V00190 ^designation[0].value = "Vasc.endoth.growth f.Rezeptor1" 
* #V00190 ^property[0].code = #parent 
* #V00190 ^property[0].valueCode = #05180 
* #V00191 "VEGF-R3"
* #V00191 ^definition = VEGF-R3
* #V00191 ^designation[0].language = #de-AT 
* #V00191 ^designation[0].value = "Vasc.endoth.growth f.Rezeptor3" 
* #V00191 ^property[0].code = #parent 
* #V00191 ^property[0].valueCode = #05180 
* #V00192 "TNF-ß"
* #V00192 ^definition = TNF-ß
* #V00192 ^designation[0].language = #de-AT 
* #V00192 ^designation[0].value = "Tumornekrosefaktor-beta" 
* #V00192 ^property[0].code = #parent 
* #V00192 ^property[0].valueCode = #05180 
* #V00193 "sTNF-Rezeptor"
* #V00193 ^definition = sTNF-Rezeptor
* #V00193 ^designation[0].language = #de-AT 
* #V00193 ^designation[0].value = "sTNF-Rezeptor" 
* #V00193 ^property[0].code = #parent 
* #V00193 ^property[0].valueCode = #05180 
* #V00194 "P-Selektin"
* #V00194 ^definition = P-Selektin
* #V00194 ^designation[0].language = #de-AT 
* #V00194 ^designation[0].value = "P-Selektin" 
* #V00194 ^property[0].code = #parent 
* #V00194 ^property[0].valueCode = #05180 
* #V00195 "L-Selektin"
* #V00195 ^definition = L-Selektin
* #V00195 ^designation[0].language = #de-AT 
* #V00195 ^designation[0].value = "L-Selektin" 
* #V00195 ^property[0].code = #parent 
* #V00195 ^property[0].valueCode = #05180 
* #V00196 "sICAM-1"
* #V00196 ^definition = sICAM-1
* #V00196 ^designation[0].language = #de-AT 
* #V00196 ^designation[0].value = "sICAM-1" 
* #V00196 ^property[0].code = #parent 
* #V00196 ^property[0].valueCode = #05180 
* #V00220 "Interleukin-3"
* #V00220 ^definition = Interleukin-3
* #V00220 ^designation[0].language = #de-AT 
* #V00220 ^designation[0].value = "Interleukin-3" 
* #V00220 ^property[0].code = #parent 
* #V00220 ^property[0].valueCode = #05180 
* #V00220 ^property[1].code = #status 
* #V00220 ^property[1].valueCode = #retired 
* #V00220 ^property[2].code = #Relationships 
* #V00220 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00220 ^property[3].code = #hints 
* #V00220 ^property[3].valueString = "DEPRECATED" 
* #V00221 "sIL-6 Rezeptor"
* #V00221 ^definition = sIL-6 Rezeptor
* #V00221 ^designation[0].language = #de-AT 
* #V00221 ^designation[0].value = "sIL-6 Rezeptor" 
* #V00221 ^property[0].code = #parent 
* #V00221 ^property[0].valueCode = #05180 
* #V00267 "oGTT Anf."
* #V00267 ^definition = oGTT  Anf.
* #V00267 ^designation[0].language = #de-AT 
* #V00267 ^designation[0].value = "Oraler Glukosetoleranztest Anfo." 
* #V00267 ^property[0].code = #parent 
* #V00267 ^property[0].valueCode = #05180 
* #V00268 "APRI Score"
* #V00268 ^definition = APRI Score
* #V00268 ^designation[0].language = #de-AT 
* #V00268 ^designation[0].value = "AST Plättchen Ratio Index (APRI Score)" 
* #V00268 ^property[0].code = #parent 
* #V00268 ^property[0].valueCode = #05180 
* #V00461-6 "H2O-Clearance"
* #V00461-6 ^definition = H2O-Clearance
* #V00461-6 ^designation[0].language = #de-AT 
* #V00461-6 ^designation[0].value = "H2O-Clearance" 
* #V00461-6 ^property[0].code = #parent 
* #V00461-6 ^property[0].valueCode = #05180 
* #V00579-5 "Maddrey Score"
* #V00579-5 ^definition = Maddrey Score
* #V00579-5 ^designation[0].language = #de-AT 
* #V00579-5 ^designation[0].value = "Maddrey Score" 
* #V00579-5 ^property[0].code = #parent 
* #V00579-5 ^property[0].valueCode = #05180 
* #V00610-8 "ELF-Score (Enhanced Liver Fibrosis Score)"
* #V00610-8 ^definition = ELF-Score
* #V00610-8 ^designation[0].language = #de-AT 
* #V00610-8 ^designation[0].value = "ELF-Score (Enhanced Liver Fibrosis Score)" 
* #V00610-8 ^property[0].code = #parent 
* #V00610-8 ^property[0].valueCode = #05180 
* #V00610-8 ^property[1].code = #status 
* #V00610-8 ^property[1].valueCode = #retired 
* #V00610-8 ^property[2].code = #Relationships 
* #V00610-8 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00610-8 ^property[3].code = #hints 
* #V00610-8 ^property[3].valueString = "DEPRECATED" 
* #V00611-6 "Lipopolysaccharid binding Protein (LBP)"
* #V00611-6 ^definition = LBP
* #V00611-6 ^designation[0].language = #de-AT 
* #V00611-6 ^designation[0].value = "Lipopolysaccharid binding Protein (LBP)" 
* #V00611-6 ^property[0].code = #parent 
* #V00611-6 ^property[0].valueCode = #05180 
* #V00611-6 ^property[1].code = #status 
* #V00611-6 ^property[1].valueCode = #retired 
* #V00611-6 ^property[2].code = #Relationships 
* #V00611-6 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00611-6 ^property[3].code = #hints 
* #V00611-6 ^property[3].valueString = "DEPRECATED" 
* #V00639-7 "AP-LpX-gebundene Frakt. Rel."
* #V00639-7 ^definition = AP-LpX-gb.Frakt.rel.
* #V00639-7 ^designation[0].language = #de-AT 
* #V00639-7 ^designation[0].value = "Alkalische Phosphatase-LpX-gebundene Fraktion rel." 
* #V00639-7 ^property[0].code = #parent 
* #V00639-7 ^property[0].valueCode = #05180 
* #V00640-5 "H2 10 min (Fructose-Atemtest)"
* #V00640-5 ^definition = H2 10M(Fruct.Atemt.)
* #V00640-5 ^designation[0].language = #de-AT 
* #V00640-5 ^designation[0].value = "H2 10 min (Fructose-Atemtest)" 
* #V00640-5 ^property[0].code = #parent 
* #V00640-5 ^property[0].valueCode = #05180 
* #V00640-5 ^property[1].code = #status 
* #V00640-5 ^property[1].valueCode = #retired 
* #V00640-5 ^property[2].code = #Relationships 
* #V00640-5 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00640-5 ^property[3].code = #hints 
* #V00640-5 ^property[3].valueString = "DEPRECATED" 
* #V00641-3 "H2 20 min (Fructose-Atemtest)"
* #V00641-3 ^definition = H2 20M(Fruct.Atemt.)
* #V00641-3 ^designation[0].language = #de-AT 
* #V00641-3 ^designation[0].value = "H2 20 min (Fructose-Atemtest)" 
* #V00641-3 ^property[0].code = #parent 
* #V00641-3 ^property[0].valueCode = #05180 
* #V00641-3 ^property[1].code = #status 
* #V00641-3 ^property[1].valueCode = #retired 
* #V00641-3 ^property[2].code = #Relationships 
* #V00641-3 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00641-3 ^property[3].code = #hints 
* #V00641-3 ^property[3].valueString = "DEPRECATED" 
* #V00642-1 "H2 40 min (Fructose-Atemtest)"
* #V00642-1 ^definition = H2 40M(Fruct.Atemt.)
* #V00642-1 ^designation[0].language = #de-AT 
* #V00642-1 ^designation[0].value = "H2 40 min (Fructose-Atemtest)" 
* #V00642-1 ^property[0].code = #parent 
* #V00642-1 ^property[0].valueCode = #05180 
* #V00642-1 ^property[1].code = #status 
* #V00642-1 ^property[1].valueCode = #retired 
* #V00642-1 ^property[2].code = #Relationships 
* #V00642-1 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00642-1 ^property[3].code = #hints 
* #V00642-1 ^property[3].valueString = "DEPRECATED" 
* #V00643-9 "H2 50 min (Fructose-Atemtest)"
* #V00643-9 ^definition = H2 50M(Fruct.Atemt.)
* #V00643-9 ^designation[0].language = #de-AT 
* #V00643-9 ^designation[0].value = "H2 50 min (Fructose-Atemtest)" 
* #V00643-9 ^property[0].code = #parent 
* #V00643-9 ^property[0].valueCode = #05180 
* #V00643-9 ^property[1].code = #status 
* #V00643-9 ^property[1].valueCode = #retired 
* #V00643-9 ^property[2].code = #Relationships 
* #V00643-9 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00643-9 ^property[3].code = #hints 
* #V00643-9 ^property[3].valueString = "DEPRECATED" 
* #V00644-7 "H2 10 min (Lactose-Atemtest)"
* #V00644-7 ^definition = H2 10M(Lact.Atemte.)
* #V00644-7 ^designation[0].language = #de-AT 
* #V00644-7 ^designation[0].value = "H2 10 min (Lactose-Atemtest)" 
* #V00644-7 ^property[0].code = #parent 
* #V00644-7 ^property[0].valueCode = #05180 
* #V00644-7 ^property[1].code = #status 
* #V00644-7 ^property[1].valueCode = #retired 
* #V00644-7 ^property[2].code = #Relationships 
* #V00644-7 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00644-7 ^property[3].code = #hints 
* #V00644-7 ^property[3].valueString = "DEPRECATED" 
* #V00645-4 "H2 20 min (Lactose-Atemtest)"
* #V00645-4 ^definition = H2 20M(Lact.Atemte.)
* #V00645-4 ^designation[0].language = #de-AT 
* #V00645-4 ^designation[0].value = "H2 20 min (Lactose-Atemtest)" 
* #V00645-4 ^property[0].code = #parent 
* #V00645-4 ^property[0].valueCode = #05180 
* #V00645-4 ^property[1].code = #status 
* #V00645-4 ^property[1].valueCode = #retired 
* #V00645-4 ^property[2].code = #Relationships 
* #V00645-4 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00645-4 ^property[3].code = #hints 
* #V00645-4 ^property[3].valueString = "DEPRECATED" 
* #V00646-2 "H2 40 min (Lactose-Atemtest)"
* #V00646-2 ^definition = H2 40M(Lact.Atemte.)
* #V00646-2 ^designation[0].language = #de-AT 
* #V00646-2 ^designation[0].value = "H2 40 min (Lactose-Atemtest)" 
* #V00646-2 ^property[0].code = #parent 
* #V00646-2 ^property[0].valueCode = #05180 
* #V00646-2 ^property[1].code = #status 
* #V00646-2 ^property[1].valueCode = #retired 
* #V00646-2 ^property[2].code = #Relationships 
* #V00646-2 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00646-2 ^property[3].code = #hints 
* #V00646-2 ^property[3].valueString = "DEPRECATED" 
* #V00647-0 "H2 50 min (Lactose-Atemtest)"
* #V00647-0 ^definition = H2 50M(Lact.Atemte.)
* #V00647-0 ^designation[0].language = #de-AT 
* #V00647-0 ^designation[0].value = "H2 50 min (Lactose-Atemtest)" 
* #V00647-0 ^property[0].code = #parent 
* #V00647-0 ^property[0].valueCode = #05180 
* #V00647-0 ^property[1].code = #status 
* #V00647-0 ^property[1].valueCode = #retired 
* #V00647-0 ^property[2].code = #Relationships 
* #V00647-0 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00647-0 ^property[3].code = #hints 
* #V00647-0 ^property[3].valueString = "DEPRECATED" 
* #V00729-6 "Kupfer, freies (kalkuliert)"
* #V00729-6 ^definition = Kupfer, freies
* #V00729-6 ^designation[0].language = #de-AT 
* #V00729-6 ^designation[0].value = "Kupfer, freies (kalkuliert)" 
* #V00729-6 ^property[0].code = #parent 
* #V00729-6 ^property[0].valueCode = #05180 
* #V00729-6 ^property[1].code = #status 
* #V00729-6 ^property[1].valueCode = #retired 
* #V00729-6 ^property[2].code = #Relationships 
* #V00729-6 ^property[2].valueString = "verwendbar bis 30.06.2021" 
* #V00729-6 ^property[3].code = #hints 
* #V00729-6 ^property[3].valueString = "DEPRECATED" 
* #V00769-2 "Soluble-Urokinase-Plasminogen-Aktivator-Rezeptor"
* #V00769-2 ^definition = sUPAR
* #V00769-2 ^designation[0].language = #de-AT 
* #V00769-2 ^designation[0].value = "Soluble-Urokinase-Plasminogen-Aktivator-Rezeptor" 
* #V00769-2 ^property[0].code = #parent 
* #V00769-2 ^property[0].valueCode = #05180 
* #05190 "Proteindiagnostik"
* #05190 ^property[0].code = #parent 
* #05190 ^property[0].valueCode = #500 
* #05190 ^property[1].code = #child 
* #05190 ^property[1].valueCode = #V00570-4 
* #V00570-4 "Freie Kappa/freie Lambda-Differenz"
* #V00570-4 ^definition = fKappa/fLambda-Diff.
* #V00570-4 ^designation[0].language = #de-AT 
* #V00570-4 ^designation[0].value = "Freie Kappa/freie Lambda-Differenz" 
* #V00570-4 ^property[0].code = #parent 
* #V00570-4 ^property[0].valueCode = #05190 
* #05200 "Spurenelemente"
* #05200 ^property[0].code = #parent 
* #05200 ^property[0].valueCode = #500 
* #05210 "Sondermaterialien"
* #05210 ^property[0].code = #parent 
* #05210 ^property[0].valueCode = #500 
* #05210 ^property[1].code = #child 
* #05210 ^property[1].valueCode = #V00140 
* #05210 ^property[2].code = #child 
* #05210 ^property[2].valueCode = #V00288 
* #05210 ^property[3].code = #child 
* #05210 ^property[3].valueCode = #V00289 
* #05210 ^property[4].code = #child 
* #05210 ^property[4].valueCode = #V00575-3 
* #05210 ^property[5].code = #child 
* #05210 ^property[5].valueCode = #V00667-8 
* #05210 ^property[6].code = #child 
* #05210 ^property[6].valueCode = #V00668-6 
* #05210 ^property[7].code = #child 
* #05210 ^property[7].valueCode = #V00669-4 
* #05210 ^property[8].code = #child 
* #05210 ^property[8].valueCode = #V00670-2 
* #05210 ^property[9].code = #child 
* #05210 ^property[9].valueCode = #V00671-0 
* #V00140 "Spermien Gesamtzahl"
* #V00140 ^definition = Spermien Gesamtzahl
* #V00140 ^designation[0].language = #de-AT 
* #V00140 ^designation[0].value = "Spermien Gesamtzahl" 
* #V00140 ^property[0].code = #parent 
* #V00140 ^property[0].valueCode = #05210 
* #V00288 "Kommentar Sondermaterial"
* #V00288 ^definition = Komm.Sondermaterial
* #V00288 ^designation[0].language = #de-AT 
* #V00288 ^designation[0].value = "Kommentar Sondermaterial" 
* #V00288 ^property[0].code = #parent 
* #V00288 ^property[0].valueCode = #05210 
* #V00289 "Komm.Spermiogramm"
* #V00289 ^definition = Komm.Spermiogramm
* #V00289 ^designation[0].language = #de-AT 
* #V00289 ^designation[0].value = "Kommentar Spermiogramm" 
* #V00289 ^property[0].code = #parent 
* #V00289 ^property[0].valueCode = #05210 
* #V00575-3 "proGRP /SM"
* #V00575-3 ^definition = proGRP /SM
* #V00575-3 ^designation[0].language = #de-AT 
* #V00575-3 ^designation[0].value = "proGRP /Sondermaterial" 
* #V00575-3 ^property[0].code = #parent 
* #V00575-3 ^property[0].valueCode = #05210 
* #V00667-8 "Lipase /PD"
* #V00667-8 ^definition = Lipase /PD
* #V00667-8 ^designation[0].language = #de-AT 
* #V00667-8 ^designation[0].value = "Lipase /Peritonealdialysat" 
* #V00667-8 ^property[0].code = #parent 
* #V00667-8 ^property[0].valueCode = #05210 
* #V00667-8 ^property[1].code = #status 
* #V00667-8 ^property[1].valueCode = #retired 
* #V00667-8 ^property[2].code = #Relationships 
* #V00667-8 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00667-8 ^property[3].code = #hints 
* #V00667-8 ^property[3].valueString = "DEPRECATED" 
* #V00668-6 "Pankreas-Amylase /PD"
* #V00668-6 ^definition = Pankreas-Amylase /PD
* #V00668-6 ^designation[0].language = #de-AT 
* #V00668-6 ^designation[0].value = "Pankreas-Amylase /Peritonealdialysat" 
* #V00668-6 ^property[0].code = #parent 
* #V00668-6 ^property[0].valueCode = #05210 
* #V00668-6 ^property[1].code = #status 
* #V00668-6 ^property[1].valueCode = #retired 
* #V00668-6 ^property[2].code = #Relationships 
* #V00668-6 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00668-6 ^property[3].code = #hints 
* #V00668-6 ^property[3].valueString = "DEPRECATED" 
* #V00669-4 "Beta-2-Mikroglobulin /PD"
* #V00669-4 ^definition = Beta-2-Mikroglob./PD
* #V00669-4 ^designation[0].language = #de-AT 
* #V00669-4 ^designation[0].value = "Beta-2-Mikroglobulin /Peritonealdialysat" 
* #V00669-4 ^property[0].code = #parent 
* #V00669-4 ^property[0].valueCode = #05210 
* #V00669-4 ^property[1].code = #status 
* #V00669-4 ^property[1].valueCode = #retired 
* #V00669-4 ^property[2].code = #Relationships 
* #V00669-4 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00669-4 ^property[3].code = #hints 
* #V00669-4 ^property[3].valueString = "DEPRECATED" 
* #V00670-2 "Freies Hämoglobin /SM"
* #V00670-2 ^definition = Freies Hb /SM
* #V00670-2 ^designation[0].language = #de-AT 
* #V00670-2 ^designation[0].value = "Freies Hämoglobin /Sondermaterial" 
* #V00670-2 ^property[0].code = #parent 
* #V00670-2 ^property[0].valueCode = #05210 
* #V00670-2 ^property[1].code = #status 
* #V00670-2 ^property[1].valueCode = #retired 
* #V00670-2 ^property[2].code = #Relationships 
* #V00670-2 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00670-2 ^property[3].code = #hints 
* #V00670-2 ^property[3].valueString = "DEPRECATED" 
* #V00671-0 "Cholinesterase /SM"
* #V00671-0 ^definition = Cholinesterase /SM
* #V00671-0 ^designation[0].language = #de-AT 
* #V00671-0 ^designation[0].value = "Cholinesterase /Sondermaterial" 
* #V00671-0 ^property[0].code = #parent 
* #V00671-0 ^property[0].valueCode = #05210 
* #V00671-0 ^property[1].code = #status 
* #V00671-0 ^property[1].valueCode = #retired 
* #V00671-0 ^property[2].code = #Relationships 
* #V00671-0 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00671-0 ^property[3].code = #hints 
* #V00671-0 ^property[3].valueString = "DEPRECATED" 
* #05220 "Unters. bei Stoffwechselerkrankungen"
* #05220 ^property[0].code = #parent 
* #05220 ^property[0].valueCode = #500 
* #05220 ^property[1].code = #child 
* #05220 ^property[1].valueCode = #V00164 
* #05220 ^property[2].code = #child 
* #05220 ^property[2].valueCode = #V00165 
* #05220 ^property[3].code = #child 
* #05220 ^property[3].valueCode = #V00290 
* #V00164 "C16+18:1/C2 karn."
* #V00164 ^definition = C16+18:1/C2 karn.
* #V00164 ^designation[0].language = #de-AT 
* #V00164 ^designation[0].value = "C16+18:1/C2 Karnitin i.S./Pl." 
* #V00164 ^property[0].code = #parent 
* #V00164 ^property[0].valueCode = #05220 
* #V00164 ^property[1].code = #status 
* #V00164 ^property[1].valueCode = #retired 
* #V00164 ^property[2].code = #Relationships 
* #V00164 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00164 ^property[3].code = #hints 
* #V00164 ^property[3].valueString = "DEPRECATED" 
* #V00165 "C0/C16+C18 karn."
* #V00165 ^definition = C0/C16+C18 karn.
* #V00165 ^designation[0].language = #de-AT 
* #V00165 ^designation[0].value = "C0/C16+C18 Karnitin i.S./Pl." 
* #V00165 ^property[0].code = #parent 
* #V00165 ^property[0].valueCode = #05220 
* #V00165 ^property[1].code = #status 
* #V00165 ^property[1].valueCode = #retired 
* #V00165 ^property[2].code = #Relationships 
* #V00165 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00165 ^property[3].code = #hints 
* #V00165 ^property[3].valueString = "DEPRECATED" 
* #V00290 "Komm.Stoffwechselunt"
* #V00290 ^definition = Komm.Stoffwechselunt
* #V00290 ^designation[0].language = #de-AT 
* #V00290 ^designation[0].value = "Kommentar Unters. bei Stoffwechselerkrankungen" 
* #V00290 ^property[0].code = #parent 
* #V00290 ^property[0].valueCode = #05220 
* #600 "Hormone/Vitamine/Tumormarker"
* #600 ^property[0].code = #parent 
* #600 ^property[0].valueCode = #1 
* #600 ^property[1].code = #child 
* #600 ^property[1].valueCode = #06330 
* #600 ^property[2].code = #child 
* #600 ^property[2].valueCode = #06340 
* #600 ^property[3].code = #child 
* #600 ^property[3].valueCode = #06350 
* #06330 "Hormone"
* #06330 ^property[0].code = #parent 
* #06330 ^property[0].valueCode = #600 
* #06330 ^property[1].code = #child 
* #06330 ^property[1].valueCode = #V00004 
* #06330 ^property[2].code = #child 
* #06330 ^property[2].valueCode = #V00005 
* #06330 ^property[3].code = #child 
* #06330 ^property[3].valueCode = #V00246 
* #06330 ^property[4].code = #child 
* #06330 ^property[4].valueCode = #V00455-8 
* #06330 ^property[5].code = #child 
* #06330 ^property[5].valueCode = #V00459-0 
* #06330 ^property[6].code = #child 
* #06330 ^property[6].valueCode = #V00460-8 
* #06330 ^property[7].code = #child 
* #06330 ^property[7].valueCode = #V00485-5 
* #06330 ^property[8].code = #child 
* #06330 ^property[8].valueCode = #V00612-4 
* #06330 ^property[9].code = #child 
* #06330 ^property[9].valueCode = #V00613-2 
* #06330 ^property[10].code = #child 
* #06330 ^property[10].valueCode = #V00614-0 
* #06330 ^property[11].code = #child 
* #06330 ^property[11].valueCode = #V00615-7 
* #06330 ^property[12].code = #child 
* #06330 ^property[12].valueCode = #V00616-5 
* #06330 ^property[13].code = #child 
* #06330 ^property[13].valueCode = #V00617-3 
* #06330 ^property[14].code = #child 
* #06330 ^property[14].valueCode = #V00618-1 
* #06330 ^property[15].code = #child 
* #06330 ^property[15].valueCode = #V00619-9 
* #06330 ^property[16].code = #child 
* #06330 ^property[16].valueCode = #V00620-7 
* #06330 ^property[17].code = #child 
* #06330 ^property[17].valueCode = #V00621-5 
* #06330 ^property[18].code = #child 
* #06330 ^property[18].valueCode = #V00622-3 
* #06330 ^property[19].code = #child 
* #06330 ^property[19].valueCode = #V00623-1 
* #06330 ^property[20].code = #child 
* #06330 ^property[20].valueCode = #V00624-9 
* #06330 ^property[21].code = #child 
* #06330 ^property[21].valueCode = #V00625-6 
* #06330 ^property[22].code = #child 
* #06330 ^property[22].valueCode = #V00626-4 
* #06330 ^property[23].code = #child 
* #06330 ^property[23].valueCode = #V00627-2 
* #06330 ^property[24].code = #child 
* #06330 ^property[24].valueCode = #V00628-0 
* #06330 ^property[25].code = #child 
* #06330 ^property[25].valueCode = #V00629-8 
* #06330 ^property[26].code = #child 
* #06330 ^property[26].valueCode = #V00630-6 
* #06330 ^property[27].code = #child 
* #06330 ^property[27].valueCode = #V00631-4 
* #06330 ^property[28].code = #child 
* #06330 ^property[28].valueCode = #V00632-2 
* #06330 ^property[29].code = #child 
* #06330 ^property[29].valueCode = #V00633-0 
* #06330 ^property[30].code = #child 
* #06330 ^property[30].valueCode = #V00634-8 
* #06330 ^property[31].code = #child 
* #06330 ^property[31].valueCode = #V00635-5 
* #06330 ^property[32].code = #child 
* #06330 ^property[32].valueCode = #V00770-0 
* #V00004 "Cortisol-Profil 9 Uhr 2.Tag"
* #V00004 ^definition = Cortisol-Pr. 9U.2.T.
* #V00004 ^designation[0].language = #de-AT 
* #V00004 ^designation[0].value = "Cortisol-Profil 9 Uhr 2.T." 
* #V00004 ^property[0].code = #parent 
* #V00004 ^property[0].valueCode = #06330 
* #V00005 "ACTH-Profil 9 Uhr 2.Tag"
* #V00005 ^definition = ACTH-Profil 9U. 2.T.
* #V00005 ^designation[0].language = #de-AT 
* #V00005 ^designation[0].value = "ACTH-Profil 9 Uhr 2.T." 
* #V00005 ^property[0].code = #parent 
* #V00005 ^property[0].valueCode = #06330 
* #V00246 "Thyreoiditis assoziierte AK"
* #V00246 ^definition = Thyreoiditis ass.AK
* #V00246 ^designation[0].language = #de-AT 
* #V00246 ^designation[0].value = "Thyreoiditis assoziierte Antikörper" 
* #V00246 ^property[0].code = #parent 
* #V00246 ^property[0].valueCode = #06330 
* #V00455-8 "Thyreoglobulin human sensitiv"
* #V00455-8 ^definition = Thyreoglobulin h.s.
* #V00455-8 ^designation[0].language = #de-AT 
* #V00455-8 ^designation[0].value = "Thyreoglobulin human sensitiv" 
* #V00455-8 ^property[0].code = #parent 
* #V00455-8 ^property[0].valueCode = #06330 
* #V00459-0 "Renin stim. 30 min"
* #V00459-0 ^definition = Renin stim. 30M
* #V00459-0 ^designation[0].language = #de-AT 
* #V00459-0 ^designation[0].value = "Renin stimuliert 30M" 
* #V00459-0 ^property[0].code = #parent 
* #V00459-0 ^property[0].valueCode = #06330 
* #V00460-8 "HGH 20 min (Insulintest)"
* #V00460-8 ^definition = HGH 20m (Ins.t)
* #V00460-8 ^designation[0].language = #de-AT 
* #V00460-8 ^designation[0].value = "HGH 20 min (Insulintest)" 
* #V00460-8 ^property[0].code = #parent 
* #V00460-8 ^property[0].valueCode = #06330 
* #V00485-5 "PTH 1-84"
* #V00485-5 ^definition = PTH 1-84
* #V00485-5 ^designation[0].language = #de-AT 
* #V00485-5 ^designation[0].value = "Parathormon 184" 
* #V00485-5 ^property[0].code = #parent 
* #V00485-5 ^property[0].valueCode = #06330 
* #V00612-4 "C-Peptid Abnahme 1"
* #V00612-4 ^definition = C-Peptid Abnahme 1
* #V00612-4 ^designation[0].language = #de-AT 
* #V00612-4 ^designation[0].value = "C-Peptid Abnahme 1" 
* #V00612-4 ^property[0].code = #parent 
* #V00612-4 ^property[0].valueCode = #06330 
* #V00613-2 "C-Peptid Abnahme 2"
* #V00613-2 ^definition = C-Peptid Abnahme 2
* #V00613-2 ^designation[0].language = #de-AT 
* #V00613-2 ^designation[0].value = "C-Peptid Abnahme 2" 
* #V00613-2 ^property[0].code = #parent 
* #V00613-2 ^property[0].valueCode = #06330 
* #V00614-0 "C-Peptid Abnahme 3"
* #V00614-0 ^definition = C-Peptid Abnahme 3
* #V00614-0 ^designation[0].language = #de-AT 
* #V00614-0 ^designation[0].value = "C-Peptid Abnahme 3" 
* #V00614-0 ^property[0].code = #parent 
* #V00614-0 ^property[0].valueCode = #06330 
* #V00615-7 "C-Peptid Abnahme 4"
* #V00615-7 ^definition = C-Peptid Abnahme 4
* #V00615-7 ^designation[0].language = #de-AT 
* #V00615-7 ^designation[0].value = "C-Peptid Abnahme 4" 
* #V00615-7 ^property[0].code = #parent 
* #V00615-7 ^property[0].valueCode = #06330 
* #V00616-5 "C-Peptid Abnahme 5"
* #V00616-5 ^definition = C-Peptid Abnahme 5
* #V00616-5 ^designation[0].language = #de-AT 
* #V00616-5 ^designation[0].value = "C-Peptid Abnahme 5" 
* #V00616-5 ^property[0].code = #parent 
* #V00616-5 ^property[0].valueCode = #06330 
* #V00617-3 "C-Peptid Abnahme 6"
* #V00617-3 ^definition = C-Peptid Abnahme 6
* #V00617-3 ^designation[0].language = #de-AT 
* #V00617-3 ^designation[0].value = "C-Peptid Abnahme 6" 
* #V00617-3 ^property[0].code = #parent 
* #V00617-3 ^property[0].valueCode = #06330 
* #V00618-1 "C-Peptid Abnahme 7"
* #V00618-1 ^definition = C-Peptid Abnahme 7
* #V00618-1 ^designation[0].language = #de-AT 
* #V00618-1 ^designation[0].value = "C-Peptid Abnahme 7" 
* #V00618-1 ^property[0].code = #parent 
* #V00618-1 ^property[0].valueCode = #06330 
* #V00619-9 "C-Peptid Abnahme 8"
* #V00619-9 ^definition = C-Peptid Abnahme 8
* #V00619-9 ^designation[0].language = #de-AT 
* #V00619-9 ^designation[0].value = "C-Peptid Abnahme 8" 
* #V00619-9 ^property[0].code = #parent 
* #V00619-9 ^property[0].valueCode = #06330 
* #V00620-7 "17-Hydroxy-Progesteron Abnahme 1"
* #V00620-7 ^definition = 17OH-Progesteron #1
* #V00620-7 ^designation[0].language = #de-AT 
* #V00620-7 ^designation[0].value = "17-Hydroxy-Progesteron Abnahme 1" 
* #V00620-7 ^property[0].code = #parent 
* #V00620-7 ^property[0].valueCode = #06330 
* #V00620-7 ^property[1].code = #status 
* #V00620-7 ^property[1].valueCode = #retired 
* #V00620-7 ^property[2].code = #Relationships 
* #V00620-7 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00620-7 ^property[3].code = #hints 
* #V00620-7 ^property[3].valueString = "DEPRECATED" 
* #V00621-5 "17-Hydroxy-Progesteron Abnahme 2"
* #V00621-5 ^definition = 17OH-Progesteron #2
* #V00621-5 ^designation[0].language = #de-AT 
* #V00621-5 ^designation[0].value = "17-Hydroxy-Progesteron Abnahme 2" 
* #V00621-5 ^property[0].code = #parent 
* #V00621-5 ^property[0].valueCode = #06330 
* #V00621-5 ^property[1].code = #status 
* #V00621-5 ^property[1].valueCode = #retired 
* #V00621-5 ^property[2].code = #Relationships 
* #V00621-5 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00621-5 ^property[3].code = #hints 
* #V00621-5 ^property[3].valueString = "DEPRECATED" 
* #V00622-3 "17-Hydroxy-Progesteron Abnahme 3"
* #V00622-3 ^definition = 17OH-Progesteron #3
* #V00622-3 ^designation[0].language = #de-AT 
* #V00622-3 ^designation[0].value = "17-Hydroxy-Progesteron Abnahme 3" 
* #V00622-3 ^property[0].code = #parent 
* #V00622-3 ^property[0].valueCode = #06330 
* #V00622-3 ^property[1].code = #status 
* #V00622-3 ^property[1].valueCode = #retired 
* #V00622-3 ^property[2].code = #Relationships 
* #V00622-3 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00622-3 ^property[3].code = #hints 
* #V00622-3 ^property[3].valueString = "DEPRECATED" 
* #V00623-1 "17-Hydroxy-Progesteron Abnahme 4"
* #V00623-1 ^definition = 17OH-Progesteron #4
* #V00623-1 ^designation[0].language = #de-AT 
* #V00623-1 ^designation[0].value = "17-Hydroxy-Progesteron Abnahme 4" 
* #V00623-1 ^property[0].code = #parent 
* #V00623-1 ^property[0].valueCode = #06330 
* #V00623-1 ^property[1].code = #status 
* #V00623-1 ^property[1].valueCode = #retired 
* #V00623-1 ^property[2].code = #Relationships 
* #V00623-1 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00623-1 ^property[3].code = #hints 
* #V00623-1 ^property[3].valueString = "DEPRECATED" 
* #V00624-9 "17-Hydroxy-Progesteron Abnahme 5"
* #V00624-9 ^definition = 17OH-Progesteron #5
* #V00624-9 ^designation[0].language = #de-AT 
* #V00624-9 ^designation[0].value = "17-Hydroxy-Progesteron Abnahme 5" 
* #V00624-9 ^property[0].code = #parent 
* #V00624-9 ^property[0].valueCode = #06330 
* #V00624-9 ^property[1].code = #status 
* #V00624-9 ^property[1].valueCode = #retired 
* #V00624-9 ^property[2].code = #Relationships 
* #V00624-9 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00624-9 ^property[3].code = #hints 
* #V00624-9 ^property[3].valueString = "DEPRECATED" 
* #V00625-6 "17-Hydroxy-Progesteron Abnahme 6"
* #V00625-6 ^definition = 17OH-Progesteron #6
* #V00625-6 ^designation[0].language = #de-AT 
* #V00625-6 ^designation[0].value = "17-Hydroxy-Progesteron Abnahme 6" 
* #V00625-6 ^property[0].code = #parent 
* #V00625-6 ^property[0].valueCode = #06330 
* #V00625-6 ^property[1].code = #status 
* #V00625-6 ^property[1].valueCode = #retired 
* #V00625-6 ^property[2].code = #Relationships 
* #V00625-6 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00625-6 ^property[3].code = #hints 
* #V00625-6 ^property[3].valueString = "DEPRECATED" 
* #V00626-4 "17-Hydroxy-Progesteron Abnahme 7"
* #V00626-4 ^definition = 17OH-Progesteron #7
* #V00626-4 ^designation[0].language = #de-AT 
* #V00626-4 ^designation[0].value = "17-Hydroxy-Progesteron Abnahme 7" 
* #V00626-4 ^property[0].code = #parent 
* #V00626-4 ^property[0].valueCode = #06330 
* #V00626-4 ^property[1].code = #status 
* #V00626-4 ^property[1].valueCode = #retired 
* #V00626-4 ^property[2].code = #Relationships 
* #V00626-4 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00626-4 ^property[3].code = #hints 
* #V00626-4 ^property[3].valueString = "DEPRECATED" 
* #V00627-2 "17-Hydroxy-Progesteron Abnahme 8"
* #V00627-2 ^definition = 17OH-Progesteron #8
* #V00627-2 ^designation[0].language = #de-AT 
* #V00627-2 ^designation[0].value = "17-Hydroxy-Progesteron Abnahme 8" 
* #V00627-2 ^property[0].code = #parent 
* #V00627-2 ^property[0].valueCode = #06330 
* #V00627-2 ^property[1].code = #status 
* #V00627-2 ^property[1].valueCode = #retired 
* #V00627-2 ^property[2].code = #Relationships 
* #V00627-2 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00627-2 ^property[3].code = #hints 
* #V00627-2 ^property[3].valueString = "DEPRECATED" 
* #V00628-0 "Proinsulin Abnahme 1"
* #V00628-0 ^definition = Proinsulin Abnahme 1
* #V00628-0 ^designation[0].language = #de-AT 
* #V00628-0 ^designation[0].value = "Proinsulin Abnahme 1" 
* #V00628-0 ^property[0].code = #parent 
* #V00628-0 ^property[0].valueCode = #06330 
* #V00628-0 ^property[1].code = #status 
* #V00628-0 ^property[1].valueCode = #retired 
* #V00628-0 ^property[2].code = #Relationships 
* #V00628-0 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00628-0 ^property[3].code = #hints 
* #V00628-0 ^property[3].valueString = "DEPRECATED" 
* #V00629-8 "Proinsulin Abnahme 2"
* #V00629-8 ^definition = Proinsulin Abnahme 2
* #V00629-8 ^designation[0].language = #de-AT 
* #V00629-8 ^designation[0].value = "Proinsulin Abnahme 2" 
* #V00629-8 ^property[0].code = #parent 
* #V00629-8 ^property[0].valueCode = #06330 
* #V00629-8 ^property[1].code = #status 
* #V00629-8 ^property[1].valueCode = #retired 
* #V00629-8 ^property[2].code = #Relationships 
* #V00629-8 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00629-8 ^property[3].code = #hints 
* #V00629-8 ^property[3].valueString = "DEPRECATED" 
* #V00630-6 "Proinsulin Abnahme 3"
* #V00630-6 ^definition = Proinsulin Abnahme 3
* #V00630-6 ^designation[0].language = #de-AT 
* #V00630-6 ^designation[0].value = "Proinsulin Abnahme 3" 
* #V00630-6 ^property[0].code = #parent 
* #V00630-6 ^property[0].valueCode = #06330 
* #V00630-6 ^property[1].code = #status 
* #V00630-6 ^property[1].valueCode = #retired 
* #V00630-6 ^property[2].code = #Relationships 
* #V00630-6 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00630-6 ^property[3].code = #hints 
* #V00630-6 ^property[3].valueString = "DEPRECATED" 
* #V00631-4 "Proinsulin Abnahme 4"
* #V00631-4 ^definition = Proinsulin Abnahme 4
* #V00631-4 ^designation[0].language = #de-AT 
* #V00631-4 ^designation[0].value = "Proinsulin Abnahme 4" 
* #V00631-4 ^property[0].code = #parent 
* #V00631-4 ^property[0].valueCode = #06330 
* #V00631-4 ^property[1].code = #status 
* #V00631-4 ^property[1].valueCode = #retired 
* #V00631-4 ^property[2].code = #Relationships 
* #V00631-4 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00631-4 ^property[3].code = #hints 
* #V00631-4 ^property[3].valueString = "DEPRECATED" 
* #V00632-2 "Proinsulin Abnahme 5"
* #V00632-2 ^definition = Proinsulin Abnahme 5
* #V00632-2 ^designation[0].language = #de-AT 
* #V00632-2 ^designation[0].value = "Proinsulin Abnahme 5" 
* #V00632-2 ^property[0].code = #parent 
* #V00632-2 ^property[0].valueCode = #06330 
* #V00632-2 ^property[1].code = #status 
* #V00632-2 ^property[1].valueCode = #retired 
* #V00632-2 ^property[2].code = #Relationships 
* #V00632-2 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00632-2 ^property[3].code = #hints 
* #V00632-2 ^property[3].valueString = "DEPRECATED" 
* #V00633-0 "Proinsulin Abnahme 6"
* #V00633-0 ^definition = Proinsulin Abnahme 6
* #V00633-0 ^designation[0].language = #de-AT 
* #V00633-0 ^designation[0].value = "Proinsulin Abnahme 6" 
* #V00633-0 ^property[0].code = #parent 
* #V00633-0 ^property[0].valueCode = #06330 
* #V00633-0 ^property[1].code = #status 
* #V00633-0 ^property[1].valueCode = #retired 
* #V00633-0 ^property[2].code = #Relationships 
* #V00633-0 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00633-0 ^property[3].code = #hints 
* #V00633-0 ^property[3].valueString = "DEPRECATED" 
* #V00634-8 "Proinsulin Abnahme 7"
* #V00634-8 ^definition = Proinsulin Abnahme 7
* #V00634-8 ^designation[0].language = #de-AT 
* #V00634-8 ^designation[0].value = "Proinsulin Abnahme 7" 
* #V00634-8 ^property[0].code = #parent 
* #V00634-8 ^property[0].valueCode = #06330 
* #V00634-8 ^property[1].code = #status 
* #V00634-8 ^property[1].valueCode = #retired 
* #V00634-8 ^property[2].code = #Relationships 
* #V00634-8 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00634-8 ^property[3].code = #hints 
* #V00634-8 ^property[3].valueString = "DEPRECATED" 
* #V00635-5 "Proinsulin Abnahme 8"
* #V00635-5 ^definition = Proinsulin Abnahme 8
* #V00635-5 ^designation[0].language = #de-AT 
* #V00635-5 ^designation[0].value = "Proinsulin Abnahme 8" 
* #V00635-5 ^property[0].code = #parent 
* #V00635-5 ^property[0].valueCode = #06330 
* #V00635-5 ^property[1].code = #status 
* #V00635-5 ^property[1].valueCode = #retired 
* #V00635-5 ^property[2].code = #Relationships 
* #V00635-5 ^property[2].valueString = "verwendbar bis 31.12.2017" 
* #V00635-5 ^property[3].code = #hints 
* #V00635-5 ^property[3].valueString = "DEPRECATED" 
* #V00770-0 "17-Hydroxy-Progesteron /Speichel"
* #V00770-0 ^definition = 17OH-Progesteron /OF
* #V00770-0 ^designation[0].language = #de-AT 
* #V00770-0 ^designation[0].value = "17-Hydroxy-Progesteron /Speichel" 
* #V00770-0 ^property[0].code = #parent 
* #V00770-0 ^property[0].valueCode = #06330 
* #06340 "Vitamine"
* #06340 ^property[0].code = #parent 
* #06340 ^property[0].valueCode = #600 
* #06340 ^property[1].code = #child 
* #06340 ^property[1].valueCode = #V00294 
* #06340 ^property[2].code = #child 
* #06340 ^property[2].valueCode = #V00571-2 
* #V00294 "Komm.Vit./Spurenel."
* #V00294 ^definition = Komm.Vit./Spurenel.
* #V00294 ^designation[0].language = #de-AT 
* #V00294 ^designation[0].value = "Kommentar Vitamine/Spurenelemente" 
* #V00294 ^property[0].code = #parent 
* #V00294 ^property[0].valueCode = #06340 
* #V00571-2 "Vitamin B6 Pyridoxin /B"
* #V00571-2 ^definition = Vitamin B6 /B
* #V00571-2 ^designation[0].language = #de-AT 
* #V00571-2 ^designation[0].value = "Vitamin B6 Pyridoxin /Blut" 
* #V00571-2 ^property[0].code = #parent 
* #V00571-2 ^property[0].valueCode = #06340 
* #06350 "Tumormarker"
* #06350 ^property[0].code = #parent 
* #06350 ^property[0].valueCode = #600 
* #06350 ^property[1].code = #child 
* #06350 ^property[1].valueCode = #V00278 
* #06350 ^property[2].code = #child 
* #06350 ^property[2].valueCode = #V00578-7 
* #V00278 "Komm.Tumormarker"
* #V00278 ^definition = Komm.Tumormarker
* #V00278 ^designation[0].language = #de-AT 
* #V00278 ^designation[0].value = "Kommentar Tumormarker" 
* #V00278 ^property[0].code = #parent 
* #V00278 ^property[0].valueCode = #06350 
* #V00578-7 "proGRP"
* #V00578-7 ^definition = proGRP
* #V00578-7 ^designation[0].language = #de-AT 
* #V00578-7 ^designation[0].value = "proGRP" 
* #V00578-7 ^property[0].code = #parent 
* #V00578-7 ^property[0].valueCode = #06350 
* #900 "Toxikologie"
* #900 ^property[0].code = #parent 
* #900 ^property[0].valueCode = #1 
* #900 ^property[1].code = #child 
* #900 ^property[1].valueCode = #07230 
* #900 ^property[2].code = #child 
* #900 ^property[2].valueCode = #07240 
* #900 ^property[3].code = #child 
* #900 ^property[3].valueCode = #07250 
* #07230 "Urin-Screening"
* #07230 ^property[0].code = #parent 
* #07230 ^property[0].valueCode = #900 
* #07240 "Blut"
* #07240 ^property[0].code = #parent 
* #07240 ^property[0].valueCode = #900 
* #07240 ^property[1].code = #child 
* #07240 ^property[1].valueCode = #V00244 
* #07240 ^property[2].code = #child 
* #07240 ^property[2].valueCode = #V00543-1 
* #V00244 "Toxikologie Screen 2"
* #V00244 ^definition = Toxikologie Screen 2
* #V00244 ^designation[0].language = #de-AT 
* #V00244 ^designation[0].value = "Toxikologie Screen 2 Blut" 
* #V00244 ^property[0].code = #parent 
* #V00244 ^property[0].valueCode = #07240 
* #V00543-1 "Amanitin"
* #V00543-1 ^definition = Amanitin
* #V00543-1 ^designation[0].language = #de-AT 
* #V00543-1 ^designation[0].value = "Amanitin" 
* #V00543-1 ^property[0].code = #parent 
* #V00543-1 ^property[0].valueCode = #07240 
* #V00543-1 ^property[1].code = #status 
* #V00543-1 ^property[1].valueCode = #retired 
* #V00543-1 ^property[2].code = #Relationships 
* #V00543-1 ^property[2].valueString = "verwendbar bis 31.12.2018" 
* #V00543-1 ^property[3].code = #hints 
* #V00543-1 ^property[3].valueString = "DEPRECATED" 
* #07250 "Toxikologie Sonstiges"
* #07250 ^property[0].code = #parent 
* #07250 ^property[0].valueCode = #900 
* #10 "Probeninformation"
* #20 "Befundbewertung"
