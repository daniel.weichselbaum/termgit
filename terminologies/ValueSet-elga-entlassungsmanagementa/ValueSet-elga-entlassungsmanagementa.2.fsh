Instance: elga-entlassungsmanagementa 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-entlassungsmanagementa" 
* name = "elga-entlassungsmanagementa" 
* title = "ELGA_EntlassungsmanagementA" 
* status = #active 
* version = "2.8" 
* description = "**Description:** Describes the variety in a discharge management disposition (describes the kind of discharge date)

**Beschreibung:** Beschreibt die Möglichkeiten in einer Entlassungsmanagement-Disposition (Beschreibt die Art des Entlassungsdatums)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.53" 
* date = "2014-09-01" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-elga-entlassungsmanagementart"
* compose.include[0].concept[0].code = "GEPLENTLDAT"
* compose.include[0].concept[0].display = "Geplantes Entlassungsdatum"
