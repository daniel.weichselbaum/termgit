Instance: elga-auditparticipantobjectidtype 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-auditparticipantobjectidtype" 
* name = "elga-auditparticipantobjectidtype" 
* title = "ELGA_AuditParticipantObjectIdType" 
* status = #active 
* version = "3.0" 
* description = "**Beschreibung:** Liste der ELGA spezifischen Audit Participant Object ID Type Codes. Der Object ID Type Code beschreibt die Art eines Objekts, welches referenziert wird. " 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.156" 
* date = "2015-06-01" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-elga-auditparticipantobjectidtype"
* compose.include[0].concept[0].code = "0"
* compose.include[0].concept[0].display = "Transaktions ID"
* compose.include[0].concept[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[0].designation[0].value = "Transaktions ID (Transaktionsklammer)" 
* compose.include[0].concept[1].code = "100"
* compose.include[0].concept[1].display = "Policy ID"
* compose.include[0].concept[2].code = "110"
* compose.include[0].concept[2].display = "KBS ID"
* compose.include[0].concept[3].code = "120"
* compose.include[0].concept[3].display = "Assertion ID"
* compose.include[0].concept[4].code = "130"
* compose.include[0].concept[4].display = "Vollmacht Referenz"
* compose.include[0].concept[5].code = "140"
* compose.include[0].concept[5].display = "GDA ID"
