Instance: iana-mime-type 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-iana-mime-type" 
* name = "iana-mime-type" 
* title = "IANA Mime Type" 
* status = #active 
* content = #complete 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.840.10003.5.109" 
* date = "2013-01-10" 
* count = 8 
* property[0].code = #hints 
* property[0].type = #string 
* concept[0].code = #application/dicom
* concept[0].display = "Mime Type application/dicom"
* concept[0].designation[0].language = #de-AT 
* concept[0].designation[0].value = "DICOM Service Object" 
* concept[0].property[0].code = #hints 
* concept[0].property[0].valueString = "DICOM Service Object Pair [RFC3240]. Verwendung in WADO-Links" 
* concept[1].code = #application/pdf
* concept[1].display = "Mime Type application/pdf"
* concept[2].code = #audio/mpeg
* concept[2].display = "Mime Type audio/mpeg"
* concept[3].code = #image/gif
* concept[3].display = "Mime Type image/gif"
* concept[4].code = #image/jpeg
* concept[4].display = "Mime Type image/jpeg"
* concept[5].code = #image/png
* concept[5].display = "Mime Type image/png"
* concept[6].code = #text/xml
* concept[6].display = "Mime Type text/xml"
* concept[7].code = #video/mpeg
* concept[7].display = "Mime Type video/mpeg"
