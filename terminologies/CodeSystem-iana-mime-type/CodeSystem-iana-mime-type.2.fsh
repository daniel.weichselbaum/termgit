Instance: iana-mime-type 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-iana-mime-type" 
* name = "iana-mime-type" 
* title = "IANA Mime Type" 
* status = #active 
* content = #complete 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.840.10003.5.109" 
* date = "2013-01-10" 
* count = 8 
* property[0].code = #hints 
* property[0].type = #string 
* #application/dicom "Mime Type application/dicom"
* #application/dicom ^designation[0].language = #de-AT 
* #application/dicom ^designation[0].value = "DICOM Service Object" 
* #application/dicom ^property[0].code = #hints 
* #application/dicom ^property[0].valueString = "DICOM Service Object Pair [RFC3240]. Verwendung in WADO-Links" 
* #application/pdf "Mime Type application/pdf"
* #audio/mpeg "Mime Type audio/mpeg"
* #image/gif "Mime Type image/gif"
* #image/jpeg "Mime Type image/jpeg"
* #image/png "Mime Type image/png"
* #text/xml "Mime Type text/xml"
* #video/mpeg "Mime Type video/mpeg"
