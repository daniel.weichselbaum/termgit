Instance: ems-genogruppe 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-genogruppe" 
* name = "ems-genogruppe" 
* title = "EMS_Genogruppe" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Liste Genogruppe" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.70" 
* date = "2017-01-26" 
* count = 5 
* concept[0].code = #I
* concept[0].display = "I"
* concept[1].code = #II
* concept[1].display = "II"
* concept[2].code = #III
* concept[2].display = "III"
* concept[3].code = #IV
* concept[3].display = "IV"
* concept[4].code = #V
* concept[4].display = "V"
