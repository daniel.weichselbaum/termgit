Instance: ems-genogruppe 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-genogruppe" 
* name = "ems-genogruppe" 
* title = "EMS_Genogruppe" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Liste Genogruppe" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.70" 
* date = "2017-01-26" 
* count = 5 
* #I "I"
* #II "II"
* #III "III"
* #IV "IV"
* #V "V"
