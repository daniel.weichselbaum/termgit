Instance: ems-tbc-resultat 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-tbc-resultat" 
* name = "ems-tbc-resultat" 
* title = "EMS_TBC_Resultat" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Histologieergebnis: Verwendung in Histologie-Resultat, Kultur-Resultat, Mikroskopie Resulatat, NAT Resultat" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.75" 
* date = "2017-01-26" 
* count = 4 
* #EA "Ergebnis ausständig"
* #NA "nicht anwendbar"
* #NE "Nachweis erfolgt"
* #NNE "Nachweis nicht erfolgt"
