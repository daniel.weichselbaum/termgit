Instance: hl7-role-class 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-role-class" 
* name = "hl7-role-class" 
* title = "HL7 Role Class" 
* status = #active 
* content = #complete 
* version = "HL7:RoleClass" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.5.110" 
* date = "2013-01-10" 
* count = 108 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #CHILD "child"
* #CRED "credentialed entity"
* #NURPRAC "nurse practitioner"
* #NURS "nurse"
* #PA "physician assistant"
* #PHYS "physician"
* #ROL "role"
* #ROL ^property[0].code = #child 
* #ROL ^property[0].valueCode = #_RoleClassAssociative 
* #ROL ^property[1].code = #child 
* #ROL ^property[1].valueCode = #_RoleClassOntological 
* #ROL ^property[2].code = #child 
* #ROL ^property[2].valueCode = #_RoleClassPartitive 
* #_RoleClassAssociative "RoleClassAssociative"
* #_RoleClassAssociative ^property[0].code = #parent 
* #_RoleClassAssociative ^property[0].valueCode = #ROL 
* #_RoleClassAssociative ^property[1].code = #child 
* #_RoleClassAssociative ^property[1].valueCode = #_RoleClassMutualRelationship 
* #_RoleClassAssociative ^property[2].code = #child 
* #_RoleClassAssociative ^property[2].valueCode = #_RoleClassPassive 
* #_RoleClassMutualRelationship "RoleClassMutualRelationship"
* #_RoleClassMutualRelationship ^property[0].code = #parent 
* #_RoleClassMutualRelationship ^property[0].valueCode = #_RoleClassAssociative 
* #_RoleClassMutualRelationship ^property[1].code = #child 
* #_RoleClassMutualRelationship ^property[1].valueCode = #CAREGIVER 
* #_RoleClassMutualRelationship ^property[2].code = #child 
* #_RoleClassMutualRelationship ^property[2].valueCode = #PRS 
* #_RoleClassMutualRelationship ^property[3].code = #child 
* #_RoleClassMutualRelationship ^property[3].valueCode = #_RoleClassRelationshipFormal 
* #CAREGIVER "caregiver"
* #CAREGIVER ^property[0].code = #parent 
* #CAREGIVER ^property[0].valueCode = #_RoleClassMutualRelationship 
* #PRS "personal relationship"
* #PRS ^property[0].code = #parent 
* #PRS ^property[0].valueCode = #_RoleClassMutualRelationship 
* #_RoleClassRelationshipFormal "RoleClassRelationshipFormal"
* #_RoleClassRelationshipFormal ^property[0].code = #parent 
* #_RoleClassRelationshipFormal ^property[0].valueCode = #_RoleClassMutualRelationship 
* #_RoleClassRelationshipFormal ^property[1].code = #child 
* #_RoleClassRelationshipFormal ^property[1].valueCode = #AFFL 
* #_RoleClassRelationshipFormal ^property[2].code = #child 
* #_RoleClassRelationshipFormal ^property[2].valueCode = #AGNT 
* #_RoleClassRelationshipFormal ^property[3].code = #child 
* #_RoleClassRelationshipFormal ^property[3].valueCode = #CIT 
* #_RoleClassRelationshipFormal ^property[4].code = #child 
* #_RoleClassRelationshipFormal ^property[4].valueCode = #COVPTY 
* #_RoleClassRelationshipFormal ^property[5].code = #child 
* #_RoleClassRelationshipFormal ^property[5].valueCode = #CRINV 
* #_RoleClassRelationshipFormal ^property[6].code = #child 
* #_RoleClassRelationshipFormal ^property[6].valueCode = #CRSPNSR 
* #_RoleClassRelationshipFormal ^property[7].code = #child 
* #_RoleClassRelationshipFormal ^property[7].valueCode = #EMP 
* #_RoleClassRelationshipFormal ^property[8].code = #child 
* #_RoleClassRelationshipFormal ^property[8].valueCode = #GUAR 
* #_RoleClassRelationshipFormal ^property[9].code = #child 
* #_RoleClassRelationshipFormal ^property[9].valueCode = #INVSBJ 
* #_RoleClassRelationshipFormal ^property[10].code = #child 
* #_RoleClassRelationshipFormal ^property[10].valueCode = #LIC 
* #_RoleClassRelationshipFormal ^property[11].code = #child 
* #_RoleClassRelationshipFormal ^property[11].valueCode = #PAT 
* #_RoleClassRelationshipFormal ^property[12].code = #child 
* #_RoleClassRelationshipFormal ^property[12].valueCode = #PAYEE 
* #_RoleClassRelationshipFormal ^property[13].code = #child 
* #_RoleClassRelationshipFormal ^property[13].valueCode = #PAYOR 
* #_RoleClassRelationshipFormal ^property[14].code = #child 
* #_RoleClassRelationshipFormal ^property[14].valueCode = #POLHOLD 
* #_RoleClassRelationshipFormal ^property[15].code = #child 
* #_RoleClassRelationshipFormal ^property[15].valueCode = #QUAL 
* #_RoleClassRelationshipFormal ^property[16].code = #child 
* #_RoleClassRelationshipFormal ^property[16].valueCode = #SPNSR 
* #_RoleClassRelationshipFormal ^property[17].code = #child 
* #_RoleClassRelationshipFormal ^property[17].valueCode = #STD 
* #_RoleClassRelationshipFormal ^property[18].code = #child 
* #_RoleClassRelationshipFormal ^property[18].valueCode = #UNDWRT 
* #AFFL "affiliate"
* #AFFL ^property[0].code = #parent 
* #AFFL ^property[0].valueCode = #_RoleClassRelationshipFormal 
* #AGNT "agent"
* #AGNT ^property[0].code = #parent 
* #AGNT ^property[0].valueCode = #_RoleClassRelationshipFormal 
* #AGNT ^property[1].code = #child 
* #AGNT ^property[1].valueCode = #ASSIGNED 
* #AGNT ^property[2].code = #child 
* #AGNT ^property[2].valueCode = #CON 
* #AGNT ^property[3].code = #child 
* #AGNT ^property[3].valueCode = #GUARD 
* #ASSIGNED "assigned entity"
* #ASSIGNED ^property[0].code = #parent 
* #ASSIGNED ^property[0].valueCode = #AGNT 
* #ASSIGNED ^property[1].code = #child 
* #ASSIGNED ^property[1].valueCode = #COMPAR 
* #ASSIGNED ^property[2].code = #child 
* #ASSIGNED ^property[2].valueCode = #SGNOFF 
* #COMPAR "commissioning party"
* #COMPAR ^property[0].code = #parent 
* #COMPAR ^property[0].valueCode = #ASSIGNED 
* #SGNOFF "signing authority or officer"
* #SGNOFF ^property[0].code = #parent 
* #SGNOFF ^property[0].valueCode = #ASSIGNED 
* #CON "contact"
* #CON ^property[0].code = #parent 
* #CON ^property[0].valueCode = #AGNT 
* #CON ^property[1].code = #child 
* #CON ^property[1].valueCode = #ECON 
* #CON ^property[2].code = #child 
* #CON ^property[2].valueCode = #NOK 
* #ECON "emergency contact"
* #ECON ^property[0].code = #parent 
* #ECON ^property[0].valueCode = #CON 
* #NOK "next of kin"
* #NOK ^property[0].code = #parent 
* #NOK ^property[0].valueCode = #CON 
* #GUARD "guardian"
* #GUARD ^property[0].code = #parent 
* #GUARD ^property[0].valueCode = #AGNT 
* #CIT "citizen"
* #CIT ^property[0].code = #parent 
* #CIT ^property[0].valueCode = #_RoleClassRelationshipFormal 
* #COVPTY "covered party"
* #COVPTY ^property[0].code = #parent 
* #COVPTY ^property[0].valueCode = #_RoleClassRelationshipFormal 
* #COVPTY ^property[1].code = #child 
* #COVPTY ^property[1].valueCode = #CLAIM 
* #COVPTY ^property[2].code = #child 
* #COVPTY ^property[2].valueCode = #NAMED 
* #COVPTY ^property[3].code = #child 
* #COVPTY ^property[3].valueCode = #PROG 
* #CLAIM "claimant"
* #CLAIM ^property[0].code = #parent 
* #CLAIM ^property[0].valueCode = #COVPTY 
* #NAMED "named insured"
* #NAMED ^property[0].code = #parent 
* #NAMED ^property[0].valueCode = #COVPTY 
* #NAMED ^property[1].code = #child 
* #NAMED ^property[1].valueCode = #DEPEN 
* #NAMED ^property[2].code = #child 
* #NAMED ^property[2].valueCode = #INDIV 
* #NAMED ^property[3].code = #child 
* #NAMED ^property[3].valueCode = #SUBSCR 
* #DEPEN "dependent"
* #DEPEN ^property[0].code = #parent 
* #DEPEN ^property[0].valueCode = #NAMED 
* #INDIV "individual"
* #INDIV ^property[0].code = #parent 
* #INDIV ^property[0].valueCode = #NAMED 
* #SUBSCR "subscriber"
* #SUBSCR ^property[0].code = #parent 
* #SUBSCR ^property[0].valueCode = #NAMED 
* #PROG "program eligible"
* #PROG ^property[0].code = #parent 
* #PROG ^property[0].valueCode = #COVPTY 
* #CRINV "clinical research investigator"
* #CRINV ^property[0].code = #parent 
* #CRINV ^property[0].valueCode = #_RoleClassRelationshipFormal 
* #CRSPNSR "clinical research sponsor"
* #CRSPNSR ^property[0].code = #parent 
* #CRSPNSR ^property[0].valueCode = #_RoleClassRelationshipFormal 
* #EMP "employee"
* #EMP ^property[0].code = #parent 
* #EMP ^property[0].valueCode = #_RoleClassRelationshipFormal 
* #EMP ^property[1].code = #child 
* #EMP ^property[1].valueCode = #MIL 
* #MIL "military person"
* #MIL ^property[0].code = #parent 
* #MIL ^property[0].valueCode = #EMP 
* #GUAR "guarantor"
* #GUAR ^property[0].code = #parent 
* #GUAR ^property[0].valueCode = #_RoleClassRelationshipFormal 
* #INVSBJ "Investigation Subject"
* #INVSBJ ^property[0].code = #parent 
* #INVSBJ ^property[0].valueCode = #_RoleClassRelationshipFormal 
* #INVSBJ ^property[1].code = #child 
* #INVSBJ ^property[1].valueCode = #CASEBJ 
* #INVSBJ ^property[2].code = #child 
* #INVSBJ ^property[2].valueCode = #RESBJ 
* #CASEBJ "Case Subject"
* #CASEBJ ^property[0].code = #parent 
* #CASEBJ ^property[0].valueCode = #INVSBJ 
* #RESBJ "research subject"
* #RESBJ ^property[0].code = #parent 
* #RESBJ ^property[0].valueCode = #INVSBJ 
* #LIC "licensed entity"
* #LIC ^property[0].code = #parent 
* #LIC ^property[0].valueCode = #_RoleClassRelationshipFormal 
* #LIC ^property[1].code = #child 
* #LIC ^property[1].valueCode = #NOT 
* #LIC ^property[2].code = #child 
* #LIC ^property[2].valueCode = #PROV 
* #NOT "notary public"
* #NOT ^property[0].code = #parent 
* #NOT ^property[0].valueCode = #LIC 
* #PROV "healthcare provider"
* #PROV ^property[0].code = #parent 
* #PROV ^property[0].valueCode = #LIC 
* #PAT "patient"
* #PAT ^property[0].code = #parent 
* #PAT ^property[0].valueCode = #_RoleClassRelationshipFormal 
* #PAYEE "payee"
* #PAYEE ^property[0].code = #parent 
* #PAYEE ^property[0].valueCode = #_RoleClassRelationshipFormal 
* #PAYOR "invoice payor"
* #PAYOR ^property[0].code = #parent 
* #PAYOR ^property[0].valueCode = #_RoleClassRelationshipFormal 
* #POLHOLD "policy holder"
* #POLHOLD ^property[0].code = #parent 
* #POLHOLD ^property[0].valueCode = #_RoleClassRelationshipFormal 
* #QUAL "qualified entity"
* #QUAL ^property[0].code = #parent 
* #QUAL ^property[0].valueCode = #_RoleClassRelationshipFormal 
* #SPNSR "coverage sponsor"
* #SPNSR ^property[0].code = #parent 
* #SPNSR ^property[0].valueCode = #_RoleClassRelationshipFormal 
* #STD "student"
* #STD ^property[0].code = #parent 
* #STD ^property[0].valueCode = #_RoleClassRelationshipFormal 
* #UNDWRT "underwriter"
* #UNDWRT ^property[0].code = #parent 
* #UNDWRT ^property[0].valueCode = #_RoleClassRelationshipFormal 
* #_RoleClassPassive "RoleClassPassive"
* #_RoleClassPassive ^property[0].code = #parent 
* #_RoleClassPassive ^property[0].valueCode = #_RoleClassAssociative 
* #_RoleClassPassive ^property[1].code = #child 
* #_RoleClassPassive ^property[1].valueCode = #ADJY 
* #_RoleClassPassive ^property[2].code = #child 
* #_RoleClassPassive ^property[2].valueCode = #ADMM 
* #_RoleClassPassive ^property[3].code = #child 
* #_RoleClassPassive ^property[3].valueCode = #BIRTHPL 
* #_RoleClassPassive ^property[4].code = #child 
* #_RoleClassPassive ^property[4].valueCode = #DEATHPLC 
* #_RoleClassPassive ^property[5].code = #child 
* #_RoleClassPassive ^property[5].valueCode = #DST 
* #_RoleClassPassive ^property[6].code = #child 
* #_RoleClassPassive ^property[6].valueCode = #EXPR 
* #_RoleClassPassive ^property[7].code = #child 
* #_RoleClassPassive ^property[7].valueCode = #HLD 
* #_RoleClassPassive ^property[8].code = #child 
* #_RoleClassPassive ^property[8].valueCode = #HLTHCHRT 
* #_RoleClassPassive ^property[9].code = #child 
* #_RoleClassPassive ^property[9].valueCode = #IDENT 
* #_RoleClassPassive ^property[10].code = #child 
* #_RoleClassPassive ^property[10].valueCode = #MANU 
* #_RoleClassPassive ^property[11].code = #child 
* #_RoleClassPassive ^property[11].valueCode = #MNT 
* #_RoleClassPassive ^property[12].code = #child 
* #_RoleClassPassive ^property[12].valueCode = #OWN 
* #_RoleClassPassive ^property[13].code = #child 
* #_RoleClassPassive ^property[13].valueCode = #RGPR 
* #_RoleClassPassive ^property[14].code = #child 
* #_RoleClassPassive ^property[14].valueCode = #SDLOC 
* #_RoleClassPassive ^property[15].code = #child 
* #_RoleClassPassive ^property[15].valueCode = #TERR 
* #_RoleClassPassive ^property[16].code = #child 
* #_RoleClassPassive ^property[16].valueCode = #USED 
* #_RoleClassPassive ^property[17].code = #child 
* #_RoleClassPassive ^property[17].valueCode = #WRTE 
* #ADJY "adjacency"
* #ADJY ^property[0].code = #parent 
* #ADJY ^property[0].valueCode = #_RoleClassPassive 
* #ADJY ^property[1].code = #child 
* #ADJY ^property[1].valueCode = #CONC 
* #CONC "connection"
* #CONC ^property[0].code = #parent 
* #CONC ^property[0].valueCode = #ADJY 
* #CONC ^property[1].code = #child 
* #CONC ^property[1].valueCode = #BOND 
* #CONC ^property[2].code = #child 
* #CONC ^property[2].valueCode = #CONY 
* #BOND "molecular bond"
* #BOND ^property[0].code = #parent 
* #BOND ^property[0].valueCode = #CONC 
* #CONY "continuity"
* #CONY ^property[0].code = #parent 
* #CONY ^property[0].valueCode = #CONC 
* #ADMM "Administerable Material"
* #ADMM ^property[0].code = #parent 
* #ADMM ^property[0].valueCode = #_RoleClassPassive 
* #BIRTHPL "birthplace"
* #BIRTHPL ^property[0].code = #parent 
* #BIRTHPL ^property[0].valueCode = #_RoleClassPassive 
* #DEATHPLC "place of death"
* #DEATHPLC ^property[0].code = #parent 
* #DEATHPLC ^property[0].valueCode = #_RoleClassPassive 
* #DST "distributed material"
* #DST ^property[0].code = #parent 
* #DST ^property[0].valueCode = #_RoleClassPassive 
* #DST ^property[1].code = #child 
* #DST ^property[1].valueCode = #RET 
* #RET "retailed material"
* #RET ^property[0].code = #parent 
* #RET ^property[0].valueCode = #DST 
* #EXPR "exposed entity"
* #EXPR ^property[0].code = #parent 
* #EXPR ^property[0].valueCode = #_RoleClassPassive 
* #HLD "held entity"
* #HLD ^property[0].code = #parent 
* #HLD ^property[0].valueCode = #_RoleClassPassive 
* #HLTHCHRT "health chart"
* #HLTHCHRT ^property[0].code = #parent 
* #HLTHCHRT ^property[0].valueCode = #_RoleClassPassive 
* #IDENT "identified entity"
* #IDENT ^property[0].code = #parent 
* #IDENT ^property[0].valueCode = #_RoleClassPassive 
* #MANU "manufactured product"
* #MANU ^property[0].code = #parent 
* #MANU ^property[0].valueCode = #_RoleClassPassive 
* #MANU ^property[1].code = #child 
* #MANU ^property[1].valueCode = #THER 
* #THER "therapeutic agent"
* #THER ^property[0].code = #parent 
* #THER ^property[0].valueCode = #MANU 
* #MNT "maintained entity"
* #MNT ^property[0].code = #parent 
* #MNT ^property[0].valueCode = #_RoleClassPassive 
* #OWN "owned entity"
* #OWN ^property[0].code = #parent 
* #OWN ^property[0].valueCode = #_RoleClassPassive 
* #RGPR "regulated product"
* #RGPR ^property[0].code = #parent 
* #RGPR ^property[0].valueCode = #_RoleClassPassive 
* #SDLOC "service delivery location"
* #SDLOC ^property[0].code = #parent 
* #SDLOC ^property[0].valueCode = #_RoleClassPassive 
* #SDLOC ^property[1].code = #child 
* #SDLOC ^property[1].valueCode = #DSDLOC 
* #SDLOC ^property[2].code = #child 
* #SDLOC ^property[2].valueCode = #ISDLOC 
* #DSDLOC "dedicated service delivery location"
* #DSDLOC ^property[0].code = #parent 
* #DSDLOC ^property[0].valueCode = #SDLOC 
* #ISDLOC "incidental service delivery location"
* #ISDLOC ^property[0].code = #parent 
* #ISDLOC ^property[0].valueCode = #SDLOC 
* #TERR "territory of authority"
* #TERR ^property[0].code = #parent 
* #TERR ^property[0].valueCode = #_RoleClassPassive 
* #USED "used entity"
* #USED ^property[0].code = #parent 
* #USED ^property[0].valueCode = #_RoleClassPassive 
* #WRTE "warranted product"
* #WRTE ^property[0].code = #parent 
* #WRTE ^property[0].valueCode = #_RoleClassPassive 
* #_RoleClassOntological "RoleClassOntological"
* #_RoleClassOntological ^property[0].code = #parent 
* #_RoleClassOntological ^property[0].valueCode = #ROL 
* #_RoleClassOntological ^property[1].code = #child 
* #_RoleClassOntological ^property[1].valueCode = #EQUIV 
* #_RoleClassOntological ^property[2].code = #child 
* #_RoleClassOntological ^property[2].valueCode = #GEN 
* #_RoleClassOntological ^property[3].code = #child 
* #_RoleClassOntological ^property[3].valueCode = #INST 
* #_RoleClassOntological ^property[4].code = #child 
* #_RoleClassOntological ^property[4].valueCode = #SUBS 
* #EQUIV "equivalent entity"
* #EQUIV ^property[0].code = #parent 
* #EQUIV ^property[0].valueCode = #_RoleClassOntological 
* #EQUIV ^property[1].code = #child 
* #EQUIV ^property[1].valueCode = #SAME 
* #EQUIV ^property[2].code = #child 
* #EQUIV ^property[2].valueCode = #SUBY 
* #SAME "same"
* #SAME ^property[0].code = #parent 
* #SAME ^property[0].valueCode = #EQUIV 
* #SUBY "subsumed by"
* #SUBY ^property[0].code = #parent 
* #SUBY ^property[0].valueCode = #EQUIV 
* #GEN "has generalization"
* #GEN ^property[0].code = #parent 
* #GEN ^property[0].valueCode = #_RoleClassOntological 
* #GEN ^property[1].code = #child 
* #GEN ^property[1].valueCode = #GRIC 
* #GRIC "has generic"
* #GRIC ^property[0].code = #parent 
* #GRIC ^property[0].valueCode = #GEN 
* #INST "instance"
* #INST ^property[0].code = #parent 
* #INST ^property[0].valueCode = #_RoleClassOntological 
* #SUBS "subsumer"
* #SUBS ^property[0].code = #parent 
* #SUBS ^property[0].valueCode = #_RoleClassOntological 
* #_RoleClassPartitive "RoleClassPartitive"
* #_RoleClassPartitive ^property[0].code = #parent 
* #_RoleClassPartitive ^property[0].valueCode = #ROL 
* #_RoleClassPartitive ^property[1].code = #child 
* #_RoleClassPartitive ^property[1].valueCode = #CONT 
* #_RoleClassPartitive ^property[2].code = #child 
* #_RoleClassPartitive ^property[2].valueCode = #EXPAGTCAR 
* #_RoleClassPartitive ^property[3].code = #child 
* #_RoleClassPartitive ^property[3].valueCode = #INGR 
* #_RoleClassPartitive ^property[4].code = #child 
* #_RoleClassPartitive ^property[4].valueCode = #LOCE 
* #_RoleClassPartitive ^property[5].code = #child 
* #_RoleClassPartitive ^property[5].valueCode = #MBR 
* #_RoleClassPartitive ^property[6].code = #child 
* #_RoleClassPartitive ^property[6].valueCode = #PART 
* #_RoleClassPartitive ^property[7].code = #child 
* #_RoleClassPartitive ^property[7].valueCode = #SPEC 
* #CONT "content"
* #CONT ^property[0].code = #parent 
* #CONT ^property[0].valueCode = #_RoleClassPartitive 
* #EXPAGTCAR "exposure agent carrier"
* #EXPAGTCAR ^property[0].code = #parent 
* #EXPAGTCAR ^property[0].valueCode = #_RoleClassPartitive 
* #EXPAGTCAR ^property[1].code = #child 
* #EXPAGTCAR ^property[1].valueCode = #EXPVECTOR 
* #EXPAGTCAR ^property[2].code = #child 
* #EXPAGTCAR ^property[2].valueCode = #FOMITE 
* #EXPVECTOR "exposure vector"
* #EXPVECTOR ^property[0].code = #parent 
* #EXPVECTOR ^property[0].valueCode = #EXPAGTCAR 
* #FOMITE "fomite"
* #FOMITE ^property[0].code = #parent 
* #FOMITE ^property[0].valueCode = #EXPAGTCAR 
* #INGR "ingredient"
* #INGR ^property[0].code = #parent 
* #INGR ^property[0].valueCode = #_RoleClassPartitive 
* #INGR ^property[1].code = #child 
* #INGR ^property[1].valueCode = #ACTI 
* #INGR ^property[2].code = #child 
* #INGR ^property[2].valueCode = #ADJV 
* #INGR ^property[3].code = #child 
* #INGR ^property[3].valueCode = #ADTV 
* #INGR ^property[4].code = #child 
* #INGR ^property[4].valueCode = #BASE 
* #INGR ^property[5].code = #child 
* #INGR ^property[5].valueCode = #IACT 
* #INGR ^property[6].code = #child 
* #INGR ^property[6].valueCode = #MECH 
* #ACTI "active ingredient"
* #ACTI ^property[0].code = #parent 
* #ACTI ^property[0].valueCode = #INGR 
* #ACTI ^property[1].code = #child 
* #ACTI ^property[1].valueCode = #ACTIB 
* #ACTI ^property[2].code = #child 
* #ACTI ^property[2].valueCode = #ACTIM 
* #ACTI ^property[3].code = #child 
* #ACTI ^property[3].valueCode = #ACTIR 
* #ACTIB "active ingredient - basis of strength"
* #ACTIB ^property[0].code = #parent 
* #ACTIB ^property[0].valueCode = #ACTI 
* #ACTIM "active ingredient - moiety is basis of strength"
* #ACTIM ^property[0].code = #parent 
* #ACTIM ^property[0].valueCode = #ACTI 
* #ACTIR "active ingredient - reference substance is basis of strength"
* #ACTIR ^property[0].code = #parent 
* #ACTIR ^property[0].valueCode = #ACTI 
* #ADJV "adjuvant"
* #ADJV ^property[0].code = #parent 
* #ADJV ^property[0].valueCode = #INGR 
* #ADTV "additive"
* #ADTV ^property[0].code = #parent 
* #ADTV ^property[0].valueCode = #INGR 
* #BASE "base"
* #BASE ^property[0].code = #parent 
* #BASE ^property[0].valueCode = #INGR 
* #IACT "inactive ingredient"
* #IACT ^property[0].code = #parent 
* #IACT ^property[0].valueCode = #INGR 
* #IACT ^property[1].code = #child 
* #IACT ^property[1].valueCode = #COLR 
* #IACT ^property[2].code = #child 
* #IACT ^property[2].valueCode = #FLVR 
* #IACT ^property[3].code = #child 
* #IACT ^property[3].valueCode = #PRSV 
* #IACT ^property[4].code = #child 
* #IACT ^property[4].valueCode = #STBL 
* #COLR "color additive"
* #COLR ^property[0].code = #parent 
* #COLR ^property[0].valueCode = #IACT 
* #FLVR "flavor additive"
* #FLVR ^property[0].code = #parent 
* #FLVR ^property[0].valueCode = #IACT 
* #PRSV "preservative"
* #PRSV ^property[0].code = #parent 
* #PRSV ^property[0].valueCode = #IACT 
* #STBL "stabilizer"
* #STBL ^property[0].code = #parent 
* #STBL ^property[0].valueCode = #IACT 
* #MECH "mechanical ingredient"
* #MECH ^property[0].code = #parent 
* #MECH ^property[0].valueCode = #INGR 
* #LOCE "located entity"
* #LOCE ^property[0].code = #parent 
* #LOCE ^property[0].valueCode = #_RoleClassPartitive 
* #LOCE ^property[1].code = #child 
* #LOCE ^property[1].valueCode = #STOR 
* #STOR "stored entity"
* #STOR ^property[0].code = #parent 
* #STOR ^property[0].valueCode = #LOCE 
* #MBR "member"
* #MBR ^property[0].code = #parent 
* #MBR ^property[0].valueCode = #_RoleClassPartitive 
* #PART "part"
* #PART ^property[0].code = #parent 
* #PART ^property[0].valueCode = #_RoleClassPartitive 
* #PART ^property[1].code = #child 
* #PART ^property[1].valueCode = #ACTM 
* #ACTM "active moiety"
* #ACTM ^property[0].code = #parent 
* #ACTM ^property[0].valueCode = #PART 
* #SPEC "specimen"
* #SPEC ^property[0].code = #parent 
* #SPEC ^property[0].valueCode = #_RoleClassPartitive 
* #SPEC ^property[1].code = #child 
* #SPEC ^property[1].valueCode = #ALQT 
* #SPEC ^property[2].code = #child 
* #SPEC ^property[2].valueCode = #ISLT 
* #ALQT "aliquot"
* #ALQT ^property[0].code = #parent 
* #ALQT ^property[0].valueCode = #SPEC 
* #ISLT "isolate"
* #ISLT ^property[0].code = #parent 
* #ISLT ^property[0].valueCode = #SPEC 
