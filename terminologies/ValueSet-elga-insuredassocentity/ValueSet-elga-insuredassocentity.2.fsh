Instance: elga-insuredassocentity 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-insuredassocentity" 
* name = "elga-insuredassocentity" 
* title = "ELGA_InsuredAssocEntity" 
* status = #active 
* version = "2.6" 
* description = "**Description:** Codes for a further detailed description of the insurance relationship. Excerpt of the 'CoverageRoleType' of the Rolecode-Codelist. Currently the ELGA_InsuredAssocEntity value set only uses two codes from the parent code system.

**Beschreibung:** Codes zur näheren Beschreibung des Versicherungsverhältnisses. Auszug aus dem Bereich 'CoverageRoleType' der Codeliste RoleCode. Derzeit werden nur zwei Codes aus der Liste verwendet." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.9" 
* date = "2013-01-10" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.hl7.org" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-hl7-role-code"
* compose.include[0].concept[0].code = "FAMDEP"
* compose.include[0].concept[0].display = "family dependent"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "Mitversichert" 
* compose.include[0].concept[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[0].designation[1].value = "Patient ist bei einem Familienmitglied mitversichert" 
* compose.include[0].concept[1].code = "SELF"
* compose.include[0].concept[1].display = "self"
* compose.include[0].concept[1].designation[0].language = #de-AT 
* compose.include[0].concept[1].designation[0].value = "Selbstversichert" 
* compose.include[0].concept[1].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[1].designation[1].value = "Patient ist selbst der Versicherte" 
