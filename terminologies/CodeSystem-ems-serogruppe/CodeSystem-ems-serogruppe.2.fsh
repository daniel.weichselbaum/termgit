Instance: ems-serogruppe 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-serogruppe" 
* name = "ems-serogruppe" 
* title = "EMS_Serogruppe" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** Serogruppe" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.90" 
* date = "2017-01-26" 
* count = 23 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #CHOLERA "Cholera"
* #CHOLERA ^property[0].code = #child 
* #CHOLERA ^property[0].valueCode = #NO1_NO139 
* #CHOLERA ^property[1].code = #child 
* #CHOLERA ^property[1].valueCode = #O1 
* #CHOLERA ^property[2].code = #child 
* #CHOLERA ^property[2].valueCode = #O139 
* #NO1_NO139 "non-O1 / non-O139"
* #NO1_NO139 ^property[0].code = #parent 
* #NO1_NO139 ^property[0].valueCode = #CHOLERA 
* #O1 "O1"
* #O1 ^property[0].code = #parent 
* #O1 ^property[0].valueCode = #CHOLERA 
* #O139 "O139"
* #O139 ^property[0].code = #parent 
* #O139 ^property[0].valueCode = #CHOLERA 
* #LISTERIOSE "Listeriose"
* #LISTERIOSE ^property[0].code = #child 
* #LISTERIOSE ^property[0].valueCode = #IIa 
* #LISTERIOSE ^property[1].code = #child 
* #LISTERIOSE ^property[1].valueCode = #IIb 
* #LISTERIOSE ^property[2].code = #child 
* #LISTERIOSE ^property[2].valueCode = #IIc 
* #LISTERIOSE ^property[3].code = #child 
* #LISTERIOSE ^property[3].valueCode = #IVb 
* #LISTERIOSE ^property[4].code = #child 
* #LISTERIOSE ^property[4].valueCode = #L 
* #LISTERIOSE ^property[5].code = #child 
* #LISTERIOSE ^property[5].valueCode = #nicht_anwendbar 
* #IIa "IIA"
* #IIa ^property[0].code = #parent 
* #IIa ^property[0].valueCode = #LISTERIOSE 
* #IIb "IIB"
* #IIb ^property[0].code = #parent 
* #IIb ^property[0].valueCode = #LISTERIOSE 
* #IIc "IIC"
* #IIc ^property[0].code = #parent 
* #IIc ^property[0].valueCode = #LISTERIOSE 
* #IVb "IVB"
* #IVb ^property[0].code = #parent 
* #IVb ^property[0].valueCode = #LISTERIOSE 
* #L "L"
* #L ^property[0].code = #parent 
* #L ^property[0].valueCode = #LISTERIOSE 
* #nicht_anwendbar "NA"
* #nicht_anwendbar ^property[0].code = #parent 
* #nicht_anwendbar ^property[0].valueCode = #LISTERIOSE 
* #MENINGOKOKKEN "Meningokokken"
* #MENINGOKOKKEN ^property[0].code = #child 
* #MENINGOKOKKEN ^property[0].valueCode = #Nicht_durchgeführt 
* #MENINGOKOKKEN ^property[1].code = #child 
* #MENINGOKOKKEN ^property[1].valueCode = #not_groupable 
* #MENINGOKOKKEN ^property[2].code = #child 
* #MENINGOKOKKEN ^property[2].valueCode = #serogroup_29E 
* #MENINGOKOKKEN ^property[3].code = #child 
* #MENINGOKOKKEN ^property[3].valueCode = #serogroup_A 
* #MENINGOKOKKEN ^property[4].code = #child 
* #MENINGOKOKKEN ^property[4].valueCode = #serogroup_B 
* #MENINGOKOKKEN ^property[5].code = #child 
* #MENINGOKOKKEN ^property[5].valueCode = #serogroup_C 
* #MENINGOKOKKEN ^property[6].code = #child 
* #MENINGOKOKKEN ^property[6].valueCode = #serogroup_W135 
* #MENINGOKOKKEN ^property[7].code = #child 
* #MENINGOKOKKEN ^property[7].valueCode = #serogroup_X_ 
* #MENINGOKOKKEN ^property[8].code = #child 
* #MENINGOKOKKEN ^property[8].valueCode = #serogroup_Y 
* #MENINGOKOKKEN ^property[9].code = #child 
* #MENINGOKOKKEN ^property[9].valueCode = #serogroup_Z 
* #MENINGOKOKKEN ^property[10].code = #child 
* #MENINGOKOKKEN ^property[10].valueCode = #serogroup_Z/29E 
* #Nicht_durchgeführt "NOTEST"
* #Nicht_durchgeführt ^property[0].code = #parent 
* #Nicht_durchgeführt ^property[0].valueCode = #MENINGOKOKKEN 
* #not_groupable "NOGRP"
* #not_groupable ^property[0].code = #parent 
* #not_groupable ^property[0].valueCode = #MENINGOKOKKEN 
* #serogroup_29E "29E"
* #serogroup_29E ^property[0].code = #parent 
* #serogroup_29E ^property[0].valueCode = #MENINGOKOKKEN 
* #serogroup_A "A"
* #serogroup_A ^property[0].code = #parent 
* #serogroup_A ^property[0].valueCode = #MENINGOKOKKEN 
* #serogroup_B "B"
* #serogroup_B ^property[0].code = #parent 
* #serogroup_B ^property[0].valueCode = #MENINGOKOKKEN 
* #serogroup_C "C"
* #serogroup_C ^property[0].code = #parent 
* #serogroup_C ^property[0].valueCode = #MENINGOKOKKEN 
* #serogroup_W135 "W135"
* #serogroup_W135 ^property[0].code = #parent 
* #serogroup_W135 ^property[0].valueCode = #MENINGOKOKKEN 
* #serogroup_X_ "X"
* #serogroup_X_ ^property[0].code = #parent 
* #serogroup_X_ ^property[0].valueCode = #MENINGOKOKKEN 
* #serogroup_Y "Y"
* #serogroup_Y ^property[0].code = #parent 
* #serogroup_Y ^property[0].valueCode = #MENINGOKOKKEN 
* #serogroup_Z "Z"
* #serogroup_Z ^property[0].code = #parent 
* #serogroup_Z ^property[0].valueCode = #MENINGOKOKKEN 
* #serogroup_Z/29E "Z/29E"
* #serogroup_Z/29E ^property[0].code = #parent 
* #serogroup_Z/29E ^property[0].valueCode = #MENINGOKOKKEN 
