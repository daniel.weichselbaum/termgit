Instance: ems-materialmethode 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-materialmethode" 
* name = "ems-materialmethode" 
* title = "EMS_MaterialMethode" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** Diese Codeliste definiert Inhalte für procedure/methodCode" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.99" 
* date = "2017-01-26" 
* count = 8 
* concept[0].code = #HIST
* concept[0].display = "Methode Material Histologie"
* concept[1].code = #KULTUR
* concept[1].display = "Methode Kultur Material"
* concept[2].code = #MAT1
* concept[2].display = "Primary Sample"
* concept[3].code = #MAT2
* concept[3].display = "Secondary Sample"
* concept[4].code = #MIKRO
* concept[4].display = "Methode Mikroskopie Material"
* concept[5].code = #NAT
* concept[5].display = "Methode NAT Material"
* concept[6].code = #SERO
* concept[6].display = "Methode Material Serologie"
* concept[7].code = #VIRUS
* concept[7].display = "Methode Material Virusnachweis"
