Instance: ems-materialmethode 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-materialmethode" 
* name = "ems-materialmethode" 
* title = "EMS_MaterialMethode" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** Diese Codeliste definiert Inhalte für procedure/methodCode" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.99" 
* date = "2017-01-26" 
* count = 8 
* #HIST "Methode Material Histologie"
* #KULTUR "Methode Kultur Material"
* #MAT1 "Primary Sample"
* #MAT2 "Secondary Sample"
* #MIKRO "Methode Mikroskopie Material"
* #NAT "Methode NAT Material"
* #SERO "Methode Material Serologie"
* #VIRUS "Methode Material Virusnachweis"
