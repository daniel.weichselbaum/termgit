Instance: elga-practicesetting 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-practicesetting" 
* name = "elga-practicesetting" 
* title = "ELGA_PracticeSetting" 
* status = #active 
* content = #complete 
* version = "201907" 
* description = "**Description:** Describes the assignment of a document to a medical area. These codes are used within XDS Metadata attribute ''practiceSettingCode''.      Terminologies are inherited from ''Österreichische Strukturplan Gesundheit'' as far as possilbe. Opposite to this piece of work, some concepts are newly introduced. Where differentiation was reasonable, concepts were conducted in more detail. More concepts will be added if necessary, especially for physiotherapy, ergotherapy a.s.f.. A CDA implementation guide may specify the use of this codes.

**Beschreibung:** Beschreibt die fachlliche Zuordnung eines Dokumentes in ELGA. Verwendung in XDS Metadaten im Attribut ''practiceSettingCode''.      Die Benennungen sind soweit möglich aus der Nomenklatur des Österreichischen Strukturplan Gesundheit (ÖSG) übernommen. Gegenüber diesem Codewerk sind einige Konzepte neu hinzugefügt worden. Wo Unterscheidungen sinnvoll waren, wurden Konzepte auch genauer ausgeführt. Weitere Konzepte werden hinzugefügt, wenn Bedarf durch weitere CDA-Dokumentenklassen gegeben sein wird, zB für nicht-ärztliche Gesundheitsberufe wie Physiotherapie, Ergotherapie etc.     Ein CDA-Implementierungsleitfaden kann die Verwendung der möglichen PracticeSettingCodes vorschreiben.     Ein Mapping zu den LKF Funktionscodes ist angegeben, wenn möglich." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.12" 
* date = "2019-07-01" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* count = 43 
* property[0].code = #Relationships 
* property[0].type = #string 
* #F001 "Allgemeinmedizin"
* #F001 ^definition = Dokumente aus Allgemeinmedizinischen Organisationen
* #F001 ^designation[0].language = #de-AT 
* #F001 ^designation[0].value = "Allgemeinmedizin" 
* #F001 ^property[0].code = #Relationships 
* #F001 ^property[0].valueString = "LKF Funktionscode: 1- 91 11" 
* #F002 "Anästhesiologie und Intensivmedizin"
* #F002 ^definition = Anästhesiologie und Intensivmedizin
* #F002 ^designation[0].language = #de-AT 
* #F002 ^designation[0].value = "Anästhesiologie und Intensivmedizin" 
* #F002 ^property[0].code = #Relationships 
* #F002 ^property[0].valueString = "LKF Funktionscode: 1- 71 --" 
* #F005 "Augenheilkunde"
* #F005 ^definition = Augenheilkunde
* #F005 ^designation[0].language = #de-AT 
* #F005 ^designation[0].value = "Augenheilkunde" 
* #F005 ^property[0].code = #Relationships 
* #F005 ^property[0].valueString = "LKF Funktionscode: 1- 41 --" 
* #F006 "Blutgruppenserologie und Transfusionsmedizin"
* #F006 ^definition = Blutgruppenserologie und Transfusionsmedizin
* #F006 ^designation[0].language = #de-AT 
* #F006 ^designation[0].value = "Blutgruppenserologie und Transfusionsmedizin" 
* #F006 ^property[0].code = #Relationships 
* #F006 ^property[0].valueString = "LKF Funktionscode: 1- 71 12" 
* #F007 "Chirurgie"
* #F007 ^definition = Chirurgie (inklusive Gefäß- und Transplantationschirugie)
* #F007 ^designation[0].language = #de-AT 
* #F007 ^designation[0].value = "Chirurgie" 
* #F007 ^property[0].code = #Relationships 
* #F007 ^property[0].valueString = "LKF Funktionscode: 1- 21 -- (nicht 1- 21 14, 1- 21 16)" 
* #F010 "Gynäkologie und Geburtshilfe"
* #F010 ^definition = Frauenheilkunde (Gynäkologie), Geburtshilfe, inklusive Hebammentätigkeit, Wochenbettbetreuung, Schwangerenvorsorge
* #F010 ^designation[0].language = #de-AT 
* #F010 ^designation[0].value = "Gynäkologie und Geburtshilfe" 
* #F010 ^property[0].code = #Relationships 
* #F010 ^property[0].valueString = "LKF Funktionscode: 1- 31 --             LKF Funktionscode: 1- 32 --             LKF Funktionscode: 1- 33 --" 
* #F014 "Hals-, Nasen- und Ohrenheilkunde"
* #F014 ^definition = Hals-, Nasen- und Ohrenkrankheiten
* #F014 ^designation[0].language = #de-AT 
* #F014 ^designation[0].value = "Hals-, Nasen- und Ohrenheilkunde" 
* #F014 ^property[0].code = #Relationships 
* #F014 ^property[0].valueString = "LKF Funktionscode: 1- 42 --" 
* #F015 "Dermatologie"
* #F015 ^definition = Haut- und Geschlechtskrankheiten
* #F015 ^designation[0].language = #de-AT 
* #F015 ^designation[0].value = "Dermatologie" 
* #F015 ^property[0].code = #Relationships 
* #F015 ^property[0].valueString = "LKF Funktionscode: 1- 45 --" 
* #F016 "Mikrobiologie"
* #F016 ^definition = Dokumente aus dem Bereich mikrobiologisch-serologische Labordiagnostik
* #F016 ^designation[0].language = #de-AT 
* #F016 ^designation[0].value = "Mikrobiologie" 
* #F016 ^property[0].code = #Relationships 
* #F016 ^property[0].valueString = "LKF Funktionscode: 1- 83 --             LKF Funktionscode: 1- -- 79" 
* #F019 "Innere Medizin"
* #F019 ^definition = Innere Medizin
* #F019 ^designation[0].language = #de-AT 
* #F019 ^designation[0].value = "Innere Medizin" 
* #F019 ^property[0].code = #Relationships 
* #F019 ^property[0].valueString = "LKF Funktionscode: 1- 11 --" 
* #F023 "Interdisziplinärer Bereich"
* #F023 ^definition = Interdisziplinärer Bereich  (Eingeschränkter Geltungsbereich, nur wenn in einem CDA-Leitfaden vorgesehen)
* #F023 ^designation[0].language = #de-AT 
* #F023 ^designation[0].value = "Interdisziplinärer Bereich" 
* #F023 ^property[0].code = #Relationships 
* #F023 ^property[0].valueString = "LKF Funktionscode: 1- 91 --" 
* #F025 "Kinder- und Jugendpsychiatrie"
* #F025 ^definition = Kinder- und Jugendpsychiatrie
* #F025 ^designation[0].language = #de-AT 
* #F025 ^designation[0].value = "Kinder- und Jugendpsychiatrie" 
* #F025 ^property[0].code = #Relationships 
* #F025 ^property[0].valueString = "LKF Funktionscode: 1- 64 --" 
* #F026 "Kinder- und Jugendchirurgie"
* #F026 ^definition = Kinder-Chirurgie
* #F026 ^designation[0].language = #de-AT 
* #F026 ^designation[0].value = "Kinder- und Jugendchirurgie" 
* #F026 ^property[0].code = #Relationships 
* #F026 ^property[0].valueString = "LKF Funktionscode: 1- 27 --" 
* #F027 "Kinder- und Jugendheilkunde"
* #F027 ^definition = Kinderheilkunde
* #F027 ^designation[0].language = #de-AT 
* #F027 ^designation[0].value = "Kinder- und Jugendheilkunde" 
* #F027 ^property[0].code = #Relationships 
* #F027 ^property[0].valueString = "LKF Funktionscode: 1- 51 --" 
* #F028 "Labordiagnostik"
* #F028 ^definition = Dokumente aus dem Bereich medizinisch-chemische Labordiagnostik
* #F028 ^designation[0].language = #de-AT 
* #F028 ^designation[0].value = "Labordiagnostik" 
* #F028 ^property[0].code = #Relationships 
* #F028 ^property[0].valueString = "LKF Funktionscode: 1- 82 --" 
* #F029 "Mund-, Kiefer- und Gesichtschirurgie"
* #F029 ^definition = Mund-, Kiefer- und Gesichtschirurgie
* #F029 ^designation[0].language = #de-AT 
* #F029 ^designation[0].value = "Mund-, Kiefer- und Gesichtschirurgie" 
* #F029 ^property[0].code = #Relationships 
* #F029 ^property[0].valueString = "LKF Funktionscode: 1- 24 --" 
* #F031 "Neurochirurgie"
* #F031 ^definition = Neurochirurgie
* #F031 ^designation[0].language = #de-AT 
* #F031 ^designation[0].value = "Neurochirurgie" 
* #F031 ^property[0].code = #Relationships 
* #F031 ^property[0].valueString = "LKF Funktionscode: 1- 25 --" 
* #F032 "Neurologie"
* #F032 ^definition = Neurologie, Inkl. Stroke Unit sowie Neurologie Phase B, C
* #F032 ^designation[0].language = #de-AT 
* #F032 ^designation[0].value = "Neurologie" 
* #F032 ^property[0].code = #Relationships 
* #F032 ^property[0].valueString = "LKF Funktionscode: 1- 63 --" 
* #F033 "Nuklearmedizin"
* #F033 ^definition = Nuklearmedizin
* #F033 ^designation[0].language = #de-AT 
* #F033 ^designation[0].value = "Nuklearmedizin" 
* #F033 ^property[0].code = #Relationships 
* #F033 ^property[0].valueString = "LKF Funktionscode: 1- 75 --" 
* #F035 "Orthopädie und orthopädische Chirurgie"
* #F035 ^definition = Orthopädie und orthopädische Chirurgie
* #F035 ^designation[0].language = #de-AT 
* #F035 ^designation[0].value = "Orthopädie und orthopädische Chirurgie" 
* #F035 ^property[0].code = #Relationships 
* #F035 ^property[0].valueString = "LKF Funktionscode: 1- 23 --" 
* #F036 "Palliativmedizin"
* #F036 ^definition = Dokumente der palliativ-medizinischen Versorgung
* #F036 ^designation[0].language = #de-AT 
* #F036 ^designation[0].value = "Palliativmedizin" 
* #F036 ^property[0].code = #Relationships 
* #F036 ^property[0].valueString = "LKF Funktionscode: 1- 91 37" 
* #F037 "Pathologie"
* #F037 ^definition = Pathologie
* #F037 ^designation[0].language = #de-AT 
* #F037 ^designation[0].value = "Pathologie" 
* #F037 ^property[0].code = #Relationships 
* #F037 ^property[0].valueString = "LKF Funktionscode: 1- 81 --" 
* #F040 "Physikalische Medizin und Rehabilitation"
* #F040 ^definition = Physikalische Medizin
* #F040 ^designation[0].language = #de-AT 
* #F040 ^designation[0].value = "Physikalische Medizin und Rehabilitation" 
* #F040 ^property[0].code = #Relationships 
* #F040 ^property[0].valueString = "LKF Funktionscode: 1- 78 --" 
* #F041 "Plastische Chirurgie"
* #F041 ^definition = Plastische Chirurgie
* #F041 ^designation[0].language = #de-AT 
* #F041 ^designation[0].value = "Plastische Chirurgie" 
* #F041 ^property[0].code = #Relationships 
* #F041 ^property[0].valueString = "LKF Funktionscode: 1- 26 --" 
* #F042 "Psychiatrie"
* #F042 ^definition = Psychiatrie
* #F042 ^designation[0].language = #de-AT 
* #F042 ^designation[0].value = "Psychiatrie" 
* #F042 ^property[0].code = #Relationships 
* #F042 ^property[0].valueString = "LKF Funktionscode: 1- 62 --" 
* #F043 "Pulmologie"
* #F043 ^definition = Pulmologie (Lungenheilkunde)
* #F043 ^designation[0].language = #de-AT 
* #F043 ^designation[0].value = "Pulmologie" 
* #F043 ^property[0].code = #Relationships 
* #F043 ^property[0].valueString = "LKF Funktionscode: 1- 12 --" 
* #F044 "Radiologie"
* #F044 ^definition = Radiologie
* #F044 ^designation[0].language = #de-AT 
* #F044 ^designation[0].value = "Radiologie" 
* #F044 ^property[0].code = #Relationships 
* #F044 ^property[0].valueString = "LKF Funktionscode: 1- 72 --" 
* #F048 "Remobilisation/Nachsorge"
* #F048 ^definition = Remobilisation/Nachsorge
* #F048 ^designation[0].language = #de-AT 
* #F048 ^designation[0].value = "Remobilisation/Nachsorge" 
* #F048 ^property[0].code = #Relationships 
* #F048 ^property[0].valueString = "LKF Funktionscode: 1- 91 35" 
* #F052 "Unfallchirurgie"
* #F052 ^definition = Unfallchirurgie
* #F052 ^designation[0].language = #de-AT 
* #F052 ^designation[0].value = "Unfallchirurgie" 
* #F052 ^property[0].code = #Relationships 
* #F052 ^property[0].valueString = "LKF Funktionscode: 1- 22 --" 
* #F053 "Urologie"
* #F053 ^definition = Urologie
* #F053 ^designation[0].language = #de-AT 
* #F053 ^designation[0].value = "Urologie" 
* #F053 ^property[0].code = #Relationships 
* #F053 ^property[0].valueString = "LKF Funktionscode: 1- 43 --" 
* #F055 "Zahn-, Mund- und Kieferheilkunde"
* #F055 ^definition = Zahn-, Mund- und Kieferheilkunde
* #F055 ^designation[0].language = #de-AT 
* #F055 ^designation[0].value = "Zahn-, Mund- und Kieferheilkunde" 
* #F055 ^property[0].code = #Relationships 
* #F055 ^property[0].valueString = "LKF Funktionscode: 1- 48 --" 
* #F056 "Akutgeriatrie/Remobilisation"
* #F056 ^definition = Dokumente aus dem Bereich der Akutgeriatrie und Remobilisation
* #F056 ^designation[0].language = #de-AT 
* #F056 ^designation[0].value = "Akutgeriatrie/Remobilisation" 
* #F056 ^property[0].code = #Relationships 
* #F056 ^property[0].valueString = "LKF Funktionscode: 1- 11 36             LKF Funktionscode: 1- 63 36" 
* #F057 "Gesundheits- und Krankenpflege"
* #F057 ^definition = Gesundheits- und Krankenpflegerische Dokumente (nicht zu verwenden für Dokumente aus dem Krankenhausbereich)
* #F057 ^designation[0].language = #de-AT 
* #F057 ^designation[0].value = "Gesundheits- und Krankenpflege" 
* #F058 "Herzchirurgie"
* #F058 ^definition = Herzchirurgie
* #F058 ^designation[0].language = #de-AT 
* #F058 ^designation[0].value = "Herzchirurgie" 
* #F058 ^property[0].code = #Relationships 
* #F058 ^property[0].valueString = "LKF Funktionscode: 1- 21 14" 
* #F059 "Klinische Psychologie"
* #F059 ^definition = Klinische Psychologie
* #F059 ^designation[0].language = #de-AT 
* #F059 ^designation[0].value = "Klinische Psychologie" 
* #F059 ^property[0].code = #Relationships 
* #F059 ^property[0].valueString = "LKF Funktionscode: 1- 62 17" 
* #F060 "Kur- und Prävention"
* #F060 ^definition = Kur- und Prävention
* #F060 ^designation[0].language = #de-AT 
* #F060 ^designation[0].value = "Kur- und Prävention" 
* #F061 "Psychosomatik"
* #F061 ^definition = Psychosomatik
* #F061 ^designation[0].language = #de-AT 
* #F061 ^designation[0].value = "Psychosomatik" 
* #F061 ^property[0].code = #Relationships 
* #F061 ^property[0].valueString = "LKF Funktionscode: 1- -- 68" 
* #F062 "Radioonkologie"
* #F062 ^definition = Dokumente aus Strahlentherapie und Radioonkologie
* #F062 ^designation[0].language = #de-AT 
* #F062 ^designation[0].value = "Radioonkologie" 
* #F062 ^property[0].code = #Relationships 
* #F062 ^property[0].valueString = "LKF Funktionscode: 1- 72 12             LKF Funktionscode: 1- 72 58" 
* #F063 "Rechtliche Dokumente"
* #F063 ^definition = Rechtliche Dokumente wie Patientenverfügungen
* #F063 ^designation[0].language = #de-AT 
* #F063 ^designation[0].value = "Rechtliche Dokumente" 
* #F064 "Thoraxchirurgie"
* #F064 ^definition = Thoraxchirurgie
* #F064 ^designation[0].language = #de-AT 
* #F064 ^designation[0].value = "Thoraxchirurgie" 
* #F064 ^property[0].code = #Relationships 
* #F064 ^property[0].valueString = "LKF Funktionscode: 1- 21 16" 
* #F065 "Patienten Verwaltung"
* #F065 ^definition = Dokumente aus dem Bereich der Patienten Verwaltung (Administration)
* #F065 ^designation[0].language = #de-AT 
* #F065 ^designation[0].value = "Patienten Verwaltung" 
* #F066 "Krankenhauspflege"
* #F066 ^definition = Dokumente aus dem pflegerischen Bereich im Krankenhaus (nur zu verwenden, wenn nicht eine spezifischere Zuordnung zu einem anderen Fachbereich möglich ist)
* #F066 ^designation[0].language = #de-AT 
* #F066 ^designation[0].value = "Krankenhauspflege" 
* #F067 "Orthopädie und Traumatologie"
* #F067 ^definition = Orthopädie und Traumatologie
* #F067 ^designation[0].language = #de-AT 
* #F067 ^designation[0].value = "Orthopädie und Traumatologie" 
* #F067 ^property[0].code = #Relationships 
* #F067 ^property[0].valueString = "LKF Funktionscode: 1- 30 --" 
