Instance: hl7-entity-name-part-qualifier 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-entity-name-part-qualifier" 
* name = "hl7-entity-name-part-qualifier" 
* title = "HL7 Entity Name Part Qualifier" 
* status = #active 
* content = #complete 
* version = "HL7:EntityNamePartQualifier (OrganizationNamePartQualifier)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.5.43" 
* date = "2013-01-10" 
* count = 20 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #_OrganizationNamePartQualifier "OrganizationNamePartQualifier"
* #_OrganizationNamePartQualifier ^property[0].code = #child 
* #_OrganizationNamePartQualifier ^property[0].valueCode = #AC 
* #_OrganizationNamePartQualifier ^property[1].code = #child 
* #_OrganizationNamePartQualifier ^property[1].valueCode = #AD 
* #_OrganizationNamePartQualifier ^property[2].code = #child 
* #_OrganizationNamePartQualifier ^property[2].valueCode = #BR 
* #_OrganizationNamePartQualifier ^property[3].code = #child 
* #_OrganizationNamePartQualifier ^property[3].valueCode = #CL 
* #_OrganizationNamePartQualifier ^property[4].code = #child 
* #_OrganizationNamePartQualifier ^property[4].valueCode = #CON 
* #_OrganizationNamePartQualifier ^property[5].code = #child 
* #_OrganizationNamePartQualifier ^property[5].valueCode = #DEV 
* #_OrganizationNamePartQualifier ^property[6].code = #child 
* #_OrganizationNamePartQualifier ^property[6].valueCode = #FRM 
* #_OrganizationNamePartQualifier ^property[7].code = #child 
* #_OrganizationNamePartQualifier ^property[7].valueCode = #IN 
* #_OrganizationNamePartQualifier ^property[8].code = #child 
* #_OrganizationNamePartQualifier ^property[8].valueCode = #INV 
* #_OrganizationNamePartQualifier ^property[9].code = #child 
* #_OrganizationNamePartQualifier ^property[9].valueCode = #LS 
* #_OrganizationNamePartQualifier ^property[10].code = #child 
* #_OrganizationNamePartQualifier ^property[10].valueCode = #NB 
* #_OrganizationNamePartQualifier ^property[11].code = #child 
* #_OrganizationNamePartQualifier ^property[11].valueCode = #PR 
* #_OrganizationNamePartQualifier ^property[12].code = #child 
* #_OrganizationNamePartQualifier ^property[12].valueCode = #SCI 
* #_OrganizationNamePartQualifier ^property[13].code = #child 
* #_OrganizationNamePartQualifier ^property[13].valueCode = #SP 
* #_OrganizationNamePartQualifier ^property[14].code = #child 
* #_OrganizationNamePartQualifier ^property[14].valueCode = #STR 
* #_OrganizationNamePartQualifier ^property[15].code = #child 
* #_OrganizationNamePartQualifier ^property[15].valueCode = #TITLE 
* #_OrganizationNamePartQualifier ^property[16].code = #child 
* #_OrganizationNamePartQualifier ^property[16].valueCode = #TMK 
* #_OrganizationNamePartQualifier ^property[17].code = #child 
* #_OrganizationNamePartQualifier ^property[17].valueCode = #USE 
* #_OrganizationNamePartQualifier ^property[18].code = #child 
* #_OrganizationNamePartQualifier ^property[18].valueCode = #VV 
* #AC "academic"
* #AC ^property[0].code = #parent 
* #AC ^property[0].valueCode = #_OrganizationNamePartQualifier 
* #AD "adopted"
* #AD ^property[0].code = #parent 
* #AD ^property[0].valueCode = #_OrganizationNamePartQualifier 
* #BR "birth"
* #BR ^property[0].code = #parent 
* #BR ^property[0].valueCode = #_OrganizationNamePartQualifier 
* #CL "callme"
* #CL ^property[0].code = #parent 
* #CL ^property[0].valueCode = #_OrganizationNamePartQualifier 
* #CON "container name"
* #CON ^property[0].code = #parent 
* #CON ^property[0].valueCode = #_OrganizationNamePartQualifier 
* #DEV "device name"
* #DEV ^property[0].code = #parent 
* #DEV ^property[0].valueCode = #_OrganizationNamePartQualifier 
* #FRM "form name"
* #FRM ^property[0].code = #parent 
* #FRM ^property[0].valueCode = #_OrganizationNamePartQualifier 
* #IN "initial"
* #IN ^property[0].code = #parent 
* #IN ^property[0].valueCode = #_OrganizationNamePartQualifier 
* #INV "invented name"
* #INV ^property[0].code = #parent 
* #INV ^property[0].valueCode = #_OrganizationNamePartQualifier 
* #LS "Legal status"
* #LS ^property[0].code = #parent 
* #LS ^property[0].valueCode = #_OrganizationNamePartQualifier 
* #NB "nobility"
* #NB ^property[0].code = #parent 
* #NB ^property[0].valueCode = #_OrganizationNamePartQualifier 
* #PR "professional"
* #PR ^property[0].code = #parent 
* #PR ^property[0].valueCode = #_OrganizationNamePartQualifier 
* #SCI "scientific name"
* #SCI ^property[0].code = #parent 
* #SCI ^property[0].valueCode = #_OrganizationNamePartQualifier 
* #SP "spouse"
* #SP ^property[0].code = #parent 
* #SP ^property[0].valueCode = #_OrganizationNamePartQualifier 
* #STR "strength name"
* #STR ^property[0].code = #parent 
* #STR ^property[0].valueCode = #_OrganizationNamePartQualifier 
* #TITLE "title"
* #TITLE ^property[0].code = #parent 
* #TITLE ^property[0].valueCode = #_OrganizationNamePartQualifier 
* #TMK "trademark name"
* #TMK ^property[0].code = #parent 
* #TMK ^property[0].valueCode = #_OrganizationNamePartQualifier 
* #USE "intended use name"
* #USE ^property[0].code = #parent 
* #USE ^property[0].valueCode = #_OrganizationNamePartQualifier 
* #VV "voorvoegsel"
* #VV ^property[0].code = #parent 
* #VV ^property[0].valueCode = #_OrganizationNamePartQualifier 
