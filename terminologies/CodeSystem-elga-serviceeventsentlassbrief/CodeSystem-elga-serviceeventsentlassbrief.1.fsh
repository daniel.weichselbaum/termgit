Instance: elga-serviceeventsentlassbrief 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-serviceeventsentlassbrief" 
* name = "elga-serviceeventsentlassbrief" 
* title = "ELGA_ServiceEventsEntlassbrief" 
* status = #active 
* content = #complete 
* version = "3.1" 
* description = "**Description:** Codes to distinguish discharge service events

**Beschreibung:** Codeliste zur Unterscheidung der Service Events im Bereich Entlassung" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.21" 
* date = "2015-09-01" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* count = 2 
* property[0].code = #hints 
* property[0].type = #string 
* concept[0].code = #GDLPUB
* concept[0].display = "Gesundheitsdienstleistung Pflege und Betreuung"
* concept[0].designation[0].language = #de-AT 
* concept[0].designation[0].value = "Gesundheitsdienstleistung Pflege und Betreuung" 
* concept[0].property[0].code = #hints 
* concept[0].property[0].valueString = "Verwendung im Pflegesituationsbericht" 
* concept[1].code = #GDLSTATAUF
* concept[1].display = "Gesundheitsdienstleistung im Rahmen eines stationären Aufenthalts"
* concept[1].designation[0].language = #de-AT 
* concept[1].designation[0].value = "Gesundheitsdienstleistung im Rahmen eines stationären Aufenthalts" 
