Instance: elga-dosisparameter 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-dosisparameter" 
* name = "elga-dosisparameter" 
* title = "ELGA_Dosisparameter" 
* status = #active 
* version = "4.0" 
* description = "**Beschreibung:** Codes für Dosisparameter zur Ermittlung der Patientendosis" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.166" 
* date = "2017-02-17" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-dcm"
* compose.include[0].concept[0].code = #111636
* compose.include[0].concept[0].display = "Entrance Exposure at RP"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "Eingangsdosis" 
* compose.include[0].concept[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#einheit_codiert "einheit_codiert" 
* compose.include[0].concept[0].designation[1].value = "mGy" 
* compose.include[0].concept[0].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#einheit_print "einheit_print" 
* compose.include[0].concept[0].designation[2].value = "mGy" 
* compose.include[0].concept[1].code = #111637
* compose.include[0].concept[1].display = "Accumulated Average Glandular Dose"
* compose.include[0].concept[1].designation[0].language = #de-AT 
* compose.include[0].concept[1].designation[0].value = "Mittlere Parenchymdosis (AGD)" 
* compose.include[0].concept[1].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#einheit_codiert "einheit_codiert" 
* compose.include[0].concept[1].designation[1].value = "mGy" 
* compose.include[0].concept[1].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#einheit_print "einheit_print" 
* compose.include[0].concept[1].designation[2].value = "mGy" 
* compose.include[0].concept[2].code = #113507
* compose.include[0].concept[2].display = "Administered activity"
* compose.include[0].concept[2].designation[0].language = #de-AT 
* compose.include[0].concept[2].designation[0].value = "Applizierte Aktivität" 
* compose.include[0].concept[2].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#einheit_codiert "einheit_codiert" 
* compose.include[0].concept[2].designation[1].value = "MBq" 
* compose.include[0].concept[2].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#einheit_print "einheit_print" 
* compose.include[0].concept[2].designation[2].value = "MBq" 
* compose.include[0].concept[3].code = #113722
* compose.include[0].concept[3].display = "Dose Area Product Total"
* compose.include[0].concept[3].designation[0].language = #de-AT 
* compose.include[0].concept[3].designation[0].value = "Dosisflächenprodukt (DAP)" 
* compose.include[0].concept[3].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#einheit_codiert "einheit_codiert" 
* compose.include[0].concept[3].designation[1].value = "cGy.m2" 
* compose.include[0].concept[3].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#einheit_print "einheit_print" 
* compose.include[0].concept[3].designation[2].value = "cGym²" 
* compose.include[0].concept[4].code = #113813
* compose.include[0].concept[4].display = "CT Dose Length Product Total"
* compose.include[0].concept[4].designation[0].language = #de-AT 
* compose.include[0].concept[4].designation[0].value = "Dosislängenprodukt (DLP)" 
* compose.include[0].concept[4].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#einheit_codiert "einheit_codiert" 
* compose.include[0].concept[4].designation[1].value = "mGy.cm" 
* compose.include[0].concept[4].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#einheit_print "einheit_print" 
* compose.include[0].concept[4].designation[2].value = "mGycm" 
* compose.include[0].concept[5].code = #113839
* compose.include[0].concept[5].display = "Effective Dose"
* compose.include[0].concept[5].designation[0].language = #de-AT 
* compose.include[0].concept[5].designation[0].value = "Effektive Dosis" 
* compose.include[0].concept[5].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#einheit_codiert "einheit_codiert" 
* compose.include[0].concept[5].designation[1].value = "mSv" 
* compose.include[0].concept[5].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#einheit_print "einheit_print" 
* compose.include[0].concept[5].designation[2].value = "mSv" 

* expansion.timestamp = "2022-09-13T14:15:20.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem-dcm"
* expansion.contains[0].code = #111636
* expansion.contains[0].display = "Entrance Exposure at RP"
* expansion.contains[0].designation[0].language = #de-AT 
* expansion.contains[0].designation[0].value = "Eingangsdosis" 
* expansion.contains[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#einheit_codiert "einheit_codiert" 
* expansion.contains[0].designation[1].value = "mGy" 
* expansion.contains[0].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#einheit_print "einheit_print" 
* expansion.contains[0].designation[2].value = "mGy" 
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem-dcm"
* expansion.contains[1].code = #111637
* expansion.contains[1].display = "Accumulated Average Glandular Dose"
* expansion.contains[1].designation[0].language = #de-AT 
* expansion.contains[1].designation[0].value = "Mittlere Parenchymdosis (AGD)" 
* expansion.contains[1].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#einheit_codiert "einheit_codiert" 
* expansion.contains[1].designation[1].value = "mGy" 
* expansion.contains[1].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#einheit_print "einheit_print" 
* expansion.contains[1].designation[2].value = "mGy" 
* expansion.contains[2].system = "https://termgit.elga.gv.at/CodeSystem-dcm"
* expansion.contains[2].code = #113507
* expansion.contains[2].display = "Administered activity"
* expansion.contains[2].designation[0].language = #de-AT 
* expansion.contains[2].designation[0].value = "Applizierte Aktivität" 
* expansion.contains[2].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#einheit_codiert "einheit_codiert" 
* expansion.contains[2].designation[1].value = "MBq" 
* expansion.contains[2].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#einheit_print "einheit_print" 
* expansion.contains[2].designation[2].value = "MBq" 
* expansion.contains[3].system = "https://termgit.elga.gv.at/CodeSystem-dcm"
* expansion.contains[3].code = #113722
* expansion.contains[3].display = "Dose Area Product Total"
* expansion.contains[3].designation[0].language = #de-AT 
* expansion.contains[3].designation[0].value = "Dosisflächenprodukt (DAP)" 
* expansion.contains[3].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#einheit_codiert "einheit_codiert" 
* expansion.contains[3].designation[1].value = "cGy.m2" 
* expansion.contains[3].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#einheit_print "einheit_print" 
* expansion.contains[3].designation[2].value = "cGym²" 
* expansion.contains[4].system = "https://termgit.elga.gv.at/CodeSystem-dcm"
* expansion.contains[4].code = #113813
* expansion.contains[4].display = "CT Dose Length Product Total"
* expansion.contains[4].designation[0].language = #de-AT 
* expansion.contains[4].designation[0].value = "Dosislängenprodukt (DLP)" 
* expansion.contains[4].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#einheit_codiert "einheit_codiert" 
* expansion.contains[4].designation[1].value = "mGy.cm" 
* expansion.contains[4].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#einheit_print "einheit_print" 
* expansion.contains[4].designation[2].value = "mGycm" 
* expansion.contains[5].system = "https://termgit.elga.gv.at/CodeSystem-dcm"
* expansion.contains[5].code = #113839
* expansion.contains[5].display = "Effective Dose"
* expansion.contains[5].designation[0].language = #de-AT 
* expansion.contains[5].designation[0].value = "Effektive Dosis" 
* expansion.contains[5].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#einheit_codiert "einheit_codiert" 
* expansion.contains[5].designation[1].value = "mSv" 
* expansion.contains[5].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#einheit_print "einheit_print" 
* expansion.contains[5].designation[2].value = "mSv" 
