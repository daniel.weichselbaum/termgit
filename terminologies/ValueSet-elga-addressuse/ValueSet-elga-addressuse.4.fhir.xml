<?xml version="1.0" ?>
<ValueSet xmlns="http://hl7.org/fhir">
    <id value="elga-addressuse"/>
    <url value="https://termgit.elga.gv.at/ValueSet-elga-addressuse"/>
    <identifier>
        <use value="official"/>
        <system value="urn:ietf:rfc:3986"/>
        <value value="urn:oid:1.2.40.0.34.10.16"/>
    </identifier>
    <version value="202008"/>
    <name value="elga-addressuse"/>
    <title value="ELGA_AddressUse"/>
    <status value="active"/>
    <date value="2020-08-20"/>
    <contact>
        <telecom>
            <system value="url"/>
            <value value="http://www.hl7.org"/>
        </telecom>
    </contact>
    <description value="**Description:** Describes the meaning and use of addresses. Naming within 'ELGA Referenz-Stylesheet' XLS as indicated in the german translation&#10;&#10;**Beschreibung:** Beschreibt die Bedeutung und Verwendung einer Adresse. Benennung im ELGA Referenz-Stylesheet XLS wie in deutscher Übersetzung angegeben"/>
    <compose>
        <include>
            <system value="https://termgit.elga.gv.at/CodeSystem-hl7-address-use"/>
            <concept>
                <code value="H"/>
                <display value="home address"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="Wohnort"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem-austrian-designation-use"/>
                        <code value="concept_beschreibung"/>
                        <display value="concept_beschreibung"/>
                    </use>
                    <value value="zu Hause (A communication address at a home, attempted contacts for business purposes might intrude privacy and chances are one will contact family or other household members instead of the person one wishes to call. Typically used with urgent cases, or if no other contacts are available.)"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem-austrian-designation-use"/>
                        <code value="hinweise"/>
                        <display value="hinweise"/>
                    </use>
                    <value value="kann für Nebenwohnsitze verwendet werden"/>
                </designation>
            </concept>
            <concept>
                <code value="HP"/>
                <display value="primary home"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="Hauptwohnsitz"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem-austrian-designation-use"/>
                        <code value="concept_beschreibung"/>
                        <display value="concept_beschreibung"/>
                    </use>
                    <value value="Privat (The primary home, to reach a person after business hours.)"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem-austrian-designation-use"/>
                        <code value="hinweise"/>
                        <display value="hinweise"/>
                    </use>
                    <value value="für den Hauptwohnsitz zu verwenden"/>
                </designation>
            </concept>
            <concept>
                <code value="HV"/>
                <display value="vacation home"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="Ferienwohnort"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem-austrian-designation-use"/>
                        <code value="concept_beschreibung"/>
                        <display value="concept_beschreibung"/>
                    </use>
                    <value value="Im Urlaub (A vacation home, to reach a person while on vacation.)"/>
                </designation>
            </concept>
            <concept>
                <code value="TMP"/>
                <display value="temporary address"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="Temporäre Adresse"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem-austrian-designation-use"/>
                        <code value="concept_beschreibung"/>
                        <display value="concept_beschreibung"/>
                    </use>
                    <value value="A temporary address, may be good for visit or mailing. Note that an address history can provide more detailed information."/>
                </designation>
            </concept>
            <concept>
                <code value="WP"/>
                <display value="work place"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="Geschäftlich"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem-austrian-designation-use"/>
                        <code value="concept_beschreibung"/>
                        <display value="concept_beschreibung"/>
                    </use>
                    <value value="Geschäftlich (An office address. First choice for business related contacts during business hours.)"/>
                </designation>
            </concept>
            <concept>
                <code value="DIR"/>
                <display value="direct"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="Geschäftlich - Direkt"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem-austrian-designation-use"/>
                        <code value="concept_beschreibung"/>
                        <display value="concept_beschreibung"/>
                    </use>
                    <value value="Indicates a work place address or telecommunication address that reaches the individual or organization directly without intermediaries. For phones, often referred to as a private line."/>
                </designation>
            </concept>
            <concept>
                <code value="PUB"/>
                <display value="public"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="Geschäftlich - Vermittlung"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem-austrian-designation-use"/>
                        <code value="concept_beschreibung"/>
                        <display value="concept_beschreibung"/>
                    </use>
                    <value value="Indicates a work place address or telecommunication address that is a standard address which may reach a reception service, mail-room, or other intermediary prior to the target entity."/>
                </designation>
            </concept>
            <concept>
                <code value="PHYS"/>
                <display value="physical visit address"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="Adresse für (physische) Besuche"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem-austrian-designation-use"/>
                        <code value="concept_beschreibung"/>
                        <display value="concept_beschreibung"/>
                    </use>
                    <value value="Used primarily to visit an address."/>
                </designation>
            </concept>
            <concept>
                <code value="PST"/>
                <display value="postal address"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="Postadresse"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem-austrian-designation-use"/>
                        <code value="concept_beschreibung"/>
                        <display value="concept_beschreibung"/>
                    </use>
                    <value value="Used to send mail."/>
                </designation>
            </concept>
        </include>
    </compose>
    <expansion>
        <timestamp value="2022-09-13T14:16:34.0000Z"/>
        <contains>
            <system value="https://termgit.elga.gv.at/CodeSystem-hl7-address-use"/>
            <code value="H"/>
            <display value="home address"/>
            <designation>
                <language value="de-AT"/>
                <value value="Wohnort"/>
            </designation>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem-austrian-designation-use"/>
                    <code value="concept_beschreibung"/>
                    <display value="concept_beschreibung"/>
                </use>
                <value value="zu Hause (A communication address at a home, attempted contacts for business purposes might intrude privacy and chances are one will contact family or other household members instead of the person one wishes to call. Typically used with urgent cases, or if no other contacts are available.)"/>
            </designation>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem-austrian-designation-use"/>
                    <code value="hinweise"/>
                    <display value="hinweise"/>
                </use>
                <value value="kann für Nebenwohnsitze verwendet werden"/>
            </designation>
            <contains>
                <system value="https://termgit.elga.gv.at/CodeSystem-hl7-address-use"/>
                <code value="HP"/>
                <display value="primary home"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="Hauptwohnsitz"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem-austrian-designation-use"/>
                        <code value="concept_beschreibung"/>
                        <display value="concept_beschreibung"/>
                    </use>
                    <value value="Privat (The primary home, to reach a person after business hours.)"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem-austrian-designation-use"/>
                        <code value="hinweise"/>
                        <display value="hinweise"/>
                    </use>
                    <value value="für den Hauptwohnsitz zu verwenden"/>
                </designation>
            </contains>
            <contains>
                <system value="https://termgit.elga.gv.at/CodeSystem-hl7-address-use"/>
                <code value="HV"/>
                <display value="vacation home"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="Ferienwohnort"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem-austrian-designation-use"/>
                        <code value="concept_beschreibung"/>
                        <display value="concept_beschreibung"/>
                    </use>
                    <value value="Im Urlaub (A vacation home, to reach a person while on vacation.)"/>
                </designation>
            </contains>
        </contains>
        <contains>
            <system value="https://termgit.elga.gv.at/CodeSystem-hl7-address-use"/>
            <code value="TMP"/>
            <display value="temporary address"/>
            <designation>
                <language value="de-AT"/>
                <value value="Temporäre Adresse"/>
            </designation>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem-austrian-designation-use"/>
                    <code value="concept_beschreibung"/>
                    <display value="concept_beschreibung"/>
                </use>
                <value value="A temporary address, may be good for visit or mailing. Note that an address history can provide more detailed information."/>
            </designation>
        </contains>
        <contains>
            <system value="https://termgit.elga.gv.at/CodeSystem-hl7-address-use"/>
            <code value="WP"/>
            <display value="work place"/>
            <designation>
                <language value="de-AT"/>
                <value value="Geschäftlich"/>
            </designation>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem-austrian-designation-use"/>
                    <code value="concept_beschreibung"/>
                    <display value="concept_beschreibung"/>
                </use>
                <value value="Geschäftlich (An office address. First choice for business related contacts during business hours.)"/>
            </designation>
            <contains>
                <system value="https://termgit.elga.gv.at/CodeSystem-hl7-address-use"/>
                <code value="DIR"/>
                <display value="direct"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="Geschäftlich - Direkt"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem-austrian-designation-use"/>
                        <code value="concept_beschreibung"/>
                        <display value="concept_beschreibung"/>
                    </use>
                    <value value="Indicates a work place address or telecommunication address that reaches the individual or organization directly without intermediaries. For phones, often referred to as a private line."/>
                </designation>
            </contains>
            <contains>
                <system value="https://termgit.elga.gv.at/CodeSystem-hl7-address-use"/>
                <code value="PUB"/>
                <display value="public"/>
                <designation>
                    <language value="de-AT"/>
                    <value value="Geschäftlich - Vermittlung"/>
                </designation>
                <designation>
                    <use>
                        <system value="https://termgit.elga.gv.at/CodeSystem-austrian-designation-use"/>
                        <code value="concept_beschreibung"/>
                        <display value="concept_beschreibung"/>
                    </use>
                    <value value="Indicates a work place address or telecommunication address that is a standard address which may reach a reception service, mail-room, or other intermediary prior to the target entity."/>
                </designation>
            </contains>
        </contains>
        <contains>
            <system value="https://termgit.elga.gv.at/CodeSystem-hl7-address-use"/>
            <code value="PHYS"/>
            <display value="physical visit address"/>
            <designation>
                <language value="de-AT"/>
                <value value="Adresse für (physische) Besuche"/>
            </designation>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem-austrian-designation-use"/>
                    <code value="concept_beschreibung"/>
                    <display value="concept_beschreibung"/>
                </use>
                <value value="Used primarily to visit an address."/>
            </designation>
        </contains>
        <contains>
            <system value="https://termgit.elga.gv.at/CodeSystem-hl7-address-use"/>
            <code value="PST"/>
            <display value="postal address"/>
            <designation>
                <language value="de-AT"/>
                <value value="Postadresse"/>
            </designation>
            <designation>
                <use>
                    <system value="https://termgit.elga.gv.at/CodeSystem-austrian-designation-use"/>
                    <code value="concept_beschreibung"/>
                    <display value="concept_beschreibung"/>
                </use>
                <value value="Used to send mail."/>
            </designation>
        </contains>
    </expansion>
</ValueSet>
