Instance: elga-languageabilitymode 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-languageabilitymode" 
* name = "elga-languageabilitymode" 
* title = "ELGA_LanguageAbilityMode" 
* status = #active 
* version = "4.2" 
* description = "**Description:** List of codes representing the method of expression of the language

**Beschreibung:** Codes zur Angabe der Sprachfähigkeit von Patienten (Methode zum Ausdruck von Spache)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.175" 
* date = "2017-07-27" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.hl7.org/documentcenter/public_temp_ED0BB625-1C23-BA17-0CE4EA7EB5B49D1F/standards/vocabulary/vocabulary_tables/infrastructure/vocabulary/LanguageAbilityMode.html" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-hl7-languageabilitymode"
* compose.include[0].concept[0].code = "ESP"
* compose.include[0].concept[0].display = "Expressed spoken"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "spricht" 
* compose.include[0].concept[1].code = "EWR"
* compose.include[0].concept[1].display = "Expressed written"
* compose.include[0].concept[1].designation[0].language = #de-AT 
* compose.include[0].concept[1].designation[0].value = "schreibt" 
* compose.include[0].concept[2].code = "RSP"
* compose.include[0].concept[2].display = "Received spoken"
* compose.include[0].concept[2].designation[0].language = #de-AT 
* compose.include[0].concept[2].designation[0].value = "versteht gesprochen" 
* compose.include[0].concept[3].code = "RWR"
* compose.include[0].concept[3].display = "Received written"
* compose.include[0].concept[3].designation[0].language = #de-AT 
* compose.include[0].concept[3].designation[0].value = "versteht geschrieben" 
