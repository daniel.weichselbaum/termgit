Instance: ems-biotyp-biovar 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-biotyp-biovar" 
* name = "ems-biotyp-biovar" 
* title = "EMS_Biotyp_Biovar" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Liste Biotyp" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.65" 
* date = "2017-01-26" 
* count = 17 
* #1 "1"
* #1 ^definition = "1" wird verwendet bei Shigellose und Yersiniose
* #1A "1A"
* #1A ^definition = "1A" wird verwendet bei Yersiniose
* #1B "1B"
* #1B ^definition = "1B" wird verwendet bei Yersiniose
* #2 "2"
* #2 ^definition = "2" wird verwendet bei Shigellose und Yersiniose
* #3 "3"
* #3 ^definition = "3" wird verwendet bei Shigellose und Yersiniose
* #4 "4"
* #4 ^definition = "4" wird verwendet bei Shigellose und Yersiniose
* #5 "5"
* #5 ^definition = "5" wird verwendet bei Shigellose und Yersiniose
* #6 "6"
* #6 ^definition = "6" wird verwendet bei Shigellose
* #AB "ab"
* #AB ^definition = "ab" wird verwendet bei Shigellose
* #CD "cd"
* #CD ^definition = "cd" wird verwendet bei Shigellose
* #ELTOR "El-Tor"
* #ELTOR ^definition = "El-Tor" wird verwendet bei Cholera
* #GL "gl"
* #GL ^definition = "gl" wird verwendet bei Shigellose
* #VARBELF "var belfanti"
* #VARBELF ^definition = bei Diphtherie
* #VARGRAV "var gravis"
* #VARGRAV ^definition = bei Diphtherie
* #VARINTMED "var intermedius"
* #VARINTMED ^definition = bei Diphtherie
* #VARMITIS "var mitis"
* #VARMITIS ^definition = bei Diphtherie
* #VIBRIOC "Vibrio Comma (klassisch)"
* #VIBRIOC ^definition = "Vibrio Comma" wird verwendet bei Cholera
