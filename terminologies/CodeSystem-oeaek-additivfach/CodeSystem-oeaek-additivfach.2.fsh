Instance: oeaek-additivfach 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-oeaek-additivfach" 
* name = "oeaek-additivfach" 
* title = "OEAEK_Additivfach" 
* status = #active 
* content = #complete 
* version = "1.0" 
* description = "**Description:** Code List of all additive subject areas valid in Austria issued by the Medical Chamber of Austria

**Beschreibung:** Code Liste aller in Österreich gültigen Additivfächer" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.3.1.10.30" 
* date = "2018-07-01" 
* count = 38 
* #56 "Geriatrie"
* #56 ^designation[0].language = #de-AT 
* #56 ^designation[0].value = "Geriatrie" 
* #60 "Infektiologie und Tropenmedizin"
* #60 ^designation[0].language = #de-AT 
* #60 ^designation[0].value = "Infektiologie und Tropenmedizin" 
* #61 "Infektologie"
* #61 ^designation[0].language = #de-AT 
* #61 ^designation[0].value = "Infektologie" 
* #64 "Pädiatrische Intensivmedizin"
* #64 ^designation[0].language = #de-AT 
* #64 ^designation[0].value = "Pädiatrische Intensivmedizin" 
* #65 "Kinderchirurgie"
* #65 ^designation[0].language = #de-AT 
* #65 ^designation[0].value = "Kinderchirurgie" 
* #66 "Zf. Plastische Chirurgie"
* #66 ^designation[0].language = #de-AT 
* #66 ^designation[0].value = "Zf. Plastische Chirurgie" 
* #67 "Nuklearmedizin"
* #67 ^designation[0].language = #de-AT 
* #67 ^designation[0].value = "Nuklearmedizin" 
* #68 "Kinder- und Jugendneuropsychiatrie"
* #68 ^designation[0].language = #de-AT 
* #68 ^designation[0].value = "Kinder- und Jugendneuropsychiatrie" 
* #69 "Mund-, Kiefer- und Gesichtschirurgie"
* #69 ^designation[0].language = #de-AT 
* #69 ^designation[0].value = "Mund-, Kiefer- und Gesichtschirurgie" 
* #70 "Klinische Pharmakologie"
* #70 ^designation[0].language = #de-AT 
* #70 ^designation[0].value = "Klinische Pharmakologie" 
* #71 "Hochvolt- und Brachytherapie"
* #71 ^designation[0].language = #de-AT 
* #71 ^designation[0].value = "Hochvolt- und Brachytherapie" 
* #72 "Gefäßchirurgie"
* #72 ^designation[0].language = #de-AT 
* #72 ^designation[0].value = "Gefäßchirurgie" 
* #73 "Zytodiagnostik"
* #73 ^designation[0].language = #de-AT 
* #73 ^designation[0].value = "Zytodiagnostik" 
* #74 "Kardiologie"
* #74 ^designation[0].language = #de-AT 
* #74 ^designation[0].value = "Kardiologie" 
* #75 "Nephrologie"
* #75 ^designation[0].language = #de-AT 
* #75 ^designation[0].value = "Nephrologie" 
* #76 "Phoniatrie"
* #76 ^designation[0].language = #de-AT 
* #76 ^designation[0].value = "Phoniatrie" 
* #77 "Humangenetik"
* #77 ^designation[0].language = #de-AT 
* #77 ^designation[0].value = "Humangenetik" 
* #78 "Angiologie"
* #78 ^designation[0].language = #de-AT 
* #78 ^designation[0].value = "Angiologie" 
* #79 "Endokrinologie u. Stoffwechselerkr."
* #79 ^designation[0].language = #de-AT 
* #79 ^designation[0].value = "Endokrinologie u. Stoffwechselerkr." 
* #80 "Gastroenterologie und Hepatologie"
* #80 ^designation[0].language = #de-AT 
* #80 ^designation[0].value = "Gastroenterologie und Hepatologie" 
* #81 "Herzchirurgie"
* #81 ^designation[0].language = #de-AT 
* #81 ^designation[0].value = "Herzchirurgie" 
* #82 "Hämatologie und internistische Onkologie"
* #82 ^designation[0].language = #de-AT 
* #82 ^designation[0].value = "Hämatologie und internistische Onkologie" 
* #83 "Intensivmedizin"
* #83 ^designation[0].language = #de-AT 
* #83 ^designation[0].value = "Intensivmedizin" 
* #84 "Internistische Sportheilkunde"
* #84 ^designation[0].language = #de-AT 
* #84 ^designation[0].value = "Internistische Sportheilkunde" 
* #85 "Physikalische Sportheilkunde"
* #85 ^designation[0].language = #de-AT 
* #85 ^designation[0].value = "Physikalische Sportheilkunde" 
* #86 "Rheumatologie"
* #86 ^designation[0].language = #de-AT 
* #86 ^designation[0].value = "Rheumatologie" 
* #87 "Sportorthopädie"
* #87 ^designation[0].language = #de-AT 
* #87 ^designation[0].value = "Sportorthopädie" 
* #88 "Sporttraumatologie"
* #88 ^designation[0].language = #de-AT 
* #88 ^designation[0].value = "Sporttraumatologie" 
* #89 "Thoraxchirurgie"
* #89 ^designation[0].language = #de-AT 
* #89 ^designation[0].value = "Thoraxchirurgie" 
* #90 "Tropenmedizin"
* #90 ^designation[0].language = #de-AT 
* #90 ^designation[0].value = "Tropenmedizin" 
* #91 "Viszeralchirurgie"
* #91 ^designation[0].language = #de-AT 
* #91 ^designation[0].value = "Viszeralchirurgie" 
* #92 "Neonatologie und pädiatrische Intensivmedizin"
* #92 ^designation[0].language = #de-AT 
* #92 ^designation[0].value = "Neonatologie und pädiatrische Intensivmedizin" 
* #93 "Pädiatrische Intensivmedizin und Neonatologie"
* #93 ^designation[0].language = #de-AT 
* #93 ^designation[0].value = "Pädiatrische Intensivmedizin und Neonatologie" 
* #94 "Neuropädiatrie"
* #94 ^designation[0].language = #de-AT 
* #94 ^designation[0].value = "Neuropädiatrie" 
* #95 "Pädiatrische Endokrinologie und Diabetologie"
* #95 ^designation[0].language = #de-AT 
* #95 ^designation[0].value = "Pädiatrische Endokrinologie und Diabetologie" 
* #96 "Pädiatrische Hämatologie und Onkologie"
* #96 ^designation[0].language = #de-AT 
* #96 ^designation[0].value = "Pädiatrische Hämatologie und Onkologie" 
* #97 "Pädiatrische Kardiologie"
* #97 ^designation[0].language = #de-AT 
* #97 ^designation[0].value = "Pädiatrische Kardiologie" 
* #98 "Pädiatrische Pulmonologie"
* #98 ^designation[0].language = #de-AT 
* #98 ^designation[0].value = "Pädiatrische Pulmonologie" 
