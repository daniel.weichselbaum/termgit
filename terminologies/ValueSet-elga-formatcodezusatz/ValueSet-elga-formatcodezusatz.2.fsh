Instance: elga-formatcodezusatz 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-formatcodezusatz" 
* name = "elga-formatcodezusatz" 
* title = "ELGA_FormatCodeZusatz" 
* status = #active 
* version = "2.8" 
* description = "**Description:** Defintion of valid additions for the FormatCode by declaration in Context of XDS document metadata

**Beschreibung:** Definition der gültigen Zusätze für den FormatCode bei Angabe im Rahmen der XDS Dokument Metadaten" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.60" 
* date = "2014-09-01" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-elga-formatcodezusatz"
* compose.include[0].concept[0].code = "CDAL1"
* compose.include[0].concept[0].display = "CDAL1"
* compose.include[0].concept[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[0].designation[0].value = "Das Dokument enthält im CDA Body Bereich strukturierte Angaben gemäß CDA Level 1 (structuredBody)" 
* compose.include[0].concept[1].code = "GIF"
* compose.include[0].concept[1].display = "GIF"
* compose.include[0].concept[1].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[1].designation[0].value = "Das Dokument enthält im CDA Body Bereich ein eingebettetes Bild im Graphics Interchange Format (GIF)." 
* compose.include[0].concept[2].code = "JPG"
* compose.include[0].concept[2].display = "JPG"
* compose.include[0].concept[2].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[2].designation[0].value = "Das Dokument enthält im CDA Body Bereich ein eingebettetes Bild gemäß der Norm ISO/IEC 10918-1 bzw. CCITT Recommendation T.81 der Joint Photographic Experts Group (JPEG)." 
* compose.include[0].concept[3].code = "PDF"
* compose.include[0].concept[3].display = "PDF"
* compose.include[0].concept[3].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[3].designation[0].value = "Das Dokument enthält im CDA Body Bereich ein eingebettetes PDF/A-1a Dokument." 
* compose.include[0].concept[4].code = "PNG"
* compose.include[0].concept[4].display = "PNG"
* compose.include[0].concept[4].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[4].designation[0].value = "Das Dokument enthält im CDA Body Bereich ein eingebettetes Bild im Portable Network Graphics (PNG) Format." 
* compose.include[0].concept[5].code = "TEXT"
* compose.include[0].concept[5].display = "TEXT"
* compose.include[0].concept[5].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[5].designation[0].value = "Das Dokument enthält im CDA Body Bereich unstrukturierten Text gemäß CDA Level 1 (nonXMLBody)" 
