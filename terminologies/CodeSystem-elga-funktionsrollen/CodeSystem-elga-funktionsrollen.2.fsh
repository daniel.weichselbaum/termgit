Instance: elga-funktionsrollen 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-funktionsrollen" 
* name = "elga-funktionsrollen" 
* title = "ELGA_Funktionsrollen" 
* status = #active 
* content = #complete 
* version = "201904" 
* description = "**Beschreibung:** ELGA Codeliste für Funktionsrollen" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.158" 
* date = "2019-04-01" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* count = 5 
* #607 "ELGA-Widerspruchstelle"
* #607 ^designation[0].language = #de-AT 
* #607 ^designation[0].value = "ELGA-Widerspruchstelle" 
* #608 "ELGA-Regelwerkadministration"
* #608 ^designation[0].language = #de-AT 
* #608 ^designation[0].value = "ELGA-Regelwerkadministration" 
* #609 "ELGA-Sicherheitsadministration"
* #609 ^designation[0].language = #de-AT 
* #609 ^designation[0].value = "ELGA-Sicherheitsadministration" 
* #610 "ELGA-Teilnehmerin/Teilnehmer"
* #610 ^definition = Bürgerin/Bürger
* #610 ^designation[0].language = #de-AT 
* #610 ^designation[0].value = "ELGA-Teilnehmerin/Teilnehmer" 
* #611 "ELGA-Vollmachtnehmende Person"
* #611 ^definition = Vertretende Person einer ELGA-Teilnehmerin/ Teilnehmer (natürliche Person)
* #611 ^designation[0].language = #de-AT 
* #611 ^designation[0].value = "ELGA-Vollmachtnehmende Person" 
