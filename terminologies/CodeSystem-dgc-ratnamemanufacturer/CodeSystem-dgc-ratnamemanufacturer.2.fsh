Instance: dgc-ratnamemanufacturer 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-dgc-ratnamemanufacturer" 
* name = "dgc-ratnamemanufacturer" 
* title = "DGC_RATNameManufacturer" 
* status = #active 
* content = #complete 
* version = "202207.1" 
* description = "**Beschreibung:** In der Liste sind ausschließlich RAT enthalten." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.205" 
* date = "2022-07-25" 
* count = 221 
* property[0].code = #Relationships 
* property[0].type = #string 
* #1065 "Becton Dickinson, BD Veritor System for Rapid Detection of SARS CoV 2"
* #1065 ^definition = Becton Dickinson, BD Veritor System for Rapid Detection of SARS CoV 2
* #1065 ^designation[0].language = #de-AT 
* #1065 ^designation[0].value = "Becton Dickinson, BD Veritor System for Rapid Detection of SARS CoV 2" 
* #1065 ^property[0].code = #Relationships 
* #1065 ^property[0].valueString = "$.t..ma" 
* #1097 "Quidel Corporation, Sofia 2 SARS Antigen FIA"
* #1097 ^definition = Quidel Corporation, Sofia 2 SARS Antigen FIA
* #1097 ^designation[0].language = #de-AT 
* #1097 ^designation[0].value = "Quidel Corporation, Sofia 2 SARS Antigen FIA" 
* #1097 ^property[0].code = #Relationships 
* #1097 ^property[0].valueString = "$.t..ma" 
* #1114 "Sugentech, Inc., SGTi-flex COVID-19 Ag"
* #1114 ^definition = Sugentech, Inc., SGTi-flex COVID-19 Ag
* #1114 ^designation[0].language = #de-AT 
* #1114 ^designation[0].value = "Sugentech, Inc., SGTi-flex COVID-19 Ag" 
* #1114 ^property[0].code = #Relationships 
* #1114 ^property[0].valueString = "$.t..ma" 
* #1144 "Green Cross Medical Science Corp., GENEDIA W COVID-19 Ag"
* #1144 ^definition = Green Cross Medical Science Corp., GENEDIA W COVID-19 Ag
* #1144 ^designation[0].language = #de-AT 
* #1144 ^designation[0].value = "Green Cross Medical Science Corp., GENEDIA W COVID-19 Ag" 
* #1144 ^property[0].code = #Relationships 
* #1144 ^property[0].valueString = "$.t..ma" 
* #1173 "CerTest Biotec, CerTest SARS-CoV-2 Card test"
* #1173 ^definition = CerTest Biotec, CerTest SARS-CoV-2 Card test
* #1173 ^designation[0].language = #de-AT 
* #1173 ^designation[0].value = "CerTest Biotec, CerTest SARS-CoV-2 Card test" 
* #1173 ^property[0].code = #Relationships 
* #1173 ^property[0].valueString = "$.t..ma" 
* #1178 "Shenzhen Microprofit Biotech Co., Ltd., SARS-CoV-2 Spike Protein Test Kit (Colloidal Gold Chromatographic Immunoassay)"
* #1178 ^definition = Shenzhen Microprofit Biotech Co., Ltd., SARS-CoV-2 Spike Protein Test Kit (Colloidal Gold Chromatographic Immunoassay)
* #1178 ^designation[0].language = #de-AT 
* #1178 ^designation[0].value = "Shenzhen Microprofit Biotech Co., Ltd., SARS-CoV-2 Spike Protein Test Kit (Colloidal Gold Chromatographic Immunoassay)" 
* #1178 ^property[0].code = #Relationships 
* #1178 ^property[0].valueString = "$.t..ma" 
* #1180 "MEDsan GmbH, MEDsan SARS-CoV-2 Antigen Rapid Test"
* #1180 ^definition = MEDsan GmbH, MEDsan SARS-CoV-2 Antigen Rapid Test
* #1180 ^designation[0].language = #de-AT 
* #1180 ^designation[0].value = "MEDsan GmbH, MEDsan SARS-CoV-2 Antigen Rapid Test" 
* #1180 ^property[0].code = #Relationships 
* #1180 ^property[0].valueString = "$.t..ma" 
* #1190 "möLab, COVID-19 Rapid Antigen Test"
* #1190 ^definition = möLab, COVID-19 Rapid Antigen Test
* #1190 ^designation[0].language = #de-AT 
* #1190 ^designation[0].value = "möLab, COVID-19 Rapid Antigen Test" 
* #1190 ^property[0].code = #Relationships 
* #1190 ^property[0].valueString = "$.t..ma" 
* #1197 "Goldsite Diagnostic Inc., SARS-CoV-2 Antigen Kit (Colloidal Gold)"
* #1197 ^definition = Goldsite Diagnostic Inc., SARS-CoV-2 Antigen Kit (Colloidal Gold)
* #1197 ^designation[0].language = #de-AT 
* #1197 ^designation[0].value = "Goldsite Diagnostic Inc., SARS-CoV-2 Antigen Kit (Colloidal Gold)" 
* #1197 ^property[0].code = #Relationships 
* #1197 ^property[0].valueString = "$.t..ma" 
* #1199 "Oncosem Onkolojik Sistemler San. ve Tic. A.S., CAT"
* #1199 ^definition = Oncosem Onkolojik Sistemler San. ve Tic. A.S., CAT
* #1199 ^designation[0].language = #de-AT 
* #1199 ^designation[0].value = "Oncosem Onkolojik Sistemler San. ve Tic. A.S., CAT" 
* #1199 ^property[0].code = #Relationships 
* #1199 ^property[0].valueString = "$.t..ma" 
* #1201 "ScheBo Biotech AG, ScheBo SARS CoV-2 Quick Antigen"
* #1201 ^definition = ScheBo Biotech AG, ScheBo SARS CoV-2 Quick Antigen
* #1201 ^designation[0].language = #de-AT 
* #1201 ^designation[0].value = "ScheBo Biotech AG, ScheBo SARS CoV-2 Quick Antigen" 
* #1201 ^property[0].code = #Relationships 
* #1201 ^property[0].valueString = "$.t..ma" 
* #1215 "Hangzhou Laihe Biotech Co., LYHER Novel Coronavirus (COVID-19) Antigen Test Kit (Colloidal Gold)"
* #1215 ^definition = Hangzhou Laihe Biotech Co., LYHER Novel Coronavirus (COVID-19) Antigen Test Kit (Colloidal Gold)
* #1215 ^designation[0].language = #de-AT 
* #1215 ^designation[0].value = "Hangzhou Laihe Biotech Co., LYHER Novel Coronavirus (COVID-19) Antigen Test Kit (Colloidal Gold)" 
* #1215 ^property[0].code = #Relationships 
* #1215 ^property[0].valueString = "$.t..ma" 
* #1216 "Guangdong Longsee Biomedical Co., Ltd., COVID-2019-nCoV Ag Rapid TestDetection Kit(Immuno-Chromatography)"
* #1216 ^definition = Guangdong Longsee Biomedical Co., Ltd., COVID-2019-nCoV Ag Rapid TestDetection Kit(Immuno-Chromatography)
* #1216 ^designation[0].language = #de-AT 
* #1216 ^designation[0].value = "Guangdong Longsee Biomedical Co., Ltd., COVID-2019-nCoV Ag Rapid TestDetection Kit(Immuno-Chromatography)" 
* #1216 ^property[0].code = #Relationships 
* #1216 ^property[0].valueString = "$.t..ma" 
* #1218 "Siemens Healthineers, CLINITEST Rapid COVID-19 Antigen Test"
* #1218 ^definition = Siemens Healthineers, CLINITEST Rapid COVID-19 Antigen Test
* #1218 ^designation[0].language = #de-AT 
* #1218 ^designation[0].value = "Siemens Healthineers, CLINITEST Rapid COVID-19 Antigen Test" 
* #1218 ^property[0].code = #Relationships 
* #1218 ^property[0].valueString = "$.t..ma" 
* #1223 "BIOSYNEX SA , BIOSYNEX COVID-19 Ag BSS"
* #1223 ^definition = BIOSYNEX SA , BIOSYNEX COVID-19 Ag BSS
* #1223 ^designation[0].language = #de-AT 
* #1223 ^designation[0].value = "BIOSYNEX SA , BIOSYNEX COVID-19 Ag BSS" 
* #1223 ^property[0].code = #Relationships 
* #1223 ^property[0].valueString = "$.t..ma" 
* #1225 "DDS DIAGNOSTIC, Test Rapid Covid-19 Antigen (tampon nazofaringian)"
* #1225 ^definition = DDS DIAGNOSTIC, Test Rapid Covid-19 Antigen (tampon nazofaringian)
* #1225 ^designation[0].language = #de-AT 
* #1225 ^designation[0].value = "DDS DIAGNOSTIC, Test Rapid Covid-19 Antigen (tampon nazofaringian)" 
* #1225 ^property[0].code = #Relationships 
* #1225 ^property[0].valueString = "$.t..ma" 
* #1228 "Shenzhen Microprofit Biotech Co., Ltd, SARS-CoV-2 Spike Protein Test Kit (Fluorescence Immunoassay)"
* #1228 ^definition = Shenzhen Microprofit Biotech Co., Ltd, SARS-CoV-2 Spike Protein Test Kit (Fluorescence Immunoassay)
* #1228 ^designation[0].language = #de-AT 
* #1228 ^designation[0].value = "Shenzhen Microprofit Biotech Co., Ltd, SARS-CoV-2 Spike Protein Test Kit (Fluorescence Immunoassay)" 
* #1228 ^property[0].code = #Relationships 
* #1228 ^property[0].valueString = "$.t..ma" 
* #1232 "Abbott Rapid Diagnostics, Panbio COVID-19 Ag Rapid Test"
* #1232 ^definition = Abbott Rapid Diagnostics, Panbio COVID-19 Ag Rapid Test
* #1232 ^designation[0].language = #de-AT 
* #1232 ^designation[0].value = "Abbott Rapid Diagnostics, Panbio COVID-19 Ag Rapid Test " 
* #1232 ^property[0].code = #Relationships 
* #1232 ^property[0].valueString = "$.t..ma" 
* #1236 "BTNX Inc., Rapid Response COVID-19 Antigen Rapid Test Device"
* #1236 ^definition = BTNX Inc., Rapid Response COVID-19 Antigen Rapid Test Device
* #1236 ^designation[0].language = #de-AT 
* #1236 ^designation[0].value = "BTNX Inc., Rapid Response COVID-19 Antigen Rapid Test Device" 
* #1236 ^property[0].code = #Relationships 
* #1236 ^property[0].valueString = "$.t..ma" 
* #1242 "BIONOTE, NowCheck COVID-19 Ag Test"
* #1242 ^definition = BIONOTE, NowCheck COVID-19 Ag Test
* #1242 ^designation[0].language = #de-AT 
* #1242 ^designation[0].value = "BIONOTE, NowCheck COVID-19 Ag Test" 
* #1242 ^property[0].code = #Relationships 
* #1242 ^property[0].valueString = "$.t..ma" 
* #1243 "Edinburgh Genetics Limited, Edinburgh Genetics ActivXpress+ COVID-19 Antigen Complete Testing Kit"
* #1243 ^definition = Edinburgh Genetics Limited, Edinburgh Genetics ActivXpress+ COVID-19 Antigen Complete Testing Kit
* #1243 ^designation[0].language = #de-AT 
* #1243 ^designation[0].value = "Edinburgh Genetics Limited, Edinburgh Genetics ActivXpress+ COVID-19 Antigen Complete Testing Kit" 
* #1243 ^property[0].code = #Relationships 
* #1243 ^property[0].valueString = "$.t..ma" 
* #1244 "GenBody Inc, GenBody COVID-19 Ag Test"
* #1244 ^definition = GenBody Inc, GenBody COVID-19 Ag Test
* #1244 ^designation[0].language = #de-AT 
* #1244 ^designation[0].value = "GenBody Inc, GenBody COVID-19 Ag Test" 
* #1244 ^property[0].code = #Relationships 
* #1244 ^property[0].valueString = "$.t..ma" 
* #1253 "GenSure Biotech Inc, Gensure COVID-19 Antigen Rapid Test Kit (REF: P2004) (DIA-COVID -19 Ag Rapid Test)"
* #1253 ^definition = GenSure Biotech Inc, Gensure COVID-19 Antigen Rapid Test Kit (REF: P2004) (DIA-COVID -19 Ag Rapid Test)
* #1253 ^designation[0].language = #de-AT 
* #1253 ^designation[0].value = "GenSure Biotech Inc, Gensure COVID-19 Antigen Rapid Test Kit (REF: P2004) (DIA-COVID -19 Ag Rapid Test)" 
* #1253 ^property[0].code = #Relationships 
* #1253 ^property[0].valueString = "$.t..ma" 
* #1257 "Hangzhou AllTest Biotech Co., Ltd, COVID-19 Antigen Rapid Test"
* #1257 ^definition = Hangzhou AllTest Biotech Co., Ltd, COVID-19 Antigen Rapid Test
* #1257 ^designation[0].language = #de-AT 
* #1257 ^designation[0].value = "Hangzhou AllTest Biotech Co., Ltd, COVID-19 Antigen Rapid Test  " 
* #1257 ^property[0].code = #Relationships 
* #1257 ^property[0].valueString = "$.t..ma" 
* #1266 "Labnovation Technologies Inc., SARS-CoV-2 Antigen Rapid Test Kit"
* #1266 ^definition = Labnovation Technologies Inc., SARS-CoV-2 Antigen Rapid Test Kit
* #1266 ^designation[0].language = #de-AT 
* #1266 ^designation[0].value = "Labnovation Technologies Inc., SARS-CoV-2 Antigen Rapid Test Kit" 
* #1266 ^property[0].code = #Relationships 
* #1266 ^property[0].valueString = "$.t..ma" 
* #1267 "LumiQuick Diagnostics Inc., QuickProfil COVID-19 ANTIGEN Test"
* #1267 ^definition = LumiQuick Diagnostics Inc., QuickProfile? COVID-19 ANTIGEN Test
* #1267 ^designation[0].language = #de-AT 
* #1267 ^designation[0].value = "LumiQuick Diagnostics Inc., QuickProfile? COVID-19 ANTIGEN Test" 
* #1267 ^property[0].code = #Relationships 
* #1267 ^property[0].valueString = "$.t..ma" 
* #1268 "LumiraDX , LumiraDx SARS-CoV-2 Ag Test"
* #1268 ^definition = LumiraDX , LumiraDx SARS-CoV-2 Ag Test
* #1268 ^designation[0].language = #de-AT 
* #1268 ^designation[0].value = "LumiraDX , LumiraDx SARS-CoV-2 Ag Test " 
* #1268 ^property[0].code = #Relationships 
* #1268 ^property[0].valueString = "$.t..ma" 
* #1271 "Precision Biosensor Inc., Exdia COVI-19 Ag Test"
* #1271 ^definition = Precision Biosensor Inc., Exdia COVI-19 Ag Test
* #1271 ^designation[0].language = #de-AT 
* #1271 ^designation[0].value = "Precision Biosensor Inc., Exdia COVI-19 Ag Test" 
* #1271 ^property[0].code = #Relationships 
* #1271 ^property[0].valueString = "$.t..ma" 
* #1276 "Willi Fox GmbH, Willi Fox COVID-19 Antigen rapid test"
* #1276 ^definition = Willi Fox GmbH, Willi Fox COVID-19 Antigen rapid test
* #1276 ^designation[0].language = #de-AT 
* #1276 ^designation[0].value = "Willi Fox GmbH, Willi Fox COVID-19 Antigen rapid test" 
* #1276 ^property[0].code = #Relationships 
* #1276 ^property[0].valueString = "$.t..ma" 
* #1278 "Xiamen Boson Biotech Co, Rapid SARS-CoV-2 Antigen Test card"
* #1278 ^definition = Xiamen Boson Biotech Co, Rapid SARS-CoV-2 Antigen Test card
* #1278 ^designation[0].language = #de-AT 
* #1278 ^designation[0].value = "Xiamen Boson Biotech Co, Rapid SARS-CoV-2 Antigen Test card " 
* #1278 ^property[0].code = #Relationships 
* #1278 ^property[0].valueString = "$.t..ma" 
* #1286 "BIOHIT HealthCcare (Hefei) Co., Ltd., SARS-CoV-2 Antigen Rapid Test Kit (Fluorescence Immunochromatography)"
* #1286 ^definition = BIOHIT HealthCcare (Hefei) Co., Ltd., SARS-CoV-2 Antigen Rapid Test Kit (Fluorescence Immunochromatography)
* #1286 ^designation[0].language = #de-AT 
* #1286 ^designation[0].value = "BIOHIT HealthCcare (Hefei) Co., Ltd., SARS-CoV-2 Antigen Rapid Test Kit (Fluorescence Immunochromatography)" 
* #1286 ^property[0].code = #Relationships 
* #1286 ^property[0].valueString = "$.t..ma" 
* #1295 "Zhejiang Anji Saianfu Biotech Co.., Ltd., reOpenTest COVID-19  Antigen Rapid Test"
* #1295 ^definition = Zhejiang Anji Saianfu Biotech Co.., Ltd., reOpenTest COVID-19  Antigen Rapid Test
* #1295 ^designation[0].language = #de-AT 
* #1295 ^designation[0].value = "Zhejiang Anji Saianfu Biotech Co.., Ltd., reOpenTest COVID-19  Antigen Rapid Test" 
* #1295 ^property[0].code = #Relationships 
* #1295 ^property[0].valueString = "$.t..ma" 
* #1296 "Zhejiang Anji Saianfu Biotech Co.., Ltd., AndLucky COVID-19 Antigen Rapid Test"
* #1296 ^definition = Zhejiang Anji Saianfu Biotech Co.., Ltd., AndLucky COVID-19 Antigen Rapid Test
* #1296 ^designation[0].language = #de-AT 
* #1296 ^designation[0].value = "Zhejiang Anji Saianfu Biotech Co.., Ltd., AndLucky COVID-19 Antigen Rapid Test" 
* #1296 ^property[0].code = #Relationships 
* #1296 ^property[0].valueString = "$.t..ma" 
* #1304 "AMEDA Labordiagnostik GmbH, AMP Rapid Test SARS-CoV-2 Ag"
* #1304 ^definition = AMEDA Labordiagnostik GmbH, AMP Rapid Test SARS-CoV-2 Ag
* #1304 ^designation[0].language = #de-AT 
* #1304 ^designation[0].value = "AMEDA Labordiagnostik GmbH, AMP Rapid Test SARS-CoV-2 Ag" 
* #1304 ^property[0].code = #Relationships 
* #1304 ^property[0].valueString = "$.t..ma" 
* #1319 "SGA Medikal, V-Chek SARS-CoV2- Rapid Ag Tets (Coloidal Gold)"
* #1319 ^definition = SGA Medikal, V-Chek SARS-CoV2- Rapid Ag Tets (Coloidal Gold)
* #1319 ^designation[0].language = #de-AT 
* #1319 ^designation[0].value = "SGA Medikal, V-Chek SARS-CoV2- Rapid Ag Tets (Coloidal Gold)" 
* #1319 ^property[0].code = #Relationships 
* #1319 ^property[0].valueString = "$.t..ma" 
* #1331 "Beijing Lepu Medical Technology, SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold immunochromatography)"
* #1331 ^definition = Beijing Lepu Medical Technology, SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold immunochromatography)
* #1331 ^designation[0].language = #de-AT 
* #1331 ^designation[0].value = "Beijing Lepu Medical Technology, SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold immunochromatography)" 
* #1331 ^property[0].code = #Relationships 
* #1331 ^property[0].valueString = "$.t..ma" 
* #1333 "Joinstar Biomedical Technology Co. Ltd, COVID-19 Antigen Rapid Test (Colloidal Gold)"
* #1333 ^definition = Joinstar Biomedical Technology Co. Ltd, COVID-19 Antigen Rapid Test (Colloidal Gold)
* #1333 ^designation[0].language = #de-AT 
* #1333 ^designation[0].value = "Joinstar Biomedical Technology Co. Ltd, COVID-19 Antigen Rapid Test (Colloidal Gold)" 
* #1333 ^property[0].code = #Relationships 
* #1333 ^property[0].valueString = "$.t..ma" 
* #1341 "Qingdao Hightop Biotech Co. Ltd, SARS-CoV-2 Antigen Rapid Test"
* #1341 ^definition = Qingdao Hightop Biotech Co. Ltd, SARS-CoV-2 Antigen Rapid Test
* #1341 ^designation[0].language = #de-AT 
* #1341 ^designation[0].value = "Qingdao Hightop Biotech Co. Ltd, SARS-CoV-2 Antigen Rapid Test" 
* #1341 ^property[0].code = #Relationships 
* #1341 ^property[0].valueString = "$.t..ma" 
* #1343 "Zhejiang Orient Gene Biotech Co., Ltd, Coronavirus Ag Rapid Test Cassette (Swab)"
* #1343 ^definition = Zhejiang Orient Gene Biotech Co., Ltd, Coronavirus Ag Rapid Test Cassette (Swab)
* #1343 ^designation[0].language = #de-AT 
* #1343 ^designation[0].value = "Zhejiang Orient Gene Biotech Co., Ltd, Coronavirus Ag Rapid Test Cassette (Swab)" 
* #1343 ^property[0].code = #Relationships 
* #1343 ^property[0].valueString = "$.t..ma" 
* #1347 "Shenzhen YHLO Biotech Co., Ltd., GLINE-2019-nCoV Ag"
* #1347 ^definition = Shenzhen YHLO Biotech Co., Ltd., GLINE-2019-nCoV Ag
* #1347 ^designation[0].language = #de-AT 
* #1347 ^designation[0].value = "Shenzhen YHLO Biotech Co., Ltd., GLINE-2019-nCoV Ag" 
* #1347 ^property[0].code = #Relationships 
* #1347 ^property[0].valueString = "$.t..ma" 
* #1353 "LINKCARE (NANTONG DIAGNOS BIO), COVID-19 Antigen Test Kit (Colloidal Gold)"
* #1353 ^definition = LINKCARE (NANTONG DIAGNOS BIO), COVID-19 Antigen Test Kit (Colloidal Gold)
* #1353 ^designation[0].language = #de-AT 
* #1353 ^designation[0].value = "LINKCARE (NANTONG DIAGNOS BIO), COVID-19 Antigen Test Kit (Colloidal Gold)" 
* #1353 ^property[0].code = #Relationships 
* #1353 ^property[0].valueString = "$.t..ma" 
* #1357 "SGA Medikal, V-Chek SARS-CoV-2 Rapid Ag Test (colloidal gold)"
* #1357 ^definition = SGA Medikal, V-Chek SARS-CoV-2 Rapid Ag Test (colloidal gold)
* #1357 ^designation[0].language = #de-AT 
* #1357 ^designation[0].value = "SGA Medikal, V-Chek SARS-CoV-2 Rapid Ag Test (colloidal gold)" 
* #1357 ^property[0].code = #Relationships 
* #1357 ^property[0].valueString = "$.t..ma" 
* #1360 "Guangdong Wesail Biotech Co. Ltd, COVID-19 AG Test Kit"
* #1360 ^definition = Guangdong Wesail Biotech Co. Ltd, COVID-19 AG Test Kit
* #1360 ^designation[0].language = #de-AT 
* #1360 ^designation[0].value = "Guangdong Wesail Biotech Co. Ltd, COVID-19 AG Test Kit " 
* #1360 ^property[0].code = #Relationships 
* #1360 ^property[0].valueString = "$.t..ma" 
* #1363 "Hangzhou Clongene Biotech Co., Ltd., COVID-19 Antigen Rapid Test Kit"
* #1363 ^definition = Hangzhou Clongene Biotech Co., Ltd., COVID-19 Antigen Rapid Test Kit
* #1363 ^designation[0].language = #de-AT 
* #1363 ^designation[0].value = "Hangzhou Clongene Biotech Co., Ltd., COVID-19 Antigen Rapid Test Kit" 
* #1363 ^property[0].code = #Relationships 
* #1363 ^property[0].valueString = "$.t..ma" 
* #1365 "Hangzhou Clongene Biotech Co., Ltd., COVID-19/Influenza A+B Antigen Combo Rapid Test"
* #1365 ^definition = Hangzhou Clongene Biotech Co., Ltd., COVID-19/Influenza A+B Antigen Combo Rapid Test
* #1365 ^designation[0].language = #de-AT 
* #1365 ^designation[0].value = "Hangzhou Clongene Biotech Co., Ltd., COVID-19/Influenza A+B Antigen Combo Rapid Test" 
* #1365 ^property[0].code = #Relationships 
* #1365 ^property[0].valueString = "$.t..ma" 
* #1375 "DIALAB GmbH, DIAQUICK COVID -19 Ag Cassette"
* #1375 ^definition = DIALAB GmbH, DIAQUICK COVID -19 Ag Cassette
* #1375 ^designation[0].language = #de-AT 
* #1375 ^designation[0].value = "DIALAB GmbH, DIAQUICK COVID -19 Ag Cassette" 
* #1375 ^property[0].code = #Relationships 
* #1375 ^property[0].valueString = "$.t..ma" 
* #1392 "Hangzhou Testsea Biotechnology Co., Ltd., Testsealabs Covid-19 Antigen Rapid Test Cassette"
* #1392 ^definition = Hangzhou Testsea Biotechnology Co., Ltd., Testsealabs Covid-19 Antigen Rapid Test Cassette
* #1392 ^designation[0].language = #de-AT 
* #1392 ^designation[0].value = "Hangzhou Testsea Biotechnology Co., Ltd., Testsealabs Covid-19 Antigen Rapid Test Cassette" 
* #1392 ^property[0].code = #Relationships 
* #1392 ^property[0].valueString = "$.t..ma" 
* #1443 "Vitrosens Biotechnology Co. Ltd, RapidFor SARS-CoV-2 Ag Test Kit"
* #1443 ^definition = Vitrosens Biotechnology Co. Ltd, RapidFor SARS-CoV-2 Ag Test Kit
* #1443 ^designation[0].language = #de-AT 
* #1443 ^designation[0].value = "Vitrosens Biotechnology Co. Ltd, RapidFor SARS-CoV-2 Ag Test Kit" 
* #1443 ^property[0].code = #Relationships 
* #1443 ^property[0].valueString = "$.t..ma" 
* #1457 "Acon Biotech (Hangzhou) Co., Ltd, SARS-CoV-2 Antigen Rapid Test"
* #1457 ^definition = Acon Biotech (Hangzhou) Co., Ltd, SARS-CoV-2 Antigen Rapid Test
* #1457 ^designation[0].language = #de-AT 
* #1457 ^designation[0].value = "Acon Biotech (Hangzhou) Co., Ltd, SARS-CoV-2 Antigen Rapid Test" 
* #1457 ^property[0].code = #Relationships 
* #1457 ^property[0].valueString = "$.t..ma" 
* #1465 "Triplex International Biosciences Co., Ltd, China, SARS-CoV-2 Antigen Rapid Test Kit"
* #1465 ^definition = Triplex International Biosciences Co., Ltd, China, SARS-CoV-2 Antigen Rapid Test Kit
* #1465 ^designation[0].language = #de-AT 
* #1465 ^designation[0].value = "Triplex International Biosciences Co., Ltd, China, SARS-CoV-2 Antigen Rapid Test Kit" 
* #1465 ^property[0].code = #Relationships 
* #1465 ^property[0].valueString = "$.t..ma" 
* #1466 "TODA PHARMA, TODA CORONADIAG Ag"
* #1466 ^definition = TODA PHARMA, TODA CORONADIAG Ag
* #1466 ^designation[0].language = #de-AT 
* #1466 ^designation[0].value = "TODA PHARMA, TODA CORONADIAG Ag" 
* #1466 ^property[0].code = #Relationships 
* #1466 ^property[0].valueString = "$.t..ma" 
* #1468 "ACON Laboratories, Inc, Flowflex SARS-CoV-2 Antigen Rapid Test"
* #1468 ^definition = ACON Laboratories, Inc, Flowflex SARS-CoV-2 Antigen Rapid Test
* #1468 ^designation[0].language = #de-AT 
* #1468 ^designation[0].value = "ACON Laboratories, Inc, Flowflex SARS-CoV-2 Antigen Rapid Test " 
* #1468 ^property[0].code = #Relationships 
* #1468 ^property[0].valueString = "$.t..ma" 
* #1481 "MP Biomedicals , Rapid SARS-CoV-2 Antigen Test Card"
* #1481 ^definition = MP Biomedicals , Rapid SARS-CoV-2 Antigen Test Card
* #1481 ^designation[0].language = #de-AT 
* #1481 ^designation[0].value = "MP Biomedicals , Rapid SARS-CoV-2 Antigen Test Card" 
* #1481 ^property[0].code = #Relationships 
* #1481 ^property[0].valueString = "$.t..ma" 
* #1485 "Beijing Wantai Biological Pharmacy Enterprise Co., Ltd, Wantai SARS-CoV-2 Ag Rapid Test (colloidal gold)"
* #1485 ^definition = Beijing Wantai Biological Pharmacy Enterprise Co., Ltd, Wantai SARS-CoV-2 Ag Rapid Test (colloidal gold)
* #1485 ^designation[0].language = #de-AT 
* #1485 ^designation[0].value = "Beijing Wantai Biological Pharmacy Enterprise Co., Ltd, Wantai SARS-CoV-2 Ag Rapid Test (colloidal gold)" 
* #1485 ^property[0].code = #Relationships 
* #1485 ^property[0].valueString = "$.t..ma" 
* #1489 "Safecare Biotech Hangzhou Co, COVID-19 Antigen Rapid Test Kit (Swab)"
* #1489 ^definition = Safecare Biotech Hangzhou Co, COVID-19 Antigen Rapid Test Kit (Swab)
* #1489 ^designation[0].language = #de-AT 
* #1489 ^designation[0].value = "Safecare Biotech Hangzhou Co, COVID-19 Antigen Rapid Test Kit (Swab)" 
* #1489 ^property[0].code = #Relationships 
* #1489 ^property[0].valueString = "$.t..ma" 
* #1490 "Safecare Biotech Hangzhou Co, Multi-Respiratory Virus Antigen Test Kit (Swab) (Influenza A+B/COVID-19)"
* #1490 ^definition = Safecare Biotech Hangzhou Co, Multi-Respiratory Virus Antigen Test Kit (Swab) (Influenza A+B/COVID-19)
* #1490 ^designation[0].language = #de-AT 
* #1490 ^designation[0].value = "Safecare Biotech Hangzhou Co, Multi-Respiratory Virus Antigen Test Kit (Swab) (Influenza A+B/COVID-19)" 
* #1490 ^property[0].code = #Relationships 
* #1490 ^property[0].valueString = "$.t..ma" 
* #1494 "BIOSYNEX SA, BIOSYNEX COVID-19 Ag+ BSS"
* #1494 ^definition = BIOSYNEX SA, BIOSYNEX COVID-19 Ag+ BSS
* #1494 ^designation[0].language = #de-AT 
* #1494 ^designation[0].value = "BIOSYNEX SA, BIOSYNEX COVID-19 Ag+ BSS" 
* #1494 ^property[0].code = #Relationships 
* #1494 ^property[0].valueString = "$.t..ma" 
* #1495 "Prognosis Biotech, Rapid Test Ag 2019-nCov"
* #1495 ^definition = Prognosis Biotech, Rapid Test Ag 2019-nCov
* #1495 ^designation[0].language = #de-AT 
* #1495 ^designation[0].value = "Prognosis Biotech, Rapid Test Ag 2019-nCov" 
* #1495 ^property[0].code = #Relationships 
* #1495 ^property[0].valueString = "$.t..ma" 
* #1501 "New Gene (Hangzhou) Bioengineering Co., Ltd., COVID-19 Antigen Detection Kit"
* #1501 ^definition = New Gene (Hangzhou) Bioengineering Co., Ltd., COVID-19 Antigen Detection Kit
* #1501 ^designation[0].language = #de-AT 
* #1501 ^designation[0].value = "New Gene (Hangzhou) Bioengineering Co., Ltd., COVID-19 Antigen Detection Kit" 
* #1501 ^property[0].code = #Relationships 
* #1501 ^property[0].valueString = "$.t..ma" 
* #1573 "Nantong Egens Biotechnology Co.,Ltd, COVID-19 Antigen Rapid Test Kit"
* #1573 ^definition = Nantong Egens Biotechnology Co.,Ltd, COVID-19 Antigen Rapid Test Kit
* #1573 ^designation[0].language = #de-AT 
* #1573 ^designation[0].value = "Nantong Egens Biotechnology Co.,Ltd, COVID-19 Antigen Rapid Test Kit" 
* #1573 ^property[0].code = #Relationships 
* #1573 ^property[0].valueString = "$.t..ma" 
* #1581 "CTK Biotech, Inc, OnSite COVID-19 Ag Rapid Test"
* #1581 ^definition = CTK Biotech, Inc, OnSite COVID-19 Ag Rapid Test
* #1581 ^designation[0].language = #de-AT 
* #1581 ^designation[0].value = "CTK Biotech, Inc, OnSite COVID-19 Ag Rapid Test" 
* #1581 ^property[0].code = #Relationships 
* #1581 ^property[0].valueString = "$.t..ma" 
* #1592 "Shenzhen Lifotronic Technology Co., Ltd., Antigen Rapid Test Ag SARS-CoV-2"
* #1592 ^definition = Shenzhen Lifotronic Technology Co., Ltd., Antigen Rapid Test Ag SARS-CoV-2
* #1592 ^designation[0].language = #de-AT 
* #1592 ^designation[0].value = "Shenzhen Lifotronic Technology Co., Ltd., Antigen Rapid Test Ag SARS-CoV-2" 
* #1592 ^property[0].code = #Relationships 
* #1592 ^property[0].valueString = "$.t..ma" 
* #1593 "OSANG Healthcare Co., Ltd., GeneFinder COVID-19 Ag Rapid Test"
* #1593 ^definition = OSANG Healthcare Co., Ltd., GeneFinder COVID-19 Ag Rapid Test
* #1593 ^designation[0].language = #de-AT 
* #1593 ^designation[0].value = "OSANG Healthcare Co., Ltd., GeneFinder COVID-19 Ag Rapid Test" 
* #1593 ^property[0].code = #Relationships 
* #1593 ^property[0].valueString = "$.t..ma" 
* #1599 "Biomerica Inc., Biomerica COVID-19 Antigen Rapid Test (nasopharyngeal swab)"
* #1599 ^definition = Biomerica Inc., Biomerica COVID-19 Antigen Rapid Test (nasopharyngeal swab)
* #1599 ^designation[0].language = #de-AT 
* #1599 ^designation[0].value = "Biomerica Inc., Biomerica COVID-19 Antigen Rapid Test (nasopharyngeal swab)" 
* #1599 ^property[0].code = #Relationships 
* #1599 ^property[0].valueString = "$.t..ma" 
* #1604 "Roche (SD BIOSENSOR), SARS-CoV-2 Antigen Rapid Test"
* #1604 ^definition = Roche (SD BIOSENSOR), SARS-CoV-2 Antigen Rapid Test
* #1604 ^designation[0].language = #de-AT 
* #1604 ^designation[0].value = "Roche (SD BIOSENSOR), SARS-CoV-2 Antigen Rapid Test" 
* #1604 ^property[0].code = #Relationships 
* #1604 ^property[0].valueString = "$.t..ma" 
* #1610 "Hangzhou Clongene Biotech Co., Ltd, COVID-19 Antigen Rapid Test Casette"
* #1610 ^definition = Hangzhou Clongene Biotech Co., Ltd, COVID-19 Antigen Rapid Test Casette
* #1610 ^designation[0].language = #de-AT 
* #1610 ^designation[0].value = "Hangzhou Clongene Biotech Co., Ltd, COVID-19 Antigen Rapid Test Casette" 
* #1610 ^property[0].code = #Relationships 
* #1610 ^property[0].valueString = "$.t..ma" 
* #1618 "Artron Laboratories Inc., Artron COVID-19 Antigen Test"
* #1618 ^definition = Artron Laboratories Inc., Artron COVID-19 Antigen Test
* #1618 ^designation[0].language = #de-AT 
* #1618 ^designation[0].value = "Artron Laboratories Inc., Artron COVID-19 Antigen Test" 
* #1618 ^property[0].code = #Relationships 
* #1618 ^property[0].valueString = "$.t..ma" 
* #1647 "CALTH Inc., AllCheck COVID19 Ag"
* #1647 ^definition = CALTH Inc., AllCheck COVID19 Ag
* #1647 ^designation[0].language = #de-AT 
* #1647 ^designation[0].value = "CALTH Inc., AllCheck COVID19 Ag" 
* #1647 ^property[0].code = #Relationships 
* #1647 ^property[0].valueString = "$.t..ma" 
* #1654 "Asan Pharmaceutical CO., LTD, Asan Easy Test COVID-19 Ag"
* #1654 ^definition = Asan Pharmaceutical CO., LTD, Asan Easy Test COVID-19 Ag
* #1654 ^designation[0].language = #de-AT 
* #1654 ^designation[0].value = "Asan Pharmaceutical CO., LTD, Asan Easy Test COVID-19 Ag" 
* #1654 ^property[0].code = #Relationships 
* #1654 ^property[0].valueString = "$.t..ma" 
* #1689 "TÜRKLAB TIBBI MALZEMELER SAN. ve TIC. A.S., Covid-19 Ag Test"
* #1689 ^definition = TÜRKLAB TIBBI MALZEMELER SAN. ve TIC. A.S., Covid-19 Ag Test
* #1689 ^designation[0].language = #de-AT 
* #1689 ^designation[0].value = "TÜRKLAB TIBBI MALZEMELER SAN. ve TIC. A.S., Covid-19 Ag Test" 
* #1689 ^property[0].code = #Relationships 
* #1689 ^property[0].valueString = "$.t..ma" 
* #1691 "Chil Tibbi Malzeme Sanayi ve Ticaret Limited Sirketi, CHIL COVID-19 Antigen Rapid Test (Nasopharyngeal / Oropharyngeal Swab-Casette)"
* #1691 ^definition = Chil T?bbi Malzeme Sanayi ve Ticaret Limited Sirketi, CHIL COVID-19 Antigen Rapid Test (Nasopharyngeal / Oropharyngeal Swab-Casette)
* #1691 ^designation[0].language = #de-AT 
* #1691 ^designation[0].value = "Chil T?bbi Malzeme Sanayi ve Ticaret Limited Sirketi, CHIL COVID-19 Antigen Rapid Test (Nasopharyngeal / Oropharyngeal Swab-Casette)" 
* #1691 ^property[0].code = #Relationships 
* #1691 ^property[0].valueString = "$.t..ma" 
* #1722 "TÜRKLAB TIBBI MALZEMELER SAN. ve TIC. A.S., TOYO Covid-19 Ag Tes"
* #1722 ^definition = TÜRKLAB TIBBI MALZEMELER SAN. ve TIC. A.S., TOYO Covid-19 Ag Tes
* #1722 ^designation[0].language = #de-AT 
* #1722 ^designation[0].value = "TÜRKLAB TIBBI MALZEMELER SAN. ve TIC. A.S., TOYO Covid-19 Ag Tes" 
* #1722 ^property[0].code = #Relationships 
* #1722 ^property[0].valueString = "$.t..ma" 
* #1736 "Anhui Deep Blue Medical Technology Co. Ltd, COVID-19 (SARS-CoV-2) Antigen Test Kit (Colloidal Gold)"
* #1736 ^definition = Anhui Deep Blue Medical Technology Co. Ltd, COVID-19 (SARS-CoV-2) Antigen Test Kit (Colloidal Gold)
* #1736 ^designation[0].language = #de-AT 
* #1736 ^designation[0].value = "Anhui Deep Blue Medical Technology Co. Ltd, COVID-19 (SARS-CoV-2) Antigen Test Kit (Colloidal Gold)" 
* #1736 ^property[0].code = #Relationships 
* #1736 ^property[0].valueString = "$.t..ma" 
* #1739 "Eurobio Scientific, EBS SARS-CoV-2 Ag Rapid Test"
* #1739 ^definition = Eurobio Scientific, EBS SARS-CoV-2 Ag Rapid Test
* #1739 ^designation[0].language = #de-AT 
* #1739 ^designation[0].value = "Eurobio Scientific, EBS SARS-CoV-2 Ag Rapid Test" 
* #1739 ^property[0].code = #Relationships 
* #1739 ^property[0].valueString = "$.t..ma" 
* #1747 "Guangdong Hecin Scientific, Inc., 2019-nCoV Antigen Test Kit (colloidal gold method)"
* #1747 ^definition = Guangdong Hecin Scientific, Inc., 2019-nCoV Antigen Test Kit (colloidal gold method)
* #1747 ^designation[0].language = #de-AT 
* #1747 ^designation[0].value = "Guangdong Hecin Scientific, Inc., 2019-nCoV Antigen Test Kit (colloidal gold method)" 
* #1747 ^property[0].code = #Relationships 
* #1747 ^property[0].valueString = "$.t..ma" 
* #1751 "TÜRKLAB TIBBI MALZEMELER SAN. ve TIC. A.S., RAPIDAN TESTER Covid-19 Ag Test"
* #1751 ^definition = TÜRKLAB TIBBI MALZEMELER SAN. ve TIC. A.S., RAPIDAN TESTER Covid-19 Ag Test
* #1751 ^designation[0].language = #de-AT 
* #1751 ^designation[0].value = "TÜRKLAB TIBBI MALZEMELER SAN. ve TIC. A.S., RAPIDAN TESTER Covid-19 Ag Test" 
* #1751 ^property[0].code = #Relationships 
* #1751 ^property[0].valueString = "$.t..ma" 
* #1759 "Hubei Jinjian Biology Co., Ltd, SARS-CoV-2 Antigen Test Kit"
* #1759 ^definition = Hubei Jinjian Biology Co., Ltd, SARS-CoV-2 Antigen Test Kit
* #1759 ^designation[0].language = #de-AT 
* #1759 ^designation[0].value = "Hubei Jinjian Biology Co., Ltd, SARS-CoV-2 Antigen Test Kit" 
* #1759 ^property[0].code = #Relationships 
* #1759 ^property[0].valueString = "$.t..ma" 
* #1762 "Novatech, SARS-CoV-2 Antigen Rapid Test"
* #1762 ^definition = Novatech, SARS-CoV-2 Antigen Rapid Test
* #1762 ^designation[0].language = #de-AT 
* #1762 ^designation[0].value = "Novatech, SARS-CoV-2 Antigen Rapid Test" 
* #1762 ^property[0].code = #Relationships 
* #1762 ^property[0].valueString = "$.t..ma" 
* #1763 "Xiamen AmonMed Biotechnology Co., Ltd., COVID-19 Antigen Rapid Test Kit (Colloidal Gold)"
* #1763 ^definition = Xiamen AmonMed Biotechnology Co., Ltd., COVID-19 Antigen Rapid Test Kit (Colloidal Gold)
* #1763 ^designation[0].language = #de-AT 
* #1763 ^designation[0].value = "Xiamen AmonMed Biotechnology Co., Ltd., COVID-19 Antigen Rapid Test Kit (Colloidal Gold)  " 
* #1763 ^property[0].code = #Relationships 
* #1763 ^property[0].valueString = "$.t..ma" 
* #1764 "JOYSBIO (Tianjin) Biotechnology Co., Ltd., SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold immunochromatography)"
* #1764 ^definition = JOYSBIO (Tianjin) Biotechnology Co., Ltd., SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold immunochromatography)
* #1764 ^designation[0].language = #de-AT 
* #1764 ^designation[0].value = "JOYSBIO (Tianjin) Biotechnology Co., Ltd., SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold immunochromatography)" 
* #1764 ^property[0].code = #Relationships 
* #1764 ^property[0].valueString = "$.t..ma" 
* #1767 "Healgen Scientific Limited, Coronavirus Ag Rapid Test Cassette (Swab)"
* #1767 ^definition = Healgen Scientific Limited, Coronavirus Ag Rapid Test Cassette (Swab)
* #1767 ^designation[0].language = #de-AT 
* #1767 ^designation[0].value = "Healgen Scientific Limited, Coronavirus Ag Rapid Test Cassette (Swab)" 
* #1767 ^property[0].code = #Relationships 
* #1767 ^property[0].valueString = "$.t..ma" 
* #1768 "Shenzhen Watmind Medical Co., Ltd, SARS-CoV-2 Ag Diagnostic Test Kit (Immuno-fluorescence)"
* #1768 ^definition = Shenzhen Watmind Medical Co., Ltd, SARS-CoV-2 Ag Diagnostic Test Kit (Immuno-fluorescence)
* #1768 ^designation[0].language = #de-AT 
* #1768 ^designation[0].value = "Shenzhen Watmind Medical Co., Ltd, SARS-CoV-2 Ag Diagnostic Test Kit (Immuno-fluorescence)" 
* #1768 ^property[0].code = #Relationships 
* #1768 ^property[0].valueString = "$.t..ma" 
* #1769 "Shenzhen Watmind Medical Co., Ltd, SARS-CoV-2 Ag Diagnostic Test Kit (Colloidal Gold)"
* #1769 ^definition = Shenzhen Watmind Medical Co., Ltd, SARS-CoV-2 Ag Diagnostic Test Kit (Colloidal Gold)
* #1769 ^designation[0].language = #de-AT 
* #1769 ^designation[0].value = "Shenzhen Watmind Medical Co., Ltd, SARS-CoV-2 Ag Diagnostic Test Kit (Colloidal Gold)" 
* #1769 ^property[0].code = #Relationships 
* #1769 ^property[0].valueString = "$.t..ma" 
* #1773 "Wuhan Life Origin Biotech Joint Stock Co., Ltd., SARS-CoV-2 Antigen Assay Kit (Immunochromatography)"
* #1773 ^definition = Wuhan Life Origin Biotech Joint Stock Co., Ltd., SARS-CoV-2 Antigen Assay Kit (Immunochromatography)
* #1773 ^designation[0].language = #de-AT 
* #1773 ^designation[0].value = "Wuhan Life Origin Biotech Joint Stock Co., Ltd., SARS-CoV-2 Antigen Assay Kit (Immunochromatography)" 
* #1773 ^property[0].code = #Relationships 
* #1773 ^property[0].valueString = "$.t..ma" 
* #1775 "MEXACARE GmbH, MEXACARE COVID-19 Antigen Rapid Test"
* #1775 ^definition = MEXACARE GmbH, MEXACARE COVID-19 Antigen Rapid Test
* #1775 ^designation[0].language = #de-AT 
* #1775 ^designation[0].value = "MEXACARE GmbH, MEXACARE COVID-19 Antigen Rapid Test" 
* #1775 ^property[0].code = #Relationships 
* #1775 ^property[0].valueString = "$.t..ma" 
* #1778 "Beijing Kewei Clinical Diagnostic Reagent Inc, COVID19 Antigen Rapid Test Kit"
* #1778 ^definition = Beijing Kewei Clinical Diagnostic Reagent Inc, COVID19 Antigen Rapid Test Kit
* #1778 ^designation[0].language = #de-AT 
* #1778 ^designation[0].value = "Beijing Kewei Clinical Diagnostic Reagent Inc, COVID19 Antigen Rapid Test Kit" 
* #1778 ^property[0].code = #Relationships 
* #1778 ^property[0].valueString = "$.t..ma" 
* #1780 "Spring Healthcare Services AG, SARS-Cov-2 Antigen Rapid Test Cassette (swab)"
* #1780 ^definition = Spring Healthcare Services AG, SARS-Cov-2 Antigen Rapid Test Cassette (swab)
* #1780 ^designation[0].language = #de-AT 
* #1780 ^designation[0].value = "Spring Healthcare Services AG, SARS-Cov-2 Antigen Rapid Test Cassette (swab)" 
* #1780 ^property[0].code = #Relationships 
* #1780 ^property[0].valueString = "$.t..ma" 
* #1783 "InTec PRODUCTS, INC, Rapid SARS-CoV-2 Antigen Test (nasopharyngeal/nasal specimen)"
* #1783 ^definition = InTec PRODUCTS, INC, Rapid SARS-CoV-2 Antigen Test (nasopharyngeal/nasal specimen)
* #1783 ^designation[0].language = #de-AT 
* #1783 ^designation[0].value = "InTec PRODUCTS, INC, Rapid SARS-CoV-2 Antigen Test (nasopharyngeal/nasal specimen)" 
* #1783 ^property[0].code = #Relationships 
* #1783 ^property[0].valueString = "$.t..ma" 
* #1791 "Immunospark s.r.l., Rapid SARS-Cov2 Antigen Test"
* #1791 ^definition = Immunospark s.r.l., Rapid SARS-Cov2 Antigen Test
* #1791 ^designation[0].language = #de-AT 
* #1791 ^designation[0].value = "Immunospark s.r.l., Rapid SARS-Cov2 Antigen Test" 
* #1791 ^property[0].code = #Relationships 
* #1791 ^property[0].valueString = "$.t..ma" 
* #1800 "Avalun, Ksmart SARS-COV2 Antigen Rapid Test"
* #1800 ^definition = Avalun, Ksmart SARS-COV2 Antigen Rapid Test
* #1800 ^designation[0].language = #de-AT 
* #1800 ^designation[0].value = "Avalun, Ksmart SARS-COV2 Antigen Rapid Test" 
* #1800 ^property[0].code = #Relationships 
* #1800 ^property[0].valueString = "$.t..ma" 
* #1801 "Innova Medical Group.Inc, Innova SARS-CoV-2 Antigen Rapid Qualitative Test"
* #1801 ^definition = Innova Medical Group.Inc, Innova SARS-CoV-2 Antigen Rapid Qualitative Test
* #1801 ^designation[0].language = #de-AT 
* #1801 ^designation[0].value = "Innova Medical Group.Inc, Innova SARS-CoV-2 Antigen Rapid Qualitative Test" 
* #1801 ^property[0].code = #Relationships 
* #1801 ^property[0].valueString = "$.t..ma" 
* #1813 "Shenzhen Kisshealth Biotechnology Co., Ltd, SARS-CoV-2 Antigen Test Kit (GICA)"
* #1813 ^definition = Shenzhen Kisshealth Biotechnology Co., Ltd, SARS-CoV-2 Antigen Test Kit (GICA)
* #1813 ^designation[0].language = #de-AT 
* #1813 ^designation[0].value = "Shenzhen Kisshealth Biotechnology Co., Ltd, SARS-CoV-2 Antigen Test Kit (GICA)" 
* #1813 ^property[0].code = #Relationships 
* #1813 ^property[0].valueString = "$.t..ma" 
* #1815 "Anhui DeepBlue Medical Technology Co. Ltd, COVID-19 (SARS-CoV-2) Antigen Test Kit (Colloidal Gold) ? Nasal swab"
* #1815 ^definition = Anhui DeepBlue Medical Technology Co. Ltd, COVID-19 (SARS-CoV-2) Antigen Test Kit (Colloidal Gold) ? Nasal swab
* #1815 ^designation[0].language = #de-AT 
* #1815 ^designation[0].value = "Anhui DeepBlue Medical Technology Co. Ltd, COVID-19 (SARS-CoV-2) Antigen Test Kit (Colloidal Gold) ? Nasal swab" 
* #1815 ^property[0].code = #Relationships 
* #1815 ^property[0].valueString = "$.t..ma" 
* #1820 "Getein Biotech, Inc, SARS-CoV-2 Antigen (Colloidal Gold)"
* #1820 ^definition = Getein Biotech, Inc, SARS-CoV-2 Antigen (Colloidal Gold)
* #1820 ^designation[0].language = #de-AT 
* #1820 ^designation[0].value = "Getein Biotech, Inc, SARS-CoV-2 Antigen (Colloidal Gold)" 
* #1820 ^property[0].code = #Relationships 
* #1820 ^property[0].valueString = "$.t..ma" 
* #1822 "Anbio (Xiamen) Biotechnology Co., Ltd., Rapid COVID-19 Antigen-Test (colloidal Gold)"
* #1822 ^definition = Anbio (Xiamen) Biotechnology Co., Ltd., Rapid COVID-19 Antigen-Test (colloidal Gold)
* #1822 ^designation[0].language = #de-AT 
* #1822 ^designation[0].value = "Anbio (Xiamen) Biotechnology Co., Ltd., Rapid COVID-19 Antigen-Test (colloidal Gold)" 
* #1822 ^property[0].code = #Relationships 
* #1822 ^property[0].valueString = "$.t..ma" 
* #1833 "AAZ-LMB, COVID-VIRO Rapid antigen test COVID-19"
* #1833 ^definition = AAZ-LMB, COVID-VIRO Rapid antigen test COVID-19
* #1833 ^designation[0].language = #de-AT 
* #1833 ^designation[0].value = "AAZ-LMB, COVID-VIRO Rapid antigen test COVID-19" 
* #1833 ^property[0].code = #Relationships 
* #1833 ^property[0].valueString = "$.t..ma" 
* #1855 "GA Generic Assays GmbH, GA CoV-2 Antigen Rapid Test"
* #1855 ^definition = GA Generic Assays GmbH, GA CoV-2 Antigen Rapid Test
* #1855 ^designation[0].language = #de-AT 
* #1855 ^designation[0].value = "GA Generic Assays GmbH, GA CoV-2 Antigen Rapid Test" 
* #1855 ^property[0].code = #Relationships 
* #1855 ^property[0].valueString = "$.t..ma" 
* #1865 "ACON Biotech(Hangzhou) Co., Ltd., Flowflex SARS-CoV-2 Antigen Rapid Test (Nasal/Saliva)"
* #1865 ^definition = ACON Biotech(Hangzhou) Co., Ltd., Flowflex SARS-CoV-2 Antigen Rapid Test (Nasal/Saliva)
* #1865 ^designation[0].language = #de-AT 
* #1865 ^designation[0].value = "ACON Biotech(Hangzhou) Co., Ltd., Flowflex SARS-CoV-2 Antigen Rapid Test (Nasal/Saliva)" 
* #1865 ^property[0].code = #Relationships 
* #1865 ^property[0].valueString = "$.t..ma" 
* #1870 "Beijing Hotgen Biotech Co., Ltd., Novel Coronavirus 2019-nCoV Antigen Test (Colloidal Gold)"
* #1870 ^definition = Beijing Hotgen Biotech Co., Ltd., Novel Coronavirus 2019-nCoV Antigen Test (Colloidal Gold)
* #1870 ^designation[0].language = #de-AT 
* #1870 ^designation[0].value = "Beijing Hotgen Biotech Co., Ltd., Novel Coronavirus 2019-nCoV Antigen Test (Colloidal Gold)" 
* #1870 ^property[0].code = #Relationships 
* #1870 ^property[0].valueString = "$.t..ma" 
* #1876 "Hangzhou Biotest Biotech Co., Ltd, COVID-19 Antigen Rapid Test Cassette(Nasal Swab)"
* #1876 ^definition = Hangzhou Biotest Biotech Co., Ltd, COVID-19 Antigen Rapid Test Cassette(Nasal Swab)
* #1876 ^designation[0].language = #de-AT 
* #1876 ^designation[0].value = "Hangzhou Biotest Biotech Co., Ltd, COVID-19 Antigen Rapid Test Cassette(Nasal Swab)" 
* #1876 ^property[0].code = #Relationships 
* #1876 ^property[0].valueString = "$.t..ma" 
* #1880 "NG Biotech, Ninonasal"
* #1880 ^definition = NG Biotech, Ninonasal
* #1880 ^designation[0].language = #de-AT 
* #1880 ^designation[0].value = "NG Biotech, Ninonasal" 
* #1880 ^property[0].code = #Relationships 
* #1880 ^property[0].valueString = "$.t..ma" 
* #1899 "Jiangsu Konsung Bio-Medical Science and Technology Co., H+W_COVID-19 Antigen Rapid Test Kit (Colloidal Gold)"
* #1899 ^definition = Jiangsu Konsung Bio-Medical Science and Technology Co., H+W_COVID-19 Antigen Rapid Test Kit (Colloidal Gold)
* #1899 ^designation[0].language = #de-AT 
* #1899 ^designation[0].value = "Jiangsu Konsung Bio-Medical Science and Technology Co., H+W_COVID-19 Antigen Rapid Test Kit (Colloidal Gold)" 
* #1899 ^property[0].code = #Relationships 
* #1899 ^property[0].valueString = "$.t..ma" 
* #1902 "Zhuhai Encode Medical Engineering Co.,Ltd, ENCODE SARS-COV-2 Antigen Rapid Test Device"
* #1902 ^definition = Zhuhai Encode Medical Engineering Co.,Ltd, ENCODE SARS-COV-2 Antigen Rapid Test Device
* #1902 ^designation[0].language = #de-AT 
* #1902 ^designation[0].value = "Zhuhai Encode Medical Engineering Co.,Ltd, ENCODE SARS-COV-2 Antigen Rapid Test Device" 
* #1902 ^property[0].code = #Relationships 
* #1902 ^property[0].valueString = "$.t..ma" 
* #1920 "Jiangsu Diagnostics Biotechnology Co., Ltd, COVID-19 Antigen Rapid Test Cassette (Colloidal Gold)"
* #1920 ^definition = Jiangsu Diagnostics Biotechnology Co., Ltd, COVID-19 Antigen Rapid Test Cassette (Colloidal Gold)
* #1920 ^designation[0].language = #de-AT 
* #1920 ^designation[0].value = "Jiangsu Diagnostics Biotechnology Co., Ltd, COVID-19 Antigen Rapid Test Cassette (Colloidal Gold)" 
* #1920 ^property[0].code = #Relationships 
* #1920 ^property[0].valueString = "$.t..ma" 
* #1926 "ARISTA Biotech Pte.LTD., ARISTA COVID-19 Antigen Rapid Test"
* #1926 ^definition = ARISTA Biotech Pte.LTD., ARISTA COVID-19 Antigen Rapid Test
* #1926 ^designation[0].language = #de-AT 
* #1926 ^designation[0].value = "ARISTA Biotech Pte.LTD., ARISTA COVID-19 Antigen Rapid Test" 
* #1926 ^property[0].code = #Relationships 
* #1926 ^property[0].valueString = "$.t..ma" 
* #1929 "Hoyotek Biomedical Co.,Ltd., Corona Virus (COVID-19) Antigen Rapid Test (Colloidal Gold)"
* #1929 ^definition = Hoyotek Biomedical Co.,Ltd., Corona Virus (COVID-19) Antigen Rapid Test (Colloidal Gold)
* #1929 ^designation[0].language = #de-AT 
* #1929 ^designation[0].value = "Hoyotek Biomedical Co.,Ltd., Corona Virus (COVID-19) Antigen Rapid Test (Colloidal Gold)" 
* #1929 ^property[0].code = #Relationships 
* #1929 ^property[0].valueString = "$.t..ma" 
* #1942 "Surge Medical Inc., COVID-19 Antigen Test Kit"
* #1942 ^definition = Surge Medical Inc., COVID-19 Antigen Test Kit
* #1942 ^designation[0].language = #de-AT 
* #1942 ^designation[0].value = "Surge Medical Inc., COVID-19 Antigen Test Kit" 
* #1942 ^property[0].code = #Relationships 
* #1942 ^property[0].valueString = "$.t..ma" 
* #1945 "Hangzhou Sejoy Electronics & Instruments Co.Ltd, SARS-CoV-2 Antigen Rapid Test Cassette"
* #1945 ^definition = Hangzhou Sejoy Electronics & Instruments Co.Ltd, SARS-CoV-2 Antigen Rapid Test Cassette
* #1945 ^designation[0].language = #de-AT 
* #1945 ^designation[0].value = "Hangzhou Sejoy Electronics & Instruments Co.Ltd, SARS-CoV-2 Antigen Rapid Test Cassette" 
* #1945 ^property[0].code = #Relationships 
* #1945 ^property[0].valueString = "$.t..ma" 
* #1952 "Hangzhou Sejoy Electronics & Instruments Co.Ltd, SARS-CoV-2 Antigen Rapid Test Cassette (nasal, nasopharyngeal, oropharyngeal, saliva)"
* #1952 ^definition = Hangzhou Sejoy Electronics & Instruments Co.Ltd, SARS-CoV-2 Antigen Rapid Test Cassette (nasal, nasopharyngeal, oropharyngeal, saliva)
* #1952 ^designation[0].language = #de-AT 
* #1952 ^designation[0].value = "Hangzhou Sejoy Electronics & Instruments Co.Ltd, SARS-CoV-2 Antigen Rapid Test Cassette (nasal, nasopharyngeal, oropharyngeal, saliva)" 
* #1952 ^property[0].code = #Relationships 
* #1952 ^property[0].valueString = "$.t..ma" 
* #1957 "Zhuhai Lituo Biotechnology Co., Ltd., COVID-19 Antigen Detection Kit (Colloidal Gold)"
* #1957 ^definition = Zhuhai Lituo Biotechnology Co., Ltd., COVID-19 Antigen Detection Kit (Colloidal Gold)
* #1957 ^designation[0].language = #de-AT 
* #1957 ^designation[0].value = "Zhuhai Lituo Biotechnology Co., Ltd., COVID-19 Antigen Detection Kit (Colloidal Gold)" 
* #1957 ^property[0].code = #Relationships 
* #1957 ^property[0].valueString = "$.t..ma" 
* #1967 "Shenzhen Microprofit Biotech Co., Ltd, SARS-CoV-2 Antigen Test Kit (Colloidal Gold Chromatographic Immunoassay)"
* #1967 ^definition = Shenzhen Microprofit Biotech Co., Ltd, SARS-CoV-2 Antigen Test Kit (Colloidal Gold Chromatographic Immunoassay)
* #1967 ^designation[0].language = #de-AT 
* #1967 ^designation[0].value = "Shenzhen Microprofit Biotech Co., Ltd, SARS-CoV-2 Antigen Test Kit (Colloidal Gold Chromatographic Immunoassay)" 
* #1967 ^property[0].code = #Relationships 
* #1967 ^property[0].valueString = "$.t..ma" 
* #1988 "Inzek International Trading B.V., Biozek covid-19 Antigen Rapidtest BCOV-502"
* #1988 ^definition = Inzek International Trading B.V., Biozek covid-19 Antigen Rapidtest BCOV-502
* #1988 ^designation[0].language = #de-AT 
* #1988 ^designation[0].value = "Inzek International Trading B.V., Biozek covid-19 Antigen Rapidtest BCOV-502" 
* #1988 ^property[0].code = #Relationships 
* #1988 ^property[0].valueString = "$.t..ma" 
* #1989 "Boditech Med Inc, AFIAS COVID-19 Ag"
* #1989 ^definition = Boditech Med Inc, AFIAS COVID-19 Ag
* #1989 ^designation[0].language = #de-AT 
* #1989 ^designation[0].value = "Boditech Med Inc, AFIAS COVID-19 Ag" 
* #1989 ^property[0].code = #Relationships 
* #1989 ^property[0].valueString = "$.t..ma" 
* #2006 "Jiangsu Medomics medical technology Co.,Ltd., SARS-CoV-2 antigen Test Kit (LFIA)"
* #2006 ^definition = Jiangsu Medomics medical technology Co.,Ltd., SARS-CoV-2 antigen Test Kit (LFIA)
* #2006 ^designation[0].language = #de-AT 
* #2006 ^designation[0].value = "Jiangsu Medomics medical technology Co.,Ltd., SARS-CoV-2 antigen Test Kit (LFIA)" 
* #2006 ^property[0].code = #Relationships 
* #2006 ^property[0].valueString = "$.t..ma" 
* #2012 "Genrui Biotech Inc, SARS-CoV-2 Antigen Test Kit (Colloidal Gold)"
* #2012 ^definition = Genrui Biotech Inc, SARS-CoV-2 Antigen Test Kit (Colloidal Gold)
* #2012 ^designation[0].language = #de-AT 
* #2012 ^designation[0].value = "Genrui Biotech Inc, SARS-CoV-2 Antigen Test Kit (Colloidal Gold)" 
* #2012 ^property[0].code = #Relationships 
* #2012 ^property[0].valueString = "$.t..ma" 
* #2013 "Biotical Health S.L.U.BIOTICAL HEALTH S.L.U, biotical SARS-CoV-2 Ag Card"
* #2013 ^definition = Biotical Health S.L.U.BIOTICAL HEALTH S.L.U, biotical SARS-CoV-2 Ag Card
* #2013 ^designation[0].language = #de-AT 
* #2013 ^designation[0].value = "Biotical Health S.L.U.BIOTICAL HEALTH S.L.U, biotical SARS-CoV-2 Ag Card" 
* #2013 ^property[0].code = #Relationships 
* #2013 ^property[0].valueString = "$.t..ma" 
* #2017 "Shenzen Ultra-Diagnostics Biotec Co., SARS-CoV-2 Antigen test Kit (colloidal gold)"
* #2017 ^definition = Shenzen Ultra-Diagnostics Biotec Co., SARS-CoV-2 Antigen test Kit (colloidal gold)
* #2017 ^designation[0].language = #de-AT 
* #2017 ^designation[0].value = "Shenzen Ultra-Diagnostics Biotec Co., SARS-CoV-2 Antigen test Kit (colloidal gold)" 
* #2017 ^property[0].code = #Relationships 
* #2017 ^property[0].valueString = "$.t..ma" 
* #2026 "Shenzhen Reagent Technology Co.,Ltd, SARS-CoV-2 antigen IVD kit SWAB"
* #2026 ^definition = Shenzhen Reagent Technology Co.,Ltd, SARS-CoV-2 antigen IVD kit SWAB
* #2026 ^designation[0].language = #de-AT 
* #2026 ^designation[0].value = "Shenzhen Reagent Technology Co.,Ltd, SARS-CoV-2 antigen IVD kit SWAB" 
* #2026 ^property[0].code = #Relationships 
* #2026 ^property[0].valueString = "$.t..ma" 
* #2029 "Merlin Biomedical (Xiamen) Co., Ltd., SARS-CoV-2 Antigen Rapid Test Cassette"
* #2029 ^definition = Merlin Biomedical (Xiamen) Co., Ltd., SARS-CoV-2 Antigen Rapid Test Cassette
* #2029 ^designation[0].language = #de-AT 
* #2029 ^designation[0].value = "Merlin Biomedical (Xiamen) Co., Ltd., SARS-CoV-2 Antigen Rapid Test Cassette" 
* #2029 ^property[0].code = #Relationships 
* #2029 ^property[0].valueString = "$.t..ma" 
* #2031 "BIO-RAD, CORONAVIRUS AG RAPID TEST CASSETTE"
* #2031 ^definition = BIO-RAD, CORONAVIRUS AG RAPID TEST CASSETTE
* #2031 ^designation[0].language = #de-AT 
* #2031 ^designation[0].value = "BIO-RAD, CORONAVIRUS AG RAPID TEST CASSETTE" 
* #2031 ^property[0].code = #Relationships 
* #2031 ^property[0].valueString = "$.t..ma" 
* #2035 "BioMaxima SA, SARS-CoV-2 Ag Rapid Test"
* #2035 ^definition = BioMaxima SA, SARS-CoV-2 Ag Rapid Test
* #2035 ^designation[0].language = #de-AT 
* #2035 ^designation[0].value = "BioMaxima SA, SARS-CoV-2 Ag Rapid Test" 
* #2035 ^property[0].code = #Relationships 
* #2035 ^property[0].valueString = "$.t..ma" 
* #2038 "Koch Biotechnology (Beijing) Co., Ltd, COVID-19 Antigen Rapid Test Kit"
* #2038 ^definition = Koch Biotechnology (Beijing) Co., Ltd, COVID-19 Antigen Rapid Test Kit
* #2038 ^designation[0].language = #de-AT 
* #2038 ^designation[0].value = "Koch Biotechnology (Beijing) Co., Ltd, COVID-19 Antigen Rapid Test Kit " 
* #2038 ^property[0].code = #Relationships 
* #2038 ^property[0].valueString = "$.t..ma" 
* #2052 "SD BIOSENSOR Inc. (manufacturer), STANDARD Q COVID-19 Ag Test Nasal"
* #2052 ^definition = SD BIOSENSOR Inc. (manufacturer), STANDARD Q COVID-19 Ag Test Nasal
* #2052 ^designation[0].language = #de-AT 
* #2052 ^designation[0].value = "SD BIOSENSOR Inc. (manufacturer), STANDARD Q COVID-19 Ag Test Nasal" 
* #2052 ^property[0].code = #Relationships 
* #2052 ^property[0].valueString = "$.t..ma" 
* #2067 "BIOTEKE CORPORATION (WUXI) CO., LTD, SARS-CoV-2 Antigen Test Kit (colloidal gold method)"
* #2067 ^definition = BIOTEKE CORPORATION (WUXI) CO., LTD, SARS-CoV-2 Antigen Test Kit (colloidal gold method)
* #2067 ^designation[0].language = #de-AT 
* #2067 ^designation[0].value = "BIOTEKE CORPORATION (WUXI) CO., LTD, SARS-CoV-2 Antigen Test Kit (colloidal gold method)" 
* #2067 ^property[0].code = #Relationships 
* #2067 ^property[0].valueString = "$.t..ma" 
* #2072 "Beijing Jinwofu Bioengineering Technology Co.,Ltd., Novel Coronavirus (SARS-CoV-2) Antigen Rapid Test Kit"
* #2072 ^definition = Beijing Jinwofu Bioengineering Technology Co.,Ltd., Novel Coronavirus (SARS-CoV-2) Antigen Rapid Test Kit
* #2072 ^designation[0].language = #de-AT 
* #2072 ^designation[0].value = "Beijing Jinwofu Bioengineering Technology Co.,Ltd., Novel Coronavirus (SARS-CoV-2) Antigen Rapid Test Kit" 
* #2072 ^property[0].code = #Relationships 
* #2072 ^property[0].valueString = "$.t..ma" 
* #2074 "Triplex International Biosciences Co., Ltd, SARS-CoV-2 Antigen Rapid Test Kit"
* #2074 ^definition = Triplex International Biosciences Co., Ltd, SARS-CoV-2 Antigen Rapid Test Kit
* #2074 ^designation[0].language = #de-AT 
* #2074 ^designation[0].value = "Triplex International Biosciences Co., Ltd, SARS-CoV-2 Antigen Rapid Test Kit" 
* #2074 ^property[0].code = #Relationships 
* #2074 ^property[0].valueString = "$.t..ma" 
* #2078 "ArcDia International Oy Ltd, mariPOC Respi+"
* #2078 ^definition = ArcDia International Oy Ltd, mariPOC Respi+
* #2078 ^designation[0].language = #de-AT 
* #2078 ^designation[0].value = "ArcDia International Oy Ltd, mariPOC Respi+" 
* #2078 ^property[0].code = #Relationships 
* #2078 ^property[0].valueString = "$.t..ma" 
* #2079 "ArcDia International Oy Ltd, mariPOC Quick Flu+"
* #2079 ^definition = ArcDia International Oy Ltd, mariPOC Quick Flu+
* #2079 ^designation[0].language = #de-AT 
* #2079 ^designation[0].value = "ArcDia International Oy Ltd, mariPOC Quick Flu+" 
* #2079 ^property[0].code = #Relationships 
* #2079 ^property[0].valueString = "$.t..ma" 
* #2089 "Anhui Formaster Biosci Co., Ltd., New Coronavirus (COVID-19) Antigen Rapid Test"
* #2089 ^definition = Anhui Formaster Biosci Co., Ltd., New Coronavirus (COVID-19) Antigen Rapid Test
* #2089 ^designation[0].language = #de-AT 
* #2089 ^designation[0].value = "Anhui Formaster Biosci Co., Ltd., New Coronavirus (COVID-19) Antigen Rapid Test" 
* #2089 ^property[0].code = #Relationships 
* #2089 ^property[0].valueString = "$.t..ma" 
* #2090 "Wuhan UNscience Biotechnology Co., Ltd., SARS-CoV-2 Antigen Rapid Test Kit"
* #2090 ^definition = Wuhan UNscience Biotechnology Co., Ltd., SARS-CoV-2 Antigen Rapid Test Kit
* #2090 ^designation[0].language = #de-AT 
* #2090 ^designation[0].value = "Wuhan UNscience Biotechnology Co., Ltd., SARS-CoV-2 Antigen Rapid Test Kit" 
* #2090 ^property[0].code = #Relationships 
* #2090 ^property[0].valueString = "$.t..ma" 
* #2097 "Sansure Biotech Inc, SARS-CoV-2 Rapid Antigen Test (Colloidal Gold Method)"
* #2097 ^definition = Sansure Biotech Inc, SARS-CoV-2 Rapid Antigen Test (Colloidal Gold Method)
* #2097 ^designation[0].language = #de-AT 
* #2097 ^designation[0].value = "Sansure Biotech Inc, SARS-CoV-2 Rapid Antigen Test (Colloidal Gold Method)" 
* #2097 ^property[0].code = #Relationships 
* #2097 ^property[0].valueString = "$.t..ma" 
* #2098 "Wuhan EasyDiagnosis Biomedicine Co., Ltd., COVID-19  (SARS-CoV-2) Antigen-Test kit"
* #2098 ^definition = Wuhan EasyDiagnosis Biomedicine Co., Ltd., COVID-19  (SARS-CoV-2) Antigen-Test kit
* #2098 ^designation[0].language = #de-AT 
* #2098 ^designation[0].value = "Wuhan EasyDiagnosis Biomedicine Co., Ltd., COVID-19  (SARS-CoV-2) Antigen-Test kit " 
* #2098 ^property[0].code = #Relationships 
* #2098 ^property[0].valueString = "$.t..ma" 
* #2100 "VivaChek Biotech (Hangzhou) Co., Ltd, China, Verino Pro SARS CoV 2 Ag Rapid Test"
* #2100 ^definition = VivaChek Biotech (Hangzhou) Co., Ltd, China, Verino Pro SARS CoV 2 Ag Rapid Test
* #2100 ^designation[0].language = #de-AT 
* #2100 ^designation[0].value = "VivaChek Biotech (Hangzhou) Co., Ltd, China, Verino Pro SARS CoV 2 Ag Rapid Test" 
* #2100 ^property[0].code = #Relationships 
* #2100 ^property[0].valueString = "$.t..ma" 
* #2101 "AXIOM Gesellschaft für Diagnostica und Biochemica mbH, COVID-19 Antigen Rapid Test"
* #2101 ^definition = AXIOM Gesellschaft für Diagnostica und Biochemica mbH, COVID-19 Antigen Rapid Test
* #2101 ^designation[0].language = #de-AT 
* #2101 ^designation[0].value = "AXIOM Gesellschaft für Diagnostica und Biochemica mbH, COVID-19 Antigen Rapid Test" 
* #2101 ^property[0].code = #Relationships 
* #2101 ^property[0].valueString = "$.t..ma" 
* #2107 "Jiangsu Bioperfectus Technologies Co., Ltd., Novel Corona Virus (SARS-CoV-2) Ag Rapid Test Kit"
* #2107 ^definition = Jiangsu Bioperfectus Technologies Co., Ltd., Novel Corona Virus (SARS-CoV-2) Ag Rapid Test Kit
* #2107 ^designation[0].language = #de-AT 
* #2107 ^designation[0].value = "Jiangsu Bioperfectus Technologies Co., Ltd., Novel Corona Virus (SARS-CoV-2) Ag Rapid Test Kit" 
* #2107 ^property[0].code = #Relationships 
* #2107 ^property[0].valueString = "$.t..ma" 
* #2109 "Shenzhen Lvshiyuan Biotechnology Co., Ltd., Green Spring SARS-CoV-2 Antigen-Rapid test-Set"
* #2109 ^definition = Shenzhen Lvshiyuan Biotechnology Co., Ltd., Green Spring SARS-CoV-2 Antigen-Rapid test-Set
* #2109 ^designation[0].language = #de-AT 
* #2109 ^designation[0].value = "Shenzhen Lvshiyuan Biotechnology Co., Ltd., Green Spring SARS-CoV-2 Antigen-Rapid test-Set" 
* #2109 ^property[0].code = #Relationships 
* #2109 ^property[0].valueString = "$.t..ma" 
* #2111 "VivaChek Biotech (Hangzhou) Co., Ltd, SARS-CoV-2 Ag Rapid Test"
* #2111 ^definition = VivaChek Biotech (Hangzhou) Co., Ltd, SARS-CoV-2 Ag Rapid Test
* #2111 ^designation[0].language = #de-AT 
* #2111 ^designation[0].value = "VivaChek Biotech (Hangzhou) Co., Ltd, SARS-CoV-2 Ag Rapid Test" 
* #2111 ^property[0].code = #Relationships 
* #2111 ^property[0].valueString = "$.t..ma" 
* #2116 "PerGrande Biotech Development Co., Ltd., SARS-CoV-2 Antigen Detection Kit (Colloidal Gold Immunochromato-graphic assay)"
* #2116 ^definition = PerGrande Biotech Development Co., Ltd., SARS-CoV-2 Antigen Detection Kit (Colloidal Gold Immunochromato-graphic assay)
* #2116 ^designation[0].language = #de-AT 
* #2116 ^designation[0].value = "PerGrande Biotech Development Co., Ltd., SARS-CoV-2 Antigen Detection Kit (Colloidal Gold Immunochromato-graphic assay)" 
* #2116 ^property[0].code = #Relationships 
* #2116 ^property[0].valueString = "$.t..ma" 
* #2124 "Fujirebio, Lumipulse G SARS-CoV-2 Ag"
* #2124 ^definition = Fujirebio, Lumipulse G SARS-CoV-2 Ag
* #2124 ^designation[0].language = #de-AT 
* #2124 ^designation[0].value = "Fujirebio, Lumipulse G SARS-CoV-2 Ag" 
* #2124 ^property[0].code = #Relationships 
* #2124 ^property[0].valueString = "$.t..ma" 
* #2128 "Lumigenex (Suzhou) Co., Ltd, PocRoc SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold)"
* #2128 ^definition = Lumigenex (Suzhou) Co., Ltd, PocRoc SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold)
* #2128 ^designation[0].language = #de-AT 
* #2128 ^designation[0].value = "Lumigenex (Suzhou) Co., Ltd, PocRoc SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold)" 
* #2128 ^property[0].code = #Relationships 
* #2128 ^property[0].valueString = "$.t..ma" 
* #2130 "Affimedix Inc., TestNOW - COVID-19 Antigen Test"
* #2130 ^definition = Affimedix Inc., TestNOW - COVID-19 Antigen Test
* #2130 ^designation[0].language = #de-AT 
* #2130 ^designation[0].value = "Affimedix Inc., TestNOW - COVID-19 Antigen Test" 
* #2130 ^property[0].code = #Relationships 
* #2130 ^property[0].valueString = "$.t..ma" 
* #2139 "Hangzhou Lysun Biotechnology Co., Ltd., COVID-19 antigen Rapid Test Device (Colloidal Gold)"
* #2139 ^definition = Hangzhou Lysun Biotechnology Co., Ltd., COVID-19 antigen Rapid Test Device (Colloidal Gold)
* #2139 ^designation[0].language = #de-AT 
* #2139 ^designation[0].value = "Hangzhou Lysun Biotechnology Co., Ltd., COVID-19 antigen Rapid Test Device (Colloidal Gold)" 
* #2139 ^property[0].code = #Relationships 
* #2139 ^property[0].valueString = "$.t..ma" 
* #2143 "Wuxi Biohermes Bio & Medical Technology Co., Ltd, SARS-CoV-2 Antigen Test Kit (Lateral Flow Assay)"
* #2143 ^definition = Wuxi Biohermes Bio & Medical Technology Co., Ltd, SARS-CoV-2 Antigen Test Kit (Lateral Flow Assay)
* #2143 ^designation[0].language = #de-AT 
* #2143 ^designation[0].value = "Wuxi Biohermes Bio & Medical Technology Co., Ltd, SARS-CoV-2 Antigen Test Kit (Lateral Flow Assay)" 
* #2143 ^property[0].code = #Relationships 
* #2143 ^property[0].valueString = "$.t..ma" 
* #2144 "Jiangsu Well Biotech Co., Ltd., COVID-19 Ag Rapid Test Device"
* #2144 ^definition = Jiangsu Well Biotech Co., Ltd., COVID-19 Ag Rapid Test Device
* #2144 ^designation[0].language = #de-AT 
* #2144 ^designation[0].value = "Jiangsu Well Biotech Co., Ltd., COVID-19 Ag Rapid Test Device" 
* #2144 ^property[0].code = #Relationships 
* #2144 ^property[0].valueString = "$.t..ma" 
* #2147 "Fujirebio , ESPLINE SARS-CoV-2"
* #2147 ^definition = Fujirebio , ESPLINE SARS-CoV-2
* #2147 ^designation[0].language = #de-AT 
* #2147 ^designation[0].value = "Fujirebio , ESPLINE SARS-CoV-2" 
* #2147 ^property[0].code = #Relationships 
* #2147 ^property[0].valueString = "$.t..ma" 
* #2150 "Chongqing M&D Biotechnology Co. Ltd, 2019-nCoV Antigen Test Kit"
* #2150 ^definition = Chongqing M&D Biotechnology Co. Ltd, 2019-nCoV Antigen Test Kit
* #2150 ^designation[0].language = #de-AT 
* #2150 ^designation[0].value = "Chongqing M&D Biotechnology Co. Ltd, 2019-nCoV Antigen Test Kit" 
* #2150 ^property[0].code = #Relationships 
* #2150 ^property[0].valueString = "$.t..ma" 
* #2152 "Shenzhen CAS-Envision Medical Technology Co., Ltd., SARS-CoV-2-Antigen Rapid Detection Kit"
* #2152 ^definition = Shenzhen CAS-Envision Medical Technology Co., Ltd., SARS-CoV-2-Antigen Rapid Detection Kit
* #2152 ^designation[0].language = #de-AT 
* #2152 ^designation[0].value = "Shenzhen CAS-Envision Medical Technology Co., Ltd., SARS-CoV-2-Antigen Rapid Detection Kit" 
* #2152 ^property[0].code = #Relationships 
* #2152 ^property[0].valueString = "$.t..ma" 
* #2164 "Nanjing Synthgene Medical Technology Co., Ltd., SARS-COV-2 Nucleocapsid (N) Antigen Rapid Detection Kit (Colloidal gold method)"
* #2164 ^definition = Nanjing Synthgene Medical Technology Co., Ltd., SARS-COV-2 Nucleocapsid (N) Antigen Rapid Detection Kit (Colloidal gold method)
* #2164 ^designation[0].language = #de-AT 
* #2164 ^designation[0].value = "Nanjing Synthgene Medical Technology Co., Ltd., SARS-COV-2 Nucleocapsid (N) Antigen Rapid Detection Kit (Colloidal gold method)" 
* #2164 ^property[0].code = #Relationships 
* #2164 ^property[0].valueString = "$.t..ma" 
* #2183 "Getein Biotech, Inc., One Step Test for SARS-CoV-2 Antigen (Colloidal Gold)"
* #2183 ^definition = Getein Biotech, Inc., One Step Test for SARS-CoV-2 Antigen (Colloidal Gold)
* #2183 ^designation[0].language = #de-AT 
* #2183 ^designation[0].value = "Getein Biotech, Inc., One Step Test for SARS-CoV-2 Antigen (Colloidal Gold)" 
* #2183 ^property[0].code = #Relationships 
* #2183 ^property[0].valueString = "$.t..ma" 
* #2200 "NanoRepro AG, NanoRepro SARS-CoV-2 Antigen Rapid Test"
* #2200 ^definition = NanoRepro AG, NanoRepro SARS-CoV-2 Antigen Rapid Test
* #2200 ^designation[0].language = #de-AT 
* #2200 ^designation[0].value = "NanoRepro AG, NanoRepro SARS-CoV-2 Antigen Rapid Test" 
* #2200 ^property[0].code = #Relationships 
* #2200 ^property[0].valueString = "$.t..ma" 
* #2201 "Zybio Inc., SARS-CoV-2 Antigen Assay Kit (Colloidal Gold Method)"
* #2201 ^definition = Zybio Inc., SARS-CoV-2 Antigen Assay Kit (Colloidal Gold Method)
* #2201 ^designation[0].language = #de-AT 
* #2201 ^designation[0].value = "Zybio Inc., SARS-CoV-2 Antigen Assay Kit (Colloidal Gold Method)" 
* #2201 ^property[0].code = #Relationships 
* #2201 ^property[0].valueString = "$.t..ma" 
* #2228 "Roche (SD BIOSENSOR), SARS-CoV-2 Rapid Antigen Test Nasal"
* #2228 ^definition = Roche (SD BIOSENSOR), SARS-CoV-2 Rapid Antigen Test Nasal
* #2228 ^designation[0].language = #de-AT 
* #2228 ^designation[0].value = "Roche (SD BIOSENSOR), SARS-CoV-2 Rapid Antigen Test Nasal" 
* #2228 ^property[0].code = #Relationships 
* #2228 ^property[0].valueString = "$.t..ma" 
* #2230 "BIOHIT HealthCcare (Hefei) Co., Ltd., SARS-CoV-2 Antigen Rapid Test (Colloidal Gold Method)"
* #2230 ^definition = BIOHIT HealthCcare (Hefei) Co., Ltd., SARS-CoV-2 Antigen Rapid Test (Colloidal Gold Method)
* #2230 ^designation[0].language = #de-AT 
* #2230 ^designation[0].value = "BIOHIT HealthCcare (Hefei) Co., Ltd., SARS-CoV-2 Antigen Rapid Test (Colloidal Gold Method)" 
* #2230 ^property[0].code = #Relationships 
* #2230 ^property[0].valueString = "$.t..ma" 
* #2241 "NESAPOR EUROPA SL, MARESKIT COVID-19 ANTIGEN RAPID TEST KIT"
* #2241 ^definition = NESAPOR EUROPA SL, MARESKIT COVID-19 ANTIGEN RAPID TEST KIT
* #2241 ^designation[0].language = #de-AT 
* #2241 ^designation[0].value = "NESAPOR EUROPA SL, MARESKIT COVID-19 ANTIGEN RAPID TEST KIT" 
* #2241 ^property[0].code = #Relationships 
* #2241 ^property[0].valueString = "$.t..ma" 
* #2242 "DNA Diagnostic, COVID-19 Antigen Detection Kit"
* #2242 ^definition = DNA Diagnostic, COVID-19 Antigen Detection Kit
* #2242 ^designation[0].language = #de-AT 
* #2242 ^designation[0].value = "DNA Diagnostic, COVID-19 Antigen Detection Kit" 
* #2242 ^property[0].code = #Relationships 
* #2242 ^property[0].valueString = "$.t..ma" 
* #2247 "BioGnost Ltd, CoviGnost AG Test Device 1x20"
* #2247 ^definition = BioGnost Ltd, CoviGnost AG Test Device 1x20
* #2247 ^designation[0].language = #de-AT 
* #2247 ^designation[0].value = "BioGnost Ltd, CoviGnost AG Test Device 1x20" 
* #2247 ^property[0].code = #Relationships 
* #2247 ^property[0].valueString = "$.t..ma" 
* #2257 "Hangzhou AllTest Biotech Co., Ltd, SARS-CoV-2 Antigen Rapid Test (Nasal Swab)"
* #2257 ^definition = Hangzhou AllTest Biotech Co., Ltd, SARS-CoV-2 Antigen Rapid Test (Nasal Swab)
* #2257 ^designation[0].language = #de-AT 
* #2257 ^designation[0].value = "Hangzhou AllTest Biotech Co., Ltd, SARS-CoV-2 Antigen Rapid Test (Nasal Swab)" 
* #2257 ^property[0].code = #Relationships 
* #2257 ^property[0].valueString = "$.t..ma" 
* #2260 "Multi-G bvba, Covid19Check-NAS"
* #2260 ^definition = Multi-G bvba, Covid19Check-NAS
* #2260 ^designation[0].language = #de-AT 
* #2260 ^designation[0].value = "Multi-G bvba, Covid19Check-NAS" 
* #2260 ^property[0].code = #Relationships 
* #2260 ^property[0].valueString = "$.t..ma" 
* #2271 "Pantest SA, Pantest Coronavirus Ag (Nasopharyngeal Swab)"
* #2271 ^definition = Pantest SA, Pantest Coronavirus Ag (Nasopharyngeal Swab)
* #2271 ^designation[0].language = #de-AT 
* #2271 ^designation[0].value = "Pantest SA, Pantest Coronavirus Ag (Nasopharyngeal Swab)" 
* #2271 ^property[0].code = #Relationships 
* #2271 ^property[0].valueString = "$.t..ma" 
* #2273 "Dräger Safety AG & Co. KGaA, Dräger Antigen Test SARS-CoV-2"
* #2273 ^definition = Dräger Safety AG & Co. KGaA, Dräger Antigen Test SARS-CoV-2
* #2273 ^designation[0].language = #de-AT 
* #2273 ^designation[0].value = "Dräger Safety AG & Co. KGaA, Dräger Antigen Test SARS-CoV-2" 
* #2273 ^property[0].code = #Relationships 
* #2273 ^property[0].valueString = "$.t..ma" 
* #2278 "Innovation Biotech(Beijing) Co.Ltd, Coronavirus (SARS-Cov-2) Antigen Rapid Test Cassette (Nasal swab)"
* #2278 ^definition = Innovation Biotech(Beijing) Co.Ltd, Coronavirus (SARS-Cov-2) Antigen Rapid Test Cassette (Nasal swab)
* #2278 ^designation[0].language = #de-AT 
* #2278 ^designation[0].value = "Innovation Biotech(Beijing) Co.Ltd, Coronavirus (SARS-Cov-2) Antigen Rapid Test Cassette (Nasal swab)" 
* #2278 ^property[0].code = #Relationships 
* #2278 ^property[0].valueString = "$.t..ma" 
* #2282 "Becton Dickinson, BD Kit for Rapid Detection of SARS-CoV-2"
* #2282 ^definition = Becton Dickinson, BD Kit for Rapid Detection of SARS-CoV-2
* #2282 ^designation[0].language = #de-AT 
* #2282 ^designation[0].value = "Becton Dickinson, BD Kit for Rapid Detection of SARS-CoV-2" 
* #2282 ^property[0].code = #Relationships 
* #2282 ^property[0].valueString = "$.t..ma" 
* #2290 "Rapid Pathogen Screening, Inc, LIAISON Quick Detect Covid Ag Assay"
* #2290 ^definition = Rapid Pathogen Screening, Inc, LIAISON Quick Detect Covid Ag Assay
* #2290 ^designation[0].language = #de-AT 
* #2290 ^designation[0].value = "Rapid Pathogen Screening, Inc, LIAISON Quick Detect Covid Ag Assay" 
* #2290 ^property[0].code = #Relationships 
* #2290 ^property[0].valueString = "$.t..ma" 
* #2297 "SureScreen Diagnostics, SARS-CoV-2 Rapid Antigen Test Cassette"
* #2297 ^definition = SureScreen Diagnostics, SARS-CoV-2 Rapid Antigen Test Cassette
* #2297 ^designation[0].language = #de-AT 
* #2297 ^designation[0].value = "SureScreen Diagnostics, SARS-CoV-2 Rapid Antigen Test Cassette" 
* #2297 ^property[0].code = #Relationships 
* #2297 ^property[0].valueString = "$.t..ma" 
* #2301 "Nanjing Liming Bio-Products Co., Ltd., StrongStep SARS-CoV-2 Antigen Rapid Test"
* #2301 ^definition = Nanjing Liming Bio-Products Co., Ltd., StrongStep SARS-CoV-2 Antigen Rapid Test
* #2301 ^designation[0].language = #de-AT 
* #2301 ^designation[0].value = "Nanjing Liming Bio-Products Co., Ltd., StrongStep SARS-CoV-2 Antigen Rapid Test" 
* #2301 ^property[0].code = #Relationships 
* #2301 ^property[0].valueString = "$.t..ma" 
* #2302 "Hangzhou AllTest Biotech Co., Ltd, COVID-19 Antigen Test Cassette (Nasopharyngeal Swab)(FIA)"
* #2302 ^definition = Hangzhou AllTest Biotech Co., Ltd, COVID-19 Antigen Test Cassette (Nasopharyngeal Swab)(FIA)
* #2302 ^designation[0].language = #de-AT 
* #2302 ^designation[0].value = "Hangzhou AllTest Biotech Co., Ltd, COVID-19 Antigen Test Cassette (Nasopharyngeal Swab)(FIA)" 
* #2302 ^property[0].code = #Relationships 
* #2302 ^property[0].valueString = "$.t..ma" 
* #2319 "Hangzhou AllTest Biotech Co., Ltd, CVAG4080A ? GSD NovaGen SARS-CoV-2 Ag Rapid Test (NP Swab)"
* #2319 ^definition = Hangzhou AllTest Biotech Co., Ltd, CVAG4080A ? GSD NovaGen SARS-CoV-2 Ag Rapid Test (NP Swab)
* #2319 ^designation[0].language = #de-AT 
* #2319 ^designation[0].value = "Hangzhou AllTest Biotech Co., Ltd, CVAG4080A ? GSD NovaGen SARS-CoV-2 Ag Rapid Test (NP Swab)" 
* #2319 ^property[0].code = #Relationships 
* #2319 ^property[0].valueString = "$.t..ma" 
* #2325 "Hangzhou AllTest Biotech Co., Ltd, GSD NovaGen SARS-CoV-2 Ag Rapid Test (Nasal Swab)"
* #2325 ^definition = Hangzhou AllTest Biotech Co., Ltd, GSD NovaGen SARS-CoV-2 Ag Rapid Test (Nasal Swab)
* #2325 ^designation[0].language = #de-AT 
* #2325 ^designation[0].value = "Hangzhou AllTest Biotech Co., Ltd, GSD NovaGen SARS-CoV-2 Ag Rapid Test (Nasal Swab)" 
* #2325 ^property[0].code = #Relationships 
* #2325 ^property[0].valueString = "$.t..ma" 
* #2350 "Assure Tech. (Hangzhou) Co., Ltd., ECOTEST COVID-19 Antigen Rapid Test Device"
* #2350 ^definition = Assure Tech. (Hangzhou) Co., Ltd., ECOTEST COVID-19 Antigen Rapid Test Device
* #2350 ^designation[0].language = #de-AT 
* #2350 ^designation[0].value = "Assure Tech. (Hangzhou) Co., Ltd., ECOTEST COVID-19 Antigen Rapid Test Device" 
* #2350 ^property[0].code = #Relationships 
* #2350 ^property[0].valueString = "$.t..ma" 
* #2374 "ABIOTEQ, Cora Gentest-19"
* #2374 ^definition = ABIOTEQ, Cora Gentest-19
* #2374 ^designation[0].language = #de-AT 
* #2374 ^designation[0].value = "ABIOTEQ, Cora Gentest-19" 
* #2374 ^property[0].code = #Relationships 
* #2374 ^property[0].valueString = "$.t..ma" 
* #2380 "BioSpeedia International, COVID19Speed-Antigen Test BSD_0503"
* #2380 ^definition = BioSpeedia International, COVID19Speed-Antigen Test BSD_0503
* #2380 ^designation[0].language = #de-AT 
* #2380 ^designation[0].value = "BioSpeedia International, COVID19Speed-Antigen Test BSD_0503 " 
* #2380 ^property[0].code = #Relationships 
* #2380 ^property[0].valueString = "$.t..ma" 
* #2414 "Shenzhen Huian Biosci Technology Co., Ltd., SARS-CoV-2 Antigen Test Kit (Colloidal Gold)"
* #2414 ^definition = Shenzhen Huian Biosci Technology Co., Ltd., SARS-CoV-2 Antigen Test Kit (Colloidal Gold)
* #2414 ^designation[0].language = #de-AT 
* #2414 ^designation[0].value = "Shenzhen Huian Biosci Technology Co., Ltd., SARS-CoV-2 Antigen Test Kit (Colloidal Gold)" 
* #2414 ^property[0].code = #Relationships 
* #2414 ^property[0].valueString = "$.t..ma" 
* #2415 "Shenzhen Dymind Biotechnology Co., Ltd, SARS-CoV-2 Antigen Test Kit (Colloidal Gold)"
* #2415 ^definition = Shenzhen Dymind Biotechnology Co., Ltd, SARS-CoV-2 Antigen Test Kit (Colloidal Gold)
* #2415 ^designation[0].language = #de-AT 
* #2415 ^designation[0].value = "Shenzhen Dymind Biotechnology Co., Ltd, SARS-CoV-2 Antigen Test Kit (Colloidal Gold)" 
* #2415 ^property[0].code = #Relationships 
* #2415 ^property[0].valueString = "$.t..ma" 
* #2419 "InTec PRODUCTS, INC., Rapid SARS-CoV-2 Antigen Test (nasopharyngeal specimen)"
* #2419 ^definition = InTec PRODUCTS, INC., Rapid SARS-CoV-2 Antigen Test (nasopharyngeal specimen)
* #2419 ^designation[0].language = #de-AT 
* #2419 ^designation[0].value = "InTec PRODUCTS, INC., Rapid SARS-CoV-2 Antigen Test (nasopharyngeal specimen)" 
* #2419 ^property[0].code = #Relationships 
* #2419 ^property[0].valueString = "$.t..ma" 
* #2449 "Citest Diagnostics Inc., COVID-19 Antigen Rapid Test (Nasal Swab)"
* #2449 ^definition = Citest Diagnostics Inc., COVID-19 Antigen Rapid Test (Nasal Swab)
* #2449 ^designation[0].language = #de-AT 
* #2449 ^designation[0].value = "Citest Diagnostics Inc., COVID-19 Antigen Rapid Test (Nasal Swab)" 
* #2449 ^property[0].code = #Relationships 
* #2449 ^property[0].valueString = "$.t..ma" 
* #2494 "Beijing O&D Biotech Co., Ltd., COVID-19 Antigen Rapid Test"
* #2494 ^definition = Beijing O&D Biotech Co., Ltd., COVID-19 Antigen Rapid Test
* #2494 ^designation[0].language = #de-AT 
* #2494 ^designation[0].value = "Beijing O&D Biotech Co., Ltd., COVID-19 Antigen Rapid Test" 
* #2494 ^property[0].code = #Relationships 
* #2494 ^property[0].valueString = "$.t..ma" 
* #2506 "Nanjing Norman Biological Technology Co., Ltd., Novel Coronavirus (2019-nCoV) Antigen Testing Kit (Colloidal Gold)"
* #2506 ^definition = Nanjing Norman Biological Technology Co., Ltd., Novel Coronavirus (2019-nCoV) Antigen Testing Kit (Colloidal Gold)
* #2506 ^designation[0].language = #de-AT 
* #2506 ^designation[0].value = "Nanjing Norman Biological Technology Co., Ltd., Novel Coronavirus (2019-nCoV) Antigen Testing Kit (Colloidal Gold)" 
* #2506 ^property[0].code = #Relationships 
* #2506 ^property[0].valueString = "$.t..ma" 
* #2519 "BIOLAN HEALTH, S.L, COVID-19 Antigen Rapid Test (Colloidal Gold Method)"
* #2519 ^definition = BIOLAN HEALTH, S.L, COVID-19 Antigen Rapid Test (Colloidal Gold Method)
* #2519 ^designation[0].language = #de-AT 
* #2519 ^designation[0].value = "BIOLAN HEALTH, S.L, COVID-19 Antigen Rapid Test (Colloidal Gold Method)" 
* #2519 ^property[0].code = #Relationships 
* #2519 ^property[0].valueString = "$.t..ma" 
* #2533 "Dynamiker Biotechnolgy(Tianjin) Co., Ltd., Dynamiker SARS-CoV-2 Ag Rapid Test"
* #2533 ^definition = Dynamiker Biotechnolgy(Tianjin) Co., Ltd., Dynamiker SARS-CoV-2 Ag Rapid Test
* #2533 ^designation[0].language = #de-AT 
* #2533 ^designation[0].value = "Dynamiker Biotechnolgy(Tianjin) Co., Ltd., Dynamiker SARS-CoV-2 Ag Rapid Test" 
* #2533 ^property[0].code = #Relationships 
* #2533 ^property[0].valueString = "$.t..ma" 
* #2555 "IEDAU INTERNATIONAL GMBH, Covid-19 Antigen Schnelltest (Colloidales Gold)"
* #2555 ^definition = IEDAU INTERNATIONAL GMBH, Covid-19 Antigen Schnelltest (Colloidales Gold)
* #2555 ^designation[0].language = #de-AT 
* #2555 ^designation[0].value = "IEDAU INTERNATIONAL GMBH, Covid-19 Antigen Schnelltest (Colloidales Gold)" 
* #2555 ^property[0].code = #Relationships 
* #2555 ^property[0].valueString = "$.t..ma" 
* #2579 "AccuBioTech Co.,Ltd, Accu-Tell SARS-CoV-2 Ag Cassette"
* #2579 ^definition = AccuBioTech Co.,Ltd, Accu-Tell SARS-CoV-2 Ag Cassette
* #2579 ^designation[0].language = #de-AT 
* #2579 ^designation[0].value = "AccuBioTech Co.,Ltd, Accu-Tell SARS-CoV-2 Ag Cassette" 
* #2579 ^property[0].code = #Relationships 
* #2579 ^property[0].valueString = "$.t..ma" 
* #2584 "TÜRKLAB TIBBI MALZEMELER SAN. ve TIC. A.S., INFO Covid-19 Ag Test"
* #2584 ^definition = TÜRKLAB TIBBI MALZEMELER SAN. ve TIC. A.S., INFO Covid-19 Ag Test
* #2584 ^designation[0].language = #de-AT 
* #2584 ^designation[0].value = "TÜRKLAB TIBBI MALZEMELER SAN. ve TIC. A.S., INFO Covid-19 Ag Test" 
* #2584 ^property[0].code = #Relationships 
* #2584 ^property[0].valueString = "$.t..ma" 
* #2586 "Jiangsu Mole Bioscience CO., LTD., SARS-CoV-2 Antigen Test Cassette"
* #2586 ^definition = Jiangsu Mole Bioscience CO., LTD., SARS-CoV-2 Antigen Test Cassette
* #2586 ^designation[0].language = #de-AT 
* #2586 ^designation[0].value = "Jiangsu Mole Bioscience CO., LTD., SARS-CoV-2 Antigen Test Cassette" 
* #2586 ^property[0].code = #Relationships 
* #2586 ^property[0].valueString = "$.t..ma" 
* #2588 "Changzhou Biowin Pharmaceutical Co.,Ltd., Novel Coronavirus(COVID-19) Antigen Test Kit (Colloidal Gold)"
* #2588 ^definition = Changzhou Biowin Pharmaceutical Co.,Ltd., Novel Coronavirus(COVID-19) Antigen Test Kit (Colloidal Gold)
* #2588 ^designation[0].language = #de-AT 
* #2588 ^designation[0].value = "Changzhou Biowin Pharmaceutical Co.,Ltd., Novel Coronavirus(COVID-19) Antigen Test Kit (Colloidal Gold)" 
* #2588 ^property[0].code = #Relationships 
* #2588 ^property[0].valueString = "$.t..ma" 
* #2608 "Neo-nostics (Suzhou) Bioengineering Co., Ltd., COVID 19 Antigen Test Kit (Colloidal Gold Method)"
* #2608 ^definition = Neo-nostics (Suzhou) Bioengineering Co., Ltd., COVID 19 Antigen Test Kit (Colloidal Gold Method)
* #2608 ^designation[0].language = #de-AT 
* #2608 ^designation[0].value = "Neo-nostics (Suzhou) Bioengineering Co., Ltd., COVID 19 Antigen Test Kit (Colloidal Gold Method)" 
* #2608 ^property[0].code = #Relationships 
* #2608 ^property[0].valueString = "$.t..ma" 
* #2629 "Hangzhou DIAN Biotechnology Co., Ltd., COVID-19 Antigen Test Cassette"
* #2629 ^definition = Hangzhou DIAN Biotechnology Co., Ltd., COVID-19 Antigen Test Cassette
* #2629 ^designation[0].language = #de-AT 
* #2629 ^designation[0].value = "Hangzhou DIAN Biotechnology Co., Ltd., COVID-19 Antigen Test Cassette" 
* #2629 ^property[0].code = #Relationships 
* #2629 ^property[0].valueString = "$.t..ma" 
* #2640 "Mologic Ltd, COVIOS Ag COVID-19 Antigen Rapid Diagnostic Test"
* #2640 ^definition = Mologic Ltd, COVIOS Ag COVID-19 Antigen Rapid Diagnostic Test
* #2640 ^designation[0].language = #de-AT 
* #2640 ^designation[0].value = "Mologic Ltd, COVIOS Ag COVID-19 Antigen Rapid Diagnostic Test" 
* #2640 ^property[0].code = #Relationships 
* #2640 ^property[0].valueString = "$.t..ma" 
* #2642 "Genobio Pharmaceutical Co., Ltd., Virusee SARS-CoV-2 Antigen Rapid Test (Colloidal Gold)"
* #2642 ^definition = Genobio Pharmaceutical Co., Ltd., Virusee SARS-CoV-2 Antigen Rapid Test (Colloidal Gold)
* #2642 ^designation[0].language = #de-AT 
* #2642 ^designation[0].value = "Genobio Pharmaceutical Co., Ltd., Virusee SARS-CoV-2 Antigen Rapid Test (Colloidal Gold)" 
* #2642 ^property[0].code = #Relationships 
* #2642 ^property[0].valueString = "$.t..ma" 
* #2672 "Pierenkemper GmbH, (SARS-CoV-2) Antigen Rapid Test COVIDENT (SWAB) COVID-19"
* #2672 ^definition = Pierenkemper GmbH, (SARS-CoV-2) Antigen Rapid Test COVIDENT (SWAB) COVID-19
* #2672 ^designation[0].language = #de-AT 
* #2672 ^designation[0].value = "Pierenkemper GmbH, (SARS-CoV-2) Antigen Rapid Test COVIDENT (SWAB) COVID-19" 
* #2672 ^property[0].code = #Relationships 
* #2672 ^property[0].valueString = "$.t..ma" 
* #2678 "NDFOS Co., Ltd., ND COVID-19 Ag Test"
* #2678 ^definition = NDFOS Co., Ltd., ND COVID-19 Ag Test
* #2678 ^designation[0].language = #de-AT 
* #2678 ^designation[0].value = "NDFOS Co., Ltd., ND COVID-19 Ag Test" 
* #2678 ^property[0].code = #Relationships 
* #2678 ^property[0].valueString = "$.t..ma" 
* #2684 "Zhejiang GENE SCIENCE Co., Ltd, Novel Coronavirus (COVID-19) Antigen Detection Kit (Swab)"
* #2684 ^definition = Zhejiang GENE SCIENCE Co., Ltd, Novel Coronavirus (COVID-19) Antigen Detection Kit (Swab)
* #2684 ^designation[0].language = #de-AT 
* #2684 ^designation[0].value = "Zhejiang GENE SCIENCE Co., Ltd, Novel Coronavirus (COVID-19) Antigen Detection Kit (Swab)" 
* #2684 ^property[0].code = #Relationships 
* #2684 ^property[0].valueString = "$.t..ma" 
* #2685 "PRIMA Lab SA, COVID-19 Antigen Rapid Test"
* #2685 ^definition = PRIMA Lab SA, COVID-19 Antigen Rapid Test
* #2685 ^designation[0].language = #de-AT 
* #2685 ^designation[0].value = "PRIMA Lab SA, COVID-19 Antigen Rapid Test" 
* #2685 ^property[0].code = #Relationships 
* #2685 ^property[0].valueString = "$.t..ma" 
* #2687 "Zephyr Biomedicals - Tulip Diagnostics, PerkinElmer COVID 19 Antigen Test (NS, NP)"
* #2687 ^definition = Zephyr Biomedicals - Tulip Diagnostics, PerkinElmer COVID 19 Antigen Test (NS, NP)
* #2687 ^designation[0].language = #de-AT 
* #2687 ^designation[0].value = "Zephyr Biomedicals - Tulip Diagnostics, PerkinElmer COVID 19 Antigen Test (NS, NP)" 
* #2687 ^property[0].code = #Relationships 
* #2687 ^property[0].valueString = "$.t..ma" 
* #2691 "CALTH Inc., AllCheck COVID19 Ag Nasal"
* #2691 ^definition = CALTH Inc., AllCheck COVID19 Ag Nasal
* #2691 ^designation[0].language = #de-AT 
* #2691 ^designation[0].value = "CALTH Inc., AllCheck COVID19 Ag Nasal" 
* #2691 ^property[0].code = #Relationships 
* #2691 ^property[0].valueString = "$.t..ma" 
* #2695 "Glallergen CO., LTD., Novel Coronavirus (2019-nCoV) Antigen Test Kit (Colloidal gold immunochromatography)"
* #2695 ^definition = Glallergen CO., LTD., Novel Coronavirus (2019-nCoV) Antigen Test Kit (Colloidal gold immunochromatography)
* #2695 ^designation[0].language = #de-AT 
* #2695 ^designation[0].value = "Glallergen CO., LTD., Novel Coronavirus (2019-nCoV) Antigen Test Kit (Colloidal gold immunochromatography)" 
* #2695 ^property[0].code = #Relationships 
* #2695 ^property[0].valueString = "$.t..ma" 
* #2696 "Cesna Biyoteknoloji Arast?rma Gelistirme Laboratuvar Sist.Ins.Müh.Dan.San.Tic.Ltd.Sti., CHECK UP SARS-COV-2 NASAL ANTIGEN RAPID TEST"
* #2696 ^definition = Cesna Biyoteknoloji Arast?rma Gelistirme Laboratuvar Sist.Ins.Müh.Dan.San.Tic.Ltd.Sti., CHECK UP SARS-COV-2 NASAL ANTIGEN RAPID TEST
* #2696 ^designation[0].language = #de-AT 
* #2696 ^designation[0].value = "Cesna Biyoteknoloji Arast?rma Gelistirme Laboratuvar Sist.Ins.Müh.Dan.San.Tic.Ltd.Sti., CHECK UP SARS-COV-2 NASAL ANTIGEN RAPID TEST" 
* #2696 ^property[0].code = #Relationships 
* #2696 ^property[0].valueString = "$.t..ma" 
* #2724 "Fosun Diagnostics (Shanghai) Co.,Ltd., China, Fosun Covid-19 Ag Card"
* #2724 ^definition = Fosun Diagnostics (Shanghai) Co.,Ltd., China, Fosun Covid-19 Ag Card
* #2724 ^designation[0].language = #de-AT 
* #2724 ^designation[0].value = "Fosun Diagnostics (Shanghai) Co.,Ltd., China, Fosun Covid-19 Ag Card" 
* #2724 ^property[0].code = #Relationships 
* #2724 ^property[0].valueString = "$.t..ma" 
* #2741 "OSANG Healthcare Co., Ltd., GeneFinder COVID-19 Ag Plus Rapid Test"
* #2741 ^definition = OSANG Healthcare Co., Ltd., GeneFinder COVID-19 Ag Plus Rapid Test
* #2741 ^designation[0].language = #de-AT 
* #2741 ^designation[0].value = "OSANG Healthcare Co., Ltd., GeneFinder COVID-19 Ag Plus Rapid Test" 
* #2741 ^property[0].code = #Relationships 
* #2741 ^property[0].valueString = "$.t..ma" 
* #2742 "Wuhan HealthCare Biotechnology Co. Ltd., SARS-CoV-2 Antigen Test Kit (Colloidal Gold)"
* #2742 ^definition = Wuhan HealthCare Biotechnology Co. Ltd., SARS-CoV-2 Antigen Test Kit (Colloidal Gold)
* #2742 ^designation[0].language = #de-AT 
* #2742 ^designation[0].value = "Wuhan HealthCare Biotechnology Co. Ltd., SARS-CoV-2 Antigen Test Kit (Colloidal Gold)" 
* #2742 ^property[0].code = #Relationships 
* #2742 ^property[0].valueString = "$.t..ma" 
* #2746 "Cesna Biyoteknoloji Arast?rma Gelistirme Laboratuvar Sist.Ins.Müh.Dan.San.Tic.Ltd.Sti., CHECK UP SARS-COV-2 NASOPHARYNGEAL RAPID ANTIGEN TEST"
* #2746 ^definition = Cesna Biyoteknoloji Arast?rma Gelistirme Laboratuvar Sist.Ins.Müh.Dan.San.Tic.Ltd.Sti., CHECK UP SARS-COV-2 NASOPHARYNGEAL RAPID ANTIGEN TEST
* #2746 ^designation[0].language = #de-AT 
* #2746 ^designation[0].value = "Cesna Biyoteknoloji Arast?rma Gelistirme Laboratuvar Sist.Ins.Müh.Dan.San.Tic.Ltd.Sti., CHECK UP SARS-COV-2 NASOPHARYNGEAL RAPID ANTIGEN TEST" 
* #2746 ^property[0].code = #Relationships 
* #2746 ^property[0].valueString = "$.t..ma" 
* #2754 "Qingdao Hightop Biotech Co., Ltd., SARS-CoV-2/Flu A+B/RSV Antigen Rapid Test"
* #2754 ^definition = Qingdao Hightop Biotech Co., Ltd., SARS-CoV-2/Flu A+B/RSV Antigen Rapid Test
* #2754 ^designation[0].language = #de-AT 
* #2754 ^designation[0].value = "Qingdao Hightop Biotech Co., Ltd., SARS-CoV-2/Flu A+B/RSV Antigen Rapid Test" 
* #2754 ^property[0].code = #Relationships 
* #2754 ^property[0].valueString = "$.t..ma" 
* #2756 "DNA Diagnostic, SARS-CoV-2 Antigen Rapid Test"
* #2756 ^definition = DNA Diagnostic, SARS-CoV-2 Antigen Rapid Test
* #2756 ^designation[0].language = #de-AT 
* #2756 ^designation[0].value = "DNA Diagnostic, SARS-CoV-2 Antigen Rapid Test" 
* #2756 ^property[0].code = #Relationships 
* #2756 ^property[0].valueString = "$.t..ma" 
* #2763 "ScheBo Biotech, ScheBo SARS CoV-2 Quick ANTIGEN (Colloidal Gold Method)"
* #2763 ^definition = ScheBo Biotech, ScheBo SARS CoV-2 Quick ANTIGEN (Colloidal Gold Method)
* #2763 ^designation[0].language = #de-AT 
* #2763 ^designation[0].value = "ScheBo Biotech, ScheBo SARS CoV-2 Quick ANTIGEN (Colloidal Gold Method)" 
* #2763 ^property[0].code = #Relationships 
* #2763 ^property[0].valueString = "$.t..ma" 
* #2807 "Beijing Hotgen Biotech Co., Ltd, Coronavirus (2019-nCoV)-Antigentest"
* #2807 ^definition = Beijing Hotgen Biotech Co., Ltd, Coronavirus (2019-nCoV)-Antigentest
* #2807 ^designation[0].language = #de-AT 
* #2807 ^designation[0].value = "Beijing Hotgen Biotech Co., Ltd, Coronavirus (2019-nCoV)-Antigentest" 
* #2807 ^property[0].code = #Relationships 
* #2807 ^property[0].valueString = "$.t..ma" 
* #2812 "Shenzhen Huaree Technology Co.,Ltd, SARS-CoV-2 Antigen Rapid Test Kit (Immunochromatography)"
* #2812 ^definition = Shenzhen Huaree Technology Co.,Ltd, SARS-CoV-2 Antigen Rapid Test Kit (Immunochromatography)
* #2812 ^designation[0].language = #de-AT 
* #2812 ^designation[0].value = "Shenzhen Huaree Technology Co.,Ltd, SARS-CoV-2 Antigen Rapid Test Kit (Immunochromatography)" 
* #2812 ^property[0].code = #Relationships 
* #2812 ^property[0].valueString = "$.t..ma" 
* #2853 "GenSure Biotech Inc., GenSure COVID-19 Antigen Rapid Test Kit"
* #2853 ^definition = GenSure Biotech Inc., GenSure COVID-19 Antigen Rapid Test Kit
* #2853 ^designation[0].language = #de-AT 
* #2853 ^designation[0].value = "GenSure Biotech Inc., GenSure COVID-19 Antigen Rapid Test Kit" 
* #2853 ^property[0].code = #Relationships 
* #2853 ^property[0].valueString = "$.t..ma" 
* #2858 "Bioscience (Tianjin) Diagnostic Technology Co.,Ltd, Novel Coronavirus (2019-nCoV) Antigen Rapid Detection"
* #2858 ^definition = Bioscience (Tianjin) Diagnostic Technology Co.,Ltd, Novel Coronavirus (2019-nCoV) Antigen Rapid Detection
* #2858 ^designation[0].language = #de-AT 
* #2858 ^designation[0].value = "Bioscience (Tianjin) Diagnostic Technology Co.,Ltd, Novel Coronavirus (2019-nCoV) Antigen Rapid Detection" 
* #2858 ^property[0].code = #Relationships 
* #2858 ^property[0].valueString = "$.t..ma" 
* #2862 "Hangzhou Funworld Biotech Co., Ltd, SARS-CoV-2 Antigen Rapid Test Device"
* #2862 ^definition = Hangzhou Funworld Biotech Co., Ltd, SARS-CoV-2 Antigen Rapid Test Device
* #2862 ^designation[0].language = #de-AT 
* #2862 ^designation[0].value = "Hangzhou Funworld Biotech Co., Ltd, SARS-CoV-2 Antigen Rapid Test Device" 
* #2862 ^property[0].code = #Relationships 
* #2862 ^property[0].valueString = "$.t..ma" 
* #2866 "Lifecosm Biotech Limited, COVID-19 Antigen Test Cassette"
* #2866 ^definition = Lifecosm Biotech Limited, COVID-19 Antigen Test Cassette
* #2866 ^designation[0].language = #de-AT 
* #2866 ^designation[0].value = "Lifecosm Biotech Limited, COVID-19 Antigen Test Cassette" 
* #2866 ^property[0].code = #Relationships 
* #2866 ^property[0].valueString = "$.t..ma" 
* #2885 "Hangzhou GENESIS Biodetection and Biocontrol CO.,LTD, KaiBiLi COVID-19 Antigen Pro"
* #2885 ^definition = Hangzhou GENESIS Biodetection and Biocontrol CO.,LTD, KaiBiLi COVID-19 Antigen Pro
* #2885 ^designation[0].language = #de-AT 
* #2885 ^designation[0].value = "Hangzhou GENESIS Biodetection and Biocontrol CO.,LTD, KaiBiLi COVID-19 Antigen Pro" 
* #2885 ^property[0].code = #Relationships 
* #2885 ^property[0].valueString = "$.t..ma" 
* #2941 "Shenzhen Kingfocus Biomedical Engineering Co., Ltd., COVID-19 Antigen Detection Kit (Quantum Dots-Based Immunofluorescence Chromatography)"
* #2941 ^definition = Shenzhen Kingfocus Biomedical Engineering Co., Ltd., COVID-19 Antigen Detection Kit (Quantum Dots-Based Immunofluorescence Chromatography)
* #2941 ^designation[0].language = #de-AT 
* #2941 ^designation[0].value = "Shenzhen Kingfocus Biomedical Engineering Co., Ltd., COVID-19 Antigen Detection Kit (Quantum Dots-Based Immunofluorescence Chromatography)" 
* #2941 ^property[0].code = #Relationships 
* #2941 ^property[0].valueString = "$.t..ma" 
* #2942 "Hangzhou Zheda Dixun Biological Gene Engineering Co., Ltd., SARS-CoV-2 Nucleocapsid (N) Antigen Rapid Test Cassette (Swab)"
* #2942 ^definition = Hangzhou Zheda Dixun Biological Gene Engineering Co., Ltd., SARS-CoV-2 Nucleocapsid (N) Antigen Rapid Test Cassette (Swab)
* #2942 ^designation[0].language = #de-AT 
* #2942 ^designation[0].value = "Hangzhou Zheda Dixun Biological Gene Engineering Co., Ltd., SARS-CoV-2 Nucleocapsid (N) Antigen Rapid Test Cassette (Swab)" 
* #2942 ^property[0].code = #Relationships 
* #2942 ^property[0].valueString = "$.t..ma" 
* #2963 "Jiangxi Province JinHuan Medical Instrument Co., LTD., DREHA Novel Coronavirus (SARS-CoV-2) Antigen Rapid Detection Kit"
* #2963 ^definition = Jiangxi Province JinHuan Medical Instrument Co., LTD., DREHA Novel Coronavirus (SARS-CoV-2) Antigen Rapid Detection Kit
* #2963 ^designation[0].language = #de-AT 
* #2963 ^designation[0].value = "Jiangxi Province JinHuan Medical Instrument Co., LTD., DREHA Novel Coronavirus (SARS-CoV-2) Antigen Rapid Detection Kit" 
* #2963 ^property[0].code = #Relationships 
* #2963 ^property[0].valueString = "$.t..ma" 
* #2979 "Hangzhou Jucheng Medical Products Co., Ltd, SARS-CoV-2 Ag Rapid Test Kit"
* #2979 ^definition = Hangzhou Jucheng Medical Products Co., Ltd, SARS-CoV-2 Ag Rapid Test Kit
* #2979 ^designation[0].language = #de-AT 
* #2979 ^designation[0].value = "Hangzhou Jucheng Medical Products Co., Ltd, SARS-CoV-2 Ag Rapid Test Kit" 
* #2979 ^property[0].code = #Relationships 
* #2979 ^property[0].valueString = "$.t..ma" 
* #3015 "Suzhou Soochow University Saier Immuno Biotech Co., Ltd., InstantSure Covid-19 Ag CARD"
* #3015 ^definition = Suzhou Soochow University Saier Immuno Biotech Co., Ltd., InstantSure Covid-19 Ag CARD
* #3015 ^designation[0].language = #de-AT 
* #3015 ^designation[0].value = "Suzhou Soochow University Saier Immuno Biotech Co., Ltd., InstantSure Covid-19 Ag CARD" 
* #3015 ^property[0].code = #Relationships 
* #3015 ^property[0].valueString = "$.t..ma" 
* #3093 "TBG BIOTECHNOLOGY XIAMEN INC., SARS-CoV-2 Antigen Rapid Test (Nasal Swab)"
* #3093 ^definition = TBG BIOTECHNOLOGY XIAMEN INC., SARS-CoV-2 Antigen Rapid Test (Nasal Swab)
* #3093 ^designation[0].language = #de-AT 
* #3093 ^designation[0].value = "TBG BIOTECHNOLOGY XIAMEN INC., SARS-CoV-2 Antigen Rapid Test (Nasal Swab)" 
* #3093 ^property[0].code = #Relationships 
* #3093 ^property[0].valueString = "$.t..ma" 
* #3099 "BioDetect (Xiamen) Biotechnology Co., Ltd., RAPID SARS-COV-2 ANTIGEN TEST CARD"
* #3099 ^definition = BioDetect (Xiamen) Biotechnology Co., Ltd., RAPID SARS-COV-2 ANTIGEN TEST CARD
* #3099 ^designation[0].language = #de-AT 
* #3099 ^designation[0].value = "BioDetect (Xiamen) Biotechnology Co., Ltd., RAPID SARS-COV-2 ANTIGEN TEST CARD" 
* #3099 ^property[0].code = #Relationships 
* #3099 ^property[0].valueString = "$.t..ma" 
* #344 "SD BIOSENSOR Inc., STANDARD F COVID-19 Ag FIA"
* #344 ^definition = SD BIOSENSOR Inc., STANDARD F COVID-19 Ag FIA
* #344 ^designation[0].language = #de-AT 
* #344 ^designation[0].value = "SD BIOSENSOR Inc., STANDARD F COVID-19 Ag FIA" 
* #344 ^property[0].code = #Relationships 
* #344 ^property[0].valueString = "$.t..ma" 
* #345 "SD BIOSENSOR Inc., STANDARD Q COVID-19 Ag Test"
* #345 ^definition = SD BIOSENSOR Inc., STANDARD Q COVID-19 Ag Test
* #345 ^designation[0].language = #de-AT 
* #345 ^designation[0].value = "SD BIOSENSOR Inc., STANDARD Q COVID-19 Ag Test" 
* #345 ^property[0].code = #Relationships 
* #345 ^property[0].valueString = "$.t..ma" 
* #768 "ArcDia International Ltd, mariPOC SARS-CoV-2"
* #768 ^definition = ArcDia International Ltd, mariPOC SARS-CoV-2
* #768 ^designation[0].language = #de-AT 
* #768 ^designation[0].value = "ArcDia International Ltd, mariPOC SARS-CoV-2" 
* #768 ^property[0].code = #Relationships 
* #768 ^property[0].valueString = "$.t..ma" 
* #770 "Assure Tech. (Hangzhou) Co., Ltd., ECOTEST COVID-19 Antigen Rapid Test Device"
* #770 ^definition = Assure Tech. (Hangzhou) Co., Ltd., ECOTEST COVID-19 Antigen Rapid Test Device
* #770 ^designation[0].language = #de-AT 
* #770 ^designation[0].value = "Assure Tech. (Hangzhou) Co., Ltd., ECOTEST COVID-19 Antigen Rapid Test Device" 
* #770 ^property[0].code = #Relationships 
* #770 ^property[0].valueString = "$.t..ma" 
