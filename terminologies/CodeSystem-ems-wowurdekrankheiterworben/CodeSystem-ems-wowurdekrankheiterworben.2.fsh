Instance: ems-wowurdekrankheiterworben 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-wowurdekrankheiterworben" 
* name = "ems-wowurdekrankheiterworben" 
* title = "EMS_WoWurdeKrankheitErworben" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Liste wo Krankheit erworben wurde" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.77" 
* date = "2017-01-26" 
* count = 7 
* #AL "Ausland"
* #GE "Gemeinde, eindeutig"
* #GPFL "Gesundheitspflege"
* #GV "Gemeinde, vermutet"
* #IL "Inland"
* #KH "nosokomial (Krankenhaus)"
* #O "anderes"
