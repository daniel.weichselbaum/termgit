Instance: ems-wowurdekrankheiterworben 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-wowurdekrankheiterworben" 
* name = "ems-wowurdekrankheiterworben" 
* title = "EMS_WoWurdeKrankheitErworben" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Liste wo Krankheit erworben wurde" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.77" 
* date = "2017-01-26" 
* count = 7 
* concept[0].code = #AL
* concept[0].display = "Ausland"
* concept[1].code = #GE
* concept[1].display = "Gemeinde, eindeutig"
* concept[2].code = #GPFL
* concept[2].display = "Gesundheitspflege"
* concept[3].code = #GV
* concept[3].display = "Gemeinde, vermutet"
* concept[4].code = #IL
* concept[4].display = "Inland"
* concept[5].code = #KH
* concept[5].display = "nosokomial (Krankenhaus)"
* concept[6].code = #O
* concept[6].display = "anderes"
