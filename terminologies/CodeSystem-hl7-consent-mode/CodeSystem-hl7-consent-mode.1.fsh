Instance: hl7-consent-mode 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-consent-mode" 
* name = "hl7-consent-mode" 
* title = "HL7 Consent Mode" 
* status = #active 
* content = #complete 
* description = "**Description:** Consent Mode (Table 0497)

**Beschreibung:** Form der Einverständniserklärung (Tabelle 0497)

**Versions-Beschreibung:** Form der Einverständniserklärung" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.12.497" 
* date = "2015-06-01" 
* count = 3 
* property[0].code = #hints 
* property[0].type = #string 
* concept[0].code = #T
* concept[0].display = "Telephone"
* concept[0].designation[0].language = #de-AT 
* concept[0].designation[0].value = "Telefonisch" 
* concept[1].code = #V
* concept[1].display = "Verbal"
* concept[1].designation[0].language = #de-AT 
* concept[1].designation[0].value = "Mündlich" 
* concept[1].property[0].code = #hints 
* concept[1].property[0].valueString = "Gültiger Wert für ELGA-SOO" 
* concept[2].code = #W
* concept[2].display = "Written"
* concept[2].designation[0].language = #de-AT 
* concept[2].designation[0].value = "Schriftlich" 
* concept[2].property[0].code = #hints 
* concept[2].property[0].valueString = "Bevorzugter Wert für ELGA-SOO" 
