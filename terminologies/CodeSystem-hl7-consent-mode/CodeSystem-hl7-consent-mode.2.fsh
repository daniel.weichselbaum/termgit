Instance: hl7-consent-mode 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-consent-mode" 
* name = "hl7-consent-mode" 
* title = "HL7 Consent Mode" 
* status = #active 
* content = #complete 
* description = "**Description:** Consent Mode (Table 0497)

**Beschreibung:** Form der Einverständniserklärung (Tabelle 0497)

**Versions-Beschreibung:** Form der Einverständniserklärung" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.12.497" 
* date = "2015-06-01" 
* count = 3 
* property[0].code = #hints 
* property[0].type = #string 
* #T "Telephone"
* #T ^designation[0].language = #de-AT 
* #T ^designation[0].value = "Telefonisch" 
* #V "Verbal"
* #V ^designation[0].language = #de-AT 
* #V ^designation[0].value = "Mündlich" 
* #V ^property[0].code = #hints 
* #V ^property[0].valueString = "Gültiger Wert für ELGA-SOO" 
* #W "Written"
* #W ^designation[0].language = #de-AT 
* #W ^designation[0].value = "Schriftlich" 
* #W ^property[0].code = #hints 
* #W ^property[0].valueString = "Bevorzugter Wert für ELGA-SOO" 
