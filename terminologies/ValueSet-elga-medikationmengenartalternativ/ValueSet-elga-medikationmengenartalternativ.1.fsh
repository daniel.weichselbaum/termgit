Instance: elga-medikationmengenartalternativ 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-medikationmengenartalternativ" 
* name = "elga-medikationmengenartalternativ" 
* title = "ELGA_MedikationMengenartAlternativ" 
* status = #active 
* version = "202009" 
* description = "**Beschreibung:** ELGA ValueSet für alternative Mengenarten (zählbar)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.67" 
* date = "2020-09-18" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-elga-medikationmengenartalternativ"
* compose.include[0].concept[0].code = #{HUB}
* compose.include[0].concept[0].display = "Hub"
* compose.include[0].concept[1].code = #{TAB}
* compose.include[0].concept[1].display = "Tablette"

* expansion.timestamp = "2022-09-13T14:18:13.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem-elga-medikationmengenartalternativ"
* expansion.contains[0].code = #{HUB}
* expansion.contains[0].display = "Hub"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem-elga-medikationmengenartalternativ"
* expansion.contains[1].code = #{TAB}
* expansion.contains[1].display = "Tablette"
