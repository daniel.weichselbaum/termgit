Instance: zeiteinheiten 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-zeiteinheiten" 
* name = "zeiteinheiten" 
* title = "Zeiteinheiten" 
* status = #active 
* version = "4.0" 
* description = "**Beschreibung:** Zeiteinheiten von UCUM" 
* date = "2018-06-21" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-ucum"
* compose.include[0].concept[0].code = #a
* compose.include[0].concept[0].display = "Year"
* compose.include[0].concept[1].code = #d
* compose.include[0].concept[1].display = "Day"
* compose.include[0].concept[2].code = #h
* compose.include[0].concept[2].display = "Hour"
* compose.include[0].concept[3].code = #ks
* compose.include[0].concept[3].display = "KiloSecond"
* compose.include[0].concept[4].code = #min
* compose.include[0].concept[4].display = "Minute"
* compose.include[0].concept[5].code = #mo
* compose.include[0].concept[5].display = "Month"
* compose.include[0].concept[6].code = #ms
* compose.include[0].concept[6].display = "MilliSecond"
* compose.include[0].concept[7].code = #ns
* compose.include[0].concept[7].display = "NanoSecond"
* compose.include[0].concept[8].code = #ps
* compose.include[0].concept[8].display = "PicoSecond"
* compose.include[0].concept[9].code = #s
* compose.include[0].concept[9].display = "Second"
* compose.include[0].concept[10].code = #us
* compose.include[0].concept[10].display = "MicroSecond"
* compose.include[0].concept[11].code = #wk
* compose.include[0].concept[11].display = "Week"

* expansion.timestamp = "2022-09-13T14:17:41.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem-ucum"
* expansion.contains[0].code = #a
* expansion.contains[0].display = "Year"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem-ucum"
* expansion.contains[1].code = #d
* expansion.contains[1].display = "Day"
* expansion.contains[2].system = "https://termgit.elga.gv.at/CodeSystem-ucum"
* expansion.contains[2].code = #h
* expansion.contains[2].display = "Hour"
* expansion.contains[3].system = "https://termgit.elga.gv.at/CodeSystem-ucum"
* expansion.contains[3].code = #ks
* expansion.contains[3].display = "KiloSecond"
* expansion.contains[4].system = "https://termgit.elga.gv.at/CodeSystem-ucum"
* expansion.contains[4].code = #min
* expansion.contains[4].display = "Minute"
* expansion.contains[5].system = "https://termgit.elga.gv.at/CodeSystem-ucum"
* expansion.contains[5].code = #mo
* expansion.contains[5].display = "Month"
* expansion.contains[6].system = "https://termgit.elga.gv.at/CodeSystem-ucum"
* expansion.contains[6].code = #ms
* expansion.contains[6].display = "MilliSecond"
* expansion.contains[7].system = "https://termgit.elga.gv.at/CodeSystem-ucum"
* expansion.contains[7].code = #ns
* expansion.contains[7].display = "NanoSecond"
* expansion.contains[8].system = "https://termgit.elga.gv.at/CodeSystem-ucum"
* expansion.contains[8].code = #ps
* expansion.contains[8].display = "PicoSecond"
* expansion.contains[9].system = "https://termgit.elga.gv.at/CodeSystem-ucum"
* expansion.contains[9].code = #s
* expansion.contains[9].display = "Second"
* expansion.contains[10].system = "https://termgit.elga.gv.at/CodeSystem-ucum"
* expansion.contains[10].code = #us
* expansion.contains[10].display = "MicroSecond"
* expansion.contains[11].system = "https://termgit.elga.gv.at/CodeSystem-ucum"
* expansion.contains[11].code = #wk
* expansion.contains[11].display = "Week"
