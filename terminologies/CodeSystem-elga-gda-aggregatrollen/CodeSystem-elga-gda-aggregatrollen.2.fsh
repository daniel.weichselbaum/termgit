Instance: elga-gda-aggregatrollen 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-gda-aggregatrollen" 
* name = "elga-gda-aggregatrollen" 
* title = "ELGA_GDA_Aggregatrollen" 
* status = #active 
* content = #complete 
* version = "202209" 
* description = "**Beschreibung:** ELGA Codeliste für GDA Aggregatrollen (höchste Hierarchieebene)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.3" 
* date = "2022-09-06" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* count = 18 
* #700 "Ärztin/Arzt"
* #700 ^designation[0].language = #de-AT 
* #700 ^designation[0].value = "Ärztin/Arzt" 
* #701 "Zahnärztin/Zahnarzt"
* #701 ^designation[0].language = #de-AT 
* #701 ^designation[0].value = "Zahnärztin/Zahnarzt" 
* #702 "Krankenanstalt"
* #702 ^designation[0].language = #de-AT 
* #702 ^designation[0].value = "Krankenanstalt" 
* #703 "Einrichtung der Pflege"
* #703 ^designation[0].language = #de-AT 
* #703 ^designation[0].value = "Einrichtung der Pflege" 
* #704 "Apotheke"
* #704 ^designation[0].language = #de-AT 
* #704 ^designation[0].value = "Apotheke" 
* #705 "ELGA-Beratung"
* #705 ^designation[0].language = #de-AT 
* #705 ^designation[0].value = "ELGA-Beratung" 
* #706 "ELGA-Ombudsstelle"
* #706 ^designation[0].language = #de-AT 
* #706 ^designation[0].value = "ELGA-Ombudsstelle" 
* #716 "Amtsärztin/Amtsarzt"
* #716 ^designation[0].language = #de-AT 
* #716 ^designation[0].value = "Amtsärztin/Amtsarzt" 
* #717 "Korrekturberechtigte Person"
* #717 ^designation[0].language = #de-AT 
* #717 ^designation[0].value = "Korrekturberechtigte Person" 
* #718 "Krisenmanagerin/Krisenmanager"
* #718 ^designation[0].language = #de-AT 
* #718 ^designation[0].value = "Krisenmanagerin/Krisenmanager" 
* #719 "Auswertungsberechtigte Person"
* #719 ^designation[0].language = #de-AT 
* #719 ^designation[0].value = "Auswertungsberechtigte Person" 
* #720 "Verrechnungsberechtigte Person"
* #720 ^designation[0].language = #de-AT 
* #720 ^designation[0].value = "Verrechnungsberechtigte Person" 
* #721 "Arbeitsmedizin"
* #721 ^designation[0].language = #de-AT 
* #721 ^designation[0].value = "Arbeitsmedizin" 
* #722 "Hebamme"
* #722 ^designation[0].language = #de-AT 
* #722 ^designation[0].value = "Hebamme" 
* #723 "Straf- und Maßnahmenvollzug"
* #723 ^designation[0].language = #de-AT 
* #723 ^designation[0].value = "Straf- und Maßnahmenvollzug" 
* #724 "Labor und Pathologie"
* #724 ^designation[0].language = #de-AT 
* #724 ^designation[0].value = "Labor und Pathologie" 
* #725 "EMS/EPI-Service"
* #725 ^designation[0].language = #de-AT 
* #725 ^designation[0].value = "EMS/EPI-Service" 
* #726 "Diplomierte Gesundheits- und Krankenpflegerin/Diplomierter Gesundheits- und Krankenpfleger"
* #726 ^designation[0].language = #de-AT 
* #726 ^designation[0].value = "Diplomierte Gesundheits- und Krankenpflegerin/Diplomierter Gesundheits- und Krankenpfleger" 
