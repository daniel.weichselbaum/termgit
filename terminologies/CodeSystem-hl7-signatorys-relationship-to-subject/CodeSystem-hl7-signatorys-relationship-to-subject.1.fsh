Instance: hl7-signatorys-relationship-to-subject 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-signatorys-relationship-to-subject" 
* name = "hl7-signatorys-relationship-to-subject" 
* title = "HL7 Signatory's Relationship to Subject " 
* status = #active 
* content = #complete 
* description = "**Description:** Signatory's Relationship to Subject (Table 0548)

**Beschreibung:** Beziehung des Unterschreibenden zum Patienten (Tabelle 0548)

**Versions-Beschreibung:** Beziehung des Unterschreibenden zum Patienten" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.12.548" 
* date = "2015-09-07" 
* count = 7 
* concept[0].code = #1
* concept[0].display = "Self"
* concept[0].designation[0].language = #de-AT 
* concept[0].designation[0].value = "Die Person selbst" 
* concept[1].code = #2
* concept[1].display = "Parent"
* concept[1].designation[0].language = #de-AT 
* concept[1].designation[0].value = "Nächste Angehörige" 
* concept[2].code = #3
* concept[2].display = "Next of Kin"
* concept[2].designation[0].language = #de-AT 
* concept[2].designation[0].value = "Nächste Angehörige" 
* concept[3].code = #4
* concept[3].display = "Durable Power of Attorney in Healthcare Affairs"
* concept[3].designation[0].language = #de-AT 
* concept[3].designation[0].value = "Vertreter mit dauerhafter Vollmacht für Gesundheits-Angelegenheiten" 
* concept[4].code = #5
* concept[4].display = "Conservator"
* concept[4].designation[0].language = #de-AT 
* concept[4].designation[0].value = "Vormund" 
* concept[5].code = #6
* concept[5].display = "Emergent Practitioner (practitioner judging case as emergency requiring care without a consent)"
* concept[5].designation[0].language = #de-AT 
* concept[5].designation[0].value = "Arzt, der in dringendem Fall ohne vorliegende Zustimmung entscheidet" 
* concept[6].code = #7
* concept[6].display = "Non-Emergent Practitioner (i.e. medical ethics committee)"
* concept[6].designation[0].language = #de-AT 
* concept[6].designation[0].value = "Arzt (ohne Dringlichkeit, zB Ethikkommission) " 
