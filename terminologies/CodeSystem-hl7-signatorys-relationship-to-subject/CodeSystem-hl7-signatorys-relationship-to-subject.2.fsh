Instance: hl7-signatorys-relationship-to-subject 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-signatorys-relationship-to-subject" 
* name = "hl7-signatorys-relationship-to-subject" 
* title = "HL7 Signatory's Relationship to Subject " 
* status = #active 
* content = #complete 
* description = "**Description:** Signatory's Relationship to Subject (Table 0548)

**Beschreibung:** Beziehung des Unterschreibenden zum Patienten (Tabelle 0548)

**Versions-Beschreibung:** Beziehung des Unterschreibenden zum Patienten" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.12.548" 
* date = "2015-09-07" 
* count = 7 
* #1 "Self"
* #1 ^designation[0].language = #de-AT 
* #1 ^designation[0].value = "Die Person selbst" 
* #2 "Parent"
* #2 ^designation[0].language = #de-AT 
* #2 ^designation[0].value = "Nächste Angehörige" 
* #3 "Next of Kin"
* #3 ^designation[0].language = #de-AT 
* #3 ^designation[0].value = "Nächste Angehörige" 
* #4 "Durable Power of Attorney in Healthcare Affairs"
* #4 ^designation[0].language = #de-AT 
* #4 ^designation[0].value = "Vertreter mit dauerhafter Vollmacht für Gesundheits-Angelegenheiten" 
* #5 "Conservator"
* #5 ^designation[0].language = #de-AT 
* #5 ^designation[0].value = "Vormund" 
* #6 "Emergent Practitioner (practitioner judging case as emergency requiring care without a consent)"
* #6 ^designation[0].language = #de-AT 
* #6 ^designation[0].value = "Arzt, der in dringendem Fall ohne vorliegende Zustimmung entscheidet" 
* #7 "Non-Emergent Practitioner (i.e. medical ethics committee)"
* #7 ^designation[0].language = #de-AT 
* #7 ^designation[0].value = "Arzt (ohne Dringlichkeit, zB Ethikkommission) " 
