Instance: elga-auditparticipantobjectidtype 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-auditparticipantobjectidtype" 
* name = "elga-auditparticipantobjectidtype" 
* title = "ELGA_AuditParticipantObjectIdType" 
* status = #active 
* content = #complete 
* description = "**Beschreibung:** Liste der ELGA spezifischen Audit Participant Object ID Type Codes. Der Object ID Type Code beschreibt die Art eines Objekts, welches referenziert wird. " 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.155" 
* date = "2015-06-01" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* count = 6 
* #0 "Transaktions ID"
* #0 ^definition = Transaktions ID (Transaktionsklammer)
* #0 ^designation[0].language = #de-AT 
* #0 ^designation[0].value = "Transaktions ID" 
* #100 "Policy ID"
* #100 ^designation[0].language = #de-AT 
* #100 ^designation[0].value = "Policy ID" 
* #110 "KBS ID"
* #110 ^designation[0].language = #de-AT 
* #110 ^designation[0].value = "KBS ID" 
* #120 "Assertion ID"
* #120 ^designation[0].language = #de-AT 
* #120 ^designation[0].value = "Assertion ID" 
* #130 "Vollmacht Referenz"
* #130 ^designation[0].language = #de-AT 
* #130 ^designation[0].value = "Vollmacht Referenz" 
* #140 "GDA ID"
* #140 ^designation[0].language = #de-AT 
* #140 ^designation[0].value = "GDA ID" 
