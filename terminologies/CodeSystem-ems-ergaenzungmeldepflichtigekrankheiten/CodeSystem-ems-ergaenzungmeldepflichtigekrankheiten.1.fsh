Instance: ems-ergaenzungmeldepflichtigekrankheiten 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-ergaenzungmeldepflichtigekrankheiten" 
* name = "ems-ergaenzungmeldepflichtigekrankheiten" 
* title = "EMS_ErgaenzungMeldepflichtigeKrankheiten" 
* status = #active 
* content = #complete 
* version = "202002" 
* description = "**Beschreibung:** Ergänzungsliste der meldepflichtigen Krankheiten für das österreichische EMS" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.187" 
* date = "2020-01-01" 
* count = 5 
* concept[0].code = #A81.9a
* concept[0].display = "sonstige transmissible spongiforme Enzephalopathie"
* concept[1].code = #J09a
* concept[1].display = "A/H1N1-Virus (Neue Influenza A)"
* concept[2].code = #J09b
* concept[2].display = "A/H5N1-Virus (Vogelgrippe)"
* concept[3].code = #J09c
* concept[3].display = "A/H7N9-Virus"
* concept[4].code = #J09d
* concept[4].display = "MERS-CoV"
