Instance: dgc-resultofthetest 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-dgc-resultofthetest" 
* name = "dgc-resultofthetest" 
* title = "DGC_ResultOfTheTest" 
* status = #active 
* version = "202105" 
* description = "**Description:** To be used in certificate 2. Contains information protected by copyright of SNOMED International. Any use of SNOMED CT in Austria requires a valid affiliate license or sublicense. The corresponding license is free of charge, provided that the use only takes place in Austria and fulfills the conditions of the Affiliate License Agreement. Affiliate licenses can be requested directly from the respective NRC via the [Member Licensing and Distribution Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS).

**Beschreibung:** Enthält durch SNOMED International urheberrechtlich geschützte Information. Jede Verwendung von SNOMED CT in Österreich erfordert eine aufrechte Affiliate Lizenz oder eine Sublizenz. Die entsprechende Lizenz ist kostenlos, vorausgesetzt die Verwendung findet nur in Österreich statt und erfüllt die Bedingungen des Affiliate License Agreements. Affiliate Lizenzen können über das [Mitglieder-Lizenzierungs- und Distributions-Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS) direkt beim jeweiligen NRC beantragt werden." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.60" 
* date = "2021-05-28" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* compose.include[0].concept[0].code = "260373001"
* compose.include[0].concept[0].display = "Detected (qualifier value)"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "nachgewiesen" 
* compose.include[0].concept[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[0].designation[1].value = "detected" 
* compose.include[0].concept[0].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[0].designation[2].value = "0" 
* compose.include[0].concept[0].designation[3].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#relationships "relationships" 
* compose.include[0].concept[0].designation[3].value = "2.16.840.1.113883.6.96:260373001" 
* compose.include[0].concept[1].code = "260415000"
* compose.include[0].concept[1].display = "Not detected (qualifier value)"
* compose.include[0].concept[1].designation[0].language = #de-AT 
* compose.include[0].concept[1].designation[0].value = "nicht nachgewiesen" 
* compose.include[0].concept[1].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[1].designation[1].value = "not detected" 
* compose.include[0].concept[1].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[1].designation[2].value = "1" 
* compose.include[0].concept[1].designation[3].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#relationships "relationships" 
* compose.include[0].concept[1].designation[3].value = "2.16.840.1.113883.6.96:260415000" 
* compose.include[0].concept[2].code = "82334004"
* compose.include[0].concept[2].display = "Indeterminate"
* compose.include[0].concept[2].designation[0].language = #de-AT 
* compose.include[0].concept[2].designation[0].value = "nicht bestimmbar" 
* compose.include[0].concept[2].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[2].designation[1].value = "Indeterminate" 
* compose.include[0].concept[2].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[2].designation[2].value = "0" 
* compose.include[0].concept[2].designation[3].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#relationships "relationships" 
* compose.include[0].concept[2].designation[3].value = "2.16.840.1.113883.6.96:82334004" 
