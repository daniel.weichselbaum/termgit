Instance: elga-participationfunctioncode 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-participationfunctioncode" 
* name = "elga-participationfunctioncode" 
* title = "ELGA_ParticipationFunctionCode" 
* status = #active 
* version = "2.6" 
* description = "**Description:** Set of valid type codes for participants (admitting physician, primary care physician...)

**Beschreibung:** Erlaubte TypeCodes für Participants (Einweiser, Hausarzt). " 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.15" 
* date = "2013-01-10" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.hl7.org" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-hl7-participation-function"
* compose.include[0].concept[0].code = #PCP
* compose.include[0].concept[0].display = "primary care physician"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "Hausärztin/Hausarzt" 

* expansion.timestamp = "2022-09-13T14:15:23.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem-hl7-participation-function"
* expansion.contains[0].code = #PCP
* expansion.contains[0].display = "primary care physician"
* expansion.contains[0].designation[0].language = #de-AT 
* expansion.contains[0].designation[0].value = "Hausärztin/Hausarzt" 
