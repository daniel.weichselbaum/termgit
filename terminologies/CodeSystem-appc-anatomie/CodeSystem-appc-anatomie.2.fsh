Instance: appc-anatomie 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-appc-anatomie" 
* name = "appc-anatomie" 
* title = "APPC_Anatomie" 
* status = #active 
* content = #complete 
* version = "1.0.10a" 
* description = "**Description:** Code List for all APPC-Codes for 4th Axis: Anatomy

**Beschreibung:** Code Liste aller Codes der 4. APPC-Achse: Anatomie

**Versions-Beschreibung:** Korrekturversion zu 1.0.10" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.38.4" 
* date = "2016-11-24" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.bura.at/appc/" 
* count = 189 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* property[2].code = #status 
* property[2].type = #code 
* property[3].code = #hints 
* property[3].type = #string 
* #0 "Maßnahme ohne regionale Zuordnung"
* #0 ^property[0].code = #child 
* #0 ^property[0].valueCode = #0-1 
* #0 ^property[1].code = #child 
* #0 ^property[1].valueCode = #0-2 
* #0 ^property[2].code = #child 
* #0 ^property[2].valueCode = #0-3 
* #0 ^property[3].code = #child 
* #0 ^property[3].valueCode = #0-4 
* #0-1 "Ganzkörper"
* #0-1 ^property[0].code = #parent 
* #0-1 ^property[0].valueCode = #0 
* #0-2 "Körperstamm"
* #0-2 ^property[0].code = #parent 
* #0-2 ^property[0].valueCode = #0 
* #0-3 "Teilkörper"
* #0-3 ^property[0].code = #parent 
* #0-3 ^property[0].valueCode = #0 
* #0-4 "Fetus"
* #0-4 ^property[0].code = #parent 
* #0-4 ^property[0].valueCode = #0 
* #1 "Schädel"
* #1 ^property[0].code = #child 
* #1 ^property[0].valueCode = #1-1 
* #1 ^property[1].code = #child 
* #1 ^property[1].valueCode = #1-2 
* #1-1 "Hirn"
* #1-1 ^property[0].code = #parent 
* #1-1 ^property[0].valueCode = #1 
* #1-1 ^property[1].code = #child 
* #1-1 ^property[1].valueCode = #1-1-1 
* #1-1 ^property[2].code = #child 
* #1-1 ^property[2].valueCode = #1-1-2 
* #1-1-1 "Kleinhirnbrückenwinkel"
* #1-1-1 ^property[0].code = #parent 
* #1-1-1 ^property[0].valueCode = #1-1 
* #1-1-2 "Sella"
* #1-1-2 ^property[0].code = #parent 
* #1-1-2 ^property[0].valueCode = #1-1 
* #1-2 "Gesichtsschädel"
* #1-2 ^property[0].code = #parent 
* #1-2 ^property[0].valueCode = #1 
* #1-2 ^property[1].code = #child 
* #1-2 ^property[1].valueCode = #1-2-1 
* #1-2 ^property[2].code = #child 
* #1-2 ^property[2].valueCode = #1-2-10 
* #1-2 ^property[3].code = #child 
* #1-2 ^property[3].valueCode = #1-2-2 
* #1-2 ^property[4].code = #child 
* #1-2 ^property[4].valueCode = #1-2-3 
* #1-2 ^property[5].code = #child 
* #1-2 ^property[5].valueCode = #1-2-4 
* #1-2 ^property[6].code = #child 
* #1-2 ^property[6].valueCode = #1-2-5 
* #1-2 ^property[7].code = #child 
* #1-2 ^property[7].valueCode = #1-2-6 
* #1-2 ^property[8].code = #child 
* #1-2 ^property[8].valueCode = #1-2-7 
* #1-2 ^property[9].code = #child 
* #1-2 ^property[9].valueCode = #1-2-8 
* #1-2 ^property[10].code = #child 
* #1-2 ^property[10].valueCode = #1-2-9 
* #1-2-1 "Orbita"
* #1-2-1 ^property[0].code = #parent 
* #1-2-1 ^property[0].valueCode = #1-2 
* #1-2-1 ^property[1].code = #child 
* #1-2-1 ^property[1].valueCode = #1-2-1-1 
* #1-2-1 ^property[2].code = #child 
* #1-2-1 ^property[2].valueCode = #1-2-1-2 
* #1-2-1-1 "Tränengang"
* #1-2-1-1 ^property[0].code = #parent 
* #1-2-1-1 ^property[0].valueCode = #1-2-1 
* #1-2-1-2 "Auge"
* #1-2-1-2 ^property[0].code = #parent 
* #1-2-1-2 ^property[0].valueCode = #1-2-1 
* #1-2-10 "Zunge"
* #1-2-10 ^property[0].code = #parent 
* #1-2-10 ^property[0].valueCode = #1-2 
* #1-2-2 "Kiefer"
* #1-2-2 ^property[0].code = #parent 
* #1-2-2 ^property[0].valueCode = #1-2 
* #1-2-2 ^property[1].code = #child 
* #1-2-2 ^property[1].valueCode = #1-2-2-1 
* #1-2-2 ^property[2].code = #child 
* #1-2-2 ^property[2].valueCode = #1-2-2-2 
* #1-2-2 ^property[3].code = #child 
* #1-2-2 ^property[3].valueCode = #1-2-2-3 
* #1-2-2 ^property[4].code = #child 
* #1-2-2 ^property[4].valueCode = #1-2-2-4 
* #1-2-2-1 "Oberkiefer"
* #1-2-2-1 ^property[0].code = #parent 
* #1-2-2-1 ^property[0].valueCode = #1-2-2 
* #1-2-2-2 "Unterkiefer"
* #1-2-2-2 ^property[0].code = #parent 
* #1-2-2-2 ^property[0].valueCode = #1-2-2 
* #1-2-2-3 "Kiefergelenk"
* #1-2-2-3 ^property[0].code = #parent 
* #1-2-2-3 ^property[0].valueCode = #1-2-2 
* #1-2-2-4 "Zahn"
* #1-2-2-4 ^property[0].code = #parent 
* #1-2-2-4 ^property[0].valueCode = #1-2-2 
* #1-2-3 "Nasennebenhöhlen"
* #1-2-3 ^property[0].code = #parent 
* #1-2-3 ^property[0].valueCode = #1-2 
* #1-2-4 "Schläfenbein"
* #1-2-4 ^property[0].code = #parent 
* #1-2-4 ^property[0].valueCode = #1-2 
* #1-2-5 "Schädelbasis"
* #1-2-5 ^property[0].code = #parent 
* #1-2-5 ^property[0].valueCode = #1-2 
* #1-2-6 "Nasenbein"
* #1-2-6 ^property[0].code = #parent 
* #1-2-6 ^property[0].valueCode = #1-2 
* #1-2-7 "Speicheldrüsen"
* #1-2-7 ^property[0].code = #parent 
* #1-2-7 ^property[0].valueCode = #1-2 
* #1-2-7 ^property[1].code = #child 
* #1-2-7 ^property[1].valueCode = #1-2-7-1 
* #1-2-7-1 "Speicheldrüsengang"
* #1-2-7-1 ^property[0].code = #parent 
* #1-2-7-1 ^property[0].valueCode = #1-2-7 
* #1-2-8 "Mundboden"
* #1-2-8 ^property[0].code = #parent 
* #1-2-8 ^property[0].valueCode = #1-2 
* #1-2-9 "Jochbogen"
* #1-2-9 ^property[0].code = #parent 
* #1-2-9 ^property[0].valueCode = #1-2 
* #10 "Nervensystem"
* #10 ^property[0].code = #child 
* #10 ^property[0].valueCode = #10-1 
* #10 ^property[1].code = #child 
* #10 ^property[1].valueCode = #10-2 
* #10 ^property[2].code = #child 
* #10 ^property[2].valueCode = #10-3 
* #10-1 "Plexus cervicobrachialis"
* #10-1 ^property[0].code = #parent 
* #10-1 ^property[0].valueCode = #10 
* #10-2 "N. ulnaris"
* #10-2 ^property[0].code = #parent 
* #10-2 ^property[0].valueCode = #10 
* #10-3 "N. medianus"
* #10-3 ^property[0].code = #parent 
* #10-3 ^property[0].valueCode = #10 
* #2 "Hals"
* #2 ^property[0].code = #child 
* #2 ^property[0].valueCode = #2-1 
* #2 ^property[1].code = #child 
* #2 ^property[1].valueCode = #2-2 
* #2 ^property[2].code = #child 
* #2 ^property[2].valueCode = #2-3 
* #2 ^property[3].code = #child 
* #2 ^property[3].valueCode = #2-4 
* #2 ^property[4].code = #child 
* #2 ^property[4].valueCode = #2-5 
* #2 ^property[5].code = #child 
* #2 ^property[5].valueCode = #2-6 
* #2-1 "Trachea"
* #2-1 ^property[0].code = #parent 
* #2-1 ^property[0].valueCode = #2 
* #2-2 "Schilddrüse"
* #2-2 ^property[0].code = #parent 
* #2-2 ^property[0].valueCode = #2 
* #2-3 "Nebenschilddrüse"
* #2-3 ^property[0].code = #parent 
* #2-3 ^property[0].valueCode = #2 
* #2-4 "Pharynx"
* #2-4 ^property[0].code = #parent 
* #2-4 ^property[0].valueCode = #2 
* #2-5 "Ösophagus (cervical)"
* #2-5 ^property[0].code = #parent 
* #2-5 ^property[0].valueCode = #2 
* #2-6 "Larynx"
* #2-6 ^property[0].code = #parent 
* #2-6 ^property[0].valueCode = #2 
* #3 "Thorax"
* #3 ^property[0].code = #child 
* #3 ^property[0].valueCode = #3-1 
* #3 ^property[1].code = #child 
* #3 ^property[1].valueCode = #3-2 
* #3 ^property[2].code = #child 
* #3 ^property[2].valueCode = #3-3 
* #3 ^property[3].code = #child 
* #3 ^property[3].valueCode = #3-4 
* #3 ^property[4].code = #child 
* #3 ^property[4].valueCode = #3-5 
* #3-1 "Lunge"
* #3-1 ^property[0].code = #parent 
* #3-1 ^property[0].valueCode = #3 
* #3-1 ^property[1].code = #child 
* #3-1 ^property[1].valueCode = #3-1-1 
* #3-1 ^property[2].code = #child 
* #3-1 ^property[2].valueCode = #3-1-2 
* #3-1-1 "Bronchien"
* #3-1-1 ^property[0].code = #parent 
* #3-1-1 ^property[0].valueCode = #3-1 
* #3-1-2 "Pleura"
* #3-1-2 ^property[0].code = #parent 
* #3-1-2 ^property[0].valueCode = #3-1 
* #3-2 "Mediastinum"
* #3-2 ^property[0].code = #parent 
* #3-2 ^property[0].valueCode = #3 
* #3-3 "Herz"
* #3-3 ^property[0].code = #parent 
* #3-3 ^property[0].valueCode = #3 
* #3-3 ^property[1].code = #child 
* #3-3 ^property[1].valueCode = #3-3-1 
* #3-3 ^property[2].code = #child 
* #3-3 ^property[2].valueCode = #3-3-2 
* #3-3 ^property[3].code = #child 
* #3-3 ^property[3].valueCode = #3-3-3 
* #3-3-1 "Myocard"
* #3-3-1 ^property[0].code = #parent 
* #3-3-1 ^property[0].valueCode = #3-3 
* #3-3-2 "Klappen"
* #3-3-2 ^property[0].code = #parent 
* #3-3-2 ^property[0].valueCode = #3-3 
* #3-3-3 "Reizleitungssystem"
* #3-3-3 ^property[0].code = #parent 
* #3-3-3 ^property[0].valueCode = #3-3 
* #3-4 "Ösophagus (thoracal)"
* #3-4 ^property[0].code = #parent 
* #3-4 ^property[0].valueCode = #3 
* #3-5 "Thoraxwand"
* #3-5 ^property[0].code = #parent 
* #3-5 ^property[0].valueCode = #3 
* #3-5 ^property[1].code = #child 
* #3-5 ^property[1].valueCode = #3-5-1 
* #3-5 ^property[2].code = #child 
* #3-5 ^property[2].valueCode = #3-5-2 
* #3-5 ^property[3].code = #child 
* #3-5 ^property[3].valueCode = #3-5-3 
* #3-5-1 "Knöcherner Thorax"
* #3-5-1 ^property[0].code = #parent 
* #3-5-1 ^property[0].valueCode = #3-5 
* #3-5-2 "Sternum"
* #3-5-2 ^property[0].code = #parent 
* #3-5-2 ^property[0].valueCode = #3-5 
* #3-5-3 "Rippen"
* #3-5-3 ^property[0].code = #parent 
* #3-5-3 ^property[0].valueCode = #3-5 
* #4 "Abdomen"
* #4 ^property[0].code = #child 
* #4 ^property[0].valueCode = #4-1 
* #4 ^property[1].code = #child 
* #4 ^property[1].valueCode = #4-2 
* #4 ^property[2].code = #child 
* #4 ^property[2].valueCode = #4-3 
* #4 ^property[3].code = #child 
* #4 ^property[3].valueCode = #4-4 
* #4 ^property[4].code = #child 
* #4 ^property[4].valueCode = #4-5 
* #4 ^property[5].code = #child 
* #4 ^property[5].valueCode = #4-6 
* #4 ^property[6].code = #child 
* #4 ^property[6].valueCode = #4-7 
* #4-1 "Oberbauch"
* #4-1 ^property[0].code = #parent 
* #4-1 ^property[0].valueCode = #4 
* #4-1 ^property[1].code = #child 
* #4-1 ^property[1].valueCode = #4-1-1 
* #4-1 ^property[2].code = #child 
* #4-1 ^property[2].valueCode = #4-1-2 
* #4-1-1 "Leber / Milz"
* #4-1-1 ^property[0].code = #parent 
* #4-1-1 ^property[0].valueCode = #4-1 
* #4-1-1 ^property[1].code = #child 
* #4-1-1 ^property[1].valueCode = #4-1-1-1 
* #4-1-1 ^property[2].code = #child 
* #4-1-1 ^property[2].valueCode = #4-1-1-2 
* #4-1-1 ^property[3].code = #child 
* #4-1-1 ^property[3].valueCode = #4-1-1-3 
* #4-1-1-1 "Gallenblase / Gallengang"
* #4-1-1-1 ^property[0].code = #parent 
* #4-1-1-1 ^property[0].valueCode = #4-1-1 
* #4-1-1-2 "Leber"
* #4-1-1-2 ^property[0].code = #parent 
* #4-1-1-2 ^property[0].valueCode = #4-1-1 
* #4-1-1-3 "Milz"
* #4-1-1-3 ^property[0].code = #parent 
* #4-1-1-3 ^property[0].valueCode = #4-1-1 
* #4-1-2 "Pankreas"
* #4-1-2 ^property[0].code = #parent 
* #4-1-2 ^property[0].valueCode = #4-1 
* #4-1-2 ^property[1].code = #child 
* #4-1-2 ^property[1].valueCode = #4-1-2-1 
* #4-1-2-1 "Pankreasgang"
* #4-1-2-1 ^property[0].code = #parent 
* #4-1-2-1 ^property[0].valueCode = #4-1-2 
* #4-2 "Darmtrakt"
* #4-2 ^property[0].code = #parent 
* #4-2 ^property[0].valueCode = #4 
* #4-2 ^property[1].code = #child 
* #4-2 ^property[1].valueCode = #4-2-1 
* #4-2 ^property[2].code = #child 
* #4-2 ^property[2].valueCode = #4-2-2 
* #4-2 ^property[3].code = #child 
* #4-2 ^property[3].valueCode = #4-2-3 
* #4-2-1 "Magen / Duodenum"
* #4-2-1 ^property[0].code = #parent 
* #4-2-1 ^property[0].valueCode = #4-2 
* #4-2-2 "Dünndam"
* #4-2-2 ^property[0].code = #parent 
* #4-2-2 ^property[0].valueCode = #4-2 
* #4-2-3 "Dickdarm"
* #4-2-3 ^property[0].code = #parent 
* #4-2-3 ^property[0].valueCode = #4-2 
* #4-2-3 ^property[1].code = #child 
* #4-2-3 ^property[1].valueCode = #4-2-3-1 
* #4-2-3 ^property[2].code = #child 
* #4-2-3 ^property[2].valueCode = #4-2-3-2 
* #4-2-3-1 "Appendix"
* #4-2-3-1 ^property[0].code = #parent 
* #4-2-3-1 ^property[0].valueCode = #4-2-3 
* #4-2-3-2 "Rektum"
* #4-2-3-2 ^property[0].code = #parent 
* #4-2-3-2 ^property[0].valueCode = #4-2-3 
* #4-3 "Nebennieren"
* #4-3 ^property[0].code = #parent 
* #4-3 ^property[0].valueCode = #4 
* #4-4 "Harntrakt"
* #4-4 ^property[0].code = #parent 
* #4-4 ^property[0].valueCode = #4 
* #4-4 ^property[1].code = #child 
* #4-4 ^property[1].valueCode = #4-4-1 
* #4-4 ^property[2].code = #child 
* #4-4 ^property[2].valueCode = #4-4-2 
* #4-4 ^property[3].code = #child 
* #4-4 ^property[3].valueCode = #4-4-3 
* #4-4 ^property[4].code = #child 
* #4-4 ^property[4].valueCode = #4-4-4 
* #4-4 ^property[5].code = #child 
* #4-4 ^property[5].valueCode = #4-4-5 
* #4-4-1 "Nieren"
* #4-4-1 ^property[0].code = #parent 
* #4-4-1 ^property[0].valueCode = #4-4 
* #4-4-1 ^property[1].code = #child 
* #4-4-1 ^property[1].valueCode = #4-4-1-1 
* #4-4-1-1 "Nierenbecken"
* #4-4-1-1 ^property[0].code = #parent 
* #4-4-1-1 ^property[0].valueCode = #4-4-1 
* #4-4-2 "Ureter"
* #4-4-2 ^property[0].code = #parent 
* #4-4-2 ^property[0].valueCode = #4-4 
* #4-4-3 "Harnblase"
* #4-4-3 ^property[0].code = #parent 
* #4-4-3 ^property[0].valueCode = #4-4 
* #4-4-4 "Urethra"
* #4-4-4 ^property[0].code = #parent 
* #4-4-4 ^property[0].valueCode = #4-4 
* #4-4-5 "NTX"
* #4-4-5 ^property[0].code = #parent 
* #4-4-5 ^property[0].valueCode = #4-4 
* #4-5 "Becken / Unterbauch"
* #4-5 ^property[0].code = #parent 
* #4-5 ^property[0].valueCode = #4 
* #4-5 ^property[1].code = #child 
* #4-5 ^property[1].valueCode = #4-5-1 
* #4-5 ^property[2].code = #child 
* #4-5 ^property[2].valueCode = #4-5-2 
* #4-5 ^property[3].code = #child 
* #4-5 ^property[3].valueCode = #4-5-3 
* #4-5 ^property[4].code = #child 
* #4-5 ^property[4].valueCode = #4-5-4 
* #4-5 ^property[5].code = #child 
* #4-5 ^property[5].valueCode = #4-5-5 
* #4-5 ^property[6].code = #child 
* #4-5 ^property[6].valueCode = #4-5-6 
* #4-5 ^property[7].code = #child 
* #4-5 ^property[7].valueCode = #4-5-7 
* #4-5 ^property[8].code = #child 
* #4-5 ^property[8].valueCode = #4-5-8 
* #4-5-1 "Uterus"
* #4-5-1 ^property[0].code = #parent 
* #4-5-1 ^property[0].valueCode = #4-5 
* #4-5-2 "Tuben"
* #4-5-2 ^property[0].code = #parent 
* #4-5-2 ^property[0].valueCode = #4-5 
* #4-5-3 "Ovar"
* #4-5-3 ^property[0].code = #parent 
* #4-5-3 ^property[0].valueCode = #4-5 
* #4-5-4 "Schwangerschaft"
* #4-5-4 ^property[0].code = #parent 
* #4-5-4 ^property[0].valueCode = #4-5 
* #4-5-5 "Prostata"
* #4-5-5 ^property[0].code = #parent 
* #4-5-5 ^property[0].valueCode = #4-5 
* #4-5-6 "Scrotum /Hoden"
* #4-5-6 ^property[0].code = #parent 
* #4-5-6 ^property[0].valueCode = #4-5 
* #4-5-7 "Penis / Corpus Cavernosum"
* #4-5-7 ^property[0].code = #parent 
* #4-5-7 ^property[0].valueCode = #4-5 
* #4-5-8 "knöchernes Becken"
* #4-5-8 ^property[0].code = #parent 
* #4-5-8 ^property[0].valueCode = #4-5 
* #4-6 "Bauchdecke"
* #4-6 ^property[0].code = #parent 
* #4-6 ^property[0].valueCode = #4 
* #4-7 "Retroperitoneum"
* #4-7 ^property[0].code = #parent 
* #4-7 ^property[0].valueCode = #4 
* #5 "Muskulo-Skelettal"
* #5 ^property[0].code = #child 
* #5 ^property[0].valueCode = #5-1 
* #5 ^property[1].code = #child 
* #5 ^property[1].valueCode = #5-2 
* #5 ^property[2].code = #child 
* #5 ^property[2].valueCode = #5-3 
* #5-1 "Arm (gesamt)"
* #5-1 ^property[0].code = #parent 
* #5-1 ^property[0].valueCode = #5 
* #5-1 ^property[1].code = #child 
* #5-1 ^property[1].valueCode = #5-1-1 
* #5-1 ^property[2].code = #child 
* #5-1 ^property[2].valueCode = #5-1-2 
* #5-1 ^property[3].code = #child 
* #5-1 ^property[3].valueCode = #5-1-3 
* #5-1 ^property[4].code = #child 
* #5-1 ^property[4].valueCode = #5-1-4 
* #5-1 ^property[5].code = #child 
* #5-1 ^property[5].valueCode = #5-1-5 
* #5-1 ^property[6].code = #child 
* #5-1 ^property[6].valueCode = #5-1-6 
* #5-1 ^property[7].code = #child 
* #5-1 ^property[7].valueCode = #5-1-7 
* #5-1-1 "Schulter"
* #5-1-1 ^property[0].code = #parent 
* #5-1-1 ^property[0].valueCode = #5-1 
* #5-1-1 ^property[1].code = #child 
* #5-1-1 ^property[1].valueCode = #5-1-1-1 
* #5-1-1 ^property[2].code = #child 
* #5-1-1 ^property[2].valueCode = #5-1-1-2 
* #5-1-1 ^property[3].code = #child 
* #5-1-1 ^property[3].valueCode = #5-1-1-3 
* #5-1-1 ^property[4].code = #child 
* #5-1-1 ^property[4].valueCode = #5-1-1-4 
* #5-1-1-1 "Schulterblatt"
* #5-1-1-1 ^property[0].code = #parent 
* #5-1-1-1 ^property[0].valueCode = #5-1-1 
* #5-1-1-2 "Clavicula"
* #5-1-1-2 ^property[0].code = #parent 
* #5-1-1-2 ^property[0].valueCode = #5-1-1 
* #5-1-1-3 "Sternoclaviculargelenk"
* #5-1-1-3 ^property[0].code = #parent 
* #5-1-1-3 ^property[0].valueCode = #5-1-1 
* #5-1-1-4 "Acromio-Claviculargelenk"
* #5-1-1-4 ^property[0].code = #parent 
* #5-1-1-4 ^property[0].valueCode = #5-1-1 
* #5-1-2 "Axilla"
* #5-1-2 ^property[0].code = #parent 
* #5-1-2 ^property[0].valueCode = #5-1 
* #5-1-3 "Oberarm"
* #5-1-3 ^property[0].code = #parent 
* #5-1-3 ^property[0].valueCode = #5-1 
* #5-1-4 "Ellenbogen"
* #5-1-4 ^property[0].code = #parent 
* #5-1-4 ^property[0].valueCode = #5-1 
* #5-1-5 "Unterarm"
* #5-1-5 ^property[0].code = #parent 
* #5-1-5 ^property[0].valueCode = #5-1 
* #5-1-6 "DEPRECATED: Handwurzel"
* #5-1-6 ^property[0].code = #status 
* #5-1-6 ^property[0].valueCode = #retired 
* #5-1-6 ^property[1].code = #parent 
* #5-1-6 ^property[1].valueCode = #5-1 
* #5-1-6 ^property[2].code = #hints 
* #5-1-6 ^property[2].valueString = "DEPRECATED: nicht mehr verwenden" 
* #5-1-7 "Hand"
* #5-1-7 ^property[0].code = #parent 
* #5-1-7 ^property[0].valueCode = #5-1 
* #5-1-7 ^property[1].code = #child 
* #5-1-7 ^property[1].valueCode = #5-1-7-1 
* #5-1-7 ^property[2].code = #child 
* #5-1-7 ^property[2].valueCode = #5-1-7-2 
* #5-1-7 ^property[3].code = #child 
* #5-1-7 ^property[3].valueCode = #5-1-7-3 
* #5-1-7 ^property[4].code = #child 
* #5-1-7 ^property[4].valueCode = #5-1-7-4 
* #5-1-7-1 "Finger"
* #5-1-7-1 ^property[0].code = #parent 
* #5-1-7-1 ^property[0].valueCode = #5-1-7 
* #5-1-7-2 "Handgelenk"
* #5-1-7-2 ^property[0].code = #parent 
* #5-1-7-2 ^property[0].valueCode = #5-1-7 
* #5-1-7-3 "Mittelhand"
* #5-1-7-3 ^property[0].code = #parent 
* #5-1-7-3 ^property[0].valueCode = #5-1-7 
* #5-1-7-4 "Handwurzel"
* #5-1-7-4 ^property[0].code = #parent 
* #5-1-7-4 ^property[0].valueCode = #5-1-7 
* #5-2 "Bein (gesamt)"
* #5-2 ^property[0].code = #parent 
* #5-2 ^property[0].valueCode = #5 
* #5-2 ^property[1].code = #child 
* #5-2 ^property[1].valueCode = #5-2-1 
* #5-2 ^property[2].code = #child 
* #5-2 ^property[2].valueCode = #5-2-2 
* #5-2 ^property[3].code = #child 
* #5-2 ^property[3].valueCode = #5-2-3 
* #5-2 ^property[4].code = #child 
* #5-2 ^property[4].valueCode = #5-2-4 
* #5-2 ^property[5].code = #child 
* #5-2 ^property[5].valueCode = #5-2-5 
* #5-2 ^property[6].code = #child 
* #5-2 ^property[6].valueCode = #5-2-6 
* #5-2 ^property[7].code = #child 
* #5-2 ^property[7].valueCode = #5-2-7 
* #5-2-1 "Hüfte"
* #5-2-1 ^property[0].code = #parent 
* #5-2-1 ^property[0].valueCode = #5-2 
* #5-2-2 "Inguinal Region"
* #5-2-2 ^property[0].code = #parent 
* #5-2-2 ^property[0].valueCode = #5-2 
* #5-2-3 "Oberschenkel"
* #5-2-3 ^property[0].code = #parent 
* #5-2-3 ^property[0].valueCode = #5-2 
* #5-2-4 "Knie"
* #5-2-4 ^property[0].code = #parent 
* #5-2-4 ^property[0].valueCode = #5-2 
* #5-2-4 ^property[1].code = #child 
* #5-2-4 ^property[1].valueCode = #5-2-4-1 
* #5-2-4-1 "Patella"
* #5-2-4-1 ^property[0].code = #parent 
* #5-2-4-1 ^property[0].valueCode = #5-2-4 
* #5-2-5 "Unterschenkel"
* #5-2-5 ^property[0].code = #parent 
* #5-2-5 ^property[0].valueCode = #5-2 
* #5-2-6 "Sprunggelenk"
* #5-2-6 ^property[0].code = #parent 
* #5-2-6 ^property[0].valueCode = #5-2 
* #5-2-7 "Fuß"
* #5-2-7 ^property[0].code = #parent 
* #5-2-7 ^property[0].valueCode = #5-2 
* #5-2-7 ^property[1].code = #child 
* #5-2-7 ^property[1].valueCode = #5-2-7-1 
* #5-2-7 ^property[2].code = #child 
* #5-2-7 ^property[2].valueCode = #5-2-7-2 
* #5-2-7 ^property[3].code = #child 
* #5-2-7 ^property[3].valueCode = #5-2-7-3 
* #5-2-7 ^property[4].code = #child 
* #5-2-7 ^property[4].valueCode = #5-2-7-4 
* #5-2-7 ^property[5].code = #child 
* #5-2-7 ^property[5].valueCode = #5-2-7-5 
* #5-2-7 ^property[6].code = #child 
* #5-2-7 ^property[6].valueCode = #5-2-7-6 
* #5-2-7-1 "Zehe"
* #5-2-7-1 ^property[0].code = #parent 
* #5-2-7-1 ^property[0].valueCode = #5-2-7 
* #5-2-7-2 "Fersenbein"
* #5-2-7-2 ^property[0].code = #parent 
* #5-2-7-2 ^property[0].valueCode = #5-2-7 
* #5-2-7-3 "Mittelfuß"
* #5-2-7-3 ^property[0].code = #parent 
* #5-2-7-3 ^property[0].valueCode = #5-2-7 
* #5-2-7-4 "Fußwurzel"
* #5-2-7-4 ^property[0].code = #parent 
* #5-2-7-4 ^property[0].valueCode = #5-2-7 
* #5-2-7-5 "Achillessehne"
* #5-2-7-5 ^property[0].code = #parent 
* #5-2-7-5 ^property[0].valueCode = #5-2-7 
* #5-2-7-6 "Vorfuß"
* #5-2-7-6 ^property[0].code = #parent 
* #5-2-7-6 ^property[0].valueCode = #5-2-7 
* #5-3 "Wirbelsäule"
* #5-3 ^property[0].code = #parent 
* #5-3 ^property[0].valueCode = #5 
* #5-3 ^property[1].code = #child 
* #5-3 ^property[1].valueCode = #5-3-1 
* #5-3 ^property[2].code = #child 
* #5-3 ^property[2].valueCode = #5-3-2 
* #5-3 ^property[3].code = #child 
* #5-3 ^property[3].valueCode = #5-3-3 
* #5-3 ^property[4].code = #child 
* #5-3 ^property[4].valueCode = #5-3-4 
* #5-3 ^property[5].code = #child 
* #5-3 ^property[5].valueCode = #5-3-5 
* #5-3-1 "Halswirbelsäule"
* #5-3-1 ^property[0].code = #parent 
* #5-3-1 ^property[0].valueCode = #5-3 
* #5-3-2 "Brustwirbelsäule"
* #5-3-2 ^property[0].code = #parent 
* #5-3-2 ^property[0].valueCode = #5-3 
* #5-3-3 "Lendenwirbelsäule"
* #5-3-3 ^property[0].code = #parent 
* #5-3-3 ^property[0].valueCode = #5-3 
* #5-3-4 "Kreuz / Steißbein / Sacro-Iliacalgelenke"
* #5-3-4 ^property[0].code = #parent 
* #5-3-4 ^property[0].valueCode = #5-3 
* #5-3-5 "Gesamtaufnahme der Wirbelsäule"
* #5-3-5 ^property[0].code = #parent 
* #5-3-5 ^property[0].valueCode = #5-3 
* #6 "Gefäßsystem"
* #6 ^property[0].code = #child 
* #6 ^property[0].valueCode = #6-1 
* #6 ^property[1].code = #child 
* #6 ^property[1].valueCode = #6-2 
* #6 ^property[2].code = #child 
* #6 ^property[2].valueCode = #6-3 
* #6-1 "Gefäße arteriell"
* #6-1 ^property[0].code = #parent 
* #6-1 ^property[0].valueCode = #6 
* #6-1 ^property[1].code = #child 
* #6-1 ^property[1].valueCode = #6-1-1 
* #6-1 ^property[2].code = #child 
* #6-1 ^property[2].valueCode = #6-1-2 
* #6-1 ^property[3].code = #child 
* #6-1 ^property[3].valueCode = #6-1-3 
* #6-1 ^property[4].code = #child 
* #6-1 ^property[4].valueCode = #6-1-4 
* #6-1-1 "intrakraniell"
* #6-1-1 ^property[0].code = #parent 
* #6-1-1 ^property[0].valueCode = #6-1 
* #6-1-1 ^property[1].code = #child 
* #6-1-1 ^property[1].valueCode = #6-1-1-1 
* #6-1-1 ^property[2].code = #child 
* #6-1-1 ^property[2].valueCode = #6-1-1-2 
* #6-1-1 ^property[3].code = #child 
* #6-1-1 ^property[3].valueCode = #6-1-1-3 
* #6-1-1 ^property[4].code = #child 
* #6-1-1 ^property[4].valueCode = #6-1-1-4 
* #6-1-1 ^property[5].code = #child 
* #6-1-1 ^property[5].valueCode = #6-1-1-5 
* #6-1-1-1 "A. cerebri anterior"
* #6-1-1-1 ^property[0].code = #parent 
* #6-1-1-1 ^property[0].valueCode = #6-1-1 
* #6-1-1-2 "A.cerebri media"
* #6-1-1-2 ^property[0].code = #parent 
* #6-1-1-2 ^property[0].valueCode = #6-1-1 
* #6-1-1-3 "A. cerebri posterior"
* #6-1-1-3 ^property[0].code = #parent 
* #6-1-1-3 ^property[0].valueCode = #6-1-1 
* #6-1-1-4 "A basilaris"
* #6-1-1-4 ^property[0].code = #parent 
* #6-1-1-4 ^property[0].valueCode = #6-1-1 
* #6-1-1-5 "Circulus arteriosus"
* #6-1-1-5 ^property[0].code = #parent 
* #6-1-1-5 ^property[0].valueCode = #6-1-1 
* #6-1-2 "Aorta"
* #6-1-2 ^property[0].code = #parent 
* #6-1-2 ^property[0].valueCode = #6-1 
* #6-1-2 ^property[1].code = #child 
* #6-1-2 ^property[1].valueCode = #6-1-2-1 
* #6-1-2 ^property[2].code = #child 
* #6-1-2 ^property[2].valueCode = #6-1-2-2 
* #6-1-2-1 "thorakal"
* #6-1-2-1 ^property[0].code = #parent 
* #6-1-2-1 ^property[0].valueCode = #6-1-2 
* #6-1-2-2 "abdominal"
* #6-1-2-2 ^property[0].code = #parent 
* #6-1-2-2 ^property[0].valueCode = #6-1-2 
* #6-1-3 "Aortenäste"
* #6-1-3 ^property[0].code = #parent 
* #6-1-3 ^property[0].valueCode = #6-1 
* #6-1-3 ^property[1].code = #child 
* #6-1-3 ^property[1].valueCode = #6-1-3-1 
* #6-1-3 ^property[2].code = #child 
* #6-1-3 ^property[2].valueCode = #6-1-3-2 
* #6-1-3 ^property[3].code = #child 
* #6-1-3 ^property[3].valueCode = #6-1-3-3 
* #6-1-3 ^property[4].code = #child 
* #6-1-3 ^property[4].valueCode = #6-1-3-4 
* #6-1-3 ^property[5].code = #child 
* #6-1-3 ^property[5].valueCode = #6-1-3-5 
* #6-1-3 ^property[6].code = #child 
* #6-1-3 ^property[6].valueCode = #6-1-3-6 
* #6-1-3 ^property[7].code = #child 
* #6-1-3 ^property[7].valueCode = #6-1-3-7 
* #6-1-3-1 "Hals (Carotis / Vertebralis)"
* #6-1-3-1 ^property[0].code = #parent 
* #6-1-3-1 ^property[0].valueCode = #6-1-3 
* #6-1-3-1 ^property[1].code = #child 
* #6-1-3-1 ^property[1].valueCode = #6-1-3-1-1 
* #6-1-3-1 ^property[2].code = #child 
* #6-1-3-1 ^property[2].valueCode = #6-1-3-1-2 
* #6-1-3-1 ^property[3].code = #child 
* #6-1-3-1 ^property[3].valueCode = #6-1-3-1-3 
* #6-1-3-1 ^property[4].code = #child 
* #6-1-3-1 ^property[4].valueCode = #6-1-3-1-4 
* #6-1-3-1-1 "Carotis"
* #6-1-3-1-1 ^property[0].code = #parent 
* #6-1-3-1-1 ^property[0].valueCode = #6-1-3-1 
* #6-1-3-1-2 "Carotis externa"
* #6-1-3-1-2 ^property[0].code = #parent 
* #6-1-3-1-2 ^property[0].valueCode = #6-1-3-1 
* #6-1-3-1-3 "Carotis interna"
* #6-1-3-1-3 ^property[0].code = #parent 
* #6-1-3-1-3 ^property[0].valueCode = #6-1-3-1 
* #6-1-3-1-4 "Vertebralis"
* #6-1-3-1-4 ^property[0].code = #parent 
* #6-1-3-1-4 ^property[0].valueCode = #6-1-3-1 
* #6-1-3-2 "Coronarien"
* #6-1-3-2 ^property[0].code = #parent 
* #6-1-3-2 ^property[0].valueCode = #6-1-3 
* #6-1-3-3 "Pulmonalgefäße"
* #6-1-3-3 ^property[0].code = #parent 
* #6-1-3-3 ^property[0].valueCode = #6-1-3 
* #6-1-3-4 "visceral"
* #6-1-3-4 ^property[0].code = #parent 
* #6-1-3-4 ^property[0].valueCode = #6-1-3 
* #6-1-3-4 ^property[1].code = #child 
* #6-1-3-4 ^property[1].valueCode = #6-1-3-4-1 
* #6-1-3-4 ^property[2].code = #child 
* #6-1-3-4 ^property[2].valueCode = #6-1-3-4-2 
* #6-1-3-4 ^property[3].code = #child 
* #6-1-3-4 ^property[3].valueCode = #6-1-3-4-3 
* #6-1-3-4 ^property[4].code = #child 
* #6-1-3-4 ^property[4].valueCode = #6-1-3-4-4 
* #6-1-3-4-1 "Truncus coeliacus"
* #6-1-3-4-1 ^property[0].code = #parent 
* #6-1-3-4-1 ^property[0].valueCode = #6-1-3-4 
* #6-1-3-4-2 "A. mesenterica superior"
* #6-1-3-4-2 ^property[0].code = #parent 
* #6-1-3-4-2 ^property[0].valueCode = #6-1-3-4 
* #6-1-3-4-3 "A. mesenterica inferior"
* #6-1-3-4-3 ^property[0].code = #parent 
* #6-1-3-4-3 ^property[0].valueCode = #6-1-3-4 
* #6-1-3-4-4 "A. spinalis"
* #6-1-3-4-4 ^property[0].code = #parent 
* #6-1-3-4-4 ^property[0].valueCode = #6-1-3-4 
* #6-1-3-5 "Nierenarterien"
* #6-1-3-5 ^property[0].code = #parent 
* #6-1-3-5 ^property[0].valueCode = #6-1-3 
* #6-1-3-6 "Armarterien"
* #6-1-3-6 ^property[0].code = #parent 
* #6-1-3-6 ^property[0].valueCode = #6-1-3 
* #6-1-3-6 ^property[1].code = #child 
* #6-1-3-6 ^property[1].valueCode = #6-1-3-6-1 
* #6-1-3-6 ^property[2].code = #child 
* #6-1-3-6 ^property[2].valueCode = #6-1-3-6-2 
* #6-1-3-6 ^property[3].code = #child 
* #6-1-3-6 ^property[3].valueCode = #6-1-3-6-3 
* #6-1-3-6 ^property[4].code = #child 
* #6-1-3-6 ^property[4].valueCode = #6-1-3-6-4 
* #6-1-3-6-1 "Subclavia / Truncus Brachiocephalicus"
* #6-1-3-6-1 ^property[0].code = #parent 
* #6-1-3-6-1 ^property[0].valueCode = #6-1-3-6 
* #6-1-3-6-2 "Oberarm"
* #6-1-3-6-2 ^property[0].code = #parent 
* #6-1-3-6-2 ^property[0].valueCode = #6-1-3-6 
* #6-1-3-6-3 "Unterarm"
* #6-1-3-6-3 ^property[0].code = #parent 
* #6-1-3-6-3 ^property[0].valueCode = #6-1-3-6 
* #6-1-3-6-4 "Hand"
* #6-1-3-6-4 ^property[0].code = #parent 
* #6-1-3-6-4 ^property[0].valueCode = #6-1-3-6 
* #6-1-3-7 "Beinarterien"
* #6-1-3-7 ^property[0].code = #parent 
* #6-1-3-7 ^property[0].valueCode = #6-1-3 
* #6-1-3-7 ^property[1].code = #child 
* #6-1-3-7 ^property[1].valueCode = #6-1-3-7-1 
* #6-1-3-7 ^property[2].code = #child 
* #6-1-3-7 ^property[2].valueCode = #6-1-3-7-2 
* #6-1-3-7 ^property[3].code = #child 
* #6-1-3-7 ^property[3].valueCode = #6-1-3-7-3 
* #6-1-3-7 ^property[4].code = #child 
* #6-1-3-7 ^property[4].valueCode = #6-1-3-7-4 
* #6-1-3-7-1 "Iliaca"
* #6-1-3-7-1 ^property[0].code = #parent 
* #6-1-3-7-1 ^property[0].valueCode = #6-1-3-7 
* #6-1-3-7-2 "Oberschenkel"
* #6-1-3-7-2 ^property[0].code = #parent 
* #6-1-3-7-2 ^property[0].valueCode = #6-1-3-7 
* #6-1-3-7-3 "Unterschenkel"
* #6-1-3-7-3 ^property[0].code = #parent 
* #6-1-3-7-3 ^property[0].valueCode = #6-1-3-7 
* #6-1-3-7-4 "Fuß"
* #6-1-3-7-4 ^property[0].code = #parent 
* #6-1-3-7-4 ^property[0].valueCode = #6-1-3-7 
* #6-1-4 "Dialyseshunt"
* #6-1-4 ^property[0].code = #parent 
* #6-1-4 ^property[0].valueCode = #6-1 
* #6-2 "Gefäße venös"
* #6-2 ^property[0].code = #parent 
* #6-2 ^property[0].valueCode = #6 
* #6-2 ^property[1].code = #child 
* #6-2 ^property[1].valueCode = #6-2-1 
* #6-2 ^property[2].code = #child 
* #6-2 ^property[2].valueCode = #6-2-10 
* #6-2 ^property[3].code = #child 
* #6-2 ^property[3].valueCode = #6-2-2 
* #6-2 ^property[4].code = #child 
* #6-2 ^property[4].valueCode = #6-2-3 
* #6-2 ^property[5].code = #child 
* #6-2 ^property[5].valueCode = #6-2-4 
* #6-2 ^property[6].code = #child 
* #6-2 ^property[6].valueCode = #6-2-5 
* #6-2 ^property[7].code = #child 
* #6-2 ^property[7].valueCode = #6-2-6 
* #6-2 ^property[8].code = #child 
* #6-2 ^property[8].valueCode = #6-2-7 
* #6-2 ^property[9].code = #child 
* #6-2 ^property[9].valueCode = #6-2-8 
* #6-2 ^property[10].code = #child 
* #6-2 ^property[10].valueCode = #6-2-9 
* #6-2-1 "Sinus cerebrales"
* #6-2-1 ^property[0].code = #parent 
* #6-2-1 ^property[0].valueCode = #6-2 
* #6-2-10 "viszerale Venen"
* #6-2-10 ^property[0].code = #parent 
* #6-2-10 ^property[0].valueCode = #6-2 
* #6-2-2 "Armvenen"
* #6-2-2 ^property[0].code = #parent 
* #6-2-2 ^property[0].valueCode = #6-2 
* #6-2-2 ^property[1].code = #child 
* #6-2-2 ^property[1].valueCode = #6-2-2-1 
* #6-2-2 ^property[2].code = #child 
* #6-2-2 ^property[2].valueCode = #6-2-2-2 
* #6-2-2 ^property[3].code = #child 
* #6-2-2 ^property[3].valueCode = #6-2-2-3 
* #6-2-2 ^property[4].code = #child 
* #6-2-2 ^property[4].valueCode = #6-2-2-4 
* #6-2-2-1 "Subclavia"
* #6-2-2-1 ^property[0].code = #parent 
* #6-2-2-1 ^property[0].valueCode = #6-2-2 
* #6-2-2-2 "Oberarm"
* #6-2-2-2 ^property[0].code = #parent 
* #6-2-2-2 ^property[0].valueCode = #6-2-2 
* #6-2-2-3 "Unterarm"
* #6-2-2-3 ^property[0].code = #parent 
* #6-2-2-3 ^property[0].valueCode = #6-2-2 
* #6-2-2-4 "Hand"
* #6-2-2-4 ^property[0].code = #parent 
* #6-2-2-4 ^property[0].valueCode = #6-2-2 
* #6-2-3 "Cava superior"
* #6-2-3 ^property[0].code = #parent 
* #6-2-3 ^property[0].valueCode = #6-2 
* #6-2-4 "Cava inferior"
* #6-2-4 ^property[0].code = #parent 
* #6-2-4 ^property[0].valueCode = #6-2 
* #6-2-5 "Lebervenen"
* #6-2-5 ^property[0].code = #parent 
* #6-2-5 ^property[0].valueCode = #6-2 
* #6-2-6 "Nierenvenen"
* #6-2-6 ^property[0].code = #parent 
* #6-2-6 ^property[0].valueCode = #6-2 
* #6-2-6 ^property[1].code = #child 
* #6-2-6 ^property[1].valueCode = #6-2-6-1 
* #6-2-6-1 "V. spermatica / ovarica"
* #6-2-6-1 ^property[0].code = #parent 
* #6-2-6-1 ^property[0].valueCode = #6-2-6 
* #6-2-7 "Beinvenen"
* #6-2-7 ^property[0].code = #parent 
* #6-2-7 ^property[0].valueCode = #6-2 
* #6-2-7 ^property[1].code = #child 
* #6-2-7 ^property[1].valueCode = #6-2-7-1 
* #6-2-7 ^property[2].code = #child 
* #6-2-7 ^property[2].valueCode = #6-2-7-2 
* #6-2-7 ^property[3].code = #child 
* #6-2-7 ^property[3].valueCode = #6-2-7-3 
* #6-2-7 ^property[4].code = #child 
* #6-2-7 ^property[4].valueCode = #6-2-7-4 
* #6-2-7-1 "Iliaca / Bein"
* #6-2-7-1 ^property[0].code = #parent 
* #6-2-7-1 ^property[0].valueCode = #6-2-7 
* #6-2-7-2 "Oberschenkel"
* #6-2-7-2 ^property[0].code = #parent 
* #6-2-7-2 ^property[0].valueCode = #6-2-7 
* #6-2-7-3 "Unterschenkel"
* #6-2-7-3 ^property[0].code = #parent 
* #6-2-7-3 ^property[0].valueCode = #6-2-7 
* #6-2-7-4 "Fuß"
* #6-2-7-4 ^property[0].code = #parent 
* #6-2-7-4 ^property[0].valueCode = #6-2-7 
* #6-2-8 "V. jugularis"
* #6-2-8 ^property[0].code = #parent 
* #6-2-8 ^property[0].valueCode = #6-2 
* #6-2-9 "Pfortader"
* #6-2-9 ^property[0].code = #parent 
* #6-2-9 ^property[0].valueCode = #6-2 
* #6-3 "Lymphgefäße und Lymphknoten"
* #6-3 ^property[0].code = #parent 
* #6-3 ^property[0].valueCode = #6 
* #6-3 ^property[1].code = #child 
* #6-3 ^property[1].valueCode = #6-3-1 
* #6-3 ^property[2].code = #child 
* #6-3 ^property[2].valueCode = #6-3-2 
* #6-3 ^property[3].code = #child 
* #6-3 ^property[3].valueCode = #6-3-3 
* #6-3 ^property[4].code = #child 
* #6-3 ^property[4].valueCode = #6-3-4 
* #6-3 ^property[5].code = #child 
* #6-3 ^property[5].valueCode = #6-3-5 
* #6-3 ^property[6].code = #child 
* #6-3 ^property[6].valueCode = #6-3-6 
* #6-3 ^property[7].code = #child 
* #6-3 ^property[7].valueCode = #6-3-7 
* #6-3-1 "Ganzkörper"
* #6-3-1 ^property[0].code = #parent 
* #6-3-1 ^property[0].valueCode = #6-3 
* #6-3-2 "Kopf"
* #6-3-2 ^property[0].code = #parent 
* #6-3-2 ^property[0].valueCode = #6-3 
* #6-3-3 "Hals"
* #6-3-3 ^property[0].code = #parent 
* #6-3-3 ^property[0].valueCode = #6-3 
* #6-3-4 "Thorax"
* #6-3-4 ^property[0].code = #parent 
* #6-3-4 ^property[0].valueCode = #6-3 
* #6-3-5 "Abdomen"
* #6-3-5 ^property[0].code = #parent 
* #6-3-5 ^property[0].valueCode = #6-3 
* #6-3-6 "Arm"
* #6-3-6 ^property[0].code = #parent 
* #6-3-6 ^property[0].valueCode = #6-3 
* #6-3-7 "Bein"
* #6-3-7 ^property[0].code = #parent 
* #6-3-7 ^property[0].valueCode = #6-3 
* #7 "Mamma"
* #7 ^property[0].code = #child 
* #7 ^property[0].valueCode = #7-1 
* #7-1 "Milchgang"
* #7-1 ^property[0].code = #parent 
* #7-1 ^property[0].valueCode = #7 
