Instance: elga-maritalstatus 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-maritalstatus" 
* name = "elga-maritalstatus" 
* title = "ELGA_MaritalStatus" 
* status = #active 
* version = "2.6" 
* description = "**Description:** This Value set is used to state the martial status.

**Beschreibung:** Das Value Set wird verwendet, um den Familienstand anzugeben." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.11" 
* date = "2013-01-10" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.hl7.org" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-hl7-marital-status"
* compose.include[0].concept[0].code = "D"
* compose.include[0].concept[0].display = "Divorced"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "Geschieden" 
* compose.include[0].concept[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[0].designation[1].value = "Marriage contract has been declared dissolved and inactive" 
* compose.include[0].concept[1].code = "M"
* compose.include[0].concept[1].display = "Married"
* compose.include[0].concept[1].designation[0].language = #de-AT 
* compose.include[0].concept[1].designation[0].value = "Verheiratet" 
* compose.include[0].concept[1].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[1].designation[1].value = "A current marriage contract is active" 
* compose.include[0].concept[2].code = "S"
* compose.include[0].concept[2].display = "Never Married"
* compose.include[0].concept[2].designation[0].language = #de-AT 
* compose.include[0].concept[2].designation[0].value = "Ledig" 
* compose.include[0].concept[3].code = "T"
* compose.include[0].concept[3].display = "Domestic partner"
* compose.include[0].concept[3].designation[0].language = #de-AT 
* compose.include[0].concept[3].designation[0].value = "In Lebenspartnerschaft" 
* compose.include[0].concept[3].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[3].designation[1].value = "Person declares that a domestic partner relationship exists." 
* compose.include[0].concept[4].code = "W"
* compose.include[0].concept[4].display = "Widowed"
* compose.include[0].concept[4].designation[0].language = #de-AT 
* compose.include[0].concept[4].designation[0].value = "Verwitwet" 
* compose.include[0].concept[4].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[4].designation[1].value = "The spouse has died" 
