Instance: appc-lateralitaet 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-appc-lateralitaet" 
* name = "appc-lateralitaet" 
* title = "APPC_Lateralitaet" 
* status = #active 
* content = #complete 
* version = "1.0.8" 
* description = "**Description:** Code List for all APPC-Codes for 2nd Axis: Laterality

**Beschreibung:** Code Liste aller Codes der 2. APPC-Achse: Lateralität" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.38.2" 
* date = "2014-09-01" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.bura.at/appc/" 
* count = 6 
* #0 "Lateralitaet unbestimmt"
* #0 ^designation[0].language = #de-AT 
* #0 ^designation[0].value = "Lateralitaet unbestimmt" 
* #1 "rechts"
* #1 ^designation[0].language = #de-AT 
* #1 ^designation[0].value = "rechts" 
* #2 "links"
* #2 ^designation[0].language = #de-AT 
* #2 ^designation[0].value = "links" 
* #3 "beidseits"
* #3 ^designation[0].language = #de-AT 
* #3 ^designation[0].value = "beidseits" 
* #4 "unpaariges Organ"
* #4 ^designation[0].language = #de-AT 
* #4 ^designation[0].value = "unpaariges Organ" 
* #5 "Atypische Situation ( - Transplantat etc.)"
* #5 ^designation[0].language = #de-AT 
* #5 ^designation[0].value = "Atypische Situation ( - Transplantat etc.)" 
