Instance: hl7-entity-name-use 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-entity-name-use" 
* name = "hl7-entity-name-use" 
* title = "HL7 Entity Name Use" 
* status = #active 
* content = #complete 
* version = "HL7:EntityNameUse" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.5.45" 
* date = "2013-01-10" 
* count = 15 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #ASGN "assigned"
* #C "License"
* #I "Indigenous/Tribal"
* #L "Legal"
* #L ^property[0].code = #child 
* #L ^property[0].valueCode = #OR 
* #OR "official registry"
* #OR ^property[0].code = #parent 
* #OR ^property[0].valueCode = #L 
* #P "pseudonym"
* #P ^property[0].code = #child 
* #P ^property[0].valueCode = #A 
* #A "Artist/Stage"
* #A ^property[0].code = #parent 
* #A ^property[0].valueCode = #P 
* #R "Religious"
* #SRCH "search v:HL7SearchUse"
* #SRCH ^property[0].code = #child 
* #SRCH ^property[0].valueCode = #PHON 
* #SRCH ^property[1].code = #child 
* #SRCH ^property[1].valueCode = #SNDX 
* #PHON "phonetic"
* #PHON ^property[0].code = #parent 
* #PHON ^property[0].valueCode = #SRCH 
* #SNDX "Soundex"
* #SNDX ^property[0].code = #parent 
* #SNDX ^property[0].valueCode = #SRCH 
* #_NameRepresentationUse "NameRepresentationUse"
* #_NameRepresentationUse ^property[0].code = #child 
* #_NameRepresentationUse ^property[0].valueCode = #ABC 
* #_NameRepresentationUse ^property[1].code = #child 
* #_NameRepresentationUse ^property[1].valueCode = #IDE 
* #_NameRepresentationUse ^property[2].code = #child 
* #_NameRepresentationUse ^property[2].valueCode = #SYL 
* #ABC "Alphabetic"
* #ABC ^property[0].code = #parent 
* #ABC ^property[0].valueCode = #_NameRepresentationUse 
* #IDE "Ideographic"
* #IDE ^property[0].code = #parent 
* #IDE ^property[0].valueCode = #_NameRepresentationUse 
* #SYL "Syllabic"
* #SYL ^property[0].code = #parent 
* #SYL ^property[0].valueCode = #_NameRepresentationUse 
