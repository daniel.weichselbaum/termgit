Instance: oeaek-berechtigungen 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-oeaek-berechtigungen" 
* name = "oeaek-berechtigungen" 
* title = "OEAEK_Berechtigungen" 
* status = #active 
* content = #complete 
* version = "1.0" 
* description = "**Description:** Code List of all disciplines valid in Austria issued by the Medical Chamber of Austria

**Beschreibung:** Code Liste aller in Österreich gültigen Fachrichtungen" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.3.1.10" 
* date = "2018-07-01" 
* count = 2 
* concept[0].code = #0
* concept[0].display = "Allgemeinmedizin"
* concept[0].designation[0].language = #de-AT 
* concept[0].designation[0].value = "Allgemeinmedizin" 
* concept[1].code = #99
* concept[1].display = "Approbierter Arzt"
* concept[1].designation[0].language = #de-AT 
* concept[1].designation[0].value = "Approbierter Arzt" 
