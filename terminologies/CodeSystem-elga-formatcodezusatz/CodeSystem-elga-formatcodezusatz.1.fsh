Instance: elga-formatcodezusatz 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-formatcodezusatz" 
* name = "elga-formatcodezusatz" 
* title = "ELGA_FormatCodeZusatz" 
* status = #active 
* content = #complete 
* version = "2.8" 
* description = "**Description:** Defintion of valid additions for the FormatCode by declaration in Context of XDS document metadata

**Beschreibung:** Definition der gültigen Zusätze für den FormatCode bei Angabe im Rahmen der XDS Dokument Metadaten" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.41" 
* date = "2013-01-10" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* count = 6 
* concept[0].code = #CDAL1
* concept[0].display = "CDAL1"
* concept[0].definition = "Das Dokument enthält im CDA Body Bereich strukturierte Angaben gemäß CDA Level 1 (structuredBody)"
* concept[0].designation[0].language = #de-AT 
* concept[0].designation[0].value = "CDAL1" 
* concept[1].code = #GIF
* concept[1].display = "GIF"
* concept[1].definition = "Das Dokument enthält im CDA Body Bereich ein eingebettetes Bild im Graphics Interchange Format (GIF)."
* concept[1].designation[0].language = #de-AT 
* concept[1].designation[0].value = "GIF" 
* concept[2].code = #JPG
* concept[2].display = "JPG"
* concept[2].definition = "Das Dokument enthält im CDA Body Bereich ein eingebettetes Bild gemäß der Norm ISO/IEC 10918-1 bzw. CCITT Recommendation T.81 der Joint Photographic Experts Group (JPEG)."
* concept[2].designation[0].language = #de-AT 
* concept[2].designation[0].value = "JPG" 
* concept[3].code = #PDF
* concept[3].display = "PDF"
* concept[3].definition = "Das Dokument enthält im CDA Body Bereich ein eingebettetes PDF/A-1a Dokument."
* concept[3].designation[0].language = #de-AT 
* concept[3].designation[0].value = "PDF" 
* concept[4].code = #PNG
* concept[4].display = "PNG"
* concept[4].definition = "Das Dokument enthält im CDA Body Bereich ein eingebettetes Bild im Portable Network Graphics (PNG) Format."
* concept[4].designation[0].language = #de-AT 
* concept[4].designation[0].value = "PNG" 
* concept[5].code = #TEXT
* concept[5].display = "TEXT"
* concept[5].definition = "Das Dokument enthält im CDA Body Bereich unstrukturierten Text gemäß CDA Level 1 (nonXMLBody)"
* concept[5].designation[0].language = #de-AT 
* concept[5].designation[0].value = "TEXT" 
