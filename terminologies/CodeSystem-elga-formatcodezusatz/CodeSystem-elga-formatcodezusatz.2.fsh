Instance: elga-formatcodezusatz 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-formatcodezusatz" 
* name = "elga-formatcodezusatz" 
* title = "ELGA_FormatCodeZusatz" 
* status = #active 
* content = #complete 
* version = "2.8" 
* description = "**Description:** Defintion of valid additions for the FormatCode by declaration in Context of XDS document metadata

**Beschreibung:** Definition der gültigen Zusätze für den FormatCode bei Angabe im Rahmen der XDS Dokument Metadaten" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.41" 
* date = "2013-01-10" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* count = 6 
* #CDAL1 "CDAL1"
* #CDAL1 ^definition = Das Dokument enthält im CDA Body Bereich strukturierte Angaben gemäß CDA Level 1 (structuredBody)
* #CDAL1 ^designation[0].language = #de-AT 
* #CDAL1 ^designation[0].value = "CDAL1" 
* #GIF "GIF"
* #GIF ^definition = Das Dokument enthält im CDA Body Bereich ein eingebettetes Bild im Graphics Interchange Format (GIF).
* #GIF ^designation[0].language = #de-AT 
* #GIF ^designation[0].value = "GIF" 
* #JPG "JPG"
* #JPG ^definition = Das Dokument enthält im CDA Body Bereich ein eingebettetes Bild gemäß der Norm ISO/IEC 10918-1 bzw. CCITT Recommendation T.81 der Joint Photographic Experts Group (JPEG).
* #JPG ^designation[0].language = #de-AT 
* #JPG ^designation[0].value = "JPG" 
* #PDF "PDF"
* #PDF ^definition = Das Dokument enthält im CDA Body Bereich ein eingebettetes PDF/A-1a Dokument.
* #PDF ^designation[0].language = #de-AT 
* #PDF ^designation[0].value = "PDF" 
* #PNG "PNG"
* #PNG ^definition = Das Dokument enthält im CDA Body Bereich ein eingebettetes Bild im Portable Network Graphics (PNG) Format.
* #PNG ^designation[0].language = #de-AT 
* #PNG ^designation[0].value = "PNG" 
* #TEXT "TEXT"
* #TEXT ^definition = Das Dokument enthält im CDA Body Bereich unstrukturierten Text gemäß CDA Level 1 (nonXMLBody)
* #TEXT ^designation[0].language = #de-AT 
* #TEXT ^designation[0].value = "TEXT" 
