Instance: ems-gewinnung 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-gewinnung" 
* name = "ems-gewinnung" 
* title = "EMS_Gewinnung" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** Gewinnung" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.93" 
* date = "2017-01-26" 
* count = 7 
* concept[0].code = #BIOPSY
* concept[0].display = "Biopsie"
* concept[1].code = #BRUSH
* concept[1].display = "Bürste"
* concept[2].code = #LAVAGE
* concept[2].display = "Lavage"
* concept[3].code = #PUNCT
* concept[3].display = "Punktion"
* concept[4].code = #SCUM
* concept[4].display = "Abstrich"
* concept[5].code = #SPUTUM
* concept[5].display = "Auswurf"
* concept[6].code = #SUCTION
* concept[6].display = "Absaugung"
