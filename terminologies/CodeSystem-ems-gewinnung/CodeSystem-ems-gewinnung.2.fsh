Instance: ems-gewinnung 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-gewinnung" 
* name = "ems-gewinnung" 
* title = "EMS_Gewinnung" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** Gewinnung" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.93" 
* date = "2017-01-26" 
* count = 7 
* #BIOPSY "Biopsie"
* #BRUSH "Bürste"
* #LAVAGE "Lavage"
* #PUNCT "Punktion"
* #SCUM "Abstrich"
* #SPUTUM "Auswurf"
* #SUCTION "Absaugung"
