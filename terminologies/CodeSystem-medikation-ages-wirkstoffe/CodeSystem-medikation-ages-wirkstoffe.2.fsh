Instance: medikation-ages-wirkstoffe 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-medikation-ages-wirkstoffe" 
* name = "medikation-ages-wirkstoffe" 
* title = "Medikation_AGES_Wirkstoffe" 
* status = #active 
* content = #complete 
* version = "20220930" 
* description = "**Description:** Medikation AGES Wirkstoffe

**Beschreibung:** Medikation AGES Wirkstoffe" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.156" 
* date = "2022-09-30" 
* count = 2205 
* #10007909 "TIVOZANIB HYDROCHLORID MONOHYDRAT"
* #10007909 ^designation[0].language = #de-AT 
* #10007909 ^designation[0].value = "TIVOZANIB HYDROCHLORID MONOHYDRAT" 
* #100099032 "SELUMETINIBSULFAT"
* #100099032 ^designation[0].language = #de-AT 
* #100099032 ^designation[0].value = "SELUMETINIBSULFAT" 
* #100099044 "TRALOKINUMAB"
* #100099044 ^designation[0].language = #de-AT 
* #100099044 ^designation[0].value = "TRALOKINUMAB" 
* #100099122 "ASCIMINIBHYDROCHLORID"
* #100099122 ^designation[0].language = #de-AT 
* #100099122 ^designation[0].value = "ASCIMINIBHYDROCHLORID" 
* #100192767 "VERICIGUAT"
* #100192767 ^designation[0].language = #de-AT 
* #100192767 ^designation[0].value = "VERICIGUAT" 
* #100192769 "ODEVIXIBAT SESQUIHYDRAT"
* #100192769 ^designation[0].language = #de-AT 
* #100192769 ^designation[0].value = "ODEVIXIBAT SESQUIHYDRAT" 
* #100192817 "TIRBANIBULIN"
* #100192817 ^designation[0].language = #de-AT 
* #100192817 ^designation[0].value = "TIRBANIBULIN" 
* #100192857 "RELUGOLIX"
* #100192857 ^designation[0].language = #de-AT 
* #100192857 ^designation[0].value = "RELUGOLIX" 
* #10021492 "TRASTUZUMAB EMTANSIN"
* #10021492 ^designation[0].language = #de-AT 
* #10021492 ^designation[0].value = "TRASTUZUMAB EMTANSIN" 
* #10021497 "PASIREOTIDEMBONAT"
* #10021497 ^designation[0].language = #de-AT 
* #10021497 ^designation[0].value = "PASIREOTIDEMBONAT" 
* #100216920 "ROXADUSTAT"
* #100216920 ^designation[0].language = #de-AT 
* #100216920 ^designation[0].value = "ROXADUSTAT" 
* #10021858 "EISEN(III)OXIDHYDROXID-SACCHAROSE-STÄRKE-KOMPLEX"
* #10021858 ^designation[0].language = #de-AT 
* #10021858 ^designation[0].value = "EISEN(III)OXIDHYDROXID-SACCHAROSE-STÄRKE-KOMPLEX" 
* #100252126 "BIMEKIZUMAB"
* #100252126 ^designation[0].language = #de-AT 
* #100252126 ^designation[0].value = "BIMEKIZUMAB" 
* #100351149 "TAFASITAMAB"
* #100351149 ^designation[0].language = #de-AT 
* #100351149 ^designation[0].value = "TAFASITAMAB" 
* #100464429 "REGDANVIMAB"
* #100464429 ^designation[0].language = #de-AT 
* #100464429 ^designation[0].value = "REGDANVIMAB" 
* #100542802 "ZANUBRUTINIB"
* #100542802 ^designation[0].language = #de-AT 
* #100542802 ^designation[0].value = "ZANUBRUTINIB" 
* #100542810 "PRALSETINIB"
* #100542810 ^designation[0].language = #de-AT 
* #100542810 ^designation[0].value = "PRALSETINIB" 
* #100542872 "DIROXIMELFUMARAT"
* #100542872 ^designation[0].language = #de-AT 
* #100542872 ^designation[0].value = "DIROXIMELFUMARAT" 
* #100593880 "SELINEXOR"
* #100593880 ^designation[0].language = #de-AT 
* #100593880 ^designation[0].value = "SELINEXOR" 
* #100631050 "AMIVANTAMAB"
* #100631050 ^designation[0].language = #de-AT 
* #100631050 ^designation[0].value = "AMIVANTAMAB" 
* #100686793 "NIRMATRELVIR"
* #100686793 ^designation[0].language = #de-AT 
* #100686793 ^designation[0].value = "NIRMATRELVIR" 
* #100738780 "SOTORASIB"
* #100738780 ^designation[0].language = #de-AT 
* #100738780 ^designation[0].value = "SOTORASIB" 
* #100738885 "RIPRETINIB"
* #100738885 ^designation[0].language = #de-AT 
* #100738885 ^designation[0].value = "RIPRETINIB" 
* #100738887 "AVACOPAN"
* #100738887 ^designation[0].language = #de-AT 
* #100738887 ^designation[0].value = "AVACOPAN" 
* #100738889 "PEGCETACOPLAN"
* #100738889 ^designation[0].language = #de-AT 
* #100738889 ^designation[0].value = "PEGCETACOPLAN" 
* #100809553 "FINERENON"
* #100809553 ^designation[0].language = #de-AT 
* #100809553 ^designation[0].value = "FINERENON" 
* #100809559 "ANIFROLUMAB"
* #100809559 ^designation[0].language = #de-AT 
* #100809559 ^designation[0].value = "ANIFROLUMAB" 
* #100842437 "ICOSAPENT-ETHYL"
* #100842437 ^designation[0].language = #de-AT 
* #100842437 ^designation[0].value = "ICOSAPENT-ETHYL" 
* #100874933 "SOMATROGON"
* #100874933 ^designation[0].language = #de-AT 
* #100874933 ^designation[0].value = "SOMATROGON" 
* #100945371 "ELASOMERAN"
* #100945371 ^designation[0].language = #de-AT 
* #100945371 ^designation[0].value = "ELASOMERAN" 
* #100990845 "EPTINEZUMAB"
* #100990845 ^designation[0].language = #de-AT 
* #100990845 ^designation[0].value = "EPTINEZUMAB" 
* #10099784 "LENALIDOMIDHYDROCHLORID MONOHYDRAT"
* #10099784 ^designation[0].language = #de-AT 
* #10099784 ^designation[0].value = "LENALIDOMIDHYDROCHLORID MONOHYDRAT" 
* #10099945 "CERLIPONASE ALFA"
* #10099945 ^designation[0].language = #de-AT 
* #10099945 ^designation[0].value = "CERLIPONASE ALFA" 
* #10099952 "VON WILLEBRAND FAKTOR, REKOMBINANT, HUMAN"
* #10099952 ^designation[0].language = #de-AT 
* #10099952 ^designation[0].value = "VON WILLEBRAND FAKTOR, REKOMBINANT, HUMAN" 
* #101100117 "OLIPUDASE ALFA"
* #101100117 ^designation[0].language = #de-AT 
* #101100117 ^designation[0].value = "OLIPUDASE ALFA" 
* #101102201 "TOZINAMERAN"
* #101102201 ^designation[0].language = #de-AT 
* #101102201 ^designation[0].value = "TOZINAMERAN" 
* #101143732 "MOSUNETUZUMAB"
* #101143732 ^designation[0].language = #de-AT 
* #101143732 ^designation[0].value = "MOSUNETUZUMAB" 
* #101143735 "CAPMATINIB DIHYDROCHLORID MONOHYDRAT"
* #101143735 ^designation[0].language = #de-AT 
* #101143735 ^designation[0].value = "CAPMATINIB DIHYDROCHLORID MONOHYDRAT" 
* #101223138 "DIFELIKEFALINACETAT"
* #101223138 ^designation[0].language = #de-AT 
* #101223138 ^designation[0].value = "DIFELIKEFALINACETAT" 
* #101223166 "INEBILIZUMAB"
* #101223166 ^designation[0].language = #de-AT 
* #101223166 ^designation[0].value = "INEBILIZUMAB" 
* #101223168 "RIMEGEPANT HEMISULFAT SESQUIHYDRAT"
* #101223168 ^designation[0].language = #de-AT 
* #101223168 ^designation[0].value = "RIMEGEPANT HEMISULFAT SESQUIHYDRAT" 
* #101223188 "MOLNUPIRAVIR"
* #101223188 ^designation[0].language = #de-AT 
* #101223188 ^designation[0].value = "MOLNUPIRAVIR" 
* #101271282 "IMELASOMERAN"
* #101271282 ^designation[0].language = #de-AT 
* #101271282 ^designation[0].value = "IMELASOMERAN" 
* #101271284 "RILTOZINAMERAN"
* #101271284 ^designation[0].language = #de-AT 
* #101271284 ^designation[0].value = "RILTOZINAMERAN" 
* #101271286 "FAMTOZINAMERAN"
* #101271286 ^designation[0].language = #de-AT 
* #101271286 ^designation[0].value = "FAMTOZINAMERAN" 
* #101296343 "LASMIDITAN HEMISUCCINAT"
* #101296343 ^designation[0].language = #de-AT 
* #101296343 ^designation[0].value = "LASMIDITAN HEMISUCCINAT" 
* #101296487 "TECLISTAMAB"
* #101296487 ^designation[0].language = #de-AT 
* #101296487 ^designation[0].value = "TECLISTAMAB" 
* #10156351 "INOTUZUMAB OZOGAMICIN"
* #10156351 ^designation[0].language = #de-AT 
* #10156351 ^designation[0].value = "INOTUZUMAB OZOGAMICIN" 
* #10156525 "SARILUMAB"
* #10156525 ^designation[0].language = #de-AT 
* #10156525 ^designation[0].value = "SARILUMAB" 
* #10233365 "PATIROMER SORBITEX KALZIUM"
* #10233365 ^designation[0].language = #de-AT 
* #10233365 ^designation[0].value = "PATIROMER SORBITEX KALZIUM" 
* #10233388 "VOXILAPREVIR"
* #10233388 ^designation[0].language = #de-AT 
* #10233388 ^designation[0].value = "VOXILAPREVIR" 
* #10233390 "GLECAPREVIR"
* #10233390 ^designation[0].language = #de-AT 
* #10233390 ^designation[0].value = "GLECAPREVIR" 
* #10233393 "PIBRENTASVIR"
* #10233393 ^designation[0].language = #de-AT 
* #10233393 ^designation[0].value = "PIBRENTASVIR" 
* #10233395 "BRODALUMAB"
* #10233395 ^designation[0].language = #de-AT 
* #10233395 ^designation[0].value = "BRODALUMAB" 
* #10242401 "CALCIUM MUPIROCINAT DIHYDRAT"
* #10242401 ^designation[0].language = #de-AT 
* #10242401 ^designation[0].value = "CALCIUM MUPIROCINAT DIHYDRAT" 
* #10242403 "TERAZOSINHYDROCHLORID DIHYDRAT"
* #10242403 ^designation[0].language = #de-AT 
* #10242403 ^designation[0].value = "TERAZOSINHYDROCHLORID DIHYDRAT" 
* #10287719 "TACALCITOL MONOHYDRAT"
* #10287719 ^designation[0].language = #de-AT 
* #10287719 ^designation[0].value = "TACALCITOL MONOHYDRAT" 
* #10287724 "APIS MELLIFERA (ALLERG.)"
* #10287724 ^designation[0].language = #de-AT 
* #10287724 ^designation[0].value = "APIS MELLIFERA (ALLERG.)" 
* #10287834 "VESPULA SSP. (ALLERG.)"
* #10287834 ^designation[0].language = #de-AT 
* #10287834 ^designation[0].value = "VESPULA SSP. (ALLERG.)" 
* #10305146 "RIBOCICLIB SUCCINAT"
* #10305146 ^designation[0].language = #de-AT 
* #10305146 ^designation[0].value = "RIBOCICLIB SUCCINAT" 
* #10305148 "CALCIPOTRIOL, WASSERFREI"
* #10305148 ^designation[0].language = #de-AT 
* #10305148 ^designation[0].value = "CALCIPOTRIOL, WASSERFREI" 
* #10376708 "AVELUMAB"
* #10376708 ^designation[0].language = #de-AT 
* #10376708 ^designation[0].value = "AVELUMAB" 
* #10376710 "MIDOSTAURIN"
* #10376710 ^designation[0].language = #de-AT 
* #10376710 ^designation[0].value = "MIDOSTAURIN" 
* #10376715 "ATEZOLIZUMAB"
* #10376715 ^designation[0].language = #de-AT 
* #10376715 ^designation[0].value = "ATEZOLIZUMAB" 
* #10376717 "TELOTRISTATETIPRAT"
* #10376717 ^designation[0].language = #de-AT 
* #10376717 ^designation[0].value = "TELOTRISTATETIPRAT" 
* #10388691 "TENOFOVIR DISOPROXIL MALEAT"
* #10388691 ^designation[0].language = #de-AT 
* #10388691 ^designation[0].value = "TENOFOVIR DISOPROXIL MALEAT" 
* #10388707 "DUPILUMAB"
* #10388707 ^designation[0].language = #de-AT 
* #10388707 ^designation[0].value = "DUPILUMAB" 
* #10443255 "COFFEIN, WASSERFREI"
* #10443255 ^designation[0].language = #de-AT 
* #10443255 ^designation[0].value = "COFFEIN, WASSERFREI" 
* #10443287 "CARIPRAZINHYDROCHLORID"
* #10443287 ^designation[0].language = #de-AT 
* #10443287 ^designation[0].value = "CARIPRAZINHYDROCHLORID" 
* #10445194 "LUTETIUM [*177*LU] OXODOTREOTID"
* #10445194 ^designation[0].language = #de-AT 
* #10445194 ^designation[0].value = "LUTETIUM [*177*LU] OXODOTREOTID" 
* #10445431 "SPHÄROIDE AUS HUMANEN AUTOLOGEN MATRIX-ASSOZIERTEN CHONDROZYTEN"
* #10445431 ^designation[0].language = #de-AT 
* #10445431 ^designation[0].value = "SPHÄROIDE AUS HUMANEN AUTOLOGEN MATRIX-ASSOZIERTEN CHONDROZYTEN" 
* #10503892 "MELPHALANHYDROCHLORID"
* #10503892 ^designation[0].language = #de-AT 
* #10503892 ^designation[0].value = "MELPHALANHYDROCHLORID" 
* #10535045 "NIRAPARIB TOSYLAT MONOHYDRAT"
* #10535045 ^designation[0].language = #de-AT 
* #10535045 ^designation[0].value = "NIRAPARIB TOSYLAT MONOHYDRAT" 
* #10535057 "GUSELKUMAB"
* #10535057 ^designation[0].language = #de-AT 
* #10535057 ^designation[0].value = "GUSELKUMAB" 
* #10585224 "STREPTOCOCCUS SSP."
* #10585224 ^designation[0].language = #de-AT 
* #10585224 ^designation[0].value = "STREPTOCOCCUS SSP." 
* #10610600 "LETERMOVIR"
* #10610600 ^designation[0].language = #de-AT 
* #10610600 ^designation[0].value = "LETERMOVIR" 
* #10610605 "BENRALIZUMAB"
* #10610605 ^designation[0].language = #de-AT 
* #10610605 ^designation[0].value = "BENRALIZUMAB" 
* #10610609 "OCRELIZUMAB"
* #10610609 ^designation[0].language = #de-AT 
* #10610609 ^designation[0].value = "OCRELIZUMAB" 
* #10730016 "TRIENTIN DIHYDROCHLORID"
* #10730016 ^designation[0].language = #de-AT 
* #10730016 ^designation[0].value = "TRIENTIN DIHYDROCHLORID" 
* #10752518 "BUROSUMAB"
* #10752518 ^designation[0].language = #de-AT 
* #10752518 ^designation[0].value = "BUROSUMAB" 
* #10752794 "EMICIZUMAB"
* #10752794 ^designation[0].language = #de-AT 
* #10752794 ^designation[0].value = "EMICIZUMAB" 
* #10752829 "SEMAGLUTID"
* #10752829 ^designation[0].language = #de-AT 
* #10752829 ^designation[0].value = "SEMAGLUTID" 
* #10792699 "NATRIUMZIRCONIUMHYDROGENCYCLOHEXASILICAT HYDRAT (3:2:1:1:x)"
* #10792699 ^designation[0].language = #de-AT 
* #10792699 ^designation[0].value = "NATRIUMZIRCONIUMHYDROGENCYCLOHEXASILICAT HYDRAT (3:2:1:1:x)" 
* #10792942 "ERTUGLIFLOZIN L-PYROGLUTAMAT"
* #10792942 ^designation[0].language = #de-AT 
* #10792942 ^designation[0].value = "ERTUGLIFLOZIN L-PYROGLUTAMAT" 
* #10792954 "VARICELLA ZOSTERVIRUS (AUSZUG, PRODUKTE)"
* #10792954 ^designation[0].language = #de-AT 
* #10792954 ^designation[0].value = "VARICELLA ZOSTERVIRUS (AUSZUG, PRODUKTE)" 
* #10829696 "STAMMZELLEN"
* #10829696 ^designation[0].language = #de-AT 
* #10829696 ^designation[0].value = "STAMMZELLEN" 
* #10880107 "MERCAPTOPURIN MONOHYDRAT"
* #10880107 ^designation[0].language = #de-AT 
* #10880107 ^designation[0].value = "MERCAPTOPURIN MONOHYDRAT" 
* #10880109 "CIPROFLOXACINHYDROCHLORID HYDRAT"
* #10880109 ^designation[0].language = #de-AT 
* #10880109 ^designation[0].value = "CIPROFLOXACINHYDROCHLORID HYDRAT" 
* #10880111 "VACCINIUM MACROCARPON AIT. FRUCTUS (AUSZUG)"
* #10880111 ^designation[0].language = #de-AT 
* #10880111 ^designation[0].value = "VACCINIUM MACROCARPON AIT. FRUCTUS (AUSZUG)" 
* #10907372 "URTICAE FOLIUM (AUSZUG)"
* #10907372 ^designation[0].language = #de-AT 
* #10907372 ^designation[0].value = "URTICAE FOLIUM (AUSZUG)" 
* #10907392 "AVATROMBOPAG MALEAT"
* #10907392 ^designation[0].language = #de-AT 
* #10907392 ^designation[0].value = "AVATROMBOPAG MALEAT" 
* #10948232 "DRONEDARON HYDROCHLORID"
* #10948232 ^designation[0].language = #de-AT 
* #10948232 ^designation[0].value = "DRONEDARON HYDROCHLORID" 
* #10948281 "GEMTUZUMAB OZOGAMICIN"
* #10948281 ^designation[0].language = #de-AT 
* #10948281 ^designation[0].value = "GEMTUZUMAB OZOGAMICIN" 
* #11181545 "RUTOSID TRIHYDRAT"
* #11181545 ^designation[0].language = #de-AT 
* #11181545 ^designation[0].value = "RUTOSID TRIHYDRAT" 
* #11181870 "BICTEGRAVIR NATRIUM"
* #11181870 ^designation[0].language = #de-AT 
* #11181870 ^designation[0].value = "BICTEGRAVIR NATRIUM" 
* #11188853 "DARUNAVIR PROPYLENGLYCOL"
* #11188853 ^designation[0].language = #de-AT 
* #11188853 ^designation[0].value = "DARUNAVIR PROPYLENGLYCOL" 
* #11198826 "ROMOSOZUMAB"
* #11198826 ^designation[0].language = #de-AT 
* #11198826 ^designation[0].value = "ROMOSOZUMAB" 
* #11198853 "INOTERSEN NATRIUM"
* #11198853 ^designation[0].language = #de-AT 
* #11198853 ^designation[0].value = "INOTERSEN NATRIUM" 
* #11220920 "GLUCAGON"
* #11220920 ^designation[0].language = #de-AT 
* #11220920 ^designation[0].value = "GLUCAGON" 
* #11222139 "MAGNESIUMSULFAT DIHYDRAT"
* #11222139 ^designation[0].language = #de-AT 
* #11222139 ^designation[0].value = "MAGNESIUMSULFAT DIHYDRAT" 
* #11264544 "ESZOPICLON"
* #11264544 ^designation[0].language = #de-AT 
* #11264544 ^designation[0].value = "ESZOPICLON" 
* #11277999 "ERENUMAB"
* #11277999 ^designation[0].language = #de-AT 
* #11277999 ^designation[0].value = "ERENUMAB" 
* #11362085 "TREOSULFAN"
* #11362085 ^designation[0].language = #de-AT 
* #11362085 ^designation[0].value = "TREOSULFAN" 
* #11453150 "TILDRAKIZUMAB"
* #11453150 ^designation[0].language = #de-AT 
* #11453150 ^designation[0].value = "TILDRAKIZUMAB" 
* #11453314 "ABEMACICLIB"
* #11453314 ^designation[0].language = #de-AT 
* #11453314 ^designation[0].value = "ABEMACICLIB" 
* #11453316 "PATISIRAN NATRIUM"
* #11453316 ^designation[0].language = #de-AT 
* #11453316 ^designation[0].value = "PATISIRAN NATRIUM" 
* #11453321 "BINIMETINIB"
* #11453321 ^designation[0].language = #de-AT 
* #11453321 ^designation[0].value = "BINIMETINIB" 
* #11453326 "DURVALUMAB"
* #11453326 ^designation[0].language = #de-AT 
* #11453326 ^designation[0].value = "DURVALUMAB" 
* #11453385 "ENCORAFENIB"
* #11453385 ^designation[0].language = #de-AT 
* #11453385 ^designation[0].value = "ENCORAFENIB" 
* #11453389 "MICAFUNGIN NATRIUM"
* #11453389 ^designation[0].language = #de-AT 
* #11453389 ^designation[0].value = "MICAFUNGIN NATRIUM" 
* #11535815 "TEZACAFTOR"
* #11535815 ^designation[0].language = #de-AT 
* #11535815 ^designation[0].value = "TEZACAFTOR" 
* #11619804 "BRIGATINIB"
* #11619804 ^designation[0].language = #de-AT 
* #11619804 ^designation[0].value = "BRIGATINIB" 
* #11619809 "MOGAMULIZUMAB"
* #11619809 ^designation[0].language = #de-AT 
* #11619809 ^designation[0].value = "MOGAMULIZUMAB" 
* #11619814 "LANADELUMAB"
* #11619814 ^designation[0].language = #de-AT 
* #11619814 ^designation[0].value = "LANADELUMAB" 
* #11619816 "DORAVIRIN"
* #11619816 ^designation[0].language = #de-AT 
* #11619816 ^designation[0].value = "DORAVIRIN" 
* #11619818 "VORETIGEN NEPARVOVEC"
* #11619818 ^designation[0].language = #de-AT 
* #11619818 ^designation[0].value = "VORETIGEN NEPARVOVEC" 
* #11619820 "VABORBACTAM"
* #11619820 ^designation[0].language = #de-AT 
* #11619820 ^designation[0].value = "VABORBACTAM" 
* #11619822 "GALCANEZUMAB"
* #11619822 ^designation[0].language = #de-AT 
* #11619822 ^designation[0].value = "GALCANEZUMAB" 
* #11619824 "ERAVACYCLIN DIHYDROCHLORID"
* #11619824 ^designation[0].language = #de-AT 
* #11619824 ^designation[0].value = "ERAVACYCLIN DIHYDROCHLORID" 
* #11619830 "NERATINITIBMALEAT"
* #11619830 ^designation[0].language = #de-AT 
* #11619830 ^designation[0].value = "NERATINITIBMALEAT" 
* #11619832 "METRELEPTIN"
* #11619832 ^designation[0].language = #de-AT 
* #11619832 ^designation[0].value = "METRELEPTIN" 
* #11791791 "APALUTAMID"
* #11791791 ^designation[0].language = #de-AT 
* #11791791 ^designation[0].value = "APALUTAMID" 
* #11869766 "TREPROSTINIL NATRIUM"
* #11869766 ^designation[0].language = #de-AT 
* #11869766 ^designation[0].value = "TREPROSTINIL NATRIUM" 
* #11887642 "DAROLUTAMID"
* #11887642 ^designation[0].language = #de-AT 
* #11887642 ^designation[0].value = "DAROLUTAMID" 
* #11955748 "FREMANEZUMAB"
* #11955748 ^designation[0].language = #de-AT 
* #11955748 ^designation[0].value = "FREMANEZUMAB" 
* #12060542 "LUSPATERCEPT"
* #12060542 ^designation[0].language = #de-AT 
* #12060542 ^designation[0].value = "LUSPATERCEPT" 
* #12060556 "GLASDEGIBMALEAT"
* #12060556 ^designation[0].language = #de-AT 
* #12060556 ^designation[0].value = "GLASDEGIBMALEAT" 
* #12060572 "INDACATEROLACETAT"
* #12060572 ^designation[0].language = #de-AT 
* #12060572 ^designation[0].value = "INDACATEROLACETAT" 
* #12060584 "VOLANESORSEN NATRIUM"
* #12060584 ^designation[0].language = #de-AT 
* #12060584 ^designation[0].value = "VOLANESORSEN NATRIUM" 
* #12060616 "DACOMITINIB MONOHYDRAT"
* #12060616 ^designation[0].language = #de-AT 
* #12060616 ^designation[0].value = "DACOMITINIB MONOHYDRAT" 
* #12060633 "RISANKIZUMAB"
* #12060633 ^designation[0].language = #de-AT 
* #12060633 ^designation[0].value = "RISANKIZUMAB" 
* #12060635 "LORLATINIB"
* #12060635 ^designation[0].language = #de-AT 
* #12060635 ^designation[0].value = "LORLATINIB" 
* #12094907 "PEGVALIASE"
* #12094907 ^designation[0].language = #de-AT 
* #12094907 ^designation[0].value = "PEGVALIASE" 
* #12094909 "ANDEXANET ALFA"
* #12094909 ^designation[0].language = #de-AT 
* #12094909 ^designation[0].value = "ANDEXANET ALFA" 
* #12129927 "CRIZANLIZUMAB"
* #12129927 ^designation[0].language = #de-AT 
* #12129927 ^designation[0].value = "CRIZANLIZUMAB" 
* #12197686 "ATROPINSULFAT MONOHYDRAT"
* #12197686 ^designation[0].language = #de-AT 
* #12197686 ^designation[0].value = "ATROPINSULFAT MONOHYDRAT" 
* #12218241 "SITAGLIPTIN HYDROCHLORID MONOHYDRAT"
* #12218241 ^designation[0].language = #de-AT 
* #12218241 ^designation[0].value = "SITAGLIPTIN HYDROCHLORID MONOHYDRAT" 
* #12218245 "RAVULIZUMAB"
* #12218245 ^designation[0].language = #de-AT 
* #12218245 ^designation[0].value = "RAVULIZUMAB" 
* #12233314 "PALIPERIDON PALMITAT"
* #12233314 ^designation[0].language = #de-AT 
* #12233314 ^designation[0].value = "PALIPERIDON PALMITAT" 
* #12272682 "CEMIPLIMAB"
* #12272682 ^designation[0].language = #de-AT 
* #12272682 ^designation[0].value = "CEMIPLIMAB" 
* #12289391 "TALAZOPARIBTOSILAT"
* #12289391 ^designation[0].language = #de-AT 
* #12289391 ^designation[0].value = "TALAZOPARIBTOSILAT" 
* #12376057 "SATRALIZUMAB"
* #12376057 ^designation[0].language = #de-AT 
* #12376057 ^designation[0].value = "SATRALIZUMAB" 
* #12469204 "LAROTRECTINIBSULFAT"
* #12469204 ^designation[0].language = #de-AT 
* #12469204 ^designation[0].value = "LAROTRECTINIBSULFAT" 
* #12469230 "TROPAEOLI HERBA"
* #12469230 ^designation[0].language = #de-AT 
* #12469230 ^designation[0].value = "TROPAEOLI HERBA" 
* #12469386 "ARMORACIAE RADIX"
* #12469386 ^designation[0].language = #de-AT 
* #12469386 ^designation[0].value = "ARMORACIAE RADIX" 
* #12537848 "GILTERITINIB FUMARAT"
* #12537848 ^designation[0].language = #de-AT 
* #12537848 ^designation[0].value = "GILTERITINIB FUMARAT" 
* #12565908 "BALOXAVIR MARBOXIL"
* #12565908 ^designation[0].language = #de-AT 
* #12565908 ^designation[0].value = "BALOXAVIR MARBOXIL" 
* #12634075 "CANNABIDIOL"
* #12634075 ^designation[0].language = #de-AT 
* #12634075 ^designation[0].value = "CANNABIDIOL" 
* #12634087 "PEMIGATINIB"
* #12634087 ^designation[0].language = #de-AT 
* #12634087 ^designation[0].value = "PEMIGATINIB" 
* #12711169 "UPADACITINIB HEMIHYDRAT"
* #12711169 ^designation[0].language = #de-AT 
* #12711169 ^designation[0].value = "UPADACITINIB HEMIHYDRAT" 
* #12779008 "IBALIZUMAB"
* #12779008 ^designation[0].language = #de-AT 
* #12779008 ^designation[0].value = "IBALIZUMAB" 
* #12779012 "DELAFLOXACIN MEGLUMIN"
* #12779012 ^designation[0].language = #de-AT 
* #12779012 ^designation[0].value = "DELAFLOXACIN MEGLUMIN" 
* #12779016 "FEDRATINIB DIHYDROCHLORID MONOHYDRAT"
* #12779016 ^designation[0].language = #de-AT 
* #12779016 ^designation[0].value = "FEDRATINIB DIHYDROCHLORID MONOHYDRAT" 
* #12792136 "SIPONIMOD HEMIFUMARAT"
* #12792136 ^designation[0].language = #de-AT 
* #12792136 ^designation[0].value = "SIPONIMOD HEMIFUMARAT" 
* #12792141 "POLATUZUMAB VEDOTIN"
* #12792141 ^designation[0].language = #de-AT 
* #12792141 ^designation[0].value = "POLATUZUMAB VEDOTIN" 
* #12842358 "ESTETROL MONOHYDRAT"
* #12842358 ^designation[0].language = #de-AT 
* #12842358 ^designation[0].value = "ESTETROL MONOHYDRAT" 
* #12862163 "TAFAMIDIS"
* #12862163 ^designation[0].language = #de-AT 
* #12862163 ^designation[0].value = "TAFAMIDIS" 
* #12862188 "BROLUCIZUMAB"
* #12862188 ^designation[0].language = #de-AT 
* #12862188 ^designation[0].value = "BROLUCIZUMAB" 
* #12889891 "PONESIMOD"
* #12889891 ^designation[0].language = #de-AT 
* #12889891 ^designation[0].value = "PONESIMOD" 
* #12889929 "GIVOSIRAN NATRIUM"
* #12889929 ^designation[0].language = #de-AT 
* #12889929 ^designation[0].value = "GIVOSIRAN NATRIUM" 
* #12889937 "SOLRIAMFETOL HYDROCHLORID"
* #12889937 ^designation[0].language = #de-AT 
* #12889937 ^designation[0].value = "SOLRIAMFETOL HYDROCHLORID" 
* #12889941 "OSILODROSTATPHOSPHAT"
* #12889941 ^designation[0].language = #de-AT 
* #12889941 ^designation[0].value = "OSILODROSTATPHOSPHAT" 
* #12889945 "ANGIOTENSIN-II-ACETAT"
* #12889945 ^designation[0].language = #de-AT 
* #12889945 ^designation[0].value = "ANGIOTENSIN-II-ACETAT" 
* #13098466 "BEMPEDOSÄURE"
* #13098466 ^designation[0].language = #de-AT 
* #13098466 ^designation[0].value = "BEMPEDOSÄURE" 
* #13220199 "OZANIMODHYDROCHLORID"
* #13220199 ^designation[0].language = #de-AT 
* #13220199 ^designation[0].value = "OZANIMODHYDROCHLORID" 
* #13220208 "ISATUXIMAB"
* #13220208 ^designation[0].language = #de-AT 
* #13220208 ^designation[0].value = "ISATUXIMAB" 
* #13368382 "RISDIPLAM"
* #13368382 ^designation[0].language = #de-AT 
* #13368382 ^designation[0].value = "RISDIPLAM" 
* #13368437 "VOSORITID"
* #13368437 ^designation[0].language = #de-AT 
* #13368437 ^designation[0].value = "VOSORITID" 
* #13368446 "ENTRECTINIB"
* #13368446 ^designation[0].language = #de-AT 
* #13368446 ^designation[0].value = "ENTRECTINIB" 
* #13368450 "ALPELISIB"
* #13368450 ^designation[0].language = #de-AT 
* #13368450 ^designation[0].value = "ALPELISIB" 
* #13368452 "BULEVIRTIDACETAT"
* #13368452 ^designation[0].language = #de-AT 
* #13368452 ^designation[0].value = "BULEVIRTIDACETAT" 
* #13437266 "ABROCITINIB"
* #13437266 ^designation[0].language = #de-AT 
* #13437266 ^designation[0].value = "ABROCITINIB" 
* #13456182 "ELEXACAFTOR"
* #13456182 ^designation[0].language = #de-AT 
* #13456182 ^designation[0].value = "ELEXACAFTOR" 
* #13502620 "AVALGLUCOSIDASE ALFA"
* #13502620 ^designation[0].language = #de-AT 
* #13502620 ^designation[0].value = "AVALGLUCOSIDASE ALFA" 
* #13502645 "LENALIDOMID AMMONIUMCHLORID"
* #13502645 ^designation[0].language = #de-AT 
* #13502645 ^designation[0].value = "LENALIDOMID AMMONIUMCHLORID" 
* #13573644 "FILGOTINIBMALEAT"
* #13573644 ^designation[0].language = #de-AT 
* #13573644 ^designation[0].value = "FILGOTINIBMALEAT" 
* #13676715 "TEPOTINIBHYDROCHLORID MONOHYDRAT"
* #13676715 ^designation[0].language = #de-AT 
* #13676715 ^designation[0].value = "TEPOTINIBHYDROCHLORID MONOHYDRAT" 
* #13676767 "ACALABRUTINIB"
* #13676767 ^designation[0].language = #de-AT 
* #13676767 ^designation[0].value = "ACALABRUTINIB" 
* #13759187 "AVAPRITINIB"
* #13759187 ^designation[0].language = #de-AT 
* #13759187 ^designation[0].value = "AVAPRITINIB" 
* #13759206 "LUMASIRAN-NATRIUM"
* #13759206 ^designation[0].language = #de-AT 
* #13759206 ^designation[0].value = "LUMASIRAN-NATRIUM" 
* #13759378 "BELANTAMAB MAFODOTIN"
* #13759378 ^designation[0].language = #de-AT 
* #13759378 ^designation[0].value = "BELANTAMAB MAFODOTIN" 
* #13759403 "CORONAVIRUS (AUSZUG, PRODUKTE)"
* #13759403 ^designation[0].language = #de-AT 
* #13759403 ^designation[0].value = "CORONAVIRUS (AUSZUG, PRODUKTE)" 
* #13870957 "INCLISIRAN-NATRIUM"
* #13870957 ^designation[0].language = #de-AT 
* #13870957 ^designation[0].value = "INCLISIRAN-NATRIUM" 
* #13871165 "PAROXETINHYDROCHLORID HEMIHYDRAT"
* #13871165 ^designation[0].language = #de-AT 
* #13871165 ^designation[0].value = "PAROXETINHYDROCHLORID HEMIHYDRAT" 
* #13966372 "CABOTEGRAVIR"
* #13966372 ^designation[0].language = #de-AT 
* #13966372 ^designation[0].value = "CABOTEGRAVIR" 
* #13966376 "CABOTEGRAVIR NATRIUM"
* #13966376 ^designation[0].language = #de-AT 
* #13966376 ^designation[0].value = "CABOTEGRAVIR NATRIUM" 
* #13966535 "FOSTEMSAVIR TROMETAMOL"
* #13966535 ^designation[0].language = #de-AT 
* #13966535 ^designation[0].value = "FOSTEMSAVIR TROMETAMOL" 
* #13966549 "TRASTUZUMAB DERUXTECAN"
* #13966549 ^designation[0].language = #de-AT 
* #13966549 ^designation[0].value = "TRASTUZUMAB DERUXTECAN" 
* #14046746 "SELPERCATINIB"
* #14046746 ^designation[0].language = #de-AT 
* #14046746 ^designation[0].value = "SELPERCATINIB" 
* #14046850 "TUCATINIB HEMIETHANOL"
* #14046850 ^designation[0].language = #de-AT 
* #14046850 ^designation[0].value = "TUCATINIB HEMIETHANOL" 
* #14156682 "CENOBAMAT"
* #14156682 ^designation[0].language = #de-AT 
* #14156682 ^designation[0].value = "CENOBAMAT" 
* #14307991 "DOSTARLIMAB"
* #14307991 ^designation[0].language = #de-AT 
* #14307991 ^designation[0].value = "DOSTARLIMAB" 
* #14317539 "BEROTRALSTATDIHYDROCHLORID"
* #14317539 ^designation[0].language = #de-AT 
* #14317539 ^designation[0].value = "BEROTRALSTATDIHYDROCHLORID" 
* #14317628 "TAGRAXOFUSP"
* #14317628 ^designation[0].language = #de-AT 
* #14317628 ^designation[0].value = "TAGRAXOFUSP" 
* #1705366 "D-CAMPHER"
* #1705366 ^designation[0].language = #de-AT 
* #1705366 ^designation[0].value = "D-CAMPHER" 
* #1705374 "DOXYLAMIN HYDROGENSUCCINAT"
* #1705374 ^designation[0].language = #de-AT 
* #1705374 ^designation[0].value = "DOXYLAMIN HYDROGENSUCCINAT" 
* #1705376 "SAMBUCI FLOS"
* #1705376 ^designation[0].language = #de-AT 
* #1705376 ^designation[0].value = "SAMBUCI FLOS" 
* #1705378 "EQUISETI HERBA"
* #1705378 ^designation[0].language = #de-AT 
* #1705378 ^designation[0].value = "EQUISETI HERBA" 
* #1705383 "GELATINE"
* #1705383 ^designation[0].language = #de-AT 
* #1705383 ^designation[0].value = "GELATINE" 
* #1705405 "MORPHINSULFAT"
* #1705405 ^designation[0].language = #de-AT 
* #1705405 ^designation[0].value = "MORPHINSULFAT" 
* #1705406 "MUCOPOLYSACCHARIDPOLYSCHWEFELSÄUREESTER"
* #1705406 ^designation[0].language = #de-AT 
* #1705406 ^designation[0].value = "MUCOPOLYSACCHARIDPOLYSCHWEFELSÄUREESTER" 
* #1705409 "MYRRHA"
* #1705409 ^designation[0].language = #de-AT 
* #1705409 ^designation[0].value = "MYRRHA" 
* #1705415 "MYRISTICAE FRAGRANTIS AETHEROLEUM"
* #1705415 ^designation[0].language = #de-AT 
* #1705415 ^designation[0].value = "MYRISTICAE FRAGRANTIS AETHEROLEUM" 
* #1705420 "KOHLENDIOXID"
* #1705420 ^designation[0].language = #de-AT 
* #1705420 ^designation[0].value = "KOHLENDIOXID" 
* #1705433 "CENTAURII HERBA (AUSZUG)"
* #1705433 ^designation[0].language = #de-AT 
* #1705433 ^designation[0].value = "CENTAURII HERBA (AUSZUG)" 
* #1705439 "MATRICARIAE AETHEROLEUM"
* #1705439 ^designation[0].language = #de-AT 
* #1705439 ^designation[0].value = "MATRICARIAE AETHEROLEUM" 
* #1705445 "RUSCI RHIZOMA"
* #1705445 ^designation[0].language = #de-AT 
* #1705445 ^designation[0].value = "RUSCI RHIZOMA" 
* #1705461 "CINNAMOMI ZEYLANICI CORTICIS AETHEROLEUM"
* #1705461 ^designation[0].language = #de-AT 
* #1705461 ^designation[0].value = "CINNAMOMI ZEYLANICI CORTICIS AETHEROLEUM" 
* #1705464 "CENTAURII HERBA"
* #1705464 ^designation[0].language = #de-AT 
* #1705464 ^designation[0].value = "CENTAURII HERBA" 
* #1705480 "NEDOCROMIL DINATRIUM"
* #1705480 ^designation[0].language = #de-AT 
* #1705480 ^designation[0].value = "NEDOCROMIL DINATRIUM" 
* #1705481 "NEISSERIA MENINGITIDIS (AUSZUG, PRODUKTE)"
* #1705481 ^designation[0].language = #de-AT 
* #1705481 ^designation[0].value = "NEISSERIA MENINGITIDIS (AUSZUG, PRODUKTE)" 
* #1705485 "MANGANSULFAT"
* #1705485 ^designation[0].language = #de-AT 
* #1705485 ^designation[0].value = "MANGANSULFAT" 
* #1705486 "MANNA"
* #1705486 ^designation[0].language = #de-AT 
* #1705486 ^designation[0].value = "MANNA" 
* #1705487 "MANGANCHLORID"
* #1705487 ^designation[0].language = #de-AT 
* #1705487 ^designation[0].value = "MANGANCHLORID" 
* #1705498 "MEFLOQUIN HYDROCHLORID"
* #1705498 ^designation[0].language = #de-AT 
* #1705498 ^designation[0].value = "MEFLOQUIN HYDROCHLORID" 
* #1705500 "P-MENTHAN-3-ON"
* #1705500 ^designation[0].language = #de-AT 
* #1705500 ^designation[0].value = "P-MENTHAN-3-ON" 
* #1705507 "CALCIUM GLYCEROL PHOSPHAT"
* #1705507 ^designation[0].language = #de-AT 
* #1705507 ^designation[0].value = "CALCIUM GLYCEROL PHOSPHAT" 
* #1705513 "CALCIUMCITRAT"
* #1705513 ^designation[0].language = #de-AT 
* #1705513 ^designation[0].value = "CALCIUMCITRAT" 
* #1705521 "CALCIUM POLYSTYROLSULFONAT"
* #1705521 ^designation[0].language = #de-AT 
* #1705521 ^designation[0].value = "CALCIUM POLYSTYROLSULFONAT" 
* #1705525 "CAMPHEN"
* #1705525 ^designation[0].language = #de-AT 
* #1705525 ^designation[0].value = "CAMPHEN" 
* #1705538 "AGRIMONIAE HERBA"
* #1705538 ^designation[0].language = #de-AT 
* #1705538 ^designation[0].value = "AGRIMONIAE HERBA" 
* #1705541 "URAPIDIL"
* #1705541 ^designation[0].language = #de-AT 
* #1705541 ^designation[0].value = "URAPIDIL" 
* #1705547 "ARNICAE FLOS"
* #1705547 ^designation[0].language = #de-AT 
* #1705547 ^designation[0].value = "ARNICAE FLOS" 
* #1705551 "AMIKACINSULFAT"
* #1705551 ^designation[0].language = #de-AT 
* #1705551 ^designation[0].value = "AMIKACINSULFAT" 
* #1705552 "ANISI FRUCTUS"
* #1705552 ^designation[0].language = #de-AT 
* #1705552 ^designation[0].value = "ANISI FRUCTUS" 
* #1705556 "VERBENAE HERBA"
* #1705556 ^designation[0].language = #de-AT 
* #1705556 ^designation[0].value = "VERBENAE HERBA" 
* #1705561 "JUGLANDIS FOLIUM (AUSZUG)"
* #1705561 ^designation[0].language = #de-AT 
* #1705561 ^designation[0].value = "JUGLANDIS FOLIUM (AUSZUG)" 
* #1705571 "LOTEPREDNOL ETABONAT"
* #1705571 ^designation[0].language = #de-AT 
* #1705571 ^designation[0].value = "LOTEPREDNOL ETABONAT" 
* #1705578 "MAGNESIUM ASPARTAT HYDROCHLORID TRIHYDRAT"
* #1705578 ^designation[0].language = #de-AT 
* #1705578 ^designation[0].value = "MAGNESIUM ASPARTAT HYDROCHLORID TRIHYDRAT" 
* #1705579 "BASISCHES MAGNESIUMCARBONAT"
* #1705579 ^designation[0].language = #de-AT 
* #1705579 ^designation[0].value = "BASISCHES MAGNESIUMCARBONAT" 
* #1705582 "MAGNESIUMHYDROGENPHOSPHAT"
* #1705582 ^designation[0].language = #de-AT 
* #1705582 ^designation[0].value = "MAGNESIUMHYDROGENPHOSPHAT" 
* #1705585 "MAGNESIUMACETAT"
* #1705585 ^designation[0].language = #de-AT 
* #1705585 ^designation[0].value = "MAGNESIUMACETAT" 
* #1705587 "MAGNESIUM DIGLUCONAT"
* #1705587 ^designation[0].language = #de-AT 
* #1705587 ^designation[0].value = "MAGNESIUM DIGLUCONAT" 
* #1705588 "MAGNESIUM HYDROGENASPARTAT"
* #1705588 ^designation[0].language = #de-AT 
* #1705588 ^designation[0].value = "MAGNESIUM HYDROGENASPARTAT" 
* #1705590 "CAPSAICIN"
* #1705590 ^designation[0].language = #de-AT 
* #1705590 ^designation[0].value = "CAPSAICIN" 
* #1705594 "WISMUTSUBCITRAT"
* #1705594 ^designation[0].language = #de-AT 
* #1705594 ^designation[0].value = "WISMUTSUBCITRAT" 
* #1705596 "BETHANECHOLCHLORID"
* #1705596 ^designation[0].language = #de-AT 
* #1705596 ^designation[0].value = "BETHANECHOLCHLORID" 
* #1705597 "FLAVONOIDE"
* #1705597 ^designation[0].language = #de-AT 
* #1705597 ^designation[0].value = "FLAVONOIDE" 
* #1705603 "BORNEOL"
* #1705603 ^designation[0].language = #de-AT 
* #1705603 ^designation[0].value = "BORNEOL" 
* #1705613 "BENZALKONIUMCHLORID-LÖSUNG"
* #1705613 ^designation[0].language = #de-AT 
* #1705613 ^designation[0].value = "BENZALKONIUMCHLORID-LÖSUNG" 
* #1705615 "BETULAE FOLIUM"
* #1705615 ^designation[0].language = #de-AT 
* #1705615 ^designation[0].value = "BETULAE FOLIUM" 
* #1705619 "TRIFLURIDIN"
* #1705619 ^designation[0].language = #de-AT 
* #1705619 ^designation[0].value = "TRIFLURIDIN" 
* #1705642 "TOLTERODIN TARTRAT"
* #1705642 ^designation[0].language = #de-AT 
* #1705642 ^designation[0].value = "TOLTERODIN TARTRAT" 
* #1705647 "1-(P-TOLYL)ÄTHYL NICOTINAT"
* #1705647 ^designation[0].language = #de-AT 
* #1705647 ^designation[0].value = "1-(P-TOLYL)ÄTHYL NICOTINAT" 
* #1705651 "KLEBSIELLA"
* #1705651 ^designation[0].language = #de-AT 
* #1705651 ^designation[0].value = "KLEBSIELLA" 
* #1705652 "LACTOBACILLUS ACIDOPHILUS"
* #1705652 ^designation[0].language = #de-AT 
* #1705652 ^designation[0].value = "LACTOBACILLUS ACIDOPHILUS" 
* #1705655 "LACTOBACILLUS HELVETICUS (AUSZUG, PRODUKTE)"
* #1705655 ^designation[0].language = #de-AT 
* #1705655 ^designation[0].value = "LACTOBACILLUS HELVETICUS (AUSZUG, PRODUKTE)" 
* #1705657 "PROTEINE"
* #1705657 ^designation[0].language = #de-AT 
* #1705657 ^designation[0].value = "PROTEINE" 
* #1705659 "LECITHIN"
* #1705659 ^designation[0].language = #de-AT 
* #1705659 ^designation[0].value = "LECITHIN" 
* #1705660 "LEVOBUNOLOL HYDROCHLORID"
* #1705660 ^designation[0].language = #de-AT 
* #1705660 ^designation[0].value = "LEVOBUNOLOL HYDROCHLORID" 
* #1705661 "LICHEN ISLANDICUS"
* #1705661 ^designation[0].language = #de-AT 
* #1705661 ^designation[0].value = "LICHEN ISLANDICUS" 
* #1705687 "TIOCONAZOL"
* #1705687 ^designation[0].language = #de-AT 
* #1705687 ^designation[0].value = "TIOCONAZOL" 
* #1705688 "TIOGUANIN"
* #1705688 ^designation[0].language = #de-AT 
* #1705688 ^designation[0].value = "TIOGUANIN" 
* #1705692 "TIOTROPIUMBROMID"
* #1705692 ^designation[0].language = #de-AT 
* #1705692 ^designation[0].value = "TIOTROPIUMBROMID" 
* #1705702 "TRIGLYCERIDE"
* #1705702 ^designation[0].language = #de-AT 
* #1705702 ^designation[0].value = "TRIGLYCERIDE" 
* #1705711 "EISEN(III)-ISOMALTOSEKOMPLEX"
* #1705711 ^designation[0].language = #de-AT 
* #1705711 ^designation[0].value = "EISEN(III)-ISOMALTOSEKOMPLEX" 
* #1705718 "ANETHOL"
* #1705718 ^designation[0].language = #de-AT 
* #1705718 ^designation[0].value = "ANETHOL" 
* #1705720 "APRACLONIDIN HYDROCHLORID"
* #1705720 ^designation[0].language = #de-AT 
* #1705720 ^designation[0].value = "APRACLONIDIN HYDROCHLORID" 
* #1705724 "ARNICAE FLOS (AUSZUG)"
* #1705724 ^designation[0].language = #de-AT 
* #1705724 ^designation[0].value = "ARNICAE FLOS (AUSZUG)" 
* #1705732 "TRIAMCINOLON HEXACETONID"
* #1705732 ^designation[0].language = #de-AT 
* #1705732 ^designation[0].value = "TRIAMCINOLON HEXACETONID" 
* #1705740 "SUMATRIPTAN SUCCINAT"
* #1705740 ^designation[0].language = #de-AT 
* #1705740 ^designation[0].value = "SUMATRIPTAN SUCCINAT" 
* #1705744 "HELIANTHI ANNUI OLEUM RAFFINATUM"
* #1705744 ^designation[0].language = #de-AT 
* #1705744 ^designation[0].value = "HELIANTHI ANNUI OLEUM RAFFINATUM" 
* #1705751 "2-PROPANOL"
* #1705751 ^designation[0].language = #de-AT 
* #1705751 ^designation[0].value = "2-PROPANOL" 
* #1705767 "IBUTILID FUMARAT"
* #1705767 ^designation[0].language = #de-AT 
* #1705767 ^designation[0].value = "IBUTILID FUMARAT" 
* #1705770 "NATRIUM SULFOBITUMINOSUM"
* #1705770 ^designation[0].language = #de-AT 
* #1705770 ^designation[0].value = "NATRIUM SULFOBITUMINOSUM" 
* #1705771 "IMMUNGLOBULIN"
* #1705771 ^designation[0].language = #de-AT 
* #1705771 ^designation[0].value = "IMMUNGLOBULIN" 
* #1705775 "ALUMINIUMKALIUMSULFAT"
* #1705775 ^designation[0].language = #de-AT 
* #1705775 ^designation[0].value = "ALUMINIUMKALIUMSULFAT" 
* #1705780 "ALUMINIUMHYDROXID X MAGNESIUMCARBONAT"
* #1705780 ^designation[0].language = #de-AT 
* #1705780 ^designation[0].value = "ALUMINIUMHYDROXID X MAGNESIUMCARBONAT" 
* #1705788 "AMMONIAK"
* #1705788 ^designation[0].language = #de-AT 
* #1705788 ^designation[0].value = "AMMONIAK" 
* #1705802 "SOJAÖL"
* #1705802 ^designation[0].language = #de-AT 
* #1705802 ^designation[0].value = "SOJAÖL" 
* #1705808 "SOMATOSTATIN ACETAT"
* #1705808 ^designation[0].language = #de-AT 
* #1705808 ^designation[0].value = "SOMATOSTATIN ACETAT" 
* #1705832 "HEMIN"
* #1705832 ^designation[0].language = #de-AT 
* #1705832 ^designation[0].value = "HEMIN" 
* #1705846 "AJMALIN"
* #1705846 ^designation[0].language = #de-AT 
* #1705846 ^designation[0].value = "AJMALIN" 
* #1705852 "ALLERGENE (POLLEN)"
* #1705852 ^designation[0].language = #de-AT 
* #1705852 ^designation[0].value = "ALLERGENE (POLLEN)" 
* #1705856 "DIHYDROCODEIN BITARTRAT"
* #1705856 ^designation[0].language = #de-AT 
* #1705856 ^designation[0].value = "DIHYDROCODEIN BITARTRAT" 
* #1705857 "HEXAKALIUMHEXANATRIUMTRIHYDROGENPENTACITRAT"
* #1705857 ^designation[0].language = #de-AT 
* #1705857 ^designation[0].value = "HEXAKALIUMHEXANATRIUMTRIHYDROGENPENTACITRAT" 
* #1705858 "ALLANTOIN"
* #1705858 ^designation[0].language = #de-AT 
* #1705858 ^designation[0].value = "ALLANTOIN" 
* #1705860 "WASSER FÜR INJEKTIONSZWECKE"
* #1705860 ^designation[0].language = #de-AT 
* #1705860 ^designation[0].value = "WASSER FÜR INJEKTIONSZWECKE" 
* #1705869 "SALZSÄURE-LÖSUNG"
* #1705869 ^designation[0].language = #de-AT 
* #1705869 ^designation[0].value = "SALZSÄURE-LÖSUNG" 
* #1705874 "NATRIUMGLYCEROPHOSPHAT"
* #1705874 ^designation[0].language = #de-AT 
* #1705874 ^designation[0].value = "NATRIUMGLYCEROPHOSPHAT" 
* #1705885 "NATRIUM DODECYLOXYCARBONYLMETHANSULFONAT"
* #1705885 ^designation[0].language = #de-AT 
* #1705885 ^designation[0].value = "NATRIUM DODECYLOXYCARBONYLMETHANSULFONAT" 
* #1705887 "NATRIUM PENTOSAN POLYSULFAT"
* #1705887 ^designation[0].language = #de-AT 
* #1705887 ^designation[0].value = "NATRIUM PENTOSAN POLYSULFAT" 
* #1705889 "NATRIUMSULFAT"
* #1705889 ^designation[0].language = #de-AT 
* #1705889 ^designation[0].value = "NATRIUMSULFAT" 
* #1705899 "NATRIUMPERCHLORAT"
* #1705899 ^designation[0].language = #de-AT 
* #1705899 ^designation[0].value = "NATRIUMPERCHLORAT" 
* #1705900 "NATRIUMMONOHYDROGENPHOSPHAT"
* #1705900 ^designation[0].language = #de-AT 
* #1705900 ^designation[0].value = "NATRIUMMONOHYDROGENPHOSPHAT" 
* #1705904 "HISTIDINHYDROCHLORID"
* #1705904 ^designation[0].language = #de-AT 
* #1705904 ^designation[0].value = "HISTIDINHYDROCHLORID" 
* #1705907 "ZINGIBERIS RHIZOMA"
* #1705907 ^designation[0].language = #de-AT 
* #1705907 ^designation[0].value = "ZINGIBERIS RHIZOMA" 
* #1705908 "GINKGONIS FOLIUM (AUSZUG)"
* #1705908 ^designation[0].language = #de-AT 
* #1705908 ^designation[0].value = "GINKGONIS FOLIUM (AUSZUG)" 
* #1705910 "GLUCOSE"
* #1705910 ^designation[0].language = #de-AT 
* #1705910 ^designation[0].value = "GLUCOSE" 
* #1705929 "TALKUM"
* #1705929 ^designation[0].language = #de-AT 
* #1705929 ^designation[0].value = "TALKUM" 
* #1705931 "1-NAPTHYLESSIGSÄURE"
* #1705931 ^designation[0].language = #de-AT 
* #1705931 ^designation[0].value = "1-NAPTHYLESSIGSÄURE" 
* #1705950 "CALCIUMSALZE DER SENNOSIDE A+B"
* #1705950 ^designation[0].language = #de-AT 
* #1705950 ^designation[0].value = "CALCIUMSALZE DER SENNOSIDE A+B" 
* #1705966 "EISEN(II)-LACTAT"
* #1705966 ^designation[0].language = #de-AT 
* #1705966 ^designation[0].value = "EISEN(II)-LACTAT" 
* #1705968 "EISEN(II)-SULFAT"
* #1705968 ^designation[0].language = #de-AT 
* #1705968 ^designation[0].value = "EISEN(II)-SULFAT" 
* #1705969 "CARYOPHYLLI FLOS"
* #1705969 ^designation[0].language = #de-AT 
* #1705969 ^designation[0].value = "CARYOPHYLLI FLOS" 
* #1705973 "FISCHLEBERÖL"
* #1705973 ^designation[0].language = #de-AT 
* #1705973 ^designation[0].value = "FISCHLEBERÖL" 
* #1705974 "FISCHÖL"
* #1705974 ^designation[0].language = #de-AT 
* #1705974 ^designation[0].value = "FISCHÖL" 
* #1705986 "NATRIUMDIHYDROGENPHOSPHAT"
* #1705986 ^designation[0].language = #de-AT 
* #1705986 ^designation[0].value = "NATRIUMDIHYDROGENPHOSPHAT" 
* #1705989 "KOLLAGEN"
* #1705989 ^designation[0].language = #de-AT 
* #1705989 ^designation[0].value = "KOLLAGEN" 
* #1705992 "POLYSORBAT 20"
* #1705992 ^designation[0].language = #de-AT 
* #1705992 ^designation[0].value = "POLYSORBAT 20" 
* #1705995 "NATRIUMCARBONAT"
* #1705995 ^designation[0].language = #de-AT 
* #1705995 ^designation[0].value = "NATRIUMCARBONAT" 
* #1705998 "PREDNISOLON SUCCINAT"
* #1705998 ^designation[0].language = #de-AT 
* #1705998 ^designation[0].value = "PREDNISOLON SUCCINAT" 
* #1706002 "ETORICOXIB"
* #1706002 ^designation[0].language = #de-AT 
* #1706002 ^designation[0].value = "ETORICOXIB" 
* #1706011 "SILBERNITRAT"
* #1706011 ^designation[0].language = #de-AT 
* #1706011 ^designation[0].value = "SILBERNITRAT" 
* #1706021 "ROSMARINI AETHEROLEUM"
* #1706021 ^designation[0].language = #de-AT 
* #1706021 ^designation[0].value = "ROSMARINI AETHEROLEUM" 
* #1706030 "GINSENG RADIX (AUSZUG)"
* #1706030 ^designation[0].language = #de-AT 
* #1706030 ^designation[0].value = "GINSENG RADIX (AUSZUG)" 
* #1706034 "EISEN(III)-CHLORID"
* #1706034 ^designation[0].language = #de-AT 
* #1706034 ^designation[0].value = "EISEN(III)-CHLORID" 
* #1706038 "1,3,3-TRIMETHYLNORBORNAN-2-ON"
* #1706038 ^designation[0].language = #de-AT 
* #1706038 ^designation[0].value = "1,3,3-TRIMETHYLNORBORNAN-2-ON" 
* #1706047 "WEINSÄURE"
* #1706047 ^designation[0].language = #de-AT 
* #1706047 ^designation[0].value = "WEINSÄURE" 
* #1706048 "TRYPSIN"
* #1706048 ^designation[0].language = #de-AT 
* #1706048 ^designation[0].value = "TRYPSIN" 
* #1706051 "CALCIUM ACAMPROSAT"
* #1706051 ^designation[0].language = #de-AT 
* #1706051 ^designation[0].value = "CALCIUM ACAMPROSAT" 
* #1706056 "ZOLMITRIPTAN"
* #1706056 ^designation[0].language = #de-AT 
* #1706056 ^designation[0].value = "ZOLMITRIPTAN" 
* #1706057 "ABCIXIMAB"
* #1706057 ^designation[0].language = #de-AT 
* #1706057 ^designation[0].value = "ABCIXIMAB" 
* #1706060 "ADENOSIN"
* #1706060 ^designation[0].language = #de-AT 
* #1706060 ^designation[0].value = "ADENOSIN" 
* #1706075 "PRIMULAE FLOS"
* #1706075 ^designation[0].language = #de-AT 
* #1706075 ^designation[0].value = "PRIMULAE FLOS" 
* #1706076 "1-PROPANOL"
* #1706076 ^designation[0].language = #de-AT 
* #1706076 ^designation[0].value = "1-PROPANOL" 
* #1706081 "CUCURBITAE SEMEN OLEUM"
* #1706081 ^designation[0].language = #de-AT 
* #1706081 ^designation[0].value = "CUCURBITAE SEMEN OLEUM" 
* #1706087 "EPHEDRINSULFAT"
* #1706087 ^designation[0].language = #de-AT 
* #1706087 ^designation[0].value = "EPHEDRINSULFAT" 
* #1706089 "ESCHERICHIA COLI"
* #1706089 ^designation[0].language = #de-AT 
* #1706089 ^designation[0].value = "ESCHERICHIA COLI" 
* #1706095 "ETHANOL"
* #1706095 ^designation[0].language = #de-AT 
* #1706095 ^designation[0].value = "ETHANOL" 
* #1706111 "MITTELKETTIGE TRIGLYCERIDE"
* #1706111 ^designation[0].language = #de-AT 
* #1706111 ^designation[0].value = "MITTELKETTIGE TRIGLYCERIDE" 
* #1706121 "VIGABATRIN"
* #1706121 ^designation[0].language = #de-AT 
* #1706121 ^designation[0].value = "VIGABATRIN" 
* #1706122 "VINFLUNIN"
* #1706122 ^designation[0].language = #de-AT 
* #1706122 ^designation[0].value = "VINFLUNIN" 
* #1706135 "KALIUM 4-AMINOBENZOAT"
* #1706135 ^designation[0].language = #de-AT 
* #1706135 ^designation[0].value = "KALIUM 4-AMINOBENZOAT" 
* #1706142 "DIHYDROERGOCRYPTIN METHANSULFONAT"
* #1706142 ^designation[0].language = #de-AT 
* #1706142 ^designation[0].value = "DIHYDROERGOCRYPTIN METHANSULFONAT" 
* #1706151 "DIMETHYLFUMARAT"
* #1706151 ^designation[0].language = #de-AT 
* #1706151 ^designation[0].value = "DIMETHYLFUMARAT" 
* #1706161 "ABSINTHII HERBA"
* #1706161 ^designation[0].language = #de-AT 
* #1706161 ^designation[0].value = "ABSINTHII HERBA" 
* #1706167 "ROSMARINI FOLIUM"
* #1706167 ^designation[0].language = #de-AT 
* #1706167 ^designation[0].value = "ROSMARINI FOLIUM" 
* #1706169 "SELENDISULFID"
* #1706169 ^designation[0].language = #de-AT 
* #1706169 ^designation[0].value = "SELENDISULFID" 
* #1706173 "ONONIDIS RADIX"
* #1706173 ^designation[0].language = #de-AT 
* #1706173 ^designation[0].value = "ONONIDIS RADIX" 
* #1706178 "NATRIUM FUSIDAT"
* #1706178 ^designation[0].language = #de-AT 
* #1706178 ^designation[0].value = "NATRIUM FUSIDAT" 
* #1706182 "NATRIUMHYDROGENCARBONAT"
* #1706182 ^designation[0].language = #de-AT 
* #1706182 ^designation[0].value = "NATRIUMHYDROGENCARBONAT" 
* #1706185 "XIPAMID"
* #1706185 ^designation[0].language = #de-AT 
* #1706185 ^designation[0].value = "XIPAMID" 
* #1706196 "KALIUM HYDROGENASPARTAT"
* #1706196 ^designation[0].language = #de-AT 
* #1706196 ^designation[0].value = "KALIUM HYDROGENASPARTAT" 
* #1706200 "SALIZYLSÄUREPHENYLESTER"
* #1706200 ^designation[0].language = #de-AT 
* #1706200 ^designation[0].value = "SALIZYLSÄUREPHENYLESTER" 
* #1706210 "PHYSOSTIGMINSALICYLAT"
* #1706210 ^designation[0].language = #de-AT 
* #1706210 ^designation[0].value = "PHYSOSTIGMINSALICYLAT" 
* #1706212 "PINI SILVESTRIS AETHEROLEUM"
* #1706212 ^designation[0].language = #de-AT 
* #1706212 ^designation[0].value = "PINI SILVESTRIS AETHEROLEUM" 
* #1706227 "DANAPAROID"
* #1706227 ^designation[0].language = #de-AT 
* #1706227 ^designation[0].value = "DANAPAROID" 
* #1706229 "DESOXYCHOLSÄURE"
* #1706229 ^designation[0].language = #de-AT 
* #1706229 ^designation[0].value = "DESOXYCHOLSÄURE" 
* #1706241 "DIETHYLAMINSALICYLAT"
* #1706241 ^designation[0].language = #de-AT 
* #1706241 ^designation[0].value = "DIETHYLAMINSALICYLAT" 
* #1706248 "OMEGA-3-SÄURENETHYLESTER"
* #1706248 ^designation[0].language = #de-AT 
* #1706248 ^designation[0].value = "OMEGA-3-SÄURENETHYLESTER" 
* #1706253 "OMEGA-3-SÄUREN-TRIGLYCERIDE"
* #1706253 ^designation[0].language = #de-AT 
* #1706253 ^designation[0].value = "OMEGA-3-SÄUREN-TRIGLYCERIDE" 
* #1706262 "FLUVOXAMINMALEAT"
* #1706262 ^designation[0].language = #de-AT 
* #1706262 ^designation[0].value = "FLUVOXAMINMALEAT" 
* #1706266 "CALCIUMHYDROGENPHOSPHAT"
* #1706266 ^designation[0].language = #de-AT 
* #1706266 ^designation[0].value = "CALCIUMHYDROGENPHOSPHAT" 
* #1706274 "PECTINUM"
* #1706274 ^designation[0].language = #de-AT 
* #1706274 ^designation[0].value = "PECTINUM" 
* #1706275 "PANKREAS(EXTRAKT)"
* #1706275 ^designation[0].language = #de-AT 
* #1706275 ^designation[0].value = "PANKREAS(EXTRAKT)" 
* #1706278 "PAPAYOTIN"
* #1706278 ^designation[0].language = #de-AT 
* #1706278 ^designation[0].value = "PAPAYOTIN" 
* #1706283 "DIHYDROERGOCORNIN METHANSULFONAT"
* #1706283 ^designation[0].language = #de-AT 
* #1706283 ^designation[0].value = "DIHYDROERGOCORNIN METHANSULFONAT" 
* #1706293 "KOLLAGENASE"
* #1706293 ^designation[0].language = #de-AT 
* #1706293 ^designation[0].value = "KOLLAGENASE" 
* #1706294 "KUPFERDICHLORID"
* #1706294 ^designation[0].language = #de-AT 
* #1706294 ^designation[0].value = "KUPFERDICHLORID" 
* #1706300 "FRANGULAE CORTEX"
* #1706300 ^designation[0].language = #de-AT 
* #1706300 ^designation[0].value = "FRANGULAE CORTEX" 
* #1706306 "KALIUMDIHYDROGENPHOSPHAT"
* #1706306 ^designation[0].language = #de-AT 
* #1706306 ^designation[0].value = "KALIUMDIHYDROGENPHOSPHAT" 
* #1706309 "PLANTAGINIS OVATAE SEMEN"
* #1706309 ^designation[0].language = #de-AT 
* #1706309 ^designation[0].value = "PLANTAGINIS OVATAE SEMEN" 
* #1706310 "JOSAMYCIN"
* #1706310 ^designation[0].language = #de-AT 
* #1706310 ^designation[0].value = "JOSAMYCIN" 
* #1706312 "KALIUMHYDROGENCARBONAT"
* #1706312 ^designation[0].language = #de-AT 
* #1706312 ^designation[0].value = "KALIUMHYDROGENCARBONAT" 
* #1706326 "LIQUIRITIAE RADIX"
* #1706326 ^designation[0].language = #de-AT 
* #1706326 ^designation[0].value = "LIQUIRITIAE RADIX" 
* #1706327 "LEVISTICI RADIX"
* #1706327 ^designation[0].language = #de-AT 
* #1706327 ^designation[0].value = "LEVISTICI RADIX" 
* #1706333 "EUCALYPTI AETHEROLEUM"
* #1706333 ^designation[0].language = #de-AT 
* #1706333 ^designation[0].value = "EUCALYPTI AETHEROLEUM" 
* #1706334 "ALPHA-TOCOPHEROLACETAT"
* #1706334 ^designation[0].language = #de-AT 
* #1706334 ^designation[0].value = "ALPHA-TOCOPHEROLACETAT" 
* #1706341 "NICOTINALDEHYD"
* #1706341 ^designation[0].language = #de-AT 
* #1706341 ^designation[0].value = "NICOTINALDEHYD" 
* #1706345 "NICOTIN"
* #1706345 ^designation[0].language = #de-AT 
* #1706345 ^designation[0].value = "NICOTIN" 
* #1706356 "OLIVAE OLEUM"
* #1706356 ^designation[0].language = #de-AT 
* #1706356 ^designation[0].value = "OLIVAE OLEUM" 
* #1706357 "ORNITHIN ASPARTAT"
* #1706357 ^designation[0].language = #de-AT 
* #1706357 ^designation[0].value = "ORNITHIN ASPARTAT" 
* #1706364 "CIMICIFUGAE RHIZOMA (AUSZUG)"
* #1706364 ^designation[0].language = #de-AT 
* #1706364 ^designation[0].value = "CIMICIFUGAE RHIZOMA (AUSZUG)" 
* #1706373 "CITRONELLAE AETHEROLEUM"
* #1706373 ^designation[0].language = #de-AT 
* #1706373 ^designation[0].value = "CITRONELLAE AETHEROLEUM" 
* #1706374 "CARYOPHYLLI FLORIS AETHEROLEUM"
* #1706374 ^designation[0].language = #de-AT 
* #1706374 ^designation[0].value = "CARYOPHYLLI FLORIS AETHEROLEUM" 
* #1706385 "GENTIANAE RADIX"
* #1706385 ^designation[0].language = #de-AT 
* #1706385 ^designation[0].value = "GENTIANAE RADIX" 
* #1706402 "CLEVIDIPIN BUTYRAT"
* #1706402 ^designation[0].language = #de-AT 
* #1706402 ^designation[0].value = "CLEVIDIPIN BUTYRAT" 
* #1706408 "ROFLUMILAST"
* #1706408 ^designation[0].language = #de-AT 
* #1706408 ^designation[0].value = "ROFLUMILAST" 
* #1706412 "ROCURONIUM BROMID"
* #1706412 ^designation[0].language = #de-AT 
* #1706412 ^designation[0].value = "ROCURONIUM BROMID" 
* #1706417 "RALTITREXED"
* #1706417 ^designation[0].language = #de-AT 
* #1706417 ^designation[0].value = "RALTITREXED" 
* #1706419 "MODAFINIL"
* #1706419 ^designation[0].language = #de-AT 
* #1706419 ^designation[0].value = "MODAFINIL" 
* #1706426 "THIAMINMONOPHOSPHAT CHLORID DIHYDRAT"
* #1706426 ^designation[0].language = #de-AT 
* #1706426 ^designation[0].value = "THIAMINMONOPHOSPHAT CHLORID DIHYDRAT" 
* #1706430 "GLIBENCLAMID"
* #1706430 ^designation[0].language = #de-AT 
* #1706430 ^designation[0].value = "GLIBENCLAMID" 
* #1706431 "FLUTRIMAZOL"
* #1706431 ^designation[0].language = #de-AT 
* #1706431 ^designation[0].value = "FLUTRIMAZOL" 
* #1706436 "FOTEMUSTIN"
* #1706436 ^designation[0].language = #de-AT 
* #1706436 ^designation[0].value = "FOTEMUSTIN" 
* #1706440 "DYDROGESTERON"
* #1706440 ^designation[0].language = #de-AT 
* #1706440 ^designation[0].value = "DYDROGESTERON" 
* #1706447 "RIFAMPICIN"
* #1706447 ^designation[0].language = #de-AT 
* #1706447 ^designation[0].value = "RIFAMPICIN" 
* #1706449 "RETIGABIN"
* #1706449 ^designation[0].language = #de-AT 
* #1706449 ^designation[0].value = "RETIGABIN" 
* #1706452 "MOXIFLOXACIN"
* #1706452 ^designation[0].language = #de-AT 
* #1706452 ^designation[0].value = "MOXIFLOXACIN" 
* #1706453 "MIDAZOLAM"
* #1706453 ^designation[0].language = #de-AT 
* #1706453 ^designation[0].value = "MIDAZOLAM" 
* #1706454 "MIFEPRISTON"
* #1706454 ^designation[0].language = #de-AT 
* #1706454 ^designation[0].value = "MIFEPRISTON" 
* #1706455 "MIGLITOL"
* #1706455 ^designation[0].language = #de-AT 
* #1706455 ^designation[0].value = "MIGLITOL" 
* #1706456 "MILRINON"
* #1706456 ^designation[0].language = #de-AT 
* #1706456 ^designation[0].value = "MILRINON" 
* #1706458 "FLUFENAMINSÄURE"
* #1706458 ^designation[0].language = #de-AT 
* #1706458 ^designation[0].value = "FLUFENAMINSÄURE" 
* #1706461 "FLUDROCORTISON"
* #1706461 ^designation[0].language = #de-AT 
* #1706461 ^designation[0].value = "FLUDROCORTISON" 
* #1706462 "FLUMAZENIL"
* #1706462 ^designation[0].language = #de-AT 
* #1706462 ^designation[0].value = "FLUMAZENIL" 
* #1706463 "FLUOCORTOLON"
* #1706463 ^designation[0].language = #de-AT 
* #1706463 ^designation[0].value = "FLUOCORTOLON" 
* #1706471 "DORNASE ALFA"
* #1706471 ^designation[0].language = #de-AT 
* #1706471 ^designation[0].value = "DORNASE ALFA" 
* #1706475 "PRULIFLOXACIN"
* #1706475 ^designation[0].language = #de-AT 
* #1706475 ^designation[0].value = "PRULIFLOXACIN" 
* #1706476 "PYRAZINAMID"
* #1706476 ^designation[0].language = #de-AT 
* #1706476 ^designation[0].value = "PYRAZINAMID" 
* #1706483 "PRUCALOPRID"
* #1706483 ^designation[0].language = #de-AT 
* #1706483 ^designation[0].value = "PRUCALOPRID" 
* #1706491 "METHYLTHIONINIUM CHLORID"
* #1706491 ^designation[0].language = #de-AT 
* #1706491 ^designation[0].value = "METHYLTHIONINIUM CHLORID" 
* #1706493 "MITOMYCIN"
* #1706493 ^designation[0].language = #de-AT 
* #1706493 ^designation[0].value = "MITOMYCIN" 
* #1706495 "MIVACURIUM CHLORID"
* #1706495 ^designation[0].language = #de-AT 
* #1706495 ^designation[0].value = "MIVACURIUM CHLORID" 
* #1706512 "DRONEDARON"
* #1706512 ^designation[0].language = #de-AT 
* #1706512 ^designation[0].value = "DRONEDARON" 
* #1706513 "DROPERIDOL"
* #1706513 ^designation[0].language = #de-AT 
* #1706513 ^designation[0].value = "DROPERIDOL" 
* #1706526 "PROLIN"
* #1706526 ^designation[0].language = #de-AT 
* #1706526 ^designation[0].value = "PROLIN" 
* #1706544 "FELBAMAT"
* #1706544 ^designation[0].language = #de-AT 
* #1706544 ^designation[0].value = "FELBAMAT" 
* #1706552 "CARBASALAT CALCIUM"
* #1706552 ^designation[0].language = #de-AT 
* #1706552 ^designation[0].value = "CARBASALAT CALCIUM" 
* #1706553 "CARBETOCIN"
* #1706553 ^designation[0].language = #de-AT 
* #1706553 ^designation[0].value = "CARBETOCIN" 
* #1706555 "CAROVERIN"
* #1706555 ^designation[0].language = #de-AT 
* #1706555 ^designation[0].value = "CAROVERIN" 
* #1706560 "PROPYLTHIOURACIL"
* #1706560 ^designation[0].language = #de-AT 
* #1706560 ^designation[0].value = "PROPYLTHIOURACIL" 
* #1706579 "METHOXYFLURAN"
* #1706579 ^designation[0].language = #de-AT 
* #1706579 ^designation[0].value = "METHOXYFLURAN" 
* #1706594 "CETRIMONIUM BROMID"
* #1706594 ^designation[0].language = #de-AT 
* #1706594 ^designation[0].value = "CETRIMONIUM BROMID" 
* #1706604 "FAMPRIDIN"
* #1706604 ^designation[0].language = #de-AT 
* #1706604 ^designation[0].value = "FAMPRIDIN" 
* #1706605 "CHLORAMBUCIL"
* #1706605 ^designation[0].language = #de-AT 
* #1706605 ^designation[0].value = "CHLORAMBUCIL" 
* #1706610 "CANRENOINSÄURE"
* #1706610 ^designation[0].language = #de-AT 
* #1706610 ^designation[0].value = "CANRENOINSÄURE" 
* #1706612 "CALCIUM SACCHARAT"
* #1706612 ^designation[0].language = #de-AT 
* #1706612 ^designation[0].value = "CALCIUM SACCHARAT" 
* #1706618 "PIRFENIDON"
* #1706618 ^designation[0].language = #de-AT 
* #1706618 ^designation[0].value = "PIRFENIDON" 
* #1706620 "PRIMIDON"
* #1706620 ^designation[0].language = #de-AT 
* #1706620 ^designation[0].value = "PRIMIDON" 
* #1706633 "LYMECYCLIN"
* #1706633 ^designation[0].language = #de-AT 
* #1706633 ^designation[0].value = "LYMECYCLIN" 
* #1706656 "DISULFIRAM"
* #1706656 ^designation[0].language = #de-AT 
* #1706656 ^designation[0].value = "DISULFIRAM" 
* #1706665 "CICLOPIROX"
* #1706665 ^designation[0].language = #de-AT 
* #1706665 ^designation[0].value = "CICLOPIROX" 
* #1706671 "CICLESONID"
* #1706671 ^designation[0].language = #de-AT 
* #1706671 ^designation[0].value = "CICLESONID" 
* #1706688 "LOMUSTIN"
* #1706688 ^designation[0].language = #de-AT 
* #1706688 ^designation[0].value = "LOMUSTIN" 
* #1706694 "ETONOGESTREL"
* #1706694 ^designation[0].language = #de-AT 
* #1706694 ^designation[0].value = "ETONOGESTREL" 
* #1706695 "ETOPOSID"
* #1706695 ^designation[0].language = #de-AT 
* #1706695 ^designation[0].value = "ETOPOSID" 
* #1706699 "ETOMIDAT"
* #1706699 ^designation[0].language = #de-AT 
* #1706699 ^designation[0].value = "ETOMIDAT" 
* #1706701 "THIOTEPA"
* #1706701 ^designation[0].language = #de-AT 
* #1706701 ^designation[0].value = "THIOTEPA" 
* #1706733 "PHENPROCOUMON"
* #1706733 ^designation[0].language = #de-AT 
* #1706733 ^designation[0].value = "PHENPROCOUMON" 
* #1706736 "LEVOMENOL"
* #1706736 ^designation[0].language = #de-AT 
* #1706736 ^designation[0].value = "LEVOMENOL" 
* #1706752 "TAURIN"
* #1706752 ^designation[0].language = #de-AT 
* #1706752 ^designation[0].value = "TAURIN" 
* #1706754 "THIAMAZOL"
* #1706754 ^designation[0].language = #de-AT 
* #1706754 ^designation[0].value = "THIAMAZOL" 
* #1706756 "TAUROLIDIN"
* #1706756 ^designation[0].language = #de-AT 
* #1706756 ^designation[0].value = "TAUROLIDIN" 
* #1706757 "TEGAFUR"
* #1706757 ^designation[0].language = #de-AT 
* #1706757 ^designation[0].value = "TEGAFUR" 
* #1706759 "PENICILLAMIN"
* #1706759 ^designation[0].language = #de-AT 
* #1706759 ^designation[0].value = "PENICILLAMIN" 
* #1706766 "PENTETSÄURE"
* #1706766 ^designation[0].language = #de-AT 
* #1706766 ^designation[0].value = "PENTETSÄURE" 
* #1706767 "PENTETREOTID"
* #1706767 ^designation[0].language = #de-AT 
* #1706767 ^designation[0].value = "PENTETREOTID" 
* #1706799 "TETRACYCLIN"
* #1706799 ^designation[0].language = #de-AT 
* #1706799 ^designation[0].value = "TETRACYCLIN" 
* #1706821 "KEBUZON"
* #1706821 ^designation[0].language = #de-AT 
* #1706821 ^designation[0].value = "KEBUZON" 
* #1706835 "TACALCITOL"
* #1706835 ^designation[0].language = #de-AT 
* #1706835 ^designation[0].value = "TACALCITOL" 
* #1706847 "IOMEPROL"
* #1706847 ^designation[0].language = #de-AT 
* #1706847 ^designation[0].value = "IOMEPROL" 
* #1706862 "DESFLURAN"
* #1706862 ^designation[0].language = #de-AT 
* #1706862 ^designation[0].value = "DESFLURAN" 
* #1706864 "DESOGESTREL"
* #1706864 ^designation[0].language = #de-AT 
* #1706864 ^designation[0].value = "DESOGESTREL" 
* #1706866 "DEXIBUPROFEN"
* #1706866 ^designation[0].language = #de-AT 
* #1706866 ^designation[0].value = "DEXIBUPROFEN" 
* #1706887 "SULFAMETROL"
* #1706887 ^designation[0].language = #de-AT 
* #1706887 ^designation[0].value = "SULFAMETROL" 
* #1706892 "OLAFLUR"
* #1706892 ^designation[0].language = #de-AT 
* #1706892 ^designation[0].value = "OLAFLUR" 
* #1706909 "IOBITRIDOL"
* #1706909 ^designation[0].language = #de-AT 
* #1706909 ^designation[0].value = "IOBITRIDOL" 
* #1706914 "DECITABIN"
* #1706914 ^designation[0].language = #de-AT 
* #1706914 ^designation[0].value = "DECITABIN" 
* #1706915 "DECTAFLUR"
* #1706915 ^designation[0].language = #de-AT 
* #1706915 ^designation[0].value = "DECTAFLUR" 
* #1706918 "SUCCIMER"
* #1706918 ^designation[0].language = #de-AT 
* #1706918 ^designation[0].value = "SUCCIMER" 
* #1706934 "NONIVAMID"
* #1706934 ^designation[0].language = #de-AT 
* #1706934 ^designation[0].value = "NONIVAMID" 
* #1706942 "IOHEXOL"
* #1706942 ^designation[0].language = #de-AT 
* #1706942 ^designation[0].value = "IOHEXOL" 
* #1706945 "ICATIBANT"
* #1706945 ^designation[0].language = #de-AT 
* #1706945 ^designation[0].value = "ICATIBANT" 
* #1706950 "IFOSFAMID"
* #1706950 ^designation[0].language = #de-AT 
* #1706950 ^designation[0].value = "IFOSFAMID" 
* #1706985 "NILVADIPIN"
* #1706985 ^designation[0].language = #de-AT 
* #1706985 ^designation[0].value = "NILVADIPIN" 
* #1707010 "SERIN"
* #1707010 ^designation[0].language = #de-AT 
* #1707010 ^designation[0].value = "SERIN" 
* #1707019 "NICOBOXIL"
* #1707019 ^designation[0].language = #de-AT 
* #1707019 ^designation[0].value = "NICOBOXIL" 
* #1707026 "GLYCOPYRRONIUMBROMID"
* #1707026 ^designation[0].language = #de-AT 
* #1707026 ^designation[0].value = "GLYCOPYRRONIUMBROMID" 
* #1707038 "CLOBAZAM"
* #1707038 ^designation[0].language = #de-AT 
* #1707038 ^designation[0].value = "CLOBAZAM" 
* #1707043 "SERTINDOL"
* #1707043 ^designation[0].language = #de-AT 
* #1707043 ^designation[0].value = "SERTINDOL" 
* #1707054 "MYRTECAIN"
* #1707054 ^designation[0].language = #de-AT 
* #1707054 ^designation[0].value = "MYRTECAIN" 
* #1707055 "NABILON"
* #1707055 ^designation[0].language = #de-AT 
* #1707055 ^designation[0].value = "NABILON" 
* #1707064 "GADOTERIDOL"
* #1707064 ^designation[0].language = #de-AT 
* #1707064 ^designation[0].value = "GADOTERIDOL" 
* #1707067 "CLONAZEPAM"
* #1707067 ^designation[0].language = #de-AT 
* #1707067 ^designation[0].value = "CLONAZEPAM" 
* #1707078 "ACITRETIN"
* #1707078 ^designation[0].language = #de-AT 
* #1707078 ^designation[0].value = "ACITRETIN" 
* #1707080 "TIROFIBAN HYDROCHLORID"
* #1707080 ^designation[0].language = #de-AT 
* #1707080 ^designation[0].value = "TIROFIBAN HYDROCHLORID" 
* #1707088 "TRANYLCYPROMIN SULFAT"
* #1707088 ^designation[0].language = #de-AT 
* #1707088 ^designation[0].value = "TRANYLCYPROMIN SULFAT" 
* #1707092 "TRIPTORELIN EMBONAT"
* #1707092 ^designation[0].language = #de-AT 
* #1707092 ^designation[0].value = "TRIPTORELIN EMBONAT" 
* #1707097 "TRAMAZOLIN HYDROCHLORID"
* #1707097 ^designation[0].language = #de-AT 
* #1707097 ^designation[0].value = "TRAMAZOLIN HYDROCHLORID" 
* #1707098 "TRIAMCINOLON ACETONID DIKALIUMPHOSPHAT"
* #1707098 ^designation[0].language = #de-AT 
* #1707098 ^designation[0].value = "TRIAMCINOLON ACETONID DIKALIUMPHOSPHAT" 
* #1707125 "CHLOROPROCAIN HYDROCHLORID"
* #1707125 ^designation[0].language = #de-AT 
* #1707125 ^designation[0].value = "CHLOROPROCAIN HYDROCHLORID" 
* #1707127 "ARACHIDIS OLEUM RAFFINATUM"
* #1707127 ^designation[0].language = #de-AT 
* #1707127 ^designation[0].value = "ARACHIDIS OLEUM RAFFINATUM" 
* #1707131 "PIXANTRON"
* #1707131 ^designation[0].language = #de-AT 
* #1707131 ^designation[0].value = "PIXANTRON" 
* #1707142 "TERBUTALINSULFAT"
* #1707142 ^designation[0].language = #de-AT 
* #1707142 ^designation[0].value = "TERBUTALINSULFAT" 
* #1707146 "CHLORTETRACYCLIN HYDROCHLORID"
* #1707146 ^designation[0].language = #de-AT 
* #1707146 ^designation[0].value = "CHLORTETRACYCLIN HYDROCHLORID" 
* #1707153 "CICLOPIROX OLAMIN"
* #1707153 ^designation[0].language = #de-AT 
* #1707153 ^designation[0].value = "CICLOPIROX OLAMIN" 
* #1707155 "BUSERELIN ACETAT"
* #1707155 ^designation[0].language = #de-AT 
* #1707155 ^designation[0].value = "BUSERELIN ACETAT" 
* #1707161 "CALCITONIN"
* #1707161 ^designation[0].language = #de-AT 
* #1707161 ^designation[0].value = "CALCITONIN" 
* #1707176 "CARBOMER"
* #1707176 ^designation[0].language = #de-AT 
* #1707176 ^designation[0].value = "CARBOMER" 
* #1707180 "CHYMOTRYPSIN"
* #1707180 ^designation[0].language = #de-AT 
* #1707180 ^designation[0].value = "CHYMOTRYPSIN" 
* #1707181 "EISENSACCHAROSE"
* #1707181 ^designation[0].language = #de-AT 
* #1707181 ^designation[0].value = "EISENSACCHAROSE" 
* #1707183 "METHYLSALICYLAT"
* #1707183 ^designation[0].language = #de-AT 
* #1707183 ^designation[0].value = "METHYLSALICYLAT" 
* #1707184 "RILMENIDIN DIHYDROGENPHOSPHAT"
* #1707184 ^designation[0].language = #de-AT 
* #1707184 ^designation[0].value = "RILMENIDIN DIHYDROGENPHOSPHAT" 
* #1707188 "NICORANDIL"
* #1707188 ^designation[0].language = #de-AT 
* #1707188 ^designation[0].value = "NICORANDIL" 
* #1707190 "BETAMETHASON ACETAT"
* #1707190 ^designation[0].language = #de-AT 
* #1707190 ^designation[0].value = "BETAMETHASON ACETAT" 
* #1707191 "JOSAMYCIN PROPIONAT"
* #1707191 ^designation[0].language = #de-AT 
* #1707191 ^designation[0].value = "JOSAMYCIN PROPIONAT" 
* #1707204 "ROPIVACAIN HYDROCHLORID"
* #1707204 ^designation[0].language = #de-AT 
* #1707204 ^designation[0].value = "ROPIVACAIN HYDROCHLORID" 
* #1707215 "BAMIPIN LACTAT"
* #1707215 ^designation[0].language = #de-AT 
* #1707215 ^designation[0].value = "BAMIPIN LACTAT" 
* #1707238 "BENDAMUSTIN HYDROCHLORID"
* #1707238 ^designation[0].language = #de-AT 
* #1707238 ^designation[0].value = "BENDAMUSTIN HYDROCHLORID" 
* #1707244 "NATRIUM PICOSULFAT"
* #1707244 ^designation[0].language = #de-AT 
* #1707244 ^designation[0].value = "NATRIUM PICOSULFAT" 
* #1707254 "ESOMEPRAZOL MAGNESIUM"
* #1707254 ^designation[0].language = #de-AT 
* #1707254 ^designation[0].value = "ESOMEPRAZOL MAGNESIUM" 
* #1707256 "BENZOYLPEROXID"
* #1707256 ^designation[0].language = #de-AT 
* #1707256 ^designation[0].value = "BENZOYLPEROXID" 
* #1707257 "NATRIUMACETAT"
* #1707257 ^designation[0].language = #de-AT 
* #1707257 ^designation[0].value = "NATRIUMACETAT" 
* #1707260 "SULTAMICILLINTOSILAT-DIHYDRAT"
* #1707260 ^designation[0].language = #de-AT 
* #1707260 ^designation[0].value = "SULTAMICILLINTOSILAT-DIHYDRAT" 
* #1707265 "PRIDINOL MESILAT"
* #1707265 ^designation[0].language = #de-AT 
* #1707265 ^designation[0].value = "PRIDINOL MESILAT" 
* #1707266 "PRILOCAIN HYDROCHLORID"
* #1707266 ^designation[0].language = #de-AT 
* #1707266 ^designation[0].value = "PRILOCAIN HYDROCHLORID" 
* #1707269 "PROPIVERIN HYDROCHLORID"
* #1707269 ^designation[0].language = #de-AT 
* #1707269 ^designation[0].value = "PROPIVERIN HYDROCHLORID" 
* #1707273 "PROCYCLIDIN HYDROCHLORID"
* #1707273 ^designation[0].language = #de-AT 
* #1707273 ^designation[0].value = "PROCYCLIDIN HYDROCHLORID" 
* #1707281 "PROTAMIN HYDROCHLORID"
* #1707281 ^designation[0].language = #de-AT 
* #1707281 ^designation[0].value = "PROTAMIN HYDROCHLORID" 
* #1707282 "PROTHIPENDYLHYDROCHLORID"
* #1707282 ^designation[0].language = #de-AT 
* #1707282 ^designation[0].value = "PROTHIPENDYLHYDROCHLORID" 
* #1707285 "BLEOMYCIN SULFAT"
* #1707285 ^designation[0].language = #de-AT 
* #1707285 ^designation[0].value = "BLEOMYCIN SULFAT" 
* #1707292 "ARGININ HYDROCHLORID"
* #1707292 ^designation[0].language = #de-AT 
* #1707292 ^designation[0].value = "ARGININ HYDROCHLORID" 
* #1707293 "OXALIPLATIN"
* #1707293 ^designation[0].language = #de-AT 
* #1707293 ^designation[0].value = "OXALIPLATIN" 
* #1707305 "GADODIAMID"
* #1707305 ^designation[0].language = #de-AT 
* #1707305 ^designation[0].value = "GADODIAMID" 
* #1707307 "LANTHANCARBONAT"
* #1707307 ^designation[0].language = #de-AT 
* #1707307 ^designation[0].value = "LANTHANCARBONAT" 
* #1707308 "NATRIUM FUROSEMID"
* #1707308 ^designation[0].language = #de-AT 
* #1707308 ^designation[0].value = "NATRIUM FUROSEMID" 
* #1707311 "TETRACOSACTID"
* #1707311 ^designation[0].language = #de-AT 
* #1707311 ^designation[0].value = "TETRACOSACTID" 
* #1707312 "ATRACURIUM BESILAT"
* #1707312 ^designation[0].language = #de-AT 
* #1707312 ^designation[0].value = "ATRACURIUM BESILAT" 
* #1707313 "PHENAZON"
* #1707313 ^designation[0].language = #de-AT 
* #1707313 ^designation[0].value = "PHENAZON" 
* #1707319 "ATROPINSULFAT"
* #1707319 ^designation[0].language = #de-AT 
* #1707319 ^designation[0].value = "ATROPINSULFAT" 
* #1707322 "TENECTEPLASE"
* #1707322 ^designation[0].language = #de-AT 
* #1707322 ^designation[0].value = "TENECTEPLASE" 
* #1707325 "TIPRANAVIR"
* #1707325 ^designation[0].language = #de-AT 
* #1707325 ^designation[0].value = "TIPRANAVIR" 
* #1707327 "BIVALIRUDIN"
* #1707327 ^designation[0].language = #de-AT 
* #1707327 ^designation[0].value = "BIVALIRUDIN" 
* #1707328 "BRINZOLAMID"
* #1707328 ^designation[0].language = #de-AT 
* #1707328 ^designation[0].value = "BRINZOLAMID" 
* #1707329 "BUSULFAN"
* #1707329 ^designation[0].language = #de-AT 
* #1707329 ^designation[0].value = "BUSULFAN" 
* #1707337 "AGOMELATIN"
* #1707337 ^designation[0].language = #de-AT 
* #1707337 ^designation[0].value = "AGOMELATIN" 
* #1707338 "AMLODIPIN"
* #1707338 ^designation[0].language = #de-AT 
* #1707338 ^designation[0].value = "AMLODIPIN" 
* #1707339 "ANAKINRA"
* #1707339 ^designation[0].language = #de-AT 
* #1707339 ^designation[0].value = "ANAKINRA" 
* #1707340 "BOSENTAN"
* #1707340 ^designation[0].language = #de-AT 
* #1707340 ^designation[0].value = "BOSENTAN" 
* #1707341 "BUPRENORPHIN"
* #1707341 ^designation[0].language = #de-AT 
* #1707341 ^designation[0].value = "BUPRENORPHIN" 
* #1707343 "EFAVIRENZ"
* #1707343 ^designation[0].language = #de-AT 
* #1707343 ^designation[0].value = "EFAVIRENZ" 
* #1707348 "PHENOXYBENZAMIN HYDROCHLORID"
* #1707348 ^designation[0].language = #de-AT 
* #1707348 ^designation[0].value = "PHENOXYBENZAMIN HYDROCHLORID" 
* #1707357 "PHENOXYMETHYLPENICILLIN BENZATHIN"
* #1707357 ^designation[0].language = #de-AT 
* #1707357 ^designation[0].value = "PHENOXYMETHYLPENICILLIN BENZATHIN" 
* #1707366 "POLYGELIN"
* #1707366 ^designation[0].language = #de-AT 
* #1707366 ^designation[0].value = "POLYGELIN" 
* #1707369 "POLIDOCANOL"
* #1707369 ^designation[0].language = #de-AT 
* #1707369 ^designation[0].value = "POLIDOCANOL" 
* #1707376 "MAPROTILIN MESILAT"
* #1707376 ^designation[0].language = #de-AT 
* #1707376 ^designation[0].value = "MAPROTILIN MESILAT" 
* #1707379 "RUPATADIN FUMARAT"
* #1707379 ^designation[0].language = #de-AT 
* #1707379 ^designation[0].value = "RUPATADIN FUMARAT" 
* #1707381 "TEICOPLANIN"
* #1707381 ^designation[0].language = #de-AT 
* #1707381 ^designation[0].value = "TEICOPLANIN" 
* #1707382 "2-PHENYLPHENOL"
* #1707382 ^designation[0].language = #de-AT 
* #1707382 ^designation[0].value = "2-PHENYLPHENOL" 
* #1707383 "CYSTEINHYDROCHLORID"
* #1707383 ^designation[0].language = #de-AT 
* #1707383 ^designation[0].value = "CYSTEINHYDROCHLORID" 
* #1707384 "ENTACAPON"
* #1707384 ^designation[0].language = #de-AT 
* #1707384 ^designation[0].value = "ENTACAPON" 
* #1707385 "CLADRIBIN"
* #1707385 ^designation[0].language = #de-AT 
* #1707385 ^designation[0].value = "CLADRIBIN" 
* #1707386 "DACLIZUMAB"
* #1707386 ^designation[0].language = #de-AT 
* #1707386 ^designation[0].value = "DACLIZUMAB" 
* #1707392 "EPOETIN ALFA"
* #1707392 ^designation[0].language = #de-AT 
* #1707392 ^designation[0].value = "EPOETIN ALFA" 
* #1707393 "GERINNUNGSFAKTOR VIIa, REKOMBINANT - EPTACOG ALFA (AKTIVIERT)"
* #1707393 ^designation[0].language = #de-AT 
* #1707393 ^designation[0].value = "GERINNUNGSFAKTOR VIIa, REKOMBINANT - EPTACOG ALFA (AKTIVIERT)" 
* #1707394 "DAPTOMYCIN"
* #1707394 ^designation[0].language = #de-AT 
* #1707394 ^designation[0].value = "DAPTOMYCIN" 
* #1707395 "DEFERIPRON"
* #1707395 ^designation[0].language = #de-AT 
* #1707395 ^designation[0].value = "DEFERIPRON" 
* #1707396 "DEXRAZOXAN"
* #1707396 ^designation[0].language = #de-AT 
* #1707396 ^designation[0].value = "DEXRAZOXAN" 
* #1707399 "FILGRASTIM"
* #1707399 ^designation[0].language = #de-AT 
* #1707399 ^designation[0].value = "FILGRASTIM" 
* #1707401 "IMIGLUCERASE"
* #1707401 ^designation[0].language = #de-AT 
* #1707401 ^designation[0].value = "IMIGLUCERASE" 
* #1707402 "INSULIN ASPART"
* #1707402 ^designation[0].language = #de-AT 
* #1707402 ^designation[0].value = "INSULIN ASPART" 
* #1707403 "IRBESARTAN"
* #1707403 ^designation[0].language = #de-AT 
* #1707403 ^designation[0].value = "IRBESARTAN" 
* #1707404 "LAMIVUDIN"
* #1707404 ^designation[0].language = #de-AT 
* #1707404 ^designation[0].value = "LAMIVUDIN" 
* #1707406 "LEVODOPA"
* #1707406 ^designation[0].language = #de-AT 
* #1707406 ^designation[0].value = "LEVODOPA" 
* #1707407 "MITOTAN"
* #1707407 ^designation[0].language = #de-AT 
* #1707407 ^designation[0].value = "MITOTAN" 
* #1707408 "NEPAFENAC"
* #1707408 ^designation[0].language = #de-AT 
* #1707408 ^designation[0].value = "NEPAFENAC" 
* #1707409 "NITISINON"
* #1707409 ^designation[0].language = #de-AT 
* #1707409 ^designation[0].value = "NITISINON" 
* #1707412 "GLIMEPIRID"
* #1707412 ^designation[0].language = #de-AT 
* #1707412 ^designation[0].value = "GLIMEPIRID" 
* #1707413 "HYDROXYCARBAMID"
* #1707413 ^designation[0].language = #de-AT 
* #1707413 ^designation[0].value = "HYDROXYCARBAMID" 
* #1707414 "ILOPROST"
* #1707414 ^designation[0].language = #de-AT 
* #1707414 ^designation[0].value = "ILOPROST" 
* #1707415 "INSULIN GLARGIN"
* #1707415 ^designation[0].language = #de-AT 
* #1707415 ^designation[0].value = "INSULIN GLARGIN" 
* #1707417 "LEFLUNOMID"
* #1707417 ^designation[0].language = #de-AT 
* #1707417 ^designation[0].value = "LEFLUNOMID" 
* #1707418 "MECASERMIN"
* #1707418 ^designation[0].language = #de-AT 
* #1707418 ^designation[0].value = "MECASERMIN" 
* #1707420 "NELARABIN"
* #1707420 ^designation[0].language = #de-AT 
* #1707420 ^designation[0].value = "NELARABIN" 
* #1707421 "NEVIRAPIN"
* #1707421 ^designation[0].language = #de-AT 
* #1707421 ^designation[0].value = "NEVIRAPIN" 
* #1707422 "OLANZAPIN"
* #1707422 ^designation[0].language = #de-AT 
* #1707422 ^designation[0].value = "OLANZAPIN" 
* #1707423 "OXYBUTYNIN"
* #1707423 ^designation[0].language = #de-AT 
* #1707423 ^designation[0].value = "OXYBUTYNIN" 
* #1707424 "PACLITAXEL"
* #1707424 ^designation[0].language = #de-AT 
* #1707424 ^designation[0].value = "PACLITAXEL" 
* #1707425 "POLYESTRADIOL PHOSPHAT"
* #1707425 ^designation[0].language = #de-AT 
* #1707425 ^designation[0].value = "POLYESTRADIOL PHOSPHAT" 
* #1707427 "PRASTERON ENANTAT"
* #1707427 ^designation[0].language = #de-AT 
* #1707427 ^designation[0].value = "PRASTERON ENANTAT" 
* #1707428 "ORNITHIN HYDROCHLORID"
* #1707428 ^designation[0].language = #de-AT 
* #1707428 ^designation[0].value = "ORNITHIN HYDROCHLORID" 
* #1707432 "PYRITHION ZINK"
* #1707432 ^designation[0].language = #de-AT 
* #1707432 ^designation[0].value = "PYRITHION ZINK" 
* #1707436 "OXYBUPROCAIN HYDROCHLORID"
* #1707436 ^designation[0].language = #de-AT 
* #1707436 ^designation[0].value = "OXYBUPROCAIN HYDROCHLORID" 
* #1707439 "NATRIUM PANTOTHENAT"
* #1707439 ^designation[0].language = #de-AT 
* #1707439 ^designation[0].value = "NATRIUM PANTOTHENAT" 
* #1707441 "PAROMOMYCIN SULFAT"
* #1707441 ^designation[0].language = #de-AT 
* #1707441 ^designation[0].value = "PAROMOMYCIN SULFAT" 
* #1707461 "URAPIDIL HYDROCHLORID"
* #1707461 ^designation[0].language = #de-AT 
* #1707461 ^designation[0].value = "URAPIDIL HYDROCHLORID" 
* #1707462 "DINATRIUM GADOXETAT"
* #1707462 ^designation[0].language = #de-AT 
* #1707462 ^designation[0].value = "DINATRIUM GADOXETAT" 
* #1707468 "FOLSÄURE"
* #1707468 ^designation[0].language = #de-AT 
* #1707468 ^designation[0].value = "FOLSÄURE" 
* #1707470 "CALCIPOTRIOL"
* #1707470 ^designation[0].language = #de-AT 
* #1707470 ^designation[0].value = "CALCIPOTRIOL" 
* #1707486 "NAFTIFIN HYDROCHLORID"
* #1707486 ^designation[0].language = #de-AT 
* #1707486 ^designation[0].value = "NAFTIFIN HYDROCHLORID" 
* #1707501 "NONOXYNOL"
* #1707501 ^designation[0].language = #de-AT 
* #1707501 ^designation[0].value = "NONOXYNOL" 
* #1707503 "NOSCAPINHYDROCHLORID"
* #1707503 ^designation[0].language = #de-AT 
* #1707503 ^designation[0].value = "NOSCAPINHYDROCHLORID" 
* #1707507 "EPOLAMIN DICLOFENACAT"
* #1707507 ^designation[0].language = #de-AT 
* #1707507 ^designation[0].value = "EPOLAMIN DICLOFENACAT" 
* #1707515 "ZUCLOPENTHIXOL DECANOAT"
* #1707515 ^designation[0].language = #de-AT 
* #1707515 ^designation[0].value = "ZUCLOPENTHIXOL DECANOAT" 
* #1707516 "WASSER"
* #1707516 ^designation[0].language = #de-AT 
* #1707516 ^designation[0].value = "WASSER" 
* #1707517 "HALOPERIDOL DECANOAT"
* #1707517 ^designation[0].language = #de-AT 
* #1707517 ^designation[0].value = "HALOPERIDOL DECANOAT" 
* #1707518 "PROTEIN S"
* #1707518 ^designation[0].language = #de-AT 
* #1707518 ^designation[0].value = "PROTEIN S" 
* #1707524 "BROTIZOLAM"
* #1707524 ^designation[0].language = #de-AT 
* #1707524 ^designation[0].value = "BROTIZOLAM" 
* #1707525 "BETIATID"
* #1707525 ^designation[0].language = #de-AT 
* #1707525 ^designation[0].value = "BETIATID" 
* #1707527 "BICALUTAMID"
* #1707527 ^designation[0].language = #de-AT 
* #1707527 ^designation[0].value = "BICALUTAMID" 
* #1707528 "BEZAFIBRAT"
* #1707528 ^designation[0].language = #de-AT 
* #1707528 ^designation[0].value = "BEZAFIBRAT" 
* #1707535 "OMEPRAZOL MAGNESIUM"
* #1707535 ^designation[0].language = #de-AT 
* #1707535 ^designation[0].value = "OMEPRAZOL MAGNESIUM" 
* #1707541 "OCTENIDIN DIHYDROCHLORID"
* #1707541 ^designation[0].language = #de-AT 
* #1707541 ^designation[0].value = "OCTENIDIN DIHYDROCHLORID" 
* #1707553 "NATRIUMSELENAT"
* #1707553 ^designation[0].language = #de-AT 
* #1707553 ^designation[0].value = "NATRIUMSELENAT" 
* #1707560 "THIOCTSÄURE"
* #1707560 ^designation[0].language = #de-AT 
* #1707560 ^designation[0].value = "THIOCTSÄURE" 
* #1707562 "ASCORBINSÄURE"
* #1707562 ^designation[0].language = #de-AT 
* #1707562 ^designation[0].value = "ASCORBINSÄURE" 
* #1707563 "NICOMORPHIN HYDROCHLORID"
* #1707563 ^designation[0].language = #de-AT 
* #1707563 ^designation[0].value = "NICOMORPHIN HYDROCHLORID" 
* #1707568 "NICOTINDITARTRAT"
* #1707568 ^designation[0].language = #de-AT 
* #1707568 ^designation[0].value = "NICOTINDITARTRAT" 
* #1707595 "MEBEVERIN HYDROCHLORID"
* #1707595 ^designation[0].language = #de-AT 
* #1707595 ^designation[0].value = "MEBEVERIN HYDROCHLORID" 
* #1707606 "ALPHA-TOCOPHEROL"
* #1707606 ^designation[0].language = #de-AT 
* #1707606 ^designation[0].value = "ALPHA-TOCOPHEROL" 
* #1707610 "HUMANES PAPILLOMVIRUS-TYP 11 L1-PROTEIN"
* #1707610 ^designation[0].language = #de-AT 
* #1707610 ^designation[0].value = "HUMANES PAPILLOMVIRUS-TYP 11 L1-PROTEIN" 
* #1707611 "HUMANES PAPILLOMVIRUS-TYP 6 L1-PROTEIN"
* #1707611 ^designation[0].language = #de-AT 
* #1707611 ^designation[0].value = "HUMANES PAPILLOMVIRUS-TYP 6 L1-PROTEIN" 
* #1707612 "HUMANES PAPILLOMVIRUS-TYP 16 L1-PROTEIN"
* #1707612 ^designation[0].language = #de-AT 
* #1707612 ^designation[0].value = "HUMANES PAPILLOMVIRUS-TYP 16 L1-PROTEIN" 
* #1707613 "HUMANES PAPILLOMVIRUS-TYP 18 L1-PROTEIN"
* #1707613 ^designation[0].language = #de-AT 
* #1707613 ^designation[0].value = "HUMANES PAPILLOMVIRUS-TYP 18 L1-PROTEIN" 
* #1707626 "BENZYLALKOHOL"
* #1707626 ^designation[0].language = #de-AT 
* #1707626 ^designation[0].value = "BENZYLALKOHOL" 
* #1707628 "MELITRACEN HYDROCHLORID"
* #1707628 ^designation[0].language = #de-AT 
* #1707628 ^designation[0].value = "MELITRACEN HYDROCHLORID" 
* #1707654 "NEOSTIGMIN METILSULFAT"
* #1707654 ^designation[0].language = #de-AT 
* #1707654 ^designation[0].value = "NEOSTIGMIN METILSULFAT" 
* #1707667 "KALIUMMONOHYDROGENPHOSPHAT"
* #1707667 ^designation[0].language = #de-AT 
* #1707667 ^designation[0].value = "KALIUMMONOHYDROGENPHOSPHAT" 
* #1707671 "PIRITRAMID"
* #1707671 ^designation[0].language = #de-AT 
* #1707671 ^designation[0].value = "PIRITRAMID" 
* #1707674 "ARGININ"
* #1707674 ^designation[0].language = #de-AT 
* #1707674 ^designation[0].value = "ARGININ" 
* #1707678 "ARTICAIN"
* #1707678 ^designation[0].language = #de-AT 
* #1707678 ^designation[0].value = "ARTICAIN" 
* #1707681 "ATOVAQUON"
* #1707681 ^designation[0].language = #de-AT 
* #1707681 ^designation[0].value = "ATOVAQUON" 
* #1707682 "LENOGRASTIM"
* #1707682 ^designation[0].language = #de-AT 
* #1707682 ^designation[0].value = "LENOGRASTIM" 
* #1707684 "D-GLUCOSE 1-PHOSPHAT DINATRIUMSALZ"
* #1707684 ^designation[0].language = #de-AT 
* #1707684 ^designation[0].value = "D-GLUCOSE 1-PHOSPHAT DINATRIUMSALZ" 
* #1707687 "AURANOFIN"
* #1707687 ^designation[0].language = #de-AT 
* #1707687 ^designation[0].value = "AURANOFIN" 
* #1707689 "GOSERELIN ACETAT"
* #1707689 ^designation[0].language = #de-AT 
* #1707689 ^designation[0].value = "GOSERELIN ACETAT" 
* #1707693 "HISTRELIN ACETAT"
* #1707693 ^designation[0].language = #de-AT 
* #1707693 ^designation[0].value = "HISTRELIN ACETAT" 
* #1707694 "HYALURONIDASE"
* #1707694 ^designation[0].language = #de-AT 
* #1707694 ^designation[0].value = "HYALURONIDASE" 
* #1707699 "HEXOPRENALIN SULFAT"
* #1707699 ^designation[0].language = #de-AT 
* #1707699 ^designation[0].value = "HEXOPRENALIN SULFAT" 
* #1707705 "SACCHAROSE"
* #1707705 ^designation[0].language = #de-AT 
* #1707705 ^designation[0].value = "SACCHAROSE" 
* #1707718 "AZACITIDIN"
* #1707718 ^designation[0].language = #de-AT 
* #1707718 ^designation[0].value = "AZACITIDIN" 
* #1707726 "AMYLMETACRESOL"
* #1707726 ^designation[0].language = #de-AT 
* #1707726 ^designation[0].value = "AMYLMETACRESOL" 
* #1707729 "NATRIUM FLUCLOXACILLINAT"
* #1707729 ^designation[0].language = #de-AT 
* #1707729 ^designation[0].value = "NATRIUM FLUCLOXACILLINAT" 
* #1707734 "INDINAVIR SULFAT"
* #1707734 ^designation[0].language = #de-AT 
* #1707734 ^designation[0].value = "INDINAVIR SULFAT" 
* #1707741 "FUSAFUNGIN"
* #1707741 ^designation[0].language = #de-AT 
* #1707741 ^designation[0].value = "FUSAFUNGIN" 
* #1707753 "CARMELLOSE NATRIUM"
* #1707753 ^designation[0].language = #de-AT 
* #1707753 ^designation[0].value = "CARMELLOSE NATRIUM" 
* #1707758 "MILCHSÄURE"
* #1707758 ^designation[0].language = #de-AT 
* #1707758 ^designation[0].value = "MILCHSÄURE" 
* #1707759 "HYPROMELLOSE"
* #1707759 ^designation[0].language = #de-AT 
* #1707759 ^designation[0].value = "HYPROMELLOSE" 
* #1707760 "ISOPRENALIN HYDROCHLORID"
* #1707760 ^designation[0].language = #de-AT 
* #1707760 ^designation[0].value = "ISOPRENALIN HYDROCHLORID" 
* #1707763 "DIDANOSIN"
* #1707763 ^designation[0].language = #de-AT 
* #1707763 ^designation[0].value = "DIDANOSIN" 
* #1707772 "AMIDEFRIN MESILAT"
* #1707772 ^designation[0].language = #de-AT 
* #1707772 ^designation[0].value = "AMIDEFRIN MESILAT" 
* #1707775 "AMISULPRID"
* #1707775 ^designation[0].language = #de-AT 
* #1707775 ^designation[0].value = "AMISULPRID" 
* #1707777 "GLUCAGON HYDROCHLORID"
* #1707777 ^designation[0].language = #de-AT 
* #1707777 ^designation[0].value = "GLUCAGON HYDROCHLORID" 
* #1707779 "EPINASTIN HYDROCHLORID"
* #1707779 ^designation[0].language = #de-AT 
* #1707779 ^designation[0].value = "EPINASTIN HYDROCHLORID" 
* #1707788 "EPINEPHRIN HYDROGENTARTRAT"
* #1707788 ^designation[0].language = #de-AT 
* #1707788 ^designation[0].value = "EPINEPHRIN HYDROGENTARTRAT" 
* #1707790 "ESMOLOL HYDROCHLORID"
* #1707790 ^designation[0].language = #de-AT 
* #1707790 ^designation[0].value = "ESMOLOL HYDROCHLORID" 
* #1707794 "ETHAMBUTOL DIHYDROCHLORID"
* #1707794 ^designation[0].language = #de-AT 
* #1707794 ^designation[0].value = "ETHAMBUTOL DIHYDROCHLORID" 
* #1707799 "DINATRIUM ESTRAMUSTIN PHOSPHAT"
* #1707799 ^designation[0].language = #de-AT 
* #1707799 ^designation[0].value = "DINATRIUM ESTRAMUSTIN PHOSPHAT" 
* #1707809 "ZINN(II)CHLORID DIHYDRAT"
* #1707809 ^designation[0].language = #de-AT 
* #1707809 ^designation[0].value = "ZINN(II)CHLORID DIHYDRAT" 
* #1707810 "GENTAMICINSULFAT"
* #1707810 ^designation[0].language = #de-AT 
* #1707810 ^designation[0].value = "GENTAMICINSULFAT" 
* #1707831 "ETILEFRIN HYDROCHLORID"
* #1707831 ^designation[0].language = #de-AT 
* #1707831 ^designation[0].value = "ETILEFRIN HYDROCHLORID" 
* #1707836 "DIHYDROCODEIN THIOCYANAT"
* #1707836 ^designation[0].language = #de-AT 
* #1707836 ^designation[0].value = "DIHYDROCODEIN THIOCYANAT" 
* #1707840 "DIMETINDEN MALEAT"
* #1707840 ^designation[0].language = #de-AT 
* #1707840 ^designation[0].value = "DIMETINDEN MALEAT" 
* #1707841 "DOBUTAMIN HYDROCHLORID"
* #1707841 ^designation[0].language = #de-AT 
* #1707841 ^designation[0].value = "DOBUTAMIN HYDROCHLORID" 
* #1707843 "DIPHENYLPYRALIN HYDROCHLORID"
* #1707843 ^designation[0].language = #de-AT 
* #1707843 ^designation[0].value = "DIPHENYLPYRALIN HYDROCHLORID" 
* #1707851 "L-CYSTEIN"
* #1707851 ^designation[0].language = #de-AT 
* #1707851 ^designation[0].value = "L-CYSTEIN" 
* #1707852 "KONJUGIERTE ESTROGENE"
* #1707852 ^designation[0].language = #de-AT 
* #1707852 ^designation[0].value = "KONJUGIERTE ESTROGENE" 
* #1707857 "MAGNESIUMOXID"
* #1707857 ^designation[0].language = #de-AT 
* #1707857 ^designation[0].value = "MAGNESIUMOXID" 
* #1707860 "GLYCEROLTRINITRAT"
* #1707860 ^designation[0].language = #de-AT 
* #1707860 ^designation[0].value = "GLYCEROLTRINITRAT" 
* #1707863 "FOENICULI AMARI FRUCTUS AETHEROLEUM"
* #1707863 ^designation[0].language = #de-AT 
* #1707863 ^designation[0].value = "FOENICULI AMARI FRUCTUS AETHEROLEUM" 
* #1707864 "LIQUIRITIAE RADIX (AUSZUG)"
* #1707864 ^designation[0].language = #de-AT 
* #1707864 ^designation[0].value = "LIQUIRITIAE RADIX (AUSZUG)" 
* #1707865 "ANISI AETHEROLEUM"
* #1707865 ^designation[0].language = #de-AT 
* #1707865 ^designation[0].value = "ANISI AETHEROLEUM" 
* #1707866 "ADAPALEN"
* #1707866 ^designation[0].language = #de-AT 
* #1707866 ^designation[0].value = "ADAPALEN" 
* #1707870 "ALBENDAZOL"
* #1707870 ^designation[0].language = #de-AT 
* #1707870 ^designation[0].value = "ALBENDAZOL" 
* #1707893 "DESMOPRESSIN ACETAT"
* #1707893 ^designation[0].language = #de-AT 
* #1707893 ^designation[0].value = "DESMOPRESSIN ACETAT" 
* #1707902 "LACTOBACILLUS ACIDOPHILUS (AUSZUG, PRODUKTE)"
* #1707902 ^designation[0].language = #de-AT 
* #1707902 ^designation[0].value = "LACTOBACILLUS ACIDOPHILUS (AUSZUG, PRODUKTE)" 
* #1707903 "PLANTAGINIS OVATAE SEMINIS TEGUMENTUM"
* #1707903 ^designation[0].language = #de-AT 
* #1707903 ^designation[0].value = "PLANTAGINIS OVATAE SEMINIS TEGUMENTUM" 
* #1707911 "VINDESIN SULFAT"
* #1707911 ^designation[0].language = #de-AT 
* #1707911 ^designation[0].value = "VINDESIN SULFAT" 
* #1707919 "VINBLASTIN SULFAT"
* #1707919 ^designation[0].language = #de-AT 
* #1707919 ^designation[0].value = "VINBLASTIN SULFAT" 
* #1707925 "ACEMETACIN"
* #1707925 ^designation[0].language = #de-AT 
* #1707925 ^designation[0].value = "ACEMETACIN" 
* #1707941 "CYCLIZIN HYDROCHLORID"
* #1707941 ^designation[0].language = #de-AT 
* #1707941 ^designation[0].value = "CYCLIZIN HYDROCHLORID" 
* #1707942 "SUNITINIB"
* #1707942 ^designation[0].language = #de-AT 
* #1707942 ^designation[0].value = "SUNITINIB" 
* #1707944 "COLESTYRAMIN"
* #1707944 ^designation[0].language = #de-AT 
* #1707944 ^designation[0].value = "COLESTYRAMIN" 
* #1707948 "IDURSULFASE"
* #1707948 ^designation[0].language = #de-AT 
* #1707948 ^designation[0].value = "IDURSULFASE" 
* #1707949 "VALSARTAN"
* #1707949 ^designation[0].language = #de-AT 
* #1707949 ^designation[0].value = "VALSARTAN" 
* #1707951 "ZIDOVUDIN"
* #1707951 ^designation[0].language = #de-AT 
* #1707951 ^designation[0].value = "ZIDOVUDIN" 
* #1707953 "DASATINIB"
* #1707953 ^designation[0].language = #de-AT 
* #1707953 ^designation[0].value = "DASATINIB" 
* #1707955 "VORICONAZOL"
* #1707955 ^designation[0].language = #de-AT 
* #1707955 ^designation[0].value = "VORICONAZOL" 
* #1707956 "ZONISAMID"
* #1707956 ^designation[0].language = #de-AT 
* #1707956 ^designation[0].value = "ZONISAMID" 
* #1707957 "VIBRIO CHOLERAE"
* #1707957 ^designation[0].language = #de-AT 
* #1707957 ^designation[0].value = "VIBRIO CHOLERAE" 
* #1707958 "ROTAVIRUS"
* #1707958 ^designation[0].language = #de-AT 
* #1707958 ^designation[0].value = "ROTAVIRUS" 
* #1707962 "RANIBIZUMAB"
* #1707962 ^designation[0].language = #de-AT 
* #1707962 ^designation[0].value = "RANIBIZUMAB" 
* #1707966 "DINATRIUM CLODRONAT"
* #1707966 ^designation[0].language = #de-AT 
* #1707966 ^designation[0].value = "DINATRIUM CLODRONAT" 
* #1707968 "PINI PUMILIONIS AETHEROLEUM"
* #1707968 ^designation[0].language = #de-AT 
* #1707968 ^designation[0].value = "PINI PUMILIONIS AETHEROLEUM" 
* #1707969 "HEFE"
* #1707969 ^designation[0].language = #de-AT 
* #1707969 ^designation[0].value = "HEFE" 
* #1707971 "2-(2-CARBAMOYLPHENOXY)ESSIGSÄURE"
* #1707971 ^designation[0].language = #de-AT 
* #1707971 ^designation[0].value = "2-(2-CARBAMOYLPHENOXY)ESSIGSÄURE" 
* #1707974 "IMIPENEM"
* #1707974 ^designation[0].language = #de-AT 
* #1707974 ^designation[0].value = "IMIPENEM" 
* #1707981 "DINATRIUM FOSFOMYCINAT"
* #1707981 ^designation[0].language = #de-AT 
* #1707981 ^designation[0].value = "DINATRIUM FOSFOMYCINAT" 
* #1707983 "CISPLATIN"
* #1707983 ^designation[0].language = #de-AT 
* #1707983 ^designation[0].value = "CISPLATIN" 
* #1707986 "VINORELBINTARTRAT"
* #1707986 ^designation[0].language = #de-AT 
* #1707986 ^designation[0].value = "VINORELBINTARTRAT" 
* #1707987 "DINATRIUMSELENIT"
* #1707987 ^designation[0].language = #de-AT 
* #1707987 ^designation[0].value = "DINATRIUMSELENIT" 
* #1707989 "DOCOSANOL"
* #1707989 ^designation[0].language = #de-AT 
* #1707989 ^designation[0].value = "DOCOSANOL" 
* #1707990 "NALBUPHIN HYDROCHLORID"
* #1707990 ^designation[0].language = #de-AT 
* #1707990 ^designation[0].value = "NALBUPHIN HYDROCHLORID" 
* #1707991 "METHOHEXITAL NATRIUM"
* #1707991 ^designation[0].language = #de-AT 
* #1707991 ^designation[0].value = "METHOHEXITAL NATRIUM" 
* #1707993 "PHENYLEPHRIN"
* #1707993 ^designation[0].language = #de-AT 
* #1707993 ^designation[0].value = "PHENYLEPHRIN" 
* #1707997 "BROMELAIN"
* #1707997 ^designation[0].language = #de-AT 
* #1707997 ^designation[0].value = "BROMELAIN" 
* #1708002 "TIANEPTIN NATRIUM"
* #1708002 ^designation[0].language = #de-AT 
* #1708002 ^designation[0].value = "TIANEPTIN NATRIUM" 
* #1708003 "ZUCLOPENTHIXOL ACETAT"
* #1708003 ^designation[0].language = #de-AT 
* #1708003 ^designation[0].value = "ZUCLOPENTHIXOL ACETAT" 
* #1708006 "N-ACETYLTYROSIN"
* #1708006 ^designation[0].language = #de-AT 
* #1708006 ^designation[0].value = "N-ACETYLTYROSIN" 
* #1708007 "C1-ESTERASE-INHIBITOR"
* #1708007 ^designation[0].language = #de-AT 
* #1708007 ^designation[0].value = "C1-ESTERASE-INHIBITOR" 
* #1708011 "CALCIUMPHOSPHAT"
* #1708011 ^designation[0].language = #de-AT 
* #1708011 ^designation[0].value = "CALCIUMPHOSPHAT" 
* #1708016 "FENOTEROL HYDROBROMID"
* #1708016 ^designation[0].language = #de-AT 
* #1708016 ^designation[0].value = "FENOTEROL HYDROBROMID" 
* #1708017 "NOREPINEPHRINTARTRAT MONOHYDRAT"
* #1708017 ^designation[0].language = #de-AT 
* #1708017 ^designation[0].value = "NOREPINEPHRINTARTRAT MONOHYDRAT" 
* #1708021 "DEMECLOCYCLIN HYDROCHLORID"
* #1708021 ^designation[0].language = #de-AT 
* #1708021 ^designation[0].value = "DEMECLOCYCLIN HYDROCHLORID" 
* #1708022 "DACARBAZIN"
* #1708022 ^designation[0].language = #de-AT 
* #1708022 ^designation[0].value = "DACARBAZIN" 
* #1708023 "MOXONIDIN"
* #1708023 ^designation[0].language = #de-AT 
* #1708023 ^designation[0].value = "MOXONIDIN" 
* #1708030 "FROVATRIPTAN SUCCINAT"
* #1708030 ^designation[0].language = #de-AT 
* #1708030 ^designation[0].value = "FROVATRIPTAN SUCCINAT" 
* #1708031 "FLUPENTIXOL DECANOAT"
* #1708031 ^designation[0].language = #de-AT 
* #1708031 ^designation[0].value = "FLUPENTIXOL DECANOAT" 
* #1708037 "BUMETANID"
* #1708037 ^designation[0].language = #de-AT 
* #1708037 ^designation[0].value = "BUMETANID" 
* #1708039 "AMOROLFIN HYDROCHLORID"
* #1708039 ^designation[0].language = #de-AT 
* #1708039 ^designation[0].value = "AMOROLFIN HYDROCHLORID" 
* #1708040 "ARGININ ASPARTAT"
* #1708040 ^designation[0].language = #de-AT 
* #1708040 ^designation[0].value = "ARGININ ASPARTAT" 
* #1708042 "HESPERIDIN"
* #1708042 ^designation[0].language = #de-AT 
* #1708042 ^designation[0].value = "HESPERIDIN" 
* #1708051 "FENFLURAMIN HYDROCHLORID"
* #1708051 ^designation[0].language = #de-AT 
* #1708051 ^designation[0].value = "FENFLURAMIN HYDROCHLORID" 
* #1708055 "IODIXANOL"
* #1708055 ^designation[0].language = #de-AT 
* #1708055 ^designation[0].value = "IODIXANOL" 
* #1708059 "THIAMIN DISULFID"
* #1708059 ^designation[0].language = #de-AT 
* #1708059 ^designation[0].value = "THIAMIN DISULFID" 
* #1708060 "KUPFERSULFAT"
* #1708060 ^designation[0].language = #de-AT 
* #1708060 ^designation[0].value = "KUPFERSULFAT" 
* #1708065 "BUTIZID"
* #1708065 ^designation[0].language = #de-AT 
* #1708065 ^designation[0].value = "BUTIZID" 
* #1708066 "BRIVUDIN"
* #1708066 ^designation[0].language = #de-AT 
* #1708066 ^designation[0].value = "BRIVUDIN" 
* #1708069 "ALUMINIUMHYDROXID"
* #1708069 ^designation[0].language = #de-AT 
* #1708069 ^designation[0].value = "ALUMINIUMHYDROXID" 
* #1708073 "BROMFENAC"
* #1708073 ^designation[0].language = #de-AT 
* #1708073 ^designation[0].value = "BROMFENAC" 
* #1708088 "FLUNARIZIN DIHYDROCHLORID"
* #1708088 ^designation[0].language = #de-AT 
* #1708088 ^designation[0].value = "FLUNARIZIN DIHYDROCHLORID" 
* #1708100 "TETRACAIN"
* #1708100 ^designation[0].language = #de-AT 
* #1708100 ^designation[0].value = "TETRACAIN" 
* #1708101 "PERINDOPRIL ARGININ"
* #1708101 ^designation[0].language = #de-AT 
* #1708101 ^designation[0].value = "PERINDOPRIL ARGININ" 
* #1708102 "SULTIAM"
* #1708102 ^designation[0].language = #de-AT 
* #1708102 ^designation[0].value = "SULTIAM" 
* #1708103 "ZIPRASIDONHYDROCHLORID"
* #1708103 ^designation[0].language = #de-AT 
* #1708103 ^designation[0].value = "ZIPRASIDONHYDROCHLORID" 
* #1708106 "PROTEIN-SILBER"
* #1708106 ^designation[0].language = #de-AT 
* #1708106 ^designation[0].value = "PROTEIN-SILBER" 
* #1708107 "CORTICORELIN TRIFLUTAT"
* #1708107 ^designation[0].language = #de-AT 
* #1708107 ^designation[0].value = "CORTICORELIN TRIFLUTAT" 
* #1708114 "CORYNEBACTERIUM DIPHTHERIAE (AUSZUG, PRODUKTE)"
* #1708114 ^designation[0].language = #de-AT 
* #1708114 ^designation[0].value = "CORYNEBACTERIUM DIPHTHERIAE (AUSZUG, PRODUKTE)" 
* #1708117 "ERYTHROMYCIN LACTOBIONAT"
* #1708117 ^designation[0].language = #de-AT 
* #1708117 ^designation[0].value = "ERYTHROMYCIN LACTOBIONAT" 
* #1708126 "CALCIUMGLUCONAT"
* #1708126 ^designation[0].language = #de-AT 
* #1708126 ^designation[0].value = "CALCIUMGLUCONAT" 
* #1708127 "SULFADIAZIN SILBER"
* #1708127 ^designation[0].language = #de-AT 
* #1708127 ^designation[0].value = "SULFADIAZIN SILBER" 
* #1708138 "LACTOBIONSÄURE"
* #1708138 ^designation[0].language = #de-AT 
* #1708138 ^designation[0].value = "LACTOBIONSÄURE" 
* #1708140 "ESSIGSÄURE"
* #1708140 ^designation[0].language = #de-AT 
* #1708140 ^designation[0].value = "ESSIGSÄURE" 
* #1708141 "TESTOSTERON UNDECENOAT"
* #1708141 ^designation[0].language = #de-AT 
* #1708141 ^designation[0].value = "TESTOSTERON UNDECENOAT" 
* #1708142 "RIBOFLAVIN"
* #1708142 ^designation[0].language = #de-AT 
* #1708142 ^designation[0].value = "RIBOFLAVIN" 
* #1708144 "NICOTINRESINAT"
* #1708144 ^designation[0].language = #de-AT 
* #1708144 ^designation[0].value = "NICOTINRESINAT" 
* #1708146 "HUMANALBUMIN"
* #1708146 ^designation[0].language = #de-AT 
* #1708146 ^designation[0].value = "HUMANALBUMIN" 
* #1708147 "NATRIUM RIBOFLAVINPHOSPHAT"
* #1708147 ^designation[0].language = #de-AT 
* #1708147 ^designation[0].value = "NATRIUM RIBOFLAVINPHOSPHAT" 
* #1708152 "HYDROXOCOBALAMIN ACETAT"
* #1708152 ^designation[0].language = #de-AT 
* #1708152 ^designation[0].value = "HYDROXOCOBALAMIN ACETAT" 
* #1708155 "PYRANTEL EMBONAT"
* #1708155 ^designation[0].language = #de-AT 
* #1708155 ^designation[0].value = "PYRANTEL EMBONAT" 
* #1708158 "LEVOFLOXACIN"
* #1708158 ^designation[0].language = #de-AT 
* #1708158 ^designation[0].value = "LEVOFLOXACIN" 
* #1708166 "GERINNUNGSFAKTOR IX"
* #1708166 ^designation[0].language = #de-AT 
* #1708166 ^designation[0].value = "GERINNUNGSFAKTOR IX" 
* #1708167 "NATRIUMALENDRONAT"
* #1708167 ^designation[0].language = #de-AT 
* #1708167 ^designation[0].value = "NATRIUMALENDRONAT" 
* #1708168 "GERINNUNGSFAKTOR VII"
* #1708168 ^designation[0].language = #de-AT 
* #1708168 ^designation[0].value = "GERINNUNGSFAKTOR VII" 
* #1708172 "ANTITHROMBIN III"
* #1708172 ^designation[0].language = #de-AT 
* #1708172 ^designation[0].value = "ANTITHROMBIN III" 
* #1708173 "GERINNUNGSFAKTOR II"
* #1708173 ^designation[0].language = #de-AT 
* #1708173 ^designation[0].value = "GERINNUNGSFAKTOR II" 
* #1708174 "GERINNUNGSFAKTOR VIII"
* #1708174 ^designation[0].language = #de-AT 
* #1708174 ^designation[0].value = "GERINNUNGSFAKTOR VIII" 
* #1708175 "HEPATITIS-B-VIRUS-ANTIGEN"
* #1708175 ^designation[0].language = #de-AT 
* #1708175 ^designation[0].value = "HEPATITIS-B-VIRUS-ANTIGEN" 
* #1708176 "GERINNUNGSFAKTOR X"
* #1708176 ^designation[0].language = #de-AT 
* #1708176 ^designation[0].value = "GERINNUNGSFAKTOR X" 
* #1708178 "BLUTPLASMA"
* #1708178 ^designation[0].language = #de-AT 
* #1708178 ^designation[0].value = "BLUTPLASMA" 
* #1708179 "ZINKSULFAT"
* #1708179 ^designation[0].language = #de-AT 
* #1708179 ^designation[0].value = "ZINKSULFAT" 
* #1708180 "ECULIZUMAB"
* #1708180 ^designation[0].language = #de-AT 
* #1708180 ^designation[0].value = "ECULIZUMAB" 
* #1708181 "NATRIUM 4-HYDROXYBUTYRAT"
* #1708181 ^designation[0].language = #de-AT 
* #1708181 ^designation[0].value = "NATRIUM 4-HYDROXYBUTYRAT" 
* #1708183 "EMEDASTIN DIFUMARAT"
* #1708183 ^designation[0].language = #de-AT 
* #1708183 ^designation[0].value = "EMEDASTIN DIFUMARAT" 
* #1708184 "EPOETIN ZETA"
* #1708184 ^designation[0].language = #de-AT 
* #1708184 ^designation[0].value = "EPOETIN ZETA" 
* #1708185 "VILDAGLIPTIN"
* #1708185 ^designation[0].language = #de-AT 
* #1708185 ^designation[0].value = "VILDAGLIPTIN" 
* #1708186 "ZICONOTID ACETAT"
* #1708186 ^designation[0].language = #de-AT 
* #1708186 ^designation[0].value = "ZICONOTID ACETAT" 
* #1708187 "RANOLAZIN"
* #1708187 ^designation[0].language = #de-AT 
* #1708187 ^designation[0].value = "RANOLAZIN" 
* #1708188 "RIBAVIRIN"
* #1708188 ^designation[0].language = #de-AT 
* #1708188 ^designation[0].value = "RIBAVIRIN" 
* #1708189 "RITONAVIR"
* #1708189 ^designation[0].language = #de-AT 
* #1708189 ^designation[0].value = "RITONAVIR" 
* #1708191 "SIROLIMUS"
* #1708191 ^designation[0].language = #de-AT 
* #1708191 ^designation[0].value = "SIROLIMUS" 
* #1708193 "TACROLIMUS"
* #1708193 ^designation[0].language = #de-AT 
* #1708193 ^designation[0].value = "TACROLIMUS" 
* #1708195 "RETEPLASE"
* #1708195 ^designation[0].language = #de-AT 
* #1708195 ^designation[0].value = "RETEPLASE" 
* #1708196 "RILUZOL"
* #1708196 ^designation[0].language = #de-AT 
* #1708196 ^designation[0].value = "RILUZOL" 
* #1708197 "RIVASTIGMIN"
* #1708197 ^designation[0].language = #de-AT 
* #1708197 ^designation[0].value = "RIVASTIGMIN" 
* #1708198 "RUFINAMID"
* #1708198 ^designation[0].language = #de-AT 
* #1708198 ^designation[0].value = "RUFINAMID" 
* #1708199 "SILDENAFIL"
* #1708199 ^designation[0].language = #de-AT 
* #1708199 ^designation[0].value = "SILDENAFIL" 
* #1708200 "SOMATROPIN"
* #1708200 ^designation[0].language = #de-AT 
* #1708200 ^designation[0].value = "SOMATROPIN" 
* #1708201 "SUFENTANIL"
* #1708201 ^designation[0].language = #de-AT 
* #1708201 ^designation[0].value = "SUFENTANIL" 
* #1708202 "TELMISARTAN"
* #1708202 ^designation[0].language = #de-AT 
* #1708202 ^designation[0].value = "TELMISARTAN" 
* #1708203 "TESTOSTERON"
* #1708203 ^designation[0].language = #de-AT 
* #1708203 ^designation[0].value = "TESTOSTERON" 
* #1708204 "TIMOLOL"
* #1708204 ^designation[0].language = #de-AT 
* #1708204 ^designation[0].value = "TIMOLOL" 
* #1708205 "TERIPARATID"
* #1708205 ^designation[0].language = #de-AT 
* #1708205 ^designation[0].value = "TERIPARATID" 
* #1708206 "THALIDOMID"
* #1708206 ^designation[0].language = #de-AT 
* #1708206 ^designation[0].value = "THALIDOMID" 
* #1708207 "TOLCAPON"
* #1708207 ^designation[0].language = #de-AT 
* #1708207 ^designation[0].value = "TOLCAPON" 
* #1708208 "HISTAMINDIHYDROCHLORID"
* #1708208 ^designation[0].language = #de-AT 
* #1708208 ^designation[0].value = "HISTAMINDIHYDROCHLORID" 
* #1708209 "SAXAGLIPTIN"
* #1708209 ^designation[0].language = #de-AT 
* #1708209 ^designation[0].value = "SAXAGLIPTIN" 
* #1708210 "NILOTINIB"
* #1708210 ^designation[0].language = #de-AT 
* #1708210 ^designation[0].value = "NILOTINIB" 
* #1708211 "LIRAGLUTID"
* #1708211 ^designation[0].language = #de-AT 
* #1708211 ^designation[0].value = "LIRAGLUTID" 
* #1708212 "GELBFIEBERVIRUS"
* #1708212 ^designation[0].language = #de-AT 
* #1708212 ^designation[0].value = "GELBFIEBERVIRUS" 
* #1708213 "HEPATITIS-A-VIRUS"
* #1708213 ^designation[0].language = #de-AT 
* #1708213 ^designation[0].value = "HEPATITIS-A-VIRUS" 
* #1708214 "BORDETELLA PERTUSSIS"
* #1708214 ^designation[0].language = #de-AT 
* #1708214 ^designation[0].value = "BORDETELLA PERTUSSIS" 
* #1708215 "OFATUMUMAB"
* #1708215 ^designation[0].language = #de-AT 
* #1708215 ^designation[0].value = "OFATUMUMAB" 
* #1708216 "MARAVIROC"
* #1708216 ^designation[0].language = #de-AT 
* #1708216 ^designation[0].value = "MARAVIROC" 
* #1708217 "MERCAPTAMIN HYDROCHLORID"
* #1708217 ^designation[0].language = #de-AT 
* #1708217 ^designation[0].value = "MERCAPTAMIN HYDROCHLORID" 
* #1708218 "TELBIVUDIN"
* #1708218 ^designation[0].language = #de-AT 
* #1708218 ^designation[0].value = "TELBIVUDIN" 
* #1708219 "CLOSTRIDIUM TETANI (AUSZUG, PRODUKTE)"
* #1708219 ^designation[0].language = #de-AT 
* #1708219 ^designation[0].value = "CLOSTRIDIUM TETANI (AUSZUG, PRODUKTE)" 
* #1708220 "MUMPSVIRUS"
* #1708220 ^designation[0].language = #de-AT 
* #1708220 ^designation[0].value = "MUMPSVIRUS" 
* #1708221 "AGALSIDASE BETA"
* #1708221 ^designation[0].language = #de-AT 
* #1708221 ^designation[0].value = "AGALSIDASE BETA" 
* #1708222 "DIARSENTRIOXID"
* #1708222 ^designation[0].language = #de-AT 
* #1708222 ^designation[0].value = "DIARSENTRIOXID" 
* #1708223 "CASPOFUNGIN ACETAT"
* #1708223 ^designation[0].language = #de-AT 
* #1708223 ^designation[0].value = "CASPOFUNGIN ACETAT" 
* #1708225 "EPTIFIBATID"
* #1708225 ^designation[0].language = #de-AT 
* #1708225 ^designation[0].value = "EPTIFIBATID" 
* #1708226 "FONDAPARINUX NATRIUM"
* #1708226 ^designation[0].language = #de-AT 
* #1708226 ^designation[0].value = "FONDAPARINUX NATRIUM" 
* #1708228 "OMALIZUMAB"
* #1708228 ^designation[0].language = #de-AT 
* #1708228 ^designation[0].value = "OMALIZUMAB" 
* #1708229 "RITUXIMAB"
* #1708229 ^designation[0].language = #de-AT 
* #1708229 ^designation[0].value = "RITUXIMAB" 
* #1708230 "BIMATOPROST"
* #1708230 ^designation[0].language = #de-AT 
* #1708230 ^designation[0].value = "BIMATOPROST" 
* #1708231 "CAPECITABIN"
* #1708231 ^designation[0].language = #de-AT 
* #1708231 ^designation[0].value = "CAPECITABIN" 
* #1708232 "CETRORELIX ACETAT"
* #1708232 ^designation[0].language = #de-AT 
* #1708232 ^designation[0].value = "CETRORELIX ACETAT" 
* #1708234 "FOLLITROPIN BETA"
* #1708234 ^designation[0].language = #de-AT 
* #1708234 ^designation[0].value = "FOLLITROPIN BETA" 
* #1708236 "PARECOXIB NATRIUM"
* #1708236 ^designation[0].language = #de-AT 
* #1708236 ^designation[0].value = "PARECOXIB NATRIUM" 
* #1708237 "TADALAFIL"
* #1708237 ^designation[0].language = #de-AT 
* #1708237 ^designation[0].value = "TADALAFIL" 
* #1708239 "TRAVOPROST"
* #1708239 ^designation[0].language = #de-AT 
* #1708239 ^designation[0].value = "TRAVOPROST" 
* #1708241 "TRASTUZUMAB"
* #1708241 ^designation[0].language = #de-AT 
* #1708241 ^designation[0].value = "TRASTUZUMAB" 
* #1708242 "ZINKDIACETAT"
* #1708242 ^designation[0].language = #de-AT 
* #1708242 ^designation[0].value = "ZINKDIACETAT" 
* #1708243 "ZINKOXID"
* #1708243 ^designation[0].language = #de-AT 
* #1708243 ^designation[0].value = "ZINKOXID" 
* #1708244 "TANNINUM ALBUMINATUM"
* #1708244 ^designation[0].language = #de-AT 
* #1708244 ^designation[0].value = "TANNINUM ALBUMINATUM" 
* #1708245 "BETAIN"
* #1708245 ^designation[0].language = #de-AT 
* #1708245 ^designation[0].value = "BETAIN" 
* #1708246 "CLOPIDOGREL SULFAT"
* #1708246 ^designation[0].language = #de-AT 
* #1708246 ^designation[0].value = "CLOPIDOGREL SULFAT" 
* #1708247 "MASERNVIRUS"
* #1708247 ^designation[0].language = #de-AT 
* #1708247 ^designation[0].value = "MASERNVIRUS" 
* #1708249 "DARUNAVIR"
* #1708249 ^designation[0].language = #de-AT 
* #1708249 ^designation[0].value = "DARUNAVIR" 
* #1708250 "AMLODIPIN MALEAT"
* #1708250 ^designation[0].language = #de-AT 
* #1708250 ^designation[0].value = "AMLODIPIN MALEAT" 
* #1708253 "LAPATINIB"
* #1708253 ^designation[0].language = #de-AT 
* #1708253 ^designation[0].value = "LAPATINIB" 
* #1708254 "FEBUXOSTAT"
* #1708254 ^designation[0].language = #de-AT 
* #1708254 ^designation[0].value = "FEBUXOSTAT" 
* #1708255 "FOSAPREPITANT"
* #1708255 ^designation[0].language = #de-AT 
* #1708255 ^designation[0].value = "FOSAPREPITANT" 
* #1708256 "PANITUMUMAB"
* #1708256 ^designation[0].language = #de-AT 
* #1708256 ^designation[0].value = "PANITUMUMAB" 
* #1708257 "FAKTOR VIII-INHIBITOR-BYPASSING-AKTIVITÄT"
* #1708257 ^designation[0].language = #de-AT 
* #1708257 ^designation[0].value = "FAKTOR VIII-INHIBITOR-BYPASSING-AKTIVITÄT" 
* #1708258 "ALITRETINOIN"
* #1708258 ^designation[0].language = #de-AT 
* #1708258 ^designation[0].value = "ALITRETINOIN" 
* #1708260 "ATOSIBAN ACETAT"
* #1708260 ^designation[0].language = #de-AT 
* #1708260 ^designation[0].value = "ATOSIBAN ACETAT" 
* #1708261 "CELECOXIB"
* #1708261 ^designation[0].language = #de-AT 
* #1708261 ^designation[0].value = "CELECOXIB" 
* #1708262 "CORIFOLLITROPIN ALFA"
* #1708262 ^designation[0].language = #de-AT 
* #1708262 ^designation[0].value = "CORIFOLLITROPIN ALFA" 
* #1708263 "BEXAROTEN"
* #1708263 ^designation[0].language = #de-AT 
* #1708263 ^designation[0].value = "BEXAROTEN" 
* #1708264 "DESLORATADIN"
* #1708264 ^designation[0].language = #de-AT 
* #1708264 ^designation[0].value = "DESLORATADIN" 
* #1708265 "GANCICLOVIR NATRIUM"
* #1708265 ^designation[0].language = #de-AT 
* #1708265 ^designation[0].value = "GANCICLOVIR NATRIUM" 
* #1708267 "INTERFERON ALFA"
* #1708267 ^designation[0].language = #de-AT 
* #1708267 ^designation[0].value = "INTERFERON ALFA" 
* #1708268 "LOPINAVIR"
* #1708268 ^designation[0].language = #de-AT 
* #1708268 ^designation[0].value = "LOPINAVIR" 
* #1708269 "GERINNUNGSFAKTOR IX, REKOMBINANT (NONACOG ALFA)"
* #1708269 ^designation[0].language = #de-AT 
* #1708269 ^designation[0].value = "GERINNUNGSFAKTOR IX, REKOMBINANT (NONACOG ALFA)" 
* #1708270 "PRAMIPEXOL DIHYDROCHLORID"
* #1708270 ^designation[0].language = #de-AT 
* #1708270 ^designation[0].value = "PRAMIPEXOL DIHYDROCHLORID" 
* #1708271 "EFLORNITHIN HYDROCHLORID"
* #1708271 ^designation[0].language = #de-AT 
* #1708271 ^designation[0].value = "EFLORNITHIN HYDROCHLORID" 
* #1708273 "INFLIXIMAB"
* #1708273 ^designation[0].language = #de-AT 
* #1708273 ^designation[0].value = "INFLIXIMAB" 
* #1708274 "INSULIN DETEMIR"
* #1708274 ^designation[0].language = #de-AT 
* #1708274 ^designation[0].value = "INSULIN DETEMIR" 
* #1708275 "INTERFERON BETA"
* #1708275 ^designation[0].language = #de-AT 
* #1708275 ^designation[0].value = "INTERFERON BETA" 
* #1708276 "MERCAPTAMIN BITARTRAT"
* #1708276 ^designation[0].language = #de-AT 
* #1708276 ^designation[0].value = "MERCAPTAMIN BITARTRAT" 
* #1708277 "PEGVISOMANT"
* #1708277 ^designation[0].language = #de-AT 
* #1708277 ^designation[0].value = "PEGVISOMANT" 
* #1708279 "RASBURICASE"
* #1708279 ^designation[0].language = #de-AT 
* #1708279 ^designation[0].value = "RASBURICASE" 
* #1708280 "CATUMAXOMAB"
* #1708280 ^designation[0].language = #de-AT 
* #1708280 ^designation[0].value = "CATUMAXOMAB" 
* #1708281 "LACOSAMID"
* #1708281 ^designation[0].language = #de-AT 
* #1708281 ^designation[0].value = "LACOSAMID" 
* #1708282 "AMBRISENTAN"
* #1708282 ^designation[0].language = #de-AT 
* #1708282 ^designation[0].value = "AMBRISENTAN" 
* #1708283 "AXITINIB"
* #1708283 ^designation[0].language = #de-AT 
* #1708283 ^designation[0].value = "AXITINIB" 
* #1708284 "CERTOLIZUMAB PEGOL"
* #1708284 ^designation[0].language = #de-AT 
* #1708284 ^designation[0].value = "CERTOLIZUMAB PEGOL" 
* #1708285 "GONADOTROPES HORMON"
* #1708285 ^designation[0].language = #de-AT 
* #1708285 ^designation[0].value = "GONADOTROPES HORMON" 
* #1708286 "SEVELAMER"
* #1708286 ^designation[0].language = #de-AT 
* #1708286 ^designation[0].value = "SEVELAMER" 
* #1708287 "FULVESTRANT"
* #1708287 ^designation[0].language = #de-AT 
* #1708287 ^designation[0].value = "FULVESTRANT" 
* #1708289 "INTERFERON GAMMA"
* #1708289 ^designation[0].language = #de-AT 
* #1708289 ^designation[0].value = "INTERFERON GAMMA" 
* #1708290 "THROMBIN"
* #1708290 ^designation[0].language = #de-AT 
* #1708290 ^designation[0].value = "THROMBIN" 
* #1708292 "ZINKCITRAT"
* #1708292 ^designation[0].language = #de-AT 
* #1708292 ^designation[0].value = "ZINKCITRAT" 
* #1708294 "ZINKDIGLUCONAT"
* #1708294 ^designation[0].language = #de-AT 
* #1708294 ^designation[0].value = "ZINKDIGLUCONAT" 
* #1708296 "ATAZANAVIR"
* #1708296 ^designation[0].language = #de-AT 
* #1708296 ^designation[0].value = "ATAZANAVIR" 
* #1708297 "NATRIUM MYCOPHENOLAT"
* #1708297 ^designation[0].language = #de-AT 
* #1708297 ^designation[0].value = "NATRIUM MYCOPHENOLAT" 
* #1708298 "NATRIUM IBANDRONAT"
* #1708298 ^designation[0].language = #de-AT 
* #1708298 ^designation[0].value = "NATRIUM IBANDRONAT" 
* #1708300 "ADALIMUMAB"
* #1708300 ^designation[0].language = #de-AT 
* #1708300 ^designation[0].value = "ADALIMUMAB" 
* #1708301 "MIGLUSTAT"
* #1708301 ^designation[0].language = #de-AT 
* #1708301 ^designation[0].value = "MIGLUSTAT" 
* #1708302 "INSULIN GLULISIN"
* #1708302 ^designation[0].language = #de-AT 
* #1708302 ^designation[0].value = "INSULIN GLULISIN" 
* #1708304 "POSACONAZOL"
* #1708304 ^designation[0].language = #de-AT 
* #1708304 ^designation[0].value = "POSACONAZOL" 
* #1708305 "ZINK OROTAT"
* #1708305 ^designation[0].language = #de-AT 
* #1708305 ^designation[0].value = "ZINK OROTAT" 
* #1708307 "BEVACIZUMAB"
* #1708307 ^designation[0].language = #de-AT 
* #1708307 ^designation[0].value = "BEVACIZUMAB" 
* #1708308 "GERINNUNGSFAKTOR VIII, REKOMBINANT (MOROCTOCOG ALFA)"
* #1708308 ^designation[0].language = #de-AT 
* #1708308 ^designation[0].value = "GERINNUNGSFAKTOR VIII, REKOMBINANT (MOROCTOCOG ALFA)" 
* #1708309 "GERINNUNGSFAKTOR VIII, REKOMBINANT (OCTOCOG ALFA)"
* #1708309 ^designation[0].language = #de-AT 
* #1708309 ^designation[0].value = "GERINNUNGSFAKTOR VIII, REKOMBINANT (OCTOCOG ALFA)" 
* #1708310 "TIGECYCLIN"
* #1708310 ^designation[0].language = #de-AT 
* #1708310 ^designation[0].value = "TIGECYCLIN" 
* #1708312 "APREPITANT"
* #1708312 ^designation[0].language = #de-AT 
* #1708312 ^designation[0].value = "APREPITANT" 
* #1708314 "LARONIDASE"
* #1708314 ^designation[0].language = #de-AT 
* #1708314 ^designation[0].value = "LARONIDASE" 
* #1708315 "CARGLUMSÄURE"
* #1708315 ^designation[0].language = #de-AT 
* #1708315 ^designation[0].value = "CARGLUMSÄURE" 
* #1708317 "PALONOSETRON HYDROCHLORID"
* #1708317 ^designation[0].language = #de-AT 
* #1708317 ^designation[0].value = "PALONOSETRON HYDROCHLORID" 
* #1708318 "ABATACEPT"
* #1708318 ^designation[0].language = #de-AT 
* #1708318 ^designation[0].value = "ABATACEPT" 
* #1708320 "HEXAMINOLÄVULINAT-HYDROCHLORID"
* #1708320 ^designation[0].language = #de-AT 
* #1708320 ^designation[0].value = "HEXAMINOLÄVULINAT-HYDROCHLORID" 
* #1708321 "GALSULFASE"
* #1708321 ^designation[0].language = #de-AT 
* #1708321 ^designation[0].value = "GALSULFASE" 
* #1708322 "ALGLUCOSIDASE ALFA"
* #1708322 ^designation[0].language = #de-AT 
* #1708322 ^designation[0].value = "ALGLUCOSIDASE ALFA" 
* #1708323 "ALISKIREN"
* #1708323 ^designation[0].language = #de-AT 
* #1708323 ^designation[0].value = "ALISKIREN" 
* #1708324 "ENTECAVIR"
* #1708324 ^designation[0].language = #de-AT 
* #1708324 ^designation[0].value = "ENTECAVIR" 
* #1708325 "METHYL 5-AMINOLEVULINAT HYDROCHLORID"
* #1708325 ^designation[0].language = #de-AT 
* #1708325 ^designation[0].value = "METHYL 5-AMINOLEVULINAT HYDROCHLORID" 
* #1708326 "PARATHYROIDHORMON"
* #1708326 ^designation[0].language = #de-AT 
* #1708326 ^designation[0].value = "PARATHYROIDHORMON" 
* #1708327 "EXENATIDE"
* #1708327 ^designation[0].language = #de-AT 
* #1708327 ^designation[0].value = "EXENATIDE" 
* #1708328 "DEFERASIROX"
* #1708328 ^designation[0].language = #de-AT 
* #1708328 ^designation[0].value = "DEFERASIROX" 
* #1708329 "AMLODIPIN MESILAT"
* #1708329 ^designation[0].language = #de-AT 
* #1708329 ^designation[0].value = "AMLODIPIN MESILAT" 
* #1708330 "ANIDULAFUNGIN"
* #1708330 ^designation[0].language = #de-AT 
* #1708330 ^designation[0].value = "ANIDULAFUNGIN" 
* #1708331 "VARDENAFIL HYDROCHLORID"
* #1708331 ^designation[0].language = #de-AT 
* #1708331 ^designation[0].value = "VARDENAFIL HYDROCHLORID" 
* #1708332 "PALIFERMIN"
* #1708332 ^designation[0].language = #de-AT 
* #1708332 ^designation[0].value = "PALIFERMIN" 
* #1708334 "ROTIGOTIN"
* #1708334 ^designation[0].language = #de-AT 
* #1708334 ^designation[0].value = "ROTIGOTIN" 
* #1708335 "TEMSIROLIMUS"
* #1708335 ^designation[0].language = #de-AT 
* #1708335 ^designation[0].value = "TEMSIROLIMUS" 
* #1708337 "5-AMINOLEVULINSÄURE HYDROCHLORID"
* #1708337 ^designation[0].language = #de-AT 
* #1708337 ^designation[0].value = "5-AMINOLEVULINSÄURE HYDROCHLORID" 
* #1708339 "CLOFARABIN"
* #1708339 ^designation[0].language = #de-AT 
* #1708339 ^designation[0].value = "CLOFARABIN" 
* #1708341 "NATALIZUMAB"
* #1708341 ^designation[0].language = #de-AT 
* #1708341 ^designation[0].value = "NATALIZUMAB" 
* #1708342 "ATAZANAVIR SULFAT"
* #1708342 ^designation[0].language = #de-AT 
* #1708342 ^designation[0].value = "ATAZANAVIR SULFAT" 
* #1708343 "FLUPENTIXOL DIHYDROCHLORID"
* #1708343 ^designation[0].language = #de-AT 
* #1708343 ^designation[0].value = "FLUPENTIXOL DIHYDROCHLORID" 
* #1708345 "CETYLPYRIDINIUMCHLORID"
* #1708345 ^designation[0].language = #de-AT 
* #1708345 ^designation[0].value = "CETYLPYRIDINIUMCHLORID" 
* #1708347 "GUANFACIN HYDROCHLORID"
* #1708347 ^designation[0].language = #de-AT 
* #1708347 ^designation[0].value = "GUANFACIN HYDROCHLORID" 
* #1708350 "TEMOZOLOMID"
* #1708350 ^designation[0].language = #de-AT 
* #1708350 ^designation[0].value = "TEMOZOLOMID" 
* #1708351 "SUGAMMADEX"
* #1708351 ^designation[0].language = #de-AT 
* #1708351 ^designation[0].value = "SUGAMMADEX" 
* #1708356 "DIMEGLUMIN GADOBENAT"
* #1708356 ^designation[0].language = #de-AT 
* #1708356 ^designation[0].value = "DIMEGLUMIN GADOBENAT" 
* #1708357 "MOXIFLOXACIN HYDROCHLORID"
* #1708357 ^designation[0].language = #de-AT 
* #1708357 ^designation[0].value = "MOXIFLOXACIN HYDROCHLORID" 
* #1708358 "BAZEDOXIFEN"
* #1708358 ^designation[0].language = #de-AT 
* #1708358 ^designation[0].value = "BAZEDOXIFEN" 
* #1708359 "METHYLNALTREXONIUMBROMID"
* #1708359 ^designation[0].language = #de-AT 
* #1708359 ^designation[0].value = "METHYLNALTREXONIUMBROMID" 
* #1708360 "FOSAMPRENAVIR CALCIUM"
* #1708360 ^designation[0].language = #de-AT 
* #1708360 ^designation[0].value = "FOSAMPRENAVIR CALCIUM" 
* #1708361 "ARTICAIN HYDROCHLORID"
* #1708361 ^designation[0].language = #de-AT 
* #1708361 ^designation[0].value = "ARTICAIN HYDROCHLORID" 
* #1708362 "TAMSULOSIN HYDROCHLORID"
* #1708362 ^designation[0].language = #de-AT 
* #1708362 ^designation[0].value = "TAMSULOSIN HYDROCHLORID" 
* #1708363 "KALIUMHYDROGENTARTRAT"
* #1708363 ^designation[0].language = #de-AT 
* #1708363 ^designation[0].value = "KALIUMHYDROGENTARTRAT" 
* #1708366 "COFFEIN"
* #1708366 ^designation[0].language = #de-AT 
* #1708366 ^designation[0].value = "COFFEIN" 
* #1708367 "STAVUDIN"
* #1708367 ^designation[0].language = #de-AT 
* #1708367 ^designation[0].value = "STAVUDIN" 
* #1708368 "KALIUMACETAT"
* #1708368 ^designation[0].language = #de-AT 
* #1708368 ^designation[0].value = "KALIUMACETAT" 
* #1708371 "ESOMEPRAZOL NATRIUM"
* #1708371 ^designation[0].language = #de-AT 
* #1708371 ^designation[0].value = "ESOMEPRAZOL NATRIUM" 
* #1708373 "GONADORELIN ACETAT"
* #1708373 ^designation[0].language = #de-AT 
* #1708373 ^designation[0].value = "GONADORELIN ACETAT" 
* #1708374 "BARIUMSULFAT"
* #1708374 ^designation[0].language = #de-AT 
* #1708374 ^designation[0].value = "BARIUMSULFAT" 
* #1708377 "GEMCITABIN HYDROCHLORID"
* #1708377 ^designation[0].language = #de-AT 
* #1708377 ^designation[0].value = "GEMCITABIN HYDROCHLORID" 
* #1708378 "ISOTRETINOIN"
* #1708378 ^designation[0].language = #de-AT 
* #1708378 ^designation[0].value = "ISOTRETINOIN" 
* #1708379 "ETRAVIRIN"
* #1708379 ^designation[0].language = #de-AT 
* #1708379 ^designation[0].value = "ETRAVIRIN" 
* #1708380 "JAPANISCHE ENZEPHALITIS VIRUS"
* #1708380 ^designation[0].language = #de-AT 
* #1708380 ^designation[0].value = "JAPANISCHE ENZEPHALITIS VIRUS" 
* #1708382 "LABETALOL HYDROCHLORID"
* #1708382 ^designation[0].language = #de-AT 
* #1708382 ^designation[0].value = "LABETALOL HYDROCHLORID" 
* #1708383 "BENZOCAIN"
* #1708383 ^designation[0].language = #de-AT 
* #1708383 ^designation[0].value = "BENZOCAIN" 
* #1708384 "SIMETICON"
* #1708384 ^designation[0].language = #de-AT 
* #1708384 ^designation[0].value = "SIMETICON" 
* #1708385 "PEGFILGRASTIM"
* #1708385 ^designation[0].language = #de-AT 
* #1708385 ^designation[0].value = "PEGFILGRASTIM" 
* #1708387 "DILTIAZEM HYDROCHLORID"
* #1708387 ^designation[0].language = #de-AT 
* #1708387 ^designation[0].value = "DILTIAZEM HYDROCHLORID" 
* #1708388 "CEFTRIAXON DINATRIUM"
* #1708388 ^designation[0].language = #de-AT 
* #1708388 ^designation[0].value = "CEFTRIAXON DINATRIUM" 
* #1708389 "GADOTERSÄURE"
* #1708389 ^designation[0].language = #de-AT 
* #1708389 ^designation[0].value = "GADOTERSÄURE" 
* #1708390 "LANSOPRAZOL"
* #1708390 ^designation[0].language = #de-AT 
* #1708390 ^designation[0].value = "LANSOPRAZOL" 
* #1708393 "FOSFOMYCIN-TROMETAMOL"
* #1708393 ^designation[0].language = #de-AT 
* #1708393 ^designation[0].value = "FOSFOMYCIN-TROMETAMOL" 
* #1708394 "FLUVASTATIN NATRIUM"
* #1708394 ^designation[0].language = #de-AT 
* #1708394 ^designation[0].value = "FLUVASTATIN NATRIUM" 
* #1708395 "CYTARABIN"
* #1708395 ^designation[0].language = #de-AT 
* #1708395 ^designation[0].value = "CYTARABIN" 
* #1708396 "SUXAMETHONIUM CHLORID"
* #1708396 ^designation[0].language = #de-AT 
* #1708396 ^designation[0].value = "SUXAMETHONIUM CHLORID" 
* #1708397 "INSULIN"
* #1708397 ^designation[0].language = #de-AT 
* #1708397 ^designation[0].value = "INSULIN" 
* #1708398 "ALEMTUZUMAB"
* #1708398 ^designation[0].language = #de-AT 
* #1708398 ^designation[0].value = "ALEMTUZUMAB" 
* #1708399 "RABIESVIRUS"
* #1708399 ^designation[0].language = #de-AT 
* #1708399 ^designation[0].value = "RABIESVIRUS" 
* #1708403 "FRÜHSOMMER-MENINGOENCEPHALITIS-VIRUS"
* #1708403 ^designation[0].language = #de-AT 
* #1708403 ^designation[0].value = "FRÜHSOMMER-MENINGOENCEPHALITIS-VIRUS" 
* #1708404 "METOCLOPRAMIDHYDROCHLORID"
* #1708404 ^designation[0].language = #de-AT 
* #1708404 ^designation[0].value = "METOCLOPRAMIDHYDROCHLORID" 
* #1708405 "ALTEPLASE"
* #1708405 ^designation[0].language = #de-AT 
* #1708405 ^designation[0].value = "ALTEPLASE" 
* #1708406 "PYRIDOXIN HYDROCHLORID"
* #1708406 ^designation[0].language = #de-AT 
* #1708406 ^designation[0].value = "PYRIDOXIN HYDROCHLORID" 
* #1708408 "LYSIN HYDROCHLORID"
* #1708408 ^designation[0].language = #de-AT 
* #1708408 ^designation[0].value = "LYSIN HYDROCHLORID" 
* #1708409 "NALTREXON HYDROCHLORID"
* #1708409 ^designation[0].language = #de-AT 
* #1708409 ^designation[0].value = "NALTREXON HYDROCHLORID" 
* #1708413 "ARIPIPRAZOL"
* #1708413 ^designation[0].language = #de-AT 
* #1708413 ^designation[0].value = "ARIPIPRAZOL" 
* #1708416 "PIPERACILLIN-NATRIUM"
* #1708416 ^designation[0].language = #de-AT 
* #1708416 ^designation[0].value = "PIPERACILLIN-NATRIUM" 
* #1708417 "PAROXETINHYDROCHLORID"
* #1708417 ^designation[0].language = #de-AT 
* #1708417 ^designation[0].value = "PAROXETINHYDROCHLORID" 
* #1708418 "OLMESARTAN MEDOXOMIL"
* #1708418 ^designation[0].language = #de-AT 
* #1708418 ^designation[0].value = "OLMESARTAN MEDOXOMIL" 
* #1708420 "RACECADOTRIL"
* #1708420 ^designation[0].language = #de-AT 
* #1708420 ^designation[0].value = "RACECADOTRIL" 
* #1708421 "DOXORUBICIN HYDROCHLORID"
* #1708421 ^designation[0].language = #de-AT 
* #1708421 ^designation[0].value = "DOXORUBICIN HYDROCHLORID" 
* #1708422 "PHENYTOIN NATRIUM"
* #1708422 ^designation[0].language = #de-AT 
* #1708422 ^designation[0].value = "PHENYTOIN NATRIUM" 
* #1708423 "NATRIUM LIOTHYRONINAT"
* #1708423 ^designation[0].language = #de-AT 
* #1708423 ^designation[0].value = "NATRIUM LIOTHYRONINAT" 
* #1708424 "AMPICILLIN NATRIUM"
* #1708424 ^designation[0].language = #de-AT 
* #1708424 ^designation[0].value = "AMPICILLIN NATRIUM" 
* #1708425 "NYSTATIN"
* #1708425 ^designation[0].language = #de-AT 
* #1708425 ^designation[0].value = "NYSTATIN" 
* #1708426 "DOXAZOSIN MESILAT"
* #1708426 ^designation[0].language = #de-AT 
* #1708426 ^designation[0].value = "DOXAZOSIN MESILAT" 
* #1708427 "FELODIPIN"
* #1708427 ^designation[0].language = #de-AT 
* #1708427 ^designation[0].value = "FELODIPIN" 
* #1708429 "BETAHISTIN DIHYDROCHLORID"
* #1708429 ^designation[0].language = #de-AT 
* #1708429 ^designation[0].value = "BETAHISTIN DIHYDROCHLORID" 
* #1708431 "CEFAZOLIN NATRIUM"
* #1708431 ^designation[0].language = #de-AT 
* #1708431 ^designation[0].value = "CEFAZOLIN NATRIUM" 
* #1708433 "QUINAGOLID HYDROCHLORID"
* #1708433 ^designation[0].language = #de-AT 
* #1708433 ^designation[0].value = "QUINAGOLID HYDROCHLORID" 
* #1708436 "FLUDARABIN PHOSPHAT"
* #1708436 ^designation[0].language = #de-AT 
* #1708436 ^designation[0].value = "FLUDARABIN PHOSPHAT" 
* #1708438 "SULBACTAM NATRIUM"
* #1708438 ^designation[0].language = #de-AT 
* #1708438 ^designation[0].value = "SULBACTAM NATRIUM" 
* #1708439 "NADROPARIN CALCIUM"
* #1708439 ^designation[0].language = #de-AT 
* #1708439 ^designation[0].value = "NADROPARIN CALCIUM" 
* #1708440 "NEOMYCIN SULFAT"
* #1708440 ^designation[0].language = #de-AT 
* #1708440 ^designation[0].value = "NEOMYCIN SULFAT" 
* #1708441 "RABEPRAZOL NATRIUM"
* #1708441 ^designation[0].language = #de-AT 
* #1708441 ^designation[0].value = "RABEPRAZOL NATRIUM" 
* #1708442 "TIMOLOLMALEAT"
* #1708442 ^designation[0].language = #de-AT 
* #1708442 ^designation[0].value = "TIMOLOLMALEAT" 
* #1708443 "SALICYLSÄURE"
* #1708443 ^designation[0].language = #de-AT 
* #1708443 ^designation[0].value = "SALICYLSÄURE" 
* #1708446 "BELIMUMAB"
* #1708446 ^designation[0].language = #de-AT 
* #1708446 ^designation[0].value = "BELIMUMAB" 
* #1708447 "DISTRONTIUM RANELAT"
* #1708447 ^designation[0].language = #de-AT 
* #1708447 ^designation[0].value = "DISTRONTIUM RANELAT" 
* #1708448 "PERFLUTREN"
* #1708448 ^designation[0].language = #de-AT 
* #1708448 ^designation[0].value = "PERFLUTREN" 
* #1708449 "NATRIUMHYALURONAT"
* #1708449 ^designation[0].language = #de-AT 
* #1708449 ^designation[0].value = "NATRIUMHYALURONAT" 
* #1708450 "AMLODIPIN BESILAT"
* #1708450 ^designation[0].language = #de-AT 
* #1708450 ^designation[0].value = "AMLODIPIN BESILAT" 
* #1708451 "BETAMETHASON DINATRIUMPHOSPHAT"
* #1708451 ^designation[0].language = #de-AT 
* #1708451 ^designation[0].value = "BETAMETHASON DINATRIUMPHOSPHAT" 
* #1708452 "DEFEROXAMIN MESILAT"
* #1708452 ^designation[0].language = #de-AT 
* #1708452 ^designation[0].value = "DEFEROXAMIN MESILAT" 
* #1708453 "MEMANTIN HYDROCHLORID"
* #1708453 ^designation[0].language = #de-AT 
* #1708453 ^designation[0].value = "MEMANTIN HYDROCHLORID" 
* #1708455 "NATRIUMLACTAT"
* #1708455 ^designation[0].language = #de-AT 
* #1708455 ^designation[0].value = "NATRIUMLACTAT" 
* #1708457 "MAGNESIUMHYDROXID"
* #1708457 ^designation[0].language = #de-AT 
* #1708457 ^designation[0].value = "MAGNESIUMHYDROXID" 
* #1708458 "BIPERIDEN"
* #1708458 ^designation[0].language = #de-AT 
* #1708458 ^designation[0].value = "BIPERIDEN" 
* #1708459 "CLENBUTEROL HYDROCHLORID"
* #1708459 ^designation[0].language = #de-AT 
* #1708459 ^designation[0].value = "CLENBUTEROL HYDROCHLORID" 
* #1708460 "MAGNESIUMCHLORID"
* #1708460 ^designation[0].language = #de-AT 
* #1708460 ^designation[0].value = "MAGNESIUMCHLORID" 
* #1708461 "IBUPROFEN LYSIN"
* #1708461 ^designation[0].language = #de-AT 
* #1708461 ^designation[0].value = "IBUPROFEN LYSIN" 
* #1708462 "NATRIUM AMOXICILLINAT"
* #1708462 ^designation[0].language = #de-AT 
* #1708462 ^designation[0].value = "NATRIUM AMOXICILLINAT" 
* #1708463 "MIANSERIN HYDROCHLORID"
* #1708463 ^designation[0].language = #de-AT 
* #1708463 ^designation[0].value = "MIANSERIN HYDROCHLORID" 
* #1708468 "MERCAPTOPURIN"
* #1708468 ^designation[0].language = #de-AT 
* #1708468 ^designation[0].value = "MERCAPTOPURIN" 
* #1708469 "NATRIUMALGINAT"
* #1708469 ^designation[0].language = #de-AT 
* #1708469 ^designation[0].value = "NATRIUMALGINAT" 
* #1708470 "METHYLPHENIDAT HYDROCHLORID"
* #1708470 ^designation[0].language = #de-AT 
* #1708470 ^designation[0].value = "METHYLPHENIDAT HYDROCHLORID" 
* #1708471 "MEPIVACAIN HYDROCHLORID"
* #1708471 ^designation[0].language = #de-AT 
* #1708471 ^designation[0].value = "MEPIVACAIN HYDROCHLORID" 
* #1708472 "ERTAPENEM NATRIUM"
* #1708472 ^designation[0].language = #de-AT 
* #1708472 ^designation[0].value = "ERTAPENEM NATRIUM" 
* #1708475 "HEPARIN NATRIUM"
* #1708475 ^designation[0].language = #de-AT 
* #1708475 ^designation[0].value = "HEPARIN NATRIUM" 
* #1708476 "GLICLAZID"
* #1708476 ^designation[0].language = #de-AT 
* #1708476 ^designation[0].value = "GLICLAZID" 
* #1708477 "ENOXAPARIN NATRIUM"
* #1708477 ^designation[0].language = #de-AT 
* #1708477 ^designation[0].value = "ENOXAPARIN NATRIUM" 
* #1708478 "FLUOCINOLON ACETONID"
* #1708478 ^designation[0].language = #de-AT 
* #1708478 ^designation[0].value = "FLUOCINOLON ACETONID" 
* #1708479 "LOPERAMID HYDROCHLORID"
* #1708479 ^designation[0].language = #de-AT 
* #1708479 ^designation[0].value = "LOPERAMID HYDROCHLORID" 
* #1708481 "PREDNISOLON ACETAT"
* #1708481 ^designation[0].language = #de-AT 
* #1708481 ^designation[0].value = "PREDNISOLON ACETAT" 
* #1708482 "DIMETICON"
* #1708482 ^designation[0].language = #de-AT 
* #1708482 ^designation[0].value = "DIMETICON" 
* #1708484 "PHENYLEPHRINHYDROCHLORID"
* #1708484 ^designation[0].language = #de-AT 
* #1708484 ^designation[0].value = "PHENYLEPHRINHYDROCHLORID" 
* #1708486 "CILASTATIN NATRIUM"
* #1708486 ^designation[0].language = #de-AT 
* #1708486 ^designation[0].value = "CILASTATIN NATRIUM" 
* #1708487 "OMEPRAZOL NATRIUM"
* #1708487 ^designation[0].language = #de-AT 
* #1708487 ^designation[0].value = "OMEPRAZOL NATRIUM" 
* #1708488 "ISOCONAZOL NITRAT"
* #1708488 ^designation[0].language = #de-AT 
* #1708488 ^designation[0].value = "ISOCONAZOL NITRAT" 
* #1708489 "TAMOXIFENCITRAT"
* #1708489 ^designation[0].language = #de-AT 
* #1708489 ^designation[0].value = "TAMOXIFENCITRAT" 
* #1708490 "NATRIUMHYDROXID"
* #1708490 ^designation[0].language = #de-AT 
* #1708490 ^designation[0].value = "NATRIUMHYDROXID" 
* #1708492 "CARMUSTIN"
* #1708492 ^designation[0].language = #de-AT 
* #1708492 ^designation[0].value = "CARMUSTIN" 
* #1708493 "MECETRONIUM ETILSULFAT"
* #1708493 ^designation[0].language = #de-AT 
* #1708493 ^designation[0].value = "MECETRONIUM ETILSULFAT" 
* #1708494 "CHLORPHENAMIN HYDROGENMALEAT"
* #1708494 ^designation[0].language = #de-AT 
* #1708494 ^designation[0].value = "CHLORPHENAMIN HYDROGENMALEAT" 
* #1708497 "NITRENDIPIN"
* #1708497 ^designation[0].language = #de-AT 
* #1708497 ^designation[0].value = "NITRENDIPIN" 
* #1708500 "ERGOCALCIFEROL"
* #1708500 ^designation[0].language = #de-AT 
* #1708500 ^designation[0].value = "ERGOCALCIFEROL" 
* #1708501 "ZINKCHLORID"
* #1708501 ^designation[0].language = #de-AT 
* #1708501 ^designation[0].value = "ZINKCHLORID" 
* #1708502 "MELATONIN"
* #1708502 ^designation[0].language = #de-AT 
* #1708502 ^designation[0].value = "MELATONIN" 
* #1708506 "INSULIN LISPRO"
* #1708506 ^designation[0].language = #de-AT 
* #1708506 ^designation[0].value = "INSULIN LISPRO" 
* #1708507 "METHOTREXAT"
* #1708507 ^designation[0].language = #de-AT 
* #1708507 ^designation[0].value = "METHOTREXAT" 
* #1708508 "PALIVIZUMAB"
* #1708508 ^designation[0].language = #de-AT 
* #1708508 ^designation[0].value = "PALIVIZUMAB" 
* #1708509 "CALCIUMSULFAT"
* #1708509 ^designation[0].language = #de-AT 
* #1708509 ^designation[0].value = "CALCIUMSULFAT" 
* #1708512 "MITOXANTRON DIHYDROCHLORID"
* #1708512 ^designation[0].language = #de-AT 
* #1708512 ^designation[0].value = "MITOXANTRON DIHYDROCHLORID" 
* #1708513 "RIZATRIPTAN BENZOAT"
* #1708513 ^designation[0].language = #de-AT 
* #1708513 ^designation[0].value = "RIZATRIPTAN BENZOAT" 
* #1708514 "OXYMETAZOLIN HYDROCHLORID"
* #1708514 ^designation[0].language = #de-AT 
* #1708514 ^designation[0].value = "OXYMETAZOLIN HYDROCHLORID" 
* #1708515 "PARACETAMOL"
* #1708515 ^designation[0].language = #de-AT 
* #1708515 ^designation[0].value = "PARACETAMOL" 
* #1708516 "PHENOXYMETHYLPENICILLIN-KALIUM"
* #1708516 ^designation[0].language = #de-AT 
* #1708516 ^designation[0].value = "PHENOXYMETHYLPENICILLIN-KALIUM" 
* #1708517 "CYCLOPHOSPHAMID"
* #1708517 ^designation[0].language = #de-AT 
* #1708517 ^designation[0].value = "CYCLOPHOSPHAMID" 
* #1708518 "MIDODRIN HYDROCHLORID"
* #1708518 ^designation[0].language = #de-AT 
* #1708518 ^designation[0].value = "MIDODRIN HYDROCHLORID" 
* #1708519 "NOMEGESTROL ACETAT"
* #1708519 ^designation[0].language = #de-AT 
* #1708519 ^designation[0].value = "NOMEGESTROL ACETAT" 
* #1708520 "CALCIUM FOLINAT"
* #1708520 ^designation[0].language = #de-AT 
* #1708520 ^designation[0].value = "CALCIUM FOLINAT" 
* #1708521 "IBRITUMOMAB TIUXETAN"
* #1708521 ^designation[0].language = #de-AT 
* #1708521 ^designation[0].value = "IBRITUMOMAB TIUXETAN" 
* #1708522 "METRONIDAZOL"
* #1708522 ^designation[0].language = #de-AT 
* #1708522 ^designation[0].value = "METRONIDAZOL" 
* #1708524 "TORASEMID"
* #1708524 ^designation[0].language = #de-AT 
* #1708524 ^designation[0].value = "TORASEMID" 
* #1708526 "PEGINTERFERON ALFA-2B"
* #1708526 ^designation[0].language = #de-AT 
* #1708526 ^designation[0].value = "PEGINTERFERON ALFA-2B" 
* #1708527 "BIPERIDEN HYDROCHLORID"
* #1708527 ^designation[0].language = #de-AT 
* #1708527 ^designation[0].value = "BIPERIDEN HYDROCHLORID" 
* #1708528 "DIMETHYLSULFOXID"
* #1708528 ^designation[0].language = #de-AT 
* #1708528 ^designation[0].value = "DIMETHYLSULFOXID" 
* #1708529 "ATORVASTATIN CALCIUM"
* #1708529 ^designation[0].language = #de-AT 
* #1708529 ^designation[0].value = "ATORVASTATIN CALCIUM" 
* #1708530 "TRAZODON HYDROCHLORID"
* #1708530 ^designation[0].language = #de-AT 
* #1708530 ^designation[0].value = "TRAZODON HYDROCHLORID" 
* #1708532 "AMITRIPTYLIN HYDROCHLORID"
* #1708532 ^designation[0].language = #de-AT 
* #1708532 ^designation[0].value = "AMITRIPTYLIN HYDROCHLORID" 
* #1708534 "DORZOLAMID HYDROCHLORID"
* #1708534 ^designation[0].language = #de-AT 
* #1708534 ^designation[0].value = "DORZOLAMID HYDROCHLORID" 
* #1708535 "CARBIDOPA"
* #1708535 ^designation[0].language = #de-AT 
* #1708535 ^designation[0].value = "CARBIDOPA" 
* #1708536 "FUROSEMID"
* #1708536 ^designation[0].language = #de-AT 
* #1708536 ^designation[0].value = "FUROSEMID" 
* #1708537 "MAGALDRAT"
* #1708537 ^designation[0].language = #de-AT 
* #1708537 ^designation[0].value = "MAGALDRAT" 
* #1708538 "SORAFENIBTOSILAT"
* #1708538 ^designation[0].language = #de-AT 
* #1708538 ^designation[0].value = "SORAFENIBTOSILAT" 
* #1708539 "CYPROTERON ACETAT"
* #1708539 ^designation[0].language = #de-AT 
* #1708539 ^designation[0].value = "CYPROTERON ACETAT" 
* #1708540 "LEVOCARNITIN"
* #1708540 ^designation[0].language = #de-AT 
* #1708540 ^designation[0].value = "LEVOCARNITIN" 
* #1708541 "CLONIDIN HYDROCHLORID"
* #1708541 ^designation[0].language = #de-AT 
* #1708541 ^designation[0].value = "CLONIDIN HYDROCHLORID" 
* #1708544 "BACLOFEN"
* #1708544 ^designation[0].language = #de-AT 
* #1708544 ^designation[0].value = "BACLOFEN" 
* #1708545 "GLIPIZID"
* #1708545 ^designation[0].language = #de-AT 
* #1708545 ^designation[0].value = "GLIPIZID" 
* #1708546 "LENALIDOMID"
* #1708546 ^designation[0].language = #de-AT 
* #1708546 ^designation[0].value = "LENALIDOMID" 
* #1708550 "OXETACAIN"
* #1708550 ^designation[0].language = #de-AT 
* #1708550 ^designation[0].value = "OXETACAIN" 
* #1708552 "PROGESTERON"
* #1708552 ^designation[0].language = #de-AT 
* #1708552 ^designation[0].value = "PROGESTERON" 
* #1708553 "PANTOPRAZOL NATRIUM"
* #1708553 ^designation[0].language = #de-AT 
* #1708553 ^designation[0].value = "PANTOPRAZOL NATRIUM" 
* #1708554 "PHENOXYMETHYLPENICILLIN"
* #1708554 ^designation[0].language = #de-AT 
* #1708554 ^designation[0].value = "PHENOXYMETHYLPENICILLIN" 
* #1708555 "IBUPROFEN"
* #1708555 ^designation[0].language = #de-AT 
* #1708555 ^designation[0].value = "IBUPROFEN" 
* #1708556 "MINOXIDIL"
* #1708556 ^designation[0].language = #de-AT 
* #1708556 ^designation[0].value = "MINOXIDIL" 
* #1708557 "OXYCODON HYDROCHLORID"
* #1708557 ^designation[0].language = #de-AT 
* #1708557 ^designation[0].value = "OXYCODON HYDROCHLORID" 
* #1708558 "MICONAZOL"
* #1708558 ^designation[0].language = #de-AT 
* #1708558 ^designation[0].value = "MICONAZOL" 
* #1708559 "SOLIFENACIN SUCCINAT"
* #1708559 ^designation[0].language = #de-AT 
* #1708559 ^designation[0].value = "SOLIFENACIN SUCCINAT" 
* #1708560 "NIFEDIPIN"
* #1708560 ^designation[0].language = #de-AT 
* #1708560 ^designation[0].value = "NIFEDIPIN" 
* #1708561 "FENTANYL"
* #1708561 ^designation[0].language = #de-AT 
* #1708561 ^designation[0].value = "FENTANYL" 
* #1708562 "NAPROXEN"
* #1708562 ^designation[0].language = #de-AT 
* #1708562 ^designation[0].value = "NAPROXEN" 
* #1708563 "TRIAMCINOLON ACETONID"
* #1708563 ^designation[0].language = #de-AT 
* #1708563 ^designation[0].value = "TRIAMCINOLON ACETONID" 
* #1708564 "VON WILLEBRAND FAKTOR AUS MENSCHLICHEM PLASMA"
* #1708564 ^designation[0].language = #de-AT 
* #1708564 ^designation[0].value = "VON WILLEBRAND FAKTOR AUS MENSCHLICHEM PLASMA" 
* #1708565 "METOPROLOL TARTRAT"
* #1708565 ^designation[0].language = #de-AT 
* #1708565 ^designation[0].value = "METOPROLOL TARTRAT" 
* #1708566 "VALACICLOVIRHYDROCHLORID"
* #1708566 ^designation[0].language = #de-AT 
* #1708566 ^designation[0].value = "VALACICLOVIRHYDROCHLORID" 
* #1708567 "NAPHAZOLIN HYDROCHLORID"
* #1708567 ^designation[0].language = #de-AT 
* #1708567 ^designation[0].value = "NAPHAZOLIN HYDROCHLORID" 
* #1708568 "ISOSORBIDMONONITRAT"
* #1708568 ^designation[0].language = #de-AT 
* #1708568 ^designation[0].value = "ISOSORBIDMONONITRAT" 
* #1708569 "NANDROLON DECANOAT"
* #1708569 ^designation[0].language = #de-AT 
* #1708569 ^designation[0].value = "NANDROLON DECANOAT" 
* #1708571 "NATRIUMPOLYSTYROLSULFONAT"
* #1708571 ^designation[0].language = #de-AT 
* #1708571 ^designation[0].value = "NATRIUMPOLYSTYROLSULFONAT" 
* #1708572 "DABIGATRANETEXILATMESILAT"
* #1708572 ^designation[0].language = #de-AT 
* #1708572 ^designation[0].value = "DABIGATRANETEXILATMESILAT" 
* #1708573 "ANAGRELIDHYDROCHLORID"
* #1708573 ^designation[0].language = #de-AT 
* #1708573 ^designation[0].value = "ANAGRELIDHYDROCHLORID" 
* #1708574 "BROMOCRIPTIN MESILAT"
* #1708574 ^designation[0].language = #de-AT 
* #1708574 ^designation[0].value = "BROMOCRIPTIN MESILAT" 
* #1708575 "CALCIUM LACTAT GLUCONAT"
* #1708575 ^designation[0].language = #de-AT 
* #1708575 ^designation[0].value = "CALCIUM LACTAT GLUCONAT" 
* #1708576 "GUAIFENESIN"
* #1708576 ^designation[0].language = #de-AT 
* #1708576 ^designation[0].value = "GUAIFENESIN" 
* #1708577 "FLECAINID ACETAT"
* #1708577 ^designation[0].language = #de-AT 
* #1708577 ^designation[0].value = "FLECAINID ACETAT" 
* #1708579 "VALPROINSÄURE"
* #1708579 ^designation[0].language = #de-AT 
* #1708579 ^designation[0].value = "VALPROINSÄURE" 
* #1708580 "ALFENTANIL HYDROCHLORID"
* #1708580 ^designation[0].language = #de-AT 
* #1708580 ^designation[0].value = "ALFENTANIL HYDROCHLORID" 
* #1708581 "ARGININ GLUTAMAT"
* #1708581 ^designation[0].language = #de-AT 
* #1708581 ^designation[0].value = "ARGININ GLUTAMAT" 
* #1708582 "AZELASTIN HYDROCHLORID"
* #1708582 ^designation[0].language = #de-AT 
* #1708582 ^designation[0].value = "AZELASTIN HYDROCHLORID" 
* #1708583 "ZINK BACITRACIN"
* #1708583 ^designation[0].language = #de-AT 
* #1708583 ^designation[0].value = "ZINK BACITRACIN" 
* #1708585 "CEFAMANDOL NAFAT"
* #1708585 ^designation[0].language = #de-AT 
* #1708585 ^designation[0].value = "CEFAMANDOL NAFAT" 
* #1708586 "BUPROPION HYDROCHLORID"
* #1708586 ^designation[0].language = #de-AT 
* #1708586 ^designation[0].value = "BUPROPION HYDROCHLORID" 
* #1708587 "BENZOESÄURE"
* #1708587 ^designation[0].language = #de-AT 
* #1708587 ^designation[0].value = "BENZOESÄURE" 
* #1708592 "SCHWEFEL"
* #1708592 ^designation[0].language = #de-AT 
* #1708592 ^designation[0].value = "SCHWEFEL" 
* #1708593 "EISEN(II)-FUMARAT"
* #1708593 ^designation[0].language = #de-AT 
* #1708593 ^designation[0].value = "EISEN(II)-FUMARAT" 
* #1708595 "PENTOXYVERIN CITRAT"
* #1708595 ^designation[0].language = #de-AT 
* #1708595 ^designation[0].value = "PENTOXYVERIN CITRAT" 
* #1708596 "RANITIDIN HYDROCHLORID"
* #1708596 ^designation[0].language = #de-AT 
* #1708596 ^designation[0].value = "RANITIDIN HYDROCHLORID" 
* #1708597 "UROFOLLITROPIN"
* #1708597 ^designation[0].language = #de-AT 
* #1708597 ^designation[0].value = "UROFOLLITROPIN" 
* #1708598 "DIPHENHYDRAMIN HYDROCHLORID"
* #1708598 ^designation[0].language = #de-AT 
* #1708598 ^designation[0].value = "DIPHENHYDRAMIN HYDROCHLORID" 
* #1708599 "GRANISETRON HYDROCHLORID"
* #1708599 ^designation[0].language = #de-AT 
* #1708599 ^designation[0].value = "GRANISETRON HYDROCHLORID" 
* #1708603 "PHENIRAMIN HYDROGENMALEAT"
* #1708603 ^designation[0].language = #de-AT 
* #1708603 ^designation[0].value = "PHENIRAMIN HYDROGENMALEAT" 
* #1708604 "QUINAPRIL HYDROCHLORID"
* #1708604 ^designation[0].language = #de-AT 
* #1708604 ^designation[0].value = "QUINAPRIL HYDROCHLORID" 
* #1708605 "REMIFENTANIL HYDROCHLORID"
* #1708605 ^designation[0].language = #de-AT 
* #1708605 ^designation[0].value = "REMIFENTANIL HYDROCHLORID" 
* #1708606 "VANCOMYCIN HYDROCHLORID"
* #1708606 ^designation[0].language = #de-AT 
* #1708606 ^designation[0].value = "VANCOMYCIN HYDROCHLORID" 
* #1708608 "VERAPAMIL HYDROCHLORID"
* #1708608 ^designation[0].language = #de-AT 
* #1708608 ^designation[0].value = "VERAPAMIL HYDROCHLORID" 
* #1708609 "DIACEREIN"
* #1708609 ^designation[0].language = #de-AT 
* #1708609 ^designation[0].value = "DIACEREIN" 
* #1708610 "CISATRACURIUM BESILAT"
* #1708610 ^designation[0].language = #de-AT 
* #1708610 ^designation[0].value = "CISATRACURIUM BESILAT" 
* #1708612 "CALCIUM PANTOTHENAT"
* #1708612 ^designation[0].language = #de-AT 
* #1708612 ^designation[0].value = "CALCIUM PANTOTHENAT" 
* #1708615 "LEVOBUPIVACAIN HYDROCHLORID"
* #1708615 ^designation[0].language = #de-AT 
* #1708615 ^designation[0].value = "LEVOBUPIVACAIN HYDROCHLORID" 
* #1708616 "ESTRADIOL VALERAT"
* #1708616 ^designation[0].language = #de-AT 
* #1708616 ^designation[0].value = "ESTRADIOL VALERAT" 
* #1708617 "MORPHINHYDROCHLORID"
* #1708617 ^designation[0].language = #de-AT 
* #1708617 ^designation[0].value = "MORPHINHYDROCHLORID" 
* #1708618 "FORMOTEROLFUMARAT"
* #1708618 ^designation[0].language = #de-AT 
* #1708618 ^designation[0].value = "FORMOTEROLFUMARAT" 
* #1708619 "SAUERSTOFF"
* #1708619 ^designation[0].language = #de-AT 
* #1708619 ^designation[0].value = "SAUERSTOFF" 
* #1708620 "CLOMIPRAMIN HYDROCHLORID"
* #1708620 ^designation[0].language = #de-AT 
* #1708620 ^designation[0].value = "CLOMIPRAMIN HYDROCHLORID" 
* #1708621 "IDARUBICIN HYDROCHLORID"
* #1708621 ^designation[0].language = #de-AT 
* #1708621 ^designation[0].value = "IDARUBICIN HYDROCHLORID" 
* #1708622 "TRIPTORELIN ACETAT"
* #1708622 ^designation[0].language = #de-AT 
* #1708622 ^designation[0].value = "TRIPTORELIN ACETAT" 
* #1708623 "ESCITALOPRAM OXALAT"
* #1708623 ^designation[0].language = #de-AT 
* #1708623 ^designation[0].value = "ESCITALOPRAM OXALAT" 
* #1708624 "BENZATHIN BENZYLPENICILLIN"
* #1708624 ^designation[0].language = #de-AT 
* #1708624 ^designation[0].value = "BENZATHIN BENZYLPENICILLIN" 
* #1708626 "INDIUM CHLORID [*111*IN]"
* #1708626 ^designation[0].language = #de-AT 
* #1708626 ^designation[0].value = "INDIUM CHLORID [*111*IN]" 
* #1708630 "NATRIUM METHYLPREDNISOLON SUCCINAT"
* #1708630 ^designation[0].language = #de-AT 
* #1708630 ^designation[0].value = "NATRIUM METHYLPREDNISOLON SUCCINAT" 
* #1708631 "ATOMOXETIN HYDROCHLORID"
* #1708631 ^designation[0].language = #de-AT 
* #1708631 ^designation[0].value = "ATOMOXETIN HYDROCHLORID" 
* #1708634 "AKTIVKOHLE"
* #1708634 ^designation[0].language = #de-AT 
* #1708634 ^designation[0].value = "AKTIVKOHLE" 
* #1708635 "DEXAMETHASON DINATRIUMPHOSPHAT"
* #1708635 ^designation[0].language = #de-AT 
* #1708635 ^designation[0].value = "DEXAMETHASON DINATRIUMPHOSPHAT" 
* #1708636 "ERYTHROMYCINETHYLSUCCINAT"
* #1708636 ^designation[0].language = #de-AT 
* #1708636 ^designation[0].value = "ERYTHROMYCINETHYLSUCCINAT" 
* #1708637 "CHLORMADINONACETAT"
* #1708637 ^designation[0].language = #de-AT 
* #1708637 ^designation[0].value = "CHLORMADINONACETAT" 
* #1708638 "CLINDAMYCIN HYDROCHLORID"
* #1708638 ^designation[0].language = #de-AT 
* #1708638 ^designation[0].value = "CLINDAMYCIN HYDROCHLORID" 
* #1708639 "CHLORMETHIN HYDROCHLORID"
* #1708639 ^designation[0].language = #de-AT 
* #1708639 ^designation[0].value = "CHLORMETHIN HYDROCHLORID" 
* #1708640 "CITALOPRAM HYDROBROMID"
* #1708640 ^designation[0].language = #de-AT 
* #1708640 ^designation[0].value = "CITALOPRAM HYDROBROMID" 
* #1708641 "CLINDAMYCIN PHOSPHAT"
* #1708641 ^designation[0].language = #de-AT 
* #1708641 ^designation[0].value = "CLINDAMYCIN PHOSPHAT" 
* #1708642 "COLCHICIN"
* #1708642 ^designation[0].language = #de-AT 
* #1708642 ^designation[0].value = "COLCHICIN" 
* #1708643 "DEXRAZOXAN HYDROCHLORID"
* #1708643 ^designation[0].language = #de-AT 
* #1708643 ^designation[0].value = "DEXRAZOXAN HYDROCHLORID" 
* #1708644 "EPIRUBICIN HYDROCHLORID"
* #1708644 ^designation[0].language = #de-AT 
* #1708644 ^designation[0].value = "EPIRUBICIN HYDROCHLORID" 
* #1708645 "GALANTAMIN HYDROBROMID"
* #1708645 ^designation[0].language = #de-AT 
* #1708645 ^designation[0].value = "GALANTAMIN HYDROBROMID" 
* #1708647 "IRINOTECAN HYDROCHLORID"
* #1708647 ^designation[0].language = #de-AT 
* #1708647 ^designation[0].value = "IRINOTECAN HYDROCHLORID" 
* #1708648 "LEVOMEPROMAZIN HYDROCHLORID"
* #1708648 ^designation[0].language = #de-AT 
* #1708648 ^designation[0].value = "LEVOMEPROMAZIN HYDROCHLORID" 
* #1708649 "SALBUTAMOLSULFAT"
* #1708649 ^designation[0].language = #de-AT 
* #1708649 ^designation[0].value = "SALBUTAMOLSULFAT" 
* #1708650 "CLOMIFEN DIHYDROGENCITRAT"
* #1708650 ^designation[0].language = #de-AT 
* #1708650 ^designation[0].value = "CLOMIFEN DIHYDROGENCITRAT" 
* #1708651 "COLESEVELAM HYDROCHLORID"
* #1708651 ^designation[0].language = #de-AT 
* #1708651 ^designation[0].value = "COLESEVELAM HYDROCHLORID" 
* #1708652 "DIHYDROERGOTAMIN MESILAT"
* #1708652 ^designation[0].language = #de-AT 
* #1708652 ^designation[0].value = "DIHYDROERGOTAMIN MESILAT" 
* #1708653 "DIMEGLUMIN GADOPENTETAT"
* #1708653 ^designation[0].language = #de-AT 
* #1708653 ^designation[0].value = "DIMEGLUMIN GADOPENTETAT" 
* #1708654 "HYDROMORPHONHYDROCHLORID"
* #1708654 ^designation[0].language = #de-AT 
* #1708654 ^designation[0].value = "HYDROMORPHONHYDROCHLORID" 
* #1708657 "LIDOCAIN HYDROCHLORID"
* #1708657 ^designation[0].language = #de-AT 
* #1708657 ^designation[0].value = "LIDOCAIN HYDROCHLORID" 
* #1708659 "SERTRALIN HYDROCHLORID"
* #1708659 ^designation[0].language = #de-AT 
* #1708659 ^designation[0].value = "SERTRALIN HYDROCHLORID" 
* #1708664 "DOCETAXEL"
* #1708664 ^designation[0].language = #de-AT 
* #1708664 ^designation[0].value = "DOCETAXEL" 
* #1708665 "TERBINAFIN HYDROCHLORID"
* #1708665 ^designation[0].language = #de-AT 
* #1708665 ^designation[0].value = "TERBINAFIN HYDROCHLORID" 
* #1708666 "FIBRINOGEN"
* #1708666 ^designation[0].language = #de-AT 
* #1708666 ^designation[0].value = "FIBRINOGEN" 
* #1708667 "PROTEIN, GERINNBARES (HUMAN)"
* #1708667 ^designation[0].language = #de-AT 
* #1708667 ^designation[0].value = "PROTEIN, GERINNBARES (HUMAN)" 
* #1708669 "PLANTAGINIS LANCEOLATAE FOLIUM"
* #1708669 ^designation[0].language = #de-AT 
* #1708669 ^designation[0].value = "PLANTAGINIS LANCEOLATAE FOLIUM" 
* #1708672 "THYMI HERBA"
* #1708672 ^designation[0].language = #de-AT 
* #1708672 ^designation[0].value = "THYMI HERBA" 
* #1708673 "SENNAE FOLIUM"
* #1708673 ^designation[0].language = #de-AT 
* #1708673 ^designation[0].value = "SENNAE FOLIUM" 
* #1708675 "ABACAVIR HEMISULFAT"
* #1708675 ^designation[0].language = #de-AT 
* #1708675 ^designation[0].value = "ABACAVIR HEMISULFAT" 
* #1708676 "DENOSUMAB"
* #1708676 ^designation[0].language = #de-AT 
* #1708676 ^designation[0].value = "DENOSUMAB" 
* #1708679 "PAZOPANIB"
* #1708679 ^designation[0].language = #de-AT 
* #1708679 ^designation[0].value = "PAZOPANIB" 
* #1708681 "LORNOXICAM"
* #1708681 ^designation[0].language = #de-AT 
* #1708681 ^designation[0].value = "LORNOXICAM" 
* #1708682 "AMBROXOL HYDROCHLORID"
* #1708682 ^designation[0].language = #de-AT 
* #1708682 ^designation[0].value = "AMBROXOL HYDROCHLORID" 
* #1708683 "POLYGONI HERBA"
* #1708683 ^designation[0].language = #de-AT 
* #1708683 ^designation[0].value = "POLYGONI HERBA" 
* #1708685 "PASSIFLORAE HERBA"
* #1708685 ^designation[0].language = #de-AT 
* #1708685 ^designation[0].value = "PASSIFLORAE HERBA" 
* #1708689 "SILODOSIN"
* #1708689 ^designation[0].language = #de-AT 
* #1708689 ^designation[0].value = "SILODOSIN" 
* #1708693 "MENTHAE PIPERITAE FOLIUM"
* #1708693 ^designation[0].language = #de-AT 
* #1708693 ^designation[0].value = "MENTHAE PIPERITAE FOLIUM" 
* #1708694 "MELISSAE FOLIUM"
* #1708694 ^designation[0].language = #de-AT 
* #1708694 ^designation[0].value = "MELISSAE FOLIUM" 
* #1708696 "TILIAE FLOS"
* #1708696 ^designation[0].language = #de-AT 
* #1708696 ^designation[0].value = "TILIAE FLOS" 
* #1708698 "HYPERICI HERBA"
* #1708698 ^designation[0].language = #de-AT 
* #1708698 ^designation[0].value = "HYPERICI HERBA" 
* #1708699 "HYPERICI HERBA (AUSZUG)"
* #1708699 ^designation[0].language = #de-AT 
* #1708699 ^designation[0].value = "HYPERICI HERBA (AUSZUG)" 
* #1708704 "LAVANDULAE AETHEROLEUM"
* #1708704 ^designation[0].language = #de-AT 
* #1708704 ^designation[0].value = "LAVANDULAE AETHEROLEUM" 
* #1708710 "LUPULI FLOS"
* #1708710 ^designation[0].language = #de-AT 
* #1708710 ^designation[0].value = "LUPULI FLOS" 
* #1708712 "CYANOCOBALAMIN"
* #1708712 ^designation[0].language = #de-AT 
* #1708712 ^designation[0].value = "CYANOCOBALAMIN" 
* #1708713 "CHLORHEXIDINDIGLUCONAT"
* #1708713 ^designation[0].language = #de-AT 
* #1708713 ^designation[0].value = "CHLORHEXIDINDIGLUCONAT" 
* #1708714 "ZOLPIDEMTARTRAT"
* #1708714 ^designation[0].language = #de-AT 
* #1708714 ^designation[0].value = "ZOLPIDEMTARTRAT" 
* #1708715 "SELEGILIN HYDROCHLORID"
* #1708715 ^designation[0].language = #de-AT 
* #1708715 ^designation[0].value = "SELEGILIN HYDROCHLORID" 
* #1708717 "TRANEXAMSÄURE"
* #1708717 ^designation[0].language = #de-AT 
* #1708717 ^designation[0].value = "TRANEXAMSÄURE" 
* #1708721 "TICLOPIDIN HYDROCHLORID"
* #1708721 ^designation[0].language = #de-AT 
* #1708721 ^designation[0].value = "TICLOPIDIN HYDROCHLORID" 
* #1708722 "AESCIN"
* #1708722 ^designation[0].language = #de-AT 
* #1708722 ^designation[0].value = "AESCIN" 
* #1708725 "EISEN(II)CHLORID"
* #1708725 ^designation[0].language = #de-AT 
* #1708725 ^designation[0].value = "EISEN(II)CHLORID" 
* #1708726 "CHLORHEXIDINDIGLUCONAT-LÖSUNG"
* #1708726 ^designation[0].language = #de-AT 
* #1708726 ^designation[0].value = "CHLORHEXIDINDIGLUCONAT-LÖSUNG" 
* #1708727 "RALOXIFEN HYDROCHLORID"
* #1708727 ^designation[0].language = #de-AT 
* #1708727 ^designation[0].value = "RALOXIFEN HYDROCHLORID" 
* #1708729 "SULPIRID"
* #1708729 ^designation[0].language = #de-AT 
* #1708729 ^designation[0].value = "SULPIRID" 
* #1708731 "LEVONORGESTREL"
* #1708731 ^designation[0].language = #de-AT 
* #1708731 ^designation[0].value = "LEVONORGESTREL" 
* #1708734 "DIHYDROERGOCRISTIN METHANSULFONAT"
* #1708734 ^designation[0].language = #de-AT 
* #1708734 ^designation[0].value = "DIHYDROERGOCRISTIN METHANSULFONAT" 
* #1708735 "TOBRAMYCIN"
* #1708735 ^designation[0].language = #de-AT 
* #1708735 ^designation[0].value = "TOBRAMYCIN" 
* #1708737 "PETHIDIN HYDROCHLORID"
* #1708737 ^designation[0].language = #de-AT 
* #1708737 ^designation[0].value = "PETHIDIN HYDROCHLORID" 
* #1708738 "DEQUALINIUM CHLORID"
* #1708738 ^designation[0].language = #de-AT 
* #1708738 ^designation[0].value = "DEQUALINIUM CHLORID" 
* #1708739 "CALCIUMASCORBAT"
* #1708739 ^designation[0].language = #de-AT 
* #1708739 ^designation[0].value = "CALCIUMASCORBAT" 
* #1708740 "TETRACYCLIN HYDROCHLORID"
* #1708740 ^designation[0].language = #de-AT 
* #1708740 ^designation[0].value = "TETRACYCLIN HYDROCHLORID" 
* #1708741 "PENCICLOVIR"
* #1708741 ^designation[0].language = #de-AT 
* #1708741 ^designation[0].value = "PENCICLOVIR" 
* #1708742 "DICLOFENAC DIETHYLAMIN"
* #1708742 ^designation[0].language = #de-AT 
* #1708742 ^designation[0].value = "DICLOFENAC DIETHYLAMIN" 
* #1708743 "NORFLOXACIN"
* #1708743 ^designation[0].language = #de-AT 
* #1708743 ^designation[0].value = "NORFLOXACIN" 
* #1708744 "LYSIN"
* #1708744 ^designation[0].language = #de-AT 
* #1708744 ^designation[0].value = "LYSIN" 
* #1708745 "ERGOTAMIN TARTRAT"
* #1708745 ^designation[0].language = #de-AT 
* #1708745 ^designation[0].value = "ERGOTAMIN TARTRAT" 
* #1708748 "PREDNISOLON HEXANOAT"
* #1708748 ^designation[0].language = #de-AT 
* #1708748 ^designation[0].value = "PREDNISOLON HEXANOAT" 
* #1708749 "CYSTIN"
* #1708749 ^designation[0].language = #de-AT 
* #1708749 ^designation[0].value = "CYSTIN" 
* #1708750 "SILDENAFIL CITRAT"
* #1708750 ^designation[0].language = #de-AT 
* #1708750 ^designation[0].value = "SILDENAFIL CITRAT" 
* #1708752 "THIAMIN HYDROCHLORID"
* #1708752 ^designation[0].language = #de-AT 
* #1708752 ^designation[0].value = "THIAMIN HYDROCHLORID" 
* #1708754 "PROCAIN HYDROCHLORID"
* #1708754 ^designation[0].language = #de-AT 
* #1708754 ^designation[0].value = "PROCAIN HYDROCHLORID" 
* #1708755 "EPINEPHRIN HYDROCHLORID"
* #1708755 ^designation[0].language = #de-AT 
* #1708755 ^designation[0].value = "EPINEPHRIN HYDROCHLORID" 
* #1708756 "FENTICONAZOL NITRAT"
* #1708756 ^designation[0].language = #de-AT 
* #1708756 ^designation[0].value = "FENTICONAZOL NITRAT" 
* #1708757 "PODOPHYLLOTOXIN"
* #1708757 ^designation[0].language = #de-AT 
* #1708757 ^designation[0].value = "PODOPHYLLOTOXIN" 
* #1708762 "SUFENTANILCITRAT"
* #1708762 ^designation[0].language = #de-AT 
* #1708762 ^designation[0].value = "SUFENTANILCITRAT" 
* #1708763 "TIAPRID HYDROCHLORID"
* #1708763 ^designation[0].language = #de-AT 
* #1708763 ^designation[0].value = "TIAPRID HYDROCHLORID" 
* #1708765 "TOPOTECAN HYDROCHLORID"
* #1708765 ^designation[0].language = #de-AT 
* #1708765 ^designation[0].value = "TOPOTECAN HYDROCHLORID" 
* #1708766 "THYMI TYPO THYMOLO AETHEROLEUM"
* #1708766 ^designation[0].language = #de-AT 
* #1708766 ^designation[0].value = "THYMI TYPO THYMOLO AETHEROLEUM" 
* #1708767 "CLOPIDOGREL"
* #1708767 ^designation[0].language = #de-AT 
* #1708767 ^designation[0].value = "CLOPIDOGREL" 
* #1708768 "ATENOLOL"
* #1708768 ^designation[0].language = #de-AT 
* #1708768 ^designation[0].value = "ATENOLOL" 
* #1708769 "VALERIANAE RADIX"
* #1708769 ^designation[0].language = #de-AT 
* #1708769 ^designation[0].value = "VALERIANAE RADIX" 
* #1708771 "TANACETI PARTHENII HERBA"
* #1708771 ^designation[0].language = #de-AT 
* #1708771 ^designation[0].value = "TANACETI PARTHENII HERBA" 
* #1708772 "DONEPEZIL HYDROCHLORID"
* #1708772 ^designation[0].language = #de-AT 
* #1708772 ^designation[0].value = "DONEPEZIL HYDROCHLORID" 
* #1708773 "MILNACIPRAN HYDROCHLORID"
* #1708773 ^designation[0].language = #de-AT 
* #1708773 ^designation[0].value = "MILNACIPRAN HYDROCHLORID" 
* #1708775 "NATRIUM PREDNISOLON M-SULFOBENZOAT"
* #1708775 ^designation[0].language = #de-AT 
* #1708775 ^designation[0].value = "NATRIUM PREDNISOLON M-SULFOBENZOAT" 
* #1708776 "RIFAMYCIN NATRIUM"
* #1708776 ^designation[0].language = #de-AT 
* #1708776 ^designation[0].value = "RIFAMYCIN NATRIUM" 
* #1708777 "TRIMETAZIDIN DIHYDROCHLORID"
* #1708777 ^designation[0].language = #de-AT 
* #1708777 ^designation[0].value = "TRIMETAZIDIN DIHYDROCHLORID" 
* #1708779 "CITALOPRAM HYDROCHLORID"
* #1708779 ^designation[0].language = #de-AT 
* #1708779 ^designation[0].value = "CITALOPRAM HYDROCHLORID" 
* #1708780 "MAGNESIUMCITRAT"
* #1708780 ^designation[0].language = #de-AT 
* #1708780 ^designation[0].value = "MAGNESIUMCITRAT" 
* #1708781 "RETINOLPALMITAT"
* #1708781 ^designation[0].language = #de-AT 
* #1708781 ^designation[0].value = "RETINOLPALMITAT" 
* #1708782 "REPAGLINID"
* #1708782 ^designation[0].language = #de-AT 
* #1708782 ^designation[0].value = "REPAGLINID" 
* #1708787 "RASAGILIN MESILAT"
* #1708787 ^designation[0].language = #de-AT 
* #1708787 ^designation[0].value = "RASAGILIN MESILAT" 
* #1708789 "BUPIVACAINHYDROCHLORID"
* #1708789 ^designation[0].language = #de-AT 
* #1708789 ^designation[0].value = "BUPIVACAINHYDROCHLORID" 
* #1708790 "PROPRANOLOL HYDROCHLORID"
* #1708790 ^designation[0].language = #de-AT 
* #1708790 ^designation[0].value = "PROPRANOLOL HYDROCHLORID" 
* #1708791 "APIXABAN"
* #1708791 ^designation[0].language = #de-AT 
* #1708791 ^designation[0].value = "APIXABAN" 
* #1708792 "DALTEPARIN NATRIUM"
* #1708792 ^designation[0].language = #de-AT 
* #1708792 ^designation[0].value = "DALTEPARIN NATRIUM" 
* #1708793 "ZOFENOPRIL CALCIUM"
* #1708793 ^designation[0].language = #de-AT 
* #1708793 ^designation[0].value = "ZOFENOPRIL CALCIUM" 
* #1708794 "EPROSARTAN MESILAT"
* #1708794 ^designation[0].language = #de-AT 
* #1708794 ^designation[0].value = "EPROSARTAN MESILAT" 
* #1708795 "BUDESONID"
* #1708795 ^designation[0].language = #de-AT 
* #1708795 ^designation[0].value = "BUDESONID" 
* #1708797 "QUETIAPIN FUMARAT"
* #1708797 ^designation[0].language = #de-AT 
* #1708797 ^designation[0].value = "QUETIAPIN FUMARAT" 
* #1708800 "KALIUMIODID"
* #1708800 ^designation[0].language = #de-AT 
* #1708800 ^designation[0].value = "KALIUMIODID" 
* #1708801 "SCHWEFELHEXAFLUORID"
* #1708801 ^designation[0].language = #de-AT 
* #1708801 ^designation[0].value = "SCHWEFELHEXAFLUORID" 
* #1708802 "VENLAFAXIN HYDROCHLORID"
* #1708802 ^designation[0].language = #de-AT 
* #1708802 ^designation[0].value = "VENLAFAXIN HYDROCHLORID" 
* #1708807 "NATRIUM EPOPROSTENOLAT"
* #1708807 ^designation[0].language = #de-AT 
* #1708807 ^designation[0].value = "NATRIUM EPOPROSTENOLAT" 
* #1708808 "EPHEDRINHYDROCHLORID"
* #1708808 ^designation[0].language = #de-AT 
* #1708808 ^designation[0].value = "EPHEDRINHYDROCHLORID" 
* #1708809 "ENALAPRIL MALEAT"
* #1708809 ^designation[0].language = #de-AT 
* #1708809 ^designation[0].value = "ENALAPRIL MALEAT" 
* #1708811 "EPOETIN BETA"
* #1708811 ^designation[0].language = #de-AT 
* #1708811 ^designation[0].value = "EPOETIN BETA" 
* #1708814 "PAROXETIN MESILAT"
* #1708814 ^designation[0].language = #de-AT 
* #1708814 ^designation[0].value = "PAROXETIN MESILAT" 
* #1708818 "NARATRIPTAN HYDROCHLORID"
* #1708818 ^designation[0].language = #de-AT 
* #1708818 ^designation[0].value = "NARATRIPTAN HYDROCHLORID" 
* #1708819 "CEFALEXIN"
* #1708819 ^designation[0].language = #de-AT 
* #1708819 ^designation[0].value = "CEFALEXIN" 
* #1708820 "LYNESTRENOL"
* #1708820 ^designation[0].language = #de-AT 
* #1708820 ^designation[0].value = "LYNESTRENOL" 
* #1708821 "VINCRISTIN SULFAT"
* #1708821 ^designation[0].language = #de-AT 
* #1708821 ^designation[0].value = "VINCRISTIN SULFAT" 
* #1708822 "PIVMECILLINAM HYDROCHLORID"
* #1708822 ^designation[0].language = #de-AT 
* #1708822 ^designation[0].value = "PIVMECILLINAM HYDROCHLORID" 
* #1708824 "MENTHOL"
* #1708824 ^designation[0].language = #de-AT 
* #1708824 ^designation[0].value = "MENTHOL" 
* #1708825 "METFORMIN HYDROCHLORID"
* #1708825 ^designation[0].language = #de-AT 
* #1708825 ^designation[0].value = "METFORMIN HYDROCHLORID" 
* #1708826 "METOPROLOL SUCCINAT"
* #1708826 ^designation[0].language = #de-AT 
* #1708826 ^designation[0].value = "METOPROLOL SUCCINAT" 
* #1708827 "PROGUANIL HYDROCHLORID"
* #1708827 ^designation[0].language = #de-AT 
* #1708827 ^designation[0].value = "PROGUANIL HYDROCHLORID" 
* #1708828 "SOTALOL HYDROCHLORID"
* #1708828 ^designation[0].language = #de-AT 
* #1708828 ^designation[0].value = "SOTALOL HYDROCHLORID" 
* #1708829 "VALGANCICLOVIR HYDROCHLORID"
* #1708829 ^designation[0].language = #de-AT 
* #1708829 ^designation[0].value = "VALGANCICLOVIR HYDROCHLORID" 
* #1708832 "SALMETEROL XINAFOAT"
* #1708832 ^designation[0].language = #de-AT 
* #1708832 ^designation[0].value = "SALMETEROL XINAFOAT" 
* #1708833 "MANNITOL, E-421"
* #1708833 ^designation[0].language = #de-AT 
* #1708833 ^designation[0].value = "MANNITOL, E-421" 
* #1708835 "METHYLPREDNISOLONHYDROGENSUCCINAT"
* #1708835 ^designation[0].language = #de-AT 
* #1708835 ^designation[0].value = "METHYLPREDNISOLONHYDROGENSUCCINAT" 
* #1708839 "INFLUENZAVIRUS (AUSZUG, PRODUKTE)"
* #1708839 ^designation[0].language = #de-AT 
* #1708839 ^designation[0].value = "INFLUENZAVIRUS (AUSZUG, PRODUKTE)" 
* #1708841 "EISEN(II)-GLUCONAT"
* #1708841 ^designation[0].language = #de-AT 
* #1708841 ^designation[0].value = "EISEN(II)-GLUCONAT" 
* #1708842 "MELPHALAN"
* #1708842 ^designation[0].language = #de-AT 
* #1708842 ^designation[0].value = "MELPHALAN" 
* #1708846 "ACENOCUMAROL"
* #1708846 ^designation[0].language = #de-AT 
* #1708846 ^designation[0].value = "ACENOCUMAROL" 
* #1708848 "HYDROXOCOBALAMIN"
* #1708848 ^designation[0].language = #de-AT 
* #1708848 ^designation[0].value = "HYDROXOCOBALAMIN" 
* #1708849 "IOPAMIDOL"
* #1708849 ^designation[0].language = #de-AT 
* #1708849 ^designation[0].value = "IOPAMIDOL" 
* #1708850 "CEFUROXIM-NATRIUM"
* #1708850 ^designation[0].language = #de-AT 
* #1708850 ^designation[0].value = "CEFUROXIM-NATRIUM" 
* #1708852 "CETUXIMAB"
* #1708852 ^designation[0].language = #de-AT 
* #1708852 ^designation[0].value = "CETUXIMAB" 
* #1708853 "PIOGLITAZON HYDROCHLORID"
* #1708853 ^designation[0].language = #de-AT 
* #1708853 ^designation[0].value = "PIOGLITAZON HYDROCHLORID" 
* #1708854 "ORLISTAT"
* #1708854 ^designation[0].language = #de-AT 
* #1708854 ^designation[0].value = "ORLISTAT" 
* #1708855 "MIRTAZAPIN"
* #1708855 ^designation[0].language = #de-AT 
* #1708855 ^designation[0].value = "MIRTAZAPIN" 
* #1708856 "LOSARTAN KALIUM"
* #1708856 ^designation[0].language = #de-AT 
* #1708856 ^designation[0].value = "LOSARTAN KALIUM" 
* #1708857 "METHADONHYDROCHLORID"
* #1708857 ^designation[0].language = #de-AT 
* #1708857 ^designation[0].value = "METHADONHYDROCHLORID" 
* #1708858 "NATRIUM ASCORBAT"
* #1708858 ^designation[0].language = #de-AT 
* #1708858 ^designation[0].value = "NATRIUM ASCORBAT" 
* #1708859 "NATRIUM VALPROAT"
* #1708859 ^designation[0].language = #de-AT 
* #1708859 ^designation[0].value = "NATRIUM VALPROAT" 
* #1708860 "ROSUVASTATIN CALCIUM"
* #1708860 ^designation[0].language = #de-AT 
* #1708860 ^designation[0].value = "ROSUVASTATIN CALCIUM" 
* #1708861 "BORTEZOMIB"
* #1708861 ^designation[0].language = #de-AT 
* #1708861 ^designation[0].value = "BORTEZOMIB" 
* #1708862 "REBOXETIN MESILAT"
* #1708862 ^designation[0].language = #de-AT 
* #1708862 ^designation[0].value = "REBOXETIN MESILAT" 
* #1708863 "ETANERCEPT"
* #1708863 ^designation[0].language = #de-AT 
* #1708863 ^designation[0].value = "ETANERCEPT" 
* #1708865 "MONTELUKAST NATRIUM"
* #1708865 ^designation[0].language = #de-AT 
* #1708865 ^designation[0].value = "MONTELUKAST NATRIUM" 
* #1708866 "METILDIGOXIN"
* #1708866 ^designation[0].language = #de-AT 
* #1708866 ^designation[0].value = "METILDIGOXIN" 
* #1708868 "OSELTAMIVIR PHOSPHAT"
* #1708868 ^designation[0].language = #de-AT 
* #1708868 ^designation[0].value = "OSELTAMIVIR PHOSPHAT" 
* #1708869 "ROPINIROL HYDROCHLORID"
* #1708869 ^designation[0].language = #de-AT 
* #1708869 ^designation[0].value = "ROPINIROL HYDROCHLORID" 
* #1708870 "CETIRIZIN DIHYDROCHLORID"
* #1708870 ^designation[0].language = #de-AT 
* #1708870 ^designation[0].value = "CETIRIZIN DIHYDROCHLORID" 
* #1708873 "CHORIOGONADOTROPIN ALFA"
* #1708873 ^designation[0].language = #de-AT 
* #1708873 ^designation[0].value = "CHORIOGONADOTROPIN ALFA" 
* #1708875 "AZATHIOPRIN"
* #1708875 ^designation[0].language = #de-AT 
* #1708875 ^designation[0].value = "AZATHIOPRIN" 
* #1708876 "PROPAFENON HYDROCHLORID"
* #1708876 ^designation[0].language = #de-AT 
* #1708876 ^designation[0].value = "PROPAFENON HYDROCHLORID" 
* #1708878 "THEOPHYLLIN"
* #1708878 ^designation[0].language = #de-AT 
* #1708878 ^designation[0].value = "THEOPHYLLIN" 
* #1708880 "THYROTROPIN ALFA"
* #1708880 ^designation[0].language = #de-AT 
* #1708880 ^designation[0].value = "THYROTROPIN ALFA" 
* #1708881 "JECORIS ASELLI OLEUM"
* #1708881 ^designation[0].language = #de-AT 
* #1708881 ^designation[0].value = "JECORIS ASELLI OLEUM" 
* #1708884 "BETAMETHASON DIPROPIONAT"
* #1708884 ^designation[0].language = #de-AT 
* #1708884 ^designation[0].value = "BETAMETHASON DIPROPIONAT" 
* #1708885 "CLOBETASOL PROPIONAT"
* #1708885 ^designation[0].language = #de-AT 
* #1708885 ^designation[0].value = "CLOBETASOL PROPIONAT" 
* #1708886 "CALCIUMCARBONAT"
* #1708886 ^designation[0].language = #de-AT 
* #1708886 ^designation[0].value = "CALCIUMCARBONAT" 
* #1708887 "PREDNISOLON"
* #1708887 ^designation[0].language = #de-AT 
* #1708887 ^designation[0].value = "PREDNISOLON" 
* #1708889 "NATRIUM GLUCONAT"
* #1708889 ^designation[0].language = #de-AT 
* #1708889 ^designation[0].value = "NATRIUM GLUCONAT" 
* #1708890 "CANDESARTAN CILEXETIL"
* #1708890 ^designation[0].language = #de-AT 
* #1708890 ^designation[0].value = "CANDESARTAN CILEXETIL" 
* #1708891 "CIPROFLOXACINHYDROCHLORID"
* #1708891 ^designation[0].language = #de-AT 
* #1708891 ^designation[0].value = "CIPROFLOXACINHYDROCHLORID" 
* #1708893 "PROPOFOL"
* #1708893 ^designation[0].language = #de-AT 
* #1708893 ^designation[0].value = "PROPOFOL" 
* #1708894 "ALFACALCIDOL"
* #1708894 ^designation[0].language = #de-AT 
* #1708894 ^designation[0].value = "ALFACALCIDOL" 
* #1708896 "SUCRALFAT"
* #1708896 ^designation[0].language = #de-AT 
* #1708896 ^designation[0].value = "SUCRALFAT" 
* #1708898 "NATRIUMFLUORID"
* #1708898 ^designation[0].language = #de-AT 
* #1708898 ^designation[0].value = "NATRIUMFLUORID" 
* #1708899 "FLUOXETIN HYDROCHLORID"
* #1708899 ^designation[0].language = #de-AT 
* #1708899 ^designation[0].value = "FLUOXETIN HYDROCHLORID" 
* #1708900 "BECLOMETASONDIPROPIONAT"
* #1708900 ^designation[0].language = #de-AT 
* #1708900 ^designation[0].value = "BECLOMETASONDIPROPIONAT" 
* #1708901 "BENSERAZID HYDROCHLORID"
* #1708901 ^designation[0].language = #de-AT 
* #1708901 ^designation[0].value = "BENSERAZID HYDROCHLORID" 
* #1708904 "GLATIRAMER ACETAT"
* #1708904 ^designation[0].language = #de-AT 
* #1708904 ^designation[0].value = "GLATIRAMER ACETAT" 
* #1708906 "LITHIUMCARBONAT"
* #1708906 ^designation[0].language = #de-AT 
* #1708906 ^designation[0].value = "LITHIUMCARBONAT" 
* #1708907 "TRABECTEDIN"
* #1708907 ^designation[0].language = #de-AT 
* #1708907 ^designation[0].value = "TRABECTEDIN" 
* #1708908 "SULFASALAZIN"
* #1708908 ^designation[0].language = #de-AT 
* #1708908 ^designation[0].value = "SULFASALAZIN" 
* #1708909 "LEUPRORELIN ACETAT"
* #1708909 ^designation[0].language = #de-AT 
* #1708909 ^designation[0].value = "LEUPRORELIN ACETAT" 
* #1708911 "BEMIPARIN NATRIUM"
* #1708911 ^designation[0].language = #de-AT 
* #1708911 ^designation[0].value = "BEMIPARIN NATRIUM" 
* #1708912 "CEFPODOXIM PROXETIL"
* #1708912 ^designation[0].language = #de-AT 
* #1708912 ^designation[0].value = "CEFPODOXIM PROXETIL" 
* #1708913 "CALCIUM DOBESILAT"
* #1708913 ^designation[0].language = #de-AT 
* #1708913 ^designation[0].value = "CALCIUM DOBESILAT" 
* #1708914 "AMMONIUMCHLORID"
* #1708914 ^designation[0].language = #de-AT 
* #1708914 ^designation[0].value = "AMMONIUMCHLORID" 
* #1708917 "LANREOTID ACETAT"
* #1708917 ^designation[0].language = #de-AT 
* #1708917 ^designation[0].value = "LANREOTID ACETAT" 
* #1708918 "OCTREOTID ACETAT"
* #1708918 ^designation[0].language = #de-AT 
* #1708918 ^designation[0].value = "OCTREOTID ACETAT" 
* #1708920 "FLUOROURACIL"
* #1708920 ^designation[0].language = #de-AT 
* #1708920 ^designation[0].value = "FLUOROURACIL" 
* #1708921 "FLUCYTOSIN"
* #1708921 ^designation[0].language = #de-AT 
* #1708921 ^designation[0].value = "FLUCYTOSIN" 
* #1708922 "AZTREONAM"
* #1708922 ^designation[0].language = #de-AT 
* #1708922 ^designation[0].value = "AZTREONAM" 
* #1708923 "AMOXICILLIN"
* #1708923 ^designation[0].language = #de-AT 
* #1708923 ^designation[0].value = "AMOXICILLIN" 
* #1708924 "EZETIMIB"
* #1708924 ^designation[0].language = #de-AT 
* #1708924 ^designation[0].value = "EZETIMIB" 
* #1708927 "IOVERSOL"
* #1708927 ^designation[0].language = #de-AT 
* #1708927 ^designation[0].value = "IOVERSOL" 
* #1708929 "VERTEPORFIN"
* #1708929 ^designation[0].language = #de-AT 
* #1708929 ^designation[0].value = "VERTEPORFIN" 
* #1708930 "RIFABUTIN"
* #1708930 ^designation[0].language = #de-AT 
* #1708930 ^designation[0].value = "RIFABUTIN" 
* #1708931 "BENZYLPENICILLIN NATRIUM"
* #1708931 ^designation[0].language = #de-AT 
* #1708931 ^designation[0].value = "BENZYLPENICILLIN NATRIUM" 
* #1708932 "MEDROXYPROGESTERON ACETAT"
* #1708932 ^designation[0].language = #de-AT 
* #1708932 ^designation[0].value = "MEDROXYPROGESTERON ACETAT" 
* #1708933 "DARBEPOETIN ALFA"
* #1708933 ^designation[0].language = #de-AT 
* #1708933 ^designation[0].value = "DARBEPOETIN ALFA" 
* #1708934 "MICAFUNGIN"
* #1708934 ^designation[0].language = #de-AT 
* #1708934 ^designation[0].value = "MICAFUNGIN" 
* #1708935 "AMILORID HYDROCHLORID"
* #1708935 ^designation[0].language = #de-AT 
* #1708935 ^designation[0].value = "AMILORID HYDROCHLORID" 
* #1708938 "ARGATROBAN"
* #1708938 ^designation[0].language = #de-AT 
* #1708938 ^designation[0].value = "ARGATROBAN" 
* #1708940 "PENTOXIFYLLIN"
* #1708940 ^designation[0].language = #de-AT 
* #1708940 ^designation[0].value = "PENTOXIFYLLIN" 
* #1708942 "LERCANIDIPIN HYDROCHLORID"
* #1708942 ^designation[0].language = #de-AT 
* #1708942 ^designation[0].value = "LERCANIDIPIN HYDROCHLORID" 
* #1708944 "LACTULOSE"
* #1708944 ^designation[0].language = #de-AT 
* #1708944 ^designation[0].value = "LACTULOSE" 
* #1708946 "LEVOTHYROXIN NATRIUM"
* #1708946 ^designation[0].language = #de-AT 
* #1708946 ^designation[0].value = "LEVOTHYROXIN NATRIUM" 
* #1708947 "NALOXON HYDROCHLORID"
* #1708947 ^designation[0].language = #de-AT 
* #1708947 ^designation[0].value = "NALOXON HYDROCHLORID" 
* #1708948 "ISOFLURAN"
* #1708948 ^designation[0].language = #de-AT 
* #1708948 ^designation[0].value = "ISOFLURAN" 
* #1708949 "KETOROLAC-TROMETAMOL"
* #1708949 ^designation[0].language = #de-AT 
* #1708949 ^designation[0].value = "KETOROLAC-TROMETAMOL" 
* #1708951 "LACTITOL, E-966"
* #1708951 ^designation[0].language = #de-AT 
* #1708951 ^designation[0].value = "LACTITOL, E-966" 
* #1708954 "GADOVERSETAMID"
* #1708954 ^designation[0].language = #de-AT 
* #1708954 ^designation[0].value = "GADOVERSETAMID" 
* #1708955 "LEVETIRACETAM"
* #1708955 ^designation[0].language = #de-AT 
* #1708955 ^designation[0].value = "LEVETIRACETAM" 
* #1708956 "CILAZAPRIL"
* #1708956 ^designation[0].language = #de-AT 
* #1708956 ^designation[0].value = "CILAZAPRIL" 
* #1708957 "IVERMECTIN"
* #1708957 ^designation[0].language = #de-AT 
* #1708957 ^designation[0].value = "IVERMECTIN" 
* #1708959 "POVIDON-IOD"
* #1708959 ^designation[0].language = #de-AT 
* #1708959 ^designation[0].value = "POVIDON-IOD" 
* #1708960 "SUMATRIPTAN"
* #1708960 ^designation[0].language = #de-AT 
* #1708960 ^designation[0].value = "SUMATRIPTAN" 
* #1708961 "ROMIPLOSTIM"
* #1708961 ^designation[0].language = #de-AT 
* #1708961 ^designation[0].value = "ROMIPLOSTIM" 
* #1708962 "SEVELAMERCARBONAT"
* #1708962 ^designation[0].language = #de-AT 
* #1708962 ^designation[0].value = "SEVELAMERCARBONAT" 
* #1708963 "TOLVAPTAN"
* #1708963 ^designation[0].language = #de-AT 
* #1708963 ^designation[0].value = "TOLVAPTAN" 
* #1708964 "GANIRELIXACETAT"
* #1708964 ^designation[0].language = #de-AT 
* #1708964 ^designation[0].value = "GANIRELIXACETAT" 
* #1708965 "RALTEGRAVIR"
* #1708965 ^designation[0].language = #de-AT 
* #1708965 ^designation[0].value = "RALTEGRAVIR" 
* #1708966 "BENZYDAMIN HYDROCHLORID"
* #1708966 ^designation[0].language = #de-AT 
* #1708966 ^designation[0].value = "BENZYDAMIN HYDROCHLORID" 
* #1708967 "ITRACONAZOL"
* #1708967 ^designation[0].language = #de-AT 
* #1708967 ^designation[0].value = "ITRACONAZOL" 
* #1708968 "SAPROPTERINDIHYDROCHLORID"
* #1708968 ^designation[0].language = #de-AT 
* #1708968 ^designation[0].value = "SAPROPTERINDIHYDROCHLORID" 
* #1708969 "TOCILIZUMAB"
* #1708969 ^designation[0].language = #de-AT 
* #1708969 ^designation[0].value = "TOCILIZUMAB" 
* #1708970 "USTEKINUMAB"
* #1708970 ^designation[0].language = #de-AT 
* #1708970 ^designation[0].value = "USTEKINUMAB" 
* #1708971 "MEFENAMINSÄURE"
* #1708971 ^designation[0].language = #de-AT 
* #1708971 ^designation[0].value = "MEFENAMINSÄURE" 
* #1708972 "GOLIMUMAB"
* #1708972 ^designation[0].language = #de-AT 
* #1708972 ^designation[0].value = "GOLIMUMAB" 
* #1708973 "ESKETAMIN HYDROCHLORID"
* #1708973 ^designation[0].language = #de-AT 
* #1708973 ^designation[0].value = "ESKETAMIN HYDROCHLORID" 
* #1708975 "MEROPENEM"
* #1708975 ^designation[0].language = #de-AT 
* #1708975 ^designation[0].value = "MEROPENEM" 
* #1708976 "LIDOCAIN"
* #1708976 ^designation[0].language = #de-AT 
* #1708976 ^designation[0].value = "LIDOCAIN" 
* #1708977 "METAMIZOL NATRIUM"
* #1708977 ^designation[0].language = #de-AT 
* #1708977 ^designation[0].value = "METAMIZOL NATRIUM" 
* #1708979 "HYDROXYETHYLSTÄRKE"
* #1708979 ^designation[0].language = #de-AT 
* #1708979 ^designation[0].value = "HYDROXYETHYLSTÄRKE" 
* #1708981 "IMIQUIMOD"
* #1708981 ^designation[0].language = #de-AT 
* #1708981 ^designation[0].value = "IMIQUIMOD" 
* #1708982 "EMTRICITABIN"
* #1708982 ^designation[0].language = #de-AT 
* #1708982 ^designation[0].value = "EMTRICITABIN" 
* #1708983 "ETHINYLESTRADIOL"
* #1708983 ^designation[0].language = #de-AT 
* #1708983 ^designation[0].value = "ETHINYLESTRADIOL" 
* #1708986 "PIMECROLIMUS"
* #1708986 ^designation[0].language = #de-AT 
* #1708986 ^designation[0].value = "PIMECROLIMUS" 
* #1708987 "DIPYRIDAMOL"
* #1708987 ^designation[0].language = #de-AT 
* #1708987 ^designation[0].value = "DIPYRIDAMOL" 
* #1708988 "PILOCARPIN HYDROCHLORID"
* #1708988 ^designation[0].language = #de-AT 
* #1708988 ^designation[0].value = "PILOCARPIN HYDROCHLORID" 
* #1708989 "HYDROCHLOROTHIAZID"
* #1708989 ^designation[0].language = #de-AT 
* #1708989 ^designation[0].value = "HYDROCHLOROTHIAZID" 
* #1708990 "GEFITINIB"
* #1708990 ^designation[0].language = #de-AT 
* #1708990 ^designation[0].value = "GEFITINIB" 
* #1708991 "ESTRADIOL"
* #1708991 ^designation[0].language = #de-AT 
* #1708991 ^designation[0].value = "ESTRADIOL" 
* #1708992 "LYSIN ACETAT"
* #1708992 ^designation[0].language = #de-AT 
* #1708992 ^designation[0].value = "LYSIN ACETAT" 
* #1708993 "VARENICLINTARTRAT"
* #1708993 ^designation[0].language = #de-AT 
* #1708993 ^designation[0].value = "VARENICLINTARTRAT" 
* #1708996 "DINATRIUMMOLYBDAT"
* #1708996 ^designation[0].language = #de-AT 
* #1708996 ^designation[0].value = "DINATRIUMMOLYBDAT" 
* #1708998 "NITRAZEPAM"
* #1708998 ^designation[0].language = #de-AT 
* #1708998 ^designation[0].value = "NITRAZEPAM" 
* #1708999 "KETOPROFEN"
* #1708999 ^designation[0].language = #de-AT 
* #1708999 ^designation[0].value = "KETOPROFEN" 
* #1709001 "YOHIMBIN HYDROCHLORID"
* #1709001 ^designation[0].language = #de-AT 
* #1709001 ^designation[0].value = "YOHIMBIN HYDROCHLORID" 
* #1709002 "THYMOL"
* #1709002 ^designation[0].language = #de-AT 
* #1709002 ^designation[0].value = "THYMOL" 
* #1709003 "ÄPFELSÄURE"
* #1709003 ^designation[0].language = #de-AT 
* #1709003 ^designation[0].value = "ÄPFELSÄURE" 
* #1709004 "KALIUMCITRAT"
* #1709004 ^designation[0].language = #de-AT 
* #1709004 ^designation[0].value = "KALIUMCITRAT" 
* #1709005 "POLIOMYELITISVIRUS"
* #1709005 ^designation[0].language = #de-AT 
* #1709005 ^designation[0].value = "POLIOMYELITISVIRUS" 
* #1709006 "CHROMCHLORID"
* #1709006 ^designation[0].language = #de-AT 
* #1709006 ^designation[0].value = "CHROMCHLORID" 
* #1709007 "SIMVASTATIN"
* #1709007 ^designation[0].language = #de-AT 
* #1709007 ^designation[0].value = "SIMVASTATIN" 
* #1709008 "PRAVASTATIN NATRIUM"
* #1709008 ^designation[0].language = #de-AT 
* #1709008 ^designation[0].value = "PRAVASTATIN NATRIUM" 
* #1709009 "BRIMONIDIN TARTRAT"
* #1709009 ^designation[0].language = #de-AT 
* #1709009 ^designation[0].value = "BRIMONIDIN TARTRAT" 
* #1709010 "FUSIDINSÄURE"
* #1709010 ^designation[0].language = #de-AT 
* #1709010 ^designation[0].value = "FUSIDINSÄURE" 
* #1709012 "TIZANIDIN HYDROCHLORID"
* #1709012 ^designation[0].language = #de-AT 
* #1709012 ^designation[0].value = "TIZANIDIN HYDROCHLORID" 
* #1709014 "ALFUZOSIN HYDROCHLORID"
* #1709014 ^designation[0].language = #de-AT 
* #1709014 ^designation[0].value = "ALFUZOSIN HYDROCHLORID" 
* #1709015 "METHYLPREDNISOLON"
* #1709015 ^designation[0].language = #de-AT 
* #1709015 ^designation[0].value = "METHYLPREDNISOLON" 
* #1709016 "KALIUMCHLORID"
* #1709016 ^designation[0].language = #de-AT 
* #1709016 ^designation[0].value = "KALIUMCHLORID" 
* #1709017 "SAQUINAVIR MESILAT"
* #1709017 ^designation[0].language = #de-AT 
* #1709017 ^designation[0].value = "SAQUINAVIR MESILAT" 
* #1709018 "ZOLEDRONSÄURE"
* #1709018 ^designation[0].language = #de-AT 
* #1709018 ^designation[0].value = "ZOLEDRONSÄURE" 
* #1709019 "THIOPENTAL NATRIUM"
* #1709019 ^designation[0].language = #de-AT 
* #1709019 ^designation[0].value = "THIOPENTAL NATRIUM" 
* #1709021 "BETA-CAROTIN"
* #1709021 ^designation[0].language = #de-AT 
* #1709021 ^designation[0].value = "BETA-CAROTIN" 
* #1709023 "IMATINIB MESILAT"
* #1709023 ^designation[0].language = #de-AT 
* #1709023 ^designation[0].value = "IMATINIB MESILAT" 
* #1709024 "KALIUMHYDROXID"
* #1709024 ^designation[0].language = #de-AT 
* #1709024 ^designation[0].value = "KALIUMHYDROXID" 
* #1709025 "NATRIUMCITRAT"
* #1709025 ^designation[0].language = #de-AT 
* #1709025 ^designation[0].value = "NATRIUMCITRAT" 
* #1709027 "NATRIUMPHENYLBUTYRAT"
* #1709027 ^designation[0].language = #de-AT 
* #1709027 ^designation[0].value = "NATRIUMPHENYLBUTYRAT" 
* #1709029 "MINOCYCLIN HYDROCHLORID"
* #1709029 ^designation[0].language = #de-AT 
* #1709029 ^designation[0].value = "MINOCYCLIN HYDROCHLORID" 
* #1709031 "DEGARELIX"
* #1709031 ^designation[0].language = #de-AT 
* #1709031 ^designation[0].value = "DEGARELIX" 
* #1709032 "PERMETHRIN"
* #1709032 ^designation[0].language = #de-AT 
* #1709032 ^designation[0].value = "PERMETHRIN" 
* #1709033 "PREGABALIN"
* #1709033 ^designation[0].language = #de-AT 
* #1709033 ^designation[0].value = "PREGABALIN" 
* #1709035 "ASPARAGINSÄURE"
* #1709035 ^designation[0].language = #de-AT 
* #1709035 ^designation[0].value = "ASPARAGINSÄURE" 
* #1709036 "RACEMISCHER CAMPHER"
* #1709036 ^designation[0].language = #de-AT 
* #1709036 ^designation[0].value = "RACEMISCHER CAMPHER" 
* #1709038 "DARUNAVIR ETHANOLAT"
* #1709038 ^designation[0].language = #de-AT 
* #1709038 ^designation[0].value = "DARUNAVIR ETHANOLAT" 
* #1709039 "PYRIDOSTIGMIN BROMID"
* #1709039 ^designation[0].language = #de-AT 
* #1709039 ^designation[0].value = "PYRIDOSTIGMIN BROMID" 
* #1709041 "MICONAZOL NITRAT"
* #1709041 ^designation[0].language = #de-AT 
* #1709041 ^designation[0].value = "MICONAZOL NITRAT" 
* #1709042 "MOMETASON FUROAT"
* #1709042 ^designation[0].language = #de-AT 
* #1709042 ^designation[0].value = "MOMETASON FUROAT" 
* #1709045 "MIFAMURTID"
* #1709045 ^designation[0].language = #de-AT 
* #1709045 ^designation[0].value = "MIFAMURTID" 
* #1709046 "SITAGLIPTIN"
* #1709046 ^designation[0].language = #de-AT 
* #1709046 ^designation[0].value = "SITAGLIPTIN" 
* #1709047 "RÖTELNVIRUS"
* #1709047 ^designation[0].language = #de-AT 
* #1709047 ^designation[0].value = "RÖTELNVIRUS" 
* #1709050 "FLUOCORTOLON PIVALAT"
* #1709050 ^designation[0].language = #de-AT 
* #1709050 ^designation[0].value = "FLUOCORTOLON PIVALAT" 
* #1709051 "FLUPREDNIDEN ACETAT"
* #1709051 ^designation[0].language = #de-AT 
* #1709051 ^designation[0].value = "FLUPREDNIDEN ACETAT" 
* #1709054 "INFLUENZAVIRUS"
* #1709054 ^designation[0].language = #de-AT 
* #1709054 ^designation[0].value = "INFLUENZAVIRUS" 
* #1709057 "ALLOPURINOL"
* #1709057 ^designation[0].language = #de-AT 
* #1709057 ^designation[0].value = "ALLOPURINOL" 
* #1709058 "ALPRAZOLAM"
* #1709058 ^designation[0].language = #de-AT 
* #1709058 ^designation[0].value = "ALPRAZOLAM" 
* #1709059 "NATRIUM PREDNISOLON SUCCINAT"
* #1709059 ^designation[0].language = #de-AT 
* #1709059 ^designation[0].value = "NATRIUM PREDNISOLON SUCCINAT" 
* #1709061 "BORNAPRIN HYDROCHLORID"
* #1709061 ^designation[0].language = #de-AT 
* #1709061 ^designation[0].value = "BORNAPRIN HYDROCHLORID" 
* #1709062 "LISINOPRIL"
* #1709062 ^designation[0].language = #de-AT 
* #1709062 ^designation[0].value = "LISINOPRIL" 
* #1709064 "BROMHEXIN HYDROCHLORID"
* #1709064 ^designation[0].language = #de-AT 
* #1709064 ^designation[0].value = "BROMHEXIN HYDROCHLORID" 
* #1709065 "DIMENHYDRINAT"
* #1709065 ^designation[0].language = #de-AT 
* #1709065 ^designation[0].value = "DIMENHYDRINAT" 
* #1709067 "OXAZEPAM"
* #1709067 ^designation[0].language = #de-AT 
* #1709067 ^designation[0].value = "OXAZEPAM" 
* #1709068 "PHYTOMENADION"
* #1709068 ^designation[0].language = #de-AT 
* #1709068 ^designation[0].value = "PHYTOMENADION" 
* #1709069 "MOLSIDOMIN"
* #1709069 ^designation[0].language = #de-AT 
* #1709069 ^designation[0].value = "MOLSIDOMIN" 
* #1709070 "TRYPTOPHAN"
* #1709070 ^designation[0].language = #de-AT 
* #1709070 ^designation[0].value = "TRYPTOPHAN" 
* #1709075 "APROTININ"
* #1709075 ^designation[0].language = #de-AT 
* #1709075 ^designation[0].value = "APROTININ" 
* #1709076 "DOSULEPIN HYDROCHLORID"
* #1709076 ^designation[0].language = #de-AT 
* #1709076 ^designation[0].value = "DOSULEPIN HYDROCHLORID" 
* #1709079 "AZELAINSÄURE"
* #1709079 ^designation[0].language = #de-AT 
* #1709079 ^designation[0].value = "AZELAINSÄURE" 
* #1709083 "PREDNISON"
* #1709083 ^designation[0].language = #de-AT 
* #1709083 ^designation[0].value = "PREDNISON" 
* #1709085 "RIFAXIMIN"
* #1709085 ^designation[0].language = #de-AT 
* #1709085 ^designation[0].value = "RIFAXIMIN" 
* #1709087 "BIFONAZOL"
* #1709087 ^designation[0].language = #de-AT 
* #1709087 ^designation[0].value = "BIFONAZOL" 
* #1709088 "BISACODYL"
* #1709088 ^designation[0].language = #de-AT 
* #1709088 ^designation[0].value = "BISACODYL" 
* #1709089 "SALICYLAMID"
* #1709089 ^designation[0].language = #de-AT 
* #1709089 ^designation[0].value = "SALICYLAMID" 
* #1709091 "MESALAZIN"
* #1709091 ^designation[0].language = #de-AT 
* #1709091 ^designation[0].value = "MESALAZIN" 
* #1709092 "TROPICAMID"
* #1709092 ^designation[0].language = #de-AT 
* #1709092 ^designation[0].value = "TROPICAMID" 
* #1709094 "TEREBINTHINAE AETHEROLEUM"
* #1709094 ^designation[0].language = #de-AT 
* #1709094 ^designation[0].value = "TEREBINTHINAE AETHEROLEUM" 
* #1709096 "LACTULOSE-SIRUP"
* #1709096 ^designation[0].language = #de-AT 
* #1709096 ^designation[0].value = "LACTULOSE-SIRUP" 
* #1709098 "GLYCIN"
* #1709098 ^designation[0].language = #de-AT 
* #1709098 ^designation[0].value = "GLYCIN" 
* #1709099 "ISOLEUCIN"
* #1709099 ^designation[0].language = #de-AT 
* #1709099 ^designation[0].value = "ISOLEUCIN" 
* #1709102 "HARNSTOFF"
* #1709102 ^designation[0].language = #de-AT 
* #1709102 ^designation[0].value = "HARNSTOFF" 
* #1709106 "IOPROMID"
* #1709106 ^designation[0].language = #de-AT 
* #1709106 ^designation[0].value = "IOPROMID" 
* #1709107 "LORMETAZEPAM"
* #1709107 ^designation[0].language = #de-AT 
* #1709107 ^designation[0].value = "LORMETAZEPAM" 
* #1709108 "ELETRIPTAN HYDROBROMID"
* #1709108 ^designation[0].language = #de-AT 
* #1709108 ^designation[0].value = "ELETRIPTAN HYDROBROMID" 
* #1709109 "PIRACETAM"
* #1709109 ^designation[0].language = #de-AT 
* #1709109 ^designation[0].value = "PIRACETAM" 
* #1709116 "CINACALCET HYDROCHLORID"
* #1709116 ^designation[0].language = #de-AT 
* #1709116 ^designation[0].value = "CINACALCET HYDROCHLORID" 
* #1709118 "PROTEIN C"
* #1709118 ^designation[0].language = #de-AT 
* #1709118 ^designation[0].value = "PROTEIN C" 
* #1709120 "APOMORPHINHYDROCHLORID"
* #1709120 ^designation[0].language = #de-AT 
* #1709120 ^designation[0].value = "APOMORPHINHYDROCHLORID" 
* #1709121 "L-ASPARAGIN-AMIDOHYDROLASE"
* #1709121 ^designation[0].language = #de-AT 
* #1709121 ^designation[0].value = "L-ASPARAGIN-AMIDOHYDROLASE" 
* #1709122 "DOPAMIN HYDROCHLORID"
* #1709122 ^designation[0].language = #de-AT 
* #1709122 ^designation[0].value = "DOPAMIN HYDROCHLORID" 
* #1709123 "DOCUSAT NATRIUM"
* #1709123 ^designation[0].language = #de-AT 
* #1709123 ^designation[0].value = "DOCUSAT NATRIUM" 
* #1709124 "ALDESLEUKIN"
* #1709124 ^designation[0].language = #de-AT 
* #1709124 ^designation[0].value = "ALDESLEUKIN" 
* #1709125 "PRILOCAIN"
* #1709125 ^designation[0].language = #de-AT 
* #1709125 ^designation[0].value = "PRILOCAIN" 
* #1709127 "NICERGOLIN"
* #1709127 ^designation[0].language = #de-AT 
* #1709127 ^designation[0].value = "NICERGOLIN" 
* #1709128 "NATRIUMLACTAT-LÖSUNG"
* #1709128 ^designation[0].language = #de-AT 
* #1709128 ^designation[0].value = "NATRIUMLACTAT-LÖSUNG" 
* #1709131 "PSEUDOEPHEDRINHYDROCHLORID"
* #1709131 ^designation[0].language = #de-AT 
* #1709131 ^designation[0].value = "PSEUDOEPHEDRINHYDROCHLORID" 
* #1709132 "BUPRENORPHIN HYDROCHLORID"
* #1709132 ^designation[0].language = #de-AT 
* #1709132 ^designation[0].value = "BUPRENORPHIN HYDROCHLORID" 
* #1709134 "CALCIUMACETAT"
* #1709134 ^designation[0].language = #de-AT 
* #1709134 ^designation[0].value = "CALCIUMACETAT" 
* #1709135 "DEXTROMETHORPHAN HYDROBROMID"
* #1709135 ^designation[0].language = #de-AT 
* #1709135 ^designation[0].value = "DEXTROMETHORPHAN HYDROBROMID" 
* #1709137 "FOLLITROPIN ALFA"
* #1709137 ^designation[0].language = #de-AT 
* #1709137 ^designation[0].value = "FOLLITROPIN ALFA" 
* #1709143 "TREPROSTINIL"
* #1709143 ^designation[0].language = #de-AT 
* #1709143 ^designation[0].value = "TREPROSTINIL" 
* #1709144 "OMEPRAZOL"
* #1709144 ^designation[0].language = #de-AT 
* #1709144 ^designation[0].value = "OMEPRAZOL" 
* #1709145 "DIOSMIN"
* #1709145 ^designation[0].language = #de-AT 
* #1709145 ^designation[0].value = "DIOSMIN" 
* #1709148 "NATRIUM RISEDRONAT"
* #1709148 ^designation[0].language = #de-AT 
* #1709148 ^designation[0].value = "NATRIUM RISEDRONAT" 
* #1709152 "AGALSIDASE ALFA"
* #1709152 ^designation[0].language = #de-AT 
* #1709152 ^designation[0].value = "AGALSIDASE ALFA" 
* #1709153 "LUTROPIN ALFA"
* #1709153 ^designation[0].language = #de-AT 
* #1709153 ^designation[0].value = "LUTROPIN ALFA" 
* #1709154 "CHONDROITINSULFAT-NATRIUM"
* #1709154 ^designation[0].language = #de-AT 
* #1709154 ^designation[0].value = "CHONDROITINSULFAT-NATRIUM" 
* #1709155 "DEXMEDETOMIDIN HYDROCHLORID"
* #1709155 ^designation[0].language = #de-AT 
* #1709155 ^designation[0].value = "DEXMEDETOMIDIN HYDROCHLORID" 
* #1709157 "MEBENDAZOL"
* #1709157 ^designation[0].language = #de-AT 
* #1709157 ^designation[0].value = "MEBENDAZOL" 
* #1709160 "MELPERON HYDROCHLORID"
* #1709160 ^designation[0].language = #de-AT 
* #1709160 ^designation[0].value = "MELPERON HYDROCHLORID" 
* #1709161 "GLUTAMINSÄURE"
* #1709161 ^designation[0].language = #de-AT 
* #1709161 ^designation[0].value = "GLUTAMINSÄURE" 
* #1709164 "ORPHENADRIN DIHYDROGENCITRAT"
* #1709164 ^designation[0].language = #de-AT 
* #1709164 ^designation[0].value = "ORPHENADRIN DIHYDROGENCITRAT" 
* #1709166 "CLOTRIMAZOL"
* #1709166 ^designation[0].language = #de-AT 
* #1709166 ^designation[0].value = "CLOTRIMAZOL" 
* #1709167 "DEXPANTHENOL"
* #1709167 ^designation[0].language = #de-AT 
* #1709167 ^designation[0].value = "DEXPANTHENOL" 
* #1709169 "KETOCONAZOL"
* #1709169 ^designation[0].language = #de-AT 
* #1709169 ^designation[0].value = "KETOCONAZOL" 
* #1709170 "BENZALKONIUMCHLORID"
* #1709170 ^designation[0].language = #de-AT 
* #1709170 ^designation[0].value = "BENZALKONIUMCHLORID" 
* #1709172 "ROXITHROMYCIN"
* #1709172 ^designation[0].language = #de-AT 
* #1709172 ^designation[0].value = "ROXITHROMYCIN" 
* #1709176 "NORELGESTROMIN"
* #1709176 ^designation[0].language = #de-AT 
* #1709176 ^designation[0].value = "NORELGESTROMIN" 
* #1709178 "RISPERIDON"
* #1709178 ^designation[0].language = #de-AT 
* #1709178 ^designation[0].value = "RISPERIDON" 
* #1709180 "FLUTAMID"
* #1709180 ^designation[0].language = #de-AT 
* #1709180 ^designation[0].value = "FLUTAMID" 
* #1709182 "SORBITOL"
* #1709182 ^designation[0].language = #de-AT 
* #1709182 ^designation[0].value = "SORBITOL" 
* #1709184 "MELOXICAM"
* #1709184 ^designation[0].language = #de-AT 
* #1709184 ^designation[0].value = "MELOXICAM" 
* #1709185 "AMPHOTERICIN B"
* #1709185 ^designation[0].language = #de-AT 
* #1709185 ^designation[0].value = "AMPHOTERICIN B" 
* #1709186 "PHENYTOIN"
* #1709186 ^designation[0].language = #de-AT 
* #1709186 ^designation[0].value = "PHENYTOIN" 
* #1709187 "TAZOBACTAM NATRIUM"
* #1709187 ^designation[0].language = #de-AT 
* #1709187 ^designation[0].value = "TAZOBACTAM NATRIUM" 
* #1709189 "URSODEOXYCHOLSÄURE"
* #1709189 ^designation[0].language = #de-AT 
* #1709189 ^designation[0].value = "URSODEOXYCHOLSÄURE" 
* #1709191 "PALIPERIDON"
* #1709191 ^designation[0].language = #de-AT 
* #1709191 ^designation[0].value = "PALIPERIDON" 
* #1709192 "CIPROFLOXACIN"
* #1709192 ^designation[0].language = #de-AT 
* #1709192 ^designation[0].value = "CIPROFLOXACIN" 
* #1709193 "CLINDAMYCIN PALMITAT HYDROCHLORID"
* #1709193 ^designation[0].language = #de-AT 
* #1709193 ^designation[0].value = "CLINDAMYCIN PALMITAT HYDROCHLORID" 
* #1709195 "NATRIUMCHLORID"
* #1709195 ^designation[0].language = #de-AT 
* #1709195 ^designation[0].value = "NATRIUMCHLORID" 
* #1709198 "DINATRIUM PAMIDRONAT"
* #1709198 ^designation[0].language = #de-AT 
* #1709198 ^designation[0].value = "DINATRIUM PAMIDRONAT" 
* #1709199 "CICLOSPORIN"
* #1709199 ^designation[0].language = #de-AT 
* #1709199 ^designation[0].value = "CICLOSPORIN" 
* #1709200 "METHYLPREDNISOLON ACEPONAT"
* #1709200 ^designation[0].language = #de-AT 
* #1709200 ^designation[0].value = "METHYLPREDNISOLON ACEPONAT" 
* #1709201 "CARBAMAZEPIN"
* #1709201 ^designation[0].language = #de-AT 
* #1709201 ^designation[0].value = "CARBAMAZEPIN" 
* #1709202 "AMANTADIN SULFAT"
* #1709202 ^designation[0].language = #de-AT 
* #1709202 ^designation[0].value = "AMANTADIN SULFAT" 
* #1709203 "PROPYPHENAZON"
* #1709203 ^designation[0].language = #de-AT 
* #1709203 ^designation[0].value = "PROPYPHENAZON" 
* #1709205 "CINCHOCAIN HYDROCHLORID"
* #1709205 ^designation[0].language = #de-AT 
* #1709205 ^designation[0].value = "CINCHOCAIN HYDROCHLORID" 
* #1709207 "BACITRACIN"
* #1709207 ^designation[0].language = #de-AT 
* #1709207 ^designation[0].value = "BACITRACIN" 
* #1709209 "TYROTHRICIN"
* #1709209 ^designation[0].language = #de-AT 
* #1709209 ^designation[0].value = "TYROTHRICIN" 
* #1709211 "TENOFOVIR DISOPROXIL FUMARAT"
* #1709211 ^designation[0].language = #de-AT 
* #1709211 ^designation[0].value = "TENOFOVIR DISOPROXIL FUMARAT" 
* #1709212 "FENTANYLCITRAT"
* #1709212 ^designation[0].language = #de-AT 
* #1709212 ^designation[0].value = "FENTANYLCITRAT" 
* #1709213 "CABERGOLIN"
* #1709213 ^designation[0].language = #de-AT 
* #1709213 ^designation[0].value = "CABERGOLIN" 
* #1709216 "DAUNORUBICIN HYDROCHLORID"
* #1709216 ^designation[0].language = #de-AT 
* #1709216 ^designation[0].value = "DAUNORUBICIN HYDROCHLORID" 
* #1709225 "TYROSIN"
* #1709225 ^designation[0].language = #de-AT 
* #1709225 ^designation[0].value = "TYROSIN" 
* #1709228 "LORAZEPAM"
* #1709228 ^designation[0].language = #de-AT 
* #1709228 ^designation[0].value = "LORAZEPAM" 
* #1709231 "LOVASTATIN"
* #1709231 ^designation[0].language = #de-AT 
* #1709231 ^designation[0].value = "LOVASTATIN" 
* #1709232 "CINNARIZIN"
* #1709232 ^designation[0].language = #de-AT 
* #1709232 ^designation[0].value = "CINNARIZIN" 
* #1709234 "DISTICKSTOFFMONOXID"
* #1709234 ^designation[0].language = #de-AT 
* #1709234 ^designation[0].value = "DISTICKSTOFFMONOXID" 
* #1709235 "OXYBUTYNIN HYDROCHLORID"
* #1709235 ^designation[0].language = #de-AT 
* #1709235 ^designation[0].value = "OXYBUTYNIN HYDROCHLORID" 
* #1709237 "CHOLIN SALICYLAT"
* #1709237 ^designation[0].language = #de-AT 
* #1709237 ^designation[0].value = "CHOLIN SALICYLAT" 
* #1709238 "CEFOTAXIM NATRIUM"
* #1709238 ^designation[0].language = #de-AT 
* #1709238 ^designation[0].value = "CEFOTAXIM NATRIUM" 
* #1709239 "INDOCYANINGRÜN"
* #1709239 ^designation[0].language = #de-AT 
* #1709239 ^designation[0].value = "INDOCYANINGRÜN" 
* #1709241 "TRIAMCINOLON"
* #1709241 ^designation[0].language = #de-AT 
* #1709241 ^designation[0].value = "TRIAMCINOLON" 
* #1709242 "LORATADIN"
* #1709242 ^designation[0].language = #de-AT 
* #1709242 ^designation[0].value = "LORATADIN" 
* #1709246 "KETOTIFEN HYDROGENFUMARAT"
* #1709246 ^designation[0].language = #de-AT 
* #1709246 ^designation[0].value = "KETOTIFEN HYDROGENFUMARAT" 
* #1709247 "OLOPATADIN HYDROCHLORID"
* #1709247 ^designation[0].language = #de-AT 
* #1709247 ^designation[0].value = "OLOPATADIN HYDROCHLORID" 
* #1709248 "PEMETREXED DINATRIUM"
* #1709248 ^designation[0].language = #de-AT 
* #1709248 ^designation[0].value = "PEMETREXED DINATRIUM" 
* #1709249 "THIAMIN NITRAT"
* #1709249 ^designation[0].language = #de-AT 
* #1709249 ^designation[0].value = "THIAMIN NITRAT" 
* #1709250 "PROTIRELIN"
* #1709250 ^designation[0].language = #de-AT 
* #1709250 ^designation[0].value = "PROTIRELIN" 
* #1709251 "NICOTINAMID"
* #1709251 ^designation[0].language = #de-AT 
* #1709251 ^designation[0].value = "NICOTINAMID" 
* #1709255 "ALPROSTADIL X ALFADEX"
* #1709255 ^designation[0].language = #de-AT 
* #1709255 ^designation[0].value = "ALPROSTADIL X ALFADEX" 
* #1709256 "DOXYCYCLIN"
* #1709256 ^designation[0].language = #de-AT 
* #1709256 ^designation[0].value = "DOXYCYCLIN" 
* #1709259 "TITANDIOXID"
* #1709259 ^designation[0].language = #de-AT 
* #1709259 ^designation[0].value = "TITANDIOXID" 
* #1709260 "DINOPROSTON"
* #1709260 ^designation[0].language = #de-AT 
* #1709260 ^designation[0].value = "DINOPROSTON" 
* #1709261 "KUPFERDIGLUCONAT"
* #1709261 ^designation[0].language = #de-AT 
* #1709261 ^designation[0].value = "KUPFERDIGLUCONAT" 
* #1709262 "OXCARBAZEPIN"
* #1709262 ^designation[0].language = #de-AT 
* #1709262 ^designation[0].value = "OXCARBAZEPIN" 
* #1709263 "ALUMINIUMOXID, WASSERHALTIGES"
* #1709263 ^designation[0].language = #de-AT 
* #1709263 ^designation[0].value = "ALUMINIUMOXID, WASSERHALTIGES" 
* #1709264 "RIMEXOLON"
* #1709264 ^designation[0].language = #de-AT 
* #1709264 ^designation[0].value = "RIMEXOLON" 
* #1709269 "ONDANSETRON HYDROCHLORID"
* #1709269 ^designation[0].language = #de-AT 
* #1709269 ^designation[0].value = "ONDANSETRON HYDROCHLORID" 
* #1709271 "RAMIPRIL"
* #1709271 ^designation[0].language = #de-AT 
* #1709271 ^designation[0].value = "RAMIPRIL" 
* #1709272 "MENTHAE PIPERITAE AETHEROLEUM"
* #1709272 ^designation[0].language = #de-AT 
* #1709272 ^designation[0].value = "MENTHAE PIPERITAE AETHEROLEUM" 
* #1709274 "NAFTIDROFURYL HYDROGENOXALAT"
* #1709274 ^designation[0].language = #de-AT 
* #1709274 ^designation[0].value = "NAFTIDROFURYL HYDROGENOXALAT" 
* #1709277 "NATRIUM NAPROXENAT"
* #1709277 ^designation[0].language = #de-AT 
* #1709277 ^designation[0].value = "NATRIUM NAPROXENAT" 
* #1709278 "DOMPERIDON"
* #1709278 ^designation[0].language = #de-AT 
* #1709278 ^designation[0].value = "DOMPERIDON" 
* #1709279 "FENTANYL HYDROCHLORID"
* #1709279 ^designation[0].language = #de-AT 
* #1709279 ^designation[0].value = "FENTANYL HYDROCHLORID" 
* #1709281 "HYDROCORTISON ACETAT"
* #1709281 ^designation[0].language = #de-AT 
* #1709281 ^designation[0].value = "HYDROCORTISON ACETAT" 
* #1709282 "ACETYLCYSTEIN"
* #1709282 ^designation[0].language = #de-AT 
* #1709282 ^designation[0].value = "ACETYLCYSTEIN" 
* #1709283 "HYDROXYETHYLSALICYLAT"
* #1709283 ^designation[0].language = #de-AT 
* #1709283 ^designation[0].value = "HYDROXYETHYLSALICYLAT" 
* #1709284 "LEUCIN"
* #1709284 ^designation[0].language = #de-AT 
* #1709284 ^designation[0].value = "LEUCIN" 
* #1709285 "LOXAPIN"
* #1709285 ^designation[0].language = #de-AT 
* #1709285 ^designation[0].value = "LOXAPIN" 
* #1709286 "PARAFFIN, DÜNNFLÜSSIGES"
* #1709286 ^designation[0].language = #de-AT 
* #1709286 ^designation[0].value = "PARAFFIN, DÜNNFLÜSSIGES" 
* #1709290 "DICLOFENAC NATRIUM"
* #1709290 ^designation[0].language = #de-AT 
* #1709290 ^designation[0].value = "DICLOFENAC NATRIUM" 
* #1709291 "NIMODIPIN"
* #1709291 ^designation[0].language = #de-AT 
* #1709291 ^designation[0].value = "NIMODIPIN" 
* #1709293 "ACARBOSE"
* #1709293 ^designation[0].language = #de-AT 
* #1709293 ^designation[0].value = "ACARBOSE" 
* #1709296 "FAMCICLOVIR"
* #1709296 ^designation[0].language = #de-AT 
* #1709296 ^designation[0].value = "FAMCICLOVIR" 
* #1709297 "FEXOFENADIN HYDROCHLORID"
* #1709297 ^designation[0].language = #de-AT 
* #1709297 ^designation[0].value = "FEXOFENADIN HYDROCHLORID" 
* #1709298 "LAMOTRIGIN"
* #1709298 ^designation[0].language = #de-AT 
* #1709298 ^designation[0].value = "LAMOTRIGIN" 
* #1709301 "PAMIDRONSÄURE"
* #1709301 ^designation[0].language = #de-AT 
* #1709301 ^designation[0].value = "PAMIDRONSÄURE" 
* #1709305 "EPLERENON"
* #1709305 ^designation[0].language = #de-AT 
* #1709305 ^designation[0].value = "EPLERENON" 
* #1709306 "EXEMESTAN"
* #1709306 ^designation[0].language = #de-AT 
* #1709306 ^designation[0].value = "EXEMESTAN" 
* #1709307 "GADOBUTROL"
* #1709307 ^designation[0].language = #de-AT 
* #1709307 ^designation[0].value = "GADOBUTROL" 
* #1709308 "LATANOPROST"
* #1709308 ^designation[0].language = #de-AT 
* #1709308 ^designation[0].value = "LATANOPROST" 
* #1709309 "ALPROSTADIL"
* #1709309 ^designation[0].language = #de-AT 
* #1709309 ^designation[0].value = "ALPROSTADIL" 
* #1709310 "OFLOXACIN"
* #1709310 ^designation[0].language = #de-AT 
* #1709310 ^designation[0].value = "OFLOXACIN" 
* #1709311 "TERBINAFIN"
* #1709311 ^designation[0].language = #de-AT 
* #1709311 ^designation[0].value = "TERBINAFIN" 
* #1709312 "CINEOL"
* #1709312 ^designation[0].language = #de-AT 
* #1709312 ^designation[0].value = "CINEOL" 
* #1709314 "TETRABENAZIN"
* #1709314 ^designation[0].language = #de-AT 
* #1709314 ^designation[0].value = "TETRABENAZIN" 
* #1709316 "ACIPIMOX"
* #1709316 ^designation[0].language = #de-AT 
* #1709316 ^designation[0].value = "ACIPIMOX" 
* #1709318 "IMIDAPRIL HYDROCHLORID"
* #1709318 ^designation[0].language = #de-AT 
* #1709318 ^designation[0].value = "IMIDAPRIL HYDROCHLORID" 
* #1709319 "DEXTROMETHORPHAN"
* #1709319 ^designation[0].language = #de-AT 
* #1709319 ^designation[0].value = "DEXTROMETHORPHAN" 
* #1709323 "TROPISETRON HYDROCHLORID"
* #1709323 ^designation[0].language = #de-AT 
* #1709323 ^designation[0].value = "TROPISETRON HYDROCHLORID" 
* #1709324 "EVEROLIMUS"
* #1709324 ^designation[0].language = #de-AT 
* #1709324 ^designation[0].value = "EVEROLIMUS" 
* #1709325 "ALANIN"
* #1709325 ^designation[0].language = #de-AT 
* #1709325 ^designation[0].value = "ALANIN" 
* #1709329 "ANASTROZOL"
* #1709329 ^designation[0].language = #de-AT 
* #1709329 ^designation[0].value = "ANASTROZOL" 
* #1709330 "ONDANSETRON"
* #1709330 ^designation[0].language = #de-AT 
* #1709330 ^designation[0].value = "ONDANSETRON" 
* #1709331 "TROSPIUM CHLORID"
* #1709331 ^designation[0].language = #de-AT 
* #1709331 ^designation[0].value = "TROSPIUM CHLORID" 
* #1709334 "COLISTIMETHAT NATRIUM"
* #1709334 ^designation[0].language = #de-AT 
* #1709334 ^designation[0].value = "COLISTIMETHAT NATRIUM" 
* #1709336 "ESTRAMUSTIN PHOSPHAT"
* #1709336 ^designation[0].language = #de-AT 
* #1709336 ^designation[0].value = "ESTRAMUSTIN PHOSPHAT" 
* #1709337 "FOSINOPRIL NATRIUM"
* #1709337 ^designation[0].language = #de-AT 
* #1709337 ^designation[0].value = "FOSINOPRIL NATRIUM" 
* #1709341 "GESTODEN"
* #1709341 ^designation[0].language = #de-AT 
* #1709341 ^designation[0].value = "GESTODEN" 
* #1709343 "AZITHROMYCIN"
* #1709343 ^designation[0].language = #de-AT 
* #1709343 ^designation[0].value = "AZITHROMYCIN" 
* #1709346 "SCHWERES BASISCHES MAGNESIUMCARBONAT"
* #1709346 ^designation[0].language = #de-AT 
* #1709346 ^designation[0].value = "SCHWERES BASISCHES MAGNESIUMCARBONAT" 
* #1709349 "DIAZEPAM"
* #1709349 ^designation[0].language = #de-AT 
* #1709349 ^designation[0].value = "DIAZEPAM" 
* #1709350 "FAMOTIDIN"
* #1709350 ^designation[0].language = #de-AT 
* #1709350 ^designation[0].value = "FAMOTIDIN" 
* #1709351 "CLARITHROMYCIN"
* #1709351 ^designation[0].language = #de-AT 
* #1709351 ^designation[0].value = "CLARITHROMYCIN" 
* #1709352 "THREONIN"
* #1709352 ^designation[0].language = #de-AT 
* #1709352 ^designation[0].value = "THREONIN" 
* #1709354 "DICLOFENAC KALIUM"
* #1709354 ^designation[0].language = #de-AT 
* #1709354 ^designation[0].value = "DICLOFENAC KALIUM" 
* #1709356 "ICHTHAMMOLUM"
* #1709356 ^designation[0].language = #de-AT 
* #1709356 ^designation[0].value = "ICHTHAMMOLUM" 
* #1709358 "DROSPIRENON"
* #1709358 ^designation[0].language = #de-AT 
* #1709358 ^designation[0].value = "DROSPIRENON" 
* #1709359 "MUPIROCIN"
* #1709359 ^designation[0].language = #de-AT 
* #1709359 ^designation[0].value = "MUPIROCIN" 
* #1709361 "EPINEPHRIN"
* #1709361 ^designation[0].language = #de-AT 
* #1709361 ^designation[0].value = "EPINEPHRIN" 
* #1709362 "POVIDON"
* #1709362 ^designation[0].language = #de-AT 
* #1709362 ^designation[0].value = "POVIDON" 
* #1709363 "ERYTHROMYCIN"
* #1709363 ^designation[0].language = #de-AT 
* #1709363 ^designation[0].value = "ERYTHROMYCIN" 
* #1709364 "OPIPRAMOL DIHYDROCHLORID"
* #1709364 ^designation[0].language = #de-AT 
* #1709364 ^designation[0].value = "OPIPRAMOL DIHYDROCHLORID" 
* #1709366 "TRIMETHOPRIM"
* #1709366 ^designation[0].language = #de-AT 
* #1709366 ^designation[0].value = "TRIMETHOPRIM" 
* #1709368 "FLUOCORTOLON HEXANOAT"
* #1709368 ^designation[0].language = #de-AT 
* #1709368 ^designation[0].value = "FLUOCORTOLON HEXANOAT" 
* #1709369 "LEVOCABASTIN HYDROCHLORID"
* #1709369 ^designation[0].language = #de-AT 
* #1709369 ^designation[0].value = "LEVOCABASTIN HYDROCHLORID" 
* #1709371 "CHLORHEXIDIN DIHYDROCHLORID"
* #1709371 ^designation[0].language = #de-AT 
* #1709371 ^designation[0].value = "CHLORHEXIDIN DIHYDROCHLORID" 
* #1709372 "OXYTOCIN"
* #1709372 ^designation[0].language = #de-AT 
* #1709372 ^designation[0].value = "OXYTOCIN" 
* #1709373 "MAPROTILIN HYDROCHLORID"
* #1709373 ^designation[0].language = #de-AT 
* #1709373 ^designation[0].value = "MAPROTILIN HYDROCHLORID" 
* #1709377 "NITROFURANTOIN"
* #1709377 ^designation[0].language = #de-AT 
* #1709377 ^designation[0].value = "NITROFURANTOIN" 
* #1709378 "BENZYLNICOTINAT"
* #1709378 ^designation[0].language = #de-AT 
* #1709378 ^designation[0].value = "BENZYLNICOTINAT" 
* #1709379 "SPIRONOLACTON"
* #1709379 ^designation[0].language = #de-AT 
* #1709379 ^designation[0].value = "SPIRONOLACTON" 
* #1709380 "NITROPRUSSIDNATRIUM DIHYDRAT"
* #1709380 ^designation[0].language = #de-AT 
* #1709380 ^designation[0].value = "NITROPRUSSIDNATRIUM DIHYDRAT" 
* #1709381 "CYCLOPENTOLAT HYDROCHLORID"
* #1709381 ^designation[0].language = #de-AT 
* #1709381 ^designation[0].value = "CYCLOPENTOLAT HYDROCHLORID" 
* #1709383 "METHIONIN"
* #1709383 ^designation[0].language = #de-AT 
* #1709383 ^designation[0].value = "METHIONIN" 
* #1709384 "METYRAPON"
* #1709384 ^designation[0].language = #de-AT 
* #1709384 ^designation[0].value = "METYRAPON" 
* #1709385 "PHENYLALANIN"
* #1709385 ^designation[0].language = #de-AT 
* #1709385 ^designation[0].value = "PHENYLALANIN" 
* #1709386 "MISOPROSTOL"
* #1709386 ^designation[0].language = #de-AT 
* #1709386 ^designation[0].value = "MISOPROSTOL" 
* #1709388 "CEFACLOR MONOHYDRAT"
* #1709388 ^designation[0].language = #de-AT 
* #1709388 ^designation[0].value = "CEFACLOR MONOHYDRAT" 
* #1709389 "VALIN"
* #1709389 ^designation[0].language = #de-AT 
* #1709389 ^designation[0].value = "VALIN" 
* #1709390 "P-AMINOSALIZYLSÄURE"
* #1709390 ^designation[0].language = #de-AT 
* #1709390 ^designation[0].value = "P-AMINOSALIZYLSÄURE" 
* #1709391 "SEVOFLURAN"
* #1709391 ^designation[0].language = #de-AT 
* #1709391 ^designation[0].value = "SEVOFLURAN" 
* #1709393 "2,4-DICHLORBENZYLALKOHOL"
* #1709393 ^designation[0].language = #de-AT 
* #1709393 ^designation[0].value = "2,4-DICHLORBENZYLALKOHOL" 
* #1709394 "MEXILETIN HYDROCHLORID"
* #1709394 ^designation[0].language = #de-AT 
* #1709394 ^designation[0].value = "MEXILETIN HYDROCHLORID" 
* #1709396 "AMIKACIN"
* #1709396 ^designation[0].language = #de-AT 
* #1709396 ^designation[0].value = "AMIKACIN" 
* #1709397 "BETAXOLOL HYDROCHLORID"
* #1709397 ^designation[0].language = #de-AT 
* #1709397 ^designation[0].value = "BETAXOLOL HYDROCHLORID" 
* #1709398 "LETROZOL"
* #1709398 ^designation[0].language = #de-AT 
* #1709398 ^designation[0].value = "LETROZOL" 
* #1709399 "LEVOSIMENDAN"
* #1709399 ^designation[0].language = #de-AT 
* #1709399 ^designation[0].value = "LEVOSIMENDAN" 
* #1709400 "MESNA"
* #1709400 ^designation[0].language = #de-AT 
* #1709400 ^designation[0].value = "MESNA" 
* #1709401 "MOCLOBEMID"
* #1709401 ^designation[0].language = #de-AT 
* #1709401 ^designation[0].value = "MOCLOBEMID" 
* #1709402 "NISOLDIPIN"
* #1709402 ^designation[0].language = #de-AT 
* #1709402 ^designation[0].value = "NISOLDIPIN" 
* #1709403 "PANTOPRAZOL"
* #1709403 ^designation[0].language = #de-AT 
* #1709403 ^designation[0].value = "PANTOPRAZOL" 
* #1709404 "PIROXICAM"
* #1709404 ^designation[0].language = #de-AT 
* #1709404 ^designation[0].value = "PIROXICAM" 
* #1709405 "LINEZOLID"
* #1709405 ^designation[0].language = #de-AT 
* #1709405 ^designation[0].value = "LINEZOLID" 
* #1709407 "OCTREOTID"
* #1709407 ^designation[0].language = #de-AT 
* #1709407 ^designation[0].value = "OCTREOTID" 
* #1709408 "PARICALCITOL"
* #1709408 ^designation[0].language = #de-AT 
* #1709408 ^designation[0].value = "PARICALCITOL" 
* #1709413 "TRIAZOLAM"
* #1709413 ^designation[0].language = #de-AT 
* #1709413 ^designation[0].value = "TRIAZOLAM" 
* #1709414 "ZOPICLON"
* #1709414 ^designation[0].language = #de-AT 
* #1709414 ^designation[0].value = "ZOPICLON" 
* #1709418 "FENOFIBRAT"
* #1709418 ^designation[0].language = #de-AT 
* #1709418 ^designation[0].value = "FENOFIBRAT" 
* #1709419 "HEXETIDIN"
* #1709419 ^designation[0].language = #de-AT 
* #1709419 ^designation[0].value = "HEXETIDIN" 
* #1709420 "TIBOLON"
* #1709420 ^designation[0].language = #de-AT 
* #1709420 ^designation[0].value = "TIBOLON" 
* #1709422 "TOPIRAMAT"
* #1709422 ^designation[0].language = #de-AT 
* #1709422 ^designation[0].value = "TOPIRAMAT" 
* #1709428 "BROMAZEPAM"
* #1709428 ^designation[0].language = #de-AT 
* #1709428 ^designation[0].value = "BROMAZEPAM" 
* #1709431 "CAPTOPRIL"
* #1709431 ^designation[0].language = #de-AT 
* #1709431 ^designation[0].value = "CAPTOPRIL" 
* #1709432 "MANGANGLUCONAT"
* #1709432 ^designation[0].language = #de-AT 
* #1709432 ^designation[0].value = "MANGANGLUCONAT" 
* #1709436 "CARBOPLATIN"
* #1709436 ^designation[0].language = #de-AT 
* #1709436 ^designation[0].value = "CARBOPLATIN" 
* #1709440 "TRIAMTEREN"
* #1709440 ^designation[0].language = #de-AT 
* #1709440 ^designation[0].value = "TRIAMTEREN" 
* #1709441 "LEVOCETIRIZIN DIHYDROCHLORID"
* #1709441 ^designation[0].language = #de-AT 
* #1709441 ^designation[0].value = "LEVOCETIRIZIN DIHYDROCHLORID" 
* #1709445 "CARVEDILOL"
* #1709445 ^designation[0].language = #de-AT 
* #1709445 ^designation[0].value = "CARVEDILOL" 
* #1709446 "CHLORTALIDON"
* #1709446 ^designation[0].language = #de-AT 
* #1709446 ^designation[0].value = "CHLORTALIDON" 
* #1709452 "TRETINOIN"
* #1709452 ^designation[0].language = #de-AT 
* #1709452 ^designation[0].value = "TRETINOIN" 
* #1709454 "METHOXSALEN"
* #1709454 ^designation[0].language = #de-AT 
* #1709454 ^designation[0].value = "METHOXSALEN" 
* #1709455 "METHYLERGOMETRIN HYDROGENMALEAT"
* #1709455 ^designation[0].language = #de-AT 
* #1709455 ^designation[0].value = "METHYLERGOMETRIN HYDROGENMALEAT" 
* #1709459 "MAGNESIUMSULFAT"
* #1709459 ^designation[0].language = #de-AT 
* #1709459 ^designation[0].value = "MAGNESIUMSULFAT" 
* #1709462 "SULPROSTON"
* #1709462 ^designation[0].language = #de-AT 
* #1709462 ^designation[0].value = "SULPROSTON" 
* #1709464 "DEXAMETHASON"
* #1709464 ^designation[0].language = #de-AT 
* #1709464 ^designation[0].value = "DEXAMETHASON" 
* #1709468 "ETOFENAMAT"
* #1709468 ^designation[0].language = #de-AT 
* #1709468 ^designation[0].value = "ETOFENAMAT" 
* #1709469 "IPRATROPIUM BROMID"
* #1709469 ^designation[0].language = #de-AT 
* #1709469 ^designation[0].value = "IPRATROPIUM BROMID" 
* #1709470 "CITRONENSÄURE"
* #1709470 ^designation[0].language = #de-AT 
* #1709470 ^designation[0].value = "CITRONENSÄURE" 
* #1709472 "FLUDEOXYGLUCOSE [*18*F]"
* #1709472 ^designation[0].language = #de-AT 
* #1709472 ^designation[0].value = "FLUDEOXYGLUCOSE [*18*F]" 
* #1709473 "PREDNISOLON PIVALAT"
* #1709473 ^designation[0].language = #de-AT 
* #1709473 ^designation[0].value = "PREDNISOLON PIVALAT" 
* #1709475 "SULFAMETHOXAZOL"
* #1709475 ^designation[0].language = #de-AT 
* #1709475 ^designation[0].value = "SULFAMETHOXAZOL" 
* #1709477 "BETAMETHASONVALERAT"
* #1709477 ^designation[0].language = #de-AT 
* #1709477 ^designation[0].value = "BETAMETHASONVALERAT" 
* #1709479 "CEFOPERAZON NATRIUM"
* #1709479 ^designation[0].language = #de-AT 
* #1709479 ^designation[0].value = "CEFOPERAZON NATRIUM" 
* #1709481 "CHLOROQUIN DI(DIHYDROGENPHOSPHAT)"
* #1709481 ^designation[0].language = #de-AT 
* #1709481 ^designation[0].value = "CHLOROQUIN DI(DIHYDROGENPHOSPHAT)" 
* #1709482 "CEFTAZIDIM"
* #1709482 ^designation[0].language = #de-AT 
* #1709482 ^designation[0].value = "CEFTAZIDIM" 
* #1709483 "DINATRIUM OXIDRONAT"
* #1709483 ^designation[0].language = #de-AT 
* #1709483 ^designation[0].value = "DINATRIUM OXIDRONAT" 
* #1709484 "CEFIXIM"
* #1709484 ^designation[0].language = #de-AT 
* #1709484 ^designation[0].value = "CEFIXIM" 
* #1709485 "HYDROCORTISON"
* #1709485 ^designation[0].language = #de-AT 
* #1709485 ^designation[0].value = "HYDROCORTISON" 
* #1709488 "DOXYCYCLINHYCLAT"
* #1709488 ^designation[0].language = #de-AT 
* #1709488 ^designation[0].value = "DOXYCYCLINHYCLAT" 
* #1709489 "CETRIMID"
* #1709489 ^designation[0].language = #de-AT 
* #1709489 ^designation[0].value = "CETRIMID" 
* #1709492 "GEMFIBROZIL"
* #1709492 ^designation[0].language = #de-AT 
* #1709492 ^designation[0].value = "GEMFIBROZIL" 
* #1709494 "HALOPERIDOL"
* #1709494 ^designation[0].language = #de-AT 
* #1709494 ^designation[0].value = "HALOPERIDOL" 
* #1709495 "HISTIDIN"
* #1709495 ^designation[0].language = #de-AT 
* #1709495 ^designation[0].value = "HISTIDIN" 
* #1709496 "IDEBENON"
* #1709496 ^designation[0].language = #de-AT 
* #1709496 ^designation[0].value = "IDEBENON" 
* #1709498 "INDAPAMID"
* #1709498 ^designation[0].language = #de-AT 
* #1709498 ^designation[0].value = "INDAPAMID" 
* #1709499 "ACETAZOLAMID"
* #1709499 ^designation[0].language = #de-AT 
* #1709499 ^designation[0].value = "ACETAZOLAMID" 
* #1709501 "CITICOLIN"
* #1709501 ^designation[0].language = #de-AT 
* #1709501 ^designation[0].value = "CITICOLIN" 
* #1709503 "FINASTERID"
* #1709503 ^designation[0].language = #de-AT 
* #1709503 ^designation[0].value = "FINASTERID" 
* #1709504 "FLUCONAZOL"
* #1709504 ^designation[0].language = #de-AT 
* #1709504 ^designation[0].value = "FLUCONAZOL" 
* #1709505 "FLUNITRAZEPAM"
* #1709505 ^designation[0].language = #de-AT 
* #1709505 ^designation[0].value = "FLUNITRAZEPAM" 
* #1709506 "FLURBIPROFEN"
* #1709506 ^designation[0].language = #de-AT 
* #1709506 ^designation[0].value = "FLURBIPROFEN" 
* #1709507 "TERAZOSIN HYDROCHLORID"
* #1709507 ^designation[0].language = #de-AT 
* #1709507 ^designation[0].value = "TERAZOSIN HYDROCHLORID" 
* #1709509 "GABAPENTIN"
* #1709509 ^designation[0].language = #de-AT 
* #1709509 ^designation[0].value = "GABAPENTIN" 
* #1709511 "ESTRIOL"
* #1709511 ^designation[0].language = #de-AT 
* #1709511 ^designation[0].value = "ESTRIOL" 
* #1709512 "UROKINASE"
* #1709512 ^designation[0].language = #de-AT 
* #1709512 ^designation[0].value = "UROKINASE" 
* #1709513 "XYLOMETAZOLIN HYDROCHLORID"
* #1709513 ^designation[0].language = #de-AT 
* #1709513 ^designation[0].value = "XYLOMETAZOLIN HYDROCHLORID" 
* #1709521 "DUTASTERID"
* #1709521 ^designation[0].language = #de-AT 
* #1709521 ^designation[0].value = "DUTASTERID" 
* #1709522 "FLUTICASONPROPIONAT"
* #1709522 ^designation[0].language = #de-AT 
* #1709522 ^designation[0].value = "FLUTICASONPROPIONAT" 
* #1709523 "COLECALCIFEROL"
* #1709523 ^designation[0].language = #de-AT 
* #1709523 ^designation[0].value = "COLECALCIFEROL" 
* #1709524 "INDACATEROLMALEAT"
* #1709524 ^designation[0].language = #de-AT 
* #1709524 ^designation[0].value = "INDACATEROLMALEAT" 
* #1709525 "IVABRADIN HYDROCHLORID"
* #1709525 ^designation[0].language = #de-AT 
* #1709525 ^designation[0].value = "IVABRADIN HYDROCHLORID" 
* #1709526 "STREPTOCOCCUS PNEUMONIAE"
* #1709526 ^designation[0].language = #de-AT 
* #1709526 ^designation[0].value = "STREPTOCOCCUS PNEUMONIAE" 
* #1709527 "ISONIAZID"
* #1709527 ^designation[0].language = #de-AT 
* #1709527 ^designation[0].value = "ISONIAZID" 
* #1709528 "FLUTICASONFUROAT"
* #1709528 ^designation[0].language = #de-AT 
* #1709528 ^designation[0].value = "FLUTICASONFUROAT" 
* #1709529 "ALPHA-1-PROTEINASE-INHIBITOR"
* #1709529 ^designation[0].language = #de-AT 
* #1709529 ^designation[0].value = "ALPHA-1-PROTEINASE-INHIBITOR" 
* #1709530 "ERLOTINIB HYDROCHLORID"
* #1709530 ^designation[0].language = #de-AT 
* #1709530 ^designation[0].value = "ERLOTINIB HYDROCHLORID" 
* #1709532 "PEGINTERFERON ALFA-2A"
* #1709532 ^designation[0].language = #de-AT 
* #1709532 ^designation[0].value = "PEGINTERFERON ALFA-2A" 
* #1709534 "MYCOPHENOLAT MOFETIL"
* #1709534 ^designation[0].language = #de-AT 
* #1709534 ^designation[0].value = "MYCOPHENOLAT MOFETIL" 
* #1709535 "CALCIUMCHLORID"
* #1709535 ^designation[0].language = #de-AT 
* #1709535 ^designation[0].value = "CALCIUMCHLORID" 
* #1709536 "THALLIUM CHLORID [*201*TL]"
* #1709536 ^designation[0].language = #de-AT 
* #1709536 ^designation[0].value = "THALLIUM CHLORID [*201*TL]" 
* #1709537 "INDOMETACIN"
* #1709537 ^designation[0].language = #de-AT 
* #1709537 ^designation[0].value = "INDOMETACIN" 
* #1709542 "BENDROFLUMETHIAZID"
* #1709542 ^designation[0].language = #de-AT 
* #1709542 ^designation[0].value = "BENDROFLUMETHIAZID" 
* #1709544 "LEVOMEPROMAZIN HYDROGENMALEAT"
* #1709544 ^designation[0].language = #de-AT 
* #1709544 ^designation[0].value = "LEVOMEPROMAZIN HYDROGENMALEAT" 
* #1709547 "CHLORAMPHENICOL"
* #1709547 ^designation[0].language = #de-AT 
* #1709547 ^designation[0].value = "CHLORAMPHENICOL" 
* #1709549 "CHLORPROTHIXEN HYDROCHLORID"
* #1709549 ^designation[0].language = #de-AT 
* #1709549 ^designation[0].value = "CHLORPROTHIXEN HYDROCHLORID" 
* #1709550 "DIGITOXIN"
* #1709550 ^designation[0].language = #de-AT 
* #1709550 ^designation[0].value = "DIGITOXIN" 
* #1709552 "GLIQUIDON"
* #1709552 ^designation[0].language = #de-AT 
* #1709552 ^designation[0].value = "GLIQUIDON" 
* #1709553 "CLOBETASON BUTYRAT"
* #1709553 ^designation[0].language = #de-AT 
* #1709553 ^designation[0].value = "CLOBETASON BUTYRAT" 
* #1709554 "BAMBUTEROL HYDROCHLORID"
* #1709554 ^designation[0].language = #de-AT 
* #1709554 ^designation[0].value = "BAMBUTEROL HYDROCHLORID" 
* #1709557 "BIOTIN"
* #1709557 ^designation[0].language = #de-AT 
* #1709557 ^designation[0].value = "BIOTIN" 
* #1709558 "CALCITRIOL"
* #1709558 ^designation[0].language = #de-AT 
* #1709558 ^designation[0].value = "CALCITRIOL" 
* #1709561 "CIMETIDIN"
* #1709561 ^designation[0].language = #de-AT 
* #1709561 ^designation[0].value = "CIMETIDIN" 
* #1709562 "CLOZAPIN"
* #1709562 ^designation[0].language = #de-AT 
* #1709562 ^designation[0].value = "CLOZAPIN" 
* #1709563 "DIFLUCORTOLON VALERAT"
* #1709563 ^designation[0].language = #de-AT 
* #1709563 ^designation[0].value = "DIFLUCORTOLON VALERAT" 
* #1709564 "DICLOFENAC"
* #1709564 ^designation[0].language = #de-AT 
* #1709564 ^designation[0].value = "DICLOFENAC" 
* #1709572 "ACICLOVIR"
* #1709572 ^designation[0].language = #de-AT 
* #1709572 ^designation[0].value = "ACICLOVIR" 
* #1709573 "RIVAROXABAN"
* #1709573 ^designation[0].language = #de-AT 
* #1709573 ^designation[0].value = "RIVAROXABAN" 
* #1709575 "DIENOGEST"
* #1709575 ^designation[0].language = #de-AT 
* #1709575 ^designation[0].value = "DIENOGEST" 
* #1709576 "ETHOSUXIMID"
* #1709576 ^designation[0].language = #de-AT 
* #1709576 ^designation[0].value = "ETHOSUXIMID" 
* #1709578 "PHENOXYETHANOL"
* #1709578 ^designation[0].language = #de-AT 
* #1709578 ^designation[0].value = "PHENOXYETHANOL" 
* #1709579 "GLYCEROL"
* #1709579 ^designation[0].language = #de-AT 
* #1709579 ^designation[0].value = "GLYCEROL" 
* #1709580 "HYDROXYPROGESTERON CAPROAT"
* #1709580 ^designation[0].language = #de-AT 
* #1709580 ^designation[0].value = "HYDROXYPROGESTERON CAPROAT" 
* #1709584 "MYCOBACTERIUM BOVIS"
* #1709584 ^designation[0].language = #de-AT 
* #1709584 ^designation[0].value = "MYCOBACTERIUM BOVIS" 
* #1709586 "AMIODARON HYDROCHLORID"
* #1709586 ^designation[0].language = #de-AT 
* #1709586 ^designation[0].value = "AMIODARON HYDROCHLORID" 
* #1709589 "ICATIBANT-ACETAT"
* #1709589 ^designation[0].language = #de-AT 
* #1709589 ^designation[0].value = "ICATIBANT-ACETAT" 
* #1709593 "GLUTATHION"
* #1709593 ^designation[0].language = #de-AT 
* #1709593 ^designation[0].value = "GLUTATHION" 
* #1709599 "IPILIMUMAB"
* #1709599 ^designation[0].language = #de-AT 
* #1709599 ^designation[0].value = "IPILIMUMAB" 
* #1709601 "PLERIXAFOR"
* #1709601 ^designation[0].language = #de-AT 
* #1709601 ^designation[0].value = "PLERIXAFOR" 
* #1709602 "BELATACEPT"
* #1709602 ^designation[0].language = #de-AT 
* #1709602 ^designation[0].value = "BELATACEPT" 
* #1709603 "ECONAZOL NITRAT"
* #1709603 ^designation[0].language = #de-AT 
* #1709603 ^designation[0].value = "ECONAZOL NITRAT" 
* #1709604 "EPOETIN THETA"
* #1709604 ^designation[0].language = #de-AT 
* #1709604 ^designation[0].value = "EPOETIN THETA" 
* #1709605 "AMIFAMPRIDIN"
* #1709605 ^designation[0].language = #de-AT 
* #1709605 ^designation[0].value = "AMIFAMPRIDIN" 
* #1709610 "DEXKETOPROFEN-TROMETAMOL"
* #1709610 ^designation[0].language = #de-AT 
* #1709610 ^designation[0].value = "DEXKETOPROFEN-TROMETAMOL" 
* #1709612 "ADEFOVIR DIPIVOXIL"
* #1709612 ^designation[0].language = #de-AT 
* #1709612 ^designation[0].value = "ADEFOVIR DIPIVOXIL" 
* #1709616 "DIBOTERMIN ALFA"
* #1709616 ^designation[0].language = #de-AT 
* #1709616 ^designation[0].value = "DIBOTERMIN ALFA" 
* #1709617 "NORETHISTERON ACETAT"
* #1709617 ^designation[0].language = #de-AT 
* #1709617 ^designation[0].value = "NORETHISTERON ACETAT" 
* #1709619 "CEFUROXIM AXETIL"
* #1709619 ^designation[0].language = #de-AT 
* #1709619 ^designation[0].value = "CEFUROXIM AXETIL" 
* #1709620 "RIVASTIGMIN HYDROGENTARTRAT"
* #1709620 ^designation[0].language = #de-AT 
* #1709620 ^designation[0].value = "RIVASTIGMIN HYDROGENTARTRAT" 
* #1709621 "INDACATEROL"
* #1709621 ^designation[0].language = #de-AT 
* #1709621 ^designation[0].value = "INDACATEROL" 
* #1709622 "BUTYLSCOPOLAMINIUMBROMID"
* #1709622 ^designation[0].language = #de-AT 
* #1709622 ^designation[0].value = "BUTYLSCOPOLAMINIUMBROMID" 
* #1709625 "KALIUMCLAVULANAT"
* #1709625 ^designation[0].language = #de-AT 
* #1709625 ^designation[0].value = "KALIUMCLAVULANAT" 
* #1709627 "NEBIVOLOL HYDROCHLORID"
* #1709627 ^designation[0].language = #de-AT 
* #1709627 ^designation[0].value = "NEBIVOLOL HYDROCHLORID" 
* #1709628 "CANAKINUMAB"
* #1709628 ^designation[0].language = #de-AT 
* #1709628 ^designation[0].value = "CANAKINUMAB" 
* #1709629 "ELTROMBOPAG"
* #1709629 ^designation[0].language = #de-AT 
* #1709629 ^designation[0].value = "ELTROMBOPAG" 
* #1709643 "TRAMADOLHYDROCHLORID"
* #1709643 ^designation[0].language = #de-AT 
* #1709643 ^designation[0].value = "TRAMADOLHYDROCHLORID" 
* #1709647 "PRASUGREL"
* #1709647 ^designation[0].language = #de-AT 
* #1709647 ^designation[0].value = "PRASUGREL" 
* #1709648 "CRATAEGI FOLIUM CUM FLORE"
* #1709648 ^designation[0].language = #de-AT 
* #1709648 ^designation[0].value = "CRATAEGI FOLIUM CUM FLORE" 
* #1709649 "MILLEFOLII HERBA"
* #1709649 ^designation[0].language = #de-AT 
* #1709649 ^designation[0].value = "MILLEFOLII HERBA" 
* #1709651 "DULOXETIN HYDROCHLORID"
* #1709651 ^designation[0].language = #de-AT 
* #1709651 ^designation[0].value = "DULOXETIN HYDROCHLORID" 
* #1709652 "DAPOXETINHYDROCHLORID"
* #1709652 ^designation[0].language = #de-AT 
* #1709652 ^designation[0].value = "DAPOXETINHYDROCHLORID" 
* #1709653 "ESLICARBAZEPINACETAT"
* #1709653 ^designation[0].language = #de-AT 
* #1709653 ^designation[0].value = "ESLICARBAZEPINACETAT" 
* #1709656 "ULIPRISTALACETAT"
* #1709656 ^designation[0].language = #de-AT 
* #1709656 ^designation[0].value = "ULIPRISTALACETAT" 
* #1709657 "ZANAMIVIR"
* #1709657 ^designation[0].language = #de-AT 
* #1709657 ^designation[0].value = "ZANAMIVIR" 
* #1709658 "MENTHAE ARVENSIS AETHEROLEUM PARTIM MENTHOLUM DEPLETUM"
* #1709658 ^designation[0].language = #de-AT 
* #1709658 ^designation[0].value = "MENTHAE ARVENSIS AETHEROLEUM PARTIM MENTHOLUM DEPLETUM" 
* #1709659 "PSEUDOEPHEDRIN SULFAT"
* #1709659 ^designation[0].language = #de-AT 
* #1709659 ^designation[0].value = "PSEUDOEPHEDRIN SULFAT" 
* #1709660 "SUNITINIBMALAT"
* #1709660 ^designation[0].language = #de-AT 
* #1709660 ^designation[0].value = "SUNITINIBMALAT" 
* #1709661 "REGADENOSON"
* #1709661 ^designation[0].language = #de-AT 
* #1709661 ^designation[0].value = "REGADENOSON" 
* #1709663 "ASENAPIN"
* #1709663 ^designation[0].language = #de-AT 
* #1709663 ^designation[0].value = "ASENAPIN" 
* #1709668 "FRAGARIAE FOLIUM"
* #1709668 ^designation[0].language = #de-AT 
* #1709668 ^designation[0].value = "FRAGARIAE FOLIUM" 
* #1709671 "BISOPROLOL HEMIFUMARAT"
* #1709671 ^designation[0].language = #de-AT 
* #1709671 ^designation[0].value = "BISOPROLOL HEMIFUMARAT" 
* #1709672 "CLOPIDOGREL BESILAT"
* #1709672 ^designation[0].language = #de-AT 
* #1709672 ^designation[0].value = "CLOPIDOGREL BESILAT" 
* #1709673 "TETRAKIS (2-METHOXY-2-METHYLPROPANISOCYANID) KUPFER (I)-TETRAFLUOROBORAT"
* #1709673 ^designation[0].language = #de-AT 
* #1709673 ^designation[0].value = "TETRAKIS (2-METHOXY-2-METHYLPROPANISOCYANID) KUPFER (I)-TETRAFLUOROBORAT" 
* #1709674 "AUTOLOGE KNORPELZELLEN"
* #1709674 ^designation[0].language = #de-AT 
* #1709674 ^designation[0].value = "AUTOLOGE KNORPELZELLEN" 
* #1709678 "LUFT ZUR MEDIZINISCHEN ANWENDUNG"
* #1709678 ^designation[0].language = #de-AT 
* #1709678 ^designation[0].value = "LUFT ZUR MEDIZINISCHEN ANWENDUNG" 
* #1709682 "DINATRIUM CROMOGLICAT"
* #1709682 ^designation[0].language = #de-AT 
* #1709682 ^designation[0].value = "DINATRIUM CROMOGLICAT" 
* #1709692 "MACROGOL"
* #1709692 ^designation[0].language = #de-AT 
* #1709692 ^designation[0].value = "MACROGOL" 
* #1709695 "VELAGLUCERASE ALFA"
* #1709695 ^designation[0].language = #de-AT 
* #1709695 ^designation[0].value = "VELAGLUCERASE ALFA" 
* #1709697 "TAFLUPROST"
* #1709697 ^designation[0].language = #de-AT 
* #1709697 ^designation[0].value = "TAFLUPROST" 
* #1709698 "CLOPIDOGREL HYDROCHLORID"
* #1709698 ^designation[0].language = #de-AT 
* #1709698 ^designation[0].value = "CLOPIDOGREL HYDROCHLORID" 
* #1709703 "HELENII RHIZOMA"
* #1709703 ^designation[0].language = #de-AT 
* #1709703 ^designation[0].value = "HELENII RHIZOMA" 
* #1709708 "BOCEPREVIR"
* #1709708 ^designation[0].language = #de-AT 
* #1709708 ^designation[0].value = "BOCEPREVIR" 
* #1709709 "RILPIVIRIN"
* #1709709 ^designation[0].language = #de-AT 
* #1709709 ^designation[0].value = "RILPIVIRIN" 
* #1709711 "CEFTAROLIN FOSAMIL"
* #1709711 ^designation[0].language = #de-AT 
* #1709711 ^designation[0].value = "CEFTAROLIN FOSAMIL" 
* #1709712 "TELAPREVIR"
* #1709712 ^designation[0].language = #de-AT 
* #1709712 ^designation[0].value = "TELAPREVIR" 
* #1709716 "ACETYLSALICYLSÄURE"
* #1709716 ^designation[0].language = #de-AT 
* #1709716 ^designation[0].value = "ACETYLSALICYLSÄURE" 
* #1709720 "RUMICIS ACETOSAE HERBA"
* #1709720 ^designation[0].language = #de-AT 
* #1709720 ^designation[0].value = "RUMICIS ACETOSAE HERBA" 
* #1709721 "CALCIUM 5-METHYLTETRAHYDROFOLAT"
* #1709721 ^designation[0].language = #de-AT 
* #1709721 ^designation[0].value = "CALCIUM 5-METHYLTETRAHYDROFOLAT" 
* #1709724 "RUXOLITINIB"
* #1709724 ^designation[0].language = #de-AT 
* #1709724 ^designation[0].value = "RUXOLITINIB" 
* #1709726 "CUCURBITAE SEMEN"
* #1709726 ^designation[0].language = #de-AT 
* #1709726 ^designation[0].value = "CUCURBITAE SEMEN" 
* #1709728 "ZINGIBERIS RHIZOMA (AUSZUG)"
* #1709728 ^designation[0].language = #de-AT 
* #1709728 ^designation[0].value = "ZINGIBERIS RHIZOMA (AUSZUG)" 
* #1709730 "INGENOLMEBUTAT"
* #1709730 ^designation[0].language = #de-AT 
* #1709730 ^designation[0].value = "INGENOLMEBUTAT" 
* #1709733 "LINACLOTID"
* #1709733 ^designation[0].language = #de-AT 
* #1709733 ^designation[0].value = "LINACLOTID" 
* #1709734 "GLUCOSAMIN SULFAT X NATRIUM CHLORID"
* #1709734 ^designation[0].language = #de-AT 
* #1709734 ^designation[0].value = "GLUCOSAMIN SULFAT X NATRIUM CHLORID" 
* #1709737 "TAPENTADOL HYDROCHLORID"
* #1709737 ^designation[0].language = #de-AT 
* #1709737 ^designation[0].value = "TAPENTADOL HYDROCHLORID" 
* #1709738 "ALOGLIPTINBENZOAT"
* #1709738 ^designation[0].language = #de-AT 
* #1709738 ^designation[0].value = "ALOGLIPTINBENZOAT" 
* #1709739 "GERINNUNGSFAKTOR XIII"
* #1709739 ^designation[0].language = #de-AT 
* #1709739 ^designation[0].value = "GERINNUNGSFAKTOR XIII" 
* #1709740 "BIFIDOBACTERIUM"
* #1709740 ^designation[0].language = #de-AT 
* #1709740 ^designation[0].value = "BIFIDOBACTERIUM" 
* #1709742 "PERAMPANEL"
* #1709742 ^designation[0].language = #de-AT 
* #1709742 ^designation[0].value = "PERAMPANEL" 
* #1709761 "SABALIS SERRULATAE FRUCTUS (AUSZUG)"
* #1709761 ^designation[0].language = #de-AT 
* #1709761 ^designation[0].value = "SABALIS SERRULATAE FRUCTUS (AUSZUG)" 
* #1709763 "AUCKLANDIAE RADIX"
* #1709763 ^designation[0].language = #de-AT 
* #1709763 ^designation[0].value = "AUCKLANDIAE RADIX" 
* #1709765 "IVACAFTOR"
* #1709765 ^designation[0].language = #de-AT 
* #1709765 ^designation[0].value = "IVACAFTOR" 
* #1709829 "2-PINEN + 2(10)-PINEN"
* #1709829 ^designation[0].language = #de-AT 
* #1709829 ^designation[0].value = "2-PINEN + 2(10)-PINEN" 
* #1709854 "5-CHLORCARVACROL"
* #1709854 ^designation[0].language = #de-AT 
* #1709854 ^designation[0].value = "5-CHLORCARVACROL" 
* #1709863 "ABIRATERONACETAT"
* #1709863 ^designation[0].language = #de-AT 
* #1709863 ^designation[0].value = "ABIRATERONACETAT" 
* #1709865 "ABSINTHII HERBA (AUSZUG)"
* #1709865 ^designation[0].language = #de-AT 
* #1709865 ^designation[0].value = "ABSINTHII HERBA (AUSZUG)" 
* #1709882 "AEGLE FRUCTUS"
* #1709882 ^designation[0].language = #de-AT 
* #1709882 ^designation[0].value = "AEGLE FRUCTUS" 
* #1709895 "ALLII SATIVI BULBUS (AUSZUG)"
* #1709895 ^designation[0].language = #de-AT 
* #1709895 ^designation[0].value = "ALLII SATIVI BULBUS (AUSZUG)" 
* #1709897 "ALTHAEAE RADIX (AUSZUG)"
* #1709897 ^designation[0].language = #de-AT 
* #1709897 ^designation[0].value = "ALTHAEAE RADIX (AUSZUG)" 
* #1709923 "ANGELICAE RADIX (AUSZUG)"
* #1709923 ^designation[0].language = #de-AT 
* #1709923 ^designation[0].value = "ANGELICAE RADIX (AUSZUG)" 
* #1709925 "ANISI FRUCTUS (AUSZUG)"
* #1709925 ^designation[0].language = #de-AT 
* #1709925 ^designation[0].value = "ANISI FRUCTUS (AUSZUG)" 
* #1709927 "ANISI STELLATI FRUCTUS (AUSZUG)"
* #1709927 ^designation[0].language = #de-AT 
* #1709927 ^designation[0].value = "ANISI STELLATI FRUCTUS (AUSZUG)" 
* #1709940 "AQUILEGIAE HERBA"
* #1709940 ^designation[0].language = #de-AT 
* #1709940 ^designation[0].value = "AQUILEGIAE HERBA" 
* #1709970 "AURANTII AMARI EPICARPIUM ET MESOCARPIUM (AUSZUG)"
* #1709970 ^designation[0].language = #de-AT 
* #1709970 ^designation[0].value = "AURANTII AMARI EPICARPIUM ET MESOCARPIUM (AUSZUG)" 
* #1709974 "AURANTII AMARI FLOS (AUSZUG)"
* #1709974 ^designation[0].language = #de-AT 
* #1709974 ^designation[0].value = "AURANTII AMARI FLOS (AUSZUG)" 
* #1709976 "AURANTII DULCIS AETHEROLEUM"
* #1709976 ^designation[0].language = #de-AT 
* #1709976 ^designation[0].value = "AURANTII DULCIS AETHEROLEUM" 
* #1709981 "AURANTII FOLIUM (AUSZUG)"
* #1709981 ^designation[0].language = #de-AT 
* #1709981 ^designation[0].value = "AURANTII FOLIUM (AUSZUG)" 
* #1709982 "AURANTII FRUCTUS IMMATURUS (AUSZUG)"
* #1709982 ^designation[0].language = #de-AT 
* #1709982 ^designation[0].value = "AURANTII FRUCTUS IMMATURUS (AUSZUG)" 
* #1709983 "AURANTII SINENSIS PERICARPIUM (AUSZUG)"
* #1709983 ^designation[0].language = #de-AT 
* #1709983 ^designation[0].value = "AURANTII SINENSIS PERICARPIUM (AUSZUG)" 
* #1710016 "BILASTIN"
* #1710016 ^designation[0].language = #de-AT 
* #1710016 ^designation[0].value = "BILASTIN" 
* #1710035 "CALAMI RHIZOMA (AUSZUG)"
* #1710035 ^designation[0].language = #de-AT 
* #1710035 ^designation[0].value = "CALAMI RHIZOMA (AUSZUG)" 
* #1710046 "CALENDULAE FLOS CUM CALYCE"
* #1710046 ^designation[0].language = #de-AT 
* #1710046 ^designation[0].value = "CALENDULAE FLOS CUM CALYCE" 
* #1710047 "CALLUNAE HERBA"
* #1710047 ^designation[0].language = #de-AT 
* #1710047 ^designation[0].value = "CALLUNAE HERBA" 
* #1710052 "CANNABIS SATIVA L.,FOLIUM CUM FLORE,CBD TYP (AUSZUG)"
* #1710052 ^designation[0].language = #de-AT 
* #1710052 ^designation[0].value = "CANNABIS SATIVA L.,FOLIUM CUM FLORE,CBD TYP (AUSZUG)" 
* #1710053 "CANNABIS SATIVA L.,FOLIUM CUM FLORE,THC TYP (AUSZUG)"
* #1710053 ^designation[0].language = #de-AT 
* #1710053 ^designation[0].value = "CANNABIS SATIVA L.,FOLIUM CUM FLORE,THC TYP (AUSZUG)" 
* #1710059 "CARDAMOMI FRUCTUS"
* #1710059 ^designation[0].language = #de-AT 
* #1710059 ^designation[0].value = "CARDAMOMI FRUCTUS" 
* #1710060 "CARDAMOMI FRUCTUS (AUSZUG)"
* #1710060 ^designation[0].language = #de-AT 
* #1710060 ^designation[0].value = "CARDAMOMI FRUCTUS (AUSZUG)" 
* #1710063 "CARLINAE RADIX (AUSZUG)"
* #1710063 ^designation[0].language = #de-AT 
* #1710063 ^designation[0].value = "CARLINAE RADIX (AUSZUG)" 
* #1710064 "CAROVERINHYDROCHLORID MONOHYDRAT"
* #1710064 ^designation[0].language = #de-AT 
* #1710064 ^designation[0].value = "CAROVERINHYDROCHLORID MONOHYDRAT" 
* #1710065 "CARVI AETHEROLEUM"
* #1710065 ^designation[0].language = #de-AT 
* #1710065 ^designation[0].value = "CARVI AETHEROLEUM" 
* #1710066 "CARVI FRUCTUS (AUSZUG)"
* #1710066 ^designation[0].language = #de-AT 
* #1710066 ^designation[0].value = "CARVI FRUCTUS (AUSZUG)" 
* #1710068 "CARYOPHYLLI FLOS (AUSZUG)"
* #1710068 ^designation[0].language = #de-AT 
* #1710068 ^designation[0].value = "CARYOPHYLLI FLOS (AUSZUG)" 
* #1710069 "CASTANEAE FOLIUM"
* #1710069 ^designation[0].language = #de-AT 
* #1710069 ^designation[0].value = "CASTANEAE FOLIUM" 
* #1710076 "CEFEPIMDIHYDROCHLORID"
* #1710076 ^designation[0].language = #de-AT 
* #1710076 ^designation[0].value = "CEFEPIMDIHYDROCHLORID" 
* #1710081 "CEFTOBIPROL MEDOCARIL NATRIUM"
* #1710081 ^designation[0].language = #de-AT 
* #1710081 ^designation[0].value = "CEFTOBIPROL MEDOCARIL NATRIUM" 
* #1710099 "CHAMOMILLAE ROMANAE FLOS (AUSZUG)"
* #1710099 ^designation[0].language = #de-AT 
* #1710099 ^designation[0].value = "CHAMOMILLAE ROMANAE FLOS (AUSZUG)" 
* #1710111 "CINNAMOMI CASSIAE FLOS (AUSZUG)"
* #1710111 ^designation[0].language = #de-AT 
* #1710111 ^designation[0].value = "CINNAMOMI CASSIAE FLOS (AUSZUG)" 
* #1710112 "CINNAMOMI CORTEX (AUSZUG)"
* #1710112 ^designation[0].language = #de-AT 
* #1710112 ^designation[0].value = "CINNAMOMI CORTEX (AUSZUG)" 
* #1710119 "CLARITHROMYCINCITRAT"
* #1710119 ^designation[0].language = #de-AT 
* #1710119 ^designation[0].value = "CLARITHROMYCINCITRAT" 
* #1710124 "CLOSTRIDIUM BOTULINUM (AUSZUG, PRODUKTE)"
* #1710124 ^designation[0].language = #de-AT 
* #1710124 ^designation[0].value = "CLOSTRIDIUM BOTULINUM (AUSZUG, PRODUKTE)" 
* #1710131 "CNICI BENEDICTI HERBA (AUSZUG)"
* #1710131 ^designation[0].language = #de-AT 
* #1710131 ^designation[0].value = "CNICI BENEDICTI HERBA (AUSZUG)" 
* #1710146 "CORIANDRI FRUCTUS (AUSZUG)"
* #1710146 ^designation[0].language = #de-AT 
* #1710146 ^designation[0].value = "CORIANDRI FRUCTUS (AUSZUG)" 
* #1710147 "CORONAVIRUS"
* #1710147 ^designation[0].language = #de-AT 
* #1710147 ^designation[0].value = "CORONAVIRUS" 
* #1710150 "CRATAEGI FOLIUM (AUSZUG)"
* #1710150 ^designation[0].language = #de-AT 
* #1710150 ^designation[0].value = "CRATAEGI FOLIUM (AUSZUG)" 
* #1710151 "CRATAEGI FRUCTUS (AUSZUG)"
* #1710151 ^designation[0].language = #de-AT 
* #1710151 ^designation[0].value = "CRATAEGI FRUCTUS (AUSZUG)" 
* #1710153 "CRATAEGI OXYACANTHAE FLOS (AUSZUG)"
* #1710153 ^designation[0].language = #de-AT 
* #1710153 ^designation[0].value = "CRATAEGI OXYACANTHAE FLOS (AUSZUG)" 
* #1710159 "CUBEBAE FRUCTUS (AUSZUG)"
* #1710159 ^designation[0].language = #de-AT 
* #1710159 ^designation[0].value = "CUBEBAE FRUCTUS (AUSZUG)" 
* #1710160 "CUCURBITAE SEMEN (AUSZUG)"
* #1710160 ^designation[0].language = #de-AT 
* #1710160 ^designation[0].value = "CUCURBITAE SEMEN (AUSZUG)" 
* #1710165 "CYCLIZIN DIHYDROCHLORID"
* #1710165 ^designation[0].language = #de-AT 
* #1710165 ^designation[0].value = "CYCLIZIN DIHYDROCHLORID" 
* #1710170 "CYNARAE FOLIUM"
* #1710170 ^designation[0].language = #de-AT 
* #1710170 ^designation[0].value = "CYNARAE FOLIUM" 
* #1710178 "DEANOL DICLOFENACAT"
* #1710178 ^designation[0].language = #de-AT 
* #1710178 ^designation[0].value = "DEANOL DICLOFENACAT" 
* #1710184 "DEPROTEINISIERTES HAEMODERIVAT AUS KÄLBERBLUT"
* #1710184 ^designation[0].language = #de-AT 
* #1710184 ^designation[0].value = "DEPROTEINISIERTES HAEMODERIVAT AUS KÄLBERBLUT" 
* #1710201 "DROSERAE HERBA ET RADIX (AUSZUG)"
* #1710201 ^designation[0].language = #de-AT 
* #1710201 ^designation[0].value = "DROSERAE HERBA ET RADIX (AUSZUG)" 
* #1710205 "ECHINACEAE PALLIDAE RADIX (AUSZUG)"
* #1710205 ^designation[0].language = #de-AT 
* #1710205 ^designation[0].value = "ECHINACEAE PALLIDAE RADIX (AUSZUG)" 
* #1710206 "ECHINACEAE PURPUREAE RADIX (AUSZUG)"
* #1710206 ^designation[0].language = #de-AT 
* #1710206 ^designation[0].value = "ECHINACEAE PURPUREAE RADIX (AUSZUG)" 
* #1710214 "EISENCARBOXYMALTOSE"
* #1710214 ^designation[0].language = #de-AT 
* #1710214 ^designation[0].value = "EISENCARBOXYMALTOSE" 
* #1710225 "ESCHERICHIA COLI (AUSZUG, PRODUKTE)"
* #1710225 ^designation[0].language = #de-AT 
* #1710225 ^designation[0].value = "ESCHERICHIA COLI (AUSZUG, PRODUKTE)" 
* #1710229 "ETHINYLESTRADIOL X BETADEX"
* #1710229 ^designation[0].language = #de-AT 
* #1710229 ^designation[0].value = "ETHINYLESTRADIOL X BETADEX" 
* #1710238 "ETHYLESTER JODIERTER FETTSÄUREN DES MOHNÖLS"
* #1710238 ^designation[0].language = #de-AT 
* #1710238 ^designation[0].value = "ETHYLESTER JODIERTER FETTSÄUREN DES MOHNÖLS" 
* #1710245 "HEFE (AUSZUG)"
* #1710245 ^designation[0].language = #de-AT 
* #1710245 ^designation[0].value = "HEFE (AUSZUG)" 
* #1710256 "FESOTERODINFUMARAT"
* #1710256 ^designation[0].language = #de-AT 
* #1710256 ^designation[0].value = "FESOTERODINFUMARAT" 
* #1710273 "FOMEPIZOL SULFAT"
* #1710273 ^designation[0].language = #de-AT 
* #1710273 ^designation[0].value = "FOMEPIZOL SULFAT" 
* #1710278 "GADOLINIUMOXID"
* #1710278 ^designation[0].language = #de-AT 
* #1710278 ^designation[0].value = "GADOLINIUMOXID" 
* #1710279 "GALANGAE RHIZOMA (AUSZUG)"
* #1710279 ^designation[0].language = #de-AT 
* #1710279 ^designation[0].value = "GALANGAE RHIZOMA (AUSZUG)" 
* #1710301 "GENTIANAE RADIX (AUSZUG)"
* #1710301 ^designation[0].language = #de-AT 
* #1710301 ^designation[0].value = "GENTIANAE RADIX (AUSZUG)" 
* #1710311 "GLYCYLTYROSIN"
* #1710311 ^designation[0].language = #de-AT 
* #1710311 ^designation[0].value = "GLYCYLTYROSIN" 
* #1710318 "GUAIACI LIGNUM (AUSZUG)"
* #1710318 ^designation[0].language = #de-AT 
* #1710318 ^designation[0].value = "GUAIACI LIGNUM (AUSZUG)" 
* #1710321 "HAEMOPHILUS SPEC."
* #1710321 ^designation[0].language = #de-AT 
* #1710321 ^designation[0].value = "HAEMOPHILUS SPEC." 
* #1710324 "HAMAMELIDIS FOLIUM (AUSZUG)"
* #1710324 ^designation[0].language = #de-AT 
* #1710324 ^designation[0].value = "HAMAMELIDIS FOLIUM (AUSZUG)" 
* #1710334 "HERNIARIAE HERBA"
* #1710334 ^designation[0].language = #de-AT 
* #1710334 ^designation[0].value = "HERNIARIAE HERBA" 
* #1710350 "HUMINSÄUREVERBINDUNG MIT SALICYLSÄURE"
* #1710350 ^designation[0].language = #de-AT 
* #1710350 ^designation[0].value = "HUMINSÄUREVERBINDUNG MIT SALICYLSÄURE" 
* #1710358 "HYDROXYZIN DIHYDROCHLORID"
* #1710358 ^designation[0].language = #de-AT 
* #1710358 ^designation[0].value = "HYDROXYZIN DIHYDROCHLORID" 
* #1710360 "HYOSCYAMI FOLIUM (AUSZUG)"
* #1710360 ^designation[0].language = #de-AT 
* #1710360 ^designation[0].value = "HYOSCYAMI FOLIUM (AUSZUG)" 
* #1710365 "IBERIDIS HERBA ET RADIX (AUSZUG)"
* #1710365 ^designation[0].language = #de-AT 
* #1710365 ^designation[0].value = "IBERIDIS HERBA ET RADIX (AUSZUG)" 
* #1710368 "IMMUNOCYANIN"
* #1710368 ^designation[0].language = #de-AT 
* #1710368 ^designation[0].value = "IMMUNOCYANIN" 
* #1710379 "IVAE MOSCHATAE HERBA (AUSZUG)"
* #1710379 ^designation[0].language = #de-AT 
* #1710379 ^designation[0].value = "IVAE MOSCHATAE HERBA (AUSZUG)" 
* #1710386 "JUNIPERI LIGNUM (AUSZUG)"
* #1710386 ^designation[0].language = #de-AT 
* #1710386 ^designation[0].value = "JUNIPERI LIGNUM (AUSZUG)" 
* #1710389 "KAEMPFERIAE GALANGAE RHIZOMA"
* #1710389 ^designation[0].language = #de-AT 
* #1710389 ^designation[0].value = "KAEMPFERIAE GALANGAE RHIZOMA" 
* #1710410 "LACTOBACILLUS CASEI"
* #1710410 ^designation[0].language = #de-AT 
* #1710410 ^designation[0].value = "LACTOBACILLUS CASEI" 
* #1710411 "LACTOBACILLUS GASSERI"
* #1710411 ^designation[0].language = #de-AT 
* #1710411 ^designation[0].value = "LACTOBACILLUS GASSERI" 
* #1710413 "LACTUCAE SATIVAE FOLIUM"
* #1710413 ^designation[0].language = #de-AT 
* #1710413 ^designation[0].value = "LACTUCAE SATIVAE FOLIUM" 
* #1710425 "LAVANDULAE LATIFOLIAE AETHEROLEUM"
* #1710425 ^designation[0].language = #de-AT 
* #1710425 ^designation[0].value = "LAVANDULAE LATIFOLIAE AETHEROLEUM" 
* #1710432 "LICHEN ISLANDICUS (AUSZUG)"
* #1710432 ^designation[0].language = #de-AT 
* #1710432 ^designation[0].value = "LICHEN ISLANDICUS (AUSZUG)" 
* #1710434 "LIMONIS AETHEROLEUM"
* #1710434 ^designation[0].language = #de-AT 
* #1710434 ^designation[0].value = "LIMONIS AETHEROLEUM" 
* #1710444 "LYSIN GLUTAMAT"
* #1710444 ^designation[0].language = #de-AT 
* #1710444 ^designation[0].value = "LYSIN GLUTAMAT" 
* #1710459 "MAGNESIUM DI(HYDROGENGLUTAMAT)"
* #1710459 ^designation[0].language = #de-AT 
* #1710459 ^designation[0].value = "MAGNESIUM DI(HYDROGENGLUTAMAT)" 
* #1710475 "MARRUBII HERBA"
* #1710475 ^designation[0].language = #de-AT 
* #1710475 ^designation[0].value = "MARRUBII HERBA" 
* #1710476 "MARRUBII HERBA (AUSZUG)"
* #1710476 ^designation[0].language = #de-AT 
* #1710476 ^designation[0].value = "MARRUBII HERBA (AUSZUG)" 
* #1710480 "MATRICARIAE FLOS (AUSZUG)"
* #1710480 ^designation[0].language = #de-AT 
* #1710480 ^designation[0].value = "MATRICARIAE FLOS (AUSZUG)" 
* #1710487 "MELISSAE FOLIUM (AUSZUG)"
* #1710487 ^designation[0].language = #de-AT 
* #1710487 ^designation[0].value = "MELISSAE FOLIUM (AUSZUG)" 
* #1710490 "MENTHAE PIPERITAE FOLIUM (AUSZUG)"
* #1710490 ^designation[0].language = #de-AT 
* #1710490 ^designation[0].value = "MENTHAE PIPERITAE FOLIUM (AUSZUG)" 
* #1710491 "MENTHAE PIPERITAE HERBA"
* #1710491 ^designation[0].language = #de-AT 
* #1710491 ^designation[0].value = "MENTHAE PIPERITAE HERBA" 
* #1710494 "MENYANTHIDIS TRIFOLIATAE FOLIUM (AUSZUG)"
* #1710494 ^designation[0].language = #de-AT 
* #1710494 ^designation[0].value = "MENYANTHIDIS TRIFOLIATAE FOLIUM (AUSZUG)" 
* #1710518 "MORAXELLA (BRANHAMELLA)"
* #1710518 ^designation[0].language = #de-AT 
* #1710518 ^designation[0].value = "MORAXELLA (BRANHAMELLA)" 
* #1710530 "MYRISTICAE SEMEN (AUSZUG)"
* #1710530 ^designation[0].language = #de-AT 
* #1710530 ^designation[0].value = "MYRISTICAE SEMEN (AUSZUG)" 
* #1710532 "MYROBALANI FRUCTUS"
* #1710532 ^designation[0].language = #de-AT 
* #1710532 ^designation[0].value = "MYROBALANI FRUCTUS" 
* #1710533 "MYRRHA (AUSZUG)"
* #1710533 ^designation[0].language = #de-AT 
* #1710533 ^designation[0].value = "MYRRHA (AUSZUG)" 
* #1710537 "N(2)-ALANYLGLUTAMIN"
* #1710537 ^designation[0].language = #de-AT 
* #1710537 ^designation[0].value = "N(2)-ALANYLGLUTAMIN" 
* #1710555 "NATRIUMMOLYBDAT[*99*MO] ZUR GEWINNUNG VON NATRIUMPERTECHNETAT[*99M*TC]"
* #1710555 ^designation[0].language = #de-AT 
* #1710555 ^designation[0].value = "NATRIUMMOLYBDAT[*99*MO] ZUR GEWINNUNG VON NATRIUMPERTECHNETAT[*99M*TC]" 
* #1710572 "O-(BETA-HYDROXYETHYL)RUTOSIDE"
* #1710572 ^designation[0].language = #de-AT 
* #1710572 ^designation[0].value = "O-(BETA-HYDROXYETHYL)RUTOSIDE" 
* #1710577 "OLANZAPIN BENZOAT"
* #1710577 ^designation[0].language = #de-AT 
* #1710577 ^designation[0].value = "OLANZAPIN BENZOAT" 
* #1710581 "OLODATEROL HYDROCHLORID"
* #1710581 ^designation[0].language = #de-AT 
* #1710581 ^designation[0].value = "OLODATEROL HYDROCHLORID" 
* #1710584 "ONONIDIS RADIX (AUSZUG)"
* #1710584 ^designation[0].language = #de-AT 
* #1710584 ^designation[0].value = "ONONIDIS RADIX (AUSZUG)" 
* #1710586 "ONOPORDI ACANTHII FLOS (AUSZUG)"
* #1710586 ^designation[0].language = #de-AT 
* #1710586 ^designation[0].value = "ONOPORDI ACANTHII FLOS (AUSZUG)" 
* #1710588 "ORIGANI HERBA"
* #1710588 ^designation[0].language = #de-AT 
* #1710588 ^designation[0].value = "ORIGANI HERBA" 
* #1710618 "PELARGONII RADIX (AUSZUG)"
* #1710618 ^designation[0].language = #de-AT 
* #1710618 ^designation[0].value = "PELARGONII RADIX (AUSZUG)" 
* #1710628 "PHENOL-METHANAL-HARNSTOFF-POLYKONDENSAT, SULFONIERT, NATRIUMSALZ"
* #1710628 ^designation[0].language = #de-AT 
* #1710628 ^designation[0].value = "PHENOL-METHANAL-HARNSTOFF-POLYKONDENSAT, SULFONIERT, NATRIUMSALZ" 
* #1710631 "PHOSPHOLIPIDE AUS SOJABOHNEN"
* #1710631 ^designation[0].language = #de-AT 
* #1710631 ^designation[0].value = "PHOSPHOLIPIDE AUS SOJABOHNEN" 
* #1710638 "PIMENTAE FRUCTUS"
* #1710638 ^designation[0].language = #de-AT 
* #1710638 ^designation[0].value = "PIMENTAE FRUCTUS" 
* #1710651 "POLYGALAE RADIX (AUSZUG)"
* #1710651 ^designation[0].language = #de-AT 
* #1710651 ^designation[0].value = "POLYGALAE RADIX (AUSZUG)" 
* #1710669 "POTENTILLAE AUREAE HERBA"
* #1710669 ^designation[0].language = #de-AT 
* #1710669 ^designation[0].value = "POTENTILLAE AUREAE HERBA" 
* #1710671 "PRIMULAE FLOS (AUSZUG)"
* #1710671 ^designation[0].language = #de-AT 
* #1710671 ^designation[0].value = "PRIMULAE FLOS (AUSZUG)" 
* #1710672 "PRIMULAE RADIX (AUSZUG)"
* #1710672 ^designation[0].language = #de-AT 
* #1710672 ^designation[0].value = "PRIMULAE RADIX (AUSZUG)" 
* #1710680 "PEPTIDE"
* #1710680 ^designation[0].language = #de-AT 
* #1710680 ^designation[0].value = "PEPTIDE" 
* #1710689 "PULMONALE PHOSPHOLIPIDFRAKTION (SURFACTANT)"
* #1710689 ^designation[0].language = #de-AT 
* #1710689 ^designation[0].value = "PULMONALE PHOSPHOLIPIDFRAKTION (SURFACTANT)" 
* #1710704 "RHODIOLA RHIZOMA (AUSZUG)"
* #1710704 ^designation[0].language = #de-AT 
* #1710704 ^designation[0].value = "RHODIOLA RHIZOMA (AUSZUG)" 
* #1710713 "ROSMARINI FOLIUM (AUSZUG)"
* #1710713 ^designation[0].language = #de-AT 
* #1710713 ^designation[0].value = "ROSMARINI FOLIUM (AUSZUG)" 
* #1710715 "RUBI IDAEI FOLIUM (AUSZUG)"
* #1710715 ^designation[0].language = #de-AT 
* #1710715 ^designation[0].value = "RUBI IDAEI FOLIUM (AUSZUG)" 
* #1710718 "RUMICIS ACETOSAE HERBA (AUSZUG)"
* #1710718 ^designation[0].language = #de-AT 
* #1710718 ^designation[0].value = "RUMICIS ACETOSAE HERBA (AUSZUG)" 
* #1710721 "SALMONELLA SPEC. (AUSZUG, PRODUKTE)"
* #1710721 ^designation[0].language = #de-AT 
* #1710721 ^designation[0].value = "SALMONELLA SPEC. (AUSZUG, PRODUKTE)" 
* #1710724 "SAMBUCI FLOS (AUSZUG)"
* #1710724 ^designation[0].language = #de-AT 
* #1710724 ^designation[0].value = "SAMBUCI FLOS (AUSZUG)" 
* #1710731 "SANTALI RUBRI LIGNUM"
* #1710731 ^designation[0].language = #de-AT 
* #1710731 ^designation[0].value = "SANTALI RUBRI LIGNUM" 
* #1710732 "SANTALI RUBRI LIGNUM (AUSZUG)"
* #1710732 ^designation[0].language = #de-AT 
* #1710732 ^designation[0].value = "SANTALI RUBRI LIGNUM (AUSZUG)" 
* #1710754 "SIDAE CORDIFOLIAE HERBA"
* #1710754 ^designation[0].language = #de-AT 
* #1710754 ^designation[0].value = "SIDAE CORDIFOLIAE HERBA" 
* #1710756 "SILIBININ DINATRIUMDIHEMISUCCINAT"
* #1710756 ^designation[0].language = #de-AT 
* #1710756 ^designation[0].value = "SILIBININ DINATRIUMDIHEMISUCCINAT" 
* #1710766 "SILYBI MARIANI FRUCTUS (AUSZUG)"
* #1710766 ^designation[0].language = #de-AT 
* #1710766 ^designation[0].value = "SILYBI MARIANI FRUCTUS (AUSZUG)" 
* #1710776 "SOLIDAGINIS HERBA (AUSZUG)"
* #1710776 ^designation[0].language = #de-AT 
* #1710776 ^designation[0].value = "SOLIDAGINIS HERBA (AUSZUG)" 
* #1710810 "TARAXACI FOLIUM"
* #1710810 ^designation[0].language = #de-AT 
* #1710810 ^designation[0].value = "TARAXACI FOLIUM" 
* #1710812 "TARAXACI HERBA"
* #1710812 ^designation[0].language = #de-AT 
* #1710812 ^designation[0].value = "TARAXACI HERBA" 
* #1710813 "TARAXACI RADIX (AUSZUG)"
* #1710813 ^designation[0].language = #de-AT 
* #1710813 ^designation[0].value = "TARAXACI RADIX (AUSZUG)" 
* #1710816 "TERLIPRESSIN DIACETAT"
* #1710816 ^designation[0].language = #de-AT 
* #1710816 ^designation[0].value = "TERLIPRESSIN DIACETAT" 
* #1710828 "THYMI HERBA (AUSZUG)"
* #1710828 ^designation[0].language = #de-AT 
* #1710828 ^designation[0].value = "THYMI HERBA (AUSZUG)" 
* #1710832 "TILIAE FLOS (AUSZUG)"
* #1710832 ^designation[0].language = #de-AT 
* #1710832 ^designation[0].value = "TILIAE FLOS (AUSZUG)" 
* #1710835 "TORMENTILLAE RHIZOMA (AUSZUG)"
* #1710835 ^designation[0].language = #de-AT 
* #1710835 ^designation[0].value = "TORMENTILLAE RHIZOMA (AUSZUG)" 
* #1710846 "GERINNUNGSFAKTOR VIII, REKOMBINANT (TUROCTOCOG ALFA)"
* #1710846 ^designation[0].language = #de-AT 
* #1710846 ^designation[0].value = "GERINNUNGSFAKTOR VIII, REKOMBINANT (TUROCTOCOG ALFA)" 
* #1710851 "URTICAE RADIX (AUSZUG)"
* #1710851 ^designation[0].language = #de-AT 
* #1710851 ^designation[0].value = "URTICAE RADIX (AUSZUG)" 
* #1710857 "VALERIANAE RADIX (AUSZUG)"
* #1710857 ^designation[0].language = #de-AT 
* #1710857 ^designation[0].value = "VALERIANAE RADIX (AUSZUG)" 
* #1710861 "VARIZELLA VIRUS"
* #1710861 ^designation[0].language = #de-AT 
* #1710861 ^designation[0].value = "VARIZELLA VIRUS" 
* #1710863 "VERBASCI FLOS (AUSZUG)"
* #1710863 ^designation[0].language = #de-AT 
* #1710863 ^designation[0].value = "VERBASCI FLOS (AUSZUG)" 
* #1710864 "VERBASCI FOLIUM"
* #1710864 ^designation[0].language = #de-AT 
* #1710864 ^designation[0].value = "VERBASCI FOLIUM" 
* #1710884 "VISCI ALBI HERBA"
* #1710884 ^designation[0].language = #de-AT 
* #1710884 ^designation[0].value = "VISCI ALBI HERBA" 
* #1710885 "VISCI ALBI HERBA (AUSZUG)"
* #1710885 ^designation[0].language = #de-AT 
* #1710885 ^designation[0].value = "VISCI ALBI HERBA (AUSZUG)" 
* #1710887 "VITIS VINIFERAE FOLIUM"
* #1710887 ^designation[0].language = #de-AT 
* #1710887 ^designation[0].value = "VITIS VINIFERAE FOLIUM" 
* #1710888 "VITIS VINIFERAE FOLIUM (AUSZUG)"
* #1710888 ^designation[0].language = #de-AT 
* #1710888 ^designation[0].value = "VITIS VINIFERAE FOLIUM (AUSZUG)" 
* #1710894 "ZEDOARIAE RHIZOMA (AUSZUG)"
* #1710894 ^designation[0].language = #de-AT 
* #1710894 ^designation[0].value = "ZEDOARIAE RHIZOMA (AUSZUG)" 
* #1710904 "ZUCLOPENTHIXOL DIHYDROCHLORID"
* #1710904 ^designation[0].language = #de-AT 
* #1710904 ^designation[0].value = "ZUCLOPENTHIXOL DIHYDROCHLORID" 
* #1711267 "ELECTUARIUM THERIACA (AUSZUG)"
* #1711267 ^designation[0].language = #de-AT 
* #1711267 ^designation[0].value = "ELECTUARIUM THERIACA (AUSZUG)" 
* #1711804 "PLASMA-PROTEINE"
* #1711804 ^designation[0].language = #de-AT 
* #1711804 ^designation[0].value = "PLASMA-PROTEINE" 
* #1711859 "RADIUM BROMATUM (KOMM D)"
* #1711859 ^designation[0].language = #de-AT 
* #1711859 ^designation[0].value = "RADIUM BROMATUM (KOMM D)" 
* #1711917 "SCHIEFERÖLE"
* #1711917 ^designation[0].language = #de-AT 
* #1711917 ^designation[0].value = "SCHIEFERÖLE" 
* #1712128 "ALKOHOLISCHE KAMPFERLÖSUNG"
* #1712128 ^designation[0].language = #de-AT 
* #1712128 ^designation[0].value = "ALKOHOLISCHE KAMPFERLÖSUNG" 
* #1712595 "AGNI CASTI FRUCTUS (AUSZUG)"
* #1712595 ^designation[0].language = #de-AT 
* #1712595 ^designation[0].value = "AGNI CASTI FRUCTUS (AUSZUG)" 
* #1712596 "ALLII CEPAE BULBUS (AUSZUG)"
* #1712596 ^designation[0].language = #de-AT 
* #1712596 ^designation[0].value = "ALLII CEPAE BULBUS (AUSZUG)" 
* #1712598 "BAPTISIAE TINCTORIAE RADIX (AUSZUG)"
* #1712598 ^designation[0].language = #de-AT 
* #1712598 ^designation[0].value = "BAPTISIAE TINCTORIAE RADIX (AUSZUG)" 
* #1712601 "BETULAE FOLIUM (AUSZUG)"
* #1712601 ^designation[0].language = #de-AT 
* #1712601 ^designation[0].value = "BETULAE FOLIUM (AUSZUG)" 
* #1712603 "CAPSICI FRUCTUS (AUSZUG)"
* #1712603 ^designation[0].language = #de-AT 
* #1712603 ^designation[0].value = "CAPSICI FRUCTUS (AUSZUG)" 
* #1712605 "CHELIDONII HERBA (AUSZUG)"
* #1712605 ^designation[0].language = #de-AT 
* #1712605 ^designation[0].value = "CHELIDONII HERBA (AUSZUG)" 
* #1712606 "CINCHONAE CORTEX (AUSZUG)"
* #1712606 ^designation[0].language = #de-AT 
* #1712606 ^designation[0].value = "CINCHONAE CORTEX (AUSZUG)" 
* #1712610 "CRATAEGI FOLIUM CUM FLORE (AUSZUG)"
* #1712610 ^designation[0].language = #de-AT 
* #1712610 ^designation[0].value = "CRATAEGI FOLIUM CUM FLORE (AUSZUG)" 
* #1712612 "DULCAMARAE STIPITES (AUSZUG)"
* #1712612 ^designation[0].language = #de-AT 
* #1712612 ^designation[0].value = "DULCAMARAE STIPITES (AUSZUG)" 
* #1712613 "ECHINACEAE ANGUSTIFOLIAE RADIX (AUSZUG)"
* #1712613 ^designation[0].language = #de-AT 
* #1712613 ^designation[0].value = "ECHINACEAE ANGUSTIFOLIAE RADIX (AUSZUG)" 
* #1712614 "ECHINACEAE PURPUREAE HERBA (AUSZUG)"
* #1712614 ^designation[0].language = #de-AT 
* #1712614 ^designation[0].value = "ECHINACEAE PURPUREAE HERBA (AUSZUG)" 
* #1712616 "FRAXINI CORTEX (AUSZUG)"
* #1712616 ^designation[0].language = #de-AT 
* #1712616 ^designation[0].value = "FRAXINI CORTEX (AUSZUG)" 
* #1712621 "HARPAGOPHYTI RADIX (AUSZUG)"
* #1712621 ^designation[0].language = #de-AT 
* #1712621 ^designation[0].value = "HARPAGOPHYTI RADIX (AUSZUG)" 
* #1712622 "HEDERAE FOLIUM (AUSZUG)"
* #1712622 ^designation[0].language = #de-AT 
* #1712622 ^designation[0].value = "HEDERAE FOLIUM (AUSZUG)" 
* #1712623 "HELENII RHIZOMA (AUSZUG)"
* #1712623 ^designation[0].language = #de-AT 
* #1712623 ^designation[0].value = "HELENII RHIZOMA (AUSZUG)" 
* #1712625 "HIPPOCASTANI SEMEN (AUSZUG)"
* #1712625 ^designation[0].language = #de-AT 
* #1712625 ^designation[0].value = "HIPPOCASTANI SEMEN (AUSZUG)" 
* #1712626 "HYOSCYAMI HERBA (AUSZUG)"
* #1712626 ^designation[0].language = #de-AT 
* #1712626 ^designation[0].value = "HYOSCYAMI HERBA (AUSZUG)" 
* #1712627 "IPECACUANHAE RADIX (AUSZUG)"
* #1712627 ^designation[0].language = #de-AT 
* #1712627 ^designation[0].value = "IPECACUANHAE RADIX (AUSZUG)" 
* #1712632 "LUPULI FLOS (AUSZUG)"
* #1712632 ^designation[0].language = #de-AT 
* #1712632 ^designation[0].value = "LUPULI FLOS (AUSZUG)" 
* #1712634 "MILLEFOLII HERBA (AUSZUG)"
* #1712634 ^designation[0].language = #de-AT 
* #1712634 ^designation[0].value = "MILLEFOLII HERBA (AUSZUG)" 
* #1712635 "NEBENNIERE(EXTRAKT)"
* #1712635 ^designation[0].language = #de-AT 
* #1712635 ^designation[0].value = "NEBENNIERE(EXTRAKT)" 
* #1712636 "ORTHOSIPHONIS FOLIUM (AUSZUG)"
* #1712636 ^designation[0].language = #de-AT 
* #1712636 ^designation[0].value = "ORTHOSIPHONIS FOLIUM (AUSZUG)" 
* #1712637 "OXOGLURSÄURE"
* #1712637 ^designation[0].language = #de-AT 
* #1712637 ^designation[0].value = "OXOGLURSÄURE" 
* #1712638 "PASSIFLORAE HERBA (AUSZUG)"
* #1712638 ^designation[0].language = #de-AT 
* #1712638 ^designation[0].value = "PASSIFLORAE HERBA (AUSZUG)" 
* #1712640 "PIPERIS NIGRI FRUCTUS (AUSZUG)"
* #1712640 ^designation[0].language = #de-AT 
* #1712640 ^designation[0].value = "PIPERIS NIGRI FRUCTUS (AUSZUG)" 
* #1712642 "PLANTAGINIS LANCEOLATAE FOLIUM (AUSZUG)"
* #1712642 ^designation[0].language = #de-AT 
* #1712642 ^designation[0].value = "PLANTAGINIS LANCEOLATAE FOLIUM (AUSZUG)" 
* #1712643 "POPULI TREMULAE CORTEX ET FOLIUM (AUSZUG)"
* #1712643 ^designation[0].language = #de-AT 
* #1712643 ^designation[0].value = "POPULI TREMULAE CORTEX ET FOLIUM (AUSZUG)" 
* #1712646 "RATANHIAE RADIX (AUSZUG)"
* #1712646 ^designation[0].language = #de-AT 
* #1712646 ^designation[0].value = "RATANHIAE RADIX (AUSZUG)" 
* #1712648 "RHEI RADIX (AUSZUG)"
* #1712648 ^designation[0].language = #de-AT 
* #1712648 ^designation[0].value = "RHEI RADIX (AUSZUG)" 
* #1712649 "RHOIS AROMATICAE RADICIS CORTEX (AUSZUG)"
* #1712649 ^designation[0].language = #de-AT 
* #1712649 ^designation[0].value = "RHOIS AROMATICAE RADICIS CORTEX (AUSZUG)" 
* #1712651 "SALMONELLA SPEC."
* #1712651 ^designation[0].language = #de-AT 
* #1712651 ^designation[0].value = "SALMONELLA SPEC." 
* #1712652 "SALVIAE OFFICINALIS FOLIUM (AUSZUG)"
* #1712652 ^designation[0].language = #de-AT 
* #1712652 ^designation[0].value = "SALVIAE OFFICINALIS FOLIUM (AUSZUG)" 
* #1712654 "SENNAE FOLIUM (AUSZUG)"
* #1712654 ^designation[0].language = #de-AT 
* #1712654 ^designation[0].value = "SENNAE FOLIUM (AUSZUG)" 
* #1712655 "SERPYLLI HERBA (AUSZUG)"
* #1712655 ^designation[0].language = #de-AT 
* #1712655 ^designation[0].value = "SERPYLLI HERBA (AUSZUG)" 
* #1712656 "SOLIDAGINIS VIRGAUREAE HERBA (AUSZUG)"
* #1712656 ^designation[0].language = #de-AT 
* #1712656 ^designation[0].value = "SOLIDAGINIS VIRGAUREAE HERBA (AUSZUG)" 
* #1712657 "STAPHYLOCOCCUS SPEC."
* #1712657 ^designation[0].language = #de-AT 
* #1712657 ^designation[0].value = "STAPHYLOCOCCUS SPEC." 
* #1712658 "SYMPHYTI HERBA (AUSZUG)"
* #1712658 ^designation[0].language = #de-AT 
* #1712658 ^designation[0].value = "SYMPHYTI HERBA (AUSZUG)" 
* #1712659 "SYMPHYTI RADIX (AUSZUG)"
* #1712659 ^designation[0].language = #de-AT 
* #1712659 ^designation[0].value = "SYMPHYTI RADIX (AUSZUG)" 
* #1712660 "TARAXACI HERBA (AUSZUG)"
* #1712660 ^designation[0].language = #de-AT 
* #1712660 ^designation[0].value = "TARAXACI HERBA (AUSZUG)" 
* #1712661 "TARAXACI RADIX ET HERBA (AUSZUG)"
* #1712661 ^designation[0].language = #de-AT 
* #1712661 ^designation[0].value = "TARAXACI RADIX ET HERBA (AUSZUG)" 
* #1712662 "TEREBINTHINA LARICINA"
* #1712662 ^designation[0].language = #de-AT 
* #1712662 ^designation[0].value = "TEREBINTHINA LARICINA" 
* #1712664 "THUJAE HERBA (AUSZUG)"
* #1712664 ^designation[0].language = #de-AT 
* #1712664 ^designation[0].value = "THUJAE HERBA (AUSZUG)" 
* #1712668 "VERBENAE HERBA (AUSZUG)"
* #1712668 ^designation[0].language = #de-AT 
* #1712668 ^designation[0].value = "VERBENAE HERBA (AUSZUG)" 
* #1712671 "AETHEROLEUM MENTHAE"
* #1712671 ^designation[0].language = #de-AT 
* #1712671 ^designation[0].value = "AETHEROLEUM MENTHAE" 
* #1712672 "SENNAE FRUCTUS"
* #1712672 ^designation[0].language = #de-AT 
* #1712672 ^designation[0].value = "SENNAE FRUCTUS" 
* #1712673 "SENNAE FRUCTUS (AUSZUG)"
* #1712673 ^designation[0].language = #de-AT 
* #1712673 ^designation[0].value = "SENNAE FRUCTUS (AUSZUG)" 
* #1712676 "STREPTOCOCCUS SPEC./ENTEROCOCCUS"
* #1712676 ^designation[0].language = #de-AT 
* #1712676 ^designation[0].value = "STREPTOCOCCUS SPEC./ENTEROCOCCUS" 
* #1712677 "STREPTOCOCCUS SPEC. /ENTEROCOCCUS (AUSZUG, PRODUKTE)"
* #1712677 ^designation[0].language = #de-AT 
* #1712677 ^designation[0].value = "STREPTOCOCCUS SPEC. /ENTEROCOCCUS (AUSZUG, PRODUKTE)" 
* #1712678 "HAEMOPHILUS SPEC. (AUSZUG, PRODUKTE)"
* #1712678 ^designation[0].language = #de-AT 
* #1712678 ^designation[0].value = "HAEMOPHILUS SPEC. (AUSZUG, PRODUKTE)" 
* #1712679 "IMMUNSERUM"
* #1712679 ^designation[0].language = #de-AT 
* #1712679 ^designation[0].value = "IMMUNSERUM" 
* #1712681 "DAMIANAE FOLIUM (AUSZUG)"
* #1712681 ^designation[0].language = #de-AT 
* #1712681 ^designation[0].value = "DAMIANAE FOLIUM (AUSZUG)" 
* #1712685 "UVAE URSI FOLIUM (AUSZUG)"
* #1712685 ^designation[0].language = #de-AT 
* #1712685 ^designation[0].value = "UVAE URSI FOLIUM (AUSZUG)" 
* #1712705 "OPIUM (AUSZUG)"
* #1712705 ^designation[0].language = #de-AT 
* #1712705 ^designation[0].value = "OPIUM (AUSZUG)" 
* #1712709 "ELEUTHEROCOCCI RADIX (AUSZUG)"
* #1712709 ^designation[0].language = #de-AT 
* #1712709 ^designation[0].value = "ELEUTHEROCOCCI RADIX (AUSZUG)" 
* #1712712 "IRIDIS RHIZOMA (AUSZUG)"
* #1712712 ^designation[0].language = #de-AT 
* #1712712 ^designation[0].value = "IRIDIS RHIZOMA (AUSZUG)" 
* #1712725 "COFFEAE SEMEN"
* #1712725 ^designation[0].language = #de-AT 
* #1712725 ^designation[0].value = "COFFEAE SEMEN" 
* #1712731 "ECHINACEAE PURPUREAE HERBA ET RADIX (AUSZUG)"
* #1712731 ^designation[0].language = #de-AT 
* #1712731 ^designation[0].value = "ECHINACEAE PURPUREAE HERBA ET RADIX (AUSZUG)" 
* #1712736 "POLLENEXTRAKT"
* #1712736 ^designation[0].language = #de-AT 
* #1712736 ^designation[0].value = "POLLENEXTRAKT" 
* #1712739 "HERBA PULSATILLAE CUM RADICE (AUSZUG)"
* #1712739 ^designation[0].language = #de-AT 
* #1712739 ^designation[0].value = "HERBA PULSATILLAE CUM RADICE (AUSZUG)" 
* #1712761 "MISCHEXTRAKT"
* #1712761 ^designation[0].language = #de-AT 
* #1712761 ^designation[0].value = "MISCHEXTRAKT" 
* #1712762 "CYNARAE FOLIUM (AUSZUG)"
* #1712762 ^designation[0].language = #de-AT 
* #1712762 ^designation[0].value = "CYNARAE FOLIUM (AUSZUG)" 
* #6987758 "SENNAE FRUCTUS ANGUSTIFOLIAE"
* #6987758 ^designation[0].language = #de-AT 
* #6987758 ^designation[0].value = "SENNAE FRUCTUS ANGUSTIFOLIAE" 
* #6987761 "SENNAE FRUCTUS ANGUSTIFOLIAE (AUSZUG)"
* #6987761 ^designation[0].language = #de-AT 
* #6987761 ^designation[0].value = "SENNAE FRUCTUS ANGUSTIFOLIAE (AUSZUG)" 
* #7000173 "ATORVASTATIN CALCIUM TRIHYDRAT"
* #7000173 ^designation[0].language = #de-AT 
* #7000173 ^designation[0].value = "ATORVASTATIN CALCIUM TRIHYDRAT" 
* #7006538 "GESAMTPROTEIN"
* #7006538 ^designation[0].language = #de-AT 
* #7006538 ^designation[0].value = "GESAMTPROTEIN" 
* #7006588 "ZIPRASIDONHYDROGENSULFAT DIHYDRAT"
* #7006588 ^designation[0].language = #de-AT 
* #7006588 ^designation[0].value = "ZIPRASIDONHYDROGENSULFAT DIHYDRAT" 
* #7006806 "IMIPENEM MONOHYDRAT"
* #7006806 ^designation[0].language = #de-AT 
* #7006806 ^designation[0].value = "IMIPENEM MONOHYDRAT" 
* #7031215 "CAMELLIAE SINENSIS NON FERMENTATUM FOLIUM (AUSZUG)"
* #7031215 ^designation[0].language = #de-AT 
* #7031215 ^designation[0].value = "CAMELLIAE SINENSIS NON FERMENTATUM FOLIUM (AUSZUG)" 
* #7033394 "OTERACIL KALIUM"
* #7033394 ^designation[0].language = #de-AT 
* #7033394 ^designation[0].value = "OTERACIL KALIUM" 
* #7033513 "D-GLUCOSE 1-PHOSPHAT DINATRIUMSALZ TETRAHYDRAT"
* #7033513 ^designation[0].language = #de-AT 
* #7033513 ^designation[0].value = "D-GLUCOSE 1-PHOSPHAT DINATRIUMSALZ TETRAHYDRAT" 
* #7033605 "APROTININ ACETAT"
* #7033605 ^designation[0].language = #de-AT 
* #7033605 ^designation[0].value = "APROTININ ACETAT" 
* #7033707 "AZITHROMYCIN DIHYDRAT"
* #7033707 ^designation[0].language = #de-AT 
* #7033707 ^designation[0].value = "AZITHROMYCIN DIHYDRAT" 
* #7033709 "AMOXICILLIN TRIHYDRAT"
* #7033709 ^designation[0].language = #de-AT 
* #7033709 ^designation[0].value = "AMOXICILLIN TRIHYDRAT" 
* #7034728 "CEFALEXIN MONOHYDRAT"
* #7034728 ^designation[0].language = #de-AT 
* #7034728 ^designation[0].value = "CEFALEXIN MONOHYDRAT" 
* #7036515 "ESOMEPRAZOL MAGNESIUM DIHYDRAT"
* #7036515 ^designation[0].language = #de-AT 
* #7036515 ^designation[0].value = "ESOMEPRAZOL MAGNESIUM DIHYDRAT" 
* #7037819 "ZOLEDRONSÄURE MONOHYDRAT"
* #7037819 ^designation[0].language = #de-AT 
* #7037819 ^designation[0].value = "ZOLEDRONSÄURE MONOHYDRAT" 
* #7057737 "LYSIN L HYDRAT"
* #7057737 ^designation[0].language = #de-AT 
* #7057737 ^designation[0].value = "LYSIN L HYDRAT" 
* #7091608 "POMALIDOMID"
* #7091608 ^designation[0].language = #de-AT 
* #7091608 ^designation[0].value = "POMALIDOMID" 
* #7091649 "HOLZKOHLE"
* #7091649 ^designation[0].language = #de-AT 
* #7091649 ^designation[0].value = "HOLZKOHLE" 
* #7104212 "TETRAAZACYCLODODECANTETRAESSIGSÄURE"
* #7104212 ^designation[0].language = #de-AT 
* #7104212 ^designation[0].value = "TETRAAZACYCLODODECANTETRAESSIGSÄURE" 
* #7151228 "PANTOPRAZOL NATRIUM SESQUIHYDRAT"
* #7151228 ^designation[0].language = #de-AT 
* #7151228 ^designation[0].value = "PANTOPRAZOL NATRIUM SESQUIHYDRAT" 
* #7151463 "FIDAXOMICIN"
* #7151463 ^designation[0].language = #de-AT 
* #7151463 ^designation[0].value = "FIDAXOMICIN" 
* #7155999 "VEMURAFENIB"
* #7155999 ^designation[0].language = #de-AT 
* #7155999 ^designation[0].value = "VEMURAFENIB" 
* #7156318 "BENDAMUSTIN HYDROCHLORID MONOHYDRAT"
* #7156318 ^designation[0].language = #de-AT 
* #7156318 ^designation[0].value = "BENDAMUSTIN HYDROCHLORID MONOHYDRAT" 
* #7156458 "LEVOMETHADONHYDROCHLORID"
* #7156458 ^designation[0].language = #de-AT 
* #7156458 ^designation[0].value = "LEVOMETHADONHYDROCHLORID" 
* #7184373 "NATRIUMALENDRONAT MONOHYDRAT"
* #7184373 ^designation[0].language = #de-AT 
* #7184373 ^designation[0].value = "NATRIUMALENDRONAT MONOHYDRAT" 
* #7191449 "EPHEDRIN HEMIHYDRAT"
* #7191449 ^designation[0].language = #de-AT 
* #7191449 ^designation[0].value = "EPHEDRIN HEMIHYDRAT" 
* #7191495 "PRAMIPEXOL DIHYDROCHLORID MONOHYDRAT"
* #7191495 ^designation[0].language = #de-AT 
* #7191495 ^designation[0].value = "PRAMIPEXOL DIHYDROCHLORID MONOHYDRAT" 
* #7211686 "LEVOFLOXACIN HEMIHYDRAT"
* #7211686 ^designation[0].language = #de-AT 
* #7211686 ^designation[0].value = "LEVOFLOXACIN HEMIHYDRAT" 
* #7211709 "AMSACRIN"
* #7211709 ^designation[0].language = #de-AT 
* #7211709 ^designation[0].value = "AMSACRIN" 
* #7211711 "ACLIDINIUM BROMID"
* #7211711 ^designation[0].language = #de-AT 
* #7211711 ^designation[0].value = "ACLIDINIUM BROMID" 
* #7211713 "LIDOCAIN HYDROCHLORID MONOHYDRAT"
* #7211713 ^designation[0].language = #de-AT 
* #7211713 ^designation[0].value = "LIDOCAIN HYDROCHLORID MONOHYDRAT" 
* #7217889 "IRINOTECAN HYDROCHLORID TRIHYDRAT"
* #7217889 ^designation[0].language = #de-AT 
* #7217889 ^designation[0].value = "IRINOTECAN HYDROCHLORID TRIHYDRAT" 
* #7225082 "NATRIUM FLUCLOXACILLINAT MONOHYDRAT"
* #7225082 ^designation[0].language = #de-AT 
* #7225082 ^designation[0].value = "NATRIUM FLUCLOXACILLINAT MONOHYDRAT" 
* #7240554 "SILTUXIMAB"
* #7240554 ^designation[0].language = #de-AT 
* #7240554 ^designation[0].value = "SILTUXIMAB" 
* #7249184 "AESCIN WASSERLÖSLICH (ALPHA)"
* #7249184 ^designation[0].language = #de-AT 
* #7249184 ^designation[0].value = "AESCIN WASSERLÖSLICH (ALPHA)" 
* #7255602 "LEVOMENTHOL"
* #7255602 ^designation[0].language = #de-AT 
* #7255602 ^designation[0].value = "LEVOMENTHOL" 
* #7280092 "TACROLIMUS MONOHYDRAT"
* #7280092 ^designation[0].language = #de-AT 
* #7280092 ^designation[0].value = "TACROLIMUS MONOHYDRAT" 
* #7280128 "ISOSORBIDMONONITRAT, VERDÜNNTES"
* #7280128 ^designation[0].language = #de-AT 
* #7280128 ^designation[0].value = "ISOSORBIDMONONITRAT, VERDÜNNTES" 
* #7280132 "MEROPENEM TRIHYDRAT"
* #7280132 ^designation[0].language = #de-AT 
* #7280132 ^designation[0].value = "MEROPENEM TRIHYDRAT" 
* #7291729 "CANGRELOR TETRANATRIUM"
* #7291729 ^designation[0].language = #de-AT 
* #7291729 ^designation[0].value = "CANGRELOR TETRANATRIUM" 
* #7291731 "AMLODIPIN MESILAT MONOHYDRAT"
* #7291731 ^designation[0].language = #de-AT 
* #7291731 ^designation[0].value = "AMLODIPIN MESILAT MONOHYDRAT" 
* #7291739 "IPRATROPIUMBROMID MONOHYDRAT"
* #7291739 ^designation[0].language = #de-AT 
* #7291739 ^designation[0].value = "IPRATROPIUMBROMID MONOHYDRAT" 
* #7291746 "RILPIVIRIN HYDROCHLORID"
* #7291746 ^designation[0].language = #de-AT 
* #7291746 ^designation[0].value = "RILPIVIRIN HYDROCHLORID" 
* #7300970 "ZOLEDRONSÄURE HEMIPENTAHYDRAT"
* #7300970 ^designation[0].language = #de-AT 
* #7300970 ^designation[0].value = "ZOLEDRONSÄURE HEMIPENTAHYDRAT" 
* #7331461 "ZIPRASIDON MESILAT TRIHYDRAT"
* #7331461 ^designation[0].language = #de-AT 
* #7331461 ^designation[0].value = "ZIPRASIDON MESILAT TRIHYDRAT" 
* #7331467 "DOXYCYCLIN MONOHYDRAT"
* #7331467 ^designation[0].language = #de-AT 
* #7331467 ^designation[0].value = "DOXYCYCLIN MONOHYDRAT" 
* #7332313 "GLUCOSE MONOHYDRAT"
* #7332313 ^designation[0].language = #de-AT 
* #7332313 ^designation[0].value = "GLUCOSE MONOHYDRAT" 
* #7332323 "ZINKDIACETAT DIHYDRAT"
* #7332323 ^designation[0].language = #de-AT 
* #7332323 ^designation[0].value = "ZINKDIACETAT DIHYDRAT" 
* #7332328 "MANGANCHLORID TETRAHYDRAT"
* #7332328 ^designation[0].language = #de-AT 
* #7332328 ^designation[0].value = "MANGANCHLORID TETRAHYDRAT" 
* #7332363 "CHROMCHLORID HEXAHYDRAT"
* #7332363 ^designation[0].language = #de-AT 
* #7332363 ^designation[0].value = "CHROMCHLORID HEXAHYDRAT" 
* #7332381 "GLUCOSE, WASSERFREI"
* #7332381 ^designation[0].language = #de-AT 
* #7332381 ^designation[0].value = "GLUCOSE, WASSERFREI" 
* #7332391 "MOMETASON FUROAT MONOHYDRAT"
* #7332391 ^designation[0].language = #de-AT 
* #7332391 ^designation[0].value = "MOMETASON FUROAT MONOHYDRAT" 
* #7341397 "IBUPROFEN NATRIUM DIHYDRAT"
* #7341397 ^designation[0].language = #de-AT 
* #7341397 ^designation[0].value = "IBUPROFEN NATRIUM DIHYDRAT" 
* #7349717 "ONDANSETRON HYDROCHLORID DIHYDRAT"
* #7349717 ^designation[0].language = #de-AT 
* #7349717 ^designation[0].value = "ONDANSETRON HYDROCHLORID DIHYDRAT" 
* #7395953 "DONEPEZIL HYDROCHLORID MONOHYDRAT"
* #7395953 ^designation[0].language = #de-AT 
* #7395953 ^designation[0].value = "DONEPEZIL HYDROCHLORID MONOHYDRAT" 
* #7395955 "CALCIUMCHLORID DIHYDRAT"
* #7395955 ^designation[0].language = #de-AT 
* #7395955 ^designation[0].value = "CALCIUMCHLORID DIHYDRAT" 
* #7395962 "MAGNESIUMCHLORID HEXAHYDRAT"
* #7395962 ^designation[0].language = #de-AT 
* #7395962 ^designation[0].value = "MAGNESIUMCHLORID HEXAHYDRAT" 
* #7396034 "MAGNESIUMSULFAT HEPTAHYDRAT"
* #7396034 ^designation[0].language = #de-AT 
* #7396034 ^designation[0].value = "MAGNESIUMSULFAT HEPTAHYDRAT" 
* #7396038 "NATRIUMACETAT TRIHYDRAT"
* #7396038 ^designation[0].language = #de-AT 
* #7396038 ^designation[0].value = "NATRIUMACETAT TRIHYDRAT" 
* #7396042 "ZINKSULFAT-HEPTAHYDRAT"
* #7396042 ^designation[0].language = #de-AT 
* #7396042 ^designation[0].value = "ZINKSULFAT-HEPTAHYDRAT" 
* #7396118 "MANGANSULFAT MONOHYDRAT"
* #7396118 ^designation[0].language = #de-AT 
* #7396118 ^designation[0].value = "MANGANSULFAT MONOHYDRAT" 
* #7396121 "ZINKCITRAT TRIHYDRAT"
* #7396121 ^designation[0].language = #de-AT 
* #7396121 ^designation[0].value = "ZINKCITRAT TRIHYDRAT" 
* #7396178 "ESTRADIOL HEMIHYDRAT"
* #7396178 ^designation[0].language = #de-AT 
* #7396178 ^designation[0].value = "ESTRADIOL HEMIHYDRAT" 
* #7409854 "EISEN(II)-SULFAT, GETROCKNETES"
* #7409854 ^designation[0].language = #de-AT 
* #7409854 ^designation[0].value = "EISEN(II)-SULFAT, GETROCKNETES" 
* #7421783 "KALIUMCITRAT MONOHYDRAT"
* #7421783 ^designation[0].language = #de-AT 
* #7421783 ^designation[0].value = "KALIUMCITRAT MONOHYDRAT" 
* #7421787 "FUSIDINSÄURE HEMIHYDRAT"
* #7421787 ^designation[0].language = #de-AT 
* #7421787 ^designation[0].value = "FUSIDINSÄURE HEMIHYDRAT" 
* #7431272 "CODEINPHOSPHAT HEMIHYDRAT"
* #7431272 ^designation[0].language = #de-AT 
* #7431272 ^designation[0].value = "CODEINPHOSPHAT HEMIHYDRAT" 
* #7431284 "MINOCYCLIN HYDROCHLORID DIHYDRAT"
* #7431284 ^designation[0].language = #de-AT 
* #7431284 ^designation[0].value = "MINOCYCLIN HYDROCHLORID DIHYDRAT" 
* #7431286 "NATRIUM PICOSULFAT MONOHYDRAT"
* #7431286 ^designation[0].language = #de-AT 
* #7431286 ^designation[0].value = "NATRIUM PICOSULFAT MONOHYDRAT" 
* #7446598 "BRENTUXIMAB VEDOTIN"
* #7446598 ^designation[0].language = #de-AT 
* #7446598 ^designation[0].value = "BRENTUXIMAB VEDOTIN" 
* #7446994 "FORMOTEROLFUMARAT DIHYDRAT"
* #7446994 ^designation[0].language = #de-AT 
* #7446994 ^designation[0].value = "FORMOTEROLFUMARAT DIHYDRAT" 
* #7447005 "LISINOPRIL-DIHYDRAT"
* #7447005 ^designation[0].language = #de-AT 
* #7447005 ^designation[0].value = "LISINOPRIL-DIHYDRAT" 
* #7447024 "METOCLOPRAMIDHYDROCHLORID MONOHYDRAT"
* #7447024 ^designation[0].language = #de-AT 
* #7447024 ^designation[0].value = "METOCLOPRAMIDHYDROCHLORID MONOHYDRAT" 
* #7447044 "AMIDOTRIZOESÄURE DIHYDRAT"
* #7447044 ^designation[0].language = #de-AT 
* #7447044 ^designation[0].value = "AMIDOTRIZOESÄURE DIHYDRAT" 
* #7460779 "CEFTAZIDIM-PENTAHYDRAT"
* #7460779 ^designation[0].language = #de-AT 
* #7460779 ^designation[0].value = "CEFTAZIDIM-PENTAHYDRAT" 
* #7461369 "FERUMOXYTOL"
* #7461369 ^designation[0].language = #de-AT 
* #7461369 ^designation[0].value = "FERUMOXYTOL" 
* #7461371 "GIMERACIL"
* #7461371 ^designation[0].language = #de-AT 
* #7461371 ^designation[0].value = "GIMERACIL" 
* #7466687 "NALOXEGOL OXALAT"
* #7466687 ^designation[0].language = #de-AT 
* #7466687 ^designation[0].value = "NALOXEGOL OXALAT" 
* #7466691 "FLUTEMETAMOL (18F)"
* #7466691 ^designation[0].language = #de-AT 
* #7466691 ^designation[0].value = "FLUTEMETAMOL (18F)" 
* #7466693 "PONATINIB"
* #7466693 ^designation[0].language = #de-AT 
* #7466693 ^designation[0].value = "PONATINIB" 
* #7466697 "ENZALUTAMID"
* #7466697 ^designation[0].language = #de-AT 
* #7466697 ^designation[0].value = "ENZALUTAMID" 
* #7466706 "BOSENTAN MONOHYDRAT"
* #7466706 ^designation[0].language = #de-AT 
* #7466706 ^designation[0].value = "BOSENTAN MONOHYDRAT" 
* #7511229 "DINATRIUM CLODRONAT TETRAHYDRAT"
* #7511229 ^designation[0].language = #de-AT 
* #7511229 ^designation[0].value = "DINATRIUM CLODRONAT TETRAHYDRAT" 
* #7524533 "ZIPRASIDONHYDROCHLORID WASSERFREI"
* #7524533 ^designation[0].language = #de-AT 
* #7524533 ^designation[0].value = "ZIPRASIDONHYDROCHLORID WASSERFREI" 
* #7524542 "AVANAFIL"
* #7524542 ^designation[0].language = #de-AT 
* #7524542 ^designation[0].value = "AVANAFIL" 
* #7525236 "AFLIBERCEPT"
* #7525236 ^designation[0].language = #de-AT 
* #7525236 ^designation[0].value = "AFLIBERCEPT" 
* #7525247 "DAPAGLIFLOZIN"
* #7525247 ^designation[0].language = #de-AT 
* #7525247 ^designation[0].value = "DAPAGLIFLOZIN" 
* #7525739 "RIOCIGUAT"
* #7525739 ^designation[0].language = #de-AT 
* #7525739 ^designation[0].value = "RIOCIGUAT" 
* #7526786 "CRIZOTINIB"
* #7526786 ^designation[0].language = #de-AT 
* #7526786 ^designation[0].value = "CRIZOTINIB" 
* #7526788 "LINAGLIPTIN"
* #7526788 ^designation[0].language = #de-AT 
* #7526788 ^designation[0].value = "LINAGLIPTIN" 
* #7611353 "CANAGLIFLOZIN"
* #7611353 ^designation[0].language = #de-AT 
* #7611353 ^designation[0].value = "CANAGLIFLOZIN" 
* #7611355 "DOLUTEGRAVIR"
* #7611355 ^designation[0].language = #de-AT 
* #7611355 ^designation[0].value = "DOLUTEGRAVIR" 
* #7611357 "DOLUTEGRAVIR NATRIUM"
* #7611357 ^designation[0].language = #de-AT 
* #7611357 ^designation[0].value = "DOLUTEGRAVIR NATRIUM" 
* #7611361 "BEDAQUILIN FUMARAT"
* #7611361 ^designation[0].language = #de-AT 
* #7611361 ^designation[0].value = "BEDAQUILIN FUMARAT" 
* #7611371 "VILANTEROL TRIFENATAT"
* #7611371 ^designation[0].language = #de-AT 
* #7611371 ^designation[0].value = "VILANTEROL TRIFENATAT" 
* #7611373 "UMECLIDINIUMBROMID"
* #7611373 ^designation[0].language = #de-AT 
* #7611373 ^designation[0].value = "UMECLIDINIUMBROMID" 
* #7611375 "SOFOSBUVIR"
* #7611375 ^designation[0].language = #de-AT 
* #7611375 ^designation[0].value = "SOFOSBUVIR" 
* #7611377 "DAPAGLIFLOZIN PROPANDIOL MONOHYDRAT"
* #7611377 ^designation[0].language = #de-AT 
* #7611377 ^designation[0].value = "DAPAGLIFLOZIN PROPANDIOL MONOHYDRAT" 
* #7611379 "TENOFOVIR DISOPROXIL PHOSPHAT"
* #7611379 ^designation[0].language = #de-AT 
* #7611379 ^designation[0].value = "TENOFOVIR DISOPROXIL PHOSPHAT" 
* #7611390 "COBICISTAT"
* #7611390 ^designation[0].language = #de-AT 
* #7611390 ^designation[0].value = "COBICISTAT" 
* #7611392 "FINGOLIMODHYDROCHLORID"
* #7611392 ^designation[0].language = #de-AT 
* #7611392 ^designation[0].value = "FINGOLIMODHYDROCHLORID" 
* #7611394 "REGORAFENIB"
* #7611394 ^designation[0].language = #de-AT 
* #7611394 ^designation[0].value = "REGORAFENIB" 
* #7611396 "TAFAMIDIS MEGLUMIN"
* #7611396 ^designation[0].language = #de-AT 
* #7611396 ^designation[0].value = "TAFAMIDIS MEGLUMIN" 
* #7611398 "ELVITEGRAVIR"
* #7611398 ^designation[0].language = #de-AT 
* #7611398 ^designation[0].value = "ELVITEGRAVIR" 
* #7611406 "GERINNUNGSFAKTOR IX, REKOMBINANT"
* #7611406 ^designation[0].language = #de-AT 
* #7611406 ^designation[0].value = "GERINNUNGSFAKTOR IX, REKOMBINANT" 
* #7611408 "GERINNUNGSFAKTOR VIII, REKOMBINANT"
* #7611408 ^designation[0].language = #de-AT 
* #7611408 ^designation[0].value = "GERINNUNGSFAKTOR VIII, REKOMBINANT" 
* #7611410 "INSULIN DEGLUDEC"
* #7611410 ^designation[0].language = #de-AT 
* #7611410 ^designation[0].value = "INSULIN DEGLUDEC" 
* #7611431 "CALCIUMASCORBAT DIHYDRAT"
* #7611431 ^designation[0].language = #de-AT 
* #7611431 ^designation[0].value = "CALCIUMASCORBAT DIHYDRAT" 
* #7611433 "CALCIUMHYDROGENPHOSPHAT, WASSERFREIES"
* #7611433 ^designation[0].language = #de-AT 
* #7611433 ^designation[0].value = "CALCIUMHYDROGENPHOSPHAT, WASSERFREIES" 
* #7611439 "KUPFERSULFAT WASSERFREI"
* #7611439 ^designation[0].language = #de-AT 
* #7611439 ^designation[0].value = "KUPFERSULFAT WASSERFREI" 
* #7611455 "MAGNESIUMHYDROGENPHOSPHAT TRIHYDRAT"
* #7611455 ^designation[0].language = #de-AT 
* #7611455 ^designation[0].value = "MAGNESIUMHYDROGENPHOSPHAT TRIHYDRAT" 
* #7611457 "ZINKSULFAT-MONOHYDRAT"
* #7611457 ^designation[0].language = #de-AT 
* #7611457 ^designation[0].value = "ZINKSULFAT-MONOHYDRAT" 
* #7611466 "VERNAKALANT HYDROCHLORID"
* #7611466 ^designation[0].language = #de-AT 
* #7611466 ^designation[0].value = "VERNAKALANT HYDROCHLORID" 
* #7658121 "VORTIOXETINHYDROBROMID"
* #7658121 ^designation[0].language = #de-AT 
* #7658121 ^designation[0].value = "VORTIOXETINHYDROBROMID" 
* #7658132 "CABOZANTINIBMALAT"
* #7658132 ^designation[0].language = #de-AT 
* #7658132 ^designation[0].value = "CABOZANTINIBMALAT" 
* #7658134 "DELAMANID"
* #7658134 ^designation[0].language = #de-AT 
* #7658134 ^designation[0].value = "DELAMANID" 
* #7658136 "AZITHROMYCIN MONOHYDRAT"
* #7658136 ^designation[0].language = #de-AT 
* #7658136 ^designation[0].value = "AZITHROMYCIN MONOHYDRAT" 
* #7665909 "CONESTAT ALFA"
* #7665909 ^designation[0].language = #de-AT 
* #7665909 ^designation[0].value = "CONESTAT ALFA" 
* #7666860 "N-ACETYLGALACTOSAMIN-6-SULFATASE, REKOMBINANT"
* #7666860 ^designation[0].language = #de-AT 
* #7666860 ^designation[0].value = "N-ACETYLGALACTOSAMIN-6-SULFATASE, REKOMBINANT" 
* #7666862 "DABRAFENIBMESILAT"
* #7666862 ^designation[0].language = #de-AT 
* #7666862 ^designation[0].value = "DABRAFENIBMESILAT" 
* #7666864 "NALMEFENHYDROCHLORID DIHYDRAT"
* #7666864 ^designation[0].language = #de-AT 
* #7666864 ^designation[0].value = "NALMEFENHYDROCHLORID DIHYDRAT" 
* #7666866 "PASIREOTIDDIASPARTAT"
* #7666866 ^designation[0].language = #de-AT 
* #7666866 ^designation[0].value = "PASIREOTIDDIASPARTAT" 
* #7666868 "TEDUGLUTID"
* #7666868 ^designation[0].language = #de-AT 
* #7666868 ^designation[0].value = "TEDUGLUTID" 
* #7666870 "TERIFLUNOMID"
* #7666870 ^designation[0].language = #de-AT 
* #7666870 ^designation[0].value = "TERIFLUNOMID" 
* #7666876 "VACCINIAVIRUS"
* #7666876 ^designation[0].language = #de-AT 
* #7666876 ^designation[0].value = "VACCINIAVIRUS" 
* #7673503 "MIRABEGRON"
* #7673503 ^designation[0].language = #de-AT 
* #7673503 ^designation[0].value = "MIRABEGRON" 
* #7673507 "TICAGRELOR"
* #7673507 ^designation[0].language = #de-AT 
* #7673507 ^designation[0].value = "TICAGRELOR" 
* #7673509 "VEDOLIZUMAB"
* #7673509 ^designation[0].language = #de-AT 
* #7673509 ^designation[0].value = "VEDOLIZUMAB" 
* #7673511 "VISMODEGIB"
* #7673511 ^designation[0].language = #de-AT 
* #7673511 ^designation[0].value = "VISMODEGIB" 
* #7675184 "BOSUTINIB MONOHYDRAT"
* #7675184 ^designation[0].language = #de-AT 
* #7675184 ^designation[0].value = "BOSUTINIB MONOHYDRAT" 
* #7675224 "AZILSARTANMEDOXOMIL KALIUM"
* #7675224 ^designation[0].language = #de-AT 
* #7675224 ^designation[0].value = "AZILSARTANMEDOXOMIL KALIUM" 
* #7675226 "LOMITAPIDMESILAT"
* #7675226 ^designation[0].language = #de-AT 
* #7675226 ^designation[0].value = "LOMITAPIDMESILAT" 
* #7675228 "DACLATASVIR DIHYDROCHLORID"
* #7675228 ^designation[0].language = #de-AT 
* #7675228 ^designation[0].value = "DACLATASVIR DIHYDROCHLORID" 
* #7682280 "CABAZITAXEL"
* #7682280 ^designation[0].language = #de-AT 
* #7682280 ^designation[0].value = "CABAZITAXEL" 
* #7682284 "PERTUZUMAB"
* #7682284 ^designation[0].language = #de-AT 
* #7682284 ^designation[0].value = "PERTUZUMAB" 
* #7685223 "ERIBULINMESILAT"
* #7685223 ^designation[0].language = #de-AT 
* #7685223 ^designation[0].value = "ERIBULINMESILAT" 
* #7685225 "DEFIBROTID"
* #7685225 ^designation[0].language = #de-AT 
* #7685225 ^designation[0].value = "DEFIBROTID" 
* #7685227 "MACITENTAN"
* #7685227 ^designation[0].language = #de-AT 
* #7685227 ^designation[0].value = "MACITENTAN" 
* #7685229 "LIXISENATID"
* #7685229 ^designation[0].language = #de-AT 
* #7685229 ^designation[0].value = "LIXISENATID" 
* #7685231 "SIMEPREVIR NATRIUM"
* #7685231 ^designation[0].language = #de-AT 
* #7685231 ^designation[0].value = "SIMEPREVIR NATRIUM" 
* #7685233 "TRAMETINIB-DIMETHYLSULFOXID (1:1)"
* #7685233 ^designation[0].language = #de-AT 
* #7685233 ^designation[0].value = "TRAMETINIB-DIMETHYLSULFOXID (1:1)" 
* #7685235 "EMPAGLIFLOZIN"
* #7685235 ^designation[0].language = #de-AT 
* #7685235 ^designation[0].value = "EMPAGLIFLOZIN" 
* #7685237 "ATALUREN"
* #7685237 ^designation[0].language = #de-AT 
* #7685237 ^designation[0].value = "ATALUREN" 
* #7685260 "LIPOPROTEINLIPASE"
* #7685260 ^designation[0].language = #de-AT 
* #7685260 ^designation[0].value = "LIPOPROTEINLIPASE" 
* #7685267 "NATRIUMDIHYDROGENPHOSPHAT DIHYDRAT"
* #7685267 ^designation[0].language = #de-AT 
* #7685267 ^designation[0].value = "NATRIUMDIHYDROGENPHOSPHAT DIHYDRAT" 
* #7685332 "AFATINIBDIMALEAT"
* #7685332 ^designation[0].language = #de-AT 
* #7685332 ^designation[0].value = "AFATINIBDIMALEAT" 
* #7687965 "OBINUTUZUMAB"
* #7687965 ^designation[0].language = #de-AT 
* #7687965 ^designation[0].value = "OBINUTUZUMAB" 
* #7694504 "PEMETREXED"
* #7694504 ^designation[0].language = #de-AT 
* #7694504 ^designation[0].value = "PEMETREXED" 
* #7724220 "HYDROCORTISON-21- PHOSPHAT DINATRIUM"
* #7724220 ^designation[0].language = #de-AT 
* #7724220 ^designation[0].value = "HYDROCORTISON-21- PHOSPHAT DINATRIUM" 
* #7724223 "PEGINTERFERON BETA-1A"
* #7724223 ^designation[0].language = #de-AT 
* #7724223 ^designation[0].value = "PEGINTERFERON BETA-1A" 
* #7724233 "IDELALISIB"
* #7724233 ^designation[0].language = #de-AT 
* #7724233 ^designation[0].value = "IDELALISIB" 
* #7724237 "TIOTROPIUMBROMID-MONOHYDRAT"
* #7724237 ^designation[0].language = #de-AT 
* #7724237 ^designation[0].value = "TIOTROPIUMBROMID-MONOHYDRAT" 
* #7724239 "TIOTROPIUMBROMID-WASSERFREI"
* #7724239 ^designation[0].language = #de-AT 
* #7724239 ^designation[0].value = "TIOTROPIUMBROMID-WASSERFREI" 
* #7732064 "PEMETREXED DINATRIUM HEMIPENTAHYDRAT"
* #7732064 ^designation[0].language = #de-AT 
* #7732064 ^designation[0].value = "PEMETREXED DINATRIUM HEMIPENTAHYDRAT" 
* #7735203 "HISTIDINHYDROCHLORID MONOHYDRAT"
* #7735203 ^designation[0].language = #de-AT 
* #7735203 ^designation[0].value = "HISTIDINHYDROCHLORID MONOHYDRAT" 
* #7735205 "MAGNESIUMACETAT TETRAHYDRAT"
* #7735205 ^designation[0].language = #de-AT 
* #7735205 ^designation[0].value = "MAGNESIUMACETAT TETRAHYDRAT" 
* #7750473 "NATRIUMALENDRONAT TRIHYDRAT"
* #7750473 ^designation[0].language = #de-AT 
* #7750473 ^designation[0].value = "NATRIUMALENDRONAT TRIHYDRAT" 
* #7782183 "KALIUMHYDROGENASPARTAT HEMIHYDRAT"
* #7782183 ^designation[0].language = #de-AT 
* #7782183 ^designation[0].value = "KALIUMHYDROGENASPARTAT HEMIHYDRAT" 
* #7782185 "CEFEPIMDIHYDROCHLORID MONOHYDRAT"
* #7782185 ^designation[0].language = #de-AT 
* #7782185 ^designation[0].value = "CEFEPIMDIHYDROCHLORID MONOHYDRAT" 
* #7792960 "NETUPITANT"
* #7792960 ^designation[0].language = #de-AT 
* #7792960 ^designation[0].value = "NETUPITANT" 
* #7792982 "NALOXON HYDROCHLORID DIHYDRAT"
* #7792982 ^designation[0].language = #de-AT 
* #7792982 ^designation[0].value = "NALOXON HYDROCHLORID DIHYDRAT" 
* #7813828 "DINATRIUMSELENIT WASSERFREI"
* #7813828 ^designation[0].language = #de-AT 
* #7813828 ^designation[0].value = "DINATRIUMSELENIT WASSERFREI" 
* #7841596 "LUMACAFTOR"
* #7841596 ^designation[0].language = #de-AT 
* #7841596 ^designation[0].value = "LUMACAFTOR" 
* #7841603 "ABACAVIRHYDROCHLORID"
* #7841603 ^designation[0].language = #de-AT 
* #7841603 ^designation[0].value = "ABACAVIRHYDROCHLORID" 
* #7841605 "LEDIPASVIR"
* #7841605 ^designation[0].language = #de-AT 
* #7841605 ^designation[0].value = "LEDIPASVIR" 
* #7841607 "NINTEDANIB ESILAT"
* #7841607 ^designation[0].language = #de-AT 
* #7841607 ^designation[0].value = "NINTEDANIB ESILAT" 
* #7841609 "TILMANOCEPT"
* #7841609 ^designation[0].language = #de-AT 
* #7841609 ^designation[0].value = "TILMANOCEPT" 
* #7864151 "APOMORPHINHYDROCHLORID HEMIHYDRAT"
* #7864151 ^designation[0].language = #de-AT 
* #7864151 ^designation[0].value = "APOMORPHINHYDROCHLORID HEMIHYDRAT" 
* #7864154 "ANAGRELIDHYDROCHLORID MONOHYDRAT"
* #7864154 ^designation[0].language = #de-AT 
* #7864154 ^designation[0].value = "ANAGRELIDHYDROCHLORID MONOHYDRAT" 
* #7864156 "BUPIVACAINHYDROCHLORID MONOHYDRAT"
* #7864156 ^designation[0].language = #de-AT 
* #7864156 ^designation[0].value = "BUPIVACAINHYDROCHLORID MONOHYDRAT" 
* #7875825 "METHYLDOPA SESQUIHYDRAT"
* #7875825 ^designation[0].language = #de-AT 
* #7875825 ^designation[0].value = "METHYLDOPA SESQUIHYDRAT" 
* #7893832 "NATRIUMGLYCEROPHOSPHAT, WASSERHALTIG"
* #7893832 ^designation[0].language = #de-AT 
* #7893832 ^designation[0].value = "NATRIUMGLYCEROPHOSPHAT, WASSERHALTIG" 
* #7893837 "EISEN(III)-CHLORID HEXAHYDRAT"
* #7893837 ^designation[0].language = #de-AT 
* #7893837 ^designation[0].value = "EISEN(III)-CHLORID HEXAHYDRAT" 
* #7893842 "KUPFERDICHLORID DIHYDRAT"
* #7893842 ^designation[0].language = #de-AT 
* #7893842 ^designation[0].value = "KUPFERDICHLORID DIHYDRAT" 
* #7893844 "DINATRIUMMOLYBDAT DIHYDRAT"
* #7893844 ^designation[0].language = #de-AT 
* #7893844 ^designation[0].value = "DINATRIUMMOLYBDAT DIHYDRAT" 
* #7893846 "RASAGILIN HEMITARTRAT"
* #7893846 ^designation[0].language = #de-AT 
* #7893846 ^designation[0].value = "RASAGILIN HEMITARTRAT" 
* #7893848 "ARGATROBAN MONOHYDRAT"
* #7893848 ^designation[0].language = #de-AT 
* #7893848 ^designation[0].value = "ARGATROBAN MONOHYDRAT" 
* #7901193 "ABACAVIR"
* #7901193 ^designation[0].language = #de-AT 
* #7901193 ^designation[0].value = "ABACAVIR" 
* #7921211 "DERMATOPHAGOIDES PTERONYSSINUS (ALLERG.)"
* #7921211 ^designation[0].language = #de-AT 
* #7921211 ^designation[0].value = "DERMATOPHAGOIDES PTERONYSSINUS (ALLERG.)" 
* #7921215 "DERMATOPHAGOIDES FARINAE (ALLERG.)"
* #7921215 ^designation[0].language = #de-AT 
* #7921215 ^designation[0].value = "DERMATOPHAGOIDES FARINAE (ALLERG.)" 
* #7921229 "ARACHIS HYPOGAEA (ALLERG.)"
* #7921229 ^designation[0].language = #de-AT 
* #7921229 ^designation[0].value = "ARACHIS HYPOGAEA (ALLERG.)" 
* #7987667 "OLAPARIB"
* #7987667 ^designation[0].language = #de-AT 
* #7987667 ^designation[0].value = "OLAPARIB" 
* #7988642 "IBRUTINIB"
* #7988642 ^designation[0].language = #de-AT 
* #7988642 ^designation[0].value = "IBRUTINIB" 
* #7988644 "DULAGLUTID"
* #7988644 ^designation[0].language = #de-AT 
* #7988644 ^designation[0].value = "DULAGLUTID" 
* #7996464 "TENOFOVIR DISOPROXIL SUCCINAT"
* #7996464 ^designation[0].language = #de-AT 
* #7996464 ^designation[0].value = "TENOFOVIR DISOPROXIL SUCCINAT" 
* #7996533 "SITAGLIPTIN MALAT"
* #7996533 ^designation[0].language = #de-AT 
* #7996533 ^designation[0].value = "SITAGLIPTIN MALAT" 
* #8007925 "LANDIOLOL HYDROCHLORID"
* #8007925 ^designation[0].language = #de-AT 
* #8007925 ^designation[0].value = "LANDIOLOL HYDROCHLORID" 
* #8007934 "PITOLISANT HYDROCHLORID"
* #8007934 ^designation[0].language = #de-AT 
* #8007934 ^designation[0].value = "PITOLISANT HYDROCHLORID" 
* #8014423 "TERIPARATIDACETAT"
* #8014423 ^designation[0].language = #de-AT 
* #8014423 ^designation[0].value = "TERIPARATIDACETAT" 
* #8023400 "METAMIZOL NATRIUM MONOHYDRAT"
* #8023400 ^designation[0].language = #de-AT 
* #8023400 ^designation[0].value = "METAMIZOL NATRIUM MONOHYDRAT" 
* #8035082 "NATRIUMSULFAT, WASSERFREIES"
* #8035082 ^designation[0].language = #de-AT 
* #8035082 ^designation[0].value = "NATRIUMSULFAT, WASSERFREIES" 
* #8059102 "ABACAVIRHYDROCHLORID MONOHYDRAT"
* #8059102 ^designation[0].language = #de-AT 
* #8059102 ^designation[0].value = "ABACAVIRHYDROCHLORID MONOHYDRAT" 
* #8059162 "CARBIDOPA MONOHYDRAT"
* #8059162 ^designation[0].language = #de-AT 
* #8059162 ^designation[0].value = "CARBIDOPA MONOHYDRAT" 
* #8063882 "TEDIZOLIDPHOSPHAT"
* #8063882 ^designation[0].language = #de-AT 
* #8063882 ^designation[0].value = "TEDIZOLIDPHOSPHAT" 
* #8087094 "AFAMELANOTID"
* #8087094 ^designation[0].language = #de-AT 
* #8087094 ^designation[0].value = "AFAMELANOTID" 
* #8145253 "NEVIRAPIN WASSERFREI"
* #8145253 ^designation[0].language = #de-AT 
* #8145253 ^designation[0].value = "NEVIRAPIN WASSERFREI" 
* #8145255 "DASABUVIR NATRIUM MONOHYDRAT"
* #8145255 ^designation[0].language = #de-AT 
* #8145255 ^designation[0].value = "DASABUVIR NATRIUM MONOHYDRAT" 
* #8145260 "PARITAPREVIR"
* #8145260 ^designation[0].language = #de-AT 
* #8145260 ^designation[0].value = "PARITAPREVIR" 
* #8145263 "OMBITASVIR"
* #8145263 ^designation[0].language = #de-AT 
* #8145263 ^designation[0].value = "OMBITASVIR" 
* #8153013 "BENZOYLPEROXID, WASSERHALTIGES"
* #8153013 ^designation[0].language = #de-AT 
* #8153013 ^designation[0].value = "BENZOYLPEROXID, WASSERHALTIGES" 
* #8167177 "LENVATINIB MESILAT"
* #8167177 ^designation[0].language = #de-AT 
* #8167177 ^designation[0].value = "LENVATINIB MESILAT" 
* #8167179 "COCARBOXYLASE TETRAHYDRAT"
* #8167179 ^designation[0].language = #de-AT 
* #8167179 ^designation[0].value = "COCARBOXYLASE TETRAHYDRAT" 
* #8167181 "NATRIUM RIBOFLAVINPHOSPHAT DIHYDRAT"
* #8167181 ^designation[0].language = #de-AT 
* #8167181 ^designation[0].value = "NATRIUM RIBOFLAVINPHOSPHAT DIHYDRAT" 
* #8197838 "NIVOLUMAB"
* #8197838 ^designation[0].language = #de-AT 
* #8197838 ^designation[0].value = "NIVOLUMAB" 
* #8197852 "CERITINIB"
* #8197852 ^designation[0].language = #de-AT 
* #8197852 ^designation[0].value = "CERITINIB" 
* #8197854 "RAMUCIRUMAB"
* #8197854 ^designation[0].language = #de-AT 
* #8197854 ^designation[0].value = "RAMUCIRUMAB" 
* #8197858 "SECUKINUMAB"
* #8197858 ^designation[0].language = #de-AT 
* #8197858 ^designation[0].value = "SECUKINUMAB" 
* #8203118 "CALCIPOTRIOL MONOHYDRAT"
* #8203118 ^designation[0].language = #de-AT 
* #8203118 ^designation[0].value = "CALCIPOTRIOL MONOHYDRAT" 
* #8203146 "GLYCYL-L-GLUTAMIN MONOHYDRAT"
* #8203146 ^designation[0].language = #de-AT 
* #8203146 ^designation[0].value = "GLYCYL-L-GLUTAMIN MONOHYDRAT" 
* #8203148 "GLYCYLTYROSIN DIHYDRAT"
* #8203148 ^designation[0].language = #de-AT 
* #8203148 ^designation[0].value = "GLYCYLTYROSIN DIHYDRAT" 
* #8218958 "VANDETANIB"
* #8218958 ^designation[0].language = #de-AT 
* #8218958 ^designation[0].value = "VANDETANIB" 
* #8218962 "APREMILAST"
* #8218962 ^designation[0].language = #de-AT 
* #8218962 ^designation[0].value = "APREMILAST" 
* #8237813 "AZADIRACHTAE INDICAE FRUCTUS"
* #8237813 ^designation[0].language = #de-AT 
* #8237813 ^designation[0].value = "AZADIRACHTAE INDICAE FRUCTUS" 
* #8253065 "CALCIUMSULFAT HEMIHYDRAT"
* #8253065 ^designation[0].language = #de-AT 
* #8253065 ^designation[0].value = "CALCIUMSULFAT HEMIHYDRAT" 
* #8253067 "TENOFOVIR DISOPROXIL"
* #8253067 ^designation[0].language = #de-AT 
* #8253067 ^designation[0].value = "TENOFOVIR DISOPROXIL" 
* #8254734 "LISDEXAMFETAMIN DIMESYLAT"
* #8254734 ^designation[0].language = #de-AT 
* #8254734 ^designation[0].value = "LISDEXAMFETAMIN DIMESYLAT" 
* #8280011 "POLYGONI AVICULARIS HERBA"
* #8280011 ^designation[0].language = #de-AT 
* #8280011 ^designation[0].value = "POLYGONI AVICULARIS HERBA" 
* #8280078 "ASFOTASE ALFA"
* #8280078 ^designation[0].language = #de-AT 
* #8280078 ^designation[0].value = "ASFOTASE ALFA" 
* #8280082 "HUMANES PAPILLOMVIRUS-TYP 31 L1-PROTEIN"
* #8280082 ^designation[0].language = #de-AT 
* #8280082 ^designation[0].value = "HUMANES PAPILLOMVIRUS-TYP 31 L1-PROTEIN" 
* #8280087 "HUMANES PAPILLOMVIRUS-TYP 33 L1-PROTEIN"
* #8280087 ^designation[0].language = #de-AT 
* #8280087 ^designation[0].value = "HUMANES PAPILLOMVIRUS-TYP 33 L1-PROTEIN" 
* #8280097 "HUMANES PAPILLOMVIRUS-TYP 45 L1-PROTEIN"
* #8280097 ^designation[0].language = #de-AT 
* #8280097 ^designation[0].value = "HUMANES PAPILLOMVIRUS-TYP 45 L1-PROTEIN" 
* #8280099 "HUMANES PAPILLOMVIRUS-TYP 52 L1-PROTEIN"
* #8280099 ^designation[0].language = #de-AT 
* #8280099 ^designation[0].value = "HUMANES PAPILLOMVIRUS-TYP 52 L1-PROTEIN" 
* #8280113 "HUMANES PAPILLOMVIRUS-TYP 58 L1-PROTEIN"
* #8280113 ^designation[0].language = #de-AT 
* #8280113 ^designation[0].value = "HUMANES PAPILLOMVIRUS-TYP 58 L1-PROTEIN" 
* #8280115 "EVOLOCUMAB"
* #8280115 ^designation[0].language = #de-AT 
* #8280115 ^designation[0].value = "EVOLOCUMAB" 
* #8280122 "SONIDEGIBDIPHOSPHAT"
* #8280122 ^designation[0].language = #de-AT 
* #8280122 ^designation[0].value = "SONIDEGIBDIPHOSPHAT" 
* #8280126 "SEBELIPASE ALFA"
* #8280126 ^designation[0].language = #de-AT 
* #8280126 ^designation[0].value = "SEBELIPASE ALFA" 
* #8280128 "PANOBINOSTATLACTAT, WASSERFREI"
* #8280128 ^designation[0].language = #de-AT 
* #8280128 ^designation[0].value = "PANOBINOSTATLACTAT, WASSERFREI" 
* #8280130 "HORNHAUTEPITHELZELLEN"
* #8280130 ^designation[0].language = #de-AT 
* #8280130 ^designation[0].value = "HORNHAUTEPITHELZELLEN" 
* #8280144 "DALBAVANCIN"
* #8280144 ^designation[0].language = #de-AT 
* #8280144 ^designation[0].value = "DALBAVANCIN" 
* #8280150 "ELIGLUSTAT TARTRAT"
* #8280150 ^designation[0].language = #de-AT 
* #8280150 ^designation[0].value = "ELIGLUSTAT TARTRAT" 
* #8291814 "EDOXABANTOSILAT"
* #8291814 ^designation[0].language = #de-AT 
* #8291814 ^designation[0].value = "EDOXABANTOSILAT" 
* #8291829 "BETULA SPP. (ALLERG.)"
* #8291829 ^designation[0].language = #de-AT 
* #8291829 ^designation[0].value = "BETULA SPP. (ALLERG.)" 
* #8291832 "ETELCALCETID HYDROCHLORID"
* #8291832 ^designation[0].language = #de-AT 
* #8291832 ^designation[0].value = "ETELCALCETID HYDROCHLORID" 
* #8291838 "CEFTOLOZANSULFAT"
* #8291838 ^designation[0].language = #de-AT 
* #8291838 ^designation[0].value = "CEFTOLOZANSULFAT" 
* #8319083 "SAFINAMID MESILAT"
* #8319083 ^designation[0].language = #de-AT 
* #8319083 ^designation[0].value = "SAFINAMID MESILAT" 
* #8319085 "CEFTRIAXON DINATRIUM HEMIHEPTAHYDRAT"
* #8319085 ^designation[0].language = #de-AT 
* #8319085 ^designation[0].value = "CEFTRIAXON DINATRIUM HEMIHEPTAHYDRAT" 
* #8319087 "ALIROCUMAB"
* #8319087 ^designation[0].language = #de-AT 
* #8319087 ^designation[0].value = "ALIROCUMAB" 
* #8319089 "PEMBROLIZUMAB"
* #8319089 ^designation[0].language = #de-AT 
* #8319089 ^designation[0].value = "PEMBROLIZUMAB" 
* #8323113 "VARDENAFIL HYDROCHLORID TRIHYDRAT"
* #8323113 ^designation[0].language = #de-AT 
* #8323113 ^designation[0].value = "VARDENAFIL HYDROCHLORID TRIHYDRAT" 
* #8323119 "AMBROSIA ARTEMISIIFOLIA (ALLERG.)"
* #8323119 ^designation[0].language = #de-AT 
* #8323119 ^designation[0].value = "AMBROSIA ARTEMISIIFOLIA (ALLERG.)" 
* #8323145 "ISAVUCONAZONIUMSULFAT"
* #8323145 ^designation[0].language = #de-AT 
* #8323145 ^designation[0].value = "ISAVUCONAZONIUMSULFAT" 
* #8332638 "MYRTI AETHEROLEUM"
* #8332638 ^designation[0].language = #de-AT 
* #8332638 ^designation[0].value = "MYRTI AETHEROLEUM" 
* #8346812 "IVABRADIN OXALAT"
* #8346812 ^designation[0].language = #de-AT 
* #8346812 ^designation[0].value = "IVABRADIN OXALAT" 
* #8346818 "BECLOMETASONDIPROPIONAT, WASSERFREIES"
* #8346818 ^designation[0].language = #de-AT 
* #8346818 ^designation[0].value = "BECLOMETASONDIPROPIONAT, WASSERFREIES" 
* #8402589 "IDARUCIZUMAB"
* #8402589 ^designation[0].language = #de-AT 
* #8402589 ^designation[0].value = "IDARUCIZUMAB" 
* #8402605 "CARFILZOMIB"
* #8402605 ^designation[0].language = #de-AT 
* #8402605 ^designation[0].value = "CARFILZOMIB" 
* #8402607 "BLINATUMOMAB"
* #8402607 ^designation[0].language = #de-AT 
* #8402607 ^designation[0].value = "BLINATUMOMAB" 
* #8402609 "COBIMETINIB HEMIFUMARAT"
* #8402609 ^designation[0].language = #de-AT 
* #8402609 ^designation[0].value = "COBIMETINIB HEMIFUMARAT" 
* #8402611 "SACUBITRIL"
* #8402611 ^designation[0].language = #de-AT 
* #8402611 ^designation[0].value = "SACUBITRIL" 
* #8426998 "TRIENTIN TETRAHYDROCHLORID"
* #8426998 ^designation[0].language = #de-AT 
* #8426998 ^designation[0].value = "TRIENTIN TETRAHYDROCHLORID" 
* #8449384 "MEPOLIZUMAB"
* #8449384 ^designation[0].language = #de-AT 
* #8449384 ^designation[0].value = "MEPOLIZUMAB" 
* #8455600 "HERPES SIMPLEX VIRUS"
* #8455600 ^designation[0].language = #de-AT 
* #8455600 ^designation[0].value = "HERPES SIMPLEX VIRUS" 
* #8455605 "VALACICLOVIRHYDROCHLORID, WASSERHALTIGES"
* #8455605 ^designation[0].language = #de-AT 
* #8455605 ^designation[0].value = "VALACICLOVIRHYDROCHLORID, WASSERHALTIGES" 
* #8479244 "PEGASPARGASE"
* #8479244 ^designation[0].language = #de-AT 
* #8479244 ^designation[0].value = "PEGASPARGASE" 
* #8490401 "OSIMERTINIB MESYLAT"
* #8490401 ^designation[0].language = #de-AT 
* #8490401 ^designation[0].value = "OSIMERTINIB MESYLAT" 
* #8502021 "BRIVARACETAM"
* #8502021 ^designation[0].language = #de-AT 
* #8502021 ^designation[0].value = "BRIVARACETAM" 
* #8515771 "ALOES ARBORESCENTIS FOLIUM (AUSZUG)"
* #8515771 ^designation[0].language = #de-AT 
* #8515771 ^designation[0].value = "ALOES ARBORESCENTIS FOLIUM (AUSZUG)" 
* #8515789 "NECITUMUMAB"
* #8515789 ^designation[0].language = #de-AT 
* #8515789 ^designation[0].value = "NECITUMUMAB" 
* #8515791 "EISEN(III)-MALTOL"
* #8515791 ^designation[0].language = #de-AT 
* #8515791 ^designation[0].value = "EISEN(III)-MALTOL" 
* #8516719 "GLYCEROLPHENYLBUTYRAT"
* #8516719 ^designation[0].language = #de-AT 
* #8516719 ^designation[0].value = "GLYCEROLPHENYLBUTYRAT" 
* #8981656 "LESINURAD"
* #8981656 ^designation[0].language = #de-AT 
* #8981656 ^designation[0].value = "LESINURAD" 
* #8982870 "BUPIVACAIN"
* #8982870 ^designation[0].language = #de-AT 
* #8982870 ^designation[0].value = "BUPIVACAIN" 
* #9006424 "METHACETIN-C13"
* #9006424 ^designation[0].language = #de-AT 
* #9006424 ^designation[0].value = "METHACETIN-C13" 
* #9054892 "DANTROLEN NATRIUM HEMIHEPTAHYDRAT"
* #9054892 ^designation[0].language = #de-AT 
* #9054892 ^designation[0].value = "DANTROLEN NATRIUM HEMIHEPTAHYDRAT" 
* #9105958 "IXEKIZUMAB"
* #9105958 ^designation[0].language = #de-AT 
* #9105958 ^designation[0].value = "IXEKIZUMAB" 
* #9106828 "FEBUXOSTAT HEMIHYDRAT"
* #9106828 ^designation[0].language = #de-AT 
* #9106828 ^designation[0].value = "FEBUXOSTAT HEMIHYDRAT" 
* #9167278 "ELOTUZUMAB"
* #9167278 ^designation[0].language = #de-AT 
* #9167278 ^designation[0].value = "ELOTUZUMAB" 
* #9167280 "DARATUMUMAB"
* #9167280 ^designation[0].language = #de-AT 
* #9167280 ^designation[0].value = "DARATUMUMAB" 
* #9167282 "SELEXIPAG"
* #9167282 ^designation[0].language = #de-AT 
* #9167282 ^designation[0].value = "SELEXIPAG" 
* #9174239 "MIGALASTAT HYDROCHLORID"
* #9174239 ^designation[0].language = #de-AT 
* #9174239 ^designation[0].value = "MIGALASTAT HYDROCHLORID" 
* #9174245 "TIPIRACIL HYDROCHLORID"
* #9174245 ^designation[0].language = #de-AT 
* #9174245 ^designation[0].value = "TIPIRACIL HYDROCHLORID" 
* #9300338 "VELPATASVIR"
* #9300338 ^designation[0].language = #de-AT 
* #9300338 ^designation[0].value = "VELPATASVIR" 
* #9300361 "OPICAPON"
* #9300361 ^designation[0].language = #de-AT 
* #9300361 ^designation[0].value = "OPICAPON" 
* #9300365 "ENTECAVIR MONOHYDRAT"
* #9300365 ^designation[0].language = #de-AT 
* #9300365 ^designation[0].value = "ENTECAVIR MONOHYDRAT" 
* #9300369 "ELBASVIR"
* #9300369 ^designation[0].language = #de-AT 
* #9300369 ^designation[0].value = "ELBASVIR" 
* #9300373 "GRAZOPREVIR MONOHYDRAT"
* #9300373 ^designation[0].language = #de-AT 
* #9300373 ^designation[0].value = "GRAZOPREVIR MONOHYDRAT" 
* #9300376 "AVIBACTAM NATRIUM"
* #9300376 ^designation[0].language = #de-AT 
* #9300376 ^designation[0].value = "AVIBACTAM NATRIUM" 
* #9309929 "RESLIZUMAB"
* #9309929 ^designation[0].language = #de-AT 
* #9309929 ^designation[0].value = "RESLIZUMAB" 
* #9310029 "ORITAVANCIN DIPHOSPHAT"
* #9310029 ^designation[0].language = #de-AT 
* #9310029 ^designation[0].value = "ORITAVANCIN DIPHOSPHAT" 
* #9317318 "SECALE CERALE L. (AUSZUG)"
* #9317318 ^designation[0].language = #de-AT 
* #9317318 ^designation[0].value = "SECALE CERALE L. (AUSZUG)" 
* #9317321 "PHLEUM PRATENSE L. (AUSZUG)"
* #9317321 ^designation[0].language = #de-AT 
* #9317321 ^designation[0].value = "PHLEUM PRATENSE L. (AUSZUG)" 
* #9317323 "ZEA MAYS L. (AUSZUG)"
* #9317323 ^designation[0].language = #de-AT 
* #9317323 ^designation[0].value = "ZEA MAYS L. (AUSZUG)" 
* #9344247 "VELMANASE ALFA"
* #9344247 ^designation[0].language = #de-AT 
* #9344247 ^designation[0].value = "VELMANASE ALFA" 
* #9371629 "ARGIPRESSIN"
* #9371629 ^designation[0].language = #de-AT 
* #9371629 ^designation[0].value = "ARGIPRESSIN" 
* #9411652 "CELLULOSE, MIKROKRISTALLIN"
* #9411652 ^designation[0].language = #de-AT 
* #9411652 ^designation[0].value = "CELLULOSE, MIKROKRISTALLIN" 
* #9412042 "MORPHINHYDROCHLORID TRIHYDRAT"
* #9412042 ^designation[0].language = #de-AT 
* #9412042 ^designation[0].value = "MORPHINHYDROCHLORID TRIHYDRAT" 
* #9451037 "ELUXADOLIN"
* #9451037 ^designation[0].language = #de-AT 
* #9451037 ^designation[0].value = "ELUXADOLIN" 
* #9504846 "PALBOCICLIB"
* #9504846 ^designation[0].language = #de-AT 
* #9504846 ^designation[0].value = "PALBOCICLIB" 
* #9654037 "FOSAPREPITANT DIMEGLUMIN"
* #9654037 ^designation[0].language = #de-AT 
* #9654037 ^designation[0].value = "FOSAPREPITANT DIMEGLUMIN" 
* #9660582 "OLARATUMAB"
* #9660582 ^designation[0].language = #de-AT 
* #9660582 ^designation[0].value = "OLARATUMAB" 
* #9660633 "IXAZOMIB CITRAT"
* #9660633 ^designation[0].language = #de-AT 
* #9660633 ^designation[0].value = "IXAZOMIB CITRAT" 
* #9714468 "VENETOCLAX"
* #9714468 ^designation[0].language = #de-AT 
* #9714468 ^designation[0].value = "VENETOCLAX" 
* #9714470 "FOLLITROPIN DELTA"
* #9714470 ^designation[0].language = #de-AT 
* #9714470 ^designation[0].value = "FOLLITROPIN DELTA" 
* #9714472 "OBETICHOLSÄURE"
* #9714472 ^designation[0].language = #de-AT 
* #9714472 ^designation[0].value = "OBETICHOLSÄURE" 
* #9714475 "EDOTREOTID"
* #9714475 ^designation[0].language = #de-AT 
* #9714475 ^designation[0].value = "EDOTREOTID" 
* #9759934 "TENOFOVIRALAFENAMID FUMARAT"
* #9759934 ^designation[0].language = #de-AT 
* #9759934 ^designation[0].value = "TENOFOVIRALAFENAMID FUMARAT" 
* #9759936 "BEZLOTOXUMAB"
* #9759936 ^designation[0].language = #de-AT 
* #9759936 ^designation[0].value = "BEZLOTOXUMAB" 
* #9786300 "LAUROMACROGOL 400"
* #9786300 ^designation[0].language = #de-AT 
* #9786300 ^designation[0].value = "LAUROMACROGOL 400" 
* #9794716 "CAPLACIZUMAB"
* #9794716 ^designation[0].language = #de-AT 
* #9794716 ^designation[0].value = "CAPLACIZUMAB" 
* #9812622 "BARICITINIB"
* #9812622 ^designation[0].language = #de-AT 
* #9812622 ^designation[0].value = "BARICITINIB" 
* #9812658 "PRASUGREL HYDROBROMID"
* #9812658 ^designation[0].language = #de-AT 
* #9812658 ^designation[0].value = "PRASUGREL HYDROBROMID" 
* #9816074 "AGOMELATIN-CITRONENSÄURE"
* #9816074 ^designation[0].language = #de-AT 
* #9816074 ^designation[0].value = "AGOMELATIN-CITRONENSÄURE" 
* #9816080 "ALECTINIBHYDROCHLORID"
* #9816080 ^designation[0].language = #de-AT 
* #9816080 ^designation[0].value = "ALECTINIBHYDROCHLORID" 
* #9860362 "PRASUGREL BESILAT"
* #9860362 ^designation[0].language = #de-AT 
* #9860362 ^designation[0].value = "PRASUGREL BESILAT" 
* #9937735 "TOFACITINIBCITRAT"
* #9937735 ^designation[0].language = #de-AT 
* #9937735 ^designation[0].value = "TOFACITINIBCITRAT" 
* #9985816 "ROLAPITANT HYDROCHLORID MONOHYDRAT"
* #9985816 ^designation[0].language = #de-AT 
* #9985816 ^designation[0].value = "ROLAPITANT HYDROCHLORID MONOHYDRAT" 
