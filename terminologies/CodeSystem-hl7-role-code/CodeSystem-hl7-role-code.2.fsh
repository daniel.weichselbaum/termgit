Instance: hl7-role-code 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-role-code" 
* name = "hl7-role-code" 
* title = "HL7 Role Code" 
* status = #active 
* content = #complete 
* version = "HL7:RoleCode" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.5.111" 
* date = "2013-01-10" 
* count = 346 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #CLAIM "claimant"
* #GT "Guarantor"
* #PH "Policy Holder"
* #PROG "program eligible"
* #PT "Patient"
* #_AffiliationRoleType "AffiliationRoleType"
* #_AffiliationRoleType ^property[0].code = #child 
* #_AffiliationRoleType ^property[0].valueCode = #RESPRSN 
* #_AffiliationRoleType ^property[1].code = #child 
* #_AffiliationRoleType ^property[1].valueCode = #_CoverageSponsorRoleType 
* #_AffiliationRoleType ^property[2].code = #child 
* #_AffiliationRoleType ^property[2].valueCode = #_PayorRoleType 
* #RESPRSN "responsible party"
* #RESPRSN ^property[0].code = #parent 
* #RESPRSN ^property[0].valueCode = #_AffiliationRoleType 
* #RESPRSN ^property[1].code = #child 
* #RESPRSN ^property[1].valueCode = #EXCEST 
* #RESPRSN ^property[2].code = #child 
* #RESPRSN ^property[2].valueCode = #GUADLTM 
* #RESPRSN ^property[3].code = #child 
* #RESPRSN ^property[3].valueCode = #GUARD 
* #RESPRSN ^property[4].code = #child 
* #RESPRSN ^property[4].valueCode = #POWATT 
* #EXCEST "executor of estate"
* #EXCEST ^property[0].code = #parent 
* #EXCEST ^property[0].valueCode = #RESPRSN 
* #GUADLTM "guardian ad lidem"
* #GUADLTM ^property[0].code = #parent 
* #GUADLTM ^property[0].valueCode = #RESPRSN 
* #GUARD "guardian"
* #GUARD ^property[0].code = #parent 
* #GUARD ^property[0].valueCode = #RESPRSN 
* #POWATT "power of attorney"
* #POWATT ^property[0].code = #parent 
* #POWATT ^property[0].valueCode = #RESPRSN 
* #POWATT ^property[1].code = #child 
* #POWATT ^property[1].valueCode = #DPOWATT 
* #POWATT ^property[2].code = #child 
* #POWATT ^property[2].valueCode = #HPOWATT 
* #POWATT ^property[3].code = #child 
* #POWATT ^property[3].valueCode = #SPOWATT 
* #DPOWATT "durable power of attorney"
* #DPOWATT ^property[0].code = #parent 
* #DPOWATT ^property[0].valueCode = #POWATT 
* #HPOWATT "healthcare power of attorney"
* #HPOWATT ^property[0].code = #parent 
* #HPOWATT ^property[0].valueCode = #POWATT 
* #SPOWATT "special power of attorney"
* #SPOWATT ^property[0].code = #parent 
* #SPOWATT ^property[0].valueCode = #POWATT 
* #_CoverageSponsorRoleType "CoverageSponsorRoleType"
* #_CoverageSponsorRoleType ^property[0].code = #parent 
* #_CoverageSponsorRoleType ^property[0].valueCode = #_AffiliationRoleType 
* #_CoverageSponsorRoleType ^property[1].code = #child 
* #_CoverageSponsorRoleType ^property[1].valueCode = #FULLINS 
* #_CoverageSponsorRoleType ^property[2].code = #child 
* #_CoverageSponsorRoleType ^property[2].valueCode = #SELFINS 
* #FULLINS "Fully insured coverage sponsor"
* #FULLINS ^property[0].code = #parent 
* #FULLINS ^property[0].valueCode = #_CoverageSponsorRoleType 
* #SELFINS "Self insured coverage sponsor"
* #SELFINS ^property[0].code = #parent 
* #SELFINS ^property[0].valueCode = #_CoverageSponsorRoleType 
* #_PayorRoleType "PayorRoleType"
* #_PayorRoleType ^property[0].code = #parent 
* #_PayorRoleType ^property[0].valueCode = #_AffiliationRoleType 
* #_PayorRoleType ^property[1].code = #child 
* #_PayorRoleType ^property[1].valueCode = #ENROLBKR 
* #_PayorRoleType ^property[2].code = #child 
* #_PayorRoleType ^property[2].valueCode = #TPA 
* #_PayorRoleType ^property[3].code = #child 
* #_PayorRoleType ^property[3].valueCode = #UMO 
* #ENROLBKR "Enrollment Broker"
* #ENROLBKR ^property[0].code = #parent 
* #ENROLBKR ^property[0].valueCode = #_PayorRoleType 
* #TPA "Third party administrator"
* #TPA ^property[0].code = #parent 
* #TPA ^property[0].valueCode = #_PayorRoleType 
* #UMO "Utilization management organization"
* #UMO ^property[0].code = #parent 
* #UMO ^property[0].valueCode = #_PayorRoleType 
* #_AssignedRoleType "AssignedRoleType"
* #_AssignedRoleType ^property[0].code = #child 
* #_AssignedRoleType ^property[0].valueCode = #_AssignedNonPersonLivingSubjectRoleType 
* #_AssignedNonPersonLivingSubjectRoleType "AssignedNonPersonLivingSubjectRoleType"
* #_AssignedNonPersonLivingSubjectRoleType ^property[0].code = #parent 
* #_AssignedNonPersonLivingSubjectRoleType ^property[0].valueCode = #_AssignedRoleType 
* #_AssignedNonPersonLivingSubjectRoleType ^property[1].code = #child 
* #_AssignedNonPersonLivingSubjectRoleType ^property[1].valueCode = #ASSIST 
* #_AssignedNonPersonLivingSubjectRoleType ^property[2].code = #child 
* #_AssignedNonPersonLivingSubjectRoleType ^property[2].valueCode = #BIOTH 
* #_AssignedNonPersonLivingSubjectRoleType ^property[3].code = #child 
* #_AssignedNonPersonLivingSubjectRoleType ^property[3].valueCode = #CCO 
* #_AssignedNonPersonLivingSubjectRoleType ^property[4].code = #child 
* #_AssignedNonPersonLivingSubjectRoleType ^property[4].valueCode = #SEE 
* #_AssignedNonPersonLivingSubjectRoleType ^property[5].code = #child 
* #_AssignedNonPersonLivingSubjectRoleType ^property[5].valueCode = #SNIFF 
* #ASSIST "Assistive non-person living subject"
* #ASSIST ^property[0].code = #parent 
* #ASSIST ^property[0].valueCode = #_AssignedNonPersonLivingSubjectRoleType 
* #BIOTH "Biotherapeutic non-person living subject"
* #BIOTH ^property[0].code = #parent 
* #BIOTH ^property[0].valueCode = #_AssignedNonPersonLivingSubjectRoleType 
* #BIOTH ^property[1].code = #child 
* #BIOTH ^property[1].valueCode = #ANTIBIOT 
* #BIOTH ^property[2].code = #child 
* #BIOTH ^property[2].valueCode = #DEBR 
* #ANTIBIOT "Antibiotic"
* #ANTIBIOT ^property[0].code = #parent 
* #ANTIBIOT ^property[0].valueCode = #BIOTH 
* #DEBR "Debridement"
* #DEBR ^property[0].code = #parent 
* #DEBR ^property[0].valueCode = #BIOTH 
* #CCO "Clinical Companion"
* #CCO ^property[0].code = #parent 
* #CCO ^property[0].valueCode = #_AssignedNonPersonLivingSubjectRoleType 
* #SEE "Seeing"
* #SEE ^property[0].code = #parent 
* #SEE ^property[0].valueCode = #_AssignedNonPersonLivingSubjectRoleType 
* #SNIFF "Sniffing"
* #SNIFF ^property[0].code = #parent 
* #SNIFF ^property[0].valueCode = #_AssignedNonPersonLivingSubjectRoleType 
* #_CertifiedEntityType "CertifiedEntityType"
* #_CitizenRoleType "CitizenRoleType"
* #_CitizenRoleType ^property[0].code = #child 
* #_CitizenRoleType ^property[0].valueCode = #CAS 
* #_CitizenRoleType ^property[1].code = #child 
* #_CitizenRoleType ^property[1].valueCode = #CN 
* #_CitizenRoleType ^property[2].code = #child 
* #_CitizenRoleType ^property[2].valueCode = #CNRP 
* #_CitizenRoleType ^property[3].code = #child 
* #_CitizenRoleType ^property[3].valueCode = #CPCA 
* #_CitizenRoleType ^property[4].code = #child 
* #_CitizenRoleType ^property[4].valueCode = #CRP 
* #CAS "asylum seeker"
* #CAS ^property[0].code = #parent 
* #CAS ^property[0].valueCode = #_CitizenRoleType 
* #CAS ^property[1].code = #child 
* #CAS ^property[1].valueCode = #CASM 
* #CASM "single minor asylum seeker"
* #CASM ^property[0].code = #parent 
* #CASM ^property[0].valueCode = #CAS 
* #CN "national"
* #CN ^property[0].code = #parent 
* #CN ^property[0].valueCode = #_CitizenRoleType 
* #CNRP "non-country member without residence permit"
* #CNRP ^property[0].code = #parent 
* #CNRP ^property[0].valueCode = #_CitizenRoleType 
* #CNRP ^property[1].code = #child 
* #CNRP ^property[1].valueCode = #CNRPM 
* #CNRPM "non-country member minor without residence permit"
* #CNRPM ^property[0].code = #parent 
* #CNRPM ^property[0].valueCode = #CNRP 
* #CPCA "permit card applicant"
* #CPCA ^property[0].code = #parent 
* #CPCA ^property[0].valueCode = #_CitizenRoleType 
* #CRP "non-country member with residence permit"
* #CRP ^property[0].code = #parent 
* #CRP ^property[0].valueCode = #_CitizenRoleType 
* #CRP ^property[1].code = #child 
* #CRP ^property[1].valueCode = #CRPM 
* #CRPM "non-country member minor with residence permit"
* #CRPM ^property[0].code = #parent 
* #CRPM ^property[0].valueCode = #CRP 
* #_ContactRoleType "ContactRoleType"
* #_ContactRoleType ^property[0].code = #child 
* #_ContactRoleType ^property[0].valueCode = #ECON 
* #_ContactRoleType ^property[1].code = #child 
* #_ContactRoleType ^property[1].valueCode = #NOK 
* #_ContactRoleType ^property[2].code = #child 
* #_ContactRoleType ^property[2].valueCode = #_AdministrativeContactRoleType 
* #ECON "emergency contact"
* #ECON ^property[0].code = #parent 
* #ECON ^property[0].valueCode = #_ContactRoleType 
* #NOK "next of kin"
* #NOK ^property[0].code = #parent 
* #NOK ^property[0].valueCode = #_ContactRoleType 
* #_AdministrativeContactRoleType "AdministrativeContactRoleType"
* #_AdministrativeContactRoleType ^property[0].code = #parent 
* #_AdministrativeContactRoleType ^property[0].valueCode = #_ContactRoleType 
* #_AdministrativeContactRoleType ^property[1].code = #child 
* #_AdministrativeContactRoleType ^property[1].valueCode = #BILL 
* #_AdministrativeContactRoleType ^property[2].code = #child 
* #_AdministrativeContactRoleType ^property[2].valueCode = #ORG 
* #_AdministrativeContactRoleType ^property[3].code = #child 
* #_AdministrativeContactRoleType ^property[3].valueCode = #PAYOR 
* #BILL "Billing Contact"
* #BILL ^property[0].code = #parent 
* #BILL ^property[0].valueCode = #_AdministrativeContactRoleType 
* #ORG "organizational contact"
* #ORG ^property[0].code = #parent 
* #ORG ^property[0].valueCode = #_AdministrativeContactRoleType 
* #PAYOR "Payor Contact"
* #PAYOR ^property[0].code = #parent 
* #PAYOR ^property[0].valueCode = #_AdministrativeContactRoleType 
* #_IdentifiedEntityType "IdentifiedEntityType"
* #_IdentifiedEntityType ^property[0].code = #child 
* #_IdentifiedEntityType ^property[0].valueCode = #_LocationIdentifiedEntityRoleCode 
* #_LocationIdentifiedEntityRoleCode "LocationIdentifiedEntityRoleCode"
* #_LocationIdentifiedEntityRoleCode ^property[0].code = #parent 
* #_LocationIdentifiedEntityRoleCode ^property[0].valueCode = #_IdentifiedEntityType 
* #_LocationIdentifiedEntityRoleCode ^property[1].code = #child 
* #_LocationIdentifiedEntityRoleCode ^property[1].valueCode = #ACHFID 
* #_LocationIdentifiedEntityRoleCode ^property[2].code = #child 
* #_LocationIdentifiedEntityRoleCode ^property[2].valueCode = #JURID 
* #_LocationIdentifiedEntityRoleCode ^property[3].code = #child 
* #_LocationIdentifiedEntityRoleCode ^property[3].valueCode = #LOCHFID 
* #ACHFID "accreditation identifier"
* #ACHFID ^property[0].code = #parent 
* #ACHFID ^property[0].valueCode = #_LocationIdentifiedEntityRoleCode 
* #JURID "jurisdiction facility identifier"
* #JURID ^property[0].code = #parent 
* #JURID ^property[0].valueCode = #_LocationIdentifiedEntityRoleCode 
* #LOCHFID "local facility identifier"
* #LOCHFID ^property[0].code = #parent 
* #LOCHFID ^property[0].valueCode = #_LocationIdentifiedEntityRoleCode 
* #_LivingSubjectProductionClass "LivingSubjectProductionClass"
* #_LivingSubjectProductionClass ^property[0].code = #child 
* #_LivingSubjectProductionClass ^property[0].valueCode = #BF 
* #_LivingSubjectProductionClass ^property[1].code = #child 
* #_LivingSubjectProductionClass ^property[1].valueCode = #BL 
* #_LivingSubjectProductionClass ^property[2].code = #child 
* #_LivingSubjectProductionClass ^property[2].valueCode = #BR 
* #_LivingSubjectProductionClass ^property[3].code = #child 
* #_LivingSubjectProductionClass ^property[3].valueCode = #CO 
* #_LivingSubjectProductionClass ^property[4].code = #child 
* #_LivingSubjectProductionClass ^property[4].valueCode = #DA 
* #_LivingSubjectProductionClass ^property[5].code = #child 
* #_LivingSubjectProductionClass ^property[5].valueCode = #DR 
* #_LivingSubjectProductionClass ^property[6].code = #child 
* #_LivingSubjectProductionClass ^property[6].valueCode = #DU 
* #_LivingSubjectProductionClass ^property[7].code = #child 
* #_LivingSubjectProductionClass ^property[7].valueCode = #FI 
* #_LivingSubjectProductionClass ^property[8].code = #child 
* #_LivingSubjectProductionClass ^property[8].valueCode = #LY 
* #_LivingSubjectProductionClass ^property[9].code = #child 
* #_LivingSubjectProductionClass ^property[9].valueCode = #MT 
* #_LivingSubjectProductionClass ^property[10].code = #child 
* #_LivingSubjectProductionClass ^property[10].valueCode = #MU 
* #_LivingSubjectProductionClass ^property[11].code = #child 
* #_LivingSubjectProductionClass ^property[11].valueCode = #PL 
* #_LivingSubjectProductionClass ^property[12].code = #child 
* #_LivingSubjectProductionClass ^property[12].valueCode = #RC 
* #_LivingSubjectProductionClass ^property[13].code = #child 
* #_LivingSubjectProductionClass ^property[13].valueCode = #SH 
* #_LivingSubjectProductionClass ^property[14].code = #child 
* #_LivingSubjectProductionClass ^property[14].valueCode = #VL 
* #_LivingSubjectProductionClass ^property[15].code = #child 
* #_LivingSubjectProductionClass ^property[15].valueCode = #WL 
* #_LivingSubjectProductionClass ^property[16].code = #child 
* #_LivingSubjectProductionClass ^property[16].valueCode = #WO 
* #BF "Beef"
* #BF ^property[0].code = #parent 
* #BF ^property[0].valueCode = #_LivingSubjectProductionClass 
* #BL "Broiler"
* #BL ^property[0].code = #parent 
* #BL ^property[0].valueCode = #_LivingSubjectProductionClass 
* #BR "Breeder"
* #BR ^property[0].code = #parent 
* #BR ^property[0].valueCode = #_LivingSubjectProductionClass 
* #CO "Companion"
* #CO ^property[0].code = #parent 
* #CO ^property[0].valueCode = #_LivingSubjectProductionClass 
* #DA "Dairy"
* #DA ^property[0].code = #parent 
* #DA ^property[0].valueCode = #_LivingSubjectProductionClass 
* #DR "Draft"
* #DR ^property[0].code = #parent 
* #DR ^property[0].valueCode = #_LivingSubjectProductionClass 
* #DU "Dual"
* #DU ^property[0].code = #parent 
* #DU ^property[0].valueCode = #_LivingSubjectProductionClass 
* #FI "Fiber"
* #FI ^property[0].code = #parent 
* #FI ^property[0].valueCode = #_LivingSubjectProductionClass 
* #LY "Layer"
* #LY ^property[0].code = #parent 
* #LY ^property[0].valueCode = #_LivingSubjectProductionClass 
* #MT "Meat"
* #MT ^property[0].code = #parent 
* #MT ^property[0].valueCode = #_LivingSubjectProductionClass 
* #MU "Multiplier"
* #MU ^property[0].code = #parent 
* #MU ^property[0].valueCode = #_LivingSubjectProductionClass 
* #PL "Pleasure"
* #PL ^property[0].code = #parent 
* #PL ^property[0].valueCode = #_LivingSubjectProductionClass 
* #RC "Racing"
* #RC ^property[0].code = #parent 
* #RC ^property[0].valueCode = #_LivingSubjectProductionClass 
* #SH "Show"
* #SH ^property[0].code = #parent 
* #SH ^property[0].valueCode = #_LivingSubjectProductionClass 
* #VL "Veal"
* #VL ^property[0].code = #parent 
* #VL ^property[0].valueCode = #_LivingSubjectProductionClass 
* #WL "Wool"
* #WL ^property[0].code = #parent 
* #WL ^property[0].valueCode = #_LivingSubjectProductionClass 
* #WO "Working"
* #WO ^property[0].code = #parent 
* #WO ^property[0].valueCode = #_LivingSubjectProductionClass 
* #_MedicationGeneralizationRoleType "MedicationGeneralizationRoleType"
* #_MedicationGeneralizationRoleType ^property[0].code = #child 
* #_MedicationGeneralizationRoleType ^property[0].valueCode = #DC 
* #_MedicationGeneralizationRoleType ^property[1].code = #child 
* #_MedicationGeneralizationRoleType ^property[1].valueCode = #GD 
* #_MedicationGeneralizationRoleType ^property[2].code = #child 
* #_MedicationGeneralizationRoleType ^property[2].valueCode = #MGDSF 
* #DC "therapeutic class"
* #DC ^property[0].code = #parent 
* #DC ^property[0].valueCode = #_MedicationGeneralizationRoleType 
* #GD "generic drug"
* #GD ^property[0].code = #parent 
* #GD ^property[0].valueCode = #_MedicationGeneralizationRoleType 
* #GD ^property[1].code = #child 
* #GD ^property[1].valueCode = #GDF 
* #GD ^property[2].code = #child 
* #GD ^property[2].valueCode = #GDS 
* #GD ^property[3].code = #child 
* #GD ^property[3].valueCode = #GDSF 
* #GDF "generic drug form"
* #GDF ^property[0].code = #parent 
* #GDF ^property[0].valueCode = #GD 
* #GDS "generic drug strength"
* #GDS ^property[0].code = #parent 
* #GDS ^property[0].valueCode = #GD 
* #GDSF "generic drug strength form"
* #GDSF ^property[0].code = #parent 
* #GDSF ^property[0].valueCode = #GD 
* #MGDSF "manufactured drug strength form"
* #MGDSF ^property[0].code = #parent 
* #MGDSF ^property[0].valueCode = #_MedicationGeneralizationRoleType 
* #_MemberRoleType "MemberRoleType"
* #_MemberRoleType ^property[0].code = #child 
* #_MemberRoleType ^property[0].valueCode = #TRB 
* #TRB "Tribal Member"
* #TRB ^property[0].code = #parent 
* #TRB ^property[0].valueCode = #_MemberRoleType 
* #_PersonalRelationshipRoleType "PersonalRelationshipRoleType"
* #_PersonalRelationshipRoleType ^property[0].code = #child 
* #_PersonalRelationshipRoleType ^property[0].valueCode = #FAMMEMB 
* #_PersonalRelationshipRoleType ^property[1].code = #child 
* #_PersonalRelationshipRoleType ^property[1].valueCode = #FRND 
* #_PersonalRelationshipRoleType ^property[2].code = #child 
* #_PersonalRelationshipRoleType ^property[2].valueCode = #NBOR 
* #_PersonalRelationshipRoleType ^property[3].code = #child 
* #_PersonalRelationshipRoleType ^property[3].valueCode = #ROOM 
* #FAMMEMB "Family Member"
* #FAMMEMB ^property[0].code = #parent 
* #FAMMEMB ^property[0].valueCode = #_PersonalRelationshipRoleType 
* #FAMMEMB ^property[1].code = #child 
* #FAMMEMB ^property[1].valueCode = #CHILD 
* #FAMMEMB ^property[2].code = #child 
* #FAMMEMB ^property[2].valueCode = #EXT 
* #FAMMEMB ^property[3].code = #child 
* #FAMMEMB ^property[3].valueCode = #PRN 
* #FAMMEMB ^property[4].code = #child 
* #FAMMEMB ^property[4].valueCode = #SIB 
* #FAMMEMB ^property[5].code = #child 
* #FAMMEMB ^property[5].valueCode = #SIGOTHR 
* #CHILD "Child"
* #CHILD ^property[0].code = #parent 
* #CHILD ^property[0].valueCode = #FAMMEMB 
* #CHILD ^property[1].code = #child 
* #CHILD ^property[1].valueCode = #CHLDADOPT 
* #CHILD ^property[2].code = #child 
* #CHILD ^property[2].valueCode = #CHLDFOST 
* #CHILD ^property[3].code = #child 
* #CHILD ^property[3].valueCode = #CHLDINLAW 
* #CHILD ^property[4].code = #child 
* #CHILD ^property[4].valueCode = #DAUC 
* #CHILD ^property[5].code = #child 
* #CHILD ^property[5].valueCode = #NCHILD 
* #CHILD ^property[6].code = #child 
* #CHILD ^property[6].valueCode = #SONC 
* #CHILD ^property[7].code = #child 
* #CHILD ^property[7].valueCode = #STPCHLD 
* #CHLDADOPT "adopted child"
* #CHLDADOPT ^property[0].code = #parent 
* #CHLDADOPT ^property[0].valueCode = #CHILD 
* #CHLDADOPT ^property[1].code = #child 
* #CHLDADOPT ^property[1].valueCode = #DAUADOPT 
* #CHLDADOPT ^property[2].code = #child 
* #CHLDADOPT ^property[2].valueCode = #SONADOPT 
* #DAUADOPT "adopted daughter"
* #DAUADOPT ^property[0].code = #parent 
* #DAUADOPT ^property[0].valueCode = #CHLDADOPT 
* #SONADOPT "adopted son"
* #SONADOPT ^property[0].code = #parent 
* #SONADOPT ^property[0].valueCode = #CHLDADOPT 
* #CHLDFOST "foster child"
* #CHLDFOST ^property[0].code = #parent 
* #CHLDFOST ^property[0].valueCode = #CHILD 
* #CHLDFOST ^property[1].code = #child 
* #CHLDFOST ^property[1].valueCode = #DAUFOST 
* #CHLDFOST ^property[2].code = #child 
* #CHLDFOST ^property[2].valueCode = #SONFOST 
* #DAUFOST "foster daughter"
* #DAUFOST ^property[0].code = #parent 
* #DAUFOST ^property[0].valueCode = #CHLDFOST 
* #SONFOST "foster son"
* #SONFOST ^property[0].code = #parent 
* #SONFOST ^property[0].valueCode = #CHLDFOST 
* #CHLDINLAW "child in-law"
* #CHLDINLAW ^property[0].code = #parent 
* #CHLDINLAW ^property[0].valueCode = #CHILD 
* #CHLDINLAW ^property[1].code = #child 
* #CHLDINLAW ^property[1].valueCode = #DAUINLAW 
* #CHLDINLAW ^property[2].code = #child 
* #CHLDINLAW ^property[2].valueCode = #SONINLAW 
* #DAUINLAW "daughter in-law"
* #DAUINLAW ^property[0].code = #parent 
* #DAUINLAW ^property[0].valueCode = #CHLDINLAW 
* #SONINLAW "son in-law"
* #SONINLAW ^property[0].code = #parent 
* #SONINLAW ^property[0].valueCode = #CHLDINLAW 
* #DAUC "Daughter"
* #DAUC ^property[0].code = #parent 
* #DAUC ^property[0].valueCode = #CHILD 
* #DAUC ^property[1].code = #child 
* #DAUC ^property[1].valueCode = #DAU 
* #DAUC ^property[2].code = #child 
* #DAUC ^property[2].valueCode = #STPDAU 
* #DAU "natural daughter"
* #DAU ^property[0].code = #parent 
* #DAU ^property[0].valueCode = #DAUC 
* #STPDAU "stepdaughter"
* #STPDAU ^property[0].code = #parent 
* #STPDAU ^property[0].valueCode = #DAUC 
* #NCHILD "natural child"
* #NCHILD ^property[0].code = #parent 
* #NCHILD ^property[0].valueCode = #CHILD 
* #NCHILD ^property[1].code = #child 
* #NCHILD ^property[1].valueCode = #SON 
* #SON "natural son"
* #SON ^property[0].code = #parent 
* #SON ^property[0].valueCode = #NCHILD 
* #SONC "son"
* #SONC ^property[0].code = #parent 
* #SONC ^property[0].valueCode = #CHILD 
* #SONC ^property[1].code = #child 
* #SONC ^property[1].valueCode = #STPSON 
* #STPSON "stepson"
* #STPSON ^property[0].code = #parent 
* #STPSON ^property[0].valueCode = #SONC 
* #STPCHLD "step child"
* #STPCHLD ^property[0].code = #parent 
* #STPCHLD ^property[0].valueCode = #CHILD 
* #EXT "extended family member"
* #EXT ^property[0].code = #parent 
* #EXT ^property[0].valueCode = #FAMMEMB 
* #EXT ^property[1].code = #child 
* #EXT ^property[1].valueCode = #AUNT 
* #EXT ^property[2].code = #child 
* #EXT ^property[2].valueCode = #COUSN 
* #EXT ^property[3].code = #child 
* #EXT ^property[3].valueCode = #GGRPRN 
* #EXT ^property[4].code = #child 
* #EXT ^property[4].valueCode = #GRNDCHILD 
* #EXT ^property[5].code = #child 
* #EXT ^property[5].valueCode = #GRPRN 
* #EXT ^property[6].code = #child 
* #EXT ^property[6].valueCode = #NIENEPH 
* #EXT ^property[7].code = #child 
* #EXT ^property[7].valueCode = #UNCLE 
* #AUNT "aunt"
* #AUNT ^property[0].code = #parent 
* #AUNT ^property[0].valueCode = #EXT 
* #AUNT ^property[1].code = #child 
* #AUNT ^property[1].valueCode = #MAUNT 
* #AUNT ^property[2].code = #child 
* #AUNT ^property[2].valueCode = #PAUNT 
* #MAUNT "MaternalAunt"
* #MAUNT ^property[0].code = #parent 
* #MAUNT ^property[0].valueCode = #AUNT 
* #PAUNT "PaternalAunt"
* #PAUNT ^property[0].code = #parent 
* #PAUNT ^property[0].valueCode = #AUNT 
* #COUSN "cousin"
* #COUSN ^property[0].code = #parent 
* #COUSN ^property[0].valueCode = #EXT 
* #COUSN ^property[1].code = #child 
* #COUSN ^property[1].valueCode = #MCOUSN 
* #COUSN ^property[2].code = #child 
* #COUSN ^property[2].valueCode = #PCOUSN 
* #MCOUSN "MaternalCousin"
* #MCOUSN ^property[0].code = #parent 
* #MCOUSN ^property[0].valueCode = #COUSN 
* #PCOUSN "PaternalCousin"
* #PCOUSN ^property[0].code = #parent 
* #PCOUSN ^property[0].valueCode = #COUSN 
* #GGRPRN "great grandparent"
* #GGRPRN ^property[0].code = #parent 
* #GGRPRN ^property[0].valueCode = #EXT 
* #GGRPRN ^property[1].code = #child 
* #GGRPRN ^property[1].valueCode = #GGRFTH 
* #GGRPRN ^property[2].code = #child 
* #GGRPRN ^property[2].valueCode = #GGRMTH 
* #GGRPRN ^property[3].code = #child 
* #GGRPRN ^property[3].valueCode = #MGGRFTH 
* #GGRPRN ^property[4].code = #child 
* #GGRPRN ^property[4].valueCode = #MGGRMTH 
* #GGRPRN ^property[5].code = #child 
* #GGRPRN ^property[5].valueCode = #MGGRPRN 
* #GGRPRN ^property[6].code = #child 
* #GGRPRN ^property[6].valueCode = #PGGRFTH 
* #GGRPRN ^property[7].code = #child 
* #GGRPRN ^property[7].valueCode = #PGGRMTH 
* #GGRPRN ^property[8].code = #child 
* #GGRPRN ^property[8].valueCode = #PGGRPRN 
* #GGRFTH "great grandfather"
* #GGRFTH ^property[0].code = #parent 
* #GGRFTH ^property[0].valueCode = #GGRPRN 
* #GGRMTH "great grandmother"
* #GGRMTH ^property[0].code = #parent 
* #GGRMTH ^property[0].valueCode = #GGRPRN 
* #MGGRFTH "MaternalGreatgrandfather"
* #MGGRFTH ^property[0].code = #parent 
* #MGGRFTH ^property[0].valueCode = #GGRPRN 
* #MGGRMTH "MaternalGreatgrandmother"
* #MGGRMTH ^property[0].code = #parent 
* #MGGRMTH ^property[0].valueCode = #GGRPRN 
* #MGGRPRN "MaternalGreatgrandparent"
* #MGGRPRN ^property[0].code = #parent 
* #MGGRPRN ^property[0].valueCode = #GGRPRN 
* #PGGRFTH "PaternalGreatgrandfather"
* #PGGRFTH ^property[0].code = #parent 
* #PGGRFTH ^property[0].valueCode = #GGRPRN 
* #PGGRMTH "PaternalGreatgrandmother"
* #PGGRMTH ^property[0].code = #parent 
* #PGGRMTH ^property[0].valueCode = #GGRPRN 
* #PGGRPRN "PaternalGreatgrandparent"
* #PGGRPRN ^property[0].code = #parent 
* #PGGRPRN ^property[0].valueCode = #GGRPRN 
* #GRNDCHILD "grandchild"
* #GRNDCHILD ^property[0].code = #parent 
* #GRNDCHILD ^property[0].valueCode = #EXT 
* #GRNDCHILD ^property[1].code = #child 
* #GRNDCHILD ^property[1].valueCode = #GRNDDAU 
* #GRNDCHILD ^property[2].code = #child 
* #GRNDCHILD ^property[2].valueCode = #GRNDSON 
* #GRNDDAU "granddaughter"
* #GRNDDAU ^property[0].code = #parent 
* #GRNDDAU ^property[0].valueCode = #GRNDCHILD 
* #GRNDSON "grandson"
* #GRNDSON ^property[0].code = #parent 
* #GRNDSON ^property[0].valueCode = #GRNDCHILD 
* #GRPRN "Grandparent"
* #GRPRN ^property[0].code = #parent 
* #GRPRN ^property[0].valueCode = #EXT 
* #GRPRN ^property[1].code = #child 
* #GRPRN ^property[1].valueCode = #GRFTH 
* #GRPRN ^property[2].code = #child 
* #GRPRN ^property[2].valueCode = #GRMTH 
* #GRPRN ^property[3].code = #child 
* #GRPRN ^property[3].valueCode = #MGRFTH 
* #GRPRN ^property[4].code = #child 
* #GRPRN ^property[4].valueCode = #MGRMTH 
* #GRPRN ^property[5].code = #child 
* #GRPRN ^property[5].valueCode = #MGRPRN 
* #GRPRN ^property[6].code = #child 
* #GRPRN ^property[6].valueCode = #PGRFTH 
* #GRPRN ^property[7].code = #child 
* #GRPRN ^property[7].valueCode = #PGRMTH 
* #GRPRN ^property[8].code = #child 
* #GRPRN ^property[8].valueCode = #PGRPRN 
* #GRFTH "Grandfather"
* #GRFTH ^property[0].code = #parent 
* #GRFTH ^property[0].valueCode = #GRPRN 
* #GRMTH "Grandmother"
* #GRMTH ^property[0].code = #parent 
* #GRMTH ^property[0].valueCode = #GRPRN 
* #MGRFTH "MaternalGrandfather"
* #MGRFTH ^property[0].code = #parent 
* #MGRFTH ^property[0].valueCode = #GRPRN 
* #MGRMTH "MaternalGrandmother"
* #MGRMTH ^property[0].code = #parent 
* #MGRMTH ^property[0].valueCode = #GRPRN 
* #MGRPRN "MaternalGrandparent"
* #MGRPRN ^property[0].code = #parent 
* #MGRPRN ^property[0].valueCode = #GRPRN 
* #PGRFTH "PaternalGrandfather"
* #PGRFTH ^property[0].code = #parent 
* #PGRFTH ^property[0].valueCode = #GRPRN 
* #PGRMTH "PaternalGrandmother"
* #PGRMTH ^property[0].code = #parent 
* #PGRMTH ^property[0].valueCode = #GRPRN 
* #PGRPRN "PaternalGrandparent"
* #PGRPRN ^property[0].code = #parent 
* #PGRPRN ^property[0].valueCode = #GRPRN 
* #NIENEPH "niece/nephew"
* #NIENEPH ^property[0].code = #parent 
* #NIENEPH ^property[0].valueCode = #EXT 
* #NIENEPH ^property[1].code = #child 
* #NIENEPH ^property[1].valueCode = #NEPHEW 
* #NIENEPH ^property[2].code = #child 
* #NIENEPH ^property[2].valueCode = #NIECE 
* #NEPHEW "nephew"
* #NEPHEW ^property[0].code = #parent 
* #NEPHEW ^property[0].valueCode = #NIENEPH 
* #NIECE "niece"
* #NIECE ^property[0].code = #parent 
* #NIECE ^property[0].valueCode = #NIENEPH 
* #UNCLE "uncle"
* #UNCLE ^property[0].code = #parent 
* #UNCLE ^property[0].valueCode = #EXT 
* #UNCLE ^property[1].code = #child 
* #UNCLE ^property[1].valueCode = #MUNCLE 
* #UNCLE ^property[2].code = #child 
* #UNCLE ^property[2].valueCode = #PUNCLE 
* #MUNCLE "MaternalUncle"
* #MUNCLE ^property[0].code = #parent 
* #MUNCLE ^property[0].valueCode = #UNCLE 
* #PUNCLE "PaternalUncle"
* #PUNCLE ^property[0].code = #parent 
* #PUNCLE ^property[0].valueCode = #UNCLE 
* #PRN "Parent"
* #PRN ^property[0].code = #parent 
* #PRN ^property[0].valueCode = #FAMMEMB 
* #PRN ^property[1].code = #child 
* #PRN ^property[1].valueCode = #FTH 
* #PRN ^property[2].code = #child 
* #PRN ^property[2].valueCode = #MTH 
* #PRN ^property[3].code = #child 
* #PRN ^property[3].valueCode = #NPRN 
* #PRN ^property[4].code = #child 
* #PRN ^property[4].valueCode = #PRNINLAW 
* #PRN ^property[5].code = #child 
* #PRN ^property[5].valueCode = #STPPRN 
* #FTH "Father"
* #FTH ^property[0].code = #parent 
* #FTH ^property[0].valueCode = #PRN 
* #MTH "Mother"
* #MTH ^property[0].code = #parent 
* #MTH ^property[0].valueCode = #PRN 
* #NPRN "natural parent"
* #NPRN ^property[0].code = #parent 
* #NPRN ^property[0].valueCode = #PRN 
* #NPRN ^property[1].code = #child 
* #NPRN ^property[1].valueCode = #NFTH 
* #NPRN ^property[2].code = #child 
* #NPRN ^property[2].valueCode = #NMTH 
* #NFTH "natural father"
* #NFTH ^property[0].code = #parent 
* #NFTH ^property[0].valueCode = #NPRN 
* #NFTH ^property[1].code = #child 
* #NFTH ^property[1].valueCode = #NFTHF 
* #NFTHF "natural father of fetus"
* #NFTHF ^property[0].code = #parent 
* #NFTHF ^property[0].valueCode = #NFTH 
* #NMTH "natural mother"
* #NMTH ^property[0].code = #parent 
* #NMTH ^property[0].valueCode = #NPRN 
* #PRNINLAW "parent in-law"
* #PRNINLAW ^property[0].code = #parent 
* #PRNINLAW ^property[0].valueCode = #PRN 
* #PRNINLAW ^property[1].code = #child 
* #PRNINLAW ^property[1].valueCode = #FTHINLAW 
* #PRNINLAW ^property[2].code = #child 
* #PRNINLAW ^property[2].valueCode = #MTHINLAW 
* #FTHINLAW "father-in-law"
* #FTHINLAW ^property[0].code = #parent 
* #FTHINLAW ^property[0].valueCode = #PRNINLAW 
* #MTHINLAW "mother-in-law"
* #MTHINLAW ^property[0].code = #parent 
* #MTHINLAW ^property[0].valueCode = #PRNINLAW 
* #STPPRN "step parent"
* #STPPRN ^property[0].code = #parent 
* #STPPRN ^property[0].valueCode = #PRN 
* #STPPRN ^property[1].code = #child 
* #STPPRN ^property[1].valueCode = #STPFTH 
* #STPPRN ^property[2].code = #child 
* #STPPRN ^property[2].valueCode = #STPMTH 
* #STPFTH "stepfather"
* #STPFTH ^property[0].code = #parent 
* #STPFTH ^property[0].valueCode = #STPPRN 
* #STPMTH "stepmother"
* #STPMTH ^property[0].code = #parent 
* #STPMTH ^property[0].valueCode = #STPPRN 
* #SIB "Sibling"
* #SIB ^property[0].code = #parent 
* #SIB ^property[0].valueCode = #FAMMEMB 
* #SIB ^property[1].code = #child 
* #SIB ^property[1].valueCode = #BRO 
* #SIB ^property[2].code = #child 
* #SIB ^property[2].valueCode = #HSIB 
* #SIB ^property[3].code = #child 
* #SIB ^property[3].valueCode = #NSIB 
* #SIB ^property[4].code = #child 
* #SIB ^property[4].valueCode = #SIBINLAW 
* #SIB ^property[5].code = #child 
* #SIB ^property[5].valueCode = #SIS 
* #SIB ^property[6].code = #child 
* #SIB ^property[6].valueCode = #STPSIB 
* #BRO "Brother"
* #BRO ^property[0].code = #parent 
* #BRO ^property[0].valueCode = #SIB 
* #HSIB "half-sibling"
* #HSIB ^property[0].code = #parent 
* #HSIB ^property[0].valueCode = #SIB 
* #HSIB ^property[1].code = #child 
* #HSIB ^property[1].valueCode = #HBRO 
* #HSIB ^property[2].code = #child 
* #HSIB ^property[2].valueCode = #HSIS 
* #HBRO "half-brother"
* #HBRO ^property[0].code = #parent 
* #HBRO ^property[0].valueCode = #HSIB 
* #HSIS "half-sister"
* #HSIS ^property[0].code = #parent 
* #HSIS ^property[0].valueCode = #HSIB 
* #NSIB "natural sibling"
* #NSIB ^property[0].code = #parent 
* #NSIB ^property[0].valueCode = #SIB 
* #NSIB ^property[1].code = #child 
* #NSIB ^property[1].valueCode = #NBRO 
* #NSIB ^property[2].code = #child 
* #NSIB ^property[2].valueCode = #NSIS 
* #NBRO "natural brother"
* #NBRO ^property[0].code = #parent 
* #NBRO ^property[0].valueCode = #NSIB 
* #NSIS "natural sister"
* #NSIS ^property[0].code = #parent 
* #NSIS ^property[0].valueCode = #NSIB 
* #SIBINLAW "sibling in-law"
* #SIBINLAW ^property[0].code = #parent 
* #SIBINLAW ^property[0].valueCode = #SIB 
* #SIBINLAW ^property[1].code = #child 
* #SIBINLAW ^property[1].valueCode = #BROINLAW 
* #SIBINLAW ^property[2].code = #child 
* #SIBINLAW ^property[2].valueCode = #SISINLAW 
* #BROINLAW "brother-in-law"
* #BROINLAW ^property[0].code = #parent 
* #BROINLAW ^property[0].valueCode = #SIBINLAW 
* #SISINLAW "sister-in-law"
* #SISINLAW ^property[0].code = #parent 
* #SISINLAW ^property[0].valueCode = #SIBINLAW 
* #SIS "Sister"
* #SIS ^property[0].code = #parent 
* #SIS ^property[0].valueCode = #SIB 
* #STPSIB "step sibling"
* #STPSIB ^property[0].code = #parent 
* #STPSIB ^property[0].valueCode = #SIB 
* #STPSIB ^property[1].code = #child 
* #STPSIB ^property[1].valueCode = #STPBRO 
* #STPSIB ^property[2].code = #child 
* #STPSIB ^property[2].valueCode = #STPSIS 
* #STPBRO "stepbrother"
* #STPBRO ^property[0].code = #parent 
* #STPBRO ^property[0].valueCode = #STPSIB 
* #STPSIS "stepsister"
* #STPSIS ^property[0].code = #parent 
* #STPSIS ^property[0].valueCode = #STPSIB 
* #SIGOTHR "significant other"
* #SIGOTHR ^property[0].code = #parent 
* #SIGOTHR ^property[0].valueCode = #FAMMEMB 
* #SIGOTHR ^property[1].code = #child 
* #SIGOTHR ^property[1].valueCode = #DOMPART 
* #SIGOTHR ^property[2].code = #child 
* #SIGOTHR ^property[2].valueCode = #SPS 
* #DOMPART "domestic partner"
* #DOMPART ^property[0].code = #parent 
* #DOMPART ^property[0].valueCode = #SIGOTHR 
* #SPS "spouse"
* #SPS ^property[0].code = #parent 
* #SPS ^property[0].valueCode = #SIGOTHR 
* #SPS ^property[1].code = #child 
* #SPS ^property[1].valueCode = #HUSB 
* #SPS ^property[2].code = #child 
* #SPS ^property[2].valueCode = #WIFE 
* #HUSB "husband"
* #HUSB ^property[0].code = #parent 
* #HUSB ^property[0].valueCode = #SPS 
* #WIFE "wife"
* #WIFE ^property[0].code = #parent 
* #WIFE ^property[0].valueCode = #SPS 
* #FRND "unrelated friend"
* #FRND ^property[0].code = #parent 
* #FRND ^property[0].valueCode = #_PersonalRelationshipRoleType 
* #NBOR "neighbor"
* #NBOR ^property[0].code = #parent 
* #NBOR ^property[0].valueCode = #_PersonalRelationshipRoleType 
* #ROOM "Roommate"
* #ROOM ^property[0].code = #parent 
* #ROOM ^property[0].valueCode = #_PersonalRelationshipRoleType 
* #_PolicyOrProgramCoverageRoleType "PolicyOrProgramCoverageRoleType"
* #_PolicyOrProgramCoverageRoleType ^property[0].code = #child 
* #_PolicyOrProgramCoverageRoleType ^property[0].valueCode = #_CoverageRoleType 
* #_PolicyOrProgramCoverageRoleType ^property[1].code = #child 
* #_PolicyOrProgramCoverageRoleType ^property[1].valueCode = #_CoveredPartyRoleType 
* #_CoverageRoleType "CoverageRoleType"
* #_CoverageRoleType ^property[0].code = #parent 
* #_CoverageRoleType ^property[0].valueCode = #_PolicyOrProgramCoverageRoleType 
* #_CoverageRoleType ^property[1].code = #child 
* #_CoverageRoleType ^property[1].valueCode = #FAMDEP 
* #_CoverageRoleType ^property[2].code = #child 
* #_CoverageRoleType ^property[2].valueCode = #HANDIC 
* #_CoverageRoleType ^property[3].code = #child 
* #_CoverageRoleType ^property[3].valueCode = #INJ 
* #_CoverageRoleType ^property[4].code = #child 
* #_CoverageRoleType ^property[4].valueCode = #SELF 
* #_CoverageRoleType ^property[5].code = #child 
* #_CoverageRoleType ^property[5].valueCode = #SPON 
* #_CoverageRoleType ^property[6].code = #child 
* #_CoverageRoleType ^property[6].valueCode = #STUD 
* #FAMDEP "family dependent"
* #FAMDEP ^property[0].code = #parent 
* #FAMDEP ^property[0].valueCode = #_CoverageRoleType 
* #HANDIC "handicapped dependent"
* #HANDIC ^property[0].code = #parent 
* #HANDIC ^property[0].valueCode = #_CoverageRoleType 
* #INJ "injured plaintiff"
* #INJ ^property[0].code = #parent 
* #INJ ^property[0].valueCode = #_CoverageRoleType 
* #SELF "self"
* #SELF ^property[0].code = #parent 
* #SELF ^property[0].valueCode = #_CoverageRoleType 
* #SPON "sponsored dependent"
* #SPON ^property[0].code = #parent 
* #SPON ^property[0].valueCode = #_CoverageRoleType 
* #STUD "student"
* #STUD ^property[0].code = #parent 
* #STUD ^property[0].valueCode = #_CoverageRoleType 
* #STUD ^property[1].code = #child 
* #STUD ^property[1].valueCode = #FSTUD 
* #STUD ^property[2].code = #child 
* #STUD ^property[2].valueCode = #PSTUD 
* #FSTUD "full-time student"
* #FSTUD ^property[0].code = #parent 
* #FSTUD ^property[0].valueCode = #STUD 
* #PSTUD "part-time student"
* #PSTUD ^property[0].code = #parent 
* #PSTUD ^property[0].valueCode = #STUD 
* #_CoveredPartyRoleType "covered party role type"
* #_CoveredPartyRoleType ^property[0].code = #parent 
* #_CoveredPartyRoleType ^property[0].valueCode = #_PolicyOrProgramCoverageRoleType 
* #_CoveredPartyRoleType ^property[1].code = #child 
* #_CoveredPartyRoleType ^property[1].valueCode = #_ClaimantCoveredPartyRoleType 
* #_CoveredPartyRoleType ^property[2].code = #child 
* #_CoveredPartyRoleType ^property[2].valueCode = #_DependentCoveredPartyRoleType 
* #_CoveredPartyRoleType ^property[3].code = #child 
* #_CoveredPartyRoleType ^property[3].valueCode = #_IndividualInsuredPartyRoleType 
* #_CoveredPartyRoleType ^property[4].code = #child 
* #_CoveredPartyRoleType ^property[4].valueCode = #_ProgramEligiblePartyRoleType 
* #_CoveredPartyRoleType ^property[5].code = #child 
* #_CoveredPartyRoleType ^property[5].valueCode = #_SubscriberCoveredPartyRoleType 
* #_ClaimantCoveredPartyRoleType "ClaimantCoveredPartyRoleType"
* #_ClaimantCoveredPartyRoleType ^property[0].code = #parent 
* #_ClaimantCoveredPartyRoleType ^property[0].valueCode = #_CoveredPartyRoleType 
* #_ClaimantCoveredPartyRoleType ^property[1].code = #child 
* #_ClaimantCoveredPartyRoleType ^property[1].valueCode = #CRIMEVIC 
* #_ClaimantCoveredPartyRoleType ^property[2].code = #child 
* #_ClaimantCoveredPartyRoleType ^property[2].valueCode = #INJWKR 
* #CRIMEVIC "crime victim"
* #CRIMEVIC ^property[0].code = #parent 
* #CRIMEVIC ^property[0].valueCode = #_ClaimantCoveredPartyRoleType 
* #INJWKR "injured worker"
* #INJWKR ^property[0].code = #parent 
* #INJWKR ^property[0].valueCode = #_ClaimantCoveredPartyRoleType 
* #_DependentCoveredPartyRoleType "DependentCoveredPartyRoleType"
* #_DependentCoveredPartyRoleType ^property[0].code = #parent 
* #_DependentCoveredPartyRoleType ^property[0].valueCode = #_CoveredPartyRoleType 
* #_DependentCoveredPartyRoleType ^property[1].code = #child 
* #_DependentCoveredPartyRoleType ^property[1].valueCode = #COCBEN 
* #_DependentCoveredPartyRoleType ^property[2].code = #child 
* #_DependentCoveredPartyRoleType ^property[2].valueCode = #DIFFABL 
* #_DependentCoveredPartyRoleType ^property[3].code = #child 
* #_DependentCoveredPartyRoleType ^property[3].valueCode = #WARD 
* #COCBEN "continuity of coverage beneficiary"
* #COCBEN ^property[0].code = #parent 
* #COCBEN ^property[0].valueCode = #_DependentCoveredPartyRoleType 
* #DIFFABL "differently abled"
* #DIFFABL ^property[0].code = #parent 
* #DIFFABL ^property[0].valueCode = #_DependentCoveredPartyRoleType 
* #WARD "ward"
* #WARD ^property[0].code = #parent 
* #WARD ^property[0].valueCode = #_DependentCoveredPartyRoleType 
* #_IndividualInsuredPartyRoleType "IndividualInsuredPartyRoleType"
* #_IndividualInsuredPartyRoleType ^property[0].code = #parent 
* #_IndividualInsuredPartyRoleType ^property[0].valueCode = #_CoveredPartyRoleType 
* #_IndividualInsuredPartyRoleType ^property[1].code = #child 
* #_IndividualInsuredPartyRoleType ^property[1].valueCode = #RETIREE 
* #RETIREE "retiree"
* #RETIREE ^property[0].code = #parent 
* #RETIREE ^property[0].valueCode = #_IndividualInsuredPartyRoleType 
* #_ProgramEligiblePartyRoleType "ProgramEligiblePartyRoleType"
* #_ProgramEligiblePartyRoleType ^property[0].code = #parent 
* #_ProgramEligiblePartyRoleType ^property[0].valueCode = #_CoveredPartyRoleType 
* #_ProgramEligiblePartyRoleType ^property[1].code = #child 
* #_ProgramEligiblePartyRoleType ^property[1].valueCode = #INDIG 
* #_ProgramEligiblePartyRoleType ^property[2].code = #child 
* #_ProgramEligiblePartyRoleType ^property[2].valueCode = #MIL 
* #INDIG "member of an indigenous people"
* #INDIG ^property[0].code = #parent 
* #INDIG ^property[0].valueCode = #_ProgramEligiblePartyRoleType 
* #MIL "military"
* #MIL ^property[0].code = #parent 
* #MIL ^property[0].valueCode = #_ProgramEligiblePartyRoleType 
* #MIL ^property[1].code = #child 
* #MIL ^property[1].valueCode = #ACTMIL 
* #MIL ^property[2].code = #child 
* #MIL ^property[2].valueCode = #RETMIL 
* #MIL ^property[3].code = #child 
* #MIL ^property[3].valueCode = #VET 
* #ACTMIL "active duty military"
* #ACTMIL ^property[0].code = #parent 
* #ACTMIL ^property[0].valueCode = #MIL 
* #RETMIL "retired military"
* #RETMIL ^property[0].code = #parent 
* #RETMIL ^property[0].valueCode = #MIL 
* #VET "veteran"
* #VET ^property[0].code = #parent 
* #VET ^property[0].valueCode = #MIL 
* #_SubscriberCoveredPartyRoleType "SubscriberCoveredPartyRoleType"
* #_SubscriberCoveredPartyRoleType ^property[0].code = #parent 
* #_SubscriberCoveredPartyRoleType ^property[0].valueCode = #_CoveredPartyRoleType 
* #_ResearchSubjectRoleBasis "ResearchSubjectRoleBasis"
* #_ResearchSubjectRoleBasis ^property[0].code = #child 
* #_ResearchSubjectRoleBasis ^property[0].valueCode = #ERL 
* #_ResearchSubjectRoleBasis ^property[1].code = #child 
* #_ResearchSubjectRoleBasis ^property[1].valueCode = #SCN 
* #ERL "enrollment"
* #ERL ^property[0].code = #parent 
* #ERL ^property[0].valueCode = #_ResearchSubjectRoleBasis 
* #SCN "screening"
* #SCN ^property[0].code = #parent 
* #SCN ^property[0].valueCode = #_ResearchSubjectRoleBasis 
* #_ServiceDeliveryLocationRoleType "ServiceDeliveryLocationRoleType"
* #_ServiceDeliveryLocationRoleType ^property[0].code = #child 
* #_ServiceDeliveryLocationRoleType ^property[0].valueCode = #_DedicatedServiceDeliveryLocationRoleType 
* #_ServiceDeliveryLocationRoleType ^property[1].code = #child 
* #_ServiceDeliveryLocationRoleType ^property[1].valueCode = #_IncidentalServiceDeliveryLocationRoleType 
* #_DedicatedServiceDeliveryLocationRoleType "DedicatedServiceDeliveryLocationRoleType"
* #_DedicatedServiceDeliveryLocationRoleType ^property[0].code = #parent 
* #_DedicatedServiceDeliveryLocationRoleType ^property[0].valueCode = #_ServiceDeliveryLocationRoleType 
* #_DedicatedServiceDeliveryLocationRoleType ^property[1].code = #child 
* #_DedicatedServiceDeliveryLocationRoleType ^property[1].valueCode = #_DedicatedClinicalLocationRoleType 
* #_DedicatedServiceDeliveryLocationRoleType ^property[2].code = #child 
* #_DedicatedServiceDeliveryLocationRoleType ^property[2].valueCode = #_DedicatedNonClinicalLocationRoleType 
* #_DedicatedClinicalLocationRoleType "DedicatedClinicalLocationRoleType"
* #_DedicatedClinicalLocationRoleType ^property[0].code = #parent 
* #_DedicatedClinicalLocationRoleType ^property[0].valueCode = #_DedicatedServiceDeliveryLocationRoleType 
* #_DedicatedClinicalLocationRoleType ^property[1].code = #child 
* #_DedicatedClinicalLocationRoleType ^property[1].valueCode = #DX 
* #_DedicatedClinicalLocationRoleType ^property[2].code = #child 
* #_DedicatedClinicalLocationRoleType ^property[2].valueCode = #HOSP 
* #_DedicatedClinicalLocationRoleType ^property[3].code = #child 
* #_DedicatedClinicalLocationRoleType ^property[3].valueCode = #HU 
* #_DedicatedClinicalLocationRoleType ^property[4].code = #child 
* #_DedicatedClinicalLocationRoleType ^property[4].valueCode = #NCCF 
* #_DedicatedClinicalLocationRoleType ^property[5].code = #child 
* #_DedicatedClinicalLocationRoleType ^property[5].valueCode = #OF 
* #_DedicatedClinicalLocationRoleType ^property[6].code = #child 
* #_DedicatedClinicalLocationRoleType ^property[6].valueCode = #RTF 
* #DX "Diagnostics or therapeutics unit"
* #DX ^property[0].code = #parent 
* #DX ^property[0].valueCode = #_DedicatedClinicalLocationRoleType 
* #DX ^property[1].code = #child 
* #DX ^property[1].valueCode = #CVDX 
* #DX ^property[2].code = #child 
* #DX ^property[2].valueCode = #GIDX 
* #DX ^property[3].code = #child 
* #DX ^property[3].valueCode = #RADDX 
* #CVDX "Cardiovascular diagnostics or therapeutics unit"
* #CVDX ^property[0].code = #parent 
* #CVDX ^property[0].valueCode = #DX 
* #CVDX ^property[1].code = #child 
* #CVDX ^property[1].valueCode = #CATH 
* #CVDX ^property[2].code = #child 
* #CVDX ^property[2].valueCode = #ECHO 
* #CATH "Cardiac catheterization lab"
* #CATH ^property[0].code = #parent 
* #CATH ^property[0].valueCode = #CVDX 
* #ECHO "Echocardiography lab"
* #ECHO ^property[0].code = #parent 
* #ECHO ^property[0].valueCode = #CVDX 
* #GIDX "Gastroenterology diagnostics or therapeutics lab"
* #GIDX ^property[0].code = #parent 
* #GIDX ^property[0].valueCode = #DX 
* #GIDX ^property[1].code = #child 
* #GIDX ^property[1].valueCode = #ENDOS 
* #ENDOS "Endoscopy lab"
* #ENDOS ^property[0].code = #parent 
* #ENDOS ^property[0].valueCode = #GIDX 
* #RADDX "Radiology diagnostics or therapeutics unit"
* #RADDX ^property[0].code = #parent 
* #RADDX ^property[0].valueCode = #DX 
* #RADDX ^property[1].code = #child 
* #RADDX ^property[1].valueCode = #RADO 
* #RADDX ^property[2].code = #child 
* #RADDX ^property[2].valueCode = #RNEU 
* #RADO "Radiation oncology unit"
* #RADO ^property[0].code = #parent 
* #RADO ^property[0].valueCode = #RADDX 
* #RNEU "Neuroradiology unit"
* #RNEU ^property[0].code = #parent 
* #RNEU ^property[0].valueCode = #RADDX 
* #HOSP "Hospital"
* #HOSP ^property[0].code = #parent 
* #HOSP ^property[0].valueCode = #_DedicatedClinicalLocationRoleType 
* #HOSP ^property[1].code = #child 
* #HOSP ^property[1].valueCode = #CHR 
* #HOSP ^property[2].code = #child 
* #HOSP ^property[2].valueCode = #GACH 
* #HOSP ^property[3].code = #child 
* #HOSP ^property[3].valueCode = #MHSP 
* #HOSP ^property[4].code = #child 
* #HOSP ^property[4].valueCode = #PSYCHF 
* #HOSP ^property[5].code = #child 
* #HOSP ^property[5].valueCode = #RH 
* #CHR "Chronic Care Facility"
* #CHR ^property[0].code = #parent 
* #CHR ^property[0].valueCode = #HOSP 
* #GACH "Hospitals; General Acute Care Hospital"
* #GACH ^property[0].code = #parent 
* #GACH ^property[0].valueCode = #HOSP 
* #MHSP "Military Hospital"
* #MHSP ^property[0].code = #parent 
* #MHSP ^property[0].valueCode = #HOSP 
* #PSYCHF "Psychatric Care Facility"
* #PSYCHF ^property[0].code = #parent 
* #PSYCHF ^property[0].valueCode = #HOSP 
* #RH "Rehabilitation hospital"
* #RH ^property[0].code = #parent 
* #RH ^property[0].valueCode = #HOSP 
* #RH ^property[1].code = #child 
* #RH ^property[1].valueCode = #RHAT 
* #RH ^property[2].code = #child 
* #RH ^property[2].valueCode = #RHII 
* #RH ^property[3].code = #child 
* #RH ^property[3].valueCode = #RHMAD 
* #RH ^property[4].code = #child 
* #RH ^property[4].valueCode = #RHPI 
* #RH ^property[5].code = #child 
* #RH ^property[5].valueCode = #RHYAD 
* #RHAT "addiction treatment center"
* #RHAT ^property[0].code = #parent 
* #RHAT ^property[0].valueCode = #RH 
* #RHII "intellectual impairment center"
* #RHII ^property[0].code = #parent 
* #RHII ^property[0].valueCode = #RH 
* #RHMAD "parents with adjustment difficulties center"
* #RHMAD ^property[0].code = #parent 
* #RHMAD ^property[0].valueCode = #RH 
* #RHPI "physical impairment center"
* #RHPI ^property[0].code = #parent 
* #RHPI ^property[0].valueCode = #RH 
* #RHPI ^property[1].code = #child 
* #RHPI ^property[1].valueCode = #RHPIH 
* #RHPI ^property[2].code = #child 
* #RHPI ^property[2].valueCode = #RHPIMS 
* #RHPI ^property[3].code = #child 
* #RHPI ^property[3].valueCode = #RHPIVS 
* #RHPIH "physical impairment - hearing center"
* #RHPIH ^property[0].code = #parent 
* #RHPIH ^property[0].valueCode = #RHPI 
* #RHPIMS "physical impairment - motor skills center"
* #RHPIMS ^property[0].code = #parent 
* #RHPIMS ^property[0].valueCode = #RHPI 
* #RHPIVS "physical impairment - visual skills center"
* #RHPIVS ^property[0].code = #parent 
* #RHPIVS ^property[0].valueCode = #RHPI 
* #RHYAD "youths with adjustment difficulties center"
* #RHYAD ^property[0].code = #parent 
* #RHYAD ^property[0].valueCode = #RH 
* #HU "Hospital unit"
* #HU ^property[0].code = #parent 
* #HU ^property[0].valueCode = #_DedicatedClinicalLocationRoleType 
* #HU ^property[1].code = #child 
* #HU ^property[1].valueCode = #BMTU 
* #HU ^property[2].code = #child 
* #HU ^property[2].valueCode = #CCU 
* #HU ^property[3].code = #child 
* #HU ^property[3].valueCode = #CHEST 
* #HU ^property[4].code = #child 
* #HU ^property[4].valueCode = #EPIL 
* #HU ^property[5].code = #child 
* #HU ^property[5].valueCode = #ER 
* #HU ^property[6].code = #child 
* #HU ^property[6].valueCode = #HD 
* #HU ^property[7].code = #child 
* #HU ^property[7].valueCode = #HLAB 
* #HU ^property[8].code = #child 
* #HU ^property[8].valueCode = #HRAD 
* #HU ^property[9].code = #child 
* #HU ^property[9].valueCode = #HUSCS 
* #HU ^property[10].code = #child 
* #HU ^property[10].valueCode = #ICU 
* #HU ^property[11].code = #child 
* #HU ^property[11].valueCode = #INPHARM 
* #HU ^property[12].code = #child 
* #HU ^property[12].valueCode = #MBL 
* #HU ^property[13].code = #child 
* #HU ^property[13].valueCode = #NCCS 
* #HU ^property[14].code = #child 
* #HU ^property[14].valueCode = #NS 
* #HU ^property[15].code = #child 
* #HU ^property[15].valueCode = #OUTPHARM 
* #HU ^property[16].code = #child 
* #HU ^property[16].valueCode = #PEDU 
* #HU ^property[17].code = #child 
* #HU ^property[17].valueCode = #PHU 
* #HU ^property[18].code = #child 
* #HU ^property[18].valueCode = #RHU 
* #HU ^property[19].code = #child 
* #HU ^property[19].valueCode = #SLEEP 
* #BMTU "Bone marrow transplant unit"
* #BMTU ^property[0].code = #parent 
* #BMTU ^property[0].valueCode = #HU 
* #CCU "Coronary care unit"
* #CCU ^property[0].code = #parent 
* #CCU ^property[0].valueCode = #HU 
* #CHEST "Chest unit"
* #CHEST ^property[0].code = #parent 
* #CHEST ^property[0].valueCode = #HU 
* #EPIL "Epilepsy unit"
* #EPIL ^property[0].code = #parent 
* #EPIL ^property[0].valueCode = #HU 
* #ER "Emergency room"
* #ER ^property[0].code = #parent 
* #ER ^property[0].valueCode = #HU 
* #ER ^property[1].code = #child 
* #ER ^property[1].valueCode = #ETU 
* #ETU "Emergency trauma unit"
* #ETU ^property[0].code = #parent 
* #ETU ^property[0].valueCode = #ER 
* #HD "Hemodialysis unit"
* #HD ^property[0].code = #parent 
* #HD ^property[0].valueCode = #HU 
* #HLAB "hospital laboratory"
* #HLAB ^property[0].code = #parent 
* #HLAB ^property[0].valueCode = #HU 
* #HLAB ^property[1].code = #child 
* #HLAB ^property[1].valueCode = #INLAB 
* #HLAB ^property[2].code = #child 
* #HLAB ^property[2].valueCode = #OUTLAB 
* #INLAB "inpatient laboratory"
* #INLAB ^property[0].code = #parent 
* #INLAB ^property[0].valueCode = #HLAB 
* #OUTLAB "outpatient laboratory"
* #OUTLAB ^property[0].code = #parent 
* #OUTLAB ^property[0].valueCode = #HLAB 
* #HRAD "radiology unit"
* #HRAD ^property[0].code = #parent 
* #HRAD ^property[0].valueCode = #HU 
* #HUSCS "specimen collection site"
* #HUSCS ^property[0].code = #parent 
* #HUSCS ^property[0].valueCode = #HU 
* #ICU "Intensive care unit"
* #ICU ^property[0].code = #parent 
* #ICU ^property[0].valueCode = #HU 
* #ICU ^property[1].code = #child 
* #ICU ^property[1].valueCode = #PEDICU 
* #PEDICU "Pediatric intensive care unit"
* #PEDICU ^property[0].code = #parent 
* #PEDICU ^property[0].valueCode = #ICU 
* #PEDICU ^property[1].code = #child 
* #PEDICU ^property[1].valueCode = #PEDNICU 
* #PEDNICU "Pediatric neonatal intensive care unit"
* #PEDNICU ^property[0].code = #parent 
* #PEDNICU ^property[0].valueCode = #PEDICU 
* #INPHARM "inpatient pharmacy"
* #INPHARM ^property[0].code = #parent 
* #INPHARM ^property[0].valueCode = #HU 
* #MBL "medical laboratory"
* #MBL ^property[0].code = #parent 
* #MBL ^property[0].valueCode = #HU 
* #NCCS "Neurology critical care and stroke unit"
* #NCCS ^property[0].code = #parent 
* #NCCS ^property[0].valueCode = #HU 
* #NS "Neurosurgery unit"
* #NS ^property[0].code = #parent 
* #NS ^property[0].valueCode = #HU 
* #OUTPHARM "outpatient pharmacy"
* #OUTPHARM ^property[0].code = #parent 
* #OUTPHARM ^property[0].valueCode = #HU 
* #PEDU "Pediatric unit"
* #PEDU ^property[0].code = #parent 
* #PEDU ^property[0].valueCode = #HU 
* #PHU "Psychiatric hospital unit"
* #PHU ^property[0].code = #parent 
* #PHU ^property[0].valueCode = #HU 
* #RHU "Rehabilitation hospital unit"
* #RHU ^property[0].code = #parent 
* #RHU ^property[0].valueCode = #HU 
* #SLEEP "Sleep disorders unit"
* #SLEEP ^property[0].code = #parent 
* #SLEEP ^property[0].valueCode = #HU 
* #NCCF "Nursing or custodial care facility"
* #NCCF ^property[0].code = #parent 
* #NCCF ^property[0].valueCode = #_DedicatedClinicalLocationRoleType 
* #NCCF ^property[1].code = #child 
* #NCCF ^property[1].valueCode = #SNF 
* #SNF "Skilled nursing facility"
* #SNF ^property[0].code = #parent 
* #SNF ^property[0].valueCode = #NCCF 
* #OF "Outpatient facility"
* #OF ^property[0].code = #parent 
* #OF ^property[0].valueCode = #_DedicatedClinicalLocationRoleType 
* #OF ^property[1].code = #child 
* #OF ^property[1].valueCode = #ALL 
* #OF ^property[2].code = #child 
* #OF ^property[2].valueCode = #AMPUT 
* #OF ^property[3].code = #child 
* #OF ^property[3].valueCode = #BMTC 
* #OF ^property[4].code = #child 
* #OF ^property[4].valueCode = #BREAST 
* #OF ^property[5].code = #child 
* #OF ^property[5].valueCode = #CANC 
* #OF ^property[6].code = #child 
* #OF ^property[6].valueCode = #CAPC 
* #OF ^property[7].code = #child 
* #OF ^property[7].valueCode = #CARD 
* #OF ^property[8].code = #child 
* #OF ^property[8].valueCode = #COAG 
* #OF ^property[9].code = #child 
* #OF ^property[9].valueCode = #CRS 
* #OF ^property[10].code = #child 
* #OF ^property[10].valueCode = #DERM 
* #OF ^property[11].code = #child 
* #OF ^property[11].valueCode = #ENDO 
* #OF ^property[12].code = #child 
* #OF ^property[12].valueCode = #ENT 
* #OF ^property[13].code = #child 
* #OF ^property[13].valueCode = #FMC 
* #OF ^property[14].code = #child 
* #OF ^property[14].valueCode = #GI 
* #OF ^property[15].code = #child 
* #OF ^property[15].valueCode = #GIM 
* #OF ^property[16].code = #child 
* #OF ^property[16].valueCode = #GYN 
* #OF ^property[17].code = #child 
* #OF ^property[17].valueCode = #HEM 
* #OF ^property[18].code = #child 
* #OF ^property[18].valueCode = #HTN 
* #OF ^property[19].code = #child 
* #OF ^property[19].valueCode = #IEC 
* #OF ^property[20].code = #child 
* #OF ^property[20].valueCode = #INFD 
* #OF ^property[21].code = #child 
* #OF ^property[21].valueCode = #INV 
* #OF ^property[22].code = #child 
* #OF ^property[22].valueCode = #LYMPH 
* #OF ^property[23].code = #child 
* #OF ^property[23].valueCode = #MGEN 
* #OF ^property[24].code = #child 
* #OF ^property[24].valueCode = #NEPH 
* #OF ^property[25].code = #child 
* #OF ^property[25].valueCode = #NEUR 
* #OF ^property[26].code = #child 
* #OF ^property[26].valueCode = #OB 
* #OF ^property[27].code = #child 
* #OF ^property[27].valueCode = #OMS 
* #OF ^property[28].code = #child 
* #OF ^property[28].valueCode = #ONCL 
* #OF ^property[29].code = #child 
* #OF ^property[29].valueCode = #OPH 
* #OF ^property[30].code = #child 
* #OF ^property[30].valueCode = #OPTC 
* #OF ^property[31].code = #child 
* #OF ^property[31].valueCode = #ORTHO 
* #OF ^property[32].code = #child 
* #OF ^property[32].valueCode = #PAINCL 
* #OF ^property[33].code = #child 
* #OF ^property[33].valueCode = #PC 
* #OF ^property[34].code = #child 
* #OF ^property[34].valueCode = #PEDC 
* #OF ^property[35].code = #child 
* #OF ^property[35].valueCode = #POD 
* #OF ^property[36].code = #child 
* #OF ^property[36].valueCode = #PREV 
* #OF ^property[37].code = #child 
* #OF ^property[37].valueCode = #PROCTO 
* #OF ^property[38].code = #child 
* #OF ^property[38].valueCode = #PROFF 
* #OF ^property[39].code = #child 
* #OF ^property[39].valueCode = #PROS 
* #OF ^property[40].code = #child 
* #OF ^property[40].valueCode = #PSI 
* #OF ^property[41].code = #child 
* #OF ^property[41].valueCode = #PSY 
* #OF ^property[42].code = #child 
* #OF ^property[42].valueCode = #RHEUM 
* #OF ^property[43].code = #child 
* #OF ^property[43].valueCode = #SPMED 
* #OF ^property[44].code = #child 
* #OF ^property[44].valueCode = #SU 
* #OF ^property[45].code = #child 
* #OF ^property[45].valueCode = #TR 
* #OF ^property[46].code = #child 
* #OF ^property[46].valueCode = #TRAVEL 
* #OF ^property[47].code = #child 
* #OF ^property[47].valueCode = #WND 
* #ALL "Allergy clinic"
* #ALL ^property[0].code = #parent 
* #ALL ^property[0].valueCode = #OF 
* #AMPUT "Amputee clinic"
* #AMPUT ^property[0].code = #parent 
* #AMPUT ^property[0].valueCode = #OF 
* #BMTC "Bone marrow transplant clinic"
* #BMTC ^property[0].code = #parent 
* #BMTC ^property[0].valueCode = #OF 
* #BREAST "Breast clinic"
* #BREAST ^property[0].code = #parent 
* #BREAST ^property[0].valueCode = #OF 
* #CANC "Child and adolescent neurology clinic"
* #CANC ^property[0].code = #parent 
* #CANC ^property[0].valueCode = #OF 
* #CAPC "Child and adolescent psychiatry clinic"
* #CAPC ^property[0].code = #parent 
* #CAPC ^property[0].valueCode = #OF 
* #CARD "Ambulatory Health Care Facilities; Clinic/Center; Rehabilitation: Cardiac Facilities"
* #CARD ^property[0].code = #parent 
* #CARD ^property[0].valueCode = #OF 
* #CARD ^property[1].code = #child 
* #CARD ^property[1].valueCode = #PEDCARD 
* #PEDCARD "Pediatric cardiology clinic"
* #PEDCARD ^property[0].code = #parent 
* #PEDCARD ^property[0].valueCode = #CARD 
* #COAG "Coagulation clinic"
* #COAG ^property[0].code = #parent 
* #COAG ^property[0].valueCode = #OF 
* #CRS "Colon and rectal surgery clinic"
* #CRS ^property[0].code = #parent 
* #CRS ^property[0].valueCode = #OF 
* #DERM "Dermatology clinic"
* #DERM ^property[0].code = #parent 
* #DERM ^property[0].valueCode = #OF 
* #ENDO "Endocrinology clinic"
* #ENDO ^property[0].code = #parent 
* #ENDO ^property[0].valueCode = #OF 
* #ENDO ^property[1].code = #child 
* #ENDO ^property[1].valueCode = #PEDE 
* #PEDE "Pediatric endocrinology clinic"
* #PEDE ^property[0].code = #parent 
* #PEDE ^property[0].valueCode = #ENDO 
* #ENT "Otorhinolaryngology clinic"
* #ENT ^property[0].code = #parent 
* #ENT ^property[0].valueCode = #OF 
* #FMC "Family medicine clinic"
* #FMC ^property[0].code = #parent 
* #FMC ^property[0].valueCode = #OF 
* #GI "Gastroenterology clinic"
* #GI ^property[0].code = #parent 
* #GI ^property[0].valueCode = #OF 
* #GI ^property[1].code = #child 
* #GI ^property[1].valueCode = #PEDGI 
* #PEDGI "Pediatric gastroenterology clinic"
* #PEDGI ^property[0].code = #parent 
* #PEDGI ^property[0].valueCode = #GI 
* #GIM "General internal medicine clinic"
* #GIM ^property[0].code = #parent 
* #GIM ^property[0].valueCode = #OF 
* #GYN "Gynecology clinic"
* #GYN ^property[0].code = #parent 
* #GYN ^property[0].valueCode = #OF 
* #HEM "Hematology clinic"
* #HEM ^property[0].code = #parent 
* #HEM ^property[0].valueCode = #OF 
* #HEM ^property[1].code = #child 
* #HEM ^property[1].valueCode = #PEDHEM 
* #PEDHEM "Pediatric hematology clinic"
* #PEDHEM ^property[0].code = #parent 
* #PEDHEM ^property[0].valueCode = #HEM 
* #HTN "Hypertension clinic"
* #HTN ^property[0].code = #parent 
* #HTN ^property[0].valueCode = #OF 
* #IEC "Impairment evaluation center"
* #IEC ^property[0].code = #parent 
* #IEC ^property[0].valueCode = #OF 
* #INFD "Infectious disease clinic"
* #INFD ^property[0].code = #parent 
* #INFD ^property[0].valueCode = #OF 
* #INFD ^property[1].code = #child 
* #INFD ^property[1].valueCode = #PEDID 
* #PEDID "Pediatric infectious disease clinic"
* #PEDID ^property[0].code = #parent 
* #PEDID ^property[0].valueCode = #INFD 
* #INV "Infertility clinic"
* #INV ^property[0].code = #parent 
* #INV ^property[0].valueCode = #OF 
* #LYMPH "Lympedema clinic"
* #LYMPH ^property[0].code = #parent 
* #LYMPH ^property[0].valueCode = #OF 
* #MGEN "Medical genetics clinic"
* #MGEN ^property[0].code = #parent 
* #MGEN ^property[0].valueCode = #OF 
* #NEPH "Nephrology clinic"
* #NEPH ^property[0].code = #parent 
* #NEPH ^property[0].valueCode = #OF 
* #NEPH ^property[1].code = #child 
* #NEPH ^property[1].valueCode = #PEDNEPH 
* #PEDNEPH "Pediatric nephrology clinic"
* #PEDNEPH ^property[0].code = #parent 
* #PEDNEPH ^property[0].valueCode = #NEPH 
* #NEUR "Neurology clinic"
* #NEUR ^property[0].code = #parent 
* #NEUR ^property[0].valueCode = #OF 
* #OB "Obstetrics clinic"
* #OB ^property[0].code = #parent 
* #OB ^property[0].valueCode = #OF 
* #OMS "Oral and maxillofacial surgery clinic"
* #OMS ^property[0].code = #parent 
* #OMS ^property[0].valueCode = #OF 
* #ONCL "Medical oncology clinic"
* #ONCL ^property[0].code = #parent 
* #ONCL ^property[0].valueCode = #OF 
* #ONCL ^property[1].code = #child 
* #ONCL ^property[1].valueCode = #PEDHO 
* #PEDHO "Pediatric oncology clinic"
* #PEDHO ^property[0].code = #parent 
* #PEDHO ^property[0].valueCode = #ONCL 
* #OPH "Opthalmology clinic"
* #OPH ^property[0].code = #parent 
* #OPH ^property[0].valueCode = #OF 
* #OPTC "optometry clinic"
* #OPTC ^property[0].code = #parent 
* #OPTC ^property[0].valueCode = #OF 
* #ORTHO "Orthopedics clinic"
* #ORTHO ^property[0].code = #parent 
* #ORTHO ^property[0].valueCode = #OF 
* #ORTHO ^property[1].code = #child 
* #ORTHO ^property[1].valueCode = #HAND 
* #HAND "Hand clinic"
* #HAND ^property[0].code = #parent 
* #HAND ^property[0].valueCode = #ORTHO 
* #PAINCL "Pain clinic"
* #PAINCL ^property[0].code = #parent 
* #PAINCL ^property[0].valueCode = #OF 
* #PC "Primary care clinic"
* #PC ^property[0].code = #parent 
* #PC ^property[0].valueCode = #OF 
* #PEDC "Pediatrics clinic"
* #PEDC ^property[0].code = #parent 
* #PEDC ^property[0].valueCode = #OF 
* #PEDC ^property[1].code = #child 
* #PEDC ^property[1].valueCode = #PEDRHEUM 
* #PEDRHEUM "Pediatric rheumatology clinic"
* #PEDRHEUM ^property[0].code = #parent 
* #PEDRHEUM ^property[0].valueCode = #PEDC 
* #POD "Podiatry clinic"
* #POD ^property[0].code = #parent 
* #POD ^property[0].valueCode = #OF 
* #PREV "Preventive medicine clinic"
* #PREV ^property[0].code = #parent 
* #PREV ^property[0].valueCode = #OF 
* #PROCTO "Proctology clinic"
* #PROCTO ^property[0].code = #parent 
* #PROCTO ^property[0].valueCode = #OF 
* #PROFF "Provider´s Office"
* #PROFF ^property[0].code = #parent 
* #PROFF ^property[0].valueCode = #OF 
* #PROS "Prosthodontics clinic"
* #PROS ^property[0].code = #parent 
* #PROS ^property[0].valueCode = #OF 
* #PSI "Psychology clinic"
* #PSI ^property[0].code = #parent 
* #PSI ^property[0].valueCode = #OF 
* #PSY "Psychiatry clinic"
* #PSY ^property[0].code = #parent 
* #PSY ^property[0].valueCode = #OF 
* #RHEUM "Rheumatology clinic"
* #RHEUM ^property[0].code = #parent 
* #RHEUM ^property[0].valueCode = #OF 
* #SPMED "Sports medicine clinic"
* #SPMED ^property[0].code = #parent 
* #SPMED ^property[0].valueCode = #OF 
* #SU "Surgery clinic"
* #SU ^property[0].code = #parent 
* #SU ^property[0].valueCode = #OF 
* #SU ^property[1].code = #child 
* #SU ^property[1].valueCode = #PLS 
* #SU ^property[2].code = #child 
* #SU ^property[2].valueCode = #URO 
* #PLS "Plastic surgery clinic"
* #PLS ^property[0].code = #parent 
* #PLS ^property[0].valueCode = #SU 
* #URO "Urology clinic"
* #URO ^property[0].code = #parent 
* #URO ^property[0].valueCode = #SU 
* #TR "Transplant clinic"
* #TR ^property[0].code = #parent 
* #TR ^property[0].valueCode = #OF 
* #TRAVEL "Travel and geographic medicine clinic"
* #TRAVEL ^property[0].code = #parent 
* #TRAVEL ^property[0].valueCode = #OF 
* #WND "Wound clinic"
* #WND ^property[0].code = #parent 
* #WND ^property[0].valueCode = #OF 
* #RTF "Residential treatment facility"
* #RTF ^property[0].code = #parent 
* #RTF ^property[0].valueCode = #_DedicatedClinicalLocationRoleType 
* #RTF ^property[1].code = #child 
* #RTF ^property[1].valueCode = #PRC 
* #RTF ^property[2].code = #child 
* #RTF ^property[2].valueCode = #SURF 
* #PRC "Pain rehabilitation center"
* #PRC ^property[0].code = #parent 
* #PRC ^property[0].valueCode = #RTF 
* #SURF "Substance use rehabilitation facility"
* #SURF ^property[0].code = #parent 
* #SURF ^property[0].valueCode = #RTF 
* #_DedicatedNonClinicalLocationRoleType "DedicatedNonClinicalLocationRoleType"
* #_DedicatedNonClinicalLocationRoleType ^property[0].code = #parent 
* #_DedicatedNonClinicalLocationRoleType ^property[0].valueCode = #_DedicatedServiceDeliveryLocationRoleType 
* #_DedicatedNonClinicalLocationRoleType ^property[1].code = #child 
* #_DedicatedNonClinicalLocationRoleType ^property[1].valueCode = #DADDR 
* #_DedicatedNonClinicalLocationRoleType ^property[2].code = #child 
* #_DedicatedNonClinicalLocationRoleType ^property[2].valueCode = #MOBL 
* #_DedicatedNonClinicalLocationRoleType ^property[3].code = #child 
* #_DedicatedNonClinicalLocationRoleType ^property[3].valueCode = #PHARM 
* #DADDR "Delivery Address"
* #DADDR ^property[0].code = #parent 
* #DADDR ^property[0].valueCode = #_DedicatedNonClinicalLocationRoleType 
* #MOBL "Mobile Unit"
* #MOBL ^property[0].code = #parent 
* #MOBL ^property[0].valueCode = #_DedicatedNonClinicalLocationRoleType 
* #MOBL ^property[1].code = #child 
* #MOBL ^property[1].valueCode = #AMB 
* #AMB "Ambulance"
* #AMB ^property[0].code = #parent 
* #AMB ^property[0].valueCode = #MOBL 
* #PHARM "Pharmacy"
* #PHARM ^property[0].code = #parent 
* #PHARM ^property[0].valueCode = #_DedicatedNonClinicalLocationRoleType 
* #_IncidentalServiceDeliveryLocationRoleType "IncidentalServiceDeliveryLocationRoleType"
* #_IncidentalServiceDeliveryLocationRoleType ^property[0].code = #parent 
* #_IncidentalServiceDeliveryLocationRoleType ^property[0].valueCode = #_ServiceDeliveryLocationRoleType 
* #_IncidentalServiceDeliveryLocationRoleType ^property[1].code = #child 
* #_IncidentalServiceDeliveryLocationRoleType ^property[1].valueCode = #ACC 
* #_IncidentalServiceDeliveryLocationRoleType ^property[2].code = #child 
* #_IncidentalServiceDeliveryLocationRoleType ^property[2].valueCode = #COMM 
* #_IncidentalServiceDeliveryLocationRoleType ^property[3].code = #child 
* #_IncidentalServiceDeliveryLocationRoleType ^property[3].valueCode = #PTRES 
* #_IncidentalServiceDeliveryLocationRoleType ^property[4].code = #child 
* #_IncidentalServiceDeliveryLocationRoleType ^property[4].valueCode = #SCHOOL 
* #_IncidentalServiceDeliveryLocationRoleType ^property[5].code = #child 
* #_IncidentalServiceDeliveryLocationRoleType ^property[5].valueCode = #UPC 
* #_IncidentalServiceDeliveryLocationRoleType ^property[6].code = #child 
* #_IncidentalServiceDeliveryLocationRoleType ^property[6].valueCode = #WORK 
* #ACC "accident site"
* #ACC ^property[0].code = #parent 
* #ACC ^property[0].valueCode = #_IncidentalServiceDeliveryLocationRoleType 
* #COMM "Community Location"
* #COMM ^property[0].code = #parent 
* #COMM ^property[0].valueCode = #_IncidentalServiceDeliveryLocationRoleType 
* #COMM ^property[1].code = #child 
* #COMM ^property[1].valueCode = #CSC 
* #CSC "community service center"
* #CSC ^property[0].code = #parent 
* #CSC ^property[0].valueCode = #COMM 
* #PTRES "Patient´s Residence"
* #PTRES ^property[0].code = #parent 
* #PTRES ^property[0].valueCode = #_IncidentalServiceDeliveryLocationRoleType 
* #SCHOOL "school"
* #SCHOOL ^property[0].code = #parent 
* #SCHOOL ^property[0].valueCode = #_IncidentalServiceDeliveryLocationRoleType 
* #UPC "underage protection center"
* #UPC ^property[0].code = #parent 
* #UPC ^property[0].valueCode = #_IncidentalServiceDeliveryLocationRoleType 
* #WORK "work site"
* #WORK ^property[0].code = #parent 
* #WORK ^property[0].valueCode = #_IncidentalServiceDeliveryLocationRoleType 
* #_SpecimenRoleType "SpecimenRoleType"
* #_SpecimenRoleType ^property[0].code = #child 
* #_SpecimenRoleType ^property[0].valueCode = #C 
* #_SpecimenRoleType ^property[1].code = #child 
* #_SpecimenRoleType ^property[1].valueCode = #G 
* #_SpecimenRoleType ^property[2].code = #child 
* #_SpecimenRoleType ^property[2].valueCode = #L 
* #_SpecimenRoleType ^property[3].code = #child 
* #_SpecimenRoleType ^property[3].valueCode = #P 
* #_SpecimenRoleType ^property[4].code = #child 
* #_SpecimenRoleType ^property[4].valueCode = #Q 
* #_SpecimenRoleType ^property[5].code = #child 
* #_SpecimenRoleType ^property[5].valueCode = #R 
* #C "Calibrator"
* #C ^property[0].code = #parent 
* #C ^property[0].valueCode = #_SpecimenRoleType 
* #G "Group"
* #G ^property[0].code = #parent 
* #G ^property[0].valueCode = #_SpecimenRoleType 
* #L "Pool"
* #L ^property[0].code = #parent 
* #L ^property[0].valueCode = #_SpecimenRoleType 
* #P "Patient"
* #P ^property[0].code = #parent 
* #P ^property[0].valueCode = #_SpecimenRoleType 
* #Q "Quality Control"
* #Q ^property[0].code = #parent 
* #Q ^property[0].valueCode = #_SpecimenRoleType 
* #Q ^property[1].code = #child 
* #Q ^property[1].valueCode = #B 
* #Q ^property[2].code = #child 
* #Q ^property[2].valueCode = #E 
* #Q ^property[3].code = #child 
* #Q ^property[3].valueCode = #F 
* #Q ^property[4].code = #child 
* #Q ^property[4].valueCode = #O 
* #Q ^property[5].code = #child 
* #Q ^property[5].valueCode = #V 
* #B "Blind"
* #B ^property[0].code = #parent 
* #B ^property[0].valueCode = #Q 
* #E "Electronic QC"
* #E ^property[0].code = #parent 
* #E ^property[0].valueCode = #Q 
* #F "Filler Proficiency"
* #F ^property[0].code = #parent 
* #F ^property[0].valueCode = #Q 
* #O "Operator Proficiency"
* #O ^property[0].code = #parent 
* #O ^property[0].valueCode = #Q 
* #V "Verifying"
* #V ^property[0].code = #parent 
* #V ^property[0].valueCode = #Q 
* #R "Replicate"
* #R ^property[0].code = #parent 
* #R ^property[0].valueCode = #_SpecimenRoleType 
* #communityLaboratory "Community Laboratory"
* #homeHealth "Home Health"
* #laboratory "Laboratory"
* #pathologist "Pathologist"
* #phlebotomist "Phlebotomist"
* #subject "Self"
* #thirdParty "Third Party"
