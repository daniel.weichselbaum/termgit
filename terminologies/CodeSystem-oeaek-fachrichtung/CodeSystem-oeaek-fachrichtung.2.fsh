Instance: oeaek-fachrichtung 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-oeaek-fachrichtung" 
* name = "oeaek-fachrichtung" 
* title = "OEAEK_Fachrichtung" 
* status = #active 
* content = #complete 
* version = "201902" 
* description = "**Description:** Code List of all disciplines valid in Austria issued by the Medical Chamber of Austria

**Beschreibung:** Code Liste aller in Österreich gültigen Fachrichtungen" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.3.1.10.20" 
* date = "2019-02-26" 
* count = 71 
* #1 "Anästhesiologie und Intensivmedizin"
* #1 ^designation[0].language = #de-AT 
* #1 ^designation[0].value = "Anästhesiologie und Intensivmedizin" 
* #10 "Neurologie und Psychiatrie"
* #10 ^designation[0].language = #de-AT 
* #10 ^designation[0].value = "Neurologie und Psychiatrie" 
* #11 "Orthopädie und Orthopädische Chirurgie"
* #11 ^designation[0].language = #de-AT 
* #11 ^designation[0].value = "Orthopädie und Orthopädische Chirurgie" 
* #114 "Orthopädie und Traumatologie"
* #114 ^designation[0].language = #de-AT 
* #114 ^designation[0].value = "Orthopädie und Traumatologie" 
* #12 "Physikalische Medizin und allgemeine Rehabilitation"
* #12 ^designation[0].language = #de-AT 
* #12 ^designation[0].value = "Physikalische Medizin und allgemeine Rehabilitation" 
* #13 "Radiologie"
* #13 ^designation[0].language = #de-AT 
* #13 ^designation[0].value = "Radiologie" 
* #14 "Unfallchirurgie"
* #14 ^designation[0].language = #de-AT 
* #14 ^designation[0].value = "Unfallchirurgie" 
* #15 "Urologie"
* #15 ^designation[0].language = #de-AT 
* #15 ^designation[0].value = "Urologie" 
* #16 "Zahn-, Mund und Kieferheilkunde"
* #16 ^designation[0].language = #de-AT 
* #16 ^designation[0].value = "Zahn-, Mund und Kieferheilkunde" 
* #18 "Psychiatrie und Neurologie"
* #18 ^designation[0].language = #de-AT 
* #18 ^designation[0].value = "Psychiatrie und Neurologie" 
* #19 "Medizinische und Chemische Labordiagnostik"
* #19 ^designation[0].language = #de-AT 
* #19 ^designation[0].value = "Medizinische und Chemische Labordiagnostik" 
* #2 "Augenheilkunde und Optometrie"
* #2 ^designation[0].language = #de-AT 
* #2 ^designation[0].value = "Augenheilkunde und Optometrie" 
* #20 "Mikrobiol.serol.Labordiagnostik"
* #20 ^designation[0].language = #de-AT 
* #20 ^designation[0].value = "Mikrobiol.serol.Labordiagnostik" 
* #21 "Anatomie"
* #21 ^designation[0].language = #de-AT 
* #21 ^designation[0].value = "Anatomie" 
* #22 "Histologie, Embryologie u. Zellbiologie"
* #22 ^designation[0].language = #de-AT 
* #22 ^designation[0].value = "Histologie, Embryologie u. Zellbiologie" 
* #23 "Klinische Mikrobiologie und Hygiene"
* #23 ^designation[0].language = #de-AT 
* #23 ^designation[0].value = "Klinische Mikrobiologie und Hygiene" 
* #24 "Klinische Pathologie und Molekularpathologie"
* #24 ^designation[0].language = #de-AT 
* #24 ^designation[0].value = "Klinische Pathologie und Molekularpathologie" 
* #25 "Pharmakologie und Toxikologie"
* #25 ^designation[0].language = #de-AT 
* #25 ^designation[0].value = "Pharmakologie und Toxikologie" 
* #26 "Physiologie"
* #26 ^designation[0].language = #de-AT 
* #26 ^designation[0].value = "Physiologie" 
* #27 "Gerichtsmedizin"
* #27 ^designation[0].language = #de-AT 
* #27 ^designation[0].value = "Gerichtsmedizin" 
* #29 "Neurochirurgie"
* #29 ^designation[0].language = #de-AT 
* #29 ^designation[0].value = "Neurochirurgie" 
* #3 "Allgemeinchirurgie und Viszeralchirurgie"
* #3 ^designation[0].language = #de-AT 
* #3 ^designation[0].value = "Allgemeinchirurgie und Viszeralchirurgie" 
* #30 "Plastische, Rekonstruktive und Ästhetische Chirurgie"
* #30 ^designation[0].language = #de-AT 
* #30 ^designation[0].value = "Plastische, Rekonstruktive und Ästhetische Chirurgie" 
* #31 "Klinische Immunologie"
* #31 ^designation[0].language = #de-AT 
* #31 ^designation[0].value = "Klinische Immunologie" 
* #32 "Medizinische Genetik"
* #32 ^designation[0].language = #de-AT 
* #32 ^designation[0].value = "Medizinische Genetik" 
* #326 "Physiologie und Pathophysiologie"
* #326 ^designation[0].language = #de-AT 
* #326 ^designation[0].value = "Physiologie und Pathophysiologie" 
* #33 "Medizinische Biophysik"
* #33 ^designation[0].language = #de-AT 
* #33 ^designation[0].value = "Medizinische Biophysik" 
* #34 "Neurobiologie"
* #34 ^designation[0].language = #de-AT 
* #34 ^designation[0].value = "Neurobiologie" 
* #35 "Neuropathologie"
* #35 ^designation[0].language = #de-AT 
* #35 ^designation[0].value = "Neuropathologie" 
* #352 "Klinische Pathologie und Neuropathologie"
* #352 ^designation[0].language = #de-AT 
* #352 ^designation[0].value = "Klinische Pathologie und Neuropathologie" 
* #36 "Pathophysiologie"
* #36 ^designation[0].language = #de-AT 
* #36 ^designation[0].value = "Pathophysiologie" 
* #37 "Sozialmedizin"
* #37 ^designation[0].language = #de-AT 
* #37 ^designation[0].value = "Sozialmedizin" 
* #370 "Public Health"
* #370 ^designation[0].language = #de-AT 
* #370 ^designation[0].value = "Public Health" 
* #372 "Allgemeinchirurgie und Gefäßchirurgie"
* #372 ^designation[0].language = #de-AT 
* #372 ^designation[0].value = "Allgemeinchirurgie und Gefäßchirurgie" 
* #38 "Tumorbiologie"
* #38 ^designation[0].language = #de-AT 
* #38 ^designation[0].value = "Tumorbiologie" 
* #4 "Frauenheilkunde und Geburtshilfe"
* #4 ^designation[0].language = #de-AT 
* #4 ^designation[0].value = "Frauenheilkunde und Geburtshilfe" 
* #40 "Arbeitsmedizin"
* #40 ^designation[0].language = #de-AT 
* #40 ^designation[0].value = "Arbeitsmedizin" 
* #400 "Arbeitsmedizin und angewandte Physiologie"
* #400 ^designation[0].language = #de-AT 
* #400 ^designation[0].value = "Arbeitsmedizin und angewandte Physiologie" 
* #41 "Transfusionsmedizin"
* #41 ^designation[0].language = #de-AT 
* #41 ^designation[0].value = "Transfusionsmedizin" 
* #42 "Kinder- und Jugendchirurgie"
* #42 ^designation[0].language = #de-AT 
* #42 ^designation[0].value = "Kinder- und Jugendchirurgie" 
* #43 "Medizinische Leistungsphysiologie"
* #43 ^designation[0].language = #de-AT 
* #43 ^designation[0].value = "Medizinische Leistungsphysiologie" 
* #44 "Mund-,  Kiefer- und Gesichtschirurgie"
* #44 ^designation[0].language = #de-AT 
* #44 ^designation[0].value = "Mund-,  Kiefer- und Gesichtschirurgie" 
* #45 "Nuklearmedizin"
* #45 ^designation[0].language = #de-AT 
* #45 ^designation[0].value = "Nuklearmedizin" 
* #46 "Spezifische Prophylaxe und Tropenmedizin"
* #46 ^designation[0].language = #de-AT 
* #46 ^designation[0].value = "Spezifische Prophylaxe und Tropenmedizin" 
* #463 "Klinische Immunologie und Spezifische Prophylaxe und Tropenmedizin"
* #463 ^designation[0].language = #de-AT 
* #463 ^designation[0].value = "Klinische Immunologie und Spezifische Prophylaxe und Tropenmedizin" 
* #47 "Strahlentherapie-Radioonkologie"
* #47 ^designation[0].language = #de-AT 
* #47 ^designation[0].value = "Strahlentherapie-Radioonkologie" 
* #48 "Virologie"
* #48 ^designation[0].language = #de-AT 
* #48 ^designation[0].value = "Virologie" 
* #482 "Klinische Mikrobiologie und Virologie"
* #482 ^designation[0].language = #de-AT 
* #482 ^designation[0].value = "Klinische Mikrobiologie und Virologie" 
* #49 "Neurologie"
* #49 ^designation[0].language = #de-AT 
* #49 ^designation[0].value = "Neurologie" 
* #5 "Hals- Nasen- und Ohrenheilkunde"
* #5 ^designation[0].language = #de-AT 
* #5 ^designation[0].value = "Hals- Nasen- und Ohrenheilkunde" 
* #50 "Psychiatrie"
* #50 ^designation[0].language = #de-AT 
* #50 ^designation[0].value = "Psychiatrie" 
* #51 "Radiologie"
* #51 ^designation[0].language = #de-AT 
* #51 ^designation[0].value = "Radiologie" 
* #52 "Psychiatrie und psychotherapeutische Medizin"
* #52 ^designation[0].language = #de-AT 
* #52 ^designation[0].value = "Psychiatrie und psychotherapeutische Medizin" 
* #53 "Herzchirurgie"
* #53 ^designation[0].language = #de-AT 
* #53 ^designation[0].value = "Herzchirurgie" 
* #54 "Thoraxchirurgie"
* #54 ^designation[0].language = #de-AT 
* #54 ^designation[0].value = "Thoraxchirurgie" 
* #55 "Kinder- und Jugendpsychiatrie"
* #55 ^designation[0].language = #de-AT 
* #55 ^designation[0].value = "Kinder- und Jugendpsychiatrie" 
* #550 "Kinder- und Jugendpsychiatrie und Psychotherapeutische Medizin"
* #550 ^designation[0].language = #de-AT 
* #550 ^designation[0].value = "Kinder- und Jugendpsychiatrie und Psychotherapeutische Medizin" 
* #6 "Haut- und Geschlechtskrankheiten"
* #6 ^designation[0].language = #de-AT 
* #6 ^designation[0].value = "Haut- und Geschlechtskrankheiten" 
* #7 "Innere Medizin"
* #7 ^designation[0].language = #de-AT 
* #7 ^designation[0].value = "Innere Medizin" 
* #709 "Innere Medizin und Pneumologie"
* #709 ^designation[0].language = #de-AT 
* #709 ^designation[0].value = "Innere Medizin und Pneumologie" 
* #761 "Innere Medizin und Infektiologie"
* #761 ^designation[0].language = #de-AT 
* #761 ^designation[0].value = "Innere Medizin und Infektiologie" 
* #774 "Innere Medizin und Kardiologie"
* #774 ^designation[0].language = #de-AT 
* #774 ^designation[0].value = "Innere Medizin und Kardiologie" 
* #775 "Innere Medizin und Nephrologie"
* #775 ^designation[0].language = #de-AT 
* #775 ^designation[0].value = "Innere Medizin und Nephrologie" 
* #778 "Innere Medizin und Angiologie"
* #778 ^designation[0].language = #de-AT 
* #778 ^designation[0].value = "Innere Medizin und Angiologie" 
* #779 "Innere Medizin und Endokrinologie und Diabetologie"
* #779 ^designation[0].language = #de-AT 
* #779 ^designation[0].value = "Innere Medizin und Endokrinologie und Diabetologie" 
* #780 "Innere Medizin und Gastroenterologie und Hepatologie"
* #780 ^designation[0].language = #de-AT 
* #780 ^designation[0].value = "Innere Medizin und Gastroenterologie und Hepatologie" 
* #782 "Innere Medizin und Hämatologie und internistische Onkologie"
* #782 ^designation[0].language = #de-AT 
* #782 ^designation[0].value = "Innere Medizin und Hämatologie und internistische Onkologie" 
* #783 "Innere Medizin und Intensivmedizin"
* #783 ^designation[0].language = #de-AT 
* #783 ^designation[0].value = "Innere Medizin und Intensivmedizin" 
* #786 "Innere Medizin und Rheumatologie"
* #786 ^designation[0].language = #de-AT 
* #786 ^designation[0].value = "Innere Medizin und Rheumatologie" 
* #8 "Kinder- und Jugendheilkunde"
* #8 ^designation[0].language = #de-AT 
* #8 ^designation[0].value = "Kinder- und Jugendheilkunde" 
* #9 "Lungenkrankheiten"
* #9 ^designation[0].language = #de-AT 
* #9 ^designation[0].value = "Lungenkrankheiten" 
