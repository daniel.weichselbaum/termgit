Instance: hl7-timing-event 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-timing-event" 
* name = "hl7-timing-event" 
* title = "HL7 Timing Event" 
* status = #active 
* content = #complete 
* version = "HL7:TimingEvent" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.5.139" 
* date = "2013-01-10" 
* count = 18 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #AC "AC"
* #ACD "ACD"
* #ACM "ACM"
* #ACV "ACV"
* #C "C"
* #C ^property[0].code = #child 
* #C ^property[0].valueCode = #CD 
* #C ^property[1].code = #child 
* #C ^property[1].valueCode = #CM 
* #C ^property[2].code = #child 
* #C ^property[2].valueCode = #CV 
* #CD "CD"
* #CD ^property[0].code = #parent 
* #CD ^property[0].valueCode = #C 
* #CM "CM"
* #CM ^property[0].code = #parent 
* #CM ^property[0].valueCode = #C 
* #CV "CV"
* #CV ^property[0].code = #parent 
* #CV ^property[0].valueCode = #C 
* #HS "HS"
* #IC "IC"
* #ICD "ICD"
* #ICM "ICM"
* #ICV "ICV"
* #PC "PC"
* #PCD "PCD"
* #PCM "PCM"
* #PCV "PCV"
* #WAKE "WAKE"
