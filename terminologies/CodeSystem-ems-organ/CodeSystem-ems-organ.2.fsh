Instance: ems-organ 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-organ" 
* name = "ems-organ" 
* title = "EMS_Organ" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Organ, welches histologisch untersucht wurde: Verwendung bei TBC" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.84" 
* date = "2017-01-26" 
* count = 11 
* #BONEOTHER "Sonstige Knochen und Gelenke"
* #CNSOTHER "Sonstiges Zentrales Nervensystem"
* #GASTROINTEST "Peritoneum, Verdauungstrakt"
* #LYMPHEXTHOR "Lymphknoten extrathorakal"
* #LYMPHINTHOR "Lymphknoten intrathorakal"
* #MENING "Hirnhaut"
* #OTHER "Sonstige Organe"
* #PLEURAL "Pleura"
* #PULMONARY "Lunge"
* #SPINE "Wirbelsäule"
* #UROGEN "Urogenitaltrakt (inkludiert Niere)"
