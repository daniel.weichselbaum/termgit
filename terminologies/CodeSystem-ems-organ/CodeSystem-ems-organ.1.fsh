Instance: ems-organ 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-organ" 
* name = "ems-organ" 
* title = "EMS_Organ" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Organ, welches histologisch untersucht wurde: Verwendung bei TBC" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.84" 
* date = "2017-01-26" 
* count = 11 
* concept[0].code = #BONEOTHER
* concept[0].display = "Sonstige Knochen und Gelenke"
* concept[1].code = #CNSOTHER
* concept[1].display = "Sonstiges Zentrales Nervensystem"
* concept[2].code = #GASTROINTEST
* concept[2].display = "Peritoneum, Verdauungstrakt"
* concept[3].code = #LYMPHEXTHOR
* concept[3].display = "Lymphknoten extrathorakal"
* concept[4].code = #LYMPHINTHOR
* concept[4].display = "Lymphknoten intrathorakal"
* concept[5].code = #MENING
* concept[5].display = "Hirnhaut"
* concept[6].code = #OTHER
* concept[6].display = "Sonstige Organe"
* concept[7].code = #PLEURAL
* concept[7].display = "Pleura"
* concept[8].code = #PULMONARY
* concept[8].display = "Lunge"
* concept[9].code = #SPINE
* concept[9].display = "Wirbelsäule"
* concept[10].code = #UROGEN
* concept[10].display = "Urogenitaltrakt (inkludiert Niere)"
