Instance: elga-auditeventid 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-auditeventid" 
* name = "elga-auditeventid" 
* title = "ELGA_AuditEventID" 
* status = #active 
* version = "3.1" 
* description = "**Beschreibung:** Valueset der ELGA spezifischen Audit Event IDs. Verwendung in IHE konformen und ELGA BeS definierten Audit Events." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.150" 
* date = "2015-06-01" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-elga-auditeventid"
* compose.include[0].concept[0].code = "0"
* compose.include[0].concept[0].display = "PAP Event"
* compose.include[0].concept[1].code = "1"
* compose.include[0].concept[1].display = "Generelle Policyverwaltung"
* compose.include[0].concept[2].code = "2"
* compose.include[0].concept[2].display = "Patientenzustimmung"
* compose.include[0].concept[3].code = "10"
* compose.include[0].concept[3].display = "Kontaktbestätigung"
* compose.include[0].concept[4].code = "20"
* compose.include[0].concept[4].display = "ELGA Token Service"
* compose.include[0].concept[5].code = "30"
* compose.include[0].concept[5].display = "Dokumentenverwaltung"
* compose.include[0].concept[6].code = "40"
* compose.include[0].concept[6].display = "e-Medikation"
* compose.include[0].concept[7].code = "90"
* compose.include[0].concept[7].display = "Administration"
