Instance: ips-conditionverificationstatus 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ips-conditionverificationstatus" 
* name = "ips-conditionverificationstatus" 
* title = "IPS_ConditionVerificationStatus" 
* status = #active 
* content = #complete 
* version = "202002" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.4.642.3.115" 
* date = "2020-02-04" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* count = 6 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #confirmed "bestätigt"
* #refuted "ausgeschlossen"
* #unconfirmed "unbestätigt"
* #unconfirmed ^property[0].code = #child 
* #unconfirmed ^property[0].valueCode = #differential 
* #unconfirmed ^property[1].code = #child 
* #unconfirmed ^property[1].valueCode = #provisional 
* #differential "alternativ möglich (Differentialdiagnose)"
* #differential ^property[0].code = #parent 
* #differential ^property[0].valueCode = #unconfirmed 
* #provisional "vorläufig"
* #provisional ^property[0].code = #parent 
* #provisional ^property[0].valueCode = #unconfirmed 
* #unknown "unbekannt"
