Instance: ems-phagentyp-vtec 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-phagentyp-vtec" 
* name = "ems-phagentyp-vtec" 
* title = "EMS_Phagentyp_VTEC" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Phagentyp VTEC (Serotyp O157)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.86" 
* date = "2017-01-26" 
* count = 85 
* #1 "1"
* #10 "10"
* #11 "11"
* #12 "12"
* #13 "13"
* #14 "14"
* #15 "15"
* #16 "16"
* #17 "17"
* #18 "18"
* #19 "19"
* #2 "2"
* #20 "20"
* #21 "21"
* #21_28 "21/28"
* #22 "22"
* #23 "23"
* #24 "24"
* #25 "25"
* #26 "26"
* #27 "27"
* #28 "28"
* #29 "29"
* #3 "3"
* #30 "30"
* #31 "31"
* #32 "32"
* #33 "33"
* #34 "34"
* #35 "35"
* #36 "36"
* #37 "37"
* #38 "38"
* #39 "39"
* #4 "4"
* #40 "40"
* #41 "41"
* #42 "42"
* #43 "43"
* #44 "44"
* #45 "45"
* #46 "46"
* #47 "47"
* #48 "48"
* #49 "49"
* #5 "5"
* #50 "50"
* #51 "51"
* #52 "52"
* #53 "53"
* #54 "54"
* #55 "55"
* #56 "56"
* #57 "57"
* #58 "58"
* #59 "59"
* #6 "6"
* #60 "60"
* #61 "61"
* #62 "62"
* #63 "63"
* #64 "64"
* #65 "65"
* #66 "66"
* #67 "67"
* #68 "68"
* #69 "69"
* #7 "7"
* #70 "70"
* #71 "71"
* #72 "72"
* #73 "73"
* #74 "74"
* #75 "75"
* #76 "76"
* #78 "78"
* #79 "79"
* #8 "8"
* #80 "80"
* #81 "81"
* #82 "82"
* #88 "88"
* #9 "9"
* #NA "Nicht Anwendbar"
* #RDNC "RDNC"
