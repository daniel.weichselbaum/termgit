Instance: ems-verotoxin-2-subtyp 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-verotoxin-2-subtyp" 
* name = "ems-verotoxin-2-subtyp" 
* title = "EMS_Verotoxin_2_Subtyp" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** Verotoxin 2 Subtyp" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.97" 
* date = "2017-01-26" 
* count = 8 
* #NOTEST "nicht durchgeführt"
* #VT2A "VT2a"
* #VT2B "VT2b"
* #VT2C "VT2c"
* #VT2D "VT2d"
* #VT2E "VT2e"
* #VT2F "VT2f"
* #VT2G "VT2g"
