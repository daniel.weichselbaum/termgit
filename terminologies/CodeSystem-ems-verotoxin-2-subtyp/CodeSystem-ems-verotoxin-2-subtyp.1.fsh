Instance: ems-verotoxin-2-subtyp 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-verotoxin-2-subtyp" 
* name = "ems-verotoxin-2-subtyp" 
* title = "EMS_Verotoxin_2_Subtyp" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** Verotoxin 2 Subtyp" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.97" 
* date = "2017-01-26" 
* count = 8 
* concept[0].code = #NOTEST
* concept[0].display = "nicht durchgeführt"
* concept[1].code = #VT2A
* concept[1].display = "VT2a"
* concept[2].code = #VT2B
* concept[2].display = "VT2b"
* concept[3].code = #VT2C
* concept[3].display = "VT2c"
* concept[4].code = #VT2D
* concept[4].display = "VT2d"
* concept[5].code = #VT2E
* concept[5].display = "VT2e"
* concept[6].code = #VT2F
* concept[6].display = "VT2f"
* concept[7].code = #VT2G
* concept[7].display = "VT2g"
