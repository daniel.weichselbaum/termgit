Instance: elga-auditeventid 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-auditeventid" 
* name = "elga-auditeventid" 
* title = "ELGA_AuditEventID" 
* status = #active 
* content = #complete 
* version = "3.1" 
* description = "**Beschreibung:** Codeliste der ELGA spezifischen Audit Event IDs. Verwendung in IHE konformen und ELGA BeS definierten Audit Events." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.150" 
* date = "2015-06-01" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* count = 8 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #0 "PAP Event"
* #0 ^designation[0].language = #de-AT 
* #0 ^designation[0].value = "PAP Event" 
* #0 ^property[0].code = #child 
* #0 ^property[0].valueCode = #1 
* #0 ^property[1].code = #child 
* #0 ^property[1].valueCode = #2 
* #1 "Generelle Policyverwaltung"
* #1 ^designation[0].language = #de-AT 
* #1 ^designation[0].value = "Generelle Policyverwaltung" 
* #1 ^property[0].code = #parent 
* #1 ^property[0].valueCode = #0 
* #2 "Patientenzustimmung"
* #2 ^designation[0].language = #de-AT 
* #2 ^designation[0].value = "Patientenzustimmung" 
* #2 ^property[0].code = #parent 
* #2 ^property[0].valueCode = #0 
* #10 "Kontaktbestätigung"
* #10 ^designation[0].language = #de-AT 
* #10 ^designation[0].value = "Kontaktbestätigung" 
* #20 "ELGA Token Service"
* #20 ^designation[0].language = #de-AT 
* #20 ^designation[0].value = "ELGA Token Service" 
* #30 "Dokumentenverwaltung"
* #30 ^designation[0].language = #de-AT 
* #30 ^designation[0].value = "Dokumentenverwaltung" 
* #40 "e-Medikation"
* #40 ^designation[0].language = #de-AT 
* #40 ^designation[0].value = "e-Medikation" 
* #90 "Administration"
* #90 ^designation[0].language = #de-AT 
* #90 ^designation[0].value = "Administration" 
