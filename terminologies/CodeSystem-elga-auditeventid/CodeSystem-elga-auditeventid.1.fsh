Instance: elga-auditeventid 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-auditeventid" 
* name = "elga-auditeventid" 
* title = "ELGA_AuditEventID" 
* status = #active 
* content = #complete 
* version = "3.1" 
* description = "**Beschreibung:** Codeliste der ELGA spezifischen Audit Event IDs. Verwendung in IHE konformen und ELGA BeS definierten Audit Events." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.150" 
* date = "2015-06-01" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* count = 8 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* concept[0].code = #0
* concept[0].display = "PAP Event"
* concept[0].designation[0].language = #de-AT 
* concept[0].designation[0].value = "PAP Event" 
* concept[0].property[0].code = #child 
* concept[0].property[0].valueCode = #1 
* concept[0].property[1].code = #child 
* concept[0].property[1].valueCode = #2 
* concept[1].code = #1
* concept[1].display = "Generelle Policyverwaltung"
* concept[1].designation[0].language = #de-AT 
* concept[1].designation[0].value = "Generelle Policyverwaltung" 
* concept[1].property[0].code = #parent 
* concept[1].property[0].valueCode = #0 
* concept[2].code = #2
* concept[2].display = "Patientenzustimmung"
* concept[2].designation[0].language = #de-AT 
* concept[2].designation[0].value = "Patientenzustimmung" 
* concept[2].property[0].code = #parent 
* concept[2].property[0].valueCode = #0 
* concept[3].code = #10
* concept[3].display = "Kontaktbestätigung"
* concept[3].designation[0].language = #de-AT 
* concept[3].designation[0].value = "Kontaktbestätigung" 
* concept[4].code = #20
* concept[4].display = "ELGA Token Service"
* concept[4].designation[0].language = #de-AT 
* concept[4].designation[0].value = "ELGA Token Service" 
* concept[5].code = #30
* concept[5].display = "Dokumentenverwaltung"
* concept[5].designation[0].language = #de-AT 
* concept[5].designation[0].value = "Dokumentenverwaltung" 
* concept[6].code = #40
* concept[6].display = "e-Medikation"
* concept[6].designation[0].language = #de-AT 
* concept[6].designation[0].value = "e-Medikation" 
* concept[7].code = #90
* concept[7].display = "Administration"
* concept[7].designation[0].language = #de-AT 
* concept[7].designation[0].value = "Administration" 
