Instance: elga-medikationpharmazeutischeempfehlungstatus 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-medikationpharmazeutischeempfehlungstatus" 
* name = "elga-medikationpharmazeutischeempfehlungstatus" 
* title = "ELGA_MedikationPharmazeutischeEmpfehlungStatus" 
* status = #active 
* version = "3.0" 
* description = "**Description:** ELGA ValueSet for Pharmaceutical Advice status

**Beschreibung:** ELGA ValueSet für Pharmazeutische Empfehlung Status" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.71" 
* date = "2015-03-31" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-ihe-pharmaceutical-advice-status-list"
* compose.include[0].concept[0].code = #CANCEL
* compose.include[0].concept[0].display = "Storno/Absetzen"
* compose.include[0].concept[1].code = #CHANGE
* compose.include[0].concept[1].display = "Änderung"

* expansion.timestamp = "2022-09-13T14:17:34.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem-ihe-pharmaceutical-advice-status-list"
* expansion.contains[0].code = #CANCEL
* expansion.contains[0].display = "Storno/Absetzen"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem-ihe-pharmaceutical-advice-status-list"
* expansion.contains[1].code = #CHANGE
* expansion.contains[1].display = "Änderung"
