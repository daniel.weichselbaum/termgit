Instance: ems-material 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-material" 
* name = "ems-material" 
* title = "EMS_Material" 
* status = #active 
* content = #complete 
* version = "2.0.0" 
* description = "**Beschreibung:** Liste der Probenmaterialien für EMS Labormeldungen" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.58" 
* date = "2020-11-06" 
* count = 65 
* #ABSTRICH "Abstriche"
* #ABSTRICH ^definition = Abstriche
* #ABSZESS "Abszess"
* #ABSZESS ^definition = Abszess
* #ANAL "Rektalabstrich"
* #ANAL ^definition = Analabstrich | Rektalabstrich | Stuhl/Analabstrich
* #ASCITES "Ascites"
* #ASCITES ^definition = Ascites
* #BIOPSIE "Biopsiematerial"
* #BIOPSIE ^definition = Biopsate | Biopsie (periphere Nerven) | Biopsiematerial
* #BLOOD "Blut"
* #BLOOD ^definition = Blut | Blut (DNA/Leukozyten)
* #BLOODAUS "Blutausstrich"
* #BLOODAUS ^definition = Blutausstrich
* #BLOODDRY "getrockneter Bluttropfen"
* #BLOODDRY ^definition = getrockneter Bluttropfen
* #BLOODFULL "Vollblut"
* #BLOODFULL ^definition = Vollblut
* #BLOODPOST "Leichenblut"
* #BLOODPOST ^definition = Leichenblut
* #BRONCHLAV "Bronchiallavage"
* #BRONCHLAV ^definition = Bronchiallavage
* #BRONCHSEKR "Bronchialsekret"
* #BRONCHSEKR ^definition = Bronchialsekret
* #CERVSWAB "Cervicalabstrich"
* #CERVSWAB ^definition = Cervicalabstrich
* #CSF "Liquor"
* #CSF ^definition = Liquor
* #EDTA "EDTA-Blut"
* #EDTA ^definition = EDTA-Blut
* #EDTAPLAS "EDTA-Blut/Plas,a"
* #EDTAPLAS ^definition = EDTA Blut/Plasma
* #EITER "Eiter"
* #EITER ^definition = Eiter
* #EJAKULAT "Ejakulat"
* #EJAKULAT ^definition = Ejakulat
* #ERBR "Erbrochenes"
* #ERBR ^definition = Erbrochenes
* #EXSUPERI "Exsudat Peritoneum"
* #EXSUPLEURA "Exsudat Pleura"
* #GALLE "Gallenflüssigkeit"
* #GALLE ^definition = Gallenflüssigkeit
* #GELENK "Gelenkflüssigkeit"
* #GELENK ^definition = Gelenkflüssigkeit
* #GEWEBE "Gewebe"
* #GEWEBE ^definition = Gewebeprobe | Gewebeproben | Epithelabstrich
* #GURGELAT "Gurgelat"
* #HAUT "Haut"
* #HAUT ^definition = Abstrich von Hautläsionen | Haut | Hautbiopsie oder Aspirat von Purpura/Petechien | verdächtige Hautareale
* #HIRN "Hirn"
* #HIRN ^definition = Hirnbiopsie/ -gewebe | Hirnbiopsie-Gewebe | Hirngewebe
* #ISOLAT "Isolat"
* #ISOLAT ^definition = Bakterienisolat | Isolat | Isolate
* #KNOCHENMARK "Knochenmark"
* #KNOCHENMARK ^definition = Knochenmark
* #KRUSTE "Kruste"
* #KRUSTE ^definition = Kruste
* #LAVAGE "Lavage"
* #LAVAGE ^definition = Lavage
* #LEBER "Leberbiopsie"
* #LEBER ^definition = Leberbiopsie
* #LUNGE "Lunge"
* #LUNGE ^definition = Lunge | Pleurapunktat
* #LYMPH "Lymphknoten"
* #LYMPH ^definition = Punktat von Lymphknoten | Resektionsmaterial aus Lymphknoten
* #MAGENSAFT "Magensaft"
* #MEMBRAN "Membran"
* #MEMBRAN ^definition = Membran
* #MILZ "Milz"
* #MILZ ^definition = Milz
* #NACKENHAUT "Nackenhautbiopsie"
* #NACKENHAUT ^definition = Nackenhautbiopsie
* #NASALSWAB "Nasenabstrich"
* #NASALSWAB ^definition = Nasenabstrich
* #NASALTHROATSWAB "Rachenabstrich"
* #NASALTHROATSWAB ^definition = Abstrich Nasen-Rachenschleimhaut | Nasen- oder Nasen-/Rachenabstrich
* #NEWBORNSWAB "Neugeborenenabstrich"
* #NEWBORNSWAB ^definition = Neugeborenenabstrich
* #OPMATERIAL "Operationsmaterial"
* #OPMATERIAL ^definition = intraoperatives Material | Operationsmaterial
* #OTHER "anderes"
* #OTHER ^definition = anderes
* #OTHERBODY "andere erregerhaltige Körperflüssigkeiten"
* #OTHERBODY ^definition = andere erregerhaltige Körperflüssigkeiten
* #OTHERNOTSTERILE "andere nicht sterile Probe"
* #OTHERNOTSTERILE ^definition = andere nicht sterile Probe | andere nicht-sterile Probe
* #OTHERSTERILE "andere sterile Probe"
* #OTHERSTERILE ^definition = andere sterile Probe | andere sterile Stelle
* #OTHERSWAB "andere Abstriche"
* #OTHERSWAB ^definition = andere Abstriche
* #PLASMA "Plasma"
* #PLASMA ^definition = Plasma
* #POSTMORTEM "post mortem-Material"
* #POSTMORTEM ^definition = post mortem-Material | Proben von Verstorbenen
* #PUNKTAT "Punktionsmaterial"
* #PUNKTAT ^definition = Punktionsmaterial
* #RESPSEKR "Respiratorische Sekrete"
* #RESPSEKR ^definition = Respiratorische Sekrete
* #RNA "nCoV RNA"
* #SEKRETE "Sekrete"
* #SEKRETE ^definition = Sekrete
* #SERUM "Serum"
* #SERUM ^definition = Serum
* #SONST "sonstiges"
* #SONST ^definition = sonstiges
* #SPEICHEL "Speichel"
* #SPEICHEL ^definition = oral Fluid | Speichel | Speichel/oral fluid
* #SPRITZE "Spritzeninhalt"
* #SPRITZE ^definition = Spritzeninhalt
* #SPUTUM "Sputum"
* #SPUTUM ^definition = Sputum | tiefes respiratorisches Sekret
* #STUHL "Stuhl"
* #STUHL ^definition = Stuhl
* #THROATSWAB "Rachenabstrich"
* #THROATSWAB ^definition = Rachenabstrich
* #THROATSWABFLUSS "Rachenspülflüssigkeit Serum"
* #THROATSWABFLUSS ^definition = Rachenspülflüssigkeit Serum
* #TONS "Tonsillenabstrich"
* #TONS ^definition = Tonsillenabstrich
* #UMGEB "Umgebungsprobe"
* #UMGEB ^definition = Umgebungsprobe
* #URINE "Harn"
* #URINE ^definition = Harn | Urin
* #WUND "Wundsekret"
* #WUND ^definition = Wundabstrich | Wundsekret
