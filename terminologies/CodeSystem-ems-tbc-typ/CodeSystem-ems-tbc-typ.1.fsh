Instance: ems-tbc-typ 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-tbc-typ" 
* name = "ems-tbc-typ" 
* title = "EMS_TBC_Typ" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** Species (alte Bezeichnung dieser Liste: EMS_Species)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.94" 
* date = "2017-01-26" 
* count = 9 
* concept[0].code = #MAFRICA
* concept[0].display = "M. africanum"
* concept[1].code = #MBOVIS
* concept[1].display = "M. bovis ssp. bovis"
* concept[2].code = #MBOVISBCG
* concept[2].display = "M. bovis BCG"
* concept[3].code = #MCANETTI
* concept[3].display = "M. canetti"
* concept[4].code = #MCAPRAE
* concept[4].display = "M. caprae"
* concept[5].code = #MMICROTI
* concept[5].display = "M. microti"
* concept[6].code = #MTUBECOMP
* concept[6].display = "M. tuberculosis complex (andere)"
* concept[7].code = #MTUBECOMPOTHER
* concept[7].display = "M. tuberculosis complex (nicht differenziert)"
* concept[8].code = #MTUBe
* concept[8].display = "M. tuberculosis"
