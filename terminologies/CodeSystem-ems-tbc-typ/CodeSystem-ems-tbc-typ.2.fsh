Instance: ems-tbc-typ 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-tbc-typ" 
* name = "ems-tbc-typ" 
* title = "EMS_TBC_Typ" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** Species (alte Bezeichnung dieser Liste: EMS_Species)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.94" 
* date = "2017-01-26" 
* count = 9 
* #MAFRICA "M. africanum"
* #MBOVIS "M. bovis ssp. bovis"
* #MBOVISBCG "M. bovis BCG"
* #MCANETTI "M. canetti"
* #MCAPRAE "M. caprae"
* #MMICROTI "M. microti"
* #MTUBECOMP "M. tuberculosis complex (andere)"
* #MTUBECOMPOTHER "M. tuberculosis complex (nicht differenziert)"
* #MTUBe "M. tuberculosis"
