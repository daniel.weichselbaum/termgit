Instance: ems-betreuung 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-ems-betreuung" 
* name = "ems-betreuung" 
* title = "EMS_Betreuung" 
* status = #active 
* version = "202010" 
* description = "**Description:** Contains information protected by copyright of SNOMED International. Any use of SNOMED CT in Austria requires a valid affiliate license or sublicense. The corresponding license is free of charge, provided that the use only takes place in Austria and fulfills the conditions of the Affiliate License Agreement. Affiliate licenses can be requested directly from the respective NRC via the [Member Licensing and Distribution Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS).

**Beschreibung:** Valueset mit Betreuungsarten. Enthält durch SNOMED International urheberrechtlich geschützte Information. Jede Verwendung von SNOMED CT in Österreich erfordert eine aufrechte Affiliate Lizenz oder eine Sublizenz. Die entsprechende Lizenz ist kostenlos, vorausgesetzt die Verwendung findet nur in Österreich statt und erfüllt die Bedingungen des Affiliate License Agreements. Affiliate Lizenzen können über das [Mitglieder-Lizenzierungs- und Distributions-Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS) direkt beim jeweiligen NRC beantragt werden." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.31" 
* date = "2020-10-27" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* compose.include[0].concept[0].code = "303119007"
* compose.include[0].concept[0].display = "Person in the community environment (person)"
* compose.include[1].system = "https://termgit.elga.gv.at/CodeSystem-ems-betreuung"
* compose.include[1].concept[0].code = "Kindergarten"
* compose.include[1].concept[0].display = "Kindergarten"
* compose.include[1].concept[1].code = "Schule"
* compose.include[1].concept[1].display = "Schule"
* compose.include[1].concept[2].code = "Seniorenheim"
* compose.include[1].concept[2].display = "Seniorenheim"
* compose.include[2].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* compose.include[2].concept[0].code = "303118004"
* compose.include[2].concept[0].display = "Person in the healthcare environment (person)"
* compose.include[3].system = "https://termgit.elga.gv.at/CodeSystem-ems-betreuung"
* compose.include[3].concept[0].code = "Pflegeheim"
* compose.include[3].concept[0].display = "Pflegeheim"
* compose.include[4].system = "https://termgit.elga.gv.at/CodeSystem-ems-nachweis"
* compose.include[4].concept[0].code = "NA"
* compose.include[4].concept[0].display = "nicht anwendbar"
