Instance: elga-medikationrezeptart 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-medikationrezeptart" 
* name = "elga-medikationrezeptart" 
* title = "ELGA_MedikationRezeptart" 
* status = #active 
* version = "3.0" 
* description = "**Description:** ELGA ValueSet for Prescription type

**Beschreibung:** ELGA ValueSet für RezeptArt" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.68" 
* date = "2015-03-31" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-medikationrezeptart"
* compose.include[0].concept[0].code = "KASSEN"
* compose.include[0].concept[0].display = "Kassenrezept"
* compose.include[0].concept[1].code = "PRIVAT"
* compose.include[0].concept[1].display = "Privatrezept"
* compose.include[0].concept[2].code = "SUBST"
* compose.include[0].concept[2].display = "Substitutionsrezept"
