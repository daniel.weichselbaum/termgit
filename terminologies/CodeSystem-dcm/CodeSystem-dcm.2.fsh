Instance: dcm 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-dcm" 
* name = "dcm" 
* title = "DCM" 
* status = #active 
* content = #complete 
* version = "2015" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.840.10008.2.16.4" 
* date = "2015-12-01" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://dicom.nema.org/medical/Dicom/2015b/output/chtml/part16/chapter_D.html#DCM_121181" 
* count = 3151 
* #109001 "Digital timecode (NOS)"
* #109001 ^definition = A signal transmitted for the purpose of interchange of the current time, not specific to any source or methodology.
* #109002 "ECG-based gating signal, processed"
* #109002 ^definition = A signal that is generated for each detection of a heart beat.
* #109003 "IRIG-B timecode"
* #109003 ^definition = A signal transmitted by the Inter-Range Instrumentation Group for the purpose of synchronizing time clocks.
* #109004 "X-Ray Fluoroscopy On Signal"
* #109004 ^definition = A signal that indicates that X-Ray source has been activated for fluoroscopy use.
* #109005 "X-Ray On Trigger"
* #109005 ^definition = A signal that indicated that the X-Ray source has been activated for image recording.
* #109006 "Differential signal"
* #109006 ^definition = An electrical signal derived from two electrodes.
* #109007 "His bundle electrogram"
* #109007 ^definition = An electrophysiological recording from the HIS nerve bundle.
* #109008 "Monopole signal"
* #109008 ^definition = An electrical signal from one electrode relative to an indifferent potential.
* #109009 "Pacing (electrical) stimulus, voltage"
* #109009 ^definition = The voltage stimulus during cardiac pacing.
* #109010 "Radio frequency ablation, power"
* #109010 ^definition = The power injected during RF ablation procedure.
* #109011 "Voltage measurement by basket catheter"
* #109011 ^definition = Electrophysiological signals acquired using a multi-splined catheter each equipped with multiple electrodes.
* #109012 "Voltage measurement by mapping catheter"
* #109012 ^definition = Electrophysiological signals acquired using a steerable catheter.
* #109013 "Voltage measurement, NOS"
* #109013 ^definition = A voltage measurement not otherwise specified.
* #109014 "35% of thermal CO"
* #109014 ^definition = A signal point that is 35% of the peak thermal cardiac output signal.
* #109015 "70% of thermal CO"
* #109015 ^definition = A signal point that is 70% of the peak thermal cardiac output signal.
* #109016 "A wave peak pressure"
* #109016 ^definition = The peak pressure of each heart beat in the atrium caused by the atrial contraction.
* #109017 "A wave pressure, average"
* #109017 ^definition = The average of several A wave pressure measurements.
* #109018 "Beat detected (accepted)"
* #109018 ^definition = An identified cardiac beat used in the determination of a measurement.
* #109019 "Beat detected (rejected)"
* #109019 ^definition = An identified cardiac beat not used in the determination of a measurement.
* #109020 "Diastolic pressure, average"
* #109020 ^definition = The average of several diastolic pressure measurements
* #109020 ^designation[0].language = #de-AT 
* #109020 ^designation[0].value = "Retired. Replaced by (F-00E22, SRT, 'Average diastolic blood pressure')" 
* #109021 "Diastolic pressure nadir"
* #109021 ^definition = The lowest pressure value excluding any undershoot artifact.
* #109021 ^designation[0].language = #de-AT 
* #109021 ^designation[0].value = "Retired. Replaced by (F-00E1F, SRT, 'Minimum diastolic blood pressure')" 
* #109022 "End diastole"
* #109022 ^definition = The moment at the end of the diastolic phase of the cardiac cycle.
* #109022 ^designation[0].language = #de-AT 
* #109022 ^designation[0].value = "Retired. Replaced by (F-32011, SRT, 'End diastole')" 
* #109023 "End of expiration"
* #109023 ^definition = The moment at the end of respiratory expiration.
* #109024 "End of inspiration"
* #109024 ^definition = The moment at the end of respiratory inspiration.
* #109025 "Max dp/dt"
* #109025 ^definition = The maximum positive rate of change of pressure.
* #109026 "Max neg dp/dt"
* #109026 ^definition = The maximum negative rate of change of pressure.
* #109027 "Mean blood pressure"
* #109027 ^definition = The average blood pressure value, generally over 2 or more seconds
* #109027 ^designation[0].language = #de-AT 
* #109027 ^designation[0].value = "Retired. Replaced by (F-31150, SRT, 'Mean blood pressure')" 
* #109028 "Peak of thermal cardiac output bolus"
* #109028 ^definition = The peak change in blood temperature during a thermal cardiac output measurement.
* #109029 "Start of expiration"
* #109029 ^definition = The moment respiratory expiration begins.
* #109030 "Start of inspiration"
* #109030 ^definition = The moment of respiratory inspiration begins.
* #109031 "Start of thermal cardiac output bolus"
* #109031 ^definition = The first discernible blood temperature change following the injectate during a thermal cardiac output measurement.
* #109032 "Systolic pressure, average"
* #109032 ^definition = The average of several systolic blood pressure measurements.
* #109032 ^designation[0].language = #de-AT 
* #109032 ^designation[0].value = "Retired. Replaced by (F-00E14, SRT, 'Average systolic blood pressure')" 
* #109033 "Systolic peak pressure"
* #109033 ^definition = The highest systolic blood pressure value excluding any overshoot artifact
* #109033 ^designation[0].language = #de-AT 
* #109033 ^designation[0].value = "Retired. Replaced by (F-00E11, SRT, 'Maximum systolic blood pressure')" 
* #109034 "V wave peak pressure"
* #109034 ^definition = The peak pressure of each heart beat in the atrium caused by the filling of the atrium.
* #109035 "V wave pressure, average"
* #109035 ^definition = The average of several V wave pressure measurements.
* #109036 "Valve close"
* #109036 ^definition = The moment at which a heart valve closes.
* #109037 "Valve open"
* #109037 ^definition = The moment at which a heart valve opens.
* #109038 "Ablation off"
* #109038 ^definition = The moment when RF ablation current is turned off.
* #109039 "Ablation on"
* #109039 ^definition = The moment when RF ablation current is turned on.
* #109040 "HIS bundle wave"
* #109040 ^definition = The moment in the cardiac cycle when the HIS bundle nerves depolarize.
* #109041 "P wave"
* #109041 ^definition = The surface electrocardiogram of the atrial contraction.
* #109042 "Q wave"
* #109042 ^definition = The first negative deflection of the electrocardiogram cause by ventricular depolarization.
* #109043 "R wave"
* #109043 ^definition = The first positive deflection the electrocardiogram cause by ventricular depolarization.
* #109044 "S wave"
* #109044 ^definition = The first negative deflection after the R wave.
* #109045 "Start of atrial contraction"
* #109045 ^definition = The beginning of the atrial contraction.
* #109046 "Start of atrial contraction (subsequent)"
* #109046 ^definition = The beginning of the second atrial contraction of two consecutive beats.
* #109047 "Stimulation at rate 1 interval"
* #109047 ^definition = The stimulation interval during cardiac stimulation first used in a pacing train.
* #109048 "Stimulation at rate 2 interval"
* #109048 ^definition = The stimulation interval different from the first stimulation interval used in a pacing train.
* #109049 "Stimulation at rate 3 interval"
* #109049 ^definition = A stimulation interval different from and subsequent to the second interval in a pacing train.
* #109050 "Stimulation at rate 4 interval"
* #109050 ^definition = Describes a stimulation interval different from and subsequent to the third interval in a pacing train.
* #109051 "T wave"
* #109051 ^definition = The electrocardiogram deflection caused by ventricular repolarization.
* #109052 "V wave"
* #109052 ^definition = The peak pressure of each heart beat monitored in the atrium caused by the filling of the atrium.
* #109053 "V wave of next beat"
* #109053 ^definition = The second V wave measurement of two consecutive beats.
* #109054 "Patient State"
* #109054 ^definition = A description of the physiological condition of the patient.
* #109055 "Protocol Stage"
* #109055 ^definition = The exercise level during a progressive cardiac stress test.
* #109056 "Stress Protocol"
* #109056 ^definition = A series of physiological challenges designed to progressively increase the work of the heart.
* #109057 "Catheterization Procedure Phase"
* #109057 ^definition = A subpart of a cardiac catheterization procedure
* #109057 ^designation[0].language = #de-AT 
* #109057 ^designation[0].value = "Retired. Replaced by (G-72BB, SRT, 'Catheterization Procedure Phase')" 
* #109058 "Contrast Phase"
* #109058 ^definition = The subpart of a cardiac catheterization procedure in which a radio-opaque contrast medium is injected into the patient.
* #109059 "Physiological challenges"
* #109059 ^definition = Physical changes administered to a patient in order to elicit an physiological response.
* #109060 "Procedure Step Number"
* #109060 ^definition = Enumeration of a subpart of a catheterization procedure.
* #109061 "EP Procedure Phase"
* #109061 ^definition = A subpart of en electrophysiological procedure.
* #109063 "Pulse train definition"
* #109063 ^definition = A means of defining a series of cardiac stimulation pulses.
* #109070 "End of systole"
* #109070 ^designation[0].language = #de-AT 
* #109070 ^designation[0].value = "Retired. Replaced by (R-FAB5B, SRT, 'End systole')" 
* #109071 "Indicator mean transit time"
* #109071 ^definition = Time for a median particle to travel from point of injection to point of detection.
* #109072 "Tau"
* #109072 ^definition = The time constant of isovolumic pressure fall.
* #109073 "V max myocardial"
* #109073 ^definition = Maximum velocity of myocardial contractility.
* #109080 "Real time acquisition"
* #109080 ^definition = Total time for the acquisition is shorter than cardiac cycle, no gating is applied; see Cardiac Synchronization Technique (0018,9037).
* #109081 "Prospective gating"
* #109081 ^definition = Certain thresholds have been set for a gating window that defines the acceptance of measurement data during the acquisition; see Cardiac Synchronization Technique (0018,9037).
* #109082 "Retrospective gating"
* #109082 ^definition = Certain thresholds have been set for a gating window that defines the acceptance of measurement data after the acquisition; see Cardiac Synchronization Technique (0018,9037).
* #109083 "Paced"
* #109083 ^definition = There is a constant RR interval, which makes thresholding not required; see Cardiac Synchronization Technique (0018,9037). E.g., Pacemaker.
* #109091 "Cardiac Stress State"
* #109091 ^definition = Imaging after injection of tracer during increased cardiac workload or increased myocardial blood flow, achieved by either exercise or pharmacologic means.
* #109091 ^designation[0].language = #de-AT 
* #109091 ^designation[0].value = "Retired. Replaced by (F-05019, SRT, 'Cardiac stress state')." 
* #109092 "Reinjection State"
* #109092 ^definition = Imaging after injection of additional tracer under resting conditions.
* #109093 "Redistribution State"
* #109093 ^definition = Imaging after allowing a moderate amount of time for tracer to move from its initial sites of uptake. Example: For Thallium imaging this would correspond to imaging 2-6 hours after injection.
* #109094 "Delayed Redistribution State"
* #109094 ^definition = Imaging after allowing an extended amount of time for tracer to move from its initial sites of uptake. Example: For Thallium imaging this would correspond to imaging more than 6 hours after injection.
* #109095 "Peak stress state"
* #109095 ^definition = Peak Cardiac stress state
* #109095 ^designation[0].language = #de-AT 
* #109095 ^designation[0].value = "Retired. Replaced by (F-05028, SRT, 'Peak stress state')" 
* #109096 "Recovery state"
* #109096 ^definition = Recovery from cardiac stress
* #109096 ^designation[0].language = #de-AT 
* #109096 ^designation[0].value = "Retired. Replaced by (F-05018, SRT, 'Cardiac stress Recovery state')" 
* #109101 "Acquisition Equipment"
* #109101 ^definition = Equipment that originally acquired the data stored within composite instances. E.g., a CT, MR or Ultrasound modality.
* #109102 "Processing Equipment"
* #109102 ^definition = Equipment that has processed composite instances to create new composite instances. E.g., a 3D Workstation.
* #109103 "Modifying Equipment"
* #109103 ^definition = Equipment that has modified existing composite instances (without creating new composite instances). E.g., a QA Station or Archive.
* #109104 "De-identifying Equipment"
* #109104 ^definition = Equipment that has modified an existing composite instance to remove patient identifying information.
* #109105 "Frame Extracting Equipment"
* #109105 ^definition = Equipment that has processed composite instances to create new composite instances by extracting selected frames from the original instance.
* #109106 "Enhanced Multi-frame Conversion Equipment"
* #109106 ^definition = Equipment that has processed composite instances to create new composite instances by converting classic single frame images to enhanced multi-frame image, or vice versa and updating other instances to maintain referential integrity.
* #109110 "Voice"
* #109110 ^definition = The sound of a human's speech, recorded during a procedure.
* #109110 ^designation[0].language = #de-AT 
* #109110 ^designation[0].value = "May include the patient's voice, or the voice of staff present in the room, or an operator's voice (whether for the purpose of recording a narrative accompanying a procedure or not)." 
* #109111 "Operator's narrative"
* #109111 ^definition = The voice of a device operator, recorded during a procedure.
* #109112 "Ambient room environment"
* #109112 ^definition = The ambient sound recorded during a procedure, which may or may not include voice and other types of sound.
* #109113 "Doppler audio"
* #109113 ^definition = The Doppler waveform recorded as an audible signal.
* #109114 "Phonocardiogram"
* #109114 ^definition = The sound of the human heart beating.
* #109114 ^designation[0].language = #de-AT 
* #109114 ^designation[0].value = "Such as might be recorded from an electronic stethoscope." 
* #109115 "Physiological audio signal"
* #109115 ^definition = Any sound made by the human body.
* #109115 ^designation[0].language = #de-AT 
* #109115 ^designation[0].value = "May include the sound of the heart, but also sound from other organs, such as bowel sounds or bruits from vessels, or sounds of respiration. Not intended to include voice." 
* #109116 "Arterial Pulse Waveform"
* #109116 ^definition = A digitized signal from the patient arterial system collected through pulse oximetry or other means.
* #109117 "Respiration Waveform"
* #109117 ^definition = A digitized signal from the patient respiratory system representing respiration.
* #109120 "On admission to unit"
* #109120 ^definition = The occasion on which a procedure was performed on admission to a specialist unit. E.g., intensive care.
* #109121 "On discharge"
* #109121 ^definition = The occasion on which a procedure was performed on discharge from hospital as an in-patient.
* #109122 "On discharge from unit"
* #109122 ^definition = The occasion on which a procedure was performed on discharge from a specialist unit. E.g., intensive care.
* #109123 "Pre-intervention"
* #109123 ^definition = The occasion on which a procedure was performed immediately prior to non-surgical intervention. E.g, percutaneous angioplasty, biopsy.
* #109124 "Post-intervention"
* #109124 ^definition = The occasion on which a procedure was performed immediately after to non-surgical intervention. E.g, percutaneous angioplasty, biopsy.
* #109125 "At last appointment"
* #109125 ^definition = The occasion on which a procedure was performed at the most recent outpatient visit.
* #109132 "Joint position method"
* #109132 ^definition = The active or passive joint positioning during acquisition.
* #109133 "Physical force"
* #109133 ^definition = A physical force applied during acquisition.
* #109134 "Prior to voiding"
* #109134 ^definition = Prior to voiding.
* #109135 "Post voiding"
* #109135 ^definition = Post voiding.
* #109136 "Neutral musculoskeletal position"
* #109136 ^definition = Neutral musculoskeletal position.
* #109200 "America Kennel Club"
* #109200 ^definition = America Kennel Club.
* #109201 "America's Pet Registry Inc."
* #109201 ^definition = America's Pet Registry Inc.
* #109202 "American Canine Association"
* #109202 ^definition = American Canine Association.
* #109203 "American Purebred Registry"
* #109203 ^definition = American Purebred Registry.
* #109204 "American Rare Breed Association"
* #109204 ^definition = American Rare Breed Association.
* #109205 "Animal Registry Unlimited"
* #109205 ^definition = Animal Registry Unlimited.
* #109206 "Animal Research Foundation"
* #109206 ^definition = Animal Research Foundation.
* #109207 "Canadian Border Collie Association"
* #109207 ^definition = Canadian Border Collie Association.
* #109208 "Canadian Kennel Club"
* #109208 ^definition = Canadian Kennel Club.
* #109209 "Canadian Livestock Records Association"
* #109209 ^definition = Canadian Livestock Records Association.
* #109210 "Canine Federation of Canada"
* #109210 ^definition = Canine Federation of Canada.
* #109211 "Continental Kennel Club"
* #109211 ^definition = Continental Kennel Club.
* #109212 "Dog Registry of America"
* #109212 ^definition = Dog Registry of America.
* #109213 "Federation of International Canines"
* #109213 ^definition = Federation of International Canines.
* #109214 "International Progressive Dog Breeders' Alliance"
* #109214 ^definition = International Progressive Dog Breeders' Alliance.
* #109215 "National Kennel Club"
* #109215 ^definition = National Kennel Club.
* #109216 "North American Purebred Dog Registry"
* #109216 ^definition = North American Purebred Dog Registry.
* #109217 "United All Breed Registry"
* #109217 ^definition = United All Breed Registry.
* #109218 "United Kennel Club"
* #109218 ^definition = United Kennel Club.
* #109219 "Universal Kennel Club International"
* #109219 ^definition = Universal Kennel Club International.
* #109220 "Working Canine Association of Canada"
* #109220 ^definition = Working Canine Association of Canada.
* #109221 "World Kennel Club"
* #109221 ^definition = World Kennel Club.
* #109222 "World Wide Kennel Club"
* #109222 ^definition = World Wide Kennel Club.
* #109701 "Overall image quality evaluation"
* #109701 ^definition = Evaluation of overall image quality as described in section 7.3.2 of [IEC 62563-1].
* #109702 "Grayscale resolution evaluation"
* #109702 ^definition = Visual verification of sufficient grayscale resolution based on 8 and 10-bit markers as described in section 7.3.3 of [IEC 62563-1].
* #109703 "Luminance response evaluation"
* #109703 ^definition = Visual evaluation of luminance response using the TG18-CT test pattern as described in section 7.3.4 of [IEC 62563-1].
* #109704 "Luminance uniformity evaluation"
* #109704 ^definition = Visual detection of luminance non-uniformities as described in section 7.3.5 of [IEC 62563-1].
* #109705 "Chromaticity evaluation"
* #109705 ^definition = Visual verification of color uniformity as described in section 7.3.6 of [IEC 62563-1].
* #109706 "Pixel faults evaluation"
* #109706 ^definition = Visual detection of defective pixels on dark (TG18-UN80) and bright (TG18-UN10) images as described in section 7.3.7 of [IEC 62563-1].
* #109707 "Veiling glare evaluation"
* #109707 ^definition = Visual evaluation of veiling glare by looking at low contrast objects on 2 test patterns as described in section 7.3.8 of [IEC 62563-1].
* #109708 "Geometrical image evaluation"
* #109708 ^definition = Visual evaluation of geometry, phase/clock correction and clipping as described in section 7.3.9 of [IEC 62563-1].
* #109709 "Angular viewing evaluation"
* #109709 ^definition = Visual evaluation of viewing angle as described in section 7.3.10 of [IEC 62563-1].
* #109710 "Clinical evaluation"
* #109710 ^definition = Visual evaluation of the appearance of clinical images as described in section 7.3.11 of [IEC 62563-1].
* #109801 "TG18-QC Pattern"
* #109801 ^definition = AAPM TG18-QC Pattern used for evaluation of resolution, luminance, distortion, artifacts.
* #109802 "TG18-BR Pattern"
* #109802 ^definition = AAPM TG18-BR Pattern used for the evaluation of the display of low-contrast, fine-detail image structures
* #109803 "TG18-PQC Pattern"
* #109803 ^definition = AAPM TG18-PQC Pattern used for evaluation of resolution, luminance, contrast transfer for prints.
* #109804 "TG18-CT Pattern"
* #109804 ^definition = AAPM TG18-CT Pattern used for evaluation of luminance response.
* #109805 "TG18-LN8-01 Pattern"
* #109805 ^definition = The 1st image in the AAPM TG18-LN8 set used for DICOM grayscale calibration.
* #109806 "TG18-LN8-02 Pattern"
* #109806 ^definition = The 2nd image in the AAPM TG18-LN8 set used for DICOM grayscale calibration.
* #109807 "TG18-LN8-03 Pattern"
* #109807 ^definition = The 3rd image in the AAPM TG18-LN8 set used for DICOM grayscale calibration.
* #109808 "TG18-LN8-04 Pattern"
* #109808 ^definition = The 4th image in the AAPM TG18-LN8 set used for DICOM grayscale calibration.
* #109809 "TG18-LN8-05 Pattern"
* #109809 ^definition = The 5th image in the AAPM TG18-LN8 set used for DICOM grayscale calibration.
* #109810 "TG18-LN8-06 Pattern"
* #109810 ^definition = The 6th image in the AAPM TG18-LN8 set used for DICOM grayscale calibration.
* #109811 "TG18-LN8-07 Pattern"
* #109811 ^definition = The 7th image in the AAPM TG18-LN8 set used for DICOM grayscale calibration.
* #109812 "TG18-LN8-08 Pattern"
* #109812 ^definition = The 8th image in the AAPM TG18-LN8 set used for DICOM grayscale calibration.
* #109813 "TG18-LN8-09 Pattern"
* #109813 ^definition = The 9th image in the AAPM TG18-LN8 set used for DICOM grayscale calibration.
* #109814 "TG18-LN8-10 Pattern"
* #109814 ^definition = The 10th image in the AAPM TG18-LN8 set used for DICOM grayscale calibration series.
* #109815 "TG18-LN8-11 Pattern"
* #109815 ^definition = The 11th image in the AAPM TG18-LN8 set used for DICOM grayscale calibration.
* #109816 "TG18-LN8-12 Pattern"
* #109816 ^definition = The 12th image in the AAPM TG18-LN8 set used for DICOM grayscale calibration.
* #109817 "TG18-LN8-13 Pattern"
* #109817 ^definition = The 13th image in the AAPM TG18-LN8 set used for DICOM grayscale calibration.
* #109818 "TG18-LN8-14 Pattern"
* #109818 ^definition = The 14th image in the AAPM TG18-LN8 set used for DICOM grayscale calibration.
* #109819 "TG18-LN8-15 Pattern"
* #109819 ^definition = The 15th image in the AAPM TG18-LN8 set used for DICOM grayscale calibration.
* #109820 "TG18-LN8-16 Pattern"
* #109820 ^definition = The 16th image in the AAPM TG18-LN8 set used for DICOM grayscale calibration.
* #109821 "TG18-LN8-17 Pattern"
* #109821 ^definition = The 17th image in the AAPM TG18-LN8 set used for DICOM grayscale calibration.
* #109822 "TG18-LN8-18 Pattern"
* #109822 ^definition = The 18th image in the AAPM TG18-LN8- set used for DICOM grayscale calibration.
* #109823 "TG18-LN12-01 Pattern"
* #109823 ^definition = The 1st image in the AAPM TG18-LN12 set used for DICOM grayscale calibration.
* #109824 "TG18-LN12-02 Pattern"
* #109824 ^definition = The 2 nd image in the AAPM TG18-LN12 set used for DICOM grayscale calibration.
* #109825 "TG18-LN12-03 Pattern"
* #109825 ^definition = The 3rd image in the AAPM TG18-LN12 set used for DICOM grayscale calibration.
* #109826 "TG18-LN12-04 Pattern"
* #109826 ^definition = The 4th image in the AAPM TG18-LN12 set used for DICOM grayscale calibration.
* #109827 "TG18-LN12-05 Pattern"
* #109827 ^definition = The 5th image in the AAPM TG18-LN12 set used for DICOM grayscale calibration.
* #109828 "TG18-LN12-06 Pattern"
* #109828 ^definition = The 6th image in the AAPM TG18-LN12 set used for DICOM grayscale calibration.
* #109829 "TG18-LN12-07 Pattern"
* #109829 ^definition = The 7th image in the AAPM TG18-LN12 set used for DICOM grayscale calibration.
* #109830 "TG18-LN12-08 Pattern"
* #109830 ^definition = The 8th image in the AAPM TG18-LN12 set used for DICOM grayscale calibration.
* #109831 "TG18-LN12-09 Pattern"
* #109831 ^definition = The 9th image in the AAPM TG18-LN12 set used for DICOM grayscale calibration.
* #109832 "TG18-LN12-10 Pattern"
* #109832 ^definition = The 10th image in the AAPM TG18-LN12 set used for DICOM grayscale calibration.
* #109833 "TG18-LN12-11 Pattern"
* #109833 ^definition = The 11th image in the AAPM TG18-LN12 set used for DICOM grayscale calibration.
* #109834 "TG18-LN12-12 Pattern"
* #109834 ^definition = The 12th image in the AAPM TG18-LN12 set used for DICOM grayscale calibration.
* #109835 "TG18-LN12-13 Pattern"
* #109835 ^definition = The 13th image in the AAPM TG18-LN12 set used for DICOM grayscale calibration.
* #109836 "TG18-LN12-14 Pattern"
* #109836 ^definition = The 14th image in the AAPM TG18-LN12 set used for DICOM grayscale calibration.
* #109837 "TG18-LN12-15 Pattern"
* #109837 ^definition = The 15th image in the AAPM TG18-LN12 set used for DICOM grayscale calibration.
* #109838 "TG18-LN12-16 Pattern"
* #109838 ^definition = The 16th image in the AAPM TG18-LN12 set used for DICOM grayscale calibration.
* #109839 "TG18-LN12-17 Pattern"
* #109839 ^definition = The 17th image in the AAPM TG18-LN12 set used for DICOM grayscale calibration.
* #109840 "TG18-LN12-18 Pattern"
* #109840 ^definition = The 18th image in the AAPM TG18-LN12 set used for DICOM grayscale calibration.
* #109841 "TG18-UN10 Pattern"
* #109841 ^definition = The AAPM TG18-UN10 Pattern used for evaluation of luminance and color uniformity, and angular response.
* #109842 "TG18-UN80 Pattern"
* #109842 ^definition = The AAPM TG18-UN80 Pattern used for evaluation of luminance and color uniformity, and angular response.
* #109843 "TG18-UNL10 Pattern"
* #109843 ^definition = The AAPM TG18-UNL10 Pattern is the AAPM TG-18 UN10 Pattern with added defining lines.
* #109844 "TG18-UNL80 Pattern"
* #109844 ^definition = The AAPM TG18-UNL80 Pattern is the AAPM TG-18 UN80 Pattern with added defining lines.
* #109845 "TG18-AD Pattern"
* #109845 ^definition = The AAPM TG18-AD Pattern used for visual evaluation of the reflection of ambient light from the display.
* #109846 "TG18-MP Pattern"
* #109846 ^definition = The AAPM TG18-MP Pattern used for evaluation of Luminance response (bit-depth resolution).
* #109847 "TG18-RH10 Pattern"
* #109847 ^definition = The AAPM TG18-RH10 Pattern used for LSF-line spectra function-(1k and 2k) evaluation by 5 horizontal lines at 10% luminance level.
* #109848 "TG18-RH50 Pattern"
* #109848 ^definition = The AAPM TG18-RH50 Pattern used for LSF-line spectra function-(1k and 2k) evaluation by 5 horizontal lines at 50% luminance level.
* #109849 "TG18-RH89 Pattern"
* #109849 ^definition = The AAPM TG18-RH89 Pattern used for LSF-line spectra function-(1k and 2k) evaluation by 5 horizontal lines at 89% luminance level.
* #109850 "TG18-RV10 Pattern"
* #109850 ^definition = The AAPM TG18-RV10 Pattern used for LSF-line spectra function-(1k and 2k) evaluation by 5 vertical lines at 10% luminance level.
* #109851 "TG18-RV50 Pattern"
* #109851 ^definition = The AAPM TG18-RV50 Pattern used for LSF-line spectra function-(1k and 2k) evaluation by 5 vertical lines at 50% luminance level.
* #109852 "TG18-RV89 Pattern"
* #109852 ^definition = The AAPM TG18-RV89 Pattern used for LSF-line spectra function-(1k and 2k) evaluation by 5 vertical lines at 89% luminance level.
* #109853 "TG18-PX Pattern"
* #109853 ^definition = The AAPM TG18-PX Pattern used for the assessment of display resolution.
* #109854 "TG18-CX Pattern"
* #109854 ^definition = The AAPM TG18-CX Pattern used to assess display resolution and resolution uniformity.
* #109855 "TG18-LPH10 Pattern"
* #109855 ^definition = The AAPM TG18-LPH10 Pattern used to assess display resolution. This pattern has horizontal bars consisting of alternating single-pixel-wide lines across the faceplate of display. The lines have a 12% positive contrast against 10% background level of the maximum pixel value.
* #109856 "TG18-LPH50 Pattern"
* #109856 ^definition = The AAPM TG18-LPH50 Pattern used to assess display resolution. This pattern has horizontal bars consisting of alternating single-pixel-wide lines across the faceplate of display. The lines have a 50% positive contrast against 10% background level of the maximum pixel value.
* #109857 "TG18-LPH89 Pattern"
* #109857 ^definition = The AAPM TG18-LPH89 Pattern used to assess display resolution. This pattern has horizontal bars consisting of alternating single-pixel-wide lines across the faceplate of display. The lines have a 12% positive contrast against 89% background level of the maximum pixel value.
* #109858 "TG18-LPV10 Pattern"
* #109858 ^definition = The AAPM TG18-LPV10 Pattern used to assess display resolution. This pattern has vertical bars consisting of alternating single-pixel-wide lines across the faceplate of display. The lines have a 12% positive contrast against 10% background level of the maximum pixel value.
* #109859 "TG18-LPV50 Pattern"
* #109859 ^definition = The AAPM TG18-LPV50 Pattern used to assess display resolution. This pattern has vertical bars consisting of alternating single-pixel-wide lines across the faceplate of display. The lines have a 12% positive contrast against 50% background level of the maximum pixel value.
* #109860 "TG18-LPV89 Pattern"
* #109860 ^definition = The AAPM TG18-LPV89 Pattern used to assess display resolution. This pattern has vertical bars consisting of alternating single-pixel-wide lines across the faceplate of display. The lines have a 12% positive contrast against 89% background level of the maximum pixel value.
* #109861 "TG18-AFC Pattern"
* #109861 ^definition = The AAPM TG18-AFC Pattern used to assess display noise.
* #109862 "TG18-NS10 Pattern"
* #109862 ^definition = The AAPM TG18-NS10 Pattern is AAPM TG18-RV10/RH10 with only difference being the absence of the single line at the center of the measurement area.
* #109863 "TG18-NS50 Pattern"
* #109863 ^definition = The AAPM TG18-NS50 Pattern is AAPM TG18-RV50/RH50 with only difference being the absence of the single line at the center of the measurement area.
* #109864 "TG18-NS89 Pattern"
* #109864 ^definition = The AAPM TG18-NS89 Pattern is AAPM TG18-RV89/RH89 with only difference being the absence of the single line at the center of the measurement area.
* #109865 "TG18-GV Pattern"
* #109865 ^definition = The TG18-GV Pattern used to assess display veiling.
* #109866 "TG18-GVN Pattern"
* #109866 ^definition = The TG18-GVN Pattern used to assess display veiling. This pattern is identical to AAPM TG18-GV Pattern except that the large-diameter white circle is replaced with a black circle, creating a completely black pattern except for the presence of low-contrast targets.
* #109867 "TG18-GQ Pattern"
* #109867 ^definition = The TG18-GQ Pattern used for quantitative assessment of veiling glare. This pattern is identical to TG18-GV except that is lacks the central low-contrast objects.
* #109868 "TG18-GQN Pattern"
* #109868 ^definition = TG18-GQN Pattern used for the quantitative assessment of veiling glare. This pattern is identical to AAPM TG18-GQ Pattern except that the large-diameter white circle is replaced with a black circle, creating a completely black pattern except for the presence of low-contrast targets.
* #109869 "TG18-GQB Pattern"
* #109869 ^definition = The TG18-GQB Pattern used for the quantitative assessment of veiling glare. This pattern is identical to AAPM TG18-GQ Pattern except eliminating the central black circle.
* #109870 "TG18-GA03 Pattern"
* #109870 ^definition = The TG18-GA03 Pattern used for quantitative assessment of veiling glare. This pattern is identical to TG18-GQ except that the radius of the central black circle is varied as r = 3.
* #109871 "TG18-GA05 Pattern"
* #109871 ^definition = The TG18-GA05 Pattern This pattern is identical to TG18-GQ except that the radius of the central black circle is varied as r = 5.
* #109872 "TG18-GA08 Pattern"
* #109872 ^definition = The TG18-GA08 Pattern used for quantitative assessment of veiling glare. This pattern is identical to TG18-GQ except that the radius of the central black circle is varied as r = 8.
* #109873 "TG18-GA10 Pattern"
* #109873 ^definition = The TG18-GA10 Pattern used for quantitative assessment of veiling glare. This pattern is identical to TG18-GQ except that the radius of the central black circle is varied as r = 10.
* #109874 "TG18-GA15 Pattern"
* #109874 ^definition = The TG18-GA15 Pattern used for quantitative assessment of veiling glare. This pattern is identical to TG18-GQ except that the radius of the central black circle is varied as r = 15.
* #109875 "TG18-GA20 Pattern"
* #109875 ^definition = The TG18-GA20 Pattern used for quantitative assessment of veiling glare. This pattern is identical to TG18-GQ except that the radius of the central black circle is varied as r = 20.
* #109876 "TG18-GA25 Pattern"
* #109876 ^definition = The TG18-GA25 Pattern used for quantitative assessment of veiling glare. This pattern is identical to TG18-GQ except that the radius of the central black circle is varied as r = 25.
* #109877 "TG18-GA30 Pattern"
* #109877 ^definition = The TG18-GA30 Pattern used for quantitative assessment of veiling glare. This pattern is identical to TG18-GQ except that the radius of the central black circle is varied as r = 30.
* #109878 "TG18-CH Image"
* #109878 ^definition = The AAPM TG18-CH Image is a reference anatomical PA chest image.
* #109879 "TG18-KN Image"
* #109879 ^definition = The AAPM TG18-KN Image is a reference anatomical knee image.
* #109880 "TG18-MM1 Image"
* #109880 ^definition = The AAPM TG18-MM1 Image is a reference anatomical mammogram image.
* #109881 "TG18-MM2 Image"
* #109881 ^definition = The AAPM TG18-MM2 Image is a reference anatomical mammogram image.
* #109901 "OIQ Pattern"
* #109901 ^definition = The IEC OIQ Pattern is used as an alternative to the TG18-QC Pattern.
* #109902 "ANG Pattern"
* #109902 ^definition = The IEC ANG Pattern used for angular viewing evaluation.
* #109903 "GD Pattern"
* #109903 ^definition = The IEC GD Pattern used for geometrical image evaluation.
* #109904 "BN01 Pattern"
* #109904 ^definition = The IEC BN01 Pattern is used as analternative to the TG18-LN-01 Pattern, to avoid the use of a cone or baffle with LCDs.
* #109905 "BN02 Pattern"
* #109905 ^definition = The IEC BN02 Pattern is used as analternative to the TG18-LN-02 Pattern, to avoid the use of a cone or baffle with LCDs.
* #109906 "BN03 Pattern"
* #109906 ^definition = The IEC BN03 Pattern is used as analternative to the TG18-LN-03 Pattern, to avoid the use of a cone or baffle with LCDs.
* #109907 "BN04 Pattern"
* #109907 ^definition = The IEC BN04 Pattern is used as analternative to the TG18-LN-04 Pattern, to avoid the use of a cone or baffle with LCDs.
* #109908 "BN05 Pattern"
* #109908 ^definition = The IEC BN05 Pattern is used as analternative to the TG18-LN-05 Pattern, to avoid the use of a cone or baffle with LCDs.
* #109909 "BN06 Pattern"
* #109909 ^definition = The IEC BN06 Pattern is used as analternative to the TG18-LN-06 Pattern, to avoid the use of a cone or baffle with LCDs.
* #109910 "BN07 Pattern"
* #109910 ^definition = The IEC BN07 Pattern is used as analternative to the TG18-LN-07 Pattern, to avoid the use of a cone or baffle with LCDs.
* #109911 "BN08 Pattern"
* #109911 ^definition = The IEC BN08 Pattern is used as analternative to the TG18-LN-08 Pattern, to avoid the use of a cone or baffle with LCDs.
* #109912 "BN09 Pattern"
* #109912 ^definition = The IEC BN09 Pattern is used as analternative to the TG18-LN-09 Pattern, to avoid the use of a cone or baffle with LCDs.
* #109913 "BN10 Pattern"
* #109913 ^definition = The IEC BN10 Pattern is used as analternative to the TG18-LN-10 Pattern, to avoid the use of a cone or baffle with LCDs.
* #109914 "BN11 Pattern"
* #109914 ^definition = The IEC BN11 Pattern is used as analternative to the TG18-LN-11 Pattern, to avoid the use of a cone or baffle with LCDs.
* #109915 "BN12 Pattern"
* #109915 ^definition = The IEC BN12 Pattern is used as analternative to the TG18-LN-12 Pattern, to avoid the use of a cone or baffle with LCDs.
* #109916 "BN13 Pattern"
* #109916 ^definition = The IEC BN13 Pattern is used as analternative to the TG18-LN-13 Pattern, to avoid the use of a cone or baffle with LCDs.
* #109917 "BN14 Pattern"
* #109917 ^definition = The IEC BN14 Pattern is used as analternative to the TG18-LN-14 Pattern, to avoid the use of a cone or baffle with LCDs.
* #109918 "BN15 Pattern"
* #109918 ^definition = The IEC BN15 Pattern is used as analternative to the TG18-LN-15 Pattern, to avoid the use of a cone or baffle with LCDs.
* #109919 "BN16 Pattern"
* #109919 ^definition = The IEC BN16 Pattern is used as analternative to the TG18-LN-16 Pattern, to avoid the use of a cone or baffle with LCDs.
* #109920 "BN17 Pattern"
* #109920 ^definition = The IEC BN17 Pattern is used as analternative to the TG18-LN-17 Pattern, to avoid the use of a cone or baffle with LCDs.
* #109921 "BN18 Pattern"
* #109921 ^definition = The IEC BN18 Pattern is used as analternative to the TG18-LN-18 Pattern, to avoid the use of a cone or baffle with LCDs.
* #109931 "DIN Grayscale Pattern"
* #109931 ^definition = Test image "Bild 2" for the gray-scale reproduction of imaging devices.
* #109932 "DIN Geometry Pattern"
* #109932 ^definition = Test image "Bild 3" for the geometrical imaging properties of imaging devices.
* #109933 "DIN Resolution Pattern"
* #109933 ^definition = Test image "Bild 5" for displaying the spatial and contrast resolution as well as the line structure of imaging devices.
* #109941 "White Pattern"
* #109941 ^definition = An alternative to AAPM TG18-UN80, specified at 100% of maximum pixel value.
* #109943 "SMPTE Pattern"
* #109943 ^definition = A standard display test pattern.
* #109991 "CRT Display"
* #109991 ^definition = A Display Device that displays images on a Cathode Ray Tube.
* #109992 "Liquid Crystal Display"
* #109992 ^definition = A Display Device that displays images on a Liquid Crystal Display.
* #109993 "Plasma Display"
* #109993 ^definition = A Display Device that displays images on a Plasma Display.
* #109994 "OLED"
* #109994 ^definition = A Display Device that displays images on an Organic Light Emitting Diode based display.
* #109995 "DLP Rear Projection System"
* #109995 ^definition = A Display Device that projects images on a surface from behind using a Digital Light Processing Projector.
* #109996 "DLP Front Projection System"
* #109996 ^definition = A Display Device that projects images on a surface from in front using a Digital Light Processing Projector.
* #109997 "CRT Rear Projection System"
* #109997 ^definition = A Display Device that projects images on a surface from behind using a Cathode Ray Tube.
* #109998 "CRT Front Projection System"
* #109998 ^definition = A Display Device that projects images on a surface from in front using a Cathode Ray Tube.
* #109999 "Other Projection System"
* #109999 ^definition = A Display Device that projects images on a surface from an unspecified direction using an unspecified means.
* #110001 "Image Processing"
* #110001 ^definition = Image processing work item.
* #110002 "Quality Control"
* #110002 ^definition = Quality control work item.
* #110003 "Computer Aided Diagnosis"
* #110003 ^definition = Computer aided diagnosis work item.
* #110004 "Computer Aided Detection"
* #110004 ^definition = Computer aided detection work item.
* #110005 "Interpretation"
* #110005 ^definition = Interpretation work item.
* #110006 "Transcription"
* #110006 ^definition = Transcription work item.
* #110007 "Report Verification"
* #110007 ^definition = Report verification work item.
* #110008 "Print"
* #110008 ^definition = Print work item.
* #110009 "No subsequent Workitems"
* #110009 ^definition = There will be no more work items scheduled.
* #110010 "Film"
* #110010 ^definition = Film type of output.
* #110011 "Dictation"
* #110011 ^definition = Dictation type of output.
* #110012 "Transcription"
* #110012 ^definition = Transcription type of output.
* #110013 "Media Import"
* #110013 ^definition = The procedure to read DICOM instances from DICOM interchange media, coerce identifying attributes into the local namespace if necessary, and make the instances available.
* #110020 "Sheet Film Digitized"
* #110020 ^definition = Digitization of Sheet Film.
* #110021 "Cine Film Digitized"
* #110021 ^definition = Digitization of Cine Film.
* #110022 "Video Tape Digitized"
* #110022 ^definition = Digitization of Video Tape.
* #110023 "Paper Digitized"
* #110023 ^definition = Digitization of pages of a paper document (Units may be specified as Pages, Documents).
* #110024 "CD Imported"
* #110024 ^definition = Importation of CD.
* #110025 "DVD Imported"
* #110025 ^definition = Importation of DVD.
* #110026 "MOD Imported"
* #110026 ^definition = Importation of MOD.
* #110027 "Studies Imported"
* #110027 ^definition = Importation of DICOM Studies.
* #110028 "Instances Imported"
* #110028 ^definition = Importation of DICOM Composite Instances.
* #110030 "USB Disk Emulation"
* #110030 ^definition = A device that connects using the USB hard drive interface. These may be USB-Sticks, portable hard drives, and other technologies.
* #110031 "Email"
* #110031 ^definition = Email and email attachments used as a media for data transport.
* #110032 "CD"
* #110032 ^definition = CD-R, CD-ROM, and CD-RW media used for data transport.
* #110033 "DVD"
* #110033 ^definition = DVD, DVD-RAM, and other DVD formatted media used for data transport.
* #110034 "Compact Flash"
* #110034 ^definition = Media that comply with the Compact Flash standard.
* #110035 "Multi-media Card"
* #110035 ^definition = Media that comply with the Multi-media Card standard.
* #110036 "Secure Digital Card"
* #110036 ^definition = Media that comply with the Secure Digital Card standard.
* #110037 "URI"
* #110037 ^definition = URI Identifier for network or other resource, see RFC 3968.
* #110038 "Paper Document"
* #110038 ^definition = Any paper or similar document.
* #110100 "Application Activity"
* #110100 ^definition = Audit event: Application Activity has taken place.
* #110101 "Audit Log Used"
* #110101 ^definition = Audit event: Audit Log has been used.
* #110102 "Begin Transferring DICOM Instances"
* #110102 ^definition = Audit event: Storage of DICOM Instances has begun.
* #110103 "DICOM Instances Accessed"
* #110103 ^definition = Audit event: DICOM Instances have been created, read, updated, or deleted -audit event.
* #110104 "DICOM Instances Transferred"
* #110104 ^definition = Audit event: Storage of DICOM Instances has been completed.
* #110105 "DICOM Study Deleted"
* #110105 ^definition = Audit event: Entire Study has been deleted.
* #110106 "Export"
* #110106 ^definition = Audit event: Data has been exported out of the system.
* #110107 "Import"
* #110107 ^definition = Audit event: Data has been imported into the system.
* #110108 "Network Entry"
* #110108 ^definition = Audit event: System has joined or left network.
* #110109 "Order Record"
* #110109 ^definition = Audit event: Order has been created, read, updated or deleted.
* #110110 "Patient Record"
* #110110 ^definition = Audit event: Patient Record has been created, read, updated, or deleted.
* #110111 "Procedure Record"
* #110111 ^definition = Audit event: Procedure Record has been created, read, updated, or deleted.
* #110112 "Query"
* #110112 ^definition = Audit event: Query has been made.
* #110113 "Security Alert"
* #110113 ^definition = Audit event: Security Alert has been raised.
* #110114 "User Authentication"
* #110114 ^definition = Audit event: User Authentication has been attempted.
* #110120 "Application Start"
* #110120 ^definition = Audit event: Application Entity has started.
* #110121 "Application Stop"
* #110121 ^definition = Audit event: Application Entity has stopped.
* #110122 "Login"
* #110122 ^definition = Audit event: User login has been attempted.
* #110123 "Logout"
* #110123 ^definition = Audit event: User logout has been attempted.
* #110124 "Attach"
* #110124 ^definition = Audit event: Node has been attached.
* #110125 "Detach"
* #110125 ^definition = Audit event: Node has been detached.
* #110126 "Node Authentication"
* #110126 ^definition = Audit event: Node Authentication has been attempted.
* #110127 "Emergency Override Started"
* #110127 ^definition = Audit event: Emergency Override has started.
* #110128 "Network Configuration"
* #110128 ^definition = Audit event: Network configuration has been changed.
* #110129 "Security Configuration"
* #110129 ^definition = Audit event: Security configuration has been changed.
* #110130 "Hardware Configuration"
* #110130 ^definition = Audit event: Hardware configuration has been changed.
* #110131 "Software Configuration"
* #110131 ^definition = Audit event: Software configuration has been changed.
* #110132 "Use of Restricted Function"
* #110132 ^definition = Audit event: A use of a restricted function has been attempted.
* #110133 "Audit Recording Stopped"
* #110133 ^definition = Audit event: Audit recording has been stopped.
* #110134 "Audit Recording Started"
* #110134 ^definition = Audit event: Audit recording has been started.
* #110135 "Object Security Attributes Changed"
* #110135 ^definition = Audit event: Security attributes of an object have been changed.
* #110136 "Security Roles Changed"
* #110136 ^definition = Audit event: Security roles have been changed.
* #110137 "User security Attributes Changed"
* #110137 ^definition = Audit event: Security attributes of a user have been changed.
* #110138 "Emergency Override Stopped"
* #110138 ^definition = Audit event: Emergency Override has Stopped.
* #110139 "Remote Service Operation Started"
* #110139 ^definition = Audit event: Remote Service Operation has Begun.
* #110140 "Remote Service Operation Stopped"
* #110140 ^definition = Audit event: Remote Service Operation has Stopped.
* #110141 "Local Service Operation Started"
* #110141 ^definition = Audit event: Local Service Operation has Begun.
* #110142 "Local Service Operation Stopped"
* #110142 ^definition = Audit event: Local Service Operation Stopped.
* #110150 "Application"
* #110150 ^definition = Audit participant role ID of software application.
* #110151 "Application Launcher"
* #110151 ^definition = Audit participant role ID of software application launcher, i.e., the entity that started or stopped an application.
* #110152 "Destination Role ID"
* #110152 ^definition = Audit participant role ID of the receiver of data.
* #110153 "Source Role ID"
* #110153 ^definition = Audit participant role ID of the sender of data.
* #110154 "Destination Media"
* #110154 ^definition = Audit participant role ID of media receiving data during an export.
* #110155 "Source Media"
* #110155 ^definition = Audit participant role ID of media providing data during an import.
* #110180 "Study Instance UID"
* #110180 ^definition = ParticipantObjectID type: Study Instance UID.
* #110181 "SOP Class UID"
* #110181 ^definition = ParticipantObjectID type: SOP Class UID.
* #110182 "Node ID"
* #110182 ^definition = ID of a node that is a participant object of an audit message.
* #110190 "Issuer of Identifier"
* #110190 ^definition = System, organization, agency, or department that has assigned an instance identifier (such as placer or filler number, patient or provider identifier, etc.).
* #110500 "Doctor canceled procedure"
* #110500 ^definition = Procedure order canceled by requesting physician or other authorized physician.
* #110501 "Equipment failure"
* #110501 ^definition = Equipment failure prevented completion of procedure.
* #110502 "Incorrect procedure ordered"
* #110502 ^definition = Procedure discontinued due to incorrect procedure being ordered.
* #110503 "Patient allergic to media/contrast"
* #110503 ^definition = Procedure discontinued due to patient allergy to media/contrast (reported or reaction).
* #110504 "Patient died"
* #110504 ^definition = Procedure discontinued due to death of Patient.
* #110505 "Patient refused to continue procedure"
* #110505 ^definition = Procedure discontinued due to patient refusal to continue procedure.
* #110506 "Patient taken for treatment or surgery"
* #110506 ^definition = Procedure discontinued due to patient being taken for treatment or surgery.
* #110507 "Patient did not arrive"
* #110507 ^definition = Patient did not arrive for procedure.
* #110508 "Patient pregnant"
* #110508 ^definition = Procedure discontinued due to patient pregnancy (reported or determined).
* #110509 "Change of procedure for correct charging"
* #110509 ^definition = Procedure discontinued to restart with new procedure code for correct charging.
* #110510 "Duplicate order"
* #110510 ^definition = Procedure discontinued due to duplicate orders received for same procedure.
* #110511 "Nursing unit cancel"
* #110511 ^definition = Procedure order canceled by nursing unit.
* #110512 "Incorrect side ordered"
* #110512 ^definition = Procedure discontinued due to incorrect side (laterality) being ordered.
* #110513 "Discontinued for unspecified reason"
* #110513 ^definition = Procedure discontinued for unspecified reason.
* #110514 "Incorrect worklist entry selected"
* #110514 ^definition = Procedure discontinued due to incorrect patient or procedure step selected from modality worklist.
* #110515 "Patient condition prevented continuing"
* #110515 ^definition = Patient condition prevented continuation of procedure.
* #110516 "Equipment change"
* #110516 ^definition = Procedure step is discontinued to change to other equipment or modality.
* #110518 "Patient Movement"
* #110518 ^definition = A movement of the patient affecting test quality.
* #110519 "Operator Error"
* #110519 ^definition = An error of the operator affecting test quality.
* #110521 "Objects incorrectly formatted"
* #110521 ^definition = One or more of the objects is malformed.
* #110522 "Object Types not supported"
* #110522 ^definition = Receiving System is unable to accept the object type.
* #110523 "Object Set incomplete"
* #110523 ^definition = One or more objects associated with the object set is missing.
* #110524 "Media Failure"
* #110524 ^definition = The contents of the Media could not be accessed properly.
* #110526 "Resource pre-empted"
* #110526 ^definition = Procedure discontinued due to necessary equipment, staff or other resource becoming (temporarily) unavailable to the procedure.
* #110527 "Resource inadequate"
* #110527 ^definition = Procedure discontinued due to necessary equipment, staff or other resource being inadequate to complete the procedure.
* #110528 "Discontinued Procedure Step rescheduled"
* #110528 ^definition = A new Procedure Step has been scheduled to replace the Discontinued Procedure Step.
* #110529 "Discontinued Procedure Step rescheduling recommended"
* #110529 ^definition = It is recommended that a new Procedure Step be scheduled to replace the Discontinued Procedure Step.
* #110700 "Ventral Diencephalon"
* #110700 ^definition = Ventral structures of the diencephalon that cannot readily be distinguished on MR imaging, including the hypothalamus, mammillary body, subthalamic nuclei, substantia nigra, red nucleus, lateral geniculate nucleus, medial geniculate nucleus, zona incerta, cerebral peduncle, lenticular fasciculus, medial lemniscus, and optic tract.
* #110701 "White Matter T1 Hypointensity"
* #110701 ^definition = Area(s) of reduced intensity on T1 weighted images relative to the surrounding white matter.
* #110702 "White Matter T2 Hyperintensity"
* #110702 ^definition = Area(s) of increased intensity on T2 weighted images relative to the surrounding white matter.
* #110703 "superior longitudinal fasciculus I"
* #110703 ^definition = The dorsal component of the SLF originating from the medial and dorsal parietal cortex and ending in the dorsal and medial part of the frontal lobe.
* #110704 "superior longitudinal fasciculus II"
* #110704 ^definition = The major component of the SLF, derived from the caudalinferior parietal region corresponding to the angular gyrus in the human and terminating within the dorsolateral frontal region.
* #110705 "superior longitudinal fasciculus III"
* #110705 ^definition = The ventral component of the SLF, originating from the supramarginal gyrus and terminating predominantly in the ventral premotor and prefrontal areas.
* #110706 "Perilesional White Matter"
* #110706 ^definition = White matter that surrounds a lesion of interest. E.g., to identify the otherwise unclassified white matetr that surrounds a tumor to be surgically resected.
* #110800 "Spin Tagging Perfusion MR Signal Intensity"
* #110800 ^definition = Signal intensity of a Spin tagging Perfusion MR image. Spin tagging is a technique for the measurement of blood perfusion, based on magnetically labeled arterial blood water as an endogenous tracer.
* #110801 "Contrast Agent Angio MR Signal Intensity"
* #110801 ^definition = Signal intensity of a Contrast Agent Angio MR image.
* #110802 "Time Of Flight Angio MR Signal Intensity"
* #110802 ^definition = Signal intensity of a Time-of-flight (TOF) MR image. Time-of-flight (TOF) is based on the phenomenon of flow-related enhancement of spins entering into an imaging slice. As a result of being unsaturated, these spins give more signal that surrounding stationary spins.
* #110803 "Proton Density Weighted MR Signal Intensity"
* #110803 ^definition = Signal intensity of a Proton Density Weighted MR image. All MR images have intensity proportional to proton density. Images with very little T1 or T2 weighting are called 'PD-weighted'.
* #110804 "T1 Weighted MR Signal Intensity"
* #110804 ^definition = Signal intensity of T1 Weighted MR image. A T1 Weighted MR image is created typically by using short TE and TR times.
* #110805 "T2 Weighted MR Signal Intensity"
* #110805 ^definition = Signal intensity of a T2 Weighted MR image. T2 Weighted image contrast state is approached by imaging with a TR long compared to tissue T1 (to reduce T1 contribution to image contrast) and a TE between the longest and shortest tissue T2s of interest.
* #110806 "T2* Weighted MR Signal Intensity"
* #110806 ^definition = Signal intensity of a T2* Weighted MR image. The T2* phenomenon results from molecular interactions (spin spin relaxation) and local magnetic field non-uniformities, which cause the protons to precess at slightly different frequencies.
* #110807 "Field Map MR Signal Intensity"
* #110807 ^definition = Signal intensity of a Field Map MR image. A Field Map MR image provides a direct measure of the B 0 inhomogeneity at each point in the image.
* #110808 "Fractional Anisotropy"
* #110808 ^definition = Coefficient reflecting the fractional anisotropy of the tissues, derived from a diffusion weighted MR image. Fractional anisotropy is proportional to the square root of the variance of the Eigen values divided by the square root of the sum of the squares of the Eigen values.
* #110809 "Relative Anisotropy"
* #110809 ^definition = Coefficient reflecting the relative anisotropy of the tissues, derived from a diffusion weighted MR image.
* #110810 "Volumetric Diffusion Dxx Component"
* #110810 ^definition = Dxx Component of the diffusion tensor, quantifying the molecular mobility along the X axis.
* #110811 "Volumetric Diffusion Dxy Component"
* #110811 ^definition = Dxy Component of the diffusion tensor, quantifying the correlation of molecular displacements in the X and Y directions.
* #110812 "Volumetric Diffusion Dxz Component"
* #110812 ^definition = Dxz Component of the diffusion tensor, quantifying the correlation of molecular displacements in the X and Z directions.
* #110813 "Volumetric Diffusion Dyy Component"
* #110813 ^definition = Dyy Component of the diffusion tensor, quantifying the molecular mobility along the Y axis.
* #110814 "Volumetric Diffusion Dyz Component"
* #110814 ^definition = Dyz Component of the diffusion tensor, quantifying the correlation of molecular displacements in the Y and Z directions.
* #110815 "Volumetric Diffusion Dzz Component"
* #110815 ^definition = Dzz Component of the diffusion tensor, quantifying the molecular mobility along the Z axis.
* #110816 "T1 Weighted Dynamic Contrast Enhanced MR Signal Intensity"
* #110816 ^definition = Signal intensity of a T1 Weighted Dynamic Contrast Enhanced MR image. A T1 Weighted Dynamic Contrast Enhanced MR image reflects the dynamics of diffusion of the exogenous contrast media from the blood pool into the extra vascular extracellular space (EES) of the brain at a rate determined by the blood flow to the tissue, the permeability of the Brain Blood Barrier (BBB), and the surface area of the perfusing vessels.
* #110817 "T2 Weighted Dynamic Contrast Enhanced MR Signal Intensity"
* #110817 ^definition = Signal intensity of a T2 Weighted Dynamic Contrast Enhanced MR image. A T2 Weighted Dynamic Contrast Enhanced MR image reflects the T2 of tissue decrease as the Gd contrast agent bolus passes through the brain.
* #110818 "T2* Weighted Dynamic Contrast Enhanced MR Signal Intensity"
* #110818 ^definition = Signal intensity of a T2* Weighted Dynamic Contrast Enhanced MR image. A T2* Weighted Dynamic Contrast Enhanced MR image reflects the T2* of tissue decrease as the Gd contrast agent bolus passes through the brain.
* #110819 "Blood Oxygenation Level"
* #110819 ^definition = Signal intensity of a Blood Oxygenation Level image. BOLD imaging is sensitive to blood oxygenation (but also to cerebral blood flow and volume). This modality is essentially used for detecting brain activation (functional MR).
* #110820 "Nuclear Medicine Projection Activity"
* #110820 ^definition = Accumulated decay event counts in a nuclear medicine projection image.
* #110821 "Nuclear Medicine Tomographic Activity"
* #110821 ^definition = Accumulated decay event counts in a Nuclear Medicine Tomographic image (including PET).
* #110822 "Spatial Displacement X Component"
* #110822 ^definition = Spatial Displacement along axis X of a non linear deformable spatial registration image. The X axis is defined in reference to the patient's orientation, and is increasing to the left hand side of the patient.
* #110823 "Spatial Displacement Y Component"
* #110823 ^definition = Spatial Displacement along axis Y of a non linear deformable spatial registration image. The Y axis is defined in reference to the patient's orientation, and is increasing to the posterior side of the patient.
* #110824 "Spatial Displacement Z Component"
* #110824 ^definition = Spatial Displacement along axis Z of a Non linear deformable spatial registration image. The Z axis is defined in reference to the patient's orientation, and is increasing toward the head of the patient.
* #110825 "Hemodynamic Resistance"
* #110825 ^definition = Measured resistance to the flow of blood. E.g., through the vasculature or through a heart value.
* #110826 "Indexed Hemodynamic Resistance"
* #110826 ^definition = Measured resistance to the flow of blood. E.g., through the vasculature or through a heart value, normalized to a particular indexed scale.
* #110827 "Tissue Velocity"
* #110827 ^definition = Velocity of tissue based on Doppler measurements.
* #110828 "Flow Velocity"
* #110828 ^definition = Velocity of blood flow based on Doppler measurements.
* #110829 "Flow Variance"
* #110829 ^definition = Statistical variance of blood velocity relative to mean.
* #110830 "Elasticity"
* #110830 ^definition = Scalar value related to the elastic properties of the tissue.
* #110831 "Perfusion"
* #110831 ^definition = Scalar value related to the volume of blood perfusing into tissue.
* #110832 "Speed of sound"
* #110832 ^definition = Speed of sound in tissue.
* #110833 "Ultrasound Attenuation"
* #110833 ^definition = Reduction in strength of ultrasound signal as the wave.
* #110834 "RGB R Component"
* #110834 ^definition = Red component of a true color image (RGB).
* #110835 "RGB G Component"
* #110835 ^definition = Green component of a true color image (RGB).
* #110836 "RGB B Component"
* #110836 ^definition = Blue component of a true color image (RGB).
* #110837 "YBR FULL Y Component"
* #110837 ^definition = Y (Luminance) component of a YBR FULL image, as defined in JPEG 2000.
* #110838 "YBR FULL CB Component"
* #110838 ^definition = CB (Blue chrominance) component of a YBR FULL image, as defined in JPEG 2000.
* #110839 "YBR FULL CR Component"
* #110839 ^definition = CR (Red chrominance) component of a YBR FULL image, as defined in JPEG 2000.
* #110840 "YBR PARTIAL Y Component"
* #110840 ^definition = Y (Luminance) component of a YBR PARTIAL image, as defined in JPEG 2000.
* #110841 "YBR PARTIAL CB Component"
* #110841 ^definition = CB (Blue chrominance) component of a YBR PARTIAL image, as defined in JPEG 2000.
* #110842 "YBR PARTIAL CR Component"
* #110842 ^definition = CR (Red chrominance) component of a YBR PARTIAL image, as defined in JPEG 2000.
* #110843 "YBR ICT Y Component"
* #110843 ^definition = Y (Luminance) component of a YBR ICT image (Irreversible Color Transform), as defined in JPEG 2000.
* #110844 "YBR ICT CB Component"
* #110844 ^definition = CB (Blue chrominance) component of a YBR ICT image (Irreversible Color Transform), as defined in JPEG 2000.
* #110845 "YBR ICT CR Component"
* #110845 ^definition = CR (Red chrominance) component of a YBR ICT image (Irreversible Color Transform), as defined in JPEG 2000.
* #110846 "YBR RCT Y Component"
* #110846 ^definition = Y (Luminance) component of a YBR RCT image (Reversible Color Transform), as defined in JPEG 2000.
* #110847 "YBR RCT CB Component"
* #110847 ^definition = CB (Blue chrominance) component of a YBR RCT image (Reversible Color Transform), as defined in JPEG 2000.
* #110848 "YBR RCT CR Component"
* #110848 ^definition = CR (Red chrominance) component of a YBR RCT image (Reversible Color Transform), as defined in JPEG 2000.
* #110849 "Echogenicity"
* #110849 ^definition = The ability of a material to create an ultrasound return echo.
* #110850 "X-Ray Attenuation"
* #110850 ^definition = Decrease in the number of photons in an X-Ray beam due to interactions with the atoms of a material substance. Attenuation is due primarily to two processes, absorption and scattering.
* #110851 "X-Ray Attenuation Coefficient"
* #110851 ^definition = Coefficient that describes the fraction of a beam of X-Rays or gamma rays that is absorbed or scattered per unit thickness of the absorber. This value basically accounts for the number of atoms in a cubic cm volume of material and the probability of a photon being scattered or absorbed from the nucleus or an electron of one of these atoms.
* #110851 ^designation[0].language = #de-AT 
* #110851 ^designation[0].value = "Retired. Replaced by (112031, DCM, 'Attenuation Coefficient')." 
* #110852 "MR signal intensity"
* #110852 ^definition = Signal intensity of an MR image, not otherwise specified.
* #110853 "Binary Segmentation"
* #110853 ^definition = Binary value denoting that the segmented property is present.
* #110854 "Fractional Probabilistic Segmentation"
* #110854 ^definition = Probability, defined as a percentage, that the segmented property occupies the spatial area defined by the voxel.
* #110855 "Fractional Occupancy Segmentation"
* #110855 ^definition = Percentage of the voxel area occupied by the segmented property.
* #110856 "Linear Displacement"
* #110856 ^definition = Spatial dimension, denoting a linear displacement.
* #110857 "Photon Energy"
* #110857 ^definition = Dimension denoting the energy (frequency or wavelength) of photons.
* #110858 "Time"
* #110858 ^definition = Dimension used to sequence events, to compare the duration of events and the intervals between events.
* #110859 "Angle"
* #110859 ^definition = Spatial dimension, denoting an angle.
* #110860 "Left-Right Axis"
* #110860 ^definition = A spatial dimension axis running along a line between the patient's left and right side.
* #110861 "Head-Foot Axis"
* #110861 ^definition = A spatial dimension axis running along a line between the patient's head and foot.
* #110862 "Anterior-Posterior Axis"
* #110862 ^definition = A spatial dimension axis running along a line between the patient's anterior and posterior sides.
* #110863 "Apex-Base Axis"
* #110863 ^definition = A spatial dimension axis running along a line between the apex and base of an organ, object, or chamber.
* #110864 "Anterior-Inferior Axis"
* #110864 ^definition = A spatial dimension axis running along a line between the anterior and inferior sides of an organ, object, or chamber.
* #110865 "Septum-Wall Axis"
* #110865 ^definition = A spatial dimension axis running along a line between the septum and wall of a chamber.
* #110866 "Right To Left"
* #110866 ^definition = Orientation of a spatial dimension where increasing values run from the right to the left side of the patient.
* #110867 "Left To Right"
* #110867 ^definition = Orientation of a spatial dimension where increasing values run from the left to the right side of the patient.
* #110868 "Head To Foot"
* #110868 ^definition = Orientation of a spatial dimension where increasing values run from the head to the foot of the patient.
* #110869 "Foot To Head"
* #110869 ^definition = Orientation of a spatial dimension where increasing values run from the foot to the head of the patient.
* #110870 "Anterior To Posterior"
* #110870 ^definition = Orientation of a spatial dimension where increasing values run from the anterior to the posterior side of the patient.
* #110871 "Posterior To Anterior"
* #110871 ^definition = Orientation of a spatial dimension where increasing values run from the posterior to the anterior side of the patient.
* #110872 "Apex To Base"
* #110872 ^definition = Orientation of a spatial dimension where increasing values run from the apex to the base.
* #110873 "Base To Apex"
* #110873 ^definition = Orientation of a spatial dimension where increasing values run from the base to the apex.
* #110874 "Anterior To Inferior"
* #110874 ^definition = Orientation of a spatial dimension where increasing values run from the anterior to the inferior.
* #110875 "Inferior To Anterior"
* #110875 ^definition = Orientation of a spatial dimension where increasing values run from the inferior to the anterior.
* #110876 "Septum To Wall"
* #110876 ^definition = Orientation of a spatial dimension where increasing values run from the septum of a chamber to the opposite wall.
* #110877 "Wall To Septum"
* #110877 ^definition = Orientation of a spatial dimension where increasing values run from the opposite wall to the septum of a chamber.
* #110901 "Image Position (Patient) X"
* #110901 ^definition = The x coordinate of the upper left hand corner (center of the first voxel transmitted) of the image, with respect to the patient-based coordinate system.
* #110902 "Image Position (Patient) Y"
* #110902 ^definition = The y coordinate of the upper left hand corner (center of the first voxel transmitted) of the image, with respect to the patient-based coordinate system.
* #110903 "Image Position (Patient) Z"
* #110903 ^definition = The z coordinate of the upper left hand corner (center of the first voxel transmitted) of the image, with respect to the patient-based coordinate system.
* #110904 "Image Orientation (Patient) Row X"
* #110904 ^definition = The x value of the first row direction cosine with respect to the patient, with respect to the patient-based coordinate system.
* #110905 "Image Orientation (Patient) Row Y"
* #110905 ^definition = The y value of the first row direction cosine with respect to the patient, with respect to the patient-based coordinate system.
* #110906 "Image Orientation (Patient) Row Z"
* #110906 ^definition = The z value of the first row direction cosine with respect to the patient, with respect to the patient-based coordinate system.
* #110907 "Image Orientation (Patient) Column X"
* #110907 ^definition = The x value of the first column direction cosine with respect to the patient, with respect to the patient-based coordinate system.
* #110908 "Image Orientation (Patient) Column Y"
* #110908 ^definition = The y value of the first column direction cosine with respect to the patient, with respect to the patient-based coordinate system.
* #110909 "Image Orientation (Patient) Column Z"
* #110909 ^definition = The z value of the first column direction cosine with respect to the patient, with respect to the patient-based coordinate system.
* #110910 "Pixel Data Rows"
* #110910 ^definition = Number of rows in the pixel data of the image.
* #110911 "Pixel Data Columns"
* #110911 ^definition = Number of columns in the pixel data of the image.
* #111001 "Algorithm Name"
* #111001 ^definition = The name assigned by a manufacturer to a specific software algorithm.
* #111002 "Algorithm Parameters"
* #111002 ^definition = The input parameters used by a manufacturer to configure the behavior of a specific software algorithm.
* #111003 "Algorithm Version"
* #111003 ^definition = The software version identifier assigned by a manufacturer to a specific software algorithm.
* #111004 "Analysis Performed"
* #111004 ^definition = The type of correlation applied to detection results. E.g., temporal, spatial.
* #111005 "Assessment Category"
* #111005 ^definition = Assignment of intermediate or overall interpretation results to a general category.
* #111006 "Breast composition"
* #111006 ^definition = Assessment of annotating tissues in breast; generally including fatty, mixed or dense
* #111006 ^designation[0].language = #de-AT 
* #111006 ^designation[0].value = "Retired. Replaced by (F-01710, SRT, 'Breast composition')." 
* #111007 "Breast Outline including Pectoral Muscle Tissue"
* #111007 ^definition = Purpose of reference for an SCOORD content item that is an outline of the breast that includes the pectoral muscle tissue
* #111007 ^designation[0].language = #de-AT 
* #111007 ^designation[0].value = "Purpose of Reference for content item of value type COMPOSITE or SCOORD" 
* #111008 "Calcification Distribution"
* #111008 ^definition = The type of distribution associated with detected calcifications.
* #111009 "Calcification Type"
* #111009 ^definition = Identification of the morphology of detected calcifications.
* #111010 "Center"
* #111010 ^definition = Purpose of reference for an SCOORD content item that identifies the central point of a finding or feature
* #111010 ^designation[0].language = #de-AT 
* #111010 ^designation[0].value = "Purpose of Reference for content item of value type COMPOSITE or SCOORD" 
* #111011 "Certainty of Feature"
* #111011 ^definition = The likelihood that the feature analyzed is in fact the type of feature identified.
* #111012 "Certainty of Finding"
* #111012 ^definition = The likelihood that the finding detected is in fact the type of finding identified.
* #111013 "Certainty of Impression"
* #111013 ^definition = The certainty that a device places on an impression, where 0 equals no certainty and 100 equals certainty.
* #111014 "Clockface or region"
* #111014 ^definition = A location identifier based on clockface numbering or anatomic subregion.
* #111015 "Composite Feature"
* #111015 ^definition = An item that is an inferred correlation relating two or more individual findings or features.
* #111016 "Composite type"
* #111016 ^definition = The inferred relationship between the findings or features making up a composite feature.
* #111017 "CAD Processing and Findings Summary"
* #111017 ^definition = General assessment of whether or not CAD processing was successful, and whether any findings resulted.
* #111018 "Content Date"
* #111018 ^definition = The date the data creation started.
* #111019 "Content Time"
* #111019 ^definition = The time the data creation started.
* #111020 "Depth"
* #111020 ^definition = A location identifier based on a feature's inferred distance from the surface of the associated anatomy.
* #111021 "Description of Change"
* #111021 ^definition = A textual description of the change that occurred over time in a qualitative characteristic of a feature.
* #111022 "Detection Performed"
* #111022 ^definition = The type of finding sought after by a specific algorithm applied to one image.
* #111023 "Differential Diagnosis/Impression"
* #111023 ^definition = A general change that occurred within an imaged area between a prior imaging procedure and the current imaging procedure.
* #111024 "Failed Analyses"
* #111024 ^definition = A group of analysis algorithms that were attempted, but failed.
* #111025 "Failed Detections"
* #111025 ^definition = A group of detection algorithms that were attempted, but failed.
* #111026 "Horizontal Pixel Spacing"
* #111026 ^definition = For projection radiography, the horizontal physical distance measured at the front plane of an Image Receptor housing between the center of each pixel. For tomographic images, the horizontal physical distance in the patient between the center of each pixel.
* #111027 "Image Laterality"
* #111027 ^definition = Laterality of (possibly paired) body part contained in an image.
* #111028 "Image Library"
* #111028 ^definition = A container that references all image data used as evidence to produce a report.
* #111029 "Image Quality Rating"
* #111029 ^definition = A numeric value in the range 0 to 100, inclusive, where 0 is worst quality and 100 is best quality.
* #111030 "Image Region"
* #111030 ^definition = Purpose of reference for an SCOORD content item that identifies a specific region of interest within an image
* #111030 ^designation[0].language = #de-AT 
* #111030 ^designation[0].value = "Purpose of Reference for content item of value type COMPOSITE or SCOORD" 
* #111031 "Image View"
* #111031 ^definition = The projection of the anatomic region of interest on an image receptor.
* #111032 "Image View Modifier"
* #111032 ^definition = Modifier for Image View.
* #111033 "Impression Description"
* #111033 ^definition = Free-form text describing the overall or an individual impression.
* #111034 "Individual Impression/Recommendation"
* #111034 ^definition = A container for a group of related results from interpretation of one or more images and associated clinical information.
* #111035 "Lesion Density"
* #111035 ^definition = The X-Ray attenuation of a lesion relative to the expected attenuation of an equal volume of fibroglandular breast tissue.
* #111036 "Mammography CAD Report"
* #111036 ^definition = A structured report containing the results of computer-aided detection or diagnosis applied to breast imaging and associated clinical information.
* #111037 "Margins"
* #111037 ^definition = The characteristic of the boundary, edges or border of a detected lesion.
* #111038 "Number of calcifications"
* #111038 ^definition = The quantity of calcifications detected within an identified group or cluster.
* #111039 "Object type"
* #111039 ^definition = A non-lesion object identified within one or more images.
* #111040 "Original Source"
* #111040 ^definition = Purpose of reference for a COMPOSITE content item that identifies it as the original source of evidence for another content item in the report
* #111040 ^designation[0].language = #de-AT 
* #111040 ^designation[0].value = "Purpose of Reference for content item of value type COMPOSITE or SCOORD" 
* #111041 "Outline"
* #111041 ^definition = Purpose of reference for an SCOORD content item that identifies the outline or bounding region of a finding or feature
* #111041 ^designation[0].language = #de-AT 
* #111041 ^designation[0].value = "Purpose of Reference for content item of value type COMPOSITE or SCOORD" 
* #111042 "Pathology"
* #111042 ^definition = The inferred type of disease associated with an identified feature.
* #111043 "Patient Orientation Column"
* #111043 ^definition = The patient orientation relative to the image plane, specified by a value that designates the anatomical direction of the positive column axis (top to bottom).
* #111044 "Patient Orientation Row"
* #111044 ^definition = The patient orientation relative to the image plane, specified by a value that designates the anatomical direction of the positive row axis (left to right).
* #111045 "Pectoral Muscle Outline"
* #111045 ^definition = Purpose of reference for an SCOORD content item that is an outline of the pectoral muscle tissue only
* #111045 ^designation[0].language = #de-AT 
* #111045 ^designation[0].value = "Purpose of Reference for content item of value type COMPOSITE or SCOORD" 
* #111046 "Percent Fibroglandular Tissue"
* #111046 ^definition = Percent of breast area that is mammographically dense, excluding pectoralis muscle.
* #111047 "Probability of cancer"
* #111047 ^definition = The likelihood that an identified finding or feature is cancerous.
* #111048 "Quadrant location"
* #111048 ^definition = A location identifier based on the division of an area into four regions.
* #111049 "Qualitative Difference"
* #111049 ^definition = A qualitative characteristic of a feature that has changed over time.
* #111050 "Quality Assessment"
* #111050 ^definition = The effect of the quality of an image on its usability.
* #111051 "Quality Control Standard"
* #111051 ^definition = The quality control standard used to make a quality assessment.
* #111052 "Quality Finding"
* #111052 ^definition = A specific quality related deficiency detected within an image.
* #111053 "Recommended Follow-up"
* #111053 ^definition = Recommended type of follow-up to an imaging procedure, based on interpreted results.
* #111054 "Recommended Follow-up Date"
* #111054 ^definition = Recommended follow-up date to an imaging procedure, based on interpreted results.
* #111055 "Recommended Follow-up Interval"
* #111055 ^definition = Recommended follow-up interval to an imaging procedure, based on interpreted results.
* #111056 "Rendering Intent"
* #111056 ^definition = The recommendation of the producer of a content item regarding presentation of the content item by recipients of the report.
* #111057 "Scope of Feature"
* #111057 ^definition = An indication of how widespread the detection of a feature is within the analyzed image data set.
* #111058 "Selected Region Description"
* #111058 ^definition = A textual description of the contents of a selected region identified within an image.
* #111059 "Single Image Finding"
* #111059 ^definition = An item that was detected on one image.
* #111060 "Study Date"
* #111060 ^definition = Date on which the acquisition of the study information was started.
* #111061 "Study Time"
* #111061 ^definition = Time at which the acquisition of the study information was started.
* #111062 "Successful Analyses"
* #111062 ^definition = A group of analysis algorithms that were attempted and completed successfully.
* #111063 "Successful Detections"
* #111063 ^definition = A group of detection algorithms that were attempted and completed successfully.
* #111064 "Summary of Detections"
* #111064 ^definition = An overall indication of whether the CAD detection algorithms applied were completed successfully.
* #111065 "Summary of Analyses"
* #111065 ^definition = An overall indication of whether the CAD analysis algorithms applied were completed successfully.
* #111066 "Vertical Pixel Spacing"
* #111066 ^definition = For projection radiography, the vertical physical distance measured at the front plane of an Image Receptor housing between the center of each pixel. For tomographic images, the vertical physical distance in the patient between the center of each pixel.
* #111069 "Crosstable"
* #111069 ^definition = A radiographic projection that has been with the patient lying on a table with the X-Ray source on one side of the table and the detector on the other. E.g., may describe a cross-table cervical spine, chest or pelvis X-Ray image.
* #111071 "CAD Operating Point"
* #111071 ^definition = One of a number of discrete points on the Receiver-Operator Characteristics (ROC) curve that reflects the expected sensitivity and specificity of a CAD algorithm, where zero indicates the highest specificity, lowest sensitivity operating point. The value should not exceed the Maximum CAD Operating Point.
* #111072 "Maximum CAD Operating Point"
* #111072 ^definition = The maximum value of CAD Operating Point for the specific CAD algorithm used.
* #111081 "CAD Operating Point Description"
* #111081 ^definition = The intended interpretation of a CAD Operating Point.
* #111086 "False Markers per Image"
* #111086 ^definition = The number of false CAD markers per image. Correlates to inverse of Image Specificity.
* #111087 "False Markers per Case"
* #111087 ^definition = The number of false markers per collection of images that are CAD processed as a group. Correlates to inverse of Case Specificity.
* #111088 "Case Sensitivity"
* #111088 ^definition = The percentage of cancers that should be detected by a CAD algorithm where CAD marks the cancers in at least one view.
* #111089 "Lesion Sensitivity"
* #111089 ^definition = The percentage of cancers that should be detected by a CAD algorithm where CAD marks the cancers in each view.
* #111090 "Case Specificity"
* #111090 ^definition = The percentage of cases (collections of images CAD processed as a group) without cancer that have no CAD findings whatsoever. Correlates to inverse of False Markers per Case.
* #111091 "Image Specificity"
* #111091 ^definition = The percentage of images without cancer that have no CAD findings whatsoever. Correlates to inverse of False Markers per Image.
* #111092 "Recommended CAD Operating Point"
* #111092 ^definition = The CAD operating point that is recommended for initial display by the creator of the structured report.
* #111093 "CAD Operating Point Table"
* #111093 ^definition = A list of CAD operating points including their corresponding characteristics.
* #111099 "Selected region"
* #111099 ^definition = A specific area of interest noted within an image.
* #111100 "Breast geometry"
* #111100 ^definition = The surface shape of all or a portion of breast related anatomy.
* #111101 "Image Quality"
* #111101 ^definition = Image quality incorporates the following clinical image evaluation parameters: assessment of positioning, compression, artifacts, exposure, contrast, sharpness, and labeling.
* #111102 "Non-lesion"
* #111102 ^definition = A finding or feature that is identified as a non-anatomic foreign object.
* #111103 "Density"
* #111103 ^definition = A space-occupying lesion identified in a single image or projection
* #111103 ^designation[0].language = #de-AT 
* #111103 ^designation[0].value = "Retired. Replaced by (F-01796, SRT, 'Mammography breast density')." 
* #111104 "Individual Calcification"
* #111104 ^definition = A single identified calcification
* #111104 ^designation[0].language = #de-AT 
* #111104 ^designation[0].value = "Retired. Replaced by (F-01776, SRT, 'Individual Calcification')." 
* #111105 "Calcification Cluster"
* #111105 ^definition = Multiple calcifications identified as occupying a small area of tissue (less than 2 cc)
* #111105 ^designation[0].language = #de-AT 
* #111105 ^designation[0].value = "Retired. Replaced by (F-01775, SRT, 'Calcification Cluster')." 
* #111111 "Cooper's ligament changes"
* #111111 ^definition = Straightening or thickening of Cooper's ligaments.
* #111112 "Mass in the skin"
* #111112 ^definition = An abnormality noted at imaging within the dermis of the breast.
* #111113 "Mass on the skin"
* #111113 ^definition = An abnormality noted at imaging on the epidermis of the breast.
* #111120 "Post Procedure Mammograms for Marker Placement"
* #111120 ^definition = An assessment category to indicate that images have been acquired to assess marker placement following a breast interventional procedure.
* #111121 "Follow-up post biopsy as directed by clinician"
* #111121 ^definition = An indication that the patient should seek post procedural follow-up directives from a clinical health care provider.
* #111122 "Known biopsy proven malignancy - take appropriate action"
* #111122 ^definition = A recommendation on a patient with known cancer to take steps appropriate to the diagnosis.
* #111123 "Marker placement"
* #111123 ^definition = Positioning of a radiopaque marker.
* #111124 "Personal history of breast cancer with mastectomy"
* #111124 ^definition = Patient has previous diagnosis of breast cancer resulting in mastectomy.
* #111125 "Known biopsy proven malignancy"
* #111125 ^definition = Patient has had biopsy containing proven malignancy.
* #111126 "Image detected mass"
* #111126 ^definition = Patient has a finding of mass reported on a prior imaging exam.
* #111127 "Targeted"
* #111127 ^definition = A breast imaging procedure performed on a specific area of the breast.
* #111128 "Survey"
* #111128 ^definition = A breast imaging procedure performed on the entire breast.
* #111129 "Clustered microcysts"
* #111129 ^definition = A cluster of tiny anechoic foci each smaller than 2-3 mm in diameter with thin (less than 0.5 mm) intervening septations and no discrete solid components.
* #111130 "Complicated cyst"
* #111130 ^definition = A fluid filled mass most commonly characterized by homogeneous low-level internal echoes on ultrasound.
* #111135 "Additional projections"
* #111135 ^definition = Views not inclusive of MLO and CC (BI-RADS®).
* #111136 "Spot magnification view(s)"
* #111136 ^definition = A spot or coned down compression of the breast providing a reduction in the thickness and a magnification of the localized area of interest and improved separation of breast tissue.
* #111137 "Ultrasound"
* #111137 ^designation[0].language = #de-AT 
* #111137 ^designation[0].value = "Retired. Replaced by (P5-B0099, SRT, 'Ultrasound procedure')." 
* #111138 "Old films for comparison"
* #111138 ^definition = Obtain previous mammography studies to compare to present study.
* #111139 "Ductography"
* #111139 ^definition = A medical procedure used for the sampling of mammary duct tissue
* #111139 ^designation[0].language = #de-AT 
* #111139 ^designation[0].value = "Retired. Replaced by (P5-40060, SRT, 'Mammary ductogram')." 
* #111140 "Normal interval follow-up"
* #111140 ^definition = Follow up study at 12 months for women ? 40 years of age having a prior negative study and no mitigating risk factors for breast cancer.
* #111141 "Any decision to biopsy should be based on clinical assessment"
* #111141 ^definition = Any decision to perform tissue acquisition should be based on clinical assessment.
* #111142 "Follow-up at short interval (1-11 months)"
* #111142 ^definition = Follow-up at short interval (1-11 months).
* #111143 "Biopsy should be considered"
* #111143 ^definition = Tissue acquisition should be considered.
* #111144 "Needle localization and biopsy"
* #111144 ^definition = Breast tissue acquisition following the identification of an area of concern with the placement of a needle or needle-wire assembly.
* #111145 "Histology using core biopsy"
* #111145 ^definition = Pathologic analysis of breast tissue and lesions using core tissue samples.
* #111146 "Suggestive of malignancy - take appropriate action"
* #111146 ^definition = Lesions that do not have the characteristic morphologies of breast cancer but have a definite probability of being malignant. There is a sufficient concern to urge a biopsy.
* #111147 "Cytologic analysis"
* #111147 ^definition = Cellular analysis of specimen.
* #111148 "Biopsy should be strongly considered"
* #111148 ^definition = Tissue acquisition should be strongly considered.
* #111149 "Highly suggestive of malignancy - take appropriate action"
* #111149 ^definition = Lesions have a high probability of being cancer, which require additional action.
* #111150 "Presentation Required: Rendering device is expected to present"
* #111150 ^definition = The producer of a report intends for a recipient of the report to present or display the associated content item.
* #111151 "Presentation Optional: Rendering device may present"
* #111151 ^definition = The producer of a report considers the presentation or display of the associated content item by a recipient to be optional.
* #111152 "Not for Presentation: Rendering device expected not to present"
* #111152 ^definition = The producer of a report intends for a recipient of the report NOT to present or display the associated content item.
* #111153 "Target content items are related temporally"
* #111153 ^definition = The associated content items are identified as being the same finding or feature at different points in time.
* #111154 "Target content items are related spatially"
* #111154 ^definition = The associated content items are identified as being the same finding or feature on different projections taken at the same point in time.
* #111155 "Target content items are related contra-laterally"
* #111155 ^definition = The associated content items are identified as being related side-to-side.
* #111156 "Feature detected on the only image"
* #111156 ^definition = There is one image in the interpreted data set.
* #111157 "Feature detected on only one of the images"
* #111157 ^definition = There is more than one image of the same modality in the interpreted data set.
* #111158 "Feature detected on multiple images"
* #111158 ^definition = There is more than one image of the same modality in the interpreted data set.
* #111159 "Feature detected on images from multiple modalities"
* #111159 ^definition = The interpreted data set contains images from multiple modalities.
* #111168 "Scar tissue"
* #111168 ^definition = The fibrous tissue replacing normal tissues destroyed by disease or injury
* #111168 ^designation[0].language = #de-AT 
* #111168 ^designation[0].value = "Retired. Replaced by (M-78060, SRT, 'Scar tissue')." 
* #111170 "J Wire"
* #111170 ^definition = A medical appliance used for localization of non palpable breast lesions to insure that the proper area is removed in a surgical biopsy
* #111170 ^designation[0].language = #de-AT 
* #111170 ^designation[0].value = "Retired. Replaced by (A-1016B, SRT, 'J Wire')." 
* #111171 "Pacemaker"
* #111171 ^definition = A medical appliance used for regulating cardiac rhythms
* #111171 ^designation[0].language = #de-AT 
* #111171 ^designation[0].value = "Retired. Replaced by (A-11101, SRT, 'Cardiac Pacemaker')." 
* #111172 "Paddle"
* #111172 ^definition = A compression device used for obtaining mammographic images
* #111172 ^designation[0].language = #de-AT 
* #111172 ^designation[0].value = "Retired. Replaced by (A-10042, SRT, 'Compression paddle')." 
* #111173 "Collimator"
* #111173 ^definition = A device used for restricting an X-Ray beam
* #111173 ^designation[0].language = #de-AT 
* #111173 ^designation[0].value = "Retired. Replaced by (A-10044, SRT, 'Collimator')." 
* #111174 "ID Plate"
* #111174 ^definition = An area designated on a radiographic film for facility and patient ID information
* #111174 ^designation[0].language = #de-AT 
* #111174 ^designation[0].value = "Retired. Replaced by (A-16016, SRT, 'ID Plate')." 
* #111175 "Other Marker"
* #111175 ^definition = Site specific markers.
* #111176 "Unspecified"
* #111176 ^definition = The value of the concept is not specified
* #111176 ^designation[0].language = #de-AT 
* #111176 ^designation[0].value = "This term may not be used in Context Group Extensions; see Section 7.2.3" 
* #111177 "View and Laterality Marker is missing"
* #111177 ^definition = Image quality deficiency according to MQSA.
* #111178 "View and Laterality Marker does not have both view and laterality"
* #111178 ^definition = Image quality deficiency according to MQCM.
* #111179 "View and Laterality Marker does not have approved codes"
* #111179 ^definition = Image quality deficiency according to MQCM.
* #111180 "View and Laterality Marker is not near the axilla"
* #111180 ^definition = Image quality deficiency according to MQCM.
* #111181 "View and Laterality Marker overlaps breast tissue"
* #111181 ^definition = Image quality deficiency according to MQCM.
* #111182 "View and Laterality Marker is partially obscured"
* #111182 ^definition = Image quality deficiency according to MQCM.
* #111183 "View and Laterality Marker is incorrect"
* #111183 ^definition = Image quality deficiency.
* #111184 "View and Laterality Marker is off image"
* #111184 ^definition = Image quality deficiency.
* #111185 "Flash is not near edge of film"
* #111185 ^definition = Image quality deficiency according to MQCM.
* #111186 "Flash is illegible, does not fit, or is lopsided"
* #111186 ^definition = Image quality deficiency according to MQSA.
* #111187 "Flash doesn't include patient name and additional patient id"
* #111187 ^definition = Image quality deficiency according to MQCM.
* #111188 "Flash doesn't include date of examination"
* #111188 ^definition = Image quality deficiency according to MQCM.
* #111189 "Flash doesn't include facility name and location"
* #111189 ^definition = Image quality deficiency according to MQSA.
* #111190 "Flash doesn't include technologist identification"
* #111190 ^definition = Image quality deficiency according to MQCM.
* #111191 "Flash doesn't include cassette/screen/detector identification"
* #111191 ^definition = Image quality deficiency according to MQCM.
* #111192 "Flash doesn't include mammography unit identification"
* #111192 ^definition = Image quality deficiency according to MQCM.
* #111193 "Date sticker is missing"
* #111193 ^definition = Image quality deficiency according to MQCM.
* #111194 "Technical factors missing"
* #111194 ^definition = Image quality deficiency according to MQCM.
* #111195 "Collimation too close to breast"
* #111195 ^definition = Image quality deficiency according to MQCM.
* #111196 "Inadequate compression"
* #111196 ^definition = Image quality deficiency according to MQCM.
* #111197 "MLO Insufficient pectoral muscle"
* #111197 ^definition = Image quality deficiency according to MQCM.
* #111198 "MLO No fat is visualized posterior to fibroglandular tissues"
* #111198 ^definition = Image quality deficiency according to MQCM.
* #111199 "MLO Poor separation of deep and superficial breast tissues"
* #111199 ^definition = Image quality deficiency according to MQCM.
* #111200 "MLO Evidence of motion blur"
* #111200 ^definition = Image quality deficiency according to MQCM.
* #111201 "MLO Inframammary fold is not open"
* #111201 ^definition = Image quality deficiency according to MQCM.
* #111202 "CC Not all medial tissue visualized"
* #111202 ^definition = Image quality deficiency according to MQCM.
* #111203 "CC Nipple not centered on image"
* #111203 ^definition = Image quality deficiency according to MQCM.
* #111204 "CC Posterior nipple line does not measure within 1 cm of MLO"
* #111204 ^definition = Image quality deficiency according to MQCM.
* #111205 "Nipple not in profile"
* #111205 ^definition = Image quality deficiency.
* #111206 "Insufficient implant displacement incorrect"
* #111206 ^definition = Image quality deficiency according to MQCM.
* #111207 "Image artifact(s)"
* #111207 ^definition = Signals that do not faithfully reproduce actual anatomic structures because of distortion or of addition or deletion of information.
* #111208 "Grid artifact(s)"
* #111208 ^definition = Feature(s) arising from the acquisition unit's anti-scatter grid mechanism. For two-dimensional systems, such features include those of mechanically damaged or incorrectly positioned grids. For moving or Bucky grids, artifacts may result from intentional grid motion that is inadequate in duration or velocity uniformity.
* #111209 "Positioning"
* #111209 ^definition = Inadequate arrangement of the anatomy of interest with respect to the X-Ray field and image detector sensitive area. Examples: 1) positioning is "cutoff" when the projection of anatomy of interest falls outside the sensitive area of the detector; 2) "cone cut", in which the X-Ray field does not adequately cover the anatomy of interest; 3) detector's sensitive surface is too small to cover the projection of the anatomy of interest; 4) improper angular orientation or "rotation" of anatomy of interest with respect to the X-Ray source, or detector; 5) projection of other anatomy or clothing over the anatomy of interest in the image.
* #111210 "Motion blur"
* #111210 ^definition = Unacceptable image blur resulting from motion of the anatomy of interest during exposure or the inadequately compensated motion of X-Ray source with respect to the image detector during exposure.
* #111211 "Under exposed"
* #111211 ^definition = Inadequate number of quanta reached the detector during exposure. Reasons for under exposed images include low kVp, low mAs product, excess Source Image Distance. Under exposed images have inadequate signal and higher noise in the areas of interest.
* #111212 "Over exposed"
* #111212 ^definition = An excess number of quanta reached the detector during exposure. Reasons for over exposed images include high kVp, high mAs product, short Source Image Distance. Over exposed images have high signal and lower noise in the areas of interest. Over exposed area may demonstrate lack of contrast from over saturation of the detector.
* #111213 "No image"
* #111213 ^definition = No evidence of a patient exposure.
* #111214 "Detector artifact(s)"
* #111214 ^definition = Superposed features or flaws of the detector.
* #111215 "Artifact(s) other than grid or detector artifact"
* #111215 ^definition = Features or discontinuities arising from causes other than the anti-scatter grid and image detector.
* #111216 "Mechanical failure"
* #111216 ^definition = Failure of the device to operate according to mechanical design specifications.
* #111217 "Electrical failure"
* #111217 ^definition = Failure of a device to operate according to electrical design specifications.
* #111218 "Software failure"
* #111218 ^definition = Attributable to software used in generation or handling of image.
* #111219 "Inappropriate image processing"
* #111219 ^definition = Images processed inappropriately, not following appropriate protocol.
* #111220 "Other failure"
* #111220 ^definition = Failure that is not mechanical or electrical or otherwise described.
* #111221 "Unknown failure"
* #111221 ^definition = Unidentified or unknown cause of failure.
* #111222 "Succeeded"
* #111222 ^definition = The attempted process was completely successful.
* #111223 "Partially Succeeded"
* #111223 ^definition = The attempted process succeeded in some ways, but failed in others.
* #111224 "Failed"
* #111224 ^definition = The attempted process completely failed.
* #111225 "Not Attempted"
* #111225 ^definition = No process was performed.
* #111233 "Individual Impression / Recommendation Analysis"
* #111233 ^definition = Analysis of a related group of findings or features detected during image data inspection, to produce a summary impression and/or recommendation.
* #111234 "Overall Impression / Recommendation Analysis"
* #111234 ^definition = Analysis of all groups of findings or features, to produce a single impression and/or recommendation.
* #111235 "Unusable - Quality renders image unusable"
* #111235 ^definition = The usability of an image for diagnostic interpretation or CAD, based on a quality control standard.
* #111236 "Usable - Does not meet the quality control standard"
* #111236 ^definition = The usability of an image for diagnostic interpretation or CAD, based on a quality control standard.
* #111237 "Usable - Meets the quality control standard"
* #111237 ^definition = The usability of an image for diagnostic interpretation or CAD, based on a quality control standard.
* #111238 "Mammography Quality Control Manual 1999, ACR"
* #111238 ^definition = An image quality control standard specified by the American College of Radiology.
* #111239 "Title 21 CFR Section 900, Subpart B"
* #111239 ^definition = An image quality control standard in the US Code of Federal Regulations.
* #111240 "Institutionally defined quality control standard"
* #111240 ^definition = An image quality control standard specified or adopted by the institution responsible for the document.
* #111241 "All algorithms succeeded; without findings"
* #111241 ^definition = No findings resulted upon successful completion of all attempted computer-aided detection and/or analysis.
* #111242 "All algorithms succeeded; with findings"
* #111242 ^definition = One or more findings resulted upon successful completion of all attempted computer-aided detection and/or analysis.
* #111243 "Not all algorithms succeeded; without findings"
* #111243 ^definition = No findings resulted from the attempted computer-aided detection and/or analysis, but one or more failures occurred in the process.
* #111244 "Not all algorithms succeeded; with findings"
* #111244 ^definition = One or more findings resulted from the attempted computer-aided detection and/or analysis, but one or more failures occurred in the process.
* #111245 "No algorithms succeeded; without findings"
* #111245 ^definition = All of the attempted computer-aided detection and/or analysis failed, so there could be no findings.
* #111248 "Adenolipoma"
* #111248 ^definition = A benign tumor having glandular characteristics but composed of fat, with the presence of normal mammary ducts
* #111248 ^designation[0].language = #de-AT 
* #111248 ^designation[0].value = "Retired. Replaced by (M-83240, SRT, 'Adenolipoma')." 
* #111249 "Ductal hyperplasia"
* #111249 ^designation[0].language = #de-AT 
* #111249 ^designation[0].value = "Retired. Replaced by (M-72170, SRT, 'Ductal hyperplasia, Usual')." 
* #111250 "Adenomyoepithelioma"
* #111250 ^definition = Neoplasms composed of myoepithelial cells
* #111250 ^designation[0].language = #de-AT 
* #111250 ^designation[0].value = "Retired. Replaced by (M-89830, SRT, 'Adenomyoepithelioma')." 
* #111251 "Normal axillary node"
* #111251 ^definition = Axillary node that is normal in appearance with no associated pathology.
* #111252 "Axillary node with calcifications"
* #111252 ^definition = Axillary node containing calcifications.
* #111253 "Axillary node hyperplasia"
* #111253 ^definition = Excessive proliferation of normal tissue arrangement of the axillary node.
* #111254 "Asynchronous involution"
* #111254 ^designation[0].language = #de-AT 
* #111254 ^designation[0].value = "Retired. Replaced by (F-8A063, SRT, 'Asynchronous involution of breast')." 
* #111255 "Benign cyst with blood"
* #111255 ^definition = Cyst with benign morphology containing blood.
* #111256 "Benign Calcifications"
* #111256 ^definition = Calcifications having typically benign morphology. They are not of intermediate or high probability of concern for malignancy.
* #111257 "Intracystic papilloma"
* #111257 ^definition = Growing within a cystic adenoma, filling the cavity with a mass of branching epithelial processes
* #111257 ^designation[0].language = #de-AT 
* #111257 ^designation[0].value = "Retired. Replaced by (M-85040, SRT, 'Intracystic papilloma')." 
* #111258 "Ductal adenoma"
* #111258 ^definition = Adenoma located in mammary duct, present as discrete sclerotic nodules, solitary or multiple.
* #111259 "Diabetic fibrous mastopathy"
* #111259 ^definition = The occurrence of fibrous tumor-forming stromal proliferation in patients with diabetes mellitus.
* #111260 "Extra abdominal desmoid"
* #111260 ^definition = A deep seated firm tumor frequently occurring on the chest consisting of collagenous tissue that infiltrates surround muscle; frequently recurs but does not metastasize
* #111260 ^designation[0].language = #de-AT 
* #111260 ^designation[0].value = "Retired. Replaced by (M-88211, SRT, 'Extra abdominal desmoid')." 
* #111262 "Epidermal inclusion cyst"
* #111262 ^definition = A cyst formed of a mass of epithelial cells, as a result of trauma has been pushed beneath the epidermis. The cyst is lined with squamous epithelium and contains concentric layers or keratin
* #111262 ^designation[0].language = #de-AT 
* #111262 ^designation[0].value = "Retired. Replaced by (M-33410, SRT, 'Epidermal inclusion cyst')." 
* #111263 "Fibroadenomatoid hyperplasia"
* #111263 ^definition = Excessive proliferation of fibroadenoma tissue.
* #111264 "Fibroadenolipoma"
* #111264 ^definition = A lipoma with an abundant stroma of fibrous tissue.
* #111265 "Foreign body (reaction)"
* #111265 ^designation[0].language = #de-AT 
* #111265 ^designation[0].value = "Retired. Replaced by (M-44140, SRT, 'Foreign body (reaction) ')." 
* #111269 "Galactocele"
* #111269 ^definition = Retention cyst caused by occlusion of a lactiferous duct
* #111269 ^designation[0].language = #de-AT 
* #111269 ^designation[0].value = "Retired. Replaced by (D7-90364, SRT, 'Galactocele')." 
* #111271 "Hemangioma - nonparenchymal, subcutaneous"
* #111271 ^definition = A congenital anomaly that leads to a proliferation of blood vessels leading to a mass that resembles a neoplasm, not located in parenchymal areas but subcutaneous
* #111271 ^designation[0].language = #de-AT 
* #111271 ^designation[0].value = "Retired. Replaced by (D3-F0620, SRT, 'Hemangioma of subcutaneous tissue')." 
* #111273 "Hyperplasia, usual"
* #111273 ^designation[0].language = #de-AT 
* #111273 ^designation[0].value = "Retired. Replaced by (M-72000, SRT, 'Hyperplasia, usual')." 
* #111277 "Juvenile papillomatosis"
* #111277 ^definition = A form of fibrocystic disease in young woman with florid and sclerosing adenosis that microscopically may suggest carcinoma.
* #111278 "Lactating adenoma"
* #111278 ^definition = Enlarging masses during lactation. A circumscribed benign tumor composed primarily of glandular structures with scanty stroma, with prominent secretory changes in the duct
* #111278 ^designation[0].language = #de-AT 
* #111278 ^designation[0].value = "Retired. Replaced by (M-82040, SRT, 'Lactating adenoma')." 
* #111279 "Lactational change"
* #111279 ^definition = Changes related to the process of lactation.
* #111281 "Large duct papilloma"
* #111281 ^definition = A papilloma pertaining to large mammary duct.
* #111283 "Myofibroblastoma"
* #111283 ^definition = Solitary or multiple tumors of muscles and fibrous tissues, or tumors composed of myofibroblasts
* #111283 ^designation[0].language = #de-AT 
* #111283 ^designation[0].value = "Retired. Replaced by (M-88250, SRT, 'Myofibroblastoma')." 
* #111284 "Microglandular adenosis"
* #111284 ^definition = Irregular clusters of small tubules are present in adipose or fibrous tissue, resembling tubular carcinoma but lacking stromal fibroblastic proliferation.
* #111285 "Multiple Intraductal Papillomas"
* #111285 ^definition = Papilloma typically involving an aggregate of adjacent ducts in the periphery of the breast, likely representing involvement of several foci of one or two duct systems.
* #111286 "No abnormality"
* #111286 ^definition = No abnormality.
* #111287 "Normal breast tissue"
* #111287 ^definition = Normal breast tissue.
* #111288 "Neurofibromatosis"
* #111288 ^definition = Condition in which there are tumors of various sizes on peripheral nerves. They may be neuromas or fibromas
* #111288 ^designation[0].language = #de-AT 
* #111288 ^designation[0].value = "Retired. Replaced by (M-95401, SRT, 'Neurofibromatosis')." 
* #111290 "Oil cyst (fat necrosis cyst)"
* #111290 ^definition = A cyst resulting from the loss of the epithelial lining of a sebaceous dermoid or lacteal cyst.
* #111291 "Post reduction mammoplasty"
* #111291 ^definition = Breast tissue with characteristics of a benign nature, following breast reduction surgery.
* #111292 "Pseudoangiomatous stromal hyperplasia"
* #111292 ^definition = A benign stromal lesion composed of intermixed stromal and epithelial elements. The lobular and duct structures of the breast parenchyma are separated by an increased amount of stroma, non specific proliferative epithelial changes include hyperplasia of duct and lobular epithelium often with accentuation of myoepithelial cells and aprocine metaplasia with or without cyst formation.
* #111293 "Radial scar"
* #111293 ^definition = An nonencapsulated stellate lesion consisting of a fibroelastic core and radiating bands of fibrous connective tissue containing lobules manifesting adenosis and ducts with papillary or diffuse intraductal hyperplasia
* #111293 ^designation[0].language = #de-AT 
* #111293 ^designation[0].value = "Retired. Replaced by (M-78731, SRT, 'Radial scar')." 
* #111294 "Sclerosing adenosis"
* #111294 ^definition = Prominent interductal fibrosis of the terminal ductules
* #111294 ^designation[0].language = #de-AT 
* #111294 ^designation[0].value = "Retired. Replaced by (M-74220, SRT, 'Sclerosing adenosis')." 
* #111296 "Silicone granuloma"
* #111296 ^definition = Nodular inflammatory lesions due to the presence of silicone in the breast tissue.
* #111297 "Nipple Characteristic"
* #111297 ^definition = The morphologic status of the nipple.
* #111298 "Virginal hyperplasia"
* #111298 ^definition = Spontaneous excessive proliferation of breast tissue, usually found in younger women.
* #111299 "Peripheral duct papillomas"
* #111299 ^definition = Papilloma(s) pertaining the peripheral ducts.
* #111300 "Axillary node with lymphoma"
* #111300 ^definition = Axillary node with lymphoid tissue neoplasm.
* #111301 "Axillary nodal metastases"
* #111301 ^definition = Metastatic disease to the axillary node.
* #111302 "Angiosarcoma"
* #111302 ^definition = A malignant neoplasm occurring most often in breast and skin, believed to originate from endothelial cells of blood vessels, microscopically composed of closely packed round or spindle shaped cells, some of which line small spaces resembling vascular clefts
* #111302 ^designation[0].language = #de-AT 
* #111302 ^designation[0].value = "Retired. Replaced by (M-91203, SRT, 'Angiosarcoma')." 
* #111303 "Blood vessel (vascular) invasion"
* #111303 ^definition = Histological changes to the vascular system related to an invasive process.
* #111304 "Carcinoma in children"
* #111304 ^definition = Carcinoma of the breast found in patients less than 20 years of age.
* #111305 "Carcinoma in ectopic breast"
* #111305 ^definition = A carcinoma found in supernumerary breasts and aberrant breast tissue.
* #111306 "Carcinoma with endocrine differentiation"
* #111306 ^definition = A carcinoma that synthesizes substances, including hormones, not considered to be normal products of the breast.
* #111307 "Basal cell carcinoma of nipple"
* #111307 ^definition = A basal cell carcinoma that arises in the nipple of the breast.
* #111308 "Carcinoma with metaplasia"
* #111308 ^designation[0].language = #de-AT 
* #111308 ^designation[0].value = "Retired. Replaced by (M-85733, SRT, 'Carcinoma with metaplasia')." 
* #111309 "Cartilaginous and osseous change"
* #111309 ^definition = Tissue changes to bones and cartilage.
* #111310 "Carcinoma in pregnancy and lactation"
* #111310 ^definition = Carcinoma of the breast presenting during pregnancy or lactation.
* #111311 "Carcinosarcoma"
* #111311 ^definition = A malignant neoplasm that contains elements of carcinoma and sarcoma, so extensively intermixed as to indicate neoplasia of epithelial and mesenchymal tissue
* #111311 ^designation[0].language = #de-AT 
* #111311 ^designation[0].value = "Retired. Replaced by (M-89803, SRT, 'Carcinosarcoma')." 
* #111312 "Intraductal comedocarcinoma with necrosis"
* #111312 ^definition = Comedocarcinoma of a duct with areas of necrotic tissue.
* #111313 "Intraductal carcinoma, low grade"
* #111313 ^definition = A non-invasive carcinoma restricted to the glandular lumen characterized by less aggressive malignant cytologic features and behavior.
* #111314 "Intraductal carcinoma micro-papillary"
* #111314 ^designation[0].language = #de-AT 
* #111314 ^designation[0].value = "Retired. Replaced by (M-85072, SRT, 'Intraductal carcinoma micro-papillary')." 
* #111315 "Intracystic papillary carcinoma"
* #111315 ^definition = A malignant neoplasm characterized by the formation of numerous, irregular, finger-like projections of fibrous stroma that is covered with a surface layer of neoplastic epithelial cells found in a cyst.
* #111316 "Invasive and in-situ carcinoma"
* #111316 ^definition = Carcinoma with both characteristics of localized and spreading disease.
* #111317 "Invasive lobular carcinoma"
* #111317 ^designation[0].language = #de-AT 
* #111317 ^designation[0].value = "Retired. Replaced by (M-85203, SRT, 'Invasive lobular carcinoma')." 
* #111318 "Leukemic infiltration"
* #111318 ^definition = Mammary infiltrates as a secondary manifestation in patients with established leukemia.
* #111320 "Lympathic vessel invasion"
* #111320 ^definition = Histological changes to the lymphatic system related to an invasive process.
* #111321 "Lymphoma"
* #111321 ^definition = A heterogeneous group of neoplasms arising in the reticuloendoethelial and lymphatic systems
* #111321 ^designation[0].language = #de-AT 
* #111321 ^designation[0].value = "Retired. Replaced by (M-95903, SRT, 'Lymphoma')." 
* #111322 "Occult carcinoma presenting with axillary lymph node metastases"
* #111322 ^definition = A small carcinoma, either asymptomatic or giving rise to metastases without symptoms due to the primary carcinoma presenting with metastatic disease in the axillary lymph nodes.
* #111323 "Metastatic cancer to the breast"
* #111323 ^definition = A malignant lesion in the breast with morphologic patterns not typical of breast carcinoma arising from a non-mammary malignant neoplasm.
* #111324 "Metastatic cancer to the breast from the colon"
* #111324 ^definition = A malignant lesion in the breast with morphologic patterns not typical of breast carcinoma arising from a neoplasm in the colon.
* #111325 "Metastatic cancer to the breast from the lung"
* #111325 ^definition = A malignant lesion in the breast with morphologic patterns not typical of breast carcinoma arising from a neoplasm in the lung.
* #111326 "Metastatic melanoma to the breast"
* #111326 ^definition = A malignant lesion in the breast with morphologic patterns not typical of breast carcinoma arising from a melanoma.
* #111327 "Metastatic cancer to the breast from the ovary"
* #111327 ^definition = A malignant lesion in the breast with morphologic patterns not typical of breast carcinoma arising from a neoplasm in the ovary.
* #111328 "Metastatic sarcoma to the breast"
* #111328 ^definition = A malignant lesion in the breast with morphologic patterns not typical of breast carcinoma arising from a sarcoma.
* #111329 "Multifocal intraductal carcinoma"
* #111329 ^definition = Multiple foci of non-invasive carcinoma restricted to the glandular lumen.
* #111330 "Metastatic disease to axillary node"
* #111330 ^definition = A malignant lesion in an axillary node arising from a non-axillary neoplasm.
* #111331 "Malignant fibrous histiocytoma"
* #111331 ^designation[0].language = #de-AT 
* #111331 ^designation[0].value = "Retired. Replaced by (M-88303, SRT, 'Malignant fibrous histiocytoma')." 
* #111332 "Multifocal invasive ductal carcinoma"
* #111332 ^definition = Multiple sites of ductal carcinoma.
* #111333 "Metastasis to an intramammary lymph node"
* #111333 ^definition = A malignant lesion in a intramammary lymph node arising from a non-intramammary lymph node neoplasm.
* #111334 "Malignant melanoma of nipple"
* #111334 ^definition = A malignant melanoma of the skin that arises in the nipple of the breast.
* #111335 "Neoplasm of the mammary skin"
* #111335 ^designation[0].language = #de-AT 
* #111335 ^designation[0].value = "Retired. Replaced by (D0-F035F, SRT, 'Neoplasm of the mammary skin')." 
* #111336 "Papillary carcinoma in-situ"
* #111336 ^designation[0].language = #de-AT 
* #111336 ^designation[0].value = "Retired. Replaced by (M-80502, SRT, 'Papillary carcinoma in-situ')." 
* #111338 "Recurrent malignancy"
* #111338 ^definition = Recurrent malignancy.
* #111340 "Squamous cell carcinoma of the nipple"
* #111340 ^definition = Squamous cell carcinoma to the terminal portion of the alveolar.
* #111341 "Intraductal carcinoma, high grade"
* #111341 ^definition = A non-invasive carcinoma restricted to the glandular lumen characterized by more aggressive malignant cytologic features and behavior.
* #111342 "Invasive cribriform carcinoma"
* #111342 ^designation[0].language = #de-AT 
* #111342 ^designation[0].value = "Retired. Replaced by (M-82013, SRT, 'Invasive cribriform carcinoma')." 
* #111343 "Angular margins"
* #111343 ^definition = An indication that some or all of the margin of a lesion has sharp corners, often forming acute angles.
* #111344 "Fine pleomorphic calcification"
* #111344 ^definition = Calcifications that vary in sizes and shapes and are usually smaller than0.5 mm in diameter.
* #111345 "Macrocalcifications"
* #111345 ^definition = Coarse calcifications that are 0.5 mm or greater in size.
* #111346 "Calcifications within a mass"
* #111346 ^definition = An indicator that calcifications are imbedded within a mass.
* #111347 "Calcifications outside of a mass"
* #111347 ^definition = An indicator that calcifications are imaged outside of a mass finding.
* #111350 "Breast background echotexture"
* #111350 ^definition = Tissue composition of the breast noted on sonography.
* #111351 "Homogeneous fat echotexture"
* #111351 ^definition = Fat lobules and uniformly echogenic bands of supporting structures comprise the bulk of breast tissue.
* #111352 "Homogeneous fibroglandular echotexture"
* #111352 ^definition = A uniformly echogenic layer of fibroglandular tissue is seen beneath a thin layer of subcutaneous fat.
* #111353 "Heterogeneous echotexture"
* #111353 ^definition = The breast texture is characterized by multiple small areas of increased and decreased echogenicity.
* #111354 "Orientation"
* #111354 ^definition = Referential relationship of the finding to the imaging device as noted on sonography.
* #111355 "Parallel"
* #111355 ^definition = The long axis of a lesion parallels the skin line ("wider-than-tall" or in a horizontal orientation).
* #111356 "Not parallel"
* #111356 ^definition = The anterior-posterior or vertical dimension is greater than the transverse or horizontal dimension.
* #111357 "Lesion boundary"
* #111357 ^definition = The lesion boundary describes the transition zone between themas and the surrounding tissue.
* #111358 "Abrupt interface"
* #111358 ^definition = The sharp demarcation between the lesion and surrounding tissue can be imperceptible or a distinct well-defined echogenic rim of any thickness.
* #111359 "Echogenic halo"
* #111359 ^definition = There is no sharp demarcation between the mass and the surrounding tissue, which is bridged by an echogenic transition zone.
* #111360 "Echo pattern"
* #111360 ^definition = An imaging characteristic of resonance noted during sonography.
* #111361 "Anechoic"
* #111361 ^definition = Without internal echoes.
* #111362 "Hyperechoic"
* #111362 ^definition = Having increased echogenicity relative to fat or equal to fibroglandular tissue.
* #111363 "Complex"
* #111363 ^definition = Mass contains both anechoic and echogenic components.
* #111364 "Hypoechoic"
* #111364 ^definition = Defined relative to fat; masses are characterized by low-level echoes throughout. E.g., appearance of a complicated cyst or fibroadenoma.
* #111365 "Isoechoic"
* #111365 ^definition = Having the same echogenicity as fat (a complicated cyst or fibroadenoma may be isoechoic or hypoechoic).
* #111366 "Posterior acoustic features"
* #111366 ^definition = The attenuation characteristics
* #111368 "Posterior enhancement"
* #111368 ^definition = Increased posterior echoes.
* #111369 "Posterior shadowing"
* #111369 ^definition = Decreased posterior echoes; edge shadows are excluded.
* #111370 "Combined posterior enhancement and shadowing"
* #111370 ^definition = More than one pattern of posterior attenuation, both shadowing and enhancement.
* #111371 "Identifiable effect on surrounding tissues"
* #111371 ^definition = Sonographic appearance of adjacent structures relative to a mass finding.
* #111372 "Vascularity"
* #111372 ^definition = Characterization of vascularization in region of interest.
* #111373 "Vascularity not present"
* #111373 ^definition = Vascularity not evident, such as on ultrasound.
* #111374 "Vascularity not assessed"
* #111374 ^definition = Vascularity not evaluated.
* #111375 "Vascularity present in lesion"
* #111375 ^definition = Vascularity on imaging is seen within a lesion.
* #111376 "Vascularity present immediately adjacent to lesion"
* #111376 ^definition = Vascularity on imaging is seen immediately adjacent to a lesion.
* #111377 "Diffusely increased vascularity in surrounding tissue"
* #111377 ^definition = Vascularity on imaging is considered diffusely elevated within the surrounding breast tissue.
* #111380 "Correlation to other Findings"
* #111380 ^definition = Relationship of the new anomaly to other clinical or imaging anomalies.
* #111381 "Correlates to physical exam findings"
* #111381 ^definition = An indication that the current imaging finding relates to a finding from a clinical breast exam.
* #111382 "Correlates to mammography findings"
* #111382 ^definition = An indication that the current imaging finding relates to a finding from a mammography exam.
* #111383 "Correlates to MRI findings"
* #111383 ^definition = An indication that the current imaging finding relates to a finding from a breast MRI exam.
* #111384 "Correlates to ultrasound findings"
* #111384 ^definition = An indication that the current imaging finding relates to a finding from a breast ultrasound exam.
* #111385 "Correlates to other imaging findings"
* #111385 ^definition = An indication that the current imaging finding relates to a finding from an imaging exam.
* #111386 "No correlation to other imaging findings"
* #111386 ^definition = An indication that the current imaging finding has no relation to findings from any other imaging exam.
* #111387 "No correlation to clinical findings"
* #111387 ^definition = An indication that the current imaging finding has no relation to any other clinical findings.
* #111388 "Malignancy Type"
* #111388 ^definition = Classification of the cancer as invasive, DCIS, or other.
* #111389 "Invasive breast carcinoma"
* #111389 ^definition = A malignancy that has spread beyond an area of focus.
* #111390 "Other malignancy type"
* #111390 ^definition = A breast cancer with malignant pathology findings that are not classified as invasive or in situ.
* #111391 "Menstrual Cycle Phase"
* #111391 ^definition = A specific timeframe during menses.
* #111392 "1st week"
* #111392 ^definition = In the first week of the menstrual cycle phase, that is, one week following menses.
* #111393 "2nd week"
* #111393 ^definition = In the second week of the menstrual cycle phase, that is, two weeks following menses.
* #111394 "3rd week"
* #111394 ^definition = In the third week of the menstrual cycle phase, that is, three weeks following menses.
* #111395 "Estimated Timeframe"
* #111395 ^definition = An estimated period of time.
* #111396 "< 3 months ago"
* #111396 ^definition = An event occurred less than 3 months ago.
* #111397 "4 months to 1 year ago"
* #111397 ^definition = An event occurred between 4 months and 1 year ago.
* #111398 "> 1 year ago"
* #111398 ^definition = An event occurred longer than 1 year ago.
* #111399 "Timeframe uncertain"
* #111399 ^definition = The timing of an event is not recalled.
* #111400 "Breast Imaging Report"
* #111400 ^definition = Report title for the diagnostic report for one or more breast imaging or intervention procedures.
* #111401 "Reason for procedure"
* #111401 ^definition = Concept name for the description of why a procedure has been performed.
* #111402 "Clinical Finding"
* #111402 ^definition = A finding during clinical examination (i.e., history and physical examination) such as pain, palpable mass or discharge.
* #111403 "Baseline screening mammogram"
* #111403 ^definition = First screening mammogram taken for patient that is used as a comparison baseline for further examinations.
* #111404 "First mammogram ever"
* #111404 ^definition = First mammogram taken for a patient without regard to whether it was for screening or a diagnostic procedure.
* #111405 "Implant type"
* #111405 ^definition = Concept name for the material of which a breast prosthetic device is constructed.
* #111406 "Number of similar findings"
* #111406 ^definition = A numeric count of findings classified as similar in nature.
* #111407 "Implant finding"
* #111407 ^definition = Concept name for the status of a breast prosthetic device as noted by imaging.
* #111408 "Film Screen Mammography"
* #111408 ^definition = Mammogram using traditional X-Ray film.
* #111409 "Digital Mammography"
* #111409 ^definition = Mammogram using a digital image acquisition system.
* #111410 "Surgical consult"
* #111410 ^definition = Referred for evaluation by a surgeon.
* #111411 "Mammography CAD"
* #111411 ^definition = Computer aided detection and/or computer aided diagnosis for mammography.
* #111412 "Narrative Summary"
* #111412 ^definition = Concept name for a text-based section of a report.
* #111413 "Overall Assessment"
* #111413 ^definition = A title for a report section that summarizes all interpretation results for a report with one overriding assessment. E.g., benign or negative.
* #111414 "Supplementary Data"
* #111414 ^definition = Concept name for a collection of supporting evidence for a report.
* #111415 "Additional evaluation requested from prior study"
* #111415 ^definition = Prior study indicates that additional imaging be performed to further evaluate a suspicious or questionable anatomic region.
* #111416 "Follow-up at short interval from prior study"
* #111416 ^definition = The prior study recommended a follow-up breast imaging exam in 1 to 11 months (generally in 6 months).
* #111417 "History of breast augmentation, asymptomatic"
* #111417 ^definition = Prior breast augmentation (breast enlargement) and is not presenting with any symptoms.
* #111418 "Review of an outside study"
* #111418 ^definition = Review or second opinion made on an image performed outside of the facility.
* #111419 "Additional evaluation requested from abnormal screening exam"
* #111419 ^definition = Additional breast imaging performed at the time of the patient's screening mammogram.
* #111420 "History of benign breast biopsy"
* #111420 ^definition = Patient has had previous benign breast biopsies.
* #111421 "Personal history of breast cancer with breast conservation therapy"
* #111421 ^definition = Patient has had a prior surgery such as a lumpectomy or quadrantectomy to remove malignant breast tissue, but breast tissue remains.
* #111423 "Physical Examination Results"
* #111423 ^definition = The results of a physical examination performed on the patient, possibly including the results of inspection, palpation, auscultation, or percussion.
* #111424 "Comparison to previous findings"
* #111424 ^definition = The result of assessing the current imaging exam in comparison to previous imaging exams.
* #111425 "Intraluminal filling defect"
* #111425 ^definition = An abnormality observed during ductography where the ductal system within the breast fills in an abnormal pattern. Ductography is an imaging exam in which a radio opaque contrast media is introduced into the ductal system of the breast through the nipple and images of the ductal system are obtained.
* #111426 "Multiple filling defect"
* #111426 ^definition = During ductography an observation of more than one filling abnormality within the breast ductal system.
* #111427 "Abrupt duct termination"
* #111427 ^definition = An abnormality observed during ductography where the ductal system within the breast terminates in an unusual fashion.
* #111428 "Extravasation"
* #111428 ^definition = Abnormal flowage of contrast media within the breast noted on ductography.
* #111429 "Duct narrowing"
* #111429 ^definition = An abnormality observed during ductography where the ductal system within the breast appears narrow.
* #111430 "Cyst fill"
* #111430 ^definition = During ductography an observation of the contrast media filling a cyst within the breast.
* #111431 "Instrument Approach"
* #111431 ^definition = The area and line within the anatomy through which a needle or instrument passes during an interventional procedure.
* #111432 "Inferolateral to superomedial"
* #111432 ^definition = The line within the anatomy from the lower outer to the upper inner aspect. E.g., through which a needle or instrument passes in an interventional procedure.
* #111433 "Inferomedial to superolateral"
* #111433 ^definition = The line within the anatomy from the lower inner to the upper outer aspect. E.g., through which a needle or instrument passes in an interventional procedure.
* #111434 "Superolateral to inferomedial"
* #111434 ^definition = The line within the anatomy from the upper outer to the lower inner aspect. E.g., through which a needle or instrument passes in an interventional procedure.
* #111435 "Superomedial to inferolateral"
* #111435 ^definition = The line within the anatomy from the upper inner to the lower outer aspect. E.g., through which a needle or instrument passes in an interventional procedure.
* #111436 "Number of passes"
* #111436 ^definition = The number of times a biopsy instrument is passed through an area of interest.
* #111437 "Number of specimens"
* #111437 ^definition = The number of biopsy specimens obtained from an interventional procedure.
* #111438 "Needle in target"
* #111438 ^definition = An indicator of whether or not a biopsy or localizing needle in an interventional procedure is seen to be in the area of interest.
* #111439 "Number of needles around target"
* #111439 ^definition = The number of localizing needles placed around the area of interest in an interventional procedure.
* #111440 "Incision made"
* #111440 ^definition = An indicator of whether or not an incision was made in the anatomy during an interventional procedure.
* #111441 "Microclip placed"
* #111441 ^definition = An indicator of whether or not a radio opaque microclip was placed in the anatomy during an interventional procedure.
* #111441 ^designation[0].language = #de-AT 
* #111441 ^designation[0].value = "Retired. Replaced by (111123, DCM, 'Marker placement') " 
* #111442 "Confirmation of target"
* #111442 ^definition = An indicator of the degree of success of an interventional procedure.
* #111443 "Target completely contained in the specimen"
* #111443 ^definition = An indicator that during an interventional procedure the area of interest was fully excised and is noted in the resultant biopsy specimen.
* #111444 "Target partially obtained in the specimen"
* #111444 ^definition = An indicator that during an interventional procedure the area of interest was partially excised and is noted in the resultant biopsy specimen.
* #111445 "Target not in the specimen"
* #111445 ^definition = An indicator that following an interventional procedure the area of interest is not seen in the resultant biopsy specimen.
* #111446 "Calcifications seen in the core"
* #111446 ^definition = An indicator that following an interventional procedure the targeted calcifications are noted in the resultant biopsy specimen.
* #111447 "Lesion completely removed"
* #111447 ^definition = An indicator that during an interventional procedure the area of interest was fully excised and is noted in the resultant biopsy specimen.
* #111448 "Lesion partially removed"
* #111448 ^definition = An indicator that during an interventional procedure the area of interest was partially excised and is noted in the resultant biopsy specimen.
* #111449 "Fluid obtained"
* #111449 ^definition = An indicator that during an interventional procedure fluid was successfully aspirated.
* #111450 "Light brown color"
* #111450 ^definition = Color that is a light shade of brown.
* #111451 "Dark red color"
* #111451 ^definition = Color that is a dark shade of red.
* #111452 "Dark brown color"
* #111452 ^definition = Color that is a dark shade of brown.
* #111453 "Bright red color"
* #111453 ^definition = Color that is a bright shade of red.
* #111454 "Blood tinged color"
* #111454 ^definition = Color that is tinged with the color of blood.
* #111455 "Occult blood test result"
* #111455 ^definition = An indicator of whether or not the fluid obtained during an interventional procedure contains red blood cells.
* #111456 "Action on fluid"
* #111456 ^definition = An indicator of whether or not fluid during an interventional procedure was sent for cytological analysis or simply discarded.
* #111457 "Sent for analysis"
* #111457 ^definition = An indicator that fluid obtained during an interventional procedure was sent to a laboratory for analysis.
* #111458 "Discarded"
* #111458 ^definition = An indicator that fluid obtained during an interventional procedure was discarded.
* #111459 "Mass with calcifications"
* #111459 ^definition = A radiopaque density noted during diagnostic imaging that has associated calcific densities.
* #111460 "Complex cyst"
* #111460 ^definition = A fluid-filled sac with greater than normal characteristics.
* #111461 "Intracystic lesion"
* #111461 ^definition = A tumor within a cyst.
* #111462 "Solid mass"
* #111462 ^definition = A tumor or lesion.
* #111463 "Supplementary Data for Intervention"
* #111463 ^definition = Supporting evidence for interpretation results of an interventional procedure.
* #111464 "Procedure Modifier"
* #111464 ^definition = A descriptor that further qualifies or characterizes a type of procedure.
* #111465 "Needle Gauge"
* #111465 ^definition = Needle size (diameter) characterization. E.g., of a biopsy needle.
* #111466 "Severity of Complication"
* #111466 ^definition = An indicator of the gravity of a problem experienced by a patient, related to a procedure that was performed.
* #111467 "Needle Length"
* #111467 ^definition = Distance from the hub or bushing to the tip of the needle.
* #111468 "Pathology Results"
* #111468 ^definition = The collection of observations and findings from pathologic analysis.
* #111469 "Sampling DateTime"
* #111469 ^definition = The date and time that the sample was collected from the patient.
* #111470 "Uninvolved"
* #111470 ^definition = Indicates that the margin of the biopsy specimen was not involved with the tumor.
* #111471 "Involved"
* #111471 ^definition = Indicates that the margin of the biopsy specimen was involved with the tumor.
* #111472 "Nipple involved"
* #111472 ^definition = Indicates whether the nipple was involved in an interventional procedure or pathologic analysis.
* #111473 "Number of nodes removed"
* #111473 ^definition = Indicates the number of lymph nodes removed.
* #111474 "Number of nodes positive"
* #111474 ^definition = Indicates the number of lymph nodes removed that contain cancer cells.
* #111475 "Estrogen receptor"
* #111475 ^definition = The result of a test for the presence of a protein that binds with estrogen.
* #111476 "Progesterone receptor"
* #111476 ^definition = The result of a test for the presence of a protein that binds with progesterone.
* #111477 "S Phase"
* #111477 ^definition = Indicates the percentage of cells in S phase. Cell division is defined by phases; the S phase is the stage during which DNA replicates.
* #111478 "Non-bloody discharge (from nipple)"
* #111478 ^definition = The visible emission of non-bloody fluid from the nipple.
* #111479 "Difficult physical/clinical examination"
* #111479 ^definition = The inability to discern normal versus abnormal breast tissue during palpation.
* #111480 "Cancer elsewhere"
* #111480 ^definition = An indication that a patient has or had a malignant occurrence in an area of the body other than the breast.
* #111481 "Saline implant"
* #111481 ^definition = A salt water filled prosthetic device implanted in the breast.
* #111482 "Polyurethane implant"
* #111482 ^definition = A polymer based (plastic) prosthetic device implanted in the breast.
* #111483 "Percutaneous silicone injection"
* #111483 ^definition = The introduction of polymeric organic silicon based material through the skin, as for breast augmentation or reconstruction.
* #111484 "Combination implant"
* #111484 ^definition = A prosthetic device that contains more than one material implanted in the breast.
* #111485 "Pre-pectoral implant"
* #111485 ^definition = A breast implant placed in front of the pectoralis major muscle.
* #111486 "Retro-pectoral implant"
* #111486 ^definition = A breast implant placed behind the pectoralis major muscle.
* #111487 "Mammographic (crosshair)"
* #111487 ^definition = Using X-Ray technique and a superimposed set of crossed lines for detection or placement.
* #111488 "Mammographic (grid)"
* #111488 ^definition = Using X-Ray technique and a superimposed aperture for detection or placement.
* #111489 "Palpation guided"
* #111489 ^definition = Using physical touch for detection or placement.
* #111490 "Vacuum assisted"
* #111490 ^definition = The performance of a biopsy procedure using a vacuum device attached to the biopsy needle.
* #111491 "Abnormal discharge"
* #111491 ^definition = Unusual or unexpected emission of fluid.
* #111492 "No complications"
* #111492 ^definition = Having experienced no adverse medical conditions related to or resulting from an interventional procedure.
* #111494 "Stage 0"
* #111494 ^definition = TNM grouping of tumor stage, from AJCC, where primary tumor is Tis, regional lymph node is N0, and distant metastasis is M0.
* #111495 "Stage I"
* #111495 ^definition = TNM grouping of tumor stage, from AJCC, where primary tumor is T1, regional lymph node is N0, and distant metastasis is M0.
* #111496 "Stage IIA"
* #111496 ^definition = TNM grouping of tumor stage, from AJCC, where primary tumor is T0 or T1, with regional lymph node N1 and distant metastasis is M0, or T2 with N0 and M0.
* #111497 "Stage IIB"
* #111497 ^definition = TNM grouping of tumor stage, from AJCC, where primary tumor is T2, with regional lymph node N1 and distant metastasis is M0, or T3 with N0 and M0.
* #111498 "Stage IIIA"
* #111498 ^definition = TNM grouping of tumor stage, from AJCC, where primary tumor is T0, T1 or T2, with regional lymph node N2 and distant metastasis is M0, or T3 with N1 or N2 and M0.
* #111499 "Stage IIIB"
* #111499 ^definition = TNM grouping of tumor stage, from AJCC, where primary tumor is T4, regional lymph node is N0, N1 or N2, and distant metastasis is M0.
* #111500 "Stage IIIC"
* #111500 ^definition = TNM grouping of tumor stage, from AJCC, where primary tumor is any T value, regional lymph node is N3, and distant metastasis is M0.
* #111501 "Stage IV"
* #111501 ^definition = TNM grouping of tumor stage, from AJCC, where primary tumor is any T value, regional lymph node is any N value, and distant metastasis is M1.
* #111502 "Bloom-Richardson Grade"
* #111502 ^definition = Histologic tumor grade (sometimes called Scarff-Bloom-Richardson grade) is based on the arrangement of the cells in relation to each other -- whether they form tubules, how closely they resemble normal breast cells (nuclear grade) and how many of the cancer cells are in the process of dividing (mitotic count).
* #111503 "Normal implants"
* #111503 ^definition = Breast prosthetic devices are intact, not leaking, and are in a normal shape and form.
* #111504 "Asymmetric implants"
* #111504 ^definition = Breast prosthetic devices are not symmetric, equal, corresponding in form, or are in one breast (unilateral).
* #111505 "Calcified implant"
* #111505 ^definition = Fibrous or calcific contracture of the tissue capsule that forms around a breast prosthetic device.
* #111506 "Distorted implant"
* #111506 ^definition = Breast prosthetic device is twisted out of normal shape or form.
* #111507 "Silicone-laden lymph nodes"
* #111507 ^definition = Silicone from breast prosthetic device found in lymphatic tissue.
* #111508 "Free silicone"
* #111508 ^definition = Silicone found in breast tissue outside of the prosthetic capsule or implant membrane.
* #111509 "Herniated implant"
* #111509 ^definition = Protrusion of part of the structure normally encapsulating the content of the breast prosthetic device.
* #111510 "Explantation"
* #111510 ^definition = Evidence of removal of a breast prosthetic device.
* #111511 "Relevant Patient Information for Breast Imaging"
* #111511 ^definition = Historical patient health information of interest to the breast health clinician.
* #111512 "Medication History"
* #111512 ^definition = Information regarding usage by the patient of certain medications, such as hormones.
* #111512 ^designation[0].language = #de-AT 
* #111512 ^designation[0].value = "Retired. Replaced by (10160-0, LN, 'History Of Medication Use') " 
* #111513 "Relevant Previous Procedures"
* #111513 ^definition = Interventional or non-interventional procedures previously performed on the patient, such as breast biopsies.
* #111514 "Relevant Indicated Problems"
* #111514 ^definition = Abnormal conditions experienced by the patient that serve as the reason for performing a procedure, such as a breast exam.
* #111514 ^designation[0].language = #de-AT 
* #111514 ^designation[0].value = "Retired. Replaced by (11450-4, LN, 'Problem List') " 
* #111515 "Relevant Risk Factors"
* #111515 ^definition = Personal, familial, and other health factors that may indicate an increase in the patient's chances of developing a health condition or disease, such as breast cancer.
* #111516 "Medication Type"
* #111516 ^definition = A classification of a medicinal substance, such as hormonal contraceptive or antibiotic.
* #111517 "Relevant Patient Information"
* #111517 ^definition = Historical patient health information for general purpose use.
* #111518 "Age when first menstrual period occurred"
* #111518 ^definition = The age of the patient at the first occurrence of menses.
* #111519 "Age at First Full Term Pregnancy"
* #111519 ^definition = The age of the patient at the time of her first full term pregnancy.
* #111520 "Age at Menopause"
* #111520 ^definition = The age of the patient at the cessation of menses.
* #111521 "Age when hysterectomy performed"
* #111521 ^definition = The age of the patient at the time her uterus was removed.
* #111522 "Age when left ovary removed"
* #111522 ^definition = The age of the patient at the time she had her left ovary removed.
* #111523 "Age when right ovary removed"
* #111523 ^definition = The age of the patient at the time she had her right ovary removed.
* #111524 "Age Started"
* #111524 ^definition = The age of a patient on the first occurrence of an event, such as the first use of a medication.
* #111525 "Age Ended"
* #111525 ^definition = The age of a patient on the last occurrence of an event, such as the last use of a medication.
* #111526 "DateTime Started"
* #111526 ^definition = The date and time of the first occurrence of an event, such as the first use of a medication.
* #111527 "DateTime Ended"
* #111527 ^definition = The date and time of the last occurrence of an event, such as the last use of a medication.
* #111528 "Ongoing"
* #111528 ^definition = An indicator of whether an event is still in progress, such as the use of a medication or substance, or environmental exposure.
* #111529 "Brand Name"
* #111529 ^definition = Product name of a device or substance, such as medication, to identify it as the product of a single firm or manufacturer.
* #111530 "Risk Factor modifier"
* #111530 ^definition = A descriptor that further qualifies or characterizes a risk factor.
* #111531 "Previous Procedure"
* #111531 ^definition = A prior non-interventional exam or interventional procedure performed on a patient.
* #111532 "Pregnancy Status"
* #111532 ^definition = Describes the pregnancy state of a referenced subject.
* #111533 "Indicated Problem"
* #111533 ^definition = A symptom experienced by a patient that is used as the reason for performing an exam or procedure.
* #111534 "Role of person reporting"
* #111534 ^definition = The function of the individual who is reporting information on a patient, which could be a specific health care related profession, the patient him/herself, or a relative or friend.
* #111535 "DateTime problem observed"
* #111535 ^definition = The date and time that a symptom was noted.
* #111536 "DateTime of last evaluation"
* #111536 ^definition = The date and time of the most recent evaluation of an indicated problem.
* #111537 "Family Member with Risk Factor"
* #111537 ^definition = A patient's biological relative who exhibits a health factor that may indicate an increase in the patient's chances of developing a particular disease or medical problem.
* #111538 "Age at Occurrence"
* #111538 ^definition = The age at which an individual experienced a specific event, such as breast cancer.
* #111539 "Menopausal phase"
* #111539 ^definition = The current stage of an individual in her gynecological development.
* #111540 "Side of Family"
* #111540 ^definition = An indicator of paternal or maternal relationship.
* #111541 "Maternal"
* #111541 ^definition = Relating to biological female parentage.
* #111542 "Unspecified gynecological hormone"
* #111542 ^definition = A gynecological hormone for which the specific type is not specified. E.g., contraceptive, estrogen, Tamoxifen.
* #111543 "Breast feeding history"
* #111543 ^definition = An indicator of whether or not a patient ever provided breast milk to her offspring.
* #111544 "Average breast feeding period"
* #111544 ^definition = The average length of time that a patient provided breast milk to her offspring.
* #111545 "Substance Use History"
* #111545 ^definition = Information regarding usage by the patient of certain legal or illicit substances.
* #111546 "Used Substance Type"
* #111546 ^definition = A classification of a substance, such as alcohol or a legal or illicit drug.
* #111547 "Environmental Exposure History"
* #111547 ^definition = Information regarding exposure of the patient to potentially harmful environmental factors.
* #111548 "Environmental Factor"
* #111548 ^definition = A classification of a potentially harmful substance or gas in a subject's environment, such as asbestos, lead, or carcinogens.
* #111549 "Previous Reports"
* #111549 ^definition = Previous Structured Reports that could have relevant information for a current imaging service request.
* #111550 "Personal breast cancer history"
* #111550 ^definition = An indication that a patient has had a previous malignancy of the breast.
* #111551 "History of endometrial cancer"
* #111551 ^definition = Indicates a previous occurrence of cancer of the lining of the uterus.
* #111552 "History of ovarian cancer"
* #111552 ^definition = Indicates a previous occurrence of cancer of the lining of the ovary.
* #111553 "History of high risk lesion on previous biopsy"
* #111553 ^definition = Indicates a prior diagnosis of pre-cancerous cells or tissue removed for pathologic evaluation.
* #111554 "Post menopausal patient"
* #111554 ^definition = A female patient whose menstrual periods have ceased.
* #111555 "Late child bearing (after 30)"
* #111555 ^definition = A female patient whose first child was born after the patient was 30 years old.
* #111556 "BRCA1 breast cancer gene"
* #111556 ^definition = The first level genetic marker indicating risk for breast cancer.
* #111557 "BRCA2 breast cancer gene"
* #111557 ^definition = The second level genetic marker indicating risk for breast cancer.
* #111558 "BRCA3 breast cancer gene"
* #111558 ^definition = The third level genetic marker indicating risk for breast cancer.
* #111559 "Weak family history of breast cancer"
* #111559 ^definition = A patient's biological aunt, grandmother, or female cousin was diagnosed with breast cancer. Definition from BI-RADS®.
* #111560 "Intermediate family history of breast cancer"
* #111560 ^definition = A patient's biological mother or sister was diagnosed with breast cancer after they had gone through menopause. Definition from BI-RADS®.
* #111561 "Very strong family history of breast cancer"
* #111561 ^definition = A patient's biological mother or sister was diagnosed with breast cancer before they had gone through menopause, or more than one of the patient's first-degree relatives (biological mother or sister) were diagnosed with breast cancer after they had gone through menopause. Definition from BI-RADS®.
* #111562 "Family history of prostate cancer"
* #111562 ^definition = Previous diagnosis of a malignancy of the prostate gland in a biological relative.
* #111563 "Family history unknown"
* #111563 ^definition = The health record of a patient's biological relatives is not known.
* #111564 "Nipple discharge cytology"
* #111564 ^definition = The study of cells obtained from fluid emitted from the breast.
* #111565 "Uterine malformations"
* #111565 ^definition = A developmental abnormality resulting in an abnormal shape of the uterus.
* #111566 "Spontaneous Abortion"
* #111566 ^definition = A naturally occurring premature expulsion from the uterus of the products of conception - the embryo or a nonviable fetus.
* #111567 "Gynecologic condition"
* #111567 ^definition = An ailment/abnormality or state of the female reproductive tract.
* #111568 "Gynecologic surgery"
* #111568 ^definition = A surgical operation performed on any portion of the female reproductive tract.
* #111569 "Previous LBW or IUGR birth"
* #111569 ^definition = Prior pregnancy with a low birth weight baby or a fetus with Intrauterine Growth Restriction or Retardation.
* #111570 "Previous fetal malformation/syndrome"
* #111570 ^definition = History of at least one prior pregnancy with fetal anatomic abnormality(s).
* #111571 "Previous RH negative or blood dyscrasia at birth"
* #111571 ^definition = History of delivering a Rhesis Isoimmunization affected child(ren) or a child(ren) with another blood disorder.
* #111572 "History of multiple fetuses"
* #111572 ^definition = History of at least one pregnancy that contained more than one fetus. E.g., twins, triplets, etc..
* #111573 "Current pregnancy, known or suspected malformations/syndromes"
* #111573 ^definition = At least one fetus of this pregnancy has an anatomic abnormality(s) that is known to exist, or a "marker" is present that suggests the abnormality(s) may be present.
* #111574 "Family history, fetal malformation/syndrome"
* #111574 ^definition = Biological relatives have previously conceived a fetus with an anatomic abnormality(s).
* #111575 "High"
* #111575 ^definition = A subjective descriptor for an elevated amount of exposure, use, or dosage, incurring high risk of adverse effects.
* #111576 "Medium"
* #111576 ^definition = A subjective descriptor for a moderate amount of exposure, use, or dosage, incurring medium risk of adverse effects.
* #111577 "Low"
* #111577 ^definition = A subjective descriptor for a limited amount of exposure, use, or dosage, incurring low risk of adverse effects.
* #111578 "Dose frequency"
* #111578 ^definition = A measurement of the rate of occurrence of which a patient takes a certain medication.
* #111579 "Rate of exposure"
* #111579 ^definition = The quantity per unit of time that a patient was or is being exposed to an environmental irritant.
* #111580 "Volume of use"
* #111580 ^definition = The quantity per unit of time that a medication or substance was or is being used.
* #111581 "Relative dose amount"
* #111581 ^definition = A qualitative descriptor for the amount of a medication that was or is being taken.
* #111582 "Relative amount of exposure"
* #111582 ^definition = A qualitative descriptor for the amount of present or past exposure to an environmental irritant.
* #111583 "Relative amount of use"
* #111583 ^definition = A qualitative descriptor for the amount of a medication or substance that was or is being used.
* #111584 "Relative dose frequency"
* #111584 ^definition = A qualitative descriptor for the frequency with which a medication was or is being taken.
* #111585 "Relative frequency of exposure"
* #111585 ^definition = A qualitative descriptor for the frequency of present or past exposure to an environmental irritant.
* #111586 "Relative frequency of use"
* #111586 ^definition = A qualitative descriptor for the frequency with which a medication or substance was or is being used.
* #111587 "No known exposure"
* #111587 ^definition = Patient is not known to have been exposed to or used the substance or medication.
* #111590 "Recall for technical reasons"
* #111590 ^definition = Patient returns for additional images to improve the quality of the most recent exam.
* #111591 "Recall for imaging findings"
* #111591 ^definition = Patient returns for additional images to clarify findings from the most recent exam.
* #111592 "Recall for patient symptoms/ clinical findings"
* #111592 ^definition = Patient returns for additional images to clarify symptoms or signs reported by the patient or a healthcare professional at the time of the most recent exam.
* #111593 "LBW or IUGR"
* #111593 ^definition = Number of births with low birth weight or intrauterine growth restriction.
* #111601 "Green filter"
* #111601 ^definition = Filter that transmits green light while blocking the other colors, typically centered at 510-540 nm
* #111601 ^designation[0].language = #de-AT 
* #111601 ^designation[0].value = "Retired. Replaced by (A-010E2, SRT, 'Green optical filter')" 
* #111602 "Red filter"
* #111602 ^definition = Filter that transmits red light while blocking the other colors, typically centered at 630-680 nm
* #111602 ^designation[0].language = #de-AT 
* #111602 ^designation[0].value = "Retired. Replaced by (A-010DF, SRT, 'Red optical filter')" 
* #111603 "Blue filter"
* #111603 ^definition = Filter that transmits blue while blocking the other colors, typically centered at 460-480 nm
* #111603 ^designation[0].language = #de-AT 
* #111603 ^designation[0].value = "Retired. Replaced by (A-010DA, SRT, 'Blue optical filter')" 
* #111604 "Yellow-green filter"
* #111604 ^definition = A filter of 560nm that is used for retinal imaging and can provide good contrast and good visibility of the retinal vasculature
* #111604 ^designation[0].language = #de-AT 
* #111604 ^designation[0].value = "Retired. Replaced by (A-010E0, SRT, 'Yellow-green optical filter')" 
* #111605 "Blue-green filter"
* #111605 ^definition = A filter of 490nm that is used for retinal imaging because of excessive scattering of some retinal structures at very short wavelengths
* #111605 ^designation[0].language = #de-AT 
* #111605 ^designation[0].value = "Retired. Replaced by (A-010D8, SRT, 'Blue-green optical filter')" 
* #111606 "Infrared filter"
* #111606 ^definition = Filter that transmits the infrared spectrum, which is light that lies outside of the visible spectrum, with wavelengths longer than those of red light, while blocking visible light
* #111606 ^designation[0].language = #de-AT 
* #111606 ^designation[0].value = "Retired. Replaced by (A-010DC, SRT, 'Infrared optical filter')" 
* #111607 "Polarizing filter"
* #111607 ^definition = A filter that reduces reflections from non-metallic surfaces such as glass or water by blocking light waves that are vibrating at selected angles to the filter.
* #111607 ^designation[0].language = #de-AT 
* #111607 ^designation[0].value = "Retired. Replaced by (A-010E1, SRT, 'Polarizing optical filter')" 
* #111609 "No filter"
* #111609 ^definition = No filter used.
* #111621 "Field 1 for Joslin 3 field"
* #111621 ^definition = Joslin NM-1 is a 45 degree field focused centrally between the temporal margin of optic disc and the center of the macula: Center the camera on the papillomacular bundle midway between the temporal margin of the optic disc and the center of the macula. The horizontal centerline of the image should pass directly through the center of the disc.
* #111622 "Field 2 for Joslin 3 field"
* #111622 ^definition = Joslin NM-2 is a 45 degree field focused superior temporal to the optic disc: Center the camera laterally approximately one-half disc diameter temporal to the center of the macula. The lower edge of the field is tangent to a horizontal line passing through the upper edge of the optic disc. The image is taken temporal to the macula but includes more retinal nasal and superior to the macula than standard field 2.
* #111623 "Field 3 for Joslin 3 field"
* #111623 ^definition = Joslin NM-3 is a 45 degree field focused nasal to the optic disc: This field is nasal to the optic disc and may include part of the optic disc. The horizontal centerline of the image should pass tangent to the lower edge of the optic disc.
* #111625 "Diffuse direct illumination"
* #111625 ^definition = A broad or "soft" light supplied from a single source.
* #111626 "Scheimpflug Camera"
* #111626 ^definition = A slit reflected light microscope, which has the ability to form an image of the back scattered light from the eye in a sagittal plane. Scheimpflug cameras are able to achieve a wide depth of focus by employing the "Sheimpflug principle" where the lens and image planes are not parallel with each other. Rotating Sheimplug cameras are able to generate three-dimensional images and calculate measurements of the anterior chamber of the eye.
* #111627 "Scotopic light"
* #111627 ^definition = Lighting condition approximately 0.04 lux.
* #111628 "Mesopic light"
* #111628 ^definition = Lighting condition approximately 4 lux.
* #111629 "Photopic light"
* #111629 ^definition = Lighting condition approximately 40 lux.
* #111630 "Dynamic light"
* #111630 ^definition = Acquisition preceded by intense light.
* #111631 "Average Glandular Dose"
* #111631 ^definition = Calculated from values of entrance exposure in air, the X-Ray beam quality (half-value layer), and compressed breast thickness, is the energy deposited per unit mass of glandular tissue averaged over all the glandular tissue in the breast.
* #111632 "Anode Target Material"
* #111632 ^definition = The primary material in the anode of an X-Ray source.
* #111633 "Compression Thickness"
* #111633 ^definition = The average thickness of the body part examined when compressed, if compression has been applied during X-Ray exposure.
* #111634 "Half Value Layer"
* #111634 ^definition = Thickness of Aluminum required to reduce the X-Ray output at the patient entrance surface by a factor of two.
* #111635 "X-Ray Grid"
* #111635 ^definition = An anti-scatter device based on radiation absorbing strips above the detector. E.g., in the patient support.
* #111636 "Entrance Exposure at RP"
* #111636 ^definition = Exposure measurement in air at the reference point that does not include back scatter, according to MQCM 1999.
* #111637 "Accumulated Average Glandular Dose"
* #111637 ^definition = Average Glandular Dose to a single breast accumulated over multiple images.
* #111638 "Patient Equivalent Thickness"
* #111638 ^definition = Value of the control variable used to parametrize the Automatic Exposure Control (AEC) closed loop. E.g., "Water Value".
* #111641 "Fixed grid"
* #111641 ^definition = An X-Ray Grid that does not move during exposure.
* #111642 "Focused grid"
* #111642 ^definition = An X-Ray Grid with radiation absorbing strips that are focused toward the focal spot, to eliminate grid cutoff.
* #111643 "Reciprocating grid"
* #111643 ^definition = An X-Ray Grid that is designed to move during exposure, to eliminate the appearance of grid lines on the image.
* #111644 "Parallel grid"
* #111644 ^definition = An X-Ray Grid with radiation absorbing strips that are parallel to each other and that is used only with long source to image distances.
* #111645 "Crossed grid"
* #111645 ^definition = An X-Ray Grid with crossed radiation absorbing strips used for more complete cleanup of scatter radiation.
* #111646 "No grid"
* #111646 ^definition = No X-Ray Grid was used due to low scatter conditions.
* #111671 "Spectacle Prescription Report"
* #111671 ^definition = The spectacle prescription for a patient.
* #111672 "Add Near"
* #111672 ^definition = Refractive measurements of the eye to correct for inability to focus at near while wearing the distance prescription.
* #111673 "Add Intermediate"
* #111673 ^definition = Refractive measurements of the eye to correct for inability to focus at intermediate distance while wearing the distance prescription.
* #111674 "Add Other"
* #111674 ^definition = Refractive measurements of the eye to correct for inability to focus at the specified distance while wearing the distance prescription.
* #111675 "Horizontal Prism Power"
* #111675 ^definition = The power of a prism to bend light in the horizontal direction, in prism diopters.
* #111676 "Horizontal Prism Base"
* #111676 ^definition = Direction of the base of a horizontal prism -- either in (toward the nose), or out (away from the nose).
* #111677 "Vertical Prism Power"
* #111677 ^definition = The power of a prism to bend light in the vertical direction, in prism diopters.
* #111678 "Vertical Prism Base"
* #111678 ^definition = Direction of the base of a vertical prism -- either up, or down.
* #111679 "Distance Pupillary Distance"
* #111679 ^definition = Distance in mm between the pupils when the patient's object of regard is in the distance.
* #111680 "Near Pupillary Distance"
* #111680 ^definition = Distance in mm between the pupils when the patient's object of regard is at near.
* #111685 "Autorefraction Visual Acuity"
* #111685 ^definition = A patient's vision with the correction measured by an autorefractor in place.
* #111686 "Habitual Visual Acuity"
* #111686 ^definition = A patient's vision with whichever vision correction the patient customarily wears.
* #111687 "Prescription Visual Acuity"
* #111687 ^definition = A patient's vision with the final spectacle prescription in place.
* #111688 "Right Eye Rx"
* #111688 ^definition = The spectacle prescription for the right eye.
* #111689 "Left Eye Rx"
* #111689 ^definition = The spectacle prescription for the left eye.
* #111690 "Macular Grid Thickness and Volume Report"
* #111690 ^definition = A macular grid thickness and volume report for a patient. The macular grid is an analytic tool described in PS3.1.
* #111691 "Number of Images Used for Macular Measurements"
* #111691 ^definition = Number of images used for the macular grid measurement.
* #111692 "Number of Samples Used per Image"
* #111692 ^definition = Number of samples used per Image for analysis.
* #111693 "Analysis Quality Rating"
* #111693 ^definition = A numeric rating of the quality of the entire analysis with respect to grading and diagnostic purposes.
* #111694 "Image Set Quality Rating"
* #111694 ^definition = A numeric rating of the quality of an entire image set with respect to grading and diagnostic purposes.
* #111695 "Interfering Tears or Drops"
* #111695 ^definition = Tear film or drops affecting test quality.
* #111696 "Visual Fixation Quality During Acquisition"
* #111696 ^definition = The assessment of the centricity and persistence of the visual fixation (direction of gaze) during the acquisition.
* #111697 "Visual Fixation Quality Problem"
* #111697 ^definition = The reason why the patient's visual fixation was not steady or was indeterminate.
* #111698 "Ophthalmic Macular Grid Problem"
* #111698 ^definition = The reason why the macular grid measurements may be questionable.
* #111700 "Specimen Container Identifier"
* #111700 ^definition = Identifier of container (box, block, microscope slide, etc.) for the specimen under observation.
* #111701 "Processing type"
* #111701 ^definition = Type of processing that tissue specimen underwent.
* #111702 "DateTime of processing"
* #111702 ^definition = Date and time of processing step.
* #111703 "Processing step description"
* #111703 ^definition = Description of the individual step in the tissue processing sequence.
* #111704 "Sampling Method"
* #111704 ^definition = Method of sampling used to derive specimen from its parent.
* #111705 "Parent Specimen Identifier"
* #111705 ^definition = Identifier of the parent specimen that gave rise to the current specimen.
* #111706 "Issuer of Parent Specimen Identifier"
* #111706 ^definition = Assigning authority for parent specimen's identifier.
* #111707 "Parent specimen type"
* #111707 ^definition = Parent specimen type that gave rise to current specimen.
* #111708 "Position Frame of Reference"
* #111708 ^definition = Description of coordinate system and origin reference point on parent specimen, or parent specimen container, or image used for localizing the sampling site or location within container or image.
* #111709 "Location of sampling site"
* #111709 ^definition = Reference to image of parent specimen localizing the sampling site; may include referenced Presentation State object.
* #111710 "Location of sampling site X offset"
* #111710 ^definition = Location of sampling site of specimen (nominal center) relative to the Position Frame of Reference.
* #111711 "Location of sampling site Y offset"
* #111711 ^definition = Location of sampling site of specimen (nominal center) relative to the Position Frame of Reference.
* #111712 "Location of sampling site Z offset"
* #111712 ^definition = Location of sampling site of specimen (nominal center) relative to the Position Frame of Reference.
* #111718 "Location of Specimen"
* #111718 ^definition = Description of specimen location, either in absolute terms or relative to the Position Frame of Reference.
* #111719 "Location of Specimen X offset"
* #111719 ^definition = Location of specimen (nominal center) relative to the Position Frame of Reference in the X dimension.
* #111720 "Location of Specimen Y offset"
* #111720 ^definition = Location of specimen (nominal center) relative to the Position Frame of Reference in the Y dimension.
* #111721 "Location of Specimen Z offset"
* #111721 ^definition = Location of specimen (nominal center) relative to the Position Frame of Reference in the Z dimension.
* #111723 "Visual Marking of Specimen"
* #111723 ^definition = Description of visual distinguishing identifiers. E.g., ink, or a particular shape of the specimen.
* #111724 "Issuer of Specimen Identifier"
* #111724 ^definition = Assigning authority for specimen identifier.
* #111726 "Dissection with entire specimen submission"
* #111726 ^definition = Dissection of specimen with submission of all its sections for further processing or examination.
* #111727 "Dissection with representative sections submission"
* #111727 ^definition = Dissection of specimen with submission of representative sections for further processing or examination.
* #111729 "Specimen storage"
* #111729 ^definition = A workflow step, during which tissue specimens are stored in a climate-controlled environment.
* #111741 "Transmission illumination"
* #111741 ^definition = Transmission illumination method for specimen microscopy.
* #111742 "Reflection illumination"
* #111742 ^definition = Reflection illumination method for specimen microscopy.
* #111743 "Epifluorescence illumination"
* #111743 ^definition = Epifluorescence illumination method for specimen microscopy.
* #111744 "Brightfield illumination"
* #111744 ^definition = Brightfield illumination method for specimen microscopy.
* #111745 "Darkfield illumination"
* #111745 ^definition = Darkfield illumination method for specimen microscopy.
* #111746 "Oblique illumination"
* #111746 ^definition = Oblique illumination method for specimen microscopy.
* #111747 "Phase contrast illumination"
* #111747 ^definition = Phase contrast illumination method for specimen microscopy.
* #111748 "Differential interference contrast"
* #111748 ^definition = Differential interference contrast method for specimen microscopy.
* #111749 "Total internal reflection fluorescence"
* #111749 ^definition = Total internal reflection fluorescence method for specimen microscopy.
* #111750 "Ultrasound Contact"
* #111750 ^definition = A method of obtaining ophthalmic axial measurements that uses ultrasound, and that requires applanation of the cornea.
* #111751 "Ultrasound Immersion"
* #111751 ^definition = A method of obtaining ophthalmic axial measurements that uses ultrasound, and that requires immersion of the patient's eye in fluid as he lies in a supine position.
* #111752 "Optical"
* #111752 ^definition = A method of obtaining ophthalmic axial measurements that uses light.
* #111753 "Manual Keratometry"
* #111753 ^definition = Measurements taken of the corneal curvature using a manual keratometer.
* #111754 "Auto Keratometry"
* #111754 ^definition = Measurements taken of the corneal curvature using an automated keratometer.
* #111755 "Simulated Keratometry"
* #111755 ^definition = Simulated Keratometry measurements derived from corneal topography.
* #111756 "Equivalent K-reading"
* #111756 ^definition = Corneal power measurements using Scheimpflug camera.
* #111760 "Haigis"
* #111760 ^definition = The Haigis IOL calculation formula.
* #111761 "Haigis-L"
* #111761 ^definition = The Haigis-L IOL calculation formula.
* #111762 "Holladay 1"
* #111762 ^definition = The Holladay 1 IOL calculation formula.
* #111763 "Holladay 2"
* #111763 ^definition = The Holladay 2 IOL calculation formula.
* #111764 "Hoffer Q"
* #111764 ^definition = The Hoffer Q IOL calculation formula. Hoffer KJ. The Hoffer Q formula: a comparison of theoretic and regression formulas. J Cataract Refract Surg 1993;19:700-12. Errata. J Cataract Refract Surg 1994;20:677 and 2007;33:2-3.
* #111765 "Olsen"
* #111765 ^definition = The Olsen IOL calculation formula. Olsen T. Calculation of intraocular lens power: a review. Acta Ophthalmol. Scand. 2007: 85: 472-485.
* #111766 "SRKII"
* #111766 ^definition = The SRKII IOL calculation formula. Sanders DR, Retzlaff J, Kraff MC. Comparison of the SRK II formula and other second generation formulas. J Cataract Refract Surg. 1988 Mar;14(2):136-41.
* #111767 "SRK-T"
* #111767 ^definition = The SRK-T IOL calculation formula.
* #111768 "ACD Constant"
* #111768 ^definition = The "ACD Constant" used in IOL calculation.
* #111769 "Haigis a0"
* #111769 ^definition = The "Haigis a0" constant used in IOL calculation.
* #111770 "Haigis a1"
* #111770 ^definition = The "Haigis a1" constant used in IOL calculation.
* #111771 "Haigis a2"
* #111771 ^definition = The "Haigis a2" constant used in IOL calculation.
* #111772 "Hoffer pACD Constant"
* #111772 ^definition = The "Hoffer pACD Constant" used in IOL calculation.
* #111773 "Surgeon Factor"
* #111773 ^definition = The "Surgeon Factor" constant used in IOL calculation.
* #111776 "Front Of Cornea To Front Of Lens"
* #111776 ^definition = Anterior chamber depth defined as the front of the cornea to the front of the lens.
* #111777 "Back Of Cornea To Front Of Lens"
* #111777 ^definition = Anterior chamber depth defined as the back of the cornea to the front of the lens.
* #111778 "Single or Anterior Lens"
* #111778 ^definition = Refers to the anterior lens when there are two lenses in the eye. The distance, in mm, from the anterior surface of the lens to the posterior surface of the lens.
* #111779 "Posterior Lens"
* #111779 ^definition = Refers to the posterior lens when there are two lenses in the eye. The distance, in mm, from the anterior surface of the lens to the posterior surface of the lens.
* #111780 "Measurement From This Device"
* #111780 ^definition = Value obtained from measurements taken by the device creating this SOP Instance.
* #111781 "External Data Source"
* #111781 ^definition = Value obtained by data transfer from an external source - not from measurements taken by the device providing the value.
* #111782 "Axial Measurements SOP Instance"
* #111782 ^definition = Axial Measurements DICOM SOP Instance.
* #111783 "Refractive Measurements SOP Instance"
* #111783 ^definition = Refractive Measurements DICOM SOP Instance.
* #111786 "Standard Deviation of measurements used"
* #111786 ^definition = Standard Deviation is a simple measure of the variability of a data set.
* #111787 "Signal to Noise Ratio"
* #111787 ^definition = Signal to Noise Ratio of the data samples taken to create a measurement.
* #111791 "Spherical projection"
* #111791 ^definition = Projection from 2D image pixels to 3D Cartesian coordinates based on a spherical mathematical model.
* #111792 "Surface contour mapping"
* #111792 ^definition = Mapping from 2D image pixels to 3D Cartesian coordinates based on measurements of the retinal surface. E.g., of the retina, derived via a measurement technology such as Optical Coherence Tomography, Ultrasound etc.
* #111800 "Visual Field 24-2 Test Pattern"
* #111800 ^definition = Test pattern, nominally covering an area within 24° of fixation. Consists of 54 test points a minimum of 3° from each meridian and placed 6° apart.
* #111801 "Visual Field 10-2 Test Pattern"
* #111801 ^definition = Test pattern, nominally covering an area within 10° of fixation. Consists of 68 test points a minimum of 1° from each meridian and placed 2° apart.
* #111802 "Visual Field 30-2 Test Pattern"
* #111802 ^definition = Test pattern consisting of test point locations within 30° of fixation. Consists of 76 test points a minimum of 3° from each meridian and placed 6° apart.
* #111803 "Visual Field 60-4 Test Pattern"
* #111803 ^definition = Test pattern consisting of 60 test point locations between 30° and 60° of fixation a minimum of 6° from each meridian and placed 12° apart.
* #111804 "Visual Field Macula Test Pattern"
* #111804 ^definition = Test pattern consisting of 16 test point locations within 10° of fixation a minimum of 1° from each meridian and placed 2° apart.
* #111805 "Visual Field Central 40 Point Test Pattern"
* #111805 ^definition = Test pattern consisting of 40 test point locations within 30° of fixation that spread out radially from fixation.
* #111806 "Visual Field Central 76 Point Test Pattern"
* #111806 ^definition = Test pattern consisting of 76 test point locations within 30° of fixation a minimum of 3° from each meridian and placed 6° apart.
* #111807 "Visual Field Peripheral 60 Point Test Pattern"
* #111807 ^definition = Test pattern consisting of 60 test point locations between 30° and 60° of fixation a minimum of 6° from each meridian and placed 12° apart.
* #111808 "Visual Field Full Field 81 Point Test Pattern"
* #111808 ^definition = Test pattern consisting of 81 test point locations within 60° of fixation that spread out radially from fixation.
* #111809 "Visual Field Full Field 120 Point Test Pattern"
* #111809 ^definition = Test pattern consisting of 120 test point locations within 60° of fixation that spread out radially from fixation, concentrated in the nasal hemisphere.
* #111810 "Visual Field G Test Pattern"
* #111810 ^definition = Test pattern for Glaucoma and general visual field assessment with 59 test locations of which 16 test locations are in the macular area (up to 10° eccentricity) and where the density of test location is reduced with eccentricity. The test can be extended with the inclusion of 14 test locations between 30° and 60° eccentricity, 6 of which are located at the nasal step.
* #111811 "Visual Field M Test Pattern"
* #111811 ^definition = Test pattern for the macular area. Orthogonal test pattern with 0.7° spacing within the central 4° of eccentricity and reduced density of test locations between 4 and 10, 5° of eccentricity. 81 test locations over all. The test can be extended to include the test locations of the Visual Field G Test Pattern between 10, 5° and 60°.
* #111812 "Visual Field 07 Test Pattern"
* #111812 ^definition = Full field test pattern with 48 test locations from 0-30° and 82 test locations from 30-70°. Reduced test point density with increased eccentricity. Can be combined with screening and threshold strategies.
* #111813 "Visual Field LVC Test Pattern"
* #111813 ^definition = Low Vision Central. Orthogonal off-center test pattern with 6° spacing. 75 test locations within the central 30°. Corresponds with the 32/30-2 excluding the 2 locations at the blind spot, including a macular test location. The LVC is linked with a staircase threshold strategy starting at 0 dB intensity and applies stimulus area V.
* #111814 "Visual Field Central Test Pattern"
* #111814 ^definition = General test corresponding to the 30-2 but excluding the 2 test locations in the blind spot area, hence with 74 instead of 76 test locations.
* #111815 "Visual Field SITA-Standard Test Strategy"
* #111815 ^definition = Swedish Interactive Thresholding Algorithm (SITA). Strategy gains testing efficiency through use of visual field and information theory models.
* #111816 "Visual Field SITA-SWAP Test Strategy"
* #111816 ^definition = Adaptation of SITA testing methods to Blue-Yellow testing.
* #111817 "Visual Field SITA-Fast Test Strategy"
* #111817 ^definition = Similar to SITA-Standard but with less strict criteria for closing test points. Intended for patients who must be tested in the shortest possible time.
* #111818 "Visual Field Full Threshold Test Strategy"
* #111818 ^definition = Threshold test algorithm that determines a patient's sensitivity at each test point in the threshold test pattern by adjusting intensity by 4 dB steps until the patient changes their response, and then adjusts the intensity in the opposite direction by 2 dB steps until the patient changes their response again. The last stimulus seen by the patient is recognized as the threshold for that point.
* #111819 "Visual Field FastPac Test Strategy"
* #111819 ^definition = Similar to the Full Threshold algorithm except that it steps by 3 dB and only crosses the threshold only once.
* #111820 "Visual Field Full From Prior Test Strategy"
* #111820 ^definition = Identical to Full Threshold except that starting values are determined by the results of a previous test performed using the same test pattern and the Full Threshold test strategy.
* #111821 "Visual Field Optima Test Strategy"
* #111821 ^definition = Similar to FastPac except that the steps are pseudo-dynamic (differ based on the intensity of the last presentation).
* #111822 "Visual Field Two-Zone Test Strategy"
* #111822 ^definition = Suprathreshold testing strategy, in which each point is initially tested using stimulus that is 6 dB brighter than the expected hill of vision. If the patient does not respond, the stimulus is presented a second time at the same brightness. If the patient sees either presentation, the point is marked as "seen"; otherwise it is marked as "not seen".
* #111823 "Visual Field Three-Zone Test Strategy"
* #111823 ^definition = An extension of the two-zone strategy in which test points where the second stimulus is not seen are presented with a third stimulus at maximum brightness.
* #111824 "Visual Field Quantify-Defects Test Strategy"
* #111824 ^definition = An extension of the two-zone strategy, in which test points where the second stimulus is not seen receive threshold testing to quantify the depth of any detected scotomas.
* #111825 "Visual Field TOP Test Strategy"
* #111825 ^definition = Tendency Oriented Perimetry. Fast thresholding algorithm. Test strategy makes use of the interaction between neighboring test locations to reduce the test time compared to normal full threshold strategy by 60-80%.
* #111826 "Visual Field Dynamic Test Strategy"
* #111826 ^definition = Dynamic strategy is a fast thresholding strategy reducing test duration by adapting the dB step sizes according to the frequency-of-seeing curve of the threshold. Reduction of test time compared to normal full threshold strategy 30-50%.
* #111827 "Visual Field Normal Test Strategy"
* #111827 ^definition = Traditional full threshold staircase strategy. Initial intensities are presented, based on anchor point sensitivities in each quadrant and based on already known neighboring sensitivities. In a first run, thresholds are changed in 4dB steps until the first response reversal. Then the threshold is changed in 2 dB steps until the second response reversal. The threshold is calculated as the average between the last seen and last not-seen stimulus, supposed to correspond with the 50% point in the frequency-of-seeing curve.
* #111828 "Visual Field 1-LT Test Strategy"
* #111828 ^definition = One level screening test: Each test location is tested with a single intensity. The result is shown as seen or not-seen. The intensity can either be a 0 dB stimulus or a predefined intensity.
* #111829 "Visual Field 2-LT Test Strategy"
* #111829 ^definition = Two level screening test: Each test location is initially tested 6 dB brighter than the age corrected normal value.
* #111830 "Visual Field LVS Test Strategy"
* #111830 ^definition = Low Vision Strategy is a full threshold normal strategy with the exception that it starts at 0 dB intensity and applies stimulus area V.
* #111831 "Visual Field GATE Test Strategy"
* #111831 ^definition = German Adaptive Threshold Estimation is a fast strategy based on a modified 4-2 staircase algorithm, using prior visual fields to calculate the starting intensity. In: Chiefer U, Pascual JP, Edmunds B, Feudner E, Hoffmann EM, Johnson CA, Lagreze WA, Pfeiffer N, Sample PA, Staubach F, Weleber RG, Vonthein R, Krapp E, Paetzold J. Comparison of the new perimetric GATE strategy with conventional full-threshold and SITA standard strategies. Investigative Ophthalmology and Visual Science, 2009, 51: 488-494.
* #111832 "Visual Field GATEi Test Strategy"
* #111832 ^definition = Similar to GATE. The i stands for initial. If there was no prior visual field test to calculate the starting values, an anchor point method is used to define the local start values.
* #111833 "Visual Field 2LT-Dynamic Test Strategy"
* #111833 ^definition = A test started as two level screening test. In the course of the test, the threshold of relative defects and/or normal test locations has been quantified using the dynamic threshold strategy.
* #111834 "Visual Field 2LT-Normal Test Strategy"
* #111834 ^definition = A test started as two level screening test. In the course of the test, the threshold of relative defects and/or normal test locations has been quantified using the normal full threshold strategy.
* #111835 "Visual Field Fast Threshold Test Strategy"
* #111835 ^definition = Takes neighborhood test point results into account and offers stimuli with an adapted value to save time.
* #111836 "Visual Field CLIP Test Strategy"
* #111836 ^definition = Continuous Luminance Incremental Perimetry, which measures at first the individual reaction time of the patient and threshold values in every quadrant. The starting value for the main test is slightly below in individual threshold.
* #111837 "Visual Field CLASS Strategy"
* #111837 ^definition = A supra threshold screening strategy. The starting stimuli intensities depend on the classification of the patient's visual hill by measuring the central (fovea) or peripheral (15° meridian) threshold. The result of each dot slightly underestimates the sensitivity value (within 5 dB).
* #111838 "Age corrected"
* #111838 ^definition = Mode for determining the starting luminance for screening test points - the starting luminance s is chosen based on the age of the patient.
* #111839 "Threshold related"
* #111839 ^definition = Mode for determining the starting luminance for screening test points - the starting luminance is chosen based on the results of thresholding a set of "primary" test points (one in each quadrant).
* #111840 "Single luminance"
* #111840 ^definition = Mode for determining the starting luminance for screening test points - in this case, all starting luminance is set to the same value.
* #111841 "Foveal sensitivity related"
* #111841 ^definition = Mode for determining the starting luminance for screening test points - the starting luminance is chosen based on the result of the foveal threshold value.
* #111842 "Related to non macular sensitivity"
* #111842 ^definition = Mode for determining the starting luminance for screening test points - the starting luminance is chosen based on the result of four threshold values measured near the 15° meridian (one in each quadrant).
* #111843 "Automated Optical"
* #111843 ^definition = Real time evaluation of the camera image to recognize blinks and fixation losses with influence on the test procedure. Blinks that interfere with stimuli presentation cause the automated repetition of such stimulus presentations. Fixation losses can be used to delay the stimulus presentation until correct fixation is regained.
* #111844 "Blind Spot Monitoring"
* #111844 ^definition = A method of monitoring the patient's fixation by periodically presenting stimulus in a location on the background surface that corresponds to the patient's blind spot.
* #111845 "Macular Fixation Testing"
* #111845 ^definition = A method of monitoring the patient's fixation by presenting the stimulus to the patient's macula.
* #111846 "Observation by Examiner"
* #111846 ^definition = A method of monitoring the patient's fixation by observation from the examiner of the patient.
* #111847 "Outside normal limits"
* #111847 ^definition = Analysis Results are outside normal limits.
* #111848 "Borderline"
* #111848 ^definition = Analysis Results are borderline.
* #111849 "Abnormally high sensitivity"
* #111849 ^definition = Analysis Results identify abnormally high sensitivity.
* #111850 "General reduction in sensitivity"
* #111850 ^definition = Analysis Results identify general reduction in sensitivity.
* #111851 "Borderline and general reduction in sensitivity"
* #111851 ^definition = Analysis Results identify Borderline and general reduction in sensitivity.
* #111852 "Visual Field Index"
* #111852 ^definition = Index of a patient's remaining visual field normalized for both age and generalized defect.
* #111853 "Visual Field Loss Due to Diffuse Defect"
* #111853 ^definition = Estimate of the portion of a patient's visual field loss that is diffuse (i.e., spread evenly across all portions of the visual field).
* #111854 "Visual Field Loss Due to Local Defect"
* #111854 ^definition = Estimate of the portion of a patient's visual field loss that is local (i.e., not spread evenly across all portions of the visual field).
* #111855 "Glaucoma Hemifield Test Analysis"
* #111855 ^definition = An analysis of asymmetry between zones of the superior and inferior visual field. It is designed to be specific for defects due to glaucoma.
* #111856 "Optical Fixation Measurements"
* #111856 ^definition = The data output of an optical fixation monitoring process, consisting of a list of positive and negative numbers indicating the quality of patient fixation over the course of a visual field test. The value 0 represents the initial fixation. Negative numbers indicate a measuring error (i.e., the patient blinked). Positive numbers quantify the degree of eccentricity from initial fixation.
* #111900 "Macula centered"
* #111900 ^definition = An image of at least 15° angular subtend that is centered on the macula; see Section U.1.8 Relative Image Position Definitions in PS3.17 .
* #111901 "Disc centered"
* #111901 ^definition = An image of at least 15° angular subtend that is centered on the optic disc; see Section U.1.8 Relative Image Position Definitions in PS3.17 .
* #111902 "Lesion centered"
* #111902 ^definition = An image of any angular subtend that is centered on a lesion located in any region of the fundus; see Section U.1.8 Relative Image Position Definitions in PS3.17 .
* #111903 "Disc-macula centered"
* #111903 ^definition = An image of at least 15° angular subtend centered midway between the disc and macula and containing at least a portion of the disc and both the disc and the macula; see Section U.1.8 Relative Image Position Definitions in PS3.17 .
* #111904 "Mid-peripheral-superior"
* #111904 ^definition = An image of at least 15° angular subtend positioned between the central zone and the equator, and spanning both the superior-temporal and superior-nasal quadrants of the fundus; see Section U.1.8 Relative Image Position Definitions in PS3.17 .
* #111905 "Mid-peripheral-superior temporal"
* #111905 ^definition = An image of at least 15° angular subtend positioned between the central zone and the equator in the superior-temporal quadrant of the fundus; see Section U.1.8 Relative Image Position Definitions in PS3.17 .
* #111906 "Mid-peripheral-temporal"
* #111906 ^definition = An image of at least 15° angular subtend positioned between the central zone and the equator, and spanning both the superior-temporal and inferior-temporal quadrants of the fundus; see Section U.1.8 Relative Image Position Definitions in PS3.17 .
* #111907 "Mid-peripheral-inferior temporal"
* #111907 ^definition = An image of at least 15° angular subtend positioned between the central zone and the equator in the inferior-temporal quadrant of the fundus; see Section U.1.8 Relative Image Position Definitions in PS3.17 .
* #111908 "Mid-peripheral-inferior"
* #111908 ^definition = An image of at least 15° angular subtend positioned between the central zone and the equator, and spanning both the inferior-temporal and inferior-nasal quadrants of the fundus; see Section U.1.8 Relative Image Position Definitions in PS3.17 .
* #111909 "Mid-peripheral-inferior nasal"
* #111909 ^definition = An image of at least 15° angular subtend positioned between the central zone and the equator in the inferior-nasal quadrant of the fundus; see Section U.1.8 Relative Image Position Definitions in PS3.17 .
* #111910 "Mid-peripheral-nasal"
* #111910 ^definition = An image of at least 15° angular subtend positioned between the central zone and the equator, and spanning both the superior-nasal and inferior-nasal quadrants of the fundus; see Section U.1.8 Relative Image Position Definitions in PS3.17 .
* #111911 "Mid-peripheral-superior nasal"
* #111911 ^definition = An image of at least 15° angular subtend positioned between the central zone and the equator in the superior-nasal quadrant of the fundus; see Section U.1.8 Relative Image Position Definitions in PS3.17 .
* #111912 "Peripheral-superior"
* #111912 ^definition = An image of at least 15° angular subtend positioned between the equator and the ora serrata, and spanning both the superior temporal and superior nasal quadrants of the fundus; see Section U.1.8 Relative Image Position Definitions in PS3.17 .
* #111913 "Peripheral-superior temporal"
* #111913 ^definition = An image of at least 15° angular subtend positioned between the equator and ora serrata in the superior-temporal quadrant of the fundus; see Section U.1.8 Relative Image Position Definitions in PS3.17 .
* #111914 "Peripheral-temporal"
* #111914 ^definition = An image of at least 15° angular subtend positioned between the equator and ora serrata, and spanning both the superior-temporal and inferior-temporal quadrants of the fundus; see Section U.1.8 Relative Image Position Definitions in PS3.17 .
* #111915 "Peripheral-inferior temporal"
* #111915 ^definition = An image of at least 15° angular subtend positioned between the equator and ora serrata in the inferior-temporal quadrant of the fundus; see Section U.1.8 Relative Image Position Definitions in PS3.17 .
* #111916 "Peripheral-inferior"
* #111916 ^definition = An image of at least 15° angular subtend positioned between the equator and ora serrata, and spanning both the inferior-temporal and inferior-nasal quadrants of the fundus; see Section U.1.8 Relative Image Position Definitions in PS3.17 .
* #111917 "Peripheral-inferior nasal"
* #111917 ^definition = An image of at least 15° angular subtend positioned between the equator and ora serrata in the inferior-nasal quadrant of the fundus; see Section U.1.8 Relative Image Position Definitions in PS3.17 .
* #111918 "Peripheral-nasal"
* #111918 ^definition = An image of at least 15° angular subtend positioned between the equator and ora serrata, and spanning both the superior-nasal and inferior-nasal quadrants of the fundus; see Section U.1.8 Relative Image Position Definitions in PS3.17 .
* #111919 "Peripheral-superior nasal"
* #111919 ^definition = An image of at least 15° angular subtend positioned between the equator and ora serrata in the superior-nasal quadrant of the fundus; see Section U.1.8 Relative Image Position Definitions in PS3.17 .
* #111920 "Time domain"
* #111920 ^definition = Identifies the use of physical signals with respect to time to capture information.
* #111921 "Spectral domain"
* #111921 ^definition = Identifies the use of physical signals with respect to multiple frequencies to capture information.
* #111922 "No corneal compensation"
* #111922 ^definition = No compensation algorithm for corneal birefringence.
* #111923 "Corneal birefringence compensation"
* #111923 ^definition = Algorithm to compensate for variability in corneal birefringence.
* #111924 "Retinal topography"
* #111924 ^definition = Measurement of the retinal surface contour relative to an assigned datum plane.
* #111925 "Retinal nerve fiber layer thickness"
* #111925 ^definition = Measurement approximating the distance related to the structure between the internal limiting membrane (ILM) and the outer boarder of the retinal nerve fiber layer (RNFL); see Section III.6 Retinal Thickness Definition in PS3.17 .
* #111926 "Ganglion cell complex thickness"
* #111926 ^definition = Measurement approximating the distance related to the structure between the ILM and the outer border of the inner plexiform layer (IPL), called the ganglion cell complex (GCC); see Section III.6 Retinal Thickness Definition in PS3.17 .
* #111927 "Total retinal thickness (ILM to IS-OS)"
* #111927 ^definition = Measurement approximating the distance related to the structure between the ILM and the inner-outer segment junction (IS-OS); see Section III.6 Retinal Thickness Definition in PS3.17 .
* #111928 "Total retinal thickness (ILM to RPE)"
* #111928 ^definition = Measurement approximating the distance related to the structure between the ILM and the retinal pigment epithelium (RPE); see Section III.6 Retinal Thickness Definition in PS3.17 .
* #111929 "Total retinal thickness (ILM to BM)"
* #111929 ^definition = Measurement approximating the distance related to the structure between the ILM and the Bruch's membrane (BM); see Section III.6 Retinal Thickness Definition in PS3.17 .
* #111930 "Absolute ophthalmic thickness"
* #111930 ^definition = Thickness of a component of the posterior segment of the eye. E.g., thickness of retina, choroid, etc.
* #111931 "Thickness deviation category from normative data"
* #111931 ^definition = Ophthalmic Thickness map based upon statistical significance category (such as percentile) from a normative data set.
* #111932 "Thickness deviation from normative data"
* #111932 ^definition = Ophthalmic Thickness map based upon deviation (such as microns) from a normative data set.
* #111933 "Related ophthalmic thickness map"
* #111933 ^definition = Ophthalmic Thickness Map related to another Ophthalmic Thickness Map or another SOP Instance.
* #111934 "Disc-Fovea"
* #111934 ^definition = An anatomic point centered midway between the disc and fovea centralis.
* #111935 "p>5%"
* #111935 ^definition = Assuming the null hypothesis is true, the conditional percent probability of observing this result is not statistically significant.
* #111936 "p<5%"
* #111936 ^definition = Assuming the null hypothesis is true, the conditional percent probability of observing this result is statistically significant, 95% unlikely to happen by chance.
* #111937 "p<2%"
* #111937 ^definition = Assuming the null hypothesis is true, the conditional percent probability of observing this result is statistically significant, 98% unlikely to happen by chance.
* #111938 "p<1%"
* #111938 ^definition = Assuming the null hypothesis is true, the conditional percent probability of observing this result is statistically significant, 99% unlikely to happen by chance.
* #111939 "p<0.5%"
* #111939 ^definition = Assuming the null hypothesis is true, the conditional percent probability of observing this result is statistically significant, 99.5% unlikely to happen by chance.
* #111940 "Corneal axial power map"
* #111940 ^definition = A two dimensional representation of the axial curvature of the cornea. Axial curvature is calculated from the reciprocal of the distance from a point on a meridian normal at the point to the corneal topographer axis. Also known as sagittal curvature.
* #111941 "Corneal instantaneous power map"
* #111941 ^definition = A two dimensional representation of the instantaneous curvature of the cornea. Instantaneous curvature is calculated from the reciprocal of the distance from a point on a meridian normal at the point to the center of curvature of that point. Also called tangential curvature.
* #111942 "Corneal refractive power map"
* #111942 ^definition = A two dimensional representation of the refractive power of the cornea. Corneal refractive power is calculated using Snell's Law.
* #111943 "Corneal elevation map"
* #111943 ^definition = A two dimensional representation of the elevation of the cornea. Elevation is calculated as the distance from a point on the corneal surface to a point on a reference surface along a line parallel to the corneal topographer axis. For the purpose of visualization the reference surface is usually a sphere or an ellipse.
* #111944 "Corneal wavefront map"
* #111944 ^definition = A two dimensional representation of a wavefront aberration surface of the cornea. Wavefront aberration surface is calculated from the corneal elevation data fit with either the Zernike polynomial series or the Fourier Series. Maps generally display total aberrations and selectable higher order aberrations.
* #111945 "Elevation-based corneal tomographer"
* #111945 ^definition = A device that measures corneal anterior surface shape using elevation-based methods (stereographic and light slit-based). Rasterstereography images a grid pattern illuminating the fluorescein dyed tear film with 2 cameras to produce 3D. Slit-based devices scan the cornea, usually by rotation about the instrument axis centered on the cornea vertex.
* #111946 "Reflection-based corneal topographer"
* #111946 ^definition = A reflection-based device that projects a pattern of light onto the cornea and an image of the reflection of that pattern from the tear film is recorded in one video frame. Light patterns include the circular mire pattern (Placido disc) and spot matrix patterns. Sequential scanning of light spots reflected from the corneal surface is also used requiring multiple video frames for recording.
* #111947 "Interferometry-based corneal tomographer"
* #111947 ^definition = An Interference-based device that projects a beam of light onto and through the cornea. Light reflected from within the cornea is combined with a reference beam giving rise to an interference pattern. Appropriately scanned, this imaging is used to construct 3-dimensional images of the cornea from anterior to posterior surfaces. E.g., swept source OCT.
* #112000 "Chest CAD Report"
* #112000 ^definition = A structured report containing the results of computer-aided detection or diagnosis applied to chest imaging and associated clinical information.
* #112001 "Opacity"
* #112001 ^definition = The shadow of an absorber that attenuates the X-Ray beam more effectively than do surrounding absorbers. In a radiograph, any circumscribed area that appears more nearly white (of lesser photometric density) than its surround [Fraser and Pare].
* #112002 "Series Instance UID"
* #112002 ^definition = A unique identifier for a series of DICOM SOP instances.
* #112003 "Associated Chest Component"
* #112003 ^definition = A named anatomic region within the chest cavity.
* #112004 "Abnormal interstitial pattern"
* #112004 ^definition = A collection of opacities detected within the continuum of loose connective tissue throughout the lung, that is not expected in a diagnostically normal radiograph.
* #112005 "Radiographic anatomy"
* #112005 ^definition = A type of anatomy that is expected to be detectable on a radiographic (X-Ray based) image.
* #112006 "Distribution Descriptor"
* #112006 ^definition = Characteristic of the extent of spreading of a finding or feature.
* #112007 "Border definition"
* #112007 ^definition = Characteristic of the clarity of the boundary or edges of a finding or feature.
* #112008 "Site involvement"
* #112008 ^definition = The part(s) of the anatomy affected or encompassed by a finding or feature.
* #112009 "Type of Content"
* #112009 ^definition = Characteristic of the matter or substance within a finding or feature.
* #112010 "Texture Descriptor"
* #112010 ^definition = Characteristic of the surface or consistency of a finding or feature.
* #112011 "Positioner Primary Angle"
* #112011 ^definition = Position of the X-Ray beam about the patient from the RAO to LAO direction where movement from RAO to vertical is positive.
* #112012 "Positioner Secondary Angle"
* #112012 ^definition = Position of the X-Ray beam about the patient from the caudal to cranial direction where movement from caudal to vertical is positive.
* #112013 "Location in Chest"
* #112013 ^definition = The zone, lobe or segment within the chest cavity in which a finding or feature is situated.
* #112014 "Orientation Descriptor"
* #112014 ^definition = Vertical refers to orientation parallel to the superior-inferior (cephalad-caudad) axis of the body, with horizontal being perpendicular to this, and an oblique orientation having projections in both the horizontal and vertical.
* #112015 "Border shape"
* #112015 ^definition = Characteristic of the shape formed by the boundary or edges of a finding or feature.
* #112016 "Baseline Category"
* #112016 ^definition = Indicates whether a finding was considered a target lesion, non-target lesion, or non-lesion during evaluation of a baseline series, according to a method such as RECIST.
* #112017 "Cavity extent as percent of volume"
* #112017 ^definition = The extent of a detected cavity, represented as the percent of the surrounding volume that it occupies.
* #112018 "Calcification extent as percent of surface"
* #112018 ^definition = The extent of a detected calcification, represented as the percent of the surrounding surface that it occupies.
* #112019 "Calcification extent as percent of volume"
* #112019 ^definition = The extent of a detected calcification, represented as the percent of the surrounding volume that it occupies.
* #112020 "Response Evaluation"
* #112020 ^definition = A heading for the reporting of response evaluation for treatment of solid tumors.
* #112021 "Response Evaluation Method"
* #112021 ^definition = The system applied in the reporting of response evaluation for treatment of solid tumors.
* #112022 "RECIST"
* #112022 ^definition = Response Evaluation Criteria In Solid Tumors; see Normative References.
* #112023 "Composite Feature Modifier"
* #112023 ^definition = A term that further specifies the name of an item that is an inferred correlation relating two or more individual findings or features.
* #112024 "Single Image Finding Modifier"
* #112024 ^definition = A term that further specifies the name of an item that was detected on one image.
* #112025 "Size Descriptor"
* #112025 ^definition = A qualitative descriptor for the extent of a finding or feature.
* #112026 "Width Descriptor"
* #112026 ^definition = A qualitative descriptor for the thickness of tubular structures, such as blood vessels.
* #112027 "Opacity Descriptor"
* #112027 ^definition = A characteristic that further describes the nature of an opacity.
* #112028 "Abnormal Distribution of Anatomic Structure"
* #112028 ^definition = The type of adverse affect that a finding or feature is having on the surrounding anatomy.
* #112029 "WHO"
* #112029 ^definition = Response evaluation method as defined in chapter 5, "Reporting of Response" of the WHO Handbook for Reporting Results for Cancer Treatment; see Normative References.
* #112030 "Calcification Descriptor"
* #112030 ^definition = Identification of the morphology of detected calcifications.
* #112031 "Attenuation Coefficient"
* #112031 ^definition = A quantitative numerical statement of the relative attenuation of the X-Ray beam at a specified point. Coefficient that describes the fraction of a beam of X-Rays or gamma rays that is absorbed or scattered per unit thickness of the absorber. This value basically accounts for the number of atoms in a cubic cm volume of material and the probability of a photon being scattered or absorbed from the nucleus or an electron of one of these atoms. Usually expressed in Hounsfield units [referred to as CT Number in Fraser and Pare].
* #112032 "Threshold Attenuation Coefficient"
* #112032 ^definition = An X-Ray attenuation coefficient that is used as a threshold. E.g., in calcium scoring.
* #112033 "Abnormal opacity"
* #112033 ^definition = An opacity that is not expected in a diagnostically normal radiograph.
* #112034 "Calculation Description"
* #112034 ^definition = A textual description of the mathematical method of calculation that resulted in a calculated value.
* #112035 "Performance of Pediatric and Adult Chest Radiography, ACR"
* #112035 ^definition = American College of Radiology. ACR Standard for the Performance of Pediatric and Adult Chest Radiography. In: Standards. Reston, Va: 2001:95-98.
* #112036 "ACR Position Statement"
* #112036 ^definition = American College of Radiology. ACR Position Statement for Quality Control and Improvement, Safety, Infection Control, and Patient Concerns. In: Practice Guidelines and Technical Standards. Reston, Va: 2001:iv.
* #112037 "Non-lesion Modifier"
* #112037 ^definition = A descriptor for a non-lesion object finding or feature, used to indicate whether the object was detected as being internal or external to the patient's body.
* #112038 "Osseous Modifier"
* #112038 ^definition = A concept modifier for an Osseous Anatomy, or bone related, finding.
* #112039 "Tracking Identifier"
* #112039 ^definition = A text label used for tracking a finding or feature, potentially across multiple reporting objects, over time. This label shall be unique within the domain in which it is used.
* #112040 "Tracking Unique Identifier"
* #112040 ^definition = A unique identifier used for tracking a finding or feature, potentially across multiple reporting objects, over time.
* #112041 "Target Lesion Complete Response"
* #112041 ^definition = Disappearance of all target lesions.
* #112042 "Target Lesion Partial Response"
* #112042 ^definition = At least a 30% decrease in the sum of the Longest Diameter of target lesions, taking as reference the baseline sum Longest Diameter.
* #112043 "Target Lesion Progressive Disease"
* #112043 ^definition = At least a 20% increase in the sum of the Longest Diameter of target lesions, taking as reference the smallest sum Longest Diameter recorded since the treatment started, or the appearance of one or more new lesions.
* #112044 "Target Lesion Stable Disease"
* #112044 ^definition = Neither sufficient shrinkage to qualify for Partial Response nor sufficient increase to qualify for Progressive Disease, taking as reference the smallest sum Longest Diameter since the treatment started.
* #112045 "Non-Target Lesion Complete Response"
* #112045 ^definition = Disappearance of all non-target lesions and normalization of tumor marker level.
* #112046 "Non-Target Lesion Incomplete Response or Stable Disease"
* #112046 ^definition = Persistence of one or more non-target lesions and/or maintenance of tumor marker level above the normal limits.
* #112047 "Non-Target Lesion Progressive Disease"
* #112047 ^definition = Appearance of one or more new lesions and/or unequivocal progression of existing non-target lesions.
* #112048 "Current Response"
* #112048 ^definition = The current response evaluation for treatment of solid tumors, according to a method such as RECIST.
* #112049 "Best Overall Response"
* #112049 ^definition = Best response recorded from the start of the treatment until disease progression/recurrence, taking as reference for Progressive Disease the smallest measurements recorded since the treatment started, according to a method such as RECIST.
* #112050 "Anatomic Identifier"
* #112050 ^definition = A text identifier of an anatomic feature when a multiplicity of features of that type may be present, such as "Rib 1", "Rib 2" or thoracic vertebrae "T1" or "T2".
* #112051 "Measurement of Response"
* #112051 ^definition = A measured or calculated evaluation of response. E.g., according to a method such as RECIST, the value would be the calculated sum of the lengths of the longest axes of a set of target lesions.
* #112052 "Bronchovascular"
* #112052 ^definition = Of or relating to a bronchial (lung) specific channel for the conveyance of a body fluid.
* #112053 "Osseous"
* #112053 ^definition = Of, relating to, or composed of bone.
* #112054 "Secondary pulmonary lobule"
* #112054 ^definition = The smallest unit of lung surrounded by connective tissue septa; the unit of lung subtended by any bronchiole that gives off three to five terminal bronchioles [Fraser and Pare].
* #112055 "Agatston scoring method"
* #112055 ^definition = A method of calculating an overall calcium score, reflecting the calcification of coronary arteries, based on the maximum X-Ray attenuation coefficient and the area of calcium deposits.
* #112056 "Volume scoring method"
* #112056 ^definition = A method of calculating an overall calcium score, reflecting the calcification of coronary arteries, based on the volume of each calcification, typically expressed in mm3.
* #112057 "Mass scoring method"
* #112057 ^definition = A method of calculating an overall calcium score, reflecting the calcification of coronary arteries, based on the total mass of calcification, typically expressed in mg.
* #112058 "Calcium score"
* #112058 ^definition = A measure often arrived at through calculation of findings from CT examination, which is a common predictor of significant stenosis of the coronary arteries.
* #112059 "Primary complex"
* #112059 ^definition = The combination of a focus of pneumonia due to a primary infection with granulomas in the draining hilar or mediastinal lymph nodes [Fraser and Pare].
* #112060 "Oligemia"
* #112060 ^definition = General or local decrease in the apparent width of visible pulmonary vessels, suggesting less than normal blood flow (reduced blood flow) [Fraser and Pare].
* #112061 "Abnormal lines (1D)"
* #112061 ^definition = Linear opacity of very fine width, i.e., a nearly one dimensional opacity.
* #112062 "Abnormal lucency"
* #112062 ^definition = Area of abnormal very low X-Ray attenuation, typically lower than aerated lung when occurring in or projecting over lung, or lower than soft tissue when occurring in or projecting over soft tissue.
* #112063 "Abnormal calcifications"
* #112063 ^definition = A calcific opacity within the lung that may be organized, but does not display the trabecular organization of true bone [Fraser and Pare].
* #112064 "Abnormal texture"
* #112064 ^definition = Relatively homogeneous, extended, pattern of abnormal opacity in the lung, typically low in contrast.
* #112065 "Reticulonodular pattern"
* #112065 ^definition = A collection of innumerable small, linear, and nodular opacities that together produce a composite appearance resembling a net with small superimposed nodules. The reticular and nodular elements are dimensionally of similar magnitude [Fraser and Pare].
* #112066 "Beaded septum sign"
* #112066 ^definition = Irregular septal thickening that suggests the appearance of a row of beads; usually a sign of lymphangitic carcinomatosis, but may also occur rarely in sarcoidosis [Fraser and Pare].
* #112067 "Nodular pattern"
* #112067 ^definition = A collection of innumerable, small discrete opacities ranging in diameter from 2-10 mm, generally uniform in size and widespread in distribution, and without marginal spiculation [Fraser and Pare].
* #112068 "Pseudoplaque"
* #112068 ^definition = An irregular band of peripheral pulmonary opacity adjacent to visceral pleura that simulates the appearance of a pleural plaque and is formed by coalescence of small nodules [Fraser and Pare] .
* #112069 "Signet-ring sign"
* #112069 ^definition = A ring of opacities (usually representing a dilated, thick-walled bronchus) in association with a smaller, round, soft tissue opacity (the adjacent pulmonary artery) suggesting a "signet ring" [Fraser and Pare].
* #112070 "Air bronchiologram"
* #112070 ^definition = Equivalent of air bronchogram, but in airways assumed to be bronchioles because of peripheral location and diameter [Fraser and Pare].
* #112071 "Air bronchogram"
* #112071 ^definition = Radiographic shadow of an air-containing bronchus; presumed to represent an air-containing segment of the bronchial tree (identity often inferred) [Fraser and Pare].
* #112072 "Air crescent"
* #112072 ^definition = Air in a crescentic shape in a nodule or mass, in which the air separates the outer wall of the lesion from an inner sequestrum, which most commonly is a fungus ball of Aspergillusspecies [Fraser and Pare].
* #112073 "Halo sign"
* #112073 ^definition = Ground-glass opacity surrounding the circumference of a nodule or mass. May be a sign of invasive aspergillosis or hemorrhage of various causes [Fraser and Pare].
* #112074 "Target Lesion at Baseline"
* #112074 ^definition = Flag denoting that this lesion was identified, at baseline, as a target lesion intended for tracking over time [RECIST].
* #112075 "Non-Target Lesion at Baseline"
* #112075 ^definition = Flag denoting that this lesion was not identified, at baseline, as a target lesion, and was not intended for tracking over time [RECIST].
* #112076 "Non-Lesion at Baseline"
* #112076 ^definition = Flag denoting that this finding was identified, at baseline, as a category other than a lesion, and was not intended for tracking over time [RECIST].
* #112077 "Vasoconstriction"
* #112077 ^definition = Local or general reduction in the caliber of visible pulmonary vessels, presumed to result from decreased flow occasioned by contraction of muscular pulmonary arteries [Fraser and Pare].
* #112078 "Vasodilation"
* #112078 ^definition = Local or general increase in the width of visible pulmonary vessels resulting from increased pulmonary blood flow [Fraser and Pare].
* #112079 "Architectural distortion"
* #112079 ^definition = A manifestation of lung disease in which bronchi, pulmonary vessels, a fissure or fissures, or septa of secondary pulmonary lobules are abnormally displaced [Fraser and Pare].
* #112080 "Mosaic perfusion"
* #112080 ^definition = A patchwork of regions of varied attenuation, interpreted as secondary to regional differences in perfusion [Fraser and Pare].
* #112081 "Pleonemia"
* #112081 ^definition = Increased blood flow to the lungs or a portion thereof, manifested by a general or local increase in the width of visible pulmonary vessels [Fraser and Pare].
* #112082 "Interface"
* #112082 ^definition = The common boundary between the shadows of two juxtaposed structures or tissues of different texture or opacity (edge, border) [Fraser and Pare].
* #112083 "Line"
* #112083 ^definition = A longitudinal opacity no greater than 2 mm in width [Fraser and Pare].
* #112084 "Lucency"
* #112084 ^definition = The shadow of an absorber that attenuates the primary X-Ray beam less effectively than do surrounding absorbers. In a radiograph, any circumscribed area that appears more nearly black (of greater photometric density) than its surround [Fraser and Pare].
* #112085 "Midlung window"
* #112085 ^definition = A midlung region, characterized by the absence of large blood vessels and by a paucity of small blood vessels, that corresponds to the minor fissure and adjacent peripheral lung [Fraser and Pare].
* #112086 "Carina angle"
* #112086 ^definition = The angle formed by the right and left main bronchi at the tracheal bifurcation [Fraser and Pare].
* #112087 "Centrilobular structures"
* #112087 ^definition = The pulmonary artery and its immediate branches in a secondary lobule; HRCT depicts these vessels in certain cases; a.k.a. core structures or lobular core structures [Fraser and Pare].
* #112088 "Anterior junction line"
* #112088 ^definition = A vertically oriented linear or curvilinear opacity approximately 1-2 mm wide, commonly projected on the tracheal air shadow [Fraser and Pare].
* #112089 "Posterior junction line"
* #112089 ^definition = A vertically oriented, linear or curvilinear opacity approximately 2 mm wide, commonly projected on the tracheal air shadow, and usually slightly concave to the right [Fraser and Pare].
* #112090 "Azygoesophageal recess interface"
* #112090 ^definition = A space in the right side of the mediastinum into which the medial edge of the right lower lobe extends [Fraser and Pare].
* #112091 "Paraspinal line"
* #112091 ^definition = A vertically oriented interface usually seen in a frontal chest radiograph to the left of the thoracic vertebral column [Fraser and Pare].
* #112092 "Posterior tracheal stripe"
* #112092 ^definition = A vertically oriented linear opacity ranging in width from 2-5 mm, extending from the thoracic inlet to the bifurcation of the trachea, and visible only on lateral radiographs of the chest [Fraser and Pare].
* #112093 "Right tracheal stripe"
* #112093 ^definition = A vertically oriented linear opacity approximately 2-3 mm wide extending from the thoracic inlet to the right tracheobronchial angle [Fraser and Pare].
* #112094 "Stripe"
* #112094 ^definition = A longitudinal composite opacity measuring 2-5 mm in width; acceptable when limited to anatomic structures within the mediastinum [Fraser and Pare].
* #112095 "Hiatus"
* #112095 ^definition = A gap or passage through an anatomical part or organ; especially : a gap through which another part or organ passes.
* #112096 "Rib Scalene Tubercle"
* #112096 ^definition = A small rounded elevation or eminence on the first rib for the attachment of the scalenus anterior.
* #112097 "Vertebral Intervertebral Notch"
* #112097 ^definition = A groove that serves for the transmission of the vertebral artery.
* #112098 "Subscapular Fossa"
* #112098 ^definition = The concave depression of the anterior surface of the scapula.
* #112099 "Scapular Spine"
* #112099 ^definition = A sloping ridge dividing the dorsal surface of the scapula into the supraspinatous fossa (above), and the infraspinatous fossa (below).
* #112100 "Scapular Supraspinatus Fossa"
* #112100 ^definition = The portion of the dorsal surface of the scapula above the scapular spine.
* #112101 "Scapular Infraspinatus Fossa"
* #112101 ^definition = The portion of the dorsal surface of the scapula below the scapular spine.
* #112102 "Aortic knob"
* #112102 ^definition = The portion of the aortic arch that defines the transition between its ascending and descending limbs.
* #112103 "Arch of the Azygos vein"
* #112103 ^definition = Section of Azygos vein near the fourth thoracic vertebra, where it arches forward over the root of the right lung, and ends in the superior vena cava, just before that vessel pierces the pericardium.
* #112104 "Air-fluid level"
* #112104 ^definition = A local collection of gas and liquid that, when traversed by a horizontal X-Ray beam, creates a shadow characterized by a sharp horizontal interface between gas density above and liquid density below [Fraser and Pare].
* #112105 "Corona radiata"
* #112105 ^definition = A circumferential pattern of fine linear spicules, approximately 5 mm long, extending outward from the margin of a solitary pulmonary nodule through a zone of relative lucency [Fraser and Pare].
* #112106 "Honeycomb pattern"
* #112106 ^definition = A number of closely approximated ring shadows representing air spaces 5-10 mm in diameter with walls 2-3 mm thick that resemble a true honeycomb; implies "end-stage" lung [Fraser and Pare].
* #112107 "Fleischner's line(s)"
* #112107 ^definition = A straight, curved, or irregular linear opacity that is visible in multiple projections; usually situated in the lower half of the lung; vary markedly in length and width [Fraser and Pare].
* #112108 "Intralobular lines"
* #112108 ^definition = Fine linear opacities present in a lobule when the intralobular interstitium is thickened. When numerous, they may appear as a fine reticular pattern [Fraser and Pare].
* #112109 "Kerley A line"
* #112109 ^definition = Essentially straight linear opacity 2-6 cm in length and 1-3 mm in width, usually in an upper lung zone [Fraser and Pare].
* #112110 "Kerley B line"
* #112110 ^definition = A straight linear opacity 1.5-2 cm in length and 1-2 mm in width, usually at the lung base [Fraser and Pare].
* #112111 "Kerley C lines"
* #112111 ^definition = A group of branching, linear opacities producing the appearing of a fine net, at the lung base [Fraser and Pare].
* #112112 "Parenchymal band"
* #112112 ^definition = Elongated opacity, usually several millimeters wide and up to about 5 cm long, often extending to the pleura, which may be thickened and retracted at the site of contact [Fraser and Pare].
* #112113 "Reticular pattern"
* #112113 ^definition = A collection of innumerable small linear opacities that together produce an appearance resembling a net [Fraser and Pare].
* #112114 "Septal line(s)"
* #112114 ^definition = Usually used in the plural, a generic term for linear opacities of varied distribution produced when the interstitium between pulmonary lobules is thickened [Fraser and Pare].
* #112115 "Subpleural line"
* #112115 ^definition = A thin curvilinear opacity, a few millimeters or less in thickness, usually less than 1 cm from the pleural surface and paralleling the pleura [Fraser and Pare].
* #112116 "Tramline shadow"
* #112116 ^definition = Parallel or slightly convergent linear opacities that suggest the planar projection of tubular structures and that correspond in location and orientation to elements of the bronchial tree [Fraser and Pare].
* #112117 "Tubular shadow"
* #112117 ^definition = Paired, parallel, or slightly convergent linear opacities presumed to represent the walls of a tubular structure seen en face; used if the anatomic nature of a shadow is obscure [Fraser and Pare].
* #112118 "Density"
* #112118 ^definition = The opacity of a radiographic shadow to visible light; film blackening; the term should never be used to mean an "opacity" or "radiopacity" [Fraser and Pare].
* #112119 "Dependent opacity"
* #112119 ^definition = Subpleural increased attenuation in dependent lung. The increased attenuation disappears when the region of lung is nondependent; a.k.a. dependent increased attenuation [Fraser and Pare].
* #112120 "Ground glass opacity"
* #112120 ^definition = Hazy increased attenuation of lung, but with preservation of bronchial and vascular margins; caused by partial filling of air spaces, interstitial thickening, partial collapse of alveoli, normal expiration, or increased capillary blood volume [Fraser and Pare].
* #112121 "Infiltrate"
* #112121 ^definition = Any ill-defined opacity in the lung [Fraser and Pare].
* #112122 "Micronodule"
* #112122 ^definition = Discrete, small, round, focal opacity of at least soft tissue attenuation and with a diameter no greater than 7 mm [Fraser and Pare].
* #112123 "Phantom tumor (pseudotumor)"
* #112123 ^definition = A shadow produced by a local collection of fluid in one of the interlobar fissures, usually elliptic in one radiographic projection and rounded in the other, resembling a tumor [Fraser and Pare].
* #112124 "Shadow"
* #112124 ^definition = Any perceptible discontinuity in film blackening attributed to the attenuation of the X-Ray beam by a specific anatomic absorber or lesion on or within the body of the patient; to be employed only when more specific identification is not possible [Fraser and Pare].
* #112125 "Small irregular opacities"
* #112125 ^definition = Term used to define a reticular pattern specific to pneumoconioses [Fraser and Pare].
* #112126 "Small rounded opacities"
* #112126 ^definition = Term used to define a nodular pattern specific to pneumoconioses [Fraser and Pare].
* #112127 "Tree-in-bud sign"
* #112127 ^definition = Nodular dilation of centrilobular branching structures that resembles a budding tree and represents exudative bronchiolar dilation [Fraser and Pare].
* #112128 "Granular pattern"
* #112128 ^definition = Any extended, finely granular pattern of pulmonary opacity within which normal anatomic details are partly obscured [Fraser and Pare].
* #112129 "Miliary pattern"
* #112129 ^definition = A collection of tiny discrete opacities in the lungs, each measuring 2 mm or less in diameter, generally uniform in size and widespread in distribution [Fraser and Pare].
* #112130 "Mosaic pattern"
* #112130 ^definition = Generalized pattern of relatively well defined areas in the lung having different X-Ray attenuations due to a longstanding underlying
* #112131 "Extremely small"
* #112131 ^definition = A qualitative descriptor of a size that is dramatically less than typical.
* #112132 "Very small"
* #112132 ^definition = A qualitative descriptor of a size that is considerably less than typical.
* #112133 "Too small"
* #112133 ^definition = A qualitative descriptor of a size that is so small as to be abnormal versus expected size.
* #112134 "Elliptic"
* #112134 ^definition = Shaped like an ellipse (oval).
* #112135 "Lobulated"
* #112135 ^definition = A border shape that is made up of, provided with, or divided into lobules (small lobes, curved or rounded projections or divisions).
* #112136 "Spiculated"
* #112136 ^definition = Radially orientated border shape.
* #112137 "Sharply defined"
* #112137 ^definition = The border of a shadow (opacity) is sharply defined [Fraser and Pare].
* #112138 "Distinctly defined"
* #112138 ^definition = The border of a shadow (opacity) is distinctly defined [Fraser and Pare].
* #112139 "Well demarcated"
* #112139 ^definition = The border of a shadow (opacity) is well distinct from adjacent structures [Fraser and Pare].
* #112140 "Sharply demarcated"
* #112140 ^definition = The border of a shadow (opacity) is sharply distinct from adjacent structures [Fraser and Pare].
* #112141 "Poorly demarcated"
* #112141 ^definition = The border of a shadow (opacity) is poorly distinct from adjacent structures [Fraser and Pare].
* #112142 "Circumscribed"
* #112142 ^definition = A shadow (opacity) possessing a complete or nearly complete visible border [Fraser and Pare].
* #112143 "Air"
* #112143 ^definition = Inspired atmospheric gas. The word is sometimes used to describe gas within the body regardless of its composition or site [Fraser and Pare].
* #112144 "Soft tissue"
* #112144 ^definition = Material having X-Ray attenuation properties similar to muscle.
* #112145 "Calcium"
* #112145 ^definition = Material having X-Ray attenuation properties similar to calcium, a silver-white bivalent metallic element occurring in plants and animals.
* #112146 "Acinar"
* #112146 ^definition = A pulmonary opacity 4-8 mm in diameter, presumed to represent anatomic acinus, or a collection of opacities in the lung, each measuring 4-8 mm in diameter, and together producing an extended, homogeneous shadow [Fraser and Pare].
* #112147 "Air space"
* #112147 ^definition = The gas-containing portion of the lung parenchyma, including the acini and excluding the interstitium [Fraser and Pare].
* #112148 "Fibronodular"
* #112148 ^definition = Sharply defined, approximately circular opacities occurring singly or in clusters, usually in the upper lobes [Fraser and Pare].
* #112149 "Fluffy"
* #112149 ^definition = A shadow (opacity) that is ill-defined, lacking clear-cut margins [Fraser and Pare].
* #112150 "Linear"
* #112150 ^definition = A shadow resembling a line; any elongated opacity of approximately uniform width [Fraser and Pare].
* #112151 "Profusion"
* #112151 ^definition = The number of small opacities per unit area or zone of lung. In the International Labor Organization (ILO) classification of radiographs of the pneumoconioses, the qualifiers 0 through 3 subdivide the profusion into 4 categories. The profusion categories may be further subdivided by employing a 12-point scale [Fraser and Pare].
* #112152 "Silhouette sign"
* #112152 ^definition = The effacement of an anatomic soft tissue border by either a normal anatomic structure or a pathologic state such as airlessness of adjacent lung or accumulation of fluid in the contiguous pleural space; useful in detecting and localizing an opacity along the axis of the X-Ray beam [Fraser and Pare].
* #112153 "Subpleural"
* #112153 ^definition = Situated or occurring between the pleura and the body wall.
* #112154 "Bat's wing distribution"
* #112154 ^definition = Spatial arrangement of opacities that bears vague resemblance to the shape of a bat in flight; bilaterally symmetric [Fraser and Pare].
* #112155 "Butterfly distribution"
* #112155 ^definition = Spatial arrangement of opacities that bears vague resemblance to the shape of a butterfly in flight; bilaterally symmetric [Fraser and Pare].
* #112156 "Centrilobular"
* #112156 ^definition = Referring to the region of the bronchioloarteriolar core of a secondary pulmonary lobule [Fraser and Pare].
* #112157 "Coalescent"
* #112157 ^definition = The joining together of a number of opacities into a single opacity [Fraser and Pare].
* #112158 "Lobar"
* #112158 ^definition = Of or relating to a lobe (a curved or rounded projection or division). E.g., involving an entire lobe of the lung.
* #112159 "Hyper-acute"
* #112159 ^definition = Extremely or excessively acute, as a qualitative measure of severity.
* #112160 "Homogeneous (uniform opacity)"
* #112160 ^definition = Of uniform opacity or texture throughout [Fraser and Pare].
* #112161 "Inhomogeneous"
* #112161 ^definition = Lack of homogeneity in opacity or texture.
* #112162 "Target"
* #112162 ^definition = Discrete opacity centrally within a larger opacity, as a calcification descriptor.
* #112163 "Fibrocalcific"
* #112163 ^definition = Pertaining to sharply defined, linear, and/or nodular opacities containing calcification(s) [Fraser and Pare].
* #112164 "Flocculent"
* #112164 ^definition = Calcifications made up of loosely aggregated particles, resembling wool.
* #112165 "Difference in border shape"
* #112165 ^definition = A change in the shape formed by the boundary or edges of a finding or feature.
* #112165 ^designation[0].language = #de-AT 
* #112165 ^designation[0].value = "Retired. Replaced by (F-0517E, SRT, 'Difference in border shape')" 
* #112166 "Difference in border definition"
* #112166 ^definition = A change in the clarity of the boundary or edges of a finding or feature.
* #112166 ^designation[0].language = #de-AT 
* #112166 ^designation[0].value = "Retired. Replaced by (F-05166, SRT, 'Difference in border definition')" 
* #112167 "Difference in distribution"
* #112167 ^definition = A change in the extent of spreading of a finding or feature.
* #112167 ^designation[0].language = #de-AT 
* #112167 ^designation[0].value = "Retired. Replaced by (F-0516C, SRT, 'Difference in distribution')" 
* #112168 "Difference in site involvement"
* #112168 ^definition = A change in the part(s) of the anatomy affected or encompassed by a finding or feature.
* #112168 ^designation[0].language = #de-AT 
* #112168 ^designation[0].value = "Retired. Replaced by (F-05170, SRT, 'Difference in site involvement')" 
* #112169 "Difference in Type of Content"
* #112169 ^definition = A change in the matter or substance within a finding or feature.
* #112169 ^designation[0].language = #de-AT 
* #112169 ^designation[0].value = "Retired. Replaced by (F-05167, SRT, 'Difference in substance')" 
* #112170 "Difference in Texture"
* #112170 ^definition = A change in the surface or consistency of a finding or feature.
* #112170 ^designation[0].language = #de-AT 
* #112170 ^designation[0].value = "Retired. Replaced by (F-0516A, SRT, 'Difference in texture')" 
* #112171 "Fiducial mark"
* #112171 ^definition = A location in image space, which may or may not correspond to an anatomical reference, which is often used for registering data sets.
* #112172 "Portacath"
* #112172 ^definition = Connected to an injection chamber placed under the skin in the upper part of the chest. When it is necessary to inject some drug, a specific needle is put in the chamber through the skin and a silicon membrane. The advantage of a portacath is that it may be left in place several months contrarily of "classical" catheters.
* #112173 "Chest tube"
* #112173 ^definition = A tube inserted into the chest wall from outside the body, for drainage. Sometimes used for collapsed lung. Usually connected to a receptor placed lower than the insertion site.
* #112174 "Central line"
* #112174 ^definition = A tube placed into the subclavian vein to deliver medication directly into the venous system.
* #112175 "Kidney stent"
* #112175 ^definition = A stent is a tube inserted into another tube. Kidney stent is a tube that is inserted into the kidney, ureter, and bladder, to help drain urine. Usually inserted through a scoping device presented through the urethra.
* #112176 "Pancreatic stent"
* #112176 ^definition = A stent is a tube inserted into another tube. Pancreatic stent is inserted through the common bile duct to the pancreatic duct, to drain bile.
* #112177 "Nipple ring"
* #112177 ^definition = A non-lesion object that appears to be a circular band, attached to the body via pierced nipple.
* #112178 "Coin"
* #112178 ^definition = A non-lesion object that appears to be a flat round piece of metal.
* #112179 "Minimum Attenuation Coefficient"
* #112179 ^definition = The least quantity assignable, admissible, or possible; the least of a set of X-Ray attenuation coefficients.
* #112180 "Maximum Attenuation Coefficient"
* #112180 ^definition = The greatest quantity or value attainable or attained; the largest of a set of X-Ray attenuation coefficients.
* #112181 "Mean Attenuation Coefficient"
* #112181 ^definition = The value that is computed by dividing the sum of a set of X-Ray attenuation coefficients by the number of values .
* #112182 "Median Attenuation Coefficient"
* #112182 ^definition = The value in an ordered set of X-Ray attenuation coefficients, below and above which there is an equal number of values.
* #112183 "Standard Deviation of Attenuation Coefficient"
* #112183 ^definition = For a set of X-Ray attenuation coefficients: 1) a measure of the dispersion of a frequency distribution that is the square root of the arithmetic mean of the squares of the deviation of each of the class frequencies from the arithmetic mean of the frequency distribution; 2) a parameter that indicates the way in which a probability function or a probability density function is centered around its mean and that is equal to the square root of the moment in which the deviation from the mean is squared.
* #112184 "Performance of Pediatric and Adult Thoracic CT"
* #112184 ^definition = American College of Radiology. ACR Standard for the Performance of Pediatric and Adult Thoracic Computed Tomography (CT). In: Standards. Reston, Va: 2001:103-107.
* #112185 "Performance of CT for Detection of Pulmonary Embolism in Adults"
* #112185 ^definition = American College of Radiology. ACR Standard for the Performance of Computed Tomography for the Detection of Pulmonary Embolism in Adults. In: Standards. Reston, Va: 2001:109-113.
* #112186 "Performance of High-Resolution CT of the Lungs in Adults"
* #112186 ^definition = American College of Radiology. ACR Standard for the Performance of High-Resolution Computed Tomography (HRCT) of the Lungs in Adults. In: Standards. Reston, Va: 2001:115-118.
* #112187 "Unspecified method of calculation"
* #112187 ^definition = The method of calculation of a measurement or other type of numeric value is not specified.
* #112188 "Two-dimensional method"
* #112188 ^definition = The calculation method was performed in two-dimensional space.
* #112189 "Three-dimensional method"
* #112189 ^definition = The calculation method was performed in three-dimensional space.
* #112191 "Breast tissue density"
* #112191 ^definition = The relative density of parenchymal tissue as a proportion of breast volume.
* #112192 "Volume of parenchymal tissue"
* #112192 ^definition = The volume of parenchymal tissue.
* #112193 "Volume of breast"
* #112193 ^definition = The volume of the breast.
* #112194 "Mass of parenchymal tissue"
* #112194 ^definition = The mass of parenchymal tissue.
* #112195 "Mass of breast"
* #112195 ^definition = The mass of the breast.
* #112196 "Area of Vascular Calcification"
* #112196 ^definition = A measured or calculated area of vascular calcification.
* #112197 "Volume of Vascular Calcification"
* #112197 ^definition = A measured or calculated volume of vascular calcification.
* #112198 "Percentage of Vascular Calcification"
* #112198 ^definition = A measured or calculated percentage of vascular calcification.
* #112199 "Mass of Vascular Calcification"
* #112199 ^definition = A measured or calculated mass of vascular calcification.
* #112200 "Average calcification distance in a calcification cluster"
* #112200 ^definition = The average nearest neighbor distance of all individual microcalcifications in a cluster.
* #112201 "Standard deviation distance of calcifications in a cluster"
* #112201 ^definition = The standard deviation of nearest neighbor distance of all individual microcalcifications in a cluster.
* #112220 "Colon CAD Report"
* #112220 ^definition = A structured report containing the results of computer-aided detection or diagnosis applied to colon imaging and associated clinical information.
* #112222 "Colon Overall Assessment"
* #112222 ^definition = Overall interpretation of the colon using C-RADS categorization system.
* #112224 "Image Set Properties"
* #112224 ^definition = Characteristics of a set of images.
* #112225 "Slice Thickness"
* #112225 ^definition = Nominal slice thickness, in mm.
* #112226 "Spacing between slices"
* #112226 ^definition = Distance between contiguous images, measured from the center-to-center of each image.
* #112227 "Frame of Reference UID"
* #112227 ^definition = Uniquely identifies groups of composite instances that have the same coordinate system that conveys spatial and/or temporal information.
* #112228 "Recumbent Patient Position with respect to gravity"
* #112228 ^definition = Patient orientation with respect to downward direction (gravity).
* #112229 "Identifying Segment"
* #112229 ^definition = Distinguishes a part of a segmentation.
* #112232 "Polyp stalk width"
* #112232 ^definition = The diameter of a polyp stalk measured perpendicular to the axis of the stalk.
* #112233 "Distance from anus"
* #112233 ^definition = The length of the path following the centerline of the colon from the anus to the area of interest.
* #112238 "Anatomic non-colon"
* #112238 ^definition = A location in the body that is outside the colon.
* #112240 "C0 - Inadequate Study/Awaiting Prior Comparisons"
* #112240 ^definition = An inadequate study or a study that is awaiting prior comparisons. The study may have inadequate preparation and cannot exclude lesions greater than or equal to ten millimeters owing to presence of fluid or feces. The study may have inadequate insufflation where one or more colonic segments collapsed on both views. Based on "CT Colonography Reporting and Data System: A Consensus Proposal", Radiology, July 2005; 236:3-9.
* #112241 "C1 - Normal Colon or Benign Lesion"
* #112241 ^definition = The study has a normal colon or benign lesion, with the recommendation to continue routine screening. The study has no visible abnormalities of the colon. The study has no polyps greater than six millimeters. The study may have lipoma, inverted diverticulum, or nonneoplastic findings, such as colonic diverticula. Based on "CT Colonography Reporting and Data System: A Consensus Proposal", Radiology, July 2005; 236:3-9.
* #112242 "C2 - Intermediate Polyp or Indeterminate Finding"
* #112242 ^definition = The study has an intermediate polyp or indeterminate finding and surveillance or colonoscopy is recommended. There may be intermediate polyps between six and nine millimeters and there are less than three in number. The study may have an intermediate finding and cannot exclude a polyp that is greater than or equal to six millimeters in a technically adequate exam. Based on "CT Colonography Reporting and Data System: A Consensus Proposal", Radiology, July 2005; 236:3-9.
* #112243 "C3 - Polyp, Possibly Advanced Adenoma"
* #112243 ^definition = The study has a polyp, possibly advanced adenoma, and a follow-up colonoscopy is recommended. The study has a polyp greater than or equal to ten millimeters or the study has three or more polyps that are each between six to nine millimeters. Based on "CT Colonography Reporting and Data System: A Consensus Proposal", Radiology, July 2005; 236:3-9.
* #112244 "C4 - Colonic Mass, Likely Malignant"
* #112244 ^definition = The study has a colonic mass, likely malignant, and surgical consultation is recommended. The lesion compromises bowel lumen and demonstrates extracolonic invasion. Based on "CT Colonography Reporting and Data System: A Consensus Proposal", Radiology, July 2005; 236:3-9.
* #112248 "ACR Guideline, Performance of Adult CT Colonography"
* #112248 ^definition = American College of Radiology Practice Guideline for the Performance of Computed Tomography (CT) Colonography in Adults. In: Practice Guidelines and Technical Standards.Reston, Va: American College of Radiology;2006:371-376.
* #112249 "ACR Standard, CT medical physics performance monitoring"
* #112249 ^definition = American College of Radiology Technical Standard for Diagnostic Medical Physics Performance Monitoring of Computed Tomography (CT) Equipment. In: Practice Guidelines and Technical Standards.Reston, Va: American College of Radiology;2006:945-948.
* #112300 "AP+45"
* #112300 ^definition = View Orientation Modifier indicates that the view orientation of the imaging plane is rotated +45° along the cranial-caudal axis.
* #112301 "AP-45"
* #112301 ^definition = View Orientation Modifier indicates that the view orientation of the imaging plane is rotated -45° along the cranial-caudal axis.
* #112302 "Anatomical axis of femur"
* #112302 ^definition = The axis following the shaft of the femur.
* #112303 "Acetabular Center of Rotation"
* #112303 ^definition = Center of Rotation of the natural Acetabulum.
* #112304 "Femur Head Center of Rotation"
* #112304 ^definition = Center of Rotation of the natural femur head.
* #112305 "Acetabular Cup Shell"
* #112305 ^definition = Prosthetic component implanted into the acetabulum. Provides hold for the insert that is mounted inside the cup.
* #112306 "Acetabular Cup Insert"
* #112306 ^definition = Prosthetic pelvic joint component. Inserted into the cup, takes in the femoral head replacement.
* #112307 "Acetabular Cup Monoblock"
* #112307 ^definition = Prosthetic pelvic joint cup including insert.
* #112308 "Femoral Head Ball Component"
* #112308 ^definition = Component for Femoral Head Prosthesis where the conic intake for the stem neck can be exchanged.
* #112309 "Femoral Head Cone Taper Component"
* #112309 ^definition = Exchangeable neck intake for composite femoral head prosthesis.
* #112310 "Femoral Stem"
* #112310 ^definition = Prosthesis Implanted into the femoral bone to provide force transmission between joint replacement and bone. On the proximal end a conic neck holds the femoral head replacement.
* #112311 "Femoral Stem Distal Component"
* #112311 ^definition = Distal half of a modular stem prosthesis system.
* #112312 "Femoral Stem Proximal Component"
* #112312 ^definition = Proximal half of a modular stem prosthesis system.
* #112313 "Femoral Stem Component"
* #112313 ^definition = Stem prosthetic component with a modular insert for an exchangeable neck component.
* #112314 "Neck Component"
* #112314 ^definition = Prosthetic Neck to be combined with a Stem Component.
* #112315 "Monoblock Stem"
* #112315 ^definition = Prosthetic Stem and Femoral Head in one piece.
* #112316 "Prosthetic Shaft Augment"
* #112316 ^definition = A proximal attachment to the shaft used to compensate for bone deficiencies or bone loss.
* #112317 "Femoral Head Resurfacing Component"
* #112317 ^definition = Artificial femur head surface needed for the partial replacement of the femoral head where only the surface is replaced.
* #112318 "Pinning"
* #112318 ^definition = Fixation using a pin.
* #112319 "Sewing"
* #112319 ^definition = Fixation sewing several objects together.
* #112320 "Bolting"
* #112320 ^definition = Fixation using a bolt.
* #112321 "Wedging"
* #112321 ^definition = Fixation due to forcing an object into a narrow space.
* #112325 "Distal Centralizer"
* #112325 ^definition = Attachment to the distal end of a cemented stem assuring that the stem is in a central position inside the drilled femoral canal before cementation.
* #112340 "Generic 2D Planning"
* #112340 ^definition = Planning by an unspecified 2D method.
* #112341 "Generic 3D Planning"
* #112341 ^definition = Planning by an unspecified 3D method.
* #112342 "Generic Planning for Hip Replacement"
* #112342 ^definition = Planning of a Hip Replacement, by an unspecified method.
* #112343 "Generic Planning for Knee Replacement"
* #112343 ^definition = Planning of Knee Replacement, by an unspecified method.
* #112344 "Müller Method Planning for Hip Replacement"
* #112344 ^definition = Planning of Hip Replacement according to the procedure of M. E. Müller [Eggli et. al.1998].
* #112345 "Implantation Plan"
* #112345 ^definition = A Report containing the results of an Implantation Planning Activity.
* #112346 "Selected Implant Component"
* #112346 ^definition = A selection of one Implant Component.
* #112347 "Component ID"
* #112347 ^definition = Identification ID of an Implant Component.
* #112348 "Implant Template"
* #112348 ^definition = An implant template describing the properties (2D/3D geometry and other data) of one Implant Component.
* #112350 "Component Connection"
* #112350 ^definition = A connection of two Connected Implantation Plan Components.
* #112351 "Mating Feature Set ID"
* #112351 ^definition = ID of a Mating Feature Set in an Implant Component.
* #112352 "Mating Feature ID"
* #112352 ^definition = ID of the Mating Feature in a Mating Feature Set in an Implant Component.
* #112353 "Spatial Registration"
* #112353 ^definition = The Spatial Registration of one or more Implant Components.
* #112354 "Patient Image"
* #112354 ^definition = Patient Images used for an implantation planning activity.
* #112355 "Assembly"
* #112355 ^definition = A collection of Component Connections of Implant Components.
* #112356 "User Selected Fiducial"
* #112356 ^definition = Fiducials that are selected by the user and may or may not belong to anatomical landmarks.
* #112357 "Derived Fiducial"
* #112357 ^definition = Fiducials that represent geometric characteristics, such as center of rotation, and are derived from other fiducials.
* #112358 "Information used for planning"
* #112358 ^definition = All parameters and data that were used for the planning activity.
* #112359 "Supporting Information"
* #112359 ^definition = A description of the plan as encapsulated PDF SOP Instance.
* #112360 "Implant Component List"
* #112360 ^definition = A list of all Implant Components selected for an implantation.
* #112361 "Patient Data Used During Planning"
* #112361 ^definition = Reference to objects containing patient data that is used for planning.
* #112362 "Degrees of Freedom Specification"
* #112362 ^definition = A specification of the values from one or more Degrees of Freedom.
* #112363 "Degree of Freedom ID"
* #112363 ^definition = ID of one Degree of Freedom.
* #112364 "Related Patient Data Not Used During Planning"
* #112364 ^definition = Reference to objects containing patient data that were not used for planning but are somehow related.
* #112365 "Related Implantation Reports"
* #112365 ^definition = Implantation Reports that are somehow related. E.g., contemporaneous implantations that are independent.
* #112366 "Implant Assembly Template"
* #112366 ^definition = Implant Assembly Template.
* #112367 "Planning Information for Intraoperative Usage"
* #112367 ^definition = Information that is intended to be used intra-operatively.
* #112368 "Implantation Patient Positioning"
* #112368 ^definition = Position of the patient on the operating room table.
* #112369 "Fiducial Intent"
* #112369 ^definition = Intended use of the fiducial.
* #112370 "Component Type"
* #112370 ^definition = Type of an Implant Component.
* #112371 "Manufacturer Implant Template"
* #112371 ^definition = Implant Template released by the Manufacturer.
* #112372 "Derived Planning Images"
* #112372 ^definition = Images that are created by a planning application.
* #112373 "Other Derived Planning Data"
* #112373 ^definition = Data that is created by a planning application.
* #112374 "Connected Implantation Plan Component"
* #112374 ^definition = One Implant Component that is connected to another Implant Component.
* #112375 "Planning Method"
* #112375 ^definition = The method used for planning.
* #112376 "Degree of Freedom Exact Translational Value"
* #112376 ^definition = Defines the exact value that was planned for translation.
* #112377 "Degree of Freedom Minimum Translational Value"
* #112377 ^definition = Defines the minimum value that was planned for translation.
* #112378 "Degree of Freedom Maximum Translational Value"
* #112378 ^definition = Defines the maximum value that was planned for translation.
* #112379 "Degree of Freedom Exact Rotational Translation Value"
* #112379 ^definition = Defines the exact value that was planned for rotation.
* #112380 "Degree of Freedom Minimum Rotational Value"
* #112380 ^definition = Defines the minimum value that was planned for rotation.
* #112381 "Degree of Freedom Maximum Rotational Value"
* #112381 ^definition = Defines the maximum value that was planned for rotation.
* #112700 "Peri-operative Photographic Imaging"
* #112700 ^definition = Procedure step protocol for photographic imaging of surgical procedures, including photography of specimens collected.
* #112701 "Gross Specimen Imaging"
* #112701 ^definition = Procedure step protocol for imaging gross specimens, typically with a photographic camera (modality XC), and planning further dissection.
* #112702 "Slide Microscopy"
* #112702 ^definition = Procedure step protocol for imaging slide specimens.
* #112703 "Whole Slide Imaging"
* #112703 ^definition = Procedure step protocol for imaging slide specimens using a whole slide scanner.
* #112704 "WSI 20X RGB"
* #112704 ^definition = Procedure step protocol for imaging slide specimens using a whole slide scanner with a 20X nominal objective lens, in full color, with a single imaging focal plane across the image.
* #112705 "WSI 40X RGB"
* #112705 ^definition = Procedure step protocol for imaging slide specimens using a whole slide scanner with a 40X nominal objective lens, in full color, with a single imaging focal plane across the image.
* #112706 "Illumination Method"
* #112706 ^definition = Technique of illuminating specimen.
* #112707 "Number of focal planes"
* #112707 ^definition = Number of focal planes for a microscopy image acquisition.
* #112708 "Focal plane Z offset"
* #112708 ^definition = Nominal distance above a reference plane (typically a slide glass substrate top surface) of the focal plane.
* #112709 "Magnification selection"
* #112709 ^definition = Microscope magnification based on nominal objective lens power.
* #112710 "Illumination wavelength"
* #112710 ^definition = Nominal center wavelength for an imaging spectral band.
* #112711 "Illumination spectral band"
* #112711 ^definition = Name (coded) for an imaging spectral band.
* #112712 "Optical filter type"
* #112712 ^definition = Type of filter inserted into the optical imaging path.
* #112713 "Tissue selection method"
* #112713 ^definition = Technique for identifying tissue to be imaged versus area of slide not to be imaged.
* #112714 "Multiple planes"
* #112714 ^definition = Imaging performed at multiple imaging (focal) planes.
* #112715 "5X"
* #112715 ^definition = Nominal 5 power objective lens, resulting in a digital image at approximately 2 um/pixel spacing.
* #112716 "10X"
* #112716 ^definition = Nominal 10 power objective lens, resulting in a digital image at approximately 1 um/pixel spacing.
* #112717 "20X"
* #112717 ^definition = Nominal 20 power microscope objective lens, resulting in a digital image at approximately 0.5 um/pixel spacing.
* #112718 "40X"
* #112718 ^definition = Nominal 40 power microscope objective lens, with a combined condenser and objective lens numerical aperture of approximately 1.3, resulting in a digital image at approximately 0.25 um/pixel spacing.
* #112719 "Nominal empty tile suppression"
* #112719 ^definition = Equipment-specific nominal or default method for identifying tiles without tissue imaged for suppression from inclusion in image object.
* #112720 "High threshold empty tile suppression"
* #112720 ^definition = Equipment-specific high threshold method for identifying tiles without tissue imaged for suppression from inclusion in image object.
* #112721 "No empty tile suppression"
* #112721 ^definition = Tiles without tissue imaged are not suppressed from inclusion in image object.
* #113000 "Of Interest"
* #113000 ^definition = Of Interest.
* #113001 "Rejected for Quality Reasons"
* #113001 ^definition = Rejected for Quality Reasons.
* #113002 "For Referring Provider"
* #113002 ^definition = For Referring Provider.
* #113003 "For Surgery"
* #113003 ^definition = For Surgery.
* #113004 "For Teaching"
* #113004 ^definition = For Teaching.
* #113005 "For Conference"
* #113005 ^definition = For Conference.
* #113006 "For Therapy"
* #113006 ^definition = For Therapy.
* #113007 "For Patient"
* #113007 ^definition = For Patient.
* #113008 "For Peer Review"
* #113008 ^definition = For Peer Review.
* #113009 "For Research"
* #113009 ^definition = For Research.
* #113010 "Quality Issue"
* #113010 ^definition = Quality Issue.
* #113011 "Document Title Modifier"
* #113011 ^definition = Document Title Modifier.
* #113012 "Key Object Description"
* #113012 ^definition = Key Object Description.
* #113013 "Best In Set"
* #113013 ^definition = A selection that represents the "best" chosen from a larger set of items. E.g., the best images within a Study or Series. The criteria against which "best" is measured is not defined. Contrast this with the more specific term "Best illustration of finding".
* #113014 "Study"
* #113014 ^definition = A study is a collection of one or more series of medical images, presentation states, and/or SR documents that are logically related for the purpose of diagnosing a patient. A study may include composite instances that are created by a single modality, multiple modalities or by multiple devices of the same modality. [From Section A.1.2.2 Study IE in PS3.3 ]
* #113015 "Series"
* #113015 ^definition = A distinct logical set used to group composite instances. All instances within a Series are of the same modality, in the same Frame of Reference (if any), and created by the same equipment. [See Section A.1.2.3 Series IE in PS3.3 ]
* #113016 "Performed Procedure Step"
* #113016 ^definition = An arbitrarily defined unit of service that has actually been performed (not just scheduled). [From Section 7.3.1.9 Modality Performed Procedure Step in PS3.3 ]
* #113017 "Stage-View"
* #113017 ^definition = An image or set of images illustrating a specific stage (phase in a stress echo exam protocol) and view (combination of the transducer position and orientation at the time of image acquisition).
* #113018 "For Printing"
* #113018 ^definition = For Printing.
* #113020 "For Report Attachment"
* #113020 ^definition = Selection of information objects for attachment to the clinical report of the Current Requested Procedure.
* #113021 "For Litigation"
* #113021 ^definition = List of objects that are related to litigation and should be specially handled. E.g., may apply if a complaint has been received regarding a patient, or a specific set of images has been the subject of a subpoena, and needs to be sequestered or excluded from automatic purging according to retention policy.
* #113026 "Double exposure"
* #113026 ^definition = Double exposure.
* #113030 "Manifest"
* #113030 ^definition = A list of objects that have been exported out of one organizational domain into another domain. Typically, the first domain has no direct control over what the second domain will do with the objects.
* #113031 "Signed Manifest"
* #113031 ^definition = A signed list of objects that have been exported out of one organizational domain into another domain, referenced securely with either Digital Signatures or MACs. Typically, the first domain has no direct control over what the second domain will do with the objects.
* #113032 "Complete Study Content"
* #113032 ^definition = The list of objects that constitute a study at the time that the list was created.
* #113033 "Signed Complete Study Content"
* #113033 ^definition = The signed list of objects that constitute a study at the time that the list was created, referenced securely with either Digital Signatures or MACs.
* #113034 "Complete Acquisition Content"
* #113034 ^definition = The list of objects that were generated in a single procedure step.
* #113035 "Signed Complete Acquisition Content"
* #113035 ^definition = The signed list of objects that were generated in a single procedure step, referenced securely with either Digital Signatures or MACs.
* #113036 "Group of Frames for Display"
* #113036 ^definition = A list of frames or single-frame or entire multi-frame instances that together constitute a set for some purpose, such as might be displayed together in the same viewport, as distinct from another set that might be displayed in a separate viewport.
* #113037 "Rejected for Patient Safety Reasons"
* #113037 ^definition = List of objects whose use is potentially harmful to the patient. E.g., an improperly labeled image could lead to dangerous surgical decisions.
* #113038 "Incorrect Modality Worklist Entry"
* #113038 ^definition = List of objects that were acquired using an incorrect modality worklist entry, and that should not be used, since they may be incorrectly identified.
* #113039 "Data Retention Policy Expired"
* #113039 ^definition = List of objects that have expired according to a defined data retention policy.
* #113040 "Lossy Compression"
* #113040 ^definition = Lossy compression has been applied to an image.
* #113041 "Apparent Diffusion Coefficient"
* #113041 ^definition = Values are derived by calculation of the apparent diffusion coefficient.
* #113042 "Pixel by pixel addition"
* #113042 ^definition = Values are derived by the pixel by pixel addition of two images.
* #113043 "Diffusion weighted"
* #113043 ^definition = Values are derived by calculation of the diffusion weighting.
* #113044 "Diffusion Anisotropy"
* #113044 ^definition = Values are derived by calculation of the diffusion anisotropy.
* #113045 "Diffusion Attenuated"
* #113045 ^definition = Values are derived by calculation of the diffusion attenuation.
* #113046 "Pixel by pixel division"
* #113046 ^definition = Values are derived by the pixel by pixel division of two images.
* #113047 "Pixel by pixel mask"
* #113047 ^definition = Values are derived by the pixel by pixel masking of one image by another.
* #113048 "Pixel by pixel Maximum"
* #113048 ^definition = Values are derived by calculating the pixel by pixel maximum of two or more images.
* #113049 "Pixel by pixel mean"
* #113049 ^definition = Values are derived by calculating the pixel by pixel mean of two or more images.
* #113050 "Metabolite Maps from spectroscopy data"
* #113050 ^definition = Values are derived by calculating from spectroscopy data pixel values localized in two dimensional space based on the concentration of specific metabolites (i.e, at specific frequencies).
* #113051 "Pixel by pixel Minimum"
* #113051 ^definition = Values are derived by calculating the pixel by pixel minimum of two or more images.
* #113052 "Mean Transit Time"
* #113052 ^definition = The time required for blood to pass through a region of tissue.
* #113053 "Pixel by pixel multiplication"
* #113053 ^definition = Values are derived by the pixel by pixel multiplication of two images.
* #113054 "Negative Enhancement Integral"
* #113054 ^definition = Values are derived by calculating negative enhancement integral values.
* #113055 "Regional Cerebral Blood Flow"
* #113055 ^definition = The flow rate of blood perfusing a region of the brain as volume per mass per unit of time.
* #113056 "Regional Cerebral Blood Volume"
* #113056 ^definition = The volume of blood perfusing a region of brain as as volume per mass.
* #113057 "R-Coefficient"
* #113057 ^definition = Correlation Coefficient, r.
* #113058 "Proton Density"
* #113058 ^definition = Values are derived by calculating proton density values.
* #113059 "Signal Change"
* #113059 ^definition = Values are derived by calculating signal change values.
* #113060 "Signal to Noise"
* #113060 ^definition = Values are derived by calculating the signal to noise ratio.
* #113061 "Standard Deviation"
* #113061 ^definition = Values are derived by calculating the standard deviation of two or more images.
* #113062 "Pixel by pixel subtraction"
* #113062 ^definition = Values are derived by the pixel by pixel subtraction of two images.
* #113063 "T1"
* #113063 ^definition = Values are derived by calculating T1 values.
* #113064 "T2*"
* #113064 ^definition = Values are derived by calculating T2* values.
* #113065 "T2"
* #113065 ^definition = Values are derived by calculating T2 values.
* #113066 "Time Course of Signal"
* #113066 ^definition = Values are derived by calculating values based on the time course of signal.
* #113067 "Temperature encoded"
* #113067 ^definition = Values are derived by calculating values based on temperature encoding.
* #113068 "Student's T-Test"
* #113068 ^definition = Values are derived by calculating the value of the Student's T-Test statistic from multiple image samples.
* #113069 "Time To Peak"
* #113069 ^definition = The time from the start of the contrast agent injection to the maximum enhancement value.
* #113070 "Velocity encoded"
* #113070 ^definition = Values are derived by calculating values based on velocity encoded. E.g., phase contrast.
* #113071 "Z-Score"
* #113071 ^definition = Values are derived by calculating the value of the Z-Score statistic from multiple image samples.
* #113072 "Multiplanar reformatting"
* #113072 ^definition = Values are derived by reformatting in a flat plane other than that originally acquired.
* #113073 "Curved multiplanar reformatting"
* #113073 ^definition = Values are derived by reformatting in a curve plane other than that originally acquired.
* #113074 "Volume rendering"
* #113074 ^definition = Values are derived by volume rendering of acquired data.
* #113075 "Surface rendering"
* #113075 ^definition = Values are derived by surface rendering of acquired data.
* #113076 "Segmentation"
* #113076 ^definition = Values are derived by segmentation (classification into tissue types) of acquired data.
* #113077 "Volume editing"
* #113077 ^definition = Values are derived by selectively editing acquired data (removing values from the volume), such as in order to remove obscuring structures or noise.
* #113078 "Maximum intensity projection"
* #113078 ^definition = Values are derived by maximum intensity projection of acquired data.
* #113079 "Minimum intensity projection"
* #113079 ^definition = Values are derived by minimum intensity projection of acquired data.
* #113080 "Glutamate and glutamine"
* #113080 ^definition = For single-proton MR spectroscopy, the resonance peak corresponding to glutamate and glutamine.
* #113081 "Choline/Creatine Ratio"
* #113081 ^definition = For single-proton MR spectroscopy, the ratio between the Choline and Creatine resonance peaks.
* #113082 "N-acetylaspartate /Creatine Ratio"
* #113082 ^definition = For single-proton MR spectroscopy, the ratio between the N-acetylaspartate and Creatine resonance peaks.
* #113083 "N-acetylaspartate /Choline Ratio"
* #113083 ^definition = For single-proton MR spectroscopy, the ratio between the N-acetylaspartate and Choline resonance peaks.
* #113085 "Spatial resampling"
* #113085 ^definition = Values are derived by spatial resampling of acquired data.
* #113086 "Edge enhancement"
* #113086 ^definition = Values are derived by edge enhancement.
* #113087 "Smoothing"
* #113087 ^definition = Values are derived by smoothing.
* #113088 "Gaussian blur"
* #113088 ^definition = Values are derived by Gaussian blurring.
* #113089 "Unsharp mask"
* #113089 ^definition = Values are derived by unsharp masking.
* #113090 "Image stitching"
* #113090 ^definition = Values are derived by stitching two or more images together.
* #113091 "Spatially-related frames extracted from the volume"
* #113091 ^definition = Spatially-related frames in this image are representative frames from the referenced 3D volume data set.
* #113092 "Temporally-related frames extracted from the set of volumes"
* #113092 ^definition = Temporally-related frames in this image are representative frames from the referenced 3D volume data set.
* #113093 "Polar to Rectangular Scan Conversion"
* #113093 ^definition = Conversion of a polar coordinate image to rectangular (Cartesian) coordinate image.
* #113094 "Creatine and Choline"
* #113094 ^definition = For single-proton MR spectroscopy, the resonance peak corresponding to creatine and choline.
* #113095 "Lipid and Lactate"
* #113095 ^definition = For single-proton MR spectroscopy, the resonance peak corresponding to lipid and lactate.
* #113096 "Creatine+Choline/ Citrate Ratio"
* #113096 ^definition = For single-proton MR spectroscopy, the ratio between the Choline and Creatine resonance peak and the Citrate resonance peak.
* #113097 "Multi-energy proportional weighting"
* #113097 ^definition = Image pixels created through proportional weighting of multiple acquisitions at distinct X-Ray energies.
* #113100 "Basic Application Confidentiality Profile"
* #113100 ^definition = De-identification using a profile defined in PS3.15 that requires removing all information related to the identity and demographic characteristics of the patient, any responsible parties or family members, any personnel involved in the procedure, the organizations involved in ordering or performing the procedure, additional information that could be used to match instances if given access to the originals, such as UIDs, dates and times, and private attributes, when that information is present in the non-Pixel Data Attributes, including graphics or overlays.
* #113101 "Clean Pixel Data Option"
* #113101 ^definition = Additional de-identification according to an option defined in PS3.15 that requires any information burned in to the Pixel Data corresponding to the Attribute information specified to be removed by the Profile and any other Options specified also be removed.
* #113102 "Clean Recognizable Visual Features Option"
* #113102 ^definition = Additional de-identification according to an option defined in PS3.15 that requires that sufficient removal or distortion of the Pixel Data shall be applied to prevent recognition of an individual from the instances themselves or a reconstruction of a set of instances.
* #113103 "Clean Graphics Option"
* #113103 ^definition = Additional de-identification according to an option defined in PS3.15 that requires that any information encoded in graphics, text annotations or overlays corresponding to the Attribute information specified to be removed by the Profile and any other Options specified also be removed.
* #113104 "Clean Structured Content Option"
* #113104 ^definition = Additional de-identification according to an option defined in PS3.15 that requires that any information encoded in SR Content Items or Acquisition Context Sequence Items corresponding to the Attribute information specified to be removed by the Profile and any other Options specified also be removed.
* #113105 "Clean Descriptors Option"
* #113105 ^definition = Additional de-identification according to an option defined in PS3.15 that requires that any information that is embedded in text or string Attributes corresponding to the Attribute information specified to be removed by the Profile and any other Options specified also be removed.
* #113106 "Retain Longitudinal Temporal Information Full Dates Option"
* #113106 ^definition = Retention of information that would otherwise be removed during de-identification according to an option defined in PS3.15 that requires that any dates and times be retained,.
* #113107 "Retain Longitudinal Temporal Information Modified Dates Option"
* #113107 ^definition = Retention of information that would otherwise be removed during de-identification according to an option defined in PS3.15 that requires that any dates and times be modified in a manner that preserves temporal relationships. E.g., Study Date and Time.
* #113108 "Retain Patient Characteristics Option"
* #113108 ^definition = Retention of information that would otherwise be removed during de-identification according to an option defined in PS3.15 that requires that any physical characteristics of the patient, which are descriptive rather than identifying information per se, be retained. E.g., Patient's Age, Sex, Size (height) and Weight.
* #113109 "Retain Device Identity Option"
* #113109 ^definition = Retention of information that would otherwise be removed during de-identification according to an option defined in PS3.15 that requires that any information that identifies a device be retained. E.g., Device Serial Number.
* #113110 "Retain UIDs Option"
* #113110 ^definition = Retention of information that would otherwise be removed during de-identification according to an option defined in PS3.15 that requires that UIDs be retained. E.g., SOP Instance UID.
* #113111 "Retain Safe Private Option"
* #113111 ^definition = Retention of information that would otherwise be removed during de-identification according to an option defined in PS3.15 that requires that private attributes that are known not to contain identity information be retained. E.g., private SUV scale factor.
* #113500 "Radiopharmaceutical Radiation Dose Report"
* #113500 ^definition = The procedure report is a Radiopharmaceutical Radiation Dose report
* #113502 "Radiopharmaceutical Administration"
* #113502 ^definition = Information pertaining to the administration of a radiopharmaceutical
* #113503 "Radiopharmaceutical Administration Event UID"
* #113503 ^definition = Unique identification of a single radiopharmaceutical administration event.
* #113505 "Intravenous Extravasation Symptoms"
* #113505 ^definition = Initial signs or symptoms of extravasation
* #113506 "Estimated Extravasation Activity"
* #113506 ^definition = The estimated percentage of administered activity lost at the injection site. The estimation includes extravasation, paravenous administration and leakage at the injection site.
* #113507 "Administered activity"
* #113507 ^definition = The calculated activity at the Radiopharmaceutical Start Time when the radiopharmaceutical is administered to the patient. The residual activity (i.e. radiopharmaceutical not administered) , if measured, is reflected in the calculated value. The estimated extravasation is not reflected in the calculated value.
* #113508 "Pre-Administration Measured Activity"
* #113508 ^definition = Radioactivity measurement of radiopharmaceutical before or during the administration.
* #113509 "Post-Administration Measured Activity"
* #113509 ^definition = Radioactivity measurement of radiopharmaceutical after the administration.
* #113510 "Drug Product Identifier"
* #113510 ^definition = Registered drug establishment code for product, coding scheme example is NDC or RxNorm
* #113511 "Radiopharmaceutical Dispense Unit Identifier"
* #113511 ^definition = The human readable identification of the specific radiopharmaceutical dispensed quantity or dose ("dose" as unit of medication delivery, not radiation dose measure) to be administered to the patient.
* #113512 "Radiopharmaceutical Lot Identifier"
* #113512 ^definition = Identifies the vial, batch or lot number from which the individual dispense radiopharmaceutical quantity (dose) is produced. The Radiopharmaceutical Dispense Unit Identifier records the identification for each individual dose.
* #113513 "Reagent Vial Identifier"
* #113513 ^definition = Identifies the lot or unit serial number for the reagent component for the radiopharmaceutical.
* #113514 "Radionuclide Vial Identifier"
* #113514 ^definition = Identifies the lot or unit serial number for the radionuclide component for the radiopharmaceutical.
* #113516 "Prescription Identifier"
* #113516 ^definition = Administered Product's Prescription Number
* #113517 "Organ Dose Information"
* #113517 ^definition = Information pertaining to the estimated absorbed radiation dose to an organ.
* #113518 "Organ Dose"
* #113518 ^definition = The absorbed radiation dose to organ
* #113520 "MIRD Pamphlet 1"
* #113520 ^definition = Reference authority
* #113521 "ICRP Publication 53"
* #113521 ^definition = Reference authority
* #113522 "ICRP Publication 80"
* #113522 ^definition = Reference authority
* #113523 "ICRP Publication 106"
* #113523 ^definition = Reference authority
* #113526 "MIRDOSE"
* #113526 ^definition = Reference authority
* #113527 "OLINDA-EXM"
* #113527 ^definition = Reference authority
* #113528 "Package Insert"
* #113528 ^definition = Reference authority
* #113529 "Institutionally Approved Estimates"
* #113529 ^definition = Reference authority
* #113530 "Investigational New Drug"
* #113530 ^definition = Reference authority
* #113540 "Activity Measurement Device"
* #113540 ^definition = The type of device that performed the activity measurement.
* #113541 "Dose Calibrator"
* #113541 ^definition = The device that measures the radiation activity of the radiopharmaceutical
* #113542 "Infusion System"
* #113542 ^definition = Radiopharmaceutical Infusion System
* #113543 "Generator"
* #113543 ^definition = Radioisotope Generator
* #113550 "Fasting Duration"
* #113550 ^definition = The number hours the patient has gone without food.
* #113551 "Hydration Volume"
* #113551 ^definition = The amount of fluids the patient has consumed before the procedure.
* #113552 "Recent Physical Activity"
* #113552 ^definition = A description of physical activity the patient performed before the start of the procedure, such as that which may affect imaging agent biodistribution.
* #113560 "Acute unilateral renal blockage"
* #113560 ^definition = Blockage in one of the tubes (ureters) that drain urine from the kidneys
* #113561 "Low Thyroid Uptake"
* #113561 ^definition = 5% or less Thyroid Uptake of Iodine
* #113562 "High Thyroid Uptake"
* #113562 ^definition = 25% or higher Thyroid Uptake of Iodine
* #113563 "Severely Jaundiced"
* #113563 ^definition = The patient exhibits symptoms severe of jaundice and/or has a Bilirubin >10 mg/dL.
* #113568 "Extravasation visible in image"
* #113568 ^definition = Extravasation or paravenous administration of the product is visible in the images.
* #113570 "Cockroft-Gault Formula estimation of GFR"
* #113570 ^definition = The measurement method of the Glomerular Filtration Rate is Cockroft-Gault Formula
* #113571 "CKD-EPI Formula estimation of GFR"
* #113571 ^definition = The measurement method of the Glomerular Filtration Rate is CKD-EPI Formula
* #113572 "Glomerular Filtration Rate (MDRD)"
* #113572 ^definition = The measurement method of the Glomerular Filtration Rate is MDRD
* #113573 "Glomerular Filtration Rate non-black (MDRD)"
* #113573 ^definition = The measurement method of the Glomerular Filtration Rate is non-black MDRD
* #113574 "Glomerular Filtration Rate black (MDRD)"
* #113574 ^definition = The measurement method of the Glomerular Filtration Rate is black (MDRD)
* #113575 "Glomerular Filtration Rate female (MDRD)"
* #113575 ^definition = The measurement method of the Glomerular Filtration Rate is female (MDRD)
* #113576 "Glomerular Filtration Rate Cystatin-based formula"
* #113576 ^definition = The measurement method of the Glomerular Filtration Rate is Cystatin-based formula
* #113577 "Glomerular Filtration Rate Creatinine-based formula (Schwartz)"
* #113577 ^definition = The measurement method of the Glomerular Filtration Rate is Creatinine-based formula (Schwartz)
* #113601 "Small: < 32.0 cm lateral thickness"
* #113601 ^definition = Small body thickness for calcium scoring adjustment. Lateral thickness is measured from skin-to-skin, at the level of the proximal ascending aorta, from an A/P localizer image.
* #113602 "Medium: 32.0-38.0 cm lateral thickness"
* #113602 ^definition = Medium body thickness for calcium scoring adjustment. Lateral thickness is measured from skin-to-skin, at the level of the proximal ascending aorta, from an A/P localizer image.
* #113603 "Large: > 38.0 cm lateral thickness"
* #113603 ^definition = Large body thickness for calcium scoring adjustment. Lateral thickness is measured from skin-to-skin, at the level of the proximal ascending aorta, from an A/P localizer image.
* #113605 "Irradiation Event Label"
* #113605 ^definition = A human-readable label identifying an irradiation event.
* #113606 "Label Type"
* #113606 ^definition = The type of a human-readable label.
* #113607 "Series Number"
* #113607 ^definition = A number that identifies a Series. Corresponds to (0020,0011) in PS3.3.
* #113608 "Acquisition Number"
* #113608 ^definition = A number that identifies an Acquisition. Corresponds to (0020,0012) in PS3.3.
* #113609 "Instance Number"
* #113609 ^definition = A number that identifies an Instance. Corresponds to (0020,0013) in PS3.3.
* #113611 "Stationary Acquisition"
* #113611 ^definition = Acquisition where the X-Ray source does not move in relation to the patient.
* #113612 "Stepping Acquisition"
* #113612 ^definition = Acquisition where the X-Ray source moves laterally in relation to the patient.
* #113613 "Rotational Acquisition"
* #113613 ^definition = Acquisition where the X-Ray source moves angularly in relation to the patient.
* #113620 "Plane A"
* #113620 ^definition = Primary plane of a Biplane acquisition equipment.
* #113621 "Plane B"
* #113621 ^definition = Secondary plane of a Biplane acquisition equipment.
* #113622 "Single Plane"
* #113622 ^definition = Single plane acquisition equipment.
* #113630 "Continuous"
* #113630 ^definition = Continuous X-Ray radiation is applied during an irradiation event.
* #113631 "Pulsed"
* #113631 ^definition = Pulsed X-Ray radiation is applied during an irradiation event.
* #113650 "Strip filter"
* #113650 ^definition = Filter with uniform thickness.
* #113651 "Wedge filter"
* #113651 ^definition = Filter with variation in thickness from one edge to the opposite edge.
* #113652 "Butterfly filter"
* #113652 ^definition = Filter with two triangular sections.
* #113653 "Flat filter"
* #113653 ^definition = Filter with uniform thickness that is for spectral filtering only. E.g., filter out low energy portion of the X-Ray that would only contribute to skin dose, but not to image.
* #113661 "Outline of lobulations"
* #113661 ^definition = A polyline defining the outline of a lobulated finding.
* #113662 "Inner limits of fuzzy margin"
* #113662 ^definition = A polyline defining the inner limits of a finding with fuzzy margin.
* #113663 "Outer limits of fuzzy margin"
* #113663 ^definition = A polyline defining the outer limits of a finding with fuzzy margin.
* #113664 "Outline of spiculations"
* #113664 ^definition = A polyline defining the outline of the spiculations of a finding.
* #113665 "Linear spiculation"
* #113665 ^definition = A polyline segment graphically indicating the location and direction of a spiculation of a finding.
* #113666 "Pixelated spiculations"
* #113666 ^definition = A collection of points indicating the pixel locations of the spiculations of a finding.
* #113669 "Orthogonal location arc"
* #113669 ^definition = Connected line segments indicating the center of location of a finding on an orthogonal view.
* #113670 "Orthogonal location arc inner margin"
* #113670 ^definition = Connected line segments indicating the inner margin of the location of a finding on an orthogonal view.
* #113671 "Orthogonal location arc outer margin"
* #113671 ^definition = Connected line segments indicating the outer location of a finding on an orthogonal view.
* #113680 "Quality Control Intent"
* #113680 ^definition = This procedure is intended to gather data that is used for calibration or other quality control purposes.
* #113681 "Phantom"
* #113681 ^definition = An artificial subject of an imaging study.
* #113682 "ACR Accreditation Phantom - CT"
* #113682 ^definition = A phantom acceptable for the ACR Computed Tomography Accreditation program.
* #113683 "ACR Accreditation Phantom - MR"
* #113683 ^definition = A phantom acceptable for the ACR Magnetic Resonance Imaging Accreditation program.
* #113684 "ACR Accreditation Phantom - Mammography"
* #113684 ^definition = A phantom acceptable for the ACR Mammography Accreditation program.
* #113685 "ACR Accreditation Phantom - Stereotactic Breast Biopsy"
* #113685 ^definition = A phantom acceptable for the ACR Stereotactic Breast Biopsy Accreditation program.
* #113686 "ACR Accreditation Phantom - ECT"
* #113686 ^definition = A phantom acceptable for the ACR SPECT Accreditation program (but not for PET).
* #113687 "ACR Accreditation Phantom - PET"
* #113687 ^definition = A phantom acceptable for the ACR PET Accreditation program (but not for SPECT).
* #113688 "ACR Accreditation Phantom - ECT/PET"
* #113688 ^definition = A SPECT phantom with a PET faceplate acceptable for both the ACR SPECT and PET Accreditation programs.
* #113689 "ACR Accreditation Phantom - PET Faceplate"
* #113689 ^definition = A PET faceplate (made to fit an existing flangeless or flanged ECT phantom) acceptable for the ACR PET Accreditation program.
* #113690 "IEC Head Dosimetry Phantom"
* #113690 ^definition = A phantom used for CTDI measurement in head modes according to IEC 60601-2-44, Ed.2.1 (Head 16 cm diameter Polymethyl methacrylate PMMA).
* #113691 "IEC Body Dosimetry Phantom"
* #113691 ^definition = A phantom used for CTDI measurement in body modes according to IEC 60601-2-44, Ed.2.1 (Body 32cm diameter Polymethyl methacrylate PMMA).
* #113692 "NEMA XR21-2000 Phantom"
* #113692 ^definition = A phantom in accordance with NEMA standard XR-21-2000.
* #113701 "X-Ray Radiation Dose Report"
* #113701 ^definition = X-Ray Radiation Dose Report.
* #113702 "Accumulated X-Ray Dose Data"
* #113702 ^definition = X-Ray dose data accumulated over multiple irradiation events. E.g., for a study or a performed procedure step.
* #113704 "Projection X-Ray"
* #113704 ^definition = Imaging using a point X-Ray source with a diverging beam projected onto a 2 dimensional detector.
* #113705 "Scope of Accumulation"
* #113705 ^definition = Entity over which dose accumulation values are integrated.
* #113706 "Irradiation Event X-Ray Data"
* #113706 ^definition = X-Ray dose data for a single Irradiation Event.
* #113710 "Niobium or Niobium compound"
* #113710 ^definition = Material containing Niobium or a Niobium compound
* #113710 ^designation[0].language = #de-AT 
* #113710 ^designation[0].value = "Retired. Replaced by (C-1190E, SRT, 'Niobium or Niobium compound')" 
* #113711 "Europium or Europium compound"
* #113711 ^definition = Material containing Europium or a Europium compound
* #113711 ^designation[0].language = #de-AT 
* #113711 ^designation[0].value = "Retired. Replaced by (C-1190F, SRT, 'Europium or Europium compound')" 
* #113720 "Calibration Protocol"
* #113720 ^definition = Describes the method used to derive the calibration factor.
* #113721 "Irradiation Event Type"
* #113721 ^definition = Denotes the type of irradiation event recorded.
* #113722 "Dose Area Product Total"
* #113722 ^definition = Total calculated Dose Area Product (in the scope of the including report).
* #113723 "Calibration Date"
* #113723 ^definition = Last calibration Date for the integrated dose meter or dose calculation.
* #113724 "Calibration Responsible Party"
* #113724 ^definition = Individual or organization responsible for calibration.
* #113725 "Dose (RP) Total"
* #113725 ^definition = Total Dose related to Reference Point (RP). (in the scope of the including report).
* #113726 "Fluoro Dose Area Product Total"
* #113726 ^definition = Total calculated Dose Area Product applied in Fluoroscopy Modes (in the scope of the including report).
* #113727 "Acquisition Dose Area Product Total"
* #113727 ^definition = Total calculated Dose Area Product applied in Acquisition Modes (in the scope of the including report).
* #113728 "Fluoro Dose (RP) Total"
* #113728 ^definition = Dose applied in Fluoroscopy Modes, related to Reference Point (RP). (in the scope of the including report).
* #113729 "Acquisition Dose (RP) Total"
* #113729 ^definition = Dose applied in Acquisition Modes, related to Reference Point (RP). (in the scope of the including report).
* #113730 "Total Fluoro Time"
* #113730 ^definition = Total accumulated clock time of Fluoroscopy in the scope of the including report (i.e., the sum of the Irradiation Duration values for accumulated fluoroscopy irradiation events).
* #113731 "Total Number of Radiographic Frames"
* #113731 ^definition = Accumulated Count of exposure pulses (single or multi-frame encoded) created from irradiation events performed with high dose (acquisition).
* #113732 "Fluoro Mode"
* #113732 ^definition = Mode of application of X-Rays during Fluoroscopy.
* #113733 "KVP"
* #113733 ^definition = Applied X-Ray Tube voltage at peak of X-Ray generation, in kilovolts; Mean value if measured over multiple peaks (pulses).
* #113734 "X-Ray Tube Current"
* #113734 ^definition = Mean value of applied Tube Current.
* #113735 "Exposure Time"
* #113735 ^definition = Cumulative time the patient has received X-Ray exposure during the irradiation event
* #113735 ^designation[0].language = #de-AT 
* #113735 ^designation[0].value = "Retired. Replaced by (113824, DCM, 'Exposure Time')." 
* #113736 "Exposure"
* #113736 ^definition = Mean value of X-Ray Current Time product.
* #113737 "Distance Source to Reference Point"
* #113737 ^definition = Distance to the Reference Point (RP) defined according to IEC 60601-2-43 or equipment defined.
* #113738 "Dose (RP)"
* #113738 ^definition = Dose applied at the Reference Point (RP).
* #113739 "Positioner Primary End Angle"
* #113739 ^definition = Positioner Primary Angle at the end of an irradiation event. For further definition see (112011, DCM, "Positioner Primary Angle") .
* #113740 "Positioner Secondary End Angle"
* #113740 ^definition = Positioner Secondary Angle at the end of an irradiation event. For further definition see (112012, DCM, "Positioner Secondary Angle") .
* #113742 "Irradiation Duration"
* #113742 ^definition = Clock time from the start of loading time of the first pulse until the loading time trailing edge of the final pulse in the same irradiation event.
* #113743 "Patient Orientation"
* #113743 ^definition = Orientation of the Patient with respect to Gravity.
* #113744 "Patient Orientation Modifier"
* #113744 ^definition = Enhances or modifies the Patient orientation specified in Patient Orientation.
* #113745 "Patient Table Relationship"
* #113745 ^definition = Orientation of the Patient with respect to the Head of the Table.
* #113748 "Distance Source to Isocenter"
* #113748 ^definition = Distance from the X-Ray Source to the Equipment C-Arm Isocenter.(Center of Rotation).
* #113750 "Distance Source to Detector"
* #113750 ^definition = Measured or calculated distance from the X-Ray source to the detector plane in the center of the beam.
* #113751 "Table Longitudinal Position"
* #113751 ^definition = Table Longitudinal Position with respect to an arbitrary chosen reference by the equipment. Table motion towards LAO is positive assuming that the patient is positioned supine and its head is in normal position.
* #113752 "Table Lateral Position"
* #113752 ^definition = Table Lateral Position with respect to an arbitrary chosen reference by the equipment. Table motion towards CRA is positive assuming that the patient is positioned supine and its head is in normal position.
* #113753 "Table Height Position"
* #113753 ^definition = Table Height Position with respect to an arbitrary chosen reference by the equipment in (mm). Table motion downwards is positive.
* #113754 "Table Head Tilt Angle"
* #113754 ^definition = Angle of the head-feet axis of the table in degrees relative to the horizontal plane. Positive values indicate that the head of the table is upwards.
* #113755 "Table Horizontal Rotation Angle"
* #113755 ^definition = Rotation of the table in the horizontal plane (clockwise when looking from above the table).
* #113756 "Table Cradle Tilt Angle"
* #113756 ^definition = Angle of the left-right axis of the table in degrees relative to the horizontal plane. Positive values indicate that the left of the table is upwards.
* #113757 "X-Ray Filter Material"
* #113757 ^definition = X-Ray absorbing material used in the filter.
* #113758 "X-Ray Filter Thickness Minimum"
* #113758 ^definition = The minimum thickness of the X-Ray absorbing material used in the filters.
* #113759 "Table Longitudinal End Position"
* #113759 ^definition = Table Longitudinal Position at the end of an irradiation event; see (113751, DCM, "Table Longitudinal Position") .
* #113760 "Table Lateral End Position"
* #113760 ^definition = Table Lateral Position at the end of an irradiation event; see (113752, DCM, "Table Lateral Position") .
* #113761 "Table Height End Position"
* #113761 ^definition = Table Height Position at the end of an irradiation event; see (113753, DCM, "Table Height Position") .
* #113763 "Calibration Uncertainty"
* #113763 ^definition = Uncertainty of the 'actual' value.
* #113764 "Acquisition Plane"
* #113764 ^definition = Identification of Acquisition Plane with Biplane systems.
* #113766 "Focal Spot Size"
* #113766 ^definition = Nominal Size of Focal Spot of X-Ray Tube.
* #113767 "Average X-Ray Tube Current"
* #113767 ^definition = Average X-Ray Tube Current averaged over time for pulse or for continuous Fluoroscopy.
* #113768 "Number of Pulses"
* #113768 ^definition = Number of pulses applied by X-Ray systems during an irradiation event (acquisition run or pulsed fluoro).
* #113769 "Irradiation Event UID"
* #113769 ^definition = Unique identification of a single irradiation event.
* #113770 "Column Angulation"
* #113770 ^definition = Angle of the X-Ray beam in degree relative to an orthogonal axis to the detector plane.
* #113771 "X-Ray Filters"
* #113771 ^definition = Devices used to modify the energy or energy distribution of X-Rays.
* #113772 "X-Ray Filter Type"
* #113772 ^definition = Type of filter(s) inserted into the X-Ray beam. E.g., wedges.
* #113773 "X-Ray Filter Thickness Maximum"
* #113773 ^definition = The maximum thickness of the X-Ray absorbing material used in the filters.
* #113780 "Reference Point Definition"
* #113780 ^definition = System provided definition of the Reference Point used for Dose calculations.
* #113788 "Collimated Field Height"
* #113788 ^definition = Distance between the collimator blades in pixel column direction as projected at the detector plane.
* #113789 "Collimated Field Width"
* #113789 ^definition = Distance between the collimator blades in pixel row direction as projected at the detector plane.
* #113790 "Collimated Field Area"
* #113790 ^definition = Collimated field area at image receptor. Area for compatibility with IEC 60601-2-43.
* #113791 "Pulse Rate"
* #113791 ^definition = Pulse rate applied by equipment during Fluoroscopy.
* #113792 "Distance Source to Table Plane"
* #113792 ^definition = Measured or calculated distance from the X-Ray source to the table plane in the center of the beam.
* #113793 "Pulse Width"
* #113793 ^definition = (Average) X-Ray pulse width.
* #113794 "Dose Measurement Device"
* #113794 ^definition = Calibrated device to perform dose measurements.
* #113795 "Acquired Image"
* #113795 ^definition = Image acquired during a specified event.
* #113800 "DLP to E conversion via MC computation"
* #113800 ^definition = Effective Dose evaluation from the product of Dose Length Product (DLP) and the Effective Dose Conversion Factor (E/DLP in units of mSv/mGy-cm), where the ratio is derived by means of Monte Carlo computations.
* #113801 "CTDIfreeair to E conversion via MC computation"
* #113801 ^definition = Effective Dose evaluation from the product of the Mean CTDIfreeair and the ratio E/CTDIfreeair (mSv/mGy), where the ratio is derived by means of Monte Carlo computations.
* #113802 "DLP to E conversion via measurement"
* #113802 ^definition = Effective Dose evaluation from the product of Dose Length Product (DLP) and the Effective Dose Conversion Factor (E/DLP in units of mSv/mGy-cm), where the ratio is derived by means of dosimetric measurements with an anthropomorphic phantom.
* #113803 "CTDIfreeair to E conversion via measurement"
* #113803 ^definition = Effective Dose evaluation from the product of the Mean CTDIfreeair and the ratio E/CTDIfreeair (mSv/mGy), where the ratio is derived by means of dosimetric measurements with an anthropomorphic phantom.
* #113804 "Sequenced Acquisition"
* #113804 ^definition = The CT acquisition was performed by acquiring single or multi detector data while rotating the source about the gantry while the table is not moving. Additional slices are acquired by incrementing the table position and again rotating the source about the gantry while the table is not moving.
* #113805 "Constant Angle Acquisition"
* #113805 ^definition = The CT acquisition was performed by holding the source at a constant angle and moving the table to obtain a projection image. E.g., localizer.
* #113806 "Stationary Acquisition"
* #113806 ^definition = The CT acquisition was performed by holding the table at a constant position and acquiring multiple slices over time at the same location.
* #113807 "Free Acquisition"
* #113807 ^definition = The CT acquisition was performed while rotating the source about the gantry while the table movement is under direct control of a human operator or under the control of an analysis application. E.g., fluoro.
* #113808 "ICRP Pub 60"
* #113808 ^definition = Reference authority
* #113809 "Start of X-Ray Irradiation"
* #113809 ^definition = Start, DateTime of the first X-Ray Irradiation Event of the accumulation within a Study.
* #113810 "End of X-Ray Irradiation"
* #113810 ^definition = End, DateTime of the last X-Ray Irradiation Event of the accumulation within a Study.
* #113811 "CT Accumulated Dose Data"
* #113811 ^definition = X-Ray dose accumulated over multiple CT irradiation events. E.g., for a study or a performed procedure step.
* #113812 "Total Number of Irradiation Events"
* #113812 ^definition = Total number of events during the defined scope of accumulation.
* #113813 "CT Dose Length Product Total"
* #113813 ^definition = The total dose length product defined scope of accumulation.
* #113814 "CT Effective Dose Total"
* #113814 ^definition = The total Effective Dose at the defined scope of accumulation.
* #113815 "Patient Model"
* #113815 ^definition = Identification of the reference-patient model used when Effective Dose is evaluated via Monte Carlo calculations or from a Dose Length Product conversion factor based on Monte Carlo calculations.
* #113816 "Condition Effective Dose measured"
* #113816 ^definition = References the physical phantom and the type of dosimeter used when measurements are done to establish Effective Dose Conversion Factors (E/DLP) or ratios E/CTDIfreeair.
* #113817 "Effective Dose Phantom Type"
* #113817 ^definition = Type of Effective Dose phantom used.
* #113818 "Dosimeter Type"
* #113818 ^definition = Type of dosimeter used.
* #113819 "CT Acquisition"
* #113819 ^definition = General description of the CT Irradiation event.
* #113820 "CT Acquisition Type"
* #113820 ^definition = Method of the CT acquisition.
* #113821 "X-Ray Filter Aluminum Equivalent"
* #113821 ^definition = Thickness of an equivalent filter in mm in Aluminum.
* #113822 "CT Acquisition Parameters"
* #113822 ^definition = General description of the acquisition parameters.
* #113823 "Number of X-Ray Sources"
* #113823 ^definition = Number of X-Ray sources.
* #113824 "Exposure Time"
* #113824 ^definition = Total time the patient has received X-Ray exposure during the irradiation event.
* #113825 "Scanning Length"
* #113825 ^definition = Length of the table travel during the entire tube loading, according to IEC 60601-2-44
* #113826 "Nominal Single Collimation Width"
* #113826 ^definition = The value of the nominal width referenced to the location of the isocenter along the z axis of a single row of acquired data in mm.
* #113827 "Nominal Total Collimation Width"
* #113827 ^definition = The value of the nominal width referenced to the location of the isocenter along the z axis of the total collimation in mm over the area of active X-Ray detection.
* #113828 "Pitch Factor"
* #113828 ^definition = For Spiral scanning: Pitch Factor = (Table Feed per Rotation (mm)) /(Nominal Total Collimation Width (mm))
* #113829 "CT Dose"
* #113829 ^definition = General description of CT dose values.
* #113830 "Mean CTDIvol"
* #113830 ^definition = "Mean CTDIvol" refers to the average value of the CTDIvol associated with this acquisition.
* #113831 "CT X-Ray Source Parameters"
* #113831 ^definition = Identification, tube-potential, tube-current, and exposure-time parameters associated with an X-Ray source during an acquisition.
* #113832 "Identification of the X-Ray Source"
* #113832 ^definition = Identifies the particular X-Ray source (in a multi-source CT system) for which the set of X-Ray source parameter values is reported.
* #113833 "Maximum X-Ray Tube Current"
* #113833 ^definition = Maximum X-Ray tube current.
* #113834 "Exposure Time per Rotation"
* #113834 ^definition = The exposure time for one rotation of the source around the object in s.
* #113835 "CTDIw Phantom Type"
* #113835 ^definition = A label describing the type of phantom used for CTDIW measurement according to IEC 60601-2-44 (Head 16 cm diameter PMMA, Body 32 cm diameter PMMA).
* #113836 "CTDIfreeair Calculation Factor"
* #113836 ^definition = The CTDIfreeair Calculation Factor is the CTDIfreeair per mAs, expressed in units of mGy/mAs. The CTDIfreeair Calculation Factor may be used in one method calculating Dose.
* #113837 "Mean CTDIfreeair"
* #113837 ^definition = The average value of the free-in-air CTDI associated with this acquisition.
* #113838 "DLP"
* #113838 ^definition = Dose Length Product (DLP), expressed in mGy-cm, is an index characterizing the product of the CTDIvol and the length scanned. For Spiral scanning, DLP = CTDIvol ? Scanning Length. For Sequenced scanning, DLP = CTDIvol ? Nominal Total Collimation Width ? Cumulative Exposure Time / Exposure Time per Rotation. For Stationary and Free scanning, DLP = CTDIvol ? Nominal Total Collimation Width.
* #113839 "Effective Dose"
* #113839 ^definition = Effective dose in mSv.
* #113840 "Effective Dose Conversion Factor"
* #113840 ^definition = Effective Dose per DLP, reference value for Effective Dose calculation, expressed in mSv/mGY.cm.
* #113841 "ICRP Pub 103"
* #113841 ^definition = Effective Dose Reference authority
* #113842 "X-Ray Modulation Type"
* #113842 ^definition = The type of exposure modulation used for the purpose of limiting the dose.
* #113845 "Exposure Index"
* #113845 ^definition = Measure of the detector response to radiation in the relevant image region of an image acquired with a digital X-Ray imaging system as defined in IEC 62494-1; see PS3.3 definition of Exposure Index Macro.
* #113846 "Target Exposure Index"
* #113846 ^definition = The target value used to calculate the Deviation Index as defined in IEC 62494-1; see PS3.3 definition of Exposure Index Macro.
* #113847 "Deviation Index"
* #113847 ^definition = A scaled representation of the accuracy of the Exposure Index compared to the Target Exposure Index as defined in IEC 62494-1; see PS3.3 definition of Exposure Index Macro.
* #113850 "Irradiation Authorizing"
* #113850 ^definition = The clinician responsible for determining that the irradiating procedure was appropriate for the indications.
* #113851 "Irradiation Administering"
* #113851 ^definition = The person responsible for the administration of radiation.
* #113852 "Irradiation Event"
* #113852 ^definition = An irradiation event is the loading of X-Ray equipment caused by a single continuous actuation of the equipment's irradiation switch, from the start of the loading time of the first pulse until the loading time trailing edge of the final pulse. Any automatic on-off switching of the irradiation source during the event is not treated as separate events, rather the event includes the time between start and stop of irradiation as triggered by the user. E.g., a pulsed fluoro X-Ray acquisition shall be treated as a single irradiation event.
* #113853 "Irradiation Event UID"
* #113853 ^definition = Unique Identifier of an Irradiation Event.
* #113854 "Source of Dose Information"
* #113854 ^definition = Method by which dose-related details of an Irradiation Event were obtained.
* #113855 "Total Acquisition Time"
* #113855 ^definition = Total accumulated acquisition clock time in the scope of the including report (i.e., the sum of the Irradiation Duration values for accumulated acquisition irradiation events).
* #113856 "Automated Data Collection"
* #113856 ^definition = Direct recording of data by a relevant system.
* #113857 "Manual Entry"
* #113857 ^definition = Recording of data by a human operator, including manual transcription of electronic data.
* #113858 "MPPS Content"
* #113858 ^definition = The data is taken from an MPPS SOP Instance.
* #113859 "Irradiating Device"
* #113859 ^definition = A device exposing a patient to ionizing radiation.
* #113860 "15cm from Isocenter toward Source"
* #113860 ^definition = 15cm from the isocenter towards the X-Ray source; See IEC 60601-2-43.
* #113861 "30cm in Front of Image Input Surface"
* #113861 ^definition = 30cm in front (towards the tube) of the input surface of the image receptor; See FDA Federal Performance Standard for Diagnostic X-Ray Systems §1020.32(d) (7).
* #113862 "1cm above Tabletop"
* #113862 ^definition = 1cm above the patient tabletop or cradle; See FDA Federal Performance Standard for Diagnostic X-Ray Systems §1020.32(d) (7).
* #113863 "30cm above Tabletop"
* #113863 ^definition = 30cm above the patient tabletop of cradle; See FDA Federal Performance Standard for Diagnostic X-Ray Systems §1020.32(d) (7).
* #113864 "15cm from Table Centerline"
* #113864 ^definition = 15cm from the centerline of the X-Ray table and in the direction of the X-Ray source; See FDA Federal Performance Standard for Diagnostic X-Ray Systems §1020.32(d) (7).
* #113865 "Entrance exposure to a 4.2 cm breast thickness"
* #113865 ^definition = Standard breast means a 4.2 centimeter (cm) thick compressed breast consisting of 50 percent glandular and 50 percent adipose tissue.
* #113866 "Copied From Image Attributes"
* #113866 ^definition = The data is copied from information present in the image attributes. E.g., dose attributes such as CTDIvol (0018,9345).
* #113867 "Computed From Image Attributes"
* #113867 ^definition = The data is computed from information present in the image attributes. E.g., by using dosimetry information for the specific irradiating device make and model, applied to technique information such as KVP and mAs.
* #113868 "Derived From Human-Readable Reports"
* #113868 ^definition = The data is derived from human-readable reports. E.g., by natural language parsing of text reports, or optical character recognition from reports saved as images by the irradiating device.
* #113870 "Person Name"
* #113870 ^definition = The name of a specific person.
* #113871 "Person ID"
* #113871 ^definition = An identification number or code for a specific person.
* #113872 "Person ID Issuer"
* #113872 ^definition = The organization that issued a Person ID.
* #113873 "Organization Name"
* #113873 ^definition = The name of an organization.
* #113874 "Person Role in Organization"
* #113874 ^definition = The role played by a person in an organization.
* #113875 "Person Role in Procedure"
* #113875 ^definition = The role played by a person in a procedure.
* #113876 "Device Role in Procedure"
* #113876 ^definition = The role played by a device in a procedure.
* #113877 "Device Name"
* #113877 ^definition = The name used to refer to a device; usually locally unique.
* #113878 "Device Manufacturer"
* #113878 ^definition = Manufacturer of a device.
* #113879 "Device Model Name"
* #113879 ^definition = Model Name of a device.
* #113880 "Device Serial Number"
* #113880 ^definition = Serial Number of a device.
* #113890 "All Planes"
* #113890 ^definition = All planes of a multi-plane acquisition equipment.
* #113893 "Length of Reconstructable Volume"
* #113893 ^definition = The length from which images may be reconstructed (i.e., excluding any overranging performed in a spiral acquisition that is required for data interpolation).
* #113895 "Top Z Location of Reconstructable Volume"
* #113895 ^definition = The Z location that is the top (highest Z value) of the Reconstructable Volume. Specified as the Z component within the Patient Coordinate System defined by a specified Frame of Reference.
* #113896 "Bottom Z Location of Reconstructable Volume"
* #113896 ^definition = The Z location that is the bottom (lowest Z value) of the Reconstructable Volume. Specified as the Z component within the Patient Coordinate System defined by a specified Frame of Reference.
* #113897 "Top Z Location of Scanning Length"
* #113897 ^definition = The Z location that is the top (highest Z value) of the scanning length. Specified as the Z component within the Patient Coordinate System defined by a specified Frame of Reference.
* #113898 "Bottom Z Location of Scanning Length"
* #113898 ^definition = The Z location that is the bottom (lowest Z value) of the scanning length. Specified as the Z component within the Patient Coordinate System defined by a specified Frame of Reference.
* #113900 "Dose Check Alert Details"
* #113900 ^definition = Report section about cumulative dose alerts during an examination.
* #113901 "DLP Alert Value Configured"
* #113901 ^definition = Flag denoting whether a DLP Alert Value was configured.
* #113902 "CTDIvol Alert Value Configured"
* #113902 ^definition = Flag denoting whether a CTDIvol Alert Value was configured.
* #113903 "DLP Alert Value"
* #113903 ^definition = Cumulative Dose Length Product value configured to trigger an alert; see NEMA XR 25-2010 Dose Check Standard.
* #113904 "CTDIvol Alert Value"
* #113904 ^definition = Cumulative CTDIvol value configured to trigger an alert; see NEMA XR 25-2010 Dose Check Standard.
* #113905 "Accumulated DLP Forward Estimate"
* #113905 ^definition = A forward estimate of the accumulated DLP plus the estimated DLP for the next Protocol Element Group; see NEMA XR 25-2010 Dose Check Standard.
* #113906 "Accumulated CTDIvol Forward Estimate"
* #113906 ^definition = A forward estimate at a given location of the accumulated CTDIvol plus the estimated CTDIvol for the next Protocol Element Group; see NEMA XR 25-2010 Dose Check Standard.
* #113907 "Reason for Proceeding"
* #113907 ^definition = Reason provided for proceeding with a procedure that is projected to exceed a configured dose value.
* #113908 "Dose Check Notification Details"
* #113908 ^definition = Report section about dose notifications during a protocol element.
* #113909 "DLP Notification Value Configured"
* #113909 ^definition = Flag denoting whether a DLP Notification Value was configured.
* #113910 "CTDIvol Notification Value Configured"
* #113910 ^definition = Flag denoting whether a CTDIvol Notification Value was configured.
* #113911 "DLP Notification Value"
* #113911 ^definition = Dose Length Product value configured to trigger a notification for a given protocol element.
* #113912 "CTDIvol Notification Value"
* #113912 ^definition = CTDIvol value configured to trigger a notification for a given protocol element.
* #113913 "DLP Forward Estimate"
* #113913 ^definition = A forward estimate of the DLP for the next Protocol Element Group; see NEMA XR 25-2010 Dose Check Standard.
* #113914 "CTDIvol Forward Estimate"
* #113914 ^definition = A forward estimate of the CTDIvol for the next Protocol Element Group; see NEMA XR 25-2010 Dose Check Standard.
* #113921 "Radiation Exposure"
* #113921 ^definition = The amount of ionizing radiation to which the patient was exposed.
* #113922 "Radioactive Substance Administered"
* #113922 ^definition = Type, amount and route of radioactive substance administered.
* #113923 "Radiation Exposure and Protection Information"
* #113923 ^definition = Exposure to ionizing radiation and associated preventive measures used to reduce the exposure of parts of the body like lead apron or eye, thyroid gland or gonad protection.
* #113923 ^designation[0].language = #de-AT 
* #113923 ^designation[0].value = "Retired. Replaced by (73569-6, LN, 'Radiation Exposure and Protection Information') " 
* #113930 "Size Specific Dose Estimation"
* #113930 ^definition = The Size-Specific Dose Estimate is a patient dose estimate that takes into account the size of the patient, such as described in AAPM Report 204 by using linear dimensions measured on the patient or patient images or estimated from patient age.
* #113931 "Measured Lateral Dimension"
* #113931 ^definition = The side-to-side (left to right) dimension of the body part being scanned (per AAPM Report 204).
* #113932 "Measured AP Dimension"
* #113932 ^definition = The thickness of the body part being scanned, in the antero-posterior dimension (per AAPM Report 204).
* #113933 "Derived Effective Diameter"
* #113933 ^definition = The diameter of the patient at a given location along the Z-axis of the patient, assuming that the patient has a circular cross-section (per AAPM Report 204).
* #113934 "AAPM 204 Lateral Dimension"
* #113934 ^definition = The Size Specific Dose Estimation is computed using Table 1B (32cm phantom) or Table 2B (16cm phantom) of AAPM Report 204.
* #113935 "AAPM 204 AP Dimension"
* #113935 ^definition = The Size Specific Dose Estimation is computed using Table 1C (32cm phantom) or Table 2C (16cm phantom) of AAPM Report 204.
* #113936 "AAPM 204 Sum of Lateral and AP Dimension"
* #113936 ^definition = The Size Specific Dose Estimation is computed using Table 1A (32cm phantom) or Table 2A (16cm phantom) of AAPM Report 204.
* #113937 "AAPM 204 Effective Diameter Estimated From Patient Age"
* #113937 ^definition = The Size Specific Dose Estimation is computed using Table 1D (32cm phantom) or Table 2D (16cm phantom) using an effective diameter estimated from the patient's age using Table 3 of AAPM Report 204.
* #113940 "System Calculated"
* #113940 ^definition = Values calculated from other existing parameters.
* #113941 "In Detector Plane"
* #113941 ^definition = A segmented region of the detector surface within the irradiated area (but might not be near the center of the detector).
* #113942 "X-Ray Reading Device"
* #113942 ^definition = A device that creates digital images from X-Ray detectors (Direct, Indirect or Storage).
* #113943 "X-Ray Source Data Available"
* #113943 ^definition = Parameters related to the X-Ray source (generator, tube, etc) are available to the recording application.
* #113944 "X-Ray Mechanical Data Available"
* #113944 ^definition = Parameters related to the X-Ray Mechanical System (Stand, Table) are available to the recording application.
* #113945 "X-Ray Detector Data Available"
* #113945 ^definition = Parameters related to the X-Ray Detector are available to the recording application.
* #113946 "Projection Eponymous Name"
* #113946 ^definition = Describes the radiographic method of patient, tube and detector positioning to achieve a well described projection or view.
* #113947 "Detector Type"
* #113947 ^definition = Type of Detector used to acquire data. E.g., Images.
* #113948 "Direct Detector"
* #113948 ^definition = Detector that directly transforms the input signal to pixel values.
* #113949 "Indirect Detector"
* #113949 ^definition = Detector that transforms an intermediate signal into pixel values. E.g., a scintillator-based detector.
* #113950 "Storage Detector"
* #113950 ^definition = Storage detector that stores a signal that is later transformed by a reader into pixel values. E.g., a phosphor-based detector.
* #113951 "Film"
* #113951 ^definition = Film that is scanned to create pixel values.
* #113952 "Table Mount"
* #113952 ^definition = The cassette/detector is mounted in the patient table.
* #113953 "Unmounted Detector"
* #113953 ^definition = The cassette/detector is not mounted.. E.g., a cassette placed underneath the patient.
* #113954 "Upright Stand Mount"
* #113954 ^definition = The cassette/detector is mounted in an upright stand.
* #113955 "C-Arm Mount"
* #113955 ^definition = The cassette/detector is mounted on a c-arm.
* #113956 "CR/DR Mechanical Configuration"
* #113956 ^definition = Method of mounting or positioning a CR/DR cassette or detector.
* #113957 "Fluoroscopy-Guided Projection Radiography System"
* #113957 ^definition = An integrated projection radiography system capable of fluoroscopy.
* #113958 "Integrated Projection Radiography System"
* #113958 ^definition = A projection radiography system where the X-Ray detector, X-Ray Source and gantry components are integrated and the managing system is able to access details of each component.
* #113959 "Cassette-based Projection Radiography System"
* #113959 ^definition = A projection radiography system where the X-Ray detector, X-Ray Source and gantry components are not integrated. E.g., cassette-based CR and DR systems.
* #113961 "Reconstruction Algorithm"
* #113961 ^definition = Description of the algorithm used when reconstructing the image from the data acquired during the acquisition process.
* #113962 "Filtered Back Projection"
* #113962 ^definition = An algorithm for reconstructing an image from multiple projections by back-projecting the measured values along the line of the projection and filtering the result to reduce blurring.
* #113963 "Iterative Reconstruction"
* #113963 ^definition = An algorithm for reconstructing an image from multiple projections by starting with an assumed reconstructed image, computing projections from the image, comparing the original projection data and updating the reconstructed image based upon the difference between the calculated and the actual projections.
* #113970 "Procedure Step To This Point"
* #113970 ^definition = The period of time from the start of a Procedure Step until the time point established by the context of the reference.
* #114000 "Not a number"
* #114000 ^definition = Measurement not available: Not a number (per IEEE 754).
* #114001 "Negative Infinity"
* #114001 ^definition = Measurement not available: Negative Infinity (per IEEE 754).
* #114002 "Positive Infinity"
* #114002 ^definition = Measurement not available: Positive Infinity (per IEEE 754).
* #114003 "Divide by zero"
* #114003 ^definition = Measurement not available: Divide by zero (per IEEE 754).
* #114004 "Underflow"
* #114004 ^definition = Measurement not available: Underflow (per IEEE 754).
* #114005 "Overflow"
* #114005 ^definition = Measurement not available: Overflow (per IEEE 754).
* #114006 "Measurement failure"
* #114006 ^definition = Measurement not available: Measurement failure.
* #114007 "Measurement not attempted"
* #114007 ^definition = Measurement not available: Measurement not attempted.
* #114008 "Calculation failure"
* #114008 ^definition = Measurement not available: Calculation failure.
* #114009 "Value out of range"
* #114009 ^definition = Measurement not available: Value out of range.
* #114010 "Value unknown"
* #114010 ^definition = Measurement not available: Value unknown.
* #114011 "Value indeterminate"
* #114011 ^definition = Measurement not available: Value indeterminate.
* #114201 "Time of flight"
* #114201 ^definition = Measures the time-of-flight of a light signal between the camera and the subject for each point of the image.
* #114202 "Interferometry"
* #114202 ^definition = Interferometry is a family of techniques in which waves are superimposed in order to extract depth information about the scanned object.
* #114203 "Laser scanning"
* #114203 ^definition = Laser scanning describes the general method to sample or scan a surface using laser technology.
* #114204 "Pattern projection"
* #114204 ^definition = Projecting a narrow band of light onto a three-dimensionally shaped surface produces a line of illumination that appears distorted from other perspectives than that of the projector. It can be used for an exact geometric reconstruction of the surface shape.
* #114205 "Shape from shading"
* #114205 ^definition = A technique for estimating the surface normal of an object by observing that object under different lighting conditions.
* #114206 "Shape from motion"
* #114206 ^definition = A technique for estimating the surface normal of an object by observing that object under different motions.
* #114207 "Confocal imaging"
* #114207 ^definition = An optical imaging technique used to increase optical resolution and contrast of a micrograph by using point illumination and a spatial pinhole to eliminate out-of-focus light in specimens that are thicker than the focal plane. It enables the reconstruction of 3D structures from the obtained images.
* #114208 "Point Cloud Algorithmic"
* #114208 ^definition = Point cloud that was calculated by an algorithm.
* #114209 "Turntable Scan Method"
* #114209 ^definition = Scanning the object from different views by placing it on a rotating table.
* #114210 "High resolution"
* #114210 ^definition = Higher resolution with a longer acquisition time.
* #114211 "Fast mode"
* #114211 ^definition = Lower resolution with a shorter acquisition time.
* #114213 "Iterative Closest Point"
* #114213 ^definition = An algorithm employed to minimize the difference between two clouds of points. It iteratively revises the transformation (translation, rotation) needed to minimize the distance between the points of two point clouds.
* #114215 "Freehand"
* #114215 ^definition = Human controlled minimization of the distance between the points of two point clouds.
* #114216 "Checkerboard"
* #114216 ^definition = Scanning the object from different views by placing it in front of a checkerboard pattern.
* #121001 "Quotation Mode"
* #121001 ^definition = Type of source for observations quoted from an external source.
* #121002 "Quoted Source"
* #121002 ^definition = Reference to external source of quoted observations.
* #121003 "Document"
* #121003 ^definition = Documentary source of quoted observations.
* #121004 "Verbal"
* #121004 ^definition = Verbal source of quoted observations.
* #121005 "Observer Type"
* #121005 ^definition = Type of observer that created the observations.
* #121006 "Person"
* #121006 ^definition = Human observer created the observations.
* #121007 "Device"
* #121007 ^definition = Automated device created the observations.
* #121008 "Person Observer Name"
* #121008 ^definition = Name of human observer that created the observations.
* #121009 "Person Observer's Organization Name"
* #121009 ^definition = Organization or institution with which the human observer is affiliated for the context of the current observation.
* #121010 "Person Observer's Role in the Organization"
* #121010 ^definition = Organizational role of human observer for the context of the current observation.
* #121011 "Person Observer's Role in this Procedure"
* #121011 ^definition = Procedural role of human observer for the context of the current observation.
* #121012 "Device Observer UID"
* #121012 ^definition = Unique identifier of automated device that created the observations.
* #121013 "Device Observer Name"
* #121013 ^definition = Institution-provided identifier of automated device that created the observations.
* #121014 "Device Observer Manufacturer"
* #121014 ^definition = Manufacturer of automated device that created the observations.
* #121015 "Device Observer Model Name"
* #121015 ^definition = Manufacturer-provided model name of automated device that created the observations.
* #121016 "Device Observer Serial Number"
* #121016 ^definition = Manufacturer-provided serial number of automated device that created the observations.
* #121017 "Device Observer Physical Location During Observation"
* #121017 ^definition = Location of automated device that created the observations whilst the observations were being made.
* #121018 "Procedure Study Instance UID"
* #121018 ^definition = Unique identifier for the Study or Requested Procedure.
* #121019 "Procedure Study Component UID"
* #121019 ^definition = Unique identifier for the Performed Procedure Step.
* #121020 "Placer Number"
* #121020 ^definition = Identifier for the Order (or Service Request) assigned by the order placer system.
* #121021 "Filler Number"
* #121021 ^definition = Identifier for the Order (or Service Request) assigned by the order filler system.
* #121022 "Accession Number"
* #121022 ^definition = Identifier for the Order (or Service Request) assigned by the department information system.
* #121023 "Procedure Code"
* #121023 ^definition = Type of procedure scheduled or performed.
* #121024 "Subject Class"
* #121024 ^definition = Type of observation subject.
* #121025 "Patient"
* #121025 ^definition = A patient is the subject of observations.
* #121026 "Fetus"
* #121026 ^definition = Fetus of patient is the subject of observations.
* #121027 "Specimen"
* #121027 ^definition = Specimen is the subject of observations.
* #121028 "Subject UID"
* #121028 ^definition = Unique Identifier of patient or fetus who is the subject of observations.
* #121029 "Subject Name"
* #121029 ^definition = Name of patient who is the subject of observations.
* #121030 "Subject ID"
* #121030 ^definition = Identifier of patient or fetus who is the subject of observations.
* #121031 "Subject Birth Date"
* #121031 ^definition = Birth Date of patient who is the subject of observations.
* #121032 "Subject Sex"
* #121032 ^definition = Sex of patient who is the subject of observations.
* #121033 "Subject Age"
* #121033 ^definition = Age of patient who is the subject of observations.
* #121034 "Subject Species"
* #121034 ^definition = Species of patient who is the subject of observations.
* #121035 "Subject Breed"
* #121035 ^definition = The breed of the subject.
* #121036 "Mother of fetus"
* #121036 ^definition = Name of mother of fetus that is the subject of observations.
* #121037 "Fetus number"
* #121037 ^designation[0].language = #de-AT 
* #121037 ^designation[0].value = "Retired. Replaced by (11951-1, LN, 'Fetus ID')." 
* #121038 "Number of Fetuses"
* #121038 ^designation[0].language = #de-AT 
* #121038 ^designation[0].value = "Retired. Replaced by (55281-0, LN, 'Number of Fetuses')." 
* #121039 "Specimen UID"
* #121039 ^definition = Unique Identifier of specimen that is the subject of observations.
* #121040 "Specimen Accession Number"
* #121040 ^definition = Accession Number of specimen that is the subject of observations
* #121040 ^designation[0].language = #de-AT 
* #121040 ^designation[0].value = "Retired." 
* #121041 "Specimen Identifier"
* #121041 ^definition = Identifier of specimen that is the subject of observations.
* #121042 "Specimen Type"
* #121042 ^definition = Coded category of specimen that is the subject of observations
* #121042 ^designation[0].language = #de-AT 
* #121042 ^designation[0].value = "Retired. Replaced by (R-00254, SRT, 'Specimen Type')" 
* #121043 "Slide Identifier"
* #121043 ^definition = Identifier of specimen microscope slide that is the subject of observations
* #121043 ^designation[0].language = #de-AT 
* #121043 ^designation[0].value = "Retired. Replaced by (111700, DCM, 'Specimen Container Identifier') " 
* #121044 "Slide UID"
* #121044 ^definition = Unique Identifier of specimen microscope slide that is the subject of observations
* #121044 ^designation[0].language = #de-AT 
* #121044 ^designation[0].value = "Retired." 
* #121045 "Language"
* #121045 ^definition = The language of the content, being a language that is primarily used for human communication. E.g., English, French.
* #121046 "Country of Language"
* #121046 ^definition = The country-specific variant of language. E.g., Canada for Candadian French.
* #121047 "Language of Value"
* #121047 ^definition = The language of the value component of a name-value pair.
* #121048 "Language of Name and Value"
* #121048 ^definition = The language of both the name component and the value component of a name-value pair.
* #121049 "Language of Content Item and Descendants"
* #121049 ^definition = The language of the current content item (node in a tree of content) and all its descendants.
* #121050 "Equivalent Meaning of Concept Name"
* #121050 ^definition = The human readable meaning of the name component of a name-value pair that is equivalent to the post-coordinated meaning conveyed by the coded name and its concept modifier children.
* #121051 "Equivalent Meaning of Value"
* #121051 ^definition = The human readable meaning of the value component of a name-value pair that is equivalent to the post-coordinated meaning conveyed by the coded value and its concept modifier children.
* #121052 "Presence of property"
* #121052 ^definition = Whether or not the property concept being modified is present or absent.
* #121053 "Present"
* #121053 ^designation[0].language = #de-AT 
* #121053 ^designation[0].value = "Retired. Replaced by (G-A203, SRT, 'Present')" 
* #121054 "Absent"
* #121054 ^designation[0].language = #de-AT 
* #121054 ^designation[0].value = "Retired. Replaced by (R-4089B, SRT, 'Absent')" 
* #121055 "Path"
* #121055 ^definition = A set of points on an image, that when connected by line segments, provide a polyline from which a linear measurement was inferred.
* #121056 "Area outline"
* #121056 ^definition = A set of points on an image, that when connected by line segments, provide a closed polyline that is the border of a defined region from which an area, or two-dimensional measurement, was inferred.
* #121057 "Perimeter outline"
* #121057 ^definition = A set of points on an image, that when connected by line segments, provide a closed polyline that is a two-dimensional border of a three-dimensional region's intersection with, or projection into the image.
* #121058 "Procedure reported"
* #121058 ^definition = The imaging procedure whose results are reported.
* #121059 "Presence Undetermined"
* #121059 ^definition = Presence or absence of a property is undetermined
* #121059 ^designation[0].language = #de-AT 
* #121059 ^designation[0].value = "Retired. Replaced by (R-4089B, SRT, 'Undetermined')" 
* #121060 "History"
* #121060 ^designation[0].language = #de-AT 
* #121060 ^designation[0].value = "Retired. Replaced by (11329-0, LN, 'History') " 
* #121062 "Request"
* #121062 ^designation[0].language = #de-AT 
* #121062 ^designation[0].value = "Retired. Replaced by (55115-0, LN, 'Request') " 
* #121064 "Current Procedure Descriptions"
* #121064 ^designation[0].language = #de-AT 
* #121064 ^designation[0].value = "Retired. Replaced by (55111-9, LN, 'Current Procedure Descriptions') " 
* #121065 "Procedure Description"
* #121065 ^definition = A description of the imaging procedure.
* #121066 "Prior Procedure Descriptions"
* #121066 ^designation[0].language = #de-AT 
* #121066 ^designation[0].value = "Retired. Replaced by (55114-3, LN, 'Prior Procedure Descriptions') " 
* #121068 "Previous Findings"
* #121068 ^designation[0].language = #de-AT 
* #121068 ^designation[0].value = "Retired. Replaced by (18834-2, LN, 'Previous Findings') " 
* #121069 "Previous Finding"
* #121069 ^definition = An observation found on a prior imaging study.
* #121070 "Findings"
* #121070 ^designation[0].language = #de-AT 
* #121070 ^designation[0].value = "Retired. Replaced by (59776-5, LN, 'Findings') " 
* #121071 "Finding"
* #121071 ^definition = An observation found on an imaging study.
* #121072 "Impressions"
* #121072 ^designation[0].language = #de-AT 
* #121072 ^designation[0].value = "Retired. Replaced by (19005-8, LN, 'Impressions') " 
* #121073 "Impression"
* #121073 ^definition = An interpretation in the clinical context of the finding(s) on an imaging study.
* #121074 "Recommendations"
* #121074 ^designation[0].language = #de-AT 
* #121074 ^designation[0].value = "Retired. Replaced by (18783-1, LN, 'Recommendations') " 
* #121075 "Recommendation"
* #121075 ^definition = A recommendation for management or investigation based on the findings and impressions of an imaging study.
* #121076 "Conclusions"
* #121076 ^designation[0].language = #de-AT 
* #121076 ^designation[0].value = "Retired. Replaced by (55110-1, LN, 'Conclusions') " 
* #121077 "Conclusion"
* #121077 ^definition = An interpretation in the clinical context of the finding(s) on an imaging study.
* #121078 "Addendum"
* #121078 ^designation[0].language = #de-AT 
* #121078 ^designation[0].value = "Retired. Replaced by (55107-7, LN, 'Addendum') " 
* #121079 "Baseline"
* #121079 ^definition = Initial images used to esyablish a beginning condition that is used for comparison over time to look for changes. [Paraphrases NCI-PT (C1442488, UMLS, "Baseline"), which is defined as "An initial measurement that is taken at an early time point to represent a beginning condition, and is used for comparison over time to look for changes. For example, the size of a tumor will be measured before treatment (baseline) and then afterwards to see if the treatment had an effect. A starting point to which things may be compared."]
* #121080 "Best illustration of finding"
* #121080 ^definition = A selection of composite instances that best illustrates a particular finding. E.g., an image slice at the location of the largest extent of a tumor.
* #121081 "Physician"
* #121081 ^designation[0].language = #de-AT 
* #121081 ^designation[0].value = "Retired. Replaced by (J-004E8, SRT, 'Physician')" 
* #121082 "Nurse"
* #121082 ^designation[0].language = #de-AT 
* #121082 ^designation[0].value = "Retired. Replaced by (J-07100, SRT, 'Nurse')" 
* #121083 "Technologist"
* #121083 ^designation[0].language = #de-AT 
* #121083 ^designation[0].value = "Retired. Replaced by (J-00187, SRT, 'Radiologic Technologist')" 
* #121084 "Radiographer"
* #121084 ^designation[0].language = #de-AT 
* #121084 ^designation[0].value = "Retired. Replaced by (J-00187, SRT, 'Radiographer')" 
* #121085 "Intern"
* #121085 ^designation[0].language = #de-AT 
* #121085 ^designation[0].value = "Retired. Replaced by (C1144859, UMLS, 'Intern')" 
* #121086 "Resident"
* #121086 ^designation[0].language = #de-AT 
* #121086 ^designation[0].value = "Retired. Replaced by (J-005E6, SRT, 'Resident')" 
* #121087 "Registrar"
* #121087 ^designation[0].language = #de-AT 
* #121087 ^designation[0].value = "Retired. Replaced by (J-00172, SRT, 'Registrar')" 
* #121088 "Fellow"
* #121088 ^definition = A medical practitioner undergoing sub-specialty training. E.g., during the period after specialty training (residency).
* #121089 "Attending [Consultant]"
* #121089 ^designation[0].language = #de-AT 
* #121089 ^designation[0].value = "Retired. Replaced by (J-005E8, SRT, 'Attending')" 
* #121090 "Scrub nurse"
* #121090 ^designation[0].language = #de-AT 
* #121090 ^designation[0].value = "Retired. Replaced by (J-0714A, SRT, 'Scrub nurse')" 
* #121091 "Surgeon"
* #121091 ^designation[0].language = #de-AT 
* #121091 ^designation[0].value = "Retired. Replaced by (J-00556, SRT, 'Surgeon')" 
* #121092 "Sonologist"
* #121092 ^definition = A medical practitioner with sub-specialty training in Ultrasound.
* #121093 "Sonographer"
* #121093 ^designation[0].language = #de-AT 
* #121093 ^designation[0].value = "Retired. Replaced by (C1954848, UMLS, 'Sonographer')" 
* #121094 "Performing"
* #121094 ^definition = The person responsible for performing the procedure.
* #121095 "Referring"
* #121095 ^definition = The person responsible for referring the patient for the procedure.
* #121095 ^designation[0].language = #de-AT 
* #121095 ^designation[0].value = "Retired. Replaced by (C1709880, UMLS, 'Referring physician')." 
* #121096 "Requesting"
* #121096 ^definition = The person responsible for requesting the procedure.
* #121097 "Recording"
* #121097 ^definition = The person responsible for recording the procedure or observation.
* #121098 "Verifying"
* #121098 ^definition = The person responsible for verifying the recorded procedure or observation.
* #121099 "Assisting"
* #121099 ^definition = The person responsible for assisting with the procedure.
* #121100 "Circulating"
* #121100 ^definition = The person responsible for making preparations for and monitoring the procedure.
* #121100 ^designation[0].language = #de-AT 
* #121100 ^designation[0].value = "Retired. Replaced by (J-0714B, SRT, 'Circulating Nurse')." 
* #121101 "Standby"
* #121101 ^definition = The person responsible for standing by to assist with the precedure if required.
* #121102 "Other sex"
* #121102 ^definition = Other sex.
* #121103 "Undetermined sex"
* #121103 ^definition = Sex of subject undetermined at time of encoding.
* #121104 "Ambiguous sex"
* #121104 ^definition = Ambiguous sex.
* #121105 "Radiation Physicist"
* #121105 ^definition = Radiation Physicist.
* #121105 ^designation[0].language = #de-AT 
* #121105 ^designation[0].value = "Retired. Replaced by (C2985483, UMLS, 'Radiation Physicist')." 
* #121106 "Comment"
* #121106 ^definition = Comment.
* #121109 "Indications for Procedure"
* #121109 ^definition = Indications for Procedure
* #121109 ^designation[0].language = #de-AT 
* #121109 ^designation[0].value = "Retired. Replaced by (18785-6, LN, 'Indications for Procedure') " 
* #121110 "Patient Presentation"
* #121110 ^definition = Patient condition at the beginning of a healthcare encounter
* #121110 ^designation[0].language = #de-AT 
* #121110 ^designation[0].value = "Retired. Replaced by (55108-5, LN, 'Patient Presentation') " 
* #121111 "Summary"
* #121111 ^definition = Summary of a procedure, including most significant findings
* #121111 ^designation[0].language = #de-AT 
* #121111 ^designation[0].value = "Retired. Replaced by (55112-7, LN, 'Summary') " 
* #121112 "Source of Measurement"
* #121112 ^definition = Image or waveform used as source for measurement.
* #121113 "Complications"
* #121113 ^definition = Complications from a procedure
* #121113 ^designation[0].language = #de-AT 
* #121113 ^designation[0].value = "Retired. Replaced by (55109-3, LN, 'Complications') " 
* #121114 "Performing Physician"
* #121114 ^definition = Physician who performed a procedure.
* #121115 "Discharge Summary"
* #121115 ^definition = Summary of patient condition upon Discharge from a healthcare facility.
* #121116 "Proximal Finding Site"
* #121116 ^definition = Proximal Anatomic Location for a differential measurement; may be considered subtype of term (G-C0E3, SRT, "Finding Site"). E.g., distance or pressure gradient.
* #121117 "Distal Finding Site"
* #121117 ^definition = Distal Anatomic Location for a differential measurement; may be considered subtype of term (G-C0E3, SRT, "Finding Site"). E.g., distance or pressure gradient.
* #121118 "Patient Characteristics"
* #121118 ^definition = Patient Characteristics (findings).
* #121120 "Cath Lab Procedure Log"
* #121120 ^definition = Time-stamped record of events that occur during a catheterization procedure.
* #121121 "Room identification"
* #121121 ^definition = Room identification.
* #121122 "Equipment Identification"
* #121122 ^definition = Equipment identification.
* #121123 "Patient Status or Event"
* #121123 ^definition = A recorded Patient Status or an event involving a patient.
* #121124 "Procedure Action Item ID"
* #121124 ^definition = Identification of a step, action, or phase of a procedure.
* #121125 "DateTime of Recording of Log Entry"
* #121125 ^definition = DateTime of Recording of an Entry in an Event Log.
* #121126 "Performed Procedure Step SOP Instance UID"
* #121126 ^definition = SOP Instance UID of a Performed Procedure Step.
* #121127 "Performed Procedure Step SOP Class UID"
* #121127 ^definition = SOP Class UID of a Performed Procedure Step.
* #121128 "Procedure Action Duration"
* #121128 ^definition = Duration of a step, action, or phase of a procedure.
* #121130 "Start Procedure Action Item"
* #121130 ^definition = Beginning of a step, action, or phase of a procedure.
* #121131 "End Procedure Action Item"
* #121131 ^definition = End of a step, action, or phase of a procedure.
* #121132 "Suspend Procedure Action Item"
* #121132 ^definition = Suspension of a step, action, or phase of a procedure.
* #121133 "Resume Procedure Action Item"
* #121133 ^definition = Resumption of a step, action, or phase of a procedure.
* #121135 "Observation DateTime Qualifier"
* #121135 ^definition = Concept modifier for the DateTime of Recording of an Entry in an Event Log.
* #121136 "DateTime Unsynchronized"
* #121136 ^definition = Recorded DateTime had its source in a system clock not synchronized to other recorded DateTimes.
* #121137 "DateTime Estimated"
* #121137 ^definition = Recorded DateTime is estimated.
* #121138 "Image Acquired"
* #121138 ^definition = Event of the acquisition of an image.
* #121139 "Modality"
* #121139 ^definition = Type of data acquisition device.
* #121140 "Number of Frames"
* #121140 ^definition = Number of Frames in a multi-frame image.
* #121141 "Image Type"
* #121141 ^definition = Descriptor of an Image.
* #121142 "Acquisition Duration"
* #121142 ^definition = Duration of the acquisition of an image or a waveform.
* #121143 "Waveform Acquired"
* #121143 ^definition = Event of the acquisition of an image.
* #121144 "Document Title"
* #121144 ^definition = Document Title.
* #121145 "Description of Material"
* #121145 ^definition = Description of Material used in a procedure.
* #121146 "Quantity of Material"
* #121146 ^definition = Quantity of Material used in a procedure.
* #121147 "Billing Code"
* #121147 ^definition = Billing Code for materials used in a procedure.
* #121148 "Unit Serial Identifier"
* #121148 ^definition = Unit or Device Serial Identifier.
* #121149 "Lot Identifier"
* #121149 ^definition = Lot Identifier.
* #121150 "Device Code"
* #121150 ^definition = Vendor or local coded value identifying a device.
* #121151 "Lesion Identifier"
* #121151 ^definition = Identification of a Lesion observed during an imaging procedure.
* #121152 "Person administering drug/contrast"
* #121152 ^definition = Person administering drug/contrast.
* #121153 "Lesion Risk"
* #121153 ^definition = Assessment of the risk a coronary lesion presents to the health of a patient.
* #121154 "Intervention attempt identifier"
* #121154 ^definition = Identifier for an attempted Intervention.
* #121155 "Deployment"
* #121155 ^definition = Use of a device to deploy another device.
* #121156 "Percutaneous Entry Action"
* #121156 ^definition = Action of a clinical professional at the site of percutaneous access to a patient's cardiovascular system.
* #121157 "Begin Circulatory Support"
* #121157 ^definition = The action or event of beginning circulatory support for a patient.
* #121158 "End Circulatory Support"
* #121158 ^definition = The action or event of ending circulatory support for a patient.
* #121160 "Oxygen Administration Rate"
* #121160 ^definition = Rate of Oxygen Administration.
* #121161 "Begin Oxygen Administration"
* #121161 ^definition = The action or event of beginning administration of oxygen to a patient.
* #121162 "End oxygen administration"
* #121162 ^definition = The action or event of ending administration of oxygen to a patient.
* #121163 "By ventilator"
* #121163 ^definition = Method of administration of oxygen to a patient by ventilator.
* #121165 "Patient Assessment Performed"
* #121165 ^definition = The action or event of assessing the clinical status of a patient.
* #121166 "Begin Pacing"
* #121166 ^definition = The action or event of beginning pacing support for a patient.
* #121167 "End Pacing"
* #121167 ^definition = The action or event of ending pacing support for a patient.
* #121168 "Begin Ventilation"
* #121168 ^definition = The action or event of beginning ventilation support for a patient.
* #121169 "End Ventilation"
* #121169 ^definition = The action or event of ending ventilation support for a patient.
* #121171 "Tech Note"
* #121171 ^definition = Procedural note originated by a technologist.
* #121172 "Nursing Note"
* #121172 ^definition = Procedural note originated by a nurse.
* #121173 "Physician Note"
* #121173 ^definition = Procedural note originated by a Physician.
* #121174 "Procedure Note"
* #121174 ^definition = General procedural note.
* #121180 "Key Images"
* #121180 ^definition = List of references to images considered significant
* #121180 ^designation[0].language = #de-AT 
* #121180 ^designation[0].value = "Retired. Replaced by (55113-5, LN, 'Key Images') " 
* #121181 "DICOM Object Catalog"
* #121181 ^definition = List of references to DICOM SOP Instances.
* #121190 "Referenced Frames"
* #121190 ^definition = Individual frames selected as a subset of a multi-frame image.
* #121191 "Referenced Segment"
* #121191 ^definition = Segment selected as a subset of a segmentation image, specifically the pixels/voxels identified as belonging to the classification of the identified segment.
* #121192 "Device Subject"
* #121192 ^definition = A device is the subject of observations.
* #121193 "Device Subject Name"
* #121193 ^definition = Name or other identifier of a device that is the subject of observations.
* #121194 "Device Subject Manufacturer"
* #121194 ^definition = Manufacturer of a device that is the subject of observations.
* #121195 "Device Subject Model Name"
* #121195 ^definition = Model Name of a device that is the subject of observations.
* #121196 "Device Subject Serial Number"
* #121196 ^definition = Serial Number of a device that is the subject of observations.
* #121197 "Device Subject Physical Location during observation"
* #121197 ^definition = Physical Location of a device that is the subject of observations during those observations.
* #121198 "Device Subject UID"
* #121198 ^definition = Unique Identifier of a device that is the subject of observations.
* #121200 "Illustration of ROI"
* #121200 ^definition = Illustration of a region of interest.
* #121201 "Area Outline"
* #121201 ^designation[0].language = #de-AT 
* #121201 ^designation[0].value = "Retired. Replaced by (121056, DCM, 'Area Outline') ." 
* #121202 "Area of Defined Region"
* #121202 ^designation[0].language = #de-AT 
* #121202 ^designation[0].value = "Retired. Replaced by (G-A16A, SRT, 'Area of defined region')." 
* #121206 "Distance"
* #121206 ^definition = A one dimensional, or linear, numeric measurement.
* #121207 "Height"
* #121207 ^definition = Vertical measurement value.
* #121208 "Inter-Marker Distance"
* #121208 ^definition = Distance between marks on a device of calibrated size. E.g., a ruler.
* #121210 "Path"
* #121210 ^designation[0].language = #de-AT 
* #121210 ^designation[0].value = "Retired. Replaced by (121055, DCM, 'Path') ." 
* #121211 "Path length"
* #121211 ^definition = A one dimensional, or linear, numeric measurement along a polyline.
* #121213 "Perimeter Outline"
* #121213 ^designation[0].language = #de-AT 
* #121213 ^designation[0].value = "Retired. Replaced by (121057, DCM, 'Perimeter Outline')." 
* #121214 "Referenced Segmentation Frame"
* #121214 ^definition = Frame selected from a segmentation image, specifically the pixels/voxels identified as belonging to the classification of the segment encompassing the identified frame.
* #121216 "Volume estimated from single 2D region"
* #121216 ^definition = A three-dimensional numeric measurement that is approximate, based on a two-dimensional region in a single image.
* #121217 "Volume estimated from three or more non-coplanar 2D regions"
* #121217 ^definition = A three-dimensional numeric measurement that is approximate, based on three or more non-coplanar two-dimensional image regions.
* #121218 "Volume estimated from two non-coplanar 2D regions"
* #121218 ^definition = A three-dimensional numeric measurement that is approximate, based on two non-coplanar two-dimensional image regions.
* #121219 "Volume of bounding three dimensional region"
* #121219 ^definition = A three-dimensional numeric measurement of the bounding region of a three-dimensional region of interest in an image set.
* #121220 "Volume of circumscribed sphere"
* #121220 ^definition = A three-dimensional numeric measurement of the bounding sphere of a three-dimensional region of interest in an image set.
* #121221 "Volume of ellipsoid"
* #121221 ^definition = A three-dimensional numeric measurement of an ellipsoid shaped three-dimensional region of interest in an image set.
* #121222 "Volume of sphere"
* #121222 ^definition = A three-dimensional numeric measurement of a sphere shaped three-dimensional region of interest in an image set.
* #121230 "Path Vertex"
* #121230 ^definition = Coordinates of a point on a defined path.
* #121231 "Volume Surface"
* #121231 ^definition = Surface of an identified or measured volume.
* #121232 "Source series for segmentation"
* #121232 ^definition = Series of image instances used as source data for a segmentation.
* #121233 "Source image for segmentation"
* #121233 ^definition = Image instances used as source data for a segmentation.
* #121242 "Distance from nipple"
* #121242 ^definition = Indicates the location of the area of interest as measured from the nipple of the breast.
* #121243 "Distance from skin"
* #121243 ^definition = Indicates the location of the area of interest as measured from the most direct skin point of the breast.
* #121244 "Distance from chest wall"
* #121244 ^definition = Indicates the location of the area of interest as measured from the chest wall.
* #121290 "Patient exposure to ionizing radiation"
* #121290 ^definition = Patient exposure to ionizing radiation (procedure).
* #121291 "Results communicated"
* #121291 ^definition = The act of communicating actionable findings to a responsible receiver.
* #121301 "Simultaneous Doppler"
* #121301 ^definition = Reference is to a Doppler waveform acquired simultaneously with an image.
* #121302 "Simultaneous Hemodynamic"
* #121302 ^definition = Reference is to a Hemodynamic waveform acquired simultaneously with an image.
* #121303 "Simultaneous ECG"
* #121303 ^definition = Reference is to a ECG waveform acquired simultaneously with an image.
* #121304 "Simultaneous Voice Narrative"
* #121304 ^definition = Reference is to a voice narrative recording acquired simultaneously with an image.
* #121305 "Simultaneous Respiratory Waveform"
* #121305 ^definition = A waveform representing chest expansion and contraction due to respiratory activity, measured simultaneously with the acquisition of this Image.
* #121306 "Simultaneous Arterial Pulse Waveform"
* #121306 ^definition = Arterial pulse waveform obtained simultaneously with acquisition of a referencing image.
* #121307 "Simultaneous Phonocardiographic Waveform"
* #121307 ^definition = Phonocardiographic waveform obtained simultaneously with acquisition of a referencing image.
* #121311 "Localizer"
* #121311 ^definition = Image providing an anatomical reference on the patient under examination, for the purpose of defining the location of the ensuing imaging.
* #121312 "Biopsy localizer"
* #121312 ^definition = Image providing an anatomical reference on the patient under examination, for the purpose of planning or documenting a biopsy.
* #121313 "Other partial views"
* #121313 ^definition = Image providing a partial view of the target anatomy, when the target anatomy is too large for a single image.
* #121314 "Other image of biplane pair"
* #121314 ^definition = Image providing a view of the target anatomy in a different imaging plane, typically from a near perpendicular angle.
* #121315 "Other image of stereoscopic pair"
* #121315 ^definition = Image providing a view of the target anatomy in a different imaging plane, typically with a small angular difference.
* #121316 "Images related to standalone object"
* #121316 ^definition = Image related to a non-image information object.
* #121317 "Spectroscopy"
* #121317 ^definition = Image where signals are identified and separated according to their frequencies. E.g., to identify individual chemicals, or individual nuclei in a chemical compound.
* #121318 "Spectroscopy Data for Water Phase Correction"
* #121318 ^definition = MR spectroscopy data acquired to correct the phase of the diagnostic data for the phase signal of the Water.
* #121320 "Uncompressed predecessor"
* #121320 ^definition = An image that has not already been lossy compressed that is used as the source for creation of a lossy compressed image.
* #121321 "Mask image for image processing operation"
* #121321 ^definition = Image used as the mask for an image processing operation, such as subtraction.
* #121322 "Source image for image processing operation"
* #121322 ^definition = Image used as the source for an image processing operation.
* #121323 "Source series for image processing operation"
* #121323 ^definition = Series used as the source for an image processing operation.
* #121324 "Source Image"
* #121324 ^definition = Image used as the source for a derived or compressed image.
* #121325 "Lossy compressed image"
* #121325 ^definition = Image encoded with a lossy compression transfer syntax.
* #121326 "Alternate SOP Class instance"
* #121326 ^definition = SOP Instance encoded with a different SOP Class but otherwise equivalent data.
* #121327 "Full fidelity image"
* #121327 ^definition = Full fidelity image, uncompressed or lossless compressed.
* #121328 "Alternate Photometric Interpretation image"
* #121328 ^definition = Image encoded with a different photometric interpretation.
* #121329 "Source image for montage"
* #121329 ^definition = Image used as a source for a montage (stitched) image.
* #121330 "Lossy compressed predecessor"
* #121330 ^definition = An image that has previously been lossy compressed that is used as the source for creation of another lossy compressed image.
* #121331 "Equivalent CDA Document"
* #121331 ^definition = HL7 Document Architecture (CDA) Document that contains clinical content equivalent to the referencing Instance.
* #121332 "Complete Rendering for Presentation"
* #121332 ^definition = Instance that contains a displayable complete rendering of the referencing Instance.
* #121333 "Partial Rendering for Presentation"
* #121333 ^definition = Instance that contains a displayable partial rendering of the referencing Instance.
* #121334 "Extended Rendering for Presentation"
* #121334 ^definition = Instance that contains a displayable complete rendering of the referencing Instance, plus additional content such as inline rendering of referenced images.
* #121335 "Source Document"
* #121335 ^definition = Document whose content has been wholly or partially transformed to create the referencing document.
* #121338 "Anatomic image"
* #121338 ^definition = Image showing structural anatomic features.
* #121339 "Functional image"
* #121339 ^definition = Image showing physical or chemical activity.
* #121340 "Spectral filtered image"
* #121340 ^definition = Image providing the same view of the target anatomy acquired using only a specific imaging wavelength, frequency or energy.
* #121341 "Device localizer"
* #121341 ^definition = Image providing an anatomical reference on the patient under examination, for the purpose of documenting the location of device such as a diagnostic or therapeutic catheter.
* #121342 "Dose Image"
* #121342 ^definition = Image providing a graphic view of the distribution of radiation dose.
* #121346 "Acquisition frames corresponding to volume"
* #121346 ^definition = The referenced image is the source of spatially-related frames from which the referencing 3D volume data set was derived.
* #121347 "Volume corresponding to spatially-related acquisition frames"
* #121347 ^definition = 3D Volume containing the spatially-related frames in the referencing instance.
* #121348 "Temporal Predecessor"
* #121348 ^definition = Instance acquired prior to the referencing instance in a set of consecutively acquired instances.
* #121349 "Temporal Successor"
* #121349 ^definition = Instance acquired subsequent to the referencing instance in a set of consecutively acquired instances.
* #121350 "Same acquisition at lower resolution"
* #121350 ^definition = Image of the same target area at lower resolution acquired in the same acquisition process.
* #121351 "Same acquisition at higher resolution"
* #121351 ^definition = Image of the same target area at higher resolution acquired in the same acquisition process.
* #121352 "Same acquisition at different focal depth"
* #121352 ^definition = Image of the same target area at different focal depth (Z-plane) acquired in the same acquisition process.
* #121353 "Same acquisition at different spectral band"
* #121353 ^definition = Image of the same target area at different spectral band acquired in the same acquisition process.
* #121354 "Imaged container label"
* #121354 ^definition = Image specifically targeting the container label.
* #121358 "For Processing predecessor"
* #121358 ^definition = Source image from which FOR PRESENTATION images were created.
* #121360 "Replaced report"
* #121360 ^definition = The reference is to a predecessor report that has been replaced by the current report.
* #121361 "Addended report"
* #121361 ^definition = The reference is to a predecessor report to which the current report provides and addendum.
* #121362 "Preliminary report"
* #121362 ^definition = A report that precedes the final report and may contain only limited information; it may be time sensitive, and it is not expected to contain all the reportable findings.
* #121363 "Partial report"
* #121363 ^definition = A report that is not complete.
* #121370 "Composed from prior doses"
* #121370 ^definition = The dose object created was calculated by summation of existing, previously calculated, RT Dose instances.
* #121371 "Composed from prior doses and current plan"
* #121371 ^definition = The dose object created was calculated by summation of existing, previously calculated, RT Dose instances and dose newly calculated by the application. The newly calculated dose may or may not exist as an independent object.
* #121372 "Source dose for composing current dose"
* #121372 ^definition = RT Dose Instances used as source for calculated dose.
* #121380 "Active Ingredient Undiluted Concentration"
* #121380 ^definition = Concentration of the chemically or physically interesting (active) ingredient of a drug or contrast agent as delivered in product form from the manufacturer, typically in mg/ml.
* #121381 "Contrast/Bolus Ingredient Opaque"
* #121381 ^definition = X-Ray absorption of the active ingredient of a contrast agent ingredient is greater than the absorption of water (tissue).
* #121382 "Quantity administered"
* #121382 ^definition = Number of units of substance administered to a patient. E.g., tablets.
* #121383 "Mass administered"
* #121383 ^definition = Mass of substance administered to a patient.
* #121401 "Derivation"
* #121401 ^definition = Method of deriving or calculating a measured value. E.g., mean, or maximum of set.
* #121402 "Normality"
* #121402 ^definition = Assessment of a measurement relative to a normal range of values; may be considered subtype of term (G-C0F2, SRT, "has interpretation").
* #121403 "Level of Significance"
* #121403 ^definition = Significance of a measurement.
* #121404 "Selection Status"
* #121404 ^definition = Status of selection of a measurement for further processing or use.
* #121405 "Population description"
* #121405 ^definition = Description of a population of measurements.
* #121406 "Reference Authority"
* #121406 ^definition = Bibliographic or clinical reference for a Description of a population of measurements.
* #121407 "Normal Range description"
* #121407 ^definition = Description of a normal range of values for a measurement concept.
* #121408 "Normal Range Authority"
* #121408 ^definition = Bibliographic or clinical reference for a Description of a normal range of values.
* #121410 "User chosen value"
* #121410 ^definition = Observation value selected by user for further processing or use, or as most representative.
* #121411 "Most recent value chosen"
* #121411 ^definition = Observation value is the recently obtained, and has been selected for further processing or use.
* #121412 "Mean value chosen"
* #121412 ^definition = Observation value is the mean of several measurements, and has been selected for further processing or use.
* #121414 "Standard deviation of population"
* #121414 ^definition = Standard deviation of a measurement in a reference population.
* #121415 "Percentile Ranking of measurement"
* #121415 ^definition = Percentile Ranking of an observation value with respect a reference population.
* #121416 "Z-Score of measurement"
* #121416 ^definition = Z-score of an observation value with respect a reference population, expressed as the dimensionless quantity (x-m) /s, where (x-m) is the deviation of the observation value (x) from the population mean (m), and s is the standard deviation of the population.
* #121417 "2 Sigma deviation of population"
* #121417 ^definition = 2 Sigma deviation of a measurement in a reference population.
* #121420 "Equation"
* #121420 ^definition = Formula used to compute a derived measurement.
* #121421 "Equation Citation"
* #121421 ^definition = Bibliographic reference to a formula used to compute a derived measurement; reference may be to a specific equation in a journal article.
* #121422 "Table of Values Citation"
* #121422 ^definition = Bibliographic reference to a Table of Values used to look up a derived measurement.
* #121423 "Method Citation"
* #121423 ^definition = Bibliographic reference to a method used to compute a derived measurement.
* #121424 "Table of Values"
* #121424 ^definition = A Table of Values used to look up a derived measurement.
* #121425 "Index"
* #121425 ^definition = Factor (divisor or multiplicand) for normalizing a measurement. E.g., body surface area used for normalizing hemodynamic measurements.
* #121427 "Estimated"
* #121427 ^definition = Measurement obtained by observer estimation, rather than with a measurement tool or by calculation
* #121427 ^designation[0].language = #de-AT 
* #121427 ^designation[0].value = "Retired. Replaced by (R-10260, SRT, 'Estimated')" 
* #121428 "Calculated"
* #121428 ^definition = Measurement obtained by calculation
* #121428 ^designation[0].language = #de-AT 
* #121428 ^designation[0].value = "Retired. Replaced by (R-41D2D, SRT, 'Calculated')" 
* #121430 "Concern"
* #121430 ^definition = Identified issue about a state or process that has the potential to require intervention or management.
* #121431 "DateTime Concern Noted"
* #121431 ^definition = DateTime concern noted (noted by whom is determined by context of use).
* #121432 "DateTime Concern Resolved"
* #121432 ^definition = DateTime the concern was resolved.
* #121433 "DateTime Problem Resolved"
* #121433 ^definition = DateTime the problem was resolved.
* #121434 "Service Delivery Location"
* #121434 ^definition = Place at which healthcare services may be provided.
* #121435 "Service Performer"
* #121435 ^definition = Identification of a healthcare provider who performed a healthcare service; may be either a person or an organization.
* #121436 "Medical Device Used"
* #121436 ^definition = Type or identifier of a medical device used on, in, or by a patient.
* #121437 "Pharmacologic and exercise stress test"
* #121437 ^definition = Cardiac stress test using pharmacologic and exercise stressors
* #121437 ^designation[0].language = #de-AT 
* #121437 ^designation[0].value = "Retired. Replaced by (P2-31011, SRT, 'Pharmacologic and exercise stress test')" 
* #121438 "Paced stress test"
* #121438 ^definition = Cardiac stress test using an implanted or external cardiac pacing device
* #121438 ^designation[0].language = #de-AT 
* #121438 ^designation[0].value = "Retired. Replaced by (P2-3110B, SRT, 'Stress test using cardiac pacing')" 
* #121439 "Correction of congenital cardiovascular deformity"
* #121439 ^definition = Procedure for correction of congenital cardiovascular deformity
* #121439 ^designation[0].language = #de-AT 
* #121439 ^designation[0].value = "Retired." 
* #121701 "RT Patient Setup"
* #121701 ^definition = Process of placing patient in the anticipated treatment position, including specification and location of positioning aids, and other treatment delivery accessories.
* #121702 "RT Patient Position Acquisition, single plane MV"
* #121702 ^definition = Acquisition of patient positioning information prior to treatment delivery, using single-plane megavoltage imaging.
* #121703 "RT Patient Position Acquisition, dual plane MV"
* #121703 ^definition = Acquisition of patient positioning information prior to treatment delivery, using dual-plane megavoltage imaging.
* #121704 "RT Patient Position Acquisition, single plane kV"
* #121704 ^definition = Acquisition of patient positioning information prior to treatment delivery, using single-plane kilovoltage imaging.
* #121705 "RT Patient Position Acquisition, dual plane kV"
* #121705 ^definition = Acquisition of patient positioning information prior to treatment delivery, using dual-plane kilovoltage imaging.
* #121706 "RT Patient Position Acquisition, dual plane kV/MV"
* #121706 ^definition = Acquisition of patient positioning information prior to treatment delivery, using dual-plane combination kilovoltage and megavoltage imaging.
* #121707 "RT Patient Position Acquisition, CT kV"
* #121707 ^definition = Acquisition of patient positioning information prior to treatment delivery, using kilovoltage CT imaging.
* #121708 "RT Patient Position Acquisition, CT MV"
* #121708 ^definition = Acquisition of patient positioning information prior to treatment delivery, using megavoltage CT imaging.
* #121709 "RT Patient Position Acquisition, Optical"
* #121709 ^definition = Acquisition of patient positioning information prior to treatment delivery, using optical imaging.
* #121710 "RT Patient Position Acquisition, Ultrasound"
* #121710 ^definition = Acquisition of patient positioning information prior to treatment delivery, using ultrasound imaging.
* #121711 "RT Patient Position Acquisition, Spatial Fiducials"
* #121711 ^definition = Acquisition of patient positioning information prior to treatment delivery, using spatial fiducials.
* #121712 "RT Patient Position Registration, single plane"
* #121712 ^definition = Registration of intended and actual patient position prior to treatment delivery, using single-plane images.
* #121713 "RT Patient Position Registration, dual plane"
* #121713 ^definition = Registration of intended and actual patient position prior to treatment delivery, using dual-plane images.
* #121714 "RT Patient Position Registration, 3D CT general"
* #121714 ^definition = Registration of intended and actual patient position prior to treatment delivery, using 3D CT images and an unspecified registration approach.
* #121715 "RT Patient Position Registration, 3D CT marker-based"
* #121715 ^definition = Registration of intended and actual patient position prior to treatment delivery, using 3D CT images and a marker-based registration approach.
* #121716 "RT Patient Position Registration, 3D CT volume-based"
* #121716 ^definition = Registration of intended and actual patient position prior to treatment delivery, using 3D CT images and a volume-based registration approach.
* #121717 "RT Patient Position Registration, 3D on 2D reference"
* #121717 ^definition = Registration of intended and actual patient position prior to treatment delivery, using 3D verification images and 2D reference images.
* #121718 "RT Patient Position Registration, 2D on 3D reference"
* #121718 ^definition = Registration of intended and actual patient position prior to treatment delivery, using 2D verification images and 3D reference images.
* #121719 "RT Patient Position Registration, Optical"
* #121719 ^definition = Registration of intended and actual patient position prior to treatment delivery, using optical images.
* #121720 "RT Patient Position Registration, Ultrasound"
* #121720 ^definition = Registration of intended and actual patient position prior to treatment delivery, using ultrasound images.
* #121721 "RT Patient Position Registration, Spatial Fiducials"
* #121721 ^definition = Registration of intended and actual patient position prior to treatment delivery, using spatial fiducials.
* #121722 "RT Patient Position Adjustment"
* #121722 ^definition = Adjustment of patient position such that the patient is correctly positioned for treatment.
* #121723 "RT Patient Position In-treatment-session Review"
* #121723 ^definition = Review of patient positioning information in the process of delivering a treatment session.
* #121724 "RT Treatment Simulation with Internal Verification"
* #121724 ^definition = Simulated radiotherapy treatment delivery using verification integral to the Treatment Delivery System.
* #121725 "RT Treatment Simulation with External Verification"
* #121725 ^definition = Simulated radiotherapy treatment delivery using verification by a external Machine Parameter Verifier.
* #121726 "RT Treatment with Internal Verification"
* #121726 ^definition = Radiotherapy treatment delivery using verification integral to the Treatment Delivery System.
* #121727 "RT Treatment with External Verification"
* #121727 ^definition = Radiotherapy treatment delivery using verification by a external Machine Parameter Verifier.
* #121728 "RT Treatment QA with Internal Verification"
* #121728 ^definition = Quality assurance of a radiotherapy treatment delivery using verification integral to the Treatment Delivery System.
* #121729 "RT Treatment QA with External Verification"
* #121729 ^definition = Quality assurance of a radiotherapy treatment delivery using verification by a external Machine Parameter Verifier.
* #121730 "RT Machine QA"
* #121730 ^definition = Quality assurance of a Treatment Delivery Device.
* #121731 "RT Treatment QA by RT Plan Dose Check"
* #121731 ^definition = Perform Quality Assurance on an RT Plan by evaluating dosimetric content of the current RT Plan.
* #121732 "RT Treatment QA by RT Plan Difference Check"
* #121732 ^definition = Perform Quality Assurance on an RT Plan by comparing the content of previously quality-assessed RT Plans with the current RT Plan.
* #121733 "RT Treatment QA by RT Ion Plan Dose Check"
* #121733 ^definition = Perform Quality Assurance on an RT Ion Plan by evaluating dosimetric content of the current RT Ion Plan.
* #121734 "RT Treatment QA with RT Ion Plan Difference Check"
* #121734 ^definition = Perform Quality Assurance on an RT Ion Plan by comparing the content of previously quality-assessed RT Ion Plans by the current RT Ion Plan.
* #121740 "Treatment Delivery Type"
* #121740 ^definition = Indicates whether the treatment to be delivered is a complete fraction or a continuation of previous incompletely treated fraction.
* #122001 "Patient called to procedure room"
* #122001 ^definition = Patient called to procedure room.
* #122002 "Patient admitted to procedure room"
* #122002 ^definition = Patient admitted to procedure room.
* #122003 "Patient given pre-procedure instruction"
* #122003 ^definition = Patient given pre-procedure instruction.
* #122004 "Patient informed consent given"
* #122004 ^definition = Patient informed consent given.
* #122005 "Patient advance directive given"
* #122005 ^definition = Patient advance directive given.
* #122006 "Nil Per Os (NPO) status confirmed"
* #122006 ^definition = Nil Per Os (NPO) status confirmed.
* #122007 "Patient assisted to table"
* #122007 ^definition = Patient assisted to table.
* #122008 "Patient prepped and draped"
* #122008 ^definition = Patient prepped and draped.
* #122009 "Patient connected to continuous monitoring"
* #122009 ^definition = Patient connected to continuous monitoring.
* #122010 "Patient transferred to holding area"
* #122010 ^definition = Patient transferred to holding area.
* #122011 "Patient transferred to surgery"
* #122011 ^definition = Patient transferred to surgery.
* #122012 "Patient transferred to CCU"
* #122012 ^definition = Patient transferred to CCU.
* #122020 "Patient disoriented"
* #122020 ^definition = Patient disoriented.
* #122021 "Patient reports nausea"
* #122021 ^definition = Patient reports nausea.
* #122022 "Patient reports discomfort"
* #122022 ^definition = Patient reports discomfort.
* #122023 "Patient reports chest pain"
* #122023 ^definition = Patient reports chest pain.
* #122024 "Patient reports no pain"
* #122024 ^definition = Patient reports no pain.
* #122025 "Patient alert"
* #122025 ^definition = Patient alert.
* #122026 "Patient restless"
* #122026 ^definition = Patient restless.
* #122027 "Patient sedated"
* #122027 ^definition = Patient sedated.
* #122028 "Patient asleep"
* #122028 ^definition = Patient asleep.
* #122029 "Patient unresponsive"
* #122029 ^definition = Patient unresponsive.
* #122030 "Patient has respiratory difficulty"
* #122030 ^definition = Patient has respiratory difficulty.
* #122031 "Patient coughed"
* #122031 ^definition = Patient coughed.
* #122032 "Patient disconnected from continuous monitoring"
* #122032 ^definition = Patient disconnected from continuous monitoring.
* #122033 "Hemostasis achieved"
* #122033 ^definition = Hemostasis achieved.
* #122034 "Hemostasis not achieved - oozing"
* #122034 ^definition = Hemostasis not achieved - oozing.
* #122035 "Hemostasis not achieved - actively bleeding"
* #122035 ^definition = Hemostasis not achieved - actively bleeding.
* #122036 "Patient given post-procedure instruction"
* #122036 ^definition = Patient given post-procedure instruction.
* #122037 "Patient discharged from department"
* #122037 ^definition = Patient discharged from department or laboratory.
* #122038 "Patient pronounced dead"
* #122038 ^definition = Patient pronounced dead.
* #122039 "Patient transferred to morgue"
* #122039 ^definition = Patient transferred to morgue.
* #122041 "Personnel Arrived"
* #122041 ^definition = Identified personnel or staff arrived in procedure room.
* #122042 "Personnel Departed"
* #122042 ^definition = Identified personnel or staff departed procedure room.
* #122043 "Page Sent To"
* #122043 ^definition = Page sent to identified personnel or staff.
* #122044 "Consultation With"
* #122044 ^definition = Consultation with identified personnel or staff.
* #122045 "Office called"
* #122045 ^definition = Office of identified personnel or staff was called.
* #122046 "Equipment failure"
* #122046 ^definition = Equipment failure
* #122046 ^designation[0].language = #de-AT 
* #122046 ^designation[0].value = "Retired. Replaced by (110501, DCM, 'Equipment failure') " 
* #122047 "Equipment brought to procedure room"
* #122047 ^definition = Equipment brought to procedure room.
* #122048 "Equipment ready"
* #122048 ^definition = Equipment ready for procedure.
* #122049 "Equipment removed"
* #122049 ^definition = Equipment removed from procedure room.
* #122052 "Bioptome"
* #122052 ^definition = Device for obtaining biopsy sample.
* #122053 "Valvular Intervention"
* #122053 ^definition = Valvular Intervention.
* #122054 "Aortic Intervention"
* #122054 ^definition = Aortic Intervention.
* #122055 "Septal Defect Intervention"
* #122055 ^definition = Septal Defect Intervention.
* #122056 "Vascular Intervention"
* #122056 ^definition = Vascular Intervention.
* #122057 "Myocardial biopsy"
* #122057 ^definition = Myocardial biopsy.
* #122058 "Arterial conduit angiography"
* #122058 ^definition = Arterial conduit angiography.
* #122059 "Single plane Angiography"
* #122059 ^definition = Single plane Angiography.
* #122060 "Bi-plane Angiography"
* #122060 ^definition = Bi-plane Angiography.
* #122061 "Percutaneous Coronary Intervention"
* #122061 ^definition = Percutaneous Coronary Intervention.
* #122062 "15-Lead ECG"
* #122062 ^definition = 15-Lead electrocardiography
* #122062 ^designation[0].language = #de-AT 
* #122062 ^designation[0].value = "Retired. Replaced by (P2-3120E, SRT, '15-Lead ECG')" 
* #122072 "Pre-procedure log"
* #122072 ^definition = Log of events occurring prior to the current procedure.
* #122073 "Current procedure evidence"
* #122073 ^definition = Analysis or measurements for current procedure (purpose of reference to evidence document).
* #122075 "Prior report for current patient"
* #122075 ^definition = Prior report for current patient.
* #122076 "Consumable taken from inventory"
* #122076 ^definition = Identifier of Consumable taken from inventory.
* #122077 "Consumable returned to inventory"
* #122077 ^definition = Identifier of Consumable returned to inventory.
* #122078 "Remaining consumable disposed"
* #122078 ^definition = Identifier of consumable whose remaining content has been disposed.
* #122079 "Consumable unusable"
* #122079 ^definition = Identifier of Consumable determined to be unusable.
* #122081 "Drug start"
* #122081 ^definition = Identifier of Drug whose administration has started.
* #122082 "Drug end"
* #122082 ^definition = Identifier of Drug whose administration has ended.
* #122083 "Drug administered"
* #122083 ^definition = Identifier of Drug administered as part of procedure.
* #122084 "Contrast start"
* #122084 ^definition = Identifier of Contrast agent whose administration has started.
* #122085 "Contrast end"
* #122085 ^definition = Identifier of Contrast agent whose administration has ended.
* #122086 "Contrast administered"
* #122086 ^definition = Identifier of Contrast agent administered.
* #122087 "Infusate start"
* #122087 ^definition = Identifier of Infusate whose administration has started.
* #122088 "Infusate end"
* #122088 ^definition = Identifier of Infusate whose administration has ended.
* #122089 "Device crossed lesion"
* #122089 ^definition = Action of a device traversing a vascular lesion.
* #122090 "Intervention Action"
* #122090 ^definition = Action of a clinical professional performed on a patient for therapeutic purpose.
* #122091 "Volume administered"
* #122091 ^definition = Volume of Drug, Contrast agent, or Infusate administered.
* #122092 "Undiluted dose administered"
* #122092 ^definition = Undiluted dose of Drug, Contrast agent, or Infusate administered.
* #122093 "Concentration"
* #122093 ^definition = Concentration of Drug, Contrast agent, or Infusate administered.
* #122094 "Rate of administration"
* #122094 ^definition = Rate of Drug, Contrast agent, or Infusate administration.
* #122095 "Duration of administration"
* #122095 ^definition = Duration of Drug, Contrast agent, or Infusate administration.
* #122096 "Volume unadministered or discarded"
* #122096 ^definition = Volume of Drug, Contrast agent, or Infusate unadministered or discarded.
* #122097 "Catheter Curve"
* #122097 ^definition = Numeric parameter of Curvature of Catheter.
* #122098 "Transmit Frequency"
* #122098 ^definition = Transmit Frequency.
* #122099 "ST change from baseline"
* #122099 ^definition = Measured change of patient electrocardiographic ST level relative to baseline measurement.
* #122101 "Aneurysm on cited vessel"
* #122101 ^definition = Anatomic term modifier indicating aneurysm on cited vessel is the subject of the finding.
* #122102 "Graft to cited segment, proximal section"
* #122102 ^definition = Anatomic term modifier indicating proximal section of graft to cited vessel is the subject of the finding.
* #122103 "Graft to cited segment, mid section"
* #122103 ^definition = Anatomic term modifier indicating mid section of graft to cited vessel is the subject of the finding.
* #122104 "Graft to cited segment, distal section"
* #122104 ^definition = Anatomic term modifier indicating distal section of graft to cited vessel is the subject of the finding.
* #122105 "DateTime of Intervention"
* #122105 ^definition = DateTime of Intervention.
* #122106 "Duration of Intervention"
* #122106 ^definition = Duration of Intervention.
* #122107 "Baseline Stenosis Measurement"
* #122107 ^definition = Lesion stenosis measured prior to any interventional procedure
* #122107 ^designation[0].language = #de-AT 
* #122107 ^designation[0].value = "Retired. Replaced by (R-101BB, SRT, 'Lumen Diameter Stenosis'), post-coordinated with (G-7293, SRT, 'Baseline Phase')" 
* #122108 "Post-Intervention Stenosis Measurement"
* #122108 ^definition = Lesion stenosis measured after an interventional procedure
* #122108 ^designation[0].language = #de-AT 
* #122108 ^designation[0].value = "Retired. Replaced by (R-101BB, SRT, 'Lumen Diameter Stenosis'), post-coordinated with (G-7298, SRT, 'Post-intervention Phase')" 
* #122109 "Baseline TIMI Flow"
* #122109 ^definition = Assessment of perfusion across a coronary lesion measured prior to any interventional procedure.
* #122110 "Post-Intervention TIMI Flow"
* #122110 ^definition = Assessment of perfusion across a coronary lesion measured after an interventional procedure.
* #122111 "Primary Intervention Device"
* #122111 ^definition = Indication that device is the primary (first and/or most significant) device used for interventional therapy of a particular pathology. E.g., lesion.
* #122112 "Normal Myocardium"
* #122112 ^definition = Normal Myocardium.
* #122113 "Sacrred Myocardial"
* #122113 ^definition = Sacrred Myocardial.
* #122114 "Thinning Myocardium"
* #122114 ^definition = Thinning Myocardium.
* #122120 "Hemodynamics Report"
* #122120 ^definition = Hemodynamics Report.
* #122121 "Atrial pressure measurements"
* #122121 ^definition = Atrial pressure measurements, report section.
* #122122 "Ventricular pressure measurements"
* #122122 ^definition = Ventricular pressure measurements, report section.
* #122123 "Gradient assessment"
* #122123 ^definition = Gradient assessment, report section.
* #122124 "Blood velocity measurements"
* #122124 ^definition = Blood velocity measurements, report section.
* #122125 "Blood lab measurements"
* #122125 ^definition = Blood lab measurements, report section.
* #122126 "Derived Hemodynamic Measurements"
* #122126 ^definition = Derived Hemodynamic Measurements, report section.
* #122127 "Clinical Context"
* #122127 ^definition = Clinical Context, report section.
* #122128 "Patient Transferred From"
* #122128 ^definition = Location from which the patient was transferred.
* #122129 "PCI during this procedure"
* #122129 ^definition = Indication that the procedure includes a percutaneous coronary intervention.
* #122130 "Dose Area Product"
* #122130 ^definition = Radiation dose times area of exposure.
* #122131 "Degree of Thrombus"
* #122131 ^definition = Finding of probability and/or severity of thrombus.
* #122132 "Severity of Calcification"
* #122132 ^definition = Severity of Calcification, property of lesion.
* #122133 "Lesion Morphology"
* #122133 ^definition = Lesion Morphology; form and/or structural properties of lesion.
* #122134 "Vessel Morphology"
* #122134 ^definition = Vessel Morphology; form and/or structural properties of vessel.
* #122138 "Circulatory Support"
* #122138 ^definition = Technique (device or procedure) of support for patient circulatory system; hemodynamic support.
* #122139 "Reason for Exam"
* #122139 ^definition = Reason for Exam.
* #122140 "Comparison with Prior Exam Done"
* #122140 ^definition = Indication that the current exam data has been compared with prior exam data.
* #122141 "Electrode Placement"
* #122141 ^definition = Electrocardiographic electrode placement technique.
* #122142 "Acquisition Device Type"
* #122142 ^definition = Acquisition Device Type.
* #122143 "Acquisition Device ID"
* #122143 ^definition = Acquisition Device ID.
* #122144 "Quantitative Analysis"
* #122144 ^definition = Quantitative Analysis, report section.
* #122145 "Qualitative Analysis"
* #122145 ^definition = Qualitative Analysis, report section.
* #122146 "Procedure DateTime"
* #122146 ^definition = The date and time on which a procedure was performed on a patient.
* #122147 "Clinical Interpretation"
* #122147 ^definition = Clinical Interpretation, report section.
* #122148 "Lead ID"
* #122148 ^definition = ECG Lead Identifier.
* #122149 "Beat Number"
* #122149 ^definition = Beat Number; ordinal of cardiac cycle within an acquisition.
* #122150 "Compound Statement"
* #122150 ^definition = Complex coded semantic unit, consisting of several coded components.
* #122151 "Trend"
* #122151 ^definition = Trend (temporal progression) of a clinical condition, finding, or disease.
* #122152 "Statement"
* #122152 ^definition = Coded semantic unit.
* #122153 "Statement Modifier"
* #122153 ^definition = Coded modifier for a semantic unit.
* #122154 "Conjunctive Term"
* #122154 ^definition = Conjunctive term between semantic units.
* #122157 "Probability"
* #122157 ^definition = Probability.
* #122158 "ECG Global Measurements"
* #122158 ^definition = ECG Global Measurements, report section.
* #122159 "ECG Lead Measurements"
* #122159 ^definition = ECG Lead Measurements, report section.
* #122160 "Derived Area, Non-Valve"
* #122160 ^definition = Derived cross-sectional area of a vessel or anatomic feature, other than a cardiac valve.
* #122161 "Pulmonary Flow"
* #122161 ^definition = Rate of blood flow through Pulmonary artery.
* #122162 "Systemic Flow"
* #122162 ^definition = Rate of blood flow through the aorta.
* #122163 "Discharge DateTime"
* #122163 ^definition = DateTime of patient discharge from hospital admission.
* #122164 "Coronary Artery Bypass During This Admission"
* #122164 ^definition = Indication that a Coronary Artery Bypass operation was performed during the current hospital admission.
* #122165 "Date of Death"
* #122165 ^definition = Date of Death.
* #122166 "Death During This Admission"
* #122166 ^definition = Indication that the patient died during the current hospital admission.
* #122167 "Death During Catheterization"
* #122167 ^definition = Indication that the patient died during the current Catheterization procedure.
* #122170 "Type of Myocardial Infarction"
* #122170 ^definition = Finding of type of Myocardial Infarction.
* #122171 "Coronary lesion > = 50% stenosis"
* #122171 ^definition = Finding of Coronary lesion with greater than 50% stenosis.
* #122172 "Acute MI Present"
* #122172 ^definition = Finding of Acute Myocardial Infarction Presence as indication for interventional procedure.
* #122173 "ST Elevation Onset DateTime"
* #122173 ^definition = DateTime of first determination of elevated ECG ST segment, as indication of Myocardial Infarction.
* #122175 "Number of lesion interventions attempted"
* #122175 ^definition = Number of lesion interventions attempted during current procedure.
* #122176 "Number of lesion interventions successful"
* #122176 ^definition = Number of lesion interventions successful during current procedure, where the residual post intervention stenosis is less than or equal to 50% of the arterial luminal diameter, TIMI Flow is 3 and the minimal decrease in stenosis was 20%.
* #122177 "Procedure Result"
* #122177 ^definition = Overall success of interventional procedure.
* #122178 "Lesion Intervention Information"
* #122178 ^definition = Lesion Intervention Information, report section.
* #122179 "Peri-procedural MI occurred"
* #122179 ^definition = Indication that Myocardial Infarction occurred during current procedure.
* #122180 "CK-MB baseline"
* #122180 ^definition = Creatine Kinase-MB value at baseline (start of procedure).
* #122181 "CK-MB peak"
* #122181 ^definition = Creatine Kinase-MB highest value measured during procedure.
* #122182 "R-R interval"
* #122182 ^definition = Time interval between ECG R-wave peaks in subsequent cardiac cycles.
* #122183 "Blood temperature"
* #122183 ^definition = Blood temperature.
* #122185 "Blood Oxygen content"
* #122185 ^definition = Blood Oxygen content.
* #122187 "Blood Carbon dioxide saturation"
* #122187 ^definition = Blood Carbon dioxide saturation.
* #122188 "Pulmonary Arterial Content (FCpa)"
* #122188 ^definition = Pulmonary Arterial Content (FCpa).
* #122189 "Pulmonary Venous Content (FCpv)"
* #122189 ^definition = Pulmonary Venous Content (FCpv).
* #122190 "Max dp/dt/P"
* #122190 ^definition = Max dp/dt/P.
* #122191 "Ventricular End Diastolic pressure"
* #122191 ^definition = Ventricular End Diastolic pressure.
* #122192 "Indicator appearance time"
* #122192 ^definition = Elapsed time from injection of an indicator bolus until it is observed at another location.
* #122193 "Maximum pressure acceleration"
* #122193 ^definition = Maximum pressure acceleration.
* #122194 "Ventricular Systolic blood pressure"
* #122194 ^definition = Ventricular Systolic blood pressure.
* #122195 "Pulse Strength"
* #122195 ^definition = Pulse Strength; palpable strength of systolic flow.
* #122196 "C wave pressure"
* #122196 ^definition = The secondary peak pressure in the atrium during atrial contraction.
* #122197 "Gradient pressure, average"
* #122197 ^definition = Gradient pressure, average.
* #122198 "Gradient pressure, peak"
* #122198 ^definition = Gradient pressure, peak.
* #122199 "Pressure at dp/dt max"
* #122199 ^definition = Pressure at dp/dt max.
* #122201 "Diastolic blood velocity, mean"
* #122201 ^definition = Diastolic blood velocity, mean.
* #122202 "Diastolic blood velocity, peak"
* #122202 ^definition = Diastolic blood velocity, peak.
* #122203 "Systolic blood velocity, mean"
* #122203 ^definition = Systolic blood velocity, mean.
* #122204 "Systolic blood velocity, peak"
* #122204 ^definition = Systolic blood velocity, peak.
* #122205 "Blood velocity, mean"
* #122205 ^definition = Blood velocity, mean.
* #122206 "Blood velocity, minimum"
* #122206 ^definition = Blood velocity, minimum.
* #122207 "Blood velocity, peak"
* #122207 ^definition = Blood velocity, peak.
* #122208 "x-descent pressure"
* #122208 ^definition = Venous or atrial pressure minimum during ventricular systole, after A-wave.
* #122209 "y-descent pressure"
* #122209 ^definition = Venous or atrial pressure minimum when tricuspid valve opens during diastole, after V-wave.
* #122210 "z-point pressure"
* #122210 ^definition = Atrial pressure upon closure of tricuspid and mitral valves.
* #122211 "Left Ventricular ejection time"
* #122211 ^definition = Left Ventricular ejection time.
* #122212 "Left Ventricular filling time"
* #122212 ^definition = Left Ventricular filling time.
* #122213 "Right Ventricular ejection time"
* #122213 ^definition = Right Ventricular ejection time.
* #122214 "Right Ventricular filling time"
* #122214 ^definition = Right Ventricular filling time.
* #122215 "Total Pulmonary Resistance"
* #122215 ^definition = Total Pulmonary Resistance.
* #122216 "Total Vascular Resistance"
* #122216 ^definition = Total Vascular Resistance.
* #122217 "Coronary Flow reserve"
* #122217 ^definition = Coronary Flow reserve.
* #122218 "Diastolic/Systolic velocity ratio"
* #122218 ^definition = Diastolic/Systolic velocity ratio.
* #122219 "Hyperemic ratio"
* #122219 ^definition = Hyperemic ratio.
* #122220 "Hemodynamic Resistance Index"
* #122220 ^definition = Hemodynamic Resistance Index.
* #122221 "Thorax diameter, sagittal"
* #122221 ^definition = Thorax diameter, sagittal.
* #122222 "Procedure Environmental Characteristics"
* #122222 ^definition = Environmental characteristics in the procedure room.
* #122223 "Room oxygen concentration"
* #122223 ^definition = Oxygen concentration in the procedure room.
* #122224 "Room temperature"
* #122224 ^definition = Temperature in the procedure room.
* #122225 "Room Barometric pressure"
* #122225 ^definition = Barometric pressure in the procedure room.
* #122227 "Left to Right Flow"
* #122227 ^definition = Left to Right Flow.
* #122228 "Right to Left Flow"
* #122228 ^definition = Right to Left Flow.
* #122229 "Arteriovenous difference"
* #122229 ^definition = Arteriovenous oxygen content difference.
* #122230 "10 Year CHD Risk"
* #122230 ^definition = Framingham Study 10 Year CHD Risk.
* #122231 "Comparative Average10 Year CHD Risk"
* #122231 ^definition = Framingham Study Comparative Average10 Year CHD Risk.
* #122232 "Comparative Low10 Year CHD Risk"
* #122232 ^definition = Framingham Study Comparative Low10 Year CHD Risk.
* #122233 "LDL Cholesterol Score Sheet for Men"
* #122233 ^definition = Framingham Study LDL Cholesterol Score Sheet for Men.
* #122234 "LDL Cholesterol Score Sheet for Women"
* #122234 ^definition = Framingham Study LDL Cholesterol Score Sheet for Women.
* #122235 "Total Cholesterol Score Sheet for Men"
* #122235 ^definition = Framingham Study Total Cholesterol Score Sheet for Men.
* #122236 "Total Cholesterol Score Sheet for Women"
* #122236 ^definition = Framingham Study Total Cholesterol Score Sheet for Women.
* #122237 "Corrected Sinus Node Recovery Time"
* #122237 ^definition = Corrected Sinus Node Recovery Time.
* #122238 "Max volume normalized to 50mmHg pulse pressure"
* #122238 ^definition = Max volume normalized to 50mmHg pulse pressure.
* #122239 "Oxygen Consumption"
* #122239 ^definition = Oxygen Consumption.
* #122240 "BSA = 3.207*WT^(0.7285-0.0188 log (WT)) *HT^0.3"
* #122240 ^definition = Body Surface Area computed from patient height and weight: BSA = 3.207*WT[g]^(0.7285-0.0188 log (WT[g])) *HT[cm] ^ 0.3 [Boyd E, The growth of the surface area of the human body. Minneapolis: University of Minnesota Press, 1935, eq. (36) ].
* #122241 "BSA = 0.007184*WT^ 0.425*HT^0.725"
* #122241 ^definition = Body Surface Area computed from patient height and weight: BSA = 0.007184* WT[kg] ^ 0.425*HT[cm] ^ 0.725 [Dubois and Dubois, Arch Int Med 1916 17:863-71].
* #122242 "BSA = 0.0235*WT^0.51456*HT^ 0.42246"
* #122242 ^definition = Body Surface Area computed from patient height and weight: BSA = 0.0235* WT[kg] ^0.51456*HT[cm]^ 0.42246
* #122243 "BSA = 0.024265*WT^0.5378*HT^0.3964"
* #122243 ^definition = Body Surface Area computed from patient height and weight: BSA = 0.024265 * WT[kg] ^ 0.5378 * HT[cm] ^ 0.3964
* #122244 "BSA = (HT * WT/36) ^0.5"
* #122244 ^definition = Body Surface Area computed from patient height and weight: BSA = (HT[m] * WT[kg] / 36) ^ 0.5
* #122245 "BSA = 1321+0.3433*WT"
* #122245 ^definition = Body Surface Area computed from patient weight:BSA = 1321 + 0.3433 * WT[kg] (for pediatrics 3-30 kg) [Current, J.D. 'A Linear Equation For Estimating The Body Surface Area In Infants And Children', The Internet Journal of Anesthesiology. 1998. 2:2].
* #122246 "BSA = 0.0004688 * WT ^ (0.8168 - 0.0154 * log(WT))"
* #122246 ^definition = BSA = 0.0004688 * (1000 * WT) ^ (0.8168 - 0.0154 * log(1000 * WT))
* #122247 "VO2male = BSA (138.1 - 11.49 * loge(age) + 0.378 * HRf)"
* #122247 ^definition = Equation for estimated oxygen consumption: VO2male = BSA (138.1 - 11.49 * loge(age) + 0.378 * HRf).
* #122248 "VO2female = BSA (138.1 - 17.04 * loge(age) + 0.378 * HRf)"
* #122248 ^definition = Equation for estimated oxygen consumption: VO2female = BSA (138.1 - 17.04 * loge(age) + 0.378 * HRf).
* #122249 "VO2 = VeSTPD * 10 * (FIO2 - FE02)"
* #122249 ^definition = Equation for estimated oxygen consumption: VO2 = VeSTPD * 10 * (FIO2 - FE02).
* #122250 "VO2 = 152 * BSA"
* #122250 ^definition = Equation for estimated oxygen consumption: VO2 = 152 * BSA.
* #122251 "VO2 = 175 * BSA"
* #122251 ^definition = Equation for estimated oxygen consumption: VO2 = 175 * BSA.
* #122252 "VO2 = 176 * BSA"
* #122252 ^definition = Equation for estimated oxygen consumption: VO2 = 176 * BSA.
* #122253 "Robertson & Reid table"
* #122253 ^definition = Robertson & Reid Table for estimated oxygen consumption.
* #122254 "Fleisch table"
* #122254 ^definition = Fleisch table for estimated oxygen consumption.
* #122255 "Boothby table"
* #122255 ^definition = Boothby table for estimated oxygen consumption.
* #122256 "if (prem age< 3days) P50 = 19.9"
* #122256 ^definition = Estimate of Oxygen partial pressure at 50% saturation for premature infants less than 3 days old: P50 = 19.9.
* #122257 "if (age < 1day) P50 = 21.6"
* #122257 ^definition = Estimate of Oxygen partial pressure at 50% saturation for infants less than 1 day old: P50 = 21.6.
* #122258 "if (age < 30day) P50 = 24.6"
* #122258 ^definition = Estimate of Oxygen partial pressure at 50% saturation for infants less than 30 days old: P50 = 24.6.
* #122259 "if (age < 18y) P50 = 27.2"
* #122259 ^definition = Estimate of Oxygen partial pressure at 50% saturation for patients less than 18 years old: P50 = 27.2.
* #122260 "if (age < 40y) P50 = 27.4"
* #122260 ^definition = Estimate of Oxygen partial pressure at 50% saturation for patients less than 40 years old: P50 = 27.4.
* #122261 "if (age > 60y) P50 = 29.3"
* #122261 ^definition = Estimate of Oxygen partial pressure at 50% saturation for patients more than 60 years old: P50 = 29.3.
* #122262 "Area = Flow / 44.5 * sqrt(Gradient[mmHg])"
* #122262 ^definition = Cardiac valve area computed from flow and pressure gradient: Area = Flow / 44.5 * sqrt(Gradient[mmHg]) [Gorlin and Gorlin, Am Heart J, 1951].
* #122263 "MVA = Flow / 38.0 * sqrt(Gradient[mmHg])"
* #122263 ^definition = Mitral valve area computed from flow and pressure gradient: Mitral valve Area = Flow / 38.0 * sqrt(Gradient[mmHg]) [Gorlin and Gorlin, Am Heart J, 1951].
* #122265 "BMI = Wt / Ht ^ 2"
* #122265 ^definition = Body Mass Index computed from weight and height: BMI = Wt/Ht^2.
* #122266 "BSA = 0.007358 * WT ^ 0.425 * HT ^ 0.725"
* #122266 ^definition = Body Surface Area computed from patient height and weight: BSA = 0.007358 * WT[kg] ^ 0.425 * HT[cm] ^ 0.725
* #122267 "BSA = 0.010265 * WT ^ 0.423 * HT ^ 0.651"
* #122267 ^definition = Body Surface Area computed from patient height and weight: BSA = 0.010265 * WT[kg] ^ 0.423 * HT[cm] ^ 0.651 (For East Asian child aged 12-14 years).
* #122268 "BSA = 0.008883 * WT ^ 0.444 * HT ^ 0.663"
* #122268 ^definition = Body Surface Area computed from patient height and weight: BSA = 0.008883* WT[kg] ^ 0.444 * HT[cm] ^ 0.663 (For East Asian child aged 6-11 years).
* #122269 "BSA = 0.038189 * WT ^ 0.423 * HT ^ 0.362"
* #122269 ^definition = Body Surface Area computed from patient height and weight: BSA = 0.038189 * WT[kg] ^ 0.423 * HT[cm] ^ 0.362 (For East Asian child aged 1-5 years).
* #122270 "BSA = 0.009568 * WT ^ 0.473 * HT ^ 0.655"
* #122270 ^definition = Body Surface Area computed from patient height and weight: BSA = 0.009568* WT[kg] ^ 0.473 * HT[cm] ^ 0.655 (For East Asian child aged 0-12 months).
* #122271 "Skin Condition Warm"
* #122271 ^definition = Skin Condition Warm.
* #122272 "Skin Condition Cool"
* #122272 ^definition = Skin Condition Cool.
* #122273 "Skin Condition Cold"
* #122273 ^definition = Skin Condition Cold.
* #122274 "Skin Condition Dry"
* #122274 ^definition = Skin Condition Dry.
* #122275 "Skin Condition Clammy"
* #122275 ^definition = Skin Condition Clammy.
* #122276 "Skin Condition Diaphoretic"
* #122276 ^definition = Skin Condition Diaphoretic.
* #122277 "Skin Condition Flush"
* #122277 ^definition = Skin Condition Flush.
* #122278 "Skin Condition Mottled"
* #122278 ^definition = Skin Condition Mottled.
* #122279 "Skin Condition Pale"
* #122279 ^definition = Skin Condition Pale.
* #122281 "Airway unobstructed"
* #122281 ^definition = Airway unobstructed.
* #122282 "Airway partially obstructed"
* #122282 ^definition = Airway partially obstructed.
* #122283 "Airway severely obstructed"
* #122283 ^definition = Airway severely obstructed.
* #122288 "Not Visualized"
* #122288 ^definition = Anatomy could not be visualized for the purpose of evaluation.
* #122291 "Quantitative Arteriography Report"
* #122291 ^definition = Quantitative Arteriography Report.
* #122292 "Quantitative Ventriculography Report"
* #122292 ^definition = Quantitative Ventriculography Report.
* #122301 "Guidewire crossing lesion unsuccessful"
* #122301 ^definition = Guidewire crossing lesion unsuccessful.
* #122302 "Guidewire crossing lesion successful"
* #122302 ^definition = Guidewire crossing lesion successful.
* #122303 "Angioplasty balloon inflated"
* #122303 ^definition = Angioplasty balloon inflated.
* #122304 "Angioplasty balloon deflated"
* #122304 ^definition = Angioplasty balloon deflated.
* #122305 "Device deployed"
* #122305 ^definition = Device deployed.
* #122306 "Stent re-expanded"
* #122306 ^definition = Stent re-expanded.
* #122307 "Object removed"
* #122307 ^definition = Object removed.
* #122308 "Radiation applied"
* #122308 ^definition = Radiation applied.
* #122309 "Radiation removed"
* #122309 ^definition = Radiation removed.
* #122310 "Interventional device placement unsuccessful"
* #122310 ^definition = Interventional device placement unsuccessful.
* #122311 "Interventional device placed"
* #122311 ^definition = Interventional device placed.
* #122312 "Intervention performed"
* #122312 ^definition = Intervention performed.
* #122313 "Interventional device withdrawn"
* #122313 ^definition = Interventional device withdrawn.
* #122319 "Catheter Size"
* #122319 ^definition = Catheter Size.
* #122320 "Injectate Temperature"
* #122320 ^definition = Injectate Temperature.
* #122321 "Injectate Volume"
* #122321 ^definition = Injectate Volume.
* #122322 "Calibration Factor"
* #122322 ^definition = Factor by which a measured or calculated value is multiplied to obtain the estimated real-world value.
* #122325 "IVUS Report"
* #122325 ^definition = Intravascular Ultrasound Report.
* #122330 "EEM Diameter"
* #122330 ^definition = External Elastic Membrane (EEM) diameter measured through the center point of the vessel. Center point of the vessel is defined as the center of gravity of the EEM area. The EEM is a discrete interface at the border between the media and the adventitia.
* #122331 "Plaque Plus Media Thickness"
* #122331 ^definition = The distance from intimal leading edge to the external elastic membrane along any line passing through the luminal center, which is defined as the center of gravity of the lumen area.
* #122332 "Lumen Perimeter"
* #122332 ^definition = Planimetered perimeter of the lumen.
* #122333 "EEM Cross-Sectional Area"
* #122333 ^definition = Vessel area measured at the External Elastic Membrane (EEM), a discrete interface at the border between the media and the adventitia.
* #122334 "Plaque plus Media Cross-Sectional Area"
* #122334 ^definition = Area within the EEM occupied by atheroma, regardless of lumen compromise. Plaque plus Media Area = EEM cross-sectional area - vessel lumen cross-sectional area.
* #122335 "In-Stent Neointimal Cross-Sectional Area"
* #122335 ^definition = Measurement of in-stent restenosis. In-Stent Intimal Area = Stent cross-sectional area - vessel lumen cross-sectional area.
* #122336 "Vascular Volume measurement length"
* #122336 ^definition = Longitudinal extent of the Vascular Volume Measurement. This is the distance from the distal edge to the proximal edge of the Volume measurement.
* #122337 "Relative position"
* #122337 ^definition = Longitudinal distance from the closest edge of a fiducial feature or reference location to the start of the vascular measurement. This value will be a positive if the measurement is distal to the fiducial feature or reference location, or negative if the measurement is proximal to the fiducial feature or reference location.
* #122339 "Stent Volume Obstruction"
* #122339 ^definition = In-Stent Neointimal Volume / Stent Volume.
* #122340 "Fiducial feature"
* #122340 ^definition = Reference, normally anatomical, which is used for locating the position of a measurement.
* #122341 "Calcium Length"
* #122341 ^definition = Longitudinal calcium length measurement.
* #122343 "Lumen Eccentricity Index"
* #122343 ^definition = Measurement of vessel lumen eccentricity. Lumen Eccentricity Index = (maximum vessel lumen diameter - minimum vessel lumen diameter) / maximum vessel lumen diameter. Lumen diameters are measured through the center point of the lumen, which is defined as the center of gravity of the lumen area.
* #122344 "Plaque plus Media Eccentricity Index"
* #122344 ^definition = Plaque plus Media Eccentricity Index = (maximum Plaque plus media thickness - minimum Plaque plus media thickness) / maximum Plaque plus media thickness.
* #122345 "Remodeling Index"
* #122345 ^definition = Measurement of increase or decrease in EEM area that occurs during the development of atherosclerosis. Remodeling Index = Lesion EEM area / reference EEM area.
* #122346 "Stent Symmetry Index"
* #122346 ^definition = Measurement of stent circularity. Stent Symmetry Index = (maximum stent diameter - minimum stent diameter) / maximum stent diameter.
* #122347 "Stent Expansion Index"
* #122347 ^definition = Measurement of stent area relative to the reference lumen area. Stent Expansion Index = Minimum stent area / reference vessel lumen cross-sectional area.
* #122348 "Lumen Shape Index"
* #122348 ^definition = Measurement of vessel lumen eccentricity. Lumen Shape Index = (2p * sqrt(Vessel lumen cross-sectional area / p) / Lumen Perimeter) 2
* #122350 "Lumen Diameter Ratio"
* #122350 ^definition = Lumen diameter ratio = minimum vessel lumen diameter / maximum vessel lumen diameter, measured at the same cross section in the vessel. Lumen diameters are measured through the center point of the lumen, which is defined as the center of gravity of the lumen area.
* #122351 "Stent Diameter Ratio"
* #122351 ^definition = Stent diameter ratio = Minimum stent diameter / Maximum stent diameter, measured at the same cross section in the vessel. Stent diameters are measured through the center point of the stent, which is defined as the center of gravity of the stent area.
* #122352 "EEM Diameter Ratio"
* #122352 ^definition = EEM diameter ratio = minimum EEM diameter / maximum EEM diameter. Measured at the same cross section in the vessel.
* #122354 "Plaque Burden"
* #122354 ^definition = Fractional area within the External Elastic Membrane (EEM) occupied by atheroma. Plaque Burden = (EEM area - vessel lumen cross-sectional area) / EEM area.
* #122355 "Arc of Calcium"
* #122355 ^definition = Angular measurement of a Calcium deposit with the apex located at the center of the lumen, which is defined as the center of gravity of the lumen area.
* #122356 "Soft plaque"
* #122356 ^definition = Plaque characterized by low density or echogenicity.
* #122357 "In-Stent Neointima"
* #122357 ^definition = Abnormal thickening of the intima within the stented segment.
* #122360 "True Lumen"
* #122360 ^definition = Lumen surrounded by all three layers of the vessel-intima, media, and adventitia.
* #122361 "False Lumen"
* #122361 ^definition = A channel, usually parallel to the true lumen, which does not communicate with the true lumen over a portion of its length.
* #122363 "Plaque Rupture"
* #122363 ^definition = Plaque ulceration with a tear detected in a fibrous cap.
* #122364 "Stent Gap"
* #122364 ^definition = Length of gap between two consecutive stents.
* #122367 "T-1 Worst"
* #122367 ^definition = Worst stenosis - the stenosis with the smallest lumen size within a vessel segment.
* #122368 "T-2 Secondary"
* #122368 ^definition = 2nd most severe stenosis within a vessel segment.
* #122369 "T-3 Secondary"
* #122369 ^definition = 3rd most severe stenosis within a vessel segment.
* #122370 "T-4 Secondary"
* #122370 ^definition = 4th most severe stenosis within a vessel segment.
* #122371 "EEM Volume"
* #122371 ^definition = External Elastic Membrane (EEM) volume measured within a specified region. The EEM is a discrete interface at the border between the media and the Adventitia.
* #122372 "Lumen Volume"
* #122372 ^definition = Lumen volume measured within a specified region.
* #122374 "In-Stent Neointimal Volume"
* #122374 ^definition = The amount of plaque between the lumen and stent, within the stent region; In-stent restenosis. In-Stent Neointimal Volume = Stent Volume - Lumen Volume.
* #122375 "Native Plaque Volume"
* #122375 ^definition = The amount of plaque between the stent and the EEM, within the stent region. Native Plaque Volume = EEM Volume - Stent Volume.
* #122376 "Total Plaque Volume"
* #122376 ^definition = Total amount of plaque between the EEM and the Lumen, over the entire region that is measured. Total Plaque Volume = EEM Volume - Lumen Volume.
* #122380 "Proximal Reference"
* #122380 ^definition = Proximal reference segment measurement site. Typically the site with the largest lumen proximal to a stenosis but within the same segment (usually within 10 mm of the stenosis with no major intervening branches).
* #122381 "Distal Reference"
* #122381 ^definition = Distal reference segment measurement site. Typically the site with the largest lumen distal to a stenosis but within the same segment (usually within 10 mm of the stenosis with no major intervening branches).
* #122382 "Site of Lumen Minimum"
* #122382 ^definition = Site of the smallest lumen in a vessel. E.g., due to a stenotic lesion.
* #122383 "Entire Pullback"
* #122383 ^definition = Measurement region that encompasses the entire vessel imaged in a single pullback acquisition.
* #122384 "Stented Region"
* #122384 ^definition = Measurement region occupied by the stent.
* #122385 "Proximal Stent Margin"
* #122385 ^definition = Region starting at the proximal edge of the Stent and extending several millimeters (usually 5 mm) proximal to the Stent edge.
* #122386 "Distal Stent Margin"
* #122386 ^definition = Region starting at the distal edge of the Stent and extending several millimeters (usually 5 mm) distal to the Stent edge.
* #122387 "Dissection Classification"
* #122387 ^definition = Classification of dissections in a vessel.
* #122388 "Intra-stent Dissection"
* #122388 ^definition = Separation of neointimal hyperplasia from stent struts, usually seen only after treatment of in-stent restenosis.
* #122389 "Vulnerable Plaque"
* #122389 ^definition = Plaque with a thin cap fibrous atheroma that is at increased risk of rupture and thrombosis (or re-thrombosis) and rapid stenosis progression.
* #122390 "Eroded Plaque"
* #122390 ^definition = Plaque erosions with no structural defect (beyond endothelial injury) or gap in the plaque.
* #122391 "Relative Stenosis Severity"
* #122391 ^definition = Stenosis severity classifications of multiple lesions in a vessel.
* #122393 "Restenotic Lesion"
* #122393 ^definition = A finding of a previously treated lesion in which stenosis has reoccurred.
* #122394 "Fibro-Lipidic Plaque"
* #122394 ^definition = Loosely packed bundles of collagen fibers with regions of lipid deposition present. Region is cellular and no cholesterol clefts or necrosis are present. Some
* #122395 "Necrotic-Lipidic Plaque"
* #122395 ^definition = Area within the plaque with very low echogenicity separated from the lumen and surrounded by more echogenic structures (fibrous cap). Highly lipidic necrotic region with remnants of foam cells and dead lymphocytes present. No collagen fibers are visible and mechanical integrity is poor. Cholesterol clefts and micro calcifications are visible.
* #122398 "Intimal Dissection"
* #122398 ^definition = Dissection limited to the intima or atheroma, and not extending to the media.
* #122399 "Medial Dissection"
* #122399 ^definition = Dissection in the arterial Media, extending into the media.
* #122400 "Simultaneously Acquired"
* #122400 ^definition = The referenced information was acquired simultaneously with the information in the object in which the reference occurs.
* #122401 "Same Anatomy"
* #122401 ^definition = Information acquired for the same anatomic region.
* #122402 "Same Indication"
* #122402 ^definition = Information acquired for the same indication. E.g., to elucidate the same diagnostic question.
* #122403 "For Attenuation Correction"
* #122403 ^definition = The referenced information was used to correct the data for differential attenuation through different anatomic tissue.
* #122404 "Reconstructed"
* #122404 ^definition = Value estimated for a vessel in the absence of a stenosis.
* #122405 "Algorithm Manufacturer"
* #122405 ^definition = Manufacturer of application used.
* #122406 "Left Atrial Ejection Fraction by Angiography"
* #122406 ^definition = Left Atrial Ejection Fraction by Angiography.
* #122407 "Left Atrial ED Volume"
* #122407 ^definition = Left Atrial End Diastolic Volume.
* #122408 "Left Atrial ES Volume"
* #122408 ^definition = Left Atrial End Systolic Volume.
* #122410 "Contour Realignment"
* #122410 ^definition = Contour repositioning of End Diastolic relative to End Systolic contour.
* #122411 "Threshold Value"
* #122411 ^definition = The minimum standard deviation to define the hypokinesis and hyperkinesis.
* #122417 "Regional Abnormal Wall Motion"
* #122417 ^definition = Report of differentiation of wall motion compared to normal.
* #122421 "Calibration Object"
* #122421 ^definition = Object used for Calibration.
* #122422 "Calibration Method"
* #122422 ^definition = Method used for Calibration.
* #122423 "Calibration Object Size"
* #122423 ^definition = Size of calibration object.
* #122428 "Area Length Method"
* #122428 ^definition = Method how long axis is positioned.
* #122429 "Volume Method"
* #122429 ^definition = Model for cardiac chamber volume calculation.
* #122430 "Reference Method"
* #122430 ^definition = Method to define original diameter of the artery.
* #122431 "Regression Slope ED"
* #122431 ^definition = Relation between calculated End Diastolic volume and ventricular End Diastolic volume. The specific meaning is dependent on volume method used.
* #122432 "Regression Offset ED"
* #122432 ^definition = Correction factor for the calculated End Diastolic volume and ventricular End Diastolic volume. The specific meaning is dependent on volume method used.
* #122433 "Regression Slope ES"
* #122433 ^definition = Relation between calculated End Systolic volume and ventricular End Systolic volume. The specific meaning is dependent on volume method used.
* #122434 "Regression Offset ES"
* #122434 ^definition = Correction factor for the calculated End Systolic volume and ventricular End Systolic volume. The specific meaning is dependent on volume method used.
* #122435 "Regression Volume Exponent"
* #122435 ^definition = Exponent of volume in regression formula.
* #122438 "Reference Points"
* #122438 ^definition = Container for spatial locations or coordinates used for calculation.
* #122445 "Wall Thickness"
* #122445 ^definition = Average thickness of the chamber wall in the current view.
* #122446 "Wall Volume"
* #122446 ^definition = Volume of the chamber wall estimated from the current view.
* #122447 "Wall Mass"
* #122447 ^definition = Mass of the chamber wall (myocardium).
* #122448 "Wall Stress"
* #122448 ^definition = Peak systolic stress of chamber wall.
* #122449 "Centerline Wall Motion Analysis"
* #122449 ^definition = Method to calculate wall motion [example: Sheehan, 1986].
* #122450 "Normalized Chord Length"
* #122450 ^definition = The length between End Diastolic and End Systolic contour perpendicular on the centerline normalized by a method dependent ventricular perimeter length. The centerline is the line equidistant between the End Diastolic and End Systolic contour [example: Sheehan, 1986].
* #122451 "Abnormal Region"
* #122451 ^definition = The report of the boundaries of the abnormal (hyperkinetic, hypokinetic, a-kinetic) regions associated with the territory of the artery [example: Sheehan, 1986].
* #122452 "First Chord of Abnormal Region"
* #122452 ^definition = The chord number specifying the begin of abnormal region [example: Sheehan, 1986].
* #122453 "Last Chord of Abnormal Region"
* #122453 ^definition = The chord number specifying the end of abnormal region [example: Sheehan, 1986].
* #122459 "Territory Region Severity"
* #122459 ^definition = Severity at the regional abnormality extent [example: Sheehan, 1986].
* #122461 "Opposite Region Severity"
* #122461 ^definition = Severity at the opposite regional abnormality extent [example: Sheehan, 1986].
* #122464 "LAD Region in RAO Projection"
* #122464 ^definition = Based on a total number of chords of 100 and RAO project the range of chords belonging to this circumferential extent lies between 5 - 85. [Sheehan, 1986].
* #122465 "RCA Region in ROA Projection"
* #122465 ^definition = Based on a total number of chords of 100 and RAO project the range of chords belonging to this circumferential extent lies between 25 - 85. [Sheehan, 1986].
* #122466 "Single LAD Region in RAO Projection"
* #122466 ^definition = Based on a total number of chords of 100 and RAO projection the range of chords belonging to this regional extent lies between 10 - 66 (hypokinetic) and 67 - 80 (hyperkinetic). [Sheehan, 1986].
* #122467 "Single RCA Region in RAO Projection"
* #122467 ^definition = Based on a total number of chords of 100 and RAO projection the range of chords belonging to this regional extent lies between 51 - 80 (hypokinetic) and 10 - 50 (hyperkinetic). [Sheehan, 1986].
* #122468 "Multiple LAD Region in RAO Projection"
* #122468 ^definition = Based on a total number of chords of 100 and RAO projection the range of chords belonging to this regional extent lies between 10 - 58 (hypokinetic) and 59 -80 (hyperkinetic). [Sheehan, 1986].
* #122469 "Multiple RCA Region in RAO Projection"
* #122469 ^definition = Based on a total number of chords of 100 and RAO projection the range of chords belonging to this regional extent lies between 59 - 80 (hypokinetic) and 10 - 58 (hyperkinetic). [Sheehan, 1986].
* #122470 "LAD Region in LAO Projection"
* #122470 ^definition = Based on a total number of chords of 100 and LAO projection the range of chords belonging to this regional extent lies between 50 -100 (hypokinetic) and 20 - 49 (hyperkinetic). [Sheehan, 1986].
* #122471 "RCA Region in LAO Projection"
* #122471 ^definition = Based on a total number of chords of 100 and LAO projection the range of chords belonging to this regional extent lies between 19 - 67 (hypokinetic) and 68 - 100 (hyperkinetic). [Sheehan, 1986].
* #122472 "CFX Region in LAO Projection"
* #122472 ^definition = Based on a total number of chords of 100 and LAO projection the range of chords belonging to this regional extent lies between 38 -74 (hypokinetic) and 75 - 100 (hyperkinetic). [Sheehan, 1986].
* #122473 "Circular Method"
* #122473 ^definition = Method based on assumption that the image object is circular.
* #122474 "Densitometric Method"
* #122474 ^definition = Method based on the gray value distribution of the image.
* #122475 "Center of Gravity"
* #122475 ^definition = End Systolic contour realigned to End Diastolic contour based on the center of gravity.
* #122476 "Long Axis Based"
* #122476 ^definition = End Systolic contour realigned to End Diastolic contour based on the mid point of the long axis. The long axis end points are defined as the posterior and apex.
* #122477 "No Realignment"
* #122477 ^definition = No Contour Realignment applied.
* #122480 "Vessel Lumen Cross-Sectional Area"
* #122480 ^definition = Calculated Vessel Lumen Cross-Sectional Area based on the referenced method.
* #122481 "Contour Start"
* #122481 ^definition = Location of the beginning of a contour.
* #122482 "Contour End"
* #122482 ^definition = Location of the end of a contour.
* #122485 "Sphere"
* #122485 ^definition = Sphere is used as calibration object.
* #122486 "Geometric Isocenter"
* #122486 ^definition = Object of interest in isocenter of image and pixel separation is calculated from geometric data.
* #122487 "Geometric Non-Isocenter"
* #122487 ^definition = Object of interest not in isocenter of image and pixel separation is calculated from geometric data and out of isocenter distances.
* #122488 "Calibration Object Used"
* #122488 ^definition = Object used for calibration. E.g., sphere.
* #122489 "Curve Fitted Reference"
* #122489 ^definition = Application dependent method to calculate the reference diameter based on the multiple diameter values.
* #122490 "Interpolated Local Reference"
* #122490 ^definition = Application dependent method to calculate reference by interpolation, based on the diameter of two or more user defined reference positions.
* #122491 "Mean Local Reference"
* #122491 ^definition = Application dependent method to calculate by averaging the reference, based on the diameter of one or more user defined reference positions.
* #122493 "Radial Based Wall Motion Analysis"
* #122493 ^definition = Method to calculate wall motion based on the lengths of the radials in the predefined regions [Ingels].
* #122495 "Regional Contribution to Ejection Fraction"
* #122495 ^definition = Contribution of Region to global Ejection factor based on radial or landmark based wall motion method.
* #122496 "Radial Shortening"
* #122496 ^definition = The reduction of area between End Systolic and End Diastolic based on radial wall motion analysis.
* #122497 "Landmark Based Wall Motion Analysis"
* #122497 ^definition = Method to calculate wall motion based on the move of landmarks on the wall [Slager].
* #122498 "Slice Contribution to Ejection Fraction"
* #122498 ^definition = Contribution to the ejection fraction of a specific slice region [Slager].
* #122499 "Frame to Frame Analysis"
* #122499 ^definition = Method to calculate volumes of heart chambers for every image in a range.
* #122501 "Area of closed irregular polygon"
* #122501 ^definition = The area is derived by considering a set of coordinates as a closed irregular polygon, accounting for inner angles.
* #122502 "Area of a closed NURBS"
* #122502 ^definition = The area is derived by using a set of coordinates as control points for a Non Uniform Rational B-Spline (NURBS).
* #122503 "Integration of sum of closed areas on contiguous slices"
* #122503 ^definition = The volume derived by integrating the sum of the areas on adjacent slices across the slice interval; each area is defined by a regular planar shape or by considering a set of coordinates as a closed irregular polygon, accounting for inner angles.
* #122505 "Calibration"
* #122505 ^definition = Procedure used to calibrate measurements or measurement devices.
* #122507 "Left Contour"
* #122507 ^definition = Left contour of lumen (direction proximal to distal).
* #122508 "Right Contour"
* #122508 ^definition = Right contour of lumen (direction proximal to distal).
* #122509 "Diameter Graph"
* #122509 ^definition = Ordered set of diameters values derived from contours (direction proximal to distal).
* #122510 "Length Luminal Segment"
* #122510 ^definition = Length Luminal Segment.
* #122511 "Graph Increment"
* #122511 ^definition = Increment value along X-axis in Diameter Graph.
* #122516 "Site of Maximum Luminal"
* #122516 ^definition = Location of the maximum lumen area in a lesion or vessel.
* #122517 "Densitometric Luminal Cross-sectional Area Graph"
* #122517 ^definition = Ordered set of cross-sectional Vessel Lumen Cross-Sectional Area values derived from contours (direction proximal to distal) based on densitometric method.
* #122528 "Position of Proximal Border"
* #122528 ^definition = Position of proximal border of segment relative to the contour start (proximal end of analysis area).
* #122529 "Position of Distal Border"
* #122529 ^definition = Position of distal border of segment relative to the contour start (proximal end of analysis area).
* #122542 "Plaque Area"
* #122542 ^definition = Longitudinal cross sectional area of plaque.
* #122544 "Diameter Symmetry"
* #122544 ^definition = Symmetry of stenosis (0 = complete asymmetry, 1 = complete symmetry); see Section T.2 Definition of Diameter Symmetry with Arterial Plaques in PS3.17 .
* #122545 "Area Symmetry"
* #122545 ^definition = Symmetry of plaque (0 = complete asymmetry, 1 = complete symmetry); see Section T.2 Definition of Diameter Symmetry with Arterial Plaques in PS3.17 .
* #122546 "Inflow Angle"
* #122546 ^definition = The average slope of the diameter function between the position of the minimum luminal diameter and the position of the proximal border of the segment.
* #122547 "Outflow Angle"
* #122547 ^definition = The average slope of the diameter function between the position of the minimum luminal diameter and the position of the distal border of the segment.
* #122548 "Stenotic Flow Reserve"
* #122548 ^definition = The relation between coronary pressure and coronary flow.
* #122549 "Poiseuille Resistance"
* #122549 ^definition = Poiseuille Resistance at the location of the stenosis.
* #122550 "Turbulence Resistance"
* #122550 ^definition = Turbulence Resistance at the location of the stenosis.
* #122551 "Pressure Drop at SFR"
* #122551 ^definition = Pressure drop over the stenosis at maximum heart output.
* #122554 "Segmentation Method"
* #122554 ^definition = Method for selection of vessel sub-segments.
* #122555 "Estimated Normal Flow"
* #122555 ^definition = Estimate of the volume of blood flow in the absence of stenosis.
* #122558 "Area Length Kennedy"
* #122558 ^definition = Area Length method defined by Kennedy [Kennedy, 1970].
* #122559 "Area Length Dodge"
* #122559 ^definition = Area Length method defined by Dodge [Dodge, 1960].
* #122560 "Area Length Wynne"
* #122560 ^definition = Area Length method defined by Wynne [Wynne].
* #122562 "Multiple Slices"
* #122562 ^definition = Volume method based on multiple slice.
* #122563 "Boak"
* #122563 ^definition = Volume method defined by Boak [Boak].
* #122564 "TS Pyramid"
* #122564 ^definition = Volume method defined by Ferlinz [Ferlinz].
* #122565 "Two Chamber"
* #122565 ^definition = Volume method defined by Graham [Graham].
* #122566 "Parallelepiped"
* #122566 ^definition = Volume method defined by Arcilla [Arcilla].
* #122572 "BSA^1.219"
* #122572 ^definition = Corrected Body Surface area for indexing the hemodynamic measurements for a pediatric patient.
* #122574 "Equidistant method"
* #122574 ^definition = Method for selecting sub-segments that are all of the same length.
* #122575 "User selected method"
* #122575 ^definition = Manually selected start and end of sub-segment.
* #122582 "Left ventricular posterobasal segment"
* #122582 ^definition = Left ventricular posterobasal segment.
* #122600 "Cardiovascular Analysis Report"
* #122600 ^definition = Report of a Cardiovascular Analysis, typically from a CT or MR study.
* #122601 "Ventricular Analysis"
* #122601 ^definition = Ventricular Analysis.
* #122602 "Myocardial Perfusion Analysis"
* #122602 ^definition = Myocardial Perfusion Analysis.
* #122603 "Calcium Scoring Analysis"
* #122603 ^definition = Calcium Scoring Analysis.
* #122604 "Flow Quantification"
* #122604 ^definition = Flow Quantification Analysis.
* #122605 "Vascular Morphological Analysis"
* #122605 ^definition = Vascular Morphological Analysis.
* #122606 "Vascular Functional Analysis"
* #122606 ^definition = Vascular Functional Analysis.
* #122607 "Thickening Analysis"
* #122607 ^definition = Analysis of myocardial wall thickening.
* #122608 "Absolute Values Of Ventricular Measurements"
* #122608 ^definition = Section Heading for absolute values of ventricular measurements.
* #122609 "Normalized Values Of Ventricular Measurements"
* #122609 ^definition = Results of normalizing ventricular measurements.
* #122611 "Reference Point"
* #122611 ^definition = Reference Point of a measurement.
* #122612 "Central breathing position"
* #122612 ^definition = Central breathing position between inspiration and expiration.
* #122616 "Peak Ejection Rate"
* #122616 ^definition = Peak of the ventricular ejection rate.
* #122617 "Peak Ejection Time"
* #122617 ^definition = Time of the peak of ventricular ejection.
* #122618 "Peak Filling Rate"
* #122618 ^definition = Peak of the fluid filling rate.
* #122619 "Peak Filling Time"
* #122619 ^definition = Time interval until time of peak filling from a given reference point. E.g., end systole.
* #122620 "Papillary Muscle Excluded"
* #122620 ^definition = Papillary muscle was excluded from the measurement.
* #122621 "Papillary Muscle Included"
* #122621 ^definition = Papillary muscle was included in the measurement.
* #122624 "Wall Thickness Ratio end-systolic to end-diastolic"
* #122624 ^definition = The ratio of the end-systolic wall thickness compared to the end-diastolic wall thickness.
* #122627 "Curve Fit Method"
* #122627 ^definition = The method to smooth a ventricular volume as a function of time.
* #122628 "Baseline Result Correction"
* #122628 ^definition = Baseline correction used in the calculation of the results.
* #122631 "Signal Earliest Peak Time"
* #122631 ^definition = The time in a dynamic set of images at which the first peak of the signal is observed for the analyzed myocardial wall segments.
* #122633 "Signal Increase Start Time"
* #122633 ^definition = This is the time at which the signal begins to increase.
* #122634 "Signal Time to Peak"
* #122634 ^definition = Time interval between the beginning of the signal increase to the time at which the signal intensity reaches its first maximum in a dynamic set of images.
* #122635 "MR Perfusion Peak"
* #122635 ^definition = Peak of the MR perfusion signal.
* #122636 "MR Perfusion Slope"
* #122636 ^definition = Signal intensity as a function of time. It is the change in the signal intensity divided by the change in the time.
* #122637 "MR Perfusion Time Integral"
* #122637 ^definition = MR perfusion time integral from baseline (foot time) to earliest peak.
* #122638 "Signal Baseline Start"
* #122638 ^definition = First time point in a dynamic set of images used in the calculation of the baseline signal intensity for each myocardial wall segment.
* #122639 "Signal Baseline End"
* #122639 ^definition = Last time point in a dynamic set of images used in the calculation of the baseline signal intensity for each myocardial wall segment.
* #122640 "Image Interval"
* #122640 ^definition = The time delta between images in a dynamic set of images.
* #122642 "Velocity Encoding Minimum Value"
* #122642 ^definition = The minimum velocity encoded by the phase encoding gradient.
* #122643 "Velocity Encoding Maximum Value"
* #122643 ^definition = The maximum velocity encoded by the phase encoding gradient.
* #122645 "Net Forward Volume"
* #122645 ^definition = Forward volume-reverse volume.
* #122650 "Area Based Method"
* #122650 ^definition = Area Based Method for estimating volume or area.
* #122651 "Diameter Based Method"
* #122651 ^definition = Diameter Based Method for estimating volume, area or diameter.
* #122652 "Volume Based Method"
* #122652 ^definition = Volume Based Method for estimating volume.
* #122655 "NASCET"
* #122655 ^definition = A method of diameter measurements according to NASCET (North American Symptomatic Carotid Endarterectomy Trial).
* #122656 "ECST"
* #122656 ^definition = A method of diameter measurements according to ECST (European Carotid Surgery Trial).
* #122657 "Agatston Score Threshold"
* #122657 ^definition = Agatston Score Threshold.
* #122658 "Calcium Mass Threshold"
* #122658 ^definition = Calcium Mass Threshold.
* #122659 "Calcium Scoring Calibration"
* #122659 ^definition = Calcium Scoring Calibration.
* #122660 "Calcium Volume"
* #122660 ^definition = Calcium Volume.
* #122661 "Calcium Mass"
* #122661 ^definition = Calcium Mass.
* #122664 "Late Contrast Enhancement"
* #122664 ^definition = Delayed hyperenhancement of a tissue observed in an image acquired after injection of contrast media.
* #122665 "Time interval since injection of contrast media"
* #122665 ^definition = Time interval since injection of contrast media.
* #122666 "Time relative to R-wave peak"
* #122666 ^definition = Time relative to R-wave peak.
* #122667 "Blood velocity vs. time of cardiac cycle"
* #122667 ^definition = Relationship between blood velocity and time relative to R-wave peak.
* #122668 "Time interval since detection of contrast bolus"
* #122668 ^definition = Time interval since detection of contrast bolus.
* #122670 "Papillary Muscle Included/Excluded"
* #122670 ^definition = Indicates if the papillary muscle was included or excluded in the measurement.
* #122675 "Anterior-Posterior"
* #122675 ^definition = Anterior to Posterior direction.
* #122680 "endoleak"
* #122680 ^definition = Persistent flow of blood into the stent-grafting.
* #122683 "Stent Fracture"
* #122683 ^definition = Fracture of a stent.
* #122684 "Stent Disintegration"
* #122684 ^definition = Disintegration of a stent.
* #122685 "Stent Composition"
* #122685 ^definition = Material that a stent consists of.
* #122686 "Parent Vessel Finding"
* #122686 ^definition = Finding about the characteristics of the parent vessel of a vessel.
* #122687 "Site of Lumen Maximum"
* #122687 ^definition = Site of Maximal lumen diameter of a vessel.
* #122698 "X-Concept"
* #122698 ^definition = The physical domain (time, space, etc.) to the horizontal axis of the graphical presentation.
* #122699 "Y-Concept"
* #122699 ^definition = The physical domain (time, space, etc.) to the vertical axis of the graphical presentation.
* #122700 "Indications for Pharmacological Stress"
* #122700 ^definition = Indications for Pharmacological Stress.
* #122701 "Procedure time base"
* #122701 ^definition = Reference time for measurement of elapsed time in a procedure.
* #122702 "Treadmill speed"
* #122702 ^definition = Treadmill speed.
* #122703 "Treadmill gradient"
* #122703 ^definition = Treadmill gradient.
* #122704 "Ergometer power"
* #122704 ^definition = Ergometer power.
* #122705 "Pharmacological Stress Agent Dose Rate"
* #122705 ^definition = Pharmacological Stress Agent Dose Rate.
* #122706 "Rating of Perceived Exertion"
* #122706 ^definition = Rating of Perceived Exertion.
* #122707 "Number of Ectopic Beats"
* #122707 ^definition = Number of ectopic beats during a period of collection.
* #122708 "Double Product"
* #122708 ^definition = Heart rate times systolic blood pressure.
* #122709 "Activity workload"
* #122709 ^definition = Physical activity workload (intensity) measurement.
* #122710 "Time since start of stage"
* #122710 ^definition = Elapsed time at stage.
* #122711 "Exercise duration after stress agent injection"
* #122711 ^definition = Exercise duration after stress agent injection.
* #122712 "Imaging Start Time"
* #122712 ^definition = Imaging Start Time.
* #122713 "Attenuation correction method"
* #122713 ^definition = Attenuation correction method.
* #122715 "Pharmacological Stress Agent Dose"
* #122715 ^definition = Pharmacological Stress Agent Dose.
* #122716 "Maximum Power Output Achieved"
* #122716 ^definition = Maximum power output achieved during course of procedure.
* #122717 "Peak activity workload"
* #122717 ^definition = Peak physical activity intensity measurement during course of procedure.
* #122718 "Peak Double Product"
* #122718 ^definition = Peak Double Product measurement during course of procedure.
* #122720 "OSEM algorithm"
* #122720 ^definition = Ordered subsets expectation maximization reconstruction algorithm.
* #122721 "Chang method"
* #122721 ^definition = Chang attenuation correction method.
* #122726 "Algorithmic attenuation correction"
* #122726 ^definition = Attenuation correction not based on image-based attenuation maps.
* #122727 "NM transmission attenuation correction"
* #122727 ^definition = NM transmission attenuation correction.
* #122728 "CT-based attenuation correction"
* #122728 ^definition = CT-based attenuation correction.
* #122729 "No Attenuation Correction"
* #122729 ^definition = No attenuation correction.
* #122730 "Bazett QTc Algorithm"
* #122730 ^definition = Bazett QT Correction Algorithm; QT/(RR ^ 0.5); Bazett HC. "An analysis of the time-relations of electrocardiograms" Heart7:353-370 (1920).
* #122731 "Hodges QTc Algorithm"
* #122731 ^definition = Hodges QT Correction Algorithm; QT + 1.75 (heart rate-60); Hodges M, Salerno Q, Erlien D. "Bazett's QT correction reviewed. Evidence that a linear QT correction for heart rate is better." J Am Coll Cardiol1:694 (1983).
* #122732 "Fridericia QTc Algorithm"
* #122732 ^definition = Fridericia QT Correction Algorithm; QT/(RR ^ 0.333); Fridericia LS. "The duration of systole in the electrocardiogram of normal subjects and of patients with heart disease" Acta Med Scand53:469-486 (1920).
* #122733 "Framingham QTc Algorithm"
* #122733 ^definition = Framingham QT Correction Algorithm; QT + 0.154 (1- RR); Sagie A, Larson MG, Goldberg RJ, et al. "An improved method for adjusting the QT interval for heart rate (the Framingham Heart Study)." Am J Cardiol70:797-801(1992).
* #122734 "Borg RPE Scale"
* #122734 ^definition = Borg Rating of Perceived Exertion Scale, range 6:20.
* #122735 "Borg CR10 Scale"
* #122735 ^definition = Borg category ratio scale, open ended range with nominal range 0:10.
* #122739 "Overall study quality"
* #122739 ^definition = Overall study quality.
* #122740 "Excellent image quality"
* #122740 ^definition = Excellent image quality.
* #122741 "Good image quality"
* #122741 ^definition = Good image quality.
* #122742 "Poor image quality"
* #122742 ^definition = Poor image quality.
* #122743 "Body habitus attenuation"
* #122743 ^definition = Image attenuation due to body physique (overweight).
* #122744 "Breast attenuation"
* #122744 ^definition = Image attenuation due to breast tissue.
* #122745 "Diaphragmatic attenuation"
* #122745 ^definition = Image attenuation due to diaphragm.
* #122748 "False positive defect finding"
* #122748 ^definition = Finding of a defect is incorrect. E.g., from automated analysis.
* #122750 "Non-diagnostic - low heart rate"
* #122750 ^definition = ECG is non-diagnostic due to low heart rate.
* #122751 "Non-diagnostic - resting ST abnormalities"
* #122751 ^definition = ECG is non-diagnostic due to resting ST abnormalities.
* #122752 "Non-diagnostic - ventricular pacing or LBBB"
* #122752 ^definition = ECG is non-diagnostic due to ventricular pacing or Left Bundle Branch Block.
* #122753 "Non-diagnostic ECG"
* #122753 ^definition = ECG is non-diagnostic for presence of acute coronary syndrome.
* #122755 "Strongly positive"
* #122755 ^definition = Strongly positive finding.
* #122756 "Strongly positive - ST elevation"
* #122756 ^definition = Strongly positive finding - ST elevation.
* #122757 "ST Depression - Horizontal"
* #122757 ^definition = Finding of ST segment depression with no slope.
* #122758 "ST Depression - Upsloping"
* #122758 ^definition = Finding of ST segment depression with upslope.
* #122759 "ST Depression - Downsloping"
* #122759 ^definition = Finding of ST segment depression with downslope.
* #122760 "Stress test score"
* #122760 ^definition = Stress test score.
* #122762 "Number of diseased vessel territories"
* #122762 ^definition = Number of diseased vessel territories.
* #122764 "Weight exceeds equipment limit"
* #122764 ^definition = Patient weight exceeds equipment limit.
* #122768 "Difference in Ejection Fraction"
* #122768 ^definition = Difference in Ejection Fraction.
* #122769 "Difference in ED LV Volume"
* #122769 ^definition = Difference in End Diastolic Left Ventricular Volume.
* #122770 "Ratio of achieved to predicted maximal oxygen consumption"
* #122770 ^definition = Ratio of achieved to predicted maximal oxygen consumption.
* #122771 "Ratio of achieved to predicted functional capacity"
* #122771 ^definition = Ratio of achieved to predicted functional capacity.
* #122772 "Aerobic index"
* #122772 ^definition = Workload (Watts) at target heart rate divided by body weight.
* #122773 "ST/HR Index"
* #122773 ^definition = ST depression at peak exercise divided by the exercise-induced increase in heart rate [Kligfield P, Ameisen O, Okin PM. "Heart rate adjustment of ST segment depression for improved detection of coronary artery disease." Circulation 1989;79:245-55.].
* #122775 "Agreement with prior findings"
* #122775 ^definition = Agreement with prior findings.
* #122776 "Disagreement with prior findings"
* #122776 ^definition = Disagreement with prior findings.
* #122781 "Rest thallium/stress technetium procedure"
* #122781 ^definition = Nuclear Medicine Rest thallium/stress technetium procedure.
* #122782 "Rest technetium/stress technetium 1 day procedure"
* #122782 ^definition = Nuclear Medicine Rest technetium/stress technetium 1 day procedure.
* #122783 "Rest technetium/stress technetium 2 day procedure"
* #122783 ^definition = Nuclear Medicine Rest technetium/stress technetium 2 day procedure.
* #122784 "Stress technetium/rest technetium 1 day procedure"
* #122784 ^definition = Nuclear Medicine Stress technetium/rest technetium 1 day procedure.
* #122785 "NM Myocardial Viability procedure"
* #122785 ^definition = Nuclear Medicine Myocardial Viability procedure.
* #122791 "PET Myocardial Perfusion, Rest only"
* #122791 ^definition = Positron Emission Tomography Perfusion Imaging procedure, rest only.
* #122792 "PET Myocardial Perfusion, Stress only"
* #122792 ^definition = Positron Emission Tomography Perfusion Imaging procedure, stress only.
* #122793 "PET Myocardial Perfusion, Rest and Stress"
* #122793 ^definition = Positron Emission Tomography Perfusion Imaging procedure, rest and stress.
* #122795 "PET Myocardial Viability, Rest only"
* #122795 ^definition = Positron Emission Tomography Myocardial Viability procedure, rest only.
* #122796 "PET Myocardial Viability, Stress only"
* #122796 ^definition = Positron Emission Tomography Myocardial Viability procedure, stress only.
* #122797 "PET Myocardial Viability, Rest and Stress"
* #122797 ^definition = Positron Emission Tomography Myocardial Viability procedure, rest and stress.
* #122799 "Anginal Equivalent"
* #122799 ^definition = Group of symptoms heralding angina pectoris that does not include chest pain (dyspnea, diaphoresis, profuse vomiting in a diabetic patient, or arm or jaw pain).
* #123001 "Radiopharmaceutical"
* #123001 ^definition = Active ingredient (molecular) used for radioactive tracing.
* #123001 ^designation[0].language = #de-AT 
* #123001 ^designation[0].value = "Retired.Replaced by (F-61FDB, SRT, 'Radiopharmaceutical agent')." 
* #123003 "Radiopharmaceutical Start Time"
* #123003 ^definition = Time of radiopharmaceutical administration to the patient for imaging purposes.
* #123004 "Radiopharmaceutical Stop Time"
* #123004 ^definition = Ending time of radiopharmaceutical administration to the patient for imaging purposes.
* #123005 "Radiopharmaceutical Volume"
* #123005 ^definition = Volume of radiopharmaceutical administered to the patient.
* #123006 "Radionuclide Total Dose"
* #123006 ^definition = Total amount of radionuclide administered to the patient at Radiopharmaceutical Start Time.
* #123007 "Radiopharmaceutical Specific Activity"
* #123007 ^definition = Activity per unit mass of the radiopharmaceutical at Radiopharmaceutical Start Time.
* #123009 "Radionuclide Syringe Counts"
* #123009 ^definition = Pre-injection syringe acquisition count rate.
* #123010 "Radionuclide Residual Syringe Counts"
* #123010 ^definition = Syringe acquisition count rate following patient injection.
* #123011 "Contrast/Bolus Agent"
* #123011 ^definition = Contrast or bolus agent.
* #123012 "Pre-Medication"
* #123012 ^definition = Medication to be administered at the beginning of the Scheduled Procedure Step.
* #123014 "Target Region"
* #123014 ^definition = Anatomic Region to be imaged.
* #123015 "Imaging Direction"
* #123015 ^definition = Direction of imaging (includes view, transducer orientation, patient orientation, and/or projection).
* #123016 "Imaging Conditions"
* #123016 ^definition = Imaging condition for refinement of protocol (includes secondary posture, instruction, X-Ray / electron beam energy or nuclide, and ultrasound modes), as used in JJ1017 v3.0.
* #123019 "Caudal 10 degree distal-cranioproximal oblique"
* #123019 ^definition = Caudal 10 degree distal-cranioproximal oblique radiographic projection, defined per Smallwood et al.
* #123101 "Neighborhood Analysis"
* #123101 ^definition = Surface processing utilizing predefined weighting factors (i.e., kernels) applied to different data values depending on their location relative to other data values within the data domain. Includes Low Pass, High Pass, Gaussian, Laplacian, etc.
* #123102 "Adaptive Filtering"
* #123102 ^definition = Surface processing applied non-uniformly utilizing a priori knowledge of the system and/or relative locations of the data values within the data domain. Example: Neighborhood analysis where weighting factors are modified continuously based on predefined criteria.
* #123103 "Edge Detection"
* #123103 ^definition = Surface processing through the exploitation of discontinuities in the data values within their domain. Includes Gradient filters.
* #123104 "Morphological Operations"
* #123104 ^definition = Surface processing based on the connectivity of values based on the shape or structure of the data values within their domain. Includes erode, dilate, etc.
* #123105 "Histogram Analysis"
* #123105 ^definition = Surface processing applied to the distribution of the data values. Includes thresholding, Bayesian Classification, etc.
* #123106 "Multi-Scale/Resolution Filtering"
* #123106 ^definition = Surface processing accomplished through varying the data domain size. Include deformable models.
* #123107 "Cluster Analysis"
* #123107 ^definition = Surface processing accomplished by combining data values based on their relative location within their domain or value distribution. Includes K- and C-means, Fuzzy Analysis, Watershed, Seed Growing, etc.
* #123108 "Multispectral Processing"
* #123108 ^definition = Surface processing accomplished through the weighted combination of multiple data sets. Includes Principle Component Analysis, linear and non-linear weighed combinations, etc.
* #123109 "Manual Processing"
* #123109 ^definition = Surface processing accomplished through human interaction. Region drawing.
* #123110 "Artificial Intelligence"
* #123110 ^definition = Surface processing using Artificial Intelligence techniques, such as Machine Learning, Neural Networks, etc.
* #123111 "Deformable Models"
* #123111 ^definition = Surface processing using Deformable Model techniques, such as Point Distribution Models, Level Sets, Simplex Meshes, etc.
* #125000 "OB-GYN Ultrasound Procedure Report"
* #125000 ^definition = Document Title of OB-GYN procedure report.
* #125001 "Fetal Biometry Ratios"
* #125001 ^definition = Report section for assessment of fetal growth using ratios and indexes.
* #125002 "Fetal Biometry"
* #125002 ^definition = Report section for assessment of fetal growth.
* #125003 "Fetal Long Bones"
* #125003 ^definition = Report section for assessment of fetal growth by long bone measurements.
* #125004 "Fetal Cranium"
* #125004 ^definition = Report section for assessment of fetal cranium growth.
* #125005 "Biometry Group"
* #125005 ^definition = Biometric assessment of.
* #125006 "Biophysical Profile"
* #125006 ^definition = Report section for assessment of biophysical observations that evaluate fetal well-being according to Manning, Antepartum Fetal Evaluation: Development of a Fetal Biophysical Profile Score, Am. J Obstet Gynecol, 1980;136:787.
* #125007 "Measurement Group"
* #125007 ^definition = A grouping of related measurements and calculations that share a common context.
* #125008 "Fetus Summary"
* #125008 ^definition = Report section for fetus specific procedure summary observations.
* #125009 "Early Gestation"
* #125009 ^definition = Report section for assessment of early gestation fetus.
* #125010 "Identifier"
* #125010 ^definition = A name to differentiate between multiple instances of some item.
* #125011 "Pelvis and Uterus"
* #125011 ^definition = Report section for assessment of pelvis and uterus.
* #125012 "Growth Percentile rank"
* #125012 ^definition = The rank of a measured growth indicator relative to a normal distribution expressed as a percentage.
* #125013 "Growth Z-score"
* #125013 ^definition = The rank of a measured growth indicator relative to a normal distribution expressed as the dimensionless quantity z = (x-m) /s where (x-m) is the deviation of the value x, from the distribution mean, m, and s is the standard deviation of the distribution.
* #125015 "Fetus Characteristics"
* #125015 ^definition = Fetus characteristics (findings section title).
* #125016 "Fetal Measurements"
* #125016 ^definition = Fetal Measurements (findings section title).
* #125021 "Frame of Reference Identity"
* #125021 ^definition = There is a defined equivalence between the Frame of Reference of the Registration SOP instance and the Frame of Reference of the referenced images.
* #125022 "Fiducial Alignment"
* #125022 ^definition = The registration is based on fiducials that represent patient or specimen features identified in each data set.
* #125023 "Acquisition Equipment Alignment"
* #125023 ^definition = Registration based on a-priori knowledge of the acquisition geometry. This is not an object registration as in fiducial registration. Rather, it specifies a known spatial relationship.
* #125024 "Image Content-based Alignment"
* #125024 ^definition = Computed registration based on global image information.
* #125025 "Visual Alignment"
* #125025 ^definition = Registration by visually guided manipulation.
* #125030 "Inter-Hemispheric Plane"
* #125030 ^definition = A plane fiducial that specifies the location of the plane separating the two hemispheres of the brain.
* #125031 "Right Hemisphere Most Anterior"
* #125031 ^definition = A point fiducial that specifies the location in the plane perpendicular to the Anterior- Posterior-Commissure axis and tangential to the anterior limit of the right brain hemisphere.
* #125032 "Right Hemisphere Most Posterior"
* #125032 ^definition = A point fiducial that specifies the location in the plane perpendicular to the Anterior- Posterior-Commissure axis and tangential to the posterior limit of the right brain hemisphere.
* #125033 "Right Hemisphere Most Superior"
* #125033 ^definition = A point fiducial that specifies the location in the plane perpendicular to the Anterior- Posterior-Commissure axis and tangential to the superior limit of the right brain hemisphere.
* #125034 "Right Hemisphere Most Inferior"
* #125034 ^definition = A point fiducial that specifies the location in the plane perpendicular to the Anterior- Posterior-Commissure axis and tangential to the inferior limit of the Right brain hemisphere.
* #125035 "Left Hemisphere Most Anterior"
* #125035 ^definition = A point fiducial that specifies the location in the plane perpendicular to the Anterior- Posterior-Commissure axis and tangential to the anterior limit of the left brain hemisphere.
* #125036 "Left Hemisphere Most Posterior"
* #125036 ^definition = A point fiducial that specifies the location in the plane perpendicular to the Anterior- Posterior-Commissure axis and tangential to the posterior limit of the left brain hemisphere.
* #125037 "Left Hemisphere Most Superior"
* #125037 ^definition = A point fiducial that specifies the location in the plane perpendicular to the Anterior- Posterior-Commissure axis and tangential to the superior limit of the left brain hemisphere.
* #125038 "Left Hemisphere Most Inferior"
* #125038 ^definition = A point fiducial that specifies the location in the plane perpendicular to the Anterior- Posterior-Commissure axis and tangential to the inferior limit of the left brain hemisphere.
* #125040 "Background"
* #125040 ^definition = That which is not part of an object.
* #125040 ^designation[0].language = #de-AT 
* #125040 ^designation[0].value = "E.g., background of an image (that which might be encoded with Pixel Padding Value, or a Segmentation Property Type." 
* #125041 "Registration Input"
* #125041 ^definition = A segment for use as an input to an image registration process. E.g., to specify the bounding region for determining a Frame of Reference Transformation Matrix.
* #125100 "Vascular Ultrasound Procedure Report"
* #125100 ^definition = Root Document Title for ultrasound vascular evidence reports (worksheets).
* #125101 "Vessel Branch"
* #125101 ^definition = The particular vessel branch, such as the inferior, medial or lateral.
* #125102 "Graft Type"
* #125102 ^definition = A descriptor or elaboration of the type of graft.
* #125105 "Measurement Orientation"
* #125105 ^definition = A modifier to a 2D distance measurement to describe its orientation. E.g., a vascular distance measurement for a vessel plague could have a modifier Transverse or Longitudinal.
* #125106 "Doppler Angle"
* #125106 ^definition = The angle formed between the Doppler beam line and the direction of blood flow within a region of interest in the body defined by the sample volume.
* #125107 "Sample Volume Depth"
* #125107 ^definition = The depth of the center of the Doppler sample volume measured from skin line along the Doppler line.
* #125195 "Pediatric Cardiac Ultrasound Report"
* #125195 ^definition = Pediatric Cardiac Ultrasound Report (document title).
* #125196 "Fetal Cardiac Ultrasound Report"
* #125196 ^definition = Fetal Cardiac Ultrasound Report (document title).
* #125197 "Adult Congenital Cardiac Ultrasound Report"
* #125197 ^definition = Adult Congenital Cardiac Ultrasound Report (document title).
* #125200 "Adult Echocardiography Procedure Report"
* #125200 ^definition = Document title of adult echocardiography procedure (evidence) report.
* #125201 "Illustration of Finding"
* #125201 ^definition = An image that is a pictorial representation of findings. The concept is typically used as a purpose of reference to an image, such as a depiction of myocardium segments depicting wall motion function.
* #125202 "LV Wall Motion Score Index"
* #125202 ^definition = The average of all scored (non-zero) Left Ventricle segment wall motion scores.
* #125203 "Acquisition Protocol"
* #125203 ^definition = A type of clinical acquisition protocol for creating images or image-derived measurements. Acquisition protocols may be specific to a manufacturer's product.
* #125204 "Area-length biplane"
* #125204 ^definition = Method for calculating left ventricular volume from two orthogonal views containing the true long axis (usually the apical 4 and 2 chamber views). Volume = [pL1 / 6] * [(4A1) ¸ (pL1) ] * [(4A2) ¸ (pL2) ].
* #125205 "Area-Length Single Plane"
* #125205 ^definition = Method for calculating left ventricular volume from a view containing the true long axis (usually the apical 4-chamber view). Volume = [8(A)2]¸[3pL].
* #125206 "Cube"
* #125206 ^definition = Method (formula) for calculating left ventricle volumes and function derivatives (EF, SV, SI, etc.) that estimates the volume as the cube of diameter.
* #125207 "Method of Disks, Biplane"
* #125207 ^definition = Method of calculating volume based on the summation of disk volumes. The disk axis is parallel to the left ventricular long axis and using a disk diameter averaged from the two chamber and four chamber views.
* #125208 "Method of Disks, Single Plane"
* #125208 ^definition = Method of calculating volume based on the summation of disk volumes. The disk axis is parallel to the left ventricular long axis with disk diameter taken from the four-chamber view.
* #125209 "Teichholz"
* #125209 ^definition = Method (formula) for calculating left ventricle volumes and function derivatives (EF, SV, SI, etc.) Volume = [7.0/(2.4+D) ]*D3.
* #125210 "Area by Pressure Half-Time"
* #125210 ^definition = Mitral valve area (cm2) by Pressure Half-time = 220 (cm2.ms) / PHT (ms).
* #125211 "Biplane Ellipse"
* #125211 ^definition = Area = P/4 X d1 X d2
* #125212 "Continuity Equation"
* #125212 ^definition = For conduits in series ("in continuity"), volume flow is equal: A1*V1 = A2*V2. where V is the velocity.
* #125213 "Continuity Equation by Mean Velocity"
* #125213 ^definition = For conduits in series ("in continuity"), volume flow is equal: A1*V1 = A2*V2. where V is the mean velocity.
* #125214 "Continuity Equation by Peak Velocity"
* #125214 ^definition = For conduits in series ("in continuity"), volume flow is equal: A1*V1 = A2*V2. where V is the peak velocity.
* #125215 "Continuity Equation by Velocity Time Integral"
* #125215 ^definition = For conduits in series ("in continuity"), volume flow is equal: A1*V1 = A2*V2. where V is the velocity time integral.
* #125216 "Proximal Isovelocity Surface Area"
* #125216 ^definition = Utilizes aliasing velocity (by color Doppler) of flow into an orifice (often regurgitant or stenotic) to measure instantaneous flow rate, orifice area, and flow volume.
* #125217 "Full Bernoulli"
* #125217 ^definition = ?P = 4*(V12 - V22).
* #125218 "Simplified Bernoulli"
* #125218 ^definition = ?P = 4*V2.
* #125219 "Doppler Volume Flow"
* #125219 ^definition = Volume flow = Conduit CSA * (Velocity Time Integral).
* #125220 "Planimetry"
* #125220 ^definition = Direct measurement of an area by tracing an irregular perimeter.
* #125221 "Left Ventricle Mass by M-mode"
* #125221 ^definition = Mass = 1.04 * [(ST+LVID+PWT)3 - LVID3] * 0.8+ 0.6. Mass unit is grams and length in cm.
* #125222 "Left Ventricle Mass by Truncated Ellipse"
* #125222 ^definition = Mass = 1.05P ((b + t)2 X (2/3 (a + t) + d - d3 /3(a + t)2) - b2 (2/3a + d - d3 /3a2))
* #125223 "4 Point Segment Finding Scale"
* #125223 ^definition = A four point, echocardiographic numeric scoring scheme of myocardium segments based on evaluation of wall motion and ventricle morphology. Recommendations for Quantitation of the Left Ventricle by Two-Dimensional Echocardiography, Journal of the American Society of Echocardiography, 2:358-367, 1989.
* #125224 "5 Point Segment Finding Scale"
* #125224 ^definition = A five point, echocardiographic numeric scoring scheme of myocardium segments based on evaluation of wall motion and ventricle morphology. Recommendations for Quantitation of the Left Ventricle by Two-Dimensional Echocardiography, Journal of the American Society of Echocardiography, 2:358-367, 1989.
* #125225 "5 Point Segment Finding Scale With Graded Hypokinesis"
* #125225 ^definition = A five point, echocardiographic numeric scoring scheme of myocardium segments based on evaluation of wall motion and ventricle morphology, with severity of hypokinesis graded. Recommendations for Quantitation of the Left Ventricle by Two-Dimensional Echocardiography, Journal of the American Society of Echocardiography, 2:358-367, 1989.
* #125226 "Single Plane Ellipse"
* #125226 ^definition = Method of estimating volume from a planar ellipse. Equivalent to Biplane Ellipse with an assumption that the ellipse in the orthogonal plane has identical major and minor diameters.
* #125227 "Modified Simpson"
* #125227 ^definition = Modified Simpson's Method of estimating ventricular volume, based on the method of disks with paired apical views.
* #125228 "Bullet Method"
* #125228 ^definition = Bullet method of estimating ventricular volume.
* #125230 "Power Doppler"
* #125230 ^definition = Color coded ultrasound images of blood flow, which depict the amplitude, or power, of Doppler signals.
* #125230 ^designation[0].language = #de-AT 
* #125230 ^designation[0].value = "Retired" 
* #125231 "3D mode"
* #125231 ^definition = Volumetric ultrasound imaging
* #125231 ^designation[0].language = #de-AT 
* #125231 ^designation[0].value = "Retired" 
* #125233 "Start of drug dose administration"
* #125233 ^definition = Onset of administration of dose of a drug.
* #125234 "Start of contrast agent administration"
* #125234 ^definition = Onset of contrast agent administration.
* #125235 "Destruction of microbubbles"
* #125235 ^definition = Destruction of ultrasonic contrast microbubbles by a high-energy ultrasound pulse.
* #125236 "Onset of exercise"
* #125236 ^definition = Instant at which exercise begins.
* #125237 "Cessation of exercise"
* #125237 ^definition = Instant at which exercise ends.
* #125238 "Onset of stimulation"
* #125238 ^definition = Instant at which stimulation begins.
* #125239 "Cessation of stimulation"
* #125239 ^definition = Instant at which stimulation ends.
* #125240 "Line scan pattern"
* #125240 ^definition = Ultrasound transducer scan pattern in which information is gathered along a line.
* #125241 "Plane scan pattern"
* #125241 ^definition = Ultrasound transducer scan pattern in which information is gathered within a plane.
* #125242 "Volume scan pattern"
* #125242 ^definition = Ultrasound transducer scan pattern in which information is gathered within a volume.
* #125251 "Non-imaging Doppler ultrasound transducer geometry"
* #125251 ^definition = Ultrasound transducer geometry characterized by a single scan line used for PW or CW Doppler scanning.
* #125252 "Linear ultrasound transducer geometry"
* #125252 ^definition = Ultrasonic transducer geometry characterized by parallel lines.
* #125253 "Curved linear ultrasound transducer geometry"
* #125253 ^definition = Ultrasonic transducer geometry characterized by radial lines normal to the outside of a curved surface.
* #125254 "Sector ultrasound transducer geometry"
* #125254 ^definition = Ultrasonic transducer geometry characterized by lines originating from a common apex.
* #125255 "Radial ultrasound transducer geometry"
* #125255 ^definition = Ultrasonic transducer geometry characterized by lines emanating radially from a single point.
* #125256 "Ring ultrasound transducer geometry"
* #125256 ^definition = Ultrasonic transducer geometry characterized by a circular ring of transducer elements.
* #125257 "Fixed beam direction"
* #125257 ^definition = Ultrasonic steering technique consisting of a single beam normal to the transducer face steered by the orientation of the probe.
* #125258 "Mechanical beam steering"
* #125258 ^definition = Ultrasonic steering technique consisting of mechanically directing the beam.
* #125259 "Phased beam steering"
* #125259 ^definition = Ultrasonic steering technique consisting of electronically-steered beams.
* #125261 "External Transducer"
* #125261 ^definition = Transducer is designed to be placed onto the surface of the subject.
* #125262 "Transesophageal Transducer"
* #125262 ^definition = Transducer is designed for insertion into the esophagus.
* #125263 "Endovaginal Transducer"
* #125263 ^definition = Transducer is designed for insertion into the vagina.
* #125264 "Endorectal Transducer"
* #125264 ^definition = Transducer is designed for insertion into the rectum.
* #125265 "Intravascular Transducer"
* #125265 ^definition = Transducer is designed for insertion via a catheter.
* #125270 "Left Ventricle Mass by Area Length"
* #125270 ^definition = method to measure the mass of the Left Ventricle via the ASE area-length method at end diastole.
* #125271 "Left Ventricle Mass by M-mode - adjusted by Height"
* #125271 ^definition = Equation = Left Ventricle Mass by M-mode (in gram) / (Height (in meter)) ^2.7
* #125272 "Left Ventricle Mass by Truncated Ellipse - adjusted by Height"
* #125272 ^definition = Equation = Left Ventricle Mass by Truncated Ellipse / Height^2.7
* #125273 "Left Ventricle Mass by Area Length - adjusted by Height"
* #125273 ^definition = Equation = Left Ventricle Mass by Area Length / Height^2.7
* #125901 "CARDIOsphere"
* #125901 ^definition = CARDIOsphere ultrasonic contrast agent produced by POINT Biomedical.
* #125902 "Echovist"
* #125902 ^definition = Echovist® ultrasonic contrast agent produced by Schering AG.
* #125903 "Imagify"
* #125903 ^definition = Imagify ultrasonic contrast agent produced by Accusphere Inc.
* #125904 "Levovist"
* #125904 ^definition = Levovist® ultrasonic contrast agent produced by Schering AG.
* #125905 "Sonazoid"
* #125905 ^definition = Sonazoid ultrasonic contrast agent produced by Daiichi Pharmaceutical / General Electric.
* #125906 "SonoVue"
* #125906 ^definition = SonoVue ultrasonic contrast agent produced by Bracco Diagnostics.
* #125907 "Targestar-B"
* #125907 ^definition = Targestar-B ultrasonic contrast agent produced by Targeson LLC.
* #125908 "Targestar-P"
* #125908 ^definition = Targestar-P ultrasonic contrast agent produced by Targeson LLC.
* #126000 "Imaging Measurement Report"
* #126000 ^definition = A structured report containing the quantitative results of human or machine analysis of images.
* #126001 "Oncology Measurement Report"
* #126001 ^definition = A structured report containing the quantitative results of human or machine analysis of images for oncology evaluation.
* #126002 "Dynamic Contrast MR Measurement Report"
* #126002 ^definition = A structured report containing the quantitative results of human or machine analysis of DCE-MR.
* #126003 "PET Measurement Report"
* #126003 ^definition = A structured report containing the quantitative results of human or machine analysis of PET images.
* #126010 "Imaging Measurements"
* #126010 ^definition = Measurements made on images
* #126011 "Derived Imaging Measurements"
* #126011 ^definition = Measurements derived from measurements made on images.
* #126020 "Multiparametric MRI"
* #126020 ^definition = An MRI procedure in which multiple parameters including diffusion, dynamic contrast and T2 are measured.
* #126021 "Multiparametric MRI of prostate"
* #126021 ^definition = An MRI procedure of the prostate in which multiple parameters including diffusion, dynamic contrast and T2 are measured.
* #126022 "Multiparametric MRI of whole body"
* #126022 ^definition = An MRI procedure of the whole body in which multiple parameters including diffusion, dynamic contrast and T2 are measured.
* #126030 "Sum of segmented voxel volumes"
* #126030 ^definition = The volume derived by summing the volumes of all the voxels (and partial voxels if the segment contains partially occupied voxels) included in the segment
* #126031 "Peak Value Within ROI"
* #126031 ^definition = Maximum average gray value that is calculated from a 1 cubic centimeter sphere placed within the region of interest.
* #126032 "Metabolic Volume"
* #126032 ^definition = The volume of a lesion (e.g., a tumor) ascertained through information about its metabolic activity (e.g., SUV on PET).
* #126033 "Total Lesion Glycolysis"
* #126033 ^definition = The total activity of a lesion obtained as the product of its volume and its glycolytic activity (on FDG-PET).
* #126034 "Glycolysis"
* #126034 ^definition = The amount glycolytic activity summed across all voxels in a defined region or within a defined range of SUV (on FDG-PET).
* #126035 "Total Lesion Proliferation"
* #126035 ^definition = The total activity of a lesion obtained as the product of its volume and its proliferative activity (on FLT-PET).
* #126036 "Proliferative Activity"
* #126036 ^definition = The amount proliferative activity summed across all voxels in a defined region or within a defined range of SUV (on FLT-PET).
* #126037 "Standardized Added Metabolic Activity (SAM)"
* #126037 ^definition = A background-corrected, partial volume independent version of TLG.
* #126038 "Standardized Added Metabolic Activity (SAM) Background"
* #126038 ^definition = The background value (VOI2-VOI1) used to calculate Standardized Added Metabolic Activity (SAM).
* #126039 "Lesion to Background SUV Ratio"
* #126039 ^definition = The ratio of the SUV within a tumor to the SUV of a pre-defined background region.
* #126040 "Background for Lesion to Background SUV Ratio"
* #126040 ^definition = The SUV of a pre-defined background region used to compute Lesion to Background SUV Ratio.
* #126050 "Fractal Dimension"
* #126050 ^definition = A statistical index of complexity comparing how detail in a fractal pattern changes with the scale at which it is measured; a ratio of the change in detail to the change in scale.
* #126051 "Skewness"
* #126051 ^definition = Measure of the asymmetry of the probability distribution of a real-valued random variable about its mean.
* #126052 "Kurtosis"
* #126052 ^definition = Measure of the peakedness of the probability distribution of a real-valued random variable.
* #126060 "Entropy of GLCM"
* #126060 ^definition = The zero order entropy of a Gray Level Co-occurrence Matrix (GLCM). A measure of disorder.
* #126061 "Energy of GLCM"
* #126061 ^definition = The energy (uniformity) (square root of the Angular Second Moment (ASM)) of a Gray Level Co-occurrence Matrix (GLCM). A measure of orderliness.
* #126062 "Homogeneity of GLCM"
* #126062 ^definition = The Inverse Difference Moment of a Gray Level Co-occurrence Matrix (GLCM).
* #126063 "Contrast of GLCM"
* #126063 ^definition = The sum of squares variance of a Gray Level Co-occurrence Matrix (GLCM).
* #126064 "Dissimilarity of GLCM"
* #126064 ^definition = The dissimilarity of a Gray Level Co-occurrence Matrix (GLCM).
* #126065 "ASM of GLCM"
* #126065 ^definition = The Angular Second Moment of a Gray Level Co-occurrence Matrix (GLCM).
* #126066 "Correlation of GLCM"
* #126066 ^definition = A measure of the linear dependency of grey levels on those of neighbouring pixels of a Gray Level Co-occurrence Matrix (GLCM).
* #126067 "Gray Level Co-occurrence Matrix (GLCM)"
* #126067 ^definition = A tabulation of how often different combinations of pixel values (grey levels) occur in an image.
* #126070 "Subject Time Point Identifier"
* #126070 ^definition = An identifier of a specific time point in a continuum, which is unique within an appropriate local context (such as an entire organization, system or treatment protocol), which identifies the time point for a specific patient.
* #126071 "Protocol Time Point Identifier"
* #126071 ^definition = An identifier of a specific time point in a continuum, which is unique within an appropriate local context (such as an entire organization, system or treatment protocol), which identifies the time point slot within a treatment protocol using the same value for all patients in the protocol.
* #126072 "Time Point Type"
* #126072 ^definition = A pre-defined type of a specific time point in a continuum.
* #126073 "Time Point Order"
* #126073 ^definition = A number indicating the order of a time point relative to other time points in the same continuum.
* #126074 "Posttreatment"
* #126074 ^definition = The time after the treatment of interest.
* #126075 "Eligibility"
* #126075 ^definition = For the purpose of determining eligibility for a protocol.
* #126075 ^designation[0].language = #de-AT 
* #126075 ^designation[0].value = "Similar but not identical to (21954-3, LN, 'Protocol eligibility status Cancer'), since not constrained to cancer, etc." 
* #126080 "RECIST 1.0"
* #126080 ^definition = Response Evaluation Criteria in Solid Tumors version 1.0. See [RECIST] in Normative References.
* #126080 ^designation[0].language = #de-AT 
* #126080 ^designation[0].value = "More specific than (112022, DCM, 'RECIST') or (C1709926, UMLS, 'RECIST') or (C49164, NCIt, 'RECIST') in that a specific version is specified." 
* #126081 "RECIST 1.1"
* #126081 ^definition = Response Evaluation Criteria in Solid Tumors Version 1.1. See Eisenhauer et al. "New Response Evaluation Criteria in Solid Tumours: Revised RECIST Guideline (version 1.1)." European Journal of Cancer 45, no. 2 (n.d.): 22847. doi:10.1016/j.ejca.2008.10.026..
* #126081 ^designation[0].language = #de-AT 
* #126081 ^designation[0].value = "More specific than (112022, DCM, 'RECIST') or (C1709926, UMLS, 'RECIST') or (C49164, NCIt, 'RECIST') in that a specific version is specified." 
* #126100 "Real World Value Map used for measurement"
* #126100 ^definition = A reference to the Real World Value Map applied to the stored image pixel values before their use for a measurement
* #126200 "Image Library Group"
* #126200 ^definition = A container that groups common information about a set of images used as evidence to produce a report.
* #126201 "Acquisition Date"
* #126201 ^definition = The date the acquisition of data started
* #126202 "Acquisition Time"
* #126202 ^definition = The time the acquisition of data started
* #126203 "PET Radionuclide Incubation Time"
* #126203 ^definition = The time between the start of injection of the PET radionuclide and the start of acquisition of the PET data.
* #126220 "R2-Coefficient"
* #126220 ^definition = Coefficient of determination, R2. An indication of goodness of fit.
* #126300 "Perfusion analysis by Stable Xenon CT technique"
* #126300 ^definition = Perfusion analysis by Stable Xenon CT technique
* #126301 "Perfusion analysis by IV Iodinated Contrast CT technique"
* #126301 ^definition = Perfusion analysis by IV Iodinated Contrast CT technique
* #126302 "Perfusion analysis by Arterial Spin Labeling MR technique"
* #126302 ^definition = Perfusion analysis by Arterial Spin Labeling (ASL) MR technique
* #126303 "Perfusion analysis by Susceptibility MR technique"
* #126303 ^definition = Perfusion analysis by Susceptibility (T2*) MR technique
* #126310 "Least Mean Square (LMS) deconvolution"
* #126310 ^definition = Least Mean Square (LMS) deconvolution
* #126311 "Singular Value Decomposition (SVD) deconvolution"
* #126311 ^definition = Singular Value Decomposition (SVD) deconvolution
* #126312 "Ktrans"
* #126312 ^definition = Ktrans, the volume transfer constant of a tracer diffusion kinetic model, specifically the volume transfer constant between blood plasma and extravascular extracellular space (EES)
* #126313 "kep"
* #126313 ^definition = kep, the rate constant between extravascular extracellular space (EES) and blood plasma
* #126314 "ve"
* #126314 ^definition = ve, the fractional (not absolute) volume of extravascular extracellular space (EES) per unit volume of tissue
* #126320 "IAUC"
* #126320 ^definition = The intial area under the contrast agent concentrationtime curve
* #126321 "IAUC60"
* #126321 ^definition = The intial area under the contrast agent concentrationtime curve at 60 seconds after the onset time
* #126322 "IAUC90"
* #126322 ^definition = The intial area under the contrast agent concentrationtime curve at 90 seconds after the onset time
* #126330 "tau_m"
* #126330 ^definition = ?m. The mean intracellular water lifetime (?i). Used in the Shutter-Speed Model (SSM) of tracer kinetics.
* #126331 "vp"
* #126331 ^definition = vp. The fractional (not absolute) blood plasma volume per unit volume of tissue.
* #126340 "Standard Tofts Model"
* #126340 ^definition = A tracer diffusion kinetic model in which the permeability is assumed to be isodirectional.
* #126341 "Extended Tofts Model"
* #126341 ^definition = A tracer diffusion kinetic model in which the permeability is not assumed to be isodirectional, and which includes the contribution of tracer in the blood plasma to the total tissue concentration.
* #126342 "Model-free concentration-time quantitification"
* #126342 ^definition = A semiquantitative analysis of the contrast-enhancement concentration versus time curve that avoids the use of a pharmacokinetic model. E.g., integration to compute the initial area under the curve.
* #126343 "First Pass Leakage Profile (FPLP)"
* #126343 ^definition = A tracer diffusion kinetic model that accounts for the tumor leakage profile during the first pass of contrast.
* #126344 "Shutter-Speed Model (SSM)"
* #126344 ^definition = A tracer diffusion kinetic model that does not assume that intercompartmental water molecule exchange is infinitely fast.
* #126350 "T1 by Multiple Flip Angles"
* #126350 ^definition = T1 measurement by Multiple Flip Angles (MFA) (variable saturation) method
* #126351 "T1 by Inversion Recovery"
* #126351 ^definition = T1 measurement by Inversion Recovery (IR) method
* #126352 "T1 by Fixed Value"
* #126352 ^definition = Calculation was performed using a fixed value of T1 rather than a measured value. The value could be encoded as the value of (126353, DCM, "T1 Used For Calculation").
* #126353 "T1 Used For Calculation"
* #126353 ^definition = The fixed value of T1 used for a calculation.
* #126360 "AIF Ignored"
* #126360 ^definition = No Arterial Input Function was used.
* #126361 "Population Averaged AIF"
* #126361 ^definition = A population-averaged Arterial Input Function.
* #126362 "User-defined AIF ROI"
* #126362 ^definition = An Arterial Input Function computed from a user-defined Region of Interest.
* #126363 "Automatically Detected AIF ROI"
* #126363 ^definition = An Arterial Input Function computed from an automatically detected Region of Interest.
* #126364 "Blind Estimation of AIF"
* #126364 ^definition = A data-driven blind source separation (BSS) algorithm that estimates AIF from individuals without any presumed AIF model and initialization. See Lin, Yu-Chun, Tsung-Han Chan, Chong-Yung Chi, Shu-Hang Ng, Hao-Li Liu, Kuo-Chen Wei, Yau-Yau Wai, Chun-Chieh Wang, and Jiun-Jie Wang. "Blind Estimation of the Arterial Input Function in Dynamic Contrast-Enhanced MRI Using Purity Maximization." Magnetic Resonance in Medicine 68, no. 5 (November 1, 2012): 143949. doi:10.1002/mrm.24144.
* #126370 "Time of Peak Concentration"
* #126370 ^definition = The time at which the concentration-time curve achieves its peak for the first time. Used as a concept name for a value or as a method. E.g., used as a method of calculation for BAT. See Shpilfoygel Med Phys 2008. doi: 10.1118/1.1288669
* #126371 "Bolus Arrival Time"
* #126371 ^definition = The nominal time at which arrival of a contrast bolus is detected, which is used as a reference point for subsequent calculations. Used as a concept name for a value or as a method. No specific computational method is implied by this general definition. Abbreviated BAT.
* #126372 "Time of Leading Half-Peak Concentration"
* #126372 ^definition = The time at which the concentration-time curve achieves half of its peak density for the first time. Used as a concept name for a value or as a method. E.g., used as a method of calculation for BAT. See Shpilfoygel Med Phys 2008. doi: 10.1118/1.1288669
* #126373 "Temporal Derivative Exceeds Threshold"
* #126373 ^definition = A method of determining BAT that involves computing the temporal derivative of the concentration-time curve and selecting the time when the temporal derivative exceeds a specified threshold. See Shpilfoygel Med Phys 2008. doi: 10.1118/1.1288669
* #126374 "Temporal Derivative Threshold"
* #126374 ^definition = A threshold applied to the temporal derivative of the concentration-time curve. E.g., used to establish BAT. See Shpilfoygel Med Phys 2008. doi: 10.1118/1.1288669
* #126375 "Maximum Slope"
* #126375 ^definition = The maximum rate of signal intensity change within a measured region of a time-activity curve. See Boonsirikamchai, Piyaporn, Harmeet Kaur, Deborah A. Kuban, Edward Jackson, Ping Hou, and Haesun Choi. Use of Maximum Slope Images Generated From Dynamic Contrast-Enhanced MRI to Detect Locally Recurrent Prostate Carcinoma After Prostatectomy: A Practical Approach. American Journal of Roentgenology 198, no. 3 (March 1, 2012): W228W236. doi:10.2214/AJR.10.6387.
* #126376 "Maximum Difference"
* #126376 ^definition = The maximum degree of signal intensity change within a measured region of a time-activity curve. See Boonsirikamchai, Piyaporn, Harmeet Kaur, Deborah A. Kuban, Edward Jackson, Ping Hou, and Haesun Choi. Use of Maximum Slope Images Generated From Dynamic Contrast-Enhanced MRI to Detect Locally Recurrent Prostate Carcinoma After Prostatectomy: A Practical Approach. American Journal of Roentgenology 198, no. 3 (March 1, 2012): W228W236. doi:10.2214/AJR.10.6387.
* #126377 "Tracer Concentration"
* #126377 ^definition = Tracer concentration in tissue. E.g., in a DCE-MR experiment, the concentration of contrast agent in mmol/l.
* #126380 "Contrast Longitudinal Relaxivity"
* #126380 ^definition = The degree to which a paramagnetic contrast agent can enhance the proton longitudinal relaxation rate constant (R1, 1/T1), normalized to the concentration of the contrast agent. Also referred to as r1. Typically expressed in units of l/mmol/s.
* #126390 "Regional Blood Flow"
* #126390 ^definition = The flow rate of blood perfusing a region as volume per mass per unit of time.
* #126391 "Regional Blood Volume"
* #126391 ^definition = The volume of blood perfusing a region as as volume per mass.
* #126392 "Oxygen Extraction Fraction"
* #126392 ^definition = The percent of the oxygen removed from the blood by tissue during its passage through the capillary network. For example, as measured by blood oxygenation level dependent (BOLD) MR. See He, Xiang, and Dmitriy A. Yablonskiy. Quantitative BOLD: Mapping of Human Cerebral Deoxygenated Blood Volume and Oxygen Extraction Fraction: Default State. Magnetic Resonance in Medicine 57, no. 1 (2007): 11526.
* #126393 "R1"
* #126393 ^definition = The longitiudinal relaxation rate constant. The inverse of longitudinal relaxation time, i.e., R1 = 1/T1.
* #126394 "R2"
* #126394 ^definition = The transverse relaxation rate constant. The inverse of transverse relaxation time, i.e., R2 = 1/T2.
* #126400 "Standardized Uptake Value"
* #126400 ^definition = A ratio of locally measured radioactivity concentration versus the injected radioactivity distibuted evenly throughout the whole body.
* #126401 "SUVbw"
* #126401 ^definition = Standardized Uptake Value calculated using body weight. The patient size correction factor for males and females is body weight.
* #126402 "SUVlbm"
* #126402 ^definition = Standardized Uptake Value calculated using lean body mass. The patient size correction factor for males is 1.10 * weight - 120 * (weight/height) ^2, and for females is 1.07 * weight - 148 * (weight/height) ^2.
* #126403 "SUVbsa"
* #126403 ^definition = Standardized Uptake Value calculated using body surface area. The patient size correction factor for males and females is weight^ 0.425 * height^0.725 * 0.007184.
* #126404 "SUVibw"
* #126404 ^definition = Standardized Uptake Value calculated using ideal body weight. The patient size correction factor for males is 48.0 + 1.06 * (height - 152) and for females is 45.5 + 0.91 * (height - 152).
* #126410 "SUV body weight calculation method"
* #126410 ^definition = Method of calculating Standardized Uptake Value using body weight. The patient size correction factor for males and females is body weight.
* #126411 "SUV lean body mass calculation method"
* #126411 ^definition = Method of calculating Standardized Uptake Value using lean body mass. The patient size correction factor for males is 1.10 * weight - 120 * (weight/height) ^2, and for females is 1.07 * weight - 148 * (weight/height) ^2.
* #126412 "SUV body surface area calculation method"
* #126412 ^definition = Method of calculating Standardized Uptake Value using body surface area. The patient size correction factor for males and females is weight^ 0.425 * height^0.725 * 0.007184.
* #126413 "SUV ideal body weight calculation method"
* #126413 ^definition = Method of calculating Standardized Uptake Value using ideal body weight. The patient size correction factor for males is 48.0 + 1.06 * (height - 152) and for females is 45.5 + 0.91 * (height - 152).
* #126500 "Pittsburgh compound B C^11^"
* #126500 ^definition = A beta-amyloid PET radiotracer that is an analog of thioflavin T.
* #126501 "Florbetaben F^18^"
* #126501 ^definition = A beta-amyloid PET radiotracer.
* #126502 "T807 F^18^"
* #126502 ^definition = A PHF-tau PET radiotracer.
* #126503 "Flubatine F^18^"
* #126503 ^definition = A nicotinic ?4?2 receptor (nAChR) PET radiotracer.
* #126510 "Monoclonal Antibody (mAb) ^64^Cu"
* #126510 ^definition = A Cu 64 Monoclonal Antibody (mAb) PET Radiotracer.
* #126511 "Monoclonal Antibody (mAb) ^89^Zr"
* #126511 ^definition = A Zr 89 Monoclonal Antibody (mAb) PET Radiotracer.
* #126512 "Trastuzumab ^89^Zr"
* #126512 ^definition = A Zr 89 Trastuzumab PET Radiotracer.
* #126513 "Cetuximab ^89^Zr"
* #126513 ^definition = A Zr 89 Cetuximab PET Radiotracer.
* #126514 "J591 ^89^Zr"
* #126514 ^definition = A Zr 89 J591 PET Radiotracer.
* #126515 "cU36 ^89^Zr"
* #126515 ^definition = A Zr 89 cU36 PET Radiotracer.
* #126516 "Bevacizumab ^89^Zr"
* #126516 ^definition = A Zr 89 Bevacizumab PET Radiotracer.
* #126517 "cG250-F(ab')(2) ^89^Zr"
* #126517 ^definition = A Zr 89 cG250-F(ab')(2) PET Radiotracer.
* #126518 "R1507 ^89^Zr"
* #126518 ^definition = A Zr 89 R1507 PET Radiotracer.
* #126519 "E4G10 ^89^Zr"
* #126519 ^definition = A Zr 89 E4G10 PET Radiotracer.
* #126520 "Df-CD45 ^89^Zr"
* #126520 ^definition = A Zr 89 Df-CD45 PET Radiotracer.
* #126600 "^44^Scandium"
* #126600 ^definition = ^44^Scandium
* #126601 "^51^Manganese"
* #126601 ^definition = ^51^Manganese
* #126602 "^70^Arsenic"
* #126602 ^definition = ^70^Arsenic
* #126603 "^90^Niobium"
* #126603 ^definition = ^90^Niobium
* #126604 "^191m^Iridium"
* #126604 ^definition = ^191m^Iridium
* #126605 "^43^Scandium"
* #126605 ^definition = ^43^Scandium
* #126606 "^152^Terbium"
* #126606 ^definition = ^152^Terbium
* #126700 "ATSM Cu^60^"
* #126700 ^definition = A Cu 60 ATSM PET radiotracer.
* #126701 "ATSM Cu^61^"
* #126701 ^definition = A Cu 61 ATSM PET radiotracer.
* #126702 "ATSM Cu^62^"
* #126702 ^definition = A Cu 62 ATSM PET radiotracer.
* #126703 "Choline C^11^"
* #126703 ^definition = A C 11 Choline PET radiotracer.
* #126704 "Fallypride C^11^"
* #126704 ^definition = A C 11 Fallypride PET radiotracer.
* #126705 "Fallypride F^18^"
* #126705 ^definition = An F 18 Fallypride PET radiotracer.
* #126706 "FLB 457 C^11^"
* #126706 ^definition = A C 11 FLB 457 PET radiotracer.
* #126707 "Fluorotriopride F^18^"
* #126707 ^definition = An F 18 Fluorotriopride PET radiotracer.
* #126708 "Fluoromisonidazole (FMISO) F^18^"
* #126708 ^definition = An F 18 Fluoromisonidazole PET radiotracer.
* #126709 "Glutamine C^11^"
* #126709 ^definition = A C 11 Glutamine PET radiotracer.
* #126710 "Glutamine C^14^"
* #126710 ^definition = A C 14 Glutamine PET radiotracer.
* #126711 "Glutamine F^18^"
* #126711 ^definition = An F 18 Glutamine PET radiotracer.
* #126712 "Flubatine F^18^"
* #126712 ^definition = An F 18 Flubatine PET radiotracer.
* #126713 "2FA F^18^"
* #126713 ^definition = An F 18 2FA PET radiotracer.
* #126714 "Nifene F^18^"
* #126714 ^definition = An F 18 Nifene PET radiotracer.
* #126715 "CLR1404 I^124^"
* #126715 ^definition = An I 124 cancer targeted phospholipid ether PET radiotracer.
* #126716 "CLR1404 I^131^"
* #126716 ^definition = An I 131 cancer targeted phospholipid ether PET radiotracer.
* #126801 "IEC6127 Patient Support Continuous Angle"
* #126801 ^definition = Patient Support Continuous Angle in IEC PATIENT SUPPORT Coordinate System [IEC 61217].
* #126802 "IEC6127 Table Top Continuous Pitch Angle"
* #126802 ^definition = Table Top Continuous Pitch Angle in the direction of the IEC TABLE TOP Coordinate System [IEC 61217].
* #126803 "IEC6127 Table Top Continuous Roll Angle"
* #126803 ^definition = Table Top Continuous Roll Angle in the direction of the IEC TABLE TOP Coordinate System [IEC 61217].
* #126804 "IEC6127 Table Top Eccentric Axis Distance"
* #126804 ^definition = Table Top Eccentric Axis Distance [IEC 61217].
* #126805 "IEC6127 Table Top Continuous Eccentric Angle"
* #126805 ^definition = Table Top Continuous Eccentric Angle in the direction of the IEC TABLE TOP ECCENTRIC Coordinate System [IEC 61217].
* #126806 "IEC6127 Table Top Lateral Position"
* #126806 ^definition = Table Top Lateral Position IEC TABLE TOP Coordinate System [IEC 61217].
* #126807 "IEC6127 Table Top Longitudinal Position"
* #126807 ^definition = Table Top Longitudinal Position IEC TABLE TOP Coordinate System [IEC 61217].
* #126808 "IEC6127 Table Top Vertical Position"
* #126808 ^definition = Table Top Vertical Position in IEC TABLE TOP Coordinate System [IEC 61217].
* #126809 "IEC6127 Gantry Continuous Roll Angle"
* #126809 ^definition = Gantry Continuous Roll Angle in degrees of the radiation source, i.e., the rotation about the Y-axis of the IEC GANTRY coordinate system [IEC 61217].
* #126810 "IEC6127 Gantry Continuous Pitch Angle"
* #126810 ^definition = Gantry Pitch Continuous Angle in degrees of the radiation source, i.e., the rotation about the X-axis of the IEC GANTRY coordinate system [IEC 61217].
* #126811 "IEC6127 Gantry Continuous Yaw Angle"
* #126811 ^definition = Gantry Yaw Continuous Angle in degrees of the radiation source, i.e., about the Z-axis of the IEC GANTRY coordinate system [IEC 61217].
* #AR "Autorefraction"
* #AR ^definition = Autorefraction device.
* #ARCHIVE "Archive"
* #ARCHIVE ^definition = Archive device.
* #AS "Angioscopy"
* #AS ^definition = Angioscopy device
* #AS ^designation[0].language = #de-AT 
* #AS ^designation[0].value = "Retired" 
* #AU "Audio"
* #AU ^definition = Audio object.
* #BDUS "Ultrasound Bone Densitometry"
* #BDUS ^definition = Ultrasound Bone Densitometry (modality).
* #BI "Biomagnetic imaging"
* #BI ^definition = Biomagnetic imaging device.
* #BMD "Bone Mineral Densitometry"
* #BMD ^definition = Bone Mineral Densitometry by X-Ray (modality), including dual-energy X-Ray absorptiometry (DXA) and morphometric X-Ray absorptiometry (MXA).
* #CAD "Computer Assisted Detection/Diagnosis"
* #CAD ^definition = Computer Assisted Detection/Diagnosis device.
* #CAPTURE "Image Capture"
* #CAPTURE ^definition = Image Capture Device, includes video capture.
* #CD "Color flow Doppler"
* #CD ^definition = Color flow Doppler
* #CD ^designation[0].language = #de-AT 
* #CD ^designation[0].value = "Retired, Replaced by (US, DCM, 'Ultrasound')" 
* #CF "Cinefluorography"
* #CF ^definition = Cinefluorography
* #CF ^designation[0].language = #de-AT 
* #CF ^designation[0].value = "Retired, Replaced by (RF, DCM, 'Radiofluoroscopy')" 
* #COMP "Computation Server"
* #COMP ^definition = Computation Server; includes radiotherapy planning.
* #CP "Culposcopy"
* #CP ^definition = Culposcopy
* #CP ^designation[0].language = #de-AT 
* #CP ^designation[0].value = "Retired" 
* #CR "Computed Radiography"
* #CR ^definition = Computed Radiography device.
* #CS "Cystoscopy"
* #CS ^definition = Cystoscopy
* #CS ^designation[0].language = #de-AT 
* #CS ^designation[0].value = "Retired" 
* #CT "Computed Tomography"
* #CT ^definition = Computed Tomography device.
* #DD "Duplex Doppler"
* #DD ^definition = Duplex Doppler
* #DD ^designation[0].language = #de-AT 
* #DD ^designation[0].value = "Retired, Replaced by (US, DCM, 'Ultrasound')" 
* #DF "Digital fluoroscopy"
* #DF ^definition = Digital fluoroscopy
* #DF ^designation[0].language = #de-AT 
* #DF ^designation[0].value = "Retired, Replaced by (RF, DCM, 'Radiofluoroscopy')" 
* #DG "Diaphanography"
* #DG ^definition = Diaphanography device.
* #DM "Digital microscopy"
* #DM ^definition = Digital microscopy
* #DM ^designation[0].language = #de-AT 
* #DM ^designation[0].value = "Retired" 
* #DOCD "Document Digitizer Equipment"
* #DOCD ^definition = Equipment that digitized hardcopy documents and imported them.
* #DS "Digital Subtraction Angiography"
* #DS ^definition = Digital Subtraction Angiography
* #DS ^designation[0].language = #de-AT 
* #DS ^designation[0].value = "Retired, Replaced by (XA, DCM, 'X-Ray Angiography')" 
* #DSS "Department System Scheduler"
* #DSS ^definition = Department System Scheduler, workflow manager; includes RIS.
* #DX "Digital Radiography"
* #DX ^definition = Digital Radiography device.
* #EC "Echocardiography"
* #EC ^definition = Echocardiography
* #EC ^designation[0].language = #de-AT 
* #EC ^designation[0].value = "Retired, Replaced by (US, DCM, 'Ultrasound')" 
* #ECG "Electrocardiography"
* #ECG ^definition = Electrocardiography device.
* #EPS "Cardiac Electrophysiology"
* #EPS ^definition = Cardiac Electrophysiology device.
* #ES "Endoscopy"
* #ES ^definition = Endoscopy device.
* #F "Female"
* #F ^definition = Female sex.
* #FA "Fluorescein angiography"
* #FA ^definition = Fluorescein angiography
* #FA ^designation[0].language = #de-AT 
* #FA ^designation[0].value = "Retired, Replaced by (OP, DCM, 'Ophthalmic photography')" 
* #FC "Female changed to Male"
* #FC ^definition = Female sex changed to Male sex.
* #FILMD "Film Digitizer"
* #FILMD ^definition = Film Digitizer.
* #FP "Female Pseudohermaphrodite"
* #FP ^definition = Female Pseudohermaphrodite.
* #FS "Fundoscopy"
* #FS ^definition = Fundoscopy
* #FS ^designation[0].language = #de-AT 
* #FS ^designation[0].value = "Retired" 
* #GM "General Microscopy"
* #GM ^definition = General Microscopy device.
* #H "Hermaphrodite"
* #H ^definition = Hermaphrodite.
* #HC "Hard Copy"
* #HC ^definition = Hard Copy.
* #HD "Hemodynamic Waveform"
* #HD ^definition = Hemodynamic Waveform acquisition device.
* #IO "Intra-oral Radiography"
* #IO ^definition = Intra-oral Radiography device.
* #IVUS "Intravascular Ultrasound"
* #IVUS ^definition = Intravascular Ultrasound device.
* #KER "Keratometry"
* #KER ^definition = Keratometry device.
* #KO "Key Object Selection"
* #KO ^definition = Key Object Selection object.
* #LEN "Lensometry"
* #LEN ^definition = Lensometry device.
* #LOG "Procedure Logging"
* #LOG ^definition = Procedure Logging device; includes cath lab logging.
* #LP "Laparoscopy"
* #LP ^definition = Laparoscopy
* #LP ^designation[0].language = #de-AT 
* #LP ^designation[0].value = "Retired" 
* #LS "Laser surface scan"
* #LS ^definition = Laser surface scan device.
* #M "Male"
* #M ^definition = Male sex.
* #MA "Magnetic resonance angiography"
* #MA ^definition = Magnetic resonance angiography
* #MA ^designation[0].language = #de-AT 
* #MA ^designation[0].value = "Retired, Replaced by (MR, DCM, 'Magnetic resonance')" 
* #MCD "Media Creation Device"
* #MCD ^definition = A device that creates DICOM PS3.10 interchange media. E.g., a CD creator that is managed by the Media Creation Management Service Class.
* #MEDIM "Portable Media Importer Equipment"
* #MEDIM ^definition = Equipment that retrieved and imported objects from interchange Media.
* #MG "Mammography"
* #MG ^definition = Mammography device.
* #MP "Male Pseudohermaphrodite"
* #MP ^definition = Male Pseudohermaphrodite.
* #MR "Magnetic Resonance"
* #MR ^definition = Magnetic Resonance device.
* #MS "Magnetic resonance spectroscopy"
* #MS ^definition = Magnetic resonance spectroscopy
* #MS ^designation[0].language = #de-AT 
* #MS ^designation[0].value = "Retired, Replaced by (MR, DCM, 'Magnetic resonance')" 
* #NEARLINE "Nearline"
* #NEARLINE ^definition = Instances need to be retrieved from relatively slow media such as optical disk or tape.
* #NM "Nuclear Medicine"
* #NM ^definition = Nuclear Medicine device.
* #OAM "Ophthalmic Axial Measurements"
* #OAM ^definition = Measurements of the axial length of the eye, which are done by various devices.
* #OCT "Optical Coherence Tomography"
* #OCT ^definition = Modality device that uses an interferometric, non-invasive optical tomographic technique to image 2D slices and 3D volumes of tissue using visible and near visible frequencies.
* #OFFLINE "Offline"
* #OFFLINE ^definition = Instances need to be retrieved by manual intervention.
* #ONLINE "Online"
* #ONLINE ^definition = Instances are immediately available.
* #OP "Ophthalmic photography"
* #OP ^definition = Ophthalmic photography modality.
* #OPM "Ophthalmic Mapping"
* #OPM ^definition = Modality device that measures corneal topography, corneal or retinal thickness, and other similar parameters that are typically displayed as maps.
* #OPR "Ophthalmic Refraction"
* #OPR ^definition = Modality device that measures the refractive characteristics of the eye.
* #OPT "Ophthalmic Tomography"
* #OPT ^definition = Tomography of the eye acquired by a modality that is based on light and optical principles. Tomography based on other principles, such as ultrasound, is excluded.
* #OPV "Ophthalmic Visual Field"
* #OPV ^definition = Modality device that measures visual fields and perform visual perimetry.
* #OT "Other Modality"
* #OT ^definition = Other Modality device.
* #PR "Presentation State"
* #PR ^definition = Presentation State object.
* #PRINT "Hard Copy Print Server"
* #PRINT ^definition = Hard Copy Print Server; includes printers with embedded DICOM print server.
* #PT "Positron emission tomography"
* #PT ^definition = Positron emission tomography (PET) device.
* #PX "Panoramic X-Ray"
* #PX ^definition = Panoramic X-Ray device.
* #REG "Registration"
* #REG ^definition = Registration.
* #RF "Radiofluoroscopy"
* #RF ^definition = Radiofluoroscopy device.
* #RG "Radiographic imaging"
* #RG ^definition = Radiographic imaging (conventional film/screen).
* #RT "Radiation Therapy Device"
* #RT ^definition = Radiation Therapy Device; includes linear accelerator, proton therapy.
* #RTDOSE "Radiotherapy Dose"
* #RTDOSE ^definition = Radiotherapy Dose.
* #RTIMAGE "Radiotherapy Image"
* #RTIMAGE ^definition = Radiotherapy Imaging device; includes portal imaging.
* #RTPLAN "Radiotherapy Plan"
* #RTPLAN ^definition = Radiotherapy Plan.
* #RTRECORD "Radiotherapy Treatment Record"
* #RTRECORD ^definition = Radiotherapy Treatment Record.
* #RTSTRUCT "Radiotherapy Structure Set"
* #RTSTRUCT ^definition = Radiotherapy Structure Set.
* #SEG "Segmentation"
* #SEG ^definition = Segmentation.
* #SM "Slide Microscopy"
* #SM ^definition = Slide Microscopy.
* #SMR "Stereometric Relationship"
* #SMR ^definition = Stereometric image pairing modality.
* #SR "Structured Report Document"
* #SR ^definition = Structured Report Document.
* #SRF "Subjective Refraction"
* #SRF ^definition = Subjective Refraction device.
* #ST "Single-photon emission computed tomography"
* #ST ^definition = Single-photon emission computed tomography (SPECT) device
* #ST ^designation[0].language = #de-AT 
* #ST ^designation[0].value = "Retired, Replaced by (NM, DCM, 'Nuclear Medicine')" 
* #TG "Thermography"
* #TG ^definition = Thermography device.
* #U "Unknown Sex"
* #U ^definition = Unknown Sex.
* #UNAVAILABLE "Unavailable"
* #UNAVAILABLE ^definition = Instances cannot be retrieved.
* #US "Ultrasound"
* #US ^definition = Ultrasound device.
* #VA "Visual Acuity"
* #VA ^definition = Visual Acuity device.
* #VF "Videofluorography"
* #VF ^definition = Videofluorography
* #VF ^designation[0].language = #de-AT 
* #VF ^designation[0].value = "Retired, Replaced by (RF, DCM, 'Radiofluoroscopy')" 
* #VIDD "Video Tape Digitizer Equipment"
* #VIDD ^definition = Equipment that digitizes video tape and imports it.
* #WSD "Workstation"
* #WSD ^definition = Workstation.
* #XA "X-Ray Angiography"
* #XA ^definition = X-Ray Angiography device.
* #XC "External-camera Photography"
* #XC ^definition = External-camera Photography device.
