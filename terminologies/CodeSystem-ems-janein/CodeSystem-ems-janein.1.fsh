Instance: ems-janein 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-janein" 
* name = "ems-janein" 
* title = "EMS_JaNein" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS-Liste für Ja, Nein, Möglich, Unzutreffend, " 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.59" 
* date = "2017-01-26" 
* count = 5 
* concept[0].code = #N
* concept[0].display = "nein"
* concept[1].code = #NA
* concept[1].display = "nicht zutreffend"
* concept[2].code = #PSBL
* concept[2].display = "möglich"
* concept[3].code = #UNK
* concept[3].display = "unbekannt"
* concept[4].code = #Y
* concept[4].display = "ja"
