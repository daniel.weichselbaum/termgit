Instance: elga-sections 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-sections" 
* name = "elga-sections" 
* title = "ELGA_Sections" 
* status = #active 
* content = #complete 
* version = "202011" 
* description = "**Description:** Set of codes defining the sections and their labeling (displayed as the title of the section) permitted by ELGA 

**Beschreibung:** Auflistung der Codes für erlaubte Sektionen in ELGA und ihre Bezeichnungen (Titelzeile)." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.40" 
* date = "2020-11-25" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* count = 34 
* property[0].code = #hints 
* property[0].type = #string 
* #ABBEM "Abschließende Bemerkungen"
* #ABBEM ^definition = Abschließende Bemerkungen
* #ABBEM ^property[0].code = #hints 
* #ABBEM ^property[0].valueString = "Titel der Sektion wird vom ELGA-Referenzstylesheet nicht dargestellt" 
* #ANGEFUNTERS "Angeforderte Untersuchungen"
* #ANGEFUNTERS ^definition = Angeforderte Untersuchungen
* #ANM "Anmerkungen"
* #ANM ^definition = Anmerkungen
* #BEFAUS "Ausstehende Befunde"
* #BEFAUS ^definition = Ausstehende Befunde
* #BEFBEI "Beigelegte erhobene Befunde"
* #BEFBEI ^definition = Beigelegte erhobene Befunde
* #BEFERH "Auszüge aus erhobenen Befunden"
* #BEFERH ^definition = Auszüge aus erhobenen Befunden
* #BEIL "Beilagen"
* #BEIL ^definition = Beilagen
* #BRIEFT "Brieftext"
* #BRIEFT ^definition = Brieftext
* #BRIEFT ^property[0].code = #hints 
* #BRIEFT ^property[0].valueString = "Titel der Sektion wird vom ELGA-Referenzstylesheet nicht dargestellt" 
* #FachAnamnse "Fachspezifische Anamnese"
* #FachAnamnse ^definition = Fachspezifische Anamnese
* #FachDiagnostik "Fachspezifische Diagnostik"
* #FachDiagnostik ^definition = Fachspezifische Diagnostik
* #INFEKTSER "Infektionsserologie"
* #INFEKTSER ^definition = Infektionsserologie
* #LISTGDA "Liste der behandelnden GDA"
* #LISTGDA ^definition = Liste der behandelnden GDA
* #MELDEPERR "Meldepflichtige Erreger"
* #MELDEPERR ^definition = Meldepflichtige Erreger
* #MOLEKERN "Molekularer Erregernachweis"
* #MOLEKERN ^definition = Molekularer Erregernachweis
* #OPBER "Operationsbericht"
* #OPBER ^definition = Operationsbericht
* #OUTCOMEMEAS "Outcome Measurement"
* #OUTCOMEMEAS ^definition = Outcome Measurement
* #PFATM "Atmung"
* #PFATM ^definition = Atmung
* #PFAUS "Ausscheidung"
* #PFAUS ^definition = Ausscheidung
* #PFDIAG "Pflege- und Betreuungsdiagnosen"
* #PFDIAG ^definition = Pflege- und Betreuungsdiagnosen
* #PFERN "Ernährung"
* #PFERN ^definition = Ernährung
* #PFHAUT "Hautzustand"
* #PFHAUT ^definition = Hautzustand
* #PFKLEI "Körperpflege und Kleiden"
* #PFKLEI ^definition = Körperpflege und Kleiden
* #PFKOMM "Kommunikation"
* #PFKOMM ^definition = Kommunikation
* #PFMED "Medikamentenverabreichung"
* #PFMED ^definition = Medikamentenverabreichung
* #PFMEDBEH "Pflegerelevante Informationen zur medizinischen Behandlung"
* #PFMEDBEH ^definition = Pflegerelevante Informationen zur medizinischen Behandlung
* #PFMOB "Mobilität"
* #PFMOB ^definition = Mobilität
* #PFORIE "Orientierung und Bewusstseinslage"
* #PFORIE ^definition = Orientierung und Bewusstseinslage
* #PFROLL "Rollenwahrnehmung und Sinnfindung"
* #PFROLL ^definition = Rollenwahrnehmung und Sinnfindung
* #PFSCHL "Schlaf"
* #PFSCHL ^definition = Schlaf
* #PFSOZV "Soziale Umstände und Verhalten"
* #PFSOZV ^definition = Soziale Umstände und Verhalten
* #PUBUMF "Pflege- und Betreuungsumfang"
* #PUBUMF ^definition = Alle Informationen zur Beschreibung des Pflege- und Betreuungsumfangs
* #REHAZIELE "Rehabilitationsziele"
* #REHAZIELE ^definition = Rehabilitationsziele
* #RES "Hilfsmittel und Ressourcen"
* #RES ^definition = Hilfsmittel und Ressourcen
* #TERMIN "Termine, Kontrollen, Wiederbestellung"
* #TERMIN ^definition = Termine, Kontrollen, Wiederbestellung
