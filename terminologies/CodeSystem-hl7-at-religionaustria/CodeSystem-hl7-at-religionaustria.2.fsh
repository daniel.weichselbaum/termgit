Instance: hl7-at-religionaustria 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-at-religionaustria" 
* name = "hl7-at-religionaustria" 
* title = "HL7 AT ReligionAustria" 
* status = #active 
* content = #complete 
* version = "HL7.AT:ReligionAustria" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.2.16.1.4.1" 
* date = "2013-01-10" 
* count = 62 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #100 "Katholische Kirche (o.n.A.)"
* #100 ^property[0].code = #child 
* #100 ^property[0].valueCode = #101 
* #100 ^property[1].code = #child 
* #100 ^property[1].valueCode = #102 
* #100 ^property[2].code = #child 
* #100 ^property[2].valueCode = #103 
* #100 ^property[3].code = #child 
* #100 ^property[3].valueCode = #104 
* #100 ^property[4].code = #child 
* #100 ^property[4].valueCode = #105 
* #100 ^property[5].code = #child 
* #100 ^property[5].valueCode = #106 
* #100 ^property[6].code = #child 
* #100 ^property[6].valueCode = #107 
* #100 ^property[7].code = #child 
* #100 ^property[7].valueCode = #108 
* #100 ^property[8].code = #child 
* #100 ^property[8].valueCode = #109 
* #101 "Römisch-Katholisch"
* #101 ^property[0].code = #parent 
* #101 ^property[0].valueCode = #100 
* #102 "Griechisch-Katholische Kirche"
* #102 ^property[0].code = #parent 
* #102 ^property[0].valueCode = #100 
* #103 "Armenisch-Katholische Kirche"
* #103 ^property[0].code = #parent 
* #103 ^property[0].valueCode = #100 
* #104 "Bulgarisch-Katholische Kirche"
* #104 ^property[0].code = #parent 
* #104 ^property[0].valueCode = #100 
* #105 "Rumänische griechisch-katholische Kirche"
* #105 ^property[0].code = #parent 
* #105 ^property[0].valueCode = #100 
* #106 "Russisch-Katholische Kirche"
* #106 ^property[0].code = #parent 
* #106 ^property[0].valueCode = #100 
* #107 "Syrisch-Katholische Kirche"
* #107 ^property[0].code = #parent 
* #107 ^property[0].valueCode = #100 
* #108 "Ukrainische Griechisch-Katholische Kirche"
* #108 ^property[0].code = #parent 
* #108 ^property[0].valueCode = #100 
* #109 "Katholische Ostkirche (ohne nähere Angabe)"
* #109 ^property[0].code = #parent 
* #109 ^property[0].valueCode = #100 
* #110 "Griechisch-Orientalische Kirchen"
* #110 ^property[0].code = #child 
* #110 ^property[0].valueCode = #111 
* #110 ^property[1].code = #child 
* #110 ^property[1].valueCode = #112 
* #110 ^property[2].code = #child 
* #110 ^property[2].valueCode = #113 
* #110 ^property[3].code = #child 
* #110 ^property[3].valueCode = #114 
* #110 ^property[4].code = #child 
* #110 ^property[4].valueCode = #115 
* #110 ^property[5].code = #child 
* #110 ^property[5].valueCode = #116 
* #110 ^property[6].code = #child 
* #110 ^property[6].valueCode = #117 
* #110 ^property[7].code = #child 
* #110 ^property[7].valueCode = #118 
* #111 "Orthodoxe Kirchen (o.n.A.)"
* #111 ^property[0].code = #parent 
* #111 ^property[0].valueCode = #110 
* #112 "Griechisch-Orthodoxe Kirche (Hl.Dreifaltigkeit)"
* #112 ^property[0].code = #parent 
* #112 ^property[0].valueCode = #110 
* #113 "Griechisch-Orthodoxe Kirche (Hl.Georg)"
* #113 ^property[0].code = #parent 
* #113 ^property[0].valueCode = #110 
* #114 "Bulgarisch-Orthodoxe Kirche"
* #114 ^property[0].code = #parent 
* #114 ^property[0].valueCode = #110 
* #115 "Rumänisch-griechisch-orientalische Kirche"
* #115 ^property[0].code = #parent 
* #115 ^property[0].valueCode = #110 
* #116 "Russisch-Orthodoxe Kirche"
* #116 ^property[0].code = #parent 
* #116 ^property[0].valueCode = #110 
* #117 "Serbisch-griechisch-Orthodoxe Kirche"
* #117 ^property[0].code = #parent 
* #117 ^property[0].valueCode = #110 
* #118 "Ukrainisch-Orthodoxe Kirche"
* #118 ^property[0].code = #parent 
* #118 ^property[0].valueCode = #110 
* #119 "Orientalisch-Orthodoxe Kirchen"
* #119 ^property[0].code = #child 
* #119 ^property[0].valueCode = #120 
* #119 ^property[1].code = #child 
* #119 ^property[1].valueCode = #121 
* #119 ^property[2].code = #child 
* #119 ^property[2].valueCode = #122 
* #119 ^property[3].code = #child 
* #119 ^property[3].valueCode = #123 
* #119 ^property[4].code = #child 
* #119 ^property[4].valueCode = #124 
* #119 ^property[5].code = #child 
* #119 ^property[5].valueCode = #125 
* #120 "Armenisch-apostolische Kirche"
* #120 ^property[0].code = #parent 
* #120 ^property[0].valueCode = #119 
* #121 "Syrisch-orthodoxe Kirche"
* #121 ^property[0].code = #parent 
* #121 ^property[0].valueCode = #119 
* #122 "Syrisch-orthodoxe Kirche"
* #122 ^property[0].code = #parent 
* #122 ^property[0].valueCode = #119 
* #123 "Koptisch-orthodoxe Kirche"
* #123 ^property[0].code = #parent 
* #123 ^property[0].valueCode = #119 
* #124 "Armenisch-apostolische Kirche"
* #124 ^property[0].code = #parent 
* #124 ^property[0].valueCode = #119 
* #125 "Äthiopisch-Orthodoxe Kirche"
* #125 ^property[0].code = #parent 
* #125 ^property[0].valueCode = #119 
* #126 "Evangelische Kirchen Österreich"
* #126 ^property[0].code = #child 
* #126 ^property[0].valueCode = #127 
* #126 ^property[1].code = #child 
* #126 ^property[1].valueCode = #128 
* #126 ^property[2].code = #child 
* #126 ^property[2].valueCode = #129 
* #127 "Evangelische Kirche (o.n.A.)"
* #127 ^property[0].code = #parent 
* #127 ^property[0].valueCode = #126 
* #128 "Evangelische Kirche A.B."
* #128 ^property[0].code = #parent 
* #128 ^property[0].valueCode = #126 
* #129 "Evangelische Kirche H.B."
* #129 ^property[0].code = #parent 
* #129 ^property[0].valueCode = #126 
* #130 "Andere Christliche Kirchen"
* #130 ^property[0].code = #child 
* #130 ^property[0].valueCode = #131 
* #130 ^property[1].code = #child 
* #130 ^property[1].valueCode = #132 
* #130 ^property[2].code = #child 
* #130 ^property[2].valueCode = #133 
* #131 "Altkatholische Kirche Österreichs"
* #131 ^property[0].code = #parent 
* #131 ^property[0].valueCode = #130 
* #132 "Anglikanische Kirche"
* #132 ^property[0].code = #parent 
* #132 ^property[0].valueCode = #130 
* #133 "Evangelisch-methodistische Kirche (EmK)"
* #133 ^property[0].code = #parent 
* #133 ^property[0].valueCode = #130 
* #134 "Sonstige Christliche Gemeinschaften"
* #134 ^property[0].code = #child 
* #134 ^property[0].valueCode = #135 
* #134 ^property[1].code = #child 
* #134 ^property[1].valueCode = #136 
* #134 ^property[2].code = #child 
* #134 ^property[2].valueCode = #137 
* #134 ^property[3].code = #child 
* #134 ^property[3].valueCode = #138 
* #134 ^property[4].code = #child 
* #134 ^property[4].valueCode = #139 
* #134 ^property[5].code = #child 
* #134 ^property[5].valueCode = #140 
* #134 ^property[6].code = #child 
* #134 ^property[6].valueCode = #141 
* #134 ^property[7].code = #child 
* #134 ^property[7].valueCode = #142 
* #134 ^property[8].code = #child 
* #134 ^property[8].valueCode = #143 
* #134 ^property[9].code = #child 
* #134 ^property[9].valueCode = #144 
* #134 ^property[10].code = #child 
* #134 ^property[10].valueCode = #145 
* #134 ^property[11].code = #child 
* #134 ^property[11].valueCode = #146 
* #135 "Baptisten"
* #135 ^property[0].code = #parent 
* #135 ^property[0].valueCode = #134 
* #136 "Bund evangelikaler Gemeinden in Österreich"
* #136 ^property[0].code = #parent 
* #136 ^property[0].valueCode = #134 
* #137 "Freie Christengemeinde/Pfingstgemeinde"
* #137 ^property[0].code = #parent 
* #137 ^property[0].valueCode = #134 
* #138 "Mennonitische Freikirche"
* #138 ^property[0].code = #parent 
* #138 ^property[0].valueCode = #134 
* #139 "Kirche der Siebenten-Tags-Adventisten"
* #139 ^property[0].code = #parent 
* #139 ^property[0].valueCode = #134 
* #140 "Christengemeinschaft"
* #140 ^property[0].code = #parent 
* #140 ^property[0].valueCode = #134 
* #141 "Jehovas Zeugen"
* #141 ^property[0].code = #parent 
* #141 ^property[0].valueCode = #134 
* #142 "Neuapostolische Kirche"
* #142 ^property[0].code = #parent 
* #142 ^property[0].valueCode = #134 
* #143 "Mormonen"
* #143 ^property[0].code = #parent 
* #143 ^property[0].valueCode = #134 
* #144 "Sonstige Christliche Gemeinschaften (O.n.A.)"
* #144 ^property[0].code = #parent 
* #144 ^property[0].valueCode = #134 
* #145 "ELAIA Christengemeinden"
* #145 ^property[0].code = #parent 
* #145 ^property[0].valueCode = #134 
* #146 "Pfingstkirche Gemeinde Gottes"
* #146 ^property[0].code = #parent 
* #146 ^property[0].valueCode = #134 
* #148 "Nicht-christliche Gemeinschaften"
* #148 ^property[0].code = #child 
* #148 ^property[0].valueCode = #149 
* #148 ^property[1].code = #child 
* #148 ^property[1].valueCode = #150 
* #148 ^property[2].code = #child 
* #148 ^property[2].valueCode = #151 
* #148 ^property[3].code = #child 
* #148 ^property[3].valueCode = #152 
* #148 ^property[4].code = #child 
* #148 ^property[4].valueCode = #153 
* #148 ^property[5].code = #child 
* #148 ^property[5].valueCode = #154 
* #148 ^property[6].code = #child 
* #148 ^property[6].valueCode = #155 
* #148 ^property[7].code = #child 
* #148 ^property[7].valueCode = #156 
* #148 ^property[8].code = #child 
* #148 ^property[8].valueCode = #157 
* #148 ^property[9].code = #child 
* #148 ^property[9].valueCode = #158 
* #148 ^property[10].code = #child 
* #148 ^property[10].valueCode = #162 
* #149 "Israelitische Religionsgesellschaft"
* #149 ^property[0].code = #parent 
* #149 ^property[0].valueCode = #148 
* #150 "Islamische Glaubensgemeinschaft"
* #150 ^property[0].code = #parent 
* #150 ^property[0].valueCode = #148 
* #151 "Alevitische Religionsgesellschaft"
* #151 ^property[0].code = #parent 
* #151 ^property[0].valueCode = #148 
* #152 "Buddhistische Religionsgesellschaft"
* #152 ^property[0].code = #parent 
* #152 ^property[0].valueCode = #148 
* #153 "Baha` i"
* #153 ^property[0].code = #parent 
* #153 ^property[0].valueCode = #148 
* #154 "Hinduistische Religionsgesellschaft"
* #154 ^property[0].code = #parent 
* #154 ^property[0].valueCode = #148 
* #155 "Sikh"
* #155 ^property[0].code = #parent 
* #155 ^property[0].valueCode = #148 
* #156 "Shintoismus"
* #156 ^property[0].code = #parent 
* #156 ^property[0].valueCode = #148 
* #157 "Vereinigungskirche"
* #157 ^property[0].code = #parent 
* #157 ^property[0].valueCode = #148 
* #158 "Andere religiöse Bekenntnisgemeinschaften"
* #158 ^property[0].code = #parent 
* #158 ^property[0].valueCode = #148 
* #162 "Pastafarianismus"
* #162 ^property[0].code = #parent 
* #162 ^property[0].valueCode = #148 
* #159 "Konfessionslos; ohne Angabe"
* #159 ^property[0].code = #child 
* #159 ^property[0].valueCode = #160 
* #159 ^property[1].code = #child 
* #159 ^property[1].valueCode = #161 
* #160 "Konfessionslos"
* #160 ^property[0].code = #parent 
* #160 ^property[0].valueCode = #159 
* #161 "Ohne Angabe"
* #161 ^property[0].code = #parent 
* #161 ^property[0].valueCode = #159 
