Instance: ems-ribotype 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-ribotype" 
* name = "ems-ribotype" 
* title = "EMS_Ribotype" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** Ribotype: Verwendung bei Diphtherie" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.89" 
* date = "2017-01-26" 
* count = 88 
* concept[0].code = #AMER
* concept[0].display = "America"
* concept[1].code = #BALT
* concept[1].display = "Baltik"
* concept[2].code = #BANG
* concept[2].display = "Bangladesh"
* concept[3].code = #BEAU
* concept[3].display = "Beaujon"
* concept[4].code = #BERL
* concept[4].display = "Berlin"
* concept[5].code = #BETH
* concept[5].display = "Bethune"
* concept[6].code = #BREA
* concept[6].display = "Breaza"
* concept[7].code = #BRIS
* concept[7].display = "Bristol"
* concept[8].code = #BROU
* concept[8].display = "Broussais"
* concept[9].code = #BURI
* concept[9].display = "Buri"
* concept[10].code = #BURIRAM
* concept[10].display = "Buriram"
* concept[11].code = #BUZA
* concept[11].display = "Buzau"
* concept[12].code = #CAIR
* concept[12].display = "Cairns"
* concept[13].code = #CARR
* concept[13].display = "Carribean"
* concept[14].code = #CLUJ
* concept[14].display = "Cluj"
* concept[15].code = #CONSTANTA
* concept[15].display = "Constanta"
* concept[16].code = #CONSTANTINE
* concept[16].display = "Constantine"
* concept[17].code = #DAGE
* concept[17].display = "Dagestan"
* concept[18].code = #DANM
* concept[18].display = "Danmark"
* concept[19].code = #DARL
* concept[19].display = "Darling"
* concept[20].code = #DOMI
* concept[20].display = "Dominica"
* concept[21].code = #ERLA
* concept[21].display = "Erlabrunn"
* concept[22].code = #GALL
* concept[22].display = "Gallia"
* concept[23].code = #GAMB
* concept[23].display = "Gambia"
* concept[24].code = #GATC
* concept[24].display = "Gatchina"
* concept[25].code = #GOETEBORG
* concept[25].display = "Goeteborg"
* concept[26].code = #GOETTINGEN
* concept[26].display = "Goettingen"
* concept[27].code = #GREI
* concept[27].display = "Greifswald"
* concept[28].code = #HO-C
* concept[28].display = "Ho-Chi-Minh"
* concept[29].code = #IASI
* concept[29].display = "Iasi"
* concept[30].code = #KALI
* concept[30].display = "Kaliningrad"
* concept[31].code = #KEMP
* concept[31].display = "Kempsey"
* concept[32].code = #LEHA
* concept[32].display = "Le Havre"
* concept[33].code = #LOND
* concept[33].display = "Londinium"
* concept[34].code = #LONJ
* concept[34].display = "Lonjumeaux"
* concept[35].code = #LYON
* concept[35].display = "Lyon"
* concept[36].code = #MANC
* concept[36].display = "Manchester"
* concept[37].code = #MARS
* concept[37].display = "Marseille"
* concept[38].code = #MINS
* concept[38].display = "Minsk"
* concept[39].code = #MOSK
* concept[39].display = "Moskva"
* concept[40].code = #NA
* concept[40].display = "nicht zutreffend"
* concept[41].code = #NAN
* concept[41].display = "Nan"
* concept[42].code = #NEAM
* concept[42].display = "Neamt"
* concept[43].code = #NEDL
* concept[43].display = "Nedlands"
* concept[44].code = #NY
* concept[44].display = "New-York"
* concept[45].code = #OTCH
* concept[45].display = "Otchakov"
* concept[46].code = #OTHER
* concept[46].display = "andere"
* concept[47].code = #PAKI
* concept[47].display = "Pakistan"
* concept[48].code = #PAMI
* concept[48].display = "Pamiers"
* concept[49].code = #PARI
* concept[49].display = "Paris"
* concept[50].code = #PERT
* concept[50].display = "Perth"
* concept[51].code = #PERU
* concept[51].display = "Peru"
* concept[52].code = #PRAH
* concept[52].display = "Prahova"
* concept[53].code = #PRAT
* concept[53].display = "Prathet-Thai"
* concept[54].code = #PRES
* concept[54].display = "Preston"
* concept[55].code = #PRZE
* concept[55].display = "Przemysl"
* concept[56].code = #RAS
* concept[56].display = "Ras-el-Ma"
* concept[57].code = #RHON
* concept[57].display = "Rhône"
* concept[58].code = #ROMA
* concept[58].display = "Roma"
* concept[59].code = #ROMANIA
* concept[59].display = "Romania"
* concept[60].code = #ROSS
* concept[60].display = "Rossija"
* concept[61].code = #SAIG
* concept[61].display = "Saigon"
* concept[62].code = #SANK
* concept[62].display = "Sankt-Peterburg"
* concept[63].code = #SCHW
* concept[63].display = "Schwarzenberg"
* concept[64].code = #SEAT
* concept[64].display = "Seattle"
* concept[65].code = #SOMA
* concept[65].display = "Somalia"
* concept[66].code = #STAL
* concept[66].display = "St-Albans"
* concept[67].code = #STRA
* concept[67].display = "Strasbourg"
* concept[68].code = #SUCE
* concept[68].display = "Suceava"
* concept[69].code = #SVER
* concept[69].display = "Sveridge"
* concept[70].code = #SWAN
* concept[70].display = "Swansea"
* concept[71].code = #SWED
* concept[71].display = "Sweden"
* concept[72].code = #SYDN
* concept[72].display = "Sydney"
* concept[73].code = #TAMP
* concept[73].display = "Tamper"
* concept[74].code = #THAI
* concept[74].display = "Thai"
* concept[75].code = #THAÏ
* concept[75].display = "Thaïland"
* concept[76].code = #TOOT
* concept[76].display = "Tooting"
* concept[77].code = #TOUR
* concept[77].display = "Tourcoing"
* concept[78].code = #TUNI
* concept[78].display = "Tunisia"
* concept[79].code = #UNK
* concept[79].display = "unbekann"
* concept[80].code = #VERS
* concept[80].display = "Versailles"
* concept[81].code = #VIBO
* concept[81].display = "Viborg"
* concept[82].code = #VICT
* concept[82].display = "Victoria"
* concept[83].code = #VIET
* concept[83].display = "Viet-Nam"
* concept[84].code = #VLAD
* concept[84].display = "Vladimir"
* concept[85].code = #VRAN
* concept[85].display = "Vrancea"
* concept[86].code = #WASH
* concept[86].display = "Washington"
* concept[87].code = #WEST
* concept[87].display = "Westmead"
