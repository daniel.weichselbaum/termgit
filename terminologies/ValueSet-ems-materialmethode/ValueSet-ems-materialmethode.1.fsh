Instance: ems-materialmethode 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-ems-materialmethode" 
* name = "ems-materialmethode" 
* title = "EMS_MaterialMethode" 
* status = #active 
* version = "202001" 
* description = "**Description:** Concepts for EMS methods for specimen extractions

**Beschreibung:** Konzepte für Methode der Materialgewinnung" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.16" 
* date = "2020-01-01" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-ems-materialmethode"
* compose.include[0].concept[0].code = #HIST
* compose.include[0].concept[0].display = "Methode Material Histologie"
* compose.include[0].concept[1].code = #KULTUR
* compose.include[0].concept[1].display = "Methode Kultur Material"
* compose.include[0].concept[2].code = #MAT1
* compose.include[0].concept[2].display = "Primary Sample"
* compose.include[0].concept[3].code = #MAT2
* compose.include[0].concept[3].display = "Secondary Sample"
* compose.include[0].concept[4].code = #MIKRO
* compose.include[0].concept[4].display = "Methode Mikroskopie Material"
* compose.include[0].concept[5].code = #NAT
* compose.include[0].concept[5].display = "Methode NAT Material"
* compose.include[0].concept[6].code = #SERO
* compose.include[0].concept[6].display = "Methode Material Serologie"
* compose.include[0].concept[7].code = #VIRUS
* compose.include[0].concept[7].display = "Methode Material Virusnachweis"

* expansion.timestamp = "2022-09-13T14:15:28.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem-ems-materialmethode"
* expansion.contains[0].code = #HIST
* expansion.contains[0].display = "Methode Material Histologie"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem-ems-materialmethode"
* expansion.contains[1].code = #KULTUR
* expansion.contains[1].display = "Methode Kultur Material"
* expansion.contains[2].system = "https://termgit.elga.gv.at/CodeSystem-ems-materialmethode"
* expansion.contains[2].code = #MAT1
* expansion.contains[2].display = "Primary Sample"
* expansion.contains[3].system = "https://termgit.elga.gv.at/CodeSystem-ems-materialmethode"
* expansion.contains[3].code = #MAT2
* expansion.contains[3].display = "Secondary Sample"
* expansion.contains[4].system = "https://termgit.elga.gv.at/CodeSystem-ems-materialmethode"
* expansion.contains[4].code = #MIKRO
* expansion.contains[4].display = "Methode Mikroskopie Material"
* expansion.contains[5].system = "https://termgit.elga.gv.at/CodeSystem-ems-materialmethode"
* expansion.contains[5].code = #NAT
* expansion.contains[5].display = "Methode NAT Material"
* expansion.contains[6].system = "https://termgit.elga.gv.at/CodeSystem-ems-materialmethode"
* expansion.contains[6].code = #SERO
* expansion.contains[6].display = "Methode Material Serologie"
* expansion.contains[7].system = "https://termgit.elga.gv.at/CodeSystem-ems-materialmethode"
* expansion.contains[7].code = #VIRUS
* expansion.contains[7].display = "Methode Material Virusnachweis"
