Instance: ems-materialmethode 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-ems-materialmethode" 
* name = "ems-materialmethode" 
* title = "EMS_MaterialMethode" 
* status = #active 
* version = "202001" 
* description = "**Description:** Concepts for EMS methods for specimen extractions

**Beschreibung:** Konzepte für Methode der Materialgewinnung" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.16" 
* date = "2020-01-01" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-ems-materialmethode"
* compose.include[0].concept[0].code = "HIST"
* compose.include[0].concept[0].display = "Methode Material Histologie"
* compose.include[0].concept[1].code = "KULTUR"
* compose.include[0].concept[1].display = "Methode Kultur Material"
* compose.include[0].concept[2].code = "MAT1"
* compose.include[0].concept[2].display = "Primary Sample"
* compose.include[0].concept[3].code = "MAT2"
* compose.include[0].concept[3].display = "Secondary Sample"
* compose.include[0].concept[4].code = "MIKRO"
* compose.include[0].concept[4].display = "Methode Mikroskopie Material"
* compose.include[0].concept[5].code = "NAT"
* compose.include[0].concept[5].display = "Methode NAT Material"
* compose.include[0].concept[6].code = "SERO"
* compose.include[0].concept[6].display = "Methode Material Serologie"
* compose.include[0].concept[7].code = "VIRUS"
* compose.include[0].concept[7].display = "Methode Material Virusnachweis"
