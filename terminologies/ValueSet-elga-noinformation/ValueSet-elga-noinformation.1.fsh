Instance: elga-noinformation 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-noinformation" 
* name = "elga-noinformation" 
* title = "ELGA_NoInformation" 
* status = #active 
* version = "3.0" 
* description = "**Description:** Describes codes, which are required to use, if no information is present. Contains information protected by copyright of SNOMED International. Any use of SNOMED CT in Austria requires a valid affiliate license or sublicense. The corresponding license is free of charge, provided that the use only takes place in Austria and fulfills the conditions of the Affiliate License Agreement. Affiliate licenses can be requested directly from the respective NRC via the [Member Licensing and Distribution Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS).

**Beschreibung:** Beschreibt Codierungen, die anzuwenden sind, wenn keine Informationen vorliegen. Enthält durch SNOMED International urheberrechtlich geschützte Information. Jede Verwendung von SNOMED CT in Österreich erfordert eine aufrechte Affiliate Lizenz oder eine Sublizenz. Die entsprechende Lizenz ist kostenlos, vorausgesetzt die Verwendung findet nur in Österreich statt und erfüllt die Bedingungen des Affiliate License Agreements. Affiliate Lizenzen können über das [Mitglieder-Lizenzierungs- und Distributions-Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS) direkt beim jeweiligen NRC beantragt werden." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.33" 
* date = "2015-03-31" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* compose.include[0].concept[0].code = #182849000
* compose.include[0].concept[0].display = "no drug therapy prescribed"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "Keine empfohlene Medikation angegeben" 
* compose.include[0].concept[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[0].designation[1].value = "To indicate the absense of any prescribed medications" 
* compose.include[0].concept[0].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[0].designation[2].value = "Wird angewandt, wenn keine empfohlene Medikation vorliegt" 
* compose.include[0].concept[1].code = #408350003
* compose.include[0].concept[1].display = "patient not on self-medications"
* compose.include[0].concept[1].designation[0].language = #de-AT 
* compose.include[0].concept[1].designation[0].value = "Keine Medikation (bei Einweisung)" 
* compose.include[0].concept[1].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[1].designation[1].value = "To indicate no treatment" 
* compose.include[0].concept[1].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[1].designation[2].value = "Wird angewandt, wenn keine Medikation bei Einweisung vorliegt" 

* expansion.timestamp = "2022-09-13T14:16:31.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* expansion.contains[0].code = #182849000
* expansion.contains[0].display = "no drug therapy prescribed"
* expansion.contains[0].designation[0].language = #de-AT 
* expansion.contains[0].designation[0].value = "Keine empfohlene Medikation angegeben" 
* expansion.contains[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[0].designation[1].value = "To indicate the absense of any prescribed medications" 
* expansion.contains[0].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* expansion.contains[0].designation[2].value = "Wird angewandt, wenn keine empfohlene Medikation vorliegt" 
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* expansion.contains[1].code = #408350003
* expansion.contains[1].display = "patient not on self-medications"
* expansion.contains[1].designation[0].language = #de-AT 
* expansion.contains[1].designation[0].value = "Keine Medikation (bei Einweisung)" 
* expansion.contains[1].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[1].designation[1].value = "To indicate no treatment" 
* expansion.contains[1].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* expansion.contains[1].designation[2].value = "Wird angewandt, wenn keine Medikation bei Einweisung vorliegt" 
