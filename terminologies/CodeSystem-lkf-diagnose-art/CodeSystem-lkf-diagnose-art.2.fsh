Instance: lkf-diagnose-art 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-lkf-diagnose-art" 
* name = "lkf-diagnose-art" 
* title = "LKF_Diagnose-Art" 
* status = #active 
* content = #complete 
* version = "202107" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.207" 
* date = "2021-07-30" 
* count = 2 
* #D "Aktuelle/Behandelte Diagnose"
* #V "Verdachtsdiagnose"
