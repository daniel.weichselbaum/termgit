Instance: hl7-consent-type 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-consent-type" 
* name = "hl7-consent-type" 
* title = "HL7 Consent Type" 
* status = #active 
* content = #complete 
* description = "**Description:** Consent Type (Table 0496)

**Beschreibung:** Art der Einverständniserklärung (Tabelle 0496)

**Versions-Beschreibung:** Art der Einverständniserklärung (Tabelle 0496)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.12.496" 
* date = "2015-06-01" 
* count = 1 
* #ELGA-SOO "Situatives Opt-Out für ELGA"
* #ELGA-SOO ^definition = Zustimmung (Consent) zum Situativen Opt-Out
* #ELGA-SOO ^designation[0].language = #de-AT 
* #ELGA-SOO ^designation[0].value = "Patient hat ein 'Situatives Opt-Out für ELGA' erklärt " 
