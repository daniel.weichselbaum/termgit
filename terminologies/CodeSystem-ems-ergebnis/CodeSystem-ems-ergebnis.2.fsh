Instance: ems-ergebnis 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-ergebnis" 
* name = "ems-ergebnis" 
* title = "EMS_Ergebnis" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Ergebnis: Verwendung in 'Ergebnis IgM' und 'Ergebnis  IgG'" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.76" 
* date = "2017-01-26" 
* count = 5 
* #EQUI "mehrdeutig, zweifelhaft"
* #NA "nicht anwendbar"
* #NEG "negativ"
* #NOTEST "nicht getestet"
* #POS "positiv"
