Instance: lkf-diagnose-typ 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-lkf-diagnose-typ" 
* name = "lkf-diagnose-typ" 
* title = "LKF_Diagnose-Typ" 
* status = #active 
* version = "202107" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.69" 
* date = "2021-07-30" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-lkf-diagnose-typ"
* compose.include[0].concept[0].code = "H"
* compose.include[0].concept[0].display = "Hauptdiagnose"
* compose.include[0].concept[1].code = "Z"
* compose.include[0].concept[1].display = "Zusatzdiagnose"
