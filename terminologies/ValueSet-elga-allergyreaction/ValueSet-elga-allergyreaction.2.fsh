Instance: elga-allergyreaction 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-allergyreaction" 
* name = "elga-allergyreaction" 
* title = "ELGA_AllergyReaction" 
* status = #active 
* version = "202104" 
* description = "**Description:** Reactions on Allergies Or Intolerances. Contains information protected by copyright of SNOMED International. Any use of SNOMED CT in Austria requires a valid affiliate license or sublicense. The corresponding license is free of charge, provided that the use only takes place in Austria and fulfills the conditions of the Affiliate License Agreement. Affiliate licenses can be requested directly from the respective NRC via the [Member Licensing and Distribution Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS).

**Beschreibung:** Überempfindlichkeitsreaktionen: Auswirkungen von Allergien und Intoleranzen. Enthält durch SNOMED International urheberrechtlich geschützte Information. Jede Verwendung von SNOMED CT in Österreich erfordert eine aufrechte Affiliate Lizenz oder eine Sublizenz. Die entsprechende Lizenz ist kostenlos, vorausgesetzt die Verwendung findet nur in Österreich statt und erfüllt die Bedingungen des Affiliate License Agreements. Affiliate Lizenzen können über das [Mitglieder-Lizenzierungs- und Distributions-Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS) direkt beim jeweiligen NRC beantragt werden." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.181" 
* date = "2021-04-28" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* compose.include[0].concept[0].code = "111209006"
* compose.include[0].concept[0].display = "Photoallergische Dermatitis"
* compose.include[0].concept[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[0].designation[0].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[1].code = "115664001"
* compose.include[0].concept[1].display = "Photoallergische Dermatitis [veraltet]"
* compose.include[0].concept[1].designation[0].language = #de-AT 
* compose.include[0].concept[1].designation[0].value = "Photoallergische Dermatitis" 
* compose.include[0].concept[1].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[1].designation[1].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[1].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[1].designation[2].value = "inaktiv; ersetzt durch 111209006" 
* compose.include[0].concept[2].code = "126485001"
* compose.include[0].concept[2].display = "Urtikaria"
* compose.include[0].concept[2].designation[0].language = #de-AT 
* compose.include[0].concept[2].designation[0].value = "Urtikaria~Nesselsucht" 
* compose.include[0].concept[2].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[2].designation[1].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[3].code = "162290004"
* compose.include[0].concept[3].display = "Trockenheit der Augen"
* compose.include[0].concept[3].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[3].designation[0].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[4].code = "195967001"
* compose.include[0].concept[4].display = "Asthma"
* compose.include[0].concept[4].designation[0].language = #de-AT 
* compose.include[0].concept[4].designation[0].value = "Asthma~Asthma bronchiale" 
* compose.include[0].concept[4].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[4].designation[1].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[5].code = "23924001"
* compose.include[0].concept[5].display = "Brustschmerzen"
* compose.include[0].concept[5].designation[0].language = #de-AT 
* compose.include[0].concept[5].designation[0].value = "Brustschmerzen~Engegefühl in der Brust" 
* compose.include[0].concept[5].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[5].designation[1].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[6].code = "24079001"
* compose.include[0].concept[6].display = "Atopische Dermatitis"
* compose.include[0].concept[6].designation[0].language = #de-AT 
* compose.include[0].concept[6].designation[0].value = "Atopische Dermatitis~Atopisches Ekzem~Neurodermitis" 
* compose.include[0].concept[6].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[6].designation[1].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[7].code = "247471006"
* compose.include[0].concept[7].display = "Makulopapulöser Ausschlag"
* compose.include[0].concept[7].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[7].designation[0].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[8].code = "247472004"
* compose.include[0].concept[8].display = "Quaddeln"
* compose.include[0].concept[8].designation[0].language = #de-AT 
* compose.include[0].concept[8].designation[0].value = "Quaddeln~Striemen~Hautrötungen" 
* compose.include[0].concept[8].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[8].designation[1].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[9].code = "267036007"
* compose.include[0].concept[9].display = "Atemnot"
* compose.include[0].concept[9].designation[0].language = #de-AT 
* compose.include[0].concept[9].designation[0].value = "Atemnot~Atemlosigkeit~Kurzatmigkeit" 
* compose.include[0].concept[9].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[9].designation[1].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[10].code = "271757001"
* compose.include[0].concept[10].display = "Papulöser Ausschlag"
* compose.include[0].concept[10].designation[0].language = #de-AT 
* compose.include[0].concept[10].designation[0].value = "Papulöser Ausschlag~Papeln" 
* compose.include[0].concept[10].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[10].designation[1].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[11].code = "271759003"
* compose.include[0].concept[11].display = "Bullöse Eruption"
* compose.include[0].concept[11].designation[0].language = #de-AT 
* compose.include[0].concept[11].designation[0].value = "Bullöse Eruption~Dermatose" 
* compose.include[0].concept[11].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[11].designation[1].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[12].code = "271807003"
* compose.include[0].concept[12].display = "Hautausschlag"
* compose.include[0].concept[12].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[12].designation[0].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[13].code = "31996006"
* compose.include[0].concept[13].display = "Vaskulitis"
* compose.include[0].concept[13].designation[0].language = #de-AT 
* compose.include[0].concept[13].designation[0].value = "Vaskulitis~Gefäßentzündung" 
* compose.include[0].concept[13].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[13].designation[1].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[14].code = "359610006"
* compose.include[0].concept[14].display = "Augenrötung [veraltet]"
* compose.include[0].concept[14].designation[0].language = #de-AT 
* compose.include[0].concept[14].designation[0].value = "Augenrötung" 
* compose.include[0].concept[14].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[14].designation[1].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[14].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[14].designation[2].value = "inaktiv; ersetzt durch 781682005" 
* compose.include[0].concept[15].code = "36715001"
* compose.include[0].concept[15].display = "Erythema exsudativum multiforme"
* compose.include[0].concept[15].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[15].designation[0].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[16].code = "3723001"
* compose.include[0].concept[16].display = "Arthritis"
* compose.include[0].concept[16].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[16].designation[0].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[17].code = "39579001"
* compose.include[0].concept[17].display = "Anaphylaxie"
* compose.include[0].concept[17].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[17].designation[0].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[18].code = "40275004"
* compose.include[0].concept[18].display = "Kontaktdermatitis"
* compose.include[0].concept[18].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[18].designation[0].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[19].code = "410430005"
* compose.include[0].concept[19].display = "Herzstillstand"
* compose.include[0].concept[19].designation[0].language = #de-AT 
* compose.include[0].concept[19].designation[0].value = "Herzstillstand~Kreislaufstillstand" 
* compose.include[0].concept[19].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[19].designation[1].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[20].code = "41291007"
* compose.include[0].concept[20].display = "Angioödem"
* compose.include[0].concept[20].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[20].designation[0].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[21].code = "418290006"
* compose.include[0].concept[21].display = "Juckreiz"
* compose.include[0].concept[21].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[21].designation[0].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[22].code = "418363000"
* compose.include[0].concept[22].display = "Hautjuckreiz"
* compose.include[0].concept[22].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[22].designation[0].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[23].code = "422400008"
* compose.include[0].concept[23].display = "Erbrechen"
* compose.include[0].concept[23].designation[0].language = #de-AT 
* compose.include[0].concept[23].designation[0].value = "Erbrechen~Emesis~Vomitus" 
* compose.include[0].concept[23].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[23].designation[1].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[24].code = "422587007"
* compose.include[0].concept[24].display = "Übelkeit"
* compose.include[0].concept[24].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[24].designation[0].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[25].code = "43116000"
* compose.include[0].concept[25].display = "Ekzem"
* compose.include[0].concept[25].designation[0].language = #de-AT 
* compose.include[0].concept[25].designation[0].value = "Ekzem~Juckflechte" 
* compose.include[0].concept[25].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[25].designation[1].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[26].code = "4386001"
* compose.include[0].concept[26].display = "Bronchospasmus"
* compose.include[0].concept[26].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[26].designation[0].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[27].code = "49727002"
* compose.include[0].concept[27].display = "Husten"
* compose.include[0].concept[27].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[27].designation[0].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[28].code = "51599000"
* compose.include[0].concept[28].display = "Larynxödem"
* compose.include[0].concept[28].designation[0].language = #de-AT 
* compose.include[0].concept[28].designation[0].value = "Larynxödem~Kehlkopfödem" 
* compose.include[0].concept[28].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[28].designation[1].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[29].code = "52845002"
* compose.include[0].concept[29].display = "Nephritis"
* compose.include[0].concept[29].designation[0].language = #de-AT 
* compose.include[0].concept[29].designation[0].value = "Nephritis~Nierenentzündung" 
* compose.include[0].concept[29].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[29].designation[1].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[30].code = "62315008"
* compose.include[0].concept[30].display = "Diarrhoe"
* compose.include[0].concept[30].designation[0].language = #de-AT 
* compose.include[0].concept[30].designation[0].value = "Diarrhoe~Durchfall" 
* compose.include[0].concept[30].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[30].designation[1].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[31].code = "698247007"
* compose.include[0].concept[31].display = "Herzrhythmusstörung"
* compose.include[0].concept[31].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[31].designation[0].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[32].code = "70076002"
* compose.include[0].concept[32].display = "Rhinitis"
* compose.include[0].concept[32].designation[0].language = #de-AT 
* compose.include[0].concept[32].designation[0].value = "Rhinitis~Nasenschleimhautentzündung" 
* compose.include[0].concept[32].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[32].designation[1].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[33].code = "702809001"
* compose.include[0].concept[33].display = "DRESS Syndrom"
* compose.include[0].concept[33].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[33].designation[0].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[34].code = "73442001"
* compose.include[0].concept[34].display = "Stevens-Johnson-Syndrom"
* compose.include[0].concept[34].designation[0].language = #de-AT 
* compose.include[0].concept[34].designation[0].value = "Stevens-Johnson-Syndrom~Dermatostomatitis Baader~ Fiessinger-Rendu-Syndrom" 
* compose.include[0].concept[34].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[34].designation[1].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[35].code = "76067001"
* compose.include[0].concept[35].display = "Niesen"
* compose.include[0].concept[35].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[35].designation[0].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[36].code = "768962006"
* compose.include[0].concept[36].display = "Lyell-Syndrom"
* compose.include[0].concept[36].designation[0].language = #de-AT 
* compose.include[0].concept[36].designation[0].value = "Lyell-Syndrom~Epidermolysis acuta toxica" 
* compose.include[0].concept[36].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[36].designation[1].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[37].code = "781682005"
* compose.include[0].concept[37].display = "Hyperemia of eye"
* compose.include[0].concept[37].designation[0].language = #de-AT 
* compose.include[0].concept[37].designation[0].value = "Augenrötung" 
* compose.include[0].concept[37].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[37].designation[1].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[38].code = "91175000"
* compose.include[0].concept[38].display = "Krampfanfall"
* compose.include[0].concept[38].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[38].designation[0].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[39].code = "95361005"
* compose.include[0].concept[39].display = "Entzündungskrankheiten der Schleimhaut"
* compose.include[0].concept[39].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[39].designation[0].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[40].code = "9826008"
* compose.include[0].concept[40].display = "Conjunktivitis"
* compose.include[0].concept[40].designation[0].language = #de-AT 
* compose.include[0].concept[40].designation[0].value = "Conjunktivitis~Konjunktivitis~Bindehautentzündung" 
* compose.include[0].concept[40].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[40].designation[1].value = "SNOMED Clinical Terms" 
