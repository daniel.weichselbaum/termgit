Instance: medikationrezeptpflichtstatus 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-medikationrezeptpflichtstatus" 
* name = "medikationrezeptpflichtstatus" 
* title = "MedikationRezeptpflichtstatus" 
* status = #active 
* content = #complete 
* version = "20211126" 
* description = "**Description:** Medikation Rezeptpflichtstatus

**Beschreibung:** The information concerning this list is reproduced from the EUTCT repository with permission of the EMA (European Medicines Agency)." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.10.1.4.3.4.3.7" 
* date = "2021-11-26" 
* count = 8 
* concept[0].code = #100000072076
* concept[0].display = "Arzneimittel zur Abgabe ohne aerztliche Verschreibung"
* concept[0].designation[0].language = #de-AT 
* concept[0].designation[0].value = "Arzneimittel zur Abgabe ohne aerztliche Verschreibung" 
* concept[1].code = #100000072077
* concept[1].display = "Arzneimittel zur einmaligen oder wiederholten Abgabe gegen aerztliche Verschreibung"
* concept[1].designation[0].language = #de-AT 
* concept[1].designation[0].value = "Arzneimittel zur einmaligen oder wiederholten Abgabe gegen aerztliche Verschreibung" 
* concept[2].code = #100000072078
* concept[2].display = "Arzneimittel zur eingeschraenkten Abgabe gegen aerztliche Verschreibung, deren Anwendung definierten Spezialisten vorbehalten ist"
* concept[2].designation[0].language = #de-AT 
* concept[2].designation[0].value = "Arzneimittel zur eingeschraenkten Abgabe gegen aerztliche Verschreibung, deren Anwendung definierten Spezialisten vorbehalten ist" 
* concept[3].code = #100000072079
* concept[3].display = "Arzneimittel zur einmaligen Abgabe auf aerztliche Verschreibung"
* concept[3].designation[0].language = #de-AT 
* concept[3].designation[0].value = "Arzneimittel zur einmaligen Abgabe auf aerztliche Verschreibung" 
* concept[4].code = #100000072084
* concept[4].display = "Arzneimittel zur Abgabe gegen aerztliche Verschreibung"
* concept[4].designation[0].language = #de-AT 
* concept[4].designation[0].value = "Arzneimittel zur Abgabe gegen aerztliche Verschreibung" 
* concept[5].code = #100000072086
* concept[5].display = "Arzneimittel zur wiederholten Abgabe gegen aerztliche Verschreibung"
* concept[5].designation[0].language = #de-AT 
* concept[5].designation[0].value = "Arzneimittel zur wiederholten Abgabe gegen aerztliche Verschreibung" 
* concept[6].code = #100000157313
* concept[6].display = "Arzneimittel zur Abgabe gegen besondere aerztliche Verschreibung, Suchtgifte"
* concept[6].designation[0].language = #de-AT 
* concept[6].designation[0].value = "Arzneimittel zur Abgabe gegen besondere aerztliche Verschreibung, Suchtgifte" 
* concept[7].code = #900000000002
* concept[7].display = "Arzneimittel zur Abgabe gegen besondere aerztliche Verschreibung, Substitution"
* concept[7].designation[0].language = #de-AT 
* concept[7].designation[0].value = "Arzneimittel zur Abgabe gegen besondere aerztliche Verschreibung, Substitution" 
