Instance: medikationrezeptpflichtstatus 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-medikationrezeptpflichtstatus" 
* name = "medikationrezeptpflichtstatus" 
* title = "MedikationRezeptpflichtstatus" 
* status = #active 
* content = #complete 
* version = "20211126" 
* description = "**Description:** Medikation Rezeptpflichtstatus

**Beschreibung:** The information concerning this list is reproduced from the EUTCT repository with permission of the EMA (European Medicines Agency)." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.10.1.4.3.4.3.7" 
* date = "2021-11-26" 
* count = 8 
* #100000072076 "Arzneimittel zur Abgabe ohne aerztliche Verschreibung"
* #100000072076 ^designation[0].language = #de-AT 
* #100000072076 ^designation[0].value = "Arzneimittel zur Abgabe ohne aerztliche Verschreibung" 
* #100000072077 "Arzneimittel zur einmaligen oder wiederholten Abgabe gegen aerztliche Verschreibung"
* #100000072077 ^designation[0].language = #de-AT 
* #100000072077 ^designation[0].value = "Arzneimittel zur einmaligen oder wiederholten Abgabe gegen aerztliche Verschreibung" 
* #100000072078 "Arzneimittel zur eingeschraenkten Abgabe gegen aerztliche Verschreibung, deren Anwendung definierten Spezialisten vorbehalten ist"
* #100000072078 ^designation[0].language = #de-AT 
* #100000072078 ^designation[0].value = "Arzneimittel zur eingeschraenkten Abgabe gegen aerztliche Verschreibung, deren Anwendung definierten Spezialisten vorbehalten ist" 
* #100000072079 "Arzneimittel zur einmaligen Abgabe auf aerztliche Verschreibung"
* #100000072079 ^designation[0].language = #de-AT 
* #100000072079 ^designation[0].value = "Arzneimittel zur einmaligen Abgabe auf aerztliche Verschreibung" 
* #100000072084 "Arzneimittel zur Abgabe gegen aerztliche Verschreibung"
* #100000072084 ^designation[0].language = #de-AT 
* #100000072084 ^designation[0].value = "Arzneimittel zur Abgabe gegen aerztliche Verschreibung" 
* #100000072086 "Arzneimittel zur wiederholten Abgabe gegen aerztliche Verschreibung"
* #100000072086 ^designation[0].language = #de-AT 
* #100000072086 ^designation[0].value = "Arzneimittel zur wiederholten Abgabe gegen aerztliche Verschreibung" 
* #100000157313 "Arzneimittel zur Abgabe gegen besondere aerztliche Verschreibung, Suchtgifte"
* #100000157313 ^designation[0].language = #de-AT 
* #100000157313 ^designation[0].value = "Arzneimittel zur Abgabe gegen besondere aerztliche Verschreibung, Suchtgifte" 
* #900000000002 "Arzneimittel zur Abgabe gegen besondere aerztliche Verschreibung, Substitution"
* #900000000002 ^designation[0].language = #de-AT 
* #900000000002 ^designation[0].value = "Arzneimittel zur Abgabe gegen besondere aerztliche Verschreibung, Substitution" 
