Instance: elga-artderdiagnose 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-artderdiagnose" 
* name = "elga-artderdiagnose" 
* title = "ELGA_ArtderDiagnose" 
* status = #active 
* version = "202005" 
* description = "**Description:** This list shows the different types of diagnoses. Contains information protected by copyright of SNOMED International. Any use of SNOMED CT in Austria requires a valid affiliate license or sublicense. The corresponding license is free of charge, provided that the use only takes place in Austria and fulfills the conditions of the Affiliate License Agreement. Affiliate licenses can be requested directly from the respective NRC via the [Member Licensing and Distribution Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS).

**Beschreibung:** In dieser Liste sind die unterschiedlichen Arten der Diagnosen abgebildet. Enthält durch SNOMED International urheberrechtlich geschützte Information. Jede Verwendung von SNOMED CT in Österreich erfordert eine aufrechte Affiliate Lizenz oder eine Sublizenz. Die entsprechende Lizenz ist kostenlos, vorausgesetzt die Verwendung findet nur in Österreich statt und erfüllt die Bedingungen des Affiliate License Agreements. Affiliate Lizenzen können über das [Mitglieder-Lizenzierungs- und Distributions-Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS) direkt beim jeweiligen NRC beantragt werden." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.23" 
* date = "2020-05-26" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* compose.include[0].concept[0].code = "406523004"
* compose.include[0].concept[0].display = "Überweisungsdiagnose"
* compose.include[0].concept[1].code = "52870002"
* compose.include[0].concept[1].display = "Aufnahmediagnose"
* compose.include[0].concept[2].code = "6934004"
* compose.include[0].concept[2].display = "Dauerdiagnose"
* compose.include[0].concept[3].code = "8319008"
* compose.include[0].concept[3].display = "Hauptdiagnose"
* compose.include[0].concept[4].code = "85097005"
* compose.include[0].concept[4].display = "Nebendiagnose"
