Instance: hl7-participation-type 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-participation-type" 
* name = "hl7-participation-type" 
* title = "HL7 Participation Type" 
* status = #active 
* content = #complete 
* version = "HL7:ParticipationType" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.5.90" 
* date = "2013-01-10" 
* count = 60 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #PART "Participation"
* #PART ^property[0].code = #child 
* #PART ^property[0].valueCode = #CST 
* #PART ^property[1].code = #child 
* #PART ^property[1].valueCode = #DIR 
* #PART ^property[2].code = #child 
* #PART ^property[2].valueCode = #IND 
* #PART ^property[3].code = #child 
* #PART ^property[3].valueCode = #IRCP 
* #PART ^property[4].code = #child 
* #PART ^property[4].valueCode = #LOC 
* #PART ^property[5].code = #child 
* #PART ^property[5].valueCode = #PRF 
* #PART ^property[6].code = #child 
* #PART ^property[6].valueCode = #RESP 
* #PART ^property[7].code = #child 
* #PART ^property[7].valueCode = #VRF 
* #PART ^property[8].code = #child 
* #PART ^property[8].valueCode = #_ParticipationAncillary 
* #PART ^property[9].code = #child 
* #PART ^property[9].valueCode = #_ParticipationInformationGenerator 
* #CST "custodian"
* #CST ^property[0].code = #parent 
* #CST ^property[0].valueCode = #PART 
* #DIR "direct target"
* #DIR ^property[0].code = #parent 
* #DIR ^property[0].valueCode = #PART 
* #DIR ^property[1].code = #child 
* #DIR ^property[1].valueCode = #ALY 
* #DIR ^property[2].code = #child 
* #DIR ^property[2].valueCode = #BBY 
* #DIR ^property[3].code = #child 
* #DIR ^property[3].valueCode = #CAT 
* #DIR ^property[4].code = #child 
* #DIR ^property[4].valueCode = #CSM 
* #DIR ^property[5].code = #child 
* #DIR ^property[5].valueCode = #DEV 
* #DIR ^property[6].code = #child 
* #DIR ^property[6].valueCode = #DON 
* #DIR ^property[7].code = #child 
* #DIR ^property[7].valueCode = #EXPAGNT 
* #DIR ^property[8].code = #child 
* #DIR ^property[8].valueCode = #EXPART 
* #DIR ^property[9].code = #child 
* #DIR ^property[9].valueCode = #PRD 
* #DIR ^property[10].code = #child 
* #DIR ^property[10].valueCode = #SBJ 
* #ALY "analyte"
* #ALY ^property[0].code = #parent 
* #ALY ^property[0].valueCode = #DIR 
* #BBY "baby"
* #BBY ^property[0].code = #parent 
* #BBY ^property[0].valueCode = #DIR 
* #CAT "catalyst"
* #CAT ^property[0].code = #parent 
* #CAT ^property[0].valueCode = #DIR 
* #CSM "consumable"
* #CSM ^property[0].code = #parent 
* #CSM ^property[0].valueCode = #DIR 
* #DEV "device"
* #DEV ^property[0].code = #parent 
* #DEV ^property[0].valueCode = #DIR 
* #DEV ^property[1].code = #child 
* #DEV ^property[1].valueCode = #NRD 
* #DEV ^property[2].code = #child 
* #DEV ^property[2].valueCode = #RDV 
* #NRD "non-reuseable device"
* #NRD ^property[0].code = #parent 
* #NRD ^property[0].valueCode = #DEV 
* #RDV "reusable device"
* #RDV ^property[0].code = #parent 
* #RDV ^property[0].valueCode = #DEV 
* #DON "donor"
* #DON ^property[0].code = #parent 
* #DON ^property[0].valueCode = #DIR 
* #EXPAGNT "ExposureAgent"
* #EXPAGNT ^property[0].code = #parent 
* #EXPAGNT ^property[0].valueCode = #DIR 
* #EXPART "ExposureParticipation"
* #EXPART ^property[0].code = #parent 
* #EXPART ^property[0].valueCode = #DIR 
* #EXPART ^property[1].code = #child 
* #EXPART ^property[1].valueCode = #EXPTRGT 
* #EXPART ^property[2].code = #child 
* #EXPART ^property[2].valueCode = #EXSRC 
* #EXPTRGT "ExposureTarget"
* #EXPTRGT ^property[0].code = #parent 
* #EXPTRGT ^property[0].valueCode = #EXPART 
* #EXSRC "ExposureSource"
* #EXSRC ^property[0].code = #parent 
* #EXSRC ^property[0].valueCode = #EXPART 
* #PRD "product"
* #PRD ^property[0].code = #parent 
* #PRD ^property[0].valueCode = #DIR 
* #SBJ "subject"
* #SBJ ^property[0].code = #parent 
* #SBJ ^property[0].valueCode = #DIR 
* #SBJ ^property[1].code = #child 
* #SBJ ^property[1].valueCode = #SPC 
* #SPC "specimen"
* #SPC ^property[0].code = #parent 
* #SPC ^property[0].valueCode = #SBJ 
* #IND "indirect target"
* #IND ^property[0].code = #parent 
* #IND ^property[0].valueCode = #PART 
* #IND ^property[1].code = #child 
* #IND ^property[1].valueCode = #BEN 
* #IND ^property[2].code = #child 
* #IND ^property[2].valueCode = #CAGNT 
* #IND ^property[3].code = #child 
* #IND ^property[3].valueCode = #COV 
* #IND ^property[4].code = #child 
* #IND ^property[4].valueCode = #GUAR 
* #IND ^property[5].code = #child 
* #IND ^property[5].valueCode = #HLD 
* #IND ^property[6].code = #child 
* #IND ^property[6].valueCode = #RCT 
* #IND ^property[7].code = #child 
* #IND ^property[7].valueCode = #RCV 
* #BEN "beneficiary"
* #BEN ^property[0].code = #parent 
* #BEN ^property[0].valueCode = #IND 
* #CAGNT "causative agent"
* #CAGNT ^property[0].code = #parent 
* #CAGNT ^property[0].valueCode = #IND 
* #COV "coverage target"
* #COV ^property[0].code = #parent 
* #COV ^property[0].valueCode = #IND 
* #GUAR "guarantor party"
* #GUAR ^property[0].code = #parent 
* #GUAR ^property[0].valueCode = #IND 
* #HLD "holder"
* #HLD ^property[0].code = #parent 
* #HLD ^property[0].valueCode = #IND 
* #RCT "record target"
* #RCT ^property[0].code = #parent 
* #RCT ^property[0].valueCode = #IND 
* #RCV "receiver"
* #RCV ^property[0].code = #parent 
* #RCV ^property[0].valueCode = #IND 
* #IRCP "information recipient"
* #IRCP ^property[0].code = #parent 
* #IRCP ^property[0].valueCode = #PART 
* #IRCP ^property[1].code = #child 
* #IRCP ^property[1].valueCode = #NOT 
* #IRCP ^property[2].code = #child 
* #IRCP ^property[2].valueCode = #PRCP 
* #IRCP ^property[3].code = #child 
* #IRCP ^property[3].valueCode = #REFB 
* #IRCP ^property[4].code = #child 
* #IRCP ^property[4].valueCode = #REFT 
* #IRCP ^property[5].code = #child 
* #IRCP ^property[5].valueCode = #TRC 
* #NOT "ugent notification contact"
* #NOT ^property[0].code = #parent 
* #NOT ^property[0].valueCode = #IRCP 
* #PRCP "primary information recipient"
* #PRCP ^property[0].code = #parent 
* #PRCP ^property[0].valueCode = #IRCP 
* #REFB "Referred By"
* #REFB ^property[0].code = #parent 
* #REFB ^property[0].valueCode = #IRCP 
* #REFT "Referred to"
* #REFT ^property[0].code = #parent 
* #REFT ^property[0].valueCode = #IRCP 
* #TRC "tracker"
* #TRC ^property[0].code = #parent 
* #TRC ^property[0].valueCode = #IRCP 
* #LOC "location"
* #LOC ^property[0].code = #parent 
* #LOC ^property[0].valueCode = #PART 
* #LOC ^property[1].code = #child 
* #LOC ^property[1].valueCode = #DST 
* #LOC ^property[2].code = #child 
* #LOC ^property[2].valueCode = #ELOC 
* #LOC ^property[3].code = #child 
* #LOC ^property[3].valueCode = #ORG 
* #LOC ^property[4].code = #child 
* #LOC ^property[4].valueCode = #RML 
* #LOC ^property[5].code = #child 
* #LOC ^property[5].valueCode = #VIA 
* #DST "destination"
* #DST ^property[0].code = #parent 
* #DST ^property[0].valueCode = #LOC 
* #ELOC "entry location"
* #ELOC ^property[0].code = #parent 
* #ELOC ^property[0].valueCode = #LOC 
* #ORG "origin"
* #ORG ^property[0].code = #parent 
* #ORG ^property[0].valueCode = #LOC 
* #RML "remote"
* #RML ^property[0].code = #parent 
* #RML ^property[0].valueCode = #LOC 
* #VIA "via"
* #VIA ^property[0].code = #parent 
* #VIA ^property[0].valueCode = #LOC 
* #PRF "performer"
* #PRF ^property[0].code = #parent 
* #PRF ^property[0].valueCode = #PART 
* #PRF ^property[1].code = #child 
* #PRF ^property[1].valueCode = #DIST 
* #PRF ^property[2].code = #child 
* #PRF ^property[2].valueCode = #PPRF 
* #PRF ^property[3].code = #child 
* #PRF ^property[3].valueCode = #SPRF 
* #DIST "distributor"
* #DIST ^property[0].code = #parent 
* #DIST ^property[0].valueCode = #PRF 
* #PPRF "primary performer"
* #PPRF ^property[0].code = #parent 
* #PPRF ^property[0].valueCode = #PRF 
* #SPRF "secondary performer"
* #SPRF ^property[0].code = #parent 
* #SPRF ^property[0].valueCode = #PRF 
* #RESP "responsible party"
* #RESP ^property[0].code = #parent 
* #RESP ^property[0].valueCode = #PART 
* #VRF "verifier"
* #VRF ^property[0].code = #parent 
* #VRF ^property[0].valueCode = #PART 
* #VRF ^property[1].code = #child 
* #VRF ^property[1].valueCode = #AUTHEN 
* #VRF ^property[2].code = #child 
* #VRF ^property[2].valueCode = #LA 
* #AUTHEN "authenticator"
* #AUTHEN ^property[0].code = #parent 
* #AUTHEN ^property[0].valueCode = #VRF 
* #LA "legal authenticator"
* #LA ^property[0].code = #parent 
* #LA ^property[0].valueCode = #VRF 
* #_ParticipationAncillary "ParticipationAncillary"
* #_ParticipationAncillary ^property[0].code = #parent 
* #_ParticipationAncillary ^property[0].valueCode = #PART 
* #_ParticipationAncillary ^property[1].code = #child 
* #_ParticipationAncillary ^property[1].valueCode = #ADM 
* #_ParticipationAncillary ^property[2].code = #child 
* #_ParticipationAncillary ^property[2].valueCode = #ATND 
* #_ParticipationAncillary ^property[3].code = #child 
* #_ParticipationAncillary ^property[3].valueCode = #CALLBCK 
* #_ParticipationAncillary ^property[4].code = #child 
* #_ParticipationAncillary ^property[4].valueCode = #CON 
* #_ParticipationAncillary ^property[5].code = #child 
* #_ParticipationAncillary ^property[5].valueCode = #DIS 
* #_ParticipationAncillary ^property[6].code = #child 
* #_ParticipationAncillary ^property[6].valueCode = #ESC 
* #_ParticipationAncillary ^property[7].code = #child 
* #_ParticipationAncillary ^property[7].valueCode = #REF 
* #ADM "admitter"
* #ADM ^property[0].code = #parent 
* #ADM ^property[0].valueCode = #_ParticipationAncillary 
* #ATND "attender"
* #ATND ^property[0].code = #parent 
* #ATND ^property[0].valueCode = #_ParticipationAncillary 
* #CALLBCK "callback contact"
* #CALLBCK ^property[0].code = #parent 
* #CALLBCK ^property[0].valueCode = #_ParticipationAncillary 
* #CON "consultant"
* #CON ^property[0].code = #parent 
* #CON ^property[0].valueCode = #_ParticipationAncillary 
* #DIS "discharger"
* #DIS ^property[0].code = #parent 
* #DIS ^property[0].valueCode = #_ParticipationAncillary 
* #ESC "escort"
* #ESC ^property[0].code = #parent 
* #ESC ^property[0].valueCode = #_ParticipationAncillary 
* #REF "referrer"
* #REF ^property[0].code = #parent 
* #REF ^property[0].valueCode = #_ParticipationAncillary 
* #_ParticipationInformationGenerator "ParticipationInformationGenerator"
* #_ParticipationInformationGenerator ^property[0].code = #parent 
* #_ParticipationInformationGenerator ^property[0].valueCode = #PART 
* #_ParticipationInformationGenerator ^property[1].code = #child 
* #_ParticipationInformationGenerator ^property[1].valueCode = #AUT 
* #_ParticipationInformationGenerator ^property[2].code = #child 
* #_ParticipationInformationGenerator ^property[2].valueCode = #INF 
* #_ParticipationInformationGenerator ^property[3].code = #child 
* #_ParticipationInformationGenerator ^property[3].valueCode = #TRANS 
* #_ParticipationInformationGenerator ^property[4].code = #child 
* #_ParticipationInformationGenerator ^property[4].valueCode = #WIT 
* #AUT "author (originator)"
* #AUT ^property[0].code = #parent 
* #AUT ^property[0].valueCode = #_ParticipationInformationGenerator 
* #INF "informant"
* #INF ^property[0].code = #parent 
* #INF ^property[0].valueCode = #_ParticipationInformationGenerator 
* #TRANS "Transcriber"
* #TRANS ^property[0].code = #parent 
* #TRANS ^property[0].valueCode = #_ParticipationInformationGenerator 
* #TRANS ^property[1].code = #child 
* #TRANS ^property[1].valueCode = #ENT 
* #ENT "data entry person"
* #ENT ^property[0].code = #parent 
* #ENT ^property[0].valueCode = #TRANS 
* #WIT "witness"
* #WIT ^property[0].code = #parent 
* #WIT ^property[0].valueCode = #_ParticipationInformationGenerator 
