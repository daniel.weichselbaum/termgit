Instance: mdc-medicaldevicecommunications 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-mdc-medicaldevicecommunications" 
* name = "mdc-medicaldevicecommunications" 
* title = "MDC_MedicalDeviceCommunications" 
* status = #active 
* content = #complete 
* version = "202102" 
* description = "**Description:** HL7-Austria and ELGA GmbH would like to thank IEEE SA for providing their CodeSystem to the Austrian eHealth Terminology Server.  The information mapped in this code system is provided free of charge to all eHealth Terminology Server users via the IEEE SA and HL7-Austria / ELGA GmbH Royalty Free Agreement. Any other information, whether from the designated standards or any other standard, shall require separate permission from IEEE.  Medical Device Communications (MDC) codes are from ISO/IEEE 11073-10101 Nomenclature standard and amendments. This is a detailed system of codes used in personal health devices and and acute-care (point-of-care) medical devices for identification of physiological measurements and also for alerts, alarms, and numerous technical conditions such as calibration state and battery state.

**Beschreibung:** HL7-Austria und die ELGA GmbH danken der IEEE SA für die Bereitstellung ihres CodesSystems am österreichischen eHealth Terminologie Server.  Die in diesem CodeSystem abgebildeten Informationen werden allen eHealth Termnologie Server-Benutzern über das IEEE-SA und HL7-Austria / ELGA GmbH Royalty Free Agreement kostenlos zur Verfügung gestellt. Jede andere Informationen, ob aus den bezeichneten Normen oder aus anderen Normen, bedürfen einer gesonderten Genehmigung durch IEEE.  Die MDC-Codes (Medical Device Communications) stammen aus der ISO/IEEE-Nomenklaturnorm 11073-10101 und deren Ergänzungen. Dabei handelt es sich um ein detailliertes System von Codes, die in persönlichen Gesundheitsgeräten und medizinischen Geräten für die Akutversorgung (Point-of-Care) zur Identifizierung von physiologischen Messungen und auch für Warnungen, Alarme und zahlreiche technische Zustände wie Kalibrierungszustand und Batteriezustand verwendet werden." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.6.24" 
* date = "2021-02-03" 
* copyright = "Copyright © 2021 by The Institute of Electrical and Electronics Engineers, Incorporated Three Park Avenue New York, New York 10016-5997, USA All rights reserved." 
* count = 1698 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* property[2].code = #hints 
* property[2].type = #string 
* #_mdc "MDC"
* #_mdc ^property[0].code = #child 
* #_mdc ^property[0].valueCode = #131840 
* #_mdc ^property[1].code = #child 
* #_mdc ^property[1].valueCode = #131841 
* #_mdc ^property[2].code = #child 
* #_mdc ^property[2].valueCode = #131842 
* #_mdc ^property[3].code = #child 
* #_mdc ^property[3].valueCode = #131843 
* #_mdc ^property[4].code = #child 
* #_mdc ^property[4].valueCode = #131844 
* #_mdc ^property[5].code = #child 
* #_mdc ^property[5].valueCode = #131845 
* #_mdc ^property[6].code = #child 
* #_mdc ^property[6].valueCode = #131846 
* #_mdc ^property[7].code = #child 
* #_mdc ^property[7].valueCode = #131847 
* #_mdc ^property[8].code = #child 
* #_mdc ^property[8].valueCode = #131848 
* #_mdc ^property[9].code = #child 
* #_mdc ^property[9].valueCode = #131873 
* #_mdc ^property[10].code = #child 
* #_mdc ^property[10].valueCode = #131874 
* #_mdc ^property[11].code = #child 
* #_mdc ^property[11].valueCode = #131875 
* #_mdc ^property[12].code = #child 
* #_mdc ^property[12].valueCode = #131876 
* #_mdc ^property[13].code = #child 
* #_mdc ^property[13].valueCode = #131877 
* #_mdc ^property[14].code = #child 
* #_mdc ^property[14].valueCode = #131878 
* #_mdc ^property[15].code = #child 
* #_mdc ^property[15].valueCode = #131901 
* #_mdc ^property[16].code = #child 
* #_mdc ^property[16].valueCode = #131902 
* #_mdc ^property[17].code = #child 
* #_mdc ^property[17].valueCode = #131903 
* #_mdc ^property[18].code = #child 
* #_mdc ^property[18].valueCode = #131904 
* #_mdc ^property[19].code = #child 
* #_mdc ^property[19].valueCode = #131927 
* #_mdc ^property[20].code = #child 
* #_mdc ^property[20].valueCode = #131931 
* #_mdc ^property[21].code = #child 
* #_mdc ^property[21].valueCode = #131932 
* #_mdc ^property[22].code = #child 
* #_mdc ^property[22].valueCode = #131937 
* #_mdc ^property[23].code = #child 
* #_mdc ^property[23].valueCode = #131971 
* #_mdc ^property[24].code = #child 
* #_mdc ^property[24].valueCode = #131972 
* #_mdc ^property[25].code = #child 
* #_mdc ^property[25].valueCode = #131973 
* #_mdc ^property[26].code = #child 
* #_mdc ^property[26].valueCode = #147232 
* #_mdc ^property[27].code = #child 
* #_mdc ^property[27].valueCode = #147236 
* #_mdc ^property[28].code = #child 
* #_mdc ^property[28].valueCode = #147240 
* #_mdc ^property[29].code = #child 
* #_mdc ^property[29].valueCode = #147626 
* #_mdc ^property[30].code = #child 
* #_mdc ^property[30].valueCode = #147842 
* #_mdc ^property[31].code = #child 
* #_mdc ^property[31].valueCode = #147850 
* #_mdc ^property[32].code = #child 
* #_mdc ^property[32].valueCode = #148065 
* #_mdc ^property[33].code = #child 
* #_mdc ^property[33].valueCode = #148066 
* #_mdc ^property[34].code = #child 
* #_mdc ^property[34].valueCode = #148496 
* #_mdc ^property[35].code = #child 
* #_mdc ^property[35].valueCode = #149514 
* #_mdc ^property[36].code = #child 
* #_mdc ^property[36].valueCode = #149522 
* #_mdc ^property[37].code = #child 
* #_mdc ^property[37].valueCode = #149530 
* #_mdc ^property[38].code = #child 
* #_mdc ^property[38].valueCode = #149546 
* #_mdc ^property[39].code = #child 
* #_mdc ^property[39].valueCode = #149554 
* #_mdc ^property[40].code = #child 
* #_mdc ^property[40].valueCode = #149562 
* #_mdc ^property[41].code = #child 
* #_mdc ^property[41].valueCode = #149760 
* #_mdc ^property[42].code = #child 
* #_mdc ^property[42].valueCode = #149764 
* #_mdc ^property[43].code = #child 
* #_mdc ^property[43].valueCode = #149772 
* #_mdc ^property[44].code = #child 
* #_mdc ^property[44].valueCode = #150016 
* #_mdc ^property[45].code = #child 
* #_mdc ^property[45].valueCode = #150017 
* #_mdc ^property[46].code = #child 
* #_mdc ^property[46].valueCode = #150018 
* #_mdc ^property[47].code = #child 
* #_mdc ^property[47].valueCode = #150019 
* #_mdc ^property[48].code = #child 
* #_mdc ^property[48].valueCode = #150020 
* #_mdc ^property[49].code = #child 
* #_mdc ^property[49].valueCode = #150021 
* #_mdc ^property[50].code = #child 
* #_mdc ^property[50].valueCode = #150022 
* #_mdc ^property[51].code = #child 
* #_mdc ^property[51].valueCode = #150023 
* #_mdc ^property[52].code = #child 
* #_mdc ^property[52].valueCode = #150025 
* #_mdc ^property[53].code = #child 
* #_mdc ^property[53].valueCode = #150026 
* #_mdc ^property[54].code = #child 
* #_mdc ^property[54].valueCode = #150027 
* #_mdc ^property[55].code = #child 
* #_mdc ^property[55].valueCode = #150028 
* #_mdc ^property[56].code = #child 
* #_mdc ^property[56].valueCode = #150029 
* #_mdc ^property[57].code = #child 
* #_mdc ^property[57].valueCode = #150030 
* #_mdc ^property[58].code = #child 
* #_mdc ^property[58].valueCode = #150031 
* #_mdc ^property[59].code = #child 
* #_mdc ^property[59].valueCode = #150032 
* #_mdc ^property[60].code = #child 
* #_mdc ^property[60].valueCode = #150033 
* #_mdc ^property[61].code = #child 
* #_mdc ^property[61].valueCode = #150034 
* #_mdc ^property[62].code = #child 
* #_mdc ^property[62].valueCode = #150035 
* #_mdc ^property[63].code = #child 
* #_mdc ^property[63].valueCode = #150036 
* #_mdc ^property[64].code = #child 
* #_mdc ^property[64].valueCode = #150037 
* #_mdc ^property[65].code = #child 
* #_mdc ^property[65].valueCode = #150038 
* #_mdc ^property[66].code = #child 
* #_mdc ^property[66].valueCode = #150039 
* #_mdc ^property[67].code = #child 
* #_mdc ^property[67].valueCode = #150044 
* #_mdc ^property[68].code = #child 
* #_mdc ^property[68].valueCode = #150045 
* #_mdc ^property[69].code = #child 
* #_mdc ^property[69].valueCode = #150046 
* #_mdc ^property[70].code = #child 
* #_mdc ^property[70].valueCode = #150047 
* #_mdc ^property[71].code = #child 
* #_mdc ^property[71].valueCode = #150052 
* #_mdc ^property[72].code = #child 
* #_mdc ^property[72].valueCode = #150056 
* #_mdc ^property[73].code = #child 
* #_mdc ^property[73].valueCode = #150057 
* #_mdc ^property[74].code = #child 
* #_mdc ^property[74].valueCode = #150058 
* #_mdc ^property[75].code = #child 
* #_mdc ^property[75].valueCode = #150059 
* #_mdc ^property[76].code = #child 
* #_mdc ^property[76].valueCode = #150064 
* #_mdc ^property[77].code = #child 
* #_mdc ^property[77].valueCode = #150065 
* #_mdc ^property[78].code = #child 
* #_mdc ^property[78].valueCode = #150066 
* #_mdc ^property[79].code = #child 
* #_mdc ^property[79].valueCode = #150067 
* #_mdc ^property[80].code = #child 
* #_mdc ^property[80].valueCode = #150068 
* #_mdc ^property[81].code = #child 
* #_mdc ^property[81].valueCode = #150069 
* #_mdc ^property[82].code = #child 
* #_mdc ^property[82].valueCode = #150070 
* #_mdc ^property[83].code = #child 
* #_mdc ^property[83].valueCode = #150071 
* #_mdc ^property[84].code = #child 
* #_mdc ^property[84].valueCode = #150076 
* #_mdc ^property[85].code = #child 
* #_mdc ^property[85].valueCode = #150077 
* #_mdc ^property[86].code = #child 
* #_mdc ^property[86].valueCode = #150078 
* #_mdc ^property[87].code = #child 
* #_mdc ^property[87].valueCode = #150079 
* #_mdc ^property[88].code = #child 
* #_mdc ^property[88].valueCode = #150084 
* #_mdc ^property[89].code = #child 
* #_mdc ^property[89].valueCode = #150085 
* #_mdc ^property[90].code = #child 
* #_mdc ^property[90].valueCode = #150086 
* #_mdc ^property[91].code = #child 
* #_mdc ^property[91].valueCode = #150087 
* #_mdc ^property[92].code = #child 
* #_mdc ^property[92].valueCode = #150088 
* #_mdc ^property[93].code = #child 
* #_mdc ^property[93].valueCode = #150089 
* #_mdc ^property[94].code = #child 
* #_mdc ^property[94].valueCode = #150090 
* #_mdc ^property[95].code = #child 
* #_mdc ^property[95].valueCode = #150091 
* #_mdc ^property[96].code = #child 
* #_mdc ^property[96].valueCode = #150101 
* #_mdc ^property[97].code = #child 
* #_mdc ^property[97].valueCode = #150102 
* #_mdc ^property[98].code = #child 
* #_mdc ^property[98].valueCode = #150103 
* #_mdc ^property[99].code = #child 
* #_mdc ^property[99].valueCode = #150105 
* #_mdc ^property[100].code = #child 
* #_mdc ^property[100].valueCode = #150106 
* #_mdc ^property[101].code = #child 
* #_mdc ^property[101].valueCode = #150107 
* #_mdc ^property[102].code = #child 
* #_mdc ^property[102].valueCode = #150272 
* #_mdc ^property[103].code = #child 
* #_mdc ^property[103].valueCode = #150276 
* #_mdc ^property[104].code = #child 
* #_mdc ^property[104].valueCode = #150300 
* #_mdc ^property[105].code = #child 
* #_mdc ^property[105].valueCode = #150301 
* #_mdc ^property[106].code = #child 
* #_mdc ^property[106].valueCode = #150302 
* #_mdc ^property[107].code = #child 
* #_mdc ^property[107].valueCode = #150303 
* #_mdc ^property[108].code = #child 
* #_mdc ^property[108].valueCode = #150304 
* #_mdc ^property[109].code = #child 
* #_mdc ^property[109].valueCode = #150308 
* #_mdc ^property[110].code = #child 
* #_mdc ^property[110].valueCode = #150312 
* #_mdc ^property[111].code = #child 
* #_mdc ^property[111].valueCode = #150320 
* #_mdc ^property[112].code = #child 
* #_mdc ^property[112].valueCode = #150324 
* #_mdc ^property[113].code = #child 
* #_mdc ^property[113].valueCode = #150332 
* #_mdc ^property[114].code = #child 
* #_mdc ^property[114].valueCode = #150336 
* #_mdc ^property[115].code = #child 
* #_mdc ^property[115].valueCode = #150344 
* #_mdc ^property[116].code = #child 
* #_mdc ^property[116].valueCode = #150348 
* #_mdc ^property[117].code = #child 
* #_mdc ^property[117].valueCode = #150352 
* #_mdc ^property[118].code = #child 
* #_mdc ^property[118].valueCode = #150356 
* #_mdc ^property[119].code = #child 
* #_mdc ^property[119].valueCode = #150364 
* #_mdc ^property[120].code = #child 
* #_mdc ^property[120].valueCode = #150368 
* #_mdc ^property[121].code = #child 
* #_mdc ^property[121].valueCode = #150372 
* #_mdc ^property[122].code = #child 
* #_mdc ^property[122].valueCode = #150376 
* #_mdc ^property[123].code = #child 
* #_mdc ^property[123].valueCode = #150380 
* #_mdc ^property[124].code = #child 
* #_mdc ^property[124].valueCode = #150388 
* #_mdc ^property[125].code = #child 
* #_mdc ^property[125].valueCode = #150392 
* #_mdc ^property[126].code = #child 
* #_mdc ^property[126].valueCode = #150396 
* #_mdc ^property[127].code = #child 
* #_mdc ^property[127].valueCode = #150404 
* #_mdc ^property[128].code = #child 
* #_mdc ^property[128].valueCode = #150416 
* #_mdc ^property[129].code = #child 
* #_mdc ^property[129].valueCode = #150420 
* #_mdc ^property[130].code = #child 
* #_mdc ^property[130].valueCode = #150428 
* #_mdc ^property[131].code = #child 
* #_mdc ^property[131].valueCode = #150448 
* #_mdc ^property[132].code = #child 
* #_mdc ^property[132].valueCode = #150452 
* #_mdc ^property[133].code = #child 
* #_mdc ^property[133].valueCode = #150456 
* #_mdc ^property[134].code = #child 
* #_mdc ^property[134].valueCode = #150468 
* #_mdc ^property[135].code = #child 
* #_mdc ^property[135].valueCode = #150472 
* #_mdc ^property[136].code = #child 
* #_mdc ^property[136].valueCode = #150476 
* #_mdc ^property[137].code = #child 
* #_mdc ^property[137].valueCode = #150484 
* #_mdc ^property[138].code = #child 
* #_mdc ^property[138].valueCode = #150488 
* #_mdc ^property[139].code = #child 
* #_mdc ^property[139].valueCode = #150492 
* #_mdc ^property[140].code = #child 
* #_mdc ^property[140].valueCode = #150496 
* #_mdc ^property[141].code = #child 
* #_mdc ^property[141].valueCode = #150520 
* #_mdc ^property[142].code = #child 
* #_mdc ^property[142].valueCode = #150528 
* #_mdc ^property[143].code = #child 
* #_mdc ^property[143].valueCode = #150532 
* #_mdc ^property[144].code = #child 
* #_mdc ^property[144].valueCode = #150565 
* #_mdc ^property[145].code = #child 
* #_mdc ^property[145].valueCode = #150568 
* #_mdc ^property[146].code = #child 
* #_mdc ^property[146].valueCode = #150572 
* #_mdc ^property[147].code = #child 
* #_mdc ^property[147].valueCode = #150576 
* #_mdc ^property[148].code = #child 
* #_mdc ^property[148].valueCode = #150580 
* #_mdc ^property[149].code = #child 
* #_mdc ^property[149].valueCode = #150584 
* #_mdc ^property[150].code = #child 
* #_mdc ^property[150].valueCode = #150588 
* #_mdc ^property[151].code = #child 
* #_mdc ^property[151].valueCode = #150604 
* #_mdc ^property[152].code = #child 
* #_mdc ^property[152].valueCode = #150605 
* #_mdc ^property[153].code = #child 
* #_mdc ^property[153].valueCode = #150612 
* #_mdc ^property[154].code = #child 
* #_mdc ^property[154].valueCode = #150616 
* #_mdc ^property[155].code = #child 
* #_mdc ^property[155].valueCode = #150620 
* #_mdc ^property[156].code = #child 
* #_mdc ^property[156].valueCode = #150624 
* #_mdc ^property[157].code = #child 
* #_mdc ^property[157].valueCode = #150628 
* #_mdc ^property[158].code = #child 
* #_mdc ^property[158].valueCode = #150632 
* #_mdc ^property[159].code = #child 
* #_mdc ^property[159].valueCode = #150636 
* #_mdc ^property[160].code = #child 
* #_mdc ^property[160].valueCode = #150640 
* #_mdc ^property[161].code = #child 
* #_mdc ^property[161].valueCode = #150644 
* #_mdc ^property[162].code = #child 
* #_mdc ^property[162].valueCode = #150648 
* #_mdc ^property[163].code = #child 
* #_mdc ^property[163].valueCode = #150649 
* #_mdc ^property[164].code = #child 
* #_mdc ^property[164].valueCode = #150650 
* #_mdc ^property[165].code = #child 
* #_mdc ^property[165].valueCode = #150651 
* #_mdc ^property[166].code = #child 
* #_mdc ^property[166].valueCode = #150652 
* #_mdc ^property[167].code = #child 
* #_mdc ^property[167].valueCode = #150656 
* #_mdc ^property[168].code = #child 
* #_mdc ^property[168].valueCode = #150660 
* #_mdc ^property[169].code = #child 
* #_mdc ^property[169].valueCode = #150664 
* #_mdc ^property[170].code = #child 
* #_mdc ^property[170].valueCode = #150668 
* #_mdc ^property[171].code = #child 
* #_mdc ^property[171].valueCode = #150672 
* #_mdc ^property[172].code = #child 
* #_mdc ^property[172].valueCode = #150676 
* #_mdc ^property[173].code = #child 
* #_mdc ^property[173].valueCode = #150680 
* #_mdc ^property[174].code = #child 
* #_mdc ^property[174].valueCode = #151562 
* #_mdc ^property[175].code = #child 
* #_mdc ^property[175].valueCode = #151570 
* #_mdc ^property[176].code = #child 
* #_mdc ^property[176].valueCode = #151578 
* #_mdc ^property[177].code = #child 
* #_mdc ^property[177].valueCode = #151586 
* #_mdc ^property[178].code = #child 
* #_mdc ^property[178].valueCode = #151594 
* #_mdc ^property[179].code = #child 
* #_mdc ^property[179].valueCode = #151602 
* #_mdc ^property[180].code = #child 
* #_mdc ^property[180].valueCode = #151610 
* #_mdc ^property[181].code = #child 
* #_mdc ^property[181].valueCode = #151618 
* #_mdc ^property[182].code = #child 
* #_mdc ^property[182].valueCode = #151626 
* #_mdc ^property[183].code = #child 
* #_mdc ^property[183].valueCode = #151634 
* #_mdc ^property[184].code = #child 
* #_mdc ^property[184].valueCode = #151642 
* #_mdc ^property[185].code = #child 
* #_mdc ^property[185].valueCode = #151650 
* #_mdc ^property[186].code = #child 
* #_mdc ^property[186].valueCode = #151658 
* #_mdc ^property[187].code = #child 
* #_mdc ^property[187].valueCode = #151666 
* #_mdc ^property[188].code = #child 
* #_mdc ^property[188].valueCode = #151674 
* #_mdc ^property[189].code = #child 
* #_mdc ^property[189].valueCode = #151680 
* #_mdc ^property[190].code = #child 
* #_mdc ^property[190].valueCode = #151684 
* #_mdc ^property[191].code = #child 
* #_mdc ^property[191].valueCode = #151688 
* #_mdc ^property[192].code = #child 
* #_mdc ^property[192].valueCode = #151692 
* #_mdc ^property[193].code = #child 
* #_mdc ^property[193].valueCode = #151696 
* #_mdc ^property[194].code = #child 
* #_mdc ^property[194].valueCode = #151700 
* #_mdc ^property[195].code = #child 
* #_mdc ^property[195].valueCode = #151708 
* #_mdc ^property[196].code = #child 
* #_mdc ^property[196].valueCode = #151712 
* #_mdc ^property[197].code = #child 
* #_mdc ^property[197].valueCode = #151716 
* #_mdc ^property[198].code = #child 
* #_mdc ^property[198].valueCode = #151718 
* #_mdc ^property[199].code = #child 
* #_mdc ^property[199].valueCode = #151744 
* #_mdc ^property[200].code = #child 
* #_mdc ^property[200].valueCode = #151756 
* #_mdc ^property[201].code = #child 
* #_mdc ^property[201].valueCode = #151760 
* #_mdc ^property[202].code = #child 
* #_mdc ^property[202].valueCode = #151764 
* #_mdc ^property[203].code = #child 
* #_mdc ^property[203].valueCode = #151768 
* #_mdc ^property[204].code = #child 
* #_mdc ^property[204].valueCode = #151769 
* #_mdc ^property[205].code = #child 
* #_mdc ^property[205].valueCode = #151772 
* #_mdc ^property[206].code = #child 
* #_mdc ^property[206].valueCode = #151773 
* #_mdc ^property[207].code = #child 
* #_mdc ^property[207].valueCode = #151776 
* #_mdc ^property[208].code = #child 
* #_mdc ^property[208].valueCode = #151780 
* #_mdc ^property[209].code = #child 
* #_mdc ^property[209].valueCode = #151784 
* #_mdc ^property[210].code = #child 
* #_mdc ^property[210].valueCode = #151788 
* #_mdc ^property[211].code = #child 
* #_mdc ^property[211].valueCode = #151792 
* #_mdc ^property[212].code = #child 
* #_mdc ^property[212].valueCode = #151793 
* #_mdc ^property[213].code = #child 
* #_mdc ^property[213].valueCode = #151794 
* #_mdc ^property[214].code = #child 
* #_mdc ^property[214].valueCode = #151795 
* #_mdc ^property[215].code = #child 
* #_mdc ^property[215].valueCode = #151796 
* #_mdc ^property[216].code = #child 
* #_mdc ^property[216].valueCode = #151801 
* #_mdc ^property[217].code = #child 
* #_mdc ^property[217].valueCode = #151804 
* #_mdc ^property[218].code = #child 
* #_mdc ^property[218].valueCode = #151808 
* #_mdc ^property[219].code = #child 
* #_mdc ^property[219].valueCode = #151812 
* #_mdc ^property[220].code = #child 
* #_mdc ^property[220].valueCode = #151813 
* #_mdc ^property[221].code = #child 
* #_mdc ^property[221].valueCode = #151814 
* #_mdc ^property[222].code = #child 
* #_mdc ^property[222].valueCode = #151816 
* #_mdc ^property[223].code = #child 
* #_mdc ^property[223].valueCode = #151817 
* #_mdc ^property[224].code = #child 
* #_mdc ^property[224].valueCode = #151818 
* #_mdc ^property[225].code = #child 
* #_mdc ^property[225].valueCode = #151819 
* #_mdc ^property[226].code = #child 
* #_mdc ^property[226].valueCode = #151820 
* #_mdc ^property[227].code = #child 
* #_mdc ^property[227].valueCode = #151824 
* #_mdc ^property[228].code = #child 
* #_mdc ^property[228].valueCode = #151828 
* #_mdc ^property[229].code = #child 
* #_mdc ^property[229].valueCode = #151832 
* #_mdc ^property[230].code = #child 
* #_mdc ^property[230].valueCode = #151836 
* #_mdc ^property[231].code = #child 
* #_mdc ^property[231].valueCode = #151840 
* #_mdc ^property[232].code = #child 
* #_mdc ^property[232].valueCode = #151844 
* #_mdc ^property[233].code = #child 
* #_mdc ^property[233].valueCode = #151848 
* #_mdc ^property[234].code = #child 
* #_mdc ^property[234].valueCode = #151852 
* #_mdc ^property[235].code = #child 
* #_mdc ^property[235].valueCode = #151856 
* #_mdc ^property[236].code = #child 
* #_mdc ^property[236].valueCode = #151860 
* #_mdc ^property[237].code = #child 
* #_mdc ^property[237].valueCode = #151864 
* #_mdc ^property[238].code = #child 
* #_mdc ^property[238].valueCode = #151868 
* #_mdc ^property[239].code = #child 
* #_mdc ^property[239].valueCode = #151872 
* #_mdc ^property[240].code = #child 
* #_mdc ^property[240].valueCode = #151876 
* #_mdc ^property[241].code = #child 
* #_mdc ^property[241].valueCode = #151880 
* #_mdc ^property[242].code = #child 
* #_mdc ^property[242].valueCode = #151884 
* #_mdc ^property[243].code = #child 
* #_mdc ^property[243].valueCode = #151888 
* #_mdc ^property[244].code = #child 
* #_mdc ^property[244].valueCode = #151908 
* #_mdc ^property[245].code = #child 
* #_mdc ^property[245].valueCode = #151912 
* #_mdc ^property[246].code = #child 
* #_mdc ^property[246].valueCode = #151940 
* #_mdc ^property[247].code = #child 
* #_mdc ^property[247].valueCode = #151944 
* #_mdc ^property[248].code = #child 
* #_mdc ^property[248].valueCode = #151945 
* #_mdc ^property[249].code = #child 
* #_mdc ^property[249].valueCode = #151948 
* #_mdc ^property[250].code = #child 
* #_mdc ^property[250].valueCode = #151949 
* #_mdc ^property[251].code = #child 
* #_mdc ^property[251].valueCode = #151952 
* #_mdc ^property[252].code = #child 
* #_mdc ^property[252].valueCode = #151956 
* #_mdc ^property[253].code = #child 
* #_mdc ^property[253].valueCode = #151957 
* #_mdc ^property[254].code = #child 
* #_mdc ^property[254].valueCode = #151958 
* #_mdc ^property[255].code = #child 
* #_mdc ^property[255].valueCode = #151964 
* #_mdc ^property[256].code = #child 
* #_mdc ^property[256].valueCode = #151972 
* #_mdc ^property[257].code = #child 
* #_mdc ^property[257].valueCode = #151973 
* #_mdc ^property[258].code = #child 
* #_mdc ^property[258].valueCode = #151974 
* #_mdc ^property[259].code = #child 
* #_mdc ^property[259].valueCode = #151975 
* #_mdc ^property[260].code = #child 
* #_mdc ^property[260].valueCode = #151976 
* #_mdc ^property[261].code = #child 
* #_mdc ^property[261].valueCode = #151980 
* #_mdc ^property[262].code = #child 
* #_mdc ^property[262].valueCode = #151984 
* #_mdc ^property[263].code = #child 
* #_mdc ^property[263].valueCode = #151988 
* #_mdc ^property[264].code = #child 
* #_mdc ^property[264].valueCode = #151992 
* #_mdc ^property[265].code = #child 
* #_mdc ^property[265].valueCode = #151996 
* #_mdc ^property[266].code = #child 
* #_mdc ^property[266].valueCode = #152000 
* #_mdc ^property[267].code = #child 
* #_mdc ^property[267].valueCode = #152004 
* #_mdc ^property[268].code = #child 
* #_mdc ^property[268].valueCode = #152008 
* #_mdc ^property[269].code = #child 
* #_mdc ^property[269].valueCode = #152012 
* #_mdc ^property[270].code = #child 
* #_mdc ^property[270].valueCode = #152016 
* #_mdc ^property[271].code = #child 
* #_mdc ^property[271].valueCode = #152020 
* #_mdc ^property[272].code = #child 
* #_mdc ^property[272].valueCode = #152024 
* #_mdc ^property[273].code = #child 
* #_mdc ^property[273].valueCode = #152028 
* #_mdc ^property[274].code = #child 
* #_mdc ^property[274].valueCode = #152032 
* #_mdc ^property[275].code = #child 
* #_mdc ^property[275].valueCode = #152036 
* #_mdc ^property[276].code = #child 
* #_mdc ^property[276].valueCode = #152040 
* #_mdc ^property[277].code = #child 
* #_mdc ^property[277].valueCode = #152044 
* #_mdc ^property[278].code = #child 
* #_mdc ^property[278].valueCode = #152048 
* #_mdc ^property[279].code = #child 
* #_mdc ^property[279].valueCode = #152052 
* #_mdc ^property[280].code = #child 
* #_mdc ^property[280].valueCode = #152056 
* #_mdc ^property[281].code = #child 
* #_mdc ^property[281].valueCode = #152060 
* #_mdc ^property[282].code = #child 
* #_mdc ^property[282].valueCode = #152064 
* #_mdc ^property[283].code = #child 
* #_mdc ^property[283].valueCode = #152068 
* #_mdc ^property[284].code = #child 
* #_mdc ^property[284].valueCode = #152072 
* #_mdc ^property[285].code = #child 
* #_mdc ^property[285].valueCode = #152076 
* #_mdc ^property[286].code = #child 
* #_mdc ^property[286].valueCode = #152080 
* #_mdc ^property[287].code = #child 
* #_mdc ^property[287].valueCode = #152084 
* #_mdc ^property[288].code = #child 
* #_mdc ^property[288].valueCode = #152088 
* #_mdc ^property[289].code = #child 
* #_mdc ^property[289].valueCode = #152092 
* #_mdc ^property[290].code = #child 
* #_mdc ^property[290].valueCode = #152096 
* #_mdc ^property[291].code = #child 
* #_mdc ^property[291].valueCode = #152100 
* #_mdc ^property[292].code = #child 
* #_mdc ^property[292].valueCode = #152104 
* #_mdc ^property[293].code = #child 
* #_mdc ^property[293].valueCode = #152108 
* #_mdc ^property[294].code = #child 
* #_mdc ^property[294].valueCode = #152112 
* #_mdc ^property[295].code = #child 
* #_mdc ^property[295].valueCode = #152116 
* #_mdc ^property[296].code = #child 
* #_mdc ^property[296].valueCode = #152120 
* #_mdc ^property[297].code = #child 
* #_mdc ^property[297].valueCode = #152124 
* #_mdc ^property[298].code = #child 
* #_mdc ^property[298].valueCode = #152128 
* #_mdc ^property[299].code = #child 
* #_mdc ^property[299].valueCode = #152132 
* #_mdc ^property[300].code = #child 
* #_mdc ^property[300].valueCode = #152136 
* #_mdc ^property[301].code = #child 
* #_mdc ^property[301].valueCode = #152140 
* #_mdc ^property[302].code = #child 
* #_mdc ^property[302].valueCode = #152144 
* #_mdc ^property[303].code = #child 
* #_mdc ^property[303].valueCode = #152148 
* #_mdc ^property[304].code = #child 
* #_mdc ^property[304].valueCode = #152152 
* #_mdc ^property[305].code = #child 
* #_mdc ^property[305].valueCode = #152156 
* #_mdc ^property[306].code = #child 
* #_mdc ^property[306].valueCode = #152160 
* #_mdc ^property[307].code = #child 
* #_mdc ^property[307].valueCode = #152164 
* #_mdc ^property[308].code = #child 
* #_mdc ^property[308].valueCode = #152168 
* #_mdc ^property[309].code = #child 
* #_mdc ^property[309].valueCode = #152172 
* #_mdc ^property[310].code = #child 
* #_mdc ^property[310].valueCode = #152176 
* #_mdc ^property[311].code = #child 
* #_mdc ^property[311].valueCode = #152180 
* #_mdc ^property[312].code = #child 
* #_mdc ^property[312].valueCode = #152184 
* #_mdc ^property[313].code = #child 
* #_mdc ^property[313].valueCode = #152188 
* #_mdc ^property[314].code = #child 
* #_mdc ^property[314].valueCode = #152192 
* #_mdc ^property[315].code = #child 
* #_mdc ^property[315].valueCode = #152196 
* #_mdc ^property[316].code = #child 
* #_mdc ^property[316].valueCode = #152200 
* #_mdc ^property[317].code = #child 
* #_mdc ^property[317].valueCode = #152204 
* #_mdc ^property[318].code = #child 
* #_mdc ^property[318].valueCode = #152208 
* #_mdc ^property[319].code = #child 
* #_mdc ^property[319].valueCode = #152212 
* #_mdc ^property[320].code = #child 
* #_mdc ^property[320].valueCode = #152216 
* #_mdc ^property[321].code = #child 
* #_mdc ^property[321].valueCode = #152220 
* #_mdc ^property[322].code = #child 
* #_mdc ^property[322].valueCode = #152224 
* #_mdc ^property[323].code = #child 
* #_mdc ^property[323].valueCode = #152416 
* #_mdc ^property[324].code = #child 
* #_mdc ^property[324].valueCode = #152420 
* #_mdc ^property[325].code = #child 
* #_mdc ^property[325].valueCode = #152424 
* #_mdc ^property[326].code = #child 
* #_mdc ^property[326].valueCode = #152428 
* #_mdc ^property[327].code = #child 
* #_mdc ^property[327].valueCode = #152432 
* #_mdc ^property[328].code = #child 
* #_mdc ^property[328].valueCode = #152436 
* #_mdc ^property[329].code = #child 
* #_mdc ^property[329].valueCode = #152440 
* #_mdc ^property[330].code = #child 
* #_mdc ^property[330].valueCode = #152444 
* #_mdc ^property[331].code = #child 
* #_mdc ^property[331].valueCode = #152448 
* #_mdc ^property[332].code = #child 
* #_mdc ^property[332].valueCode = #152452 
* #_mdc ^property[333].code = #child 
* #_mdc ^property[333].valueCode = #152456 
* #_mdc ^property[334].code = #child 
* #_mdc ^property[334].valueCode = #152460 
* #_mdc ^property[335].code = #child 
* #_mdc ^property[335].valueCode = #152464 
* #_mdc ^property[336].code = #child 
* #_mdc ^property[336].valueCode = #152482 
* #_mdc ^property[337].code = #child 
* #_mdc ^property[337].valueCode = #152490 
* #_mdc ^property[338].code = #child 
* #_mdc ^property[338].valueCode = #152498 
* #_mdc ^property[339].code = #child 
* #_mdc ^property[339].valueCode = #152506 
* #_mdc ^property[340].code = #child 
* #_mdc ^property[340].valueCode = #152514 
* #_mdc ^property[341].code = #child 
* #_mdc ^property[341].valueCode = #152522 
* #_mdc ^property[342].code = #child 
* #_mdc ^property[342].valueCode = #152530 
* #_mdc ^property[343].code = #child 
* #_mdc ^property[343].valueCode = #152538 
* #_mdc ^property[344].code = #child 
* #_mdc ^property[344].valueCode = #152546 
* #_mdc ^property[345].code = #child 
* #_mdc ^property[345].valueCode = #152554 
* #_mdc ^property[346].code = #child 
* #_mdc ^property[346].valueCode = #152562 
* #_mdc ^property[347].code = #child 
* #_mdc ^property[347].valueCode = #152584 
* #_mdc ^property[348].code = #child 
* #_mdc ^property[348].valueCode = #152585 
* #_mdc ^property[349].code = #child 
* #_mdc ^property[349].valueCode = #152586 
* #_mdc ^property[350].code = #child 
* #_mdc ^property[350].valueCode = #152587 
* #_mdc ^property[351].code = #child 
* #_mdc ^property[351].valueCode = #152596 
* #_mdc ^property[352].code = #child 
* #_mdc ^property[352].valueCode = #152600 
* #_mdc ^property[353].code = #child 
* #_mdc ^property[353].valueCode = #152604 
* #_mdc ^property[354].code = #child 
* #_mdc ^property[354].valueCode = #152608 
* #_mdc ^property[355].code = #child 
* #_mdc ^property[355].valueCode = #152612 
* #_mdc ^property[356].code = #child 
* #_mdc ^property[356].valueCode = #152616 
* #_mdc ^property[357].code = #child 
* #_mdc ^property[357].valueCode = #152621 
* #_mdc ^property[358].code = #child 
* #_mdc ^property[358].valueCode = #152624 
* #_mdc ^property[359].code = #child 
* #_mdc ^property[359].valueCode = #152628 
* #_mdc ^property[360].code = #child 
* #_mdc ^property[360].valueCode = #152632 
* #_mdc ^property[361].code = #child 
* #_mdc ^property[361].valueCode = #152636 
* #_mdc ^property[362].code = #child 
* #_mdc ^property[362].valueCode = #152640 
* #_mdc ^property[363].code = #child 
* #_mdc ^property[363].valueCode = #152644 
* #_mdc ^property[364].code = #child 
* #_mdc ^property[364].valueCode = #152648 
* #_mdc ^property[365].code = #child 
* #_mdc ^property[365].valueCode = #152652 
* #_mdc ^property[366].code = #child 
* #_mdc ^property[366].valueCode = #152656 
* #_mdc ^property[367].code = #child 
* #_mdc ^property[367].valueCode = #152660 
* #_mdc ^property[368].code = #child 
* #_mdc ^property[368].valueCode = #152664 
* #_mdc ^property[369].code = #child 
* #_mdc ^property[369].valueCode = #152668 
* #_mdc ^property[370].code = #child 
* #_mdc ^property[370].valueCode = #152672 
* #_mdc ^property[371].code = #child 
* #_mdc ^property[371].valueCode = #152676 
* #_mdc ^property[372].code = #child 
* #_mdc ^property[372].valueCode = #152679 
* #_mdc ^property[373].code = #child 
* #_mdc ^property[373].valueCode = #152680 
* #_mdc ^property[374].code = #child 
* #_mdc ^property[374].valueCode = #152684 
* #_mdc ^property[375].code = #child 
* #_mdc ^property[375].valueCode = #152688 
* #_mdc ^property[376].code = #child 
* #_mdc ^property[376].valueCode = #152692 
* #_mdc ^property[377].code = #child 
* #_mdc ^property[377].valueCode = #152696 
* #_mdc ^property[378].code = #child 
* #_mdc ^property[378].valueCode = #152700 
* #_mdc ^property[379].code = #child 
* #_mdc ^property[379].valueCode = #152704 
* #_mdc ^property[380].code = #child 
* #_mdc ^property[380].valueCode = #152708 
* #_mdc ^property[381].code = #child 
* #_mdc ^property[381].valueCode = #152712 
* #_mdc ^property[382].code = #child 
* #_mdc ^property[382].valueCode = #152716 
* #_mdc ^property[383].code = #child 
* #_mdc ^property[383].valueCode = #152720 
* #_mdc ^property[384].code = #child 
* #_mdc ^property[384].valueCode = #152724 
* #_mdc ^property[385].code = #child 
* #_mdc ^property[385].valueCode = #152728 
* #_mdc ^property[386].code = #child 
* #_mdc ^property[386].valueCode = #152732 
* #_mdc ^property[387].code = #child 
* #_mdc ^property[387].valueCode = #152736 
* #_mdc ^property[388].code = #child 
* #_mdc ^property[388].valueCode = #152740 
* #_mdc ^property[389].code = #child 
* #_mdc ^property[389].valueCode = #152744 
* #_mdc ^property[390].code = #child 
* #_mdc ^property[390].valueCode = #152748 
* #_mdc ^property[391].code = #child 
* #_mdc ^property[391].valueCode = #152752 
* #_mdc ^property[392].code = #child 
* #_mdc ^property[392].valueCode = #152756 
* #_mdc ^property[393].code = #child 
* #_mdc ^property[393].valueCode = #152760 
* #_mdc ^property[394].code = #child 
* #_mdc ^property[394].valueCode = #152764 
* #_mdc ^property[395].code = #child 
* #_mdc ^property[395].valueCode = #152768 
* #_mdc ^property[396].code = #child 
* #_mdc ^property[396].valueCode = #152772 
* #_mdc ^property[397].code = #child 
* #_mdc ^property[397].valueCode = #152776 
* #_mdc ^property[398].code = #child 
* #_mdc ^property[398].valueCode = #152780 
* #_mdc ^property[399].code = #child 
* #_mdc ^property[399].valueCode = #152784 
* #_mdc ^property[400].code = #child 
* #_mdc ^property[400].valueCode = #152788 
* #_mdc ^property[401].code = #child 
* #_mdc ^property[401].valueCode = #152792 
* #_mdc ^property[402].code = #child 
* #_mdc ^property[402].valueCode = #152796 
* #_mdc ^property[403].code = #child 
* #_mdc ^property[403].valueCode = #152800 
* #_mdc ^property[404].code = #child 
* #_mdc ^property[404].valueCode = #152804 
* #_mdc ^property[405].code = #child 
* #_mdc ^property[405].valueCode = #152808 
* #_mdc ^property[406].code = #child 
* #_mdc ^property[406].valueCode = #152812 
* #_mdc ^property[407].code = #child 
* #_mdc ^property[407].valueCode = #152816 
* #_mdc ^property[408].code = #child 
* #_mdc ^property[408].valueCode = #152820 
* #_mdc ^property[409].code = #child 
* #_mdc ^property[409].valueCode = #152824 
* #_mdc ^property[410].code = #child 
* #_mdc ^property[410].valueCode = #152828 
* #_mdc ^property[411].code = #child 
* #_mdc ^property[411].valueCode = #152832 
* #_mdc ^property[412].code = #child 
* #_mdc ^property[412].valueCode = #152836 
* #_mdc ^property[413].code = #child 
* #_mdc ^property[413].valueCode = #152840 
* #_mdc ^property[414].code = #child 
* #_mdc ^property[414].valueCode = #152844 
* #_mdc ^property[415].code = #child 
* #_mdc ^property[415].valueCode = #152848 
* #_mdc ^property[416].code = #child 
* #_mdc ^property[416].valueCode = #152852 
* #_mdc ^property[417].code = #child 
* #_mdc ^property[417].valueCode = #152856 
* #_mdc ^property[418].code = #child 
* #_mdc ^property[418].valueCode = #152860 
* #_mdc ^property[419].code = #child 
* #_mdc ^property[419].valueCode = #152864 
* #_mdc ^property[420].code = #child 
* #_mdc ^property[420].valueCode = #152868 
* #_mdc ^property[421].code = #child 
* #_mdc ^property[421].valueCode = #152872 
* #_mdc ^property[422].code = #child 
* #_mdc ^property[422].valueCode = #152876 
* #_mdc ^property[423].code = #child 
* #_mdc ^property[423].valueCode = #152880 
* #_mdc ^property[424].code = #child 
* #_mdc ^property[424].valueCode = #152884 
* #_mdc ^property[425].code = #child 
* #_mdc ^property[425].valueCode = #152888 
* #_mdc ^property[426].code = #child 
* #_mdc ^property[426].valueCode = #152892 
* #_mdc ^property[427].code = #child 
* #_mdc ^property[427].valueCode = #152896 
* #_mdc ^property[428].code = #child 
* #_mdc ^property[428].valueCode = #152900 
* #_mdc ^property[429].code = #child 
* #_mdc ^property[429].valueCode = #152904 
* #_mdc ^property[430].code = #child 
* #_mdc ^property[430].valueCode = #152908 
* #_mdc ^property[431].code = #child 
* #_mdc ^property[431].valueCode = #152912 
* #_mdc ^property[432].code = #child 
* #_mdc ^property[432].valueCode = #152916 
* #_mdc ^property[433].code = #child 
* #_mdc ^property[433].valueCode = #152920 
* #_mdc ^property[434].code = #child 
* #_mdc ^property[434].valueCode = #152924 
* #_mdc ^property[435].code = #child 
* #_mdc ^property[435].valueCode = #152928 
* #_mdc ^property[436].code = #child 
* #_mdc ^property[436].valueCode = #152932 
* #_mdc ^property[437].code = #child 
* #_mdc ^property[437].valueCode = #152936 
* #_mdc ^property[438].code = #child 
* #_mdc ^property[438].valueCode = #152940 
* #_mdc ^property[439].code = #child 
* #_mdc ^property[439].valueCode = #152944 
* #_mdc ^property[440].code = #child 
* #_mdc ^property[440].valueCode = #152948 
* #_mdc ^property[441].code = #child 
* #_mdc ^property[441].valueCode = #152952 
* #_mdc ^property[442].code = #child 
* #_mdc ^property[442].valueCode = #152956 
* #_mdc ^property[443].code = #child 
* #_mdc ^property[443].valueCode = #152960 
* #_mdc ^property[444].code = #child 
* #_mdc ^property[444].valueCode = #152964 
* #_mdc ^property[445].code = #child 
* #_mdc ^property[445].valueCode = #152968 
* #_mdc ^property[446].code = #child 
* #_mdc ^property[446].valueCode = #152972 
* #_mdc ^property[447].code = #child 
* #_mdc ^property[447].valueCode = #152976 
* #_mdc ^property[448].code = #child 
* #_mdc ^property[448].valueCode = #152980 
* #_mdc ^property[449].code = #child 
* #_mdc ^property[449].valueCode = #152984 
* #_mdc ^property[450].code = #child 
* #_mdc ^property[450].valueCode = #152988 
* #_mdc ^property[451].code = #child 
* #_mdc ^property[451].valueCode = #152992 
* #_mdc ^property[452].code = #child 
* #_mdc ^property[452].valueCode = #152996 
* #_mdc ^property[453].code = #child 
* #_mdc ^property[453].valueCode = #153000 
* #_mdc ^property[454].code = #child 
* #_mdc ^property[454].valueCode = #153004 
* #_mdc ^property[455].code = #child 
* #_mdc ^property[455].valueCode = #153008 
* #_mdc ^property[456].code = #child 
* #_mdc ^property[456].valueCode = #153012 
* #_mdc ^property[457].code = #child 
* #_mdc ^property[457].valueCode = #153016 
* #_mdc ^property[458].code = #child 
* #_mdc ^property[458].valueCode = #153020 
* #_mdc ^property[459].code = #child 
* #_mdc ^property[459].valueCode = #153024 
* #_mdc ^property[460].code = #child 
* #_mdc ^property[460].valueCode = #153028 
* #_mdc ^property[461].code = #child 
* #_mdc ^property[461].valueCode = #153032 
* #_mdc ^property[462].code = #child 
* #_mdc ^property[462].valueCode = #153036 
* #_mdc ^property[463].code = #child 
* #_mdc ^property[463].valueCode = #153040 
* #_mdc ^property[464].code = #child 
* #_mdc ^property[464].valueCode = #153044 
* #_mdc ^property[465].code = #child 
* #_mdc ^property[465].valueCode = #153048 
* #_mdc ^property[466].code = #child 
* #_mdc ^property[466].valueCode = #153052 
* #_mdc ^property[467].code = #child 
* #_mdc ^property[467].valueCode = #153056 
* #_mdc ^property[468].code = #child 
* #_mdc ^property[468].valueCode = #153060 
* #_mdc ^property[469].code = #child 
* #_mdc ^property[469].valueCode = #153064 
* #_mdc ^property[470].code = #child 
* #_mdc ^property[470].valueCode = #153068 
* #_mdc ^property[471].code = #child 
* #_mdc ^property[471].valueCode = #153072 
* #_mdc ^property[472].code = #child 
* #_mdc ^property[472].valueCode = #153076 
* #_mdc ^property[473].code = #child 
* #_mdc ^property[473].valueCode = #153080 
* #_mdc ^property[474].code = #child 
* #_mdc ^property[474].valueCode = #153084 
* #_mdc ^property[475].code = #child 
* #_mdc ^property[475].valueCode = #153088 
* #_mdc ^property[476].code = #child 
* #_mdc ^property[476].valueCode = #153092 
* #_mdc ^property[477].code = #child 
* #_mdc ^property[477].valueCode = #153096 
* #_mdc ^property[478].code = #child 
* #_mdc ^property[478].valueCode = #153100 
* #_mdc ^property[479].code = #child 
* #_mdc ^property[479].valueCode = #153104 
* #_mdc ^property[480].code = #child 
* #_mdc ^property[480].valueCode = #153108 
* #_mdc ^property[481].code = #child 
* #_mdc ^property[481].valueCode = #153112 
* #_mdc ^property[482].code = #child 
* #_mdc ^property[482].valueCode = #153116 
* #_mdc ^property[483].code = #child 
* #_mdc ^property[483].valueCode = #153120 
* #_mdc ^property[484].code = #child 
* #_mdc ^property[484].valueCode = #153124 
* #_mdc ^property[485].code = #child 
* #_mdc ^property[485].valueCode = #153128 
* #_mdc ^property[486].code = #child 
* #_mdc ^property[486].valueCode = #153132 
* #_mdc ^property[487].code = #child 
* #_mdc ^property[487].valueCode = #153136 
* #_mdc ^property[488].code = #child 
* #_mdc ^property[488].valueCode = #153140 
* #_mdc ^property[489].code = #child 
* #_mdc ^property[489].valueCode = #153144 
* #_mdc ^property[490].code = #child 
* #_mdc ^property[490].valueCode = #153148 
* #_mdc ^property[491].code = #child 
* #_mdc ^property[491].valueCode = #153152 
* #_mdc ^property[492].code = #child 
* #_mdc ^property[492].valueCode = #153156 
* #_mdc ^property[493].code = #child 
* #_mdc ^property[493].valueCode = #153160 
* #_mdc ^property[494].code = #child 
* #_mdc ^property[494].valueCode = #153164 
* #_mdc ^property[495].code = #child 
* #_mdc ^property[495].valueCode = #153168 
* #_mdc ^property[496].code = #child 
* #_mdc ^property[496].valueCode = #153172 
* #_mdc ^property[497].code = #child 
* #_mdc ^property[497].valueCode = #153176 
* #_mdc ^property[498].code = #child 
* #_mdc ^property[498].valueCode = #153180 
* #_mdc ^property[499].code = #child 
* #_mdc ^property[499].valueCode = #153184 
* #_mdc ^property[500].code = #child 
* #_mdc ^property[500].valueCode = #153188 
* #_mdc ^property[501].code = #child 
* #_mdc ^property[501].valueCode = #153192 
* #_mdc ^property[502].code = #child 
* #_mdc ^property[502].valueCode = #153196 
* #_mdc ^property[503].code = #child 
* #_mdc ^property[503].valueCode = #153200 
* #_mdc ^property[504].code = #child 
* #_mdc ^property[504].valueCode = #153204 
* #_mdc ^property[505].code = #child 
* #_mdc ^property[505].valueCode = #153208 
* #_mdc ^property[506].code = #child 
* #_mdc ^property[506].valueCode = #153212 
* #_mdc ^property[507].code = #child 
* #_mdc ^property[507].valueCode = #153216 
* #_mdc ^property[508].code = #child 
* #_mdc ^property[508].valueCode = #153220 
* #_mdc ^property[509].code = #child 
* #_mdc ^property[509].valueCode = #153224 
* #_mdc ^property[510].code = #child 
* #_mdc ^property[510].valueCode = #153228 
* #_mdc ^property[511].code = #child 
* #_mdc ^property[511].valueCode = #153236 
* #_mdc ^property[512].code = #child 
* #_mdc ^property[512].valueCode = #153240 
* #_mdc ^property[513].code = #child 
* #_mdc ^property[513].valueCode = #153244 
* #_mdc ^property[514].code = #child 
* #_mdc ^property[514].valueCode = #153248 
* #_mdc ^property[515].code = #child 
* #_mdc ^property[515].valueCode = #153252 
* #_mdc ^property[516].code = #child 
* #_mdc ^property[516].valueCode = #153256 
* #_mdc ^property[517].code = #child 
* #_mdc ^property[517].valueCode = #153260 
* #_mdc ^property[518].code = #child 
* #_mdc ^property[518].valueCode = #153264 
* #_mdc ^property[519].code = #child 
* #_mdc ^property[519].valueCode = #153272 
* #_mdc ^property[520].code = #child 
* #_mdc ^property[520].valueCode = #153276 
* #_mdc ^property[521].code = #child 
* #_mdc ^property[521].valueCode = #153280 
* #_mdc ^property[522].code = #child 
* #_mdc ^property[522].valueCode = #153284 
* #_mdc ^property[523].code = #child 
* #_mdc ^property[523].valueCode = #153288 
* #_mdc ^property[524].code = #child 
* #_mdc ^property[524].valueCode = #153292 
* #_mdc ^property[525].code = #child 
* #_mdc ^property[525].valueCode = #153296 
* #_mdc ^property[526].code = #child 
* #_mdc ^property[526].valueCode = #153300 
* #_mdc ^property[527].code = #child 
* #_mdc ^property[527].valueCode = #153304 
* #_mdc ^property[528].code = #child 
* #_mdc ^property[528].valueCode = #153308 
* #_mdc ^property[529].code = #child 
* #_mdc ^property[529].valueCode = #153312 
* #_mdc ^property[530].code = #child 
* #_mdc ^property[530].valueCode = #153316 
* #_mdc ^property[531].code = #child 
* #_mdc ^property[531].valueCode = #153320 
* #_mdc ^property[532].code = #child 
* #_mdc ^property[532].valueCode = #153324 
* #_mdc ^property[533].code = #child 
* #_mdc ^property[533].valueCode = #153328 
* #_mdc ^property[534].code = #child 
* #_mdc ^property[534].valueCode = #153332 
* #_mdc ^property[535].code = #child 
* #_mdc ^property[535].valueCode = #153336 
* #_mdc ^property[536].code = #child 
* #_mdc ^property[536].valueCode = #153360 
* #_mdc ^property[537].code = #child 
* #_mdc ^property[537].valueCode = #153364 
* #_mdc ^property[538].code = #child 
* #_mdc ^property[538].valueCode = #153368 
* #_mdc ^property[539].code = #child 
* #_mdc ^property[539].valueCode = #153372 
* #_mdc ^property[540].code = #child 
* #_mdc ^property[540].valueCode = #153376 
* #_mdc ^property[541].code = #child 
* #_mdc ^property[541].valueCode = #153380 
* #_mdc ^property[542].code = #child 
* #_mdc ^property[542].valueCode = #153384 
* #_mdc ^property[543].code = #child 
* #_mdc ^property[543].valueCode = #153388 
* #_mdc ^property[544].code = #child 
* #_mdc ^property[544].valueCode = #153392 
* #_mdc ^property[545].code = #child 
* #_mdc ^property[545].valueCode = #153396 
* #_mdc ^property[546].code = #child 
* #_mdc ^property[546].valueCode = #153604 
* #_mdc ^property[547].code = #child 
* #_mdc ^property[547].valueCode = #153608 
* #_mdc ^property[548].code = #child 
* #_mdc ^property[548].valueCode = #153609 
* #_mdc ^property[549].code = #child 
* #_mdc ^property[549].valueCode = #153610 
* #_mdc ^property[550].code = #child 
* #_mdc ^property[550].valueCode = #153611 
* #_mdc ^property[551].code = #child 
* #_mdc ^property[551].valueCode = #153636 
* #_mdc ^property[552].code = #child 
* #_mdc ^property[552].valueCode = #153640 
* #_mdc ^property[553].code = #child 
* #_mdc ^property[553].valueCode = #153644 
* #_mdc ^property[554].code = #child 
* #_mdc ^property[554].valueCode = #153648 
* #_mdc ^property[555].code = #child 
* #_mdc ^property[555].valueCode = #153652 
* #_mdc ^property[556].code = #child 
* #_mdc ^property[556].valueCode = #153656 
* #_mdc ^property[557].code = #child 
* #_mdc ^property[557].valueCode = #153660 
* #_mdc ^property[558].code = #child 
* #_mdc ^property[558].valueCode = #153728 
* #_mdc ^property[559].code = #child 
* #_mdc ^property[559].valueCode = #153730 
* #_mdc ^property[560].code = #child 
* #_mdc ^property[560].valueCode = #153731 
* #_mdc ^property[561].code = #child 
* #_mdc ^property[561].valueCode = #153732 
* #_mdc ^property[562].code = #child 
* #_mdc ^property[562].valueCode = #153856 
* #_mdc ^property[563].code = #child 
* #_mdc ^property[563].valueCode = #153892 
* #_mdc ^property[564].code = #child 
* #_mdc ^property[564].valueCode = #153896 
* #_mdc ^property[565].code = #child 
* #_mdc ^property[565].valueCode = #153900 
* #_mdc ^property[566].code = #child 
* #_mdc ^property[566].valueCode = #153980 
* #_mdc ^property[567].code = #child 
* #_mdc ^property[567].valueCode = #153984 
* #_mdc ^property[568].code = #child 
* #_mdc ^property[568].valueCode = #153988 
* #_mdc ^property[569].code = #child 
* #_mdc ^property[569].valueCode = #153992 
* #_mdc ^property[570].code = #child 
* #_mdc ^property[570].valueCode = #154028 
* #_mdc ^property[571].code = #child 
* #_mdc ^property[571].valueCode = #154032 
* #_mdc ^property[572].code = #child 
* #_mdc ^property[572].valueCode = #154036 
* #_mdc ^property[573].code = #child 
* #_mdc ^property[573].valueCode = #154040 
* #_mdc ^property[574].code = #child 
* #_mdc ^property[574].valueCode = #154068 
* #_mdc ^property[575].code = #child 
* #_mdc ^property[575].valueCode = #154072 
* #_mdc ^property[576].code = #child 
* #_mdc ^property[576].valueCode = #154076 
* #_mdc ^property[577].code = #child 
* #_mdc ^property[577].valueCode = #154080 
* #_mdc ^property[578].code = #child 
* #_mdc ^property[578].valueCode = #154977 
* #_mdc ^property[579].code = #child 
* #_mdc ^property[579].valueCode = #155024 
* #_mdc ^property[580].code = #child 
* #_mdc ^property[580].valueCode = #156241 
* #_mdc ^property[581].code = #child 
* #_mdc ^property[581].valueCode = #156242 
* #_mdc ^property[582].code = #child 
* #_mdc ^property[582].valueCode = #157744 
* #_mdc ^property[583].code = #child 
* #_mdc ^property[583].valueCode = #157760 
* #_mdc ^property[584].code = #child 
* #_mdc ^property[584].valueCode = #157784 
* #_mdc ^property[585].code = #child 
* #_mdc ^property[585].valueCode = #157816 
* #_mdc ^property[586].code = #child 
* #_mdc ^property[586].valueCode = #157864 
* #_mdc ^property[587].code = #child 
* #_mdc ^property[587].valueCode = #157872 
* #_mdc ^property[588].code = #child 
* #_mdc ^property[588].valueCode = #157880 
* #_mdc ^property[589].code = #child 
* #_mdc ^property[589].valueCode = #157884 
* #_mdc ^property[590].code = #child 
* #_mdc ^property[590].valueCode = #157904 
* #_mdc ^property[591].code = #child 
* #_mdc ^property[591].valueCode = #157908 
* #_mdc ^property[592].code = #child 
* #_mdc ^property[592].valueCode = #157916 
* #_mdc ^property[593].code = #child 
* #_mdc ^property[593].valueCode = #157924 
* #_mdc ^property[594].code = #child 
* #_mdc ^property[594].valueCode = #157984 
* #_mdc ^property[595].code = #child 
* #_mdc ^property[595].valueCode = #157985 
* #_mdc ^property[596].code = #child 
* #_mdc ^property[596].valueCode = #157987 
* #_mdc ^property[597].code = #child 
* #_mdc ^property[597].valueCode = #157988 
* #_mdc ^property[598].code = #child 
* #_mdc ^property[598].valueCode = #157990 
* #_mdc ^property[599].code = #child 
* #_mdc ^property[599].valueCode = #157991 
* #_mdc ^property[600].code = #child 
* #_mdc ^property[600].valueCode = #157992 
* #_mdc ^property[601].code = #child 
* #_mdc ^property[601].valueCode = #157993 
* #_mdc ^property[602].code = #child 
* #_mdc ^property[602].valueCode = #157994 
* #_mdc ^property[603].code = #child 
* #_mdc ^property[603].valueCode = #157995 
* #_mdc ^property[604].code = #child 
* #_mdc ^property[604].valueCode = #157996 
* #_mdc ^property[605].code = #child 
* #_mdc ^property[605].valueCode = #157997 
* #_mdc ^property[606].code = #child 
* #_mdc ^property[606].valueCode = #157999 
* #_mdc ^property[607].code = #child 
* #_mdc ^property[607].valueCode = #158000 
* #_mdc ^property[608].code = #child 
* #_mdc ^property[608].valueCode = #158002 
* #_mdc ^property[609].code = #child 
* #_mdc ^property[609].valueCode = #158003 
* #_mdc ^property[610].code = #child 
* #_mdc ^property[610].valueCode = #158004 
* #_mdc ^property[611].code = #child 
* #_mdc ^property[611].valueCode = #158005 
* #_mdc ^property[612].code = #child 
* #_mdc ^property[612].valueCode = #158006 
* #_mdc ^property[613].code = #child 
* #_mdc ^property[613].valueCode = #158007 
* #_mdc ^property[614].code = #child 
* #_mdc ^property[614].valueCode = #158008 
* #_mdc ^property[615].code = #child 
* #_mdc ^property[615].valueCode = #158009 
* #_mdc ^property[616].code = #child 
* #_mdc ^property[616].valueCode = #158010 
* #_mdc ^property[617].code = #child 
* #_mdc ^property[617].valueCode = #158011 
* #_mdc ^property[618].code = #child 
* #_mdc ^property[618].valueCode = #158012 
* #_mdc ^property[619].code = #child 
* #_mdc ^property[619].valueCode = #158013 
* #_mdc ^property[620].code = #child 
* #_mdc ^property[620].valueCode = #158014 
* #_mdc ^property[621].code = #child 
* #_mdc ^property[621].valueCode = #158015 
* #_mdc ^property[622].code = #child 
* #_mdc ^property[622].valueCode = #158016 
* #_mdc ^property[623].code = #child 
* #_mdc ^property[623].valueCode = #158017 
* #_mdc ^property[624].code = #child 
* #_mdc ^property[624].valueCode = #158018 
* #_mdc ^property[625].code = #child 
* #_mdc ^property[625].valueCode = #158019 
* #_mdc ^property[626].code = #child 
* #_mdc ^property[626].valueCode = #159748 
* #_mdc ^property[627].code = #child 
* #_mdc ^property[627].valueCode = #159752 
* #_mdc ^property[628].code = #child 
* #_mdc ^property[628].valueCode = #159756 
* #_mdc ^property[629].code = #child 
* #_mdc ^property[629].valueCode = #159760 
* #_mdc ^property[630].code = #child 
* #_mdc ^property[630].valueCode = #159764 
* #_mdc ^property[631].code = #child 
* #_mdc ^property[631].valueCode = #159768 
* #_mdc ^property[632].code = #child 
* #_mdc ^property[632].valueCode = #159792 
* #_mdc ^property[633].code = #child 
* #_mdc ^property[633].valueCode = #159796 
* #_mdc ^property[634].code = #child 
* #_mdc ^property[634].valueCode = #159800 
* #_mdc ^property[635].code = #child 
* #_mdc ^property[635].valueCode = #159804 
* #_mdc ^property[636].code = #child 
* #_mdc ^property[636].valueCode = #159816 
* #_mdc ^property[637].code = #child 
* #_mdc ^property[637].valueCode = #159852 
* #_mdc ^property[638].code = #child 
* #_mdc ^property[638].valueCode = #159960 
* #_mdc ^property[639].code = #child 
* #_mdc ^property[639].valueCode = #160004 
* #_mdc ^property[640].code = #child 
* #_mdc ^property[640].valueCode = #160008 
* #_mdc ^property[641].code = #child 
* #_mdc ^property[641].valueCode = #160012 
* #_mdc ^property[642].code = #child 
* #_mdc ^property[642].valueCode = #160016 
* #_mdc ^property[643].code = #child 
* #_mdc ^property[643].valueCode = #160020 
* #_mdc ^property[644].code = #child 
* #_mdc ^property[644].valueCode = #160024 
* #_mdc ^property[645].code = #child 
* #_mdc ^property[645].valueCode = #160064 
* #_mdc ^property[646].code = #child 
* #_mdc ^property[646].valueCode = #160068 
* #_mdc ^property[647].code = #child 
* #_mdc ^property[647].valueCode = #160080 
* #_mdc ^property[648].code = #child 
* #_mdc ^property[648].valueCode = #160104 
* #_mdc ^property[649].code = #child 
* #_mdc ^property[649].valueCode = #160116 
* #_mdc ^property[650].code = #child 
* #_mdc ^property[650].valueCode = #160132 
* #_mdc ^property[651].code = #child 
* #_mdc ^property[651].valueCode = #160184 
* #_mdc ^property[652].code = #child 
* #_mdc ^property[652].valueCode = #160188 
* #_mdc ^property[653].code = #child 
* #_mdc ^property[653].valueCode = #160192 
* #_mdc ^property[654].code = #child 
* #_mdc ^property[654].valueCode = #160196 
* #_mdc ^property[655].code = #child 
* #_mdc ^property[655].valueCode = #160200 
* #_mdc ^property[656].code = #child 
* #_mdc ^property[656].valueCode = #160204 
* #_mdc ^property[657].code = #child 
* #_mdc ^property[657].valueCode = #160208 
* #_mdc ^property[658].code = #child 
* #_mdc ^property[658].valueCode = #160212 
* #_mdc ^property[659].code = #child 
* #_mdc ^property[659].valueCode = #160216 
* #_mdc ^property[660].code = #child 
* #_mdc ^property[660].valueCode = #160220 
* #_mdc ^property[661].code = #child 
* #_mdc ^property[661].valueCode = #160252 
* #_mdc ^property[662].code = #child 
* #_mdc ^property[662].valueCode = #160256 
* #_mdc ^property[663].code = #child 
* #_mdc ^property[663].valueCode = #160260 
* #_mdc ^property[664].code = #child 
* #_mdc ^property[664].valueCode = #160264 
* #_mdc ^property[665].code = #child 
* #_mdc ^property[665].valueCode = #160268 
* #_mdc ^property[666].code = #child 
* #_mdc ^property[666].valueCode = #160272 
* #_mdc ^property[667].code = #child 
* #_mdc ^property[667].valueCode = #160276 
* #_mdc ^property[668].code = #child 
* #_mdc ^property[668].valueCode = #160280 
* #_mdc ^property[669].code = #child 
* #_mdc ^property[669].valueCode = #160284 
* #_mdc ^property[670].code = #child 
* #_mdc ^property[670].valueCode = #160288 
* #_mdc ^property[671].code = #child 
* #_mdc ^property[671].valueCode = #160292 
* #_mdc ^property[672].code = #child 
* #_mdc ^property[672].valueCode = #160296 
* #_mdc ^property[673].code = #child 
* #_mdc ^property[673].valueCode = #160300 
* #_mdc ^property[674].code = #child 
* #_mdc ^property[674].valueCode = #160304 
* #_mdc ^property[675].code = #child 
* #_mdc ^property[675].valueCode = #160308 
* #_mdc ^property[676].code = #child 
* #_mdc ^property[676].valueCode = #160312 
* #_mdc ^property[677].code = #child 
* #_mdc ^property[677].valueCode = #160316 
* #_mdc ^property[678].code = #child 
* #_mdc ^property[678].valueCode = #160320 
* #_mdc ^property[679].code = #child 
* #_mdc ^property[679].valueCode = #160324 
* #_mdc ^property[680].code = #child 
* #_mdc ^property[680].valueCode = #160364 
* #_mdc ^property[681].code = #child 
* #_mdc ^property[681].valueCode = #160368 
* #_mdc ^property[682].code = #child 
* #_mdc ^property[682].valueCode = #160372 
* #_mdc ^property[683].code = #child 
* #_mdc ^property[683].valueCode = #160376 
* #_mdc ^property[684].code = #child 
* #_mdc ^property[684].valueCode = #160380 
* #_mdc ^property[685].code = #child 
* #_mdc ^property[685].valueCode = #160384 
* #_mdc ^property[686].code = #child 
* #_mdc ^property[686].valueCode = #16927604 
* #_mdc ^property[687].code = #child 
* #_mdc ^property[687].valueCode = #16928802 
* #_mdc ^property[688].code = #child 
* #_mdc ^property[688].valueCode = #16928804 
* #_mdc ^property[689].code = #child 
* #_mdc ^property[689].valueCode = #16928805 
* #_mdc ^property[690].code = #child 
* #_mdc ^property[690].valueCode = #16929020 
* #_mdc ^property[691].code = #child 
* #_mdc ^property[691].valueCode = #16929048 
* #_mdc ^property[692].code = #child 
* #_mdc ^property[692].valueCode = #16929072 
* #_mdc ^property[693].code = #child 
* #_mdc ^property[693].valueCode = #16929164 
* #_mdc ^property[694].code = #child 
* #_mdc ^property[694].valueCode = #16929188 
* #_mdc ^property[695].code = #child 
* #_mdc ^property[695].valueCode = #16929192 
* #_mdc ^property[696].code = #child 
* #_mdc ^property[696].valueCode = #16929196 
* #_mdc ^property[697].code = #child 
* #_mdc ^property[697].valueCode = #16929224 
* #_mdc ^property[698].code = #child 
* #_mdc ^property[698].valueCode = #16929228 
* #_mdc ^property[699].code = #child 
* #_mdc ^property[699].valueCode = #16929300 
* #_mdc ^property[700].code = #child 
* #_mdc ^property[700].valueCode = #16929304 
* #_mdc ^property[701].code = #child 
* #_mdc ^property[701].valueCode = #16929308 
* #_mdc ^property[702].code = #child 
* #_mdc ^property[702].valueCode = #16929312 
* #_mdc ^property[703].code = #child 
* #_mdc ^property[703].valueCode = #16929316 
* #_mdc ^property[704].code = #child 
* #_mdc ^property[704].valueCode = #16929416 
* #_mdc ^property[705].code = #child 
* #_mdc ^property[705].valueCode = #16929420 
* #_mdc ^property[706].code = #child 
* #_mdc ^property[706].valueCode = #16929424 
* #_mdc ^property[707].code = #child 
* #_mdc ^property[707].valueCode = #16929428 
* #_mdc ^property[708].code = #child 
* #_mdc ^property[708].valueCode = #16929432 
* #_mdc ^property[709].code = #child 
* #_mdc ^property[709].valueCode = #16929632 
* #_mdc ^property[710].code = #child 
* #_mdc ^property[710].valueCode = #16929633 
* #_mdc ^property[711].code = #child 
* #_mdc ^property[711].valueCode = #16929644 
* #_mdc ^property[712].code = #child 
* #_mdc ^property[712].valueCode = #16929656 
* #_mdc ^property[713].code = #child 
* #_mdc ^property[713].valueCode = #16929698 
* #_mdc ^property[714].code = #child 
* #_mdc ^property[714].valueCode = #16929820 
* #_mdc ^property[715].code = #child 
* #_mdc ^property[715].valueCode = #16929832 
* #_mdc ^property[716].code = #child 
* #_mdc ^property[716].valueCode = #16929837 
* #_mdc ^property[717].code = #child 
* #_mdc ^property[717].valueCode = #16929840 
* #_mdc ^property[718].code = #child 
* #_mdc ^property[718].valueCode = #16929844 
* #_mdc ^property[719].code = #child 
* #_mdc ^property[719].valueCode = #16929848 
* #_mdc ^property[720].code = #child 
* #_mdc ^property[720].valueCode = #16929852 
* #_mdc ^property[721].code = #child 
* #_mdc ^property[721].valueCode = #16929860 
* #_mdc ^property[722].code = #child 
* #_mdc ^property[722].valueCode = #16929864 
* #_mdc ^property[723].code = #child 
* #_mdc ^property[723].valueCode = #16929868 
* #_mdc ^property[724].code = #child 
* #_mdc ^property[724].valueCode = #16929872 
* #_mdc ^property[725].code = #child 
* #_mdc ^property[725].valueCode = #16929932 
* #_mdc ^property[726].code = #child 
* #_mdc ^property[726].valueCode = #16929936 
* #_mdc ^property[727].code = #child 
* #_mdc ^property[727].valueCode = #16929940 
* #_mdc ^property[728].code = #child 
* #_mdc ^property[728].valueCode = #16929944 
* #_mdc ^property[729].code = #child 
* #_mdc ^property[729].valueCode = #16929948 
* #_mdc ^property[730].code = #child 
* #_mdc ^property[730].valueCode = #16929952 
* #_mdc ^property[731].code = #child 
* #_mdc ^property[731].valueCode = #16929956 
* #_mdc ^property[732].code = #child 
* #_mdc ^property[732].valueCode = #16929960 
* #_mdc ^property[733].code = #child 
* #_mdc ^property[733].valueCode = #16929964 
* #_mdc ^property[734].code = #child 
* #_mdc ^property[734].valueCode = #16929968 
* #_mdc ^property[735].code = #child 
* #_mdc ^property[735].valueCode = #16929972 
* #_mdc ^property[736].code = #child 
* #_mdc ^property[736].valueCode = #16929976 
* #_mdc ^property[737].code = #child 
* #_mdc ^property[737].valueCode = #16929980 
* #_mdc ^property[738].code = #child 
* #_mdc ^property[738].valueCode = #16929984 
* #_mdc ^property[739].code = #child 
* #_mdc ^property[739].valueCode = #16929988 
* #_mdc ^property[740].code = #child 
* #_mdc ^property[740].valueCode = #16930020 
* #_mdc ^property[741].code = #child 
* #_mdc ^property[741].valueCode = #16930024 
* #_mdc ^property[742].code = #child 
* #_mdc ^property[742].valueCode = #16930092 
* #_mdc ^property[743].code = #child 
* #_mdc ^property[743].valueCode = #16930308 
* #_mdc ^property[744].code = #child 
* #_mdc ^property[744].valueCode = #16930360 
* #_mdc ^property[745].code = #child 
* #_mdc ^property[745].valueCode = #16930372 
* #_mdc ^property[746].code = #child 
* #_mdc ^property[746].valueCode = #16930432 
* #_mdc ^property[747].code = #child 
* #_mdc ^property[747].valueCode = #16930436 
* #_mdc ^property[748].code = #child 
* #_mdc ^property[748].valueCode = #16930438 
* #_mdc ^property[749].code = #child 
* #_mdc ^property[749].valueCode = #16930448 
* #_mdc ^property[750].code = #child 
* #_mdc ^property[750].valueCode = #16930476 
* #_mdc ^property[751].code = #child 
* #_mdc ^property[751].valueCode = #16930484 
* #_mdc ^property[752].code = #child 
* #_mdc ^property[752].valueCode = #16930488 
* #_mdc ^property[753].code = #child 
* #_mdc ^property[753].valueCode = #16930560 
* #_mdc ^property[754].code = #child 
* #_mdc ^property[754].valueCode = #16930564 
* #_mdc ^property[755].code = #child 
* #_mdc ^property[755].valueCode = #16930568 
* #_mdc ^property[756].code = #child 
* #_mdc ^property[756].valueCode = #16930572 
* #_mdc ^property[757].code = #child 
* #_mdc ^property[757].valueCode = #16930604 
* #_mdc ^property[758].code = #child 
* #_mdc ^property[758].valueCode = #16961504 
* #_mdc ^property[759].code = #child 
* #_mdc ^property[759].valueCode = #16961508 
* #_mdc ^property[760].code = #child 
* #_mdc ^property[760].valueCode = #16961512 
* #_mdc ^property[761].code = #child 
* #_mdc ^property[761].valueCode = #16961620 
* #_mdc ^property[762].code = #child 
* #_mdc ^property[762].valueCode = #16961621 
* #_mdc ^property[763].code = #child 
* #_mdc ^property[763].valueCode = #16961626 
* #_mdc ^property[764].code = #child 
* #_mdc ^property[764].valueCode = #184288 
* #_mdc ^property[765].code = #child 
* #_mdc ^property[765].valueCode = #184292 
* #_mdc ^property[766].code = #child 
* #_mdc ^property[766].valueCode = #184296 
* #_mdc ^property[767].code = #child 
* #_mdc ^property[767].valueCode = #184300 
* #_mdc ^property[768].code = #child 
* #_mdc ^property[768].valueCode = #184304 
* #_mdc ^property[769].code = #child 
* #_mdc ^property[769].valueCode = #184308 
* #_mdc ^property[770].code = #child 
* #_mdc ^property[770].valueCode = #184326 
* #_mdc ^property[771].code = #child 
* #_mdc ^property[771].valueCode = #184327 
* #_mdc ^property[772].code = #child 
* #_mdc ^property[772].valueCode = #184331 
* #_mdc ^property[773].code = #child 
* #_mdc ^property[773].valueCode = #184336 
* #_mdc ^property[774].code = #child 
* #_mdc ^property[774].valueCode = #184337 
* #_mdc ^property[775].code = #child 
* #_mdc ^property[775].valueCode = #184338 
* #_mdc ^property[776].code = #child 
* #_mdc ^property[776].valueCode = #184339 
* #_mdc ^property[777].code = #child 
* #_mdc ^property[777].valueCode = #184340 
* #_mdc ^property[778].code = #child 
* #_mdc ^property[778].valueCode = #184341 
* #_mdc ^property[779].code = #child 
* #_mdc ^property[779].valueCode = #184352 
* #_mdc ^property[780].code = #child 
* #_mdc ^property[780].valueCode = #184353 
* #_mdc ^property[781].code = #child 
* #_mdc ^property[781].valueCode = #184400 
* #_mdc ^property[782].code = #child 
* #_mdc ^property[782].valueCode = #184404 
* #_mdc ^property[783].code = #child 
* #_mdc ^property[783].valueCode = #184405 
* #_mdc ^property[784].code = #child 
* #_mdc ^property[784].valueCode = #184408 
* #_mdc ^property[785].code = #child 
* #_mdc ^property[785].valueCode = #184409 
* #_mdc ^property[786].code = #child 
* #_mdc ^property[786].valueCode = #184476 
* #_mdc ^property[787].code = #child 
* #_mdc ^property[787].valueCode = #184488 
* #_mdc ^property[788].code = #child 
* #_mdc ^property[788].valueCode = #184513 
* #_mdc ^property[789].code = #child 
* #_mdc ^property[789].valueCode = #184514 
* #_mdc ^property[790].code = #child 
* #_mdc ^property[790].valueCode = #184515 
* #_mdc ^property[791].code = #child 
* #_mdc ^property[791].valueCode = #184516 
* #_mdc ^property[792].code = #child 
* #_mdc ^property[792].valueCode = #184517 
* #_mdc ^property[793].code = #child 
* #_mdc ^property[793].valueCode = #184518 
* #_mdc ^property[794].code = #child 
* #_mdc ^property[794].valueCode = #184519 
* #_mdc ^property[795].code = #child 
* #_mdc ^property[795].valueCode = #188420 
* #_mdc ^property[796].code = #child 
* #_mdc ^property[796].valueCode = #188424 
* #_mdc ^property[797].code = #child 
* #_mdc ^property[797].valueCode = #188428 
* #_mdc ^property[798].code = #child 
* #_mdc ^property[798].valueCode = #188432 
* #_mdc ^property[799].code = #child 
* #_mdc ^property[799].valueCode = #188436 
* #_mdc ^property[800].code = #child 
* #_mdc ^property[800].valueCode = #188440 
* #_mdc ^property[801].code = #child 
* #_mdc ^property[801].valueCode = #188448 
* #_mdc ^property[802].code = #child 
* #_mdc ^property[802].valueCode = #188452 
* #_mdc ^property[803].code = #child 
* #_mdc ^property[803].valueCode = #188456 
* #_mdc ^property[804].code = #child 
* #_mdc ^property[804].valueCode = #188480 
* #_mdc ^property[805].code = #child 
* #_mdc ^property[805].valueCode = #188488 
* #_mdc ^property[806].code = #child 
* #_mdc ^property[806].valueCode = #188492 
* #_mdc ^property[807].code = #child 
* #_mdc ^property[807].valueCode = #188496 
* #_mdc ^property[808].code = #child 
* #_mdc ^property[808].valueCode = #188500 
* #_mdc ^property[809].code = #child 
* #_mdc ^property[809].valueCode = #188504 
* #_mdc ^property[810].code = #child 
* #_mdc ^property[810].valueCode = #188508 
* #_mdc ^property[811].code = #child 
* #_mdc ^property[811].valueCode = #188736 
* #_mdc ^property[812].code = #child 
* #_mdc ^property[812].valueCode = #188740 
* #_mdc ^property[813].code = #child 
* #_mdc ^property[813].valueCode = #188744 
* #_mdc ^property[814].code = #child 
* #_mdc ^property[814].valueCode = #188748 
* #_mdc ^property[815].code = #child 
* #_mdc ^property[815].valueCode = #188752 
* #_mdc ^property[816].code = #child 
* #_mdc ^property[816].valueCode = #188756 
* #_mdc ^property[817].code = #child 
* #_mdc ^property[817].valueCode = #188760 
* #_mdc ^property[818].code = #child 
* #_mdc ^property[818].valueCode = #188764 
* #_mdc ^property[819].code = #child 
* #_mdc ^property[819].valueCode = #188772 
* #_mdc ^property[820].code = #child 
* #_mdc ^property[820].valueCode = #188792 
* #_mdc ^property[821].code = #child 
* #_mdc ^property[821].valueCode = #188796 
* #_mdc ^property[822].code = #child 
* #_mdc ^property[822].valueCode = #188800 
* #_mdc ^property[823].code = #child 
* #_mdc ^property[823].valueCode = #196886 
* #_mdc ^property[824].code = #child 
* #_mdc ^property[824].valueCode = #67926 
* #_mdc ^property[825].code = #child 
* #_mdc ^property[825].valueCode = #68012 
* #_mdc ^property[826].code = #child 
* #_mdc ^property[826].valueCode = #68060 
* #_mdc ^property[827].code = #child 
* #_mdc ^property[827].valueCode = #68063 
* #_mdc ^property[828].code = #child 
* #_mdc ^property[828].valueCode = #68137 
* #_mdc ^property[829].code = #child 
* #_mdc ^property[829].valueCode = #68167 
* #_mdc ^property[830].code = #child 
* #_mdc ^property[830].valueCode = #68185 
* #_mdc ^property[831].code = #child 
* #_mdc ^property[831].valueCode = #68320 
* #_mdc ^property[832].code = #child 
* #_mdc ^property[832].valueCode = #68321 
* #_mdc ^property[833].code = #child 
* #_mdc ^property[833].valueCode = #68322 
* #_mdc ^property[834].code = #child 
* #_mdc ^property[834].valueCode = #68323 
* #_mdc ^property[835].code = #child 
* #_mdc ^property[835].valueCode = #68324 
* #_mdc ^property[836].code = #child 
* #_mdc ^property[836].valueCode = #68325 
* #_mdc ^property[837].code = #child 
* #_mdc ^property[837].valueCode = #68326 
* #_mdc ^property[838].code = #child 
* #_mdc ^property[838].valueCode = #68327 
* #_mdc ^property[839].code = #child 
* #_mdc ^property[839].valueCode = #68328 
* #_mdc ^property[840].code = #child 
* #_mdc ^property[840].valueCode = #68480 
* #_mdc ^property[841].code = #child 
* #_mdc ^property[841].valueCode = #68481 
* #_mdc ^property[842].code = #child 
* #_mdc ^property[842].valueCode = #68482 
* #_mdc ^property[843].code = #child 
* #_mdc ^property[843].valueCode = #68483 
* #_mdc ^property[844].code = #child 
* #_mdc ^property[844].valueCode = #68484 
* #_mdc ^property[845].code = #child 
* #_mdc ^property[845].valueCode = #68485 
* #_mdc ^property[846].code = #child 
* #_mdc ^property[846].valueCode = #68487 
* #_mdc ^property[847].code = #child 
* #_mdc ^property[847].valueCode = #68488 
* #_mdc ^property[848].code = #child 
* #_mdc ^property[848].valueCode = #68512 
* #_mdc ^property[849].code = #child 
* #_mdc ^property[849].valueCode = #68513 
* #_mdc ^property[850].code = #child 
* #_mdc ^property[850].valueCode = #68514 
* #_mdc ^property[851].code = #child 
* #_mdc ^property[851].valueCode = #68515 
* #_mdc ^property[852].code = #child 
* #_mdc ^property[852].valueCode = #68517 
* #_mdc ^property[853].code = #child 
* #_mdc ^property[853].valueCode = #68518 
* #_mdc ^property[854].code = #child 
* #_mdc ^property[854].valueCode = #68519 
* #_mdc ^property[855].code = #child 
* #_mdc ^property[855].valueCode = #68520 
* #_mdc ^property[856].code = #child 
* #_mdc ^property[856].valueCode = #68521 
* #_mdc ^property[857].code = #child 
* #_mdc ^property[857].valueCode = #68522 
* #_mdc ^property[858].code = #child 
* #_mdc ^property[858].valueCode = #68523 
* #_mdc ^property[859].code = #child 
* #_mdc ^property[859].valueCode = #68524 
* #_mdc ^property[860].code = #child 
* #_mdc ^property[860].valueCode = #68525 
* #_mdc ^property[861].code = #child 
* #_mdc ^property[861].valueCode = #68526 
* #_mdc ^property[862].code = #child 
* #_mdc ^property[862].valueCode = #68527 
* #_mdc ^property[863].code = #child 
* #_mdc ^property[863].valueCode = #68528 
* #_mdc ^property[864].code = #child 
* #_mdc ^property[864].valueCode = #68529 
* #_mdc ^property[865].code = #child 
* #_mdc ^property[865].valueCode = #68530 
* #_mdc ^property[866].code = #child 
* #_mdc ^property[866].valueCode = #68531 
* #_mdc ^property[867].code = #child 
* #_mdc ^property[867].valueCode = #68532 
* #_mdc ^property[868].code = #child 
* #_mdc ^property[868].valueCode = #68533 
* #_mdc ^property[869].code = #child 
* #_mdc ^property[869].valueCode = #68534 
* #_mdc ^property[870].code = #child 
* #_mdc ^property[870].valueCode = #68535 
* #_mdc ^property[871].code = #child 
* #_mdc ^property[871].valueCode = #68536 
* #_mdc ^property[872].code = #child 
* #_mdc ^property[872].valueCode = #68537 
* #_mdc ^property[873].code = #child 
* #_mdc ^property[873].valueCode = #68538 
* #_mdc ^property[874].code = #child 
* #_mdc ^property[874].valueCode = #68539 
* #_mdc ^property[875].code = #child 
* #_mdc ^property[875].valueCode = #68540 
* #_mdc ^property[876].code = #child 
* #_mdc ^property[876].valueCode = #68541 
* #_mdc ^property[877].code = #child 
* #_mdc ^property[877].valueCode = #68542 
* #_mdc ^property[878].code = #child 
* #_mdc ^property[878].valueCode = #69120 
* #_mdc ^property[879].code = #child 
* #_mdc ^property[879].valueCode = #69121 
* #_mdc ^property[880].code = #child 
* #_mdc ^property[880].valueCode = #69122 
* #_mdc ^property[881].code = #child 
* #_mdc ^property[881].valueCode = #69984 
* #_mdc ^property[882].code = #child 
* #_mdc ^property[882].valueCode = #69985 
* #_mdc ^property[883].code = #child 
* #_mdc ^property[883].valueCode = #69986 
* #_mdc ^property[884].code = #child 
* #_mdc ^property[884].valueCode = #70048 
* #_mdc ^property[885].code = #child 
* #_mdc ^property[885].valueCode = #70049 
* #_mdc ^property[886].code = #child 
* #_mdc ^property[886].valueCode = #70050 
* #_mdc ^property[887].code = #child 
* #_mdc ^property[887].valueCode = #70052 
* #_mdc ^property[888].code = #child 
* #_mdc ^property[888].valueCode = #70053 
* #_mdc ^property[889].code = #child 
* #_mdc ^property[889].valueCode = #70054 
* #_mdc ^property[890].code = #child 
* #_mdc ^property[890].valueCode = #70055 
* #_mdc ^property[891].code = #child 
* #_mdc ^property[891].valueCode = #70056 
* #_mdc ^property[892].code = #child 
* #_mdc ^property[892].valueCode = #70057 
* #_mdc ^property[893].code = #child 
* #_mdc ^property[893].valueCode = #70058 
* #_mdc ^property[894].code = #child 
* #_mdc ^property[894].valueCode = #70060 
* #_mdc ^property[895].code = #child 
* #_mdc ^property[895].valueCode = #70061 
* #_mdc ^property[896].code = #child 
* #_mdc ^property[896].valueCode = #70062 
* #_mdc ^property[897].code = #child 
* #_mdc ^property[897].valueCode = #70067 
* #_mdc ^property[898].code = #child 
* #_mdc ^property[898].valueCode = #70071 
* #_mdc ^property[899].code = #child 
* #_mdc ^property[899].valueCode = #70075 
* #_mdc ^property[900].code = #child 
* #_mdc ^property[900].valueCode = #70079 
* #_mdc ^property[901].code = #child 
* #_mdc ^property[901].valueCode = #70083 
* #_mdc ^property[902].code = #child 
* #_mdc ^property[902].valueCode = #70087 
* #_mdc ^property[903].code = #child 
* #_mdc ^property[903].valueCode = #70091 
* #_mdc ^property[904].code = #child 
* #_mdc ^property[904].valueCode = #70095 
* #_mdc ^property[905].code = #child 
* #_mdc ^property[905].valueCode = #8417752 
* #_mdc ^property[906].code = #child 
* #_mdc ^property[906].valueCode = #8417760 
* #_mdc ^property[907].code = #child 
* #_mdc ^property[907].valueCode = #8417764 
* #_mdc ^property[908].code = #child 
* #_mdc ^property[908].valueCode = #8417765 
* #_mdc ^property[909].code = #child 
* #_mdc ^property[909].valueCode = #8417766 
* #_mdc ^property[910].code = #child 
* #_mdc ^property[910].valueCode = #8417767 
* #_mdc ^property[911].code = #child 
* #_mdc ^property[911].valueCode = #8417768 
* #_mdc ^property[912].code = #child 
* #_mdc ^property[912].valueCode = #8417769 
* #_mdc ^property[913].code = #child 
* #_mdc ^property[913].valueCode = #8417772 
* #_mdc ^property[914].code = #child 
* #_mdc ^property[914].valueCode = #8417776 
* #_mdc ^property[915].code = #child 
* #_mdc ^property[915].valueCode = #8417780 
* #_mdc ^property[916].code = #child 
* #_mdc ^property[916].valueCode = #8417784 
* #_mdc ^property[917].code = #child 
* #_mdc ^property[917].valueCode = #8417788 
* #_mdc ^property[918].code = #child 
* #_mdc ^property[918].valueCode = #8417792 
* #_mdc ^property[919].code = #child 
* #_mdc ^property[919].valueCode = #8417796 
* #_mdc ^property[920].code = #child 
* #_mdc ^property[920].valueCode = #8417800 
* #_mdc ^property[921].code = #child 
* #_mdc ^property[921].valueCode = #8417804 
* #_mdc ^property[922].code = #child 
* #_mdc ^property[922].valueCode = #8417808 
* #_mdc ^property[923].code = #child 
* #_mdc ^property[923].valueCode = #8417812 
* #_mdc ^property[924].code = #child 
* #_mdc ^property[924].valueCode = #8417816 
* #_mdc ^property[925].code = #child 
* #_mdc ^property[925].valueCode = #8417820 
* #_mdc ^property[926].code = #child 
* #_mdc ^property[926].valueCode = #8417844 
* #_mdc ^property[927].code = #child 
* #_mdc ^property[927].valueCode = #8417864 
* #_mdc ^property[928].code = #child 
* #_mdc ^property[928].valueCode = #8417884 
* #_mdc ^property[929].code = #child 
* #_mdc ^property[929].valueCode = #8417908 
* #_mdc ^property[930].code = #child 
* #_mdc ^property[930].valueCode = #8417909 
* #_mdc ^property[931].code = #child 
* #_mdc ^property[931].valueCode = #8417912 
* #_mdc ^property[932].code = #child 
* #_mdc ^property[932].valueCode = #8417916 
* #_mdc ^property[933].code = #child 
* #_mdc ^property[933].valueCode = #8417920 
* #_mdc ^property[934].code = #child 
* #_mdc ^property[934].valueCode = #8417924 
* #131840 "MDC_ECG_AMPL_ST"
* #131840 ^definition = ElectricalPotential | ECG <lead>; ST | Heart | CVS
* #131840 ^designation[0].language = #de-AT 
* #131840 ^designation[0].value = "STTxx Amplitude" 
* #131840 ^property[0].code = #parent 
* #131840 ^property[0].valueCode = #_mdc 
* #131840 ^property[1].code = #hints 
* #131840 ^property[1].valueString = "PART: 2 ~ CODE10: 768" 
* #131841 "MDC_ECG_AMPL_ST_I"
* #131841 ^property[0].code = #parent 
* #131841 ^property[0].valueCode = #_mdc 
* #131841 ^property[1].code = #hints 
* #131841 ^property[1].valueString = "PART: 2 ~ CODE10: 769" 
* #131842 "MDC_ECG_AMPL_ST_II"
* #131842 ^property[0].code = #parent 
* #131842 ^property[0].valueCode = #_mdc 
* #131842 ^property[1].code = #hints 
* #131842 ^property[1].valueString = "PART: 2 ~ CODE10: 770" 
* #131843 "MDC_ECG_AMPL_ST_V1"
* #131843 ^property[0].code = #parent 
* #131843 ^property[0].valueCode = #_mdc 
* #131843 ^property[1].code = #hints 
* #131843 ^property[1].valueString = "PART: 2 ~ CODE10: 771" 
* #131844 "MDC_ECG_AMPL_ST_V2"
* #131844 ^property[0].code = #parent 
* #131844 ^property[0].valueCode = #_mdc 
* #131844 ^property[1].code = #hints 
* #131844 ^property[1].valueString = "PART: 2 ~ CODE10: 772" 
* #131845 "MDC_ECG_AMPL_ST_V3"
* #131845 ^property[0].code = #parent 
* #131845 ^property[0].valueCode = #_mdc 
* #131845 ^property[1].code = #hints 
* #131845 ^property[1].valueString = "PART: 2 ~ CODE10: 773" 
* #131846 "MDC_ECG_AMPL_ST_V4"
* #131846 ^property[0].code = #parent 
* #131846 ^property[0].valueCode = #_mdc 
* #131846 ^property[1].code = #hints 
* #131846 ^property[1].valueString = "PART: 2 ~ CODE10: 774" 
* #131847 "MDC_ECG_AMPL_ST_V5"
* #131847 ^property[0].code = #parent 
* #131847 ^property[0].valueCode = #_mdc 
* #131847 ^property[1].code = #hints 
* #131847 ^property[1].valueString = "PART: 2 ~ CODE10: 775" 
* #131848 "MDC_ECG_AMPL_ST_V6"
* #131848 ^property[0].code = #parent 
* #131848 ^property[0].valueCode = #_mdc 
* #131848 ^property[1].code = #hints 
* #131848 ^property[1].valueString = "PART: 2 ~ CODE10: 776" 
* #131873 "MDC_ECG_AMPL_ST_dV1"
* #131873 ^property[0].code = #parent 
* #131873 ^property[0].valueCode = #_mdc 
* #131873 ^property[1].code = #hints 
* #131873 ^property[1].valueString = "PART: 2 ~ CODE10: 801" 
* #131874 "MDC_ECG_AMPL_ST_dV2"
* #131874 ^property[0].code = #parent 
* #131874 ^property[0].valueCode = #_mdc 
* #131874 ^property[1].code = #hints 
* #131874 ^property[1].valueString = "PART: 2 ~ CODE10: 802" 
* #131875 "MDC_ECG_AMPL_ST_dV3"
* #131875 ^property[0].code = #parent 
* #131875 ^property[0].valueCode = #_mdc 
* #131875 ^property[1].code = #hints 
* #131875 ^property[1].valueString = "PART: 2 ~ CODE10: 803" 
* #131876 "MDC_ECG_AMPL_ST_dV4"
* #131876 ^property[0].code = #parent 
* #131876 ^property[0].valueCode = #_mdc 
* #131876 ^property[1].code = #hints 
* #131876 ^property[1].valueString = "PART: 2 ~ CODE10: 804" 
* #131877 "MDC_ECG_AMPL_ST_dV5"
* #131877 ^property[0].code = #parent 
* #131877 ^property[0].valueCode = #_mdc 
* #131877 ^property[1].code = #hints 
* #131877 ^property[1].valueString = "PART: 2 ~ CODE10: 805" 
* #131878 "MDC_ECG_AMPL_ST_dV6"
* #131878 ^property[0].code = #parent 
* #131878 ^property[0].valueCode = #_mdc 
* #131878 ^property[1].code = #hints 
* #131878 ^property[1].valueString = "PART: 2 ~ CODE10: 806" 
* #131901 "MDC_ECG_AMPL_ST_III"
* #131901 ^property[0].code = #parent 
* #131901 ^property[0].valueCode = #_mdc 
* #131901 ^property[1].code = #hints 
* #131901 ^property[1].valueString = "PART: 2 ~ CODE10: 829" 
* #131902 "MDC_ECG_AMPL_ST_AVR"
* #131902 ^property[0].code = #parent 
* #131902 ^property[0].valueCode = #_mdc 
* #131902 ^property[1].code = #hints 
* #131902 ^property[1].valueString = "PART: 2 ~ CODE10: 830" 
* #131903 "MDC_ECG_AMPL_ST_AVL"
* #131903 ^property[0].code = #parent 
* #131903 ^property[0].valueCode = #_mdc 
* #131903 ^property[1].code = #hints 
* #131903 ^property[1].valueString = "PART: 2 ~ CODE10: 831" 
* #131904 "MDC_ECG_AMPL_ST_AVF"
* #131904 ^property[0].code = #parent 
* #131904 ^property[0].valueCode = #_mdc 
* #131904 ^property[1].code = #hints 
* #131904 ^property[1].valueString = "PART: 2 ~ CODE10: 832" 
* #131927 "MDC_ECG_AMPL_ST_V"
* #131927 ^property[0].code = #parent 
* #131927 ^property[0].valueCode = #_mdc 
* #131927 ^property[1].code = #hints 
* #131927 ^property[1].valueString = "PART: 2 ~ CODE10: 855" 
* #131931 "MDC_ECG_AMPL_ST_MCL"
* #131931 ^property[0].code = #parent 
* #131931 ^property[0].valueCode = #_mdc 
* #131931 ^property[1].code = #hints 
* #131931 ^property[1].valueString = "PART: 2 ~ CODE10: 859" 
* #131932 "MDC_ECG_AMPL_ST_MCL1"
* #131932 ^property[0].code = #parent 
* #131932 ^property[0].valueCode = #_mdc 
* #131932 ^property[1].code = #hints 
* #131932 ^property[1].valueString = "PART: 2 ~ CODE10: 860" 
* #131937 "MDC_ECG_AMPL_ST_MCL6"
* #131937 ^property[0].code = #parent 
* #131937 ^property[0].valueCode = #_mdc 
* #131937 ^property[1].code = #hints 
* #131937 ^property[1].valueString = "PART: 2 ~ CODE10: 865" 
* #131971 "MDC_ECG_AMPL_ST_ES"
* #131971 ^property[0].code = #parent 
* #131971 ^property[0].valueCode = #_mdc 
* #131971 ^property[1].code = #hints 
* #131971 ^property[1].valueString = "PART: 2 ~ CODE10: 899" 
* #131972 "MDC_ECG_AMPL_ST_AS"
* #131972 ^property[0].code = #parent 
* #131972 ^property[0].valueCode = #_mdc 
* #131972 ^property[1].code = #hints 
* #131972 ^property[1].valueString = "PART: 2 ~ CODE10: 900" 
* #131973 "MDC_ECG_AMPL_ST_AI"
* #131973 ^property[0].code = #parent 
* #131973 ^property[0].valueCode = #_mdc 
* #131973 ^property[1].code = #hints 
* #131973 ^property[1].valueString = "PART: 2 ~ CODE10: 901" 
* #147232 "MDC_ECG_TIME_PD_QT_GL"
* #147232 ^definition = Duration | ECG; QT | Heart | CVS
* #147232 ^designation[0].language = #de-AT 
* #147232 ^designation[0].value = "Q-T interval" 
* #147232 ^property[0].code = #parent 
* #147232 ^property[0].valueCode = #_mdc 
* #147232 ^property[1].code = #hints 
* #147232 ^property[1].valueString = "PART: 2 ~ CODE10: 16160" 
* #147236 "MDC_ECG_TIME_PD_QTc"
* #147236 ^definition = Duration | ECG; QTc | Heart | CVS
* #147236 ^designation[0].language = #de-AT 
* #147236 ^designation[0].value = "Q-T c" 
* #147236 ^property[0].code = #parent 
* #147236 ^property[0].valueCode = #_mdc 
* #147236 ^property[1].code = #hints 
* #147236 ^property[1].valueString = "PART: 2 ~ CODE10: 16164" 
* #147240 "MDC_ECG_TIME_PD_RR_GL"
* #147240 ^definition = Duration | ECG; RR | Heart | CVS
* #147240 ^designation[0].language = #de-AT 
* #147240 ^designation[0].value = "R-R interval" 
* #147240 ^property[0].code = #parent 
* #147240 ^property[0].valueCode = #_mdc 
* #147240 ^property[1].code = #hints 
* #147240 ^property[1].valueString = "PART: 2 ~ CODE10: 16168" 
* #147626 "MDC_ECG_PACED_BEAT_RATE"
* #147626 ^property[0].code = #parent 
* #147626 ^property[0].valueCode = #_mdc 
* #147626 ^property[1].code = #hints 
* #147626 ^property[1].valueString = "PART: 2 ~ CODE10: 16554" 
* #147842 "MDC_ECG_HEART_RATE"
* #147842 ^definition = Rate | beats | Heart | CVS
* #147842 ^designation[0].language = #de-AT 
* #147842 ^designation[0].value = "heart rate" 
* #147842 ^property[0].code = #parent 
* #147842 ^property[0].valueCode = #_mdc 
* #147842 ^property[1].code = #hints 
* #147842 ^property[1].valueString = "PART: 2 ~ CODE10: 16770" 
* #147850 "MDC_ECG_CARD_BEAT_RATE_BTB"
* #147850 ^property[0].code = #parent 
* #147850 ^property[0].valueCode = #_mdc 
* #147850 ^property[1].code = #hints 
* #147850 ^property[1].valueString = "PART: 2 ~ CODE10: 16778" 
* #148065 "MDC_ECG_V_P_C_CNT"
* #148065 ^property[0].code = #parent 
* #148065 ^property[0].valueCode = #_mdc 
* #148065 ^property[1].code = #hints 
* #148065 ^property[1].valueString = "PART: 2 ~ CODE10: 16993" 
* #148066 "MDC_ECG_V_P_C_RATE"
* #148066 ^property[0].code = #parent 
* #148066 ^property[0].valueCode = #_mdc 
* #148066 ^property[1].code = #hints 
* #148066 ^property[1].valueString = "PART: 2 ~ CODE10: 16994" 
* #148496 "MDC_ECG_ARRHY"
* #148496 ^definition = Pattern | Arrhythmia | ECG; Heart | CVS
* #148496 ^property[0].code = #parent 
* #148496 ^property[0].valueCode = #_mdc 
* #148496 ^property[1].code = #hints 
* #148496 ^property[1].valueString = "PART: 2 ~ CODE10: 17424" 
* #149514 "MDC_PULS_RATE"
* #149514 ^definition = Rate |  | Pulse | Blood; CVS
* #149514 ^designation[0].language = #de-AT 
* #149514 ^designation[0].value = "Pulse rate" 
* #149514 ^property[0].code = #parent 
* #149514 ^property[0].valueCode = #_mdc 
* #149514 ^property[1].code = #hints 
* #149514 ^property[1].valueString = "PART: 2 ~ CODE10: 18442" 
* #149522 "MDC_BLD_PULS_RATE_INV"
* #149522 ^definition = Rate | Invasive | Pulse | Blood; CVS
* #149522 ^designation[0].language = #de-AT 
* #149522 ^designation[0].value = "Invasive pulse rate" 
* #149522 ^property[0].code = #parent 
* #149522 ^property[0].valueCode = #_mdc 
* #149522 ^property[1].code = #hints 
* #149522 ^property[1].valueString = "PART: 2 ~ CODE10: 18450" 
* #149530 "MDC_PULS_OXIM_PULS_RATE"
* #149530 ^property[0].code = #parent 
* #149530 ^property[0].valueCode = #_mdc 
* #149530 ^property[1].code = #hints 
* #149530 ^property[1].valueString = "PART: 2 ~ CODE10: 18458" 
* #149546 "MDC_PULS_RATE_NON_INV"
* #149546 ^definition = Rate | Noninvasive | Pulse | Blood; CVS
* #149546 ^designation[0].language = #de-AT 
* #149546 ^designation[0].value = "Noninvasive pulse rate" 
* #149546 ^property[0].code = #parent 
* #149546 ^property[0].valueCode = #_mdc 
* #149546 ^property[1].code = #hints 
* #149546 ^property[1].valueString = "PART: 2 ~ CODE10: 18474" 
* #149554 "MDC_TTHOR_HEART_RATE"
* #149554 ^definition = Rate | Beats | Heart | CVS; transthoracic impedance
* #149554 ^designation[0].language = #de-AT 
* #149554 ^designation[0].value = "Heart rate by transthoracic impedance" 
* #149554 ^property[0].code = #parent 
* #149554 ^property[0].valueCode = #_mdc 
* #149554 ^property[1].code = #hints 
* #149554 ^property[1].valueString = "PART: 2 ~ CODE10: 18482" 
* #149562 "MDC_PALPATION_HEART_RATE"
* #149562 ^definition = Rate | Beats | Heart | CVS; manual palpation
* #149562 ^designation[0].language = #de-AT 
* #149562 ^designation[0].value = "Heart rate by palpation" 
* #149562 ^property[0].code = #parent 
* #149562 ^property[0].valueCode = #_mdc 
* #149562 ^property[1].code = #hints 
* #149562 ^property[1].valueString = "PART: 2 ~ CODE10: 18490" 
* #149760 "MDC_RES_VASC_SYS_INDEX"
* #149760 ^definition = Resistance | Difference(SystemicAndPulmonary) | Flow | Blood; CVS
* #149760 ^designation[0].language = #de-AT 
* #149760 ^designation[0].value = "Systemic vascular resistance indexed" 
* #149760 ^property[0].code = #parent 
* #149760 ^property[0].valueCode = #_mdc 
* #149760 ^property[1].code = #hints 
* #149760 ^property[1].valueString = "PART: 2 ~ CODE10: 18688" 
* #149764 "MDC_WK_LV_STROKE_INDEX"
* #149764 ^definition = Index | | Work; PerStroke; PerSurfaceArea | LeftVentricle; CVS
* #149764 ^designation[0].language = #de-AT 
* #149764 ^designation[0].value = "Left Ventricular Stroke Work Index" 
* #149764 ^property[0].code = #parent 
* #149764 ^property[0].valueCode = #_mdc 
* #149764 ^property[1].code = #hints 
* #149764 ^property[1].valueString = "PART: 2 ~ CODE10: 18692" 
* #149772 "MDC_OUTPUT_CARD_INDEX"
* #149772 ^definition = Index | PerMinute | Volume | Blood; LeftVentricle; CVS
* #149772 ^designation[0].language = #de-AT 
* #149772 ^designation[0].value = "Cardiac index" 
* #149772 ^property[0].code = #parent 
* #149772 ^property[0].valueCode = #_mdc 
* #149772 ^property[1].code = #hints 
* #149772 ^property[1].valueString = "PART: 2 ~ CODE10: 18700" 
* #150016 "MDC_PRESS_BLD"
* #150016 ^definition = Pressure |  | Blood | CVS
* #150016 ^designation[0].language = #de-AT 
* #150016 ^designation[0].value = "Blood pressure" 
* #150016 ^property[0].code = #parent 
* #150016 ^property[0].valueCode = #_mdc 
* #150016 ^property[1].code = #hints 
* #150016 ^property[1].valueString = "PART: 2 ~ CODE10: 18944" 
* #150017 "MDC_PRESS_BLD_SYS"
* #150017 ^definition = Pressure | Systolic | Blood | CVS
* #150017 ^designation[0].language = #de-AT 
* #150017 ^designation[0].value = "Systolic blood pressure" 
* #150017 ^property[0].code = #parent 
* #150017 ^property[0].valueCode = #_mdc 
* #150017 ^property[1].code = #hints 
* #150017 ^property[1].valueString = "PART: 2 ~ CODE10: 18945" 
* #150018 "MDC_PRESS_BLD_DIA"
* #150018 ^definition = Pressure | Diastolic | Blood | CVS
* #150018 ^designation[0].language = #de-AT 
* #150018 ^designation[0].value = "Diastolic blood pressure" 
* #150018 ^property[0].code = #parent 
* #150018 ^property[0].valueCode = #_mdc 
* #150018 ^property[1].code = #hints 
* #150018 ^property[1].valueString = "PART: 2 ~ CODE10: 18946" 
* #150019 "MDC_PRESS_BLD_MEAN"
* #150019 ^definition = Pressure | Mean | Blood | CVS
* #150019 ^designation[0].language = #de-AT 
* #150019 ^designation[0].value = "Mean blood pressure" 
* #150019 ^property[0].code = #parent 
* #150019 ^property[0].valueCode = #_mdc 
* #150019 ^property[1].code = #hints 
* #150019 ^property[1].valueString = "PART: 2 ~ CODE10: 18947" 
* #150020 "MDC_PRESS_BLD_NONINV"
* #150020 ^property[0].code = #parent 
* #150020 ^property[0].valueCode = #_mdc 
* #150020 ^property[1].code = #hints 
* #150020 ^property[1].valueString = "PART: 2 ~ CODE10: 18948" 
* #150021 "MDC_PRESS_BLD_NONINV_SYS"
* #150021 ^definition = Pressure | Noninvasive; Systolic | Blood | CVS
* #150021 ^designation[0].language = #de-AT 
* #150021 ^designation[0].value = "Noninvasive systolic blood pressure" 
* #150021 ^property[0].code = #parent 
* #150021 ^property[0].valueCode = #_mdc 
* #150021 ^property[1].code = #hints 
* #150021 ^property[1].valueString = "PART: 2 ~ CODE10: 18949" 
* #150022 "MDC_PRESS_BLD_NONINV_DIA"
* #150022 ^definition = Pressure | Noninvasive; Diastolic | Blood | CVS
* #150022 ^designation[0].language = #de-AT 
* #150022 ^designation[0].value = "Noninvasive diastolic blood pressure" 
* #150022 ^property[0].code = #parent 
* #150022 ^property[0].valueCode = #_mdc 
* #150022 ^property[1].code = #hints 
* #150022 ^property[1].valueString = "PART: 2 ~ CODE10: 18950" 
* #150023 "MDC_PRESS_BLD_NONINV_MEAN"
* #150023 ^definition = Pressure | Noninvasive; Mean | Blood | CVS
* #150023 ^designation[0].language = #de-AT 
* #150023 ^designation[0].value = "Noninvasive mean blood pressure" 
* #150023 ^property[0].code = #parent 
* #150023 ^property[0].valueCode = #_mdc 
* #150023 ^property[1].code = #hints 
* #150023 ^property[1].valueString = "PART: 2 ~ CODE10: 18951" 
* #150025 "MDC_PRESS_BLD_NONINV_SYS_CTS"
* #150025 ^definition = Pressure | Noninvasive; Continuous; Systolic | Blood | CVS
* #150025 ^designation[0].language = #de-AT 
* #150025 ^designation[0].value = "Continuous; noninvasive systolic blood pressure" 
* #150025 ^property[0].code = #parent 
* #150025 ^property[0].valueCode = #_mdc 
* #150025 ^property[1].code = #hints 
* #150025 ^property[1].valueString = "PART: 2 ~ CODE10: 18953" 
* #150026 "MDC_PRESS_BLD_NONINV_DIA_CTS"
* #150026 ^definition = Pressure | Noninvasive; Continuous; Diastolic | Blood | CVS
* #150026 ^designation[0].language = #de-AT 
* #150026 ^designation[0].value = "Continuous; noninvasive diastolic blood pressure" 
* #150026 ^property[0].code = #parent 
* #150026 ^property[0].valueCode = #_mdc 
* #150026 ^property[1].code = #hints 
* #150026 ^property[1].valueString = "PART: 2 ~ CODE10: 18954" 
* #150027 "MDC_PRESS_BLD_NONINV_MEAN_CTS"
* #150027 ^definition = Pressure | Noninvasive; Continuous; Mean | Blood | CVS
* #150027 ^designation[0].language = #de-AT 
* #150027 ^designation[0].value = "Continuous; noninvasive mean blood pressure" 
* #150027 ^property[0].code = #parent 
* #150027 ^property[0].valueCode = #_mdc 
* #150027 ^property[1].code = #hints 
* #150027 ^property[1].valueString = "PART: 2 ~ CODE10: 18955" 
* #150028 "MDC_PRESS_BLD_AORT"
* #150028 ^definition = Pressure |  | Blood | Aorta; CVS
* #150028 ^designation[0].language = #de-AT 
* #150028 ^designation[0].value = "Aortic pressure" 
* #150028 ^property[0].code = #parent 
* #150028 ^property[0].valueCode = #_mdc 
* #150028 ^property[1].code = #hints 
* #150028 ^property[1].valueString = "PART: 2 ~ CODE10: 18956" 
* #150029 "MDC_PRESS_BLD_AORT_SYS"
* #150029 ^definition = Pressure | Systolic | Blood | Aorta; CVS
* #150029 ^designation[0].language = #de-AT 
* #150029 ^designation[0].value = "Systolic aortic pressure" 
* #150029 ^property[0].code = #parent 
* #150029 ^property[0].valueCode = #_mdc 
* #150029 ^property[1].code = #hints 
* #150029 ^property[1].valueString = "PART: 2 ~ CODE10: 18957" 
* #150030 "MDC_PRESS_BLD_AORT_DIA"
* #150030 ^property[0].code = #parent 
* #150030 ^property[0].valueCode = #_mdc 
* #150030 ^property[1].code = #hints 
* #150030 ^property[1].valueString = "PART: 2 ~ CODE10: 18958" 
* #150031 "MDC_PRESS_BLD_AORT_MEAN"
* #150031 ^definition = Pressure | Mean | Blood | Artery; CVS
* #150031 ^designation[0].language = #de-AT 
* #150031 ^designation[0].value = "Mean aortic pressure" 
* #150031 ^property[0].code = #parent 
* #150031 ^property[0].valueCode = #_mdc 
* #150031 ^property[1].code = #hints 
* #150031 ^property[1].valueString = "PART: 2 ~ CODE10: 18959" 
* #150032 "MDC_PRESS_BLD_ART"
* #150032 ^definition = Pressure |  | Blood | Artery; CVS
* #150032 ^designation[0].language = #de-AT 
* #150032 ^designation[0].value = "Arterial pressure" 
* #150032 ^property[0].code = #parent 
* #150032 ^property[0].valueCode = #_mdc 
* #150032 ^property[1].code = #hints 
* #150032 ^property[1].valueString = "PART: 2 ~ CODE10: 18960" 
* #150033 "MDC_PRESS_BLD_ART_SYS"
* #150033 ^definition = Pressure | Systolic | Blood | Artery; CVS
* #150033 ^designation[0].language = #de-AT 
* #150033 ^designation[0].value = "Systolic arterial pressure" 
* #150033 ^property[0].code = #parent 
* #150033 ^property[0].valueCode = #_mdc 
* #150033 ^property[1].code = #hints 
* #150033 ^property[1].valueString = "PART: 2 ~ CODE10: 18961" 
* #150034 "MDC_PRESS_BLD_ART_DIA"
* #150034 ^definition = Pressure |  | Blood | CentralVein; CVS
* #150034 ^designation[0].language = #de-AT 
* #150034 ^designation[0].value = "Diastolic arterial pressure" 
* #150034 ^property[0].code = #parent 
* #150034 ^property[0].valueCode = #_mdc 
* #150034 ^property[1].code = #hints 
* #150034 ^property[1].valueString = "PART: 2 ~ CODE10: 18962" 
* #150035 "MDC_PRESS_BLD_ART_MEAN"
* #150035 ^definition = Pressure | Mean | Blood | Aorta; CVS
* #150035 ^designation[0].language = #de-AT 
* #150035 ^designation[0].value = "Mean arterial pressure" 
* #150035 ^property[0].code = #parent 
* #150035 ^property[0].valueCode = #_mdc 
* #150035 ^property[1].code = #hints 
* #150035 ^property[1].valueString = "PART: 2 ~ CODE10: 18963" 
* #150036 "MDC_PRESS_BLD_ART_ABP"
* #150036 ^property[0].code = #parent 
* #150036 ^property[0].valueCode = #_mdc 
* #150036 ^property[1].code = #hints 
* #150036 ^property[1].valueString = "PART: 2 ~ CODE10: 18964" 
* #150037 "MDC_PRESS_BLD_ART_ABP_SYS"
* #150037 ^property[0].code = #parent 
* #150037 ^property[0].valueCode = #_mdc 
* #150037 ^property[1].code = #hints 
* #150037 ^property[1].valueString = "PART: 2 ~ CODE10: 18965" 
* #150038 "MDC_PRESS_BLD_ART_ABP_DIA"
* #150038 ^property[0].code = #parent 
* #150038 ^property[0].valueCode = #_mdc 
* #150038 ^property[1].code = #hints 
* #150038 ^property[1].valueString = "PART: 2 ~ CODE10: 18966" 
* #150039 "MDC_PRESS_BLD_ART_ABP_MEAN"
* #150039 ^property[0].code = #parent 
* #150039 ^property[0].valueCode = #_mdc 
* #150039 ^property[1].code = #hints 
* #150039 ^property[1].valueString = "PART: 2 ~ CODE10: 18967" 
* #150044 "MDC_PRESS_BLD_ART_PULM"
* #150044 ^definition = Pressure |  | Blood | PulmonaryArtery; CVS
* #150044 ^designation[0].language = #de-AT 
* #150044 ^designation[0].value = "Pulmonary arterial pressure" 
* #150044 ^property[0].code = #parent 
* #150044 ^property[0].valueCode = #_mdc 
* #150044 ^property[1].code = #hints 
* #150044 ^property[1].valueString = "PART: 2 ~ CODE10: 18972" 
* #150045 "MDC_PRESS_BLD_ART_PULM_SYS"
* #150045 ^definition = Pressure | Systolic | Blood | PulmonaryArtery; CVS
* #150045 ^designation[0].language = #de-AT 
* #150045 ^designation[0].value = "Systolic pulmonary arterial pressure" 
* #150045 ^property[0].code = #parent 
* #150045 ^property[0].valueCode = #_mdc 
* #150045 ^property[1].code = #hints 
* #150045 ^property[1].valueString = "PART: 2 ~ CODE10: 18973" 
* #150046 "MDC_PRESS_BLD_ART_PULM_DIA"
* #150046 ^definition = Pressure | Diastolic | Blood | PulmonaryArtery; CVS
* #150046 ^designation[0].language = #de-AT 
* #150046 ^designation[0].value = "Diastolic pulmonary arterial pressure" 
* #150046 ^property[0].code = #parent 
* #150046 ^property[0].valueCode = #_mdc 
* #150046 ^property[1].code = #hints 
* #150046 ^property[1].valueString = "PART: 2 ~ CODE10: 18974" 
* #150047 "MDC_PRESS_BLD_ART_PULM_MEAN"
* #150047 ^definition = Pressure | Mean | Blood | PulmonaryArtery; CVS
* #150047 ^designation[0].language = #de-AT 
* #150047 ^designation[0].value = "Mean pulmonary arterial pressure" 
* #150047 ^property[0].code = #parent 
* #150047 ^property[0].valueCode = #_mdc 
* #150047 ^property[1].code = #hints 
* #150047 ^property[1].valueString = "PART: 2 ~ CODE10: 18975" 
* #150052 "MDC_PRESS_BLD_ART_PULM_WEDGE"
* #150052 ^definition = Pressure |  | Blood | MarginalBranch; RightCoronaryArtery; Heart; CVS
* #150052 ^designation[0].language = #de-AT 
* #150052 ^designation[0].value = "Pulmonary artery wedge pressure" 
* #150052 ^property[0].code = #parent 
* #150052 ^property[0].valueCode = #_mdc 
* #150052 ^property[1].code = #hints 
* #150052 ^property[1].valueString = "PART: 2 ~ CODE10: 18980" 
* #150056 "MDC_PRESS_BLD_ART_UMB"
* #150056 ^definition = Pressure |  | Blood | UmbilicalArtery; CVS
* #150056 ^designation[0].language = #de-AT 
* #150056 ^designation[0].value = "Umbilical pressure" 
* #150056 ^property[0].code = #parent 
* #150056 ^property[0].valueCode = #_mdc 
* #150056 ^property[1].code = #hints 
* #150056 ^property[1].valueString = "PART: 2 ~ CODE10: 18984" 
* #150057 "MDC_PRESS_BLD_ART_UMB_SYS"
* #150057 ^definition = Pressure | Systolic | Blood | UmbilicalArtery; CVS
* #150057 ^designation[0].language = #de-AT 
* #150057 ^designation[0].value = "Systolic umbilical arterial pressure" 
* #150057 ^property[0].code = #parent 
* #150057 ^property[0].valueCode = #_mdc 
* #150057 ^property[1].code = #hints 
* #150057 ^property[1].valueString = "PART: 2 ~ CODE10: 18985" 
* #150058 "MDC_PRESS_BLD_ART_UMB_DIA"
* #150058 ^definition = Pressure | Diastolic | Blood | UmbilicalArtery; CVS
* #150058 ^designation[0].language = #de-AT 
* #150058 ^designation[0].value = "Diastolic umbilical arterial pressure" 
* #150058 ^property[0].code = #parent 
* #150058 ^property[0].valueCode = #_mdc 
* #150058 ^property[1].code = #hints 
* #150058 ^property[1].valueString = "PART: 2 ~ CODE10: 18986" 
* #150059 "MDC_PRESS_BLD_ART_UMB_MEAN"
* #150059 ^definition = Pressure | Mean | Blood | UmbilicalArtery; CVS
* #150059 ^designation[0].language = #de-AT 
* #150059 ^designation[0].value = "Mean umbilical arterial pressure" 
* #150059 ^property[0].code = #parent 
* #150059 ^property[0].valueCode = #_mdc 
* #150059 ^property[1].code = #hints 
* #150059 ^property[1].valueString = "PART: 2 ~ CODE10: 18987" 
* #150064 "MDC_PRESS_BLD_ATR_LEFT"
* #150064 ^definition = Pressure |  | Blood | LeftAtrium; CVS
* #150064 ^designation[0].language = #de-AT 
* #150064 ^designation[0].value = "Left atrial pressure" 
* #150064 ^property[0].code = #parent 
* #150064 ^property[0].valueCode = #_mdc 
* #150064 ^property[1].code = #hints 
* #150064 ^property[1].valueString = "PART: 2 ~ CODE10: 18992" 
* #150065 "MDC_PRESS_BLD_ATR_LEFT_SYS"
* #150065 ^definition = Pressure | Systolic | Blood | LeftAtrium; CVS
* #150065 ^designation[0].language = #de-AT 
* #150065 ^designation[0].value = "Systolic left atrial pressure" 
* #150065 ^property[0].code = #parent 
* #150065 ^property[0].valueCode = #_mdc 
* #150065 ^property[1].code = #hints 
* #150065 ^property[1].valueString = "PART: 2 ~ CODE10: 18993" 
* #150066 "MDC_PRESS_BLD_ATR_LEFT_DIA"
* #150066 ^definition = Pressure | Diastolic | Blood | LeftAtrium; CVS
* #150066 ^designation[0].language = #de-AT 
* #150066 ^designation[0].value = "Diastolic left atrial pressure" 
* #150066 ^property[0].code = #parent 
* #150066 ^property[0].valueCode = #_mdc 
* #150066 ^property[1].code = #hints 
* #150066 ^property[1].valueString = "PART: 2 ~ CODE10: 18994" 
* #150067 "MDC_PRESS_BLD_ATR_LEFT_MEAN"
* #150067 ^definition = Pressure | Mean | Blood | LeftAtrium; CVS
* #150067 ^designation[0].language = #de-AT 
* #150067 ^designation[0].value = "Mean left atrial pressure" 
* #150067 ^property[0].code = #parent 
* #150067 ^property[0].valueCode = #_mdc 
* #150067 ^property[1].code = #hints 
* #150067 ^property[1].valueString = "PART: 2 ~ CODE10: 18995" 
* #150068 "MDC_PRESS_BLD_ATR_RIGHT"
* #150068 ^definition = Pressure |  | Blood | RightAtrium; CVS
* #150068 ^designation[0].language = #de-AT 
* #150068 ^designation[0].value = "Right atrial pressure" 
* #150068 ^property[0].code = #parent 
* #150068 ^property[0].valueCode = #_mdc 
* #150068 ^property[1].code = #hints 
* #150068 ^property[1].valueString = "PART: 2 ~ CODE10: 18996" 
* #150069 "MDC_PRESS_BLD_ATR_RIGHT_SYS"
* #150069 ^definition = Pressure | Systolic | Blood | RightAtrium; CVS
* #150069 ^designation[0].language = #de-AT 
* #150069 ^designation[0].value = "Systolic right atrial pressure" 
* #150069 ^property[0].code = #parent 
* #150069 ^property[0].valueCode = #_mdc 
* #150069 ^property[1].code = #hints 
* #150069 ^property[1].valueString = "PART: 2 ~ CODE10: 18997" 
* #150070 "MDC_PRESS_BLD_ATR_RIGHT_DIA"
* #150070 ^definition = Pressure | Diastolic | Blood | RightAtrium; CVS
* #150070 ^designation[0].language = #de-AT 
* #150070 ^designation[0].value = "Diastolic right atrial pressure" 
* #150070 ^property[0].code = #parent 
* #150070 ^property[0].valueCode = #_mdc 
* #150070 ^property[1].code = #hints 
* #150070 ^property[1].valueString = "PART: 2 ~ CODE10: 18998" 
* #150071 "MDC_PRESS_BLD_ATR_RIGHT_MEAN"
* #150071 ^definition = Pressure | Mean | Blood | RightAtrium; CVS
* #150071 ^designation[0].language = #de-AT 
* #150071 ^designation[0].value = "Mean right atrial pressure" 
* #150071 ^property[0].code = #parent 
* #150071 ^property[0].valueCode = #_mdc 
* #150071 ^property[1].code = #hints 
* #150071 ^property[1].valueString = "PART: 2 ~ CODE10: 18999" 
* #150076 "MDC_PRESS_BLD_PULM_CAP"
* #150076 ^definition = Pressure |  | Blood | PulmonaryCapillary; CVS
* #150076 ^designation[0].language = #de-AT 
* #150076 ^designation[0].value = "Pulmonary capillary blood pressure" 
* #150076 ^property[0].code = #parent 
* #150076 ^property[0].valueCode = #_mdc 
* #150076 ^property[1].code = #hints 
* #150076 ^property[1].valueString = "PART: 2 ~ CODE10: 19004" 
* #150077 "MDC_PRESS_BLD_PULM_CAP_SYS"
* #150077 ^definition = Pressure | Systolic | Blood | PulmonaryCapillary; CVS
* #150077 ^designation[0].language = #de-AT 
* #150077 ^designation[0].value = "Systolic pulmonary capillary pressure" 
* #150077 ^property[0].code = #parent 
* #150077 ^property[0].valueCode = #_mdc 
* #150077 ^property[1].code = #hints 
* #150077 ^property[1].valueString = "PART: 2 ~ CODE10: 19005" 
* #150078 "MDC_PRESS_BLD_PULM_CAP_DIA"
* #150078 ^definition = Pressure | Diastolic | Blood | PulmonaryCapillary; CVS
* #150078 ^designation[0].language = #de-AT 
* #150078 ^designation[0].value = "Diastolic pulmonary capillary pressure" 
* #150078 ^property[0].code = #parent 
* #150078 ^property[0].valueCode = #_mdc 
* #150078 ^property[1].code = #hints 
* #150078 ^property[1].valueString = "PART: 2 ~ CODE10: 19006" 
* #150079 "MDC_PRESS_BLD_PULM_CAP_MEAN"
* #150079 ^definition = Pressure | Mean | Blood | PulmonaryCapillary; CVS
* #150079 ^designation[0].language = #de-AT 
* #150079 ^designation[0].value = "Mean pulmonary capillary pressure" 
* #150079 ^property[0].code = #parent 
* #150079 ^property[0].valueCode = #_mdc 
* #150079 ^property[1].code = #hints 
* #150079 ^property[1].valueString = "PART: 2 ~ CODE10: 19007" 
* #150084 "MDC_PRESS_BLD_VEN_CENT"
* #150084 ^definition = Pressure |  | Blood | CentralVein; CVS
* #150084 ^designation[0].language = #de-AT 
* #150084 ^designation[0].value = "Central venous pressure" 
* #150084 ^property[0].code = #parent 
* #150084 ^property[0].valueCode = #_mdc 
* #150084 ^property[1].code = #hints 
* #150084 ^property[1].valueString = "PART: 2 ~ CODE10: 19012" 
* #150085 "MDC_PRESS_BLD_VEN_CENT_SYS"
* #150085 ^definition = Pressure | Systolic | Blood | CentralVein; CVS
* #150085 ^designation[0].language = #de-AT 
* #150085 ^designation[0].value = "Systolic central venous pressure" 
* #150085 ^property[0].code = #parent 
* #150085 ^property[0].valueCode = #_mdc 
* #150085 ^property[1].code = #hints 
* #150085 ^property[1].valueString = "PART: 2 ~ CODE10: 19013" 
* #150086 "MDC_PRESS_BLD_VEN_CENT_DIA"
* #150086 ^definition = Pressure | Diastolic | Blood | CentralVein; CVS
* #150086 ^designation[0].language = #de-AT 
* #150086 ^designation[0].value = "Diastolic central venous pressure" 
* #150086 ^property[0].code = #parent 
* #150086 ^property[0].valueCode = #_mdc 
* #150086 ^property[1].code = #hints 
* #150086 ^property[1].valueString = "PART: 2 ~ CODE10: 19014" 
* #150087 "MDC_PRESS_BLD_VEN_CENT_MEAN"
* #150087 ^definition = Pressure | Mean | Blood | CentralVein; CVS
* #150087 ^designation[0].language = #de-AT 
* #150087 ^designation[0].value = "Mean central venous pressure" 
* #150087 ^property[0].code = #parent 
* #150087 ^property[0].valueCode = #_mdc 
* #150087 ^property[1].code = #hints 
* #150087 ^property[1].valueString = "PART: 2 ~ CODE10: 19015" 
* #150088 "MDC_PRESS_BLD_VEN_UMB"
* #150088 ^definition = Pressure |  | Blood | UmbilicalVein; CVS
* #150088 ^designation[0].language = #de-AT 
* #150088 ^designation[0].value = "Umbilical venous blood pressure" 
* #150088 ^property[0].code = #parent 
* #150088 ^property[0].valueCode = #_mdc 
* #150088 ^property[1].code = #hints 
* #150088 ^property[1].valueString = "PART: 2 ~ CODE10: 19016" 
* #150089 "MDC_PRESS_BLD_VEN_UMB_SYS"
* #150089 ^definition = Pressure | Systolic | Blood | UmbilicalVein; CVS
* #150089 ^designation[0].language = #de-AT 
* #150089 ^designation[0].value = "Systolic umbilical venous pressure" 
* #150089 ^property[0].code = #parent 
* #150089 ^property[0].valueCode = #_mdc 
* #150089 ^property[1].code = #hints 
* #150089 ^property[1].valueString = "PART: 2 ~ CODE10: 19017" 
* #150090 "MDC_PRESS_BLD_VEN_UMB_DIA"
* #150090 ^definition = Pressure | Diastolic | Blood | UmbilicalVein; CVS
* #150090 ^designation[0].language = #de-AT 
* #150090 ^designation[0].value = "Diastolic umbilical venous pressure" 
* #150090 ^property[0].code = #parent 
* #150090 ^property[0].valueCode = #_mdc 
* #150090 ^property[1].code = #hints 
* #150090 ^property[1].valueString = "PART: 2 ~ CODE10: 19018" 
* #150091 "MDC_PRESS_BLD_VEN_UMB_MEAN"
* #150091 ^definition = Pressure | Mean | Blood | UmbilicalVein; CVS
* #150091 ^designation[0].language = #de-AT 
* #150091 ^designation[0].value = "Mean umbilical venous pressure" 
* #150091 ^property[0].code = #parent 
* #150091 ^property[0].valueCode = #_mdc 
* #150091 ^property[1].code = #hints 
* #150091 ^property[1].valueString = "PART: 2 ~ CODE10: 19019" 
* #150101 "MDC_PRESS_BLD_VENT_LEFT_SYS"
* #150101 ^definition = Pressure | Systolic | Blood | LeftVentricle; CVS
* #150101 ^designation[0].language = #de-AT 
* #150101 ^designation[0].value = "Systolic left ventricular pressure" 
* #150101 ^property[0].code = #parent 
* #150101 ^property[0].valueCode = #_mdc 
* #150101 ^property[1].code = #hints 
* #150101 ^property[1].valueString = "PART: 2 ~ CODE10: 19029" 
* #150102 "MDC_PRESS_BLD_VENT_LEFT_DIA"
* #150102 ^definition = Pressure | Diastolic | Blood | LeftVentricle; CVS
* #150102 ^designation[0].language = #de-AT 
* #150102 ^designation[0].value = "Diastolic left ventricular pressure" 
* #150102 ^property[0].code = #parent 
* #150102 ^property[0].valueCode = #_mdc 
* #150102 ^property[1].code = #hints 
* #150102 ^property[1].valueString = "PART: 2 ~ CODE10: 19030" 
* #150103 "MDC_PRESS_BLD_VENT_LEFT_MEAN"
* #150103 ^definition = Pressure | Diastolic | Blood | RightVentricle; CVS
* #150103 ^designation[0].language = #de-AT 
* #150103 ^designation[0].value = "Mean left ventricular pressure" 
* #150103 ^property[0].code = #parent 
* #150103 ^property[0].valueCode = #_mdc 
* #150103 ^property[1].code = #hints 
* #150103 ^property[1].valueString = "PART: 2 ~ CODE10: 19031" 
* #150105 "MDC_PRESS_BLD_VENT_RIGHT_SYS"
* #150105 ^definition = Pressure | Systolic | Blood | RightVentricle; CVS
* #150105 ^designation[0].language = #de-AT 
* #150105 ^designation[0].value = "Systolic right ventricular pressure" 
* #150105 ^property[0].code = #parent 
* #150105 ^property[0].valueCode = #_mdc 
* #150105 ^property[1].code = #hints 
* #150105 ^property[1].valueString = "PART: 2 ~ CODE10: 19033" 
* #150106 "MDC_PRESS_BLD_VENT_RIGHT_DIA"
* #150106 ^definition = Pressure | Diastolic | Blood | RightVentricle; CVS
* #150106 ^designation[0].language = #de-AT 
* #150106 ^designation[0].value = "Diastolic right ventricular pressure" 
* #150106 ^property[0].code = #parent 
* #150106 ^property[0].valueCode = #_mdc 
* #150106 ^property[1].code = #hints 
* #150106 ^property[1].valueString = "PART: 2 ~ CODE10: 19034" 
* #150107 "MDC_PRESS_BLD_VENT_RIGHT_MEAN"
* #150107 ^definition = Pressure | Mean | Blood | RightVentricle; CVS
* #150107 ^designation[0].language = #de-AT 
* #150107 ^designation[0].value = "Mean right ventricular pressure" 
* #150107 ^property[0].code = #parent 
* #150107 ^property[0].valueCode = #_mdc 
* #150107 ^property[1].code = #hints 
* #150107 ^property[1].valueString = "PART: 2 ~ CODE10: 19035" 
* #150272 "MDC_SAT_O2_CONSUMP"
* #150272 ^definition = Volume | PerMinute | ConsumedOxygen | Blood; CVS
* #150272 ^designation[0].language = #de-AT 
* #150272 ^designation[0].value = "Oxygen consumption" 
* #150272 ^property[0].code = #parent 
* #150272 ^property[0].valueCode = #_mdc 
* #150272 ^property[1].code = #hints 
* #150272 ^property[1].valueString = "PART: 2 ~ CODE10: 19200" 
* #150276 "MDC_OUTPUT_CARD"
* #150276 ^definition = Volume | PerMinute | Blood | LeftVentricle; CVS
* #150276 ^designation[0].language = #de-AT 
* #150276 ^designation[0].value = "Cardiac output" 
* #150276 ^property[0].code = #parent 
* #150276 ^property[0].valueCode = #_mdc 
* #150276 ^property[1].code = #hints 
* #150276 ^property[1].valueString = "PART: 2 ~ CODE10: 19204" 
* #150300 "MDC_PRESS_CUFF"
* #150300 ^property[0].code = #parent 
* #150300 ^property[0].valueCode = #_mdc 
* #150300 ^property[1].code = #hints 
* #150300 ^property[1].valueString = "PART: 2 ~ CODE10: 19228" 
* #150301 "MDC_PRESS_CUFF_SYS"
* #150301 ^definition = Pressure | Noninvasive; Discontinuous; Systolic | Blood | CVS
* #150301 ^designation[0].language = #de-AT 
* #150301 ^designation[0].value = "Discontinuous; noninvasive systolic blood pressure" 
* #150301 ^property[0].code = #parent 
* #150301 ^property[0].valueCode = #_mdc 
* #150301 ^property[1].code = #hints 
* #150301 ^property[1].valueString = "PART: 2 ~ CODE10: 19229" 
* #150302 "MDC_PRESS_CUFF_DIA"
* #150302 ^definition = Pressure | Noninvasive; Discontinuous; Diastolic | Blood | CVS
* #150302 ^designation[0].language = #de-AT 
* #150302 ^designation[0].value = "Discontinuous; noninvasive diastolic blood pressure" 
* #150302 ^property[0].code = #parent 
* #150302 ^property[0].valueCode = #_mdc 
* #150302 ^property[1].code = #hints 
* #150302 ^property[1].valueString = "PART: 2 ~ CODE10: 19230" 
* #150303 "MDC_PRESS_CUFF_MEAN"
* #150303 ^definition = Pressure | Noninvasive; Discontinuous; Mean | Blood | CVS
* #150303 ^designation[0].language = #de-AT 
* #150303 ^designation[0].value = "Discontinuous; noninvasive mean blood pressure" 
* #150303 ^property[0].code = #parent 
* #150303 ^property[0].valueCode = #_mdc 
* #150303 ^property[1].code = #hints 
* #150303 ^property[1].valueString = "PART: 2 ~ CODE10: 19231" 
* #150304 "MDC_RES_VASC"
* #150304 ^definition = Resistance |  | Flow | Blood; CVS
* #150304 ^designation[0].language = #de-AT 
* #150304 ^designation[0].value = "Vascular resistance" 
* #150304 ^property[0].code = #parent 
* #150304 ^property[0].valueCode = #_mdc 
* #150304 ^property[1].code = #hints 
* #150304 ^property[1].valueString = "PART: 2 ~ CODE10: 19232" 
* #150308 "MDC_RES_VASC_PULM"
* #150308 ^definition = Resistance |  | Flow | PulmonaryBlood; CVS
* #150308 ^designation[0].language = #de-AT 
* #150308 ^designation[0].value = "Pulmonary vascular resistance" 
* #150308 ^property[0].code = #parent 
* #150308 ^property[0].valueCode = #_mdc 
* #150308 ^property[1].code = #hints 
* #150308 ^property[1].valueString = "PART: 2 ~ CODE10: 19236" 
* #150312 "MDC_RES_VASC_SYS"
* #150312 ^definition = Resistance |  | Flow | SystemicBlood; CVS
* #150312 ^designation[0].language = #de-AT 
* #150312 ^designation[0].value = "Systemic vascular resistance" 
* #150312 ^property[0].code = #parent 
* #150312 ^property[0].valueCode = #_mdc 
* #150312 ^property[1].code = #hints 
* #150312 ^property[1].valueString = "PART: 2 ~ CODE10: 19240" 
* #150320 "MDC_SAT_O2_QUAL"
* #150320 ^property[0].code = #parent 
* #150320 ^property[0].valueCode = #_mdc 
* #150320 ^property[1].code = #hints 
* #150320 ^property[1].valueString = "PART: 2 ~ CODE10: 19248" 
* #150324 "MDC_SAT_O2_ART"
* #150324 ^definition = Concentration | Saturation; Oxygen | ArterialBlood | BloodChemistry
* #150324 ^designation[0].language = #de-AT 
* #150324 ^designation[0].value = "Arterial oxygen saturation" 
* #150324 ^property[0].code = #parent 
* #150324 ^property[0].valueCode = #_mdc 
* #150324 ^property[1].code = #hints 
* #150324 ^property[1].valueString = "PART: 2 ~ CODE10: 19252" 
* #150332 "MDC_SAT_O2_VEN"
* #150332 ^definition = Concentration | Saturation; Oxygen | VenousBlood | BloodChemistry
* #150332 ^designation[0].language = #de-AT 
* #150332 ^designation[0].value = "Venous oxygen saturation" 
* #150332 ^property[0].code = #parent 
* #150332 ^property[0].valueCode = #_mdc 
* #150332 ^property[1].code = #hints 
* #150332 ^property[1].valueString = "PART: 2 ~ CODE10: 19260" 
* #150336 "MDC_SAT_DIFF_O2_ART_ALV"
* #150336 ^property[0].code = #parent 
* #150336 ^property[0].valueCode = #_mdc 
* #150336 ^property[1].code = #hints 
* #150336 ^property[1].valueString = "PART: 2 ~ CODE10: 19264" 
* #150344 "MDC_TEMP"
* #150344 ^property[0].code = #parent 
* #150344 ^property[0].valueCode = #_mdc 
* #150344 ^property[1].code = #hints 
* #150344 ^property[1].valueString = "PART: 2 ~ CODE10: 19272" 
* #150348 "MDC_TEMP_FOLEY"
* #150348 ^property[0].code = #parent 
* #150348 ^property[0].valueCode = #_mdc 
* #150348 ^property[1].code = #hints 
* #150348 ^property[1].valueString = "PART: 2 ~ CODE10: 19276" 
* #150352 "MDC_TEMP_ART"
* #150352 ^property[0].code = #parent 
* #150352 ^property[0].valueCode = #_mdc 
* #150352 ^property[1].code = #hints 
* #150352 ^property[1].valueString = "PART: 2 ~ CODE10: 19280" 
* #150356 "MDC_TEMP_AWAY"
* #150356 ^property[0].code = #parent 
* #150356 ^property[0].valueCode = #_mdc 
* #150356 ^property[1].code = #hints 
* #150356 ^property[1].valueString = "PART: 2 ~ CODE10: 19284" 
* #150364 "MDC_TEMP_BODY"
* #150364 ^property[0].code = #parent 
* #150364 ^property[0].valueCode = #_mdc 
* #150364 ^property[1].code = #hints 
* #150364 ^property[1].valueString = "PART: 2 ~ CODE10: 19292" 
* #150368 "MDC_TEMP_CORE"
* #150368 ^property[0].code = #parent 
* #150368 ^property[0].valueCode = #_mdc 
* #150368 ^property[1].code = #hints 
* #150368 ^property[1].valueString = "PART: 2 ~ CODE10: 19296" 
* #150372 "MDC_TEMP_ESOPH"
* #150372 ^property[0].code = #parent 
* #150372 ^property[0].valueCode = #_mdc 
* #150372 ^property[1].code = #hints 
* #150372 ^property[1].valueString = "PART: 2 ~ CODE10: 19300" 
* #150376 "MDC_TEMP_INJ"
* #150376 ^definition = Temperature |  | Injectate | Heart; CVS
* #150376 ^designation[0].language = #de-AT 
* #150376 ^designation[0].value = "Temperature of injectate" 
* #150376 ^property[0].code = #parent 
* #150376 ^property[0].valueCode = #_mdc 
* #150376 ^property[1].code = #hints 
* #150376 ^property[1].valueString = "PART: 2 ~ CODE10: 19304" 
* #150380 "MDC_TEMP_NASOPH"
* #150380 ^property[0].code = #parent 
* #150380 ^property[0].valueCode = #_mdc 
* #150380 ^property[1].code = #hints 
* #150380 ^property[1].valueString = "PART: 2 ~ CODE10: 19308" 
* #150388 "MDC_TEMP_SKIN"
* #150388 ^property[0].code = #parent 
* #150388 ^property[0].valueCode = #_mdc 
* #150388 ^property[1].code = #hints 
* #150388 ^property[1].valueString = "PART: 2 ~ CODE10: 19316" 
* #150392 "MDC_TEMP_TYMP"
* #150392 ^property[0].code = #parent 
* #150392 ^property[0].valueCode = #_mdc 
* #150392 ^property[1].code = #hints 
* #150392 ^property[1].valueString = "PART: 2 ~ CODE10: 19320" 
* #150396 "MDC_TEMP_VEN"
* #150396 ^property[0].code = #parent 
* #150396 ^property[0].valueCode = #_mdc 
* #150396 ^property[1].code = #hints 
* #150396 ^property[1].valueString = "PART: 2 ~ CODE10: 19324" 
* #150404 "MDC_VOL_BLD_STROKE"
* #150404 ^definition = Volume | OneBeat | Blood | CVS
* #150404 ^designation[0].language = #de-AT 
* #150404 ^designation[0].value = "Stroke volume" 
* #150404 ^property[0].code = #parent 
* #150404 ^property[0].valueCode = #_mdc 
* #150404 ^property[1].code = #hints 
* #150404 ^property[1].valueString = "PART: 2 ~ CODE10: 19332" 
* #150416 "MDC_WK_CARD_LEFT"
* #150416 ^definition = Work | OneBeat | Heart | LeftSide; CVS
* #150416 ^designation[0].language = #de-AT 
* #150416 ^designation[0].value = "Left side ventricular stroke" 
* #150416 ^property[0].code = #parent 
* #150416 ^property[0].valueCode = #_mdc 
* #150416 ^property[1].code = #hints 
* #150416 ^property[1].valueString = "PART: 2 ~ CODE10: 19344" 
* #150420 "MDC_WK_CARD_RIGHT"
* #150420 ^definition = Work | OneBeat | Heart | RightSide; CVS
* #150420 ^designation[0].language = #de-AT 
* #150420 ^designation[0].value = "Right side ventricular stroke" 
* #150420 ^property[0].code = #parent 
* #150420 ^property[0].valueCode = #_mdc 
* #150420 ^property[1].code = #hints 
* #150420 ^property[1].valueString = "PART: 2 ~ CODE10: 19348" 
* #150428 "MDC_WK_LV_STROKE"
* #150428 ^definition = Work | OneBeat | Heart | LeftVentricle; CVS
* #150428 ^designation[0].language = #de-AT 
* #150428 ^designation[0].value = "Ventricular stroke" 
* #150428 ^property[0].code = #parent 
* #150428 ^property[0].valueCode = #_mdc 
* #150428 ^property[1].code = #hints 
* #150428 ^property[1].valueString = "PART: 2 ~ CODE10: 19356" 
* #150448 "MDC_PULS_OXIM_PERF_REL"
* #150448 ^property[0].code = #parent 
* #150448 ^property[0].valueCode = #_mdc 
* #150448 ^property[1].code = #hints 
* #150448 ^property[1].valueString = "PART: 2 ~ CODE10: 19376" 
* #150452 "MDC_PULS_OXIM_PLETH"
* #150452 ^property[0].code = #parent 
* #150452 ^property[0].valueCode = #_mdc 
* #150452 ^property[1].code = #hints 
* #150452 ^property[1].valueString = "PART: 2 ~ CODE10: 19380" 
* #150456 "MDC_PULS_OXIM_SAT_O2"
* #150456 ^property[0].code = #parent 
* #150456 ^property[0].valueCode = #_mdc 
* #150456 ^property[1].code = #hints 
* #150456 ^property[1].valueString = "PART: 2 ~ CODE10: 19384" 
* #150468 "MDC_PULS_OXIM_SAT_O2_DIFF"
* #150468 ^property[0].code = #parent 
* #150468 ^property[0].valueCode = #_mdc 
* #150468 ^property[1].code = #hints 
* #150468 ^property[1].valueString = "PART: 2 ~ CODE10: 19396" 
* #150472 "MDC_PULS_OXIM_SAT_O2_ART_LEFT"
* #150472 ^property[0].code = #parent 
* #150472 ^property[0].valueCode = #_mdc 
* #150472 ^property[1].code = #hints 
* #150472 ^property[1].valueString = "PART: 2 ~ CODE10: 19400" 
* #150476 "MDC_PULS_OXIM_SAT_O2_ART_RIGHT"
* #150476 ^property[0].code = #parent 
* #150476 ^property[0].valueCode = #_mdc 
* #150476 ^property[1].code = #hints 
* #150476 ^property[1].valueString = "PART: 2 ~ CODE10: 19404" 
* #150484 "MDC_DESAT"
* #150484 ^property[0].code = #parent 
* #150484 ^property[0].valueCode = #_mdc 
* #150484 ^property[1].code = #hints 
* #150484 ^property[1].valueString = "PART: 2 ~ CODE10: 19412" 
* #150488 "MDC_BLD_PERF_INDEX"
* #150488 ^definition = Index | PerMinute | Perfusion | Blood; CVS
* #150488 ^designation[0].language = #de-AT 
* #150488 ^designation[0].value = "Perfusion index" 
* #150488 ^property[0].code = #parent 
* #150488 ^property[0].valueCode = #_mdc 
* #150488 ^property[1].code = #hints 
* #150488 ^property[1].valueString = "PART: 2 ~ CODE10: 19416" 
* #150492 "MDC_OUTPUT_CARD_CTS"
* #150492 ^definition = Volume | PerMinute; Continuous | Blood | LeftVentricle; CVS
* #150492 ^designation[0].language = #de-AT 
* #150492 ^designation[0].value = "Continuous cardiac output" 
* #150492 ^property[0].code = #parent 
* #150492 ^property[0].valueCode = #_mdc 
* #150492 ^property[1].code = #hints 
* #150492 ^property[1].valueString = "PART: 2 ~ CODE10: 19420" 
* #150496 "MDC_OUTPUT_CARD_NONCTS"
* #150496 ^definition = Volume | PerMinute; Discontinuous | Blood | LeftVentricle; CVS
* #150496 ^designation[0].language = #de-AT 
* #150496 ^designation[0].value = "Discontinuous cardiac output" 
* #150496 ^property[0].code = #parent 
* #150496 ^property[0].valueCode = #_mdc 
* #150496 ^property[1].code = #hints 
* #150496 ^property[1].valueString = "PART: 2 ~ CODE10: 19424" 
* #150520 "MDC_TIME_PD_VENT_L_AORT_VALV"
* #150520 ^definition = Duration | SystolicEjection | Blood | AorticValve; LeftVentricle; Heart; CVS
* #150520 ^designation[0].language = #de-AT 
* #150520 ^designation[0].value = "Systolic ejection period" 
* #150520 ^property[0].code = #parent 
* #150520 ^property[0].valueCode = #_mdc 
* #150520 ^property[1].code = #hints 
* #150520 ^property[1].valueString = "PART: 2 ~ CODE10: 19448" 
* #150528 "MDC_VOL_VENT_L_END_DIA"
* #150528 ^definition = Volume | End-diastolic | Blood | LeftVentricle; Heart; CVS
* #150528 ^designation[0].language = #de-AT 
* #150528 ^designation[0].value = "Left ventricular enddiastolic volume" 
* #150528 ^property[0].code = #parent 
* #150528 ^property[0].valueCode = #_mdc 
* #150528 ^property[1].code = #hints 
* #150528 ^property[1].valueString = "PART: 2 ~ CODE10: 19456" 
* #150532 "MDC_VOL_VENT_L_END_SYS"
* #150532 ^definition = Volume | End-systolic | Blood | LeftVentricle; Heart; CVS
* #150532 ^designation[0].language = #de-AT 
* #150532 ^designation[0].value = "Left ventricular end systolic volume" 
* #150532 ^property[0].code = #parent 
* #150532 ^property[0].valueCode = #_mdc 
* #150532 ^property[1].code = #hints 
* #150532 ^property[1].valueString = "PART: 2 ~ CODE10: 19460" 
* #150565 "MDC_GRAD_PRESS_BLD_AORT_POS_MAX"
* #150565 ^definition = Gradient | BloodPressure; MaxPositive | AorticValve | Heart; CVS
* #150565 ^designation[0].language = #de-AT 
* #150565 ^designation[0].value = "Maximum aortic valve pressure gradient" 
* #150565 ^property[0].code = #parent 
* #150565 ^property[0].valueCode = #_mdc 
* #150565 ^property[1].code = #hints 
* #150565 ^property[1].valueString = "PART: 2 ~ CODE10: 19493" 
* #150568 "MDC_TRANSMISSION"
* #150568 ^property[0].code = #parent 
* #150568 ^property[0].valueCode = #_mdc 
* #150568 ^property[1].code = #hints 
* #150568 ^property[1].valueString = "PART: 2 ~ CODE10: 19496" 
* #150572 "MDC_TRANSMISSION_RED"
* #150572 ^property[0].code = #parent 
* #150572 ^property[0].valueCode = #_mdc 
* #150572 ^property[1].code = #hints 
* #150572 ^property[1].valueString = "PART: 2 ~ CODE10: 19500" 
* #150576 "MDC_TRANSMISSION_INFRARED"
* #150576 ^property[0].code = #parent 
* #150576 ^property[0].valueCode = #_mdc 
* #150576 ^property[1].code = #hints 
* #150576 ^property[1].valueString = "PART: 2 ~ CODE10: 19504" 
* #150580 "MDC_MODALITY_FAST"
* #150580 ^definition = ModalityEnumeration | Measurement response time; fast | ArterialBlood | FluidChemistry; Pulse Oximetry
* #150580 ^designation[0].language = #de-AT 
* #150580 ^designation[0].value = "SpO2 fast response" 
* #150580 ^property[0].code = #parent 
* #150580 ^property[0].valueCode = #_mdc 
* #150580 ^property[1].code = #hints 
* #150580 ^property[1].valueString = "PART: 2 ~ CODE10: 19508" 
* #150584 "MDC_MODALITY_SLOW"
* #150584 ^definition = ModalityEnumeration | Measurement response time; slow | ArterialBlood | FluidChemistry; Pulse Oximetry
* #150584 ^designation[0].language = #de-AT 
* #150584 ^designation[0].value = "SpO2 slow response" 
* #150584 ^property[0].code = #parent 
* #150584 ^property[0].valueCode = #_mdc 
* #150584 ^property[1].code = #hints 
* #150584 ^property[1].valueString = "PART: 2 ~ CODE10: 19512" 
* #150588 "MDC_MODALITY_SPOT"
* #150588 ^definition = ModalityEnumeration | Measurement response time; spot check | ArterialBlood | FluidChemistry; Pulse Oximetry
* #150588 ^designation[0].language = #de-AT 
* #150588 ^designation[0].value = "SpO2 spot check" 
* #150588 ^property[0].code = #parent 
* #150588 ^property[0].valueCode = #_mdc 
* #150588 ^property[1].code = #hints 
* #150588 ^property[1].valueString = "PART: 2 ~ CODE10: 19516" 
* #150604 "MDC_PULS_OXIM_DEV_STATUS"
* #150604 ^definition = Status | value | FunctionalStatus | Device
* #150604 ^designation[0].language = #de-AT 
* #150604 ^designation[0].value = "Pulse Oximeter Device Status" 
* #150604 ^property[0].code = #parent 
* #150604 ^property[0].valueCode = #_mdc 
* #150604 ^property[1].code = #hints 
* #150604 ^property[1].valueString = "PART: 2 ~ CODE10: 19532" 
* #150605 "MDC_PULS_OXIM_PULS_CHAR"
* #150605 ^definition = PatternEvent | Rhythm | Artifact | CVS
* #150605 ^designation[0].language = #de-AT 
* #150605 ^designation[0].value = "Pulse characteristic status" 
* #150605 ^property[0].code = #parent 
* #150605 ^property[0].valueCode = #_mdc 
* #150605 ^property[1].code = #hints 
* #150605 ^property[1].valueString = "PART: 2 ~ CODE10: 19533" 
* #150612 "MDC_ACCELERATION_INDEX"
* #150612 ^definition = Acceleration; initial | BloodFlow | Flow | Aorta; CVS
* #150612 ^designation[0].language = #de-AT 
* #150612 ^designation[0].value = "Acceleration Index" 
* #150612 ^property[0].code = #parent 
* #150612 ^property[0].valueCode = #_mdc 
* #150612 ^property[1].code = #hints 
* #150612 ^property[1].valueString = "PART: 2 ~ CODE10: 19540" 
* #150616 "MDC_SYSTOLIC_TIME_RATIO"
* #150616 ^definition = Ratio; Duration | electrical pre-ejection; mechanical systole | Flow | Aorta; CVS
* #150616 ^designation[0].language = #de-AT 
* #150616 ^designation[0].value = "Systolic Time Ratio" 
* #150616 ^property[0].code = #parent 
* #150616 ^property[0].valueCode = #_mdc 
* #150616 ^property[1].code = #hints 
* #150616 ^property[1].valueString = "PART: 2 ~ CODE10: 19544" 
* #150620 "MDC_THORACIC_FLUID_CONTENT"
* #150620 ^definition = Fluid Content; Thoracic | | Conductivity | ChestCavity; CVS
* #150620 ^designation[0].language = #de-AT 
* #150620 ^designation[0].value = "Thoracic Fluid Content" 
* #150620 ^property[0].code = #parent 
* #150620 ^property[0].valueCode = #_mdc 
* #150620 ^property[1].code = #hints 
* #150620 ^property[1].valueString = "PART: 2 ~ CODE10: 19548" 
* #150624 "MDC_TIME_PD_VENT_L_AORT_EJCT"
* #150624 ^definition = Duration | AorticValve; Opening to Closing | Flow | Aorta; CVS
* #150624 ^designation[0].language = #de-AT 
* #150624 ^designation[0].value = "Left Ventricular Ejection Time" 
* #150624 ^property[0].code = #parent 
* #150624 ^property[0].valueCode = #_mdc 
* #150624 ^property[1].code = #hints 
* #150624 ^property[1].valueString = "PART: 2 ~ CODE10: 19552" 
* #150628 "MDC_TIME_PD_VENT_L_AORT_PRE_EJCT"
* #150628 ^definition = Duration | Pre-Ejection | Flow | Aorta; CVS
* #150628 ^designation[0].language = #de-AT 
* #150628 ^designation[0].value = "Pre-Ejection Period" 
* #150628 ^property[0].code = #parent 
* #150628 ^property[0].valueCode = #_mdc 
* #150628 ^property[1].code = #hints 
* #150628 ^property[1].valueString = "PART: 2 ~ CODE10: 19556" 
* #150632 "MDC_VELOCITY_INDEX"
* #150632 ^definition = Index; Velocity | BloodFlow; Peak | Flow | Aorta; CVS
* #150632 ^designation[0].language = #de-AT 
* #150632 ^designation[0].value = "Velocity Index" 
* #150632 ^property[0].code = #parent 
* #150632 ^property[0].valueCode = #_mdc 
* #150632 ^property[1].code = #hints 
* #150632 ^property[1].valueString = "PART: 2 ~ CODE10: 19560" 
* #150636 "MDC_VOL_BLD_STROKE_INDEX"
* #150636 ^definition = Index | PerSurfaceArea | StrokeVolume | LeftVentricle; CVS
* #150636 ^designation[0].language = #de-AT 
* #150636 ^designation[0].value = "Stroke Volume Index" 
* #150636 ^property[0].code = #parent 
* #150636 ^property[0].valueCode = #_mdc 
* #150636 ^property[1].code = #hints 
* #150636 ^property[1].valueString = "PART: 2 ~ CODE10: 19564" 
* #150640 "MDC_WK_LV_WORK_INDEX"
* #150640 ^definition = Index | | Work; PerMinute; PerSurfaceArea | LeftVentricle; CVS
* #150640 ^designation[0].language = #de-AT 
* #150640 ^designation[0].value = "(per minute)" 
* #150640 ^property[0].code = #parent 
* #150640 ^property[0].valueCode = #_mdc 
* #150640 ^property[1].code = #hints 
* #150640 ^property[1].valueString = "PART: 2 ~ CODE10: 19568" 
* #150644 "MDC_WK_RV_STROKE_INDEX"
* #150644 ^definition = Index | | Work; PerStroke; PerSurfaceArea | RightVentricle; CVS
* #150644 ^designation[0].language = #de-AT 
* #150644 ^designation[0].value = "Right Ventricular Stroke Work Index" 
* #150644 ^property[0].code = #parent 
* #150644 ^property[0].valueCode = #_mdc 
* #150644 ^property[1].code = #hints 
* #150644 ^property[1].valueString = "PART: 2 ~ CODE10: 19572" 
* #150648 "MDC_PRESS_BLD_ART_FEMORAL"
* #150648 ^definition = Pressure | | Blood | FemoralArtery; CVS
* #150648 ^designation[0].language = #de-AT 
* #150648 ^designation[0].value = "Femoral Artery Pressure" 
* #150648 ^property[0].code = #parent 
* #150648 ^property[0].valueCode = #_mdc 
* #150648 ^property[1].code = #hints 
* #150648 ^property[1].valueString = "PART: 2 ~ CODE10: 19576" 
* #150649 "MDC_PRESS_BLD_ART_FEMORAL_SYS"
* #150649 ^definition = Pressure | Systolic | Blood | FemoralArtery; CVS
* #150649 ^designation[0].language = #de-AT 
* #150649 ^designation[0].value = "Systolic Femoral Arterial Catheter Pressure" 
* #150649 ^property[0].code = #parent 
* #150649 ^property[0].valueCode = #_mdc 
* #150649 ^property[1].code = #hints 
* #150649 ^property[1].valueString = "PART: 2 ~ CODE10: 19577" 
* #150650 "MDC_PRESS_BLD_ART_FEMORAL_DIA"
* #150650 ^definition = Pressure | Diastolic | Blood | FemoralArtery; CVS
* #150650 ^designation[0].language = #de-AT 
* #150650 ^designation[0].value = "Diastolic Femoral Arterial Catheter Pressure" 
* #150650 ^property[0].code = #parent 
* #150650 ^property[0].valueCode = #_mdc 
* #150650 ^property[1].code = #hints 
* #150650 ^property[1].valueString = "PART: 2 ~ CODE10: 19578" 
* #150651 "MDC_PRESS_BLD_ART_FEMORAL_MEAN"
* #150651 ^definition = Pressure | Mean | Blood | FemoralArtery; CVS
* #150651 ^designation[0].language = #de-AT 
* #150651 ^designation[0].value = "Mean Femoral Arterial Catheter Pressure" 
* #150651 ^property[0].code = #parent 
* #150651 ^property[0].valueCode = #_mdc 
* #150651 ^property[1].code = #hints 
* #150651 ^property[1].valueString = "PART: 2 ~ CODE10: 19579" 
* #150652 "MDC_FLOW_BLD_PULM_CAP"
* #150652 ^definition = Flow | | Blood; PulmonaryCapillary | CVS
* #150652 ^designation[0].language = #de-AT 
* #150652 ^designation[0].value = "Pulmonary Capillary Blood Flow" 
* #150652 ^property[0].code = #parent 
* #150652 ^property[0].valueCode = #_mdc 
* #150652 ^property[1].code = #hints 
* #150652 ^property[1].valueString = "PART: 2 ~ CODE10: 19580" 
* #150656 "MDC_O2_OXYGENATION_RATIO"
* #150656 ^definition = Ratio; Concentration | arterial oxygen; inspired oxygen (PaO2/FiO2) | ArterialOxygen | Blood; CVS
* #150656 ^designation[0].language = #de-AT 
* #150656 ^designation[0].value = "Oxygenation Ratio" 
* #150656 ^property[0].code = #parent 
* #150656 ^property[0].valueCode = #_mdc 
* #150656 ^property[1].code = #hints 
* #150656 ^property[1].valueString = "PART: 2 ~ CODE10: 19584" 
* #150660 "MDC_OUTPUT_CARDIAC_FICK"
* #150660 ^definition = Volume | PerMinute | Blood | Left Ventricle; CVS; by Fick method
* #150660 ^designation[0].language = #de-AT 
* #150660 ^designation[0].value = "Cardiac Output" 
* #150660 ^property[0].code = #parent 
* #150660 ^property[0].valueCode = #_mdc 
* #150660 ^property[1].code = #hints 
* #150660 ^property[1].valueString = "PART: 2 ~ CODE10: 19588" 
* #150664 "MDC_SAT_O2_CONSUMP_INDEX"
* #150664 ^definition = Volume | Per Minute Per Body Surface Area | ConsumedOxygen | Blood; CVS
* #150664 ^designation[0].language = #de-AT 
* #150664 ^designation[0].value = "(Estimated) O2 Consumption Index" 
* #150664 ^property[0].code = #parent 
* #150664 ^property[0].valueCode = #_mdc 
* #150664 ^property[1].code = #hints 
* #150664 ^property[1].valueString = "PART: 2 ~ CODE10: 19592" 
* #150668 "MDC_SAT_O2_DELIV_INDEX"
* #150668 ^definition = Volume | Per Minute Per Body Surface Area | DeliveredOxygen | Blood; CVS
* #150668 ^designation[0].language = #de-AT 
* #150668 ^designation[0].value = "(Estimated) O2 Delivery Index" 
* #150668 ^property[0].code = #parent 
* #150668 ^property[0].valueCode = #_mdc 
* #150668 ^property[1].code = #hints 
* #150668 ^property[1].valueString = "PART: 2 ~ CODE10: 19596" 
* #150672 "MDC_SPO2_OXYGENATION_RATIO"
* #150672 ^definition = Ratio; Concentration | arterial oxygen; inspired oxygen (PaO2/FiO2) | ArterialOxygen | Blood; CVS
* #150672 ^designation[0].language = #de-AT 
* #150672 ^designation[0].value = "SPO2 Oxygenation Ratio" 
* #150672 ^property[0].code = #parent 
* #150672 ^property[0].valueCode = #_mdc 
* #150672 ^property[1].code = #hints 
* #150672 ^property[1].valueString = "PART: 2 ~ CODE10: 19600" 
* #150676 "MDC_PRESS_BLD_VEN_FEMORAL"
* #150676 ^definition = Pressure | | Blood | FemoralVein; CVS
* #150676 ^designation[0].language = #de-AT 
* #150676 ^designation[0].value = "Femoral Venous Pressure" 
* #150676 ^property[0].code = #parent 
* #150676 ^property[0].valueCode = #_mdc 
* #150676 ^property[1].code = #hints 
* #150676 ^property[1].valueString = "PART: 2 ~ CODE10: 19604" 
* #150680 "MDC_PRESS_BLD_ART_BRACHIAL"
* #150680 ^definition = Pressure | | Blood | BrachialArtery; CVS
* #150680 ^designation[0].language = #de-AT 
* #150680 ^designation[0].value = "Brachial Arterial Pressure" 
* #150680 ^property[0].code = #parent 
* #150680 ^property[0].valueCode = #_mdc 
* #150680 ^property[1].code = #hints 
* #150680 ^property[1].valueString = "PART: 2 ~ CODE10: 19608" 
* #151562 "MDC_RESP_RATE"
* #151562 ^definition = Rate | NOS | Breath | Breathing
* #151562 ^designation[0].language = #de-AT 
* #151562 ^designation[0].value = "Respiration rate" 
* #151562 ^property[0].code = #parent 
* #151562 ^property[0].valueCode = #_mdc 
* #151562 ^property[1].code = #hints 
* #151562 ^property[1].valueString = "PART: 2 ~ CODE10: 20490" 
* #151570 "MDC_AWAY_RESP_RATE"
* #151570 ^definition = Rate | Airway | Breath | Breathing
* #151570 ^designation[0].language = #de-AT 
* #151570 ^designation[0].value = "Respiration rate" 
* #151570 ^property[0].code = #parent 
* #151570 ^property[0].valueCode = #_mdc 
* #151570 ^property[1].code = #hints 
* #151570 ^property[1].valueString = "PART: 2 ~ CODE10: 20498" 
* #151578 "MDC_TTHOR_RESP_RATE"
* #151578 ^definition = Rate | Transthoracic | Breath | Breathing
* #151578 ^designation[0].language = #de-AT 
* #151578 ^designation[0].value = "Respiration rate" 
* #151578 ^property[0].code = #parent 
* #151578 ^property[0].valueCode = #_mdc 
* #151578 ^property[1].code = #hints 
* #151578 ^property[1].valueString = "PART: 2 ~ CODE10: 20506" 
* #151586 "MDC_VENT_RESP_RATE"
* #151586 ^definition = Rate | NOS | Breath | Ventilator
* #151586 ^designation[0].language = #de-AT 
* #151586 ^designation[0].value = "Ventilation rate" 
* #151586 ^property[0].code = #parent 
* #151586 ^property[0].valueCode = #_mdc 
* #151586 ^property[1].code = #hints 
* #151586 ^property[1].valueString = "PART: 2 ~ CODE10: 20514" 
* #151594 "MDC_CO2_RESP_RATE"
* #151594 ^definition = Rate | CO2 | Breath | Breathing
* #151594 ^designation[0].language = #de-AT 
* #151594 ^designation[0].value = "Respiration rate" 
* #151594 ^property[0].code = #parent 
* #151594 ^property[0].valueCode = #_mdc 
* #151594 ^property[1].code = #hints 
* #151594 ^property[1].valueString = "PART: 2 ~ CODE10: 20522" 
* #151602 "MDC_PRESS_RESP_RATE"
* #151602 ^definition = Rate | Pressure | Breath | Breathing
* #151602 ^designation[0].language = #de-AT 
* #151602 ^designation[0].value = "Respiration rate" 
* #151602 ^property[0].code = #parent 
* #151602 ^property[0].valueCode = #_mdc 
* #151602 ^property[1].code = #hints 
* #151602 ^property[1].valueString = "PART: 2 ~ CODE10: 20530" 
* #151610 "MDC_VENT_CO2_RESP_RATE"
* #151610 ^definition = Rate | CO2 | Breath | Ventilator
* #151610 ^designation[0].language = #de-AT 
* #151610 ^designation[0].value = "Ventilation rate" 
* #151610 ^property[0].code = #parent 
* #151610 ^property[0].valueCode = #_mdc 
* #151610 ^property[1].code = #hints 
* #151610 ^property[1].valueString = "PART: 2 ~ CODE10: 20538" 
* #151618 "MDC_VENT_PRESS_RESP_RATE"
* #151618 ^definition = Rate | Pressure | Breath | Ventilator
* #151618 ^designation[0].language = #de-AT 
* #151618 ^designation[0].value = "Ventilation rate" 
* #151618 ^property[0].code = #parent 
* #151618 ^property[0].valueCode = #_mdc 
* #151618 ^property[1].code = #hints 
* #151618 ^property[1].valueString = "PART: 2 ~ CODE10: 20546" 
* #151626 "MDC_VENT_FLOW_RESP_RATE"
* #151626 ^definition = Rate | Volume/Flow | Breath | Ventilator
* #151626 ^designation[0].language = #de-AT 
* #151626 ^designation[0].value = "Ventilation rate" 
* #151626 ^property[0].code = #parent 
* #151626 ^property[0].valueCode = #_mdc 
* #151626 ^property[1].code = #hints 
* #151626 ^property[1].valueString = "PART: 2 ~ CODE10: 20554" 
* #151634 "MDC_VENT_SIGH_RATE"
* #151634 ^definition = Rate | | Sigh | Ventilator
* #151634 ^designation[0].language = #de-AT 
* #151634 ^designation[0].value = "Ventilation sigh number" 
* #151634 ^property[0].code = #parent 
* #151634 ^property[0].valueCode = #_mdc 
* #151634 ^property[1].code = #hints 
* #151634 ^property[1].valueString = "PART: 2 ~ CODE10: 20562" 
* #151642 "MDC_VENT_SIGH_MULT_RATE"
* #151642 ^definition = Rate | | SighMultiple | Ventilator
* #151642 ^designation[0].language = #de-AT 
* #151642 ^designation[0].value = "Ventilation multiple sigh number" 
* #151642 ^property[0].code = #parent 
* #151642 ^property[0].valueCode = #_mdc 
* #151642 ^property[1].code = #hints 
* #151642 ^property[1].valueString = "PART: 2 ~ CODE10: 20570" 
* #151650 "MDC_ACOUSTIC_RESP_RATE"
* #151650 ^definition = Rate | Acoustic | Breath | Breathing
* #151650 ^designation[0].language = #de-AT 
* #151650 ^designation[0].value = "Acoustic Respiration Rate" 
* #151650 ^property[0].code = #parent 
* #151650 ^property[0].valueCode = #_mdc 
* #151650 ^property[1].code = #hints 
* #151650 ^property[1].valueString = "PART: 2 ~ CODE10: 20578" 
* #151658 "MDC_PULS_OXIM_PLETH_RESP_RATE"
* #151658 ^definition = Rate | Plethysmographic | Breath | Breathing
* #151658 ^designation[0].language = #de-AT 
* #151658 ^designation[0].value = "Plethysmographic Respiration Rate" 
* #151658 ^property[0].code = #parent 
* #151658 ^property[0].valueCode = #_mdc 
* #151658 ^property[1].code = #hints 
* #151658 ^property[1].valueString = "PART: 2 ~ CODE10: 20586" 
* #151666 "MDC_RESP_SPONT_RATE"
* #151666 ^definition = Rate | | Breath | Patient; Spontaneous
* #151666 ^designation[0].language = #de-AT 
* #151666 ^designation[0].value = "Spontaneous respiration rate (legacy devices and systems)" 
* #151666 ^property[0].code = #parent 
* #151666 ^property[0].valueCode = #_mdc 
* #151666 ^property[1].code = #hints 
* #151666 ^property[1].valueString = "PART: 2 ~ CODE10: 20594" 
* #151674 "MDC_RESP_BTSD_PS_RATE"
* #151674 ^definition = Rate | | Breath | Patient; Spontaneous
* #151674 ^designation[0].language = #de-AT 
* #151674 ^designation[0].value = "Spontaneous respiration rate (preferred)" 
* #151674 ^property[0].code = #parent 
* #151674 ^property[0].valueCode = #_mdc 
* #151674 ^property[1].code = #hints 
* #151674 ^property[1].valueString = "PART: 2 ~ CODE10: 20602" 
* #151680 "MDC_CAPAC_VITAL"
* #151680 ^definition = Volume | | Lung; VitalCapacity | LungStructure
* #151680 ^designation[0].language = #de-AT 
* #151680 ^designation[0].value = "Vital capacity" 
* #151680 ^property[0].code = #parent 
* #151680 ^property[0].valueCode = #_mdc 
* #151680 ^property[1].code = #hints 
* #151680 ^property[1].valueString = "PART: 2 ~ CODE10: 20608" 
* #151684 "MDC_VENT_TIME_PD_EXP_PAUSE"
* #151684 ^definition = Duration | Expiratory pause | end expiratory flow to start inspiratory flow | Ventilator
* #151684 ^property[0].code = #parent 
* #151684 ^property[0].valueCode = #_mdc 
* #151684 ^property[1].code = #hints 
* #151684 ^property[1].valueString = "PART: 2 ~ CODE10: 20612" 
* #151688 "MDC_COMPL_LUNG"
* #151688 ^definition = Compliance | | Alveoli | LungStructure
* #151688 ^designation[0].language = #de-AT 
* #151688 ^designation[0].value = "Compliance of respiratory system" 
* #151688 ^property[0].code = #parent 
* #151688 ^property[0].valueCode = #_mdc 
* #151688 ^property[1].code = #hints 
* #151688 ^property[1].valueString = "PART: 2 ~ CODE10: 20616" 
* #151692 "MDC_COMPL_LUNG_DYN"
* #151692 ^definition = Compliance | Dynamic | Alveoli; Pleura | LungStructure
* #151692 ^designation[0].language = #de-AT 
* #151692 ^designation[0].value = "Thoracic compliance" 
* #151692 ^property[0].code = #parent 
* #151692 ^property[0].valueCode = #_mdc 
* #151692 ^property[1].code = #hints 
* #151692 ^property[1].valueString = "PART: 2 ~ CODE10: 20620" 
* #151696 "MDC_COMPL_LUNG_STATIC"
* #151696 ^definition = Compliance | Static | Alveoli; Pleura | LungStructure
* #151696 ^designation[0].language = #de-AT 
* #151696 ^designation[0].value = "Lung compliance; static" 
* #151696 ^property[0].code = #parent 
* #151696 ^property[0].valueCode = #_mdc 
* #151696 ^property[1].code = #hints 
* #151696 ^property[1].valueString = "PART: 2 ~ CODE10: 20624" 
* #151700 "MDC_CONC_AWAY_CO2"
* #151700 ^definition = Concentration | PartialPressure | CO2; Gas | Airway
* #151700 ^designation[0].language = #de-AT 
* #151700 ^designation[0].value = "Concentration (or partial pressure) of carbon dioxide in airway gas" 
* #151700 ^property[0].code = #parent 
* #151700 ^property[0].valueCode = #_mdc 
* #151700 ^property[1].code = #hints 
* #151700 ^property[1].valueString = "PART: 2 ~ CODE10: 20628" 
* #151708 "MDC_CONC_AWAY_CO2_ET"
* #151708 ^definition = Concentration | PartialPressure; Expiration | CO2; Gas | Airway
* #151708 ^designation[0].language = #de-AT 
* #151708 ^designation[0].value = "End tidal carbon dioxide concentration (or partial pressure) in airway gas" 
* #151708 ^property[0].code = #parent 
* #151708 ^property[0].valueCode = #_mdc 
* #151708 ^property[1].code = #hints 
* #151708 ^property[1].valueString = "PART: 2 ~ CODE10: 20636" 
* #151712 "MDC_CONC_AWAY_CO2_EXP"
* #151712 ^definition = Concentration | PartialPressure; Expiration | CO2; Gas | Airway
* #151712 ^designation[0].language = #de-AT 
* #151712 ^designation[0].value = "Expired carbon dioxide concentration (or partial pressure) in airway gas" 
* #151712 ^property[0].code = #parent 
* #151712 ^property[0].valueCode = #_mdc 
* #151712 ^property[1].code = #hints 
* #151712 ^property[1].valueString = "PART: 2 ~ CODE10: 20640" 
* #151716 "MDC_CONC_AWAY_CO2_INSP"
* #151716 ^definition = Concentration | PartialPressure; Inspiration | CO2; Gas | Airway
* #151716 ^designation[0].language = #de-AT 
* #151716 ^designation[0].value = "Inspiratory carbon dioxide concentration (or partial pressure) in airway gas" 
* #151716 ^property[0].code = #parent 
* #151716 ^property[0].valueCode = #_mdc 
* #151716 ^property[1].code = #hints 
* #151716 ^property[1].valueString = "PART: 2 ~ CODE10: 20644" 
* #151718 "MDC_CONC_AWAY_CO2_INSP_MIN"
* #151718 ^property[0].code = #parent 
* #151718 ^property[0].valueCode = #_mdc 
* #151718 ^property[1].code = #hints 
* #151718 ^property[1].valueString = "PART: 2 ~ CODE10: 20646" 
* #151744 "MDC_CONC_AWAY_O2_DELTA"
* #151744 ^definition = Concentration | Difference (PartialPressureInspiration; PartialPressureExpiration) | O2; Gas | Airway
* #151744 ^designation[0].language = #de-AT 
* #151744 ^designation[0].value = "O2 pressure difference" 
* #151744 ^property[0].code = #parent 
* #151744 ^property[0].valueCode = #_mdc 
* #151744 ^property[1].code = #hints 
* #151744 ^property[1].valueString = "PART: 2 ~ CODE10: 20672" 
* #151756 "MDC_CO2_TCUT"
* #151756 ^property[0].code = #parent 
* #151756 ^property[0].valueCode = #_mdc 
* #151756 ^property[1].code = #hints 
* #151756 ^property[1].valueString = "PART: 2 ~ CODE10: 20684" 
* #151760 "MDC_O2_TCUT"
* #151760 ^property[0].code = #parent 
* #151760 ^property[0].valueCode = #_mdc 
* #151760 ^property[1].code = #hints 
* #151760 ^property[1].valueString = "PART: 2 ~ CODE10: 20688" 
* #151764 "MDC_FLOW_AWAY"
* #151764 ^definition = Flow | | Gas | Breathing
* #151764 ^designation[0].language = #de-AT 
* #151764 ^designation[0].value = "Airway flow" 
* #151764 ^property[0].code = #parent 
* #151764 ^property[0].valueCode = #_mdc 
* #151764 ^property[1].code = #hints 
* #151764 ^property[1].valueString = "PART: 2 ~ CODE10: 20692" 
* #151768 "MDC_FLOW_AWAY_EXP"
* #151768 ^definition = Flow | Expiration | Gas
* #151768 ^designation[0].language = #de-AT 
* #151768 ^designation[0].value = "Expiratory airway flow" 
* #151768 ^property[0].code = #parent 
* #151768 ^property[0].valueCode = #_mdc 
* #151768 ^property[1].code = #hints 
* #151768 ^property[1].valueString = "PART: 2 ~ CODE10: 20696" 
* #151769 "MDC_FLOW_AWAY_EXP_MAX"
* #151769 ^definition = Flow | Expiration; Maximum | Gas
* #151769 ^designation[0].language = #de-AT 
* #151769 ^designation[0].value = "Expiratory maximum airway flow" 
* #151769 ^property[0].code = #parent 
* #151769 ^property[0].valueCode = #_mdc 
* #151769 ^property[1].code = #hints 
* #151769 ^property[1].valueString = "PART: 2 ~ CODE10: 20697" 
* #151772 "MDC_FLOW_AWAY_INSP"
* #151772 ^definition = Flow | Gas | Airway | Inspiratory
* #151772 ^designation[0].language = #de-AT 
* #151772 ^designation[0].value = "Inspiratory airway flow" 
* #151772 ^property[0].code = #parent 
* #151772 ^property[0].valueCode = #_mdc 
* #151772 ^property[1].code = #hints 
* #151772 ^property[1].valueString = "PART: 2 ~ CODE10: 20700" 
* #151773 "MDC_FLOW_AWAY_INSP_MAX"
* #151773 ^definition = Flow | Inspiratory; Maximum | Gas
* #151773 ^designation[0].language = #de-AT 
* #151773 ^designation[0].value = "Inspiratory maximum airway flow" 
* #151773 ^property[0].code = #parent 
* #151773 ^property[0].valueCode = #_mdc 
* #151773 ^property[1].code = #hints 
* #151773 ^property[1].valueString = "PART: 2 ~ CODE10: 20701" 
* #151776 "MDC_FLOW_CO2_PROD_RESP"
* #151776 ^definition = Flow | Production | CO2; Gas | Breathing
* #151776 ^designation[0].language = #de-AT 
* #151776 ^designation[0].value = "CO2 Production" 
* #151776 ^property[0].code = #parent 
* #151776 ^property[0].valueCode = #_mdc 
* #151776 ^property[1].code = #hints 
* #151776 ^property[1].valueString = "PART: 2 ~ CODE10: 20704" 
* #151780 "MDC_IMPED_TTHOR"
* #151780 ^definition = ElectricalImpedance | Transthoracic | Respiration | Breathing
* #151780 ^designation[0].language = #de-AT 
* #151780 ^designation[0].value = "Transthoracic Impedance" 
* #151780 ^property[0].code = #parent 
* #151780 ^property[0].valueCode = #_mdc 
* #151780 ^property[1].code = #hints 
* #151780 ^property[1].valueString = "PART: 2 ~ CODE10: 20708" 
* #151784 "MDC_PRESS_RESP_PLAT"
* #151784 ^definition = Pressure | Plateau | Gas | Airway
* #151784 ^designation[0].language = #de-AT 
* #151784 ^designation[0].value = "Plateau pressure" 
* #151784 ^property[0].code = #parent 
* #151784 ^property[0].valueCode = #_mdc 
* #151784 ^property[1].code = #hints 
* #151784 ^property[1].valueString = "PART: 2 ~ CODE10: 20712" 
* #151788 "MDC_PRESS_RESP_PAUSE"
* #151788 ^definition = Pressure | Pause | Gas | Airway
* #151788 ^designation[0].language = #de-AT 
* #151788 ^designation[0].value = "Respiratory Pause pressure" 
* #151788 ^property[0].code = #parent 
* #151788 ^property[0].valueCode = #_mdc 
* #151788 ^property[1].code = #hints 
* #151788 ^property[1].valueString = "PART: 2 ~ CODE10: 20716" 
* #151792 "MDC_PRESS_AWAY"
* #151792 ^definition = Pressure | | Gas | Airway
* #151792 ^designation[0].language = #de-AT 
* #151792 ^designation[0].value = "Airway pressure" 
* #151792 ^property[0].code = #parent 
* #151792 ^property[0].valueCode = #_mdc 
* #151792 ^property[1].code = #hints 
* #151792 ^property[1].valueString = "PART: 2 ~ CODE10: 20720" 
* #151793 "MDC_PRESS_AWAY_MAX"
* #151793 ^definition = Pressure | Maximum | Gas | Airway
* #151793 ^designation[0].language = #de-AT 
* #151793 ^designation[0].value = "Maximum airway pressure" 
* #151793 ^property[0].code = #parent 
* #151793 ^property[0].valueCode = #_mdc 
* #151793 ^property[1].code = #hints 
* #151793 ^property[1].valueString = "PART: 2 ~ CODE10: 20721" 
* #151794 "MDC_PRESS_AWAY_MIN"
* #151794 ^property[0].code = #parent 
* #151794 ^property[0].valueCode = #_mdc 
* #151794 ^property[1].code = #hints 
* #151794 ^property[1].valueString = "PART: 2 ~ CODE10: 20722" 
* #151795 "MDC_PRESS_AWAY_MEAN"
* #151795 ^definition = Pressure | Mean | Gas | Airway
* #151795 ^designation[0].language = #de-AT 
* #151795 ^designation[0].value = "Mean airway pressure" 
* #151795 ^property[0].code = #parent 
* #151795 ^property[0].valueCode = #_mdc 
* #151795 ^property[1].code = #hints 
* #151795 ^property[1].valueString = "PART: 2 ~ CODE10: 20723" 
* #151796 "MDC_PRESS_AWAY_CTS_POS"
* #151796 ^definition = Pressure | Continuous; Positive | Gas | Airway
* #151796 ^designation[0].language = #de-AT 
* #151796 ^designation[0].value = "CPAP pressure" 
* #151796 ^property[0].code = #parent 
* #151796 ^property[0].valueCode = #_mdc 
* #151796 ^property[1].code = #hints 
* #151796 ^property[1].valueString = "PART: 2 ~ CODE10: 20724" 
* #151801 "MDC_PRESS_AWAY_NEG_MAX"
* #151801 ^property[0].code = #parent 
* #151801 ^property[0].valueCode = #_mdc 
* #151801 ^property[1].code = #hints 
* #151801 ^property[1].valueString = "PART: 2 ~ CODE10: 20729" 
* #151804 "MDC_PRESS_AWAY_END_EXP_POS"
* #151804 ^definition = Pressure | End-expiratory; Extrinsic | Gas | Ventilator
* #151804 ^designation[0].language = #de-AT 
* #151804 ^designation[0].value = "Extrinsic PEEP (positive end expiratory pressure); applied" 
* #151804 ^property[0].code = #parent 
* #151804 ^property[0].valueCode = #_mdc 
* #151804 ^property[1].code = #hints 
* #151804 ^property[1].valueString = "PART: 2 ~ CODE10: 20732" 
* #151808 "MDC_PRESS_AWAY_END_EXP_POS_INTRINSIC"
* #151808 ^definition = Pressure | End-expiratory; Intrinsic | Gas | Airway
* #151808 ^designation[0].language = #de-AT 
* #151808 ^designation[0].value = "Intrinsic PEEP (aka Auto PEEP)" 
* #151808 ^property[0].code = #parent 
* #151808 ^property[0].valueCode = #_mdc 
* #151808 ^property[1].code = #hints 
* #151808 ^property[1].valueString = "PART: 2 ~ CODE10: 20736" 
* #151812 "MDC_PRESS_AWAY_EXP"
* #151812 ^definition = Pressure | Expiration | Gas | Airway
* #151812 ^designation[0].language = #de-AT 
* #151812 ^designation[0].value = "Expiratory airway pressure" 
* #151812 ^property[0].code = #parent 
* #151812 ^property[0].valueCode = #_mdc 
* #151812 ^property[1].code = #hints 
* #151812 ^property[1].valueString = "PART: 2 ~ CODE10: 20740" 
* #151813 "MDC_PRESS_AWAY_EXP_MAX"
* #151813 ^definition = Pressure | Expiration; Maximum | Gas | Airway
* #151813 ^designation[0].language = #de-AT 
* #151813 ^designation[0].value = "Maximum expiratory airway pressure" 
* #151813 ^property[0].code = #parent 
* #151813 ^property[0].valueCode = #_mdc 
* #151813 ^property[1].code = #hints 
* #151813 ^property[1].valueString = "PART: 2 ~ CODE10: 20741" 
* #151814 "MDC_PRESS_AWAY_EXP_MIN"
* #151814 ^definition = Pressure | Expiration; Minimum | Gas | Airway
* #151814 ^designation[0].language = #de-AT 
* #151814 ^designation[0].value = "Minimum expiratory airway pressure" 
* #151814 ^property[0].code = #parent 
* #151814 ^property[0].valueCode = #_mdc 
* #151814 ^property[1].code = #hints 
* #151814 ^property[1].valueString = "PART: 2 ~ CODE10: 20742" 
* #151816 "MDC_PRESS_AWAY_INSP"
* #151816 ^definition = Pressure | Inspiration | Gas | Airway
* #151816 ^designation[0].language = #de-AT 
* #151816 ^designation[0].value = "Inspiratory airway pressure" 
* #151816 ^property[0].code = #parent 
* #151816 ^property[0].valueCode = #_mdc 
* #151816 ^property[1].code = #hints 
* #151816 ^property[1].valueString = "PART: 2 ~ CODE10: 20744" 
* #151817 "MDC_PRESS_AWAY_INSP_MAX"
* #151817 ^definition = Pressure | Inspiration; Maximum | Gas | Airway
* #151817 ^designation[0].language = #de-AT 
* #151817 ^designation[0].value = "Maximum inspiratory airway pressure (peak inspiratory pressure)" 
* #151817 ^property[0].code = #parent 
* #151817 ^property[0].valueCode = #_mdc 
* #151817 ^property[1].code = #hints 
* #151817 ^property[1].valueString = "PART: 2 ~ CODE10: 20745" 
* #151818 "MDC_PRESS_AWAY_INSP_MIN"
* #151818 ^definition = Pressure | Inspiration; Minimum | Gas | Airway
* #151818 ^designation[0].language = #de-AT 
* #151818 ^designation[0].value = "Minimum inspiratory airway pressure" 
* #151818 ^property[0].code = #parent 
* #151818 ^property[0].valueCode = #_mdc 
* #151818 ^property[1].code = #hints 
* #151818 ^property[1].valueString = "PART: 2 ~ CODE10: 20746" 
* #151819 "MDC_PRESS_AWAY_INSP_MEAN"
* #151819 ^definition = Pressure | Inspiration; Mean | Gas | Airway
* #151819 ^designation[0].language = #de-AT 
* #151819 ^designation[0].value = "Mean inspiratory airway pressure" 
* #151819 ^property[0].code = #parent 
* #151819 ^property[0].valueCode = #_mdc 
* #151819 ^property[1].code = #hints 
* #151819 ^property[1].valueString = "PART: 2 ~ CODE10: 20747" 
* #151820 "MDC_PRESS_ESOPH"
* #151820 ^definition = Pressure | Esophageal | Respiration | Breathing
* #151820 ^designation[0].language = #de-AT 
* #151820 ^designation[0].value = "Esophageal pressure" 
* #151820 ^property[0].code = #parent 
* #151820 ^property[0].valueCode = #_mdc 
* #151820 ^property[1].code = #hints 
* #151820 ^property[1].valueString = "PART: 2 ~ CODE10: 20748" 
* #151824 "MDC_PRESS_INTRAPL"
* #151824 ^definition = Pressure | Intrapleural | Respiration | Breathing
* #151824 ^designation[0].language = #de-AT 
* #151824 ^designation[0].value = "Intrapleural Respiratory Pressure" 
* #151824 ^property[0].code = #parent 
* #151824 ^property[0].valueCode = #_mdc 
* #151824 ^property[1].code = #hints 
* #151824 ^property[1].valueString = "PART: 2 ~ CODE10: 20752" 
* #151828 "MDC_QUO_RESP"
* #151828 ^definition = Ratio | Flow(ExpiredCO2); Flow(O2used) | Gas | RespiratoryProcess
* #151828 ^designation[0].language = #de-AT 
* #151828 ^designation[0].value = "Respiratory quotient" 
* #151828 ^property[0].code = #parent 
* #151828 ^property[0].valueCode = #_mdc 
* #151828 ^property[1].code = #hints 
* #151828 ^property[1].valueString = "PART: 2 ~ CODE10: 20756" 
* #151832 "MDC_RATIO_IE"
* #151832 ^definition = Ratio | Duration(InspiratoryPhase); Duration(ExpiratoryPhase) | Gas | Breathing
* #151832 ^designation[0].language = #de-AT 
* #151832 ^designation[0].value = "Ratio inspiration expiration time" 
* #151832 ^property[0].code = #parent 
* #151832 ^property[0].valueCode = #_mdc 
* #151832 ^property[1].code = #hints 
* #151832 ^property[1].valueString = "PART: 2 ~ CODE10: 20760" 
* #151836 "MDC_RATIO_AWAY_DEADSP_TIDAL"
* #151836 ^definition = Ratio | DeadspaceVolume; TidalVolume | RespiratoryTract | Breathing
* #151836 ^designation[0].language = #de-AT 
* #151836 ^designation[0].value = "Dead space tidal volume ratio" 
* #151836 ^property[0].code = #parent 
* #151836 ^property[0].valueCode = #_mdc 
* #151836 ^property[1].code = #hints 
* #151836 ^property[1].valueString = "PART: 2 ~ CODE10: 20764" 
* #151840 "MDC_RES_AWAY"
* #151840 ^definition = Resistance | | Airway | Breathing
* #151840 ^designation[0].language = #de-AT 
* #151840 ^designation[0].value = "Airway Resistance" 
* #151840 ^property[0].code = #parent 
* #151840 ^property[0].valueCode = #_mdc 
* #151840 ^property[1].code = #hints 
* #151840 ^property[1].valueString = "PART: 2 ~ CODE10: 20768" 
* #151844 "MDC_RES_AWAY_EXP"
* #151844 ^definition = Resistance | Expiration | Airway | Breathing
* #151844 ^designation[0].language = #de-AT 
* #151844 ^designation[0].value = "Expiratory Airway Resistance" 
* #151844 ^property[0].code = #parent 
* #151844 ^property[0].valueCode = #_mdc 
* #151844 ^property[1].code = #hints 
* #151844 ^property[1].valueString = "PART: 2 ~ CODE10: 20772" 
* #151848 "MDC_RES_AWAY_INSP"
* #151848 ^definition = Resistance | Inspiration | Airway | Breathing
* #151848 ^designation[0].language = #de-AT 
* #151848 ^designation[0].value = "Inspiratory Airway Resistance" 
* #151848 ^property[0].code = #parent 
* #151848 ^property[0].valueCode = #_mdc 
* #151848 ^property[1].code = #hints 
* #151848 ^property[1].valueString = "PART: 2 ~ CODE10: 20776" 
* #151852 "MDC_TIME_PD_APNEA_OBSTRUC"
* #151852 ^definition = Duration | | Apnea; Obstructive | Breathing
* #151852 ^designation[0].language = #de-AT 
* #151852 ^designation[0].value = "Obstructive Apnea Duration" 
* #151852 ^property[0].code = #parent 
* #151852 ^property[0].valueCode = #_mdc 
* #151852 ^property[1].code = #hints 
* #151852 ^property[1].valueString = "PART: 2 ~ CODE10: 20780" 
* #151856 "MDC_TIME_PD_APNEA"
* #151856 ^definition = Duration | | Apnea | Breathing
* #151856 ^designation[0].language = #de-AT 
* #151856 ^designation[0].value = "Apnea Duration" 
* #151856 ^property[0].code = #parent 
* #151856 ^property[0].valueCode = #_mdc 
* #151856 ^property[1].code = #hints 
* #151856 ^property[1].valueString = "PART: 2 ~ CODE10: 20784" 
* #151860 "MDC_TIME_PD_APNEA_CENT"
* #151860 ^definition = Duration | | Apnea; Central | Breathing
* #151860 ^designation[0].language = #de-AT 
* #151860 ^designation[0].value = "Central Apnea Duration" 
* #151860 ^property[0].code = #parent 
* #151860 ^property[0].valueCode = #_mdc 
* #151860 ^property[1].code = #hints 
* #151860 ^property[1].valueString = "PART: 2 ~ CODE10: 20788" 
* #151864 "MDC_TIME_PD_APNEA_MIX"
* #151864 ^definition = Duration | | Apnea; Mixed | Breathing
* #151864 ^designation[0].language = #de-AT 
* #151864 ^designation[0].value = "Mixed Apnea Duration" 
* #151864 ^property[0].code = #parent 
* #151864 ^property[0].valueCode = #_mdc 
* #151864 ^property[1].code = #hints 
* #151864 ^property[1].valueString = "PART: 2 ~ CODE10: 20792" 
* #151868 "MDC_VOL_AWAY_TIDAL"
* #151868 ^definition = Volume gas | | Lung; Tidal | Breathing
* #151868 ^designation[0].language = #de-AT 
* #151868 ^designation[0].value = "Respiratory tidal volume" 
* #151868 ^property[0].code = #parent 
* #151868 ^property[0].valueCode = #_mdc 
* #151868 ^property[1].code = #hints 
* #151868 ^property[1].valueString = "PART: 2 ~ CODE10: 20796" 
* #151872 "MDC_VOL_AWAY_DEADSP"
* #151872 ^definition = Volume | | Lung; DeadSpace | RespiratoryTract
* #151872 ^designation[0].language = #de-AT 
* #151872 ^designation[0].value = "Airway dead space" 
* #151872 ^property[0].code = #parent 
* #151872 ^property[0].valueCode = #_mdc 
* #151872 ^property[1].code = #hints 
* #151872 ^property[1].valueString = "PART: 2 ~ CODE10: 20800" 
* #151876 "MDC_VOL_GAS_INSP_SINCE_START"
* #151876 ^definition = Volume | SinceStartInspiration | Gas | Breathing
* #151876 ^designation[0].language = #de-AT 
* #151876 ^designation[0].value = "Volume since start inspiration" 
* #151876 ^property[0].code = #parent 
* #151876 ^property[0].valueCode = #_mdc 
* #151876 ^property[1].code = #hints 
* #151876 ^property[1].valueString = "PART: 2 ~ CODE10: 20804" 
* #151880 "MDC_VOL_MINUTE_AWAY"
* #151880 ^definition = Flow | OneMinute | Gas | Breathing
* #151880 ^designation[0].language = #de-AT 
* #151880 ^designation[0].value = "Minute volume" 
* #151880 ^property[0].code = #parent 
* #151880 ^property[0].valueCode = #_mdc 
* #151880 ^property[1].code = #hints 
* #151880 ^property[1].valueString = "PART: 2 ~ CODE10: 20808" 
* #151884 "MDC_VOL_MINUTE_AWAY_EXP"
* #151884 ^definition = Flow | OneMinute; Expired | Gas
* #151884 ^designation[0].language = #de-AT 
* #151884 ^designation[0].value = "Expired Minute volume" 
* #151884 ^property[0].code = #parent 
* #151884 ^property[0].valueCode = #_mdc 
* #151884 ^property[1].code = #hints 
* #151884 ^property[1].valueString = "PART: 2 ~ CODE10: 20812" 
* #151888 "MDC_VOL_MINUTE_AWAY_INSP"
* #151888 ^definition = Flow | OneMinute; Inspiratory | Gas
* #151888 ^designation[0].language = #de-AT 
* #151888 ^designation[0].value = "Inspiratory Minute volume" 
* #151888 ^property[0].code = #parent 
* #151888 ^property[0].valueCode = #_mdc 
* #151888 ^property[1].code = #hints 
* #151888 ^property[1].valueString = "PART: 2 ~ CODE10: 20816" 
* #151908 "MDC_CONC_AWAY_O2"
* #151908 ^definition = Concentration | PartialPressure | Oxygen; Gas | Airway
* #151908 ^designation[0].language = #de-AT 
* #151908 ^designation[0].value = "Concentration (or partial pressure) of oxygen in airway gas" 
* #151908 ^property[0].code = #parent 
* #151908 ^property[0].valueCode = #_mdc 
* #151908 ^property[1].code = #hints 
* #151908 ^property[1].valueString = "PART: 2 ~ CODE10: 20836" 
* #151912 "MDC_CONC_GASDLV_O2_DELTA"
* #151912 ^definition = Concentration | Difference(Inspiration; Expiration) | O2; Gas | Ventilator
* #151912 ^designation[0].language = #de-AT 
* #151912 ^designation[0].value = "Diff. inspired and expired oxygen conc. (ventilator)" 
* #151912 ^property[0].code = #parent 
* #151912 ^property[0].valueCode = #_mdc 
* #151912 ^property[1].code = #hints 
* #151912 ^property[1].valueString = "PART: 2 ~ CODE10: 20840" 
* #151940 "MDC_VENT_FLOW"
* #151940 ^definition = Flow | | Gas | Ventilator
* #151940 ^designation[0].language = #de-AT 
* #151940 ^designation[0].value = " Ventilator airway flow" 
* #151940 ^property[0].code = #parent 
* #151940 ^property[0].valueCode = #_mdc 
* #151940 ^property[1].code = #hints 
* #151940 ^property[1].valueString = "PART: 2 ~ CODE10: 20868" 
* #151944 "MDC_VENT_FLOW_EXP"
* #151944 ^definition = Flow | Expiration | Gas | Ventilator
* #151944 ^designation[0].language = #de-AT 
* #151944 ^designation[0].value = "Ventilation expiratory flow" 
* #151944 ^property[0].code = #parent 
* #151944 ^property[0].valueCode = #_mdc 
* #151944 ^property[1].code = #hints 
* #151944 ^property[1].valueString = "PART: 2 ~ CODE10: 20872" 
* #151945 "MDC_VENT_FLOW_EXP_MAX"
* #151945 ^definition = Flow | Expiration; Maximum | Gas | Ventilator
* #151945 ^designation[0].language = #de-AT 
* #151945 ^designation[0].value = "Ventilation expiratory maximum flow" 
* #151945 ^property[0].code = #parent 
* #151945 ^property[0].valueCode = #_mdc 
* #151945 ^property[1].code = #hints 
* #151945 ^property[1].valueString = "PART: 2 ~ CODE10: 20873" 
* #151948 "MDC_VENT_FLOW_INSP"
* #151948 ^definition = Flow | Inspiration | Gas | Ventilator
* #151948 ^designation[0].language = #de-AT 
* #151948 ^designation[0].value = "Ventilation inspiratory flow" 
* #151948 ^property[0].code = #parent 
* #151948 ^property[0].valueCode = #_mdc 
* #151948 ^property[1].code = #hints 
* #151948 ^property[1].valueString = "PART: 2 ~ CODE10: 20876" 
* #151949 "MDC_VENT_FLOW_INSP_MAX"
* #151949 ^definition = Flow | Inspiration; Maximum | Gas | Ventilator
* #151949 ^designation[0].language = #de-AT 
* #151949 ^designation[0].value = "Ventilation inspiratory maximum flow" 
* #151949 ^property[0].code = #parent 
* #151949 ^property[0].valueCode = #_mdc 
* #151949 ^property[1].code = #hints 
* #151949 ^property[1].valueString = "PART: 2 ~ CODE10: 20877" 
* #151952 "MDC_VENT_FLOW_RATIO_PERF_ALV_INDEX"
* #151952 ^definition = Ratio | Flow(AlveolarVentilation); Flow(Perfusion) | LungStructure | Breathing
* #151952 ^designation[0].language = #de-AT 
* #151952 ^designation[0].value = "Ventilation-perfusion index" 
* #151952 ^property[0].code = #parent 
* #151952 ^property[0].valueCode = #_mdc 
* #151952 ^property[1].code = #hints 
* #151952 ^property[1].valueString = "PART: 2 ~ CODE10: 20880" 
* #151956 "MDC_VENT_PRESS"
* #151956 ^definition = Pressure | | Gas | Ventilator
* #151956 ^designation[0].language = #de-AT 
* #151956 ^designation[0].value = "Ventilator pressure" 
* #151956 ^property[0].code = #parent 
* #151956 ^property[0].valueCode = #_mdc 
* #151956 ^property[1].code = #hints 
* #151956 ^property[1].valueString = "PART: 2 ~ CODE10: 20884" 
* #151957 "MDC_VENT_PRESS_MAX"
* #151957 ^definition = Pressure | Maximum | Gas | Ventilator
* #151957 ^designation[0].language = #de-AT 
* #151957 ^designation[0].value = "Maximum ventilation pressure" 
* #151957 ^property[0].code = #parent 
* #151957 ^property[0].valueCode = #_mdc 
* #151957 ^property[1].code = #hints 
* #151957 ^property[1].valueString = "PART: 2 ~ CODE10: 20885" 
* #151958 "MDC_VENT_PRESS_MIN"
* #151958 ^definition = Pressure | Minimum | Gas | Ventilator
* #151958 ^designation[0].language = #de-AT 
* #151958 ^designation[0].value = "Minimum ventilation pressure" 
* #151958 ^property[0].code = #parent 
* #151958 ^property[0].valueCode = #_mdc 
* #151958 ^property[1].code = #hints 
* #151958 ^property[1].valueString = "PART: 2 ~ CODE10: 20886" 
* #151964 "MDC_VENT_PRESS_OCCL"
* #151964 ^definition = Pressure | Occlusion; Airway | Gas | Ventilator
* #151964 ^designation[0].language = #de-AT 
* #151964 ^designation[0].value = "Ventilation occlusion pressure" 
* #151964 ^property[0].code = #parent 
* #151964 ^property[0].valueCode = #_mdc 
* #151964 ^property[1].code = #hints 
* #151964 ^property[1].valueString = "PART: 2 ~ CODE10: 20892" 
* #151972 "MDC_VENT_PRESS_AWAY"
* #151972 ^definition = Pressure | | Gas | Ventilator; Airway
* #151972 ^designation[0].language = #de-AT 
* #151972 ^designation[0].value = "Inspiratory airway pressure" 
* #151972 ^property[0].code = #parent 
* #151972 ^property[0].valueCode = #_mdc 
* #151972 ^property[1].code = #hints 
* #151972 ^property[1].valueString = "PART: 2 ~ CODE10: 20900" 
* #151973 "MDC_VENT_PRESS_AWAY_MAX"
* #151973 ^definition = Pressure; maximum | | Gas | Ventilator; Airway
* #151973 ^property[0].code = #parent 
* #151973 ^property[0].valueCode = #_mdc 
* #151973 ^property[1].code = #hints 
* #151973 ^property[1].valueString = "PART: 2 ~ CODE10: 20901" 
* #151974 "MDC_VENT_PRESS_AWAY_MIN"
* #151974 ^definition = Pressure; minimum | | Gas | Ventilator; Airway
* #151974 ^property[0].code = #parent 
* #151974 ^property[0].valueCode = #_mdc 
* #151974 ^property[1].code = #hints 
* #151974 ^property[1].valueString = "PART: 2 ~ CODE10: 20902" 
* #151975 "MDC_VENT_PRESS_AWAY_MEAN"
* #151975 ^definition = Pressure; mean | | Gas | Ventilator; Airway
* #151975 ^property[0].code = #parent 
* #151975 ^property[0].valueCode = #_mdc 
* #151975 ^property[1].code = #hints 
* #151975 ^property[1].valueString = "PART: 2 ~ CODE10: 20903" 
* #151976 "MDC_VENT_PRESS_AWAY_END_EXP_POS"
* #151976 ^definition = Pressure | End-expiratory; Applied | Gas | Ventilator
* #151976 ^designation[0].language = #de-AT 
* #151976 ^designation[0].value = "Applied PEEP" 
* #151976 ^property[0].code = #parent 
* #151976 ^property[0].valueCode = #_mdc 
* #151976 ^property[1].code = #hints 
* #151976 ^property[1].valueString = "PART: 2 ~ CODE10: 20904" 
* #151980 "MDC_VENT_VOL_TIDAL"
* #151980 ^definition = Volume | | Lung; Tidal | Ventilator
* #151980 ^designation[0].language = #de-AT 
* #151980 ^designation[0].value = "Ventilation tidal volume" 
* #151980 ^property[0].code = #parent 
* #151980 ^property[0].valueCode = #_mdc 
* #151980 ^property[1].code = #hints 
* #151980 ^property[1].valueString = "PART: 2 ~ CODE10: 20908" 
* #151984 "MDC_VENT_VOL_AWAY_DEADSP"
* #151984 ^definition = Volume | | DeadSpace | Ventilator
* #151984 ^designation[0].language = #de-AT 
* #151984 ^designation[0].value = "Ventilation dead space" 
* #151984 ^property[0].code = #parent 
* #151984 ^property[0].valueCode = #_mdc 
* #151984 ^property[1].code = #hints 
* #151984 ^property[1].valueString = "PART: 2 ~ CODE10: 20912" 
* #151988 "MDC_VENT_VOL_AWAY_DEADSP_REL"
* #151988 ^definition = Ratio | DeadspaceVolume; TidalVolume | RespiratoryTract | Ventilator
* #151988 ^designation[0].language = #de-AT 
* #151988 ^designation[0].value = "Ventilation relative dead space" 
* #151988 ^property[0].code = #parent 
* #151988 ^property[0].valueCode = #_mdc 
* #151988 ^property[1].code = #hints 
* #151988 ^property[1].valueString = "PART: 2 ~ CODE10: 20916" 
* #151992 "MDC_VENT_VOL_LUNG_TRAPD"
* #151992 ^definition = Volume | | Lung; Trapped | Ventilator
* #151992 ^designation[0].language = #de-AT 
* #151992 ^designation[0].value = "Trapped volume" 
* #151992 ^property[0].code = #parent 
* #151992 ^property[0].valueCode = #_mdc 
* #151992 ^property[1].code = #hints 
* #151992 ^property[1].valueString = "PART: 2 ~ CODE10: 20920" 
* #151996 "MDC_VENT_VOL_MINUTE"
* #151996 ^definition = Flow | OneMinute; Inspiration and Expiration | Gas | Ventilator
* #151996 ^designation[0].language = #de-AT 
* #151996 ^designation[0].value = "Inspiratory or expiratory minute volume." 
* #151996 ^property[0].code = #parent 
* #151996 ^property[0].valueCode = #_mdc 
* #151996 ^property[1].code = #hints 
* #151996 ^property[1].valueString = "PART: 2 ~ CODE10: 20924" 
* #152000 "MDC_VENT_VOL_MINUTE_EXP"
* #152000 ^definition = Flow | OneMinute; Expiration | Gas | Ventilator
* #152000 ^designation[0].language = #de-AT 
* #152000 ^designation[0].value = "Expired minute volume" 
* #152000 ^property[0].code = #parent 
* #152000 ^property[0].valueCode = #_mdc 
* #152000 ^property[1].code = #hints 
* #152000 ^property[1].valueString = "PART: 2 ~ CODE10: 20928" 
* #152004 "MDC_VENT_VOL_MINUTE_INSP"
* #152004 ^definition = Flow | OneMinute; Inspiration | Gas | Ventilator
* #152004 ^designation[0].language = #de-AT 
* #152004 ^designation[0].value = "Ventilation inspiratory minute volume" 
* #152004 ^property[0].code = #parent 
* #152004 ^property[0].valueCode = #_mdc 
* #152004 ^property[1].code = #hints 
* #152004 ^property[1].valueString = "PART: 2 ~ CODE10: 20932" 
* #152008 "MDC_VENT_VOL_MINUTE_AWAY"
* #152008 ^definition = Flow | OneMinute | Gas | Ventilator
* #152008 ^designation[0].language = #de-AT 
* #152008 ^designation[0].value = "Ventilation minute volume" 
* #152008 ^property[0].code = #parent 
* #152008 ^property[0].valueCode = #_mdc 
* #152008 ^property[1].code = #hints 
* #152008 ^property[1].valueString = "PART: 2 ~ CODE10: 20936" 
* #152012 "MDC_VENT_VOL_MINUTE_AWAY_MAND"
* #152012 ^definition = Flow | OneMinute; Mandatory | Gas | Ventilator
* #152012 ^designation[0].language = #de-AT 
* #152012 ^designation[0].value = "Mandatory . Minute volume" 
* #152012 ^property[0].code = #parent 
* #152012 ^property[0].valueCode = #_mdc 
* #152012 ^property[1].code = #hints 
* #152012 ^property[1].valueString = "PART: 2 ~ CODE10: 20940" 
* #152016 "MDC_VENT_VOL_MINUTE_AWAY_INSP"
* #152016 ^definition = Flow | OneMinute; Inspiration | Gas | Ventilator
* #152016 ^designation[0].language = #de-AT 
* #152016 ^designation[0].value = "Inspiratory minute volume" 
* #152016 ^property[0].code = #parent 
* #152016 ^property[0].valueCode = #_mdc 
* #152016 ^property[1].code = #hints 
* #152016 ^property[1].valueString = "PART: 2 ~ CODE10: 20944" 
* #152020 "MDC_COEF_GAS_TRAN"
* #152020 ^definition = Index | Ratio (FlowDifference; PressureDifference) | GasTransport | LungStructure
* #152020 ^designation[0].language = #de-AT 
* #152020 ^designation[0].value = "Gas transport coefficient" 
* #152020 ^property[0].code = #parent 
* #152020 ^property[0].valueCode = #_mdc 
* #152020 ^property[1].code = #hints 
* #152020 ^property[1].valueString = "PART: 2 ~ CODE10: 20948" 
* #152024 "MDC_CONC_AWAY_DESFL"
* #152024 ^definition = Concentration | PartialPressure | Desflurane; Gas | Airway
* #152024 ^designation[0].language = #de-AT 
* #152024 ^designation[0].value = "Concentration (or partial pressure) of desflurane in airway gas" 
* #152024 ^property[0].code = #parent 
* #152024 ^property[0].valueCode = #_mdc 
* #152024 ^property[1].code = #hints 
* #152024 ^property[1].valueString = "PART: 2 ~ CODE10: 20952" 
* #152028 "MDC_CONC_AWAY_ENFL"
* #152028 ^definition = Concentration | PartialPressure | Enflurane; Gas | Airway
* #152028 ^designation[0].language = #de-AT 
* #152028 ^designation[0].value = "Concentration (or partial pressure) of enflurane in airway gas" 
* #152028 ^property[0].code = #parent 
* #152028 ^property[0].valueCode = #_mdc 
* #152028 ^property[1].code = #hints 
* #152028 ^property[1].valueString = "PART: 2 ~ CODE10: 20956" 
* #152032 "MDC_CONC_AWAY_HALOTH"
* #152032 ^definition = Concentration | PartialPressure | Halothane; Gas | Airway
* #152032 ^designation[0].language = #de-AT 
* #152032 ^designation[0].value = "Concentration (or partial pressure) of halothane in airway gas" 
* #152032 ^property[0].code = #parent 
* #152032 ^property[0].valueCode = #_mdc 
* #152032 ^property[1].code = #hints 
* #152032 ^property[1].valueString = "PART: 2 ~ CODE10: 20960" 
* #152036 "MDC_CONC_AWAY_SEVOFL"
* #152036 ^definition = Concentration | PartialPressure | Sevoflurane; Gas | Airway
* #152036 ^designation[0].language = #de-AT 
* #152036 ^designation[0].value = "Concentration (or partial pressure) of sevoflurane in airway gas" 
* #152036 ^property[0].code = #parent 
* #152036 ^property[0].valueCode = #_mdc 
* #152036 ^property[1].code = #hints 
* #152036 ^property[1].valueString = "PART: 2 ~ CODE10: 20964" 
* #152040 "MDC_CONC_AWAY_ISOFL"
* #152040 ^definition = Concentration | PartialPressure | Isoflurane; Gas | Airway
* #152040 ^designation[0].language = #de-AT 
* #152040 ^designation[0].value = "Concentration (or partial pressure) of isoflurane in airway gas" 
* #152040 ^property[0].code = #parent 
* #152040 ^property[0].valueCode = #_mdc 
* #152040 ^property[1].code = #hints 
* #152040 ^property[1].valueString = "PART: 2 ~ CODE10: 20968" 
* #152044 "MDC_CONC_AWAY_NO2"
* #152044 ^definition = Concentration | PartialPressure | NO2; Gas | Airway
* #152044 ^designation[0].language = #de-AT 
* #152044 ^designation[0].value = "Concentration (or partial pressure) of nitrogen dioxide in airway gas" 
* #152044 ^property[0].code = #parent 
* #152044 ^property[0].valueCode = #_mdc 
* #152044 ^property[1].code = #hints 
* #152044 ^property[1].valueString = "PART: 2 ~ CODE10: 20972" 
* #152048 "MDC_CONC_AWAY_N2O"
* #152048 ^definition = Concentration | PartialPressure | Nitrous Oxide; Gas | Airway
* #152048 ^designation[0].language = #de-AT 
* #152048 ^designation[0].value = "Concentration (or partial pressure) of nitrous oxide in airway gas" 
* #152048 ^property[0].code = #parent 
* #152048 ^property[0].valueCode = #_mdc 
* #152048 ^property[1].code = #hints 
* #152048 ^property[1].valueString = "PART: 2 ~ CODE10: 20976" 
* #152052 "MDC_CONC_GASDLV_DESFL"
* #152052 ^definition = Concentration | Partial Pressure | Desflurane; Gas | Gas Delivery System or Circuit
* #152052 ^designation[0].language = #de-AT 
* #152052 ^designation[0].value = "Concentration desflurane (gas delivery system or circuit)" 
* #152052 ^property[0].code = #parent 
* #152052 ^property[0].valueCode = #_mdc 
* #152052 ^property[1].code = #hints 
* #152052 ^property[1].valueString = "PART: 2 ~ CODE10: 20980" 
* #152056 "MDC_CONC_GASDLV_ENFL"
* #152056 ^definition = Concentration | Partial Pressure | Enflurane; Gas | Gas Delivery System or Circuit
* #152056 ^designation[0].language = #de-AT 
* #152056 ^designation[0].value = "Concentration enflurane (gas delivery system or circuit)" 
* #152056 ^property[0].code = #parent 
* #152056 ^property[0].valueCode = #_mdc 
* #152056 ^property[1].code = #hints 
* #152056 ^property[1].valueString = "PART: 2 ~ CODE10: 20984" 
* #152060 "MDC_CONC_GASDLV_HALOTH"
* #152060 ^definition = Concentration | Partial Pressure | Halothane; Gas | Gas Delivery System or Circuit
* #152060 ^designation[0].language = #de-AT 
* #152060 ^designation[0].value = "Concentration halothane (gas delivery system or circuit)" 
* #152060 ^property[0].code = #parent 
* #152060 ^property[0].valueCode = #_mdc 
* #152060 ^property[1].code = #hints 
* #152060 ^property[1].valueString = "PART: 2 ~ CODE10: 20988" 
* #152064 "MDC_CONC_GASDLV_SEVOFL"
* #152064 ^definition = Concentration | Partial Pressure | Sevoflurane; Gas | Gas Delivery System or Circuit
* #152064 ^designation[0].language = #de-AT 
* #152064 ^designation[0].value = "Concentration sevoflurane (gas delivery system or circuit)" 
* #152064 ^property[0].code = #parent 
* #152064 ^property[0].valueCode = #_mdc 
* #152064 ^property[1].code = #hints 
* #152064 ^property[1].valueString = "PART: 2 ~ CODE10: 20992" 
* #152068 "MDC_CONC_GASDLV_ISOFL"
* #152068 ^definition = Concentration | Partial Pressure | Isoflurane; Gas | Gas Delivery System or Circuit
* #152068 ^designation[0].language = #de-AT 
* #152068 ^designation[0].value = "Concentration Isoflurane (gas delivery system or circuit)" 
* #152068 ^property[0].code = #parent 
* #152068 ^property[0].valueCode = #_mdc 
* #152068 ^property[1].code = #hints 
* #152068 ^property[1].valueString = "PART: 2 ~ CODE10: 20996" 
* #152072 "MDC_CONC_GASDLV_NO2"
* #152072 ^definition = Concentration | Partial Pressure | NO2; Gas | Gas Delivery System
* #152072 ^designation[0].language = #de-AT 
* #152072 ^designation[0].value = "Concentration NO2 (gas delivery system)" 
* #152072 ^property[0].code = #parent 
* #152072 ^property[0].valueCode = #_mdc 
* #152072 ^property[1].code = #hints 
* #152072 ^property[1].valueString = "PART: 2 ~ CODE10: 21000" 
* #152076 "MDC_CONC_GASDLV_N2O"
* #152076 ^definition = Concentration | Partial Pressure | N2O; Gas | Gas Delivery System
* #152076 ^designation[0].language = #de-AT 
* #152076 ^designation[0].value = "Concentration N2O (gas delivery system)" 
* #152076 ^property[0].code = #parent 
* #152076 ^property[0].valueCode = #_mdc 
* #152076 ^property[1].code = #hints 
* #152076 ^property[1].valueString = "PART: 2 ~ CODE10: 21004" 
* #152080 "MDC_VENT_CONC_SUBST_DELTA"
* #152080 ^definition = Concentration | Difference(Inspiration;Expiration) | Substance; Gas | Ventilator
* #152080 ^designation[0].language = #de-AT 
* #152080 ^designation[0].value = "Diff. inspired and expired substance conc. (ventilator)" 
* #152080 ^property[0].code = #parent 
* #152080 ^property[0].valueCode = #_mdc 
* #152080 ^property[1].code = #hints 
* #152080 ^property[1].valueString = "PART: 2 ~ CODE10: 21008" 
* #152084 "MDC_CONC_AWAY_DESFL_ET"
* #152084 ^definition = Concentration | PartialPressure; End Tidal | Desflurane; Gas | Airway
* #152084 ^designation[0].language = #de-AT 
* #152084 ^designation[0].value = "End tidal desflurane concentration (or partial pressure) in airway gas" 
* #152084 ^property[0].code = #parent 
* #152084 ^property[0].valueCode = #_mdc 
* #152084 ^property[1].code = #hints 
* #152084 ^property[1].valueString = "PART: 2 ~ CODE10: 21012" 
* #152088 "MDC_CONC_AWAY_ENFL_ET"
* #152088 ^definition = Concentration | PartialPressure; End Tidal | Enflurane; Gas | Airway
* #152088 ^designation[0].language = #de-AT 
* #152088 ^designation[0].value = "End tidal enflurane concentration (or partial pressure) in airway gas" 
* #152088 ^property[0].code = #parent 
* #152088 ^property[0].valueCode = #_mdc 
* #152088 ^property[1].code = #hints 
* #152088 ^property[1].valueString = "PART: 2 ~ CODE10: 21016" 
* #152092 "MDC_CONC_AWAY_HALOTH_ET"
* #152092 ^definition = Concentration | PartialPressure; End Tidal | Halothane; Gas | Airway
* #152092 ^designation[0].language = #de-AT 
* #152092 ^designation[0].value = "End tidal halothane concentration (or partial pressure) in airway gas" 
* #152092 ^property[0].code = #parent 
* #152092 ^property[0].valueCode = #_mdc 
* #152092 ^property[1].code = #hints 
* #152092 ^property[1].valueString = "PART: 2 ~ CODE10: 21020" 
* #152096 "MDC_CONC_AWAY_SEVOFL_ET"
* #152096 ^definition = Concentration | PartialPressure; End Tidal | Sevoflurane; Gas | Airway
* #152096 ^designation[0].language = #de-AT 
* #152096 ^designation[0].value = "End tidal sevoflurane concentration (or partial pressure) in airway gas" 
* #152096 ^property[0].code = #parent 
* #152096 ^property[0].valueCode = #_mdc 
* #152096 ^property[1].code = #hints 
* #152096 ^property[1].valueString = "PART: 2 ~ CODE10: 21024" 
* #152100 "MDC_CONC_AWAY_ISOFL_ET"
* #152100 ^definition = Concentration | PartialPressure; End Tidal | Isoflurane; Gas | Airway
* #152100 ^designation[0].language = #de-AT 
* #152100 ^designation[0].value = "End tidal isoflurane concentration (or partial pressure) in airway gas" 
* #152100 ^property[0].code = #parent 
* #152100 ^property[0].valueCode = #_mdc 
* #152100 ^property[1].code = #hints 
* #152100 ^property[1].valueString = "PART: 2 ~ CODE10: 21028" 
* #152104 "MDC_CONC_AWAY_NO2_ET"
* #152104 ^definition = Concentration | PartialPressure; Expiration | NO2; Gas | Airway
* #152104 ^designation[0].language = #de-AT 
* #152104 ^designation[0].value = "End tidal nitrogen dioxide concentration (or partial pressure) in airway gas" 
* #152104 ^property[0].code = #parent 
* #152104 ^property[0].valueCode = #_mdc 
* #152104 ^property[1].code = #hints 
* #152104 ^property[1].valueString = "PART: 2 ~ CODE10: 21032" 
* #152108 "MDC_CONC_AWAY_N2O_ET"
* #152108 ^definition = Concentration | PartialPressure; End Tidal | Nitrous Oxide; Gas | Airway
* #152108 ^designation[0].language = #de-AT 
* #152108 ^designation[0].value = "End tidal nitrous oxide concentration (or partial pressure) in airway gas" 
* #152108 ^property[0].code = #parent 
* #152108 ^property[0].valueCode = #_mdc 
* #152108 ^property[1].code = #hints 
* #152108 ^property[1].valueString = "PART: 2 ~ CODE10: 21036" 
* #152112 "MDC_CONC_AWAY_DESFL_EXP"
* #152112 ^definition = Concentration | PartialPressure; Expiration | Desflurane; Gas | Airway
* #152112 ^designation[0].language = #de-AT 
* #152112 ^designation[0].value = "Expired desflurane concentration (or partial pressure) in airway gas" 
* #152112 ^property[0].code = #parent 
* #152112 ^property[0].valueCode = #_mdc 
* #152112 ^property[1].code = #hints 
* #152112 ^property[1].valueString = "PART: 2 ~ CODE10: 21040" 
* #152116 "MDC_CONC_AWAY_ENFL_EXP"
* #152116 ^definition = Concentration | PartialPressure; Expiration | Enflurane; Gas | Airway
* #152116 ^designation[0].language = #de-AT 
* #152116 ^designation[0].value = "Expired enflurane concentration (or partial pressure) in airway gas" 
* #152116 ^property[0].code = #parent 
* #152116 ^property[0].valueCode = #_mdc 
* #152116 ^property[1].code = #hints 
* #152116 ^property[1].valueString = "PART: 2 ~ CODE10: 21044" 
* #152120 "MDC_CONC_AWAY_HALOTH_EXP"
* #152120 ^definition = Concentration | PartialPressure; Expiration | Halothane; Gas | Airway
* #152120 ^designation[0].language = #de-AT 
* #152120 ^designation[0].value = "Expired halothane concentration (or partial pressure) in airway gas" 
* #152120 ^property[0].code = #parent 
* #152120 ^property[0].valueCode = #_mdc 
* #152120 ^property[1].code = #hints 
* #152120 ^property[1].valueString = "PART: 2 ~ CODE10: 21048" 
* #152124 "MDC_CONC_AWAY_SEVOFL_EXP"
* #152124 ^definition = Concentration | PartialPressure; Expiration | Sevoflurane; Gas | Airway
* #152124 ^designation[0].language = #de-AT 
* #152124 ^designation[0].value = "Expired sevoflurane concentration (or partial pressure) in airway gas" 
* #152124 ^property[0].code = #parent 
* #152124 ^property[0].valueCode = #_mdc 
* #152124 ^property[1].code = #hints 
* #152124 ^property[1].valueString = "PART: 2 ~ CODE10: 21052" 
* #152128 "MDC_CONC_AWAY_ISOFL_EXP"
* #152128 ^definition = Concentration | PartialPressure; Expiration | Isoflurane; Gas | Airway
* #152128 ^designation[0].language = #de-AT 
* #152128 ^designation[0].value = "Expired isoflurane concentration (or partial pressure) in airway gas" 
* #152128 ^property[0].code = #parent 
* #152128 ^property[0].valueCode = #_mdc 
* #152128 ^property[1].code = #hints 
* #152128 ^property[1].valueString = "PART: 2 ~ CODE10: 21056" 
* #152132 "MDC_CONC_AWAY_NO2_EXP"
* #152132 ^definition = Concentration | PartialPressure; Expiration | NO2; Gas | Airway
* #152132 ^designation[0].language = #de-AT 
* #152132 ^designation[0].value = "Expired nitrogen dioxide concentration (or partial pressure) in airway gas" 
* #152132 ^property[0].code = #parent 
* #152132 ^property[0].valueCode = #_mdc 
* #152132 ^property[1].code = #hints 
* #152132 ^property[1].valueString = "PART: 2 ~ CODE10: 21060" 
* #152136 "MDC_CONC_AWAY_N2O_EXP"
* #152136 ^definition = Concentration | PartialPressure; Expiration | Nitrous Oxide; Gas | Airway
* #152136 ^designation[0].language = #de-AT 
* #152136 ^designation[0].value = "Expired nitrous oxide concentration (or partial pressure) in airway gas" 
* #152136 ^property[0].code = #parent 
* #152136 ^property[0].valueCode = #_mdc 
* #152136 ^property[1].code = #hints 
* #152136 ^property[1].valueString = "PART: 2 ~ CODE10: 21064" 
* #152140 "MDC_CONC_GASDLV_DESFL_EXP"
* #152140 ^definition = Concentration | Partial Pressure; Expiration | Desflurane; Gas | Gas Delivery System or Circuit
* #152140 ^designation[0].language = #de-AT 
* #152140 ^designation[0].value = "Concentration airway desflurane expiratory (gas delivery system or circuit)" 
* #152140 ^property[0].code = #parent 
* #152140 ^property[0].valueCode = #_mdc 
* #152140 ^property[1].code = #hints 
* #152140 ^property[1].valueString = "PART: 2 ~ CODE10: 21068" 
* #152144 "MDC_CONC_GASDLV_ENFL_EXP"
* #152144 ^definition = Concentration | Partial Pressure; Expiration | Enflurane; Gas | Gas Delivery System or Circuit
* #152144 ^designation[0].language = #de-AT 
* #152144 ^designation[0].value = "Concentration airway enflurane expiratory (gas delivery system or circuit)" 
* #152144 ^property[0].code = #parent 
* #152144 ^property[0].valueCode = #_mdc 
* #152144 ^property[1].code = #hints 
* #152144 ^property[1].valueString = "PART: 2 ~ CODE10: 21072" 
* #152148 "MDC_CONC_GASDLV_HALOTH_EXP"
* #152148 ^definition = Concentration | Partial Pressure; Expiration | Halothane; Gas | Gas Delivery System or Circuit
* #152148 ^designation[0].language = #de-AT 
* #152148 ^designation[0].value = "Concentration airway halothane expiratory (gas delivery system or circuit)" 
* #152148 ^property[0].code = #parent 
* #152148 ^property[0].valueCode = #_mdc 
* #152148 ^property[1].code = #hints 
* #152148 ^property[1].valueString = "PART: 2 ~ CODE10: 21076" 
* #152152 "MDC_CONC_GASDLV_SEVOFL_EXP"
* #152152 ^definition = Concentration | Partial Pressure; Expiration | Sevoflurane; Gas | Gas Delivery System or Circuit
* #152152 ^designation[0].language = #de-AT 
* #152152 ^designation[0].value = "Concentration airway sevoflurane expiratory (gas delivery system or circuit)" 
* #152152 ^property[0].code = #parent 
* #152152 ^property[0].valueCode = #_mdc 
* #152152 ^property[1].code = #hints 
* #152152 ^property[1].valueString = "PART: 2 ~ CODE10: 21080" 
* #152156 "MDC_CONC_GASDLV_ISOFL_EXP"
* #152156 ^definition = Concentration | Partial Pressure; Expiration | Isoflurane; Gas | Gas Delivery System or Circuit
* #152156 ^designation[0].language = #de-AT 
* #152156 ^designation[0].value = "Concentration airway isoflurane expiratory (gas delivery system or circuit)" 
* #152156 ^property[0].code = #parent 
* #152156 ^property[0].valueCode = #_mdc 
* #152156 ^property[1].code = #hints 
* #152156 ^property[1].valueString = "PART: 2 ~ CODE10: 21084" 
* #152160 "MDC_CONC_GASDLV_NO2_EXP"
* #152160 ^definition = Concentration | Partial Pressure; Expiration | NO2; Gas | Gas Delivery System
* #152160 ^designation[0].language = #de-AT 
* #152160 ^designation[0].value = "Concentration airway nitrogen dioxide expiratory (gas delivery system)" 
* #152160 ^property[0].code = #parent 
* #152160 ^property[0].valueCode = #_mdc 
* #152160 ^property[1].code = #hints 
* #152160 ^property[1].valueString = "PART: 2 ~ CODE10: 21088" 
* #152164 "MDC_CONC_GASDLV_N2O_EXP"
* #152164 ^definition = Concentration | Partial Pressure; Expiration | N2O; Gas | Gas Delivery System
* #152164 ^designation[0].language = #de-AT 
* #152164 ^designation[0].value = "Concentration airway nitrous oxide expiratory (gas delivery system)" 
* #152164 ^property[0].code = #parent 
* #152164 ^property[0].valueCode = #_mdc 
* #152164 ^property[1].code = #hints 
* #152164 ^property[1].valueString = "PART: 2 ~ CODE10: 21092" 
* #152168 "MDC_CONC_AWAY_DESFL_INSP"
* #152168 ^definition = Concentration | PartialPressure; Inspiration | Desflurane; Gas | Airway
* #152168 ^designation[0].language = #de-AT 
* #152168 ^designation[0].value = "Inspiratory desflurane concentration (or partial pressure) in airway gas" 
* #152168 ^property[0].code = #parent 
* #152168 ^property[0].valueCode = #_mdc 
* #152168 ^property[1].code = #hints 
* #152168 ^property[1].valueString = "PART: 2 ~ CODE10: 21096" 
* #152172 "MDC_CONC_AWAY_ENFL_INSP"
* #152172 ^definition = Concentration | PartialPressure; Inspiration | Enflurane; Gas | Airway
* #152172 ^designation[0].language = #de-AT 
* #152172 ^designation[0].value = "Inspiratory enflurane concentration (or partial pressure) in airway gas" 
* #152172 ^property[0].code = #parent 
* #152172 ^property[0].valueCode = #_mdc 
* #152172 ^property[1].code = #hints 
* #152172 ^property[1].valueString = "PART: 2 ~ CODE10: 21100" 
* #152176 "MDC_CONC_AWAY_HALOTH_INSP"
* #152176 ^definition = Concentration | PartialPressure; Inspiration | Halothane; Gas | Airway
* #152176 ^designation[0].language = #de-AT 
* #152176 ^designation[0].value = "Inspiratory halothane concentration (or partial pressure) in airway gas" 
* #152176 ^property[0].code = #parent 
* #152176 ^property[0].valueCode = #_mdc 
* #152176 ^property[1].code = #hints 
* #152176 ^property[1].valueString = "PART: 2 ~ CODE10: 21104" 
* #152180 "MDC_CONC_AWAY_SEVOFL_INSP"
* #152180 ^definition = Concentration | PartialPressure; Inspiration | Sevoflurane; Gas | Airway
* #152180 ^designation[0].language = #de-AT 
* #152180 ^designation[0].value = "Inspiratory sevoflurane concentration (or partial pressure) in airway gas" 
* #152180 ^property[0].code = #parent 
* #152180 ^property[0].valueCode = #_mdc 
* #152180 ^property[1].code = #hints 
* #152180 ^property[1].valueString = "PART: 2 ~ CODE10: 21108" 
* #152184 "MDC_CONC_AWAY_ISOFL_INSP"
* #152184 ^definition = Concentration | PartialPressure; Inspiration | Isoflurane; Gas | Airway
* #152184 ^designation[0].language = #de-AT 
* #152184 ^designation[0].value = "Inspiratory isoflurane concentration (or partial pressure) in airway gas" 
* #152184 ^property[0].code = #parent 
* #152184 ^property[0].valueCode = #_mdc 
* #152184 ^property[1].code = #hints 
* #152184 ^property[1].valueString = "PART: 2 ~ CODE10: 21112" 
* #152188 "MDC_CONC_AWAY_NO2_INSP"
* #152188 ^definition = Concentration | PartialPressure; Inspiration | NO2; Gas | Airway
* #152188 ^designation[0].language = #de-AT 
* #152188 ^designation[0].value = "Inspiratory nitrogen dioxide concentration (or partial pressure) in airway gas" 
* #152188 ^property[0].code = #parent 
* #152188 ^property[0].valueCode = #_mdc 
* #152188 ^property[1].code = #hints 
* #152188 ^property[1].valueString = "PART: 2 ~ CODE10: 21116" 
* #152192 "MDC_CONC_AWAY_N2O_INSP"
* #152192 ^definition = Concentration | PartialPressure; Inspiration | Nitrous Oxide; Gas | Airway
* #152192 ^designation[0].language = #de-AT 
* #152192 ^designation[0].value = "Inspiratory nitrous oxide concentration (or partial pressure) in airway gas" 
* #152192 ^property[0].code = #parent 
* #152192 ^property[0].valueCode = #_mdc 
* #152192 ^property[1].code = #hints 
* #152192 ^property[1].valueString = "PART: 2 ~ CODE10: 21120" 
* #152196 "MDC_CONC_AWAY_O2_INSP"
* #152196 ^definition = Concentration | PartialPressure; Inspiration | Oxygen; Gas | Airway
* #152196 ^designation[0].language = #de-AT 
* #152196 ^designation[0].value = "Inspiratory oxygen concentration (or partial pressure) in airway gas" 
* #152196 ^property[0].code = #parent 
* #152196 ^property[0].valueCode = #_mdc 
* #152196 ^property[1].code = #hints 
* #152196 ^property[1].valueString = "PART: 2 ~ CODE10: 21124" 
* #152200 "MDC_CONC_GASDLV_DESFL_INSP"
* #152200 ^definition = Concentration | Partial Pressure; Inspiration | Desflurane; Gas | Gas Delivery System or Circuit
* #152200 ^designation[0].language = #de-AT 
* #152200 ^designation[0].value = "Concentration airway desflurane inspiratory (gas delivery system or circuit)" 
* #152200 ^property[0].code = #parent 
* #152200 ^property[0].valueCode = #_mdc 
* #152200 ^property[1].code = #hints 
* #152200 ^property[1].valueString = "PART: 2 ~ CODE10: 21128" 
* #152204 "MDC_CONC_GASDLV_ENFL_INSP"
* #152204 ^definition = Concentration | Partial Pressure; Inspiration | Enflurane; Gas | Gas Delivery System or Circuit
* #152204 ^designation[0].language = #de-AT 
* #152204 ^designation[0].value = "Concentration airway enflurane inspiratory (gas delivery system or circuit)" 
* #152204 ^property[0].code = #parent 
* #152204 ^property[0].valueCode = #_mdc 
* #152204 ^property[1].code = #hints 
* #152204 ^property[1].valueString = "PART: 2 ~ CODE10: 21132" 
* #152208 "MDC_CONC_GASDLV_HALOTH_INSP"
* #152208 ^definition = Concentration | Partial Pressure; Inspiration | Halothane; Gas | Gas Delivery System or Circuit
* #152208 ^designation[0].language = #de-AT 
* #152208 ^designation[0].value = "Concentration airway halothane inspiratory (gas delivery system or circuit)" 
* #152208 ^property[0].code = #parent 
* #152208 ^property[0].valueCode = #_mdc 
* #152208 ^property[1].code = #hints 
* #152208 ^property[1].valueString = "PART: 2 ~ CODE10: 21136" 
* #152212 "MDC_CONC_GASDLV_SEVOFL_INSP"
* #152212 ^definition = Concentration | Partial Pressure; Inspiration | Sevoflurane; Gas | Gas Delivery System or Circuit
* #152212 ^designation[0].language = #de-AT 
* #152212 ^designation[0].value = "Concentration airway sevoflurane inspiratory (gas delivery system or circuit)" 
* #152212 ^property[0].code = #parent 
* #152212 ^property[0].valueCode = #_mdc 
* #152212 ^property[1].code = #hints 
* #152212 ^property[1].valueString = "PART: 2 ~ CODE10: 21140" 
* #152216 "MDC_CONC_GASDLV_ISOFL_INSP"
* #152216 ^definition = Concentration | Partial Pressure; Inspiration | Isoflurane; Gas | Gas Delivery System or Circuit
* #152216 ^designation[0].language = #de-AT 
* #152216 ^designation[0].value = "Concentration airway isoflurane inspiratory (gas delivery system or circuit)" 
* #152216 ^property[0].code = #parent 
* #152216 ^property[0].valueCode = #_mdc 
* #152216 ^property[1].code = #hints 
* #152216 ^property[1].valueString = "PART: 2 ~ CODE10: 21144" 
* #152220 "MDC_CONC_GASDLV_NO2_INSP"
* #152220 ^definition = Concentration | Partial Pressure; Inspiration | NO2; Gas | Gas Delivery System
* #152220 ^designation[0].language = #de-AT 
* #152220 ^designation[0].value = "Concentration airway nitrogen dioxide inspiratory (gas delivery system)" 
* #152220 ^property[0].code = #parent 
* #152220 ^property[0].valueCode = #_mdc 
* #152220 ^property[1].code = #hints 
* #152220 ^property[1].valueString = "PART: 2 ~ CODE10: 21148" 
* #152224 "MDC_CONC_GASDLV_N2O_INSP"
* #152224 ^definition = Concentration | Partial Pressure; Inspiration | N2O; Gas | Gas Delivery System
* #152224 ^designation[0].language = #de-AT 
* #152224 ^designation[0].value = "Concentration airway nitrous oxide inspiratory (gas delivery system)" 
* #152224 ^property[0].code = #parent 
* #152224 ^property[0].valueCode = #_mdc 
* #152224 ^property[1].code = #hints 
* #152224 ^property[1].valueString = "PART: 2 ~ CODE10: 21152" 
* #152416 "MDC_VENT_TIME_PD_INSP"
* #152416 ^definition = Duration | Inspiratory phase | Inflation | Ventilator
* #152416 ^designation[0].language = #de-AT 
* #152416 ^designation[0].value = "Inspiratory time" 
* #152416 ^property[0].code = #parent 
* #152416 ^property[0].valueCode = #_mdc 
* #152416 ^property[1].code = #hints 
* #152416 ^property[1].valueString = "PART: 2 ~ CODE10: 21344" 
* #152420 "MDC_FLOW_O2_CONSUMP"
* #152420 ^definition = Flow | Consumption | O2; Gas | Breathing
* #152420 ^designation[0].language = #de-AT 
* #152420 ^designation[0].value = "O2 Consumption" 
* #152420 ^property[0].code = #parent 
* #152420 ^property[0].valueCode = #_mdc 
* #152420 ^property[1].code = #hints 
* #152420 ^property[1].valueString = "PART: 2 ~ CODE10: 21348" 
* #152424 "MDC_VENT_PRESS_RESP_PLAT"
* #152424 ^definition = Pressure | Plateau | Gas | Ventilator
* #152424 ^designation[0].language = #de-AT 
* #152424 ^designation[0].value = "Ventilation plateau pressure" 
* #152424 ^property[0].code = #parent 
* #152424 ^property[0].valueCode = #_mdc 
* #152424 ^property[1].code = #hints 
* #152424 ^property[1].valueString = "PART: 2 ~ CODE10: 21352" 
* #152428 "MDC_VENT_PRESS_TRIG_SENS"
* #152428 ^definition = Pressure | | TriggerSensitivity; start inspiration | Ventilator
* #152428 ^designation[0].language = #de-AT 
* #152428 ^designation[0].value = "Ventilator pressure trigger sensitivity" 
* #152428 ^property[0].code = #parent 
* #152428 ^property[0].valueCode = #_mdc 
* #152428 ^property[1].code = #hints 
* #152428 ^property[1].valueString = "PART: 2 ~ CODE10: 21356" 
* #152432 "MDC_VENT_VOL_LEAK"
* #152432 ^definition = Volume | Leakage | Ventilation | Ventilator
* #152432 ^designation[0].language = #de-AT 
* #152432 ^designation[0].value = "Leakage volume" 
* #152432 ^property[0].code = #parent 
* #152432 ^property[0].valueCode = #_mdc 
* #152432 ^property[1].code = #hints 
* #152432 ^property[1].valueString = "PART: 2 ~ CODE10: 21360" 
* #152436 "MDC_VENT_VOL_LUNG_ALV"
* #152436 ^definition = Volume | | Lung; Alveolar | RespiratoryTract
* #152436 ^designation[0].language = #de-AT 
* #152436 ^designation[0].value = "Alveolar ventilation" 
* #152436 ^property[0].code = #parent 
* #152436 ^property[0].valueCode = #_mdc 
* #152436 ^property[1].code = #hints 
* #152436 ^property[1].valueString = "PART: 2 ~ CODE10: 21364" 
* #152440 "MDC_CONC_AWAY_O2_ET"
* #152440 ^definition = Concentration | PartialPressure; End Tidal | Oxygen; Gas | Airway
* #152440 ^designation[0].language = #de-AT 
* #152440 ^designation[0].value = "End tidal oxygen concentration (or partial pressure) in airway gas" 
* #152440 ^property[0].code = #parent 
* #152440 ^property[0].valueCode = #_mdc 
* #152440 ^property[1].code = #hints 
* #152440 ^property[1].valueString = "PART: 2 ~ CODE10: 21368" 
* #152444 "MDC_CONC_AWAY_N2"
* #152444 ^definition = Concentration | PartialPressure | N2; Gas | Airway
* #152444 ^designation[0].language = #de-AT 
* #152444 ^designation[0].value = "Concentration (or partial pressure) of nitrogen in airway gas" 
* #152444 ^property[0].code = #parent 
* #152444 ^property[0].valueCode = #_mdc 
* #152444 ^property[1].code = #hints 
* #152444 ^property[1].valueString = "PART: 2 ~ CODE10: 21372" 
* #152448 "MDC_CONC_AWAY_N2_ET"
* #152448 ^definition = Concentration | PartialPressure; Expiration | N2; Gas | Airway
* #152448 ^designation[0].language = #de-AT 
* #152448 ^designation[0].value = "End tidal nitrogen concentration (or partial pressure) in airway gas" 
* #152448 ^property[0].code = #parent 
* #152448 ^property[0].valueCode = #_mdc 
* #152448 ^property[1].code = #hints 
* #152448 ^property[1].valueString = "PART: 2 ~ CODE10: 21376" 
* #152452 "MDC_CONC_AWAY_N2_INSP"
* #152452 ^definition = Concentration | PartialPressure; Inspiration | N2; Gas | Airway
* #152452 ^designation[0].language = #de-AT 
* #152452 ^designation[0].value = "Inspiratory nitrogen concentration (or partial pressure) in airway gas" 
* #152452 ^property[0].code = #parent 
* #152452 ^property[0].valueCode = #_mdc 
* #152452 ^property[1].code = #hints 
* #152452 ^property[1].valueString = "PART: 2 ~ CODE10: 21380" 
* #152456 "MDC_CONC_AWAY_AGENT"
* #152456 ^definition = Concentration | | Agent; Gas | Airway
* #152456 ^designation[0].language = #de-AT 
* #152456 ^designation[0].value = "Concentration airway agent" 
* #152456 ^property[0].code = #parent 
* #152456 ^property[0].valueCode = #_mdc 
* #152456 ^property[1].code = #hints 
* #152456 ^property[1].valueString = "PART: 2 ~ CODE10: 21384" 
* #152460 "MDC_CONC_AWAY_AGENT_ET"
* #152460 ^definition = Concentration | EndTidal | Agent; Gas | Airway
* #152460 ^designation[0].language = #de-AT 
* #152460 ^designation[0].value = "Concentration airway agent end tidal" 
* #152460 ^property[0].code = #parent 
* #152460 ^property[0].valueCode = #_mdc 
* #152460 ^property[1].code = #hints 
* #152460 ^property[1].valueString = "PART: 2 ~ CODE10: 21388" 
* #152464 "MDC_CONC_AWAY_AGENT_INSP"
* #152464 ^definition = Concentration | Inspiration | Agent; Gas | Airway
* #152464 ^designation[0].language = #de-AT 
* #152464 ^designation[0].value = "Concentration airway agent inspiration" 
* #152464 ^property[0].code = #parent 
* #152464 ^property[0].valueCode = #_mdc 
* #152464 ^property[1].code = #hints 
* #152464 ^property[1].valueString = "PART: 2 ~ CODE10: 21392" 
* #152482 "MDC_VENT_RESP_BACKUP_RATE"
* #152482 ^definition = Rate | Backup | Breath | Ventilator
* #152482 ^designation[0].language = #de-AT 
* #152482 ^designation[0].value = "Backup ventilation rate" 
* #152482 ^property[0].code = #parent 
* #152482 ^property[0].valueCode = #_mdc 
* #152482 ^property[1].code = #hints 
* #152482 ^property[1].valueString = "PART: 2 ~ CODE10: 21410" 
* #152490 "MDC_VENT_RESP_BTSD_PSAZC_RATE"
* #152490 ^definition = Rate | NOS | Breath | Ventilator and Patient; total breath rate; all breath types
* #152490 ^designation[0].language = #de-AT 
* #152490 ^designation[0].value = "Total respiratory rate, total breath rate" 
* #152490 ^property[0].code = #parent 
* #152490 ^property[0].valueCode = #_mdc 
* #152490 ^property[1].code = #hints 
* #152490 ^property[1].valueString = "PART: 2 ~ CODE10: 21418" 
* #152498 "MDC_VENT_RESP_BTSD_P_RATE"
* #152498 ^definition = Rate | NOS | Breath | Ventilator and Patient; patient-initiated breaths; unassisted by ventilator
* #152498 ^designation[0].language = #de-AT 
* #152498 ^designation[0].value = "Unassisted spontaneous breath rate, respiration rate" 
* #152498 ^property[0].code = #parent 
* #152498 ^property[0].valueCode = #_mdc 
* #152498 ^property[1].code = #hints 
* #152498 ^property[1].valueString = "PART: 2 ~ CODE10: 21426" 
* #152506 "MDC_VENT_RESP_BTSD_S_RATE"
* #152506 ^definition = Rate | NOS | Breath | Ventilator and Patient; patient-initiated breaths; delivered as supported breaths
* #152506 ^designation[0].language = #de-AT 
* #152506 ^designation[0].value = "Supported breath rate" 
* #152506 ^property[0].code = #parent 
* #152506 ^property[0].valueCode = #_mdc 
* #152506 ^property[1].code = #hints 
* #152506 ^property[1].valueString = "PART: 2 ~ CODE10: 21434" 
* #152514 "MDC_VENT_RESP_BTSD_A_RATE"
* #152514 ^definition = Rate | NOS | Breath | Ventilator and Patient; patient-initiated primary inflations at a rate greater than set rate; delivered as assisted breaths
* #152514 ^designation[0].language = #de-AT 
* #152514 ^designation[0].value = "Assisted breath rate" 
* #152514 ^property[0].code = #parent 
* #152514 ^property[0].valueCode = #_mdc 
* #152514 ^property[1].code = #hints 
* #152514 ^property[1].valueString = "PART: 2 ~ CODE10: 21442" 
* #152522 "MDC_VENT_RESP_BTSD_Z_RATE"
* #152522 ^definition = Rate | NOS | Breath | Ventilator and Patient; patient-initiated primary inflations within a time synchronization widow; delivered as synchronized assisted breaths
* #152522 ^designation[0].language = #de-AT 
* #152522 ^designation[0].value = "Synchronized assisted breath rate" 
* #152522 ^property[0].code = #parent 
* #152522 ^property[0].valueCode = #_mdc 
* #152522 ^property[1].code = #hints 
* #152522 ^property[1].valueString = "PART: 2 ~ CODE10: 21450" 
* #152530 "MDC_VENT_RESP_BTSD_C_RATE"
* #152530 ^definition = Rate | NOS | Breath | Ventilator and Patient; primary inflations initiated by the ventilator at the set rate; delivered as controlled breaths
* #152530 ^designation[0].language = #de-AT 
* #152530 ^designation[0].value = "Controlled breath rate" 
* #152530 ^property[0].code = #parent 
* #152530 ^property[0].valueCode = #_mdc 
* #152530 ^property[1].code = #hints 
* #152530 ^property[1].valueString = "PART: 2 ~ CODE10: 21458" 
* #152538 "MDC_VENT_RESP_BTSD_PS_RATE"
* #152538 ^definition = Rate | NOS | Breath | Ventilator and Patient; unassisted and supported breaths
* #152538 ^designation[0].language = #de-AT 
* #152538 ^designation[0].value = "Spontaneous respiration rate (traditional)" 
* #152538 ^property[0].code = #parent 
* #152538 ^property[0].valueCode = #_mdc 
* #152538 ^property[1].code = #hints 
* #152538 ^property[1].valueString = "PART: 2 ~ CODE10: 21466" 
* #152546 "MDC_VENT_RESP_BTSD_AZC_RATE"
* #152546 ^definition = Rate | NOS | Breath | Ventilator and Patient; delivered as assisted; synchronized assisted or controlled breaths by ventilator.
* #152546 ^designation[0].language = #de-AT 
* #152546 ^designation[0].value = "Mandatory respiration rate (traditional)" 
* #152546 ^property[0].code = #parent 
* #152546 ^property[0].valueCode = #_mdc 
* #152546 ^property[1].code = #hints 
* #152546 ^property[1].valueString = "PART: 2 ~ CODE10: 21474" 
* #152554 "MDC_VENT_RESP_BTSD_PSAZ_RATE"
* #152554 ^definition = Rate | NOS | Breath | Ventilator and Patient; patient-initiated breaths; unassisted or delivered as supported; assisted or synchronized assisted breaths.
* #152554 ^designation[0].language = #de-AT 
* #152554 ^designation[0].value = "Patient-initiated breath rate" 
* #152554 ^property[0].code = #parent 
* #152554 ^property[0].valueCode = #_mdc 
* #152554 ^property[1].code = #hints 
* #152554 ^property[1].valueString = "PART: 2 ~ CODE10: 21482" 
* #152562 "MDC_VENT_RESP_TARGET_AUTO_RATE"
* #152562 ^definition = Rate | Target; Calculated | Breath | Ventilator; all breath and inflation types
* #152562 ^designation[0].language = #de-AT 
* #152562 ^designation[0].value = "Target respiratory rate" 
* #152562 ^property[0].code = #parent 
* #152562 ^property[0].valueCode = #_mdc 
* #152562 ^property[1].code = #hints 
* #152562 ^property[1].valueString = "PART: 2 ~ CODE10: 21490" 
* #152584 "MDC_FLOW_AWAY_EXP_FORCED_PEAK"
* #152584 ^definition = Flow | Expiration; Maximum | Gas | Maximal Forced Expiration
* #152584 ^designation[0].language = #de-AT 
* #152584 ^designation[0].value = "peak expiratory flow" 
* #152584 ^property[0].code = #parent 
* #152584 ^property[0].valueCode = #_mdc 
* #152584 ^property[1].code = #hints 
* #152584 ^property[1].valueString = "PART: 2 ~ CODE10: 21512" 
* #152585 "MDC_FLOW_AWAY_EXP_FORCED_PEAK_PB"
* #152585 ^definition = Flow | Expiration; Maximum of Maximum | Gas | Maximal Forced Expiration
* #152585 ^designation[0].language = #de-AT 
* #152585 ^designation[0].value = "Personal best" 
* #152585 ^property[0].code = #parent 
* #152585 ^property[0].valueCode = #_mdc 
* #152585 ^property[1].code = #hints 
* #152585 ^property[1].valueString = "PART: 2 ~ CODE10: 21513" 
* #152586 "MDC_VOL_AWAY_EXP_FORCED_1S"
* #152586 ^definition = Flow | Expiration; Maximum; 1s | Gas | Maximal Forced Expiration
* #152586 ^designation[0].language = #de-AT 
* #152586 ^designation[0].value = "forced expiratory volume at 1s" 
* #152586 ^property[0].code = #parent 
* #152586 ^property[0].valueCode = #_mdc 
* #152586 ^property[1].code = #hints 
* #152586 ^property[1].valueString = "PART: 2 ~ CODE10: 21514" 
* #152587 "MDC_VOL_AWAY_EXP_FORCED_EXP_6S"
* #152587 ^definition = Flow | Expiration; Maximum; 6s | Gas | Maximal Forced Expiration
* #152587 ^designation[0].language = #de-AT 
* #152587 ^designation[0].value = "forced expiratory volume at 6s" 
* #152587 ^property[0].code = #parent 
* #152587 ^property[0].valueCode = #_mdc 
* #152587 ^property[1].code = #hints 
* #152587 ^property[1].valueString = "PART: 2 ~ CODE10: 21515" 
* #152596 "MDC_RES_AWAY_DYNAMIC"
* #152596 ^definition = Resistance | Dynamic; Least Squares | Airway | Breathing
* #152596 ^designation[0].language = #de-AT 
* #152596 ^designation[0].value = "Dynamic airway resistance" 
* #152596 ^property[0].code = #parent 
* #152596 ^property[0].valueCode = #_mdc 
* #152596 ^property[1].code = #hints 
* #152596 ^property[1].valueString = "PART: 2 ~ CODE10: 21524" 
* #152600 "MDC_COMPL_LUNG_DYNAMIC"
* #152600 ^definition = Compliance | Dynamic; Least Squares | Alveoli | LungStructure
* #152600 ^designation[0].language = #de-AT 
* #152600 ^designation[0].value = "Dynamic compliance" 
* #152600 ^property[0].code = #parent 
* #152600 ^property[0].valueCode = #_mdc 
* #152600 ^property[1].code = #hints 
* #152600 ^property[1].valueString = "PART: 2 ~ CODE10: 21528" 
* #152604 "MDC_VENT_TIME_PD_INSP_PERCENT"
* #152604 ^definition = Duration; ratio | Inspiratory phase; total respiratory period  | Inflation | Ventilator
* #152604 ^designation[0].language = #de-AT 
* #152604 ^designation[0].value = "Inspiratory time percent" 
* #152604 ^property[0].code = #parent 
* #152604 ^property[0].valueCode = #_mdc 
* #152604 ^property[1].code = #hints 
* #152604 ^property[1].valueString = "PART: 2 ~ CODE10: 21532" 
* #152608 "MDC_TIME_PD_INSP"
* #152608 ^definition = Duration | Inspiratory Phase | Gas | Breath
* #152608 ^designation[0].language = #de-AT 
* #152608 ^designation[0].value = "Inspiratory time" 
* #152608 ^property[0].code = #parent 
* #152608 ^property[0].valueCode = #_mdc 
* #152608 ^property[1].code = #hints 
* #152608 ^property[1].valueString = "PART: 2 ~ CODE10: 21536" 
* #152612 "MDC_TIME_PD_EXP"
* #152612 ^definition = Duration | Expiratory Phase | Gas | Breath
* #152612 ^designation[0].language = #de-AT 
* #152612 ^designation[0].value = "Expiratory time" 
* #152612 ^property[0].code = #parent 
* #152612 ^property[0].valueCode = #_mdc 
* #152612 ^property[1].code = #hints 
* #152612 ^property[1].valueString = "PART: 2 ~ CODE10: 21540" 
* #152616 "MDC_VENT_TIME_PD_INSP_BACKUP"
* #152616 ^definition = Duration | Inspiratory phase | Inflation; Backup | Ventilator
* #152616 ^designation[0].language = #de-AT 
* #152616 ^designation[0].value = "Backup inspiratory time" 
* #152616 ^property[0].code = #parent 
* #152616 ^property[0].valueCode = #_mdc 
* #152616 ^property[1].code = #hints 
* #152616 ^property[1].valueString = "PART: 2 ~ CODE10: 21544" 
* #152621 "MDC_VENT_TIME_PD_SUPP_MAX"
* #152621 ^definition = Duration; maximum | Inspiratory phase | Pressure support inflation | Ventilator
* #152621 ^designation[0].language = #de-AT 
* #152621 ^designation[0].value = "Maximum pressure support time" 
* #152621 ^property[0].code = #parent 
* #152621 ^property[0].valueCode = #_mdc 
* #152621 ^property[1].code = #hints 
* #152621 ^property[1].valueString = "PART: 2 ~ CODE10: 21549" 
* #152624 "MDC_VENT_TIME_PD_INSP_PAUSE"
* #152624 ^definition = Duration | Inspiratory pause | end inspiratory flow to start expiratory flow | Ventilator
* #152624 ^property[0].code = #parent 
* #152624 ^property[0].valueCode = #_mdc 
* #152624 ^property[1].code = #hints 
* #152624 ^property[1].valueString = "PART: 2 ~ CODE10: 21552" 
* #152628 "MDC_VENT_TIME_PD_INSP_PAUSE_PERCENT"
* #152628 ^definition = Duration; ratio | Inspiratory pause; inspiratory phase | end inspiratory flow to start expiratory flow | Ventilator
* #152628 ^property[0].code = #parent 
* #152628 ^property[0].valueCode = #_mdc 
* #152628 ^property[1].code = #hints 
* #152628 ^property[1].valueString = "PART: 2 ~ CODE10: 21556" 
* #152632 "MDC_VENT_TIME_PD_INSP_HOLD"
* #152632 ^definition = Duration | Inspiratory hold | temporarily maintain constant lung volume (zero flow) at end inspiratory or inflation phase | Ventilator
* #152632 ^property[0].code = #parent 
* #152632 ^property[0].valueCode = #_mdc 
* #152632 ^property[1].code = #hints 
* #152632 ^property[1].valueString = "PART: 2 ~ CODE10: 21560" 
* #152636 "MDC_VENT_TIME_PD_EXP_HOLD"
* #152636 ^definition = Duration | Expiratory hold | temporarily maintain constant lung volume (zero flow) at set extension of the expiratory phase | Ventilator
* #152636 ^property[0].code = #parent 
* #152636 ^property[0].valueCode = #_mdc 
* #152636 ^property[1].code = #hints 
* #152636 ^property[1].valueString = "PART: 2 ~ CODE10: 21564" 
* #152640 "MDC_RATIO_INSP"
* #152640 ^definition = Ratio; Duration | InspiratoryTime; TotalRespiratoryCycleTime | Flow | Gas; Airway
* #152640 ^designation[0].language = #de-AT 
* #152640 ^designation[0].value = "Inspiratory Percent" 
* #152640 ^property[0].code = #parent 
* #152640 ^property[0].valueCode = #_mdc 
* #152640 ^property[1].code = #hints 
* #152640 ^property[1].valueString = "PART: 2 ~ CODE10: 21568" 
* #152644 "MDC_VENT_TIME_PD_INSP_THIGH"
* #152644 ^definition = Duration | High baseline pressure | APRV or Bi-Level modes | Ventilator
* #152644 ^designation[0].language = #de-AT 
* #152644 ^designation[0].value = "Thigh" 
* #152644 ^property[0].code = #parent 
* #152644 ^property[0].valueCode = #_mdc 
* #152644 ^property[1].code = #hints 
* #152644 ^property[1].valueString = "PART: 2 ~ CODE10: 21572" 
* #152648 "MDC_VENT_TIME_PD_EXP_TLOW"
* #152648 ^definition = Duration | Low baseline pressure | APRV or Bi-Level modes | Ventilator
* #152648 ^designation[0].language = #de-AT 
* #152648 ^designation[0].value = "Tlow" 
* #152648 ^property[0].code = #parent 
* #152648 ^property[0].valueCode = #_mdc 
* #152648 ^property[1].code = #hints 
* #152648 ^property[1].valueString = "PART: 2 ~ CODE10: 21576" 
* #152652 "MDC_VENT_FLOW_BIAS"
* #152652 ^definition = Flow | Bias | Gas | Ventilator
* #152652 ^designation[0].language = #de-AT 
* #152652 ^designation[0].value = "Bias flow" 
* #152652 ^property[0].code = #parent 
* #152652 ^property[0].valueCode = #_mdc 
* #152652 ^property[1].code = #hints 
* #152652 ^property[1].valueString = "PART: 2 ~ CODE10: 21580" 
* #152656 "MDC_VENT_FLOW_CONTINUOUS"
* #152656 ^definition = Flow | Continuous | Gas | Ventilator
* #152656 ^designation[0].language = #de-AT 
* #152656 ^designation[0].value = "Continuous flow" 
* #152656 ^property[0].code = #parent 
* #152656 ^property[0].valueCode = #_mdc 
* #152656 ^property[1].code = #hints 
* #152656 ^property[1].valueString = "PART: 2 ~ CODE10: 21584" 
* #152660 "MDC_VOL_AWAY_TIDAL_INSP"
* #152660 ^definition = Volume | | Lung; Tidal; Inspiratory Phase | airway; per breath (breath type not specified, default = any)
* #152660 ^designation[0].language = #de-AT 
* #152660 ^designation[0].value = "Inspired Tidal Volume" 
* #152660 ^property[0].code = #parent 
* #152660 ^property[0].valueCode = #_mdc 
* #152660 ^property[1].code = #hints 
* #152660 ^property[1].valueString = "PART: 2 ~ CODE10: 21588" 
* #152664 "MDC_VOL_AWAY_TIDAL_EXP"
* #152664 ^definition = Volume | | Lung; Tidal; Expiratory Phase | airway; per breath (breath type not specified, default = any)
* #152664 ^designation[0].language = #de-AT 
* #152664 ^designation[0].value = "Expired Tidal Volume" 
* #152664 ^property[0].code = #parent 
* #152664 ^property[0].valueCode = #_mdc 
* #152664 ^property[1].code = #hints 
* #152664 ^property[1].valueString = "PART: 2 ~ CODE10: 21592" 
* #152668 "MDC_VOL_AWAY_TIDAL_EXP_BTSD_PSAZC_PER_IBW"
* #152668 ^definition = Volume per BodyMass | | Lung; Tidal; Expiratory Phase | airway; per breath; all breath and inflation types.
* #152668 ^designation[0].language = #de-AT 
* #152668 ^designation[0].value = "(for all breath types)" 
* #152668 ^property[0].code = #parent 
* #152668 ^property[0].valueCode = #_mdc 
* #152668 ^property[1].code = #hints 
* #152668 ^property[1].valueString = "PART: 2 ~ CODE10: 21596" 
* #152672 "MDC_VOL_AWAY_TIDAL_EXP_BTSD_PSAZC"
* #152672 ^definition = Volume | | Lung; Tidal; Expiratory Phase | airway; per breath; all breath and inflation types.
* #152672 ^designation[0].language = #de-AT 
* #152672 ^designation[0].value = "(for all breath types)" 
* #152672 ^property[0].code = #parent 
* #152672 ^property[0].valueCode = #_mdc 
* #152672 ^property[1].code = #hints 
* #152672 ^property[1].valueString = "PART: 2 ~ CODE10: 21600" 
* #152676 "MDC_VOL_AWAY_TIDAL_EXP_BTSD_PS"
* #152676 ^definition = Volume | | Lung; Tidal; Expiratory Phase | airway; per unassisted or supported breath
* #152676 ^designation[0].language = #de-AT 
* #152676 ^designation[0].value = "Expired Tidal Volume for unassisted or supported (aka spontaneous) breaths" 
* #152676 ^property[0].code = #parent 
* #152676 ^property[0].valueCode = #_mdc 
* #152676 ^property[1].code = #hints 
* #152676 ^property[1].valueString = "PART: 2 ~ CODE10: 21604" 
* #152679 "MDC_VOL_AWAY_TIDAL_EXP_BTSD_PS_MEAN"
* #152679 ^property[0].code = #parent 
* #152679 ^property[0].valueCode = #_mdc 
* #152679 ^property[1].code = #hints 
* #152679 ^property[1].valueString = "PART: 2 ~ CODE10: 21607" 
* #152680 "MDC_VOL_AWAY_TIDAL_EXP_BTSD_AZC"
* #152680 ^definition = Volume | | Lung; Tidal; Expiratory Phase | airway; per assisted; synchronized assisted or controlled inflation
* #152680 ^designation[0].language = #de-AT 
* #152680 ^designation[0].value = "Expired Tidal Volume for assisted; synchronized assisted or controlled (aka mandatory) breaths" 
* #152680 ^property[0].code = #parent 
* #152680 ^property[0].valueCode = #_mdc 
* #152680 ^property[1].code = #hints 
* #152680 ^property[1].valueString = "PART: 2 ~ CODE10: 21608" 
* #152684 "MDC_VOL_AWAY_TIDAL_EXP_BTSD_PS_PER_IBW"
* #152684 ^definition = Volume per BodyMass | | Lung; Tidal; Expiratory Phase | airway; per unassisted or supported breath
* #152684 ^designation[0].language = #de-AT 
* #152684 ^designation[0].value = "Expired Tidal Volume for unassisted or supported (aka spontaneous) breaths per body mass" 
* #152684 ^property[0].code = #parent 
* #152684 ^property[0].valueCode = #_mdc 
* #152684 ^property[1].code = #hints 
* #152684 ^property[1].valueString = "PART: 2 ~ CODE10: 21612" 
* #152688 "MDC_VOL_MINUTE_AWAY_EXP_BTSD_PSAZC_PER_IBW"
* #152688 ^definition = Flow per BodyMass | OneMinute | Lung; Tidal; Expiratory Phase | airway; total for all breath and inflation types.
* #152688 ^designation[0].language = #de-AT 
* #152688 ^designation[0].value = "Expired minute volume per body mass" 
* #152688 ^property[0].code = #parent 
* #152688 ^property[0].valueCode = #_mdc 
* #152688 ^property[1].code = #hints 
* #152688 ^property[1].valueString = "PART: 2 ~ CODE10: 21616" 
* #152692 "MDC_VOL_MINUTE_AWAY_EXP_BTSD_PSAZC"
* #152692 ^definition = Flow | OneMinute | Lung; Tidal; Expiratory Phase | airway; total for all breath and inflation types
* #152692 ^designation[0].language = #de-AT 
* #152692 ^designation[0].value = "Expired minute volume (total for all breath types)" 
* #152692 ^property[0].code = #parent 
* #152692 ^property[0].valueCode = #_mdc 
* #152692 ^property[1].code = #hints 
* #152692 ^property[1].valueString = "PART: 2 ~ CODE10: 21620" 
* #152696 "MDC_VOL_MINUTE_AWAY_EXP_BTSD_PS"
* #152696 ^definition = Flow | OneMinute | Lung; Tidal; Expiratory Phase | airway; for unassisted or supported breaths
* #152696 ^designation[0].language = #de-AT 
* #152696 ^designation[0].value = "Expired Minute volume for unassisted or supported (aka spontaneous) breaths" 
* #152696 ^property[0].code = #parent 
* #152696 ^property[0].valueCode = #_mdc 
* #152696 ^property[1].code = #hints 
* #152696 ^property[1].valueString = "PART: 2 ~ CODE10: 21624" 
* #152700 "MDC_VOL_MINUTE_AWAY_EXP_BTSD_AZC"
* #152700 ^definition = Flow | OneMinute | Lung; Tidal; Expiratory Phase | airway; per assisted; synchronized assisted or controlled inflations
* #152700 ^designation[0].language = #de-AT 
* #152700 ^designation[0].value = "Expired Minute volume for assisted or controlled (aka mandatory) inflations" 
* #152700 ^property[0].code = #parent 
* #152700 ^property[0].valueCode = #_mdc 
* #152700 ^property[1].code = #hints 
* #152700 ^property[1].valueString = "PART: 2 ~ CODE10: 21628" 
* #152704 "MDC_VOL_MINUTE_AWAY_EXP_BTSD_PS_PER_IBW"
* #152704 ^definition = Flow per BodyMass | OneMinute | Lung; Tidal; Expiratory Phase | airway; for unassisted or supported breaths
* #152704 ^designation[0].language = #de-AT 
* #152704 ^designation[0].value = "Expired Minute volume for unassisted or supported (aka spontaneous) breaths per body mass" 
* #152704 ^property[0].code = #parent 
* #152704 ^property[0].valueCode = #_mdc 
* #152704 ^property[1].code = #hints 
* #152704 ^property[1].valueString = "PART: 2 ~ CODE10: 21632" 
* #152708 "MDC_VOL_AWAY"
* #152708 ^definition = Flow; integral | | Gas | Airway
* #152708 ^property[0].code = #parent 
* #152708 ^property[0].valueCode = #_mdc 
* #152708 ^property[1].code = #hints 
* #152708 ^property[1].valueString = "PART: 2 ~ CODE10: 21636" 
* #152712 "MDC_PRESS_AWAY_INSP_END"
* #152712 ^definition = Pressure | Inspiration; End | Gas | Airway
* #152712 ^designation[0].language = #de-AT 
* #152712 ^designation[0].value = "End inspiratory pressure" 
* #152712 ^property[0].code = #parent 
* #152712 ^property[0].valueCode = #_mdc 
* #152712 ^property[1].code = #hints 
* #152712 ^property[1].valueString = "PART: 2 ~ CODE10: 21640" 
* #152716 "MDC_VENT_PRESS_AWAY_BASELINE"
* #152716 ^definition = Pressure; baseline | | Gas | Ventilator; Airway
* #152716 ^designation[0].language = #de-AT 
* #152716 ^designation[0].value = "Baseline airway pressure" 
* #152716 ^property[0].code = #parent 
* #152716 ^property[0].valueCode = #_mdc 
* #152716 ^property[1].code = #hints 
* #152716 ^property[1].valueString = "PART: 2 ~ CODE10: 21644" 
* #152720 "MDC_VENT_PRESS_AWAY_DELTA"
* #152720 ^definition = Pressure; delta relative to baseline | | Gas | Ventilator; Airway
* #152720 ^designation[0].language = #de-AT 
* #152720 ^designation[0].value = "Inspiratory airway pressure relative to PEEP or BAP" 
* #152720 ^property[0].code = #parent 
* #152720 ^property[0].valueCode = #_mdc 
* #152720 ^property[1].code = #hints 
* #152720 ^property[1].valueString = "PART: 2 ~ CODE10: 21648" 
* #152724 "MDC_VENT_PRESS_AWAY_BACKUP"
* #152724 ^definition = Pressure | Inspiratory phase | Inflation; Backup | Ventilator; Airway
* #152724 ^designation[0].language = #de-AT 
* #152724 ^designation[0].value = "Backup inspiratory airway pressure" 
* #152724 ^property[0].code = #parent 
* #152724 ^property[0].valueCode = #_mdc 
* #152724 ^property[1].code = #hints 
* #152724 ^property[1].valueString = "PART: 2 ~ CODE10: 21652" 
* #152728 "MDC_VENT_PRESS_AWAY_DELTA_BACKUP"
* #152728 ^definition = Pressure; delta relative to baseline | Inspiratory phase | Inflation; Backup | Ventilator; Airway
* #152728 ^designation[0].language = #de-AT 
* #152728 ^designation[0].value = "Backup inspiratory airway pressure relative to PEEP or BAP" 
* #152728 ^property[0].code = #parent 
* #152728 ^property[0].valueCode = #_mdc 
* #152728 ^property[1].code = #hints 
* #152728 ^property[1].valueString = "PART: 2 ~ CODE10: 21656" 
* #152732 "MDC_VENT_PRESS_AWAY_SUPP"
* #152732 ^definition = Pressure | Inspiratory phase | Pressure support inflation | Ventilator; Airway
* #152732 ^designation[0].language = #de-AT 
* #152732 ^designation[0].value = "Pressure for support inflations" 
* #152732 ^property[0].code = #parent 
* #152732 ^property[0].valueCode = #_mdc 
* #152732 ^property[1].code = #hints 
* #152732 ^property[1].valueString = "PART: 2 ~ CODE10: 21660" 
* #152736 "MDC_VENT_PRESS_AWAY_DELTA_SUPP"
* #152736 ^definition = Pressure; delta relative to baseline | Inspiratory phase | Pressure support inflation | Ventilator; Airway
* #152736 ^designation[0].language = #de-AT 
* #152736 ^designation[0].value = "Delta pressure for support inflations" 
* #152736 ^property[0].code = #parent 
* #152736 ^property[0].valueCode = #_mdc 
* #152736 ^property[1].code = #hints 
* #152736 ^property[1].valueString = "PART: 2 ~ CODE10: 21664" 
* #152740 "MDC_VENT_PRESS_AWAY_INSP_PHIGH"
* #152740 ^definition = Pressure | High baseline pressure | APRV or Bi-Level modes | Ventilator; Airway
* #152740 ^designation[0].language = #de-AT 
* #152740 ^designation[0].value = "High pressure" 
* #152740 ^property[0].code = #parent 
* #152740 ^property[0].valueCode = #_mdc 
* #152740 ^property[1].code = #hints 
* #152740 ^property[1].valueString = "PART: 2 ~ CODE10: 21668" 
* #152744 "MDC_VENT_PRESS_AWAY_EXP_PLOW"
* #152744 ^definition = Pressure | Low baseline pressure | APRV or Bi-Level modes | Ventilator; Airway
* #152744 ^designation[0].language = #de-AT 
* #152744 ^designation[0].value = "Low pressure" 
* #152744 ^property[0].code = #parent 
* #152744 ^property[0].valueCode = #_mdc 
* #152744 ^property[1].code = #hints 
* #152744 ^property[1].valueString = "PART: 2 ~ CODE10: 21672" 
* #152748 "MDC_VENT_PRESS_AWAY_LIMIT"
* #152748 ^definition = Pressure; limit | without cycling | Gas | Ventilator; Airway
* #152748 ^designation[0].language = #de-AT 
* #152748 ^designation[0].value = "Pressure limit" 
* #152748 ^property[0].code = #parent 
* #152748 ^property[0].valueCode = #_mdc 
* #152748 ^property[1].code = #hints 
* #152748 ^property[1].valueString = "PART: 2 ~ CODE10: 21676" 
* #152752 "MDC_VENT_PRESS_AWAY_LIMIT_PMAX"
* #152752 ^definition = Pressure; maximum limit | with cycling | Gas | Ventilator; Airway
* #152752 ^designation[0].language = #de-AT 
* #152752 ^designation[0].value = "High pressure limit" 
* #152752 ^property[0].code = #parent 
* #152752 ^property[0].valueCode = #_mdc 
* #152752 ^property[1].code = #hints 
* #152752 ^property[1].valueString = "PART: 2 ~ CODE10: 21680" 
* #152756 "MDC_VENT_PRESS_AWAY_LIMIT_RELIEF"
* #152756 ^definition = Pressure; protective relief limit | | Gas | Ventilator; Airway
* #152756 ^designation[0].language = #de-AT 
* #152756 ^designation[0].value = "High pressure relief limit" 
* #152756 ^property[0].code = #parent 
* #152756 ^property[0].valueCode = #_mdc 
* #152756 ^property[1].code = #hints 
* #152756 ^property[1].valueString = "PART: 2 ~ CODE10: 21684" 
* #152760 "MDC_VENT_PRESS_AWAY_LIMIT_PMIN"
* #152760 ^definition = Pressure; minimum limit | | Gas | Ventilator; Airway
* #152760 ^designation[0].language = #de-AT 
* #152760 ^designation[0].value = "Minimum pressure" 
* #152760 ^property[0].code = #parent 
* #152760 ^property[0].valueCode = #_mdc 
* #152760 ^property[1].code = #hints 
* #152760 ^property[1].valueString = "PART: 2 ~ CODE10: 21688" 
* #152764 "MDC_VENT_PRESS_AWAY_DELTA_LIMIT_PMIN"
* #152764 ^definition = Pressure; delta relative to baseline; minimum limit | | Gas | Ventilator; Airway
* #152764 ^designation[0].language = #de-AT 
* #152764 ^designation[0].value = "Minimum delta pressure" 
* #152764 ^property[0].code = #parent 
* #152764 ^property[0].valueCode = #_mdc 
* #152764 ^property[1].code = #hints 
* #152764 ^property[1].valueString = "PART: 2 ~ CODE10: 21692" 
* #152768 "MDC_VENT_PRESS_AWAY_RISETIME_CTLD"
* #152768 ^definition = Duration; pressure; risetime | controlled inflations | Gas | Ventilator; Airway
* #152768 ^designation[0].language = #de-AT 
* #152768 ^designation[0].value = "Rise time" 
* #152768 ^property[0].code = #parent 
* #152768 ^property[0].valueCode = #_mdc 
* #152768 ^property[1].code = #hints 
* #152768 ^property[1].valueString = "PART: 2 ~ CODE10: 21696" 
* #152772 "MDC_VENT_PRESS_AWAY_RISETIME_SUPP"
* #152772 ^definition = Duration; pressure; risetime | support inflations | Gas | Ventilator; Airway
* #152772 ^designation[0].language = #de-AT 
* #152772 ^designation[0].value = "Pressure support rise time" 
* #152772 ^property[0].code = #parent 
* #152772 ^property[0].valueCode = #_mdc 
* #152772 ^property[1].code = #hints 
* #152772 ^property[1].valueString = "PART: 2 ~ CODE10: 21700" 
* #152776 "MDC_PRESS_RESP_PLAT_DYNAMIC"
* #152776 ^definition = Pressure | Plateau | Gas | Airway; Dynamic
* #152776 ^designation[0].language = #de-AT 
* #152776 ^designation[0].value = "Dynamic Plateau Pressure" 
* #152776 ^property[0].code = #parent 
* #152776 ^property[0].valueCode = #_mdc 
* #152776 ^property[1].code = #hints 
* #152776 ^property[1].valueString = "PART: 2 ~ CODE10: 21704" 
* #152780 "MDC_VENT_PRESS_OCCL_P100MS"
* #152780 ^definition = Pressure | Occlusion; 100 ms; Airway | Gas | Ventilator
* #152780 ^designation[0].language = #de-AT 
* #152780 ^designation[0].value = "Ventilation occlusion pressure; P0.1  (100 ms)" 
* #152780 ^property[0].code = #parent 
* #152780 ^property[0].valueCode = #_mdc 
* #152780 ^property[1].code = #hints 
* #152780 ^property[1].valueString = "PART: 2 ~ CODE10: 21708" 
* #152784 "MDC_VENT_PRESS_OCCL_NIF"
* #152784 ^definition = Pressure | Occlusion; NIF maneuver; Airway | Gas | Ventilator
* #152784 ^designation[0].language = #de-AT 
* #152784 ^designation[0].value = "Negative Inspiratory Force (NIF)" 
* #152784 ^property[0].code = #parent 
* #152784 ^property[0].valueCode = #_mdc 
* #152784 ^property[1].code = #hints 
* #152784 ^property[1].valueString = "PART: 2 ~ CODE10: 21712" 
* #152788 "MDC_PRESS_AWAY_END_EXP_POS_TOTAL"
* #152788 ^definition = Pressure | End-expiratory; Total | Gas | Lungs
* #152788 ^designation[0].language = #de-AT 
* #152788 ^designation[0].value = "Total PEEP" 
* #152788 ^property[0].code = #parent 
* #152788 ^property[0].valueCode = #_mdc 
* #152788 ^property[1].code = #hints 
* #152788 ^property[1].valueString = "PART: 2 ~ CODE10: 21716" 
* #152792 "MDC_PRESS_AWAY_END_EXP_POS_EXTRINSIC_DYNAMIC"
* #152792 ^definition = Pressure | End-expiratory; Extrinsic; Dynamic | Gas | Airway
* #152792 ^designation[0].language = #de-AT 
* #152792 ^designation[0].value = "Dynamic extrinsic PEEP" 
* #152792 ^property[0].code = #parent 
* #152792 ^property[0].valueCode = #_mdc 
* #152792 ^property[1].code = #hints 
* #152792 ^property[1].valueString = "PART: 2 ~ CODE10: 21720" 
* #152796 "MDC_PRESS_AWAY_END_EXP_POS_INTRINSIC_DYNAMIC"
* #152796 ^definition = Pressure | End-expiratory; Intrinsic; Dynamic | Gas | Airway
* #152796 ^designation[0].language = #de-AT 
* #152796 ^designation[0].value = "Dynamic intrinsic PEEP" 
* #152796 ^property[0].code = #parent 
* #152796 ^property[0].valueCode = #_mdc 
* #152796 ^property[1].code = #hints 
* #152796 ^property[1].valueString = "PART: 2 ~ CODE10: 21724" 
* #152800 "MDC_PRESS_AWAY_END_EXP_POS_TOTAL_DYNAMIC"
* #152800 ^definition = Pressure | End-expiratory; Total; Dynamic | Gas | Airway
* #152800 ^designation[0].language = #de-AT 
* #152800 ^designation[0].value = "Dynamic total PEEP" 
* #152800 ^property[0].code = #parent 
* #152800 ^property[0].valueCode = #_mdc 
* #152800 ^property[1].code = #hints 
* #152800 ^property[1].valueString = "PART: 2 ~ CODE10: 21728" 
* #152804 "MDC_VENT_FLOW_TRIG_SENS"
* #152804 ^definition = Flow | | TriggerSensitivity; start inspiration | Ventilator
* #152804 ^designation[0].language = #de-AT 
* #152804 ^designation[0].value = "Ventilator flow trigger sensitivity" 
* #152804 ^property[0].code = #parent 
* #152804 ^property[0].valueCode = #_mdc 
* #152804 ^property[1].code = #hints 
* #152804 ^property[1].valueString = "PART: 2 ~ CODE10: 21732" 
* #152808 "MDC_VENT_FLOW_THRESH_END_INSP"
* #152808 ^definition = Flow; ratio; percent | | TriggerThreshold; end inspiration | Ventilator
* #152808 ^designation[0].language = #de-AT 
* #152808 ^designation[0].value = "Ventilator end-inspiratory flow threshold" 
* #152808 ^property[0].code = #parent 
* #152808 ^property[0].valueCode = #_mdc 
* #152808 ^property[1].code = #hints 
* #152808 ^property[1].valueString = "PART: 2 ~ CODE10: 21736" 
* #152812 "MDC_RESP_EXPENDED_ENERGY"
* #152812 ^definition = Energy; expended | per unit time | Metabolic; Indirect Calorimetry based on gas exchange | Patient
* #152812 ^designation[0].language = #de-AT 
* #152812 ^designation[0].value = "Expended Energy" 
* #152812 ^property[0].code = #parent 
* #152812 ^property[0].valueCode = #_mdc 
* #152812 ^property[1].code = #hints 
* #152812 ^property[1].valueString = "PART: 2 ~ CODE10: 21740" 
* #152816 "MDC_FLOW_O2_CONSUMP_PER_IBW"
* #152816 ^definition = Flow per BodyMass | Consumption | O2; Gas | Breathing
* #152816 ^designation[0].language = #de-AT 
* #152816 ^designation[0].value = "O2 Consumption per body mass (typ kg)" 
* #152816 ^property[0].code = #parent 
* #152816 ^property[0].valueCode = #_mdc 
* #152816 ^property[1].code = #hints 
* #152816 ^property[1].valueString = "PART: 2 ~ CODE10: 21744" 
* #152820 "MDC_FLOW_O2_CONSUMP_PER_BSA"
* #152820 ^definition = Flow per BodySurfaceArea | Consumption | O2; Gas | Breathing
* #152820 ^designation[0].language = #de-AT 
* #152820 ^designation[0].value = "O2 Consumption per body surface area (typ m&lt;sup>2&lt;/sup>)" 
* #152820 ^property[0].code = #parent 
* #152820 ^property[0].valueCode = #_mdc 
* #152820 ^property[1].code = #hints 
* #152820 ^property[1].valueString = "PART: 2 ~ CODE10: 21748" 
* #152824 "MDC_FLOW_CO2_PROD_RESP_PER_IBW"
* #152824 ^definition = Flow per BodyMass | Production | CO2; Gas | Breathing
* #152824 ^designation[0].language = #de-AT 
* #152824 ^designation[0].value = "CO2 Production per body mass (typ kg)" 
* #152824 ^property[0].code = #parent 
* #152824 ^property[0].valueCode = #_mdc 
* #152824 ^property[1].code = #hints 
* #152824 ^property[1].valueString = "PART: 2 ~ CODE10: 21752" 
* #152828 "MDC_FLOW_CO2_PROD_RESP_PER_BSA"
* #152828 ^definition = Flow per BodySurfaceArea | Production | CO2; Gas | Breathing
* #152828 ^designation[0].language = #de-AT 
* #152828 ^designation[0].value = "CO2 Production per body surface area (typ m&lt;sup>2&lt;/sup>)" 
* #152828 ^property[0].code = #parent 
* #152828 ^property[0].valueCode = #_mdc 
* #152828 ^property[1].code = #hints 
* #152828 ^property[1].valueString = "PART: 2 ~ CODE10: 21756" 
* #152832 "MDC_PRESS_BAROMETRIC"
* #152832 ^definition = Barometric Pressure | | Atmospheric
* #152832 ^designation[0].language = #de-AT 
* #152832 ^designation[0].value = "Barometric Pressure" 
* #152832 ^property[0].code = #parent 
* #152832 ^property[0].valueCode = #_mdc 
* #152832 ^property[1].code = #hints 
* #152832 ^property[1].valueString = "PART: 2 ~ CODE10: 21760" 
* #152836 "MDC_PRESS_AIR_AMBIENT"
* #152836 ^definition = Barometric Pressure | | Ambient; immediate patient environment
* #152836 ^designation[0].language = #de-AT 
* #152836 ^designation[0].value = "Ambient Pressure" 
* #152836 ^property[0].code = #parent 
* #152836 ^property[0].valueCode = #_mdc 
* #152836 ^property[1].code = #hints 
* #152836 ^property[1].valueString = "PART: 2 ~ CODE10: 21764" 
* #152840 "MDC_BLD_SHUNT_FRACTION"
* #152840 ^definition = Fraction | Cardiac output not exposed to ventilated alveoli relative to total cardiac output | | Blood; CVS
* #152840 ^designation[0].language = #de-AT 
* #152840 ^designation[0].value = "(Estimated) Blood Shunt Fraction" 
* #152840 ^property[0].code = #parent 
* #152840 ^property[0].valueCode = #_mdc 
* #152840 ^property[1].code = #hints 
* #152840 ^property[1].valueString = "PART: 2 ~ CODE10: 21768" 
* #152844 "MDC_CONC_PO2_ART_VEN_DIFF"
* #152844 ^definition = Difference; Oxygen Content | Arterial - Venous | Blood | Blood; CVS
* #152844 ^designation[0].language = #de-AT 
* #152844 ^designation[0].value = "(Estimated) Arterial-Venous O2 Content Difference" 
* #152844 ^property[0].code = #parent 
* #152844 ^property[0].valueCode = #_mdc 
* #152844 ^property[1].code = #hints 
* #152844 ^property[1].valueString = "PART: 2 ~ CODE10: 21772" 
* #152848 "MDC_CONC_PCO2_ART_PULM"
* #152848 ^definition = Concentration | Partial Pressure; pCO2 | Blood; Pulmonary Artery | Fluid Chemistry
* #152848 ^designation[0].language = #de-AT 
* #152848 ^designation[0].value = "Pulmonary Arterial pCO2" 
* #152848 ^property[0].code = #parent 
* #152848 ^property[0].valueCode = #_mdc 
* #152848 ^property[1].code = #hints 
* #152848 ^property[1].valueString = "PART: 2 ~ CODE10: 21776" 
* #152852 "MDC_RES_VASC_PULM_INDEX"
* #152852 ^definition = Index; Resistance | PerSurfaceArea | Flow | PulmonaryBlood; CVS
* #152852 ^designation[0].language = #de-AT 
* #152852 ^designation[0].value = "Pulmonary Vascular Resistance Index" 
* #152852 ^property[0].code = #parent 
* #152852 ^property[0].valueCode = #_mdc 
* #152852 ^property[1].code = #hints 
* #152852 ^property[1].valueString = "PART: 2 ~ CODE10: 21780" 
* #152856 "MDC_O2_EXTRACTION_RATIO"
* #152856 ^definition = Ratio; Oxygen Quantity | ConsumedOxygen; DeliveredOxygen | Blood; CVS
* #152856 ^designation[0].language = #de-AT 
* #152856 ^designation[0].value = "(Estimated) Oxygen Extraction Ratio" 
* #152856 ^property[0].code = #parent 
* #152856 ^property[0].valueCode = #_mdc 
* #152856 ^property[1].code = #hints 
* #152856 ^property[1].valueString = "PART: 2 ~ CODE10: 21784" 
* #152860 "MDC_RESP_RAPID_SHALLOW_BREATHING_INDEX"
* #152860 ^definition = Index | Ratio (SpontBreathRate; TidalVolume) OneMinute | Gas | Breathing
* #152860 ^designation[0].language = #de-AT 
* #152860 ^designation[0].value = "Rapid Shallow Breathing Index" 
* #152860 ^property[0].code = #parent 
* #152860 ^property[0].valueCode = #_mdc 
* #152860 ^property[1].code = #hints 
* #152860 ^property[1].valueString = "PART: 2 ~ CODE10: 21788" 
* #152864 "MDC_CONC_MAC_SUM"
* #152864 ^definition = Concentration; sum of ratios | Minimum Alveolar Concentration | anesthetic gas
* #152864 ^designation[0].language = #de-AT 
* #152864 ^designation[0].value = "MAC sum" 
* #152864 ^property[0].code = #parent 
* #152864 ^property[0].valueCode = #_mdc 
* #152864 ^property[1].code = #hints 
* #152864 ^property[1].valueString = "PART: 2 ~ CODE10: 21792" 
* #152868 "MDC_CONC_MAC_SUM_AGE_CORR"
* #152868 ^definition = Concentration; sum of ratios | Minimum Alveolar Concentration; Age Corrected | anesthetic gas
* #152868 ^designation[0].language = #de-AT 
* #152868 ^designation[0].value = "MAC age-corrected sum" 
* #152868 ^property[0].code = #parent 
* #152868 ^property[0].valueCode = #_mdc 
* #152868 ^property[1].code = #hints 
* #152868 ^property[1].valueString = "PART: 2 ~ CODE10: 21796" 
* #152872 "MDC_CONC_MAC"
* #152872 ^definition = Ratio; Concentration | Relative to anesthetic needed to prevent movement in 50% patients | Volatile Anesthetic Agent | Alveolar
* #152872 ^designation[0].language = #de-AT 
* #152872 ^designation[0].value = "Mean Aveolar Concentration" 
* #152872 ^property[0].code = #parent 
* #152872 ^property[0].valueCode = #_mdc 
* #152872 ^property[1].code = #hints 
* #152872 ^property[1].valueString = "PART: 2 ~ CODE10: 21800" 
* #152876 "MDC_FLOW_AIR_FG"
* #152876 ^definition = Flow | | Air; Gas
* #152876 ^designation[0].language = #de-AT 
* #152876 ^designation[0].value = "Fresh air flow" 
* #152876 ^property[0].code = #parent 
* #152876 ^property[0].valueCode = #_mdc 
* #152876 ^property[1].code = #hints 
* #152876 ^property[1].valueString = "PART: 2 ~ CODE10: 21804" 
* #152880 "MDC_VOL_DELIV_AIR_CASE"
* #152880 ^definition = Volume | Case; Delivered | Air; Gas
* #152880 ^designation[0].language = #de-AT 
* #152880 ^designation[0].value = "Air delivered during a case" 
* #152880 ^property[0].code = #parent 
* #152880 ^property[0].valueCode = #_mdc 
* #152880 ^property[1].code = #hints 
* #152880 ^property[1].valueString = "PART: 2 ~ CODE10: 21808" 
* #152884 "MDC_VOL_DELIV_AIR_TOTAL"
* #152884 ^definition = Volume | Total; Delivered | Air; Gas
* #152884 ^designation[0].language = #de-AT 
* #152884 ^designation[0].value = "Total air delivered" 
* #152884 ^property[0].code = #parent 
* #152884 ^property[0].valueCode = #_mdc 
* #152884 ^property[1].code = #hints 
* #152884 ^property[1].valueString = "PART: 2 ~ CODE10: 21812" 
* #152888 "MDC_PRESS_AIR_SUPPLY"
* #152888 ^definition = Pressure | Supply; pipeline | Air; Gas
* #152888 ^designation[0].language = #de-AT 
* #152888 ^designation[0].value = "Air supply (pipeline) pressure" 
* #152888 ^property[0].code = #parent 
* #152888 ^property[0].valueCode = #_mdc 
* #152888 ^property[1].code = #hints 
* #152888 ^property[1].valueString = "PART: 2 ~ CODE10: 21816" 
* #152892 "MDC_PRESS_AIR_CYL"
* #152892 ^definition = Pressure | Cylinder | Air; Gas
* #152892 ^designation[0].language = #de-AT 
* #152892 ^designation[0].value = "Air cylinder pressure" 
* #152892 ^property[0].code = #parent 
* #152892 ^property[0].valueCode = #_mdc 
* #152892 ^property[1].code = #hints 
* #152892 ^property[1].valueString = "PART: 2 ~ CODE10: 21820" 
* #152896 "MDC_VOL_DELIV_DESFL_CASE"
* #152896 ^definition = Volume | Case; Delivered | Desflurane; Gas
* #152896 ^designation[0].language = #de-AT 
* #152896 ^designation[0].value = "Desflurane gas delivered during a case" 
* #152896 ^property[0].code = #parent 
* #152896 ^property[0].valueCode = #_mdc 
* #152896 ^property[1].code = #hints 
* #152896 ^property[1].valueString = "PART: 2 ~ CODE10: 21824" 
* #152900 "MDC_VOL_DELIV_DESFL_LIQUID_CASE"
* #152900 ^definition = Volume | Case; Delivered | Desflurane; Liquid
* #152900 ^designation[0].language = #de-AT 
* #152900 ^designation[0].value = "Desflurane liquid delivered during a case" 
* #152900 ^property[0].code = #parent 
* #152900 ^property[0].valueCode = #_mdc 
* #152900 ^property[1].code = #hints 
* #152900 ^property[1].valueString = "PART: 2 ~ CODE10: 21828" 
* #152904 "MDC_VOL_DELIV_DESFL_LIQUID_TOTAL"
* #152904 ^definition = Volume | Total; Delivered | Desflurane; Liquid
* #152904 ^designation[0].language = #de-AT 
* #152904 ^designation[0].value = "Total desflurane liquid delivered" 
* #152904 ^property[0].code = #parent 
* #152904 ^property[0].valueCode = #_mdc 
* #152904 ^property[1].code = #hints 
* #152904 ^property[1].valueString = "PART: 2 ~ CODE10: 21832" 
* #152908 "MDC_VOL_DELIV_DESFL_TOTAL"
* #152908 ^definition = Volume | Total; Delivered | Desflurane; Gas
* #152908 ^designation[0].language = #de-AT 
* #152908 ^designation[0].value = "Total desflurane gas delivered" 
* #152908 ^property[0].code = #parent 
* #152908 ^property[0].valueCode = #_mdc 
* #152908 ^property[1].code = #hints 
* #152908 ^property[1].valueString = "PART: 2 ~ CODE10: 21836" 
* #152912 "MDC_VOL_DELIV_ENFL_CASE"
* #152912 ^definition = Volume | Case; Delivered | Enflurane; Gas
* #152912 ^designation[0].language = #de-AT 
* #152912 ^designation[0].value = "Enflurane gas delivered during a case" 
* #152912 ^property[0].code = #parent 
* #152912 ^property[0].valueCode = #_mdc 
* #152912 ^property[1].code = #hints 
* #152912 ^property[1].valueString = "PART: 2 ~ CODE10: 21840" 
* #152916 "MDC_VOL_DELIV_ENFL_LIQUID_CASE"
* #152916 ^definition = Volume | Case; Delivered | Enflurane; Liquid
* #152916 ^designation[0].language = #de-AT 
* #152916 ^designation[0].value = "Enflurane liquid delivered during a case" 
* #152916 ^property[0].code = #parent 
* #152916 ^property[0].valueCode = #_mdc 
* #152916 ^property[1].code = #hints 
* #152916 ^property[1].valueString = "PART: 2 ~ CODE10: 21844" 
* #152920 "MDC_VOL_DELIV_ENFL_LIQUID_TOTAL"
* #152920 ^definition = Volume | Total; Delivered | Enflurane; Liquid
* #152920 ^designation[0].language = #de-AT 
* #152920 ^designation[0].value = "Total enflurane liquid delivered" 
* #152920 ^property[0].code = #parent 
* #152920 ^property[0].valueCode = #_mdc 
* #152920 ^property[1].code = #hints 
* #152920 ^property[1].valueString = "PART: 2 ~ CODE10: 21848" 
* #152924 "MDC_VOL_DELIV_ENFL_TOTAL"
* #152924 ^definition = Volume | Total; Delivered | Enflurane; Gas
* #152924 ^designation[0].language = #de-AT 
* #152924 ^designation[0].value = "Total enflurane gas delivered" 
* #152924 ^property[0].code = #parent 
* #152924 ^property[0].valueCode = #_mdc 
* #152924 ^property[1].code = #hints 
* #152924 ^property[1].valueString = "PART: 2 ~ CODE10: 21852" 
* #152928 "MDC_VOL_DELIV_HALOTH_CASE"
* #152928 ^definition = Volume | Case; Delivered | Halothane; Gas
* #152928 ^designation[0].language = #de-AT 
* #152928 ^designation[0].value = "Halothane gas delivered during a case" 
* #152928 ^property[0].code = #parent 
* #152928 ^property[0].valueCode = #_mdc 
* #152928 ^property[1].code = #hints 
* #152928 ^property[1].valueString = "PART: 2 ~ CODE10: 21856" 
* #152932 "MDC_VOL_DELIV_HALOTH_LIQUID_CASE"
* #152932 ^definition = Volume | Case; Delivered | Halothane; Liquid
* #152932 ^designation[0].language = #de-AT 
* #152932 ^designation[0].value = "Halothane liquid delivered during a case" 
* #152932 ^property[0].code = #parent 
* #152932 ^property[0].valueCode = #_mdc 
* #152932 ^property[1].code = #hints 
* #152932 ^property[1].valueString = "PART: 2 ~ CODE10: 21860" 
* #152936 "MDC_VOL_DELIV_HALOTH_LIQUID_TOTAL"
* #152936 ^definition = Volume | Total; Delivered | Halothane; Liquid
* #152936 ^designation[0].language = #de-AT 
* #152936 ^designation[0].value = "Total halothane liquid delivered" 
* #152936 ^property[0].code = #parent 
* #152936 ^property[0].valueCode = #_mdc 
* #152936 ^property[1].code = #hints 
* #152936 ^property[1].valueString = "PART: 2 ~ CODE10: 21864" 
* #152940 "MDC_VOL_DELIV_HALOTH_TOTAL"
* #152940 ^definition = Volume | Total; Delivered | Halothane; Gas
* #152940 ^designation[0].language = #de-AT 
* #152940 ^designation[0].value = "Total halothane gas delivered" 
* #152940 ^property[0].code = #parent 
* #152940 ^property[0].valueCode = #_mdc 
* #152940 ^property[1].code = #hints 
* #152940 ^property[1].valueString = "PART: 2 ~ CODE10: 21868" 
* #152944 "MDC_VOL_DELIV_ISOFL_CASE"
* #152944 ^definition = Volume | Case; Delivered | Isoflurane; Gas
* #152944 ^designation[0].language = #de-AT 
* #152944 ^designation[0].value = "Isoflurane gas delivered during a case" 
* #152944 ^property[0].code = #parent 
* #152944 ^property[0].valueCode = #_mdc 
* #152944 ^property[1].code = #hints 
* #152944 ^property[1].valueString = "PART: 2 ~ CODE10: 21872" 
* #152948 "MDC_VOL_DELIV_ISOFL_LIQUID_CASE"
* #152948 ^definition = Volume | Case; Delivered | Isoflurane; Liquid
* #152948 ^designation[0].language = #de-AT 
* #152948 ^designation[0].value = "Isoflurane liquid delivered during a case" 
* #152948 ^property[0].code = #parent 
* #152948 ^property[0].valueCode = #_mdc 
* #152948 ^property[1].code = #hints 
* #152948 ^property[1].valueString = "PART: 2 ~ CODE10: 21876" 
* #152952 "MDC_VOL_DELIV_ISOFL_LIQUID_TOTAL"
* #152952 ^definition = Volume | Total; Delivered | Isoflurane; Liquid
* #152952 ^designation[0].language = #de-AT 
* #152952 ^designation[0].value = "Total isoflurane liquid delivered" 
* #152952 ^property[0].code = #parent 
* #152952 ^property[0].valueCode = #_mdc 
* #152952 ^property[1].code = #hints 
* #152952 ^property[1].valueString = "PART: 2 ~ CODE10: 21880" 
* #152956 "MDC_VOL_DELIV_ISOFL_TOTAL"
* #152956 ^definition = Volume | Total; Delivered | Isoflurane; Gas
* #152956 ^designation[0].language = #de-AT 
* #152956 ^designation[0].value = "Total isoflurane gas delivered" 
* #152956 ^property[0].code = #parent 
* #152956 ^property[0].valueCode = #_mdc 
* #152956 ^property[1].code = #hints 
* #152956 ^property[1].valueString = "PART: 2 ~ CODE10: 21884" 
* #152960 "MDC_VOL_DELIV_N2O_CASE"
* #152960 ^definition = Volume | Case; Delivered | Nitrous Oxide; Gas
* #152960 ^designation[0].language = #de-AT 
* #152960 ^designation[0].value = "Nitrous oxide gas delivered during a case" 
* #152960 ^property[0].code = #parent 
* #152960 ^property[0].valueCode = #_mdc 
* #152960 ^property[1].code = #hints 
* #152960 ^property[1].valueString = "PART: 2 ~ CODE10: 21888" 
* #152964 "MDC_VOL_DELIV_N2O_TOTAL"
* #152964 ^definition = Volume | Total; Delivered | Nitrous Oxide; Gas
* #152964 ^designation[0].language = #de-AT 
* #152964 ^designation[0].value = "Total nitrous oxide gas delivered" 
* #152964 ^property[0].code = #parent 
* #152964 ^property[0].valueCode = #_mdc 
* #152964 ^property[1].code = #hints 
* #152964 ^property[1].valueString = "PART: 2 ~ CODE10: 21892" 
* #152968 "MDC_PRESS_N2O_SUPPLY"
* #152968 ^definition = Pressure | Supply; pipeline | N2O; Gas
* #152968 ^designation[0].language = #de-AT 
* #152968 ^designation[0].value = "N2O supply (pipeline) pressure" 
* #152968 ^property[0].code = #parent 
* #152968 ^property[0].valueCode = #_mdc 
* #152968 ^property[1].code = #hints 
* #152968 ^property[1].valueString = "PART: 2 ~ CODE10: 21896" 
* #152972 "MDC_PRESS_N2O_CYL"
* #152972 ^definition = Pressure | Cylinder | N2O; Gas
* #152972 ^designation[0].language = #de-AT 
* #152972 ^designation[0].value = "N2O cylinder pressure" 
* #152972 ^property[0].code = #parent 
* #152972 ^property[0].valueCode = #_mdc 
* #152972 ^property[1].code = #hints 
* #152972 ^property[1].valueString = "PART: 2 ~ CODE10: 21900" 
* #152976 "MDC_VOL_DELIV_SEVOFL_CASE"
* #152976 ^definition = Volume | Case; Delivered | Sevoflurane; Gas
* #152976 ^designation[0].language = #de-AT 
* #152976 ^designation[0].value = "Sevoflurane gas delivered during a case" 
* #152976 ^property[0].code = #parent 
* #152976 ^property[0].valueCode = #_mdc 
* #152976 ^property[1].code = #hints 
* #152976 ^property[1].valueString = "PART: 2 ~ CODE10: 21904" 
* #152980 "MDC_VOL_DELIV_SEVOFL_LIQUID_CASE"
* #152980 ^definition = Volume | Case; Delivered | Sevoflurane; Liquid
* #152980 ^designation[0].language = #de-AT 
* #152980 ^designation[0].value = "Sevoflurane liquid delivered during a case" 
* #152980 ^property[0].code = #parent 
* #152980 ^property[0].valueCode = #_mdc 
* #152980 ^property[1].code = #hints 
* #152980 ^property[1].valueString = "PART: 2 ~ CODE10: 21908" 
* #152984 "MDC_VOL_DELIV_SEVOFL_LIQUID_TOTAL"
* #152984 ^definition = Volume | Total; Delivered | Sevoflurane; Liquid
* #152984 ^designation[0].language = #de-AT 
* #152984 ^designation[0].value = "Total sevoflurane liquid delivered" 
* #152984 ^property[0].code = #parent 
* #152984 ^property[0].valueCode = #_mdc 
* #152984 ^property[1].code = #hints 
* #152984 ^property[1].valueString = "PART: 2 ~ CODE10: 21912" 
* #152988 "MDC_VOL_DELIV_SEVOFL_TOTAL"
* #152988 ^definition = Volume | Total; Delivered | Sevoflurane; Gas
* #152988 ^designation[0].language = #de-AT 
* #152988 ^designation[0].value = "Total sevoflurane gas delivered" 
* #152988 ^property[0].code = #parent 
* #152988 ^property[0].valueCode = #_mdc 
* #152988 ^property[1].code = #hints 
* #152988 ^property[1].valueString = "PART: 2 ~ CODE10: 21916" 
* #152992 "MDC_CONC_AWAY_AR"
* #152992 ^definition = Concentration | PartialPressure | Ar; Gas | Airway
* #152992 ^designation[0].language = #de-AT 
* #152992 ^designation[0].value = "Concentration (or partial pressure) of argon in airway gas" 
* #152992 ^property[0].code = #parent 
* #152992 ^property[0].valueCode = #_mdc 
* #152992 ^property[1].code = #hints 
* #152992 ^property[1].valueString = "PART: 2 ~ CODE10: 21920" 
* #152996 "MDC_CONC_AWAY_AR_ET"
* #152996 ^definition = Concentration | PartialPressure; Expiration | Ar; Gas | Airway
* #152996 ^designation[0].language = #de-AT 
* #152996 ^designation[0].value = "End tidal argon concentration (or partial pressure) in airway gas" 
* #152996 ^property[0].code = #parent 
* #152996 ^property[0].valueCode = #_mdc 
* #152996 ^property[1].code = #hints 
* #152996 ^property[1].valueString = "PART: 2 ~ CODE10: 21924" 
* #153000 "MDC_CONC_AWAY_AR_EXP"
* #153000 ^definition = Concentration | PartialPressure; Expiration | Ar; Gas | Airway
* #153000 ^designation[0].language = #de-AT 
* #153000 ^designation[0].value = "Expired argon concentration (or partial pressure) in airway gas" 
* #153000 ^property[0].code = #parent 
* #153000 ^property[0].valueCode = #_mdc 
* #153000 ^property[1].code = #hints 
* #153000 ^property[1].valueString = "PART: 2 ~ CODE10: 21928" 
* #153004 "MDC_CONC_AWAY_AR_INSP"
* #153004 ^definition = Concentration | PartialPressure; Inspiration | Ar; Gas | Airway
* #153004 ^designation[0].language = #de-AT 
* #153004 ^designation[0].value = "Inspiratory argon concentration (or partial pressure) in airway gas" 
* #153004 ^property[0].code = #parent 
* #153004 ^property[0].valueCode = #_mdc 
* #153004 ^property[1].code = #hints 
* #153004 ^property[1].valueString = "PART: 2 ~ CODE10: 21932" 
* #153008 "MDC_CONC_GASDLV_AR"
* #153008 ^definition = Concentration | Partial Pressure | Ar; Gas | Gas Delivery System
* #153008 ^designation[0].language = #de-AT 
* #153008 ^designation[0].value = "Concentration argon (gas delivery system)" 
* #153008 ^property[0].code = #parent 
* #153008 ^property[0].valueCode = #_mdc 
* #153008 ^property[1].code = #hints 
* #153008 ^property[1].valueString = "PART: 2 ~ CODE10: 21936" 
* #153012 "MDC_CONC_GASDLV_AR_EXP"
* #153012 ^definition = Concentration | Partial Pressure; Expiration | Ar; Gas | Gas Delivery System
* #153012 ^designation[0].language = #de-AT 
* #153012 ^designation[0].value = "Concentration airway argon expiratory (gas delivery system)" 
* #153012 ^property[0].code = #parent 
* #153012 ^property[0].valueCode = #_mdc 
* #153012 ^property[1].code = #hints 
* #153012 ^property[1].valueString = "PART: 2 ~ CODE10: 21940" 
* #153016 "MDC_CONC_GASDLV_AR_INSP"
* #153016 ^definition = Concentration | Partial Pressure; Inspiration | Ar; Gas | Gas Delivery System
* #153016 ^designation[0].language = #de-AT 
* #153016 ^designation[0].value = "Concentration airway argon inspiratory (gas delivery system)" 
* #153016 ^property[0].code = #parent 
* #153016 ^property[0].valueCode = #_mdc 
* #153016 ^property[1].code = #hints 
* #153016 ^property[1].valueString = "PART: 2 ~ CODE10: 21944" 
* #153020 "MDC_CONC_GASDLV_CO2"
* #153020 ^definition = Concentration | Partial Pressure | CO2; Gas | Gas Delivery System
* #153020 ^designation[0].language = #de-AT 
* #153020 ^designation[0].value = "Concentration carbon dioxide (gas delivery system)" 
* #153020 ^property[0].code = #parent 
* #153020 ^property[0].valueCode = #_mdc 
* #153020 ^property[1].code = #hints 
* #153020 ^property[1].valueString = "PART: 2 ~ CODE10: 21948" 
* #153024 "MDC_CONC_GASDLV_CO2_EXP"
* #153024 ^definition = Concentration | Partial Pressure; Expiration | CO2; Gas | Gas Delivery System
* #153024 ^designation[0].language = #de-AT 
* #153024 ^designation[0].value = "Concentration airway carbon dioxide expiratory (gas delivery system)" 
* #153024 ^property[0].code = #parent 
* #153024 ^property[0].valueCode = #_mdc 
* #153024 ^property[1].code = #hints 
* #153024 ^property[1].valueString = "PART: 2 ~ CODE10: 21952" 
* #153028 "MDC_CONC_GASDLV_CO2_INSP"
* #153028 ^definition = Concentration | Partial Pressure; Inspiration | CO2; Gas | Gas Delivery System
* #153028 ^designation[0].language = #de-AT 
* #153028 ^designation[0].value = "Concentration airway carbon dioxide inspiratory (gas delivery system)" 
* #153028 ^property[0].code = #parent 
* #153028 ^property[0].valueCode = #_mdc 
* #153028 ^property[1].code = #hints 
* #153028 ^property[1].valueString = "PART: 2 ~ CODE10: 21956" 
* #153032 "MDC_VOL_DELIV_CO2_CASE"
* #153032 ^definition = Volume | Case; Delivered | CO2; gas
* #153032 ^designation[0].language = #de-AT 
* #153032 ^designation[0].value = "Carbon dioxide gas delivered during a case" 
* #153032 ^property[0].code = #parent 
* #153032 ^property[0].valueCode = #_mdc 
* #153032 ^property[1].code = #hints 
* #153032 ^property[1].valueString = "PART: 2 ~ CODE10: 21960" 
* #153036 "MDC_VOL_DELIV_CO2_TOTAL"
* #153036 ^definition = Volume | Total; Delivered | CO2; Gas
* #153036 ^designation[0].language = #de-AT 
* #153036 ^designation[0].value = "Total carbon dioxide gas delivered" 
* #153036 ^property[0].code = #parent 
* #153036 ^property[0].valueCode = #_mdc 
* #153036 ^property[1].code = #hints 
* #153036 ^property[1].valueString = "PART: 2 ~ CODE10: 21964" 
* #153040 "MDC_CONC_AWAY_HE"
* #153040 ^definition = Concentration | PartialPressure | He; Gas | Airway
* #153040 ^designation[0].language = #de-AT 
* #153040 ^designation[0].value = "Concentration (or partial pressure) of helium in airway gas" 
* #153040 ^property[0].code = #parent 
* #153040 ^property[0].valueCode = #_mdc 
* #153040 ^property[1].code = #hints 
* #153040 ^property[1].valueString = "PART: 2 ~ CODE10: 21968" 
* #153044 "MDC_CONC_AWAY_HE_ET"
* #153044 ^definition = Concentration | PartialPressure; Expiration | He; Gas | Airway
* #153044 ^designation[0].language = #de-AT 
* #153044 ^designation[0].value = "End tidal helium concentration (or partial pressure) in airway gas" 
* #153044 ^property[0].code = #parent 
* #153044 ^property[0].valueCode = #_mdc 
* #153044 ^property[1].code = #hints 
* #153044 ^property[1].valueString = "PART: 2 ~ CODE10: 21972" 
* #153048 "MDC_CONC_AWAY_HE_EXP"
* #153048 ^definition = Concentration | PartialPressure; Expiration | He; Gas | Airway
* #153048 ^designation[0].language = #de-AT 
* #153048 ^designation[0].value = "Expired helium concentration (or partial pressure) in airway gas" 
* #153048 ^property[0].code = #parent 
* #153048 ^property[0].valueCode = #_mdc 
* #153048 ^property[1].code = #hints 
* #153048 ^property[1].valueString = "PART: 2 ~ CODE10: 21976" 
* #153052 "MDC_CONC_AWAY_HE_INSP"
* #153052 ^definition = Concentration | PartialPressure; Inspiration | He; Gas | Airway
* #153052 ^designation[0].language = #de-AT 
* #153052 ^designation[0].value = "Inspiratory helium concentration (or partial pressure) in airway gas" 
* #153052 ^property[0].code = #parent 
* #153052 ^property[0].valueCode = #_mdc 
* #153052 ^property[1].code = #hints 
* #153052 ^property[1].valueString = "PART: 2 ~ CODE10: 21980" 
* #153056 "MDC_CONC_GASDLV_HE"
* #153056 ^definition = Concentration | Partial Pressure | He; Gas | Gas Delivery System
* #153056 ^designation[0].language = #de-AT 
* #153056 ^designation[0].value = "Concentration helium (gas delivery system)" 
* #153056 ^property[0].code = #parent 
* #153056 ^property[0].valueCode = #_mdc 
* #153056 ^property[1].code = #hints 
* #153056 ^property[1].valueString = "PART: 2 ~ CODE10: 21984" 
* #153060 "MDC_CONC_GASDLV_HE_EXP"
* #153060 ^definition = Concentration | Partial Pressure; Expiration | He; Gas | Gas Delivery System
* #153060 ^designation[0].language = #de-AT 
* #153060 ^designation[0].value = "Concentration airway helium expiratory (gas delivery system)" 
* #153060 ^property[0].code = #parent 
* #153060 ^property[0].valueCode = #_mdc 
* #153060 ^property[1].code = #hints 
* #153060 ^property[1].valueString = "PART: 2 ~ CODE10: 21988" 
* #153064 "MDC_CONC_GASDLV_HE_INSP"
* #153064 ^definition = Concentration | Partial Pressure; Inspiration | He; Gas | Gas Delivery System
* #153064 ^designation[0].language = #de-AT 
* #153064 ^designation[0].value = "Concentration airway helium inspiratory (gas delivery system)" 
* #153064 ^property[0].code = #parent 
* #153064 ^property[0].valueCode = #_mdc 
* #153064 ^property[1].code = #hints 
* #153064 ^property[1].valueString = "PART: 2 ~ CODE10: 21992" 
* #153068 "MDC_VOL_DELIV_HE_CASE"
* #153068 ^definition = Volume | Case; Delivered | He; Gas
* #153068 ^designation[0].language = #de-AT 
* #153068 ^designation[0].value = "Helium gas delivered during a case" 
* #153068 ^property[0].code = #parent 
* #153068 ^property[0].valueCode = #_mdc 
* #153068 ^property[1].code = #hints 
* #153068 ^property[1].valueString = "PART: 2 ~ CODE10: 21996" 
* #153072 "MDC_VOL_DELIV_HE_TOTAL"
* #153072 ^definition = Volume | Total; Delivered | He; Gas
* #153072 ^designation[0].language = #de-AT 
* #153072 ^designation[0].value = "Total helium gas delivered" 
* #153072 ^property[0].code = #parent 
* #153072 ^property[0].valueCode = #_mdc 
* #153072 ^property[1].code = #hints 
* #153072 ^property[1].valueString = "PART: 2 ~ CODE10: 22000" 
* #153076 "MDC_CONC_AWAY_N2_EXP"
* #153076 ^definition = Concentration | PartialPressure; Expiration | N2; Gas | Airway
* #153076 ^designation[0].language = #de-AT 
* #153076 ^designation[0].value = "Expired nitrogen concentration (or partial pressure) in airway gas" 
* #153076 ^property[0].code = #parent 
* #153076 ^property[0].valueCode = #_mdc 
* #153076 ^property[1].code = #hints 
* #153076 ^property[1].valueString = "PART: 2 ~ CODE10: 22004" 
* #153080 "MDC_CONC_GASDLV_N2"
* #153080 ^definition = Concentration | Partial Pressure | N2; Gas | Gas Delivery System
* #153080 ^designation[0].language = #de-AT 
* #153080 ^designation[0].value = "Concentration nitrogen (gas delivery system)" 
* #153080 ^property[0].code = #parent 
* #153080 ^property[0].valueCode = #_mdc 
* #153080 ^property[1].code = #hints 
* #153080 ^property[1].valueString = "PART: 2 ~ CODE10: 22008" 
* #153084 "MDC_CONC_GASDLV_N2_EXP"
* #153084 ^definition = Concentration | Partial Pressure; Expiration | N2; Gas | Gas Delivery System
* #153084 ^designation[0].language = #de-AT 
* #153084 ^designation[0].value = "Concentration airway nitrogen expiratory (gas delivery system)" 
* #153084 ^property[0].code = #parent 
* #153084 ^property[0].valueCode = #_mdc 
* #153084 ^property[1].code = #hints 
* #153084 ^property[1].valueString = "PART: 2 ~ CODE10: 22012" 
* #153088 "MDC_CONC_GASDLV_N2_INSP"
* #153088 ^definition = Concentration | Partial Pressure; Inspiration | N2; Gas | Gas Delivery System
* #153088 ^designation[0].language = #de-AT 
* #153088 ^designation[0].value = "Concentration airway nitrogen inspiratory (gas delivery system)" 
* #153088 ^property[0].code = #parent 
* #153088 ^property[0].valueCode = #_mdc 
* #153088 ^property[1].code = #hints 
* #153088 ^property[1].valueString = "PART: 2 ~ CODE10: 22016" 
* #153092 "MDC_FLOW_N2O_FG"
* #153092 ^definition = Flow | | N2O; Gas
* #153092 ^designation[0].language = #de-AT 
* #153092 ^designation[0].value = "Fresh gas nitrous oxide flow" 
* #153092 ^property[0].code = #parent 
* #153092 ^property[0].valueCode = #_mdc 
* #153092 ^property[1].code = #hints 
* #153092 ^property[1].valueString = "PART: 2 ~ CODE10: 22020" 
* #153096 "MDC_CONC_AWAY_NO"
* #153096 ^definition = Concentration | PartialPressure | NO; Gas | Airway
* #153096 ^designation[0].language = #de-AT 
* #153096 ^designation[0].value = "Concentration (or partial pressure) of nitric oxide in airway gas" 
* #153096 ^property[0].code = #parent 
* #153096 ^property[0].valueCode = #_mdc 
* #153096 ^property[1].code = #hints 
* #153096 ^property[1].valueString = "PART: 2 ~ CODE10: 22024" 
* #153100 "MDC_CONC_AWAY_NO_ET"
* #153100 ^definition = Concentration | PartialPressure; Expiration | NO; Gas | Airway
* #153100 ^designation[0].language = #de-AT 
* #153100 ^designation[0].value = "End tidal nitric oxide concentration (or partial pressure) in airway gas" 
* #153100 ^property[0].code = #parent 
* #153100 ^property[0].valueCode = #_mdc 
* #153100 ^property[1].code = #hints 
* #153100 ^property[1].valueString = "PART: 2 ~ CODE10: 22028" 
* #153104 "MDC_CONC_AWAY_NO_EXP"
* #153104 ^definition = Concentration | PartialPressure; Expiration | NO; Gas | Airway
* #153104 ^designation[0].language = #de-AT 
* #153104 ^designation[0].value = "Expired nitric oxide concentration (or partial pressure) in airway gas" 
* #153104 ^property[0].code = #parent 
* #153104 ^property[0].valueCode = #_mdc 
* #153104 ^property[1].code = #hints 
* #153104 ^property[1].valueString = "PART: 2 ~ CODE10: 22032" 
* #153108 "MDC_CONC_AWAY_NO_INSP"
* #153108 ^definition = Concentration | PartialPressure; Inspiration | NO; Gas | Airway
* #153108 ^designation[0].language = #de-AT 
* #153108 ^designation[0].value = "Inspiratory nitric oxide concentration (or partial pressure) in airway gas" 
* #153108 ^property[0].code = #parent 
* #153108 ^property[0].valueCode = #_mdc 
* #153108 ^property[1].code = #hints 
* #153108 ^property[1].valueString = "PART: 2 ~ CODE10: 22036" 
* #153112 "MDC_CONC_GASDLV_NO"
* #153112 ^definition = Concentration | Partial Pressure | NO; Gas | Gas Delivery System
* #153112 ^designation[0].language = #de-AT 
* #153112 ^designation[0].value = "Concentration nitric oxide (gas delivery system)" 
* #153112 ^property[0].code = #parent 
* #153112 ^property[0].valueCode = #_mdc 
* #153112 ^property[1].code = #hints 
* #153112 ^property[1].valueString = "PART: 2 ~ CODE10: 22040" 
* #153116 "MDC_CONC_GASDLV_NO_EXP"
* #153116 ^definition = Concentration | Partial Pressure; Expiration | NO; Gas | Gas Delivery System
* #153116 ^designation[0].language = #de-AT 
* #153116 ^designation[0].value = "Concentration airway nitric oxide expiratory (gas delivery system)" 
* #153116 ^property[0].code = #parent 
* #153116 ^property[0].valueCode = #_mdc 
* #153116 ^property[1].code = #hints 
* #153116 ^property[1].valueString = "PART: 2 ~ CODE10: 22044" 
* #153120 "MDC_CONC_GASDLV_NO_INSP"
* #153120 ^definition = Concentration | Partial Pressure; Inspiration | NO; Gas | Gas Delivery System
* #153120 ^designation[0].language = #de-AT 
* #153120 ^designation[0].value = "Concentration airway nitric oxide inspiratory (gas delivery system)" 
* #153120 ^property[0].code = #parent 
* #153120 ^property[0].valueCode = #_mdc 
* #153120 ^property[1].code = #hints 
* #153120 ^property[1].valueString = "PART: 2 ~ CODE10: 22048" 
* #153124 "MDC_VOL_DELIV_NO_CASE"
* #153124 ^definition = Volume | Case; Delivered | NO; Gas
* #153124 ^designation[0].language = #de-AT 
* #153124 ^designation[0].value = "Nitric oxide gas delivered during a case" 
* #153124 ^property[0].code = #parent 
* #153124 ^property[0].valueCode = #_mdc 
* #153124 ^property[1].code = #hints 
* #153124 ^property[1].valueString = "PART: 2 ~ CODE10: 22052" 
* #153128 "MDC_VOL_DELIV_NO_TOTAL"
* #153128 ^definition = Volume | Total; Delivered | NO; Gas
* #153128 ^designation[0].language = #de-AT 
* #153128 ^designation[0].value = "Total nitric oxide gas delivered" 
* #153128 ^property[0].code = #parent 
* #153128 ^property[0].valueCode = #_mdc 
* #153128 ^property[1].code = #hints 
* #153128 ^property[1].valueString = "PART: 2 ~ CODE10: 22056" 
* #153132 "MDC_CONC_AWAY_O2_EXP"
* #153132 ^definition = Concentration | PartialPressure; Expiration | Oxygen; Gas | Airway
* #153132 ^designation[0].language = #de-AT 
* #153132 ^designation[0].value = "Expired oxygen concentration (or partial pressure) in airway gas" 
* #153132 ^property[0].code = #parent 
* #153132 ^property[0].valueCode = #_mdc 
* #153132 ^property[1].code = #hints 
* #153132 ^property[1].valueString = "PART: 2 ~ CODE10: 22060" 
* #153136 "MDC_CONC_GASDLV_O2"
* #153136 ^definition = Concentration | Partial Pressure | O2; Gas | Gas Delivery System or Circuit
* #153136 ^designation[0].language = #de-AT 
* #153136 ^designation[0].value = "Concentration oxygen (gas delivery system or circuit)" 
* #153136 ^property[0].code = #parent 
* #153136 ^property[0].valueCode = #_mdc 
* #153136 ^property[1].code = #hints 
* #153136 ^property[1].valueString = "PART: 2 ~ CODE10: 22064" 
* #153140 "MDC_CONC_GASDLV_O2_EXP"
* #153140 ^definition = Concentration | Partial Pressure; Expiration | O2; Gas | Gas Delivery System or Circuit
* #153140 ^designation[0].language = #de-AT 
* #153140 ^designation[0].value = "Concentration airway oxygen expiratory (gas delivery system or circuit)" 
* #153140 ^property[0].code = #parent 
* #153140 ^property[0].valueCode = #_mdc 
* #153140 ^property[1].code = #hints 
* #153140 ^property[1].valueString = "PART: 2 ~ CODE10: 22068" 
* #153144 "MDC_CONC_GASDLV_O2_INSP"
* #153144 ^definition = Concentration | Partial Pressure; Inspiration | O2; Gas | Gas Delivery System or Circuit
* #153144 ^designation[0].language = #de-AT 
* #153144 ^designation[0].value = "Concentration airway oxygen inspiratory (gas delivery system or circuit)" 
* #153144 ^property[0].code = #parent 
* #153144 ^property[0].valueCode = #_mdc 
* #153144 ^property[1].code = #hints 
* #153144 ^property[1].valueString = "PART: 2 ~ CODE10: 22072" 
* #153148 "MDC_VOL_DELIV_O2_CASE"
* #153148 ^definition = Volume | Case; Delivered | O2; Gas
* #153148 ^designation[0].language = #de-AT 
* #153148 ^designation[0].value = "Oxygen gas delivered during a case" 
* #153148 ^property[0].code = #parent 
* #153148 ^property[0].valueCode = #_mdc 
* #153148 ^property[1].code = #hints 
* #153148 ^property[1].valueString = "PART: 2 ~ CODE10: 22076" 
* #153152 "MDC_VOL_DELIV_O2_TOTAL"
* #153152 ^definition = Volume | Total; Delivered | O2; Gas
* #153152 ^designation[0].language = #de-AT 
* #153152 ^designation[0].value = "Total oxygen gas delivered" 
* #153152 ^property[0].code = #parent 
* #153152 ^property[0].valueCode = #_mdc 
* #153152 ^property[1].code = #hints 
* #153152 ^property[1].valueString = "PART: 2 ~ CODE10: 22080" 
* #153156 "MDC_FLOW_O2_FG"
* #153156 ^definition = Flow | | O2; Gas
* #153156 ^designation[0].language = #de-AT 
* #153156 ^designation[0].value = "Fresh gas oxygen flow" 
* #153156 ^property[0].code = #parent 
* #153156 ^property[0].valueCode = #_mdc 
* #153156 ^property[1].code = #hints 
* #153156 ^property[1].valueString = "PART: 2 ~ CODE10: 22084" 
* #153160 "MDC_PRESS_O2_SUPPLY"
* #153160 ^definition = Pressure | Supply; pipeline | O2; Gas
* #153160 ^designation[0].language = #de-AT 
* #153160 ^designation[0].value = "O2 supply (pipeline) pressure" 
* #153160 ^property[0].code = #parent 
* #153160 ^property[0].valueCode = #_mdc 
* #153160 ^property[1].code = #hints 
* #153160 ^property[1].valueString = "PART: 2 ~ CODE10: 22088" 
* #153164 "MDC_PRESS_O2_CYL"
* #153164 ^definition = Pressure | Cylinder | O2; Gas
* #153164 ^designation[0].language = #de-AT 
* #153164 ^designation[0].value = "O2 cylinder pressure" 
* #153164 ^property[0].code = #parent 
* #153164 ^property[0].valueCode = #_mdc 
* #153164 ^property[1].code = #hints 
* #153164 ^property[1].valueString = "PART: 2 ~ CODE10: 22092" 
* #153168 "MDC_PRESS_O2_CYL_2"
* #153168 ^definition = Pressure | Cylinder; 2nd | O2; Gas
* #153168 ^designation[0].language = #de-AT 
* #153168 ^designation[0].value = "O2 cylinder #2 pressure" 
* #153168 ^property[0].code = #parent 
* #153168 ^property[0].valueCode = #_mdc 
* #153168 ^property[1].code = #hints 
* #153168 ^property[1].valueString = "PART: 2 ~ CODE10: 22096" 
* #153172 "MDC_CONC_AWAY_XE"
* #153172 ^definition = Concentration | PartialPressure | Xe; Gas | Airway
* #153172 ^designation[0].language = #de-AT 
* #153172 ^designation[0].value = "Concentration (or partial pressure) of xenon in airway gas" 
* #153172 ^property[0].code = #parent 
* #153172 ^property[0].valueCode = #_mdc 
* #153172 ^property[1].code = #hints 
* #153172 ^property[1].valueString = "PART: 2 ~ CODE10: 22100" 
* #153176 "MDC_CONC_AWAY_XE_ET"
* #153176 ^definition = Concentration | PartialPressure; Expiration | Xe; Gas | Airway
* #153176 ^designation[0].language = #de-AT 
* #153176 ^designation[0].value = "End tidal xenon concentration (or partial pressure) in airway gas" 
* #153176 ^property[0].code = #parent 
* #153176 ^property[0].valueCode = #_mdc 
* #153176 ^property[1].code = #hints 
* #153176 ^property[1].valueString = "PART: 2 ~ CODE10: 22104" 
* #153180 "MDC_CONC_AWAY_XE_EXP"
* #153180 ^definition = Concentration | PartialPressure; Expiration | Xe; Gas | Airway
* #153180 ^designation[0].language = #de-AT 
* #153180 ^designation[0].value = "Expired xenon concentration (or partial pressure) in airway gas" 
* #153180 ^property[0].code = #parent 
* #153180 ^property[0].valueCode = #_mdc 
* #153180 ^property[1].code = #hints 
* #153180 ^property[1].valueString = "PART: 2 ~ CODE10: 22108" 
* #153184 "MDC_CONC_AWAY_XE_INSP"
* #153184 ^definition = Concentration | PartialPressure; Inspiration | Xe; Gas | Airway
* #153184 ^designation[0].language = #de-AT 
* #153184 ^designation[0].value = "Inspiratory xenon concentration (or partial pressure) in airway gas" 
* #153184 ^property[0].code = #parent 
* #153184 ^property[0].valueCode = #_mdc 
* #153184 ^property[1].code = #hints 
* #153184 ^property[1].valueString = "PART: 2 ~ CODE10: 22112" 
* #153188 "MDC_CONC_GASDLV_XE"
* #153188 ^definition = Concentration | Partial Pressure | Xe; Gas | Gas Delivery System
* #153188 ^designation[0].language = #de-AT 
* #153188 ^designation[0].value = "Concentration xenon (gas delivery system)" 
* #153188 ^property[0].code = #parent 
* #153188 ^property[0].valueCode = #_mdc 
* #153188 ^property[1].code = #hints 
* #153188 ^property[1].valueString = "PART: 2 ~ CODE10: 22116" 
* #153192 "MDC_CONC_GASDLV_XE_EXP"
* #153192 ^definition = Concentration | Partial Pressure; Expiration | Xe; Gas | Gas Delivery System
* #153192 ^designation[0].language = #de-AT 
* #153192 ^designation[0].value = "Concentration airway xenon expiratory (gas delivery system)" 
* #153192 ^property[0].code = #parent 
* #153192 ^property[0].valueCode = #_mdc 
* #153192 ^property[1].code = #hints 
* #153192 ^property[1].valueString = "PART: 2 ~ CODE10: 22120" 
* #153196 "MDC_CONC_GASDLV_XE_INSP"
* #153196 ^definition = Concentration | Partial Pressure; Inspiration | Xe; Gas | Gas Delivery System
* #153196 ^designation[0].language = #de-AT 
* #153196 ^designation[0].value = "Concentration airway xenon inspiratory (gas delivery system)" 
* #153196 ^property[0].code = #parent 
* #153196 ^property[0].valueCode = #_mdc 
* #153196 ^property[1].code = #hints 
* #153196 ^property[1].valueString = "PART: 2 ~ CODE10: 22124" 
* #153200 "MDC_VOL_DELIV_XE_CASE"
* #153200 ^definition = Volume | Case; Delivered | Xe; Gas
* #153200 ^designation[0].language = #de-AT 
* #153200 ^designation[0].value = "Xenon gas delivered during a case" 
* #153200 ^property[0].code = #parent 
* #153200 ^property[0].valueCode = #_mdc 
* #153200 ^property[1].code = #hints 
* #153200 ^property[1].valueString = "PART: 2 ~ CODE10: 22128" 
* #153204 "MDC_VOL_DELIV_XE_TOTAL"
* #153204 ^definition = Volume | Total; Delivered | Xe; Gas
* #153204 ^designation[0].language = #de-AT 
* #153204 ^designation[0].value = "Total xenon gas delivered" 
* #153204 ^property[0].code = #parent 
* #153204 ^property[0].valueCode = #_mdc 
* #153204 ^property[1].code = #hints 
* #153204 ^property[1].valueString = "PART: 2 ~ CODE10: 22132" 
* #153208 "MDC_VOL_AWAY_TIDAL_PER_IBW"
* #153208 ^definition = Volume gas per body mass | | Lung; Tidal | Breathing
* #153208 ^designation[0].language = #de-AT 
* #153208 ^designation[0].value = "Tidal volume per body mass" 
* #153208 ^property[0].code = #parent 
* #153208 ^property[0].valueCode = #_mdc 
* #153208 ^property[1].code = #hints 
* #153208 ^property[1].valueString = "PART: 2 ~ CODE10: 22136" 
* #153212 "MDC_VENT_VOL_TIDAL_TARGET_AUTO"
* #153212 ^definition = Volume | Target; Calculated | Lung; Tidal | Ventilator; all breath and inflation types
* #153212 ^designation[0].language = #de-AT 
* #153212 ^designation[0].value = "Target tidal volume" 
* #153212 ^property[0].code = #parent 
* #153212 ^property[0].valueCode = #_mdc 
* #153212 ^property[1].code = #hints 
* #153212 ^property[1].valueString = "PART: 2 ~ CODE10: 22140" 
* #153216 "MDC_VENT_VOL_TIDAL_BACKUP"
* #153216 ^definition = Volume | | Lung; Tidal; Inflation; Backup | Ventilator
* #153216 ^designation[0].language = #de-AT 
* #153216 ^designation[0].value = "Backup tidal volume" 
* #153216 ^property[0].code = #parent 
* #153216 ^property[0].valueCode = #_mdc 
* #153216 ^property[1].code = #hints 
* #153216 ^property[1].valueString = "PART: 2 ~ CODE10: 22144" 
* #153220 "MDC_VENT_VOL_TIDAL_INSP"
* #153220 ^definition = Volume | | Lung; Tidal; Inspiratory Phase | Ventilator
* #153220 ^designation[0].language = #de-AT 
* #153220 ^designation[0].value = "Inspired tidal volume (vent)" 
* #153220 ^property[0].code = #parent 
* #153220 ^property[0].valueCode = #_mdc 
* #153220 ^property[1].code = #hints 
* #153220 ^property[1].valueString = "PART: 2 ~ CODE10: 22148" 
* #153224 "MDC_VENT_VOL_LEAK_PERCENT"
* #153224 ^definition = Ratio | Leakage (inspired-expired; inspired) | Ventilation | Ventilator
* #153224 ^designation[0].language = #de-AT 
* #153224 ^designation[0].value = "Leakage volume; percent" 
* #153224 ^property[0].code = #parent 
* #153224 ^property[0].valueCode = #_mdc 
* #153224 ^property[1].code = #hints 
* #153224 ^property[1].valueString = "PART: 2 ~ CODE10: 22152" 
* #153228 "MDC_VOL_MINUTE_AWAY_IBW_REF"
* #153228 ^definition = Flow | MinuteVolume; reference value based on ideal body mass | Lung; Tidal | airway; total for all breath and inflation types
* #153228 ^designation[0].language = #de-AT 
* #153228 ^designation[0].value = "Reference minute volume calculated for the patient?s ideal body mass" 
* #153228 ^property[0].code = #parent 
* #153228 ^property[0].valueCode = #_mdc 
* #153228 ^property[1].code = #hints 
* #153228 ^property[1].valueString = "PART: 2 ~ CODE10: 22156" 
* #153236 "MDC_VOL_MINUTE_AWAY_IBW_TARGET"
* #153236 ^definition = Flow | MinuteVolume; target value | Lung; Tidal | airway; total for all breath and inflation types
* #153236 ^designation[0].language = #de-AT 
* #153236 ^designation[0].value = "Target Minute Volume" 
* #153236 ^property[0].code = #parent 
* #153236 ^property[0].valueCode = #_mdc 
* #153236 ^property[1].code = #hints 
* #153236 ^property[1].valueString = "PART: 2 ~ CODE10: 22164" 
* #153240 "MDC_VENT_VOL_MINUTE_LUNG_ALV"
* #153240 ^definition = Flow; OneMinute | | Lung; Alveolar | RespiratoryTract
* #153240 ^designation[0].language = #de-AT 
* #153240 ^designation[0].value = "Alveolar minute ventilation" 
* #153240 ^property[0].code = #parent 
* #153240 ^property[0].valueCode = #_mdc 
* #153240 ^property[1].code = #hints 
* #153240 ^property[1].valueString = "PART: 2 ~ CODE10: 22168" 
* #153244 "MDC_PRESS_GASTRIC"
* #153244 ^definition = Pressure | Gastric | Respiration | Breathing
* #153244 ^designation[0].language = #de-AT 
* #153244 ^designation[0].value = "Gastric pressure" 
* #153244 ^property[0].code = #parent 
* #153244 ^property[0].valueCode = #_mdc 
* #153244 ^property[1].code = #hints 
* #153244 ^property[1].valueString = "PART: 2 ~ CODE10: 22172" 
* #153248 "MDC_PRESS_TRANSPULM"
* #153248 ^definition = Pressure difference | Transpulmonary | Respiration | Breathing
* #153248 ^designation[0].language = #de-AT 
* #153248 ^designation[0].value = "Transpulmonary pressure (difference)" 
* #153248 ^property[0].code = #parent 
* #153248 ^property[0].valueCode = #_mdc 
* #153248 ^property[1].code = #hints 
* #153248 ^property[1].valueString = "PART: 2 ~ CODE10: 22176" 
* #153252 "MDC_PRESS_ETT_CUFF"
* #153252 ^definition = Pressure | Cuff; Endotracheal | Respiration | Breathing
* #153252 ^designation[0].language = #de-AT 
* #153252 ^designation[0].value = "Endotracheal cuff pressure" 
* #153252 ^property[0].code = #parent 
* #153252 ^property[0].valueCode = #_mdc 
* #153252 ^property[1].code = #hints 
* #153252 ^property[1].valueString = "PART: 2 ~ CODE10: 22180" 
* #153256 "MDC_PRESS_ETT_CUFF_END_EXH"
* #153256 ^definition = Pressure | Cuff; Endotracheal | End-exhalation | Breathing
* #153256 ^designation[0].language = #de-AT 
* #153256 ^designation[0].value = "Endotracheal cuff pressure at the end of exhalation" 
* #153256 ^property[0].code = #parent 
* #153256 ^property[0].valueCode = #_mdc 
* #153256 ^property[1].code = #hints 
* #153256 ^property[1].valueString = "PART: 2 ~ CODE10: 22184" 
* #153260 "MDC_VENT_PRESS_AWAY_RISETIME_CTLD_PERCENT"
* #153260 ^definition = Duration; ratio; pressure; risetime | controlled inflations | Gas | Ventilator; Airway
* #153260 ^designation[0].language = #de-AT 
* #153260 ^designation[0].value = "Rise time percent" 
* #153260 ^property[0].code = #parent 
* #153260 ^property[0].valueCode = #_mdc 
* #153260 ^property[1].code = #hints 
* #153260 ^property[1].valueString = "PART: 2 ~ CODE10: 22188" 
* #153264 "MDC_VENT_PRESS_AWAY_RISETIME_SUPP_PERCENT"
* #153264 ^definition = Duration; ratio; pressure; risetime | support inflations | Gas | Ventilator; Airway
* #153264 ^designation[0].language = #de-AT 
* #153264 ^designation[0].value = "Pressure support rise time percent" 
* #153264 ^property[0].code = #parent 
* #153264 ^property[0].valueCode = #_mdc 
* #153264 ^property[1].code = #hints 
* #153264 ^property[1].valueString = "PART: 2 ~ CODE10: 22192" 
* #153272 "MDC_VENT_FLOW_THRESH_END_INSP_PERCENT"
* #153272 ^definition = Flow; ratio; percent | | TriggerThreshold; end inspiration | Ventilator
* #153272 ^designation[0].language = #de-AT 
* #153272 ^designation[0].value = "% Ventilator end-inspiratory flow threshold" 
* #153272 ^property[0].code = #parent 
* #153272 ^property[0].valueCode = #_mdc 
* #153272 ^property[1].code = #hints 
* #153272 ^property[1].valueString = "PART: 2 ~ CODE10: 22200" 
* #153276 "MDC_WORK_OF_BREATHING_PATIENT"
* #153276 ^definition = Work of breathing; expended by patient (intrinsic) | per breath; volume or time  | Resistive and Elastic | Breathing
* #153276 ^designation[0].language = #de-AT 
* #153276 ^designation[0].value = "Work of breathing (intrinsic)" 
* #153276 ^property[0].code = #parent 
* #153276 ^property[0].valueCode = #_mdc 
* #153276 ^property[1].code = #hints 
* #153276 ^property[1].valueString = "PART: 2 ~ CODE10: 22204" 
* #153280 "MDC_WORK_OF_BREATHING_PATIENT_RESISTIVE"
* #153280 ^definition = Work of breathing; expended by patient | per breath; volume or time  | Resistive | Breathing
* #153280 ^designation[0].language = #de-AT 
* #153280 ^designation[0].value = "Work of breathing - resistive" 
* #153280 ^property[0].code = #parent 
* #153280 ^property[0].valueCode = #_mdc 
* #153280 ^property[1].code = #hints 
* #153280 ^property[1].valueString = "PART: 2 ~ CODE10: 22208" 
* #153284 "MDC_WORK_OF_BREATHING_PATIENT_ELASTIC"
* #153284 ^definition = Work of breathing; expended by patient | per breath; volume or time  | Elastic | Breathing
* #153284 ^designation[0].language = #de-AT 
* #153284 ^designation[0].value = "Work of breathing - elastic" 
* #153284 ^property[0].code = #parent 
* #153284 ^property[0].valueCode = #_mdc 
* #153284 ^property[1].code = #hints 
* #153284 ^property[1].valueString = "PART: 2 ~ CODE10: 22212" 
* #153288 "MDC_WORK_OF_BREATHING_VENTILATOR"
* #153288 ^definition = Work of breathing; expended by ventilator and applied to patient | per breath; volume or time  | Resistive and Elastic | Inflations and breathing
* #153288 ^designation[0].language = #de-AT 
* #153288 ^designation[0].value = "Work of breathing - ventilator" 
* #153288 ^property[0].code = #parent 
* #153288 ^property[0].valueCode = #_mdc 
* #153288 ^property[1].code = #hints 
* #153288 ^property[1].valueString = "PART: 2 ~ CODE10: 22216" 
* #153292 "MDC_WORK_OF_BREATHING_IMPOSED"
* #153292 ^definition = Work of breathing; expended by ventilator or patient and lost to breathing apparatus (extrinsic) | per breath; volume or time | Resistive
* #153292 ^designation[0].language = #de-AT 
* #153292 ^designation[0].value = "Imposed work of breathing (extrinsic)" 
* #153292 ^property[0].code = #parent 
* #153292 ^property[0].valueCode = #_mdc 
* #153292 ^property[1].code = #hints 
* #153292 ^property[1].valueString = "PART: 2 ~ CODE10: 22220" 
* #153296 "MDC_PRESS_TIME_PRODUCT_INSP"
* #153296 ^definition = Pressure · time product | per breath or per unit time for multiple breaths | Exertion by patient | Breathing; inspiratory phase
* #153296 ^designation[0].language = #de-AT 
* #153296 ^designation[0].value = "Inspiratory pressure-time product" 
* #153296 ^property[0].code = #parent 
* #153296 ^property[0].valueCode = #_mdc 
* #153296 ^property[1].code = #hints 
* #153296 ^property[1].valueString = "PART: 2 ~ CODE10: 22224" 
* #153300 "MDC_VENT_TUBE_COMPENSATION_LEVEL"
* #153300 ^definition = Level; reduction of patient work of breathing | Percent of full tube compensation | Endotracheal or tracheal | Breathing; patient
* #153300 ^designation[0].language = #de-AT 
* #153300 ^designation[0].value = "Tube compensation; Automatic tube compensation" 
* #153300 ^property[0].code = #parent 
* #153300 ^property[0].valueCode = #_mdc 
* #153300 ^property[1].code = #hints 
* #153300 ^property[1].valueString = "PART: 2 ~ CODE10: 22228" 
* #153304 "MDC_VENT_TUBE_TYPE"
* #153304 ^definition = Type; tube |  | Endotracheal or tracheal
* #153304 ^designation[0].language = #de-AT 
* #153304 ^designation[0].value = "Tube type" 
* #153304 ^property[0].code = #parent 
* #153304 ^property[0].valueCode = #_mdc 
* #153304 ^property[1].code = #hints 
* #153304 ^property[1].valueString = "PART: 2 ~ CODE10: 22232" 
* #153308 "MDC_VENT_TUBE_SIZE"
* #153308 ^definition = Size; tube |  | Endotracheal or tracheal | diameter; inside
* #153308 ^designation[0].language = #de-AT 
* #153308 ^designation[0].value = "Tube size" 
* #153308 ^property[0].code = #parent 
* #153308 ^property[0].valueCode = #_mdc 
* #153308 ^property[1].code = #hints 
* #153308 ^property[1].valueString = "PART: 2 ~ CODE10: 22236" 
* #153312 "MDC_RESP_TIME_CONSTANT_INSP"
* #153312 ^definition = Duration | TimeConstant | Inspiratory phase | Breath; Calculation
* #153312 ^designation[0].language = #de-AT 
* #153312 ^designation[0].value = "Inspiratory time constant" 
* #153312 ^property[0].code = #parent 
* #153312 ^property[0].valueCode = #_mdc 
* #153312 ^property[1].code = #hints 
* #153312 ^property[1].valueString = "PART: 2 ~ CODE10: 22240" 
* #153316 "MDC_RESP_TIME_CONSTANT_EXP"
* #153316 ^definition = Duration | TimeConstant | Expiratory phase | Breath; passive deflation
* #153316 ^designation[0].language = #de-AT 
* #153316 ^designation[0].value = "Expiratory time constant" 
* #153316 ^property[0].code = #parent 
* #153316 ^property[0].valueCode = #_mdc 
* #153316 ^property[1].code = #hints 
* #153316 ^property[1].valueString = "PART: 2 ~ CODE10: 22244" 
* #153320 "MDC_CONC_AWAY_CO2_EXP_PLATEAU_ALV_SLOPE"
* #153320 ^definition = Concentration | PartialPressure; CO2; Alveolar Plateau; slope with respect to expired gas volume | Gas | Airway
* #153320 ^designation[0].language = #de-AT 
* #153320 ^designation[0].value = "Slope of the alveolar plateau CO2 concentration with respect to expired volume" 
* #153320 ^property[0].code = #parent 
* #153320 ^property[0].valueCode = #_mdc 
* #153320 ^property[1].code = #hints 
* #153320 ^property[1].valueString = "PART: 2 ~ CODE10: 22248" 
* #153324 "MDC_VOL_AWAY_TIDAL_CO2_EXP"
* #153324 ^definition = Volume | Expiratory Phase | CO2; Gas | per breath (breath type not specified)
* #153324 ^designation[0].language = #de-AT 
* #153324 ^designation[0].value = "Expired CO2 volume" 
* #153324 ^property[0].code = #parent 
* #153324 ^property[0].valueCode = #_mdc 
* #153324 ^property[1].code = #hints 
* #153324 ^property[1].valueString = "PART: 2 ~ CODE10: 22252" 
* #153328 "MDC_VOL_AWAY_TIDAL_CO2_INSP"
* #153328 ^definition = Volume | Inspiratory Phase | CO2; Gas | per breath (breath type not specified)
* #153328 ^designation[0].language = #de-AT 
* #153328 ^designation[0].value = "Inspired CO2 volume" 
* #153328 ^property[0].code = #parent 
* #153328 ^property[0].valueCode = #_mdc 
* #153328 ^property[1].code = #hints 
* #153328 ^property[1].valueString = "PART: 2 ~ CODE10: 22256" 
* #153332 "MDC_VOL_AWAY_TIDAL_O2_EXP"
* #153332 ^definition = Volume | Expiratory Phase | O2; Gas | per breath (breath type not specified)
* #153332 ^designation[0].language = #de-AT 
* #153332 ^designation[0].value = "Expired O2 volume" 
* #153332 ^property[0].code = #parent 
* #153332 ^property[0].valueCode = #_mdc 
* #153332 ^property[1].code = #hints 
* #153332 ^property[1].valueString = "PART: 2 ~ CODE10: 22260" 
* #153336 "MDC_VOL_AWAY_TIDAL_O2_INSP"
* #153336 ^definition = Volume | Inspiratory Phase | O2; Gas | per breath (breath type not specified)
* #153336 ^designation[0].language = #de-AT 
* #153336 ^designation[0].value = "Inspired O2 volume" 
* #153336 ^property[0].code = #parent 
* #153336 ^property[0].valueCode = #_mdc 
* #153336 ^property[1].code = #hints 
* #153336 ^property[1].valueString = "PART: 2 ~ CODE10: 22264" 
* #153360 "MDC_NEB_CYCLES_REMAIN"
* #153360 ^definition = Count; cycles | Remaining | Nebulization | Nebulizer
* #153360 ^designation[0].language = #de-AT 
* #153360 ^designation[0].value = "Remaining number of cycles" 
* #153360 ^property[0].code = #parent 
* #153360 ^property[0].valueCode = #_mdc 
* #153360 ^property[1].code = #hints 
* #153360 ^property[1].valueString = "PART: 2 ~ CODE10: 22288" 
* #153364 "MDC_NEB_TIME_PD_REMAIN_CURR_CYCLE"
* #153364 ^definition = Duration | Delivery | Remaining nebulization time; current cycle | Nebulization | Nebulizer
* #153364 ^designation[0].language = #de-AT 
* #153364 ^designation[0].value = "Remaining nebulization time for current cycle" 
* #153364 ^property[0].code = #parent 
* #153364 ^property[0].valueCode = #_mdc 
* #153364 ^property[1].code = #hints 
* #153364 ^property[1].valueString = "PART: 2 ~ CODE10: 22292" 
* #153368 "MDC_NEB_TIME_PD_REMAIN_TOTAL"
* #153368 ^definition = Duration | Delivery | Remaining nebulization time; total | Nebulization | Nebulizer
* #153368 ^designation[0].language = #de-AT 
* #153368 ^designation[0].value = "Total remaining nebulization time for current set of delivery cycles" 
* #153368 ^property[0].code = #parent 
* #153368 ^property[0].valueCode = #_mdc 
* #153368 ^property[1].code = #hints 
* #153368 ^property[1].valueString = "PART: 2 ~ CODE10: 22296" 
* #153372 "MDC_NEB_TIME_PD_ELAPSED_CURR_CYCLE"
* #153372 ^definition = Duration | Elapsed nebulization time; current cycle | Nebulization | Nebulizer
* #153372 ^designation[0].language = #de-AT 
* #153372 ^designation[0].value = "Elapsed nebulization time for current cycle" 
* #153372 ^property[0].code = #parent 
* #153372 ^property[0].valueCode = #_mdc 
* #153372 ^property[1].code = #hints 
* #153372 ^property[1].valueString = "PART: 2 ~ CODE10: 22300" 
* #153376 "MDC_NEB_TIME_PD_ELAPSED_TOTAL"
* #153376 ^definition = Volume | Elapsed nebulization time; total | Nebulization | Nebulizer
* #153376 ^designation[0].language = #de-AT 
* #153376 ^designation[0].value = "Total elapsed nebulization time for current set of delivery cycles" 
* #153376 ^property[0].code = #parent 
* #153376 ^property[0].valueCode = #_mdc 
* #153376 ^property[1].code = #hints 
* #153376 ^property[1].valueString = "PART: 2 ~ CODE10: 22304" 
* #153380 "MDC_NEB_VOL_FLUID_REMAIN_CURR_CYCLE"
* #153380 ^definition = Volume | Remaining fluid to be nebulized; current cycle | Nebulization | Nebulizer
* #153380 ^designation[0].language = #de-AT 
* #153380 ^designation[0].value = "Remaining fluid to be nebulized for current cycle" 
* #153380 ^property[0].code = #parent 
* #153380 ^property[0].valueCode = #_mdc 
* #153380 ^property[1].code = #hints 
* #153380 ^property[1].valueString = "PART: 2 ~ CODE10: 22308" 
* #153384 "MDC_NEB_VOL_FLUID_REMAIN_TOTAL"
* #153384 ^definition = Volume | Remaining fluid to be nebulized; total | Nebulization | Nebulizer
* #153384 ^designation[0].language = #de-AT 
* #153384 ^designation[0].value = "Total remaining fluid to be nebulized for current set of delivery cycles" 
* #153384 ^property[0].code = #parent 
* #153384 ^property[0].valueCode = #_mdc 
* #153384 ^property[1].code = #hints 
* #153384 ^property[1].valueString = "PART: 2 ~ CODE10: 22312" 
* #153388 "MDC_NEB_VOL_FLUID_DELIV_CURR_CYCLE"
* #153388 ^definition = Volume | Fluid nebulized; current cycle | Nebulization | Nebulizer
* #153388 ^designation[0].language = #de-AT 
* #153388 ^designation[0].value = "Volume of fluid that has been nebulized for current cycle" 
* #153388 ^property[0].code = #parent 
* #153388 ^property[0].valueCode = #_mdc 
* #153388 ^property[1].code = #hints 
* #153388 ^property[1].valueString = "PART: 2 ~ CODE10: 22316" 
* #153392 "MDC_NEB_VOL_FLUID_DELIV_TOTAL"
* #153392 ^definition = Volume | Fluid nebulized; total | Nebulization | Nebulizer
* #153392 ^designation[0].language = #de-AT 
* #153392 ^designation[0].value = "Total volume of fluid that has been nebulized for current set of delivery cycles" 
* #153392 ^property[0].code = #parent 
* #153392 ^property[0].valueCode = #_mdc 
* #153392 ^property[1].code = #hints 
* #153392 ^property[1].valueString = "PART: 2 ~ CODE10: 22320" 
* #153396 "MDC_VENT_FLOW_NEBULIZER"
* #153396 ^definition = Flow; additional | Inspiratory airway | Gas | Nebulizer; pneumatic
* #153396 ^designation[0].language = #de-AT 
* #153396 ^designation[0].value = "Nebulizer Flow" 
* #153396 ^property[0].code = #parent 
* #153396 ^property[0].valueCode = #_mdc 
* #153396 ^property[1].code = #hints 
* #153396 ^property[1].valueString = "PART: 2 ~ CODE10: 22324" 
* #153604 "MDC_PRESS_CEREB_PERF"
* #153604 ^definition = Pressure | Difference(MeanArterial; MeanIntracranial) | Head; Intracranial | CNS
* #153604 ^designation[0].language = #de-AT 
* #153604 ^designation[0].value = "Cerebral perfusion pressure" 
* #153604 ^property[0].code = #parent 
* #153604 ^property[0].valueCode = #_mdc 
* #153604 ^property[1].code = #hints 
* #153604 ^property[1].valueString = "PART: 2 ~ CODE10: 22532" 
* #153608 "MDC_PRESS_INTRA_CRAN"
* #153608 ^definition = Pressure |  | Head; Intracranial | CNS
* #153608 ^designation[0].language = #de-AT 
* #153608 ^designation[0].value = "Intracranial pressure" 
* #153608 ^property[0].code = #parent 
* #153608 ^property[0].valueCode = #_mdc 
* #153608 ^property[1].code = #hints 
* #153608 ^property[1].valueString = "PART: 2 ~ CODE10: 22536" 
* #153609 "MDC_PRESS_INTRA_CRAN_SYS"
* #153609 ^definition = Pressure | Systolic | Head; Intracranial | CNS
* #153609 ^designation[0].language = #de-AT 
* #153609 ^designation[0].value = "Systolic intracranial pressure" 
* #153609 ^property[0].code = #parent 
* #153609 ^property[0].valueCode = #_mdc 
* #153609 ^property[1].code = #hints 
* #153609 ^property[1].valueString = "PART: 2 ~ CODE10: 22537" 
* #153610 "MDC_PRESS_INTRA_CRAN_DIA"
* #153610 ^definition = Pressure | Diastolic | Head; Intracranial | CNS
* #153610 ^designation[0].language = #de-AT 
* #153610 ^designation[0].value = "Diastolic intracranial pressure" 
* #153610 ^property[0].code = #parent 
* #153610 ^property[0].valueCode = #_mdc 
* #153610 ^property[1].code = #hints 
* #153610 ^property[1].valueString = "PART: 2 ~ CODE10: 22538" 
* #153611 "MDC_PRESS_INTRA_CRAN_MEAN"
* #153611 ^definition = Pressure | Mean | Head; Intracranial | CNS
* #153611 ^designation[0].language = #de-AT 
* #153611 ^designation[0].value = "Mean intracranial pressure" 
* #153611 ^property[0].code = #parent 
* #153611 ^property[0].valueCode = #_mdc 
* #153611 ^property[1].code = #hints 
* #153611 ^property[1].valueString = "PART: 2 ~ CODE10: 22539" 
* #153636 "MDC_EEG_SIGNAL_QUALITY_INDEX"
* #153636 ^definition = Index | SignalQuality; BIS | Cortex; EEG | CNS
* #153636 ^designation[0].language = #de-AT 
* #153636 ^designation[0].value = "Signal Quality Index" 
* #153636 ^property[0].code = #parent 
* #153636 ^property[0].valueCode = #_mdc 
* #153636 ^property[1].code = #hints 
* #153636 ^property[1].valueString = "PART: 2 ~ CODE10: 22564" 
* #153640 "MDC_EMG_ELEC_POTL_MUSCL"
* #153640 ^definition = Power | Electromyographic | Cortex; EEG | CNS
* #153640 ^designation[0].language = #de-AT 
* #153640 ^designation[0].value = "Electromyography" 
* #153640 ^property[0].code = #parent 
* #153640 ^property[0].valueCode = #_mdc 
* #153640 ^property[1].code = #hints 
* #153640 ^property[1].valueString = "PART: 2 ~ CODE10: 22568" 
* #153644 "MDC_EEG_BISPECTRAL_INDEX"
* #153644 ^definition = Index | Bispectral Index | Cortex; EEG | CNS
* #153644 ^designation[0].language = #de-AT 
* #153644 ^designation[0].value = "Bispectral Index" 
* #153644 ^property[0].code = #parent 
* #153644 ^property[0].valueCode = #_mdc 
* #153644 ^property[1].code = #hints 
* #153644 ^property[1].valueString = "PART: 2 ~ CODE10: 22572" 
* #153648 "MDC_EEG_ENTROPY_RESPONSE"
* #153648 ^definition = Index | Response Entropy | Cortex; EEG | CNS
* #153648 ^designation[0].language = #de-AT 
* #153648 ^designation[0].value = "Response Entropy" 
* #153648 ^property[0].code = #parent 
* #153648 ^property[0].valueCode = #_mdc 
* #153648 ^property[1].code = #hints 
* #153648 ^property[1].valueString = "PART: 2 ~ CODE10: 22576" 
* #153652 "MDC_EEG_ENTROPY_STATE"
* #153652 ^definition = Index | State Entropy | Cortex; EEG | CNS
* #153652 ^designation[0].language = #de-AT 
* #153652 ^designation[0].value = "State Entropy" 
* #153652 ^property[0].code = #parent 
* #153652 ^property[0].valueCode = #_mdc 
* #153652 ^property[1].code = #hints 
* #153652 ^property[1].valueString = "PART: 2 ~ CODE10: 22580" 
* #153656 "MDC_EEG_SNAP_INDEX"
* #153656 ^definition = Index | SNAP Index | Cortex; EEG | CNS
* #153656 ^designation[0].language = #de-AT 
* #153656 ^designation[0].value = "SNAP Index" 
* #153656 ^property[0].code = #parent 
* #153656 ^property[0].valueCode = #_mdc 
* #153656 ^property[1].code = #hints 
* #153656 ^property[1].valueString = "PART: 2 ~ CODE10: 22584" 
* #153660 "MDC_EEG_PATIENT_STATE_INDEX"
* #153660 ^definition = Index | Patient State Index | Cortex; EEG | CNS
* #153660 ^designation[0].language = #de-AT 
* #153660 ^designation[0].value = "Patient State Index" 
* #153660 ^property[0].code = #parent 
* #153660 ^property[0].valueCode = #_mdc 
* #153660 ^property[1].code = #hints 
* #153660 ^property[1].valueString = "PART: 2 ~ CODE10: 22588" 
* #153728 "MDC_SCORE_GLAS_COMA"
* #153728 ^definition = Score | GlasgowComaScore | CNS State | CNS
* #153728 ^designation[0].language = #de-AT 
* #153728 ^designation[0].value = "Glasgow coma score" 
* #153728 ^property[0].code = #parent 
* #153728 ^property[0].valueCode = #_mdc 
* #153728 ^property[1].code = #hints 
* #153728 ^property[1].valueString = "PART: 2 ~ CODE10: 22656" 
* #153730 "MDC_SCORE_EYE_SUBSC_GLAS_COMA"
* #153730 ^definition = Score | GlasgowComaScore; SubscoreEye | CNS State | CNS
* #153730 ^designation[0].language = #de-AT 
* #153730 ^designation[0].value = "Glasgow coma score; eye" 
* #153730 ^property[0].code = #parent 
* #153730 ^property[0].valueCode = #_mdc 
* #153730 ^property[1].code = #hints 
* #153730 ^property[1].valueString = "PART: 2 ~ CODE10: 22658" 
* #153731 "MDC_SCORE_MOTOR_SUBSC_GLAS_COMA"
* #153731 ^definition = Score | GlasgowComaScore; SubscoreMotoric | CNS State | CNS
* #153731 ^designation[0].language = #de-AT 
* #153731 ^designation[0].value = "Glasgow coma score; motoric" 
* #153731 ^property[0].code = #parent 
* #153731 ^property[0].valueCode = #_mdc 
* #153731 ^property[1].code = #hints 
* #153731 ^property[1].valueString = "PART: 2 ~ CODE10: 22659" 
* #153732 "MDC_SCORE_SUBSC_VERBAL_GLAS_COMA"
* #153732 ^definition = Score | GlasgowComaScore; SubscoreVerbal | CNS State | CNS
* #153732 ^designation[0].language = #de-AT 
* #153732 ^designation[0].value = "Glasgow coma score; verbal" 
* #153732 ^property[0].code = #parent 
* #153732 ^property[0].valueCode = #_mdc 
* #153732 ^property[1].code = #hints 
* #153732 ^property[1].valueString = "PART: 2 ~ CODE10: 22660" 
* #153856 "MDC_CIRCUM_HEAD"
* #153856 ^definition = Circumference |  | Head | Body; CNS
* #153856 ^designation[0].language = #de-AT 
* #153856 ^designation[0].value = "Circum head" 
* #153856 ^property[0].code = #parent 
* #153856 ^property[0].valueCode = #_mdc 
* #153856 ^property[1].code = #hints 
* #153856 ^property[1].valueString = "PART: 2 ~ CODE10: 22784" 
* #153892 "MDC_TIME_PD_PUPIL_REACT_LEFT"
* #153892 ^definition = Duration | ReactionTime | Pupil; LeftEye | CNS
* #153892 ^property[0].code = #parent 
* #153892 ^property[0].valueCode = #_mdc 
* #153892 ^property[1].code = #hints 
* #153892 ^property[1].valueString = "PART: 2 ~ CODE10: 22820" 
* #153896 "MDC_TIME_PD_PUPIL_REACT_RIGHT"
* #153896 ^definition = Duration | ReactionTime | Pupil; RightEye | CNS
* #153896 ^property[0].code = #parent 
* #153896 ^property[0].valueCode = #_mdc 
* #153896 ^property[1].code = #hints 
* #153896 ^property[1].valueString = "PART: 2 ~ CODE10: 22824" 
* #153900 "MDC_EEG_ELEC_POTL_CRTX"
* #153900 ^definition = ElectricalPotential |  | Cortex | CNS
* #153900 ^designation[0].language = #de-AT 
* #153900 ^designation[0].value = "Electroencephalogram" 
* #153900 ^property[0].code = #parent 
* #153900 ^property[0].valueCode = #_mdc 
* #153900 ^property[1].code = #hints 
* #153900 ^property[1].valueString = "PART: 2 ~ CODE10: 22828" 
* #153980 "MDC_EEG_FREQ_PWR_SPEC_CRTX_DOM_MEAN"
* #153980 ^definition = Frequency | EEG; PowerSpectrum; MeanDominantFrequency | Cortex | CNS
* #153980 ^designation[0].language = #de-AT 
* #153980 ^designation[0].value = "Mean dominant frequency of electroencephalogram" 
* #153980 ^property[0].code = #parent 
* #153980 ^property[0].valueCode = #_mdc 
* #153980 ^property[1].code = #hints 
* #153980 ^property[1].valueString = "PART: 2 ~ CODE10: 22908" 
* #153984 "MDC_EEG_FREQ_PWR_SPEC_CRTX_MEDIAN"
* #153984 ^definition = Frequency | EEG; PowerSpectrum; MedianPowerFrequency | Cortex | CNS
* #153984 ^designation[0].language = #de-AT 
* #153984 ^designation[0].value = "Median power frequency of electroencephalogram" 
* #153984 ^property[0].code = #parent 
* #153984 ^property[0].valueCode = #_mdc 
* #153984 ^property[1].code = #hints 
* #153984 ^property[1].valueString = "PART: 2 ~ CODE10: 22912" 
* #153988 "MDC_EEG_FREQ_PWR_SPEC_CRTX_PEAK"
* #153988 ^definition = Frequency | EEG; PowerSpectrum; PeakPowerFrequency | Cortex | CNS
* #153988 ^designation[0].language = #de-AT 
* #153988 ^designation[0].value = "Peak power frequency of electroencephalogram" 
* #153988 ^property[0].code = #parent 
* #153988 ^property[0].valueCode = #_mdc 
* #153988 ^property[1].code = #hints 
* #153988 ^property[1].valueString = "PART: 2 ~ CODE10: 22916" 
* #153992 "MDC_EEG_FREQ_PWR_SPEC_CRTX_SPECTRAL_EDGE"
* #153992 ^definition = Frequency | EEG; PowerSpectrum; SpectralEdgeFrequency | Cortex | CNS
* #153992 ^designation[0].language = #de-AT 
* #153992 ^designation[0].value = "Spectral edge frequency of electroencephalogram" 
* #153992 ^property[0].code = #parent 
* #153992 ^property[0].valueCode = #_mdc 
* #153992 ^property[1].code = #hints 
* #153992 ^property[1].valueString = "PART: 2 ~ CODE10: 22920" 
* #154028 "MDC_EEG_NUM_SPK"
* #154028 ^definition = Number | EEG | Spikes | Neurology; CNS
* #154028 ^designation[0].language = #de-AT 
* #154028 ^designation[0].value = "Spikes" 
* #154028 ^property[0].code = #parent 
* #154028 ^property[0].valueCode = #_mdc 
* #154028 ^property[1].code = #hints 
* #154028 ^property[1].valueString = "PART: 2 ~ CODE10: 22956" 
* #154032 "MDC_EEG_NUM_SEIZ"
* #154032 ^definition = Number | EEG | Seizures | Neurology; CNS
* #154032 ^designation[0].language = #de-AT 
* #154032 ^designation[0].value = "Seizures" 
* #154032 ^property[0].code = #parent 
* #154032 ^property[0].valueCode = #_mdc 
* #154032 ^property[1].code = #hints 
* #154032 ^property[1].valueString = "PART: 2 ~ CODE10: 22960" 
* #154036 "MDC_EEG_PWR_SPEC_CSA"
* #154036 ^definition = Power | EEG; PowerSpectrum | Cortex | CNS
* #154036 ^designation[0].language = #de-AT 
* #154036 ^designation[0].value = "Compressed spectral array of electroencephalogram" 
* #154036 ^property[0].code = #parent 
* #154036 ^property[0].valueCode = #_mdc 
* #154036 ^property[1].code = #hints 
* #154036 ^property[1].valueString = "PART: 2 ~ CODE10: 22964" 
* #154040 "MDC_EEG_PWR_SPEC_TOT"
* #154040 ^definition = Power | EEG; PowerSpectrum; TotalPower | Cortex | CNS
* #154040 ^designation[0].language = #de-AT 
* #154040 ^designation[0].value = "Total power of electroencephalogram" 
* #154040 ^property[0].code = #parent 
* #154040 ^property[0].valueCode = #_mdc 
* #154040 ^property[1].code = #hints 
* #154040 ^property[1].valueString = "PART: 2 ~ CODE10: 22968" 
* #154068 "MDC_EEG_PWR_SPEC_ALPHA_REL"
* #154068 ^definition = Power | EEG; PowerSpectrum; AlphaBand; RelativePower | Cortex | CNS
* #154068 ^designation[0].language = #de-AT 
* #154068 ^designation[0].value = "Relative power of alpha band of electroencephalogram" 
* #154068 ^property[0].code = #parent 
* #154068 ^property[0].valueCode = #_mdc 
* #154068 ^property[1].code = #hints 
* #154068 ^property[1].valueString = "PART: 2 ~ CODE10: 22996" 
* #154072 "MDC_EEG_PWR_SPEC_BETA_REL"
* #154072 ^definition = Power | EEG; PowerSpectrum; BetaBand; RelativePower | Cortex | CNS
* #154072 ^designation[0].language = #de-AT 
* #154072 ^designation[0].value = "Relative power of beta band of electroencephalogram" 
* #154072 ^property[0].code = #parent 
* #154072 ^property[0].valueCode = #_mdc 
* #154072 ^property[1].code = #hints 
* #154072 ^property[1].valueString = "PART: 2 ~ CODE10: 23000" 
* #154076 "MDC_EEG_PWR_SPEC_DELTA_REL"
* #154076 ^definition = Power | EEG; PowerSpectrum; DeltaBand; RelativePower | Cortex | CNS
* #154076 ^designation[0].language = #de-AT 
* #154076 ^designation[0].value = "Relative power of delta band of electroencephalogram" 
* #154076 ^property[0].code = #parent 
* #154076 ^property[0].valueCode = #_mdc 
* #154076 ^property[1].code = #hints 
* #154076 ^property[1].valueString = "PART: 2 ~ CODE10: 23004" 
* #154080 "MDC_EEG_PWR_SPEC_THETA_REL"
* #154080 ^definition = Power | EEG; PowerSpectrum; ThetaBand; RelativePower | Cortex | CNS
* #154080 ^designation[0].language = #de-AT 
* #154080 ^designation[0].value = "Relative power of theta band of electroencephalogram" 
* #154080 ^property[0].code = #parent 
* #154080 ^property[0].valueCode = #_mdc 
* #154080 ^property[1].code = #hints 
* #154080 ^property[1].valueString = "PART: 2 ~ CODE10: 23008" 
* #154977 "MDC_EEG_PAROX_CRTX_SPK_CNT"
* #154977 ^definition = Count; Pattern | ParoxysmalActivity; Spike | Cortex; EEG | CNS
* #154977 ^designation[0].language = #de-AT 
* #154977 ^designation[0].value = "Spike Count" 
* #154977 ^property[0].code = #parent 
* #154977 ^property[0].valueCode = #_mdc 
* #154977 ^property[1].code = #hints 
* #154977 ^property[1].valueString = "PART: 2 ~ CODE10: 23905" 
* #155024 "MDC_EEG_PAROX_CRTX_BURST_SUPPRN"
* #155024 ^definition = Pattern | ParoxismalActivity; BurstSuppression | Cortex; EEG | CNS
* #155024 ^designation[0].language = #de-AT 
* #155024 ^designation[0].value = "Burst suppression " 
* #155024 ^property[0].code = #parent 
* #155024 ^property[0].valueCode = #_mdc 
* #155024 ^property[1].code = #hints 
* #155024 ^property[1].valueString = "PART: 2 ~ CODE10: 23952" 
* #156241 "MDC_EEG_PAROX_CRTX_BURST_CNT"
* #156241 ^definition = Count; Pattern | ParoxysmalActivity; Burst | Cortex; EEG | CNS
* #156241 ^designation[0].language = #de-AT 
* #156241 ^designation[0].value = "Burst Count" 
* #156241 ^property[0].code = #parent 
* #156241 ^property[0].valueCode = #_mdc 
* #156241 ^property[1].code = #hints 
* #156241 ^property[1].valueString = "PART: 2 ~ CODE10: 25169" 
* #156242 "MDC_EEG_PAROX_CRTX_BURST_RATE"
* #156242 ^definition = Rate; Pattern | ParoxysmalActivity; Burst | Cortex; EEG | CNS
* #156242 ^designation[0].language = #de-AT 
* #156242 ^designation[0].value = "Burst Rate" 
* #156242 ^property[0].code = #parent 
* #156242 ^property[0].valueCode = #_mdc 
* #156242 ^property[1].code = #hints 
* #156242 ^property[1].valueString = "PART: 2 ~ CODE10: 25170" 
* #157744 "MDC_VOL_URINE_COL"
* #157744 ^definition = Volume | Collected | Urine | Fluid
* #157744 ^designation[0].language = #de-AT 
* #157744 ^designation[0].value = "Urine volume in bag" 
* #157744 ^property[0].code = #parent 
* #157744 ^property[0].valueCode = #_mdc 
* #157744 ^property[1].code = #hints 
* #157744 ^property[1].valueString = "PART: 2 ~ CODE10: 26672" 
* #157760 "MDC_CONC_DRUG"
* #157760 ^definition = Concentration |  | Drug; Fluid | Pump
* #157760 ^designation[0].language = #de-AT 
* #157760 ^designation[0].value = "Drug concentration" 
* #157760 ^property[0].code = #parent 
* #157760 ^property[0].valueCode = #_mdc 
* #157760 ^property[1].code = #hints 
* #157760 ^property[1].valueString = "PART: 2 ~ CODE10: 26688" 
* #157784 "MDC_FLOW_FLUID_PUMP"
* #157784 ^definition = Flow | Delivery | Fluid | Pump
* #157784 ^designation[0].language = #de-AT 
* #157784 ^designation[0].value = "Fluid delivery rate" 
* #157784 ^property[0].code = #parent 
* #157784 ^property[0].valueCode = #_mdc 
* #157784 ^property[1].code = #hints 
* #157784 ^property[1].valueString = "PART: 2 ~ CODE10: 26712" 
* #157816 "MDC_DOSE_DRUG_BOLUS"
* #157816 ^definition = Mass | Bolus | Drug; Fluid | Pump
* #157816 ^designation[0].language = #de-AT 
* #157816 ^designation[0].value = "Bolus dose; PCA dose" 
* #157816 ^property[0].code = #parent 
* #157816 ^property[0].valueCode = #_mdc 
* #157816 ^property[1].code = #hints 
* #157816 ^property[1].valueString = "PART: 2 ~ CODE10: 26744" 
* #157864 "MDC_VOL_FLUID_DELIV"
* #157864 ^definition = Volume | Delivered | Fluid | Pump
* #157864 ^designation[0].language = #de-AT 
* #157864 ^designation[0].value = "Infused volume" 
* #157864 ^property[0].code = #parent 
* #157864 ^property[0].valueCode = #_mdc 
* #157864 ^property[1].code = #hints 
* #157864 ^property[1].valueString = "PART: 2 ~ CODE10: 26792" 
* #157872 "MDC_VOL_FLUID_TBI_REMAIN"
* #157872 ^definition = Volume | Remaining | Fluid | Pump
* #157872 ^designation[0].language = #de-AT 
* #157872 ^designation[0].value = "Volume remaining to be infused" 
* #157872 ^property[0].code = #parent 
* #157872 ^property[0].valueCode = #_mdc 
* #157872 ^property[1].code = #hints 
* #157872 ^property[1].valueString = "PART: 2 ~ CODE10: 26800" 
* #157880 "MDC_VOL_SYRINGE"
* #157880 ^definition = Volume | Size | Syringe | Pump
* #157880 ^designation[0].language = #de-AT 
* #157880 ^designation[0].value = "Syringe volume" 
* #157880 ^property[0].code = #parent 
* #157880 ^property[0].valueCode = #_mdc 
* #157880 ^property[1].code = #hints 
* #157880 ^property[1].valueString = "PART: 2 ~ CODE10: 26808" 
* #157884 "MDC_VOL_FLUID_TBI"
* #157884 ^definition = Volume | TBI | Fluid | Pump
* #157884 ^designation[0].language = #de-AT 
* #157884 ^designation[0].value = "Volume to be infused" 
* #157884 ^property[0].code = #parent 
* #157884 ^property[0].valueCode = #_mdc 
* #157884 ^property[1].code = #hints 
* #157884 ^property[1].valueString = "PART: 2 ~ CODE10: 26812" 
* #157904 "MDC_TIME_PD_DELAY"
* #157904 ^definition = Duration | Delay | Fluid | Pump
* #157904 ^designation[0].language = #de-AT 
* #157904 ^designation[0].value = "Time delay programmed" 
* #157904 ^property[0].code = #parent 
* #157904 ^property[0].valueCode = #_mdc 
* #157904 ^property[1].code = #hints 
* #157904 ^property[1].valueString = "PART: 2 ~ CODE10: 26832" 
* #157908 "MDC_TIME_PD_DELAY_REMAIN"
* #157908 ^definition = Duration | Delay; Remaining | Fluid | Pump
* #157908 ^designation[0].language = #de-AT 
* #157908 ^designation[0].value = "Time delay programmed remaining" 
* #157908 ^property[0].code = #parent 
* #157908 ^property[0].valueCode = #_mdc 
* #157908 ^property[1].code = #hints 
* #157908 ^property[1].valueString = "PART: 2 ~ CODE10: 26836" 
* #157916 "MDC_TIME_PD_REMAIN"
* #157916 ^definition = Duration | Remaining | Fluid | Pump
* #157916 ^designation[0].language = #de-AT 
* #157916 ^designation[0].value = "Infusion time remaining" 
* #157916 ^property[0].code = #parent 
* #157916 ^property[0].valueCode = #_mdc 
* #157916 ^property[1].code = #hints 
* #157916 ^property[1].valueString = "PART: 2 ~ CODE10: 26844" 
* #157924 "MDC_RATE_DOSE"
* #157924 ^definition = Flow | Mass; Delivery; Normalized; BodyMass | Drug; Fluid | Pump
* #157924 ^designation[0].language = #de-AT 
* #157924 ^designation[0].value = "Dose rate" 
* #157924 ^property[0].code = #parent 
* #157924 ^property[0].valueCode = #_mdc 
* #157924 ^property[1].code = #hints 
* #157924 ^property[1].valueString = "PART: 2 ~ CODE10: 26852" 
* #157984 "MDC_VOL_SYRINGE_ACTUAL"
* #157984 ^property[0].code = #parent 
* #157984 ^property[0].valueCode = #_mdc 
* #157984 ^property[1].code = #hints 
* #157984 ^property[1].valueString = "PART: 2 ~ CODE10: 26912" 
* #157985 "MDC_DOSE_PCA_LIMIT"
* #157985 ^designation[0].language = #de-AT 
* #157985 ^designation[0].value = "Dose Limit Amount" 
* #157985 ^property[0].code = #parent 
* #157985 ^property[0].valueCode = #_mdc 
* #157985 ^property[1].code = #hints 
* #157985 ^property[1].valueString = "PART: 2 ~ CODE10: 26913" 
* #157987 "MDC_TIME_PD_PCA_DOSE_LIMIT"
* #157987 ^property[0].code = #parent 
* #157987 ^property[0].valueCode = #_mdc 
* #157987 ^property[1].code = #hints 
* #157987 ^property[1].valueString = "PART: 2 ~ CODE10: 26915" 
* #157988 "MDC_RATE_PCA_MAX_DOSES_PER_HOUR"
* #157988 ^property[0].code = #parent 
* #157988 ^property[0].valueCode = #_mdc 
* #157988 ^property[1].code = #hints 
* #157988 ^property[1].valueString = "PART: 2 ~ CODE10: 26916" 
* #157990 "MDC_NUM_PATIENT_DOSES_GIVEN_TOTAL"
* #157990 ^property[0].code = #parent 
* #157990 ^property[0].valueCode = #_mdc 
* #157990 ^property[1].code = #hints 
* #157990 ^property[1].valueString = "PART: 2 ~ CODE10: 26918" 
* #157991 "MDC_NUM_PATIENT_DOSES_ATTEMPTED_TOTAL"
* #157991 ^property[0].code = #parent 
* #157991 ^property[0].valueCode = #_mdc 
* #157991 ^property[1].code = #hints 
* #157991 ^property[1].valueString = "PART: 2 ~ CODE10: 26919" 
* #157992 "MDC_VOL_FLUID_DELIV_SEGMENT"
* #157992 ^property[0].code = #parent 
* #157992 ^property[0].valueCode = #_mdc 
* #157992 ^property[1].code = #hints 
* #157992 ^property[1].valueString = "PART: 2 ~ CODE10: 26920" 
* #157993 "MDC_VOL_FLUID_DELIV_TOTAL"
* #157993 ^property[0].code = #parent 
* #157993 ^property[0].valueCode = #_mdc 
* #157993 ^property[1].code = #hints 
* #157993 ^property[1].valueString = "PART: 2 ~ CODE10: 26921" 
* #157994 "MDC_VOL_FLUID_CONTAINER_START"
* #157994 ^property[0].code = #parent 
* #157994 ^property[0].valueCode = #_mdc 
* #157994 ^property[1].code = #hints 
* #157994 ^property[1].valueString = "PART: 2 ~ CODE10: 26922" 
* #157995 "MDC_VOL_REMAIN_CONTAINER"
* #157995 ^property[0].code = #parent 
* #157995 ^property[0].valueCode = #_mdc 
* #157995 ^property[1].code = #hints 
* #157995 ^property[1].valueString = "PART: 2 ~ CODE10: 26923" 
* #157996 "MDC_TIME_PD_PROG"
* #157996 ^property[0].code = #parent 
* #157996 ^property[0].valueCode = #_mdc 
* #157996 ^property[1].code = #hints 
* #157996 ^property[1].valueString = "PART: 2 ~ CODE10: 26924" 
* #157997 "MDC_TIME_PD_REMAIN_CONTAINER"
* #157997 ^property[0].code = #parent 
* #157997 ^property[0].valueCode = #_mdc 
* #157997 ^property[1].code = #hints 
* #157997 ^property[1].valueString = "PART: 2 ~ CODE10: 26925" 
* #157999 "MDC_DOSE_DRUG_TBI"
* #157999 ^property[0].code = #parent 
* #157999 ^property[0].valueCode = #_mdc 
* #157999 ^property[1].code = #hints 
* #157999 ^property[1].valueString = "PART: 2 ~ CODE10: 26927" 
* #158000 "MDC_DOSE_DRUG_TBI_REMAIN"
* #158000 ^property[0].code = #parent 
* #158000 ^property[0].valueCode = #_mdc 
* #158000 ^property[1].code = #hints 
* #158000 ^property[1].valueString = "PART: 2 ~ CODE10: 26928" 
* #158002 "MDC_DOSE_DRUG_DELIV"
* #158002 ^property[0].code = #parent 
* #158002 ^property[0].valueCode = #_mdc 
* #158002 ^property[1].code = #hints 
* #158002 ^property[1].valueString = "PART: 2 ~ CODE10: 26930" 
* #158003 "MDC_DOSE_DRUG_DELIV_SEGMENT"
* #158003 ^property[0].code = #parent 
* #158003 ^property[0].valueCode = #_mdc 
* #158003 ^property[1].code = #hints 
* #158003 ^property[1].valueString = "PART: 2 ~ CODE10: 26931" 
* #158004 "MDC_MASS_DRUG_REMAIN_CONTAINER"
* #158004 ^property[0].code = #parent 
* #158004 ^property[0].valueCode = #_mdc 
* #158004 ^property[1].code = #hints 
* #158004 ^property[1].valueString = "PART: 2 ~ CODE10: 26932" 
* #158005 "MDC_DEV_PUMP_CURRENT_DELIVERY_STATUS"
* #158005 ^property[0].code = #parent 
* #158005 ^property[0].valueCode = #_mdc 
* #158005 ^property[1].code = #hints 
* #158005 ^property[1].valueString = "PART: 2 ~ CODE10: 26933" 
* #158006 "MDC_DEV_PUMP_NOT_DELIVERING_REASON"
* #158006 ^property[0].code = #parent 
* #158006 ^property[0].valueCode = #_mdc 
* #158006 ^property[1].code = #hints 
* #158006 ^property[1].valueString = "PART: 2 ~ CODE10: 26934" 
* #158007 "MDC_DEV_PUMP_PROGRAM_COMPLETION_MODE"
* #158007 ^property[0].code = #parent 
* #158007 ^property[0].valueCode = #_mdc 
* #158007 ^property[1].code = #hints 
* #158007 ^property[1].valueString = "PART: 2 ~ CODE10: 26935" 
* #158008 "MDC_DEV_PUMP_PROGRAM_DELIVERY_MODE"
* #158008 ^property[0].code = #parent 
* #158008 ^property[0].valueCode = #_mdc 
* #158008 ^property[1].code = #hints 
* #158008 ^property[1].valueString = "PART: 2 ~ CODE10: 26936" 
* #158009 "MDC_DEV_PUMP_PROGRAM_STATUS"
* #158009 ^property[0].code = #parent 
* #158009 ^property[0].valueCode = #_mdc 
* #158009 ^property[1].code = #hints 
* #158009 ^property[1].valueString = "PART: 2 ~ CODE10: 26937" 
* #158010 "MDC_DEV_PUMP_FLUSH_ENABLED"
* #158010 ^property[0].code = #parent 
* #158010 ^property[0].valueCode = #_mdc 
* #158010 ^property[1].code = #hints 
* #158010 ^property[1].valueString = "PART: 2 ~ CODE10: 26938" 
* #158011 "MDC_NUM_DOSE_PROG"
* #158011 ^property[0].code = #parent 
* #158011 ^property[0].valueCode = #_mdc 
* #158011 ^property[1].code = #hints 
* #158011 ^property[1].valueString = "PART: 2 ~ CODE10: 26939" 
* #158012 "MDC_DEV_PUMP_SOURCE_CHANNEL_LABEL"
* #158012 ^property[0].code = #parent 
* #158012 ^property[0].valueCode = #_mdc 
* #158012 ^property[1].code = #hints 
* #158012 ^property[1].valueString = "PART: 2 ~ CODE10: 26940" 
* #158013 "MDC_AMOUNT_DRUG_REMAIN_CONTAINER"
* #158013 ^property[0].code = #parent 
* #158013 ^property[0].valueCode = #_mdc 
* #158013 ^property[1].code = #hints 
* #158013 ^property[1].valueString = "PART: 2 ~ CODE10: 26941" 
* #158014 "MDC_FLOW_FLUID_PUMP_CURRENT"
* #158014 ^property[0].code = #parent 
* #158014 ^property[0].valueCode = #_mdc 
* #158014 ^property[1].code = #hints 
* #158014 ^property[1].valueString = "PART: 2 ~ CODE10: 26942" 
* #158015 "MDC_DEV_PUMP_ENABLED_SOURCES"
* #158015 ^property[0].code = #parent 
* #158015 ^property[0].valueCode = #_mdc 
* #158015 ^property[1].code = #hints 
* #158015 ^property[1].valueString = "PART: 2 ~ CODE10: 26943" 
* #158016 "MDC_DEV_PUMP_ACTIVE_SOURCES"
* #158016 ^property[0].code = #parent 
* #158016 ^property[0].valueCode = #_mdc 
* #158016 ^property[1].code = #hints 
* #158016 ^property[1].valueString = "PART: 2 ~ CODE10: 26944" 
* #158017 "MDC_DOSE_PCA_PATIENT"
* #158017 ^property[0].code = #parent 
* #158017 ^property[0].valueCode = #_mdc 
* #158017 ^property[1].code = #hints 
* #158017 ^property[1].valueString = "PART: 2 ~ CODE10: 26945" 
* #158018 "MDC_DOSE_LOADING"
* #158018 ^property[0].code = #parent 
* #158018 ^property[0].valueCode = #_mdc 
* #158018 ^property[1].code = #hints 
* #158018 ^property[1].valueString = "PART: 2 ~ CODE10: 26946" 
* #158019 "MDC_DOSE_CLINICIAN"
* #158019 ^property[0].code = #parent 
* #158019 ^property[0].valueCode = #_mdc 
* #158019 ^property[1].code = #hints 
* #158019 ^property[1].valueString = "PART: 2 ~ CODE10: 26947" 
* #159748 "MDC_CONC_PH_ART"
* #159748 ^definition = Concentration | Total; H+; Logarithmic | ArterialBlood | FluidChemistry
* #159748 ^designation[0].language = #de-AT 
* #159748 ^designation[0].value = "Arterial blood fluid pH" 
* #159748 ^property[0].code = #parent 
* #159748 ^property[0].valueCode = #_mdc 
* #159748 ^property[1].code = #hints 
* #159748 ^property[1].valueString = "PART: 2 ~ CODE10: 28676" 
* #159752 "MDC_CONC_PCO2_ART"
* #159752 ^definition = Concentration | Total; pCO2 | ArterialBlood | FluidChemistry
* #159752 ^designation[0].language = #de-AT 
* #159752 ^designation[0].value = "Arterial blood pCO2" 
* #159752 ^property[0].code = #parent 
* #159752 ^property[0].valueCode = #_mdc 
* #159752 ^property[1].code = #hints 
* #159752 ^property[1].valueString = "PART: 2 ~ CODE10: 28680" 
* #159756 "MDC_CONC_PO2_ART"
* #159756 ^definition = Concentration | Total; pO2 | ArterialBlood | FluidChemistry
* #159756 ^designation[0].language = #de-AT 
* #159756 ^designation[0].value = "Arterial blood pO2" 
* #159756 ^property[0].code = #parent 
* #159756 ^property[0].valueCode = #_mdc 
* #159756 ^property[1].code = #hints 
* #159756 ^property[1].valueString = "PART: 2 ~ CODE10: 28684" 
* #159760 "MDC_CONC_HCO3_ART"
* #159760 ^definition = Concentration | Total; hCO3 | ArterialBlood | FluidChemistry
* #159760 ^designation[0].language = #de-AT 
* #159760 ^designation[0].value = "Arterial blood bicarbonate ion concentration" 
* #159760 ^property[0].code = #parent 
* #159760 ^property[0].valueCode = #_mdc 
* #159760 ^property[1].code = #hints 
* #159760 ^property[1].valueString = "PART: 2 ~ CODE10: 28688" 
* #159764 "MDC_CONC_HB_ART"
* #159764 ^definition = Concentration | Total; Hb | ArterialBlood | FluidChemistry
* #159764 ^designation[0].language = #de-AT 
* #159764 ^designation[0].value = "Arterial blood haemoglobin concentration" 
* #159764 ^property[0].code = #parent 
* #159764 ^property[0].valueCode = #_mdc 
* #159764 ^property[1].code = #hints 
* #159764 ^property[1].valueString = "PART: 2 ~ CODE10: 28692" 
* #159768 "MDC_CONC_HB_O2_ART"
* #159768 ^definition = Concentration | Total; O2Hb | ArterialBlood | FluidChemistry
* #159768 ^designation[0].language = #de-AT 
* #159768 ^designation[0].value = "Arterial blood oxy-haemoglobin concentration" 
* #159768 ^property[0].code = #parent 
* #159768 ^property[0].valueCode = #_mdc 
* #159768 ^property[1].code = #hints 
* #159768 ^property[1].valueString = "PART: 2 ~ CODE10: 28696" 
* #159792 "MDC_CONC_CA_ART"
* #159792 ^definition = Concentration | Total; Ca | ArterialBlood | FluidChemistry
* #159792 ^designation[0].language = #de-AT 
* #159792 ^designation[0].value = "Arterial blood calcium ion concentration" 
* #159792 ^property[0].code = #parent 
* #159792 ^property[0].valueCode = #_mdc 
* #159792 ^property[1].code = #hints 
* #159792 ^property[1].valueString = "PART: 2 ~ CODE10: 28720" 
* #159796 "MDC_CONC_PH_VEN"
* #159796 ^definition = Concentration | Total; H+; Logarithmic | VenousBlood | FluidChemistry
* #159796 ^designation[0].language = #de-AT 
* #159796 ^designation[0].value = "Venous blood fluid pH" 
* #159796 ^property[0].code = #parent 
* #159796 ^property[0].valueCode = #_mdc 
* #159796 ^property[1].code = #hints 
* #159796 ^property[1].valueString = "PART: 2 ~ CODE10: 28724" 
* #159800 "MDC_CONC_PCO2_VEN"
* #159800 ^definition = Concentration | Total; pCO2 | VenousBlood | FluidChemistry
* #159800 ^designation[0].language = #de-AT 
* #159800 ^designation[0].value = "Venous blood pCO2" 
* #159800 ^property[0].code = #parent 
* #159800 ^property[0].valueCode = #_mdc 
* #159800 ^property[1].code = #hints 
* #159800 ^property[1].valueString = "PART: 2 ~ CODE10: 28728" 
* #159804 "MDC_CONC_PO2_VEN"
* #159804 ^definition = Concentration | Total; pO2 | VenousBlood | FluidChemistry
* #159804 ^designation[0].language = #de-AT 
* #159804 ^designation[0].value = "Venous blood pO2" 
* #159804 ^property[0].code = #parent 
* #159804 ^property[0].valueCode = #_mdc 
* #159804 ^property[1].code = #hints 
* #159804 ^property[1].valueString = "PART: 2 ~ CODE10: 28732" 
* #159816 "MDC_CONC_HB_O2_VEN"
* #159816 ^definition = Concentration | Total; O2Hb | VenousBlood | FluidChemistry
* #159816 ^designation[0].language = #de-AT 
* #159816 ^designation[0].value = "Venous blood oxy-haemoglobin concentration" 
* #159816 ^property[0].code = #parent 
* #159816 ^property[0].valueCode = #_mdc 
* #159816 ^property[1].code = #hints 
* #159816 ^property[1].valueString = "PART: 2 ~ CODE10: 28744" 
* #159852 "MDC_CONC_NA_URINE"
* #159852 ^definition = Concentration | Total; Na | Urine | FluidChemistry
* #159852 ^designation[0].language = #de-AT 
* #159852 ^designation[0].value = "Urine sodium ion concentration" 
* #159852 ^property[0].code = #parent 
* #159852 ^property[0].valueCode = #_mdc 
* #159852 ^property[1].code = #hints 
* #159852 ^property[1].valueString = "PART: 2 ~ CODE10: 28780" 
* #159960 "MDC_CONC_NA_SERUM"
* #159960 ^definition = Concentration | Total; Na | Serum | FluidChemistry
* #159960 ^designation[0].language = #de-AT 
* #159960 ^designation[0].value = "Serum sodium ion concentration" 
* #159960 ^property[0].code = #parent 
* #159960 ^property[0].valueCode = #_mdc 
* #159960 ^property[1].code = #hints 
* #159960 ^property[1].valueString = "PART: 2 ~ CODE10: 28888" 
* #160004 "MDC_CONC_PH_GEN"
* #160004 ^definition = Concentration | Total; H+; Logarithmic | General | FluidChemistry
* #160004 ^designation[0].language = #de-AT 
* #160004 ^designation[0].value = "General fluid pH" 
* #160004 ^property[0].code = #parent 
* #160004 ^property[0].valueCode = #_mdc 
* #160004 ^property[1].code = #hints 
* #160004 ^property[1].valueString = "PART: 2 ~ CODE10: 28932" 
* #160008 "MDC_CONC_HCO3_GEN"
* #160008 ^definition = Concentration | Total; hCO3 | General | FluidChemistry
* #160008 ^designation[0].language = #de-AT 
* #160008 ^designation[0].value = "General bicarbonate ion concentration" 
* #160008 ^property[0].code = #parent 
* #160008 ^property[0].valueCode = #_mdc 
* #160008 ^property[1].code = #hints 
* #160008 ^property[1].valueString = "PART: 2 ~ CODE10: 28936" 
* #160012 "MDC_CONC_NA_GEN"
* #160012 ^definition = Concentration | Total; Na | General | FluidChemistry
* #160012 ^designation[0].language = #de-AT 
* #160012 ^designation[0].value = "General sodium ion concentration" 
* #160012 ^property[0].code = #parent 
* #160012 ^property[0].valueCode = #_mdc 
* #160012 ^property[1].code = #hints 
* #160012 ^property[1].valueString = "PART: 2 ~ CODE10: 28940" 
* #160016 "MDC_CONC_K_GEN"
* #160016 ^definition = Concentration | Total; K | General | FluidChemistry
* #160016 ^designation[0].language = #de-AT 
* #160016 ^designation[0].value = "General potassium ion concentration" 
* #160016 ^property[0].code = #parent 
* #160016 ^property[0].valueCode = #_mdc 
* #160016 ^property[1].code = #hints 
* #160016 ^property[1].valueString = "PART: 2 ~ CODE10: 28944" 
* #160020 "MDC_CONC_GLU_GEN"
* #160020 ^definition = Concentration | Total; Glucose | General | FluidChemistry
* #160020 ^designation[0].language = #de-AT 
* #160020 ^designation[0].value = "General glucose concentration" 
* #160020 ^property[0].code = #parent 
* #160020 ^property[0].valueCode = #_mdc 
* #160020 ^property[1].code = #hints 
* #160020 ^property[1].valueString = "PART: 2 ~ CODE10: 28948" 
* #160024 "MDC_CONC_CA_GEN"
* #160024 ^definition = Concentration | Total; Ca | General | FluidChemistry
* #160024 ^designation[0].language = #de-AT 
* #160024 ^designation[0].value = "General calcium ion concentration" 
* #160024 ^property[0].code = #parent 
* #160024 ^property[0].valueCode = #_mdc 
* #160024 ^property[1].code = #hints 
* #160024 ^property[1].valueString = "PART: 2 ~ CODE10: 28952" 
* #160064 "MDC_CONC_PCO2_GEN"
* #160064 ^definition = Concentration | Total; pCO2 | General | FluidChemistry
* #160064 ^designation[0].language = #de-AT 
* #160064 ^designation[0].value = "General fluid pCO2" 
* #160064 ^property[0].code = #parent 
* #160064 ^property[0].valueCode = #_mdc 
* #160064 ^property[1].code = #hints 
* #160064 ^property[1].valueString = "PART: 2 ~ CODE10: 28992" 
* #160068 "MDC_CONC_HCT_ART"
* #160068 ^definition = Concentration | Total; HCT | ArterialBlood | FluidChemistry
* #160068 ^designation[0].language = #de-AT 
* #160068 ^designation[0].value = "Arterial blood haematocrit concentration" 
* #160068 ^property[0].code = #parent 
* #160068 ^property[0].valueCode = #_mdc 
* #160068 ^property[1].code = #hints 
* #160068 ^property[1].valueString = "PART: 2 ~ CODE10: 28996" 
* #160080 "MDC_CONC_UREA_ART"
* #160080 ^definition = Concentration | Total; Urea | ArterialBlood | FluidChemistry
* #160080 ^designation[0].language = #de-AT 
* #160080 ^designation[0].value = "Arterial blood urea concentration" 
* #160080 ^property[0].code = #parent 
* #160080 ^property[0].valueCode = #_mdc 
* #160080 ^property[1].code = #hints 
* #160080 ^property[1].valueString = "PART: 2 ~ CODE10: 29008" 
* #160104 "MDC_CONC_CHLOR_GEN"
* #160104 ^definition = Concentration | Total; Chloride | General | FluidChemistry
* #160104 ^designation[0].language = #de-AT 
* #160104 ^designation[0].value = "General chloride ion concentration" 
* #160104 ^property[0].code = #parent 
* #160104 ^property[0].valueCode = #_mdc 
* #160104 ^property[1].code = #hints 
* #160104 ^property[1].valueString = "PART: 2 ~ CODE10: 29032" 
* #160116 "MDC_CONC_PO2_GEN"
* #160116 ^definition = Concentration | Total; pO2 | General | FluidChemistry
* #160116 ^designation[0].language = #de-AT 
* #160116 ^designation[0].value = "General fluid pO2" 
* #160116 ^property[0].code = #parent 
* #160116 ^property[0].valueCode = #_mdc 
* #160116 ^property[1].code = #hints 
* #160116 ^property[1].valueString = "PART: 2 ~ CODE10: 29044" 
* #160132 "MDC_CONC_HCT_GEN"
* #160132 ^definition = Concentration | Total; HCT | General | FluidChemistry
* #160132 ^designation[0].language = #de-AT 
* #160132 ^designation[0].value = "General fluid HCT" 
* #160132 ^property[0].code = #parent 
* #160132 ^property[0].valueCode = #_mdc 
* #160132 ^property[1].code = #hints 
* #160132 ^property[1].valueString = "PART: 2 ~ CODE10: 29060" 
* #160184 "MDC_CONC_GLU_CAPILLARY_WHOLEBLOOD"
* #160184 ^definition = Concentration | Total; Glucose | CapillaryWholeBlood | FluidChemistry
* #160184 ^designation[0].language = #de-AT 
* #160184 ^designation[0].value = "Capillary whole blood glucose" 
* #160184 ^property[0].code = #parent 
* #160184 ^property[0].valueCode = #_mdc 
* #160184 ^property[1].code = #hints 
* #160184 ^property[1].valueString = "PART: 2 ~ CODE10: 29112" 
* #160188 "MDC_CONC_GLU_CAPILLARY_PLASMA"
* #160188 ^definition = Concentration | Total; Glucose | CapillaryPlasma | FluidChemistry
* #160188 ^designation[0].language = #de-AT 
* #160188 ^designation[0].value = "Capillary plasma glucose" 
* #160188 ^property[0].code = #parent 
* #160188 ^property[0].valueCode = #_mdc 
* #160188 ^property[1].code = #hints 
* #160188 ^property[1].valueString = "PART: 2 ~ CODE10: 29116" 
* #160192 "MDC_CONC_GLU_VENOUS_WHOLEBLOOD"
* #160192 ^definition = Concentration | Total; Glucose | VenousWholeBlood | FluidChemistry
* #160192 ^designation[0].language = #de-AT 
* #160192 ^designation[0].value = "Venous whole blood glucose" 
* #160192 ^property[0].code = #parent 
* #160192 ^property[0].valueCode = #_mdc 
* #160192 ^property[1].code = #hints 
* #160192 ^property[1].valueString = "PART: 2 ~ CODE10: 29120" 
* #160196 "MDC_CONC_GLU_VENOUS_PLASMA"
* #160196 ^definition = Concentration | Total; Glucose | VenousPlasma | FluidChemistry
* #160196 ^designation[0].language = #de-AT 
* #160196 ^designation[0].value = "Venous plasma glucose" 
* #160196 ^property[0].code = #parent 
* #160196 ^property[0].valueCode = #_mdc 
* #160196 ^property[1].code = #hints 
* #160196 ^property[1].valueString = "PART: 2 ~ CODE10: 29124" 
* #160200 "MDC_CONC_GLU_ARTERIAL_WHOLEBLOOD"
* #160200 ^definition = Concentration | Total; Glucose | ArterialWholeBlood | FluidChemistry
* #160200 ^designation[0].language = #de-AT 
* #160200 ^designation[0].value = "Arterial whole blood glucose" 
* #160200 ^property[0].code = #parent 
* #160200 ^property[0].valueCode = #_mdc 
* #160200 ^property[1].code = #hints 
* #160200 ^property[1].valueString = "PART: 2 ~ CODE10: 29128" 
* #160204 "MDC_CONC_GLU_ARTERIAL_PLASMA"
* #160204 ^definition = Concentration | Total; Glucose | ArterialPlasma | FluidChemistry
* #160204 ^designation[0].language = #de-AT 
* #160204 ^designation[0].value = "Arterial plasma glucose" 
* #160204 ^property[0].code = #parent 
* #160204 ^property[0].valueCode = #_mdc 
* #160204 ^property[1].code = #hints 
* #160204 ^property[1].valueString = "PART: 2 ~ CODE10: 29132" 
* #160208 "MDC_CONC_GLU_CONTROL"
* #160208 ^definition = Concentration | Glucose | ControlSolution | FluidChemistry
* #160208 ^designation[0].language = #de-AT 
* #160208 ^designation[0].value = "Control Result" 
* #160208 ^property[0].code = #parent 
* #160208 ^property[0].valueCode = #_mdc 
* #160208 ^property[1].code = #hints 
* #160208 ^property[1].valueString = "PART: 2 ~ CODE10: 29136" 
* #160212 "MDC_CONC_GLU_ISF"
* #160212 ^definition = Concentration | Glucose | InterstitialFluid | FluidChemistry
* #160212 ^designation[0].language = #de-AT 
* #160212 ^designation[0].value = "Interstitia Fluid Glucose" 
* #160212 ^property[0].code = #parent 
* #160212 ^property[0].valueCode = #_mdc 
* #160212 ^property[1].code = #hints 
* #160212 ^property[1].valueString = "PART: 2 ~ CODE10: 29140" 
* #160216 "MDC_BASE_EXCESS_FLUID_EXTRACELLULAR"
* #160216 ^definition = Concentration | BaseExcess | ExtracellularFluid | BloodChemistry
* #160216 ^designation[0].language = #de-AT 
* #160216 ^designation[0].value = "Base Excess of Extracellular Fluid" 
* #160216 ^property[0].code = #parent 
* #160216 ^property[0].valueCode = #_mdc 
* #160216 ^property[1].code = #hints 
* #160216 ^property[1].valueString = "PART: 2 ~ CODE10: 29144" 
* #160220 "MDC_CONC_HBA1C"
* #160220 ^definition = Concentration | HbA1c
* #160220 ^designation[0].language = #de-AT 
* #160220 ^designation[0].value = "HbA1c" 
* #160220 ^property[0].code = #parent 
* #160220 ^property[0].valueCode = #_mdc 
* #160220 ^property[1].code = #hints 
* #160220 ^property[1].valueString = "PART: 2 ~ CODE10: 29148" 
* #160252 "MDC_BASE_EXCESS_BLD_ART"
* #160252 ^definition = Concentration | BaseExcess | ArterialBlood | BloodChemistry
* #160252 ^designation[0].language = #de-AT 
* #160252 ^designation[0].value = "Base Excess of Arterial Blood" 
* #160252 ^property[0].code = #parent 
* #160252 ^property[0].valueCode = #_mdc 
* #160252 ^property[1].code = #hints 
* #160252 ^property[1].valueString = "PART: 2 ~ CODE10: 29180" 
* #160256 "MDC_CONC_CA_PH_NORMALIZED_ART"
* #160256 ^definition = Concentration | Total; Ca; Logarithmic; Normalized to pH 7.4 | ArterialBlood | FluidChemistry
* #160256 ^designation[0].language = #de-AT 
* #160256 ^designation[0].value = "Ionized Calcium; Normalized to pH 7.4" 
* #160256 ^property[0].code = #parent 
* #160256 ^property[0].valueCode = #_mdc 
* #160256 ^property[1].code = #hints 
* #160256 ^property[1].valueString = "PART: 2 ~ CODE10: 29184" 
* #160260 "MDC_RATIO_INR_COAG"
* #160260 ^definition = Ratio | Coagulation | Plasma | Blood Chemistry
* #160260 ^designation[0].language = #de-AT 
* #160260 ^designation[0].value = "Coagulation ratio ? INR" 
* #160260 ^property[0].code = #parent 
* #160260 ^property[0].valueCode = #_mdc 
* #160260 ^property[1].code = #hints 
* #160260 ^property[1].valueString = "PART: 2 ~ CODE10: 29188" 
* #160264 "MDC_TIME_PD_COAG"
* #160264 ^definition = Duration | Coagulation | Plasma | Blood Chemistry
* #160264 ^designation[0].language = #de-AT 
* #160264 ^designation[0].value = "Coagulation time ? prothrombin time" 
* #160264 ^property[0].code = #parent 
* #160264 ^property[0].valueCode = #_mdc 
* #160264 ^property[1].code = #hints 
* #160264 ^property[1].valueString = "PART: 2 ~ CODE10: 29192" 
* #160268 "MDC_QUICK_VALUE_COAG"
* #160268 ^definition = Quick value | Coagulation | Plasma | Blood Chemistry
* #160268 ^designation[0].language = #de-AT 
* #160268 ^designation[0].value = "Coagulation quick value" 
* #160268 ^property[0].code = #parent 
* #160268 ^property[0].valueCode = #_mdc 
* #160268 ^property[1].code = #hints 
* #160268 ^property[1].valueString = "PART: 2 ~ CODE10: 29196" 
* #160272 "MDC_ISI_COAG"
* #160272 ^definition = ISI | Coagulation | Plasma | Blood Chemistry
* #160272 ^designation[0].language = #de-AT 
* #160272 ^designation[0].value = "International Sensitivity Index" 
* #160272 ^property[0].code = #parent 
* #160272 ^property[0].valueCode = #_mdc 
* #160272 ^property[1].code = #hints 
* #160272 ^property[1].valueString = "PART: 2 ~ CODE10: 29200" 
* #160276 "MDC_COAG_CONTROL"
* #160276 ^definition = Control | Coagulation | Plasma | Blood Chemistry
* #160276 ^designation[0].language = #de-AT 
* #160276 ^designation[0].value = "Control calibration of INR" 
* #160276 ^property[0].code = #parent 
* #160276 ^property[0].valueCode = #_mdc 
* #160276 ^property[1].code = #hints 
* #160276 ^property[1].valueString = "PART: 2 ~ CODE10: 29204" 
* #160280 "MDC_PULS_OXIM_CONC_HB_O2_ART_CALC"
* #160280 ^definition = Concentration | Oxygen content; total; bound and unbound to hemoglobin | ArterialBlood | FluidChemistry; Pulse Oximetry
* #160280 ^designation[0].language = #de-AT 
* #160280 ^designation[0].value = "(Estimated) Arterial Oxygen Content" 
* #160280 ^property[0].code = #parent 
* #160280 ^property[0].valueCode = #_mdc 
* #160280 ^property[1].code = #hints 
* #160280 ^property[1].valueString = "PART: 2 ~ CODE10: 29208" 
* #160284 "MDC_PULS_OXIM_HB_CO_ART"
* #160284 ^definition = Concentration | Carboxyhemoglobin | ArterialBlood | FluidChemistry; Pulse Oximetry
* #160284 ^designation[0].language = #de-AT 
* #160284 ^designation[0].value = "Arterial blood carboxyhemoglobin concentration" 
* #160284 ^property[0].code = #parent 
* #160284 ^property[0].valueCode = #_mdc 
* #160284 ^property[1].code = #hints 
* #160284 ^property[1].valueString = "PART: 2 ~ CODE10: 29212" 
* #160288 "MDC_PULS_OXIM_HB_MET_ART"
* #160288 ^definition = Concentration | Methemoglobin | ArterialBlood | FluidChemistry; Pulse Oximetry
* #160288 ^designation[0].language = #de-AT 
* #160288 ^designation[0].value = "Arterial blood methemoglobin concentration" 
* #160288 ^property[0].code = #parent 
* #160288 ^property[0].valueCode = #_mdc 
* #160288 ^property[1].code = #hints 
* #160288 ^property[1].valueString = "PART: 2 ~ CODE10: 29216" 
* #160292 "MDC_PULS_OXIM_HB_TOTAL_ART"
* #160292 ^definition = Concentration | Hb; total | ArterialBlood | FluidChemistry; Pulse Oximetry
* #160292 ^designation[0].language = #de-AT 
* #160292 ^designation[0].value = "Arterial blood hemoglobin concentration" 
* #160292 ^property[0].code = #parent 
* #160292 ^property[0].valueCode = #_mdc 
* #160292 ^property[1].code = #hints 
* #160292 ^property[1].valueString = "PART: 2 ~ CODE10: 29220" 
* #160296 "MDC_PULS_OXIM_SAT_O2_ART_PREDUCTAL"
* #160296 ^definition = Concentration | Saturation; Oxygen | ArterialBlood; Preductal | FluidChemistry; Pulse Oximetry
* #160296 ^designation[0].language = #de-AT 
* #160296 ^designation[0].value = "Preductal SpO2" 
* #160296 ^property[0].code = #parent 
* #160296 ^property[0].valueCode = #_mdc 
* #160296 ^property[1].code = #hints 
* #160296 ^property[1].valueString = "PART: 2 ~ CODE10: 29224" 
* #160300 "MDC_PULS_OXIM_SAT_O2_ART_POSTDUCTAL"
* #160300 ^definition = Concentration | Saturation; Oxygen | ArterialBlood; Postductal | FluidChemistry; Pulse Oximetry
* #160300 ^designation[0].language = #de-AT 
* #160300 ^designation[0].value = "Postductal SpO2" 
* #160300 ^property[0].code = #parent 
* #160300 ^property[0].valueCode = #_mdc 
* #160300 ^property[1].code = #hints 
* #160300 ^property[1].valueString = "PART: 2 ~ CODE10: 29228" 
* #160304 "MDC_PULS_OXIM_SAT_O2_ART_PRE_POST_DIFF"
* #160304 ^definition = Concentration | Saturation; Oxygen | ArterialBlood; (Preductal; Postductal) difference | FluidChemistry; Pulse Oximetry
* #160304 ^designation[0].language = #de-AT 
* #160304 ^designation[0].value = "Pre-postductal SpO2 difference" 
* #160304 ^property[0].code = #parent 
* #160304 ^property[0].valueCode = #_mdc 
* #160304 ^property[1].code = #hints 
* #160304 ^property[1].valueString = "PART: 2 ~ CODE10: 29232" 
* #160308 "MDC_CONC_PCO2_GASTRIC_ART_DIFF"
* #160308 ^definition = Concentration; PartialPressure | Difference; CO2 (gastric;arterial) | Gastric; mucosal | FluidChemistry
* #160308 ^designation[0].language = #de-AT 
* #160308 ^designation[0].value = "Gastric-arterial CO2 Gap" 
* #160308 ^property[0].code = #parent 
* #160308 ^property[0].valueCode = #_mdc 
* #160308 ^property[1].code = #hints 
* #160308 ^property[1].valueString = "PART: 2 ~ CODE10: 29236" 
* #160312 "MDC_CONC_PCO2_GASTRIC_ET_DIFF"
* #160312 ^definition = Concentration; PartialPressure | Difference; CO2 (gastric;endtidal) | Gastric; mucosal | FluidChemistry
* #160312 ^designation[0].language = #de-AT 
* #160312 ^designation[0].value = "Gastric-endtidal CO2 Gap" 
* #160312 ^property[0].code = #parent 
* #160312 ^property[0].valueCode = #_mdc 
* #160312 ^property[1].code = #hints 
* #160312 ^property[1].valueString = "PART: 2 ~ CODE10: 29240" 
* #160316 "MDC_CONC_PCO2_GASTRIC_MUCOSAL"
* #160316 ^definition = Concentration; PartialPressure | Total; CO2 | Gastric; mucosal | FluidChemistry
* #160316 ^designation[0].language = #de-AT 
* #160316 ^designation[0].value = "Gastric Mucosal PCO2" 
* #160316 ^property[0].code = #parent 
* #160316 ^property[0].valueCode = #_mdc 
* #160316 ^property[1].code = #hints 
* #160316 ^property[1].valueString = "PART: 2 ~ CODE10: 29244" 
* #160320 "MDC_CONC_PH_INTRAMUCOSAL"
* #160320 ^definition = Concentration | Total; H+; Logarithmic | Gastric; Intramucosal | FluidChemistry
* #160320 ^designation[0].language = #de-AT 
* #160320 ^designation[0].value = "Intramucosal pH" 
* #160320 ^property[0].code = #parent 
* #160320 ^property[0].valueCode = #_mdc 
* #160320 ^property[1].code = #hints 
* #160320 ^property[1].valueString = "PART: 2 ~ CODE10: 29248" 
* #160324 "MDC_SPO2_SIGNAL_QUALITY_INDEX"
* #160324 ^definition = Index | SignalQuality; SpO2 | ArterialBlood | FluidChemistry; Pulse Oximetry
* #160324 ^designation[0].language = #de-AT 
* #160324 ^designation[0].value = "SPO2 Signal Quality Index" 
* #160324 ^property[0].code = #parent 
* #160324 ^property[0].valueCode = #_mdc 
* #160324 ^property[1].code = #hints 
* #160324 ^property[1].valueString = "PART: 2 ~ CODE10: 29252" 
* #160364 "MDC_CONC_GLU_UNDETERMINED_WHOLEBLOOD"
* #160364 ^definition = Concentration | Total; Glucose | UndeterminedWholeBlood | FluidChemistry
* #160364 ^designation[0].language = #de-AT 
* #160364 ^designation[0].value = "Undetermined whole blood glucose" 
* #160364 ^property[0].code = #parent 
* #160364 ^property[0].valueCode = #_mdc 
* #160364 ^property[1].code = #hints 
* #160364 ^property[1].valueString = "PART: 2 ~ CODE10: 29292" 
* #160368 "MDC_CONC_GLU_UNDETERMINED_PLASMA"
* #160368 ^definition = Concentration | Total; Glucose | UndeterminedPlasma | FluidChemistry
* #160368 ^designation[0].language = #de-AT 
* #160368 ^designation[0].value = "Undetermined arterial plasma glucose" 
* #160368 ^property[0].code = #parent 
* #160368 ^property[0].valueCode = #_mdc 
* #160368 ^property[1].code = #hints 
* #160368 ^property[1].valueString = "PART: 2 ~ CODE10: 29296" 
* #160372 "MDC_CONC_GLU_CONTROL_LEVEL_LOW"
* #160372 ^property[0].code = #parent 
* #160372 ^property[0].valueCode = #_mdc 
* #160372 ^property[1].code = #hints 
* #160372 ^property[1].valueString = "PART: 2 ~ CODE10: 29300" 
* #160376 "MDC_CONC_GLU_CONTROL_LEVEL_MEDIUM"
* #160376 ^property[0].code = #parent 
* #160376 ^property[0].valueCode = #_mdc 
* #160376 ^property[1].code = #hints 
* #160376 ^property[1].valueString = "PART: 2 ~ CODE10: 29304" 
* #160380 "MDC_CONC_GLU_CONTROL_LEVEL_HIGH"
* #160380 ^property[0].code = #parent 
* #160380 ^property[0].valueCode = #_mdc 
* #160380 ^property[1].code = #hints 
* #160380 ^property[1].valueString = "PART: 2 ~ CODE10: 29308" 
* #160384 "MDC_CONC_GLU_CONTROL_LEVEL_UNDETERMINED"
* #160384 ^property[0].code = #parent 
* #160384 ^property[0].valueCode = #_mdc 
* #160384 ^property[1].code = #hints 
* #160384 ^property[1].valueString = "PART: 2 ~ CODE10: 29312" 
* #16927604 "MDC_TEMP_SKIN_SETTING"
* #16927604 ^property[0].code = #parent 
* #16927604 ^property[0].valueCode = #_mdc 
* #16927604 ^property[1].code = #hints 
* #16927604 ^property[1].valueString = "PART: 258 ~ CODE10: 19316" 
* #16928802 "MDC_VENT_RESP_RATE_SETTING"
* #16928802 ^definition = Rate | Displayed or Actual | Breath | Ventilator; Setting
* #16928802 ^designation[0].language = #de-AT 
* #16928802 ^designation[0].value = "Set inflation rate" 
* #16928802 ^property[0].code = #parent 
* #16928802 ^property[0].valueCode = #_mdc 
* #16928802 ^property[1].code = #hints 
* #16928802 ^property[1].valueString = "PART: 258 ~ CODE10: 20514" 
* #16928804 "MDC_VENT_RESP_RATE_MIN_SETTING"
* #16928804 ^definition = Rate | Minimum assured | Breath | Ventilator; Setting
* #16928804 ^designation[0].language = #de-AT 
* #16928804 ^designation[0].value = "Minimum assured ventilation inflation rate setting" 
* #16928804 ^property[0].code = #parent 
* #16928804 ^property[0].valueCode = #_mdc 
* #16928804 ^property[1].code = #hints 
* #16928804 ^property[1].valueString = "PART: 258 ~ CODE10: 20516" 
* #16928805 "MDC_VENT_RESP_RATE_AVG_SETTING"
* #16928805 ^definition = Rate | Average | Breath | Ventilator; Setting
* #16928805 ^designation[0].language = #de-AT 
* #16928805 ^designation[0].value = "Average ventilation rate setting" 
* #16928805 ^property[0].code = #parent 
* #16928805 ^property[0].valueCode = #_mdc 
* #16928805 ^property[1].code = #hints 
* #16928805 ^property[1].valueString = "PART: 258 ~ CODE10: 20517" 
* #16929020 "MDC_PRESS_AWAY_END_EXP_POS_SETTING"
* #16929020 ^definition = Pressure | End-expiratory; Extrinsic | Gas | Ventilator; Setting
* #16929020 ^property[0].code = #parent 
* #16929020 ^property[0].valueCode = #_mdc 
* #16929020 ^property[1].code = #hints 
* #16929020 ^property[1].valueString = "PART: 258 ~ CODE10: 20732" 
* #16929048 "MDC_RATIO_IE_SETTING"
* #16929048 ^definition = Ratio | Duration(InspiratoryPhase); Duration(ExpiratoryPhase) | Gas | Ventilator; Setting
* #16929048 ^property[0].code = #parent 
* #16929048 ^property[0].valueCode = #_mdc 
* #16929048 ^property[1].code = #hints 
* #16929048 ^property[1].valueString = "PART: 258 ~ CODE10: 20760" 
* #16929072 "MDC_TIME_PD_APNEA_SETTING"
* #16929072 ^designation[0].language = #de-AT 
* #16929072 ^designation[0].value = "Apnea alarm duration setting" 
* #16929072 ^property[0].code = #parent 
* #16929072 ^property[0].valueCode = #_mdc 
* #16929072 ^property[1].code = #hints 
* #16929072 ^property[1].valueString = "PART: 258 ~ CODE10: 20784" 
* #16929164 "MDC_VENT_FLOW_INSP_SETTING"
* #16929164 ^definition = Flow | Inspiration | Gas | Ventilator; Setting
* #16929164 ^designation[0].language = #de-AT 
* #16929164 ^designation[0].value = "Inspiratory flow setting" 
* #16929164 ^property[0].code = #parent 
* #16929164 ^property[0].valueCode = #_mdc 
* #16929164 ^property[1].code = #hints 
* #16929164 ^property[1].valueString = "PART: 258 ~ CODE10: 20876" 
* #16929188 "MDC_VENT_PRESS_AWAY_SETTING"
* #16929188 ^definition = Pressure | | Gas | Ventilator; Airway; Setting
* #16929188 ^property[0].code = #parent 
* #16929188 ^property[0].valueCode = #_mdc 
* #16929188 ^property[1].code = #hints 
* #16929188 ^property[1].valueString = "PART: 258 ~ CODE10: 20900" 
* #16929192 "MDC_VENT_PRESS_AWAY_END_EXP_POS_SETTING"
* #16929192 ^definition = Pressure | End-expiratory; Applied | Gas | Ventilator; Setting
* #16929192 ^designation[0].language = #de-AT 
* #16929192 ^designation[0].value = "Set PEEP" 
* #16929192 ^property[0].code = #parent 
* #16929192 ^property[0].valueCode = #_mdc 
* #16929192 ^property[1].code = #hints 
* #16929192 ^property[1].valueString = "PART: 258 ~ CODE10: 20904" 
* #16929196 "MDC_VENT_VOL_TIDAL_SETTING"
* #16929196 ^definition = Volume | | Lung; Tidal | Ventilator; Setting
* #16929196 ^designation[0].language = #de-AT 
* #16929196 ^designation[0].value = "Tidal volume setting" 
* #16929196 ^property[0].code = #parent 
* #16929196 ^property[0].valueCode = #_mdc 
* #16929196 ^property[1].code = #hints 
* #16929196 ^property[1].valueString = "PART: 258 ~ CODE10: 20908" 
* #16929224 "MDC_VENT_VOL_MINUTE_AWAY_SETTING"
* #16929224 ^definition = Flow | OneMinute | Gas | Ventilator; Setting
* #16929224 ^property[0].code = #parent 
* #16929224 ^property[0].valueCode = #_mdc 
* #16929224 ^property[1].code = #hints 
* #16929224 ^property[1].valueString = "PART: 258 ~ CODE10: 20936" 
* #16929228 "MDC_VENT_VOL_MINUTE_AWAY_MAND_SETTING"
* #16929228 ^definition = Flow | OneMinute; Mandatory | Gas | Ventilator; Setting
* #16929228 ^property[0].code = #parent 
* #16929228 ^property[0].valueCode = #_mdc 
* #16929228 ^property[1].code = #hints 
* #16929228 ^property[1].valueString = "PART: 258 ~ CODE10: 20940" 
* #16929300 "MDC_CONC_AWAY_DESFL_ET_SETTING"
* #16929300 ^property[0].code = #parent 
* #16929300 ^property[0].valueCode = #_mdc 
* #16929300 ^property[1].code = #hints 
* #16929300 ^property[1].valueString = "PART: 258 ~ CODE10: 21012" 
* #16929304 "MDC_CONC_AWAY_ENFL_ET_SETTING"
* #16929304 ^property[0].code = #parent 
* #16929304 ^property[0].valueCode = #_mdc 
* #16929304 ^property[1].code = #hints 
* #16929304 ^property[1].valueString = "PART: 258 ~ CODE10: 21016" 
* #16929308 "MDC_CONC_AWAY_HALOTH_ET_SETTING"
* #16929308 ^property[0].code = #parent 
* #16929308 ^property[0].valueCode = #_mdc 
* #16929308 ^property[1].code = #hints 
* #16929308 ^property[1].valueString = "PART: 258 ~ CODE10: 21020" 
* #16929312 "MDC_CONC_AWAY_SEVOFL_ET_SETTING"
* #16929312 ^property[0].code = #parent 
* #16929312 ^property[0].valueCode = #_mdc 
* #16929312 ^property[1].code = #hints 
* #16929312 ^property[1].valueString = "PART: 258 ~ CODE10: 21024" 
* #16929316 "MDC_CONC_AWAY_ISOFL_ET_SETTING"
* #16929316 ^property[0].code = #parent 
* #16929316 ^property[0].valueCode = #_mdc 
* #16929316 ^property[1].code = #hints 
* #16929316 ^property[1].valueString = "PART: 258 ~ CODE10: 21028" 
* #16929416 "MDC_CONC_GASDLV_DESFL_INSP_SETTING"
* #16929416 ^property[0].code = #parent 
* #16929416 ^property[0].valueCode = #_mdc 
* #16929416 ^property[1].code = #hints 
* #16929416 ^property[1].valueString = "PART: 258 ~ CODE10: 21128" 
* #16929420 "MDC_CONC_GASDLV_ENFL_INSP_SETTING"
* #16929420 ^property[0].code = #parent 
* #16929420 ^property[0].valueCode = #_mdc 
* #16929420 ^property[1].code = #hints 
* #16929420 ^property[1].valueString = "PART: 258 ~ CODE10: 21132" 
* #16929424 "MDC_CONC_GASDLV_HALOTH_INSP_SETTING"
* #16929424 ^property[0].code = #parent 
* #16929424 ^property[0].valueCode = #_mdc 
* #16929424 ^property[1].code = #hints 
* #16929424 ^property[1].valueString = "PART: 258 ~ CODE10: 21136" 
* #16929428 "MDC_CONC_GASDLV_SEVOFL_INSP_SETTING"
* #16929428 ^property[0].code = #parent 
* #16929428 ^property[0].valueCode = #_mdc 
* #16929428 ^property[1].code = #hints 
* #16929428 ^property[1].valueString = "PART: 258 ~ CODE10: 21140" 
* #16929432 "MDC_CONC_GASDLV_ISOFL_INSP_SETTING"
* #16929432 ^property[0].code = #parent 
* #16929432 ^property[0].valueCode = #_mdc 
* #16929432 ^property[1].code = #hints 
* #16929432 ^property[1].valueString = "PART: 258 ~ CODE10: 21144" 
* #16929632 "MDC_VENT_TIME_PD_INSP_SETTING"
* #16929632 ^definition = Duration | Inspiratory phase | Inflation | Ventilator; Setting
* #16929632 ^designation[0].language = #de-AT 
* #16929632 ^designation[0].value = "Inspiratory time setting" 
* #16929632 ^property[0].code = #parent 
* #16929632 ^property[0].valueCode = #_mdc 
* #16929632 ^property[1].code = #hints 
* #16929632 ^property[1].valueString = "PART: 258 ~ CODE10: 21344" 
* #16929633 "MDC_VENT_TIME_PD_INSP_MAX_SETTING"
* #16929633 ^definition = Duration; maximum | Inspiratory phase | Inflation | Ventilator; Setting
* #16929633 ^designation[0].language = #de-AT 
* #16929633 ^designation[0].value = "Maximum inspiratory time setting" 
* #16929633 ^property[0].code = #parent 
* #16929633 ^property[0].valueCode = #_mdc 
* #16929633 ^property[1].code = #hints 
* #16929633 ^property[1].valueString = "PART: 258 ~ CODE10: 21345" 
* #16929644 "MDC_VENT_PRESS_TRIG_SENS_SETTING"
* #16929644 ^property[0].code = #parent 
* #16929644 ^property[0].valueCode = #_mdc 
* #16929644 ^property[1].code = #hints 
* #16929644 ^property[1].valueString = "PART: 258 ~ CODE10: 21356" 
* #16929656 "MDC_CONC_AWAY_O2_ET_SETTING"
* #16929656 ^property[0].code = #parent 
* #16929656 ^property[0].valueCode = #_mdc 
* #16929656 ^property[1].code = #hints 
* #16929656 ^property[1].valueString = "PART: 258 ~ CODE10: 21368" 
* #16929698 "MDC_VENT_RESP_BACKUP_RATE_SETTING"
* #16929698 ^definition = Rate | Backup | Breath | Ventilator; Setting
* #16929698 ^designation[0].language = #de-AT 
* #16929698 ^designation[0].value = "Backup ventilation rate setting" 
* #16929698 ^property[0].code = #parent 
* #16929698 ^property[0].valueCode = #_mdc 
* #16929698 ^property[1].code = #hints 
* #16929698 ^property[1].valueString = "PART: 258 ~ CODE10: 21410" 
* #16929820 "MDC_VENT_TIME_PD_INSP_PERCENT_SETTING"
* #16929820 ^definition = Duration; ratio | Inspiratory phase; total respiratory period | Inflation | Ventilator; Setting
* #16929820 ^designation[0].language = #de-AT 
* #16929820 ^designation[0].value = "Inspiratory time percent setting" 
* #16929820 ^property[0].code = #parent 
* #16929820 ^property[0].valueCode = #_mdc 
* #16929820 ^property[1].code = #hints 
* #16929820 ^property[1].valueString = "PART: 258 ~ CODE10: 21532" 
* #16929832 "MDC_VENT_TIME_PD_INSP_BACKUP_SETTING"
* #16929832 ^definition = Duration | Inspiratory phase | Inflation; Backup | Ventilator; Setting
* #16929832 ^designation[0].language = #de-AT 
* #16929832 ^designation[0].value = "Backup inspiratory time setting" 
* #16929832 ^property[0].code = #parent 
* #16929832 ^property[0].valueCode = #_mdc 
* #16929832 ^property[1].code = #hints 
* #16929832 ^property[1].valueString = "PART: 258 ~ CODE10: 21544" 
* #16929837 "MDC_VENT_TIME_PD_SUPP_MAX_SETTING"
* #16929837 ^definition = Duration; maximum | Inspiratory phase | Pressure support inflation | Ventilator; Setting
* #16929837 ^property[0].code = #parent 
* #16929837 ^property[0].valueCode = #_mdc 
* #16929837 ^property[1].code = #hints 
* #16929837 ^property[1].valueString = "PART: 258 ~ CODE10: 21549" 
* #16929840 "MDC_VENT_TIME_PD_INSP_PAUSE_SETTING"
* #16929840 ^definition = Duration | Inspiratory pause | end inspiratory flow to start expiratory flow | Ventilator; Setting
* #16929840 ^property[0].code = #parent 
* #16929840 ^property[0].valueCode = #_mdc 
* #16929840 ^property[1].code = #hints 
* #16929840 ^property[1].valueString = "PART: 258 ~ CODE10: 21552" 
* #16929844 "MDC_VENT_TIME_PD_INSP_PAUSE_PERCENT_SETTING"
* #16929844 ^definition = Duration; ratio | Inspiratory pause; inspiratory phase | end inspiratory flow to start expiratory flow | Ventilator; Setting
* #16929844 ^property[0].code = #parent 
* #16929844 ^property[0].valueCode = #_mdc 
* #16929844 ^property[1].code = #hints 
* #16929844 ^property[1].valueString = "PART: 258 ~ CODE10: 21556" 
* #16929848 "MDC_VENT_TIME_PD_INSP_HOLD_SETTING"
* #16929848 ^definition = Duration | Inspiratory hold | temporarily maintain constant lung volume (zero flow) at end inspiratory or inflation phase | Ventilator; Setting
* #16929848 ^property[0].code = #parent 
* #16929848 ^property[0].valueCode = #_mdc 
* #16929848 ^property[1].code = #hints 
* #16929848 ^property[1].valueString = "PART: 258 ~ CODE10: 21560" 
* #16929852 "MDC_VENT_TIME_PD_EXP_HOLD_SETTING"
* #16929852 ^definition = Duration | Expiratory hold | temporarily maintain constant lung volume (zero flow) at set extension of the expiratory phase | Ventilator; Setting
* #16929852 ^property[0].code = #parent 
* #16929852 ^property[0].valueCode = #_mdc 
* #16929852 ^property[1].code = #hints 
* #16929852 ^property[1].valueString = "PART: 258 ~ CODE10: 21564" 
* #16929860 "MDC_VENT_TIME_PD_INSP_THIGH_SETTING"
* #16929860 ^definition = Duration | High baseline pressure | APRV or Bi-Level modes | Ventilator; Setting
* #16929860 ^property[0].code = #parent 
* #16929860 ^property[0].valueCode = #_mdc 
* #16929860 ^property[1].code = #hints 
* #16929860 ^property[1].valueString = "PART: 258 ~ CODE10: 21572" 
* #16929864 "MDC_VENT_TIME_PD_EXP_TLOW_SETTING"
* #16929864 ^definition = Duration | Low baseline pressure | APRV or Bi-Level modes | Ventilator; Setting
* #16929864 ^property[0].code = #parent 
* #16929864 ^property[0].valueCode = #_mdc 
* #16929864 ^property[1].code = #hints 
* #16929864 ^property[1].valueString = "PART: 258 ~ CODE10: 21576" 
* #16929868 "MDC_VENT_FLOW_BIAS_SETTING"
* #16929868 ^definition = Flow | Bias | Gas | Ventilator; Setting
* #16929868 ^property[0].code = #parent 
* #16929868 ^property[0].valueCode = #_mdc 
* #16929868 ^property[1].code = #hints 
* #16929868 ^property[1].valueString = "PART: 258 ~ CODE10: 21580" 
* #16929872 "MDC_VENT_FLOW_CONTINUOUS_SETTING"
* #16929872 ^definition = Flow | Continuous | Gas | Ventilator; Setting
* #16929872 ^property[0].code = #parent 
* #16929872 ^property[0].valueCode = #_mdc 
* #16929872 ^property[1].code = #hints 
* #16929872 ^property[1].valueString = "PART: 258 ~ CODE10: 21584" 
* #16929932 "MDC_VENT_PRESS_AWAY_BASELINE_SETTING"
* #16929932 ^definition = Pressure; baseline | | Gas | Ventilator; Airway; Setting
* #16929932 ^property[0].code = #parent 
* #16929932 ^property[0].valueCode = #_mdc 
* #16929932 ^property[1].code = #hints 
* #16929932 ^property[1].valueString = "PART: 258 ~ CODE10: 21644" 
* #16929936 "MDC_VENT_PRESS_AWAY_DELTA_SETTING"
* #16929936 ^definition = Pressure; delta relative to baseline | | Gas | Ventilator; Airway; Setting
* #16929936 ^property[0].code = #parent 
* #16929936 ^property[0].valueCode = #_mdc 
* #16929936 ^property[1].code = #hints 
* #16929936 ^property[1].valueString = "PART: 258 ~ CODE10: 21648" 
* #16929940 "MDC_VENT_PRESS_AWAY_BACKUP_SETTING"
* #16929940 ^definition = Pressure | Inspiratory phase | Inflation; Backup | Ventilator; Airway; Setting
* #16929940 ^property[0].code = #parent 
* #16929940 ^property[0].valueCode = #_mdc 
* #16929940 ^property[1].code = #hints 
* #16929940 ^property[1].valueString = "PART: 258 ~ CODE10: 21652" 
* #16929944 "MDC_VENT_PRESS_AWAY_DELTA_BACKUP_SETTING"
* #16929944 ^definition = Pressure; delta relative to baseline | Inspiratory phase | Inflation; Backup | Ventilator; Airway; Setting
* #16929944 ^property[0].code = #parent 
* #16929944 ^property[0].valueCode = #_mdc 
* #16929944 ^property[1].code = #hints 
* #16929944 ^property[1].valueString = "PART: 258 ~ CODE10: 21656" 
* #16929948 "MDC_VENT_PRESS_AWAY_SUPP_SETTING"
* #16929948 ^definition = Pressure | Inspiratory phase | Pressure support inflation | Ventilator; Airway; Setting
* #16929948 ^property[0].code = #parent 
* #16929948 ^property[0].valueCode = #_mdc 
* #16929948 ^property[1].code = #hints 
* #16929948 ^property[1].valueString = "PART: 258 ~ CODE10: 21660" 
* #16929952 "MDC_VENT_PRESS_AWAY_DELTA_SUPP_SETTING"
* #16929952 ^definition = Pressure; delta relative to baseline | Inspiratory phase | Pressure support inflation | Ventilator; Airway; Setting
* #16929952 ^property[0].code = #parent 
* #16929952 ^property[0].valueCode = #_mdc 
* #16929952 ^property[1].code = #hints 
* #16929952 ^property[1].valueString = "PART: 258 ~ CODE10: 21664" 
* #16929956 "MDC_VENT_PRESS_AWAY_INSP_PHIGH_SETTING"
* #16929956 ^definition = Pressure | High baseline pressure | APRV or Bi-Level modes | Ventilator; Airway; Setting
* #16929956 ^property[0].code = #parent 
* #16929956 ^property[0].valueCode = #_mdc 
* #16929956 ^property[1].code = #hints 
* #16929956 ^property[1].valueString = "PART: 258 ~ CODE10: 21668" 
* #16929960 "MDC_VENT_PRESS_AWAY_EXP_PLOW_SETTING"
* #16929960 ^definition = Pressure | Low baseline pressure | APRV or Bi-Level modes | Ventilator; Airway; Setting
* #16929960 ^property[0].code = #parent 
* #16929960 ^property[0].valueCode = #_mdc 
* #16929960 ^property[1].code = #hints 
* #16929960 ^property[1].valueString = "PART: 258 ~ CODE10: 21672" 
* #16929964 "MDC_VENT_PRESS_AWAY_LIMIT_SETTING"
* #16929964 ^definition = Pressure; limit | without cycling | Gas | Ventilator; Airway; Setting
* #16929964 ^property[0].code = #parent 
* #16929964 ^property[0].valueCode = #_mdc 
* #16929964 ^property[1].code = #hints 
* #16929964 ^property[1].valueString = "PART: 258 ~ CODE10: 21676" 
* #16929968 "MDC_VENT_PRESS_AWAY_LIMIT_PMAX_SETTING"
* #16929968 ^definition = Pressure; maximum limit | with cycling | Gas | Ventilator; Airway; Setting
* #16929968 ^property[0].code = #parent 
* #16929968 ^property[0].valueCode = #_mdc 
* #16929968 ^property[1].code = #hints 
* #16929968 ^property[1].valueString = "PART: 258 ~ CODE10: 21680" 
* #16929972 "MDC_VENT_PRESS_AWAY_LIMIT_RELIEF_SETTING"
* #16929972 ^definition = Pressure; protective relief limit | | Gas | Ventilator; Airway; Setting
* #16929972 ^property[0].code = #parent 
* #16929972 ^property[0].valueCode = #_mdc 
* #16929972 ^property[1].code = #hints 
* #16929972 ^property[1].valueString = "PART: 258 ~ CODE10: 21684" 
* #16929976 "MDC_VENT_PRESS_AWAY_LIMIT_PMIN_SETTING"
* #16929976 ^definition = Pressure; minimum limit | | Gas | Ventilator; Airway; Setting
* #16929976 ^property[0].code = #parent 
* #16929976 ^property[0].valueCode = #_mdc 
* #16929976 ^property[1].code = #hints 
* #16929976 ^property[1].valueString = "PART: 258 ~ CODE10: 21688" 
* #16929980 "MDC_VENT_PRESS_AWAY_DELTA_LIMIT_PMIN_SETTING"
* #16929980 ^definition = Pressure; delta relative to baseline; minimum limit | | Gas | Ventilator; Airway; Setting
* #16929980 ^property[0].code = #parent 
* #16929980 ^property[0].valueCode = #_mdc 
* #16929980 ^property[1].code = #hints 
* #16929980 ^property[1].valueString = "PART: 258 ~ CODE10: 21692" 
* #16929984 "MDC_VENT_PRESS_AWAY_RISETIME_CTLD_SETTING"
* #16929984 ^definition = Duration; pressure; risetime | controlled inflations | Gas | Ventilator; Airway; Setting
* #16929984 ^property[0].code = #parent 
* #16929984 ^property[0].valueCode = #_mdc 
* #16929984 ^property[1].code = #hints 
* #16929984 ^property[1].valueString = "PART: 258 ~ CODE10: 21696" 
* #16929988 "MDC_VENT_PRESS_AWAY_RISETIME_SUPP_SETTING"
* #16929988 ^definition = Duration; pressure; risetime | support inflations | Gas | Ventilator; Airway; Setting
* #16929988 ^property[0].code = #parent 
* #16929988 ^property[0].valueCode = #_mdc 
* #16929988 ^property[1].code = #hints 
* #16929988 ^property[1].valueString = "PART: 258 ~ CODE10: 21700" 
* #16930020 "MDC_VENT_FLOW_TRIG_SENS_SETTING"
* #16930020 ^property[0].code = #parent 
* #16930020 ^property[0].valueCode = #_mdc 
* #16930020 ^property[1].code = #hints 
* #16930020 ^property[1].valueString = "PART: 258 ~ CODE10: 21732" 
* #16930024 "MDC_VENT_FLOW_THRESH_END_INSP_SETTING"
* #16930024 ^property[0].code = #parent 
* #16930024 ^property[0].valueCode = #_mdc 
* #16930024 ^property[1].code = #hints 
* #16930024 ^property[1].valueString = "PART: 258 ~ CODE10: 21736" 
* #16930092 "MDC_FLOW_AIR_FG_SETTING"
* #16930092 ^property[0].code = #parent 
* #16930092 ^property[0].valueCode = #_mdc 
* #16930092 ^property[1].code = #hints 
* #16930092 ^property[1].valueString = "PART: 258 ~ CODE10: 21804" 
* #16930308 "MDC_FLOW_N2O_FG_SETTING"
* #16930308 ^property[0].code = #parent 
* #16930308 ^property[0].valueCode = #_mdc 
* #16930308 ^property[1].code = #hints 
* #16930308 ^property[1].valueString = "PART: 258 ~ CODE10: 22020" 
* #16930360 "MDC_CONC_GASDLV_O2_INSP_SETTING"
* #16930360 ^property[0].code = #parent 
* #16930360 ^property[0].valueCode = #_mdc 
* #16930360 ^property[1].code = #hints 
* #16930360 ^property[1].valueString = "PART: 258 ~ CODE10: 22072" 
* #16930372 "MDC_FLOW_O2_FG_SETTING"
* #16930372 ^property[0].code = #parent 
* #16930372 ^property[0].valueCode = #_mdc 
* #16930372 ^property[1].code = #hints 
* #16930372 ^property[1].valueString = "PART: 258 ~ CODE10: 22084" 
* #16930432 "MDC_VENT_VOL_TIDAL_BACKUP_SETTING"
* #16930432 ^definition = Volume | | Lung; Tidal; Inflation; Backup | Ventilator; Setting
* #16930432 ^designation[0].language = #de-AT 
* #16930432 ^designation[0].value = "Backup tidal volume setting" 
* #16930432 ^property[0].code = #parent 
* #16930432 ^property[0].valueCode = #_mdc 
* #16930432 ^property[1].code = #hints 
* #16930432 ^property[1].valueString = "PART: 258 ~ CODE10: 22144" 
* #16930436 "MDC_VENT_VOL_TIDAL_INSP_SETTING"
* #16930436 ^definition = Volume | | Lung; Tidal; Inspiratory Phase | Ventilator; Setting
* #16930436 ^designation[0].language = #de-AT 
* #16930436 ^designation[0].value = "Inspired tidal volume setting" 
* #16930436 ^property[0].code = #parent 
* #16930436 ^property[0].valueCode = #_mdc 
* #16930436 ^property[1].code = #hints 
* #16930436 ^property[1].valueString = "PART: 258 ~ CODE10: 22148" 
* #16930438 "MDC_VENT_VOL_TIDAL_INSP_MIN_SETTING"
* #16930438 ^definition = Volume; minimum | | Lung; Tidal; Inspiratory Phase | Ventilator; Setting
* #16930438 ^designation[0].language = #de-AT 
* #16930438 ^designation[0].value = "Minimum inspired tidal volume setting" 
* #16930438 ^property[0].code = #parent 
* #16930438 ^property[0].valueCode = #_mdc 
* #16930438 ^property[1].code = #hints 
* #16930438 ^property[1].valueString = "PART: 258 ~ CODE10: 22150" 
* #16930448 "MDC_VOL_MINUTE_AWAY_IBW_PCTOF_REF_SETTING"
* #16930448 ^definition = Percent | MinuteVolume; desired percentage value relative to reference value based on ideal body mass | Lung; Tidal | airway; total for all breath and inflation types
* #16930448 ^designation[0].language = #de-AT 
* #16930448 ^designation[0].value = "Percentage of the reference minute volume calculated for the patient's ideal body mass" 
* #16930448 ^property[0].code = #parent 
* #16930448 ^property[0].valueCode = #_mdc 
* #16930448 ^property[1].code = #hints 
* #16930448 ^property[1].valueString = "PART: 258 ~ CODE10: 22160" 
* #16930476 "MDC_VENT_PRESS_AWAY_RISETIME_CTLD_PERCENT_SETTING"
* #16930476 ^definition = Duration; ratio; pressure; risetime | controlled inflations | Gas | Ventilator; Airway; Setting
* #16930476 ^property[0].code = #parent 
* #16930476 ^property[0].valueCode = #_mdc 
* #16930476 ^property[1].code = #hints 
* #16930476 ^property[1].valueString = "PART: 258 ~ CODE10: 22188" 
* #16930484 "MDC_VENT_PRESS_AWAY_RISETIME_SUPP_PERCENT_SETTING"
* #16930484 ^definition = Duration; ratio; pressure; risetime | support inflations | Gas | Ventilator; Airway; Setting
* #16930484 ^property[0].code = #parent 
* #16930484 ^property[0].valueCode = #_mdc 
* #16930484 ^property[1].code = #hints 
* #16930484 ^property[1].valueString = "PART: 258 ~ CODE10: 22196" 
* #16930488 "MDC_VENT_FLOW_THRESH_END_INSP_PERCENT_SETTING"
* #16930488 ^property[0].code = #parent 
* #16930488 ^property[0].valueCode = #_mdc 
* #16930488 ^property[1].code = #hints 
* #16930488 ^property[1].valueString = "PART: 258 ~ CODE10: 22200" 
* #16930560 "MDC_NEB_TIME_PD_PER_CYCLE_SETTING"
* #16930560 ^definition = Duration | Delivery | Nebulization of fluid; per cycle | Nebulizer; setting
* #16930560 ^designation[0].language = #de-AT 
* #16930560 ^designation[0].value = "(set) nebulizer run time" 
* #16930560 ^property[0].code = #parent 
* #16930560 ^property[0].valueCode = #_mdc 
* #16930560 ^property[1].code = #hints 
* #16930560 ^property[1].valueString = "PART: 258 ~ CODE10: 22272" 
* #16930564 "MDC_NEB_VOL_FLUID_PER_CYCLE_SETTING"
* #16930564 ^definition = Volume | Delivery | Fluid to be nebulized; per cycle | Nebulizer; setting
* #16930564 ^designation[0].language = #de-AT 
* #16930564 ^designation[0].value = "(set) nebulizer volume" 
* #16930564 ^property[0].code = #parent 
* #16930564 ^property[0].valueCode = #_mdc 
* #16930564 ^property[1].code = #hints 
* #16930564 ^property[1].valueString = "PART: 258 ~ CODE10: 22276" 
* #16930568 "MDC_NEB_CYCLES_SETTING"
* #16930568 ^definition = Count; cycles | Nebulization delivery cycles | Nebulization of fluid | Nebulizer; setting
* #16930568 ^designation[0].language = #de-AT 
* #16930568 ^designation[0].value = "(set) nebulizer cycles" 
* #16930568 ^property[0].code = #parent 
* #16930568 ^property[0].valueCode = #_mdc 
* #16930568 ^property[1].code = #hints 
* #16930568 ^property[1].valueString = "PART: 258 ~ CODE10: 22280" 
* #16930572 "MDC_NEB_TIME_PD_PAUSE_SETTING"
* #16930572 ^definition = Duration | Inter-delivery | Pause between nebulization deliveries | Nebulizer; setting
* #16930572 ^designation[0].language = #de-AT 
* #16930572 ^designation[0].value = "(set) nebulizer pause time" 
* #16930572 ^property[0].code = #parent 
* #16930572 ^property[0].valueCode = #_mdc 
* #16930572 ^property[1].code = #hints 
* #16930572 ^property[1].valueString = "PART: 258 ~ CODE10: 22284" 
* #16930604 "MDC_VOL_AWAY_TIDAL_PER_IBW_SETTING"
* #16930604 ^property[0].code = #parent 
* #16930604 ^property[0].valueCode = #_mdc 
* #16930604 ^property[1].code = #hints 
* #16930604 ^property[1].valueString = "PART: 258 ~ CODE10: 22316" 
* #16961504 "MDC_CONC_O2_MICROENV_SETTING"
* #16961504 ^property[0].code = #parent 
* #16961504 ^property[0].valueCode = #_mdc 
* #16961504 ^property[1].code = #hints 
* #16961504 ^property[1].valueString = "PART: 258 ~ CODE10: 53216" 
* #16961508 "MDC_REL_HUMIDITY_MICROENV_SETTING"
* #16961508 ^property[0].code = #parent 
* #16961508 ^property[0].valueCode = #_mdc 
* #16961508 ^property[1].code = #hints 
* #16961508 ^property[1].valueString = "PART: 258 ~ CODE10: 53220" 
* #16961512 "MDC_TEMP_MICROENV_SETTING"
* #16961512 ^property[0].code = #parent 
* #16961512 ^property[0].valueCode = #_mdc 
* #16961512 ^property[1].code = #hints 
* #16961512 ^property[1].valueString = "PART: 258 ~ CODE10: 53224" 
* #16961620 "MDC_GASDLV_AGENT_SETTING"
* #16961620 ^definition = Identity | | Agent(s); Gas | Airway; Setting or selection
* #16961620 ^property[0].code = #parent 
* #16961620 ^property[0].valueCode = #_mdc 
* #16961620 ^property[1].code = #hints 
* #16961620 ^property[1].valueString = "PART: 258 ~ CODE10: 53332" 
* #16961621 "MDC_GASDLV_BALANCE_GAS_SETTING"
* #16961621 ^definition = Identity | | Balance Gas(es); Gas | Airway; Setting or selection
* #16961621 ^property[0].code = #parent 
* #16961621 ^property[0].valueCode = #_mdc 
* #16961621 ^property[1].code = #hints 
* #16961621 ^property[1].valueString = "PART: 258 ~ CODE10: 53333" 
* #16961626 "MDC_NEB_DEV_MODE_SETTING"
* #16961626 ^definition = Mode |  | Nebulization Mode | Nebulizer
* #16961626 ^designation[0].language = #de-AT 
* #16961626 ^designation[0].value = "(set) nebulizer mode" 
* #16961626 ^property[0].code = #parent 
* #16961626 ^property[0].valueCode = #_mdc 
* #16961626 ^property[1].code = #hints 
* #16961626 ^property[1].valueString = "PART: 258 ~ CODE10: 53338" 
* #184288 "MDC_CONC_O2_MICROENV"
* #184288 ^definition = Concentration | PartialPressure | Oxygen; Gas | Incubator; Microenvironment
* #184288 ^designation[0].language = #de-AT 
* #184288 ^designation[0].value = "Oxygen Reading" 
* #184288 ^property[0].code = #parent 
* #184288 ^property[0].valueCode = #_mdc 
* #184288 ^property[1].code = #hints 
* #184288 ^property[1].valueString = "PART: 2 ~ CODE10: 53216" 
* #184292 "MDC_REL_HUMIDITY_MICROENV"
* #184292 ^definition = Humidity; Relative | | Compartment | Incubator; Microenvironment
* #184292 ^designation[0].language = #de-AT 
* #184292 ^designation[0].value = "Measured Relative Humidity" 
* #184292 ^property[0].code = #parent 
* #184292 ^property[0].valueCode = #_mdc 
* #184292 ^property[1].code = #hints 
* #184292 ^property[1].valueString = "PART: 2 ~ CODE10: 53220" 
* #184296 "MDC_TEMP_MICROENV"
* #184296 ^definition = Temperature | | Compartment | Incubator; Microenvironment
* #184296 ^designation[0].language = #de-AT 
* #184296 ^designation[0].value = "Compartment Probe Temperature" 
* #184296 ^property[0].code = #parent 
* #184296 ^property[0].valueCode = #_mdc 
* #184296 ^property[1].code = #hints 
* #184296 ^property[1].valueString = "PART: 2 ~ CODE10: 53224" 
* #184300 "MDC_MICROENV_HEATER_APPLIED_PWR"
* #184300 ^definition = Power; Relative | Heater | | Incubator or Warmer; Microenvironment
* #184300 ^designation[0].language = #de-AT 
* #184300 ^designation[0].value = "Heater Power" 
* #184300 ^property[0].code = #parent 
* #184300 ^property[0].valueCode = #_mdc 
* #184300 ^property[1].code = #hints 
* #184300 ^property[1].valueString = "PART: 2 ~ CODE10: 53228" 
* #184304 "MDC_MICROENV_HEATER_HEAT_SINK_RESIST"
* #184304 ^definition = Resistance | Temperature-related | Heat Sink; Heater | Incubator; Microenvironment
* #184304 ^designation[0].language = #de-AT 
* #184304 ^designation[0].value = "Incubator heater heat sink temperature sensor resistance" 
* #184304 ^property[0].code = #parent 
* #184304 ^property[0].valueCode = #_mdc 
* #184304 ^property[1].code = #hints 
* #184304 ^property[1].valueString = "PART: 2 ~ CODE10: 53232" 
* #184308 "MDC_MICROENV_HEATER_HEAT_SINK_TEMP"
* #184308 ^definition = Temperature | | Heat Sink; Heater | Incubator; Microenvironment
* #184308 ^designation[0].language = #de-AT 
* #184308 ^designation[0].value = "Incubator heater heat sink temperature" 
* #184308 ^property[0].code = #parent 
* #184308 ^property[0].valueCode = #_mdc 
* #184308 ^property[1].code = #hints 
* #184308 ^property[1].valueString = "PART: 2 ~ CODE10: 53236" 
* #184326 "MDC_ECG_STAT_ECT"
* #184326 ^property[0].code = #parent 
* #184326 ^property[0].valueCode = #_mdc 
* #184326 ^property[1].code = #hints 
* #184326 ^property[1].valueString = "PART: 2 ~ CODE10: 53254" 
* #184327 "MDC_ECG_STAT_RHY"
* #184327 ^property[0].code = #parent 
* #184327 ^property[0].valueCode = #_mdc 
* #184327 ^property[1].code = #hints 
* #184327 ^property[1].valueString = "PART: 2 ~ CODE10: 53255" 
* #184331 "MDC_TRIG_BEAT_MAX_INRUSH"
* #184331 ^designation[0].language = #de-AT 
* #184331 ^designation[0].value = "SpO2 precise pulse" 
* #184331 ^property[0].code = #parent 
* #184331 ^property[0].valueCode = #_mdc 
* #184331 ^property[1].code = #hints 
* #184331 ^property[1].valueString = "PART: 2 ~ CODE10: 53259" 
* #184336 "MDC_MICROENV_TYPE"
* #184336 ^definition = Type | Bed | | Incubator or Warmer; Microenvironment
* #184336 ^designation[0].language = #de-AT 
* #184336 ^designation[0].value = "Microenvironment Bed Type" 
* #184336 ^property[0].code = #parent 
* #184336 ^property[0].valueCode = #_mdc 
* #184336 ^property[1].code = #hints 
* #184336 ^property[1].valueString = "PART: 2 ~ CODE10: 53264" 
* #184337 "MDC_MICROENV_HEATER_TYPE"
* #184337 ^definition = Type | Heater | | Incubator or Warmer; Microenvironment
* #184337 ^designation[0].language = #de-AT 
* #184337 ^designation[0].value = "Incubator heater type" 
* #184337 ^property[0].code = #parent 
* #184337 ^property[0].valueCode = #_mdc 
* #184337 ^property[1].code = #hints 
* #184337 ^property[1].valueString = "PART: 2 ~ CODE10: 53265" 
* #184338 "MDC_MICROENV_BED_STATE"
* #184338 ^definition = State; Bed | | State; open or closed | Incubator; Microenvironment
* #184338 ^designation[0].language = #de-AT 
* #184338 ^designation[0].value = "Bed State" 
* #184338 ^property[0].code = #parent 
* #184338 ^property[0].valueCode = #_mdc 
* #184338 ^property[1].code = #hints 
* #184338 ^property[1].valueString = "PART: 2 ~ CODE10: 53266" 
* #184339 "MDC_MICROENV_AIR_CURTAIN_STATE"
* #184339 ^definition = Status; Operational | Air curtain | Airflow | Incubator; Microenvironment
* #184339 ^designation[0].language = #de-AT 
* #184339 ^designation[0].value = "Air Curtain Status" 
* #184339 ^property[0].code = #parent 
* #184339 ^property[0].valueCode = #_mdc 
* #184339 ^property[1].code = #hints 
* #184339 ^property[1].valueString = "PART: 2 ~ CODE10: 53267" 
* #184340 "MDC_MICROENV_HEATER_CNTRL_MODE"
* #184340 ^definition = Mode | Temperature Control | | Incubator or Warmer; Microenvironment
* #184340 ^designation[0].language = #de-AT 
* #184340 ^designation[0].value = "Temperature Control Mode" 
* #184340 ^property[0].code = #parent 
* #184340 ^property[0].valueCode = #_mdc 
* #184340 ^property[1].code = #hints 
* #184340 ^property[1].valueString = "PART: 2 ~ CODE10: 53268" 
* #184341 "MDC_MICROENV_FAN_SPEED"
* #184341 ^definition = Fan speed | Air curtain | Airflow | Incubator
* #184341 ^designation[0].language = #de-AT 
* #184341 ^designation[0].value = "Fan Speed" 
* #184341 ^property[0].code = #parent 
* #184341 ^property[0].valueCode = #_mdc 
* #184341 ^property[1].code = #hints 
* #184341 ^property[1].valueString = "PART: 2 ~ CODE10: 53269" 
* #184352 "MDC_VENT_MODE"
* #184352 ^definition = Mode | | VentilationMode | Ventilator
* #184352 ^designation[0].language = #de-AT 
* #184352 ^designation[0].value = "Ventilation mode" 
* #184352 ^property[0].code = #parent 
* #184352 ^property[0].valueCode = #_mdc 
* #184352 ^property[1].code = #hints 
* #184352 ^property[1].valueString = "PART: 2 ~ CODE10: 53280" 
* #184353 "MDC_VENT_MODE_RESP_SPONT"
* #184353 ^definition = Mode | | BreathingMode | Respiration
* #184353 ^designation[0].language = #de-AT 
* #184353 ^designation[0].value = "Respiration mode" 
* #184353 ^property[0].code = #parent 
* #184353 ^property[0].valueCode = #_mdc 
* #184353 ^property[1].code = #hints 
* #184353 ^property[1].valueString = "PART: 2 ~ CODE10: 53281" 
* #184400 "MDC_VENT_MODE_BACKUP"
* #184400 ^definition = Mode | | VentilationMode; Backup | Ventilator
* #184400 ^designation[0].language = #de-AT 
* #184400 ^designation[0].value = "Backup ventilation mode" 
* #184400 ^property[0].code = #parent 
* #184400 ^property[0].valueCode = #_mdc 
* #184400 ^property[1].code = #hints 
* #184400 ^property[1].valueString = "PART: 2 ~ CODE10: 53328" 
* #184404 "MDC_GASDLV_AGENT"
* #184404 ^definition = Identity | | Agent(s); Gas | Airway
* #184404 ^designation[0].language = #de-AT 
* #184404 ^designation[0].value = "Anesthetic agent(s)" 
* #184404 ^property[0].code = #parent 
* #184404 ^property[0].valueCode = #_mdc 
* #184404 ^property[1].code = #hints 
* #184404 ^property[1].valueString = "PART: 2 ~ CODE10: 53332" 
* #184405 "MDC_GASDLV_BALANCE_GAS"
* #184405 ^definition = Identity | | Balance Gas(es); Gas | Airway
* #184405 ^designation[0].language = #de-AT 
* #184405 ^designation[0].value = "Balance Gas" 
* #184405 ^property[0].code = #parent 
* #184405 ^property[0].valueCode = #_mdc 
* #184405 ^property[1].code = #hints 
* #184405 ^property[1].valueString = "PART: 2 ~ CODE10: 53333" 
* #184408 "MDC_NEB_DEV_TYPE"
* #184408 ^definition = Type | | Device | Nebulizer
* #184408 ^designation[0].language = #de-AT 
* #184408 ^designation[0].value = "Nebulizer type" 
* #184408 ^property[0].code = #parent 
* #184408 ^property[0].valueCode = #_mdc 
* #184408 ^property[1].code = #hints 
* #184408 ^property[1].valueString = "PART: 2 ~ CODE10: 53336" 
* #184409 "MDC_NEB_DEV_STATUS"
* #184409 ^definition = Status | Operational | Device | Nebulizer
* #184409 ^designation[0].language = #de-AT 
* #184409 ^designation[0].value = "Nebulizer status" 
* #184409 ^property[0].code = #parent 
* #184409 ^property[0].valueCode = #_mdc 
* #184409 ^property[1].code = #hints 
* #184409 ^property[1].valueString = "PART: 2 ~ CODE10: 53337" 
* #184476 "MDC_SYRINGE_TYPE"
* #184476 ^definition = Type |  | Syringe | Pump
* #184476 ^designation[0].language = #de-AT 
* #184476 ^designation[0].value = "Syringe type" 
* #184476 ^property[0].code = #parent 
* #184476 ^property[0].valueCode = #_mdc 
* #184476 ^property[1].code = #hints 
* #184476 ^property[1].valueString = "PART: 2 ~ CODE10: 53404" 
* #184488 "MDC_SYRINGE_MANUFACTURER"
* #184488 ^property[0].code = #parent 
* #184488 ^property[0].valueCode = #_mdc 
* #184488 ^property[1].code = #hints 
* #184488 ^property[1].valueString = "PART: 2 ~ CODE10: 53416" 
* #184513 "MDC_PUMP_NOT_INFUSING_REASON"
* #184513 ^property[0].code = #parent 
* #184513 ^property[0].valueCode = #_mdc 
* #184513 ^property[1].code = #hints 
* #184513 ^property[1].valueString = "PART: 2 ~ CODE10: 53441" 
* #184514 "MDC_DRUG_NAME_LABEL"
* #184514 ^property[0].code = #parent 
* #184514 ^property[0].valueCode = #_mdc 
* #184514 ^property[1].code = #hints 
* #184514 ^property[1].valueString = "PART: 2 ~ CODE10: 53442" 
* #184515 "MDC_DRUG_ID"
* #184515 ^property[0].code = #parent 
* #184515 ^property[0].valueCode = #_mdc 
* #184515 ^property[1].code = #hints 
* #184515 ^property[1].valueString = "PART: 2 ~ CODE10: 53443" 
* #184516 "MDC_PUMP_DRUG_LIBRARY_CARE_AREA"
* #184516 ^property[0].code = #parent 
* #184516 ^property[0].valueCode = #_mdc 
* #184516 ^property[1].code = #hints 
* #184516 ^property[1].valueString = "PART: 2 ~ CODE10: 53444" 
* #184517 "MDC_PUMP_DRUG_LIBRARY_VERSION"
* #184517 ^property[0].code = #parent 
* #184517 ^property[0].valueCode = #_mdc 
* #184517 ^property[1].code = #hints 
* #184517 ^property[1].valueString = "PART: 2 ~ CODE10: 53445" 
* #184518 "MDC_PUMP_MODE_CONCURRENT_CHAN_LABEL"
* #184518 ^property[0].code = #parent 
* #184518 ^property[0].valueCode = #_mdc 
* #184518 ^property[1].code = #hints 
* #184518 ^property[1].valueString = "PART: 2 ~ CODE10: 53446" 
* #184519 "MDC_PUMP_INFUSING_STATUS"
* #184519 ^property[0].code = #parent 
* #184519 ^property[0].valueCode = #_mdc 
* #184519 ^property[1].code = #hints 
* #184519 ^property[1].valueString = "PART: 2 ~ CODE10: 53447" 
* #188420 "MDC_TEMP_RECT"
* #188420 ^definition = Temperature |  | Rectal | Body
* #188420 ^designation[0].language = #de-AT 
* #188420 ^designation[0].value = "Rectal temperature " 
* #188420 ^property[0].code = #parent 
* #188420 ^property[0].valueCode = #_mdc 
* #188420 ^property[1].code = #hints 
* #188420 ^property[1].valueString = "PART: 2 ~ CODE10: 57348" 
* #188424 "MDC_TEMP_ORAL"
* #188424 ^definition = Temperature |  | Oral | Body
* #188424 ^designation[0].language = #de-AT 
* #188424 ^designation[0].value = "Oral temperature" 
* #188424 ^property[0].code = #parent 
* #188424 ^property[0].valueCode = #_mdc 
* #188424 ^property[1].code = #hints 
* #188424 ^property[1].valueString = "PART: 2 ~ CODE10: 57352" 
* #188428 "MDC_TEMP_EAR"
* #188428 ^definition = Temperature |  | Ear | Body
* #188428 ^designation[0].language = #de-AT 
* #188428 ^designation[0].value = "Ear temperature" 
* #188428 ^property[0].code = #parent 
* #188428 ^property[0].valueCode = #_mdc 
* #188428 ^property[1].code = #hints 
* #188428 ^property[1].valueString = "PART: 2 ~ CODE10: 57356" 
* #188432 "MDC_TEMP_FINGER"
* #188432 ^definition = Temperature |  | Finger | Body
* #188432 ^designation[0].language = #de-AT 
* #188432 ^designation[0].value = "Finger temperature" 
* #188432 ^property[0].code = #parent 
* #188432 ^property[0].valueCode = #_mdc 
* #188432 ^property[1].code = #hints 
* #188432 ^property[1].valueString = "PART: 2 ~ CODE10: 57360" 
* #188436 "MDC_TEMP_BLD"
* #188436 ^definition = Temperature |  | Blood | Body
* #188436 ^designation[0].language = #de-AT 
* #188436 ^designation[0].value = "Blood temperature" 
* #188436 ^property[0].code = #parent 
* #188436 ^property[0].valueCode = #_mdc 
* #188436 ^property[1].code = #hints 
* #188436 ^property[1].valueString = "PART: 2 ~ CODE10: 57364" 
* #188440 "MDC_TEMP_DIFF"
* #188440 ^definition = Temperature | Difference | Core; Surface | Body
* #188440 ^designation[0].language = #de-AT 
* #188440 ^designation[0].value = "Temperature difference" 
* #188440 ^property[0].code = #parent 
* #188440 ^property[0].valueCode = #_mdc 
* #188440 ^property[1].code = #hints 
* #188440 ^property[1].valueString = "PART: 2 ~ CODE10: 57368" 
* #188448 "MDC_TEMP_TOE"
* #188448 ^definition = Temperature |  | Toe | Body
* #188448 ^designation[0].language = #de-AT 
* #188448 ^designation[0].value = "Toe surface temperature" 
* #188448 ^property[0].code = #parent 
* #188448 ^property[0].valueCode = #_mdc 
* #188448 ^property[1].code = #hints 
* #188448 ^property[1].valueString = "PART: 2 ~ CODE10: 57376" 
* #188452 "MDC_TEMP_AXILLA"
* #188452 ^definition = Temperature | | Axillary | Body
* #188452 ^designation[0].language = #de-AT 
* #188452 ^designation[0].value = "Axillary (armpit) temperature" 
* #188452 ^property[0].code = #parent 
* #188452 ^property[0].valueCode = #_mdc 
* #188452 ^property[1].code = #hints 
* #188452 ^property[1].valueString = "PART: 2 ~ CODE10: 57380" 
* #188456 "MDC_TEMP_GIT"
* #188456 ^definition = Temperature | | GIT | Body
* #188456 ^designation[0].language = #de-AT 
* #188456 ^designation[0].value = "Gastro-intestinal tract temperature" 
* #188456 ^property[0].code = #parent 
* #188456 ^property[0].valueCode = #_mdc 
* #188456 ^property[1].code = #hints 
* #188456 ^property[1].valueString = "PART: 2 ~ CODE10: 57384" 
* #188480 "MDC_PRESS_GI"
* #188480 ^definition = Pressure |  | Gastric | GastrointestinalSystem
* #188480 ^designation[0].language = #de-AT 
* #188480 ^designation[0].value = "Gastric pressure" 
* #188480 ^property[0].code = #parent 
* #188480 ^property[0].valueCode = #_mdc 
* #188480 ^property[1].code = #hints 
* #188480 ^property[1].valueString = "PART: 2 ~ CODE10: 57408" 
* #188488 "MDC_POWER_TCUT"
* #188488 ^definition = Power | | Trancutaneous probe | Heater
* #188488 ^designation[0].language = #de-AT 
* #188488 ^designation[0].value = "Transcutaneous probe heater power" 
* #188488 ^property[0].code = #parent 
* #188488 ^property[0].valueCode = #_mdc 
* #188488 ^property[1].code = #hints 
* #188488 ^property[1].valueString = "PART: 2 ~ CODE10: 57416" 
* #188492 "MDC_TEMP_TCUT"
* #188492 ^definition = Temperature | | Transcutaneous probe | Sensor
* #188492 ^designation[0].language = #de-AT 
* #188492 ^designation[0].value = "Transcutaneous probe temperature" 
* #188492 ^property[0].code = #parent 
* #188492 ^property[0].valueCode = #_mdc 
* #188492 ^property[1].code = #hints 
* #188492 ^property[1].valueString = "PART: 2 ~ CODE10: 57420" 
* #188496 "MDC_TEMP_AXIL"
* #188496 ^definition = Temperature | | Axillary | Body
* #188496 ^designation[0].language = #de-AT 
* #188496 ^designation[0].value = "Axillary temperature" 
* #188496 ^property[0].code = #parent 
* #188496 ^property[0].valueCode = #_mdc 
* #188496 ^property[1].code = #hints 
* #188496 ^property[1].valueString = "PART: 2 ~ CODE10: 57424" 
* #188500 "MDC_TEMP_MYO"
* #188500 ^definition = Temperature | | Myocardial | Heart; CVS; Body
* #188500 ^designation[0].language = #de-AT 
* #188500 ^designation[0].value = "Myocardial temperature" 
* #188500 ^property[0].code = #parent 
* #188500 ^property[0].valueCode = #_mdc 
* #188500 ^property[1].code = #hints 
* #188500 ^property[1].valueString = "PART: 2 ~ CODE10: 57428" 
* #188504 "MDC_TEMP_NASAL"
* #188504 ^definition = Temperature | | Nasal | Body
* #188504 ^designation[0].language = #de-AT 
* #188504 ^designation[0].value = "Nasal temperature" 
* #188504 ^property[0].code = #parent 
* #188504 ^property[0].valueCode = #_mdc 
* #188504 ^property[1].code = #hints 
* #188504 ^property[1].valueString = "PART: 2 ~ CODE10: 57432" 
* #188508 "MDC_TEMP_ROOM"
* #188508 ^definition = Temperature | | Room | Ambient
* #188508 ^designation[0].language = #de-AT 
* #188508 ^designation[0].value = "Room temperature" 
* #188508 ^property[0].code = #parent 
* #188508 ^property[0].valueCode = #_mdc 
* #188508 ^property[1].code = #hints 
* #188508 ^property[1].valueString = "PART: 2 ~ CODE10: 57436" 
* #188736 "MDC_MASS_BODY_ACTUAL"
* #188736 ^definition = Mass | Actual | | Body
* #188736 ^designation[0].language = #de-AT 
* #188736 ^designation[0].value = "Patient actual weight" 
* #188736 ^property[0].code = #parent 
* #188736 ^property[0].valueCode = #_mdc 
* #188736 ^property[1].code = #hints 
* #188736 ^property[1].valueString = "PART: 2 ~ CODE10: 57664" 
* #188740 "MDC_LEN_BODY_ACTUAL"
* #188740 ^definition = Length | Actual |  | Body
* #188740 ^designation[0].language = #de-AT 
* #188740 ^designation[0].value = "Patient actual height" 
* #188740 ^property[0].code = #parent 
* #188740 ^property[0].valueCode = #_mdc 
* #188740 ^property[1].code = #hints 
* #188740 ^property[1].valueString = "PART: 2 ~ CODE10: 57668" 
* #188744 "MDC_AREA_BODY_SURF_ACTUAL"
* #188744 ^definition = Area | Actual | BodySurface | Body
* #188744 ^designation[0].language = #de-AT 
* #188744 ^designation[0].value = "Patient body surface area" 
* #188744 ^property[0].code = #parent 
* #188744 ^property[0].valueCode = #_mdc 
* #188744 ^property[1].code = #hints 
* #188744 ^property[1].valueString = "PART: 2 ~ CODE10: 57672" 
* #188748 "MDC_BODY_FAT"
* #188748 ^definition = Ratio | Computation | Body fat; Body weight | Body
* #188748 ^designation[0].language = #de-AT 
* #188748 ^designation[0].value = "body fat" 
* #188748 ^property[0].code = #parent 
* #188748 ^property[0].valueCode = #_mdc 
* #188748 ^property[1].code = #hints 
* #188748 ^property[1].valueString = "PART: 2 ~ CODE10: 57676" 
* #188752 "MDC_RATIO_MASS_BODY_LEN_SQ"
* #188752 ^definition = Ratio | Computation | Body mass; Body length | Body
* #188752 ^designation[0].language = #de-AT 
* #188752 ^designation[0].value = "body mass index" 
* #188752 ^property[0].code = #parent 
* #188752 ^property[0].valueCode = #_mdc 
* #188752 ^property[1].code = #hints 
* #188752 ^property[1].valueString = "PART: 2 ~ CODE10: 57680" 
* #188756 "MDC_MASS_BODY_FAT_FREE"
* #188756 ^definition = Mass | Actual | fat free mass | Body
* #188756 ^designation[0].language = #de-AT 
* #188756 ^designation[0].value = "fat free mass" 
* #188756 ^property[0].code = #parent 
* #188756 ^property[0].valueCode = #_mdc 
* #188756 ^property[1].code = #hints 
* #188756 ^property[1].valueString = "PART: 2 ~ CODE10: 57684" 
* #188760 "MDC_MASS_BODY_SOFT_LEAN"
* #188760 ^definition = Mass | Actual | Soft lean mass | Body
* #188760 ^designation[0].language = #de-AT 
* #188760 ^designation[0].value = "soft lean mass" 
* #188760 ^property[0].code = #parent 
* #188760 ^property[0].valueCode = #_mdc 
* #188760 ^property[1].code = #hints 
* #188760 ^property[1].valueString = "PART: 2 ~ CODE10: 57688" 
* #188764 "MDC_BODY_WATER"
* #188764 ^definition = Ratio | Computation | Body water; Body weight | Body
* #188764 ^designation[0].language = #de-AT 
* #188764 ^designation[0].value = "body water" 
* #188764 ^property[0].code = #parent 
* #188764 ^property[0].valueCode = #_mdc 
* #188764 ^property[1].code = #hints 
* #188764 ^property[1].valueString = "PART: 2 ~ CODE10: 57692" 
* #188772 "MDC_COMM_STATUS"
* #188772 ^property[0].code = #parent 
* #188772 ^property[0].valueCode = #_mdc 
* #188772 ^property[1].code = #hints 
* #188772 ^property[1].valueString = "PART: 2 ~ CODE10: 57700" 
* #188792 "MDC_ATTR_PT_WEIGHT_LAST"
* #188792 ^definition = Mass | Actual | | Body; last taken
* #188792 ^designation[0].language = #de-AT 
* #188792 ^designation[0].value = "Last Taken Weight" 
* #188792 ^property[0].code = #parent 
* #188792 ^property[0].valueCode = #_mdc 
* #188792 ^property[1].code = #hints 
* #188792 ^property[1].valueString = "PART: 2 ~ CODE10: 57720" 
* #188796 "MDC_MASS_BODY_EST_IBW"
* #188796 ^definition = Mass | Estimated | | Body; Ideal
* #188796 ^designation[0].language = #de-AT 
* #188796 ^designation[0].value = "Predicted Body Weight" 
* #188796 ^property[0].code = #parent 
* #188796 ^property[0].valueCode = #_mdc 
* #188796 ^property[1].code = #hints 
* #188796 ^property[1].valueString = "PART: 2 ~ CODE10: 57724" 
* #188800 "MDC_MASS_BODY_EST_ABW"
* #188800 ^definition = Mass | Estimated | | Body; Adjusted
* #188800 ^designation[0].language = #de-AT 
* #188800 ^designation[0].value = "Adjusted Body Weight" 
* #188800 ^property[0].code = #parent 
* #188800 ^property[0].valueCode = #_mdc 
* #188800 ^property[1].code = #hints 
* #188800 ^property[1].valueString = "PART: 2 ~ CODE10: 57728" 
* #196886 "MDC_EVT_LIGHT_INTERF"
* #196886 ^definition = ErrorEvent | Measurement; Light; Interference | FunctionalDisturbance | Device
* #196886 ^designation[0].language = #de-AT 
* #196886 ^designation[0].value = "Light interference with measurement" 
* #196886 ^property[0].code = #parent 
* #196886 ^property[0].valueCode = #_mdc 
* #196886 ^property[1].code = #hints 
* #196886 ^property[1].valueString = "PART: 3 ~ CODE10: 278" 
* #67926 "MDC_ATTR_PT_BSA"
* #67926 ^property[0].code = #parent 
* #67926 ^property[0].valueCode = #_mdc 
* #67926 ^property[1].code = #hints 
* #67926 ^property[1].valueString = "PART: 1 ~ CODE10: 2390" 
* #68012 "MDC_ATTR_AL_COND"
* #68012 ^property[0].code = #parent 
* #68012 ^property[0].valueCode = #_mdc 
* #68012 ^property[1].code = #hints 
* #68012 ^property[1].valueString = "PART: 1 ~ CODE10: 2476" 
* #68060 "MDC_ATTR_PT_HEIGHT"
* #68060 ^property[0].code = #parent 
* #68060 ^property[0].valueCode = #_mdc 
* #68060 ^property[1].code = #hints 
* #68060 ^property[1].valueString = "PART: 1 ~ CODE10: 2524" 
* #68063 "MDC_ATTR_PT_WEIGHT"
* #68063 ^property[0].code = #parent 
* #68063 ^property[0].valueCode = #_mdc 
* #68063 ^property[1].code = #hints 
* #68063 ^property[1].valueString = "PART: 1 ~ CODE10: 2527" 
* #68137 "MDC_ATTR_PT_LBM"
* #68137 ^property[0].code = #parent 
* #68137 ^property[0].valueCode = #_mdc 
* #68137 ^property[1].code = #hints 
* #68137 ^property[1].valueString = "PART: 1 ~ CODE10: 2601" 
* #68167 "MDC_ATTR_SOURCE_HANDLE_REF"
* #68167 ^property[0].code = #parent 
* #68167 ^property[0].valueCode = #_mdc 
* #68167 ^property[1].code = #hints 
* #68167 ^property[1].valueString = "PART: 1 ~ CODE10: 2631" 
* #68185 "MDC_ATTR_TIME_PD_MSMT_ACTIVE"
* #68185 ^property[0].code = #parent 
* #68185 ^property[0].valueCode = #_mdc 
* #68185 ^property[1].code = #hints 
* #68185 ^property[1].valueString = "PART: 1 ~ CODE10: 2649" 
* #68320 "MDC_ATTR_SAMPLE_RATE"
* #68320 ^property[0].code = #parent 
* #68320 ^property[0].valueCode = #_mdc 
* #68320 ^property[1].code = #hints 
* #68320 ^property[1].valueString = "PART: 1 ~ CODE10: 2784" 
* #68321 "MDC_ATTR_SAMPLE_COUNT"
* #68321 ^property[0].code = #parent 
* #68321 ^property[0].valueCode = #_mdc 
* #68321 ^property[1].code = #hints 
* #68321 ^property[1].valueString = "PART: 1 ~ CODE10: 2785" 
* #68322 "MDC_ATTR_WAV_ENCODING"
* #68322 ^property[0].code = #parent 
* #68322 ^property[0].valueCode = #_mdc 
* #68322 ^property[1].code = #hints 
* #68322 ^property[1].valueString = "PART: 1 ~ CODE10: 2786" 
* #68323 "MDC_ATTR_DATA_RANGE"
* #68323 ^property[0].code = #parent 
* #68323 ^property[0].valueCode = #_mdc 
* #68323 ^property[1].code = #hints 
* #68323 ^property[1].valueString = "PART: 1 ~ CODE10: 2787" 
* #68324 "MDC_ATTR_GRID_VIS"
* #68324 ^property[0].code = #parent 
* #68324 ^property[0].valueCode = #_mdc 
* #68324 ^property[1].code = #hints 
* #68324 ^property[1].valueString = "PART: 1 ~ CODE10: 2788" 
* #68325 "MDC_ATTR_VIS_COLOR"
* #68325 ^property[0].code = #parent 
* #68325 ^property[0].valueCode = #_mdc 
* #68325 ^property[1].code = #hints 
* #68325 ^property[1].valueString = "PART: 1 ~ CODE10: 2789" 
* #68326 "MDC_ATTR_SCALE_RANGE"
* #68326 ^property[0].code = #parent 
* #68326 ^property[0].valueCode = #_mdc 
* #68326 ^property[1].code = #hints 
* #68326 ^property[1].valueString = "PART: 1 ~ CODE10: 2790" 
* #68327 "MDC_ATTR_SCALE_RANGE_SIZE"
* #68327 ^property[0].code = #parent 
* #68327 ^property[0].valueCode = #_mdc 
* #68327 ^property[1].code = #hints 
* #68327 ^property[1].valueString = "PART: 1 ~ CODE10: 2791" 
* #68328 "MDC_ATTR_PHYS_RANGE"
* #68328 ^property[0].code = #parent 
* #68328 ^property[0].valueCode = #_mdc 
* #68328 ^property[1].code = #hints 
* #68328 ^property[1].valueString = "PART: 1 ~ CODE10: 2792" 
* #68480 "MDC_ATTR_ALERT_SOURCE"
* #68480 ^property[0].code = #parent 
* #68480 ^property[0].valueCode = #_mdc 
* #68480 ^property[1].code = #hints 
* #68480 ^property[1].valueString = "PART: 1 ~ CODE10: 2944" 
* #68481 "MDC_ATTR_EVENT_PHASE"
* #68481 ^property[0].code = #parent 
* #68481 ^property[0].valueCode = #_mdc 
* #68481 ^property[1].code = #hints 
* #68481 ^property[1].valueString = "PART: 1 ~ CODE10: 2945" 
* #68482 "MDC_ATTR_ALARM_STATE"
* #68482 ^property[0].code = #parent 
* #68482 ^property[0].valueCode = #_mdc 
* #68482 ^property[1].code = #hints 
* #68482 ^property[1].valueString = "PART: 1 ~ CODE10: 2946" 
* #68483 "MDC_ATTR_ALARM_INACTIVATION_STATE"
* #68483 ^property[0].code = #parent 
* #68483 ^property[0].valueCode = #_mdc 
* #68483 ^property[1].code = #hints 
* #68483 ^property[1].valueString = "PART: 1 ~ CODE10: 2947" 
* #68484 "MDC_ATTR_ALARM_PRIORITY"
* #68484 ^property[0].code = #parent 
* #68484 ^property[0].valueCode = #_mdc 
* #68484 ^property[1].code = #hints 
* #68484 ^property[1].valueString = "PART: 1 ~ CODE10: 2948" 
* #68485 "MDC_ATTR_ALERT_TYPE"
* #68485 ^property[0].code = #parent 
* #68485 ^property[0].valueCode = #_mdc 
* #68485 ^property[1].code = #hints 
* #68485 ^property[1].valueString = "PART: 1 ~ CODE10: 2949" 
* #68487 "MDC_ATTR_EVT_COND"
* #68487 ^property[0].code = #parent 
* #68487 ^property[0].valueCode = #_mdc 
* #68487 ^property[1].code = #hints 
* #68487 ^property[1].valueString = "PART: 1 ~ CODE10: 2951" 
* #68488 "MDC_ATTR_EVT_SOURCE"
* #68488 ^property[0].code = #parent 
* #68488 ^property[0].valueCode = #_mdc 
* #68488 ^property[1].code = #hints 
* #68488 ^property[1].valueString = "PART: 1 ~ CODE10: 2952" 
* #68512 "MDC_ATTR_LS_NAME"
* #68512 ^property[0].code = #parent 
* #68512 ^property[0].valueCode = #_mdc 
* #68512 ^property[1].code = #hints 
* #68512 ^property[1].valueString = "PART: 1 ~ CODE10: 2976" 
* #68513 "MDC_ATTR_LS_LOCATION"
* #68513 ^property[0].code = #parent 
* #68513 ^property[0].valueCode = #_mdc 
* #68513 ^property[1].code = #hints 
* #68513 ^property[1].valueString = "PART: 1 ~ CODE10: 2977" 
* #68514 "MDC_ATTR_LS_ADDRESS"
* #68514 ^property[0].code = #parent 
* #68514 ^property[0].valueCode = #_mdc 
* #68514 ^property[1].code = #hints 
* #68514 ^property[1].valueString = "PART: 1 ~ CODE10: 2978" 
* #68515 "MDC_ATTR_LS_PHASE"
* #68515 ^property[0].code = #parent 
* #68515 ^property[0].valueCode = #_mdc 
* #68515 ^property[1].code = #hints 
* #68515 ^property[1].valueString = "PART: 1 ~ CODE10: 2979" 
* #68517 "MDC_ATTR_LS_REF_NAME"
* #68517 ^property[0].code = #parent 
* #68517 ^property[0].valueCode = #_mdc 
* #68517 ^property[1].code = #hints 
* #68517 ^property[1].valueString = "PART: 1 ~ CODE10: 2981" 
* #68518 "MDC_ATTR_LS_REF_GPS"
* #68518 ^property[0].code = #parent 
* #68518 ^property[0].valueCode = #_mdc 
* #68518 ^property[1].code = #hints 
* #68518 ^property[1].valueString = "PART: 1 ~ CODE10: 2982" 
* #68519 "MDC_ATTR_LS_REF_GPS_LAT"
* #68519 ^property[0].code = #parent 
* #68519 ^property[0].valueCode = #_mdc 
* #68519 ^property[1].code = #hints 
* #68519 ^property[1].valueString = "PART: 1 ~ CODE10: 2983" 
* #68520 "MDC_ATTR_LS_REF_GPS_LON"
* #68520 ^property[0].code = #parent 
* #68520 ^property[0].valueCode = #_mdc 
* #68520 ^property[1].code = #hints 
* #68520 ^property[1].valueString = "PART: 1 ~ CODE10: 2984" 
* #68521 "MDC_ATTR_LS_REF_GPS_ALT"
* #68521 ^property[0].code = #parent 
* #68521 ^property[0].valueCode = #_mdc 
* #68521 ^property[1].code = #hints 
* #68521 ^property[1].valueString = "PART: 1 ~ CODE10: 2985" 
* #68522 "MDC_ATTR_LS_REF_GPS_BEARING"
* #68522 ^property[0].code = #parent 
* #68522 ^property[0].valueCode = #_mdc 
* #68522 ^property[1].code = #hints 
* #68522 ^property[1].valueString = "PART: 1 ~ CODE10: 2986" 
* #68523 "MDC_ATTR_LS_REF_LIMITS"
* #68523 ^property[0].code = #parent 
* #68523 ^property[0].valueCode = #_mdc 
* #68523 ^property[1].code = #hints 
* #68523 ^property[1].valueString = "PART: 1 ~ CODE10: 2987" 
* #68524 "MDC_ATTR_LS_COORD_XYZ"
* #68524 ^property[0].code = #parent 
* #68524 ^property[0].valueCode = #_mdc 
* #68524 ^property[1].code = #hints 
* #68524 ^property[1].valueString = "PART: 1 ~ CODE10: 2988" 
* #68525 "MDC_ATTR_LS_COORD_X"
* #68525 ^property[0].code = #parent 
* #68525 ^property[0].valueCode = #_mdc 
* #68525 ^property[1].code = #hints 
* #68525 ^property[1].valueString = "PART: 1 ~ CODE10: 2989" 
* #68526 "MDC_ATTR_LS_COORD_Y"
* #68526 ^property[0].code = #parent 
* #68526 ^property[0].valueCode = #_mdc 
* #68526 ^property[1].code = #hints 
* #68526 ^property[1].valueString = "PART: 1 ~ CODE10: 2990" 
* #68527 "MDC_ATTR_LS_COORD_Z"
* #68527 ^property[0].code = #parent 
* #68527 ^property[0].valueCode = #_mdc 
* #68527 ^property[1].code = #hints 
* #68527 ^property[1].valueString = "PART: 1 ~ CODE10: 2991" 
* #68528 "MDC_ATTR_LS_COORD_XYZ_ACCY"
* #68528 ^property[0].code = #parent 
* #68528 ^property[0].valueCode = #_mdc 
* #68528 ^property[1].code = #hints 
* #68528 ^property[1].valueString = "PART: 1 ~ CODE10: 2992" 
* #68529 "MDC_ATTR_LS_COORD_X_ACCY"
* #68529 ^property[0].code = #parent 
* #68529 ^property[0].valueCode = #_mdc 
* #68529 ^property[1].code = #hints 
* #68529 ^property[1].valueString = "PART: 1 ~ CODE10: 2993" 
* #68530 "MDC_ATTR_LS_COORD_Y_ACCY"
* #68530 ^property[0].code = #parent 
* #68530 ^property[0].valueCode = #_mdc 
* #68530 ^property[1].code = #hints 
* #68530 ^property[1].valueString = "PART: 1 ~ CODE10: 2994" 
* #68531 "MDC_ATTR_LS_COORD_Z_ACCY"
* #68531 ^property[0].code = #parent 
* #68531 ^property[0].valueCode = #_mdc 
* #68531 ^property[1].code = #hints 
* #68531 ^property[1].valueString = "PART: 1 ~ CODE10: 2995" 
* #68532 "MDC_ATTR_GPS_COORDINATES"
* #68532 ^property[0].code = #parent 
* #68532 ^property[0].valueCode = #_mdc 
* #68532 ^property[1].code = #hints 
* #68532 ^property[1].valueString = "PART: 1 ~ CODE10: 2996" 
* #68533 "MDC_ATTR_GPS_LAT"
* #68533 ^property[0].code = #parent 
* #68533 ^property[0].valueCode = #_mdc 
* #68533 ^property[1].code = #hints 
* #68533 ^property[1].valueString = "PART: 1 ~ CODE10: 2997" 
* #68534 "MDC_ATTR_GPS_LON"
* #68534 ^property[0].code = #parent 
* #68534 ^property[0].valueCode = #_mdc 
* #68534 ^property[1].code = #hints 
* #68534 ^property[1].valueString = "PART: 1 ~ CODE10: 2998" 
* #68535 "MDC_ATTR_GPS_COORD_ACCY"
* #68535 ^property[0].code = #parent 
* #68535 ^property[0].valueCode = #_mdc 
* #68535 ^property[1].code = #hints 
* #68535 ^property[1].valueString = "PART: 1 ~ CODE10: 2999" 
* #68536 "MDC_ATTR_GPS_LAT_ACCY"
* #68536 ^property[0].code = #parent 
* #68536 ^property[0].valueCode = #_mdc 
* #68536 ^property[1].code = #hints 
* #68536 ^property[1].valueString = "PART: 1 ~ CODE10: 3000" 
* #68537 "MDC_ATTR_GPS_LON_ACCY"
* #68537 ^property[0].code = #parent 
* #68537 ^property[0].valueCode = #_mdc 
* #68537 ^property[1].code = #hints 
* #68537 ^property[1].valueString = "PART: 1 ~ CODE10: 3001" 
* #68538 "MDC_ATTR_GPS_ALT"
* #68538 ^property[0].code = #parent 
* #68538 ^property[0].valueCode = #_mdc 
* #68538 ^property[1].code = #hints 
* #68538 ^property[1].valueString = "PART: 1 ~ CODE10: 3002" 
* #68539 "MDC_ATTR_GPS_ALT_ACCY"
* #68539 ^property[0].code = #parent 
* #68539 ^property[0].valueCode = #_mdc 
* #68539 ^property[1].code = #hints 
* #68539 ^property[1].valueString = "PART: 1 ~ CODE10: 3003" 
* #68540 "MDC_ATTR_GPS_HEADING"
* #68540 ^property[0].code = #parent 
* #68540 ^property[0].valueCode = #_mdc 
* #68540 ^property[1].code = #hints 
* #68540 ^property[1].valueString = "PART: 1 ~ CODE10: 3004" 
* #68541 "MDC_ATTR_GPS_PITCH"
* #68541 ^property[0].code = #parent 
* #68541 ^property[0].valueCode = #_mdc 
* #68541 ^property[1].code = #hints 
* #68541 ^property[1].valueString = "PART: 1 ~ CODE10: 3005" 
* #68542 "MDC_ATTR_GPS_SPEED"
* #68542 ^property[0].code = #parent 
* #68542 ^property[0].valueCode = #_mdc 
* #68542 ^property[1].code = #hints 
* #68542 ^property[1].valueString = "PART: 1 ~ CODE10: 3006" 
* #69120 "MDC_OBS_NOS"
* #69120 ^property[0].code = #parent 
* #69120 ^property[0].valueCode = #_mdc 
* #69120 ^property[1].code = #hints 
* #69120 ^property[1].valueString = "PART: 1 ~ CODE10: 3584" 
* #69121 "MDC_OBS_WAVE_CTS"
* #69121 ^property[0].code = #parent 
* #69121 ^property[0].valueCode = #_mdc 
* #69121 ^property[1].code = #hints 
* #69121 ^property[1].valueString = "PART: 1 ~ CODE10: 3585" 
* #69122 "MDC_OBS_WAVE_NONCTS"
* #69122 ^property[0].code = #parent 
* #69122 ^property[0].valueCode = #_mdc 
* #69122 ^property[1].code = #hints 
* #69122 ^property[1].valueString = "PART: 1 ~ CODE10: 3586" 
* #69984 "MDC_DEV_PUMP_INFUS"
* #69984 ^definition = Pump | Volume | Blood [Infusion] | <type>
* #69984 ^designation[0].language = #de-AT 
* #69984 ^designation[0].value = "Infusor" 
* #69984 ^property[0].code = #parent 
* #69984 ^property[0].valueCode = #_mdc 
* #69984 ^property[1].code = #hints 
* #69984 ^property[1].valueString = "PART: 1 ~ CODE10: 4448" 
* #69985 "MDC_DEV_PUMP_INFUS_MDS"
* #69985 ^property[0].code = #parent 
* #69985 ^property[0].valueCode = #_mdc 
* #69985 ^property[1].code = #hints 
* #69985 ^property[1].valueString = "PART: 1 ~ CODE10: 4449" 
* #69986 "MDC_DEV_PUMP_INFUS_VMD"
* #69986 ^property[0].code = #parent 
* #69986 ^property[0].valueCode = #_mdc 
* #69986 ^property[1].code = #hints 
* #69986 ^property[1].valueString = "PART: 1 ~ CODE10: 4450" 
* #70048 "MDC_DEV_PUMP_INFUS_LVP"
* #70048 ^property[0].code = #parent 
* #70048 ^property[0].valueCode = #_mdc 
* #70048 ^property[1].code = #hints 
* #70048 ^property[1].valueString = "PART: 1 ~ CODE10: 4512" 
* #70049 "MDC_DEV_PUMP_INFUS_LVP_MDS"
* #70049 ^property[0].code = #parent 
* #70049 ^property[0].valueCode = #_mdc 
* #70049 ^property[1].code = #hints 
* #70049 ^property[1].valueString = "PART: 1 ~ CODE10: 4513" 
* #70050 "MDC_DEV_PUMP_INFUS_LVP_VMD"
* #70050 ^property[0].code = #parent 
* #70050 ^property[0].valueCode = #_mdc 
* #70050 ^property[1].code = #hints 
* #70050 ^property[1].valueString = "PART: 1 ~ CODE10: 4514" 
* #70052 "MDC_DEV_PUMP_INFUS_SYRINGE"
* #70052 ^property[0].code = #parent 
* #70052 ^property[0].valueCode = #_mdc 
* #70052 ^property[1].code = #hints 
* #70052 ^property[1].valueString = "PART: 1 ~ CODE10: 4516" 
* #70053 "MDC_DEV_PUMP_INFUS_SYRINGE_MDS"
* #70053 ^property[0].code = #parent 
* #70053 ^property[0].valueCode = #_mdc 
* #70053 ^property[1].code = #hints 
* #70053 ^property[1].valueString = "PART: 1 ~ CODE10: 4517" 
* #70054 "MDC_DEV_PUMP_INFUS_SYRINGE_VMD"
* #70054 ^property[0].code = #parent 
* #70054 ^property[0].valueCode = #_mdc 
* #70054 ^property[1].code = #hints 
* #70054 ^property[1].valueString = "PART: 1 ~ CODE10: 4518" 
* #70055 "MDC_DEV_PUMP_INFUS_SYRINGE_CHAN"
* #70055 ^property[0].code = #parent 
* #70055 ^property[0].valueCode = #_mdc 
* #70055 ^property[1].code = #hints 
* #70055 ^property[1].valueString = "PART: 1 ~ CODE10: 4519" 
* #70056 "MDC_DEV_PUMP_INFUS_PCA"
* #70056 ^property[0].code = #parent 
* #70056 ^property[0].valueCode = #_mdc 
* #70056 ^property[1].code = #hints 
* #70056 ^property[1].valueString = "PART: 1 ~ CODE10: 4520" 
* #70057 "MDC_DEV_PUMP_INFUS_PCA_MDS"
* #70057 ^property[0].code = #parent 
* #70057 ^property[0].valueCode = #_mdc 
* #70057 ^property[1].code = #hints 
* #70057 ^property[1].valueString = "PART: 1 ~ CODE10: 4521" 
* #70058 "MDC_DEV_PUMP_INFUS_PCA_VMD"
* #70058 ^property[0].code = #parent 
* #70058 ^property[0].valueCode = #_mdc 
* #70058 ^property[1].code = #hints 
* #70058 ^property[1].valueString = "PART: 1 ~ CODE10: 4522" 
* #70060 "MDC_DEV_PUMP_INFUS_PCA_SYRINGE"
* #70060 ^property[0].code = #parent 
* #70060 ^property[0].valueCode = #_mdc 
* #70060 ^property[1].code = #hints 
* #70060 ^property[1].valueString = "PART: 1 ~ CODE10: 4524" 
* #70061 "MDC_DEV_PUMP_INFUS_PCA_SYRINGE_MDS"
* #70061 ^property[0].code = #parent 
* #70061 ^property[0].valueCode = #_mdc 
* #70061 ^property[1].code = #hints 
* #70061 ^property[1].valueString = "PART: 1 ~ CODE10: 4525" 
* #70062 "MDC_DEV_PUMP_INFUS_PCA_SYRINGE_VMD"
* #70062 ^property[0].code = #parent 
* #70062 ^property[0].valueCode = #_mdc 
* #70062 ^property[1].code = #hints 
* #70062 ^property[1].valueString = "PART: 1 ~ CODE10: 4526" 
* #70067 "MDC_DEV_PUMP_DELIVERY_INFO"
* #70067 ^property[0].code = #parent 
* #70067 ^property[0].valueCode = #_mdc 
* #70067 ^property[1].code = #hints 
* #70067 ^property[1].valueString = "PART: 1 ~ CODE10: 4531" 
* #70071 "MDC_DEV_PUMP_INFUSATE_SOURCE_PRIMARY"
* #70071 ^property[0].code = #parent 
* #70071 ^property[0].valueCode = #_mdc 
* #70071 ^property[1].code = #hints 
* #70071 ^property[1].valueString = "PART: 1 ~ CODE10: 4535" 
* #70075 "MDC_DEV_PUMP_INFUSATE_SOURCE_SECONDARY"
* #70075 ^property[0].code = #parent 
* #70075 ^property[0].valueCode = #_mdc 
* #70075 ^property[1].code = #hints 
* #70075 ^property[1].valueString = "PART: 1 ~ CODE10: 4539" 
* #70079 "MDC_DEV_PUMP_CLINICIAN_DOSE_INFO"
* #70079 ^property[0].code = #parent 
* #70079 ^property[0].valueCode = #_mdc 
* #70079 ^property[1].code = #hints 
* #70079 ^property[1].valueString = "PART: 1 ~ CODE10: 4543" 
* #70083 "MDC_DEV_PUMP_LOADING_DOSE_INFO"
* #70083 ^property[0].code = #parent 
* #70083 ^property[0].valueCode = #_mdc 
* #70083 ^property[1].code = #hints 
* #70083 ^property[1].valueString = "PART: 1 ~ CODE10: 4547" 
* #70087 "MDC_DEV_PUMP_INTERMITTENT_DOSE_INFO"
* #70087 ^property[0].code = #parent 
* #70087 ^property[0].valueCode = #_mdc 
* #70087 ^property[1].code = #hints 
* #70087 ^property[1].valueString = "PART: 1 ~ CODE10: 4551" 
* #70091 "MDC_DEV_PUMP_SYRINGE_INFO"
* #70091 ^property[0].code = #parent 
* #70091 ^property[0].valueCode = #_mdc 
* #70091 ^property[1].code = #hints 
* #70091 ^property[1].valueString = "PART: 1 ~ CODE10: 4555" 
* #70095 "MDC_DEV_PUMP_PCA_INFO"
* #70095 ^property[0].code = #parent 
* #70095 ^property[0].valueCode = #_mdc 
* #70095 ^property[1].code = #hints 
* #70095 ^property[1].valueString = "PART: 1 ~ CODE10: 4559" 
* #8417752 "MDC_GLU_METER_DEV_STATUS"
* #8417752 ^property[0].code = #parent 
* #8417752 ^property[0].valueCode = #_mdc 
* #8417752 ^property[1].code = #hints 
* #8417752 ^property[1].valueString = "PART: 128 ~ CODE10: 29144" 
* #8417760 "MDC_CTXT_GLU_EXERCISE"
* #8417760 ^property[0].code = #parent 
* #8417760 ^property[0].valueCode = #_mdc 
* #8417760 ^property[1].code = #hints 
* #8417760 ^property[1].valueString = "PART: 128 ~ CODE10: 29152" 
* #8417764 "MDC_CTXT_GLU_CARB"
* #8417764 ^property[0].code = #parent 
* #8417764 ^property[0].valueCode = #_mdc 
* #8417764 ^property[1].code = #hints 
* #8417764 ^property[1].valueString = "PART: 128 ~ CODE10: 29156" 
* #8417765 "MDC_CTXT_GLU_CARB_UNDETERMINED"
* #8417765 ^property[0].code = #parent 
* #8417765 ^property[0].valueCode = #_mdc 
* #8417765 ^property[1].code = #hints 
* #8417765 ^property[1].valueString = "PART: 128 ~ CODE10: 29157" 
* #8417766 "MDC_CTXT_GLU_CARB_OTHER"
* #8417766 ^property[0].code = #parent 
* #8417766 ^property[0].valueCode = #_mdc 
* #8417766 ^property[1].code = #hints 
* #8417766 ^property[1].valueString = "PART: 128 ~ CODE10: 29158" 
* #8417767 "MDC_CTXT_GLU_CARB_NO_ENTRY"
* #8417767 ^property[0].code = #parent 
* #8417767 ^property[0].valueCode = #_mdc 
* #8417767 ^property[1].code = #hints 
* #8417767 ^property[1].valueString = "PART: 128 ~ CODE10: 29159" 
* #8417768 "MDC_CTXT_GLU_CARB_BREAKFAST"
* #8417768 ^property[0].code = #parent 
* #8417768 ^property[0].valueCode = #_mdc 
* #8417768 ^property[1].code = #hints 
* #8417768 ^property[1].valueString = "PART: 128 ~ CODE10: 29160" 
* #8417769 "MDC_CTXT_GLU_CARB_NO_INGESTION"
* #8417769 ^property[0].code = #parent 
* #8417769 ^property[0].valueCode = #_mdc 
* #8417769 ^property[1].code = #hints 
* #8417769 ^property[1].valueString = "PART: 128 ~ CODE10: 29161" 
* #8417772 "MDC_CTXT_GLU_CARB_LUNCH"
* #8417772 ^property[0].code = #parent 
* #8417772 ^property[0].valueCode = #_mdc 
* #8417772 ^property[1].code = #hints 
* #8417772 ^property[1].valueString = "PART: 128 ~ CODE10: 29164" 
* #8417776 "MDC_CTXT_GLU_CARB_DINNER"
* #8417776 ^property[0].code = #parent 
* #8417776 ^property[0].valueCode = #_mdc 
* #8417776 ^property[1].code = #hints 
* #8417776 ^property[1].valueString = "PART: 128 ~ CODE10: 29168" 
* #8417780 "MDC_CTXT_GLU_CARB_SNACK"
* #8417780 ^property[0].code = #parent 
* #8417780 ^property[0].valueCode = #_mdc 
* #8417780 ^property[1].code = #hints 
* #8417780 ^property[1].valueString = "PART: 128 ~ CODE10: 29172" 
* #8417784 "MDC_CTXT_GLU_CARB_DRINK"
* #8417784 ^property[0].code = #parent 
* #8417784 ^property[0].valueCode = #_mdc 
* #8417784 ^property[1].code = #hints 
* #8417784 ^property[1].valueString = "PART: 128 ~ CODE10: 29176" 
* #8417788 "MDC_CTXT_GLU_CARB_SUPPER"
* #8417788 ^property[0].code = #parent 
* #8417788 ^property[0].valueCode = #_mdc 
* #8417788 ^property[1].code = #hints 
* #8417788 ^property[1].valueString = "PART: 128 ~ CODE10: 29180" 
* #8417792 "MDC_CTXT_GLU_CARB_BRUNCH"
* #8417792 ^property[0].code = #parent 
* #8417792 ^property[0].valueCode = #_mdc 
* #8417792 ^property[1].code = #hints 
* #8417792 ^property[1].valueString = "PART: 128 ~ CODE10: 29184" 
* #8417796 "MDC_CTXT_MEDICATION"
* #8417796 ^property[0].code = #parent 
* #8417796 ^property[0].valueCode = #_mdc 
* #8417796 ^property[1].code = #hints 
* #8417796 ^property[1].valueString = "PART: 128 ~ CODE10: 29188" 
* #8417800 "MDC_CTXT_MEDICATION_RAPIDACTING"
* #8417800 ^property[0].code = #parent 
* #8417800 ^property[0].valueCode = #_mdc 
* #8417800 ^property[1].code = #hints 
* #8417800 ^property[1].valueString = "PART: 128 ~ CODE10: 29192" 
* #8417804 "MDC_CTXT_MEDICATION_SHORTACTING"
* #8417804 ^property[0].code = #parent 
* #8417804 ^property[0].valueCode = #_mdc 
* #8417804 ^property[1].code = #hints 
* #8417804 ^property[1].valueString = "PART: 128 ~ CODE10: 29196" 
* #8417808 "MDC_CTXT_MEDICATION_INTERMEDIATEACTING"
* #8417808 ^property[0].code = #parent 
* #8417808 ^property[0].valueCode = #_mdc 
* #8417808 ^property[1].code = #hints 
* #8417808 ^property[1].valueString = "PART: 128 ~ CODE10: 29200" 
* #8417812 "MDC_CTXT_MEDICATION_LONGACTING"
* #8417812 ^property[0].code = #parent 
* #8417812 ^property[0].valueCode = #_mdc 
* #8417812 ^property[1].code = #hints 
* #8417812 ^property[1].valueString = "PART: 128 ~ CODE10: 29204" 
* #8417816 "MDC_CTXT_MEDICATION_PREMIX"
* #8417816 ^property[0].code = #parent 
* #8417816 ^property[0].valueCode = #_mdc 
* #8417816 ^property[1].code = #hints 
* #8417816 ^property[1].valueString = "PART: 128 ~ CODE10: 29208" 
* #8417820 "MDC_CTXT_GLU_HEALTH"
* #8417820 ^property[0].code = #parent 
* #8417820 ^property[0].valueCode = #_mdc 
* #8417820 ^property[1].code = #hints 
* #8417820 ^property[1].valueString = "PART: 128 ~ CODE10: 29212" 
* #8417844 "MDC_CTXT_GLU_SAMPLELOCATION"
* #8417844 ^property[0].code = #parent 
* #8417844 ^property[0].valueCode = #_mdc 
* #8417844 ^property[1].code = #hints 
* #8417844 ^property[1].valueString = "PART: 128 ~ CODE10: 29236" 
* #8417864 "MDC_CTXT_GLU_MEAL"
* #8417864 ^property[0].code = #parent 
* #8417864 ^property[0].valueCode = #_mdc 
* #8417864 ^property[1].code = #hints 
* #8417864 ^property[1].valueString = "PART: 128 ~ CODE10: 29256" 
* #8417884 "MDC_CTXT_GLU_TESTER"
* #8417884 ^property[0].code = #parent 
* #8417884 ^property[0].valueCode = #_mdc 
* #8417884 ^property[1].code = #hints 
* #8417884 ^property[1].valueString = "PART: 128 ~ CODE10: 29276" 
* #8417908 "MDC_BATCHCODE_COAG"
* #8417908 ^property[0].code = #parent 
* #8417908 ^property[0].valueCode = #_mdc 
* #8417908 ^property[1].code = #hints 
* #8417908 ^property[1].valueString = "PART: 128 ~ CODE10: 29300" 
* #8417909 "MDC_INR_METER_DEV_STATUS"
* #8417909 ^property[0].code = #parent 
* #8417909 ^property[0].valueCode = #_mdc 
* #8417909 ^property[1].code = #hints 
* #8417909 ^property[1].valueString = "PART: 128 ~ CODE10: 29301" 
* #8417912 "MDC_TARGET_LEVEL_COAG"
* #8417912 ^property[0].code = #parent 
* #8417912 ^property[0].valueCode = #_mdc 
* #8417912 ^property[1].code = #hints 
* #8417912 ^property[1].valueString = "PART: 128 ~ CODE10: 29304" 
* #8417916 "MDC_MED_CURRENT_COAG"
* #8417916 ^property[0].code = #parent 
* #8417916 ^property[0].valueCode = #_mdc 
* #8417916 ^property[1].code = #hints 
* #8417916 ^property[1].valueString = "PART: 128 ~ CODE10: 29308" 
* #8417920 "MDC_MED_NEW_COAG"
* #8417920 ^property[0].code = #parent 
* #8417920 ^property[0].valueCode = #_mdc 
* #8417920 ^property[1].code = #hints 
* #8417920 ^property[1].valueString = "PART: 128 ~ CODE10: 29312" 
* #8417924 "MDC_CTXT_INR_TESTER"
* #8417924 ^property[0].code = #parent 
* #8417924 ^property[0].valueCode = #_mdc 
* #8417924 ^property[1].code = #hints 
* #8417924 ^property[1].valueString = "PART: 128 ~ CODE10: 29316" 
* #_mdcenums "MDC enums"
* #_mdcenums ^property[0].code = #child 
* #_mdcenums ^property[0].valueCode = #157989 
* #_mdcenums ^property[1].code = #child 
* #_mdcenums ^property[1].valueCode = #157998 
* #_mdcenums ^property[2].code = #child 
* #_mdcenums ^property[2].valueCode = #196634 
* #_mdcenums ^property[3].code = #child 
* #_mdcenums ^property[3].valueCode = #196678 
* #_mdcenums ^property[4].code = #child 
* #_mdcenums ^property[4].valueCode = #196768 
* #_mdcenums ^property[5].code = #child 
* #_mdcenums ^property[5].valueCode = #196802 
* #_mdcenums ^property[6].code = #child 
* #_mdcenums ^property[6].valueCode = #196940 
* #_mdcenums ^property[7].code = #child 
* #_mdcenums ^property[7].valueCode = #196950 
* #_mdcenums ^property[8].code = #child 
* #_mdcenums ^property[8].valueCode = #197084 
* #_mdcenums ^property[9].code = #child 
* #_mdcenums ^property[9].valueCode = #197200 
* #_mdcenums ^property[10].code = #child 
* #_mdcenums ^property[10].valueCode = #197212 
* #_mdcenums ^property[11].code = #child 
* #_mdcenums ^property[11].valueCode = #197214 
* #_mdcenums ^property[12].code = #child 
* #_mdcenums ^property[12].valueCode = #197216 
* #_mdcenums ^property[13].code = #child 
* #_mdcenums ^property[13].valueCode = #197218 
* #_mdcenums ^property[14].code = #child 
* #_mdcenums ^property[14].valueCode = #197220 
* #_mdcenums ^property[15].code = #child 
* #_mdcenums ^property[15].valueCode = #197222 
* #_mdcenums ^property[16].code = #child 
* #_mdcenums ^property[16].valueCode = #197224 
* #_mdcenums ^property[17].code = #child 
* #_mdcenums ^property[17].valueCode = #197226 
* #_mdcenums ^property[18].code = #child 
* #_mdcenums ^property[18].valueCode = #197228 
* #_mdcenums ^property[19].code = #child 
* #_mdcenums ^property[19].valueCode = #197230 
* #_mdcenums ^property[20].code = #child 
* #_mdcenums ^property[20].valueCode = #197232 
* #_mdcenums ^property[21].code = #child 
* #_mdcenums ^property[21].valueCode = #197234 
* #_mdcenums ^property[22].code = #child 
* #_mdcenums ^property[22].valueCode = #197236 
* #_mdcenums ^property[23].code = #child 
* #_mdcenums ^property[23].valueCode = #197258 
* #_mdcenums ^property[24].code = #child 
* #_mdcenums ^property[24].valueCode = #197260 
* #_mdcenums ^property[25].code = #child 
* #_mdcenums ^property[25].valueCode = #197262 
* #_mdcenums ^property[26].code = #child 
* #_mdcenums ^property[26].valueCode = #197264 
* #_mdcenums ^property[27].code = #child 
* #_mdcenums ^property[27].valueCode = #197288 
* #_mdcenums ^property[28].code = #child 
* #_mdcenums ^property[28].valueCode = #197290 
* #_mdcenums ^property[29].code = #child 
* #_mdcenums ^property[29].valueCode = #197292 
* #_mdcenums ^property[30].code = #child 
* #_mdcenums ^property[30].valueCode = #197294 
* #_mdcenums ^property[31].code = #child 
* #_mdcenums ^property[31].valueCode = #197296 
* #_mdcenums ^property[32].code = #child 
* #_mdcenums ^property[32].valueCode = #197298 
* #_mdcenums ^property[33].code = #child 
* #_mdcenums ^property[33].valueCode = #197300 
* #_mdcenums ^property[34].code = #child 
* #_mdcenums ^property[34].valueCode = #197302 
* #_mdcenums ^property[35].code = #child 
* #_mdcenums ^property[35].valueCode = #197304 
* #_mdcenums ^property[36].code = #child 
* #_mdcenums ^property[36].valueCode = #197306 
* #_mdcenums ^property[37].code = #child 
* #_mdcenums ^property[37].valueCode = #197308 
* #_mdcenums ^property[38].code = #child 
* #_mdcenums ^property[38].valueCode = #197328 
* #_mdcenums ^property[39].code = #child 
* #_mdcenums ^property[39].valueCode = #197330 
* #_mdcenums ^property[40].code = #child 
* #_mdcenums ^property[40].valueCode = #197332 
* #_mdcenums ^property[41].code = #child 
* #_mdcenums ^property[41].valueCode = #197334 
* #_mdcenums ^property[42].code = #child 
* #_mdcenums ^property[42].valueCode = #197336 
* #_mdcenums ^property[43].code = #child 
* #_mdcenums ^property[43].valueCode = #197338 
* #_mdcenums ^property[44].code = #child 
* #_mdcenums ^property[44].valueCode = #197340 
* #_mdcenums ^property[45].code = #child 
* #_mdcenums ^property[45].valueCode = #197342 
* #_mdcenums ^property[46].code = #child 
* #_mdcenums ^property[46].valueCode = #197346 
* #_mdcenums ^property[47].code = #child 
* #_mdcenums ^property[47].valueCode = #197348 
* #_mdcenums ^property[48].code = #child 
* #_mdcenums ^property[48].valueCode = #197350 
* #_mdcenums ^property[49].code = #child 
* #_mdcenums ^property[49].valueCode = #197352 
* #_mdcenums ^property[50].code = #child 
* #_mdcenums ^property[50].valueCode = #197354 
* #_mdcenums ^property[51].code = #child 
* #_mdcenums ^property[51].valueCode = #197356 
* #_mdcenums ^property[52].code = #child 
* #_mdcenums ^property[52].valueCode = #197358 
* #_mdcenums ^property[53].code = #child 
* #_mdcenums ^property[53].valueCode = #197360 
* #_mdcenums ^property[54].code = #child 
* #_mdcenums ^property[54].valueCode = #197362 
* #_mdcenums ^property[55].code = #child 
* #_mdcenums ^property[55].valueCode = #197364 
* #_mdcenums ^property[56].code = #child 
* #_mdcenums ^property[56].valueCode = #197376 
* #_mdcenums ^property[57].code = #child 
* #_mdcenums ^property[57].valueCode = #197378 
* #_mdcenums ^property[58].code = #child 
* #_mdcenums ^property[58].valueCode = #203276 
* #_mdcenums ^property[59].code = #child 
* #_mdcenums ^property[59].valueCode = #203341 
* #_mdcenums ^property[60].code = #child 
* #_mdcenums ^property[60].valueCode = #203776 
* #_mdcenums ^property[61].code = #child 
* #_mdcenums ^property[61].valueCode = #203778 
* #_mdcenums ^property[62].code = #child 
* #_mdcenums ^property[62].valueCode = #203780 
* #_mdcenums ^property[63].code = #child 
* #_mdcenums ^property[63].valueCode = #203782 
* #_mdcenums ^property[64].code = #child 
* #_mdcenums ^property[64].valueCode = #203784 
* #_mdcenums ^property[65].code = #child 
* #_mdcenums ^property[65].valueCode = #203786 
* #_mdcenums ^property[66].code = #child 
* #_mdcenums ^property[66].valueCode = #203788 
* #_mdcenums ^property[67].code = #child 
* #_mdcenums ^property[67].valueCode = #203790 
* #_mdcenums ^property[68].code = #child 
* #_mdcenums ^property[68].valueCode = #203792 
* #_mdcenums ^property[69].code = #child 
* #_mdcenums ^property[69].valueCode = #203794 
* #_mdcenums ^property[70].code = #child 
* #_mdcenums ^property[70].valueCode = #203796 
* #_mdcenums ^property[71].code = #child 
* #_mdcenums ^property[71].valueCode = #203798 
* #_mdcenums ^property[72].code = #child 
* #_mdcenums ^property[72].valueCode = #258048 
* #_mdcenums ^property[73].code = #child 
* #_mdcenums ^property[73].valueCode = #459748 
* #_mdcenums ^property[74].code = #child 
* #_mdcenums ^property[74].valueCode = #459752 
* #_mdcenums ^property[75].code = #child 
* #_mdcenums ^property[75].valueCode = #459756 
* #_mdcenums ^property[76].code = #child 
* #_mdcenums ^property[76].valueCode = #459760 
* #_mdcenums ^property[77].code = #child 
* #_mdcenums ^property[77].valueCode = #459764 
* #_mdcenums ^property[78].code = #child 
* #_mdcenums ^property[78].valueCode = #459768 
* #_mdcenums ^property[79].code = #child 
* #_mdcenums ^property[79].valueCode = #459772 
* #_mdcenums ^property[80].code = #child 
* #_mdcenums ^property[80].valueCode = #459776 
* #_mdcenums ^property[81].code = #child 
* #_mdcenums ^property[81].valueCode = #459780 
* #_mdcenums ^property[82].code = #child 
* #_mdcenums ^property[82].valueCode = #459784 
* #_mdcenums ^property[83].code = #child 
* #_mdcenums ^property[83].valueCode = #459788 
* #_mdcenums ^property[84].code = #child 
* #_mdcenums ^property[84].valueCode = #459793 
* #_mdcenums ^property[85].code = #child 
* #_mdcenums ^property[85].valueCode = #459794 
* #_mdcenums ^property[86].code = #child 
* #_mdcenums ^property[86].valueCode = #459801 
* #_mdcenums ^property[87].code = #child 
* #_mdcenums ^property[87].valueCode = #459806 
* #_mdcenums ^property[88].code = #child 
* #_mdcenums ^property[88].valueCode = #459809 
* #_mdcenums ^property[89].code = #child 
* #_mdcenums ^property[89].valueCode = #459814 
* #_mdcenums ^property[90].code = #child 
* #_mdcenums ^property[90].valueCode = #459817 
* #_mdcenums ^property[91].code = #child 
* #_mdcenums ^property[91].valueCode = #459822 
* #_mdcenums ^property[92].code = #child 
* #_mdcenums ^property[92].valueCode = #459825 
* #_mdcenums ^property[93].code = #child 
* #_mdcenums ^property[93].valueCode = #459830 
* #_mdcenums ^property[94].code = #child 
* #_mdcenums ^property[94].valueCode = #459833 
* #_mdcenums ^property[95].code = #child 
* #_mdcenums ^property[95].valueCode = #459838 
* #_mdcenums ^property[96].code = #child 
* #_mdcenums ^property[96].valueCode = #459841 
* #_mdcenums ^property[97].code = #child 
* #_mdcenums ^property[97].valueCode = #459846 
* #_mdcenums ^property[98].code = #child 
* #_mdcenums ^property[98].valueCode = #459849 
* #_mdcenums ^property[99].code = #child 
* #_mdcenums ^property[99].valueCode = #459854 
* #_mdcenums ^property[100].code = #child 
* #_mdcenums ^property[100].valueCode = #459857 
* #_mdcenums ^property[101].code = #child 
* #_mdcenums ^property[101].valueCode = #459862 
* #_mdcenums ^property[102].code = #child 
* #_mdcenums ^property[102].valueCode = #459865 
* #_mdcenums ^property[103].code = #child 
* #_mdcenums ^property[103].valueCode = #459870 
* #_mdcenums ^property[104].code = #child 
* #_mdcenums ^property[104].valueCode = #459873 
* #_mdcenums ^property[105].code = #child 
* #_mdcenums ^property[105].valueCode = #459878 
* #_mdcenums ^property[106].code = #child 
* #_mdcenums ^property[106].valueCode = #459881 
* #_mdcenums ^property[107].code = #child 
* #_mdcenums ^property[107].valueCode = #459886 
* #_mdcenums ^property[108].code = #child 
* #_mdcenums ^property[108].valueCode = #459889 
* #_mdcenums ^property[109].code = #child 
* #_mdcenums ^property[109].valueCode = #459894 
* #_mdcenums ^property[110].code = #child 
* #_mdcenums ^property[110].valueCode = #459897 
* #_mdcenums ^property[111].code = #child 
* #_mdcenums ^property[111].valueCode = #459902 
* #_mdcenums ^property[112].code = #child 
* #_mdcenums ^property[112].valueCode = #459905 
* #_mdcenums ^property[113].code = #child 
* #_mdcenums ^property[113].valueCode = #459910 
* #_mdcenums ^property[114].code = #child 
* #_mdcenums ^property[114].valueCode = #459913 
* #_mdcenums ^property[115].code = #child 
* #_mdcenums ^property[115].valueCode = #459918 
* #_mdcenums ^property[116].code = #child 
* #_mdcenums ^property[116].valueCode = #459921 
* #_mdcenums ^property[117].code = #child 
* #_mdcenums ^property[117].valueCode = #459926 
* #_mdcenums ^property[118].code = #child 
* #_mdcenums ^property[118].valueCode = #459929 
* #_mdcenums ^property[119].code = #child 
* #_mdcenums ^property[119].valueCode = #459934 
* #_mdcenums ^property[120].code = #child 
* #_mdcenums ^property[120].valueCode = #459937 
* #_mdcenums ^property[121].code = #child 
* #_mdcenums ^property[121].valueCode = #459942 
* #_mdcenums ^property[122].code = #child 
* #_mdcenums ^property[122].valueCode = #459945 
* #_mdcenums ^property[123].code = #child 
* #_mdcenums ^property[123].valueCode = #459950 
* #_mdcenums ^property[124].code = #child 
* #_mdcenums ^property[124].valueCode = #459953 
* #_mdcenums ^property[125].code = #child 
* #_mdcenums ^property[125].valueCode = #459958 
* #_mdcenums ^property[126].code = #child 
* #_mdcenums ^property[126].valueCode = #459961 
* #_mdcenums ^property[127].code = #child 
* #_mdcenums ^property[127].valueCode = #459966 
* #_mdcenums ^property[128].code = #child 
* #_mdcenums ^property[128].valueCode = #459969 
* #_mdcenums ^property[129].code = #child 
* #_mdcenums ^property[129].valueCode = #459974 
* #_mdcenums ^property[130].code = #child 
* #_mdcenums ^property[130].valueCode = #459977 
* #_mdcenums ^property[131].code = #child 
* #_mdcenums ^property[131].valueCode = #459982 
* #_mdcenums ^property[132].code = #child 
* #_mdcenums ^property[132].valueCode = #459985 
* #_mdcenums ^property[133].code = #child 
* #_mdcenums ^property[133].valueCode = #459990 
* #_mdcenums ^property[134].code = #child 
* #_mdcenums ^property[134].valueCode = #459993 
* #_mdcenums ^property[135].code = #child 
* #_mdcenums ^property[135].valueCode = #459998 
* #_mdcenums ^property[136].code = #child 
* #_mdcenums ^property[136].valueCode = #460001 
* #_mdcenums ^property[137].code = #child 
* #_mdcenums ^property[137].valueCode = #460006 
* #_mdcenums ^property[138].code = #child 
* #_mdcenums ^property[138].valueCode = #460009 
* #_mdcenums ^property[139].code = #child 
* #_mdcenums ^property[139].valueCode = #460014 
* #_mdcenums ^property[140].code = #child 
* #_mdcenums ^property[140].valueCode = #460017 
* #_mdcenums ^property[141].code = #child 
* #_mdcenums ^property[141].valueCode = #460022 
* #_mdcenums ^property[142].code = #child 
* #_mdcenums ^property[142].valueCode = #460025 
* #_mdcenums ^property[143].code = #child 
* #_mdcenums ^property[143].valueCode = #460030 
* #_mdcenums ^property[144].code = #child 
* #_mdcenums ^property[144].valueCode = #460033 
* #_mdcenums ^property[145].code = #child 
* #_mdcenums ^property[145].valueCode = #460038 
* #_mdcenums ^property[146].code = #child 
* #_mdcenums ^property[146].valueCode = #460041 
* #_mdcenums ^property[147].code = #child 
* #_mdcenums ^property[147].valueCode = #460042 
* #_mdcenums ^property[148].code = #child 
* #_mdcenums ^property[148].valueCode = #460049 
* #_mdcenums ^property[149].code = #child 
* #_mdcenums ^property[149].valueCode = #460050 
* #_mdcenums ^property[150].code = #child 
* #_mdcenums ^property[150].valueCode = #460057 
* #_mdcenums ^property[151].code = #child 
* #_mdcenums ^property[151].valueCode = #460058 
* #_mdcenums ^property[152].code = #child 
* #_mdcenums ^property[152].valueCode = #460065 
* #_mdcenums ^property[153].code = #child 
* #_mdcenums ^property[153].valueCode = #460066 
* #_mdcenums ^property[154].code = #child 
* #_mdcenums ^property[154].valueCode = #460272 
* #_mdcenums ^property[155].code = #child 
* #_mdcenums ^property[155].valueCode = #460280 
* #_mdcenums ^property[156].code = #child 
* #_mdcenums ^property[156].valueCode = #460281 
* #_mdcenums ^property[157].code = #child 
* #_mdcenums ^property[157].valueCode = #460282 
* #_mdcenums ^property[158].code = #child 
* #_mdcenums ^property[158].valueCode = #460292 
* #_mdcenums ^property[159].code = #child 
* #_mdcenums ^property[159].valueCode = #460296 
* #_mdcenums ^property[160].code = #child 
* #_mdcenums ^property[160].valueCode = #460297 
* #_mdcenums ^property[161].code = #child 
* #_mdcenums ^property[161].valueCode = #460298 
* #_mdcenums ^property[162].code = #child 
* #_mdcenums ^property[162].valueCode = #460304 
* #_mdcenums ^property[163].code = #child 
* #_mdcenums ^property[163].valueCode = #460340 
* #_mdcenums ^property[164].code = #child 
* #_mdcenums ^property[164].valueCode = #460341 
* #_mdcenums ^property[165].code = #child 
* #_mdcenums ^property[165].valueCode = #460342 
* #_mdcenums ^property[166].code = #child 
* #_mdcenums ^property[166].valueCode = #460357 
* #_mdcenums ^property[167].code = #child 
* #_mdcenums ^property[167].valueCode = #460358 
* #_mdcenums ^property[168].code = #child 
* #_mdcenums ^property[168].valueCode = #460365 
* #_mdcenums ^property[169].code = #child 
* #_mdcenums ^property[169].valueCode = #460366 
* #_mdcenums ^property[170].code = #child 
* #_mdcenums ^property[170].valueCode = #460368 
* #_mdcenums ^property[171].code = #child 
* #_mdcenums ^property[171].valueCode = #460369 
* #_mdcenums ^property[172].code = #child 
* #_mdcenums ^property[172].valueCode = #460370 
* #_mdcenums ^property[173].code = #child 
* #_mdcenums ^property[173].valueCode = #460372 
* #_mdcenums ^property[174].code = #child 
* #_mdcenums ^property[174].valueCode = #460373 
* #_mdcenums ^property[175].code = #child 
* #_mdcenums ^property[175].valueCode = #460374 
* #_mdcenums ^property[176].code = #child 
* #_mdcenums ^property[176].valueCode = #460420 
* #_mdcenums ^property[177].code = #child 
* #_mdcenums ^property[177].valueCode = #460492 
* #_mdcenums ^property[178].code = #child 
* #_mdcenums ^property[178].valueCode = #460500 
* #_mdcenums ^property[179].code = #child 
* #_mdcenums ^property[179].valueCode = #460501 
* #_mdcenums ^property[180].code = #child 
* #_mdcenums ^property[180].valueCode = #460502 
* #_mdcenums ^property[181].code = #child 
* #_mdcenums ^property[181].valueCode = #460521 
* #_mdcenums ^property[182].code = #child 
* #_mdcenums ^property[182].valueCode = #460522 
* #_mdcenums ^property[183].code = #child 
* #_mdcenums ^property[183].valueCode = #460524 
* #_mdcenums ^property[184].code = #child 
* #_mdcenums ^property[184].valueCode = #460525 
* #_mdcenums ^property[185].code = #child 
* #_mdcenums ^property[185].valueCode = #460526 
* #_mdcenums ^property[186].code = #child 
* #_mdcenums ^property[186].valueCode = #460533 
* #_mdcenums ^property[187].code = #child 
* #_mdcenums ^property[187].valueCode = #460534 
* #_mdcenums ^property[188].code = #child 
* #_mdcenums ^property[188].valueCode = #460536 
* #_mdcenums ^property[189].code = #child 
* #_mdcenums ^property[189].valueCode = #460537 
* #_mdcenums ^property[190].code = #child 
* #_mdcenums ^property[190].valueCode = #460538 
* #_mdcenums ^property[191].code = #child 
* #_mdcenums ^property[191].valueCode = #460597 
* #_mdcenums ^property[192].code = #child 
* #_mdcenums ^property[192].valueCode = #460598 
* #_mdcenums ^property[193].code = #child 
* #_mdcenums ^property[193].valueCode = #460601 
* #_mdcenums ^property[194].code = #child 
* #_mdcenums ^property[194].valueCode = #460602 
* #_mdcenums ^property[195].code = #child 
* #_mdcenums ^property[195].valueCode = #460605 
* #_mdcenums ^property[196].code = #child 
* #_mdcenums ^property[196].valueCode = #460606 
* #_mdcenums ^property[197].code = #child 
* #_mdcenums ^property[197].valueCode = #460609 
* #_mdcenums ^property[198].code = #child 
* #_mdcenums ^property[198].valueCode = #460610 
* #_mdcenums ^property[199].code = #child 
* #_mdcenums ^property[199].valueCode = #460613 
* #_mdcenums ^property[200].code = #child 
* #_mdcenums ^property[200].valueCode = #460614 
* #_mdcenums ^property[201].code = #child 
* #_mdcenums ^property[201].valueCode = #460616 
* #_mdcenums ^property[202].code = #child 
* #_mdcenums ^property[202].valueCode = #460633 
* #_mdcenums ^property[203].code = #child 
* #_mdcenums ^property[203].valueCode = #460634 
* #_mdcenums ^property[204].code = #child 
* #_mdcenums ^property[204].valueCode = #460635 
* #_mdcenums ^property[205].code = #child 
* #_mdcenums ^property[205].valueCode = #460636 
* #_mdcenums ^property[206].code = #child 
* #_mdcenums ^property[206].valueCode = #460637 
* #_mdcenums ^property[207].code = #child 
* #_mdcenums ^property[207].valueCode = #460638 
* #_mdcenums ^property[208].code = #child 
* #_mdcenums ^property[208].valueCode = #460800 
* #_mdcenums ^property[209].code = #child 
* #_mdcenums ^property[209].valueCode = #460801 
* #_mdcenums ^property[210].code = #child 
* #_mdcenums ^property[210].valueCode = #460802 
* #_mdcenums ^property[211].code = #child 
* #_mdcenums ^property[211].valueCode = #460803 
* #_mdcenums ^property[212].code = #child 
* #_mdcenums ^property[212].valueCode = #460804 
* #_mdcenums ^property[213].code = #child 
* #_mdcenums ^property[213].valueCode = #460805 
* #_mdcenums ^property[214].code = #child 
* #_mdcenums ^property[214].valueCode = #460806 
* #_mdcenums ^property[215].code = #child 
* #_mdcenums ^property[215].valueCode = #460807 
* #_mdcenums ^property[216].code = #child 
* #_mdcenums ^property[216].valueCode = #460809 
* #_mdcenums ^property[217].code = #child 
* #_mdcenums ^property[217].valueCode = #460810 
* #_mdcenums ^property[218].code = #child 
* #_mdcenums ^property[218].valueCode = #460812 
* #_mdcenums ^property[219].code = #child 
* #_mdcenums ^property[219].valueCode = #460813 
* #_mdcenums ^property[220].code = #child 
* #_mdcenums ^property[220].valueCode = #460824 
* #_mdcenums ^property[221].code = #child 
* #_mdcenums ^property[221].valueCode = #460825 
* #_mdcenums ^property[222].code = #child 
* #_mdcenums ^property[222].valueCode = #466946 
* #_mdcenums ^property[223].code = #child 
* #_mdcenums ^property[223].valueCode = #466948 
* #_mdcenums ^property[224].code = #child 
* #_mdcenums ^property[224].valueCode = #8417824 
* #_mdcenums ^property[225].code = #child 
* #_mdcenums ^property[225].valueCode = #8417828 
* #_mdcenums ^property[226].code = #child 
* #_mdcenums ^property[226].valueCode = #8417832 
* #_mdcenums ^property[227].code = #child 
* #_mdcenums ^property[227].valueCode = #8417836 
* #_mdcenums ^property[228].code = #child 
* #_mdcenums ^property[228].valueCode = #8417840 
* #_mdcenums ^property[229].code = #child 
* #_mdcenums ^property[229].valueCode = #8417845 
* #_mdcenums ^property[230].code = #child 
* #_mdcenums ^property[230].valueCode = #8417846 
* #_mdcenums ^property[231].code = #child 
* #_mdcenums ^property[231].valueCode = #8417848 
* #_mdcenums ^property[232].code = #child 
* #_mdcenums ^property[232].valueCode = #8417852 
* #_mdcenums ^property[233].code = #child 
* #_mdcenums ^property[233].valueCode = #8417856 
* #_mdcenums ^property[234].code = #child 
* #_mdcenums ^property[234].valueCode = #8417860 
* #_mdcenums ^property[235].code = #child 
* #_mdcenums ^property[235].valueCode = #8417868 
* #_mdcenums ^property[236].code = #child 
* #_mdcenums ^property[236].valueCode = #8417869 
* #_mdcenums ^property[237].code = #child 
* #_mdcenums ^property[237].valueCode = #8417872 
* #_mdcenums ^property[238].code = #child 
* #_mdcenums ^property[238].valueCode = #8417876 
* #_mdcenums ^property[239].code = #child 
* #_mdcenums ^property[239].valueCode = #8417880 
* #_mdcenums ^property[240].code = #child 
* #_mdcenums ^property[240].valueCode = #8417888 
* #_mdcenums ^property[241].code = #child 
* #_mdcenums ^property[241].valueCode = #8417892 
* #_mdcenums ^property[242].code = #child 
* #_mdcenums ^property[242].valueCode = #8417896 
* #_mdcenums ^property[243].code = #child 
* #_mdcenums ^property[243].valueCode = #8417925 
* #_mdcenums ^property[244].code = #child 
* #_mdcenums ^property[244].valueCode = #8417926 
* #_mdcenums ^property[245].code = #child 
* #_mdcenums ^property[245].valueCode = #8417927 
* #157989 "MDC_TIME_PCA_LOCKOUT"
* #157989 ^property[0].code = #parent 
* #157989 ^property[0].valueCode = #_mdcenums 
* #157989 ^property[1].code = #hints 
* #157989 ^property[1].valueString = "PART: 2 ~ CODE10: 26917" 
* #157998 "MDC_TIME_PD_DOSE_START_INTERVAL"
* #157998 ^property[0].code = #parent 
* #157998 ^property[0].valueCode = #_mdcenums 
* #157998 ^property[1].code = #hints 
* #157998 ^property[1].valueString = "PART: 2 ~ CODE10: 26926" 
* #196634 "MDC_EVT_EMPTY"
* #196634 ^property[0].code = #parent 
* #196634 ^property[0].valueCode = #_mdcenums 
* #196634 ^property[1].code = #hints 
* #196634 ^property[1].valueString = "PART: 3 ~ CODE10: 26" 
* #196678 "MDC_EVT_MALF"
* #196678 ^property[0].code = #parent 
* #196678 ^property[0].valueCode = #_mdcenums 
* #196678 ^property[1].code = #hints 
* #196678 ^property[1].valueString = "PART: 3 ~ CODE10: 70" 
* #196768 "MDC_EVT_POSN_PROB"
* #196768 ^property[0].code = #parent 
* #196768 ^property[0].valueCode = #_mdcenums 
* #196768 ^property[1].code = #hints 
* #196768 ^property[1].valueString = "PART: 3 ~ CODE10: 160" 
* #196802 "MDC_EVT_BATT_LO"
* #196802 ^property[0].code = #parent 
* #196802 ^property[0].valueCode = #_mdcenums 
* #196802 ^property[1].code = #hints 
* #196802 ^property[1].valueString = "PART: 3 ~ CODE10: 194" 
* #196940 "MDC_EVT_FLUID_LINE_OCCL"
* #196940 ^property[0].code = #parent 
* #196940 ^property[0].valueCode = #_mdcenums 
* #196940 ^property[1].code = #hints 
* #196940 ^property[1].valueString = "PART: 3 ~ CODE10: 332" 
* #196950 "MDC_EVT_FLOW_LO"
* #196950 ^property[0].code = #parent 
* #196950 ^property[0].valueCode = #_mdcenums 
* #196950 ^property[1].code = #hints 
* #196950 ^property[1].valueString = "PART: 3 ~ CODE10: 342" 
* #197084 "MDC_EVT_DOOR_POSN_ERR"
* #197084 ^property[0].code = #parent 
* #197084 ^property[0].valueCode = #_mdcenums 
* #197084 ^property[1].code = #hints 
* #197084 ^property[1].valueString = "PART: 3 ~ CODE10: 476" 
* #197200 "MDC_EVT_FLUID_LINE_AIR"
* #197200 ^property[0].code = #parent 
* #197200 ^property[0].valueCode = #_mdcenums 
* #197200 ^property[1].code = #hints 
* #197200 ^property[1].valueString = "PART: 3 ~ CODE10: 592" 
* #197212 "MDC_EVT_SYRINGE_PATIENT_PRESSURE"
* #197212 ^property[0].code = #parent 
* #197212 ^property[0].valueCode = #_mdcenums 
* #197212 ^property[1].code = #hints 
* #197212 ^property[1].valueString = "PART: 3 ~ CODE10: 604" 
* #197214 "MDC_EVT_SYRINGE_PLUNGER_POSITION"
* #197214 ^property[0].code = #parent 
* #197214 ^property[0].valueCode = #_mdcenums 
* #197214 ^property[1].code = #hints 
* #197214 ^property[1].valueString = "PART: 3 ~ CODE10: 606" 
* #197216 "MDC_EVT_SYRINGE_FLANGE_POSITION"
* #197216 ^property[0].code = #parent 
* #197216 ^property[0].valueCode = #_mdcenums 
* #197216 ^property[1].code = #hints 
* #197216 ^property[1].valueString = "PART: 3 ~ CODE10: 608" 
* #197218 "MDC_EVT_SYRINGE_BARREL_CAPTURE"
* #197218 ^property[0].code = #parent 
* #197218 ^property[0].valueCode = #_mdcenums 
* #197218 ^property[1].code = #hints 
* #197218 ^property[1].valueString = "PART: 3 ~ CODE10: 610" 
* #197220 "MDC_EVT_SYRINGE_PRESSURE_DISC_POSITION"
* #197220 ^property[0].code = #parent 
* #197220 ^property[0].valueCode = #_mdcenums 
* #197220 ^property[1].code = #hints 
* #197220 ^property[1].valueString = "PART: 3 ~ CODE10: 612" 
* #197222 "MDC_EVT_SYRINGE_NUT_ENGAGED"
* #197222 ^property[0].code = #parent 
* #197222 ^property[0].valueCode = #_mdcenums 
* #197222 ^property[1].code = #hints 
* #197222 ^property[1].valueString = "PART: 3 ~ CODE10: 614" 
* #197224 "MDC_EVT_SYRINGE_END_OF_TRAVEL"
* #197224 ^property[0].code = #parent 
* #197224 ^property[0].valueCode = #_mdcenums 
* #197224 ^property[1].code = #hints 
* #197224 ^property[1].valueString = "PART: 3 ~ CODE10: 616" 
* #197226 "MDC_EVT_SYRINGE_EMPTY"
* #197226 ^property[0].code = #parent 
* #197226 ^property[0].valueCode = #_mdcenums 
* #197226 ^property[1].code = #hints 
* #197226 ^property[1].valueString = "PART: 3 ~ CODE10: 618" 
* #197228 "MDC_EVT_SYRINGE_PATIENT_PRESSURE_ALARM"
* #197228 ^property[0].code = #parent 
* #197228 ^property[0].valueCode = #_mdcenums 
* #197228 ^property[1].code = #hints 
* #197228 ^property[1].valueString = "PART: 3 ~ CODE10: 620" 
* #197230 "MDC_EVT_SYRINGE_PLUNGER"
* #197230 ^property[0].code = #parent 
* #197230 ^property[0].valueCode = #_mdcenums 
* #197230 ^property[1].code = #hints 
* #197230 ^property[1].valueString = "PART: 3 ~ CODE10: 622" 
* #197232 "MDC_EVT_SYRINGE_FLANGE"
* #197232 ^property[0].code = #parent 
* #197232 ^property[0].valueCode = #_mdcenums 
* #197232 ^property[1].code = #hints 
* #197232 ^property[1].valueString = "PART: 3 ~ CODE10: 624" 
* #197234 "MDC_EVT_SYRINGE_LEVER"
* #197234 ^property[0].code = #parent 
* #197234 ^property[0].valueCode = #_mdcenums 
* #197234 ^property[1].code = #hints 
* #197234 ^property[1].valueString = "PART: 3 ~ CODE10: 626" 
* #197236 "MDC_EVT_SYRINGE_PRESSURE_DISC"
* #197236 ^property[0].code = #parent 
* #197236 ^property[0].valueCode = #_mdcenums 
* #197236 ^property[1].code = #hints 
* #197236 ^property[1].valueString = "PART: 3 ~ CODE10: 628" 
* #197258 "MDC_EVT_PCA_DOOR_UNLOCKED"
* #197258 ^property[0].code = #parent 
* #197258 ^property[0].valueCode = #_mdcenums 
* #197258 ^property[1].code = #hints 
* #197258 ^property[1].valueString = "PART: 3 ~ CODE10: 650" 
* #197260 "MDC_EVT_PCA_HANDSET_DETACHED"
* #197260 ^property[0].code = #parent 
* #197260 ^property[0].valueCode = #_mdcenums 
* #197260 ^property[1].code = #hints 
* #197260 ^property[1].valueString = "PART: 3 ~ CODE10: 652" 
* #197262 "MDC_EVT_PCA_MAX_LIMIT"
* #197262 ^property[0].code = #parent 
* #197262 ^property[0].valueCode = #_mdcenums 
* #197262 ^property[1].code = #hints 
* #197262 ^property[1].valueString = "PART: 3 ~ CODE10: 654" 
* #197264 "MDC_EVT_PCA_PAUSED"
* #197264 ^property[0].code = #parent 
* #197264 ^property[0].valueCode = #_mdcenums 
* #197264 ^property[1].code = #hints 
* #197264 ^property[1].valueString = "PART: 3 ~ CODE10: 656" 
* #197288 "MDC_EVT_PUMP_DELIV_START"
* #197288 ^property[0].code = #parent 
* #197288 ^property[0].valueCode = #_mdcenums 
* #197288 ^property[1].code = #hints 
* #197288 ^property[1].valueString = "PART: 3 ~ CODE10: 680" 
* #197290 "MDC_EVT_PUMP_DELIV_STOP"
* #197290 ^property[0].code = #parent 
* #197290 ^property[0].valueCode = #_mdcenums 
* #197290 ^property[1].code = #hints 
* #197290 ^property[1].valueString = "PART: 3 ~ CODE10: 682" 
* #197292 "MDC_EVT_PUMP_DELIV_COMP"
* #197292 ^property[0].code = #parent 
* #197292 ^property[0].valueCode = #_mdcenums 
* #197292 ^property[1].code = #hints 
* #197292 ^property[1].valueString = "PART: 3 ~ CODE10: 684" 
* #197294 "MDC_EVT_COMM_STATUS_CHANGE"
* #197294 ^property[0].code = #parent 
* #197294 ^property[0].valueCode = #_mdcenums 
* #197294 ^property[1].code = #hints 
* #197294 ^property[1].valueString = "PART: 3 ~ CODE10: 686" 
* #197296 "MDC_EVT_PUMP_PROG_CLEARED"
* #197296 ^property[0].code = #parent 
* #197296 ^property[0].valueCode = #_mdcenums 
* #197296 ^property[1].code = #hints 
* #197296 ^property[1].valueString = "PART: 3 ~ CODE10: 688" 
* #197298 "MDC_EVT_PUMP_AUTO_PROG_CLEARED"
* #197298 ^property[0].code = #parent 
* #197298 ^property[0].valueCode = #_mdcenums 
* #197298 ^property[1].code = #hints 
* #197298 ^property[1].valueString = "PART: 3 ~ CODE10: 690" 
* #197300 "MDC_EVT_PATIENT_CHANGE"
* #197300 ^property[0].code = #parent 
* #197300 ^property[0].valueCode = #_mdcenums 
* #197300 ^property[1].code = #hints 
* #197300 ^property[1].valueString = "PART: 3 ~ CODE10: 692" 
* #197302 "MDC_EVT_PATIENT_ID_CHANGE"
* #197302 ^property[0].code = #parent 
* #197302 ^property[0].valueCode = #_mdcenums 
* #197302 ^property[1].code = #hints 
* #197302 ^property[1].valueString = "PART: 3 ~ CODE10: 694" 
* #197304 "MDC_EVT_PATIENT_WEIGHT_CHANGE"
* #197304 ^property[0].code = #parent 
* #197304 ^property[0].valueCode = #_mdcenums 
* #197304 ^property[1].code = #hints 
* #197304 ^property[1].valueString = "PART: 3 ~ CODE10: 696" 
* #197306 "MDC_EVT_PUMP_VOL_COUNTERS_CLEARED"
* #197306 ^property[0].code = #parent 
* #197306 ^property[0].valueCode = #_mdcenums 
* #197306 ^property[1].code = #hints 
* #197306 ^property[1].valueString = "PART: 3 ~ CODE10: 698" 
* #197308 "MDC_EVT_DEVICE_TIME_CHANGED"
* #197308 ^property[0].code = #parent 
* #197308 ^property[0].valueCode = #_mdcenums 
* #197308 ^property[1].code = #hints 
* #197308 ^property[1].valueString = "PART: 3 ~ CODE10: 700" 
* #197328 "MDC_EVT_IDLE"
* #197328 ^property[0].code = #parent 
* #197328 ^property[0].valueCode = #_mdcenums 
* #197328 ^property[1].code = #hints 
* #197328 ^property[1].valueString = "PART: 3 ~ CODE10: 720" 
* #197330 "MDC_EVT_CALLBACK"
* #197330 ^property[0].code = #parent 
* #197330 ^property[0].valueCode = #_mdcenums 
* #197330 ^property[1].code = #hints 
* #197330 ^property[1].valueString = "PART: 3 ~ CODE10: 722" 
* #197332 "MDC_EVT_VOL_INFUS_COMP"
* #197332 ^property[0].code = #parent 
* #197332 ^property[0].valueCode = #_mdcenums 
* #197332 ^property[1].code = #hints 
* #197332 ^property[1].valueString = "PART: 3 ~ CODE10: 724" 
* #197334 "MDC_EVT_VOL_INFUS_NEAR_COMP"
* #197334 ^property[0].code = #parent 
* #197334 ^property[0].valueCode = #_mdcenums 
* #197334 ^property[1].code = #hints 
* #197334 ^property[1].valueString = "PART: 3 ~ CODE10: 726" 
* #197336 "MDC_EVT_BATT_DEPL"
* #197336 ^property[0].code = #parent 
* #197336 ^property[0].valueCode = #_mdcenums 
* #197336 ^property[1].code = #hints 
* #197336 ^property[1].valueString = "PART: 3 ~ CODE10: 728" 
* #197338 "MDC_EVT_BATT_SERV"
* #197338 ^property[0].code = #parent 
* #197338 ^property[0].valueCode = #_mdcenums 
* #197338 ^property[1].code = #hints 
* #197338 ^property[1].valueString = "PART: 3 ~ CODE10: 730" 
* #197340 "MDC_EVT_EMER_STOP"
* #197340 ^property[0].code = #parent 
* #197340 ^property[0].valueCode = #_mdcenums 
* #197340 ^property[1].code = #hints 
* #197340 ^property[1].valueString = "PART: 3 ~ CODE10: 732" 
* #197342 "MDC_EVT_PWR_LOSS"
* #197342 ^property[0].code = #parent 
* #197342 ^property[0].valueCode = #_mdcenums 
* #197342 ^property[1].code = #hints 
* #197342 ^property[1].valueString = "PART: 3 ~ CODE10: 734" 
* #197346 "MDC_EVT_CHECK_IV_SET"
* #197346 ^property[0].code = #parent 
* #197346 ^property[0].valueCode = #_mdcenums 
* #197346 ^property[1].code = #hints 
* #197346 ^property[1].valueString = "PART: 3 ~ CODE10: 738" 
* #197348 "MDC_EVT_UNKNOWN"
* #197348 ^property[0].code = #parent 
* #197348 ^property[0].valueCode = #_mdcenums 
* #197348 ^property[1].code = #hints 
* #197348 ^property[1].valueString = "PART: 3 ~ CODE10: 740" 
* #197350 "MDC_EVT_DELAY_IMPOSSIBLE"
* #197350 ^property[0].code = #parent 
* #197350 ^property[0].valueCode = #_mdcenums 
* #197350 ^property[1].code = #hints 
* #197350 ^property[1].valueString = "PART: 3 ~ CODE10: 742" 
* #197352 "MDC_EVT_PUMP_CHAMBER_BLOCKED"
* #197352 ^property[0].code = #parent 
* #197352 ^property[0].valueCode = #_mdcenums 
* #197352 ^property[1].code = #hints 
* #197352 ^property[1].valueString = "PART: 3 ~ CODE10: 744" 
* #197354 "MDC_EVT_HANDSET_DETACHED"
* #197354 ^property[0].code = #parent 
* #197354 ^property[0].valueCode = #_mdcenums 
* #197354 ^property[1].code = #hints 
* #197354 ^property[1].valueString = "PART: 3 ~ CODE10: 746" 
* #197356 "MDC_EVT_PATIENT_PARAMETER_CHANGE"
* #197356 ^property[0].code = #parent 
* #197356 ^property[0].valueCode = #_mdcenums 
* #197356 ^property[1].code = #hints 
* #197356 ^property[1].valueString = "PART: 3 ~ CODE10: 748" 
* #197358 "MDC_EVT_FLUID_LINE_OCCL_PROXIMAL"
* #197358 ^property[0].code = #parent 
* #197358 ^property[0].valueCode = #_mdcenums 
* #197358 ^property[1].code = #hints 
* #197358 ^property[1].valueString = "PART: 3 ~ CODE10: 750" 
* #197360 "MDC_EVT_FLUID_LINE_OCCL_DISTAL"
* #197360 ^property[0].code = #parent 
* #197360 ^property[0].valueCode = #_mdcenums 
* #197360 ^property[1].code = #hints 
* #197360 ^property[1].valueString = "PART: 3 ~ CODE10: 752" 
* #197362 "MDC_EVT_FLUID_LINE_AIR_PROXIMAL"
* #197362 ^property[0].code = #parent 
* #197362 ^property[0].valueCode = #_mdcenums 
* #197362 ^property[1].code = #hints 
* #197362 ^property[1].valueString = "PART: 3 ~ CODE10: 754" 
* #197364 "MDC_EVT_FLUID_LINE_AIR_DISTAL"
* #197364 ^property[0].code = #parent 
* #197364 ^property[0].valueCode = #_mdcenums 
* #197364 ^property[1].code = #hints 
* #197364 ^property[1].valueString = "PART: 3 ~ CODE10: 756" 
* #197376 "MDC_EVT_DATA_INVALID"
* #197376 ^property[0].code = #parent 
* #197376 ^property[0].valueCode = #_mdcenums 
* #197376 ^property[1].code = #hints 
* #197376 ^property[1].valueString = "PART: 3 ~ CODE10: 768" 
* #197378 "MDC_EVT_DATA_MISSING"
* #197378 ^property[0].code = #parent 
* #197378 ^property[0].valueCode = #_mdcenums 
* #197378 ^property[1].code = #hints 
* #197378 ^property[1].valueString = "PART: 3 ~ CODE10: 770" 
* #203276 "MDC_EVT_ADVIS_SETTINGS_CHK"
* #203276 ^property[0].code = #parent 
* #203276 ^property[0].valueCode = #_mdcenums 
* #203276 ^property[1].code = #hints 
* #203276 ^property[1].valueString = "PART: 3 ~ CODE10: 6668" 
* #203341 "MDC_EVT_ADVIS_MAINT_NEEDED"
* #203341 ^property[0].code = #parent 
* #203341 ^property[0].valueCode = #_mdcenums 
* #203341 ^property[1].code = #hints 
* #203341 ^property[1].valueString = "PART: 3 ~ CODE10: 6733" 
* #203776 "MDC_EVT_LS_DEVICE"
* #203776 ^property[0].code = #parent 
* #203776 ^property[0].valueCode = #_mdcenums 
* #203776 ^property[1].code = #hints 
* #203776 ^property[1].valueString = "PART: 3 ~ CODE10: 7168" 
* #203778 "MDC_EVT_LS_PERSON"
* #203778 ^property[0].code = #parent 
* #203778 ^property[0].valueCode = #_mdcenums 
* #203778 ^property[1].code = #hints 
* #203778 ^property[1].valueString = "PART: 3 ~ CODE10: 7170" 
* #203780 "MDC_EVT_LS_MOVEMENT"
* #203780 ^property[0].code = #parent 
* #203780 ^property[0].valueCode = #_mdcenums 
* #203780 ^property[1].code = #hints 
* #203780 ^property[1].valueString = "PART: 3 ~ CODE10: 7172" 
* #203782 "MDC_EVT_LS_BOUNDARY"
* #203782 ^property[0].code = #parent 
* #203782 ^property[0].valueCode = #_mdcenums 
* #203782 ^property[1].code = #hints 
* #203782 ^property[1].valueString = "PART: 3 ~ CODE10: 7174" 
* #203784 "MDC_EVT_LS_COLOCATION"
* #203784 ^property[0].code = #parent 
* #203784 ^property[0].valueCode = #_mdcenums 
* #203784 ^property[1].code = #hints 
* #203784 ^property[1].valueString = "PART: 3 ~ CODE10: 7176" 
* #203786 "MDC_EVT_LS_DWELL"
* #203786 ^property[0].code = #parent 
* #203786 ^property[0].valueCode = #_mdcenums 
* #203786 ^property[1].code = #hints 
* #203786 ^property[1].valueString = "PART: 3 ~ CODE10: 7178" 
* #203788 "MDC_EVT_LS_ACCELEROMETER"
* #203788 ^property[0].code = #parent 
* #203788 ^property[0].valueCode = #_mdcenums 
* #203788 ^property[1].code = #hints 
* #203788 ^property[1].valueString = "PART: 3 ~ CODE10: 7180" 
* #203790 "MDC_EVT_LS_TAMPER"
* #203790 ^property[0].code = #parent 
* #203790 ^property[0].valueCode = #_mdcenums 
* #203790 ^property[1].code = #hints 
* #203790 ^property[1].valueString = "PART: 3 ~ CODE10: 7182" 
* #203792 "MDC_EVT_LS_INTERACTION"
* #203792 ^property[0].code = #parent 
* #203792 ^property[0].valueCode = #_mdcenums 
* #203792 ^property[1].code = #hints 
* #203792 ^property[1].valueString = "PART: 3 ~ CODE10: 7184" 
* #203794 "MDC_EVT_LS_MISSING"
* #203794 ^property[0].code = #parent 
* #203794 ^property[0].valueCode = #_mdcenums 
* #203794 ^property[1].code = #hints 
* #203794 ^property[1].valueString = "PART: 3 ~ CODE10: 7186" 
* #203796 "MDC_EVT_LS_ENVIRONMENT"
* #203796 ^property[0].code = #parent 
* #203796 ^property[0].valueCode = #_mdcenums 
* #203796 ^property[1].code = #hints 
* #203796 ^property[1].valueString = "PART: 3 ~ CODE10: 7188" 
* #203798 "MDC_EVT_LS_BATTERY"
* #203798 ^property[0].code = #parent 
* #203798 ^property[0].valueCode = #_mdcenums 
* #203798 ^property[1].code = #hints 
* #203798 ^property[1].valueString = "PART: 3 ~ CODE10: 7190" 
* #258048 "MDC_EVT_STANDBY_WARN"
* #258048 ^property[0].code = #parent 
* #258048 ^property[0].valueCode = #_mdcenums 
* #258048 ^property[1].code = #hints 
* #258048 ^property[1].valueString = "PART: 3 ~ CODE10: 61440" 
* #459748 "MDC_HEAD_NASION_MID"
* #459748 ^property[0].code = #parent 
* #459748 ^property[0].valueCode = #_mdcenums 
* #459748 ^property[1].code = #hints 
* #459748 ^property[1].valueString = "PART: 7 ~ CODE10: 996" 
* #459752 "MDC_HEAD_FRONT_POLAR_MID"
* #459752 ^property[0].code = #parent 
* #459752 ^property[0].valueCode = #_mdcenums 
* #459752 ^property[1].code = #hints 
* #459752 ^property[1].valueString = "PART: 7 ~ CODE10: 1000" 
* #459756 "MDC_HEAD_FRONT_ANT_MID"
* #459756 ^property[0].code = #parent 
* #459756 ^property[0].valueCode = #_mdcenums 
* #459756 ^property[1].code = #hints 
* #459756 ^property[1].valueString = "PART: 7 ~ CODE10: 1004" 
* #459760 "MDC_HEAD_FRONT_MID"
* #459760 ^property[0].code = #parent 
* #459760 ^property[0].valueCode = #_mdcenums 
* #459760 ^property[1].code = #hints 
* #459760 ^property[1].valueString = "PART: 7 ~ CODE10: 1008" 
* #459764 "MDC_HEAD_FRONT_CENT_MID"
* #459764 ^property[0].code = #parent 
* #459764 ^property[0].valueCode = #_mdcenums 
* #459764 ^property[1].code = #hints 
* #459764 ^property[1].valueString = "PART: 7 ~ CODE10: 1012" 
* #459768 "MDC_HEAD_CENT_MID"
* #459768 ^property[0].code = #parent 
* #459768 ^property[0].valueCode = #_mdcenums 
* #459768 ^property[1].code = #hints 
* #459768 ^property[1].valueString = "PART: 7 ~ CODE10: 1016" 
* #459772 "MDC_HEAD_PARIET_MEDIA"
* #459772 ^property[0].code = #parent 
* #459772 ^property[0].valueCode = #_mdcenums 
* #459772 ^property[1].code = #hints 
* #459772 ^property[1].valueString = "PART: 7 ~ CODE10: 1020" 
* #459776 "MDC_HEAD_PARIET_MID"
* #459776 ^property[0].code = #parent 
* #459776 ^property[0].valueCode = #_mdcenums 
* #459776 ^property[1].code = #hints 
* #459776 ^property[1].valueString = "PART: 7 ~ CODE10: 1024" 
* #459780 "MDC_HEAD_PARIET_OCCIP_MID"
* #459780 ^property[0].code = #parent 
* #459780 ^property[0].valueCode = #_mdcenums 
* #459780 ^property[1].code = #hints 
* #459780 ^property[1].valueString = "PART: 7 ~ CODE10: 1028" 
* #459784 "MDC_HEAD_OCCIP_MID"
* #459784 ^property[0].code = #parent 
* #459784 ^property[0].valueCode = #_mdcenums 
* #459784 ^property[1].code = #hints 
* #459784 ^property[1].valueString = "PART: 7 ~ CODE10: 1032" 
* #459788 "MDC_HEAD_INION_MID"
* #459788 ^property[0].code = #parent 
* #459788 ^property[0].valueCode = #_mdcenums 
* #459788 ^property[1].code = #hints 
* #459788 ^property[1].valueString = "PART: 7 ~ CODE10: 1036" 
* #459793 "MDC_HEAD_FRONT_POLAR_L"
* #459793 ^property[0].code = #parent 
* #459793 ^property[0].valueCode = #_mdcenums 
* #459793 ^property[1].code = #hints 
* #459793 ^property[1].valueString = "PART: 7 ~ CODE10: 1041" 
* #459794 "MDC_HEAD_FRONT_POLAR_R"
* #459794 ^property[0].code = #parent 
* #459794 ^property[0].valueCode = #_mdcenums 
* #459794 ^property[1].code = #hints 
* #459794 ^property[1].valueString = "PART: 7 ~ CODE10: 1042" 
* #459801 "MDC_HEAD_FRONT_L_1"
* #459801 ^property[0].code = #parent 
* #459801 ^property[0].valueCode = #_mdcenums 
* #459801 ^property[1].code = #hints 
* #459801 ^property[1].valueString = "PART: 7 ~ CODE10: 1049" 
* #459806 "MDC_HEAD_FRONT_R_2"
* #459806 ^property[0].code = #parent 
* #459806 ^property[0].valueCode = #_mdcenums 
* #459806 ^property[1].code = #hints 
* #459806 ^property[1].valueString = "PART: 7 ~ CODE10: 1054" 
* #459809 "MDC_HEAD_FRONT_L_3"
* #459809 ^property[0].code = #parent 
* #459809 ^property[0].valueCode = #_mdcenums 
* #459809 ^property[1].code = #hints 
* #459809 ^property[1].valueString = "PART: 7 ~ CODE10: 1057" 
* #459814 "MDC_HEAD_FRONT_R_4"
* #459814 ^property[0].code = #parent 
* #459814 ^property[0].valueCode = #_mdcenums 
* #459814 ^property[1].code = #hints 
* #459814 ^property[1].valueString = "PART: 7 ~ CODE10: 1062" 
* #459817 "MDC_HEAD_FRONT_L_5"
* #459817 ^property[0].code = #parent 
* #459817 ^property[0].valueCode = #_mdcenums 
* #459817 ^property[1].code = #hints 
* #459817 ^property[1].valueString = "PART: 7 ~ CODE10: 1065" 
* #459822 "MDC_HEAD_FRONT_R_6"
* #459822 ^property[0].code = #parent 
* #459822 ^property[0].valueCode = #_mdcenums 
* #459822 ^property[1].code = #hints 
* #459822 ^property[1].valueString = "PART: 7 ~ CODE10: 1070" 
* #459825 "MDC_HEAD_FRONT_L_7"
* #459825 ^property[0].code = #parent 
* #459825 ^property[0].valueCode = #_mdcenums 
* #459825 ^property[1].code = #hints 
* #459825 ^property[1].valueString = "PART: 7 ~ CODE10: 1073" 
* #459830 "MDC_HEAD_FRONT_R_8"
* #459830 ^property[0].code = #parent 
* #459830 ^property[0].valueCode = #_mdcenums 
* #459830 ^property[1].code = #hints 
* #459830 ^property[1].valueString = "PART: 7 ~ CODE10: 1078" 
* #459833 "MDC_HEAD_FRONT_L_9"
* #459833 ^property[0].code = #parent 
* #459833 ^property[0].valueCode = #_mdcenums 
* #459833 ^property[1].code = #hints 
* #459833 ^property[1].valueString = "PART: 7 ~ CODE10: 1081" 
* #459838 "MDC_HEAD_FRONT_R_10"
* #459838 ^property[0].code = #parent 
* #459838 ^property[0].valueCode = #_mdcenums 
* #459838 ^property[1].code = #hints 
* #459838 ^property[1].valueString = "PART: 7 ~ CODE10: 1086" 
* #459841 "MDC_HEAD_FRONT_CENT_L_1"
* #459841 ^property[0].code = #parent 
* #459841 ^property[0].valueCode = #_mdcenums 
* #459841 ^property[1].code = #hints 
* #459841 ^property[1].valueString = "PART: 7 ~ CODE10: 1089" 
* #459846 "MDC_HEAD_FRONT_CENT_R_2"
* #459846 ^property[0].code = #parent 
* #459846 ^property[0].valueCode = #_mdcenums 
* #459846 ^property[1].code = #hints 
* #459846 ^property[1].valueString = "PART: 7 ~ CODE10: 1094" 
* #459849 "MDC_HEAD_FRONT_CENT_L_3"
* #459849 ^property[0].code = #parent 
* #459849 ^property[0].valueCode = #_mdcenums 
* #459849 ^property[1].code = #hints 
* #459849 ^property[1].valueString = "PART: 7 ~ CODE10: 1097" 
* #459854 "MDC_HEAD_FRONT_CENT_R_4"
* #459854 ^property[0].code = #parent 
* #459854 ^property[0].valueCode = #_mdcenums 
* #459854 ^property[1].code = #hints 
* #459854 ^property[1].valueString = "PART: 7 ~ CODE10: 1102" 
* #459857 "MDC_HEAD_FRONT_CENT_L_5"
* #459857 ^property[0].code = #parent 
* #459857 ^property[0].valueCode = #_mdcenums 
* #459857 ^property[1].code = #hints 
* #459857 ^property[1].valueString = "PART: 7 ~ CODE10: 1105" 
* #459862 "MDC_HEAD_FRONT_CENT_R_6"
* #459862 ^property[0].code = #parent 
* #459862 ^property[0].valueCode = #_mdcenums 
* #459862 ^property[1].code = #hints 
* #459862 ^property[1].valueString = "PART: 7 ~ CODE10: 1110" 
* #459865 "MDC_HEAD_FRONT_TEMPOR_L_7"
* #459865 ^property[0].code = #parent 
* #459865 ^property[0].valueCode = #_mdcenums 
* #459865 ^property[1].code = #hints 
* #459865 ^property[1].valueString = "PART: 7 ~ CODE10: 1113" 
* #459870 "MDC_HEAD_FRONT_TEMPOR_R_8"
* #459870 ^property[0].code = #parent 
* #459870 ^property[0].valueCode = #_mdcenums 
* #459870 ^property[1].code = #hints 
* #459870 ^property[1].valueString = "PART: 7 ~ CODE10: 1118" 
* #459873 "MDC_HEAD_FRONT_TEMPOR_L_9"
* #459873 ^property[0].code = #parent 
* #459873 ^property[0].valueCode = #_mdcenums 
* #459873 ^property[1].code = #hints 
* #459873 ^property[1].valueString = "PART: 7 ~ CODE10: 1121" 
* #459878 "MDC_HEAD_FRONT_TEMPOR_R_10"
* #459878 ^property[0].code = #parent 
* #459878 ^property[0].valueCode = #_mdcenums 
* #459878 ^property[1].code = #hints 
* #459878 ^property[1].valueString = "PART: 7 ~ CODE10: 1126" 
* #459881 "MDC_HEAD_CENT_L_1"
* #459881 ^property[0].code = #parent 
* #459881 ^property[0].valueCode = #_mdcenums 
* #459881 ^property[1].code = #hints 
* #459881 ^property[1].valueString = "PART: 7 ~ CODE10: 1129" 
* #459886 "MDC_HEAD_CENT_R_2"
* #459886 ^property[0].code = #parent 
* #459886 ^property[0].valueCode = #_mdcenums 
* #459886 ^property[1].code = #hints 
* #459886 ^property[1].valueString = "PART: 7 ~ CODE10: 1134" 
* #459889 "MDC_HEAD_CENT_L_3"
* #459889 ^property[0].code = #parent 
* #459889 ^property[0].valueCode = #_mdcenums 
* #459889 ^property[1].code = #hints 
* #459889 ^property[1].valueString = "PART: 7 ~ CODE10: 1137" 
* #459894 "MDC_HEAD_CENT_R_4"
* #459894 ^property[0].code = #parent 
* #459894 ^property[0].valueCode = #_mdcenums 
* #459894 ^property[1].code = #hints 
* #459894 ^property[1].valueString = "PART: 7 ~ CODE10: 1142" 
* #459897 "MDC_HEAD_CENT_L_5"
* #459897 ^property[0].code = #parent 
* #459897 ^property[0].valueCode = #_mdcenums 
* #459897 ^property[1].code = #hints 
* #459897 ^property[1].valueString = "PART: 7 ~ CODE10: 1145" 
* #459902 "MDC_HEAD_CENT_R_6"
* #459902 ^property[0].code = #parent 
* #459902 ^property[0].valueCode = #_mdcenums 
* #459902 ^property[1].code = #hints 
* #459902 ^property[1].valueString = "PART: 7 ~ CODE10: 1150" 
* #459905 "MDC_HEAD_PARIET_CENT_L_1"
* #459905 ^property[0].code = #parent 
* #459905 ^property[0].valueCode = #_mdcenums 
* #459905 ^property[1].code = #hints 
* #459905 ^property[1].valueString = "PART: 7 ~ CODE10: 1153" 
* #459910 "MDC_HEAD_PARIET_CENT_R_2"
* #459910 ^property[0].code = #parent 
* #459910 ^property[0].valueCode = #_mdcenums 
* #459910 ^property[1].code = #hints 
* #459910 ^property[1].valueString = "PART: 7 ~ CODE10: 1158" 
* #459913 "MDC_HEAD_PARIET_CENT_L_3"
* #459913 ^property[0].code = #parent 
* #459913 ^property[0].valueCode = #_mdcenums 
* #459913 ^property[1].code = #hints 
* #459913 ^property[1].valueString = "PART: 7 ~ CODE10: 1161" 
* #459918 "MDC_HEAD_PARIET_CENT_R_4"
* #459918 ^property[0].code = #parent 
* #459918 ^property[0].valueCode = #_mdcenums 
* #459918 ^property[1].code = #hints 
* #459918 ^property[1].valueString = "PART: 7 ~ CODE10: 1166" 
* #459921 "MDC_HEAD_PARIET_CENT_L_5"
* #459921 ^property[0].code = #parent 
* #459921 ^property[0].valueCode = #_mdcenums 
* #459921 ^property[1].code = #hints 
* #459921 ^property[1].valueString = "PART: 7 ~ CODE10: 1169" 
* #459926 "MDC_HEAD_PARIET_CENT_R_6"
* #459926 ^property[0].code = #parent 
* #459926 ^property[0].valueCode = #_mdcenums 
* #459926 ^property[1].code = #hints 
* #459926 ^property[1].valueString = "PART: 7 ~ CODE10: 1174" 
* #459929 "MDC_HEAD_PARIET_L_1"
* #459929 ^property[0].code = #parent 
* #459929 ^property[0].valueCode = #_mdcenums 
* #459929 ^property[1].code = #hints 
* #459929 ^property[1].valueString = "PART: 7 ~ CODE10: 1177" 
* #459934 "MDC_HEAD_PARIET_R_2"
* #459934 ^property[0].code = #parent 
* #459934 ^property[0].valueCode = #_mdcenums 
* #459934 ^property[1].code = #hints 
* #459934 ^property[1].valueString = "PART: 7 ~ CODE10: 1182" 
* #459937 "MDC_HEAD_PARIET_L_3"
* #459937 ^property[0].code = #parent 
* #459937 ^property[0].valueCode = #_mdcenums 
* #459937 ^property[1].code = #hints 
* #459937 ^property[1].valueString = "PART: 7 ~ CODE10: 1185" 
* #459942 "MDC_HEAD_PARIET_R_4"
* #459942 ^property[0].code = #parent 
* #459942 ^property[0].valueCode = #_mdcenums 
* #459942 ^property[1].code = #hints 
* #459942 ^property[1].valueString = "PART: 7 ~ CODE10: 1190" 
* #459945 "MDC_HEAD_PARIET_L_5"
* #459945 ^property[0].code = #parent 
* #459945 ^property[0].valueCode = #_mdcenums 
* #459945 ^property[1].code = #hints 
* #459945 ^property[1].valueString = "PART: 7 ~ CODE10: 1193" 
* #459950 "MDC_HEAD_PARIET_R_6"
* #459950 ^property[0].code = #parent 
* #459950 ^property[0].valueCode = #_mdcenums 
* #459950 ^property[1].code = #hints 
* #459950 ^property[1].valueString = "PART: 7 ~ CODE10: 1198" 
* #459953 "MDC_HEAD_PARIET_L_9"
* #459953 ^property[0].code = #parent 
* #459953 ^property[0].valueCode = #_mdcenums 
* #459953 ^property[1].code = #hints 
* #459953 ^property[1].valueString = "PART: 7 ~ CODE10: 1201" 
* #459958 "MDC_HEAD_PARIET_R_10"
* #459958 ^property[0].code = #parent 
* #459958 ^property[0].valueCode = #_mdcenums 
* #459958 ^property[1].code = #hints 
* #459958 ^property[1].valueString = "PART: 7 ~ CODE10: 1206" 
* #459961 "MDC_HEAD_OCCIP_L"
* #459961 ^property[0].code = #parent 
* #459961 ^property[0].valueCode = #_mdcenums 
* #459961 ^property[1].code = #hints 
* #459961 ^property[1].valueString = "PART: 7 ~ CODE10: 1209" 
* #459966 "MDC_HEAD_OCCIP_R"
* #459966 ^property[0].code = #parent 
* #459966 ^property[0].valueCode = #_mdcenums 
* #459966 ^property[1].code = #hints 
* #459966 ^property[1].valueString = "PART: 7 ~ CODE10: 1214" 
* #459969 "MDC_HEAD_FRONT_ANT_L_3"
* #459969 ^property[0].code = #parent 
* #459969 ^property[0].valueCode = #_mdcenums 
* #459969 ^property[1].code = #hints 
* #459969 ^property[1].valueString = "PART: 7 ~ CODE10: 1217" 
* #459974 "MDC_HEAD_FRONT_ANT_R_4"
* #459974 ^property[0].code = #parent 
* #459974 ^property[0].valueCode = #_mdcenums 
* #459974 ^property[1].code = #hints 
* #459974 ^property[1].valueString = "PART: 7 ~ CODE10: 1222" 
* #459977 "MDC_HEAD_FRONT_ANT_L_7"
* #459977 ^property[0].code = #parent 
* #459977 ^property[0].valueCode = #_mdcenums 
* #459977 ^property[1].code = #hints 
* #459977 ^property[1].valueString = "PART: 7 ~ CODE10: 1225" 
* #459982 "MDC_HEAD_FRONT_ANT_R_8"
* #459982 ^property[0].code = #parent 
* #459982 ^property[0].valueCode = #_mdcenums 
* #459982 ^property[1].code = #hints 
* #459982 ^property[1].valueString = "PART: 7 ~ CODE10: 1230" 
* #459985 "MDC_HEAD_PARIET_OCCIP_L_3"
* #459985 ^property[0].code = #parent 
* #459985 ^property[0].valueCode = #_mdcenums 
* #459985 ^property[1].code = #hints 
* #459985 ^property[1].valueString = "PART: 7 ~ CODE10: 1233" 
* #459990 "MDC_HEAD_PARIET_OCCIP_R_4"
* #459990 ^property[0].code = #parent 
* #459990 ^property[0].valueCode = #_mdcenums 
* #459990 ^property[1].code = #hints 
* #459990 ^property[1].valueString = "PART: 7 ~ CODE10: 1238" 
* #459993 "MDC_HEAD_PARIET_OCCIP_L_7"
* #459993 ^property[0].code = #parent 
* #459993 ^property[0].valueCode = #_mdcenums 
* #459993 ^property[1].code = #hints 
* #459993 ^property[1].valueString = "PART: 7 ~ CODE10: 1241" 
* #459998 "MDC_HEAD_PARIET_OCCIP_R_8"
* #459998 ^property[0].code = #parent 
* #459998 ^property[0].valueCode = #_mdcenums 
* #459998 ^property[1].code = #hints 
* #459998 ^property[1].valueString = "PART: 7 ~ CODE10: 1246" 
* #460001 "MDC_HEAD_TEMPOR_L_3"
* #460001 ^property[0].code = #parent 
* #460001 ^property[0].valueCode = #_mdcenums 
* #460001 ^property[1].code = #hints 
* #460001 ^property[1].valueString = "PART: 7 ~ CODE10: 1249" 
* #460006 "MDC_HEAD_TEMPOR_R_4"
* #460006 ^property[0].code = #parent 
* #460006 ^property[0].valueCode = #_mdcenums 
* #460006 ^property[1].code = #hints 
* #460006 ^property[1].valueString = "PART: 7 ~ CODE10: 1254" 
* #460009 "MDC_HEAD_TEMPOR_L_5"
* #460009 ^property[0].code = #parent 
* #460009 ^property[0].valueCode = #_mdcenums 
* #460009 ^property[1].code = #hints 
* #460009 ^property[1].valueString = "PART: 7 ~ CODE10: 1257" 
* #460014 "MDC_HEAD_TEMPOR_R_6"
* #460014 ^property[0].code = #parent 
* #460014 ^property[0].valueCode = #_mdcenums 
* #460014 ^property[1].code = #hints 
* #460014 ^property[1].valueString = "PART: 7 ~ CODE10: 1262" 
* #460017 "MDC_HEAD_TEMPOR_L_9"
* #460017 ^property[0].code = #parent 
* #460017 ^property[0].valueCode = #_mdcenums 
* #460017 ^property[1].code = #hints 
* #460017 ^property[1].valueString = "PART: 7 ~ CODE10: 1265" 
* #460022 "MDC_HEAD_TEMPOR_R_10"
* #460022 ^property[0].code = #parent 
* #460022 ^property[0].valueCode = #_mdcenums 
* #460022 ^property[1].code = #hints 
* #460022 ^property[1].valueString = "PART: 7 ~ CODE10: 1270" 
* #460025 "MDC_HEAD_TEMPOR_PARIET_L_7"
* #460025 ^property[0].code = #parent 
* #460025 ^property[0].valueCode = #_mdcenums 
* #460025 ^property[1].code = #hints 
* #460025 ^property[1].valueString = "PART: 7 ~ CODE10: 1273" 
* #460030 "MDC_HEAD_TEMPOR_PARIET_R_8"
* #460030 ^property[0].code = #parent 
* #460030 ^property[0].valueCode = #_mdcenums 
* #460030 ^property[1].code = #hints 
* #460030 ^property[1].valueString = "PART: 7 ~ CODE10: 1278" 
* #460033 "MDC_HEAD_TEMPOR_PARIET_L_9"
* #460033 ^property[0].code = #parent 
* #460033 ^property[0].valueCode = #_mdcenums 
* #460033 ^property[1].code = #hints 
* #460033 ^property[1].valueString = "PART: 7 ~ CODE10: 1281" 
* #460038 "MDC_HEAD_TEMPOR_PARIET_R_10"
* #460038 ^property[0].code = #parent 
* #460038 ^property[0].valueCode = #_mdcenums 
* #460038 ^property[1].code = #hints 
* #460038 ^property[1].valueString = "PART: 7 ~ CODE10: 1286" 
* #460041 "MDC_HEAD_AURIC_L"
* #460041 ^property[0].code = #parent 
* #460041 ^property[0].valueCode = #_mdcenums 
* #460041 ^property[1].code = #hints 
* #460041 ^property[1].valueString = "PART: 7 ~ CODE10: 1289" 
* #460042 "MDC_HEAD_AURIC_R"
* #460042 ^property[0].code = #parent 
* #460042 ^property[0].valueCode = #_mdcenums 
* #460042 ^property[1].code = #hints 
* #460042 ^property[1].valueString = "PART: 7 ~ CODE10: 1290" 
* #460049 "MDC_HEAD_TEMPOR_ANT_L"
* #460049 ^property[0].code = #parent 
* #460049 ^property[0].valueCode = #_mdcenums 
* #460049 ^property[1].code = #hints 
* #460049 ^property[1].valueString = "PART: 7 ~ CODE10: 1297" 
* #460050 "MDC_HEAD_TEMPOR_ANT_R"
* #460050 ^property[0].code = #parent 
* #460050 ^property[0].valueCode = #_mdcenums 
* #460050 ^property[1].code = #hints 
* #460050 ^property[1].valueString = "PART: 7 ~ CODE10: 1298" 
* #460057 "MDC_HEAD_PHARYNGEAL_L"
* #460057 ^property[0].code = #parent 
* #460057 ^property[0].valueCode = #_mdcenums 
* #460057 ^property[1].code = #hints 
* #460057 ^property[1].valueString = "PART: 7 ~ CODE10: 1305" 
* #460058 "MDC_HEAD_PHARYNGEAL_R"
* #460058 ^property[0].code = #parent 
* #460058 ^property[0].valueCode = #_mdcenums 
* #460058 ^property[1].code = #hints 
* #460058 ^property[1].valueString = "PART: 7 ~ CODE10: 1306" 
* #460065 "MDC_HEAD_SPHENOIDAL_L"
* #460065 ^property[0].code = #parent 
* #460065 ^property[0].valueCode = #_mdcenums 
* #460065 ^property[1].code = #hints 
* #460065 ^property[1].valueString = "PART: 7 ~ CODE10: 1313" 
* #460066 "MDC_HEAD_SPHENOIDAL_R"
* #460066 ^property[0].code = #parent 
* #460066 ^property[0].valueCode = #_mdcenums 
* #460066 ^property[1].code = #hints 
* #460066 ^property[1].valueString = "PART: 7 ~ CODE10: 1314" 
* #460272 "MDC_HEAD_EAR"
* #460272 ^property[0].code = #parent 
* #460272 ^property[0].valueCode = #_mdcenums 
* #460272 ^property[1].code = #hints 
* #460272 ^property[1].valueString = "PART: 7 ~ CODE10: 1520" 
* #460280 "MDC_HEAD_FORE"
* #460280 ^property[0].code = #parent 
* #460280 ^property[0].valueCode = #_mdcenums 
* #460280 ^property[1].code = #hints 
* #460280 ^property[1].valueString = "PART: 7 ~ CODE10: 1528" 
* #460281 "MDC_HEAD_FORE_L"
* #460281 ^property[0].code = #parent 
* #460281 ^property[0].valueCode = #_mdcenums 
* #460281 ^property[1].code = #hints 
* #460281 ^property[1].valueString = "PART: 7 ~ CODE10: 1529" 
* #460282 "MDC_HEAD_FORE_R"
* #460282 ^property[0].code = #parent 
* #460282 ^property[0].valueCode = #_mdcenums 
* #460282 ^property[1].code = #hints 
* #460282 ^property[1].valueString = "PART: 7 ~ CODE10: 1530" 
* #460292 "MDC_HEAD_MOUTH"
* #460292 ^property[0].code = #parent 
* #460292 ^property[0].valueCode = #_mdcenums 
* #460292 ^property[1].code = #hints 
* #460292 ^property[1].valueString = "PART: 7 ~ CODE10: 1540" 
* #460296 "MDC_HEAD_NARIS"
* #460296 ^property[0].code = #parent 
* #460296 ^property[0].valueCode = #_mdcenums 
* #460296 ^property[1].code = #hints 
* #460296 ^property[1].valueString = "PART: 7 ~ CODE10: 1544" 
* #460297 "MDC_HEAD_NARIS_L"
* #460297 ^property[0].code = #parent 
* #460297 ^property[0].valueCode = #_mdcenums 
* #460297 ^property[1].code = #hints 
* #460297 ^property[1].valueString = "PART: 7 ~ CODE10: 1545" 
* #460298 "MDC_HEAD_NARIS_R"
* #460298 ^property[0].code = #parent 
* #460298 ^property[0].valueCode = #_mdcenums 
* #460298 ^property[1].code = #hints 
* #460298 ^property[1].valueString = "PART: 7 ~ CODE10: 1546" 
* #460304 "MDC_HEAD_NOSE"
* #460304 ^property[0].code = #parent 
* #460304 ^property[0].valueCode = #_mdcenums 
* #460304 ^property[1].code = #hints 
* #460304 ^property[1].valueString = "PART: 7 ~ CODE10: 1552" 
* #460340 "MDC_LOEXT_FOOT"
* #460340 ^property[0].code = #parent 
* #460340 ^property[0].valueCode = #_mdcenums 
* #460340 ^property[1].code = #hints 
* #460340 ^property[1].valueString = "PART: 7 ~ CODE10: 1588" 
* #460341 "MDC_LOEXT_FOOT_L"
* #460341 ^property[0].code = #parent 
* #460341 ^property[0].valueCode = #_mdcenums 
* #460341 ^property[1].code = #hints 
* #460341 ^property[1].valueString = "PART: 7 ~ CODE10: 1589" 
* #460342 "MDC_LOEXT_FOOT_R"
* #460342 ^property[0].code = #parent 
* #460342 ^property[0].valueCode = #_mdcenums 
* #460342 ^property[1].code = #hints 
* #460342 ^property[1].valueString = "PART: 7 ~ CODE10: 1590" 
* #460357 "MDC_LOEXT_LEG_L"
* #460357 ^property[0].code = #parent 
* #460357 ^property[0].valueCode = #_mdcenums 
* #460357 ^property[1].code = #hints 
* #460357 ^property[1].valueString = "PART: 7 ~ CODE10: 1605" 
* #460358 "MDC_LOEXT_LEG_R"
* #460358 ^property[0].code = #parent 
* #460358 ^property[0].valueCode = #_mdcenums 
* #460358 ^property[1].code = #hints 
* #460358 ^property[1].valueString = "PART: 7 ~ CODE10: 1606" 
* #460365 "MDC_LOEXT_THIGH_L"
* #460365 ^property[0].code = #parent 
* #460365 ^property[0].valueCode = #_mdcenums 
* #460365 ^property[1].code = #hints 
* #460365 ^property[1].valueString = "PART: 7 ~ CODE10: 1613" 
* #460366 "MDC_LOEXT_THIGH_R"
* #460366 ^property[0].code = #parent 
* #460366 ^property[0].valueCode = #_mdcenums 
* #460366 ^property[1].code = #hints 
* #460366 ^property[1].valueString = "PART: 7 ~ CODE10: 1614" 
* #460368 "MDC_LOEXT_TOE"
* #460368 ^property[0].code = #parent 
* #460368 ^property[0].valueCode = #_mdcenums 
* #460368 ^property[1].code = #hints 
* #460368 ^property[1].valueString = "PART: 7 ~ CODE10: 1616" 
* #460369 "MDC_LOEXT_TOE_L"
* #460369 ^property[0].code = #parent 
* #460369 ^property[0].valueCode = #_mdcenums 
* #460369 ^property[1].code = #hints 
* #460369 ^property[1].valueString = "PART: 7 ~ CODE10: 1617" 
* #460370 "MDC_LOEXT_TOE_R"
* #460370 ^property[0].code = #parent 
* #460370 ^property[0].valueCode = #_mdcenums 
* #460370 ^property[1].code = #hints 
* #460370 ^property[1].valueString = "PART: 7 ~ CODE10: 1618" 
* #460372 "MDC_LOEXT_TOE_GREAT"
* #460372 ^property[0].code = #parent 
* #460372 ^property[0].valueCode = #_mdcenums 
* #460372 ^property[1].code = #hints 
* #460372 ^property[1].valueString = "PART: 7 ~ CODE10: 1620" 
* #460373 "MDC_LOEXT_TOE_GREAT_L"
* #460373 ^property[0].code = #parent 
* #460373 ^property[0].valueCode = #_mdcenums 
* #460373 ^property[1].code = #hints 
* #460373 ^property[1].valueString = "PART: 7 ~ CODE10: 1621" 
* #460374 "MDC_LOEXT_TOE_GREAT_R"
* #460374 ^property[0].code = #parent 
* #460374 ^property[0].valueCode = #_mdcenums 
* #460374 ^property[1].code = #hints 
* #460374 ^property[1].valueString = "PART: 7 ~ CODE10: 1622" 
* #460420 "MDC_TRUNK_BUTTOCK"
* #460420 ^property[0].code = #parent 
* #460420 ^property[0].valueCode = #_mdcenums 
* #460420 ^property[1].code = #hints 
* #460420 ^property[1].valueString = "PART: 7 ~ CODE10: 1668" 
* #460492 "MDC_UPEXT_AXILLA"
* #460492 ^property[0].code = #parent 
* #460492 ^property[0].valueCode = #_mdcenums 
* #460492 ^property[1].code = #hints 
* #460492 ^property[1].valueString = "PART: 7 ~ CODE10: 1740" 
* #460500 "MDC_UPEXT_FINGER"
* #460500 ^property[0].code = #parent 
* #460500 ^property[0].valueCode = #_mdcenums 
* #460500 ^property[1].code = #hints 
* #460500 ^property[1].valueString = "PART: 7 ~ CODE10: 1748" 
* #460501 "MDC_UPEXT_FINGER_L"
* #460501 ^property[0].code = #parent 
* #460501 ^property[0].valueCode = #_mdcenums 
* #460501 ^property[1].code = #hints 
* #460501 ^property[1].valueString = "PART: 7 ~ CODE10: 1749" 
* #460502 "MDC_UPEXT_FINGER_R"
* #460502 ^property[0].code = #parent 
* #460502 ^property[0].valueCode = #_mdcenums 
* #460502 ^property[1].code = #hints 
* #460502 ^property[1].valueString = "PART: 7 ~ CODE10: 1750" 
* #460521 "MDC_UPEXT_FOREARM_L"
* #460521 ^property[0].code = #parent 
* #460521 ^property[0].valueCode = #_mdcenums 
* #460521 ^property[1].code = #hints 
* #460521 ^property[1].valueString = "PART: 7 ~ CODE10: 1769" 
* #460522 "MDC_UPEXT_FOREARM_R"
* #460522 ^property[0].code = #parent 
* #460522 ^property[0].valueCode = #_mdcenums 
* #460522 ^property[1].code = #hints 
* #460522 ^property[1].valueString = "PART: 7 ~ CODE10: 1770" 
* #460524 "MDC_UPEXT_HAND"
* #460524 ^property[0].code = #parent 
* #460524 ^property[0].valueCode = #_mdcenums 
* #460524 ^property[1].code = #hints 
* #460524 ^property[1].valueString = "PART: 7 ~ CODE10: 1772" 
* #460525 "MDC_UPEXT_HAND_L"
* #460525 ^property[0].code = #parent 
* #460525 ^property[0].valueCode = #_mdcenums 
* #460525 ^property[1].code = #hints 
* #460525 ^property[1].valueString = "PART: 7 ~ CODE10: 1773" 
* #460526 "MDC_UPEXT_HAND_R"
* #460526 ^property[0].code = #parent 
* #460526 ^property[0].valueCode = #_mdcenums 
* #460526 ^property[1].code = #hints 
* #460526 ^property[1].valueString = "PART: 7 ~ CODE10: 1774" 
* #460533 "MDC_UPEXT_ARM_UPPER_L"
* #460533 ^property[0].code = #parent 
* #460533 ^property[0].valueCode = #_mdcenums 
* #460533 ^property[1].code = #hints 
* #460533 ^property[1].valueString = "PART: 7 ~ CODE10: 1781" 
* #460534 "MDC_UPEXT_ARM_UPPER_R"
* #460534 ^property[0].code = #parent 
* #460534 ^property[0].valueCode = #_mdcenums 
* #460534 ^property[1].code = #hints 
* #460534 ^property[1].valueString = "PART: 7 ~ CODE10: 1782" 
* #460536 "MDC_UPEXT_WRIST"
* #460536 ^property[0].code = #parent 
* #460536 ^property[0].valueCode = #_mdcenums 
* #460536 ^property[1].code = #hints 
* #460536 ^property[1].valueString = "PART: 7 ~ CODE10: 1784" 
* #460537 "MDC_UPEXT_WRIST_L"
* #460537 ^property[0].code = #parent 
* #460537 ^property[0].valueCode = #_mdcenums 
* #460537 ^property[1].code = #hints 
* #460537 ^property[1].valueString = "PART: 7 ~ CODE10: 1785" 
* #460538 "MDC_UPEXT_WRIST_R"
* #460538 ^property[0].code = #parent 
* #460538 ^property[0].valueCode = #_mdcenums 
* #460538 ^property[1].code = #hints 
* #460538 ^property[1].valueString = "PART: 7 ~ CODE10: 1786" 
* #460597 "MDC_HEAD_REGIONAL_OCCIPITAL_L"
* #460597 ^property[0].code = #parent 
* #460597 ^property[0].valueCode = #_mdcenums 
* #460597 ^property[1].code = #hints 
* #460597 ^property[1].valueString = "PART: 7 ~ CODE10: 1845" 
* #460598 "MDC_HEAD_REGIONAL_OCCIPITAL_R"
* #460598 ^property[0].code = #parent 
* #460598 ^property[0].valueCode = #_mdcenums 
* #460598 ^property[1].code = #hints 
* #460598 ^property[1].valueString = "PART: 7 ~ CODE10: 1846" 
* #460601 "MDC_HEAD_REGIONAL_PARIETAL_L"
* #460601 ^property[0].code = #parent 
* #460601 ^property[0].valueCode = #_mdcenums 
* #460601 ^property[1].code = #hints 
* #460601 ^property[1].valueString = "PART: 7 ~ CODE10: 1849" 
* #460602 "MDC_HEAD_REGIONAL_PARIETAL_R"
* #460602 ^property[0].code = #parent 
* #460602 ^property[0].valueCode = #_mdcenums 
* #460602 ^property[1].code = #hints 
* #460602 ^property[1].valueString = "PART: 7 ~ CODE10: 1850" 
* #460605 "MDC_HEAD_REGIONAL_TEMPORAL_L"
* #460605 ^property[0].code = #parent 
* #460605 ^property[0].valueCode = #_mdcenums 
* #460605 ^property[1].code = #hints 
* #460605 ^property[1].valueString = "PART: 7 ~ CODE10: 1853" 
* #460606 "MDC_HEAD_REGIONAL_TEMPORAL_R"
* #460606 ^property[0].code = #parent 
* #460606 ^property[0].valueCode = #_mdcenums 
* #460606 ^property[1].code = #hints 
* #460606 ^property[1].valueString = "PART: 7 ~ CODE10: 1854" 
* #460609 "MDC_HEAD_REGIONAL_FRONTAL_L"
* #460609 ^property[0].code = #parent 
* #460609 ^property[0].valueCode = #_mdcenums 
* #460609 ^property[1].code = #hints 
* #460609 ^property[1].valueString = "PART: 7 ~ CODE10: 1857" 
* #460610 "MDC_HEAD_REGIONAL_FRONTAL_R"
* #460610 ^property[0].code = #parent 
* #460610 ^property[0].valueCode = #_mdcenums 
* #460610 ^property[1].code = #hints 
* #460610 ^property[1].valueString = "PART: 7 ~ CODE10: 1858" 
* #460613 "MDC_HEAD_REGIONAL_FRONTAL_POLAR_L"
* #460613 ^property[0].code = #parent 
* #460613 ^property[0].valueCode = #_mdcenums 
* #460613 ^property[1].code = #hints 
* #460613 ^property[1].valueString = "PART: 7 ~ CODE10: 1861" 
* #460614 "MDC_HEAD_REGIONAL_FRONTAL_POLAR_R"
* #460614 ^property[0].code = #parent 
* #460614 ^property[0].valueCode = #_mdcenums 
* #460614 ^property[1].code = #hints 
* #460614 ^property[1].valueString = "PART: 7 ~ CODE10: 1862" 
* #460616 "MDC_NERV_SPIN_CERVIC_5"
* #460616 ^property[0].code = #parent 
* #460616 ^property[0].valueCode = #_mdcenums 
* #460616 ^property[1].code = #hints 
* #460616 ^property[1].valueString = "PART: 7 ~ CODE10: 1864" 
* #460633 "MDC_HEAD_CUSTOM_1"
* #460633 ^property[0].code = #parent 
* #460633 ^property[0].valueCode = #_mdcenums 
* #460633 ^property[1].code = #hints 
* #460633 ^property[1].valueString = "PART: 7 ~ CODE10: 1881" 
* #460634 "MDC_HEAD_CUSTOM_2"
* #460634 ^property[0].code = #parent 
* #460634 ^property[0].valueCode = #_mdcenums 
* #460634 ^property[1].code = #hints 
* #460634 ^property[1].valueString = "PART: 7 ~ CODE10: 1882" 
* #460635 "MDC_HEAD_CUSTOM_3"
* #460635 ^property[0].code = #parent 
* #460635 ^property[0].valueCode = #_mdcenums 
* #460635 ^property[1].code = #hints 
* #460635 ^property[1].valueString = "PART: 7 ~ CODE10: 1883" 
* #460636 "MDC_HEAD_CUSTOM_4"
* #460636 ^property[0].code = #parent 
* #460636 ^property[0].valueCode = #_mdcenums 
* #460636 ^property[1].code = #hints 
* #460636 ^property[1].valueString = "PART: 7 ~ CODE10: 1884" 
* #460637 "MDC_HEAD_CUSTOM_5"
* #460637 ^property[0].code = #parent 
* #460637 ^property[0].valueCode = #_mdcenums 
* #460637 ^property[1].code = #hints 
* #460637 ^property[1].valueString = "PART: 7 ~ CODE10: 1885" 
* #460638 "MDC_HEAD_CUSTOM_6"
* #460638 ^property[0].code = #parent 
* #460638 ^property[0].valueCode = #_mdcenums 
* #460638 ^property[1].code = #hints 
* #460638 ^property[1].valueString = "PART: 7 ~ CODE10: 1886" 
* #460800 "MDC_GAS_MSMT_SITE_NOS"
* #460800 ^property[0].code = #parent 
* #460800 ^property[0].valueCode = #_mdcenums 
* #460800 ^property[1].code = #hints 
* #460800 ^property[1].valueString = "PART: 7 ~ CODE10: 2048" 
* #460801 "MDC_GAS_MSMT_SITE_AWAY"
* #460801 ^property[0].code = #parent 
* #460801 ^property[0].valueCode = #_mdcenums 
* #460801 ^property[1].code = #hints 
* #460801 ^property[1].valueString = "PART: 7 ~ CODE10: 2049" 
* #460802 "MDC_GAS_MSMT_SITE_YPI"
* #460802 ^property[0].code = #parent 
* #460802 ^property[0].valueCode = #_mdcenums 
* #460802 ^property[1].code = #hints 
* #460802 ^property[1].valueString = "PART: 7 ~ CODE10: 2050" 
* #460803 "MDC_GAS_MSMT_SITE_ETT"
* #460803 ^property[0].code = #parent 
* #460803 ^property[0].valueCode = #_mdcenums 
* #460803 ^property[1].code = #hints 
* #460803 ^property[1].valueString = "PART: 7 ~ CODE10: 2051" 
* #460804 "MDC_GAS_MSMT_SITE_FGF"
* #460804 ^property[0].code = #parent 
* #460804 ^property[0].valueCode = #_mdcenums 
* #460804 ^property[1].code = #hints 
* #460804 ^property[1].valueString = "PART: 7 ~ CODE10: 2052" 
* #460805 "MDC_GAS_MSMT_SITE_EXH"
* #460805 ^property[0].code = #parent 
* #460805 ^property[0].valueCode = #_mdcenums 
* #460805 ^property[1].code = #hints 
* #460805 ^property[1].valueString = "PART: 7 ~ CODE10: 2053" 
* #460806 "MDC_GAS_MSMT_SITE_IL"
* #460806 ^property[0].code = #parent 
* #460806 ^property[0].valueCode = #_mdcenums 
* #460806 ^property[1].code = #hints 
* #460806 ^property[1].valueString = "PART: 7 ~ CODE10: 2054" 
* #460807 "MDC_GAS_MSMT_SITE_EL"
* #460807 ^property[0].code = #parent 
* #460807 ^property[0].valueCode = #_mdcenums 
* #460807 ^property[1].code = #hints 
* #460807 ^property[1].valueString = "PART: 7 ~ CODE10: 2055" 
* #460809 "MDC_GAS_MSMT_SITE_LB"
* #460809 ^property[0].code = #parent 
* #460809 ^property[0].valueCode = #_mdcenums 
* #460809 ^property[1].code = #hints 
* #460809 ^property[1].valueString = "PART: 7 ~ CODE10: 2057" 
* #460810 "MDC_GAS_MSMT_SITE_RB"
* #460810 ^property[0].code = #parent 
* #460810 ^property[0].valueCode = #_mdcenums 
* #460810 ^property[1].code = #hints 
* #460810 ^property[1].valueString = "PART: 7 ~ CODE10: 2058" 
* #460812 "MDC_GAS_MSMT_SITE_PI"
* #460812 ^property[0].code = #parent 
* #460812 ^property[0].valueCode = #_mdcenums 
* #460812 ^property[1].code = #hints 
* #460812 ^property[1].valueString = "PART: 7 ~ CODE10: 2060" 
* #460813 "MDC_GAS_MSMT_SITE_ETTC"
* #460813 ^property[0].code = #parent 
* #460813 ^property[0].valueCode = #_mdcenums 
* #460813 ^property[1].code = #hints 
* #460813 ^property[1].valueString = "PART: 7 ~ CODE10: 2061" 
* #460824 "MDC_GAS_CIRC_SITE_ABSe"
* #460824 ^property[0].code = #parent 
* #460824 ^property[0].valueCode = #_mdcenums 
* #460824 ^property[1].code = #hints 
* #460824 ^property[1].valueString = "PART: 7 ~ CODE10: 2072" 
* #460825 "MDC_GAS_CIRC_SITE_ABSi"
* #460825 ^property[0].code = #parent 
* #460825 ^property[0].valueCode = #_mdcenums 
* #460825 ^property[1].code = #hints 
* #460825 ^property[1].valueString = "PART: 7 ~ CODE10: 2073" 
* #466946 "MDC_BS_QUAL_LEFT"
* #466946 ^property[0].code = #parent 
* #466946 ^property[0].valueCode = #_mdcenums 
* #466946 ^property[1].code = #hints 
* #466946 ^property[1].valueString = "PART: 7 ~ CODE10: 8194" 
* #466948 "MDC_BS_QUAL_RIGHT"
* #466948 ^property[0].code = #parent 
* #466948 ^property[0].valueCode = #_mdcenums 
* #466948 ^property[1].code = #hints 
* #466948 ^property[1].valueString = "PART: 7 ~ CODE10: 8196" 
* #8417824 "MDC_CTXT_GLU_HEALTH_MINOR"
* #8417824 ^property[0].code = #parent 
* #8417824 ^property[0].valueCode = #_mdcenums 
* #8417824 ^property[1].code = #hints 
* #8417824 ^property[1].valueString = "PART: 128 ~ CODE10: 29216" 
* #8417828 "MDC_CTXT_GLU_HEALTH_MAJOR"
* #8417828 ^property[0].code = #parent 
* #8417828 ^property[0].valueCode = #_mdcenums 
* #8417828 ^property[1].code = #hints 
* #8417828 ^property[1].valueString = "PART: 128 ~ CODE10: 29220" 
* #8417832 "MDC_CTXT_GLU_HEALTH_MENSES"
* #8417832 ^property[0].code = #parent 
* #8417832 ^property[0].valueCode = #_mdcenums 
* #8417832 ^property[1].code = #hints 
* #8417832 ^property[1].valueString = "PART: 128 ~ CODE10: 29224" 
* #8417836 "MDC_CTXT_GLU_HEALTH_STRESS"
* #8417836 ^property[0].code = #parent 
* #8417836 ^property[0].valueCode = #_mdcenums 
* #8417836 ^property[1].code = #hints 
* #8417836 ^property[1].valueString = "PART: 128 ~ CODE10: 29228" 
* #8417840 "MDC_CTXT_GLU_HEALTH_NONE"
* #8417840 ^property[0].code = #parent 
* #8417840 ^property[0].valueCode = #_mdcenums 
* #8417840 ^property[1].code = #hints 
* #8417840 ^property[1].valueString = "PART: 128 ~ CODE10: 29232" 
* #8417845 "MDC_CTXT_GLU_SAMPLELOCATION_UNDETERMINED"
* #8417845 ^property[0].code = #parent 
* #8417845 ^property[0].valueCode = #_mdcenums 
* #8417845 ^property[1].code = #hints 
* #8417845 ^property[1].valueString = "PART: 128 ~ CODE10: 29237" 
* #8417846 "MDC_CTXT_GLU_SAMPLELOCATION_OTHER"
* #8417846 ^property[0].code = #parent 
* #8417846 ^property[0].valueCode = #_mdcenums 
* #8417846 ^property[1].code = #hints 
* #8417846 ^property[1].valueString = "PART: 128 ~ CODE10: 29238" 
* #8417848 "MDC_CTXT_GLU_SAMPLELOCATION_FINGER"
* #8417848 ^property[0].code = #parent 
* #8417848 ^property[0].valueCode = #_mdcenums 
* #8417848 ^property[1].code = #hints 
* #8417848 ^property[1].valueString = "PART: 128 ~ CODE10: 29240" 
* #8417852 "MDC_CTXT_GLU_SAMPLELOCATION_AST"
* #8417852 ^property[0].code = #parent 
* #8417852 ^property[0].valueCode = #_mdcenums 
* #8417852 ^property[1].code = #hints 
* #8417852 ^property[1].valueString = "PART: 128 ~ CODE10: 29244" 
* #8417856 "MDC_CTXT_GLU_SAMPLELOCATION_EARLOBE"
* #8417856 ^property[0].code = #parent 
* #8417856 ^property[0].valueCode = #_mdcenums 
* #8417856 ^property[1].code = #hints 
* #8417856 ^property[1].valueString = "PART: 128 ~ CODE10: 29248" 
* #8417860 "MDC_CTXT_GLU_SAMPLELOCATION_CTRLSOLUTION"
* #8417860 ^property[0].code = #parent 
* #8417860 ^property[0].valueCode = #_mdcenums 
* #8417860 ^property[1].code = #hints 
* #8417860 ^property[1].valueString = "PART: 128 ~ CODE10: 29252" 
* #8417868 "MDC_CTXT_GLU_MEAL_PREPRANDIAL"
* #8417868 ^property[0].code = #parent 
* #8417868 ^property[0].valueCode = #_mdcenums 
* #8417868 ^property[1].code = #hints 
* #8417868 ^property[1].valueString = "PART: 128 ~ CODE10: 29260" 
* #8417869 "MDC_CTXT_GLU_MEAL_BEDTIME"
* #8417869 ^property[0].code = #parent 
* #8417869 ^property[0].valueCode = #_mdcenums 
* #8417869 ^property[1].code = #hints 
* #8417869 ^property[1].valueString = "PART: 128 ~ CODE10: 29261" 
* #8417872 "MDC_CTXT_GLU_MEAL_POSTPRANDIAL"
* #8417872 ^property[0].code = #parent 
* #8417872 ^property[0].valueCode = #_mdcenums 
* #8417872 ^property[1].code = #hints 
* #8417872 ^property[1].valueString = "PART: 128 ~ CODE10: 29264" 
* #8417876 "MDC_CTXT_GLU_MEAL_FASTING"
* #8417876 ^property[0].code = #parent 
* #8417876 ^property[0].valueCode = #_mdcenums 
* #8417876 ^property[1].code = #hints 
* #8417876 ^property[1].valueString = "PART: 128 ~ CODE10: 29268" 
* #8417880 "MDC_CTXT_GLU_MEAL_CASUAL"
* #8417880 ^property[0].code = #parent 
* #8417880 ^property[0].valueCode = #_mdcenums 
* #8417880 ^property[1].code = #hints 
* #8417880 ^property[1].valueString = "PART: 128 ~ CODE10: 29272" 
* #8417888 "MDC_CTXT_GLU_TESTER_SELF"
* #8417888 ^property[0].code = #parent 
* #8417888 ^property[0].valueCode = #_mdcenums 
* #8417888 ^property[1].code = #hints 
* #8417888 ^property[1].valueString = "PART: 128 ~ CODE10: 29280" 
* #8417892 "MDC_CTXT_GLU_TESTER_HCP"
* #8417892 ^property[0].code = #parent 
* #8417892 ^property[0].valueCode = #_mdcenums 
* #8417892 ^property[1].code = #hints 
* #8417892 ^property[1].valueString = "PART: 128 ~ CODE10: 29284" 
* #8417896 "MDC_CTXT_GLU_TESTER_LAB"
* #8417896 ^property[0].code = #parent 
* #8417896 ^property[0].valueCode = #_mdcenums 
* #8417896 ^property[1].code = #hints 
* #8417896 ^property[1].valueString = "PART: 128 ~ CODE10: 29288" 
* #8417925 "MDC_CTXT_INR_TESTER_SELF"
* #8417925 ^property[0].code = #parent 
* #8417925 ^property[0].valueCode = #_mdcenums 
* #8417925 ^property[1].code = #hints 
* #8417925 ^property[1].valueString = "PART: 128 ~ CODE10: 29317" 
* #8417926 "MDC_CTXT_INR_TESTER_HCP"
* #8417926 ^property[0].code = #parent 
* #8417926 ^property[0].valueCode = #_mdcenums 
* #8417926 ^property[1].code = #hints 
* #8417926 ^property[1].valueString = "PART: 128 ~ CODE10: 29318" 
* #8417927 "MDC_CTXT_INR_TESTER_LAB"
* #8417927 ^property[0].code = #parent 
* #8417927 ^property[0].valueCode = #_mdcenums 
* #8417927 ^property[1].code = #hints 
* #8417927 ^property[1].valueString = "PART: 128 ~ CODE10: 29319" 
* #_mdcunits "MDC units"
* #_mdcunits ^property[0].code = #child 
* #_mdcunits ^property[0].valueCode = #262144 
* #_mdcunits ^property[1].code = #child 
* #_mdcunits ^property[1].valueCode = #262656 
* #_mdcunits ^property[2].code = #child 
* #_mdcunits ^property[2].valueCode = #262688 
* #_mdcunits ^property[3].code = #child 
* #_mdcunits ^property[3].valueCode = #262720 
* #_mdcunits ^property[4].code = #child 
* #_mdcunits ^property[4].valueCode = #262752 
* #_mdcunits ^property[5].code = #child 
* #_mdcunits ^property[5].valueCode = #262784 
* #_mdcunits ^property[6].code = #child 
* #_mdcunits ^property[6].valueCode = #262816 
* #_mdcunits ^property[7].code = #child 
* #_mdcunits ^property[7].valueCode = #262880 
* #_mdcunits ^property[8].code = #child 
* #_mdcunits ^property[8].valueCode = #262912 
* #_mdcunits ^property[9].code = #child 
* #_mdcunits ^property[9].valueCode = #262944 
* #_mdcunits ^property[10].code = #child 
* #_mdcunits ^property[10].valueCode = #262976 
* #_mdcunits ^property[11].code = #child 
* #_mdcunits ^property[11].valueCode = #262994 
* #_mdcunits ^property[12].code = #child 
* #_mdcunits ^property[12].valueCode = #262995 
* #_mdcunits ^property[13].code = #child 
* #_mdcunits ^property[13].valueCode = #262996 
* #_mdcunits ^property[14].code = #child 
* #_mdcunits ^property[14].valueCode = #263008 
* #_mdcunits ^property[15].code = #child 
* #_mdcunits ^property[15].valueCode = #263026 
* #_mdcunits ^property[16].code = #child 
* #_mdcunits ^property[16].valueCode = #263040 
* #_mdcunits ^property[17].code = #child 
* #_mdcunits ^property[17].valueCode = #263072 
* #_mdcunits ^property[18].code = #child 
* #_mdcunits ^property[18].valueCode = #263104 
* #_mdcunits ^property[19].code = #child 
* #_mdcunits ^property[19].valueCode = #263136 
* #_mdcunits ^property[20].code = #child 
* #_mdcunits ^property[20].valueCode = #263168 
* #_mdcunits ^property[21].code = #child 
* #_mdcunits ^property[21].valueCode = #263200 
* #_mdcunits ^property[22].code = #child 
* #_mdcunits ^property[22].valueCode = #263232 
* #_mdcunits ^property[23].code = #child 
* #_mdcunits ^property[23].valueCode = #263264 
* #_mdcunits ^property[24].code = #child 
* #_mdcunits ^property[24].valueCode = #263296 
* #_mdcunits ^property[25].code = #child 
* #_mdcunits ^property[25].valueCode = #263328 
* #_mdcunits ^property[26].code = #child 
* #_mdcunits ^property[26].valueCode = #263360 
* #_mdcunits ^property[27].code = #child 
* #_mdcunits ^property[27].valueCode = #263392 
* #_mdcunits ^property[28].code = #child 
* #_mdcunits ^property[28].valueCode = #263424 
* #_mdcunits ^property[29].code = #child 
* #_mdcunits ^property[29].valueCode = #263441 
* #_mdcunits ^property[30].code = #child 
* #_mdcunits ^property[30].valueCode = #263442 
* #_mdcunits ^property[31].code = #child 
* #_mdcunits ^property[31].valueCode = #263443 
* #_mdcunits ^property[32].code = #child 
* #_mdcunits ^property[32].valueCode = #263520 
* #_mdcunits ^property[33].code = #child 
* #_mdcunits ^property[33].valueCode = #263552 
* #_mdcunits ^property[34].code = #child 
* #_mdcunits ^property[34].valueCode = #263570 
* #_mdcunits ^property[35].code = #child 
* #_mdcunits ^property[35].valueCode = #263584 
* #_mdcunits ^property[36].code = #child 
* #_mdcunits ^property[36].valueCode = #263616 
* #_mdcunits ^property[37].code = #child 
* #_mdcunits ^property[37].valueCode = #263680 
* #_mdcunits ^property[38].code = #child 
* #_mdcunits ^property[38].valueCode = #263712 
* #_mdcunits ^property[39].code = #child 
* #_mdcunits ^property[39].valueCode = #263744 
* #_mdcunits ^property[40].code = #child 
* #_mdcunits ^property[40].valueCode = #263762 
* #_mdcunits ^property[41].code = #child 
* #_mdcunits ^property[41].valueCode = #263763 
* #_mdcunits ^property[42].code = #child 
* #_mdcunits ^property[42].valueCode = #263776 
* #_mdcunits ^property[43].code = #child 
* #_mdcunits ^property[43].valueCode = #263794 
* #_mdcunits ^property[44].code = #child 
* #_mdcunits ^property[44].valueCode = #263808 
* #_mdcunits ^property[45].code = #child 
* #_mdcunits ^property[45].valueCode = #263840 
* #_mdcunits ^property[46].code = #child 
* #_mdcunits ^property[46].valueCode = #263872 
* #_mdcunits ^property[47].code = #child 
* #_mdcunits ^property[47].valueCode = #263875 
* #_mdcunits ^property[48].code = #child 
* #_mdcunits ^property[48].valueCode = #263890 
* #_mdcunits ^property[49].code = #child 
* #_mdcunits ^property[49].valueCode = #263891 
* #_mdcunits ^property[50].code = #child 
* #_mdcunits ^property[50].valueCode = #263892 
* #_mdcunits ^property[51].code = #child 
* #_mdcunits ^property[51].valueCode = #263904 
* #_mdcunits ^property[52].code = #child 
* #_mdcunits ^property[52].valueCode = #263936 
* #_mdcunits ^property[53].code = #child 
* #_mdcunits ^property[53].valueCode = #263968 
* #_mdcunits ^property[54].code = #child 
* #_mdcunits ^property[54].valueCode = #264000 
* #_mdcunits ^property[55].code = #child 
* #_mdcunits ^property[55].valueCode = #264032 
* #_mdcunits ^property[56].code = #child 
* #_mdcunits ^property[56].valueCode = #264064 
* #_mdcunits ^property[57].code = #child 
* #_mdcunits ^property[57].valueCode = #264128 
* #_mdcunits ^property[58].code = #child 
* #_mdcunits ^property[58].valueCode = #264160 
* #_mdcunits ^property[59].code = #child 
* #_mdcunits ^property[59].valueCode = #264192 
* #_mdcunits ^property[60].code = #child 
* #_mdcunits ^property[60].valueCode = #264224 
* #_mdcunits ^property[61].code = #child 
* #_mdcunits ^property[61].valueCode = #264256 
* #_mdcunits ^property[62].code = #child 
* #_mdcunits ^property[62].valueCode = #264274 
* #_mdcunits ^property[63].code = #child 
* #_mdcunits ^property[63].valueCode = #264288 
* #_mdcunits ^property[64].code = #child 
* #_mdcunits ^property[64].valueCode = #264306 
* #_mdcunits ^property[65].code = #child 
* #_mdcunits ^property[65].valueCode = #264307 
* #_mdcunits ^property[66].code = #child 
* #_mdcunits ^property[66].valueCode = #264308 
* #_mdcunits ^property[67].code = #child 
* #_mdcunits ^property[67].valueCode = #264320 
* #_mdcunits ^property[68].code = #child 
* #_mdcunits ^property[68].valueCode = #264338 
* #_mdcunits ^property[69].code = #child 
* #_mdcunits ^property[69].valueCode = #264339 
* #_mdcunits ^property[70].code = #child 
* #_mdcunits ^property[70].valueCode = #264340 
* #_mdcunits ^property[71].code = #child 
* #_mdcunits ^property[71].valueCode = #264352 
* #_mdcunits ^property[72].code = #child 
* #_mdcunits ^property[72].valueCode = #264384 
* #_mdcunits ^property[73].code = #child 
* #_mdcunits ^property[73].valueCode = #264416 
* #_mdcunits ^property[74].code = #child 
* #_mdcunits ^property[74].valueCode = #264448 
* #_mdcunits ^property[75].code = #child 
* #_mdcunits ^property[75].valueCode = #264480 
* #_mdcunits ^property[76].code = #child 
* #_mdcunits ^property[76].valueCode = #264512 
* #_mdcunits ^property[77].code = #child 
* #_mdcunits ^property[77].valueCode = #264544 
* #_mdcunits ^property[78].code = #child 
* #_mdcunits ^property[78].valueCode = #264576 
* #_mdcunits ^property[79].code = #child 
* #_mdcunits ^property[79].valueCode = #264608 
* #_mdcunits ^property[80].code = #child 
* #_mdcunits ^property[80].valueCode = #264672 
* #_mdcunits ^property[81].code = #child 
* #_mdcunits ^property[81].valueCode = #264704 
* #_mdcunits ^property[82].code = #child 
* #_mdcunits ^property[82].valueCode = #264736 
* #_mdcunits ^property[83].code = #child 
* #_mdcunits ^property[83].valueCode = #264768 
* #_mdcunits ^property[84].code = #child 
* #_mdcunits ^property[84].valueCode = #264800 
* #_mdcunits ^property[85].code = #child 
* #_mdcunits ^property[85].valueCode = #264832 
* #_mdcunits ^property[86].code = #child 
* #_mdcunits ^property[86].valueCode = #264864 
* #_mdcunits ^property[87].code = #child 
* #_mdcunits ^property[87].valueCode = #264896 
* #_mdcunits ^property[88].code = #child 
* #_mdcunits ^property[88].valueCode = #264928 
* #_mdcunits ^property[89].code = #child 
* #_mdcunits ^property[89].valueCode = #264960 
* #_mdcunits ^property[90].code = #child 
* #_mdcunits ^property[90].valueCode = #264963 
* #_mdcunits ^property[91].code = #child 
* #_mdcunits ^property[91].valueCode = #264978 
* #_mdcunits ^property[92].code = #child 
* #_mdcunits ^property[92].valueCode = #264992 
* #_mdcunits ^property[93].code = #child 
* #_mdcunits ^property[93].valueCode = #265010 
* #_mdcunits ^property[94].code = #child 
* #_mdcunits ^property[94].valueCode = #265024 
* #_mdcunits ^property[95].code = #child 
* #_mdcunits ^property[95].valueCode = #265056 
* #_mdcunits ^property[96].code = #child 
* #_mdcunits ^property[96].valueCode = #265088 
* #_mdcunits ^property[97].code = #child 
* #_mdcunits ^property[97].valueCode = #265120 
* #_mdcunits ^property[98].code = #child 
* #_mdcunits ^property[98].valueCode = #265152 
* #_mdcunits ^property[99].code = #child 
* #_mdcunits ^property[99].valueCode = #265184 
* #_mdcunits ^property[100].code = #child 
* #_mdcunits ^property[100].valueCode = #265202 
* #_mdcunits ^property[101].code = #child 
* #_mdcunits ^property[101].valueCode = #265216 
* #_mdcunits ^property[102].code = #child 
* #_mdcunits ^property[102].valueCode = #265234 
* #_mdcunits ^property[103].code = #child 
* #_mdcunits ^property[103].valueCode = #265235 
* #_mdcunits ^property[104].code = #child 
* #_mdcunits ^property[104].valueCode = #265248 
* #_mdcunits ^property[105].code = #child 
* #_mdcunits ^property[105].valueCode = #265266 
* #_mdcunits ^property[106].code = #child 
* #_mdcunits ^property[106].valueCode = #265280 
* #_mdcunits ^property[107].code = #child 
* #_mdcunits ^property[107].valueCode = #265312 
* #_mdcunits ^property[108].code = #child 
* #_mdcunits ^property[108].valueCode = #265330 
* #_mdcunits ^property[109].code = #child 
* #_mdcunits ^property[109].valueCode = #265344 
* #_mdcunits ^property[110].code = #child 
* #_mdcunits ^property[110].valueCode = #265376 
* #_mdcunits ^property[111].code = #child 
* #_mdcunits ^property[111].valueCode = #265408 
* #_mdcunits ^property[112].code = #child 
* #_mdcunits ^property[112].valueCode = #265440 
* #_mdcunits ^property[113].code = #child 
* #_mdcunits ^property[113].valueCode = #265472 
* #_mdcunits ^property[114].code = #child 
* #_mdcunits ^property[114].valueCode = #265490 
* #_mdcunits ^property[115].code = #child 
* #_mdcunits ^property[115].valueCode = #265491 
* #_mdcunits ^property[116].code = #child 
* #_mdcunits ^property[116].valueCode = #265492 
* #_mdcunits ^property[117].code = #child 
* #_mdcunits ^property[117].valueCode = #265504 
* #_mdcunits ^property[118].code = #child 
* #_mdcunits ^property[118].valueCode = #265522 
* #_mdcunits ^property[119].code = #child 
* #_mdcunits ^property[119].valueCode = #265523 
* #_mdcunits ^property[120].code = #child 
* #_mdcunits ^property[120].valueCode = #265524 
* #_mdcunits ^property[121].code = #child 
* #_mdcunits ^property[121].valueCode = #265536 
* #_mdcunits ^property[122].code = #child 
* #_mdcunits ^property[122].valueCode = #265554 
* #_mdcunits ^property[123].code = #child 
* #_mdcunits ^property[123].valueCode = #265555 
* #_mdcunits ^property[124].code = #child 
* #_mdcunits ^property[124].valueCode = #265556 
* #_mdcunits ^property[125].code = #child 
* #_mdcunits ^property[125].valueCode = #265568 
* #_mdcunits ^property[126].code = #child 
* #_mdcunits ^property[126].valueCode = #265600 
* #_mdcunits ^property[127].code = #child 
* #_mdcunits ^property[127].valueCode = #265618 
* #_mdcunits ^property[128].code = #child 
* #_mdcunits ^property[128].valueCode = #265619 
* #_mdcunits ^property[129].code = #child 
* #_mdcunits ^property[129].valueCode = #265620 
* #_mdcunits ^property[130].code = #child 
* #_mdcunits ^property[130].valueCode = #265632 
* #_mdcunits ^property[131].code = #child 
* #_mdcunits ^property[131].valueCode = #265650 
* #_mdcunits ^property[132].code = #child 
* #_mdcunits ^property[132].valueCode = #265651 
* #_mdcunits ^property[133].code = #child 
* #_mdcunits ^property[133].valueCode = #265652 
* #_mdcunits ^property[134].code = #child 
* #_mdcunits ^property[134].valueCode = #265664 
* #_mdcunits ^property[135].code = #child 
* #_mdcunits ^property[135].valueCode = #265682 
* #_mdcunits ^property[136].code = #child 
* #_mdcunits ^property[136].valueCode = #265683 
* #_mdcunits ^property[137].code = #child 
* #_mdcunits ^property[137].valueCode = #265684 
* #_mdcunits ^property[138].code = #child 
* #_mdcunits ^property[138].valueCode = #265696 
* #_mdcunits ^property[139].code = #child 
* #_mdcunits ^property[139].valueCode = #265728 
* #_mdcunits ^property[140].code = #child 
* #_mdcunits ^property[140].valueCode = #265760 
* #_mdcunits ^property[141].code = #child 
* #_mdcunits ^property[141].valueCode = #265792 
* #_mdcunits ^property[142].code = #child 
* #_mdcunits ^property[142].valueCode = #265824 
* #_mdcunits ^property[143].code = #child 
* #_mdcunits ^property[143].valueCode = #265856 
* #_mdcunits ^property[144].code = #child 
* #_mdcunits ^property[144].valueCode = #265888 
* #_mdcunits ^property[145].code = #child 
* #_mdcunits ^property[145].valueCode = #265920 
* #_mdcunits ^property[146].code = #child 
* #_mdcunits ^property[146].valueCode = #265952 
* #_mdcunits ^property[147].code = #child 
* #_mdcunits ^property[147].valueCode = #265984 
* #_mdcunits ^property[148].code = #child 
* #_mdcunits ^property[148].valueCode = #265986 
* #_mdcunits ^property[149].code = #child 
* #_mdcunits ^property[149].valueCode = #265987 
* #_mdcunits ^property[150].code = #child 
* #_mdcunits ^property[150].valueCode = #266016 
* #_mdcunits ^property[151].code = #child 
* #_mdcunits ^property[151].valueCode = #266048 
* #_mdcunits ^property[152].code = #child 
* #_mdcunits ^property[152].valueCode = #266080 
* #_mdcunits ^property[153].code = #child 
* #_mdcunits ^property[153].valueCode = #266098 
* #_mdcunits ^property[154].code = #child 
* #_mdcunits ^property[154].valueCode = #266112 
* #_mdcunits ^property[155].code = #child 
* #_mdcunits ^property[155].valueCode = #266144 
* #_mdcunits ^property[156].code = #child 
* #_mdcunits ^property[156].valueCode = #266176 
* #_mdcunits ^property[157].code = #child 
* #_mdcunits ^property[157].valueCode = #266194 
* #_mdcunits ^property[158].code = #child 
* #_mdcunits ^property[158].valueCode = #266195 
* #_mdcunits ^property[159].code = #child 
* #_mdcunits ^property[159].valueCode = #266196 
* #_mdcunits ^property[160].code = #child 
* #_mdcunits ^property[160].valueCode = #266197 
* #_mdcunits ^property[161].code = #child 
* #_mdcunits ^property[161].valueCode = #266208 
* #_mdcunits ^property[162].code = #child 
* #_mdcunits ^property[162].valueCode = #266240 
* #_mdcunits ^property[163].code = #child 
* #_mdcunits ^property[163].valueCode = #266242 
* #_mdcunits ^property[164].code = #child 
* #_mdcunits ^property[164].valueCode = #266304 
* #_mdcunits ^property[165].code = #child 
* #_mdcunits ^property[165].valueCode = #266336 
* #_mdcunits ^property[166].code = #child 
* #_mdcunits ^property[166].valueCode = #266368 
* #_mdcunits ^property[167].code = #child 
* #_mdcunits ^property[167].valueCode = #266400 
* #_mdcunits ^property[168].code = #child 
* #_mdcunits ^property[168].valueCode = #266418 
* #_mdcunits ^property[169].code = #child 
* #_mdcunits ^property[169].valueCode = #266419 
* #_mdcunits ^property[170].code = #child 
* #_mdcunits ^property[170].valueCode = #266420 
* #_mdcunits ^property[171].code = #child 
* #_mdcunits ^property[171].valueCode = #266432 
* #_mdcunits ^property[172].code = #child 
* #_mdcunits ^property[172].valueCode = #266450 
* #_mdcunits ^property[173].code = #child 
* #_mdcunits ^property[173].valueCode = #266464 
* #_mdcunits ^property[174].code = #child 
* #_mdcunits ^property[174].valueCode = #266496 
* #_mdcunits ^property[175].code = #child 
* #_mdcunits ^property[175].valueCode = #266528 
* #_mdcunits ^property[176].code = #child 
* #_mdcunits ^property[176].valueCode = #266560 
* #_mdcunits ^property[177].code = #child 
* #_mdcunits ^property[177].valueCode = #266592 
* #_mdcunits ^property[178].code = #child 
* #_mdcunits ^property[178].valueCode = #266624 
* #_mdcunits ^property[179].code = #child 
* #_mdcunits ^property[179].valueCode = #266656 
* #_mdcunits ^property[180].code = #child 
* #_mdcunits ^property[180].valueCode = #266688 
* #_mdcunits ^property[181].code = #child 
* #_mdcunits ^property[181].valueCode = #266706 
* #_mdcunits ^property[182].code = #child 
* #_mdcunits ^property[182].valueCode = #266720 
* #_mdcunits ^property[183].code = #child 
* #_mdcunits ^property[183].valueCode = #266738 
* #_mdcunits ^property[184].code = #child 
* #_mdcunits ^property[184].valueCode = #266752 
* #_mdcunits ^property[185].code = #child 
* #_mdcunits ^property[185].valueCode = #266784 
* #_mdcunits ^property[186].code = #child 
* #_mdcunits ^property[186].valueCode = #266816 
* #_mdcunits ^property[187].code = #child 
* #_mdcunits ^property[187].valueCode = #266848 
* #_mdcunits ^property[188].code = #child 
* #_mdcunits ^property[188].valueCode = #266866 
* #_mdcunits ^property[189].code = #child 
* #_mdcunits ^property[189].valueCode = #266880 
* #_mdcunits ^property[190].code = #child 
* #_mdcunits ^property[190].valueCode = #266898 
* #_mdcunits ^property[191].code = #child 
* #_mdcunits ^property[191].valueCode = #266912 
* #_mdcunits ^property[192].code = #child 
* #_mdcunits ^property[192].valueCode = #266944 
* #_mdcunits ^property[193].code = #child 
* #_mdcunits ^property[193].valueCode = #266976 
* #_mdcunits ^property[194].code = #child 
* #_mdcunits ^property[194].valueCode = #266994 
* #_mdcunits ^property[195].code = #child 
* #_mdcunits ^property[195].valueCode = #267008 
* #_mdcunits ^property[196].code = #child 
* #_mdcunits ^property[196].valueCode = #267026 
* #_mdcunits ^property[197].code = #child 
* #_mdcunits ^property[197].valueCode = #267040 
* #_mdcunits ^property[198].code = #child 
* #_mdcunits ^property[198].valueCode = #267072 
* #_mdcunits ^property[199].code = #child 
* #_mdcunits ^property[199].valueCode = #267090 
* #_mdcunits ^property[200].code = #child 
* #_mdcunits ^property[200].valueCode = #267104 
* #_mdcunits ^property[201].code = #child 
* #_mdcunits ^property[201].valueCode = #267136 
* #_mdcunits ^property[202].code = #child 
* #_mdcunits ^property[202].valueCode = #267154 
* #_mdcunits ^property[203].code = #child 
* #_mdcunits ^property[203].valueCode = #267168 
* #_mdcunits ^property[204].code = #child 
* #_mdcunits ^property[204].valueCode = #267186 
* #_mdcunits ^property[205].code = #child 
* #_mdcunits ^property[205].valueCode = #267200 
* #_mdcunits ^property[206].code = #child 
* #_mdcunits ^property[206].valueCode = #267218 
* #_mdcunits ^property[207].code = #child 
* #_mdcunits ^property[207].valueCode = #267232 
* #_mdcunits ^property[208].code = #child 
* #_mdcunits ^property[208].valueCode = #267264 
* #_mdcunits ^property[209].code = #child 
* #_mdcunits ^property[209].valueCode = #267282 
* #_mdcunits ^property[210].code = #child 
* #_mdcunits ^property[210].valueCode = #267296 
* #_mdcunits ^property[211].code = #child 
* #_mdcunits ^property[211].valueCode = #267314 
* #_mdcunits ^property[212].code = #child 
* #_mdcunits ^property[212].valueCode = #267328 
* #_mdcunits ^property[213].code = #child 
* #_mdcunits ^property[213].valueCode = #267346 
* #_mdcunits ^property[214].code = #child 
* #_mdcunits ^property[214].valueCode = #267360 
* #_mdcunits ^property[215].code = #child 
* #_mdcunits ^property[215].valueCode = #267392 
* #_mdcunits ^property[216].code = #child 
* #_mdcunits ^property[216].valueCode = #267410 
* #_mdcunits ^property[217].code = #child 
* #_mdcunits ^property[217].valueCode = #267424 
* #_mdcunits ^property[218].code = #child 
* #_mdcunits ^property[218].valueCode = #267442 
* #_mdcunits ^property[219].code = #child 
* #_mdcunits ^property[219].valueCode = #267456 
* #_mdcunits ^property[220].code = #child 
* #_mdcunits ^property[220].valueCode = #267474 
* #_mdcunits ^property[221].code = #child 
* #_mdcunits ^property[221].valueCode = #267488 
* #_mdcunits ^property[222].code = #child 
* #_mdcunits ^property[222].valueCode = #267520 
* #_mdcunits ^property[223].code = #child 
* #_mdcunits ^property[223].valueCode = #267538 
* #_mdcunits ^property[224].code = #child 
* #_mdcunits ^property[224].valueCode = #267552 
* #_mdcunits ^property[225].code = #child 
* #_mdcunits ^property[225].valueCode = #267570 
* #_mdcunits ^property[226].code = #child 
* #_mdcunits ^property[226].valueCode = #267584 
* #_mdcunits ^property[227].code = #child 
* #_mdcunits ^property[227].valueCode = #267602 
* #_mdcunits ^property[228].code = #child 
* #_mdcunits ^property[228].valueCode = #267616 
* #_mdcunits ^property[229].code = #child 
* #_mdcunits ^property[229].valueCode = #267619 
* #_mdcunits ^property[230].code = #child 
* #_mdcunits ^property[230].valueCode = #267620 
* #_mdcunits ^property[231].code = #child 
* #_mdcunits ^property[231].valueCode = #267634 
* #_mdcunits ^property[232].code = #child 
* #_mdcunits ^property[232].valueCode = #267648 
* #_mdcunits ^property[233].code = #child 
* #_mdcunits ^property[233].valueCode = #267680 
* #_mdcunits ^property[234].code = #child 
* #_mdcunits ^property[234].valueCode = #267712 
* #_mdcunits ^property[235].code = #child 
* #_mdcunits ^property[235].valueCode = #267744 
* #_mdcunits ^property[236].code = #child 
* #_mdcunits ^property[236].valueCode = #267747 
* #_mdcunits ^property[237].code = #child 
* #_mdcunits ^property[237].valueCode = #267748 
* #_mdcunits ^property[238].code = #child 
* #_mdcunits ^property[238].valueCode = #267762 
* #_mdcunits ^property[239].code = #child 
* #_mdcunits ^property[239].valueCode = #267776 
* #_mdcunits ^property[240].code = #child 
* #_mdcunits ^property[240].valueCode = #267780 
* #_mdcunits ^property[241].code = #child 
* #_mdcunits ^property[241].valueCode = #267808 
* #_mdcunits ^property[242].code = #child 
* #_mdcunits ^property[242].valueCode = #267812 
* #_mdcunits ^property[243].code = #child 
* #_mdcunits ^property[243].valueCode = #267826 
* #_mdcunits ^property[244].code = #child 
* #_mdcunits ^property[244].valueCode = #267840 
* #_mdcunits ^property[245].code = #child 
* #_mdcunits ^property[245].valueCode = #267843 
* #_mdcunits ^property[246].code = #child 
* #_mdcunits ^property[246].valueCode = #267844 
* #_mdcunits ^property[247].code = #child 
* #_mdcunits ^property[247].valueCode = #267858 
* #_mdcunits ^property[248].code = #child 
* #_mdcunits ^property[248].valueCode = #267872 
* #_mdcunits ^property[249].code = #child 
* #_mdcunits ^property[249].valueCode = #267875 
* #_mdcunits ^property[250].code = #child 
* #_mdcunits ^property[250].valueCode = #267876 
* #_mdcunits ^property[251].code = #child 
* #_mdcunits ^property[251].valueCode = #267890 
* #_mdcunits ^property[252].code = #child 
* #_mdcunits ^property[252].valueCode = #267904 
* #_mdcunits ^property[253].code = #child 
* #_mdcunits ^property[253].valueCode = #267908 
* #_mdcunits ^property[254].code = #child 
* #_mdcunits ^property[254].valueCode = #267922 
* #_mdcunits ^property[255].code = #child 
* #_mdcunits ^property[255].valueCode = #267936 
* #_mdcunits ^property[256].code = #child 
* #_mdcunits ^property[256].valueCode = #267940 
* #_mdcunits ^property[257].code = #child 
* #_mdcunits ^property[257].valueCode = #267954 
* #_mdcunits ^property[258].code = #child 
* #_mdcunits ^property[258].valueCode = #267968 
* #_mdcunits ^property[259].code = #child 
* #_mdcunits ^property[259].valueCode = #267971 
* #_mdcunits ^property[260].code = #child 
* #_mdcunits ^property[260].valueCode = #267972 
* #_mdcunits ^property[261].code = #child 
* #_mdcunits ^property[261].valueCode = #267986 
* #_mdcunits ^property[262].code = #child 
* #_mdcunits ^property[262].valueCode = #268000 
* #_mdcunits ^property[263].code = #child 
* #_mdcunits ^property[263].valueCode = #268003 
* #_mdcunits ^property[264].code = #child 
* #_mdcunits ^property[264].valueCode = #268004 
* #_mdcunits ^property[265].code = #child 
* #_mdcunits ^property[265].valueCode = #268018 
* #_mdcunits ^property[266].code = #child 
* #_mdcunits ^property[266].valueCode = #268032 
* #_mdcunits ^property[267].code = #child 
* #_mdcunits ^property[267].valueCode = #268050 
* #_mdcunits ^property[268].code = #child 
* #_mdcunits ^property[268].valueCode = #268064 
* #_mdcunits ^property[269].code = #child 
* #_mdcunits ^property[269].valueCode = #268096 
* #_mdcunits ^property[270].code = #child 
* #_mdcunits ^property[270].valueCode = #268128 
* #_mdcunits ^property[271].code = #child 
* #_mdcunits ^property[271].valueCode = #268160 
* #_mdcunits ^property[272].code = #child 
* #_mdcunits ^property[272].valueCode = #268192 
* #_mdcunits ^property[273].code = #child 
* #_mdcunits ^property[273].valueCode = #268224 
* #_mdcunits ^property[274].code = #child 
* #_mdcunits ^property[274].valueCode = #268256 
* #_mdcunits ^property[275].code = #child 
* #_mdcunits ^property[275].valueCode = #268274 
* #_mdcunits ^property[276].code = #child 
* #_mdcunits ^property[276].valueCode = #268288 
* #_mdcunits ^property[277].code = #child 
* #_mdcunits ^property[277].valueCode = #268320 
* #_mdcunits ^property[278].code = #child 
* #_mdcunits ^property[278].valueCode = #268352 
* #_mdcunits ^property[279].code = #child 
* #_mdcunits ^property[279].valueCode = #268384 
* #_mdcunits ^property[280].code = #child 
* #_mdcunits ^property[280].valueCode = #268416 
* #_mdcunits ^property[281].code = #child 
* #_mdcunits ^property[281].valueCode = #268448 
* #_mdcunits ^property[282].code = #child 
* #_mdcunits ^property[282].valueCode = #268480 
* #_mdcunits ^property[283].code = #child 
* #_mdcunits ^property[283].valueCode = #268512 
* #_mdcunits ^property[284].code = #child 
* #_mdcunits ^property[284].valueCode = #268544 
* #_mdcunits ^property[285].code = #child 
* #_mdcunits ^property[285].valueCode = #268562 
* #_mdcunits ^property[286].code = #child 
* #_mdcunits ^property[286].valueCode = #268576 
* #_mdcunits ^property[287].code = #child 
* #_mdcunits ^property[287].valueCode = #268608 
* #_mdcunits ^property[288].code = #child 
* #_mdcunits ^property[288].valueCode = #268640 
* #_mdcunits ^property[289].code = #child 
* #_mdcunits ^property[289].valueCode = #268672 
* #_mdcunits ^property[290].code = #child 
* #_mdcunits ^property[290].valueCode = #268704 
* #_mdcunits ^property[291].code = #child 
* #_mdcunits ^property[291].valueCode = #268736 
* #_mdcunits ^property[292].code = #child 
* #_mdcunits ^property[292].valueCode = #268832 
* #_mdcunits ^property[293].code = #child 
* #_mdcunits ^property[293].valueCode = #268864 
* #_mdcunits ^property[294].code = #child 
* #_mdcunits ^property[294].valueCode = #268896 
* #_mdcunits ^property[295].code = #child 
* #_mdcunits ^property[295].valueCode = #268914 
* #_mdcunits ^property[296].code = #child 
* #_mdcunits ^property[296].valueCode = #268915 
* #_mdcunits ^property[297].code = #child 
* #_mdcunits ^property[297].valueCode = #268916 
* #_mdcunits ^property[298].code = #child 
* #_mdcunits ^property[298].valueCode = #268928 
* #_mdcunits ^property[299].code = #child 
* #_mdcunits ^property[299].valueCode = #268946 
* #_mdcunits ^property[300].code = #child 
* #_mdcunits ^property[300].valueCode = #268947 
* #_mdcunits ^property[301].code = #child 
* #_mdcunits ^property[301].valueCode = #268948 
* #_mdcunits ^property[302].code = #child 
* #_mdcunits ^property[302].valueCode = #268960 
* #_mdcunits ^property[303].code = #child 
* #_mdcunits ^property[303].valueCode = #268978 
* #_mdcunits ^property[304].code = #child 
* #_mdcunits ^property[304].valueCode = #268979 
* #_mdcunits ^property[305].code = #child 
* #_mdcunits ^property[305].valueCode = #268980 
* #_mdcunits ^property[306].code = #child 
* #_mdcunits ^property[306].valueCode = #268992 
* #_mdcunits ^property[307].code = #child 
* #_mdcunits ^property[307].valueCode = #269010 
* #_mdcunits ^property[308].code = #child 
* #_mdcunits ^property[308].valueCode = #269011 
* #_mdcunits ^property[309].code = #child 
* #_mdcunits ^property[309].valueCode = #269012 
* #_mdcunits ^property[310].code = #child 
* #_mdcunits ^property[310].valueCode = #269024 
* #_mdcunits ^property[311].code = #child 
* #_mdcunits ^property[311].valueCode = #269042 
* #_mdcunits ^property[312].code = #child 
* #_mdcunits ^property[312].valueCode = #269056 
* #_mdcunits ^property[313].code = #child 
* #_mdcunits ^property[313].valueCode = #269074 
* #_mdcunits ^property[314].code = #child 
* #_mdcunits ^property[314].valueCode = #269088 
* #_mdcunits ^property[315].code = #child 
* #_mdcunits ^property[315].valueCode = #269092 
* #_mdcunits ^property[316].code = #child 
* #_mdcunits ^property[316].valueCode = #269106 
* #_mdcunits ^property[317].code = #child 
* #_mdcunits ^property[317].valueCode = #269120 
* #_mdcunits ^property[318].code = #child 
* #_mdcunits ^property[318].valueCode = #269124 
* #_mdcunits ^property[319].code = #child 
* #_mdcunits ^property[319].valueCode = #269138 
* #_mdcunits ^property[320].code = #child 
* #_mdcunits ^property[320].valueCode = #269152 
* #_mdcunits ^property[321].code = #child 
* #_mdcunits ^property[321].valueCode = #269170 
* #_mdcunits ^property[322].code = #child 
* #_mdcunits ^property[322].valueCode = #269184 
* #_mdcunits ^property[323].code = #child 
* #_mdcunits ^property[323].valueCode = #269202 
* #_mdcunits ^property[324].code = #child 
* #_mdcunits ^property[324].valueCode = #269216 
* #_mdcunits ^property[325].code = #child 
* #_mdcunits ^property[325].valueCode = #269234 
* #_mdcunits ^property[326].code = #child 
* #_mdcunits ^property[326].valueCode = #269248 
* #_mdcunits ^property[327].code = #child 
* #_mdcunits ^property[327].valueCode = #269266 
* #_mdcunits ^property[328].code = #child 
* #_mdcunits ^property[328].valueCode = #269280 
* #_mdcunits ^property[329].code = #child 
* #_mdcunits ^property[329].valueCode = #269298 
* #_mdcunits ^property[330].code = #child 
* #_mdcunits ^property[330].valueCode = #269312 
* #_mdcunits ^property[331].code = #child 
* #_mdcunits ^property[331].valueCode = #269330 
* #_mdcunits ^property[332].code = #child 
* #_mdcunits ^property[332].valueCode = #269331 
* #_mdcunits ^property[333].code = #child 
* #_mdcunits ^property[333].valueCode = #269332 
* #_mdcunits ^property[334].code = #child 
* #_mdcunits ^property[334].valueCode = #269344 
* #_mdcunits ^property[335].code = #child 
* #_mdcunits ^property[335].valueCode = #269348 
* #_mdcunits ^property[336].code = #child 
* #_mdcunits ^property[336].valueCode = #269362 
* #_mdcunits ^property[337].code = #child 
* #_mdcunits ^property[337].valueCode = #269376 
* #_mdcunits ^property[338].code = #child 
* #_mdcunits ^property[338].valueCode = #269394 
* #_mdcunits ^property[339].code = #child 
* #_mdcunits ^property[339].valueCode = #269408 
* #_mdcunits ^property[340].code = #child 
* #_mdcunits ^property[340].valueCode = #269426 
* #_mdcunits ^property[341].code = #child 
* #_mdcunits ^property[341].valueCode = #269440 
* #_mdcunits ^property[342].code = #child 
* #_mdcunits ^property[342].valueCode = #269458 
* #_mdcunits ^property[343].code = #child 
* #_mdcunits ^property[343].valueCode = #269472 
* #_mdcunits ^property[344].code = #child 
* #_mdcunits ^property[344].valueCode = #269490 
* #_mdcunits ^property[345].code = #child 
* #_mdcunits ^property[345].valueCode = #269504 
* #_mdcunits ^property[346].code = #child 
* #_mdcunits ^property[346].valueCode = #269522 
* #_mdcunits ^property[347].code = #child 
* #_mdcunits ^property[347].valueCode = #269536 
* #_mdcunits ^property[348].code = #child 
* #_mdcunits ^property[348].valueCode = #269554 
* #_mdcunits ^property[349].code = #child 
* #_mdcunits ^property[349].valueCode = #269568 
* #_mdcunits ^property[350].code = #child 
* #_mdcunits ^property[350].valueCode = #269586 
* #_mdcunits ^property[351].code = #child 
* #_mdcunits ^property[351].valueCode = #269600 
* #_mdcunits ^property[352].code = #child 
* #_mdcunits ^property[352].valueCode = #269618 
* #_mdcunits ^property[353].code = #child 
* #_mdcunits ^property[353].valueCode = #269632 
* #_mdcunits ^property[354].code = #child 
* #_mdcunits ^property[354].valueCode = #269650 
* #_mdcunits ^property[355].code = #child 
* #_mdcunits ^property[355].valueCode = #269664 
* #_mdcunits ^property[356].code = #child 
* #_mdcunits ^property[356].valueCode = #269682 
* #_mdcunits ^property[357].code = #child 
* #_mdcunits ^property[357].valueCode = #269696 
* #_mdcunits ^property[358].code = #child 
* #_mdcunits ^property[358].valueCode = #269714 
* #_mdcunits ^property[359].code = #child 
* #_mdcunits ^property[359].valueCode = #269728 
* #_mdcunits ^property[360].code = #child 
* #_mdcunits ^property[360].valueCode = #269746 
* #_mdcunits ^property[361].code = #child 
* #_mdcunits ^property[361].valueCode = #269760 
* #_mdcunits ^property[362].code = #child 
* #_mdcunits ^property[362].valueCode = #269778 
* #_mdcunits ^property[363].code = #child 
* #_mdcunits ^property[363].valueCode = #269792 
* #_mdcunits ^property[364].code = #child 
* #_mdcunits ^property[364].valueCode = #269796 
* #_mdcunits ^property[365].code = #child 
* #_mdcunits ^property[365].valueCode = #269810 
* #_mdcunits ^property[366].code = #child 
* #_mdcunits ^property[366].valueCode = #269824 
* #_mdcunits ^property[367].code = #child 
* #_mdcunits ^property[367].valueCode = #269827 
* #_mdcunits ^property[368].code = #child 
* #_mdcunits ^property[368].valueCode = #269828 
* #_mdcunits ^property[369].code = #child 
* #_mdcunits ^property[369].valueCode = #269842 
* #_mdcunits ^property[370].code = #child 
* #_mdcunits ^property[370].valueCode = #269843 
* #_mdcunits ^property[371].code = #child 
* #_mdcunits ^property[371].valueCode = #269844 
* #_mdcunits ^property[372].code = #child 
* #_mdcunits ^property[372].valueCode = #269856 
* #_mdcunits ^property[373].code = #child 
* #_mdcunits ^property[373].valueCode = #269860 
* #_mdcunits ^property[374].code = #child 
* #_mdcunits ^property[374].valueCode = #269874 
* #_mdcunits ^property[375].code = #child 
* #_mdcunits ^property[375].valueCode = #269875 
* #_mdcunits ^property[376].code = #child 
* #_mdcunits ^property[376].valueCode = #269876 
* #_mdcunits ^property[377].code = #child 
* #_mdcunits ^property[377].valueCode = #269888 
* #_mdcunits ^property[378].code = #child 
* #_mdcunits ^property[378].valueCode = #269891 
* #_mdcunits ^property[379].code = #child 
* #_mdcunits ^property[379].valueCode = #269906 
* #_mdcunits ^property[380].code = #child 
* #_mdcunits ^property[380].valueCode = #269907 
* #_mdcunits ^property[381].code = #child 
* #_mdcunits ^property[381].valueCode = #269908 
* #_mdcunits ^property[382].code = #child 
* #_mdcunits ^property[382].valueCode = #269920 
* #_mdcunits ^property[383].code = #child 
* #_mdcunits ^property[383].valueCode = #269952 
* #_mdcunits ^property[384].code = #child 
* #_mdcunits ^property[384].valueCode = #269984 
* #_mdcunits ^property[385].code = #child 
* #_mdcunits ^property[385].valueCode = #270004 
* #_mdcunits ^property[386].code = #child 
* #_mdcunits ^property[386].valueCode = #270016 
* #_mdcunits ^property[387].code = #child 
* #_mdcunits ^property[387].valueCode = #270048 
* #_mdcunits ^property[388].code = #child 
* #_mdcunits ^property[388].valueCode = #270080 
* #_mdcunits ^property[389].code = #child 
* #_mdcunits ^property[389].valueCode = #270112 
* #_mdcunits ^property[390].code = #child 
* #_mdcunits ^property[390].valueCode = #270144 
* #_mdcunits ^property[391].code = #child 
* #_mdcunits ^property[391].valueCode = #270160 
* #_mdcunits ^property[392].code = #child 
* #_mdcunits ^property[392].valueCode = #270176 
* #_mdcunits ^property[393].code = #child 
* #_mdcunits ^property[393].valueCode = #270208 
* #_mdcunits ^property[394].code = #child 
* #_mdcunits ^property[394].valueCode = #270210 
* #_mdcunits ^property[395].code = #child 
* #_mdcunits ^property[395].valueCode = #270240 
* #_mdcunits ^property[396].code = #child 
* #_mdcunits ^property[396].valueCode = #270272 
* #_mdcunits ^property[397].code = #child 
* #_mdcunits ^property[397].valueCode = #270304 
* #_mdcunits ^property[398].code = #child 
* #_mdcunits ^property[398].valueCode = #270322 
* #_mdcunits ^property[399].code = #child 
* #_mdcunits ^property[399].valueCode = #270336 
* #_mdcunits ^property[400].code = #child 
* #_mdcunits ^property[400].valueCode = #270368 
* #_mdcunits ^property[401].code = #child 
* #_mdcunits ^property[401].valueCode = #270388 
* #_mdcunits ^property[402].code = #child 
* #_mdcunits ^property[402].valueCode = #270400 
* #_mdcunits ^property[403].code = #child 
* #_mdcunits ^property[403].valueCode = #270419 
* #_mdcunits ^property[404].code = #child 
* #_mdcunits ^property[404].valueCode = #270432 
* #_mdcunits ^property[405].code = #child 
* #_mdcunits ^property[405].valueCode = #270435 
* #_mdcunits ^property[406].code = #child 
* #_mdcunits ^property[406].valueCode = #270464 
* #_mdcunits ^property[407].code = #child 
* #_mdcunits ^property[407].valueCode = #270496 
* #_mdcunits ^property[408].code = #child 
* #_mdcunits ^property[408].valueCode = #270499 
* #_mdcunits ^property[409].code = #child 
* #_mdcunits ^property[409].valueCode = #270528 
* #_mdcunits ^property[410].code = #child 
* #_mdcunits ^property[410].valueCode = #270560 
* #_mdcunits ^property[411].code = #child 
* #_mdcunits ^property[411].valueCode = #270563 
* #_mdcunits ^property[412].code = #child 
* #_mdcunits ^property[412].valueCode = #270592 
* #_mdcunits ^property[413].code = #child 
* #_mdcunits ^property[413].valueCode = #270624 
* #_mdcunits ^property[414].code = #child 
* #_mdcunits ^property[414].valueCode = #270656 
* #_mdcunits ^property[415].code = #child 
* #_mdcunits ^property[415].valueCode = #270688 
* #_mdcunits ^property[416].code = #child 
* #_mdcunits ^property[416].valueCode = #270720 
* #_mdcunits ^property[417].code = #child 
* #_mdcunits ^property[417].valueCode = #270752 
* #_mdcunits ^property[418].code = #child 
* #_mdcunits ^property[418].valueCode = #270784 
* #_mdcunits ^property[419].code = #child 
* #_mdcunits ^property[419].valueCode = #270816 
* #_mdcunits ^property[420].code = #child 
* #_mdcunits ^property[420].valueCode = #270834 
* #_mdcunits ^property[421].code = #child 
* #_mdcunits ^property[421].valueCode = #270866 
* #_mdcunits ^property[422].code = #child 
* #_mdcunits ^property[422].valueCode = #270880 
* #_mdcunits ^property[423].code = #child 
* #_mdcunits ^property[423].valueCode = #270912 
* #_mdcunits ^property[424].code = #child 
* #_mdcunits ^property[424].valueCode = #270930 
* #_mdcunits ^property[425].code = #child 
* #_mdcunits ^property[425].valueCode = #270944 
* #_mdcunits ^property[426].code = #child 
* #_mdcunits ^property[426].valueCode = #270976 
* #_mdcunits ^property[427].code = #child 
* #_mdcunits ^property[427].valueCode = #271008 
* #_mdcunits ^property[428].code = #child 
* #_mdcunits ^property[428].valueCode = #271011 
* #_mdcunits ^property[429].code = #child 
* #_mdcunits ^property[429].valueCode = #271040 
* #_mdcunits ^property[430].code = #child 
* #_mdcunits ^property[430].valueCode = #271043 
* #_mdcunits ^property[431].code = #child 
* #_mdcunits ^property[431].valueCode = #271072 
* #_mdcunits ^property[432].code = #child 
* #_mdcunits ^property[432].valueCode = #271075 
* #_mdcunits ^property[433].code = #child 
* #_mdcunits ^property[433].valueCode = #271200 
* #_mdcunits ^property[434].code = #child 
* #_mdcunits ^property[434].valueCode = #271203 
* #_mdcunits ^property[435].code = #child 
* #_mdcunits ^property[435].valueCode = #271204 
* #_mdcunits ^property[436].code = #child 
* #_mdcunits ^property[436].valueCode = #271218 
* #_mdcunits ^property[437].code = #child 
* #_mdcunits ^property[437].valueCode = #271232 
* #_mdcunits ^property[438].code = #child 
* #_mdcunits ^property[438].valueCode = #271264 
* #_mdcunits ^property[439].code = #child 
* #_mdcunits ^property[439].valueCode = #271296 
* #_mdcunits ^property[440].code = #child 
* #_mdcunits ^property[440].valueCode = #271328 
* #_mdcunits ^property[441].code = #child 
* #_mdcunits ^property[441].valueCode = #271331 
* #_mdcunits ^property[442].code = #child 
* #_mdcunits ^property[442].valueCode = #271332 
* #_mdcunits ^property[443].code = #child 
* #_mdcunits ^property[443].valueCode = #271346 
* #_mdcunits ^property[444].code = #child 
* #_mdcunits ^property[444].valueCode = #271360 
* #_mdcunits ^property[445].code = #child 
* #_mdcunits ^property[445].valueCode = #271364 
* #_mdcunits ^property[446].code = #child 
* #_mdcunits ^property[446].valueCode = #271392 
* #_mdcunits ^property[447].code = #child 
* #_mdcunits ^property[447].valueCode = #271396 
* #_mdcunits ^property[448].code = #child 
* #_mdcunits ^property[448].valueCode = #271410 
* #_mdcunits ^property[449].code = #child 
* #_mdcunits ^property[449].valueCode = #271424 
* #_mdcunits ^property[450].code = #child 
* #_mdcunits ^property[450].valueCode = #271427 
* #_mdcunits ^property[451].code = #child 
* #_mdcunits ^property[451].valueCode = #271428 
* #_mdcunits ^property[452].code = #child 
* #_mdcunits ^property[452].valueCode = #271442 
* #_mdcunits ^property[453].code = #child 
* #_mdcunits ^property[453].valueCode = #271456 
* #_mdcunits ^property[454].code = #child 
* #_mdcunits ^property[454].valueCode = #271459 
* #_mdcunits ^property[455].code = #child 
* #_mdcunits ^property[455].valueCode = #271460 
* #_mdcunits ^property[456].code = #child 
* #_mdcunits ^property[456].valueCode = #271474 
* #_mdcunits ^property[457].code = #child 
* #_mdcunits ^property[457].valueCode = #271488 
* #_mdcunits ^property[458].code = #child 
* #_mdcunits ^property[458].valueCode = #271491 
* #_mdcunits ^property[459].code = #child 
* #_mdcunits ^property[459].valueCode = #271492 
* #_mdcunits ^property[460].code = #child 
* #_mdcunits ^property[460].valueCode = #271506 
* #_mdcunits ^property[461].code = #child 
* #_mdcunits ^property[461].valueCode = #271520 
* #_mdcunits ^property[462].code = #child 
* #_mdcunits ^property[462].valueCode = #271524 
* #_mdcunits ^property[463].code = #child 
* #_mdcunits ^property[463].valueCode = #271538 
* #_mdcunits ^property[464].code = #child 
* #_mdcunits ^property[464].valueCode = #271552 
* #_mdcunits ^property[465].code = #child 
* #_mdcunits ^property[465].valueCode = #271556 
* #_mdcunits ^property[466].code = #child 
* #_mdcunits ^property[466].valueCode = #271584 
* #_mdcunits ^property[467].code = #child 
* #_mdcunits ^property[467].valueCode = #271588 
* #_mdcunits ^property[468].code = #child 
* #_mdcunits ^property[468].valueCode = #271602 
* #_mdcunits ^property[469].code = #child 
* #_mdcunits ^property[469].valueCode = #271616 
* #_mdcunits ^property[470].code = #child 
* #_mdcunits ^property[470].valueCode = #271619 
* #_mdcunits ^property[471].code = #child 
* #_mdcunits ^property[471].valueCode = #271620 
* #_mdcunits ^property[472].code = #child 
* #_mdcunits ^property[472].valueCode = #271634 
* #_mdcunits ^property[473].code = #child 
* #_mdcunits ^property[473].valueCode = #271648 
* #_mdcunits ^property[474].code = #child 
* #_mdcunits ^property[474].valueCode = #271651 
* #_mdcunits ^property[475].code = #child 
* #_mdcunits ^property[475].valueCode = #271652 
* #_mdcunits ^property[476].code = #child 
* #_mdcunits ^property[476].valueCode = #271666 
* #_mdcunits ^property[477].code = #child 
* #_mdcunits ^property[477].valueCode = #271680 
* #_mdcunits ^property[478].code = #child 
* #_mdcunits ^property[478].valueCode = #271698 
* #_mdcunits ^property[479].code = #child 
* #_mdcunits ^property[479].valueCode = #271712 
* #_mdcunits ^property[480].code = #child 
* #_mdcunits ^property[480].valueCode = #271744 
* #_mdcunits ^property[481].code = #child 
* #_mdcunits ^property[481].valueCode = #271748 
* #_mdcunits ^property[482].code = #child 
* #_mdcunits ^property[482].valueCode = #271762 
* #_mdcunits ^property[483].code = #child 
* #_mdcunits ^property[483].valueCode = #271776 
* #_mdcunits ^property[484].code = #child 
* #_mdcunits ^property[484].valueCode = #271780 
* #_mdcunits ^property[485].code = #child 
* #_mdcunits ^property[485].valueCode = #271794 
* #_mdcunits ^property[486].code = #child 
* #_mdcunits ^property[486].valueCode = #271808 
* #_mdcunits ^property[487].code = #child 
* #_mdcunits ^property[487].valueCode = #271812 
* #_mdcunits ^property[488].code = #child 
* #_mdcunits ^property[488].valueCode = #271826 
* #_mdcunits ^property[489].code = #child 
* #_mdcunits ^property[489].valueCode = #271840 
* #_mdcunits ^property[490].code = #child 
* #_mdcunits ^property[490].valueCode = #271844 
* #_mdcunits ^property[491].code = #child 
* #_mdcunits ^property[491].valueCode = #271858 
* #_mdcunits ^property[492].code = #child 
* #_mdcunits ^property[492].valueCode = #272544 
* #_mdcunits ^property[493].code = #child 
* #_mdcunits ^property[493].valueCode = #272562 
* #_mdcunits ^property[494].code = #child 
* #_mdcunits ^property[494].valueCode = #272576 
* #_mdcunits ^property[495].code = #child 
* #_mdcunits ^property[495].valueCode = #272594 
* #_mdcunits ^property[496].code = #child 
* #_mdcunits ^property[496].valueCode = #272640 
* #_mdcunits ^property[497].code = #child 
* #_mdcunits ^property[497].valueCode = #272672 
* #_mdcunits ^property[498].code = #child 
* #_mdcunits ^property[498].valueCode = #272704 
* #_mdcunits ^property[499].code = #child 
* #_mdcunits ^property[499].valueCode = #272739 
* #_mdcunits ^property[500].code = #child 
* #_mdcunits ^property[500].valueCode = #272914 
* #_mdcunits ^property[501].code = #child 
* #_mdcunits ^property[501].valueCode = #272978 
* #_mdcunits ^property[502].code = #child 
* #_mdcunits ^property[502].valueCode = #272992 
* #_mdcunits ^property[503].code = #child 
* #_mdcunits ^property[503].valueCode = #273024 
* #_mdcunits ^property[504].code = #child 
* #_mdcunits ^property[504].valueCode = #273042 
* #_mdcunits ^property[505].code = #child 
* #_mdcunits ^property[505].valueCode = #273056 
* #_mdcunits ^property[506].code = #child 
* #_mdcunits ^property[506].valueCode = #273376 
* #_mdcunits ^property[507].code = #child 
* #_mdcunits ^property[507].valueCode = #273408 
* #_mdcunits ^property[508].code = #child 
* #_mdcunits ^property[508].valueCode = #273458 
* #_mdcunits ^property[509].code = #child 
* #_mdcunits ^property[509].valueCode = #273475 
* #_mdcunits ^property[510].code = #child 
* #_mdcunits ^property[510].valueCode = #273525 
* #_mdcunits ^property[511].code = #child 
* #_mdcunits ^property[511].valueCode = #273536 
* #_mdcunits ^property[512].code = #child 
* #_mdcunits ^property[512].valueCode = #273568 
* #_mdcunits ^property[513].code = #child 
* #_mdcunits ^property[513].valueCode = #273600 
* #262144 "MDC_DIM_NOS"
* #262144 ^definition = {unknown}
* #262144 ^designation[0].language = #de-AT 
* #262144 ^designation[0].value = " Unspecified" 
* #262144 ^property[0].code = #parent 
* #262144 ^property[0].valueCode = #_mdcunits 
* #262144 ^property[1].code = #hints 
* #262144 ^property[1].valueString = "PART: 4 ~ CODE10: 0" 
* #262656 "MDC_DIM_DIMLESS"
* #262656 ^definition = 1 {unitless}
* #262656 ^designation[0].language = #de-AT 
* #262656 ^designation[0].value = " Dimension- less" 
* #262656 ^property[0].code = #parent 
* #262656 ^property[0].valueCode = #_mdcunits 
* #262656 ^property[1].code = #hints 
* #262656 ^property[1].valueString = "PART: 4 ~ CODE10: 512" 
* #262688 "MDC_DIM_PERCENT"
* #262688 ^definition = %
* #262688 ^designation[0].language = #de-AT 
* #262688 ^designation[0].value = " - Bel and deciBel   (proposal dB3')'" 
* #262688 ^property[0].code = #parent 
* #262688 ^property[0].valueCode = #_mdcunits 
* #262688 ^property[1].code = #hints 
* #262688 ^property[1].valueString = "PART: 4 ~ CODE10: 544" 
* #262720 "MDC_DIM_PARTS_PER_10_TO_MINUS_3"
* #262720 ^definition = [ppth]
* #262720 ^designation[0].language = #de-AT 
* #262720 ^designation[0].value = " - Bel and deciBel   (proposal dB3')'" 
* #262720 ^property[0].code = #parent 
* #262720 ^property[0].valueCode = #_mdcunits 
* #262720 ^property[1].code = #hints 
* #262720 ^property[1].valueString = "PART: 4 ~ CODE10: 576" 
* #262752 "MDC_DIM_PARTS_PER_10_TO_MINUS_6"
* #262752 ^definition = [ppm]
* #262752 ^designation[0].language = #de-AT 
* #262752 ^designation[0].value = " - Bel and deciBel   (proposal dB3')'" 
* #262752 ^property[0].code = #parent 
* #262752 ^property[0].valueCode = #_mdcunits 
* #262752 ^property[1].code = #hints 
* #262752 ^property[1].valueString = "PART: 4 ~ CODE10: 608" 
* #262784 "MDC_DIM_PARTS_PER_10_TO_MINUS_9"
* #262784 ^definition = 10*-9 10^-9
* #262784 ^designation[0].language = #de-AT 
* #262784 ^designation[0].value = " - Bel and deciBel   (proposal dB3')'" 
* #262784 ^property[0].code = #parent 
* #262784 ^property[0].valueCode = #_mdcunits 
* #262784 ^property[1].code = #hints 
* #262784 ^property[1].valueString = "PART: 4 ~ CODE10: 640" 
* #262816 "MDC_DIM_PARTS_PER_10_TO_MINUS_12"
* #262816 ^definition = 10*-12 10^-12
* #262816 ^designation[0].language = #de-AT 
* #262816 ^designation[0].value = " - Bel and deciBel   (proposal dB3')'" 
* #262816 ^property[0].code = #parent 
* #262816 ^property[0].valueCode = #_mdcunits 
* #262816 ^property[1].code = #hints 
* #262816 ^property[1].valueString = "PART: 4 ~ CODE10: 672" 
* #262880 "MDC_DIM_ANG_DEG"
* #262880 ^definition = deg
* #262880 ^designation[0].language = #de-AT 
* #262880 ^designation[0].value = " - angles" 
* #262880 ^property[0].code = #parent 
* #262880 ^property[0].valueCode = #_mdcunits 
* #262880 ^property[1].code = #hints 
* #262880 ^property[1].valueString = "PART: 4 ~ CODE10: 736" 
* #262912 "MDC_DIM_ANG_RAD"
* #262912 ^definition = rad
* #262912 ^designation[0].language = #de-AT 
* #262912 ^designation[0].value = " - angles" 
* #262912 ^property[0].code = #parent 
* #262912 ^property[0].valueCode = #_mdcunits 
* #262912 ^property[1].code = #hints 
* #262912 ^property[1].valueString = "PART: 4 ~ CODE10: 768" 
* #262944 "MDC_DIM_X_G_PER_G"
* #262944 ^definition = g/g
* #262944 ^designation[0].language = #de-AT 
* #262944 ^designation[0].value = " - mass fraction" 
* #262944 ^property[0].code = #parent 
* #262944 ^property[0].valueCode = #_mdcunits 
* #262944 ^property[1].code = #hints 
* #262944 ^property[1].valueString = "PART: 4 ~ CODE10: 800" 
* #262976 "MDC_DIM_X_G_PER_KG"
* #262976 ^definition = g/kg
* #262976 ^designation[0].language = #de-AT 
* #262976 ^designation[0].value = " - mass fraction" 
* #262976 ^property[0].code = #parent 
* #262976 ^property[0].valueCode = #_mdcunits 
* #262976 ^property[1].code = #hints 
* #262976 ^property[1].valueString = "PART: 4 ~ CODE10: 832" 
* #262994 "MDC_DIM_MILLI_G_PER_KG"
* #262994 ^definition = mg/kg
* #262994 ^designation[0].language = #de-AT 
* #262994 ^designation[0].value = " - mass fraction" 
* #262994 ^property[0].code = #parent 
* #262994 ^property[0].valueCode = #_mdcunits 
* #262994 ^property[1].code = #hints 
* #262994 ^property[1].valueString = "PART: 4 ~ CODE10: 850" 
* #262995 "MDC_DIM_MICRO_G_PER_KG"
* #262995 ^definition = ug/kg
* #262995 ^designation[0].language = #de-AT 
* #262995 ^designation[0].value = " - mass fraction" 
* #262995 ^property[0].code = #parent 
* #262995 ^property[0].valueCode = #_mdcunits 
* #262995 ^property[1].code = #hints 
* #262995 ^property[1].valueString = "PART: 4 ~ CODE10: 851" 
* #262996 "MDC_DIM_NANO_G_PER_KG"
* #262996 ^definition = ng/kg
* #262996 ^designation[0].language = #de-AT 
* #262996 ^designation[0].value = " - mass fraction" 
* #262996 ^property[0].code = #parent 
* #262996 ^property[0].valueCode = #_mdcunits 
* #262996 ^property[1].code = #hints 
* #262996 ^property[1].valueString = "PART: 4 ~ CODE10: 852" 
* #263008 "MDC_DIM_X_MOLE_PER_MOLE"
* #263008 ^definition = mol/mol
* #263008 ^designation[0].language = #de-AT 
* #263008 ^designation[0].value = " - relative quantity" 
* #263008 ^property[0].code = #parent 
* #263008 ^property[0].valueCode = #_mdcunits 
* #263008 ^property[1].code = #hints 
* #263008 ^property[1].valueString = "PART: 4 ~ CODE10: 864" 
* #263026 "MDC_DIM_MILLI_MOLE_PER_MOLE"
* #263026 ^definition = mmol/mol
* #263026 ^property[0].code = #parent 
* #263026 ^property[0].valueCode = #_mdcunits 
* #263026 ^property[1].code = #hints 
* #263026 ^property[1].valueString = "PART: 4 ~ CODE10: 882" 
* #263040 "MDC_DIM_X_L_PER_L"
* #263040 ^definition = L/L
* #263040 ^designation[0].language = #de-AT 
* #263040 ^designation[0].value = " - volume fraction" 
* #263040 ^property[0].code = #parent 
* #263040 ^property[0].valueCode = #_mdcunits 
* #263040 ^property[1].code = #hints 
* #263040 ^property[1].valueString = "PART: 4 ~ CODE10: 896" 
* #263072 "MDC_DIM_CUBIC_X_M_PER_M_CUBE"
* #263072 ^definition = DO_NOT_USE
* #263072 ^designation[0].language = #de-AT 
* #263072 ^designation[0].value = " - volume fraction" 
* #263072 ^property[0].code = #parent 
* #263072 ^property[0].valueCode = #_mdcunits 
* #263072 ^property[1].code = #hints 
* #263072 ^property[1].valueString = "PART: 4 ~ CODE10: 928" 
* #263104 "MDC_DIM_CUBIC_X_M_PER_CM_CUBE"
* #263104 ^definition = DO_NOT_USE
* #263104 ^designation[0].language = #de-AT 
* #263104 ^designation[0].value = " - volume fraction" 
* #263104 ^property[0].code = #parent 
* #263104 ^property[0].valueCode = #_mdcunits 
* #263104 ^property[1].code = #hints 
* #263104 ^property[1].valueString = "PART: 4 ~ CODE10: 960" 
* #263136 "MDC_DIM_PH"
* #263136 ^definition = [pH]
* #263136 ^designation[0].language = #de-AT 
* #263136 ^designation[0].value = " - special vital signs counts" 
* #263136 ^property[0].code = #parent 
* #263136 ^property[0].valueCode = #_mdcunits 
* #263136 ^property[1].code = #hints 
* #263136 ^property[1].valueString = "PART: 4 ~ CODE10: 992" 
* #263168 "MDC_DIM_DROP"
* #263168 ^definition = [drp]
* #263168 ^designation[0].language = #de-AT 
* #263168 ^designation[0].value = " - special vital signs counts" 
* #263168 ^property[0].code = #parent 
* #263168 ^property[0].valueCode = #_mdcunits 
* #263168 ^property[1].code = #hints 
* #263168 ^property[1].valueString = "PART: 4 ~ CODE10: 1024" 
* #263200 "MDC_DIM_RBC"
* #263200 ^definition = {rbc}
* #263200 ^designation[0].language = #de-AT 
* #263200 ^designation[0].value = " - special vital signs counts" 
* #263200 ^property[0].code = #parent 
* #263200 ^property[0].valueCode = #_mdcunits 
* #263200 ^property[1].code = #hints 
* #263200 ^property[1].valueString = "PART: 4 ~ CODE10: 1056" 
* #263232 "MDC_DIM_BEAT"
* #263232 ^definition = {beat}
* #263232 ^designation[0].language = #de-AT 
* #263232 ^designation[0].value = " - special vital signs counts" 
* #263232 ^property[0].code = #parent 
* #263232 ^property[0].valueCode = #_mdcunits 
* #263232 ^property[1].code = #hints 
* #263232 ^property[1].valueString = "PART: 4 ~ CODE10: 1088" 
* #263264 "MDC_DIM_BREATH"
* #263264 ^definition = {breath}
* #263264 ^designation[0].language = #de-AT 
* #263264 ^designation[0].value = " - special vital signs counts" 
* #263264 ^property[0].code = #parent 
* #263264 ^property[0].valueCode = #_mdcunits 
* #263264 ^property[1].code = #hints 
* #263264 ^property[1].valueString = "PART: 4 ~ CODE10: 1120" 
* #263296 "MDC_DIM_CELL"
* #263296 ^definition = {cell}
* #263296 ^designation[0].language = #de-AT 
* #263296 ^designation[0].value = " - special vital signs counts" 
* #263296 ^property[0].code = #parent 
* #263296 ^property[0].valueCode = #_mdcunits 
* #263296 ^property[1].code = #hints 
* #263296 ^property[1].valueString = "PART: 4 ~ CODE10: 1152" 
* #263328 "MDC_DIM_COUGH"
* #263328 ^definition = {cough}
* #263328 ^designation[0].language = #de-AT 
* #263328 ^designation[0].value = " - special vital signs counts" 
* #263328 ^property[0].code = #parent 
* #263328 ^property[0].valueCode = #_mdcunits 
* #263328 ^property[1].code = #hints 
* #263328 ^property[1].valueString = "PART: 4 ~ CODE10: 1184" 
* #263360 "MDC_DIM_SIGH"
* #263360 ^definition = {sigh}
* #263360 ^designation[0].language = #de-AT 
* #263360 ^designation[0].value = " - special vital signs counts" 
* #263360 ^property[0].code = #parent 
* #263360 ^property[0].valueCode = #_mdcunits 
* #263360 ^property[1].code = #hints 
* #263360 ^property[1].valueString = "PART: 4 ~ CODE10: 1216" 
* #263392 "MDC_DIM_PCT_PCV"
* #263392 ^definition = %{pcv}
* #263392 ^designation[0].language = #de-AT 
* #263392 ^designation[0].value = " - special vital signs counts" 
* #263392 ^property[0].code = #parent 
* #263392 ^property[0].valueCode = #_mdcunits 
* #263392 ^property[1].code = #hints 
* #263392 ^property[1].valueString = "PART: 4 ~ CODE10: 1248" 
* #263424 "MDC_DIM_M"
* #263424 ^definition = m
* #263424 ^designation[0].language = #de-AT 
* #263424 ^designation[0].value = " L (length)" 
* #263424 ^property[0].code = #parent 
* #263424 ^property[0].valueCode = #_mdcunits 
* #263424 ^property[1].code = #hints 
* #263424 ^property[1].valueString = "PART: 4 ~ CODE10: 1280" 
* #263441 "MDC_DIM_CENTI_M"
* #263441 ^definition = cm
* #263441 ^designation[0].language = #de-AT 
* #263441 ^designation[0].value = " L (length)" 
* #263441 ^property[0].code = #parent 
* #263441 ^property[0].valueCode = #_mdcunits 
* #263441 ^property[1].code = #hints 
* #263441 ^property[1].valueString = "PART: 4 ~ CODE10: 1297" 
* #263442 "MDC_DIM_MILLI_M"
* #263442 ^definition = mm
* #263442 ^designation[0].language = #de-AT 
* #263442 ^designation[0].value = " L (length)" 
* #263442 ^property[0].code = #parent 
* #263442 ^property[0].valueCode = #_mdcunits 
* #263442 ^property[1].code = #hints 
* #263442 ^property[1].valueString = "PART: 4 ~ CODE10: 1298" 
* #263443 "MDC_DIM_MICRO_M"
* #263443 ^definition = um
* #263443 ^designation[0].language = #de-AT 
* #263443 ^designation[0].value = " L (length)" 
* #263443 ^property[0].code = #parent 
* #263443 ^property[0].valueCode = #_mdcunits 
* #263443 ^property[1].code = #hints 
* #263443 ^property[1].valueString = "PART: 4 ~ CODE10: 1299" 
* #263520 "MDC_DIM_INCH"
* #263520 ^definition = [in_i]
* #263520 ^property[0].code = #parent 
* #263520 ^property[0].valueCode = #_mdcunits 
* #263520 ^property[1].code = #hints 
* #263520 ^property[1].valueString = "PART: 4 ~ CODE10: 1376" 
* #263552 "MDC_DIM_X_L_PER_M_SQ"
* #263552 ^definition = L/m2
* #263552 ^designation[0].language = #de-AT 
* #263552 ^designation[0].value = " L (areic volume)" 
* #263552 ^property[0].code = #parent 
* #263552 ^property[0].valueCode = #_mdcunits 
* #263552 ^property[1].code = #hints 
* #263552 ^property[1].valueString = "PART: 4 ~ CODE10: 1408" 
* #263570 "MDC_DIM_MILLI_L_PER_M_SQ"
* #263570 ^definition = mL/m2
* #263570 ^designation[0].language = #de-AT 
* #263570 ^designation[0].value = " L (areic volume)" 
* #263570 ^property[0].code = #parent 
* #263570 ^property[0].valueCode = #_mdcunits 
* #263570 ^property[1].code = #hints 
* #263570 ^property[1].valueString = "PART: 4 ~ CODE10: 1426" 
* #263584 "MDC_DIM_PER_X_M"
* #263584 ^definition = /m
* #263584 ^designation[0].language = #de-AT 
* #263584 ^designation[0].value = " L-1" 
* #263584 ^property[0].code = #parent 
* #263584 ^property[0].valueCode = #_mdcunits 
* #263584 ^property[1].code = #hints 
* #263584 ^property[1].valueString = "PART: 4 ~ CODE10: 1440" 
* #263616 "MDC_DIM_SQ_M"
* #263616 ^definition = m2
* #263616 ^designation[0].language = #de-AT 
* #263616 ^designation[0].value = " L2 (area)" 
* #263616 ^property[0].code = #parent 
* #263616 ^property[0].valueCode = #_mdcunits 
* #263616 ^property[1].code = #hints 
* #263616 ^property[1].valueString = "PART: 4 ~ CODE10: 1472" 
* #263680 "MDC_DIM_PER_SQ_X_M"
* #263680 ^definition = /m2
* #263680 ^designation[0].language = #de-AT 
* #263680 ^designation[0].value = " L-2" 
* #263680 ^property[0].code = #parent 
* #263680 ^property[0].valueCode = #_mdcunits 
* #263680 ^property[1].code = #hints 
* #263680 ^property[1].valueString = "PART: 4 ~ CODE10: 1536" 
* #263712 "MDC_DIM_CUBIC_X_M"
* #263712 ^definition = m3
* #263712 ^designation[0].language = #de-AT 
* #263712 ^designation[0].value = " L3 (volume)" 
* #263712 ^property[0].code = #parent 
* #263712 ^property[0].valueCode = #_mdcunits 
* #263712 ^property[1].code = #hints 
* #263712 ^property[1].valueString = "PART: 4 ~ CODE10: 1568" 
* #263744 "MDC_DIM_L"
* #263744 ^definition = L
* #263744 ^property[0].code = #parent 
* #263744 ^property[0].valueCode = #_mdcunits 
* #263744 ^property[1].code = #hints 
* #263744 ^property[1].valueString = "PART: 4 ~ CODE10: 1600" 
* #263762 "MDC_DIM_MILLI_L"
* #263762 ^definition = mL
* #263762 ^designation[0].language = #de-AT 
* #263762 ^designation[0].value = " L3 (volume)" 
* #263762 ^property[0].code = #parent 
* #263762 ^property[0].valueCode = #_mdcunits 
* #263762 ^property[1].code = #hints 
* #263762 ^property[1].valueString = "PART: 4 ~ CODE10: 1618" 
* #263763 "MDC_DIM_MICRO_L"
* #263763 ^definition = uL
* #263763 ^designation[0].language = #de-AT 
* #263763 ^designation[0].value = " L3 (volume)" 
* #263763 ^property[0].code = #parent 
* #263763 ^property[0].valueCode = #_mdcunits 
* #263763 ^property[1].code = #hints 
* #263763 ^property[1].valueString = "PART: 4 ~ CODE10: 1619" 
* #263776 "MDC_DIM_X_L_PER_BREATH"
* #263776 ^definition = L/{breath}
* #263776 ^designation[0].language = #de-AT 
* #263776 ^designation[0].value = " L3 (volume)" 
* #263776 ^property[0].code = #parent 
* #263776 ^property[0].valueCode = #_mdcunits 
* #263776 ^property[1].code = #hints 
* #263776 ^property[1].valueString = "PART: 4 ~ CODE10: 1632" 
* #263794 "MDC_DIM_MILLI_L_PER_BREATH"
* #263794 ^definition = mL/{breath}
* #263794 ^designation[0].language = #de-AT 
* #263794 ^designation[0].value = " L3 (volume)" 
* #263794 ^property[0].code = #parent 
* #263794 ^property[0].valueCode = #_mdcunits 
* #263794 ^property[1].code = #hints 
* #263794 ^property[1].valueString = "PART: 4 ~ CODE10: 1650" 
* #263808 "MDC_DIM_PER_CUBIC_X_M"
* #263808 ^definition = /m3
* #263808 ^designation[0].language = #de-AT 
* #263808 ^designation[0].value = " L-3" 
* #263808 ^property[0].code = #parent 
* #263808 ^property[0].valueCode = #_mdcunits 
* #263808 ^property[1].code = #hints 
* #263808 ^property[1].valueString = "PART: 4 ~ CODE10: 1664" 
* #263840 "MDC_DIM_PER_X_L"
* #263840 ^definition = /L
* #263840 ^designation[0].language = #de-AT 
* #263840 ^designation[0].value = " L-3" 
* #263840 ^property[0].code = #parent 
* #263840 ^property[0].valueCode = #_mdcunits 
* #263840 ^property[1].code = #hints 
* #263840 ^property[1].valueString = "PART: 4 ~ CODE10: 1696" 
* #263872 "MDC_DIM_G"
* #263872 ^definition = g
* #263872 ^property[0].code = #parent 
* #263872 ^property[0].valueCode = #_mdcunits 
* #263872 ^property[1].code = #hints 
* #263872 ^property[1].valueString = "PART: 4 ~ CODE10: 1728" 
* #263875 "MDC_DIM_KILO_G"
* #263875 ^definition = kg
* #263875 ^designation[0].language = #de-AT 
* #263875 ^designation[0].value = " M (mass)" 
* #263875 ^property[0].code = #parent 
* #263875 ^property[0].valueCode = #_mdcunits 
* #263875 ^property[1].code = #hints 
* #263875 ^property[1].valueString = "PART: 4 ~ CODE10: 1731" 
* #263890 "MDC_DIM_MILLI_G"
* #263890 ^definition = mg
* #263890 ^designation[0].language = #de-AT 
* #263890 ^designation[0].value = " M (mass)" 
* #263890 ^property[0].code = #parent 
* #263890 ^property[0].valueCode = #_mdcunits 
* #263890 ^property[1].code = #hints 
* #263890 ^property[1].valueString = "PART: 4 ~ CODE10: 1746" 
* #263891 "MDC_DIM_MICRO_G"
* #263891 ^definition = ug
* #263891 ^designation[0].language = #de-AT 
* #263891 ^designation[0].value = " M (mass)" 
* #263891 ^property[0].code = #parent 
* #263891 ^property[0].valueCode = #_mdcunits 
* #263891 ^property[1].code = #hints 
* #263891 ^property[1].valueString = "PART: 4 ~ CODE10: 1747" 
* #263892 "MDC_DIM_NANO_G"
* #263892 ^definition = ng
* #263892 ^designation[0].language = #de-AT 
* #263892 ^designation[0].value = " M (mass)" 
* #263892 ^property[0].code = #parent 
* #263892 ^property[0].valueCode = #_mdcunits 
* #263892 ^property[1].code = #hints 
* #263892 ^property[1].valueString = "PART: 4 ~ CODE10: 1748" 
* #263904 "MDC_DIM_LB"
* #263904 ^definition = [lb_av]
* #263904 ^property[0].code = #parent 
* #263904 ^property[0].valueCode = #_mdcunits 
* #263904 ^property[1].code = #hints 
* #263904 ^property[1].valueString = "PART: 4 ~ CODE10: 1760" 
* #263936 "MDC_DIM_OZ"
* #263936 ^definition = [oz_av]
* #263936 ^property[0].code = #parent 
* #263936 ^property[0].valueCode = #_mdcunits 
* #263936 ^property[1].code = #hints 
* #263936 ^property[1].valueString = "PART: 4 ~ CODE10: 1792" 
* #263968 "MDC_DIM_PER_X_G"
* #263968 ^definition = /g
* #263968 ^designation[0].language = #de-AT 
* #263968 ^designation[0].value = " M-1" 
* #263968 ^property[0].code = #parent 
* #263968 ^property[0].valueCode = #_mdcunits 
* #263968 ^property[1].code = #hints 
* #263968 ^property[1].valueString = "PART: 4 ~ CODE10: 1824" 
* #264000 "MDC_DIM_G_M"
* #264000 ^definition = g.m
* #264000 ^property[0].code = #parent 
* #264000 ^property[0].valueCode = #_mdcunits 
* #264000 ^property[1].code = #hints 
* #264000 ^property[1].valueString = "PART: 4 ~ CODE10: 1856" 
* #264032 "MDC_DIM_X_G_M_PER_M_SQ"
* #264032 ^definition = g.m/m2
* #264032 ^designation[0].language = #de-AT 
* #264032 ^designation[0].value = " ML-1" 
* #264032 ^property[0].code = #parent 
* #264032 ^property[0].valueCode = #_mdcunits 
* #264032 ^property[1].code = #hints 
* #264032 ^property[1].valueString = "PART: 4 ~ CODE10: 1888" 
* #264064 "MDC_DIM_X_G_M_SQ"
* #264064 ^definition = g.m2
* #264064 ^designation[0].language = #de-AT 
* #264064 ^designation[0].value = " ML2 (moment of inertia)" 
* #264064 ^property[0].code = #parent 
* #264064 ^property[0].valueCode = #_mdcunits 
* #264064 ^property[1].code = #hints 
* #264064 ^property[1].valueString = "PART: 4 ~ CODE10: 1920" 
* #264128 "MDC_DIM_X_G_PER_M_CUBE"
* #264128 ^definition = g/m3
* #264128 ^designation[0].language = #de-AT 
* #264128 ^designation[0].value = " ML-3 (concentration of mass)" 
* #264128 ^property[0].code = #parent 
* #264128 ^property[0].valueCode = #_mdcunits 
* #264128 ^property[1].code = #hints 
* #264128 ^property[1].valueString = "PART: 4 ~ CODE10: 1984" 
* #264160 "MDC_DIM_X_G_PER_CM_CUBE"
* #264160 ^definition = g/cm3
* #264160 ^designation[0].language = #de-AT 
* #264160 ^designation[0].value = " ML-3 (concentration of mass)" 
* #264160 ^property[0].code = #parent 
* #264160 ^property[0].valueCode = #_mdcunits 
* #264160 ^property[1].code = #hints 
* #264160 ^property[1].valueString = "PART: 4 ~ CODE10: 2016" 
* #264192 "MDC_DIM_G_PER_L"
* #264192 ^definition = g/L
* #264192 ^property[0].code = #parent 
* #264192 ^property[0].valueCode = #_mdcunits 
* #264192 ^property[1].code = #hints 
* #264192 ^property[1].valueString = "PART: 4 ~ CODE10: 2048" 
* #264224 "MDC_DIM_G_PER_CL"
* #264224 ^definition = g/cL
* #264224 ^designation[0].language = #de-AT 
* #264224 ^designation[0].value = " ML-3 (concentration of mass)" 
* #264224 ^property[0].code = #parent 
* #264224 ^property[0].valueCode = #_mdcunits 
* #264224 ^property[1].code = #hints 
* #264224 ^property[1].valueString = "PART: 4 ~ CODE10: 2080" 
* #264256 "MDC_DIM_G_PER_DL"
* #264256 ^definition = g/dL
* #264256 ^property[0].code = #parent 
* #264256 ^property[0].valueCode = #_mdcunits 
* #264256 ^property[1].code = #hints 
* #264256 ^property[1].valueString = "PART: 4 ~ CODE10: 2112" 
* #264274 "MDC_DIM_MILLI_G_PER_DL"
* #264274 ^definition = mg/dL
* #264274 ^designation[0].language = #de-AT 
* #264274 ^designation[0].value = " ML-3 (concentration of mass)" 
* #264274 ^property[0].code = #parent 
* #264274 ^property[0].valueCode = #_mdcunits 
* #264274 ^property[1].code = #hints 
* #264274 ^property[1].valueString = "PART: 4 ~ CODE10: 2130" 
* #264288 "MDC_DIM_X_G_PER_ML"
* #264288 ^definition = g/mL
* #264288 ^designation[0].language = #de-AT 
* #264288 ^designation[0].value = " ML-3 (concentration of mass)" 
* #264288 ^property[0].code = #parent 
* #264288 ^property[0].valueCode = #_mdcunits 
* #264288 ^property[1].code = #hints 
* #264288 ^property[1].valueString = "PART: 4 ~ CODE10: 2144" 
* #264306 "MDC_DIM_MILLI_G_PER_ML"
* #264306 ^definition = mg/mL
* #264306 ^designation[0].language = #de-AT 
* #264306 ^designation[0].value = " ML-3 (concentration of mass)" 
* #264306 ^property[0].code = #parent 
* #264306 ^property[0].valueCode = #_mdcunits 
* #264306 ^property[1].code = #hints 
* #264306 ^property[1].valueString = "PART: 4 ~ CODE10: 2162" 
* #264307 "MDC_DIM_MICRO_G_PER_ML"
* #264307 ^definition = ug/mL
* #264307 ^designation[0].language = #de-AT 
* #264307 ^designation[0].value = " ML-3 (concentration of mass)" 
* #264307 ^property[0].code = #parent 
* #264307 ^property[0].valueCode = #_mdcunits 
* #264307 ^property[1].code = #hints 
* #264307 ^property[1].valueString = "PART: 4 ~ CODE10: 2163" 
* #264308 "MDC_DIM_NANO_G_PER_ML"
* #264308 ^definition = ng/mL
* #264308 ^designation[0].language = #de-AT 
* #264308 ^designation[0].value = " ML-3 (concentration of mass)" 
* #264308 ^property[0].code = #parent 
* #264308 ^property[0].valueCode = #_mdcunits 
* #264308 ^property[1].code = #hints 
* #264308 ^property[1].valueString = "PART: 4 ~ CODE10: 2164" 
* #264320 "MDC_DIM_SEC"
* #264320 ^definition = s
* #264320 ^property[0].code = #parent 
* #264320 ^property[0].valueCode = #_mdcunits 
* #264320 ^property[1].code = #hints 
* #264320 ^property[1].valueString = "PART: 4 ~ CODE10: 2176" 
* #264338 "MDC_DIM_MILLI_SEC"
* #264338 ^definition = ms
* #264338 ^designation[0].language = #de-AT 
* #264338 ^designation[0].value = " T (time)" 
* #264338 ^property[0].code = #parent 
* #264338 ^property[0].valueCode = #_mdcunits 
* #264338 ^property[1].code = #hints 
* #264338 ^property[1].valueString = "PART: 4 ~ CODE10: 2194" 
* #264339 "MDC_DIM_MICRO_SEC"
* #264339 ^definition = us
* #264339 ^designation[0].language = #de-AT 
* #264339 ^designation[0].value = " T (time)" 
* #264339 ^property[0].code = #parent 
* #264339 ^property[0].valueCode = #_mdcunits 
* #264339 ^property[1].code = #hints 
* #264339 ^property[1].valueString = "PART: 4 ~ CODE10: 2195" 
* #264340 "MDC_DIM_NANO_SEC"
* #264340 ^definition = ns
* #264340 ^designation[0].language = #de-AT 
* #264340 ^designation[0].value = " T (time)" 
* #264340 ^property[0].code = #parent 
* #264340 ^property[0].valueCode = #_mdcunits 
* #264340 ^property[1].code = #hints 
* #264340 ^property[1].valueString = "PART: 4 ~ CODE10: 2196" 
* #264352 "MDC_DIM_MIN"
* #264352 ^definition = min
* #264352 ^designation[0].language = #de-AT 
* #264352 ^designation[0].value = " T (time)" 
* #264352 ^property[0].code = #parent 
* #264352 ^property[0].valueCode = #_mdcunits 
* #264352 ^property[1].code = #hints 
* #264352 ^property[1].valueString = "PART: 4 ~ CODE10: 2208" 
* #264384 "MDC_DIM_HR"
* #264384 ^definition = h
* #264384 ^designation[0].language = #de-AT 
* #264384 ^designation[0].value = " T (time)" 
* #264384 ^property[0].code = #parent 
* #264384 ^property[0].valueCode = #_mdcunits 
* #264384 ^property[1].code = #hints 
* #264384 ^property[1].valueString = "PART: 4 ~ CODE10: 2240" 
* #264416 "MDC_DIM_DAY"
* #264416 ^definition = d
* #264416 ^designation[0].language = #de-AT 
* #264416 ^designation[0].value = " T (time)" 
* #264416 ^property[0].code = #parent 
* #264416 ^property[0].valueCode = #_mdcunits 
* #264416 ^property[1].code = #hints 
* #264416 ^property[1].valueString = "PART: 4 ~ CODE10: 2272" 
* #264448 "MDC_DIM_WEEKS"
* #264448 ^definition = wk
* #264448 ^designation[0].language = #de-AT 
* #264448 ^designation[0].value = " T (time)" 
* #264448 ^property[0].code = #parent 
* #264448 ^property[0].valueCode = #_mdcunits 
* #264448 ^property[1].code = #hints 
* #264448 ^property[1].valueString = "PART: 4 ~ CODE10: 2304" 
* #264480 "MDC_DIM_MON"
* #264480 ^definition = mo
* #264480 ^designation[0].language = #de-AT 
* #264480 ^designation[0].value = " T (time)" 
* #264480 ^property[0].code = #parent 
* #264480 ^property[0].valueCode = #_mdcunits 
* #264480 ^property[1].code = #hints 
* #264480 ^property[1].valueString = "PART: 4 ~ CODE10: 2336" 
* #264512 "MDC_DIM_YR"
* #264512 ^definition = a
* #264512 ^designation[0].language = #de-AT 
* #264512 ^designation[0].value = " T (time)" 
* #264512 ^property[0].code = #parent 
* #264512 ^property[0].valueCode = #_mdcunits 
* #264512 ^property[1].code = #hints 
* #264512 ^property[1].valueString = "PART: 4 ~ CODE10: 2368" 
* #264544 "MDC_DIM_TOD"
* #264544 ^designation[0].language = #de-AT 
* #264544 ^designation[0].value = " T (time)" 
* #264544 ^property[0].code = #parent 
* #264544 ^property[0].valueCode = #_mdcunits 
* #264544 ^property[1].code = #hints 
* #264544 ^property[1].valueString = "PART: 4 ~ CODE10: 2400" 
* #264576 "MDC_DIM_DATE"
* #264576 ^designation[0].language = #de-AT 
* #264576 ^designation[0].value = " T (time)" 
* #264576 ^property[0].code = #parent 
* #264576 ^property[0].valueCode = #_mdcunits 
* #264576 ^property[1].code = #hints 
* #264576 ^property[1].valueString = "PART: 4 ~ CODE10: 2432" 
* #264608 "MDC_DIM_PER_X_SEC"
* #264608 ^definition = /s
* #264608 ^designation[0].language = #de-AT 
* #264608 ^designation[0].value = " T-1 (rate, frequency)" 
* #264608 ^property[0].code = #parent 
* #264608 ^property[0].valueCode = #_mdcunits 
* #264608 ^property[1].code = #hints 
* #264608 ^property[1].valueString = "PART: 4 ~ CODE10: 2464" 
* #264672 "MDC_DIM_PER_MIN"
* #264672 ^definition = /min 1/min {count}/min
* #264672 ^designation[0].language = #de-AT 
* #264672 ^designation[0].value = " T-1 (rate, frequency)" 
* #264672 ^property[0].code = #parent 
* #264672 ^property[0].valueCode = #_mdcunits 
* #264672 ^property[1].code = #hints 
* #264672 ^property[1].valueString = "PART: 4 ~ CODE10: 2528" 
* #264704 "MDC_DIM_PER_HR"
* #264704 ^definition = /h
* #264704 ^designation[0].language = #de-AT 
* #264704 ^designation[0].value = " T-1 (rate, frequency)" 
* #264704 ^property[0].code = #parent 
* #264704 ^property[0].valueCode = #_mdcunits 
* #264704 ^property[1].code = #hints 
* #264704 ^property[1].valueString = "PART: 4 ~ CODE10: 2560" 
* #264736 "MDC_DIM_PER_DAY"
* #264736 ^definition = /d
* #264736 ^designation[0].language = #de-AT 
* #264736 ^designation[0].value = " T-1 (rate, frequency)" 
* #264736 ^property[0].code = #parent 
* #264736 ^property[0].valueCode = #_mdcunits 
* #264736 ^property[1].code = #hints 
* #264736 ^property[1].valueString = "PART: 4 ~ CODE10: 2592" 
* #264768 "MDC_DIM_PER_WK"
* #264768 ^definition = /wk
* #264768 ^designation[0].language = #de-AT 
* #264768 ^designation[0].value = " T-1 (rate, frequency)" 
* #264768 ^property[0].code = #parent 
* #264768 ^property[0].valueCode = #_mdcunits 
* #264768 ^property[1].code = #hints 
* #264768 ^property[1].valueString = "PART: 4 ~ CODE10: 2624" 
* #264800 "MDC_DIM_PER_MO"
* #264800 ^definition = /mo
* #264800 ^designation[0].language = #de-AT 
* #264800 ^designation[0].value = " T-1 (rate, frequency)" 
* #264800 ^property[0].code = #parent 
* #264800 ^property[0].valueCode = #_mdcunits 
* #264800 ^property[1].code = #hints 
* #264800 ^property[1].valueString = "PART: 4 ~ CODE10: 2656" 
* #264832 "MDC_DIM_PER_YR"
* #264832 ^definition = /a
* #264832 ^designation[0].language = #de-AT 
* #264832 ^designation[0].value = " T-1 (rate, frequency)" 
* #264832 ^property[0].code = #parent 
* #264832 ^property[0].valueCode = #_mdcunits 
* #264832 ^property[1].code = #hints 
* #264832 ^property[1].valueString = "PART: 4 ~ CODE10: 2688" 
* #264864 "MDC_DIM_BEAT_PER_MIN"
* #264864 ^definition = /min 1/min {beats}/min {beat}/min
* #264864 ^designation[0].language = #de-AT 
* #264864 ^designation[0].value = " - special vital signs rates" 
* #264864 ^property[0].code = #parent 
* #264864 ^property[0].valueCode = #_mdcunits 
* #264864 ^property[1].code = #hints 
* #264864 ^property[1].valueString = "PART: 4 ~ CODE10: 2720" 
* #264896 "MDC_DIM_PULS_PER_MIN"
* #264896 ^definition = /min 1/min {pulses}/min {pulse}/min
* #264896 ^designation[0].language = #de-AT 
* #264896 ^designation[0].value = " - special vital signs rates" 
* #264896 ^property[0].code = #parent 
* #264896 ^property[0].valueCode = #_mdcunits 
* #264896 ^property[1].code = #hints 
* #264896 ^property[1].valueString = "PART: 4 ~ CODE10: 2752" 
* #264928 "MDC_DIM_RESP_PER_MIN"
* #264928 ^definition = /min 1/min {breaths}/min {breath}/min {resp}/min
* #264928 ^designation[0].language = #de-AT 
* #264928 ^designation[0].value = " - special vital signs rates" 
* #264928 ^property[0].code = #parent 
* #264928 ^property[0].valueCode = #_mdcunits 
* #264928 ^property[1].code = #hints 
* #264928 ^property[1].valueString = "PART: 4 ~ CODE10: 2784" 
* #264960 "MDC_DIM_X_M_PER_SEC"
* #264960 ^definition = m/s
* #264960 ^designation[0].language = #de-AT 
* #264960 ^designation[0].value = " LT-1  (velocity)" 
* #264960 ^property[0].code = #parent 
* #264960 ^property[0].valueCode = #_mdcunits 
* #264960 ^property[1].code = #hints 
* #264960 ^property[1].valueString = "PART: 4 ~ CODE10: 2816" 
* #264963 "MDC_DIM_KILO_M_PER_SEC"
* #264963 ^definition = km/h
* #264963 ^designation[0].language = #de-AT 
* #264963 ^designation[0].value = " LT-1  (velocity)" 
* #264963 ^property[0].code = #parent 
* #264963 ^property[0].valueCode = #_mdcunits 
* #264963 ^property[1].code = #hints 
* #264963 ^property[1].valueString = "PART: 4 ~ CODE10: 2819" 
* #264978 "MDC_DIM_MILLI_M_PER_SEC"
* #264978 ^definition = mm/s
* #264978 ^designation[0].language = #de-AT 
* #264978 ^designation[0].value = " LT-1  (velocity)" 
* #264978 ^property[0].code = #parent 
* #264978 ^property[0].valueCode = #_mdcunits 
* #264978 ^property[1].code = #hints 
* #264978 ^property[1].valueString = "PART: 4 ~ CODE10: 2834" 
* #264992 "MDC_DIM_L_PER_MIN_PER_M_SQ"
* #264992 ^definition = L/min/m2
* #264992 ^property[0].code = #parent 
* #264992 ^property[0].valueCode = #_mdcunits 
* #264992 ^property[1].code = #hints 
* #264992 ^property[1].valueString = "PART: 4 ~ CODE10: 2848" 
* #265010 "MDC_DIM_MILLI_L_PER_MIN_PER_M_SQ"
* #265010 ^definition = mL/min/m2
* #265010 ^designation[0].language = #de-AT 
* #265010 ^designation[0].value = " Volume per minute per body surface area" 
* #265010 ^property[0].code = #parent 
* #265010 ^property[0].valueCode = #_mdcunits 
* #265010 ^property[1].code = #hints 
* #265010 ^property[1].valueString = "PART: 4 ~ CODE10: 2866" 
* #265024 "MDC_DIM_SQ_X_M_PER_SEC"
* #265024 ^definition = m2/s
* #265024 ^designation[0].language = #de-AT 
* #265024 ^designation[0].value = " Volume per minute per body surface area" 
* #265024 ^property[0].code = #parent 
* #265024 ^property[0].valueCode = #_mdcunits 
* #265024 ^property[1].code = #hints 
* #265024 ^property[1].valueString = "PART: 4 ~ CODE10: 2880" 
* #265056 "MDC_DIM_CUBIC_X_M_PER_SEC"
* #265056 ^definition = m3/s
* #265056 ^designation[0].language = #de-AT 
* #265056 ^designation[0].value = " L3T-1  (volume flow rate)" 
* #265056 ^property[0].code = #parent 
* #265056 ^property[0].valueCode = #_mdcunits 
* #265056 ^property[1].code = #hints 
* #265056 ^property[1].valueString = "PART: 4 ~ CODE10: 2912" 
* #265088 "MDC_DIM_CUBIC_X_M_PER_MIN"
* #265088 ^definition = m3/min
* #265088 ^designation[0].language = #de-AT 
* #265088 ^designation[0].value = " L3T-1  (volume flow rate)" 
* #265088 ^property[0].code = #parent 
* #265088 ^property[0].valueCode = #_mdcunits 
* #265088 ^property[1].code = #hints 
* #265088 ^property[1].valueString = "PART: 4 ~ CODE10: 2944" 
* #265120 "MDC_DIM_CUBIC_X_M_PER_HR"
* #265120 ^definition = m3/h
* #265120 ^designation[0].language = #de-AT 
* #265120 ^designation[0].value = " L3T-1  (volume flow rate)" 
* #265120 ^property[0].code = #parent 
* #265120 ^property[0].valueCode = #_mdcunits 
* #265120 ^property[1].code = #hints 
* #265120 ^property[1].valueString = "PART: 4 ~ CODE10: 2976" 
* #265152 "MDC_DIM_CUBIC_X_M_PER_DAY"
* #265152 ^definition = m3/d
* #265152 ^designation[0].language = #de-AT 
* #265152 ^designation[0].value = " L3T-1  (volume flow rate)" 
* #265152 ^property[0].code = #parent 
* #265152 ^property[0].valueCode = #_mdcunits 
* #265152 ^property[1].code = #hints 
* #265152 ^property[1].valueString = "PART: 4 ~ CODE10: 3008" 
* #265184 "MDC_DIM_L_PER_SEC"
* #265184 ^definition = L/s
* #265184 ^designation[0].language = #de-AT 
* #265184 ^designation[0].value = " L3T-1  (volume flow rate)" 
* #265184 ^property[0].code = #parent 
* #265184 ^property[0].valueCode = #_mdcunits 
* #265184 ^property[1].code = #hints 
* #265184 ^property[1].valueString = "PART: 4 ~ CODE10: 3040" 
* #265202 "MDC_DIM_MILLI_L_PER_SEC"
* #265202 ^definition = mL/s
* #265202 ^designation[0].language = #de-AT 
* #265202 ^designation[0].value = " L3T-1  (volume flow rate)" 
* #265202 ^property[0].code = #parent 
* #265202 ^property[0].valueCode = #_mdcunits 
* #265202 ^property[1].code = #hints 
* #265202 ^property[1].valueString = "PART: 4 ~ CODE10: 3058" 
* #265216 "MDC_DIM_L_PER_MIN"
* #265216 ^definition = L/min
* #265216 ^designation[0].language = #de-AT 
* #265216 ^designation[0].value = " L3T-1  (volume flow rate)" 
* #265216 ^property[0].code = #parent 
* #265216 ^property[0].valueCode = #_mdcunits 
* #265216 ^property[1].code = #hints 
* #265216 ^property[1].valueString = "PART: 4 ~ CODE10: 3072" 
* #265234 "MDC_DIM_MILLI_L_PER_MIN"
* #265234 ^definition = mL/min
* #265234 ^designation[0].language = #de-AT 
* #265234 ^designation[0].value = " L3T-1  (volume flow rate)" 
* #265234 ^property[0].code = #parent 
* #265234 ^property[0].valueCode = #_mdcunits 
* #265234 ^property[1].code = #hints 
* #265234 ^property[1].valueString = "PART: 4 ~ CODE10: 3090" 
* #265235 "MDC_DIM_MICRO_L_PER_MIN"
* #265235 ^definition = uL/min
* #265235 ^designation[0].language = #de-AT 
* #265235 ^designation[0].value = " L3T-1  (volume flow rate)" 
* #265235 ^property[0].code = #parent 
* #265235 ^property[0].valueCode = #_mdcunits 
* #265235 ^property[1].code = #hints 
* #265235 ^property[1].valueString = "PART: 4 ~ CODE10: 3091" 
* #265248 "MDC_DIM_X_L_PER_HR"
* #265248 ^definition = L/h
* #265248 ^designation[0].language = #de-AT 
* #265248 ^designation[0].value = " L3T-1  (volume flow rate)" 
* #265248 ^property[0].code = #parent 
* #265248 ^property[0].valueCode = #_mdcunits 
* #265248 ^property[1].code = #hints 
* #265248 ^property[1].valueString = "PART: 4 ~ CODE10: 3104" 
* #265266 "MDC_DIM_MILLI_L_PER_HR"
* #265266 ^definition = mL/h
* #265266 ^designation[0].language = #de-AT 
* #265266 ^designation[0].value = " L3T-1  (volume flow rate)" 
* #265266 ^property[0].code = #parent 
* #265266 ^property[0].valueCode = #_mdcunits 
* #265266 ^property[1].code = #hints 
* #265266 ^property[1].valueString = "PART: 4 ~ CODE10: 3122" 
* #265280 "MDC_DIM_X_L_PER_DAY"
* #265280 ^definition = L/d
* #265280 ^designation[0].language = #de-AT 
* #265280 ^designation[0].value = " L3T-1  (volume flow rate)" 
* #265280 ^property[0].code = #parent 
* #265280 ^property[0].valueCode = #_mdcunits 
* #265280 ^property[1].code = #hints 
* #265280 ^property[1].valueString = "PART: 4 ~ CODE10: 3136" 
* #265312 "MDC_DIM_L_PER_KG"
* #265312 ^definition = L/kg
* #265312 ^property[0].code = #parent 
* #265312 ^property[0].valueCode = #_mdcunits 
* #265312 ^property[1].code = #hints 
* #265312 ^property[1].valueString = "PART: 4 ~ CODE10: 3168" 
* #265330 "MDC_DIM_MILLI_L_PER_KG"
* #265330 ^definition = mL/kg
* #265330 ^designation[0].language = #de-AT 
* #265330 ^designation[0].value = " L3M-1 (volume content)" 
* #265330 ^property[0].code = #parent 
* #265330 ^property[0].valueCode = #_mdcunits 
* #265330 ^property[1].code = #hints 
* #265330 ^property[1].valueString = "PART: 4 ~ CODE10: 3186" 
* #265344 "MDC_DIM_CUBIC_X_L_PER_KG"
* #265344 ^definition = m3/kg
* #265344 ^designation[0].language = #de-AT 
* #265344 ^designation[0].value = " L3M-1 (volume content)" 
* #265344 ^property[0].code = #parent 
* #265344 ^property[0].valueCode = #_mdcunits 
* #265344 ^property[1].code = #hints 
* #265344 ^property[1].valueString = "PART: 4 ~ CODE10: 3200" 
* #265376 "MDC_DIM_X_M_PER_PASCAL_SEC"
* #265376 ^definition = m/Pa/s
* #265376 ^designation[0].language = #de-AT 
* #265376 ^designation[0].value = " L2M-1T (permeability)" 
* #265376 ^property[0].code = #parent 
* #265376 ^property[0].valueCode = #_mdcunits 
* #265376 ^property[1].code = #hints 
* #265376 ^property[1].valueString = "PART: 4 ~ CODE10: 3232" 
* #265408 "MDC_DIM_X_L_PER_MIN_PER_ML_HG"
* #265408 ^definition = L/min/mm[Hg]
* #265408 ^designation[0].language = #de-AT 
* #265408 ^designation[0].value = " L2M-1T (permeability)" 
* #265408 ^property[0].code = #parent 
* #265408 ^property[0].valueCode = #_mdcunits 
* #265408 ^property[1].code = #hints 
* #265408 ^property[1].valueString = "PART: 4 ~ CODE10: 3264" 
* #265440 "MDC_DIM_X_G_PER_SEC"
* #265440 ^definition = g/s
* #265440 ^designation[0].language = #de-AT 
* #265440 ^designation[0].value = " MT-1 (mass flow rate)" 
* #265440 ^property[0].code = #parent 
* #265440 ^property[0].valueCode = #_mdcunits 
* #265440 ^property[1].code = #hints 
* #265440 ^property[1].valueString = "PART: 4 ~ CODE10: 3296" 
* #265472 "MDC_DIM_X_G_PER_MIN"
* #265472 ^definition = g/min
* #265472 ^designation[0].language = #de-AT 
* #265472 ^designation[0].value = " MT-1 (mass flow rate)" 
* #265472 ^property[0].code = #parent 
* #265472 ^property[0].valueCode = #_mdcunits 
* #265472 ^property[1].code = #hints 
* #265472 ^property[1].valueString = "PART: 4 ~ CODE10: 3328" 
* #265490 "MDC_DIM_MILLI_G_PER_MIN"
* #265490 ^definition = mg/min
* #265490 ^designation[0].language = #de-AT 
* #265490 ^designation[0].value = " MT-1 (mass flow rate)" 
* #265490 ^property[0].code = #parent 
* #265490 ^property[0].valueCode = #_mdcunits 
* #265490 ^property[1].code = #hints 
* #265490 ^property[1].valueString = "PART: 4 ~ CODE10: 3346" 
* #265491 "MDC_DIM_MICRO_G_PER_MIN"
* #265491 ^definition = ug/min
* #265491 ^designation[0].language = #de-AT 
* #265491 ^designation[0].value = " MT-1 (mass flow rate)" 
* #265491 ^property[0].code = #parent 
* #265491 ^property[0].valueCode = #_mdcunits 
* #265491 ^property[1].code = #hints 
* #265491 ^property[1].valueString = "PART: 4 ~ CODE10: 3347" 
* #265492 "MDC_DIM_NANO_G_PER_MIN"
* #265492 ^definition = ng/min
* #265492 ^designation[0].language = #de-AT 
* #265492 ^designation[0].value = " MT-1 (mass flow rate)" 
* #265492 ^property[0].code = #parent 
* #265492 ^property[0].valueCode = #_mdcunits 
* #265492 ^property[1].code = #hints 
* #265492 ^property[1].valueString = "PART: 4 ~ CODE10: 3348" 
* #265504 "MDC_DIM_X_G_PER_HR"
* #265504 ^definition = g/h
* #265504 ^designation[0].language = #de-AT 
* #265504 ^designation[0].value = " MT-1 (mass flow rate)" 
* #265504 ^property[0].code = #parent 
* #265504 ^property[0].valueCode = #_mdcunits 
* #265504 ^property[1].code = #hints 
* #265504 ^property[1].valueString = "PART: 4 ~ CODE10: 3360" 
* #265522 "MDC_DIM_MILLI_G_PER_HR"
* #265522 ^definition = mg/h
* #265522 ^designation[0].language = #de-AT 
* #265522 ^designation[0].value = " MT-1 (mass flow rate)" 
* #265522 ^property[0].code = #parent 
* #265522 ^property[0].valueCode = #_mdcunits 
* #265522 ^property[1].code = #hints 
* #265522 ^property[1].valueString = "PART: 4 ~ CODE10: 3378" 
* #265523 "MDC_DIM_MICRO_G_PER_HR"
* #265523 ^definition = ug/h
* #265523 ^designation[0].language = #de-AT 
* #265523 ^designation[0].value = " MT-1 (mass flow rate)" 
* #265523 ^property[0].code = #parent 
* #265523 ^property[0].valueCode = #_mdcunits 
* #265523 ^property[1].code = #hints 
* #265523 ^property[1].valueString = "PART: 4 ~ CODE10: 3379" 
* #265524 "MDC_DIM_NANO_G_PER_HR"
* #265524 ^definition = ng/h
* #265524 ^designation[0].language = #de-AT 
* #265524 ^designation[0].value = " MT-1 (mass flow rate)" 
* #265524 ^property[0].code = #parent 
* #265524 ^property[0].valueCode = #_mdcunits 
* #265524 ^property[1].code = #hints 
* #265524 ^property[1].valueString = "PART: 4 ~ CODE10: 3380" 
* #265536 "MDC_DIM_X_G_PER_DAY"
* #265536 ^definition = g/d
* #265536 ^designation[0].language = #de-AT 
* #265536 ^designation[0].value = " MT-1 (mass flow rate)" 
* #265536 ^property[0].code = #parent 
* #265536 ^property[0].valueCode = #_mdcunits 
* #265536 ^property[1].code = #hints 
* #265536 ^property[1].valueString = "PART: 4 ~ CODE10: 3392" 
* #265554 "MDC_DIM_MILLI_G_PER_DAY"
* #265554 ^definition = mg/d
* #265554 ^designation[0].language = #de-AT 
* #265554 ^designation[0].value = " MT-1 (mass flow rate)" 
* #265554 ^property[0].code = #parent 
* #265554 ^property[0].valueCode = #_mdcunits 
* #265554 ^property[1].code = #hints 
* #265554 ^property[1].valueString = "PART: 4 ~ CODE10: 3410" 
* #265555 "MDC_DIM_MICRO_G_PER_DAY"
* #265555 ^definition = ug/d
* #265555 ^designation[0].language = #de-AT 
* #265555 ^designation[0].value = " MT-1 (mass flow rate)" 
* #265555 ^property[0].code = #parent 
* #265555 ^property[0].valueCode = #_mdcunits 
* #265555 ^property[1].code = #hints 
* #265555 ^property[1].valueString = "PART: 4 ~ CODE10: 3411" 
* #265556 "MDC_DIM_NANO_G_PER_DAY"
* #265556 ^definition = ng/d
* #265556 ^designation[0].language = #de-AT 
* #265556 ^designation[0].value = " MT-1 (mass flow rate)" 
* #265556 ^property[0].code = #parent 
* #265556 ^property[0].valueCode = #_mdcunits 
* #265556 ^property[1].code = #hints 
* #265556 ^property[1].valueString = "PART: 4 ~ CODE10: 3412" 
* #265568 "MDC_DIM_X_G_PER_KG_PER_SEC"
* #265568 ^definition = g/kg/s
* #265568 ^designation[0].language = #de-AT 
* #265568 ^designation[0].value = " - mass dose rate per body weight" 
* #265568 ^property[0].code = #parent 
* #265568 ^property[0].valueCode = #_mdcunits 
* #265568 ^property[1].code = #hints 
* #265568 ^property[1].valueString = "PART: 4 ~ CODE10: 3424" 
* #265600 "MDC_DIM_X_G_PER_KG_PER_MIN"
* #265600 ^definition = g/kg/min
* #265600 ^designation[0].language = #de-AT 
* #265600 ^designation[0].value = " - mass dose rate per body weight" 
* #265600 ^property[0].code = #parent 
* #265600 ^property[0].valueCode = #_mdcunits 
* #265600 ^property[1].code = #hints 
* #265600 ^property[1].valueString = "PART: 4 ~ CODE10: 3456" 
* #265618 "MDC_DIM_MILLI_G_PER_KG_PER_MIN"
* #265618 ^definition = mg/kg/min
* #265618 ^designation[0].language = #de-AT 
* #265618 ^designation[0].value = " - mass dose rate per body weight" 
* #265618 ^property[0].code = #parent 
* #265618 ^property[0].valueCode = #_mdcunits 
* #265618 ^property[1].code = #hints 
* #265618 ^property[1].valueString = "PART: 4 ~ CODE10: 3474" 
* #265619 "MDC_DIM_MICRO_G_PER_KG_PER_MIN"
* #265619 ^definition = ug/kg/min
* #265619 ^designation[0].language = #de-AT 
* #265619 ^designation[0].value = " - mass dose rate per body weight" 
* #265619 ^property[0].code = #parent 
* #265619 ^property[0].valueCode = #_mdcunits 
* #265619 ^property[1].code = #hints 
* #265619 ^property[1].valueString = "PART: 4 ~ CODE10: 3475" 
* #265620 "MDC_DIM_NANO_G_PER_KG_PER_MIN"
* #265620 ^definition = ng/kg/min
* #265620 ^designation[0].language = #de-AT 
* #265620 ^designation[0].value = " - mass dose rate per body weight" 
* #265620 ^property[0].code = #parent 
* #265620 ^property[0].valueCode = #_mdcunits 
* #265620 ^property[1].code = #hints 
* #265620 ^property[1].valueString = "PART: 4 ~ CODE10: 3476" 
* #265632 "MDC_DIM_X_G_PER_KG_PER_HR"
* #265632 ^definition = g/kg/h
* #265632 ^designation[0].language = #de-AT 
* #265632 ^designation[0].value = " - mass dose rate per body weight" 
* #265632 ^property[0].code = #parent 
* #265632 ^property[0].valueCode = #_mdcunits 
* #265632 ^property[1].code = #hints 
* #265632 ^property[1].valueString = "PART: 4 ~ CODE10: 3488" 
* #265650 "MDC_DIM_MILLI_G_PER_KG_PER_HR"
* #265650 ^definition = mg/kg/h
* #265650 ^designation[0].language = #de-AT 
* #265650 ^designation[0].value = " - mass dose rate per body weight" 
* #265650 ^property[0].code = #parent 
* #265650 ^property[0].valueCode = #_mdcunits 
* #265650 ^property[1].code = #hints 
* #265650 ^property[1].valueString = "PART: 4 ~ CODE10: 3506" 
* #265651 "MDC_DIM_MICRO_G_PER_KG_PER_HR"
* #265651 ^definition = ug/kg/h
* #265651 ^designation[0].language = #de-AT 
* #265651 ^designation[0].value = " - mass dose rate per body weight" 
* #265651 ^property[0].code = #parent 
* #265651 ^property[0].valueCode = #_mdcunits 
* #265651 ^property[1].code = #hints 
* #265651 ^property[1].valueString = "PART: 4 ~ CODE10: 3507" 
* #265652 "MDC_DIM_NANO_G_PER_KG_PER_HR"
* #265652 ^definition = ng/kg/h
* #265652 ^designation[0].language = #de-AT 
* #265652 ^designation[0].value = " - mass dose rate per body weight" 
* #265652 ^property[0].code = #parent 
* #265652 ^property[0].valueCode = #_mdcunits 
* #265652 ^property[1].code = #hints 
* #265652 ^property[1].valueString = "PART: 4 ~ CODE10: 3508" 
* #265664 "MDC_DIM_X_G_PER_KG_PER_DAY"
* #265664 ^definition = g/kg/d
* #265664 ^designation[0].language = #de-AT 
* #265664 ^designation[0].value = " - mass dose rate per body weight" 
* #265664 ^property[0].code = #parent 
* #265664 ^property[0].valueCode = #_mdcunits 
* #265664 ^property[1].code = #hints 
* #265664 ^property[1].valueString = "PART: 4 ~ CODE10: 3520" 
* #265682 "MDC_DIM_MILLI_G_PER_KG_PER_DAY"
* #265682 ^definition = mg/kg/d
* #265682 ^designation[0].language = #de-AT 
* #265682 ^designation[0].value = " - mass dose rate per body weight" 
* #265682 ^property[0].code = #parent 
* #265682 ^property[0].valueCode = #_mdcunits 
* #265682 ^property[1].code = #hints 
* #265682 ^property[1].valueString = "PART: 4 ~ CODE10: 3538" 
* #265683 "MDC_DIM_MICRO_G_PER_KG_PER_DAY"
* #265683 ^definition = ug/kg/d
* #265683 ^designation[0].language = #de-AT 
* #265683 ^designation[0].value = " - mass dose rate per body weight" 
* #265683 ^property[0].code = #parent 
* #265683 ^property[0].valueCode = #_mdcunits 
* #265683 ^property[1].code = #hints 
* #265683 ^property[1].valueString = "PART: 4 ~ CODE10: 3539" 
* #265684 "MDC_DIM_NANO_G_PER_KG_PER_DAY"
* #265684 ^definition = ng/kg/d
* #265684 ^designation[0].language = #de-AT 
* #265684 ^designation[0].value = " - mass dose rate per body weight" 
* #265684 ^property[0].code = #parent 
* #265684 ^property[0].valueCode = #_mdcunits 
* #265684 ^property[1].code = #hints 
* #265684 ^property[1].valueString = "PART: 4 ~ CODE10: 3540" 
* #265696 "MDC_DIM_X_G_PER_L_PER_SEC"
* #265696 ^definition = g/L/s
* #265696 ^designation[0].language = #de-AT 
* #265696 ^designation[0].value = " L-3MT-1 (mass concentration rate)" 
* #265696 ^property[0].code = #parent 
* #265696 ^property[0].valueCode = #_mdcunits 
* #265696 ^property[1].code = #hints 
* #265696 ^property[1].valueString = "PART: 4 ~ CODE10: 3552" 
* #265728 "MDC_DIM_X_G_PER_L_PER_MIN"
* #265728 ^definition = g/L/min
* #265728 ^designation[0].language = #de-AT 
* #265728 ^designation[0].value = " L-3MT-1 (mass concentration rate)" 
* #265728 ^property[0].code = #parent 
* #265728 ^property[0].valueCode = #_mdcunits 
* #265728 ^property[1].code = #hints 
* #265728 ^property[1].valueString = "PART: 4 ~ CODE10: 3584" 
* #265760 "MDC_DIM_X_G_PER_L_PER_HR"
* #265760 ^definition = g/L/h
* #265760 ^designation[0].language = #de-AT 
* #265760 ^designation[0].value = " L-3MT-1 (mass concentration rate)" 
* #265760 ^property[0].code = #parent 
* #265760 ^property[0].valueCode = #_mdcunits 
* #265760 ^property[1].code = #hints 
* #265760 ^property[1].valueString = "PART: 4 ~ CODE10: 3616" 
* #265792 "MDC_DIM_X_G_PER_L_PER_DAY"
* #265792 ^definition = g/L/d
* #265792 ^designation[0].language = #de-AT 
* #265792 ^designation[0].value = " L-3MT-1 (mass concentration rate)" 
* #265792 ^property[0].code = #parent 
* #265792 ^property[0].valueCode = #_mdcunits 
* #265792 ^property[1].code = #hints 
* #265792 ^property[1].valueString = "PART: 4 ~ CODE10: 3648" 
* #265824 "MDC_DIM_X_G_PER_M_PER_SEC"
* #265824 ^definition = g/m/s
* #265824 ^designation[0].language = #de-AT 
* #265824 ^designation[0].value = " L-1MT-1 (dynamic viscosity)" 
* #265824 ^property[0].code = #parent 
* #265824 ^property[0].valueCode = #_mdcunits 
* #265824 ^property[1].code = #hints 
* #265824 ^property[1].valueString = "PART: 4 ~ CODE10: 3680" 
* #265856 "MDC_DIM_X_G_M_PER_SEC"
* #265856 ^definition = g.m/s
* #265856 ^designation[0].language = #de-AT 
* #265856 ^designation[0].value = " LMT-1 (momentum, impulse)" 
* #265856 ^property[0].code = #parent 
* #265856 ^property[0].valueCode = #_mdcunits 
* #265856 ^property[1].code = #hints 
* #265856 ^property[1].valueString = "PART: 4 ~ CODE10: 3712" 
* #265888 "MDC_DIM_X_NEWTON_SEC"
* #265888 ^definition = N.s
* #265888 ^designation[0].language = #de-AT 
* #265888 ^designation[0].value = " LMT-1 (momentum, impulse)" 
* #265888 ^property[0].code = #parent 
* #265888 ^property[0].valueCode = #_mdcunits 
* #265888 ^property[1].code = #hints 
* #265888 ^property[1].valueString = "PART: 4 ~ CODE10: 3744" 
* #265920 "MDC_DIM_X_NEWTON"
* #265920 ^definition = N
* #265920 ^designation[0].language = #de-AT 
* #265920 ^designation[0].value = " LMT-2 (force)" 
* #265920 ^property[0].code = #parent 
* #265920 ^property[0].valueCode = #_mdcunits 
* #265920 ^property[1].code = #hints 
* #265920 ^property[1].valueString = "PART: 4 ~ CODE10: 3776" 
* #265952 "MDC_DIM_X_DYNE"
* #265952 ^definition = dyn
* #265952 ^designation[0].language = #de-AT 
* #265952 ^designation[0].value = " LMT-2 (force)" 
* #265952 ^property[0].code = #parent 
* #265952 ^property[0].valueCode = #_mdcunits 
* #265952 ^property[1].code = #hints 
* #265952 ^property[1].valueString = "PART: 4 ~ CODE10: 3808" 
* #265984 "MDC_DIM_X_PASCAL"
* #265984 ^definition = Pa
* #265984 ^designation[0].language = #de-AT 
* #265984 ^designation[0].value = " L-1MT-2 (pressure)" 
* #265984 ^property[0].code = #parent 
* #265984 ^property[0].valueCode = #_mdcunits 
* #265984 ^property[1].code = #hints 
* #265984 ^property[1].valueString = "PART: 4 ~ CODE10: 3840" 
* #265986 "MDC_DIM_HECTO_PASCAL"
* #265986 ^definition = hPa
* #265986 ^designation[0].language = #de-AT 
* #265986 ^designation[0].value = " L-1MT-2 (pressure)" 
* #265986 ^property[0].code = #parent 
* #265986 ^property[0].valueCode = #_mdcunits 
* #265986 ^property[1].code = #hints 
* #265986 ^property[1].valueString = "PART: 4 ~ CODE10: 3842" 
* #265987 "MDC_DIM_KILO_PASCAL"
* #265987 ^definition = kPa
* #265987 ^designation[0].language = #de-AT 
* #265987 ^designation[0].value = " L-1MT-2 (pressure)" 
* #265987 ^property[0].code = #parent 
* #265987 ^property[0].valueCode = #_mdcunits 
* #265987 ^property[1].code = #hints 
* #265987 ^property[1].valueString = "PART: 4 ~ CODE10: 3843" 
* #266016 "MDC_DIM_MMHG"
* #266016 ^definition = mm[Hg]
* #266016 ^designation[0].language = #de-AT 
* #266016 ^designation[0].value = " L-1MT-2 (pressure)" 
* #266016 ^property[0].code = #parent 
* #266016 ^property[0].valueCode = #_mdcunits 
* #266016 ^property[1].code = #hints 
* #266016 ^property[1].valueString = "PART: 4 ~ CODE10: 3872" 
* #266048 "MDC_DIM_CM_H2O"
* #266048 ^definition = cm[H2O]
* #266048 ^designation[0].language = #de-AT 
* #266048 ^designation[0].value = " L-1MT-2 (pressure)" 
* #266048 ^property[0].code = #parent 
* #266048 ^property[0].valueCode = #_mdcunits 
* #266048 ^property[1].code = #hints 
* #266048 ^property[1].valueString = "PART: 4 ~ CODE10: 3904" 
* #266080 "MDC_DIM_X_BAR"
* #266080 ^definition = bar
* #266080 ^designation[0].language = #de-AT 
* #266080 ^designation[0].value = " L-1MT-2 (pressure)" 
* #266080 ^property[0].code = #parent 
* #266080 ^property[0].valueCode = #_mdcunits 
* #266080 ^property[1].code = #hints 
* #266080 ^property[1].valueString = "PART: 4 ~ CODE10: 3936" 
* #266098 "MDC_DIM_MILLI_BAR"
* #266098 ^definition = mbar
* #266098 ^property[0].code = #parent 
* #266098 ^property[0].valueCode = #_mdcunits 
* #266098 ^property[1].code = #hints 
* #266098 ^property[1].valueString = "PART: 4 ~ CODE10: 3954" 
* #266112 "MDC_DIM_X_JOULES"
* #266112 ^definition = J
* #266112 ^designation[0].language = #de-AT 
* #266112 ^designation[0].value = " L2MT-2 (energy; work)" 
* #266112 ^property[0].code = #parent 
* #266112 ^property[0].valueCode = #_mdcunits 
* #266112 ^property[1].code = #hints 
* #266112 ^property[1].valueString = "PART: 4 ~ CODE10: 3968" 
* #266144 "MDC_DIM_EVOLT"
* #266144 ^definition = eV
* #266144 ^designation[0].language = #de-AT 
* #266144 ^designation[0].value = " L2MT-2 (energy; work)" 
* #266144 ^property[0].code = #parent 
* #266144 ^property[0].valueCode = #_mdcunits 
* #266144 ^property[1].code = #hints 
* #266144 ^property[1].valueString = "PART: 4 ~ CODE10: 4000" 
* #266176 "MDC_DIM_WATT"
* #266176 ^definition = W
* #266176 ^designation[0].language = #de-AT 
* #266176 ^designation[0].value = " L2MT-3 (power)" 
* #266176 ^property[0].code = #parent 
* #266176 ^property[0].valueCode = #_mdcunits 
* #266176 ^property[1].code = #hints 
* #266176 ^property[1].valueString = "PART: 4 ~ CODE10: 4032" 
* #266194 "MDC_DIM_MILLI_WATT"
* #266194 ^definition = mW
* #266194 ^designation[0].language = #de-AT 
* #266194 ^designation[0].value = " L2MT-3 (power)" 
* #266194 ^property[0].code = #parent 
* #266194 ^property[0].valueCode = #_mdcunits 
* #266194 ^property[1].code = #hints 
* #266194 ^property[1].valueString = "PART: 4 ~ CODE10: 4050" 
* #266195 "MDC_DIM_MICRO_WATT"
* #266195 ^definition = uW
* #266195 ^designation[0].language = #de-AT 
* #266195 ^designation[0].value = " L2MT-3 (power)" 
* #266195 ^property[0].code = #parent 
* #266195 ^property[0].valueCode = #_mdcunits 
* #266195 ^property[1].code = #hints 
* #266195 ^property[1].valueString = "PART: 4 ~ CODE10: 4051" 
* #266196 "MDC_DIM_NANO_WATT"
* #266196 ^definition = nW
* #266196 ^designation[0].language = #de-AT 
* #266196 ^designation[0].value = " L2MT-3 (power)" 
* #266196 ^property[0].code = #parent 
* #266196 ^property[0].valueCode = #_mdcunits 
* #266196 ^property[1].code = #hints 
* #266196 ^property[1].valueString = "PART: 4 ~ CODE10: 4052" 
* #266197 "MDC_DIM_PICO_WATT"
* #266197 ^definition = pW
* #266197 ^property[0].code = #parent 
* #266197 ^property[0].valueCode = #_mdcunits 
* #266197 ^property[1].code = #hints 
* #266197 ^property[1].valueString = "PART: 4 ~ CODE10: 4053" 
* #266208 "MDC_DIM_X_PASCAL_SEC_PER_M_CUBE"
* #266208 ^definition = Pa.s/m3
* #266208 ^designation[0].language = #de-AT 
* #266208 ^designation[0].value = " L-4MT-1 (hydraulic impedance)" 
* #266208 ^property[0].code = #parent 
* #266208 ^property[0].valueCode = #_mdcunits 
* #266208 ^property[1].code = #hints 
* #266208 ^property[1].valueString = "PART: 4 ~ CODE10: 4064" 
* #266240 "MDC_DIM_X_PASCAL_SEC_PER_L"
* #266240 ^definition = Pa.s/L
* #266240 ^designation[0].language = #de-AT 
* #266240 ^designation[0].value = " L-4MT-1 (hydraulic impedance)" 
* #266240 ^property[0].code = #parent 
* #266240 ^property[0].valueCode = #_mdcunits 
* #266240 ^property[1].code = #hints 
* #266240 ^property[1].valueString = "PART: 4 ~ CODE10: 4096" 
* #266242 "MDC_DIM_HECTO_PASCAL_SEC_PER_L"
* #266242 ^definition = hPa.s/L hPa/(L/s)
* #266242 ^property[0].code = #parent 
* #266242 ^property[0].valueCode = #_mdcunits 
* #266242 ^property[1].code = #hints 
* #266242 ^property[1].valueString = "PART: 4 ~ CODE10: 4098" 
* #266304 "MDC_DIM_X_AMPS"
* #266304 ^definition = a
* #266304 ^designation[0].language = #de-AT 
* #266304 ^designation[0].value = " I (electrical current)" 
* #266304 ^property[0].code = #parent 
* #266304 ^property[0].valueCode = #_mdcunits 
* #266304 ^property[1].code = #hints 
* #266304 ^property[1].valueString = "PART: 4 ~ CODE10: 4160" 
* #266336 "MDC_DIM_X_COULOMB"
* #266336 ^definition = C
* #266336 ^designation[0].language = #de-AT 
* #266336 ^designation[0].value = " IT (electrical charge)" 
* #266336 ^property[0].code = #parent 
* #266336 ^property[0].valueCode = #_mdcunits 
* #266336 ^property[1].code = #hints 
* #266336 ^property[1].valueString = "PART: 4 ~ CODE10: 4192" 
* #266368 "MDC_DIM_X_AMPS_PER_M"
* #266368 ^definition = A/m
* #266368 ^designation[0].language = #de-AT 
* #266368 ^designation[0].value = " IL-1 (magnetic field strength)" 
* #266368 ^property[0].code = #parent 
* #266368 ^property[0].valueCode = #_mdcunits 
* #266368 ^property[1].code = #hints 
* #266368 ^property[1].valueString = "PART: 4 ~ CODE10: 4224" 
* #266400 "MDC_DIM_X_VOLT"
* #266400 ^definition = V
* #266400 ^designation[0].language = #de-AT 
* #266400 ^designation[0].value = " ML2I-1T-3 (electric potential)" 
* #266400 ^property[0].code = #parent 
* #266400 ^property[0].valueCode = #_mdcunits 
* #266400 ^property[1].code = #hints 
* #266400 ^property[1].valueString = "PART: 4 ~ CODE10: 4256" 
* #266418 "MDC_DIM_MILLI_VOLT"
* #266418 ^definition = mV
* #266418 ^designation[0].language = #de-AT 
* #266418 ^designation[0].value = " ML2I-1T-3 (electric potential)" 
* #266418 ^property[0].code = #parent 
* #266418 ^property[0].valueCode = #_mdcunits 
* #266418 ^property[1].code = #hints 
* #266418 ^property[1].valueString = "PART: 4 ~ CODE10: 4274" 
* #266419 "MDC_DIM_MICRO_VOLT"
* #266419 ^definition = uV
* #266419 ^designation[0].language = #de-AT 
* #266419 ^designation[0].value = " ML2I-1T-3 (electric potential)" 
* #266419 ^property[0].code = #parent 
* #266419 ^property[0].valueCode = #_mdcunits 
* #266419 ^property[1].code = #hints 
* #266419 ^property[1].valueString = "PART: 4 ~ CODE10: 4275" 
* #266420 "MDC_DIM_NANO_VOLT"
* #266420 ^definition = nV
* #266420 ^designation[0].language = #de-AT 
* #266420 ^designation[0].value = " ML2I-1T-3 (electric potential)" 
* #266420 ^property[0].code = #parent 
* #266420 ^property[0].valueCode = #_mdcunits 
* #266420 ^property[1].code = #hints 
* #266420 ^property[1].valueString = "PART: 4 ~ CODE10: 4276" 
* #266432 "MDC_DIM_X_OHM"
* #266432 ^definition = Ohm
* #266432 ^designation[0].language = #de-AT 
* #266432 ^designation[0].value = " ML2I-2T-3 (electric resistance)" 
* #266432 ^property[0].code = #parent 
* #266432 ^property[0].valueCode = #_mdcunits 
* #266432 ^property[1].code = #hints 
* #266432 ^property[1].valueString = "PART: 4 ~ CODE10: 4288" 
* #266450 "MDC_DIM_MILLI_OHM"
* #266450 ^definition = mOhm
* #266450 ^designation[0].language = #de-AT 
* #266450 ^designation[0].value = " ML2I-2T-3 (electric resistance)" 
* #266450 ^property[0].code = #parent 
* #266450 ^property[0].valueCode = #_mdcunits 
* #266450 ^property[1].code = #hints 
* #266450 ^property[1].valueString = "PART: 4 ~ CODE10: 4306" 
* #266464 "MDC_DIM_X_OHM_M"
* #266464 ^definition = Ohm.m
* #266464 ^designation[0].language = #de-AT 
* #266464 ^designation[0].value = " ML3I-2T-3 (electrical resistivity)" 
* #266464 ^property[0].code = #parent 
* #266464 ^property[0].valueCode = #_mdcunits 
* #266464 ^property[1].code = #hints 
* #266464 ^property[1].valueString = "PART: 4 ~ CODE10: 4320" 
* #266496 "MDC_DIM_X_FARAD"
* #266496 ^definition = F
* #266496 ^designation[0].language = #de-AT 
* #266496 ^designation[0].value = " I2T4M-1L-2 (electrical capacitance)" 
* #266496 ^property[0].code = #parent 
* #266496 ^property[0].valueCode = #_mdcunits 
* #266496 ^property[1].code = #hints 
* #266496 ^property[1].valueString = "PART: 4 ~ CODE10: 4352" 
* #266528 "MDC_DIM_KELVIN"
* #266528 ^definition = K
* #266528 ^designation[0].language = #de-AT 
* #266528 ^designation[0].value = " Q (temperature)" 
* #266528 ^property[0].code = #parent 
* #266528 ^property[0].valueCode = #_mdcunits 
* #266528 ^property[1].code = #hints 
* #266528 ^property[1].valueString = "PART: 4 ~ CODE10: 4384" 
* #266560 "MDC_DIM_FAHR"
* #266560 ^definition = [degF]
* #266560 ^designation[0].language = #de-AT 
* #266560 ^designation[0].value = " Q (temperature)" 
* #266560 ^property[0].code = #parent 
* #266560 ^property[0].valueCode = #_mdcunits 
* #266560 ^property[1].code = #hints 
* #266560 ^property[1].valueString = "PART: 4 ~ CODE10: 4416" 
* #266592 "MDC_DIM_KELVIN_PER_X_WATT"
* #266592 ^definition = K/W
* #266592 ^designation[0].language = #de-AT 
* #266592 ^designation[0].value = " QT3M-1L-2 (thermal resistance)" 
* #266592 ^property[0].code = #parent 
* #266592 ^property[0].valueCode = #_mdcunits 
* #266592 ^property[1].code = #hints 
* #266592 ^property[1].valueString = "PART: 4 ~ CODE10: 4448" 
* #266624 "MDC_DIM_X_CANDELA"
* #266624 ^definition = cd
* #266624 ^designation[0].language = #de-AT 
* #266624 ^designation[0].value = " J (luminous intensity)" 
* #266624 ^property[0].code = #parent 
* #266624 ^property[0].valueCode = #_mdcunits 
* #266624 ^property[1].code = #hints 
* #266624 ^property[1].valueString = "PART: 4 ~ CODE10: 4480" 
* #266656 "MDC_DIM_X_OSM"
* #266656 ^definition = osm
* #266656 ^designation[0].language = #de-AT 
* #266656 ^designation[0].value = " N (amount of substance)" 
* #266656 ^property[0].code = #parent 
* #266656 ^property[0].valueCode = #_mdcunits 
* #266656 ^property[1].code = #hints 
* #266656 ^property[1].valueString = "PART: 4 ~ CODE10: 4512" 
* #266688 "MDC_DIM_X_MOLE"
* #266688 ^definition = mol
* #266688 ^designation[0].language = #de-AT 
* #266688 ^designation[0].value = " N (amount of substance)" 
* #266688 ^property[0].code = #parent 
* #266688 ^property[0].valueCode = #_mdcunits 
* #266688 ^property[1].code = #hints 
* #266688 ^property[1].valueString = "PART: 4 ~ CODE10: 4544" 
* #266706 "MDC_DIM_MILLI_MOLE"
* #266706 ^definition = mmol
* #266706 ^designation[0].language = #de-AT 
* #266706 ^designation[0].value = " N (amount of substance)" 
* #266706 ^property[0].code = #parent 
* #266706 ^property[0].valueCode = #_mdcunits 
* #266706 ^property[1].code = #hints 
* #266706 ^property[1].valueString = "PART: 4 ~ CODE10: 4562" 
* #266720 "MDC_DIM_X_EQUIV"
* #266720 ^definition = eq
* #266720 ^designation[0].language = #de-AT 
* #266720 ^designation[0].value = " N (amount of substance)" 
* #266720 ^property[0].code = #parent 
* #266720 ^property[0].valueCode = #_mdcunits 
* #266720 ^property[1].code = #hints 
* #266720 ^property[1].valueString = "PART: 4 ~ CODE10: 4576" 
* #266738 "MDC_DIM_MILLI_EQUIV"
* #266738 ^definition = meq
* #266738 ^designation[0].language = #de-AT 
* #266738 ^designation[0].value = " N (amount of substance)" 
* #266738 ^property[0].code = #parent 
* #266738 ^property[0].valueCode = #_mdcunits 
* #266738 ^property[1].code = #hints 
* #266738 ^property[1].valueString = "PART: 4 ~ CODE10: 4594" 
* #266752 "MDC_DIM_X_OSM_PER_L"
* #266752 ^definition = osm/L
* #266752 ^designation[0].language = #de-AT 
* #266752 ^designation[0].value = " NL-3 (concentration)" 
* #266752 ^property[0].code = #parent 
* #266752 ^property[0].valueCode = #_mdcunits 
* #266752 ^property[1].code = #hints 
* #266752 ^property[1].valueString = "PART: 4 ~ CODE10: 4608" 
* #266784 "MDC_DIM_X_MOLE_PER_CM_CUBE"
* #266784 ^definition = mol/cm3
* #266784 ^designation[0].language = #de-AT 
* #266784 ^designation[0].value = " NL-3 (concentration)" 
* #266784 ^property[0].code = #parent 
* #266784 ^property[0].valueCode = #_mdcunits 
* #266784 ^property[1].code = #hints 
* #266784 ^property[1].valueString = "PART: 4 ~ CODE10: 4640" 
* #266816 "MDC_DIM_X_MOLE_PER_M_CUBE"
* #266816 ^definition = mol/m3
* #266816 ^designation[0].language = #de-AT 
* #266816 ^designation[0].value = " NL-3 (concentration)" 
* #266816 ^property[0].code = #parent 
* #266816 ^property[0].valueCode = #_mdcunits 
* #266816 ^property[1].code = #hints 
* #266816 ^property[1].valueString = "PART: 4 ~ CODE10: 4672" 
* #266848 "MDC_DIM_X_MOLE_PER_L"
* #266848 ^definition = mol/L
* #266848 ^designation[0].language = #de-AT 
* #266848 ^designation[0].value = " NL-3 (concentration)" 
* #266848 ^property[0].code = #parent 
* #266848 ^property[0].valueCode = #_mdcunits 
* #266848 ^property[1].code = #hints 
* #266848 ^property[1].valueString = "PART: 4 ~ CODE10: 4704" 
* #266866 "MDC_DIM_MILLI_MOLE_PER_L"
* #266866 ^definition = mmol/L
* #266866 ^designation[0].language = #de-AT 
* #266866 ^designation[0].value = " NL-3 (concentration)" 
* #266866 ^property[0].code = #parent 
* #266866 ^property[0].valueCode = #_mdcunits 
* #266866 ^property[1].code = #hints 
* #266866 ^property[1].valueString = "PART: 4 ~ CODE10: 4722" 
* #266880 "MDC_DIM_X_MOLE_PER_ML"
* #266880 ^definition = mol/mL
* #266880 ^designation[0].language = #de-AT 
* #266880 ^designation[0].value = " NL-3 (concentration)" 
* #266880 ^property[0].code = #parent 
* #266880 ^property[0].valueCode = #_mdcunits 
* #266880 ^property[1].code = #hints 
* #266880 ^property[1].valueString = "PART: 4 ~ CODE10: 4736" 
* #266898 "MDC_DIM_MILLI_MOLE_PER_ML"
* #266898 ^definition = mmol/mL
* #266898 ^designation[0].language = #de-AT 
* #266898 ^designation[0].value = " NL-3 (concentration)" 
* #266898 ^property[0].code = #parent 
* #266898 ^property[0].valueCode = #_mdcunits 
* #266898 ^property[1].code = #hints 
* #266898 ^property[1].valueString = "PART: 4 ~ CODE10: 4754" 
* #266912 "MDC_DIM_X_EQUIV_PER_CM_CUBE"
* #266912 ^definition = mol/cm3
* #266912 ^designation[0].language = #de-AT 
* #266912 ^designation[0].value = " NL-3 (concentration)" 
* #266912 ^property[0].code = #parent 
* #266912 ^property[0].valueCode = #_mdcunits 
* #266912 ^property[1].code = #hints 
* #266912 ^property[1].valueString = "PART: 4 ~ CODE10: 4768" 
* #266944 "MDC_DIM_X_EQUIV_PER_M_CUBE"
* #266944 ^definition = eq/m3
* #266944 ^designation[0].language = #de-AT 
* #266944 ^designation[0].value = " NL-3 (concentration)" 
* #266944 ^property[0].code = #parent 
* #266944 ^property[0].valueCode = #_mdcunits 
* #266944 ^property[1].code = #hints 
* #266944 ^property[1].valueString = "PART: 4 ~ CODE10: 4800" 
* #266976 "MDC_DIM_X_EQUIV_PER_L"
* #266976 ^definition = eq/L
* #266976 ^designation[0].language = #de-AT 
* #266976 ^designation[0].value = " NL-3 (concentration)" 
* #266976 ^property[0].code = #parent 
* #266976 ^property[0].valueCode = #_mdcunits 
* #266976 ^property[1].code = #hints 
* #266976 ^property[1].valueString = "PART: 4 ~ CODE10: 4832" 
* #266994 "MDC_DIM_MILLI_EQUIV_PER_L"
* #266994 ^definition = meq/L
* #266994 ^designation[0].language = #de-AT 
* #266994 ^designation[0].value = " NL-3 (concentration)" 
* #266994 ^property[0].code = #parent 
* #266994 ^property[0].valueCode = #_mdcunits 
* #266994 ^property[1].code = #hints 
* #266994 ^property[1].valueString = "PART: 4 ~ CODE10: 4850" 
* #267008 "MDC_DIM_X_EQUIV_PER_ML"
* #267008 ^definition = eq/mL
* #267008 ^designation[0].language = #de-AT 
* #267008 ^designation[0].value = " NL-3 (concentration)" 
* #267008 ^property[0].code = #parent 
* #267008 ^property[0].valueCode = #_mdcunits 
* #267008 ^property[1].code = #hints 
* #267008 ^property[1].valueString = "PART: 4 ~ CODE10: 4864" 
* #267026 "MDC_DIM_MILLI_EQUIV_PER_ML"
* #267026 ^definition = meq/mL
* #267026 ^designation[0].language = #de-AT 
* #267026 ^designation[0].value = " NL-3 (concentration)" 
* #267026 ^property[0].code = #parent 
* #267026 ^property[0].valueCode = #_mdcunits 
* #267026 ^property[1].code = #hints 
* #267026 ^property[1].valueString = "PART: 4 ~ CODE10: 4882" 
* #267040 "MDC_DIM_X_OSM_PER_KG"
* #267040 ^definition = osm/kg
* #267040 ^designation[0].language = #de-AT 
* #267040 ^designation[0].value = " NM-1 (substance content)" 
* #267040 ^property[0].code = #parent 
* #267040 ^property[0].valueCode = #_mdcunits 
* #267040 ^property[1].code = #hints 
* #267040 ^property[1].valueString = "PART: 4 ~ CODE10: 4896" 
* #267072 "MDC_DIM_X_MOLE_PER_KG"
* #267072 ^definition = mol/kg
* #267072 ^designation[0].language = #de-AT 
* #267072 ^designation[0].value = " NM-1 (substance content)" 
* #267072 ^property[0].code = #parent 
* #267072 ^property[0].valueCode = #_mdcunits 
* #267072 ^property[1].code = #hints 
* #267072 ^property[1].valueString = "PART: 4 ~ CODE10: 4928" 
* #267090 "MDC_DIM_MILLI_MOLE_PER_KG"
* #267090 ^definition = mmol/kg
* #267090 ^designation[0].language = #de-AT 
* #267090 ^designation[0].value = " NM-1 (substance content)" 
* #267090 ^property[0].code = #parent 
* #267090 ^property[0].valueCode = #_mdcunits 
* #267090 ^property[1].code = #hints 
* #267090 ^property[1].valueString = "PART: 4 ~ CODE10: 4946" 
* #267104 "MDC_DIM_X_MOLE_PER_SEC"
* #267104 ^definition = mol/s
* #267104 ^designation[0].language = #de-AT 
* #267104 ^designation[0].value = " - mol dose rate" 
* #267104 ^property[0].code = #parent 
* #267104 ^property[0].valueCode = #_mdcunits 
* #267104 ^property[1].code = #hints 
* #267104 ^property[1].valueString = "PART: 4 ~ CODE10: 4960" 
* #267136 "MDC_DIM_X_MOLE_PER_MIN"
* #267136 ^definition = mol/min
* #267136 ^designation[0].language = #de-AT 
* #267136 ^designation[0].value = " - mol dose rate" 
* #267136 ^property[0].code = #parent 
* #267136 ^property[0].valueCode = #_mdcunits 
* #267136 ^property[1].code = #hints 
* #267136 ^property[1].valueString = "PART: 4 ~ CODE10: 4992" 
* #267154 "MDC_DIM_MILLI_MOLE_PER_MIN"
* #267154 ^definition = mmol/min
* #267154 ^designation[0].language = #de-AT 
* #267154 ^designation[0].value = " - mol dose rate" 
* #267154 ^property[0].code = #parent 
* #267154 ^property[0].valueCode = #_mdcunits 
* #267154 ^property[1].code = #hints 
* #267154 ^property[1].valueString = "PART: 4 ~ CODE10: 5010" 
* #267168 "MDC_DIM_X_MOLE_PER_HR"
* #267168 ^definition = mol/h
* #267168 ^designation[0].language = #de-AT 
* #267168 ^designation[0].value = " - mol dose rate" 
* #267168 ^property[0].code = #parent 
* #267168 ^property[0].valueCode = #_mdcunits 
* #267168 ^property[1].code = #hints 
* #267168 ^property[1].valueString = "PART: 4 ~ CODE10: 5024" 
* #267186 "MDC_DIM_MILLI_MOLE_PER_HR"
* #267186 ^definition = mmol/h
* #267186 ^designation[0].language = #de-AT 
* #267186 ^designation[0].value = " - mol dose rate" 
* #267186 ^property[0].code = #parent 
* #267186 ^property[0].valueCode = #_mdcunits 
* #267186 ^property[1].code = #hints 
* #267186 ^property[1].valueString = "PART: 4 ~ CODE10: 5042" 
* #267200 "MDC_DIM_X_MOLE_PER_DAY"
* #267200 ^definition = mol/d
* #267200 ^designation[0].language = #de-AT 
* #267200 ^designation[0].value = " - mol dose rate" 
* #267200 ^property[0].code = #parent 
* #267200 ^property[0].valueCode = #_mdcunits 
* #267200 ^property[1].code = #hints 
* #267200 ^property[1].valueString = "PART: 4 ~ CODE10: 5056" 
* #267218 "MDC_DIM_MILLI_MOLE_PER_DAY"
* #267218 ^definition = mmol/d
* #267218 ^designation[0].language = #de-AT 
* #267218 ^designation[0].value = " - mol dose rate" 
* #267218 ^property[0].code = #parent 
* #267218 ^property[0].valueCode = #_mdcunits 
* #267218 ^property[1].code = #hints 
* #267218 ^property[1].valueString = "PART: 4 ~ CODE10: 5074" 
* #267232 "MDC_DIM_X_EQUIV_PER_SEC"
* #267232 ^definition = eq/s
* #267232 ^designation[0].language = #de-AT 
* #267232 ^designation[0].value = " - eq dose rate" 
* #267232 ^property[0].code = #parent 
* #267232 ^property[0].valueCode = #_mdcunits 
* #267232 ^property[1].code = #hints 
* #267232 ^property[1].valueString = "PART: 4 ~ CODE10: 5088" 
* #267264 "MDC_DIM_X_EQUIV_PER_MIN"
* #267264 ^definition = eq/min
* #267264 ^designation[0].language = #de-AT 
* #267264 ^designation[0].value = " - eq dose rate" 
* #267264 ^property[0].code = #parent 
* #267264 ^property[0].valueCode = #_mdcunits 
* #267264 ^property[1].code = #hints 
* #267264 ^property[1].valueString = "PART: 4 ~ CODE10: 5120" 
* #267282 "MDC_DIM_MILLI_EQUIV_PER_MIN"
* #267282 ^definition = meq/min
* #267282 ^designation[0].language = #de-AT 
* #267282 ^designation[0].value = " - eq dose rate" 
* #267282 ^property[0].code = #parent 
* #267282 ^property[0].valueCode = #_mdcunits 
* #267282 ^property[1].code = #hints 
* #267282 ^property[1].valueString = "PART: 4 ~ CODE10: 5138" 
* #267296 "MDC_DIM_X_EQUIV_PER_HR"
* #267296 ^definition = eq/h
* #267296 ^designation[0].language = #de-AT 
* #267296 ^designation[0].value = " - eq dose rate" 
* #267296 ^property[0].code = #parent 
* #267296 ^property[0].valueCode = #_mdcunits 
* #267296 ^property[1].code = #hints 
* #267296 ^property[1].valueString = "PART: 4 ~ CODE10: 5152" 
* #267314 "MDC_DIM_MILLI_EQUIV_PER_HR"
* #267314 ^definition = meq/h
* #267314 ^designation[0].language = #de-AT 
* #267314 ^designation[0].value = " - eq dose rate" 
* #267314 ^property[0].code = #parent 
* #267314 ^property[0].valueCode = #_mdcunits 
* #267314 ^property[1].code = #hints 
* #267314 ^property[1].valueString = "PART: 4 ~ CODE10: 5170" 
* #267328 "MDC_DIM_X_EQUIV_PER_DAY"
* #267328 ^definition = eq/d
* #267328 ^designation[0].language = #de-AT 
* #267328 ^designation[0].value = " - eq dose rate" 
* #267328 ^property[0].code = #parent 
* #267328 ^property[0].valueCode = #_mdcunits 
* #267328 ^property[1].code = #hints 
* #267328 ^property[1].valueString = "PART: 4 ~ CODE10: 5184" 
* #267346 "MDC_DIM_MILLI_EQUIV_PER_DAY"
* #267346 ^definition = meq/d
* #267346 ^designation[0].language = #de-AT 
* #267346 ^designation[0].value = " - eq dose rate" 
* #267346 ^property[0].code = #parent 
* #267346 ^property[0].valueCode = #_mdcunits 
* #267346 ^property[1].code = #hints 
* #267346 ^property[1].valueString = "PART: 4 ~ CODE10: 5202" 
* #267360 "MDC_DIM_X_MOLE_PER_KG_PER_SEC"
* #267360 ^definition = mol/kg/s
* #267360 ^designation[0].language = #de-AT 
* #267360 ^designation[0].value = " - mol dose rate per body weight" 
* #267360 ^property[0].code = #parent 
* #267360 ^property[0].valueCode = #_mdcunits 
* #267360 ^property[1].code = #hints 
* #267360 ^property[1].valueString = "PART: 4 ~ CODE10: 5216" 
* #267392 "MDC_DIM_X_MOLE_PER_KG_PER_MIN"
* #267392 ^definition = mol/kg/min
* #267392 ^designation[0].language = #de-AT 
* #267392 ^designation[0].value = " - mol dose rate per body weight" 
* #267392 ^property[0].code = #parent 
* #267392 ^property[0].valueCode = #_mdcunits 
* #267392 ^property[1].code = #hints 
* #267392 ^property[1].valueString = "PART: 4 ~ CODE10: 5248" 
* #267410 "MDC_DIM_MILLI_MOLE_PER_KG_PER_MIN"
* #267410 ^definition = mmol/kg/min
* #267410 ^designation[0].language = #de-AT 
* #267410 ^designation[0].value = " - mol dose rate per body weight" 
* #267410 ^property[0].code = #parent 
* #267410 ^property[0].valueCode = #_mdcunits 
* #267410 ^property[1].code = #hints 
* #267410 ^property[1].valueString = "PART: 4 ~ CODE10: 5266" 
* #267424 "MDC_DIM_X_MOLE_PER_KG_PER_HR"
* #267424 ^definition = mol/kg/h
* #267424 ^designation[0].language = #de-AT 
* #267424 ^designation[0].value = " - mol dose rate per body weight" 
* #267424 ^property[0].code = #parent 
* #267424 ^property[0].valueCode = #_mdcunits 
* #267424 ^property[1].code = #hints 
* #267424 ^property[1].valueString = "PART: 4 ~ CODE10: 5280" 
* #267442 "MDC_DIM_MILLI_MOLE_PER_KG_PER_HR"
* #267442 ^definition = mmol/kg/h
* #267442 ^designation[0].language = #de-AT 
* #267442 ^designation[0].value = " - mol dose rate per body weight" 
* #267442 ^property[0].code = #parent 
* #267442 ^property[0].valueCode = #_mdcunits 
* #267442 ^property[1].code = #hints 
* #267442 ^property[1].valueString = "PART: 4 ~ CODE10: 5298" 
* #267456 "MDC_DIM_X_MOLE_PER_KG_PER_DAY"
* #267456 ^definition = mol/kg/d
* #267456 ^designation[0].language = #de-AT 
* #267456 ^designation[0].value = " - mol dose rate per body weight" 
* #267456 ^property[0].code = #parent 
* #267456 ^property[0].valueCode = #_mdcunits 
* #267456 ^property[1].code = #hints 
* #267456 ^property[1].valueString = "PART: 4 ~ CODE10: 5312" 
* #267474 "MDC_DIM_MILLI_MOLE_PER_KG_PER_DAY"
* #267474 ^definition = mmol/kg/d
* #267474 ^designation[0].language = #de-AT 
* #267474 ^designation[0].value = " - mol dose rate per body weight" 
* #267474 ^property[0].code = #parent 
* #267474 ^property[0].valueCode = #_mdcunits 
* #267474 ^property[1].code = #hints 
* #267474 ^property[1].valueString = "PART: 4 ~ CODE10: 5330" 
* #267488 "MDC_DIM_X_EQUIV_PER_KG_PER_SEC"
* #267488 ^definition = eq/kg/s
* #267488 ^designation[0].language = #de-AT 
* #267488 ^designation[0].value = " - eq dose rate per body weight" 
* #267488 ^property[0].code = #parent 
* #267488 ^property[0].valueCode = #_mdcunits 
* #267488 ^property[1].code = #hints 
* #267488 ^property[1].valueString = "PART: 4 ~ CODE10: 5344" 
* #267520 "MDC_DIM_X_EQUIV_PER_KG_PER_MIN"
* #267520 ^definition = eq/kg/min
* #267520 ^designation[0].language = #de-AT 
* #267520 ^designation[0].value = " - eq dose rate per body weight" 
* #267520 ^property[0].code = #parent 
* #267520 ^property[0].valueCode = #_mdcunits 
* #267520 ^property[1].code = #hints 
* #267520 ^property[1].valueString = "PART: 4 ~ CODE10: 5376" 
* #267538 "MDC_DIM_MILLI_EQUIV_PER_KG_PER_MIN"
* #267538 ^definition = meq/kg/min
* #267538 ^designation[0].language = #de-AT 
* #267538 ^designation[0].value = " - eq dose rate per body weight" 
* #267538 ^property[0].code = #parent 
* #267538 ^property[0].valueCode = #_mdcunits 
* #267538 ^property[1].code = #hints 
* #267538 ^property[1].valueString = "PART: 4 ~ CODE10: 5394" 
* #267552 "MDC_DIM_X_EQUIV_PER_KG_PER_HR"
* #267552 ^definition = eq/kg/h
* #267552 ^designation[0].language = #de-AT 
* #267552 ^designation[0].value = " - eq dose rate per body weight" 
* #267552 ^property[0].code = #parent 
* #267552 ^property[0].valueCode = #_mdcunits 
* #267552 ^property[1].code = #hints 
* #267552 ^property[1].valueString = "PART: 4 ~ CODE10: 5408" 
* #267570 "MDC_DIM_MILLI_EQUIV_PER_KG_PER_HR"
* #267570 ^definition = meq/kg/h
* #267570 ^designation[0].language = #de-AT 
* #267570 ^designation[0].value = " - eq dose rate per body weight" 
* #267570 ^property[0].code = #parent 
* #267570 ^property[0].valueCode = #_mdcunits 
* #267570 ^property[1].code = #hints 
* #267570 ^property[1].valueString = "PART: 4 ~ CODE10: 5426" 
* #267584 "MDC_DIM_X_EQUIV_PER_KG_PER_DAY"
* #267584 ^definition = eq/kg/d
* #267584 ^designation[0].language = #de-AT 
* #267584 ^designation[0].value = " - eq dose rate per body weight" 
* #267584 ^property[0].code = #parent 
* #267584 ^property[0].valueCode = #_mdcunits 
* #267584 ^property[1].code = #hints 
* #267584 ^property[1].valueString = "PART: 4 ~ CODE10: 5440" 
* #267602 "MDC_DIM_MILLI_EQUIV_PER_KG_PER_DAY"
* #267602 ^definition = meq/kg/d
* #267602 ^designation[0].language = #de-AT 
* #267602 ^designation[0].value = " - eq dose rate per body weight" 
* #267602 ^property[0].code = #parent 
* #267602 ^property[0].valueCode = #_mdcunits 
* #267602 ^property[1].code = #hints 
* #267602 ^property[1].valueString = "PART: 4 ~ CODE10: 5458" 
* #267616 "MDC_DIM_INTL_UNIT"
* #267616 ^definition = [iU]
* #267616 ^designation[0].language = #de-AT 
* #267616 ^designation[0].value = " International Unit" 
* #267616 ^property[0].code = #parent 
* #267616 ^property[0].valueCode = #_mdcunits 
* #267616 ^property[1].code = #hints 
* #267616 ^property[1].valueString = "PART: 4 ~ CODE10: 5472" 
* #267619 "MDC_DIM_KILO_INTL_UNIT"
* #267619 ^definition = k[iU]
* #267619 ^designation[0].language = #de-AT 
* #267619 ^designation[0].value = "  International Unit" 
* #267619 ^property[0].code = #parent 
* #267619 ^property[0].valueCode = #_mdcunits 
* #267619 ^property[1].code = #hints 
* #267619 ^property[1].valueString = "PART: 4 ~ CODE10: 5475" 
* #267620 "MDC_DIM_MEGA_INTL_UNIT"
* #267620 ^definition = M[iU]
* #267620 ^designation[0].language = #de-AT 
* #267620 ^designation[0].value = " International Unit" 
* #267620 ^property[0].code = #parent 
* #267620 ^property[0].valueCode = #_mdcunits 
* #267620 ^property[1].code = #hints 
* #267620 ^property[1].valueString = "PART: 4 ~ CODE10: 5476" 
* #267634 "MDC_DIM_MILLI_INTL_UNIT"
* #267634 ^definition = m[iU]
* #267634 ^designation[0].language = #de-AT 
* #267634 ^designation[0].value = " International Unit" 
* #267634 ^property[0].code = #parent 
* #267634 ^property[0].valueCode = #_mdcunits 
* #267634 ^property[1].code = #hints 
* #267634 ^property[1].valueString = "PART: 4 ~ CODE10: 5490" 
* #267648 "MDC_DIM_X_INTL_UNIT_PER_CM_CUBE"
* #267648 ^definition = [iU]/cm3
* #267648 ^designation[0].language = #de-AT 
* #267648 ^designation[0].value = " - IU concentration" 
* #267648 ^property[0].code = #parent 
* #267648 ^property[0].valueCode = #_mdcunits 
* #267648 ^property[1].code = #hints 
* #267648 ^property[1].valueString = "PART: 4 ~ CODE10: 5504" 
* #267680 "MDC_DIM_X_INTL_UNIT_PER_M_CUBE"
* #267680 ^definition = [iU]/m3
* #267680 ^designation[0].language = #de-AT 
* #267680 ^designation[0].value = " - IU concentration" 
* #267680 ^property[0].code = #parent 
* #267680 ^property[0].valueCode = #_mdcunits 
* #267680 ^property[1].code = #hints 
* #267680 ^property[1].valueString = "PART: 4 ~ CODE10: 5536" 
* #267712 "MDC_DIM_X_INTL_UNIT_PER_L"
* #267712 ^definition = [iU]/L
* #267712 ^designation[0].language = #de-AT 
* #267712 ^designation[0].value = " - IU concentration" 
* #267712 ^property[0].code = #parent 
* #267712 ^property[0].valueCode = #_mdcunits 
* #267712 ^property[1].code = #hints 
* #267712 ^property[1].valueString = "PART: 4 ~ CODE10: 5568" 
* #267744 "MDC_DIM_X_INTL_UNIT_PER_ML"
* #267744 ^definition = [iU]/mL
* #267744 ^designation[0].language = #de-AT 
* #267744 ^designation[0].value = " - IU concentration" 
* #267744 ^property[0].code = #parent 
* #267744 ^property[0].valueCode = #_mdcunits 
* #267744 ^property[1].code = #hints 
* #267744 ^property[1].valueString = "PART: 4 ~ CODE10: 5600" 
* #267747 "MDC_DIM_KILO_INTL_UNIT_PER_ML"
* #267747 ^definition = k[iU]/mL
* #267747 ^designation[0].language = #de-AT 
* #267747 ^designation[0].value = " - IU concentration" 
* #267747 ^property[0].code = #parent 
* #267747 ^property[0].valueCode = #_mdcunits 
* #267747 ^property[1].code = #hints 
* #267747 ^property[1].valueString = "PART: 4 ~ CODE10: 5603" 
* #267748 "MDC_DIM_MEGA_INTL_UNIT_PER_ML"
* #267748 ^definition = M[iU]/mL
* #267748 ^designation[0].language = #de-AT 
* #267748 ^designation[0].value = " - IU concentration" 
* #267748 ^property[0].code = #parent 
* #267748 ^property[0].valueCode = #_mdcunits 
* #267748 ^property[1].code = #hints 
* #267748 ^property[1].valueString = "PART: 4 ~ CODE10: 5604" 
* #267762 "MDC_DIM_MILLI_INTL_UNIT_PER_ML"
* #267762 ^definition = m[iU]/mL
* #267762 ^designation[0].language = #de-AT 
* #267762 ^designation[0].value = " - IU concentration" 
* #267762 ^property[0].code = #parent 
* #267762 ^property[0].valueCode = #_mdcunits 
* #267762 ^property[1].code = #hints 
* #267762 ^property[1].valueString = "PART: 4 ~ CODE10: 5618" 
* #267776 "MDC_DIM_X_INTL_UNIT_PER_SEC"
* #267776 ^definition = [iU]/s
* #267776 ^designation[0].language = #de-AT 
* #267776 ^designation[0].value = " - IU mass flow rate" 
* #267776 ^property[0].code = #parent 
* #267776 ^property[0].valueCode = #_mdcunits 
* #267776 ^property[1].code = #hints 
* #267776 ^property[1].valueString = "PART: 4 ~ CODE10: 5632" 
* #267780 "MDC_DIM_MEGA_INTL_UNIT_PER_SEC"
* #267780 ^definition = M[iU]/s
* #267780 ^designation[0].language = #de-AT 
* #267780 ^designation[0].value = " - IU mass flow rate" 
* #267780 ^property[0].code = #parent 
* #267780 ^property[0].valueCode = #_mdcunits 
* #267780 ^property[1].code = #hints 
* #267780 ^property[1].valueString = "PART: 4 ~ CODE10: 5636" 
* #267808 "MDC_DIM_X_INTL_UNIT_PER_MIN"
* #267808 ^definition = [iU]/min
* #267808 ^designation[0].language = #de-AT 
* #267808 ^designation[0].value = " - IU mass flow rate" 
* #267808 ^property[0].code = #parent 
* #267808 ^property[0].valueCode = #_mdcunits 
* #267808 ^property[1].code = #hints 
* #267808 ^property[1].valueString = "PART: 4 ~ CODE10: 5664" 
* #267812 "MDC_DIM_MEGA_INTL_UNIT_PER_MIN"
* #267812 ^definition = M[iU]/min
* #267812 ^designation[0].language = #de-AT 
* #267812 ^designation[0].value = " - IU mass flow rate" 
* #267812 ^property[0].code = #parent 
* #267812 ^property[0].valueCode = #_mdcunits 
* #267812 ^property[1].code = #hints 
* #267812 ^property[1].valueString = "PART: 4 ~ CODE10: 5668" 
* #267826 "MDC_DIM_MILLI_INTL_UNIT_PER_MIN"
* #267826 ^definition = m[iU]/min
* #267826 ^designation[0].language = #de-AT 
* #267826 ^designation[0].value = " - IU mass flow rate" 
* #267826 ^property[0].code = #parent 
* #267826 ^property[0].valueCode = #_mdcunits 
* #267826 ^property[1].code = #hints 
* #267826 ^property[1].valueString = "PART: 4 ~ CODE10: 5682" 
* #267840 "MDC_DIM_X_INTL_UNIT_PER_HR"
* #267840 ^definition = [iU]/h
* #267840 ^designation[0].language = #de-AT 
* #267840 ^designation[0].value = " - IU mass flow rate" 
* #267840 ^property[0].code = #parent 
* #267840 ^property[0].valueCode = #_mdcunits 
* #267840 ^property[1].code = #hints 
* #267840 ^property[1].valueString = "PART: 4 ~ CODE10: 5696" 
* #267843 "MDC_DIM_KILO_INTL_UNIT_PER_HR"
* #267843 ^definition = k[iU]/h
* #267843 ^designation[0].language = #de-AT 
* #267843 ^designation[0].value = " - IU mass flow rate" 
* #267843 ^property[0].code = #parent 
* #267843 ^property[0].valueCode = #_mdcunits 
* #267843 ^property[1].code = #hints 
* #267843 ^property[1].valueString = "PART: 4 ~ CODE10: 5699" 
* #267844 "MDC_DIM_MEGA_INTL_UNIT_PER_HR"
* #267844 ^definition = M[iU]/h
* #267844 ^designation[0].language = #de-AT 
* #267844 ^designation[0].value = " - IU mass flow rate" 
* #267844 ^property[0].code = #parent 
* #267844 ^property[0].valueCode = #_mdcunits 
* #267844 ^property[1].code = #hints 
* #267844 ^property[1].valueString = "PART: 4 ~ CODE10: 5700" 
* #267858 "MDC_DIM_MILLI_INTL_UNIT_PER_HR"
* #267858 ^definition = m[iU]/h
* #267858 ^designation[0].language = #de-AT 
* #267858 ^designation[0].value = " - IU mass flow rate" 
* #267858 ^property[0].code = #parent 
* #267858 ^property[0].valueCode = #_mdcunits 
* #267858 ^property[1].code = #hints 
* #267858 ^property[1].valueString = "PART: 4 ~ CODE10: 5714" 
* #267872 "MDC_DIM_X_INTL_UNIT_PER_DAY"
* #267872 ^definition = [iU]/d
* #267872 ^designation[0].language = #de-AT 
* #267872 ^designation[0].value = " - IU mass flow rate" 
* #267872 ^property[0].code = #parent 
* #267872 ^property[0].valueCode = #_mdcunits 
* #267872 ^property[1].code = #hints 
* #267872 ^property[1].valueString = "PART: 4 ~ CODE10: 5728" 
* #267875 "MDC_DIM_KILO_INTL_UNIT_PER_DAY"
* #267875 ^definition = k[iU]/d
* #267875 ^designation[0].language = #de-AT 
* #267875 ^designation[0].value = "  - IU mass flow rate" 
* #267875 ^property[0].code = #parent 
* #267875 ^property[0].valueCode = #_mdcunits 
* #267875 ^property[1].code = #hints 
* #267875 ^property[1].valueString = "PART: 4 ~ CODE10: 5731" 
* #267876 "MDC_DIM_MEGA_INTL_UNIT_PER_DAY"
* #267876 ^definition = M[iU]/d
* #267876 ^designation[0].language = #de-AT 
* #267876 ^designation[0].value = " - IU mass flow rate" 
* #267876 ^property[0].code = #parent 
* #267876 ^property[0].valueCode = #_mdcunits 
* #267876 ^property[1].code = #hints 
* #267876 ^property[1].valueString = "PART: 4 ~ CODE10: 5732" 
* #267890 "MDC_DIM_MILLI_INTL_UNIT_PER_DAY"
* #267890 ^definition = m[iU]/d
* #267890 ^designation[0].language = #de-AT 
* #267890 ^designation[0].value = " - IU mass flow rate" 
* #267890 ^property[0].code = #parent 
* #267890 ^property[0].valueCode = #_mdcunits 
* #267890 ^property[1].code = #hints 
* #267890 ^property[1].valueString = "PART: 4 ~ CODE10: 5746" 
* #267904 "MDC_DIM_X_INTL_UNIT_PER_KG_PER_SEC"
* #267904 ^definition = [iU]/kg/s
* #267904 ^designation[0].language = #de-AT 
* #267904 ^designation[0].value = " - IU dose rate per body weight" 
* #267904 ^property[0].code = #parent 
* #267904 ^property[0].valueCode = #_mdcunits 
* #267904 ^property[1].code = #hints 
* #267904 ^property[1].valueString = "PART: 4 ~ CODE10: 5760" 
* #267908 "MDC_DIM_MEGA_INTL_UNIT_PER_KG_PER_SEC"
* #267908 ^definition = M[iU]/kg/s
* #267908 ^designation[0].language = #de-AT 
* #267908 ^designation[0].value = " - IU dose rate per body weight" 
* #267908 ^property[0].code = #parent 
* #267908 ^property[0].valueCode = #_mdcunits 
* #267908 ^property[1].code = #hints 
* #267908 ^property[1].valueString = "PART: 4 ~ CODE10: 5764" 
* #267922 "MDC_DIM_MILLI_INTL_UNIT_PER_KG_PER_SEC"
* #267922 ^definition = m[iU]/kg/s
* #267922 ^designation[0].language = #de-AT 
* #267922 ^designation[0].value = " - IU dose rate per body weight" 
* #267922 ^property[0].code = #parent 
* #267922 ^property[0].valueCode = #_mdcunits 
* #267922 ^property[1].code = #hints 
* #267922 ^property[1].valueString = "PART: 4 ~ CODE10: 5778" 
* #267936 "MDC_DIM_X_INTL_UNIT_PER_KG_PER_MIN"
* #267936 ^definition = [iU]/kg/min
* #267936 ^designation[0].language = #de-AT 
* #267936 ^designation[0].value = " - IU dose rate per body weight" 
* #267936 ^property[0].code = #parent 
* #267936 ^property[0].valueCode = #_mdcunits 
* #267936 ^property[1].code = #hints 
* #267936 ^property[1].valueString = "PART: 4 ~ CODE10: 5792" 
* #267940 "MDC_DIM_MEGA_INTL_UNIT_PER_KG_PER_MIN"
* #267940 ^definition = M[iU]/kg/min
* #267940 ^designation[0].language = #de-AT 
* #267940 ^designation[0].value = " - IU dose rate per body weight" 
* #267940 ^property[0].code = #parent 
* #267940 ^property[0].valueCode = #_mdcunits 
* #267940 ^property[1].code = #hints 
* #267940 ^property[1].valueString = "PART: 4 ~ CODE10: 5796" 
* #267954 "MDC_DIM_MILLI_INTL_UNIT_PER_KG_PER_MIN"
* #267954 ^definition = m[iU]/kg/min
* #267954 ^designation[0].language = #de-AT 
* #267954 ^designation[0].value = " - IU dose rate per body weight" 
* #267954 ^property[0].code = #parent 
* #267954 ^property[0].valueCode = #_mdcunits 
* #267954 ^property[1].code = #hints 
* #267954 ^property[1].valueString = "PART: 4 ~ CODE10: 5810" 
* #267968 "MDC_DIM_X_INTL_UNIT_PER_KG_PER_HR"
* #267968 ^definition = [iU]/kg/h
* #267968 ^designation[0].language = #de-AT 
* #267968 ^designation[0].value = " - IU dose rate per body weight" 
* #267968 ^property[0].code = #parent 
* #267968 ^property[0].valueCode = #_mdcunits 
* #267968 ^property[1].code = #hints 
* #267968 ^property[1].valueString = "PART: 4 ~ CODE10: 5824" 
* #267971 "MDC_DIM_KILO_INTL_UNIT_PER_KG_PER_HR"
* #267971 ^definition = k[iU]/kg/h
* #267971 ^designation[0].language = #de-AT 
* #267971 ^designation[0].value = "  - IU mass flow rate" 
* #267971 ^property[0].code = #parent 
* #267971 ^property[0].valueCode = #_mdcunits 
* #267971 ^property[1].code = #hints 
* #267971 ^property[1].valueString = "PART: 4 ~ CODE10: 5827" 
* #267972 "MDC_DIM_MEGA_INTL_UNIT_PER_KG_PER_HR"
* #267972 ^definition = M[iU]/kg/h
* #267972 ^designation[0].language = #de-AT 
* #267972 ^designation[0].value = " - IU dose rate per body weight" 
* #267972 ^property[0].code = #parent 
* #267972 ^property[0].valueCode = #_mdcunits 
* #267972 ^property[1].code = #hints 
* #267972 ^property[1].valueString = "PART: 4 ~ CODE10: 5828" 
* #267986 "MDC_DIM_MILLI_INTL_UNIT_PER_KG_PER_HR"
* #267986 ^definition = m[iU]/kg/h
* #267986 ^designation[0].language = #de-AT 
* #267986 ^designation[0].value = " - IU dose rate per body weight" 
* #267986 ^property[0].code = #parent 
* #267986 ^property[0].valueCode = #_mdcunits 
* #267986 ^property[1].code = #hints 
* #267986 ^property[1].valueString = "PART: 4 ~ CODE10: 5842" 
* #268000 "MDC_DIM_X_INTL_UNIT_PER_KG_PER_DAY"
* #268000 ^definition = [iU]/kg/d
* #268000 ^designation[0].language = #de-AT 
* #268000 ^designation[0].value = " - IU dose rate per body weight" 
* #268000 ^property[0].code = #parent 
* #268000 ^property[0].valueCode = #_mdcunits 
* #268000 ^property[1].code = #hints 
* #268000 ^property[1].valueString = "PART: 4 ~ CODE10: 5856" 
* #268003 "MDC_DIM_KILO_INTL_UNIT_PER_KG_PER_DAY"
* #268003 ^definition = k[iU]/kg/d
* #268003 ^designation[0].language = #de-AT 
* #268003 ^designation[0].value = "  - IU mass flow rate" 
* #268003 ^property[0].code = #parent 
* #268003 ^property[0].valueCode = #_mdcunits 
* #268003 ^property[1].code = #hints 
* #268003 ^property[1].valueString = "PART: 4 ~ CODE10: 5859" 
* #268004 "MDC_DIM_MEGA_INTL_UNIT_PER_KG_PER_DAY"
* #268004 ^definition = M[iU]/kg/d
* #268004 ^designation[0].language = #de-AT 
* #268004 ^designation[0].value = " - IU dose rate per body weight" 
* #268004 ^property[0].code = #parent 
* #268004 ^property[0].valueCode = #_mdcunits 
* #268004 ^property[1].code = #hints 
* #268004 ^property[1].valueString = "PART: 4 ~ CODE10: 5860" 
* #268018 "MDC_DIM_MILLI_INTL_UNIT_PER_KG_PER_DAY"
* #268018 ^definition = m[iU]/kg/d
* #268018 ^designation[0].language = #de-AT 
* #268018 ^designation[0].value = " - IU dose rate per body weight" 
* #268018 ^property[0].code = #parent 
* #268018 ^property[0].valueCode = #_mdcunits 
* #268018 ^property[1].code = #hints 
* #268018 ^property[1].valueString = "PART: 4 ~ CODE10: 5874" 
* #268032 "MDC_DIM_X_L_PER_CM_H2O"
* #268032 ^definition = L/cm[H2O]
* #268032 ^designation[0].language = #de-AT 
* #268032 ^designation[0].value = " compliance" 
* #268032 ^property[0].code = #parent 
* #268032 ^property[0].valueCode = #_mdcunits 
* #268032 ^property[1].code = #hints 
* #268032 ^property[1].valueString = "PART: 4 ~ CODE10: 5888" 
* #268050 "MDC_DIM_MILLI_L_PER_CM_H2O"
* #268050 ^definition = mL/cm[H2O]
* #268050 ^designation[0].language = #de-AT 
* #268050 ^designation[0].value = " compliance" 
* #268050 ^property[0].code = #parent 
* #268050 ^property[0].valueCode = #_mdcunits 
* #268050 ^property[1].code = #hints 
* #268050 ^property[1].valueString = "PART: 4 ~ CODE10: 5906" 
* #268064 "MDC_DIM_CM_H2O_PER_L_PER_SEC"
* #268064 ^definition = cm[H2O].s/L cm[H2O]/(L/s)
* #268064 ^designation[0].language = #de-AT 
* #268064 ^designation[0].value = " - Lung Resistance" 
* #268064 ^property[0].code = #parent 
* #268064 ^property[0].valueCode = #_mdcunits 
* #268064 ^property[1].code = #hints 
* #268064 ^property[1].valueString = "PART: 4 ~ CODE10: 5920" 
* #268096 "MDC_DIM_X_L_SQ_PER_SEC"
* #268096 ^definition = L2/s
* #268096 ^designation[0].language = #de-AT 
* #268096 ^designation[0].value = " - HF Transport Coefficient" 
* #268096 ^property[0].code = #parent 
* #268096 ^property[0].valueCode = #_mdcunits 
* #268096 ^property[1].code = #hints 
* #268096 ^property[1].valueString = "PART: 4 ~ CODE10: 5952" 
* #268128 "MDC_DIM_CM_H2O_PER_PERCENT"
* #268128 ^definition = cm[H2O]/%
* #268128 ^designation[0].language = #de-AT 
* #268128 ^designation[0].value = " - ratio of paO2 to FIO2" 
* #268128 ^property[0].code = #parent 
* #268128 ^property[0].valueCode = #_mdcunits 
* #268128 ^property[1].code = #hints 
* #268128 ^property[1].valueString = "PART: 4 ~ CODE10: 5984" 
* #268160 "MDC_DIM_DYNE_SEC_PER_M_SQ_PER_CM_5"
* #268160 ^definition = dyn.s.cm-5.m-2 dyn.s.m2/cm5 dyn.s/cm5/m2
* #268160 ^designation[0].language = #de-AT 
* #268160 ^designation[0].value = " - Pulmonary/ Systemic Vascular Resistance Index" 
* #268160 ^property[0].code = #parent 
* #268160 ^property[0].valueCode = #_mdcunits 
* #268160 ^property[1].code = #hints 
* #268160 ^property[1].valueString = "PART: 4 ~ CODE10: 6016" 
* #268192 "MDC_DIM_DEGC"
* #268192 ^definition = Cel
* #268192 ^designation[0].language = #de-AT 
* #268192 ^designation[0].value = " Q (temperature)" 
* #268192 ^property[0].code = #parent 
* #268192 ^property[0].valueCode = #_mdcunits 
* #268192 ^property[1].code = #hints 
* #268192 ^property[1].valueString = "PART: 4 ~ CODE10: 6048" 
* #268224 "MDC_DIM_X_AMP_HR"
* #268224 ^definition = A.h
* #268224 ^designation[0].language = #de-AT 
* #268224 ^designation[0].value = " IT (electrical charge)" 
* #268224 ^property[0].code = #parent 
* #268224 ^property[0].valueCode = #_mdcunits 
* #268224 ^property[1].code = #hints 
* #268224 ^property[1].valueString = "PART: 4 ~ CODE10: 6080" 
* #268256 "MDC_DIM_X_L_PER_BEAT"
* #268256 ^definition = L/{beat}
* #268256 ^designation[0].language = #de-AT 
* #268256 ^designation[0].value = " L3 (volume)" 
* #268256 ^property[0].code = #parent 
* #268256 ^property[0].valueCode = #_mdcunits 
* #268256 ^property[1].code = #hints 
* #268256 ^property[1].valueString = "PART: 4 ~ CODE10: 6112" 
* #268274 "MDC_DIM_MILLI_L_PER_BEAT"
* #268274 ^definition = mL/{beat}
* #268274 ^designation[0].language = #de-AT 
* #268274 ^designation[0].value = " L3 (volume)" 
* #268274 ^property[0].code = #parent 
* #268274 ^property[0].valueCode = #_mdcunits 
* #268274 ^property[1].code = #hints 
* #268274 ^property[1].valueString = "PART: 4 ~ CODE10: 6130" 
* #268288 "MDC_DIM_CM_H2O_PER_L"
* #268288 ^definition = cm[H2O]/L
* #268288 ^designation[0].language = #de-AT 
* #268288 ^designation[0].value = " L-4M1T-2 (elastance)" 
* #268288 ^property[0].code = #parent 
* #268288 ^property[0].valueCode = #_mdcunits 
* #268288 ^property[1].code = #hints 
* #268288 ^property[1].valueString = "PART: 4 ~ CODE10: 6144" 
* #268320 "MDC_DIM_MM_HG_PER_PERCENT"
* #268320 ^definition = mm[Hg]/%
* #268320 ^designation[0].language = #de-AT 
* #268320 ^designation[0].value = " - ratio of paO2 to FIO2" 
* #268320 ^property[0].code = #parent 
* #268320 ^property[0].valueCode = #_mdcunits 
* #268320 ^property[1].code = #hints 
* #268320 ^property[1].valueString = "PART: 4 ~ CODE10: 6176" 
* #268352 "MDC_DIM_X_PA_PER_PERCENT"
* #268352 ^definition = Pa/%
* #268352 ^designation[0].language = #de-AT 
* #268352 ^designation[0].value = " - ratio of paO2 to FIO2" 
* #268352 ^property[0].code = #parent 
* #268352 ^property[0].valueCode = #_mdcunits 
* #268352 ^property[1].code = #hints 
* #268352 ^property[1].valueString = "PART: 4 ~ CODE10: 6208" 
* #268384 "MDC_DIM_VOL_PERCENT"
* #268384 ^definition = %{vol}
* #268384 ^designation[0].language = #de-AT 
* #268384 ^designation[0].value = " - volume fraction" 
* #268384 ^property[0].code = #parent 
* #268384 ^property[0].valueCode = #_mdcunits 
* #268384 ^property[1].code = #hints 
* #268384 ^property[1].valueString = "PART: 4 ~ CODE10: 6240" 
* #268416 "MDC_DIM_X_L_PER_MM_HG"
* #268416 ^definition = L/mm[Hg]
* #268416 ^designation[0].language = #de-AT 
* #268416 ^designation[0].value = " compliance" 
* #268416 ^property[0].code = #parent 
* #268416 ^property[0].valueCode = #_mdcunits 
* #268416 ^property[1].code = #hints 
* #268416 ^property[1].valueString = "PART: 4 ~ CODE10: 6272" 
* #268448 "MDC_DIM_X_L_PER_MM_PA"
* #268448 ^definition = L/Pa
* #268448 ^designation[0].language = #de-AT 
* #268448 ^designation[0].value = " compliance" 
* #268448 ^property[0].code = #parent 
* #268448 ^property[0].valueCode = #_mdcunits 
* #268448 ^property[1].code = #hints 
* #268448 ^property[1].valueString = "PART: 4 ~ CODE10: 6304" 
* #268480 "MDC_DIM_MM_HG_PER_L"
* #268480 ^definition = mm[Hg]/L
* #268480 ^property[0].code = #parent 
* #268480 ^property[0].valueCode = #_mdcunits 
* #268480 ^property[1].code = #hints 
* #268480 ^property[1].valueString = "PART: 4 ~ CODE10: 6336" 
* #268512 "MDC_DIM_PA_PER_X_L"
* #268512 ^definition = Pa/L
* #268512 ^designation[0].language = #de-AT 
* #268512 ^designation[0].value = " L-4MT-2 (elastance)" 
* #268512 ^property[0].code = #parent 
* #268512 ^property[0].valueCode = #_mdcunits 
* #268512 ^property[1].code = #hints 
* #268512 ^property[1].valueString = "PART: 4 ~ CODE10: 6368" 
* #268544 "MDC_DIM_X_L_PER_DL"
* #268544 ^definition = L/dL
* #268544 ^designation[0].language = #de-AT 
* #268544 ^designation[0].value = " - volume fraction" 
* #268544 ^property[0].code = #parent 
* #268544 ^property[0].valueCode = #_mdcunits 
* #268544 ^property[1].code = #hints 
* #268544 ^property[1].valueString = "PART: 4 ~ CODE10: 6400" 
* #268562 "MDC_DIM_MILLI_L_PER_DL"
* #268562 ^definition = mL/dL
* #268562 ^designation[0].language = #de-AT 
* #268562 ^designation[0].value = " - volume fraction" 
* #268562 ^property[0].code = #parent 
* #268562 ^property[0].valueCode = #_mdcunits 
* #268562 ^property[1].code = #hints 
* #268562 ^property[1].valueString = "PART: 4 ~ CODE10: 6418" 
* #268576 "MDC_DIM_DECIBEL"
* #268576 ^definition = dB dB[10.nV]
* #268576 ^designation[0].language = #de-AT 
* #268576 ^designation[0].value = " - decibel  (existing 11073 definition)" 
* #268576 ^property[0].code = #parent 
* #268576 ^property[0].valueCode = #_mdcunits 
* #268576 ^property[1].code = #hints 
* #268576 ^property[1].valueString = "PART: 4 ~ CODE10: 6432" 
* #268608 "MDC_DIM_X_G_PER_MG"
* #268608 ^definition = g/mg
* #268608 ^designation[0].language = #de-AT 
* #268608 ^designation[0].value = " - mass fraction" 
* #268608 ^property[0].code = #parent 
* #268608 ^property[0].valueCode = #_mdcunits 
* #268608 ^property[1].code = #hints 
* #268608 ^property[1].valueString = "PART: 4 ~ CODE10: 6464" 
* #268640 "MDC_DIM_BEAT_PER_MIN_PER_X_L"
* #268640 ^definition = /min/L 1/min/L {beats}/min/L {beat}/min/L
* #268640 ^designation[0].language = #de-AT 
* #268640 ^designation[0].value = " - special vital signs rates" 
* #268640 ^property[0].code = #parent 
* #268640 ^property[0].valueCode = #_mdcunits 
* #268640 ^property[1].code = #hints 
* #268640 ^property[1].valueString = "PART: 4 ~ CODE10: 6496" 
* #268672 "MDC_DIM_PER_X_L_PER_MIN"
* #268672 ^definition = /L/min
* #268672 ^designation[0].language = #de-AT 
* #268672 ^designation[0].value = " L-4M1T-2 (elastance)" 
* #268672 ^property[0].code = #parent 
* #268672 ^property[0].valueCode = #_mdcunits 
* #268672 ^property[1].code = #hints 
* #268672 ^property[1].valueString = "PART: 4 ~ CODE10: 6528" 
* #268704 "MDC_DIM_X_M_PER_MIN"
* #268704 ^definition = m/min
* #268704 ^designation[0].language = #de-AT 
* #268704 ^designation[0].value = " LT-1" 
* #268704 ^property[0].code = #parent 
* #268704 ^property[0].valueCode = #_mdcunits 
* #268704 ^property[1].code = #hints 
* #268704 ^property[1].valueString = "PART: 4 ~ CODE10: 6560" 
* #268736 "MDC_DIM_PSI"
* #268736 ^definition = [psi]
* #268736 ^designation[0].language = #de-AT 
* #268736 ^designation[0].value = " L-1MT-2 (pressure)" 
* #268736 ^property[0].code = #parent 
* #268736 ^property[0].valueCode = #_mdcunits 
* #268736 ^property[1].code = #hints 
* #268736 ^property[1].valueString = "PART: 4 ~ CODE10: 6592" 
* #268832 "MDC_DIM_X_RAD_PER_SEC"
* #268832 ^definition = rad/s
* #268832 ^designation[0].language = #de-AT 
* #268832 ^designation[0].value = " LT-1  (velocity)" 
* #268832 ^property[0].code = #parent 
* #268832 ^property[0].valueCode = #_mdcunits 
* #268832 ^property[1].code = #hints 
* #268832 ^property[1].valueString = "PART: 4 ~ CODE10: 6688" 
* #268864 "MDC_DIM_X_LUMEN_PER_M_SQ"
* #268864 ^definition = lm/m2
* #268864 ^designation[0].language = #de-AT 
* #268864 ^designation[0].value = " Luminance" 
* #268864 ^property[0].code = #parent 
* #268864 ^property[0].valueCode = #_mdcunits 
* #268864 ^property[1].code = #hints 
* #268864 ^property[1].valueString = "PART: 4 ~ CODE10: 6720" 
* #268896 "MDC_DIM_X_G_PER_LB_PER_HR"
* #268896 ^definition = g/[lb_av]/h
* #268896 ^designation[0].language = #de-AT 
* #268896 ^designation[0].value = " - mass dose rate per body weight" 
* #268896 ^property[0].code = #parent 
* #268896 ^property[0].valueCode = #_mdcunits 
* #268896 ^property[1].code = #hints 
* #268896 ^property[1].valueString = "PART: 4 ~ CODE10: 6752" 
* #268914 "MDC_DIM_MILLI_G_PER_LB_PER_HR"
* #268914 ^definition = mg/[lb_av]/h
* #268914 ^designation[0].language = #de-AT 
* #268914 ^designation[0].value = " - mass dose rate per body weight" 
* #268914 ^property[0].code = #parent 
* #268914 ^property[0].valueCode = #_mdcunits 
* #268914 ^property[1].code = #hints 
* #268914 ^property[1].valueString = "PART: 4 ~ CODE10: 6770" 
* #268915 "MDC_DIM_MICRO_G_PER_LB_PER_HR"
* #268915 ^definition = ug/[lb_av]/h
* #268915 ^designation[0].language = #de-AT 
* #268915 ^designation[0].value = " - mass dose rate per body weight" 
* #268915 ^property[0].code = #parent 
* #268915 ^property[0].valueCode = #_mdcunits 
* #268915 ^property[1].code = #hints 
* #268915 ^property[1].valueString = "PART: 4 ~ CODE10: 6771" 
* #268916 "MDC_DIM_NANO_G_PER_LB_PER_HR"
* #268916 ^definition = ng/[lb_av]/h
* #268916 ^designation[0].language = #de-AT 
* #268916 ^designation[0].value = " - mass dose rate per body weight" 
* #268916 ^property[0].code = #parent 
* #268916 ^property[0].valueCode = #_mdcunits 
* #268916 ^property[1].code = #hints 
* #268916 ^property[1].valueString = "PART: 4 ~ CODE10: 6772" 
* #268928 "MDC_DIM_X_G_PER_LB_PER_MIN"
* #268928 ^definition = g/[lb_av]/min
* #268928 ^designation[0].language = #de-AT 
* #268928 ^designation[0].value = " - mass dose rate per body weight" 
* #268928 ^property[0].code = #parent 
* #268928 ^property[0].valueCode = #_mdcunits 
* #268928 ^property[1].code = #hints 
* #268928 ^property[1].valueString = "PART: 4 ~ CODE10: 6784" 
* #268946 "MDC_DIM_MILLI_G_PER_LB_PER_MIN"
* #268946 ^definition = mg/[lb_av]/min
* #268946 ^designation[0].language = #de-AT 
* #268946 ^designation[0].value = " - mass dose rate per body weight" 
* #268946 ^property[0].code = #parent 
* #268946 ^property[0].valueCode = #_mdcunits 
* #268946 ^property[1].code = #hints 
* #268946 ^property[1].valueString = "PART: 4 ~ CODE10: 6802" 
* #268947 "MDC_DIM_MICRO_G_PER_LB_PER_MIN"
* #268947 ^definition = ug/[lb_av]/min
* #268947 ^designation[0].language = #de-AT 
* #268947 ^designation[0].value = " - mass dose rate per body weight" 
* #268947 ^property[0].code = #parent 
* #268947 ^property[0].valueCode = #_mdcunits 
* #268947 ^property[1].code = #hints 
* #268947 ^property[1].valueString = "PART: 4 ~ CODE10: 6803" 
* #268948 "MDC_DIM_NANO_G_PER_LB_PER_MIN"
* #268948 ^definition = ng/[lb_av]/min
* #268948 ^designation[0].language = #de-AT 
* #268948 ^designation[0].value = " - mass dose rate per body weight" 
* #268948 ^property[0].code = #parent 
* #268948 ^property[0].valueCode = #_mdcunits 
* #268948 ^property[1].code = #hints 
* #268948 ^property[1].valueString = "PART: 4 ~ CODE10: 6804" 
* #268960 "MDC_DIM_X_G_PER_M_SQ_PER_HR"
* #268960 ^definition = g/m2/h
* #268960 ^designation[0].language = #de-AT 
* #268960 ^designation[0].value = " - mass dose rate per body surface area" 
* #268960 ^property[0].code = #parent 
* #268960 ^property[0].valueCode = #_mdcunits 
* #268960 ^property[1].code = #hints 
* #268960 ^property[1].valueString = "PART: 4 ~ CODE10: 6816" 
* #268978 "MDC_DIM_MILLI_G_PER_M_SQ_PER_HR"
* #268978 ^definition = mg/m2/h
* #268978 ^designation[0].language = #de-AT 
* #268978 ^designation[0].value = " - mass dose rate per body surface area" 
* #268978 ^property[0].code = #parent 
* #268978 ^property[0].valueCode = #_mdcunits 
* #268978 ^property[1].code = #hints 
* #268978 ^property[1].valueString = "PART: 4 ~ CODE10: 6834" 
* #268979 "MDC_DIM_MICRO_G_PER_M_SQ_PER_HR"
* #268979 ^definition = ug/m2/h
* #268979 ^designation[0].language = #de-AT 
* #268979 ^designation[0].value = " - mass dose rate per body surface area" 
* #268979 ^property[0].code = #parent 
* #268979 ^property[0].valueCode = #_mdcunits 
* #268979 ^property[1].code = #hints 
* #268979 ^property[1].valueString = "PART: 4 ~ CODE10: 6835" 
* #268980 "MDC_DIM_NANO_G_PER_M_SQ_PER_HR"
* #268980 ^definition = ng/m2/h
* #268980 ^designation[0].language = #de-AT 
* #268980 ^designation[0].value = " - mass dose rate per body surface area" 
* #268980 ^property[0].code = #parent 
* #268980 ^property[0].valueCode = #_mdcunits 
* #268980 ^property[1].code = #hints 
* #268980 ^property[1].valueString = "PART: 4 ~ CODE10: 6836" 
* #268992 "MDC_DIM_X_G_PER_M_SQ_PER_MIN"
* #268992 ^definition = g/m2/min
* #268992 ^designation[0].language = #de-AT 
* #268992 ^designation[0].value = " - mass dose rate per body surface area" 
* #268992 ^property[0].code = #parent 
* #268992 ^property[0].valueCode = #_mdcunits 
* #268992 ^property[1].code = #hints 
* #268992 ^property[1].valueString = "PART: 4 ~ CODE10: 6848" 
* #269010 "MDC_DIM_MILLI_G_PER_M_SQ_PER_MIN"
* #269010 ^definition = mg/m2/min
* #269010 ^designation[0].language = #de-AT 
* #269010 ^designation[0].value = " - mass dose rate per body surface area" 
* #269010 ^property[0].code = #parent 
* #269010 ^property[0].valueCode = #_mdcunits 
* #269010 ^property[1].code = #hints 
* #269010 ^property[1].valueString = "PART: 4 ~ CODE10: 6866" 
* #269011 "MDC_DIM_MICRO_G_PER_M_SQ_PER_MIN"
* #269011 ^definition = ug/m2/min
* #269011 ^designation[0].language = #de-AT 
* #269011 ^designation[0].value = " - mass dose rate per body surface area" 
* #269011 ^property[0].code = #parent 
* #269011 ^property[0].valueCode = #_mdcunits 
* #269011 ^property[1].code = #hints 
* #269011 ^property[1].valueString = "PART: 4 ~ CODE10: 6867" 
* #269012 "MDC_DIM_NANO_G_PER_M_SQ_PER_MIN"
* #269012 ^definition = ng/m2/min
* #269012 ^designation[0].language = #de-AT 
* #269012 ^designation[0].value = " - mass dose rate per body surface area" 
* #269012 ^property[0].code = #parent 
* #269012 ^property[0].valueCode = #_mdcunits 
* #269012 ^property[1].code = #hints 
* #269012 ^property[1].valueString = "PART: 4 ~ CODE10: 6868" 
* #269024 "MDC_DIM_X_INTL_UNIT_PER_LB_PER_DAY"
* #269024 ^definition = [iU]/[lb_av]/d
* #269024 ^designation[0].language = #de-AT 
* #269024 ^designation[0].value = "  - IU mass flow rate" 
* #269024 ^property[0].code = #parent 
* #269024 ^property[0].valueCode = #_mdcunits 
* #269024 ^property[1].code = #hints 
* #269024 ^property[1].valueString = "PART: 4 ~ CODE10: 6880" 
* #269042 "MDC_DIM_MILLI_INTL_UNIT_PER_LB_PER_DAY"
* #269042 ^definition = m[iU]/[lb_av]/d
* #269042 ^designation[0].language = #de-AT 
* #269042 ^designation[0].value = "  - IU mass flow rate" 
* #269042 ^property[0].code = #parent 
* #269042 ^property[0].valueCode = #_mdcunits 
* #269042 ^property[1].code = #hints 
* #269042 ^property[1].valueString = "PART: 4 ~ CODE10: 6898" 
* #269056 "MDC_DIM_X_INTL_UNIT_PER_LB_PER_MIN"
* #269056 ^definition = [iU]/[lb_av]/min
* #269056 ^designation[0].language = #de-AT 
* #269056 ^designation[0].value = " - IU dose rate per body weight" 
* #269056 ^property[0].code = #parent 
* #269056 ^property[0].valueCode = #_mdcunits 
* #269056 ^property[1].code = #hints 
* #269056 ^property[1].valueString = "PART: 4 ~ CODE10: 6912" 
* #269074 "MDC_DIM_MILLI_INTL_UNIT_PER_LB_PER_MIN"
* #269074 ^definition = m[iU]/[lb_av]/min
* #269074 ^designation[0].language = #de-AT 
* #269074 ^designation[0].value = " - IU dose rate per body weight" 
* #269074 ^property[0].code = #parent 
* #269074 ^property[0].valueCode = #_mdcunits 
* #269074 ^property[1].code = #hints 
* #269074 ^property[1].valueString = "PART: 4 ~ CODE10: 6930" 
* #269088 "MDC_DIM_X_INTL_UNIT_PER_M_SQ_PER_HR"
* #269088 ^definition = [iU]/m2/h
* #269088 ^designation[0].language = #de-AT 
* #269088 ^designation[0].value = " - IU dose rate per body surface area" 
* #269088 ^property[0].code = #parent 
* #269088 ^property[0].valueCode = #_mdcunits 
* #269088 ^property[1].code = #hints 
* #269088 ^property[1].valueString = "PART: 4 ~ CODE10: 6944" 
* #269092 "MDC_DIM_MEGA_INTL_UNIT_PER_M_SQ_PER_HR"
* #269092 ^definition = M[iU]/m2/h
* #269092 ^designation[0].language = #de-AT 
* #269092 ^designation[0].value = " - IU dose rate per body surface area" 
* #269092 ^property[0].code = #parent 
* #269092 ^property[0].valueCode = #_mdcunits 
* #269092 ^property[1].code = #hints 
* #269092 ^property[1].valueString = "PART: 4 ~ CODE10: 6948" 
* #269106 "MDC_DIM_MILLI_INTL_UNIT_PER_M_SQ_PER_HR"
* #269106 ^definition = m[iU]/m2/h
* #269106 ^designation[0].language = #de-AT 
* #269106 ^designation[0].value = " - IU dose rate per body surface area" 
* #269106 ^property[0].code = #parent 
* #269106 ^property[0].valueCode = #_mdcunits 
* #269106 ^property[1].code = #hints 
* #269106 ^property[1].valueString = "PART: 4 ~ CODE10: 6962" 
* #269120 "MDC_DIM_X_INTL_UNIT_PER_M_SQ_PER_MIN"
* #269120 ^definition = [iU]/m2/min
* #269120 ^designation[0].language = #de-AT 
* #269120 ^designation[0].value = " - IU dose rate per body surface area" 
* #269120 ^property[0].code = #parent 
* #269120 ^property[0].valueCode = #_mdcunits 
* #269120 ^property[1].code = #hints 
* #269120 ^property[1].valueString = "PART: 4 ~ CODE10: 6976" 
* #269124 "MDC_DIM_MEGA_INTL_UNIT_PER_M_SQ_PER_MIN"
* #269124 ^definition = M[iU]/m2/min
* #269124 ^designation[0].language = #de-AT 
* #269124 ^designation[0].value = " - IU dose rate per body surface area" 
* #269124 ^property[0].code = #parent 
* #269124 ^property[0].valueCode = #_mdcunits 
* #269124 ^property[1].code = #hints 
* #269124 ^property[1].valueString = "PART: 4 ~ CODE10: 6980" 
* #269138 "MDC_DIM_MILLI_INTL_UNIT_PER_M_SQ_PER_MIN"
* #269138 ^definition = m[iU]/m2/min
* #269138 ^designation[0].language = #de-AT 
* #269138 ^designation[0].value = " - IU dose rate per body surface area" 
* #269138 ^property[0].code = #parent 
* #269138 ^property[0].valueCode = #_mdcunits 
* #269138 ^property[1].code = #hints 
* #269138 ^property[1].valueString = "PART: 4 ~ CODE10: 6994" 
* #269152 "MDC_DIM_X_EQUIV_PER_LB_PER_HR"
* #269152 ^definition = eq/[lb_av]/h
* #269152 ^designation[0].language = #de-AT 
* #269152 ^designation[0].value = " - eq dose rate per body weight" 
* #269152 ^property[0].code = #parent 
* #269152 ^property[0].valueCode = #_mdcunits 
* #269152 ^property[1].code = #hints 
* #269152 ^property[1].valueString = "PART: 4 ~ CODE10: 7008" 
* #269170 "MDC_DIM_MILLI_EQUIV_PER_LB_PER_HR"
* #269170 ^definition = meq/[lb_av]/h
* #269170 ^designation[0].language = #de-AT 
* #269170 ^designation[0].value = " - eq dose rate per body weight" 
* #269170 ^property[0].code = #parent 
* #269170 ^property[0].valueCode = #_mdcunits 
* #269170 ^property[1].code = #hints 
* #269170 ^property[1].valueString = "PART: 4 ~ CODE10: 7026" 
* #269184 "MDC_DIM_X_EQUIV_PER_LB_PER_MIN"
* #269184 ^definition = eq/[lb_av]/min
* #269184 ^designation[0].language = #de-AT 
* #269184 ^designation[0].value = " - eq dose rate per body weight" 
* #269184 ^property[0].code = #parent 
* #269184 ^property[0].valueCode = #_mdcunits 
* #269184 ^property[1].code = #hints 
* #269184 ^property[1].valueString = "PART: 4 ~ CODE10: 7040" 
* #269202 "MDC_DIM_MILLI_EQUIV_PER_LB_PER_MIN"
* #269202 ^definition = meq/[lb_av]/min
* #269202 ^designation[0].language = #de-AT 
* #269202 ^designation[0].value = " - eq dose rate per body weight" 
* #269202 ^property[0].code = #parent 
* #269202 ^property[0].valueCode = #_mdcunits 
* #269202 ^property[1].code = #hints 
* #269202 ^property[1].valueString = "PART: 4 ~ CODE10: 7058" 
* #269216 "MDC_DIM_X_EQUIV_PER_M_SQ_PER_HR"
* #269216 ^definition = eq/m2/h
* #269216 ^designation[0].language = #de-AT 
* #269216 ^designation[0].value = " - eq dose rate per body surface area" 
* #269216 ^property[0].code = #parent 
* #269216 ^property[0].valueCode = #_mdcunits 
* #269216 ^property[1].code = #hints 
* #269216 ^property[1].valueString = "PART: 4 ~ CODE10: 7072" 
* #269234 "MDC_DIM_MILLI_EQUIV_PER_M_SQ_PER_HR"
* #269234 ^definition = meq/m2/h
* #269234 ^designation[0].language = #de-AT 
* #269234 ^designation[0].value = " - eq dose rate per body surface area" 
* #269234 ^property[0].code = #parent 
* #269234 ^property[0].valueCode = #_mdcunits 
* #269234 ^property[1].code = #hints 
* #269234 ^property[1].valueString = "PART: 4 ~ CODE10: 7090" 
* #269248 "MDC_DIM_X_EQUIV_PER_M_SQ_PER_MIN"
* #269248 ^definition = eq/m2/min
* #269248 ^designation[0].language = #de-AT 
* #269248 ^designation[0].value = " - eq dose rate per body surface area" 
* #269248 ^property[0].code = #parent 
* #269248 ^property[0].valueCode = #_mdcunits 
* #269248 ^property[1].code = #hints 
* #269248 ^property[1].valueString = "PART: 4 ~ CODE10: 7104" 
* #269266 "MDC_DIM_MILLI_EQUIV_PER_M_SQ_PER_MIN"
* #269266 ^definition = meq/m2/min
* #269266 ^designation[0].language = #de-AT 
* #269266 ^designation[0].value = " - eq dose rate per body surface area" 
* #269266 ^property[0].code = #parent 
* #269266 ^property[0].valueCode = #_mdcunits 
* #269266 ^property[1].code = #hints 
* #269266 ^property[1].valueString = "PART: 4 ~ CODE10: 7122" 
* #269280 "MDC_DIM_X_EQUIV_PER_M_SQ_PER_DAY"
* #269280 ^definition = eq/m2/d
* #269280 ^designation[0].language = #de-AT 
* #269280 ^designation[0].value = " - eq dose rate per body surface area" 
* #269280 ^property[0].code = #parent 
* #269280 ^property[0].valueCode = #_mdcunits 
* #269280 ^property[1].code = #hints 
* #269280 ^property[1].valueString = "PART: 4 ~ CODE10: 7136" 
* #269298 "MDC_DIM_MILLI_EQUIV_PER_M_SQ_PER_DAY"
* #269298 ^definition = meq/m2/d
* #269298 ^designation[0].language = #de-AT 
* #269298 ^designation[0].value = " - eq dose rate per body surface area" 
* #269298 ^property[0].code = #parent 
* #269298 ^property[0].valueCode = #_mdcunits 
* #269298 ^property[1].code = #hints 
* #269298 ^property[1].valueString = "PART: 4 ~ CODE10: 7154" 
* #269312 "MDC_DIM_X_G_PER_M_SQ_PER_DAY"
* #269312 ^definition = g/m2/d
* #269312 ^designation[0].language = #de-AT 
* #269312 ^designation[0].value = " - mass dose rate per body surface area" 
* #269312 ^property[0].code = #parent 
* #269312 ^property[0].valueCode = #_mdcunits 
* #269312 ^property[1].code = #hints 
* #269312 ^property[1].valueString = "PART: 4 ~ CODE10: 7168" 
* #269330 "MDC_DIM_MILLI_G_PER_M_SQ_PER_DAY"
* #269330 ^definition = mg/m2/d
* #269330 ^designation[0].language = #de-AT 
* #269330 ^designation[0].value = " - mass dose rate per body surface area" 
* #269330 ^property[0].code = #parent 
* #269330 ^property[0].valueCode = #_mdcunits 
* #269330 ^property[1].code = #hints 
* #269330 ^property[1].valueString = "PART: 4 ~ CODE10: 7186" 
* #269331 "MDC_DIM_MICRO_G_PER_M_SQ_PER_DAY"
* #269331 ^definition = ug/m2/d
* #269331 ^designation[0].language = #de-AT 
* #269331 ^designation[0].value = " - mass dose rate per body surface area" 
* #269331 ^property[0].code = #parent 
* #269331 ^property[0].valueCode = #_mdcunits 
* #269331 ^property[1].code = #hints 
* #269331 ^property[1].valueString = "PART: 4 ~ CODE10: 7187" 
* #269332 "MDC_DIM_NANO_G_PER_M_SQ_PER_DAY"
* #269332 ^definition = ng/m2/d
* #269332 ^designation[0].language = #de-AT 
* #269332 ^designation[0].value = " - mass dose rate per body surface area" 
* #269332 ^property[0].code = #parent 
* #269332 ^property[0].valueCode = #_mdcunits 
* #269332 ^property[1].code = #hints 
* #269332 ^property[1].valueString = "PART: 4 ~ CODE10: 7188" 
* #269344 "MDC_DIM_X_INTL_UNIT_PER_M_SQ_PER_DAY"
* #269344 ^definition = [iU]/m2/d
* #269344 ^designation[0].language = #de-AT 
* #269344 ^designation[0].value = " - IU dose rate per body surface area" 
* #269344 ^property[0].code = #parent 
* #269344 ^property[0].valueCode = #_mdcunits 
* #269344 ^property[1].code = #hints 
* #269344 ^property[1].valueString = "PART: 4 ~ CODE10: 7200" 
* #269348 "MDC_DIM_MEGA_INTL_UNIT_PER_M_SQ_PER_DAY"
* #269348 ^definition = M[iU]/m2/d
* #269348 ^designation[0].language = #de-AT 
* #269348 ^designation[0].value = " - IU dose rate per body surface area" 
* #269348 ^property[0].code = #parent 
* #269348 ^property[0].valueCode = #_mdcunits 
* #269348 ^property[1].code = #hints 
* #269348 ^property[1].valueString = "PART: 4 ~ CODE10: 7204" 
* #269362 "MDC_DIM_MILLI_INTL_UNIT_PER_M_SQ_PER_DAY"
* #269362 ^definition = m[iU]/m2/d
* #269362 ^designation[0].language = #de-AT 
* #269362 ^designation[0].value = " - IU dose rate per body surface area" 
* #269362 ^property[0].code = #parent 
* #269362 ^property[0].valueCode = #_mdcunits 
* #269362 ^property[1].code = #hints 
* #269362 ^property[1].valueString = "PART: 4 ~ CODE10: 7218" 
* #269376 "MDC_DIM_X_L_PER_KG_PER_HR"
* #269376 ^definition = L/kg/h
* #269376 ^designation[0].language = #de-AT 
* #269376 ^designation[0].value = " Flow rate per body weight" 
* #269376 ^property[0].code = #parent 
* #269376 ^property[0].valueCode = #_mdcunits 
* #269376 ^property[1].code = #hints 
* #269376 ^property[1].valueString = "PART: 4 ~ CODE10: 7232" 
* #269394 "MDC_DIM_MILLI_L_PER_KG_PER_HR"
* #269394 ^definition = mL/kg/h
* #269394 ^designation[0].language = #de-AT 
* #269394 ^designation[0].value = " Flow rate per body weight" 
* #269394 ^property[0].code = #parent 
* #269394 ^property[0].valueCode = #_mdcunits 
* #269394 ^property[1].code = #hints 
* #269394 ^property[1].valueString = "PART: 4 ~ CODE10: 7250" 
* #269408 "MDCX_DIM_X_L_PER_KG_PER_MIN"
* #269408 ^definition = L/kg/min
* #269408 ^designation[0].language = #de-AT 
* #269408 ^designation[0].value = " Flow rate per body weight" 
* #269408 ^property[0].code = #parent 
* #269408 ^property[0].valueCode = #_mdcunits 
* #269408 ^property[1].code = #hints 
* #269408 ^property[1].valueString = "PART: 4 ~ CODE10: 7264" 
* #269426 "MDC_DIM_MILLI_L_PER_KG_PER_MIN"
* #269426 ^definition = mL/kg/min
* #269426 ^designation[0].language = #de-AT 
* #269426 ^designation[0].value = " Flow rate per body weight" 
* #269426 ^property[0].code = #parent 
* #269426 ^property[0].valueCode = #_mdcunits 
* #269426 ^property[1].code = #hints 
* #269426 ^property[1].valueString = "PART: 4 ~ CODE10: 7282" 
* #269440 "MDC_DIM_X_L_PER_KG_PER_DAY"
* #269440 ^definition = L/kg/d
* #269440 ^designation[0].language = #de-AT 
* #269440 ^designation[0].value = " Flow rate per body weight" 
* #269440 ^property[0].code = #parent 
* #269440 ^property[0].valueCode = #_mdcunits 
* #269440 ^property[1].code = #hints 
* #269440 ^property[1].valueString = "PART: 4 ~ CODE10: 7296" 
* #269458 "MDC_DIM_MILLI_L_PER_KG_PER_DAY"
* #269458 ^definition = mL/kg/d
* #269458 ^designation[0].language = #de-AT 
* #269458 ^designation[0].value = " Flow rate per body weight" 
* #269458 ^property[0].code = #parent 
* #269458 ^property[0].valueCode = #_mdcunits 
* #269458 ^property[1].code = #hints 
* #269458 ^property[1].valueString = "PART: 4 ~ CODE10: 7314" 
* #269472 "MDC_DIM_X_L_PER_M_SQ_PER_MIN"
* #269472 ^definition = L/m2/min
* #269472 ^designation[0].language = #de-AT 
* #269472 ^designation[0].value = " Volume per minute per body surface area" 
* #269472 ^property[0].code = #parent 
* #269472 ^property[0].valueCode = #_mdcunits 
* #269472 ^property[1].code = #hints 
* #269472 ^property[1].valueString = "PART: 4 ~ CODE10: 7328" 
* #269490 "MDC_DIM_MILLI_L_PER_M_SQ_PER_MIN"
* #269490 ^definition = mL/m2/min
* #269490 ^designation[0].language = #de-AT 
* #269490 ^designation[0].value = " Flow rate per body surface area" 
* #269490 ^property[0].code = #parent 
* #269490 ^property[0].valueCode = #_mdcunits 
* #269490 ^property[1].code = #hints 
* #269490 ^property[1].valueString = "PART: 4 ~ CODE10: 7346" 
* #269504 "MDC_DIM_X_L_PER_M_SQ_PER_HR"
* #269504 ^definition = L/m2/h
* #269504 ^designation[0].language = #de-AT 
* #269504 ^designation[0].value = " Flow rate per body surface area" 
* #269504 ^property[0].code = #parent 
* #269504 ^property[0].valueCode = #_mdcunits 
* #269504 ^property[1].code = #hints 
* #269504 ^property[1].valueString = "PART: 4 ~ CODE10: 7360" 
* #269522 "MDC_DIM_MILLI_L_PER_M_SQ_PER_HR"
* #269522 ^definition = mL/m2/h
* #269522 ^designation[0].language = #de-AT 
* #269522 ^designation[0].value = " Flow rate per body surface area" 
* #269522 ^property[0].code = #parent 
* #269522 ^property[0].valueCode = #_mdcunits 
* #269522 ^property[1].code = #hints 
* #269522 ^property[1].valueString = "PART: 4 ~ CODE10: 7378" 
* #269536 "MDC_DIM_X_L_PER_M_SQ_PER_DAY"
* #269536 ^definition = L/m2/d
* #269536 ^designation[0].language = #de-AT 
* #269536 ^designation[0].value = " Flow rate per body surface area" 
* #269536 ^property[0].code = #parent 
* #269536 ^property[0].valueCode = #_mdcunits 
* #269536 ^property[1].code = #hints 
* #269536 ^property[1].valueString = "PART: 4 ~ CODE10: 7392" 
* #269554 "MDC_DIM_MILLI_L_PER_M_SQ_PER_DAY"
* #269554 ^definition = mL/m2/d
* #269554 ^designation[0].language = #de-AT 
* #269554 ^designation[0].value = " Flow rate per body surface area" 
* #269554 ^property[0].code = #parent 
* #269554 ^property[0].valueCode = #_mdcunits 
* #269554 ^property[1].code = #hints 
* #269554 ^property[1].valueString = "PART: 4 ~ CODE10: 7410" 
* #269568 "MDC_DIM_X_MOLE_PER_M_SQ_PER_SEC"
* #269568 ^definition = mol/m2/s
* #269568 ^designation[0].language = #de-AT 
* #269568 ^designation[0].value = " - mol dose rate per body surface area" 
* #269568 ^property[0].code = #parent 
* #269568 ^property[0].valueCode = #_mdcunits 
* #269568 ^property[1].code = #hints 
* #269568 ^property[1].valueString = "PART: 4 ~ CODE10: 7424" 
* #269586 "MDCX_DIM_MILLI_MOLE_PER_M_SQ_PER_SEC"
* #269586 ^definition = mmol/m2/s
* #269586 ^designation[0].language = #de-AT 
* #269586 ^designation[0].value = " - mol dose rate per body surface area" 
* #269586 ^property[0].code = #parent 
* #269586 ^property[0].valueCode = #_mdcunits 
* #269586 ^property[1].code = #hints 
* #269586 ^property[1].valueString = "PART: 4 ~ CODE10: 7442" 
* #269600 "MDC_DIM_X_MOLE_PER_M_SQ_PER_MIN"
* #269600 ^definition = mol/m2/min
* #269600 ^designation[0].language = #de-AT 
* #269600 ^designation[0].value = " - mol dose rate per body surface area" 
* #269600 ^property[0].code = #parent 
* #269600 ^property[0].valueCode = #_mdcunits 
* #269600 ^property[1].code = #hints 
* #269600 ^property[1].valueString = "PART: 4 ~ CODE10: 7456" 
* #269618 "MDC_DIM_MILLI_MOLE_PER_M_SQ_PER_MIN"
* #269618 ^definition = mmol/m2/min
* #269618 ^designation[0].language = #de-AT 
* #269618 ^designation[0].value = " - mol dose rate per body surface area" 
* #269618 ^property[0].code = #parent 
* #269618 ^property[0].valueCode = #_mdcunits 
* #269618 ^property[1].code = #hints 
* #269618 ^property[1].valueString = "PART: 4 ~ CODE10: 7474" 
* #269632 "MDC_DIM_X_MOLE_PER_M_SQ_PER_HR"
* #269632 ^definition = mol/m2/h
* #269632 ^designation[0].language = #de-AT 
* #269632 ^designation[0].value = " - mol dose rate per body surface area" 
* #269632 ^property[0].code = #parent 
* #269632 ^property[0].valueCode = #_mdcunits 
* #269632 ^property[1].code = #hints 
* #269632 ^property[1].valueString = "PART: 4 ~ CODE10: 7488" 
* #269650 "MDC_DIM_MILLI_MOLE_PER_M_SQ_PER_HR"
* #269650 ^definition = mmol/m2/h
* #269650 ^designation[0].language = #de-AT 
* #269650 ^designation[0].value = " - mol dose rate per body surface area" 
* #269650 ^property[0].code = #parent 
* #269650 ^property[0].valueCode = #_mdcunits 
* #269650 ^property[1].code = #hints 
* #269650 ^property[1].valueString = "PART: 4 ~ CODE10: 7506" 
* #269664 "MDC_DIM_X_MOLE_PER_M_SQ_PER_DAY"
* #269664 ^definition = mol/m2/d
* #269664 ^designation[0].language = #de-AT 
* #269664 ^designation[0].value = " - mol dose rate per body surface area" 
* #269664 ^property[0].code = #parent 
* #269664 ^property[0].valueCode = #_mdcunits 
* #269664 ^property[1].code = #hints 
* #269664 ^property[1].valueString = "PART: 4 ~ CODE10: 7520" 
* #269682 "MDC_DIM_MILLI_MOLE_PER_M_SQ_PER_DAY"
* #269682 ^definition = mmol/m2/d
* #269682 ^designation[0].language = #de-AT 
* #269682 ^designation[0].value = " - mol dose rate per body surface area" 
* #269682 ^property[0].code = #parent 
* #269682 ^property[0].valueCode = #_mdcunits 
* #269682 ^property[1].code = #hints 
* #269682 ^property[1].valueString = "PART: 4 ~ CODE10: 7538" 
* #269696 "MDC_DIM_X_MOLE_PER_M_SQ"
* #269696 ^definition = mol/m2
* #269696 ^designation[0].language = #de-AT 
* #269696 ^designation[0].value = " NM-1 (substance content)" 
* #269696 ^property[0].code = #parent 
* #269696 ^property[0].valueCode = #_mdcunits 
* #269696 ^property[1].code = #hints 
* #269696 ^property[1].valueString = "PART: 4 ~ CODE10: 7552" 
* #269714 "MDC_DIM_MILLI_MOLE_PER_M_SQ"
* #269714 ^definition = mmol/m2
* #269714 ^designation[0].language = #de-AT 
* #269714 ^designation[0].value = " NM-1 (substance content)" 
* #269714 ^property[0].code = #parent 
* #269714 ^property[0].valueCode = #_mdcunits 
* #269714 ^property[1].code = #hints 
* #269714 ^property[1].valueString = "PART: 4 ~ CODE10: 7570" 
* #269728 "MDC_DIM_X_EQUIV_PER_KG"
* #269728 ^definition = eq/kg
* #269728 ^designation[0].language = #de-AT 
* #269728 ^designation[0].value = " NM-1 (substance content)" 
* #269728 ^property[0].code = #parent 
* #269728 ^property[0].valueCode = #_mdcunits 
* #269728 ^property[1].code = #hints 
* #269728 ^property[1].valueString = "PART: 4 ~ CODE10: 7584" 
* #269746 "MDC_DIM_MILLI_EQUIV_PER_KG"
* #269746 ^definition = meq/kg
* #269746 ^designation[0].language = #de-AT 
* #269746 ^designation[0].value = " NM-1 (substance content)" 
* #269746 ^property[0].code = #parent 
* #269746 ^property[0].valueCode = #_mdcunits 
* #269746 ^property[1].code = #hints 
* #269746 ^property[1].valueString = "PART: 4 ~ CODE10: 7602" 
* #269760 "MDC_DIM_X_EQUIV_PER_M_SQ"
* #269760 ^definition = eq/m2
* #269760 ^designation[0].language = #de-AT 
* #269760 ^designation[0].value = " NM-1 (substance content)" 
* #269760 ^property[0].code = #parent 
* #269760 ^property[0].valueCode = #_mdcunits 
* #269760 ^property[1].code = #hints 
* #269760 ^property[1].valueString = "PART: 4 ~ CODE10: 7616" 
* #269778 "MDC_DIM_MILLI_EQUIV_PER_M_SQ"
* #269778 ^definition = meq/m2
* #269778 ^designation[0].language = #de-AT 
* #269778 ^designation[0].value = " NM-1 (substance content)" 
* #269778 ^property[0].code = #parent 
* #269778 ^property[0].valueCode = #_mdcunits 
* #269778 ^property[1].code = #hints 
* #269778 ^property[1].valueString = "PART: 4 ~ CODE10: 7634" 
* #269792 "MDC_DIM_X_INTL_UNIT_PER_M_SQ_PER_SEC"
* #269792 ^definition = [iU]/m2/s
* #269792 ^designation[0].language = #de-AT 
* #269792 ^designation[0].value = " - IU dose rate per body surface area" 
* #269792 ^property[0].code = #parent 
* #269792 ^property[0].valueCode = #_mdcunits 
* #269792 ^property[1].code = #hints 
* #269792 ^property[1].valueString = "PART: 4 ~ CODE10: 7648" 
* #269796 "MDC_DIM_MEGA_INTL_UNIT_PER_M_SQ_PER_SEC"
* #269796 ^definition = M[iU]/m2/s
* #269796 ^designation[0].language = #de-AT 
* #269796 ^designation[0].value = " - IU dose rate per body surface area" 
* #269796 ^property[0].code = #parent 
* #269796 ^property[0].valueCode = #_mdcunits 
* #269796 ^property[1].code = #hints 
* #269796 ^property[1].valueString = "PART: 4 ~ CODE10: 7652" 
* #269810 "MDC_DIM_MILLI_INTL_UNIT_PER_M_SQ_PER_SEC"
* #269810 ^definition = m[iU]/m2/s
* #269810 ^designation[0].language = #de-AT 
* #269810 ^designation[0].value = " - IU dose rate per body surface area" 
* #269810 ^property[0].code = #parent 
* #269810 ^property[0].valueCode = #_mdcunits 
* #269810 ^property[1].code = #hints 
* #269810 ^property[1].valueString = "PART: 4 ~ CODE10: 7666" 
* #269824 "MDC_DIM_X_INTL_UNIT_PER_KG"
* #269824 ^definition = [iU]/kg
* #269824 ^designation[0].language = #de-AT 
* #269824 ^designation[0].value = " - IU dose quantity per body weight" 
* #269824 ^property[0].code = #parent 
* #269824 ^property[0].valueCode = #_mdcunits 
* #269824 ^property[1].code = #hints 
* #269824 ^property[1].valueString = "PART: 4 ~ CODE10: 7680" 
* #269827 "MDC_DIM_KILO_INTL_UNIT_PER_KG"
* #269827 ^definition = k[iU]/kg
* #269827 ^designation[0].language = #de-AT 
* #269827 ^designation[0].value = " - IU dose quantity per body weight" 
* #269827 ^property[0].code = #parent 
* #269827 ^property[0].valueCode = #_mdcunits 
* #269827 ^property[1].code = #hints 
* #269827 ^property[1].valueString = "PART: 4 ~ CODE10: 7683" 
* #269828 "MDC_DIM_MEGA_INTL_UNIT_PER_KG"
* #269828 ^definition = M[iU]/kg
* #269828 ^designation[0].language = #de-AT 
* #269828 ^designation[0].value = " - IU dose quantity per body weight" 
* #269828 ^property[0].code = #parent 
* #269828 ^property[0].valueCode = #_mdcunits 
* #269828 ^property[1].code = #hints 
* #269828 ^property[1].valueString = "PART: 4 ~ CODE10: 7684" 
* #269842 "MDC_DIM_MILLI_INTL_UNIT_PER_KG"
* #269842 ^definition = m[iU]/kg
* #269842 ^designation[0].language = #de-AT 
* #269842 ^designation[0].value = " - IU dose quantity per body weight" 
* #269842 ^property[0].code = #parent 
* #269842 ^property[0].valueCode = #_mdcunits 
* #269842 ^property[1].code = #hints 
* #269842 ^property[1].valueString = "PART: 4 ~ CODE10: 7698" 
* #269843 "MDC_DIM_MICRO_INTL_UNIT_PER_KG"
* #269843 ^definition = u[iU]/kg
* #269843 ^designation[0].language = #de-AT 
* #269843 ^designation[0].value = " - IU dose quantity per body weight" 
* #269843 ^property[0].code = #parent 
* #269843 ^property[0].valueCode = #_mdcunits 
* #269843 ^property[1].code = #hints 
* #269843 ^property[1].valueString = "PART: 4 ~ CODE10: 7699" 
* #269844 "MDC_DIM_NANO_INTL_UNIT_PER_KG"
* #269844 ^definition = n[iU]/kg
* #269844 ^designation[0].language = #de-AT 
* #269844 ^designation[0].value = " - IU dose quantity per body weight" 
* #269844 ^property[0].code = #parent 
* #269844 ^property[0].valueCode = #_mdcunits 
* #269844 ^property[1].code = #hints 
* #269844 ^property[1].valueString = "PART: 4 ~ CODE10: 7700" 
* #269856 "MDC_DIM_X_INTL_UNIT_PER_M_SQ"
* #269856 ^definition = [iU]/m2
* #269856 ^designation[0].language = #de-AT 
* #269856 ^designation[0].value = " - IU dose quantity per body surface area" 
* #269856 ^property[0].code = #parent 
* #269856 ^property[0].valueCode = #_mdcunits 
* #269856 ^property[1].code = #hints 
* #269856 ^property[1].valueString = "PART: 4 ~ CODE10: 7712" 
* #269860 "MDC_DIM_MEGA_INTL_UNIT_PER_M_SQ"
* #269860 ^definition = M[iU]/m2
* #269860 ^designation[0].language = #de-AT 
* #269860 ^designation[0].value = " - IU dose quantity per body surface area" 
* #269860 ^property[0].code = #parent 
* #269860 ^property[0].valueCode = #_mdcunits 
* #269860 ^property[1].code = #hints 
* #269860 ^property[1].valueString = "PART: 4 ~ CODE10: 7716" 
* #269874 "MDC_DIM_MILLI_INTL_UNIT_PER_M_SQ"
* #269874 ^definition = m[iU]/m2
* #269874 ^designation[0].language = #de-AT 
* #269874 ^designation[0].value = " - IU dose quantity per body surface area" 
* #269874 ^property[0].code = #parent 
* #269874 ^property[0].valueCode = #_mdcunits 
* #269874 ^property[1].code = #hints 
* #269874 ^property[1].valueString = "PART: 4 ~ CODE10: 7730" 
* #269875 "MDC_DIM_MICRO_INTL_UNIT_PER_M_SQ"
* #269875 ^definition = u[iU]/m2
* #269875 ^designation[0].language = #de-AT 
* #269875 ^designation[0].value = " - IU dose quantity per body surface area" 
* #269875 ^property[0].code = #parent 
* #269875 ^property[0].valueCode = #_mdcunits 
* #269875 ^property[1].code = #hints 
* #269875 ^property[1].valueString = "PART: 4 ~ CODE10: 7731" 
* #269876 "MDC_DIM_NANO_INTL_UNIT_PER_M_SQ"
* #269876 ^definition = n[iU]/m2
* #269876 ^designation[0].language = #de-AT 
* #269876 ^designation[0].value = " - IU dose quantity per body surface area" 
* #269876 ^property[0].code = #parent 
* #269876 ^property[0].valueCode = #_mdcunits 
* #269876 ^property[1].code = #hints 
* #269876 ^property[1].valueString = "PART: 4 ~ CODE10: 7732" 
* #269888 "MDC_DIM_X_G_PER_M_SQ"
* #269888 ^definition = g/m2
* #269888 ^designation[0].language = #de-AT 
* #269888 ^designation[0].value = " ML-2" 
* #269888 ^property[0].code = #parent 
* #269888 ^property[0].valueCode = #_mdcunits 
* #269888 ^property[1].code = #hints 
* #269888 ^property[1].valueString = "PART: 4 ~ CODE10: 7744" 
* #269891 "MDC_DIM_KILO_G_PER_M_SQ"
* #269891 ^definition = kg/m2
* #269891 ^designation[0].language = #de-AT 
* #269891 ^designation[0].value = " ML-2" 
* #269891 ^property[0].code = #parent 
* #269891 ^property[0].valueCode = #_mdcunits 
* #269891 ^property[1].code = #hints 
* #269891 ^property[1].valueString = "PART: 4 ~ CODE10: 7747" 
* #269906 "MDC_DIM_MILLI_G_PER_M_SQ"
* #269906 ^definition = mg/m2
* #269906 ^designation[0].language = #de-AT 
* #269906 ^designation[0].value = " ML-2" 
* #269906 ^property[0].code = #parent 
* #269906 ^property[0].valueCode = #_mdcunits 
* #269906 ^property[1].code = #hints 
* #269906 ^property[1].valueString = "PART: 4 ~ CODE10: 7762" 
* #269907 "MDC_DIM_MICRO_G_PER_M_SQ"
* #269907 ^definition = ug/m2
* #269907 ^designation[0].language = #de-AT 
* #269907 ^designation[0].value = " ML-2" 
* #269907 ^property[0].code = #parent 
* #269907 ^property[0].valueCode = #_mdcunits 
* #269907 ^property[1].code = #hints 
* #269907 ^property[1].valueString = "PART: 4 ~ CODE10: 7763" 
* #269908 "MDC_DIM_NANO_G_PER_M_SQ"
* #269908 ^definition = ng/m2
* #269908 ^designation[0].language = #de-AT 
* #269908 ^designation[0].value = " ML-2" 
* #269908 ^property[0].code = #parent 
* #269908 ^property[0].valueCode = #_mdcunits 
* #269908 ^property[1].code = #hints 
* #269908 ^property[1].valueString = "PART: 4 ~ CODE10: 7764" 
* #269920 "MDC_DIM_BOOLEAN"
* #269920 ^definition = 1 {bool}
* #269920 ^designation[0].language = #de-AT 
* #269920 ^designation[0].value = " Dimension- less" 
* #269920 ^property[0].code = #parent 
* #269920 ^property[0].valueCode = #_mdcunits 
* #269920 ^property[1].code = #hints 
* #269920 ^property[1].valueString = "PART: 4 ~ CODE10: 7776" 
* #269952 "MDC_DIM_DECIBEL_X_V"
* #269952 ^definition = dB[V]
* #269952 ^designation[0].language = #de-AT 
* #269952 ^designation[0].value = " - decibel  (proposal dB1')'" 
* #269952 ^property[0].code = #parent 
* #269952 ^property[0].valueCode = #_mdcunits 
* #269952 ^property[1].code = #hints 
* #269952 ^property[1].valueString = "PART: 4 ~ CODE10: 7808" 
* #269984 "MDC_DIM_DECIBEL_X_MV"
* #269984 ^definition = dB[mV]
* #269984 ^designation[0].language = #de-AT 
* #269984 ^designation[0].value = " - decibel  (proposal dB1')'" 
* #269984 ^property[0].code = #parent 
* #269984 ^property[0].valueCode = #_mdcunits 
* #269984 ^property[1].code = #hints 
* #269984 ^property[1].valueString = "PART: 4 ~ CODE10: 7840" 
* #270004 "MDC_DIM_DECIBEL_10_NANO_VOLT"
* #270004 ^definition = dB[10.nV]
* #270004 ^property[0].code = #parent 
* #270004 ^property[0].valueCode = #_mdcunits 
* #270004 ^property[1].code = #hints 
* #270004 ^property[1].valueString = "PART: 4 ~ CODE10: 7860" 
* #270016 "MDC_DIM_DECIBEL_X_UV"
* #270016 ^definition = dB[uV]
* #270016 ^designation[0].language = #de-AT 
* #270016 ^designation[0].value = " - decibel  (proposal dB1')'" 
* #270016 ^property[0].code = #parent 
* #270016 ^property[0].valueCode = #_mdcunits 
* #270016 ^property[1].code = #hints 
* #270016 ^property[1].valueString = "PART: 4 ~ CODE10: 7872" 
* #270048 "MDC_DIM_DECIBEL_X_NV"
* #270048 ^definition = dB[nV]
* #270048 ^designation[0].language = #de-AT 
* #270048 ^designation[0].value = " - decibel  (proposal dB1')'" 
* #270048 ^property[0].code = #parent 
* #270048 ^property[0].valueCode = #_mdcunits 
* #270048 ^property[1].code = #hints 
* #270048 ^property[1].valueString = "PART: 4 ~ CODE10: 7904" 
* #270080 "MDC_DIM_DECIBEL_DECA_NV"
* #270080 ^definition = dB[10.nV]
* #270080 ^designation[0].language = #de-AT 
* #270080 ^designation[0].value = " - decibel  (proposal dB1')'" 
* #270080 ^property[0].code = #parent 
* #270080 ^property[0].valueCode = #_mdcunits 
* #270080 ^property[1].code = #hints 
* #270080 ^property[1].valueString = "PART: 4 ~ CODE10: 7936" 
* #270112 "MDC_DIM_X_BEL_MV"
* #270112 ^definition = B[mV]
* #270112 ^designation[0].language = #de-AT 
* #270112 ^designation[0].value = " - decibel  (proposal dB2')'" 
* #270112 ^property[0].code = #parent 
* #270112 ^property[0].valueCode = #_mdcunits 
* #270112 ^property[1].code = #hints 
* #270112 ^property[1].valueString = "PART: 4 ~ CODE10: 7968" 
* #270144 "MDC_DIM_X_BEL"
* #270144 ^definition = B
* #270144 ^designation[0].language = #de-AT 
* #270144 ^designation[0].value = " - Bel and deciBel   (proposal dB3')'" 
* #270144 ^property[0].code = #parent 
* #270144 ^property[0].valueCode = #_mdcunits 
* #270144 ^property[1].code = #hints 
* #270144 ^property[1].valueString = "PART: 4 ~ CODE10: 8000" 
* #270160 "MDC_DIM_DECI_BEL"
* #270160 ^definition = dB
* #270160 ^designation[0].language = #de-AT 
* #270160 ^designation[0].value = " - Bel and deciBel   (proposal dB3')'" 
* #270160 ^property[0].code = #parent 
* #270160 ^property[0].valueCode = #_mdcunits 
* #270160 ^property[1].code = #hints 
* #270160 ^property[1].valueString = "PART: 4 ~ CODE10: 8016" 
* #270176 "MDC_DIM_SQ_X_CM"
* #270176 ^definition = cm2
* #270176 ^designation[0].language = #de-AT 
* #270176 ^designation[0].value = " L2 (area)" 
* #270176 ^property[0].code = #parent 
* #270176 ^property[0].valueCode = #_mdcunits 
* #270176 ^property[1].code = #hints 
* #270176 ^property[1].valueString = "PART: 4 ~ CODE10: 8032" 
* #270208 "MDC_DIM_PER_X_SEC_SQ"
* #270208 ^definition = /s2
* #270208 ^designation[0].language = #de-AT 
* #270208 ^designation[0].value = " T-2 (rate, frequency squared)" 
* #270208 ^property[0].code = #parent 
* #270208 ^property[0].valueCode = #_mdcunits 
* #270208 ^property[1].code = #hints 
* #270208 ^property[1].valueString = "PART: 4 ~ CODE10: 8064" 
* #270210 "MDC_DIM_PER_HECTO_SEC_SQ"
* #270210 ^definition = /(100.s2)
* #270210 ^designation[0].language = #de-AT 
* #270210 ^designation[0].value = " T-2 (rate, frequency squared)" 
* #270210 ^property[0].code = #parent 
* #270210 ^property[0].valueCode = #_mdcunits 
* #270210 ^property[1].code = #hints 
* #270210 ^property[1].valueString = "PART: 4 ~ CODE10: 8066" 
* #270240 "MDC_DIM_X_ROTATIONS_PER_MIN"
* #270240 ^definition = 2.[pi].rad/min 360.deg/min
* #270240 ^designation[0].language = #de-AT 
* #270240 ^designation[0].value = " LT-1  (velocity)" 
* #270240 ^property[0].code = #parent 
* #270240 ^property[0].valueCode = #_mdcunits 
* #270240 ^property[1].code = #hints 
* #270240 ^property[1].valueString = "PART: 4 ~ CODE10: 8096" 
* #270272 "MDC_DIM_X_DROPS_PER_MIN"
* #270272 ^definition = [drp]/min
* #270272 ^designation[0].language = #de-AT 
* #270272 ^designation[0].value = " L3T-1  (volume flow rate)" 
* #270272 ^property[0].code = #parent 
* #270272 ^property[0].valueCode = #_mdcunits 
* #270272 ^property[1].code = #hints 
* #270272 ^property[1].valueString = "PART: 4 ~ CODE10: 8128" 
* #270304 "MDC_DIM_X_L_PER_CM_H2O_PER_KG"
* #270304 ^definition = L/cm[H2O]/kg
* #270304 ^designation[0].language = #de-AT 
* #270304 ^designation[0].value = " compliance per body weight" 
* #270304 ^property[0].code = #parent 
* #270304 ^property[0].valueCode = #_mdcunits 
* #270304 ^property[1].code = #hints 
* #270304 ^property[1].valueString = "PART: 4 ~ CODE10: 8160" 
* #270322 "MDC_DIM_MILLI_L_PER_CM_H2O_PER_KG"
* #270322 ^definition = mL/cm[H2O]/kg
* #270322 ^designation[0].language = #de-AT 
* #270322 ^designation[0].value = " compliance per body weight" 
* #270322 ^property[0].code = #parent 
* #270322 ^property[0].valueCode = #_mdcunits 
* #270322 ^property[1].code = #hints 
* #270322 ^property[1].valueString = "PART: 4 ~ CODE10: 8178" 
* #270336 "MDC_DIM_X_TESLA"
* #270336 ^definition = T
* #270336 ^designation[0].language = #de-AT 
* #270336 ^designation[0].value = " IL-1 (magnetic field strength)" 
* #270336 ^property[0].code = #parent 
* #270336 ^property[0].valueCode = #_mdcunits 
* #270336 ^property[1].code = #hints 
* #270336 ^property[1].valueString = "PART: 4 ~ CODE10: 8192" 
* #270368 "MDC_DIM_X_VOLT_SEC"
* #270368 ^definition = V.s
* #270368 ^designation[0].language = #de-AT 
* #270368 ^designation[0].value = " ML2I-1T-2 (electric potential time integral)" 
* #270368 ^property[0].code = #parent 
* #270368 ^property[0].valueCode = #_mdcunits 
* #270368 ^property[1].code = #hints 
* #270368 ^property[1].valueString = "PART: 4 ~ CODE10: 8224" 
* #270388 "MDC_DIM_NANO_VOLT_SEC"
* #270388 ^definition = uV.ms
* #270388 ^designation[0].language = #de-AT 
* #270388 ^designation[0].value = " ML2I-1T-2 (electric potential time integral)" 
* #270388 ^property[0].code = #parent 
* #270388 ^property[0].valueCode = #_mdcunits 
* #270388 ^property[1].code = #hints 
* #270388 ^property[1].valueString = "PART: 4 ~ CODE10: 8244" 
* #270400 "MDC_DIM_X_VOLT_PER_SEC"
* #270400 ^definition = V/s
* #270400 ^designation[0].language = #de-AT 
* #270400 ^designation[0].value = " ML2I-1T-4 (electric potential rate of change)" 
* #270400 ^property[0].code = #parent 
* #270400 ^property[0].valueCode = #_mdcunits 
* #270400 ^property[1].code = #hints 
* #270400 ^property[1].valueString = "PART: 4 ~ CODE10: 8256" 
* #270419 "MDC_DIM_MICRO_VOLT_PER_SEC"
* #270419 ^definition = uV/s
* #270419 ^designation[0].language = #de-AT 
* #270419 ^designation[0].value = " ML2I-1T-4 (electric potential rate of change)" 
* #270419 ^property[0].code = #parent 
* #270419 ^property[0].valueCode = #_mdcunits 
* #270419 ^property[1].code = #hints 
* #270419 ^property[1].valueString = "PART: 4 ~ CODE10: 8275" 
* #270432 "MDC_DIM_PER_X_OHM"
* #270432 ^definition = /Ohm 1/Ohm s
* #270432 ^designation[0].language = #de-AT 
* #270432 ^designation[0].value = " M-1L-2I2T3 (electric conductance)" 
* #270432 ^property[0].code = #parent 
* #270432 ^property[0].valueCode = #_mdcunits 
* #270432 ^property[1].code = #hints 
* #270432 ^property[1].valueString = "PART: 4 ~ CODE10: 8288" 
* #270435 "MDC_DIM_PER_KILO_OHM"
* #270435 ^definition = /kOhm 1/kOhm
* #270435 ^designation[0].language = #de-AT 
* #270435 ^designation[0].value = " M-1L-2I2T3 (electric conductance)" 
* #270435 ^property[0].code = #parent 
* #270435 ^property[0].valueCode = #_mdcunits 
* #270435 ^property[1].code = #hints 
* #270435 ^property[1].valueString = "PART: 4 ~ CODE10: 8291" 
* #270464 "MDC_DIM_DYNE_SEC_M_SQ_PER_CM_5"
* #270464 ^definition = dyn.s.cm-5.m2 dyn.s.m2/cm5
* #270464 ^designation[0].language = #de-AT 
* #270464 ^designation[0].value = " - Pulmonary/ Systemic Vascular Resistance Index" 
* #270464 ^property[0].code = #parent 
* #270464 ^property[0].valueCode = #_mdcunits 
* #270464 ^property[1].code = #hints 
* #270464 ^property[1].valueString = "PART: 4 ~ CODE10: 8320" 
* #270496 "MDC_DIM_X_CAL"
* #270496 ^definition = cal
* #270496 ^designation[0].language = #de-AT 
* #270496 ^designation[0].value = " thermal calories" 
* #270496 ^property[0].code = #parent 
* #270496 ^property[0].valueCode = #_mdcunits 
* #270496 ^property[1].code = #hints 
* #270496 ^property[1].valueString = "PART: 4 ~ CODE10: 8352" 
* #270499 "MDC_DIM_KILO_CAL"
* #270499 ^definition = kcal
* #270499 ^designation[0].language = #de-AT 
* #270499 ^designation[0].value = " thermal calories" 
* #270499 ^property[0].code = #parent 
* #270499 ^property[0].valueCode = #_mdcunits 
* #270499 ^property[1].code = #hints 
* #270499 ^property[1].valueString = "PART: 4 ~ CODE10: 8355" 
* #270528 "MDC_DIM_X_NUTR_CAL"
* #270528 ^definition = [Cal]
* #270528 ^designation[0].language = #de-AT 
* #270528 ^designation[0].value = " nutrition label calories" 
* #270528 ^property[0].code = #parent 
* #270528 ^property[0].valueCode = #_mdcunits 
* #270528 ^property[1].code = #hints 
* #270528 ^property[1].valueString = "PART: 4 ~ CODE10: 8384" 
* #270560 "MDC_DIM_X_CAL_PER_DAY"
* #270560 ^definition = cal/d
* #270560 ^designation[0].language = #de-AT 
* #270560 ^designation[0].value = " thermal caloric rate" 
* #270560 ^property[0].code = #parent 
* #270560 ^property[0].valueCode = #_mdcunits 
* #270560 ^property[1].code = #hints 
* #270560 ^property[1].valueString = "PART: 4 ~ CODE10: 8416" 
* #270563 "MDC_DIM_KILO_CAL_PER_DAY"
* #270563 ^definition = kcal/d
* #270563 ^designation[0].language = #de-AT 
* #270563 ^designation[0].value = " thermal caloric rate" 
* #270563 ^property[0].code = #parent 
* #270563 ^property[0].valueCode = #_mdcunits 
* #270563 ^property[1].code = #hints 
* #270563 ^property[1].valueString = "PART: 4 ~ CODE10: 8419" 
* #270592 "MDC_DIM_JOULES_PER_BREATH"
* #270592 ^definition = J/{breath}
* #270592 ^property[0].code = #parent 
* #270592 ^property[0].valueCode = #_mdcunits 
* #270592 ^property[1].code = #hints 
* #270592 ^property[1].valueString = "PART: 4 ~ CODE10: 8448" 
* #270624 "MDC_DIM_JOULES_PER_L"
* #270624 ^definition = J/L
* #270624 ^property[0].code = #parent 
* #270624 ^property[0].valueCode = #_mdcunits 
* #270624 ^property[1].code = #hints 
* #270624 ^property[1].valueString = "PART: 4 ~ CODE10: 8480" 
* #270656 "MDC_DIM_DYNE_SEC_PER_CM_5"
* #270656 ^definition = dyn.s/cm5
* #270656 ^designation[0].language = #de-AT 
* #270656 ^designation[0].value = " Vascular Resistance" 
* #270656 ^property[0].code = #parent 
* #270656 ^property[0].valueCode = #_mdcunits 
* #270656 ^property[1].code = #hints 
* #270656 ^property[1].valueString = "PART: 4 ~ CODE10: 8512" 
* #270688 "MDC_DIM_MMHG_SEC_PER_ML"
* #270688 ^definition = mm[Hg].s/mL [PRU]
* #270688 ^designation[0].language = #de-AT 
* #270688 ^designation[0].value = " Vascular Resistance (Peripheral Reistance Unit)" 
* #270688 ^property[0].code = #parent 
* #270688 ^property[0].valueCode = #_mdcunits 
* #270688 ^property[1].code = #hints 
* #270688 ^property[1].valueString = "PART: 4 ~ CODE10: 8544" 
* #270720 "MDC_DIM_MMHG_MIN_PER_L"
* #270720 ^definition = mm[Hg].min/L [wood'U]
* #270720 ^designation[0].language = #de-AT 
* #270720 ^designation[0].value = " Vascular Resistance (Wood's unit; pediatric)" 
* #270720 ^property[0].code = #parent 
* #270720 ^property[0].valueCode = #_mdcunits 
* #270720 ^property[1].code = #hints 
* #270720 ^property[1].valueString = "PART: 4 ~ CODE10: 8576" 
* #270752 "MDC_DIM_BIT"
* #270752 ^definition = bit
* #270752 ^designation[0].language = #de-AT 
* #270752 ^designation[0].value = " Amount of information" 
* #270752 ^property[0].code = #parent 
* #270752 ^property[0].valueCode = #_mdcunits 
* #270752 ^property[1].code = #hints 
* #270752 ^property[1].valueString = "PART: 4 ~ CODE10: 8608" 
* #270784 "MDC_DIM_BYTE"
* #270784 ^definition = By
* #270784 ^designation[0].language = #de-AT 
* #270784 ^designation[0].value = " Amount of information" 
* #270784 ^property[0].code = #parent 
* #270784 ^property[0].valueCode = #_mdcunits 
* #270784 ^property[1].code = #hints 
* #270784 ^property[1].valueString = "PART: 4 ~ CODE10: 8640" 
* #270816 "MDC_DIM_DROPS_PER_X_L"
* #270816 ^definition = [drp]/L
* #270816 ^designation[0].language = #de-AT 
* #270816 ^designation[0].value = " Volume concentration" 
* #270816 ^property[0].code = #parent 
* #270816 ^property[0].valueCode = #_mdcunits 
* #270816 ^property[1].code = #hints 
* #270816 ^property[1].valueString = "PART: 4 ~ CODE10: 8672" 
* #270834 "MDC_DIM_DROPS_PER_MILLI_L"
* #270834 ^definition = [drp]/mL
* #270834 ^designation[0].language = #de-AT 
* #270834 ^designation[0].value = " Volume concentration" 
* #270834 ^property[0].code = #parent 
* #270834 ^property[0].valueCode = #_mdcunits 
* #270834 ^property[1].code = #hints 
* #270834 ^property[1].valueString = "PART: 4 ~ CODE10: 8690" 
* #270866 "MDC_DIM_BREATHS_PER_MIN_PER_MILLI_L"
* #270866 ^definition = /min/mL {breaths}/min/mL {breath}/min/mL
* #270866 ^property[0].code = #parent 
* #270866 ^property[0].valueCode = #_mdcunits 
* #270866 ^property[1].code = #hints 
* #270866 ^property[1].valueString = "PART: 4 ~ CODE10: 8722" 
* #270880 "MDC_DIM_SQUARE_BREATHS_PER_MIN_PER_L"
* #270880 ^definition = /min/L {breaths-squared}/min/L
* #270880 ^designation[0].language = #de-AT 
* #270880 ^designation[0].value = " Shallow Breathing Index (breaths squared)" 
* #270880 ^property[0].code = #parent 
* #270880 ^property[0].valueCode = #_mdcunits 
* #270880 ^property[1].code = #hints 
* #270880 ^property[1].valueString = "PART: 4 ~ CODE10: 8736" 
* #270912 "MDC_DIM_X_L_PER_MIN_PER_KG"
* #270912 ^definition = L/min/kg
* #270912 ^designation[0].language = #de-AT 
* #270912 ^designation[0].value = " Volume per minute per body weight" 
* #270912 ^property[0].code = #parent 
* #270912 ^property[0].valueCode = #_mdcunits 
* #270912 ^property[1].code = #hints 
* #270912 ^property[1].valueString = "PART: 4 ~ CODE10: 8768" 
* #270930 "MDC_DIM_MILLI_L_PER_MIN_PER_KG"
* #270930 ^definition = mL/min/kg
* #270930 ^property[0].code = #parent 
* #270930 ^property[0].valueCode = #_mdcunits 
* #270930 ^property[1].code = #hints 
* #270930 ^property[1].valueString = "PART: 4 ~ CODE10: 8786" 
* #270944 "MDC_DIM_O2_SAT_PERCENT_SEC"
* #270944 ^definition = {sat}.%.s
* #270944 ^designation[0].language = #de-AT 
* #270944 ^designation[0].value = " Oxygen saturation-seconds" 
* #270944 ^property[0].code = #parent 
* #270944 ^property[0].valueCode = #_mdcunits 
* #270944 ^property[1].code = #hints 
* #270944 ^property[1].valueString = "PART: 4 ~ CODE10: 8800" 
* #270976 "MDC_DIM_X_M_PER_VOLT"
* #270976 ^definition = m/V mm/mV
* #270976 ^designation[0].language = #de-AT 
* #270976 ^designation[0].value = " millimeters per millivolt  (display gain)" 
* #270976 ^property[0].code = #parent 
* #270976 ^property[0].valueCode = #_mdcunits 
* #270976 ^property[1].code = #hints 
* #270976 ^property[1].valueString = "PART: 4 ~ CODE10: 8832" 
* #271008 "MDC_DIM_X_G_FORCE_M"
* #271008 ^definition = gf.m
* #271008 ^designation[0].language = #de-AT 
* #271008 ^designation[0].value = " Cardiac stroke work" 
* #271008 ^property[0].code = #parent 
* #271008 ^property[0].valueCode = #_mdcunits 
* #271008 ^property[1].code = #hints 
* #271008 ^property[1].valueString = "PART: 4 ~ CODE10: 8864" 
* #271011 "MDC_DIM_KILO_G_FORCE_M"
* #271011 ^definition = kgf.m
* #271011 ^designation[0].language = #de-AT 
* #271011 ^designation[0].value = " Cardiac stroke work" 
* #271011 ^property[0].code = #parent 
* #271011 ^property[0].valueCode = #_mdcunits 
* #271011 ^property[1].code = #hints 
* #271011 ^property[1].valueString = "PART: 4 ~ CODE10: 8867" 
* #271040 "MDC_DIM_X_G_FORCE_M_PER_L"
* #271040 ^definition = gf.m/L
* #271040 ^designation[0].language = #de-AT 
* #271040 ^designation[0].value = " Cardiac stroke work per liter of fluid" 
* #271040 ^property[0].code = #parent 
* #271040 ^property[0].valueCode = #_mdcunits 
* #271040 ^property[1].code = #hints 
* #271040 ^property[1].valueString = "PART: 4 ~ CODE10: 8896" 
* #271043 "MDC_DIM_KILO_G_FORCE_M_PER_L"
* #271043 ^definition = kgf.m/L
* #271043 ^designation[0].language = #de-AT 
* #271043 ^designation[0].value = " Cardiac stroke work per liter of fluid" 
* #271043 ^property[0].code = #parent 
* #271043 ^property[0].valueCode = #_mdcunits 
* #271043 ^property[1].code = #hints 
* #271043 ^property[1].valueString = "PART: 4 ~ CODE10: 8899" 
* #271072 "MDC_DIM_G_FORCE_M_PER_M_SQ"
* #271072 ^definition = gf.m/m2
* #271072 ^designation[0].language = #de-AT 
* #271072 ^designation[0].value = " Cardiac Stroke Work per Body Surface Area" 
* #271072 ^property[0].code = #parent 
* #271072 ^property[0].valueCode = #_mdcunits 
* #271072 ^property[1].code = #hints 
* #271072 ^property[1].valueString = "PART: 4 ~ CODE10: 8928" 
* #271075 "MDC_DIM_KILO_G_FORCE_M_PER_M_SQ"
* #271075 ^definition = kgf.m/m2
* #271075 ^designation[0].language = #de-AT 
* #271075 ^designation[0].value = " Cardiac Stroke Work per Body Surface Area" 
* #271075 ^property[0].code = #parent 
* #271075 ^property[0].valueCode = #_mdcunits 
* #271075 ^property[1].code = #hints 
* #271075 ^property[1].valueString = "PART: 4 ~ CODE10: 8931" 
* #271200 "MDC_DIM_X_ARB_UNIT"
* #271200 ^definition = [arb'U]
* #271200 ^designation[0].language = #de-AT 
* #271200 ^designation[0].value = " Arbitrary Unit (arb?U)  " 
* #271200 ^property[0].code = #parent 
* #271200 ^property[0].valueCode = #_mdcunits 
* #271200 ^property[1].code = #hints 
* #271200 ^property[1].valueString = "PART: 4 ~ CODE10: 9056" 
* #271203 "MDC_DIM_KILO_ARB_UNIT"
* #271203 ^definition = k[arb'U]
* #271203 ^designation[0].language = #de-AT 
* #271203 ^designation[0].value = " Arbitrary Unit (arb?U)  " 
* #271203 ^property[0].code = #parent 
* #271203 ^property[0].valueCode = #_mdcunits 
* #271203 ^property[1].code = #hints 
* #271203 ^property[1].valueString = "PART: 4 ~ CODE10: 9059" 
* #271204 "MDC_DIM_MEGA_ARB_UNIT"
* #271204 ^definition = M[arb'U]
* #271204 ^designation[0].language = #de-AT 
* #271204 ^designation[0].value = " Arbitrary Unit (arb?U)  " 
* #271204 ^property[0].code = #parent 
* #271204 ^property[0].valueCode = #_mdcunits 
* #271204 ^property[1].code = #hints 
* #271204 ^property[1].valueString = "PART: 4 ~ CODE10: 9060" 
* #271218 "MDC_DIM_MILLI_ARB_UNIT"
* #271218 ^definition = m[arb'U]
* #271218 ^designation[0].language = #de-AT 
* #271218 ^designation[0].value = " Arbitrary Unit (arb?U)  " 
* #271218 ^property[0].code = #parent 
* #271218 ^property[0].valueCode = #_mdcunits 
* #271218 ^property[1].code = #hints 
* #271218 ^property[1].valueString = "PART: 4 ~ CODE10: 9074" 
* #271232 "MDC_DIM_X_ARB_UNIT_PER_CM_CUBE"
* #271232 ^definition = [arb'U]/cm3
* #271232 ^designation[0].language = #de-AT 
* #271232 ^designation[0].value = " arb'U  concentration" 
* #271232 ^property[0].code = #parent 
* #271232 ^property[0].valueCode = #_mdcunits 
* #271232 ^property[1].code = #hints 
* #271232 ^property[1].valueString = "PART: 4 ~ CODE10: 9088" 
* #271264 "MDC_DIM_X_ARB_UNIT_PER_M_CUBE"
* #271264 ^definition = [arb'U]/m3
* #271264 ^designation[0].language = #de-AT 
* #271264 ^designation[0].value = " arb'U  concentration" 
* #271264 ^property[0].code = #parent 
* #271264 ^property[0].valueCode = #_mdcunits 
* #271264 ^property[1].code = #hints 
* #271264 ^property[1].valueString = "PART: 4 ~ CODE10: 9120" 
* #271296 "MDC_DIM_X_ARB_UNIT_PER_L"
* #271296 ^definition = [arb'U]/L
* #271296 ^designation[0].language = #de-AT 
* #271296 ^designation[0].value = " arb'U  concentration" 
* #271296 ^property[0].code = #parent 
* #271296 ^property[0].valueCode = #_mdcunits 
* #271296 ^property[1].code = #hints 
* #271296 ^property[1].valueString = "PART: 4 ~ CODE10: 9152" 
* #271328 "MDC_DIM_X_ARB_UNIT_PER_ML"
* #271328 ^definition = [arb'U]/mL
* #271328 ^designation[0].language = #de-AT 
* #271328 ^designation[0].value = " arb'U  concentration" 
* #271328 ^property[0].code = #parent 
* #271328 ^property[0].valueCode = #_mdcunits 
* #271328 ^property[1].code = #hints 
* #271328 ^property[1].valueString = "PART: 4 ~ CODE10: 9184" 
* #271331 "MDC_DIM_KILO_ARB_UNIT_PER_ML"
* #271331 ^definition = k[arb'U]/mL
* #271331 ^designation[0].language = #de-AT 
* #271331 ^designation[0].value = " arb'U  concentration" 
* #271331 ^property[0].code = #parent 
* #271331 ^property[0].valueCode = #_mdcunits 
* #271331 ^property[1].code = #hints 
* #271331 ^property[1].valueString = "PART: 4 ~ CODE10: 9187" 
* #271332 "MDC_DIM_MEGA_ARB_UNIT_PER_ML"
* #271332 ^definition = M[arb'U]/mL
* #271332 ^designation[0].language = #de-AT 
* #271332 ^designation[0].value = " arb'U  concentration" 
* #271332 ^property[0].code = #parent 
* #271332 ^property[0].valueCode = #_mdcunits 
* #271332 ^property[1].code = #hints 
* #271332 ^property[1].valueString = "PART: 4 ~ CODE10: 9188" 
* #271346 "MDC_DIM_MILLI_ARB_UNIT_PER_ML"
* #271346 ^definition = m[arb'U]/mL
* #271346 ^designation[0].language = #de-AT 
* #271346 ^designation[0].value = " arb'U  concentration" 
* #271346 ^property[0].code = #parent 
* #271346 ^property[0].valueCode = #_mdcunits 
* #271346 ^property[1].code = #hints 
* #271346 ^property[1].valueString = "PART: 4 ~ CODE10: 9202" 
* #271360 "MDC_DIM_X_ARB_UNIT_PER_SEC"
* #271360 ^definition = [arb'U]/s
* #271360 ^designation[0].language = #de-AT 
* #271360 ^designation[0].value = " arb'U  mass flow rate" 
* #271360 ^property[0].code = #parent 
* #271360 ^property[0].valueCode = #_mdcunits 
* #271360 ^property[1].code = #hints 
* #271360 ^property[1].valueString = "PART: 4 ~ CODE10: 9216" 
* #271364 "MDC_DIM_MEGA_ARB_UNIT_PER_SEC"
* #271364 ^definition = M[arb'U]/s
* #271364 ^designation[0].language = #de-AT 
* #271364 ^designation[0].value = " arb'U  mass flow rate" 
* #271364 ^property[0].code = #parent 
* #271364 ^property[0].valueCode = #_mdcunits 
* #271364 ^property[1].code = #hints 
* #271364 ^property[1].valueString = "PART: 4 ~ CODE10: 9220" 
* #271392 "MDC_DIM_X_ARB_UNIT_PER_MIN"
* #271392 ^definition = [arb'U]/min
* #271392 ^designation[0].language = #de-AT 
* #271392 ^designation[0].value = " arb'U  mass flow rate" 
* #271392 ^property[0].code = #parent 
* #271392 ^property[0].valueCode = #_mdcunits 
* #271392 ^property[1].code = #hints 
* #271392 ^property[1].valueString = "PART: 4 ~ CODE10: 9248" 
* #271396 "MDC_DIM_MEGA_ARB_UNIT_PER_MIN"
* #271396 ^definition = M[arb'U]/min
* #271396 ^designation[0].language = #de-AT 
* #271396 ^designation[0].value = " arb'U  mass flow rate" 
* #271396 ^property[0].code = #parent 
* #271396 ^property[0].valueCode = #_mdcunits 
* #271396 ^property[1].code = #hints 
* #271396 ^property[1].valueString = "PART: 4 ~ CODE10: 9252" 
* #271410 "MDC_DIM_MILLI_ARB_UNIT_PER_MIN"
* #271410 ^definition = m[arb'U]/min
* #271410 ^designation[0].language = #de-AT 
* #271410 ^designation[0].value = " arb'U  mass flow rate" 
* #271410 ^property[0].code = #parent 
* #271410 ^property[0].valueCode = #_mdcunits 
* #271410 ^property[1].code = #hints 
* #271410 ^property[1].valueString = "PART: 4 ~ CODE10: 9266" 
* #271424 "MDC_DIM_X_ARB_UNIT_PER_HR"
* #271424 ^definition = [arb'U]/h
* #271424 ^designation[0].language = #de-AT 
* #271424 ^designation[0].value = " arb'U  mass flow rate" 
* #271424 ^property[0].code = #parent 
* #271424 ^property[0].valueCode = #_mdcunits 
* #271424 ^property[1].code = #hints 
* #271424 ^property[1].valueString = "PART: 4 ~ CODE10: 9280" 
* #271427 "MDC_DIM_KILO_ARB_UNIT_PER_HR"
* #271427 ^definition = k[arb'U]/h
* #271427 ^designation[0].language = #de-AT 
* #271427 ^designation[0].value = " arb'U  mass flow rate" 
* #271427 ^property[0].code = #parent 
* #271427 ^property[0].valueCode = #_mdcunits 
* #271427 ^property[1].code = #hints 
* #271427 ^property[1].valueString = "PART: 4 ~ CODE10: 9283" 
* #271428 "MDC_DIM_MEGA_ARB_UNIT_PER_HR"
* #271428 ^definition = M[arb'U]/h
* #271428 ^designation[0].language = #de-AT 
* #271428 ^designation[0].value = " arb'U  mass flow rate" 
* #271428 ^property[0].code = #parent 
* #271428 ^property[0].valueCode = #_mdcunits 
* #271428 ^property[1].code = #hints 
* #271428 ^property[1].valueString = "PART: 4 ~ CODE10: 9284" 
* #271442 "MDC_DIM_MILLI_ARB_UNIT_PER_HR"
* #271442 ^definition = m[arb'U]/h
* #271442 ^designation[0].language = #de-AT 
* #271442 ^designation[0].value = " arb'U  mass flow rate" 
* #271442 ^property[0].code = #parent 
* #271442 ^property[0].valueCode = #_mdcunits 
* #271442 ^property[1].code = #hints 
* #271442 ^property[1].valueString = "PART: 4 ~ CODE10: 9298" 
* #271456 "MDC_DIM_X_ARB_UNIT_PER_DAY"
* #271456 ^definition = [arb'U]/d
* #271456 ^designation[0].language = #de-AT 
* #271456 ^designation[0].value = " arb'U  mass flow rate" 
* #271456 ^property[0].code = #parent 
* #271456 ^property[0].valueCode = #_mdcunits 
* #271456 ^property[1].code = #hints 
* #271456 ^property[1].valueString = "PART: 4 ~ CODE10: 9312" 
* #271459 "MDC_DIM_KILO_ARB_UNIT_PER_DAY"
* #271459 ^definition = k[arb'U]/d
* #271459 ^designation[0].language = #de-AT 
* #271459 ^designation[0].value = " arb'U  mass flow rate" 
* #271459 ^property[0].code = #parent 
* #271459 ^property[0].valueCode = #_mdcunits 
* #271459 ^property[1].code = #hints 
* #271459 ^property[1].valueString = "PART: 4 ~ CODE10: 9315" 
* #271460 "MDC_DIM_MEGA_ARB_UNIT_PER_DAY"
* #271460 ^definition = M[arb'U]/d
* #271460 ^designation[0].language = #de-AT 
* #271460 ^designation[0].value = " arb'U  mass flow rate" 
* #271460 ^property[0].code = #parent 
* #271460 ^property[0].valueCode = #_mdcunits 
* #271460 ^property[1].code = #hints 
* #271460 ^property[1].valueString = "PART: 4 ~ CODE10: 9316" 
* #271474 "MDC_DIM_MILLI_ARB_UNIT_PER_DAY"
* #271474 ^definition = m[arb'U]/d
* #271474 ^designation[0].language = #de-AT 
* #271474 ^designation[0].value = " arb'U  mass flow rate" 
* #271474 ^property[0].code = #parent 
* #271474 ^property[0].valueCode = #_mdcunits 
* #271474 ^property[1].code = #hints 
* #271474 ^property[1].valueString = "PART: 4 ~ CODE10: 9330" 
* #271488 "MDC_DIM_X_ARB_UNIT_PER_KG"
* #271488 ^definition = [arb'U]/kg
* #271488 ^designation[0].language = #de-AT 
* #271488 ^designation[0].value = " arb'U  dose quantity per body weight" 
* #271488 ^property[0].code = #parent 
* #271488 ^property[0].valueCode = #_mdcunits 
* #271488 ^property[1].code = #hints 
* #271488 ^property[1].valueString = "PART: 4 ~ CODE10: 9344" 
* #271491 "MDC_DIM_KILO_ARB_UNIT_PER_KG"
* #271491 ^definition = k[arb'U]/kg
* #271491 ^designation[0].language = #de-AT 
* #271491 ^designation[0].value = " arb'U  dose quantity per body weight" 
* #271491 ^property[0].code = #parent 
* #271491 ^property[0].valueCode = #_mdcunits 
* #271491 ^property[1].code = #hints 
* #271491 ^property[1].valueString = "PART: 4 ~ CODE10: 9347" 
* #271492 "MDC_DIM_MEGA_ARB_UNIT_PER_KG"
* #271492 ^definition = M[arb'U]/kg
* #271492 ^designation[0].language = #de-AT 
* #271492 ^designation[0].value = " arb'U  dose quantity per body weight" 
* #271492 ^property[0].code = #parent 
* #271492 ^property[0].valueCode = #_mdcunits 
* #271492 ^property[1].code = #hints 
* #271492 ^property[1].valueString = "PART: 4 ~ CODE10: 9348" 
* #271506 "MDC_DIM_MILLI_ARB_UNIT_PER_KG"
* #271506 ^definition = m[arb'U]/kg
* #271506 ^designation[0].language = #de-AT 
* #271506 ^designation[0].value = " arb'U  dose quantity per body weight" 
* #271506 ^property[0].code = #parent 
* #271506 ^property[0].valueCode = #_mdcunits 
* #271506 ^property[1].code = #hints 
* #271506 ^property[1].valueString = "PART: 4 ~ CODE10: 9362" 
* #271520 "MDC_DIM_X_ARB_UNIT_PER_M_SQ"
* #271520 ^definition = [arb'U]/m2
* #271520 ^designation[0].language = #de-AT 
* #271520 ^designation[0].value = " arb'U  dose quantity per body surface area" 
* #271520 ^property[0].code = #parent 
* #271520 ^property[0].valueCode = #_mdcunits 
* #271520 ^property[1].code = #hints 
* #271520 ^property[1].valueString = "PART: 4 ~ CODE10: 9376" 
* #271524 "MDC_DIM_MEGA_ARB_UNIT_PER_M_SQ"
* #271524 ^definition = M[arb'U]/m2
* #271524 ^designation[0].language = #de-AT 
* #271524 ^designation[0].value = " arb'U  dose quantity per body surface area" 
* #271524 ^property[0].code = #parent 
* #271524 ^property[0].valueCode = #_mdcunits 
* #271524 ^property[1].code = #hints 
* #271524 ^property[1].valueString = "PART: 4 ~ CODE10: 9380" 
* #271538 "MDC_DIM_MILLI_ARB_UNIT_PER_M_SQ"
* #271538 ^definition = m[arb'U]/m2
* #271538 ^designation[0].language = #de-AT 
* #271538 ^designation[0].value = " arb'U  dose quantity per body surface area" 
* #271538 ^property[0].code = #parent 
* #271538 ^property[0].valueCode = #_mdcunits 
* #271538 ^property[1].code = #hints 
* #271538 ^property[1].valueString = "PART: 4 ~ CODE10: 9394" 
* #271552 "MDC_DIM_X_ARB_UNIT_PER_KG_PER_SEC"
* #271552 ^definition = [arb'U]/kg/s
* #271552 ^designation[0].language = #de-AT 
* #271552 ^designation[0].value = " arb'U  dose rate per body weight" 
* #271552 ^property[0].code = #parent 
* #271552 ^property[0].valueCode = #_mdcunits 
* #271552 ^property[1].code = #hints 
* #271552 ^property[1].valueString = "PART: 4 ~ CODE10: 9408" 
* #271556 "MDC_DIM_MEGA_ARB_UNIT_PER_KG_PER_SEC"
* #271556 ^definition = M[arb'U]/kg/s
* #271556 ^designation[0].language = #de-AT 
* #271556 ^designation[0].value = " arb'U  dose rate per body weight" 
* #271556 ^property[0].code = #parent 
* #271556 ^property[0].valueCode = #_mdcunits 
* #271556 ^property[1].code = #hints 
* #271556 ^property[1].valueString = "PART: 4 ~ CODE10: 9412" 
* #271584 "MDC_DIM_X_ARB_UNIT_PER_KG_PER_MIN"
* #271584 ^definition = [arb'U]/kg/min
* #271584 ^designation[0].language = #de-AT 
* #271584 ^designation[0].value = " arb'U  dose rate per body weight" 
* #271584 ^property[0].code = #parent 
* #271584 ^property[0].valueCode = #_mdcunits 
* #271584 ^property[1].code = #hints 
* #271584 ^property[1].valueString = "PART: 4 ~ CODE10: 9440" 
* #271588 "MDC_DIM_MEGA_ARB_UNIT_PER_KG_PER_MIN"
* #271588 ^definition = M[arb'U]/kg/min
* #271588 ^designation[0].language = #de-AT 
* #271588 ^designation[0].value = " arb'U  dose rate per body weight" 
* #271588 ^property[0].code = #parent 
* #271588 ^property[0].valueCode = #_mdcunits 
* #271588 ^property[1].code = #hints 
* #271588 ^property[1].valueString = "PART: 4 ~ CODE10: 9444" 
* #271602 "MDC_DIM_MILLI_ARB_UNIT_PER_KG_PER_MIN"
* #271602 ^definition = m[arb'U]/kg/min
* #271602 ^designation[0].language = #de-AT 
* #271602 ^designation[0].value = " arb'U  dose rate per body weight" 
* #271602 ^property[0].code = #parent 
* #271602 ^property[0].valueCode = #_mdcunits 
* #271602 ^property[1].code = #hints 
* #271602 ^property[1].valueString = "PART: 4 ~ CODE10: 9458" 
* #271616 "MDC_DIM_X_ARB_UNIT_PER_KG_PER_HR"
* #271616 ^definition = [arb'U]/kg/h
* #271616 ^designation[0].language = #de-AT 
* #271616 ^designation[0].value = " arb'U  dose rate per body weight" 
* #271616 ^property[0].code = #parent 
* #271616 ^property[0].valueCode = #_mdcunits 
* #271616 ^property[1].code = #hints 
* #271616 ^property[1].valueString = "PART: 4 ~ CODE10: 9472" 
* #271619 "MDC_DIM_KILO_ARB_UNIT_PER_KG_PER_HR"
* #271619 ^definition = k[arb'U]/kg/h
* #271619 ^designation[0].language = #de-AT 
* #271619 ^designation[0].value = " arb'U  mass flow rate" 
* #271619 ^property[0].code = #parent 
* #271619 ^property[0].valueCode = #_mdcunits 
* #271619 ^property[1].code = #hints 
* #271619 ^property[1].valueString = "PART: 4 ~ CODE10: 9475" 
* #271620 "MDC_DIM_MEGA_ARB_UNIT_PER_KG_PER_HR"
* #271620 ^definition = M[arb'U]/kg/h
* #271620 ^designation[0].language = #de-AT 
* #271620 ^designation[0].value = " arb'U  dose rate per body weight" 
* #271620 ^property[0].code = #parent 
* #271620 ^property[0].valueCode = #_mdcunits 
* #271620 ^property[1].code = #hints 
* #271620 ^property[1].valueString = "PART: 4 ~ CODE10: 9476" 
* #271634 "MDC_DIM_MILLI_ARB_UNIT_PER_KG_PER_HR"
* #271634 ^definition = m[arb'U]/kg/h
* #271634 ^designation[0].language = #de-AT 
* #271634 ^designation[0].value = " arb'U  dose rate per body weight" 
* #271634 ^property[0].code = #parent 
* #271634 ^property[0].valueCode = #_mdcunits 
* #271634 ^property[1].code = #hints 
* #271634 ^property[1].valueString = "PART: 4 ~ CODE10: 9490" 
* #271648 "MDC_DIM_X_ARB_UNIT_PER_KG_PER_DAY"
* #271648 ^definition = [arb'U]/kg/d
* #271648 ^designation[0].language = #de-AT 
* #271648 ^designation[0].value = " arb'U  dose rate per body weight" 
* #271648 ^property[0].code = #parent 
* #271648 ^property[0].valueCode = #_mdcunits 
* #271648 ^property[1].code = #hints 
* #271648 ^property[1].valueString = "PART: 4 ~ CODE10: 9504" 
* #271651 "MDC_DIM_KILO_ARB_UNIT_PER_KG_PER_DAY"
* #271651 ^definition = k[arb'U]/kg/d
* #271651 ^designation[0].language = #de-AT 
* #271651 ^designation[0].value = " arb'U  mass flow rate" 
* #271651 ^property[0].code = #parent 
* #271651 ^property[0].valueCode = #_mdcunits 
* #271651 ^property[1].code = #hints 
* #271651 ^property[1].valueString = "PART: 4 ~ CODE10: 9507" 
* #271652 "MDC_DIM_MEGA_ARB_UNIT_PER_KG_PER_DAY"
* #271652 ^definition = M[arb'U]/kg/d
* #271652 ^designation[0].language = #de-AT 
* #271652 ^designation[0].value = " arb'U  dose rate per body weight" 
* #271652 ^property[0].code = #parent 
* #271652 ^property[0].valueCode = #_mdcunits 
* #271652 ^property[1].code = #hints 
* #271652 ^property[1].valueString = "PART: 4 ~ CODE10: 9508" 
* #271666 "MDC_DIM_MILLI_ARB_UNIT_PER_KG_PER_DAY"
* #271666 ^definition = m[arb'U]/kg/d
* #271666 ^designation[0].language = #de-AT 
* #271666 ^designation[0].value = " arb'U  dose rate per body weight" 
* #271666 ^property[0].code = #parent 
* #271666 ^property[0].valueCode = #_mdcunits 
* #271666 ^property[1].code = #hints 
* #271666 ^property[1].valueString = "PART: 4 ~ CODE10: 9522" 
* #271680 "MDC_DIM_X_ARB_UNIT_PER_LB_PER_MIN"
* #271680 ^definition = [arb'U]/[lb_av]/min
* #271680 ^designation[0].language = #de-AT 
* #271680 ^designation[0].value = " arb'U  dose rate per body weight" 
* #271680 ^property[0].code = #parent 
* #271680 ^property[0].valueCode = #_mdcunits 
* #271680 ^property[1].code = #hints 
* #271680 ^property[1].valueString = "PART: 4 ~ CODE10: 9536" 
* #271698 "MDC_DIM_MILLI_ARB_UNIT_PER_LB_PER_MIN"
* #271698 ^definition = m[arb'U]/[lb_av]/min
* #271698 ^designation[0].language = #de-AT 
* #271698 ^designation[0].value = " arb'U  dose rate per body weight" 
* #271698 ^property[0].code = #parent 
* #271698 ^property[0].valueCode = #_mdcunits 
* #271698 ^property[1].code = #hints 
* #271698 ^property[1].valueString = "PART: 4 ~ CODE10: 9554" 
* #271712 "MDC_DIM_X_ARB_UNIT_PER_LB_PER_DAY"
* #271712 ^definition = [arb'U]/[lb_av]/d
* #271712 ^designation[0].language = #de-AT 
* #271712 ^designation[0].value = " arb'U  dose rate per body weight" 
* #271712 ^property[0].code = #parent 
* #271712 ^property[0].valueCode = #_mdcunits 
* #271712 ^property[1].code = #hints 
* #271712 ^property[1].valueString = "PART: 4 ~ CODE10: 9568" 
* #271744 "MDC_DIM_X_ARB_UNIT_PER_M_SQ_PER_SEC"
* #271744 ^definition = [arb'U]/m2/s
* #271744 ^designation[0].language = #de-AT 
* #271744 ^designation[0].value = " arb'U  dose rate per body surface area" 
* #271744 ^property[0].code = #parent 
* #271744 ^property[0].valueCode = #_mdcunits 
* #271744 ^property[1].code = #hints 
* #271744 ^property[1].valueString = "PART: 4 ~ CODE10: 9600" 
* #271748 "MDC_DIM_MEGA_ARB_UNIT_PER_M_SQ_PER_SEC"
* #271748 ^definition = M[arb'U]/m2/s
* #271748 ^designation[0].language = #de-AT 
* #271748 ^designation[0].value = " arb'U  dose rate per body surface area" 
* #271748 ^property[0].code = #parent 
* #271748 ^property[0].valueCode = #_mdcunits 
* #271748 ^property[1].code = #hints 
* #271748 ^property[1].valueString = "PART: 4 ~ CODE10: 9604" 
* #271762 "MDC_DIM_MILLI_ARB_UNIT_PER_M_SQ_PER_SEC"
* #271762 ^definition = m[arb'U]/m2/s
* #271762 ^designation[0].language = #de-AT 
* #271762 ^designation[0].value = " arb'U  dose rate per body surface area" 
* #271762 ^property[0].code = #parent 
* #271762 ^property[0].valueCode = #_mdcunits 
* #271762 ^property[1].code = #hints 
* #271762 ^property[1].valueString = "PART: 4 ~ CODE10: 9618" 
* #271776 "MDC_DIM_X_ARB_UNIT_PER_M_SQ_PER_MIN"
* #271776 ^definition = [arb'U]/m2/min
* #271776 ^designation[0].language = #de-AT 
* #271776 ^designation[0].value = " arb'U  dose rate per body surface area" 
* #271776 ^property[0].code = #parent 
* #271776 ^property[0].valueCode = #_mdcunits 
* #271776 ^property[1].code = #hints 
* #271776 ^property[1].valueString = "PART: 4 ~ CODE10: 9632" 
* #271780 "MDC_DIM_MEGA_ARB_UNIT_PER_M_SQ_PER_MIN"
* #271780 ^definition = M[arb'U]/m2/min
* #271780 ^designation[0].language = #de-AT 
* #271780 ^designation[0].value = " arb'U  dose rate per body surface area" 
* #271780 ^property[0].code = #parent 
* #271780 ^property[0].valueCode = #_mdcunits 
* #271780 ^property[1].code = #hints 
* #271780 ^property[1].valueString = "PART: 4 ~ CODE10: 9636" 
* #271794 "MDC_DIM_MILLI_ARB_UNIT_PER_M_SQ_PER_MIN"
* #271794 ^definition = m[arb'U]/m2/min
* #271794 ^designation[0].language = #de-AT 
* #271794 ^designation[0].value = " arb'U  dose rate per body surface area" 
* #271794 ^property[0].code = #parent 
* #271794 ^property[0].valueCode = #_mdcunits 
* #271794 ^property[1].code = #hints 
* #271794 ^property[1].valueString = "PART: 4 ~ CODE10: 9650" 
* #271808 "MDC_DIM_X_ARB_UNIT_PER_M_SQ_PER_HR"
* #271808 ^definition = [arb'U]/m2/h
* #271808 ^designation[0].language = #de-AT 
* #271808 ^designation[0].value = " arb'U  dose rate per body surface area" 
* #271808 ^property[0].code = #parent 
* #271808 ^property[0].valueCode = #_mdcunits 
* #271808 ^property[1].code = #hints 
* #271808 ^property[1].valueString = "PART: 4 ~ CODE10: 9664" 
* #271812 "MDC_DIM_MEGA_ARB_UNIT_PER_M_SQ_PER_HR"
* #271812 ^definition = M[arb'U]/m2/h
* #271812 ^designation[0].language = #de-AT 
* #271812 ^designation[0].value = " arb'U  dose rate per body surface area" 
* #271812 ^property[0].code = #parent 
* #271812 ^property[0].valueCode = #_mdcunits 
* #271812 ^property[1].code = #hints 
* #271812 ^property[1].valueString = "PART: 4 ~ CODE10: 9668" 
* #271826 "MDC_DIM_MILLI_ARB_UNIT_PER_M_SQ_PER_HR"
* #271826 ^definition = m[arb'U]/m2/h
* #271826 ^designation[0].language = #de-AT 
* #271826 ^designation[0].value = " arb'U  dose rate per body surface area" 
* #271826 ^property[0].code = #parent 
* #271826 ^property[0].valueCode = #_mdcunits 
* #271826 ^property[1].code = #hints 
* #271826 ^property[1].valueString = "PART: 4 ~ CODE10: 9682" 
* #271840 "MDC_DIM_X_ARB_UNIT_PER_M_SQ_PER_DAY"
* #271840 ^definition = [arb'U]/m2/d
* #271840 ^designation[0].language = #de-AT 
* #271840 ^designation[0].value = " arb'U  dose rate per body surface area" 
* #271840 ^property[0].code = #parent 
* #271840 ^property[0].valueCode = #_mdcunits 
* #271840 ^property[1].code = #hints 
* #271840 ^property[1].valueString = "PART: 4 ~ CODE10: 9696" 
* #271844 "MDC_DIM_MEGA_ARB_UNIT_PER_M_SQ_PER_DAY"
* #271844 ^definition = M[arb'U]/m2/d
* #271844 ^designation[0].language = #de-AT 
* #271844 ^designation[0].value = " arb'U  dose rate per body surface area" 
* #271844 ^property[0].code = #parent 
* #271844 ^property[0].valueCode = #_mdcunits 
* #271844 ^property[1].code = #hints 
* #271844 ^property[1].valueString = "PART: 4 ~ CODE10: 9700" 
* #271858 "MDC_DIM_MILLI_ARB_UNIT_PER_M_SQ_PER_DAY"
* #271858 ^definition = m[arb'U]/m2/d
* #271858 ^designation[0].language = #de-AT 
* #271858 ^designation[0].value = " arb'U  dose rate per body surface area" 
* #271858 ^property[0].code = #parent 
* #271858 ^property[0].valueCode = #_mdcunits 
* #271858 ^property[1].code = #hints 
* #271858 ^property[1].valueString = "PART: 4 ~ CODE10: 9714" 
* #272544 "MDC_DIM_X_INTL_UNIT_PER_LB_PER_HR"
* #272544 ^definition = [iU]/[lb_av]/h
* #272544 ^designation[0].language = #de-AT 
* #272544 ^designation[0].value = " - IU dose rate per body weight" 
* #272544 ^property[0].code = #parent 
* #272544 ^property[0].valueCode = #_mdcunits 
* #272544 ^property[1].code = #hints 
* #272544 ^property[1].valueString = "PART: 4 ~ CODE10: 10400" 
* #272562 "MDC_DIM_MILLI_INTL_UNIT_PER_LB_PER_HR"
* #272562 ^definition = m[iU]/[lb_av]/h
* #272562 ^designation[0].language = #de-AT 
* #272562 ^designation[0].value = " - IU dose rate per body weight" 
* #272562 ^property[0].code = #parent 
* #272562 ^property[0].valueCode = #_mdcunits 
* #272562 ^property[1].code = #hints 
* #272562 ^property[1].valueString = "PART: 4 ~ CODE10: 10418" 
* #272576 "MDC_DIM_X_ARB_UNIT_PER_LB_PER_HR"
* #272576 ^definition = [arb'U]/[lb_av]/h
* #272576 ^designation[0].language = #de-AT 
* #272576 ^designation[0].value = " arb'U  dose rate per body weight" 
* #272576 ^property[0].code = #parent 
* #272576 ^property[0].valueCode = #_mdcunits 
* #272576 ^property[1].code = #hints 
* #272576 ^property[1].valueString = "PART: 4 ~ CODE10: 10432" 
* #272594 "MDC_DIM_MILLI_ARB_UNIT_PER_LB_PER_HR"
* #272594 ^definition = m[arb'U]/[lb_av]/h
* #272594 ^designation[0].language = #de-AT 
* #272594 ^designation[0].value = " arb'U  dose rate per body weight" 
* #272594 ^property[0].code = #parent 
* #272594 ^property[0].valueCode = #_mdcunits 
* #272594 ^property[1].code = #hints 
* #272594 ^property[1].valueString = "PART: 4 ~ CODE10: 10450" 
* #272640 "MDC_DIM_JOULES_PER_DAY"
* #272640 ^definition = J/d
* #272640 ^property[0].code = #parent 
* #272640 ^property[0].valueCode = #_mdcunits 
* #272640 ^property[1].code = #hints 
* #272640 ^property[1].valueString = "PART: 4 ~ CODE10: 10496" 
* #272672 "MDC_DIM_JOULES_PER_ML"
* #272672 ^definition = J/mL
* #272672 ^property[0].code = #parent 
* #272672 ^property[0].valueCode = #_mdcunits 
* #272672 ^property[1].code = #hints 
* #272672 ^property[1].valueString = "PART: 4 ~ CODE10: 10528" 
* #272704 "MDC_DIM_VOL_PERCENT_PER_L"
* #272704 ^definition = %{vol}/L
* #272704 ^designation[0].language = #de-AT 
* #272704 ^designation[0].value = " Slope of component gas concentration per expired total gas volume" 
* #272704 ^property[0].code = #parent 
* #272704 ^property[0].valueCode = #_mdcunits 
* #272704 ^property[1].code = #hints 
* #272704 ^property[1].valueString = "PART: 4 ~ CODE10: 10560" 
* #272739 "MDC_DIM_KILO_PASCAL_PER_L"
* #272739 ^definition = kPa/L
* #272739 ^property[0].code = #parent 
* #272739 ^property[0].valueCode = #_mdcunits 
* #272739 ^property[1].code = #hints 
* #272739 ^property[1].valueString = "PART: 4 ~ CODE10: 10595" 
* #272914 "MDC_DIM_MILLI_BAR_SEC_PER_L"
* #272914 ^definition = mbar.s/L mbar/(L/s)
* #272914 ^property[0].code = #parent 
* #272914 ^property[0].valueCode = #_mdcunits 
* #272914 ^property[1].code = #hints 
* #272914 ^property[1].valueString = "PART: 4 ~ CODE10: 10770" 
* #272978 "MDC_DIM_MILLI_L_PER_HPA"
* #272978 ^definition = mL/hPa
* #272978 ^property[0].code = #parent 
* #272978 ^property[0].valueCode = #_mdcunits 
* #272978 ^property[1].code = #hints 
* #272978 ^property[1].valueString = "PART: 4 ~ CODE10: 10834" 
* #272992 "MDC_DIM_L_PER_BAR"
* #272992 ^definition = L/bar
* #272992 ^property[0].code = #parent 
* #272992 ^property[0].valueCode = #_mdcunits 
* #272992 ^property[1].code = #hints 
* #272992 ^property[1].valueString = "PART: 4 ~ CODE10: 10848" 
* #273024 "MDC_DIM_L_PER_MBAR"
* #273024 ^definition = L/mbar
* #273024 ^property[0].code = #parent 
* #273024 ^property[0].valueCode = #_mdcunits 
* #273024 ^property[1].code = #hints 
* #273024 ^property[1].valueString = "PART: 4 ~ CODE10: 10880" 
* #273042 "MDC_DIM_MILLI_L_PER_MBAR"
* #273042 ^definition = mL/mbar
* #273042 ^property[0].code = #parent 
* #273042 ^property[0].valueCode = #_mdcunits 
* #273042 ^property[1].code = #hints 
* #273042 ^property[1].valueString = "PART: 4 ~ CODE10: 10898" 
* #273056 "MDC_DIM_JOULES_PER_L_PER_SEC"
* #273056 ^definition = J/L/s
* #273056 ^property[0].code = #parent 
* #273056 ^property[0].valueCode = #_mdcunits 
* #273056 ^property[1].code = #hints 
* #273056 ^property[1].valueString = "PART: 4 ~ CODE10: 10912" 
* #273376 "MDC_DIM_CM_H2O_SEC_PER_BREATH"
* #273376 ^definition = cm[H2O].s/{breath}
* #273376 ^designation[0].language = #de-AT 
* #273376 ^designation[0].value = " Pressure Time Product" 
* #273376 ^property[0].code = #parent 
* #273376 ^property[0].valueCode = #_mdcunits 
* #273376 ^property[1].code = #hints 
* #273376 ^property[1].valueString = "PART: 4 ~ CODE10: 11232" 
* #273408 "MDC_DIM_CM_H2O_SEC_PER_MIN"
* #273408 ^definition = cm[H2O].s/min
* #273408 ^property[0].code = #parent 
* #273408 ^property[0].valueCode = #_mdcunits 
* #273408 ^property[1].code = #hints 
* #273408 ^property[1].valueString = "PART: 4 ~ CODE10: 11264" 
* #273458 "MDC_DIM_MILLI_BAR_SEC_PER_BREATH"
* #273458 ^definition = mbar.s mbar.s/{breath}
* #273458 ^property[0].code = #parent 
* #273458 ^property[0].valueCode = #_mdcunits 
* #273458 ^property[1].code = #hints 
* #273458 ^property[1].valueString = "PART: 4 ~ CODE10: 11314" 
* #273475 "MDC_DIM_KILO_G_FORCE_M_PER_MIN_PER_M2"
* #273475 ^definition = kgf.m/min/m2
* #273475 ^property[0].code = #parent 
* #273475 ^property[0].valueCode = #_mdcunits 
* #273475 ^property[1].code = #hints 
* #273475 ^property[1].valueString = "PART: 4 ~ CODE10: 11331" 
* #273525 "MDC_DIM_PICO_WATT_PER_HZ"
* #273525 ^definition = pW/Hz
* #273525 ^property[0].code = #parent 
* #273525 ^property[0].valueCode = #_mdcunits 
* #273525 ^property[1].code = #hints 
* #273525 ^property[1].valueString = "PART: 4 ~ CODE10: 11381" 
* #273536 "MDC_DIM_INR"
* #273536 ^definition = {INR}
* #273536 ^property[0].code = #parent 
* #273536 ^property[0].valueCode = #_mdcunits 
* #273536 ^property[1].code = #hints 
* #273536 ^property[1].valueString = "PART: 4 ~ CODE10: 11392" 
* #273568 "MDC_DIM_JOULES_PER_L_PER_KG"
* #273568 ^definition = J/L/kg
* #273568 ^property[0].code = #parent 
* #273568 ^property[0].valueCode = #_mdcunits 
* #273568 ^property[1].code = #hints 
* #273568 ^property[1].valueString = "PART: 4 ~ CODE10: 11424" 
* #273600 "MDC_DIM_JOULES_PER_ML_PER_KG"
* #273600 ^definition = J/mL/kg
* #273600 ^property[0].code = #parent 
* #273600 ^property[0].valueCode = #_mdcunits 
* #273600 ^property[1].code = #hints 
* #273600 ^property[1].valueString = "PART: 4 ~ CODE10: 11456" 
