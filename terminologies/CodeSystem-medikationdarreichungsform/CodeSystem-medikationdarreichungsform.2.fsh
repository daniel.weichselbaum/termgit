Instance: medikationdarreichungsform 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-medikationdarreichungsform" 
* name = "medikationdarreichungsform" 
* title = "MedikationDarreichungsform" 
* status = #active 
* content = #complete 
* version = "20220930" 
* description = "**Description:** Medikation Darreichungsform

**Beschreibung:** Medikation Darreichungsform" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.10.1.4.3.4.3.5" 
* date = "2022-09-30" 
* count = 241 
* #100000073362 "Suspension zum Einnehmen"
* #100000073362 ^designation[0].language = #de-AT 
* #100000073362 ^designation[0].value = "Suspension zum Einnehmen" 
* #100000073363 "Gel zum Einnehmen"
* #100000073363 ^designation[0].language = #de-AT 
* #100000073363 ^designation[0].value = "Gel zum Einnehmen" 
* #100000073364 "Pulver zur Herstellung einer Lösung zum Einnehmen"
* #100000073364 ^designation[0].language = #de-AT 
* #100000073364 ^designation[0].value = "Pulver zur Herstellung einer Lösung zum Einnehmen" 
* #100000073365 "Granulat zur Herstellung einer Lösung zum Einnehmen"
* #100000073365 ^designation[0].language = #de-AT 
* #100000073365 ^designation[0].value = "Granulat zur Herstellung einer Lösung zum Einnehmen" 
* #100000073366 "Pulver und Lösungsmittel zur Herstellung einer Lösung zum Einnehmen"
* #100000073366 ^designation[0].language = #de-AT 
* #100000073366 ^designation[0].value = "Pulver und Lösungsmittel zur Herstellung einer Lösung zum Einnehmen" 
* #100000073369 "Tablette zur Herstellung einer Lösung zum Einnehmen"
* #100000073369 ^designation[0].language = #de-AT 
* #100000073369 ^designation[0].value = "Tablette zur Herstellung einer Lösung zum Einnehmen" 
* #100000073370 "Arzneitee"
* #100000073370 ^designation[0].language = #de-AT 
* #100000073370 ^designation[0].value = "Arzneitee" 
* #100000073371 "Teeaufgusspulver"
* #100000073371 ^designation[0].language = #de-AT 
* #100000073371 ^designation[0].value = "Teeaufgusspulver" 
* #100000073372 "Granulat"
* #100000073372 ^designation[0].language = #de-AT 
* #100000073372 ^designation[0].value = "Granulat" 
* #100000073373 "magensaftresistentes Granulat"
* #100000073373 ^designation[0].language = #de-AT 
* #100000073373 ^designation[0].value = "magensaftresistentes Granulat" 
* #100000073375 "Hartkapsel"
* #100000073375 ^designation[0].language = #de-AT 
* #100000073375 ^designation[0].value = "Hartkapsel" 
* #100000073376 "magensaftresistente Hartkapsel"
* #100000073376 ^designation[0].language = #de-AT 
* #100000073376 ^designation[0].value = "magensaftresistente Hartkapsel" 
* #100000073380 "überzogene Tablette"
* #100000073380 ^designation[0].language = #de-AT 
* #100000073380 ^designation[0].value = "überzogene Tablette" 
* #100000073642 "Tropfen zum Einnehmen, Lösung"
* #100000073642 ^designation[0].language = #de-AT 
* #100000073642 ^designation[0].value = "Tropfen zum Einnehmen, Lösung" 
* #100000073643 "Tropfen zum Einnehmen, Suspension"
* #100000073643 ^designation[0].language = #de-AT 
* #100000073643 ^designation[0].value = "Tropfen zum Einnehmen, Suspension" 
* #100000073645 "Flüssigkeit zum Einnehmen"
* #100000073645 ^designation[0].language = #de-AT 
* #100000073645 ^designation[0].value = "Flüssigkeit zum Einnehmen" 
* #100000073646 "Lösung zum Einnehmen"
* #100000073646 ^designation[0].language = #de-AT 
* #100000073646 ^designation[0].value = "Lösung zum Einnehmen" 
* #100000073647 "Emulsion zum Einnehmen"
* #100000073647 ^designation[0].language = #de-AT 
* #100000073647 ^designation[0].value = "Emulsion zum Einnehmen" 
* #100000073649 "Pulver zur Herstellung einer Suspension zum Einnehmen"
* #100000073649 ^designation[0].language = #de-AT 
* #100000073649 ^designation[0].value = "Pulver zur Herstellung einer Suspension zum Einnehmen" 
* #100000073650 "Granulat zur Herstellung einer Suspension zum Einnehmen"
* #100000073650 ^designation[0].language = #de-AT 
* #100000073650 ^designation[0].value = "Granulat zur Herstellung einer Suspension zum Einnehmen" 
* #100000073652 "Sirup"
* #100000073652 ^designation[0].language = #de-AT 
* #100000073652 ^designation[0].value = "Sirup" 
* #100000073654 "Tablette zur Herstellung einer Suspension zum Einnehmen"
* #100000073654 ^designation[0].language = #de-AT 
* #100000073654 ^designation[0].value = "Tablette zur Herstellung einer Suspension zum Einnehmen" 
* #100000073655 "Pulver zum Einnehmen"
* #100000073655 ^designation[0].language = #de-AT 
* #100000073655 ^designation[0].value = "Pulver zum Einnehmen" 
* #100000073656 "Brausepulver"
* #100000073656 ^designation[0].language = #de-AT 
* #100000073656 ^designation[0].value = "Brausepulver" 
* #100000073657 "Brausegranulat"
* #100000073657 ^designation[0].language = #de-AT 
* #100000073657 ^designation[0].value = "Brausegranulat" 
* #100000073658 "Retardgranulat"
* #100000073658 ^designation[0].language = #de-AT 
* #100000073658 ^designation[0].value = "Retardgranulat" 
* #100000073660 "Weichkapsel"
* #100000073660 ^designation[0].language = #de-AT 
* #100000073660 ^designation[0].value = "Weichkapsel" 
* #100000073661 "magensaftresistente Weichkapsel"
* #100000073661 ^designation[0].language = #de-AT 
* #100000073661 ^designation[0].value = "magensaftresistente Weichkapsel" 
* #100000073662 "Hartkapsel, retardiert"
* #100000073662 ^designation[0].language = #de-AT 
* #100000073662 ^designation[0].value = "Hartkapsel, retardiert" 
* #100000073663 "Hartkapsel mit veränderter Wirkstofffreisetzung"
* #100000073663 ^designation[0].language = #de-AT 
* #100000073663 ^designation[0].value = "Hartkapsel mit veränderter Wirkstofffreisetzung" 
* #100000073664 "Tablette"
* #100000073664 ^designation[0].language = #de-AT 
* #100000073664 ^designation[0].value = "Tablette" 
* #100000073665 "Filmtablette"
* #100000073665 ^designation[0].language = #de-AT 
* #100000073665 ^designation[0].value = "Filmtablette" 
* #100000073666 "Schmelztablette"
* #100000073666 ^designation[0].language = #de-AT 
* #100000073666 ^designation[0].value = "Schmelztablette" 
* #100000073667 "magensaftresistente Tablette"
* #100000073667 ^designation[0].language = #de-AT 
* #100000073667 ^designation[0].value = "magensaftresistente Tablette" 
* #100000073668 "Tablette mit veränderter Wirkstofffreisetzung"
* #100000073668 ^designation[0].language = #de-AT 
* #100000073668 ^designation[0].value = "Tablette mit veränderter Wirkstofffreisetzung" 
* #100000073669 "wirkstoffhaltiges Kaugummi"
* #100000073669 ^designation[0].language = #de-AT 
* #100000073669 ^designation[0].value = "wirkstoffhaltiges Kaugummi" 
* #100000073673 "Gurgellösung"
* #100000073673 ^designation[0].language = #de-AT 
* #100000073673 ^designation[0].value = "Gurgellösung" 
* #100000073675 "Suspension zur Anwendung in der Mundhöhle"
* #100000073675 ^designation[0].language = #de-AT 
* #100000073675 ^designation[0].value = "Suspension zur Anwendung in der Mundhöhle" 
* #100000073676 "Spray zur Anwendung in der Mundhöhle"
* #100000073676 ^designation[0].language = #de-AT 
* #100000073676 ^designation[0].value = "Spray zur Anwendung in der Mundhöhle" 
* #100000073678 "Lösung zur Anwendung am Zahnfleisch"
* #100000073678 ^designation[0].language = #de-AT 
* #100000073678 ^designation[0].value = "Lösung zur Anwendung am Zahnfleisch" 
* #100000073680 "Gel zur Anwendung am Zahnfleisch"
* #100000073680 ^designation[0].language = #de-AT 
* #100000073680 ^designation[0].value = "Gel zur Anwendung am Zahnfleisch" 
* #100000073681 "Brausetablette"
* #100000073681 ^designation[0].language = #de-AT 
* #100000073681 ^designation[0].value = "Brausetablette" 
* #100000073682 "Lyophilisat zum Einnehmen"
* #100000073682 ^designation[0].language = #de-AT 
* #100000073682 ^designation[0].value = "Lyophilisat zum Einnehmen" 
* #100000073683 "Retardtablette"
* #100000073683 ^designation[0].language = #de-AT 
* #100000073683 ^designation[0].value = "Retardtablette" 
* #100000073684 "Kautablette"
* #100000073684 ^designation[0].language = #de-AT 
* #100000073684 ^designation[0].value = "Kautablette" 
* #100000073685 "Lutschpastille"
* #100000073685 ^designation[0].language = #de-AT 
* #100000073685 ^designation[0].value = "Lutschpastille" 
* #100000073691 "Lösung zur Anwendung in der Mundhöhle"
* #100000073691 ^designation[0].language = #de-AT 
* #100000073691 ^designation[0].value = "Lösung zur Anwendung in der Mundhöhle" 
* #100000073693 "Sublingualspray"
* #100000073693 ^designation[0].language = #de-AT 
* #100000073693 ^designation[0].value = "Sublingualspray" 
* #100000073695 "Gel zur Anwendung in der Mundhöhle"
* #100000073695 ^designation[0].language = #de-AT 
* #100000073695 ^designation[0].value = "Gel zur Anwendung in der Mundhöhle" 
* #100000073698 "Sublingualtablette"
* #100000073698 ^designation[0].language = #de-AT 
* #100000073698 ^designation[0].value = "Sublingualtablette" 
* #100000073699 "Buccaltablette"
* #100000073699 ^designation[0].language = #de-AT 
* #100000073699 ^designation[0].value = "Buccaltablette" 
* #100000073700 "Lutschtablette, gepresst"
* #100000073700 ^designation[0].language = #de-AT 
* #100000073700 ^designation[0].value = "Lutschtablette, gepresst" 
* #100000073703 "Lutschtablette"
* #100000073703 ^designation[0].language = #de-AT 
* #100000073703 ^designation[0].value = "Lutschtablette" 
* #100000073704 "Pastille"
* #100000073704 ^designation[0].language = #de-AT 
* #100000073704 ^designation[0].value = "Pastille" 
* #100000073705 "Dentalgel"
* #100000073705 ^designation[0].language = #de-AT 
* #100000073705 ^designation[0].value = "Dentalgel" 
* #100000073710 "Gel zur periodontalen Anwendung"
* #100000073710 ^designation[0].language = #de-AT 
* #100000073710 ^designation[0].value = "Gel zur periodontalen Anwendung" 
* #100000073711 "Badezusatz"
* #100000073711 ^designation[0].language = #de-AT 
* #100000073711 ^designation[0].value = "Badezusatz" 
* #100000073712 "Creme"
* #100000073712 ^designation[0].language = #de-AT 
* #100000073712 ^designation[0].value = "Creme" 
* #100000073713 "Salbe"
* #100000073713 ^designation[0].language = #de-AT 
* #100000073713 ^designation[0].value = "Salbe" 
* #100000073714 "wirkstoffhaltiges Pflaster"
* #100000073714 ^designation[0].language = #de-AT 
* #100000073714 ^designation[0].value = "wirkstoffhaltiges Pflaster" 
* #100000073715 "Shampoo"
* #100000073715 ^designation[0].language = #de-AT 
* #100000073715 ^designation[0].value = "Shampoo" 
* #100000073716 "Spray zur Anwendung auf der Haut, Suspension"
* #100000073716 ^designation[0].language = #de-AT 
* #100000073716 ^designation[0].value = "Spray zur Anwendung auf der Haut, Suspension" 
* #100000073717 "Flüssigkeit zur Anwendung auf der Haut"
* #100000073717 ^designation[0].language = #de-AT 
* #100000073717 ^designation[0].value = "Flüssigkeit zur Anwendung auf der Haut" 
* #100000073719 "Emulsion zur Anwendung auf der Haut"
* #100000073719 ^designation[0].language = #de-AT 
* #100000073719 ^designation[0].value = "Emulsion zur Anwendung auf der Haut" 
* #100000073720 "Kutanes Pflaster"
* #100000073720 ^designation[0].language = #de-AT 
* #100000073720 ^designation[0].value = "Kutanes Pflaster" 
* #100000073723 "Dentallösung"
* #100000073723 ^designation[0].language = #de-AT 
* #100000073723 ^designation[0].value = "Dentallösung" 
* #100000073726 "Gel"
* #100000073726 ^designation[0].language = #de-AT 
* #100000073726 ^designation[0].value = "Gel" 
* #100000073727 "Paste zur Anwendung auf der Haut"
* #100000073727 ^designation[0].language = #de-AT 
* #100000073727 ^designation[0].value = "Paste zur Anwendung auf der Haut" 
* #100000073728 "Schaum zur Anwendung auf der Haut"
* #100000073728 ^designation[0].language = #de-AT 
* #100000073728 ^designation[0].value = "Schaum zur Anwendung auf der Haut" 
* #100000073729 "Spray zur Anwendung auf der Haut, Lösung"
* #100000073729 ^designation[0].language = #de-AT 
* #100000073729 ^designation[0].value = "Spray zur Anwendung auf der Haut, Lösung" 
* #100000073731 "Lösung zur Anwendung auf der Haut"
* #100000073731 ^designation[0].language = #de-AT 
* #100000073731 ^designation[0].value = "Lösung zur Anwendung auf der Haut" 
* #100000073732 "Suspension zur Anwendung auf der Haut"
* #100000073732 ^designation[0].language = #de-AT 
* #100000073732 ^designation[0].value = "Suspension zur Anwendung auf der Haut" 
* #100000073733 "Pulver zur Anwendung auf der Haut"
* #100000073733 ^designation[0].language = #de-AT 
* #100000073733 ^designation[0].value = "Pulver zur Anwendung auf der Haut" 
* #100000073736 "Umschlagpaste"
* #100000073736 ^designation[0].language = #de-AT 
* #100000073736 ^designation[0].value = "Umschlagpaste" 
* #100000073741 "transdermales Pflaster"
* #100000073741 ^designation[0].language = #de-AT 
* #100000073741 ^designation[0].value = "transdermales Pflaster" 
* #100000073742 "wirkstoffhaltiger Nagellack"
* #100000073742 ^designation[0].language = #de-AT 
* #100000073742 ^designation[0].value = "wirkstoffhaltiger Nagellack" 
* #100000073743 "Stift zur Anwendung auf der Haut"
* #100000073743 ^designation[0].language = #de-AT 
* #100000073743 ^designation[0].value = "Stift zur Anwendung auf der Haut" 
* #100000073744 "imprägnierter Verband"
* #100000073744 ^designation[0].language = #de-AT 
* #100000073744 ^designation[0].value = "imprägnierter Verband" 
* #100000073752 "Lösung zum Auftropfen"
* #100000073752 ^designation[0].language = #de-AT 
* #100000073752 ^designation[0].value = "Lösung zum Auftropfen" 
* #100000073756 "Pricktestlösung"
* #100000073756 ^designation[0].language = #de-AT 
* #100000073756 ^designation[0].value = "Pricktestlösung" 
* #100000073758 "Augengel"
* #100000073758 ^designation[0].language = #de-AT 
* #100000073758 ^designation[0].value = "Augengel" 
* #100000073759 "Augentropfen, Lösung"
* #100000073759 ^designation[0].language = #de-AT 
* #100000073759 ^designation[0].value = "Augentropfen, Lösung" 
* #100000073760 "Augentropfensuspension"
* #100000073760 ^designation[0].language = #de-AT 
* #100000073760 ^designation[0].value = "Augentropfensuspension" 
* #100000073762 "Konzentrat zur Herstellung eines Tauchbades, Emulsion"
* #100000073762 ^designation[0].language = #de-AT 
* #100000073762 ^designation[0].value = "Konzentrat zur Herstellung eines Tauchbades, Emulsion" 
* #100000073769 "Transdermales System"
* #100000073769 ^designation[0].language = #de-AT 
* #100000073769 ^designation[0].value = "Transdermales System" 
* #100000073772 "Augensalbe"
* #100000073772 ^designation[0].language = #de-AT 
* #100000073772 ^designation[0].value = "Augensalbe" 
* #100000073773 "Augentropfen, Emulsion"
* #100000073773 ^designation[0].language = #de-AT 
* #100000073773 ^designation[0].value = "Augentropfen, Emulsion" 
* #100000073777 "Augeninsert"
* #100000073777 ^designation[0].language = #de-AT 
* #100000073777 ^designation[0].value = "Augeninsert" 
* #100000073786 "Ohrentropfen, Lösung"
* #100000073786 ^designation[0].language = #de-AT 
* #100000073786 ^designation[0].value = "Ohrentropfen, Lösung" 
* #100000073793 "Nasengel"
* #100000073793 ^designation[0].language = #de-AT 
* #100000073793 ^designation[0].value = "Nasengel" 
* #100000073794 "Nasentropfen, Lösung"
* #100000073794 ^designation[0].language = #de-AT 
* #100000073794 ^designation[0].value = "Nasentropfen, Lösung" 
* #100000073796 "Nasenspray, Lösung"
* #100000073796 ^designation[0].language = #de-AT 
* #100000073796 ^designation[0].value = "Nasenspray, Lösung" 
* #100000073798 "Nasenstift"
* #100000073798 ^designation[0].language = #de-AT 
* #100000073798 ^designation[0].value = "Nasenstift" 
* #100000073799 "Vaginalgel"
* #100000073799 ^designation[0].language = #de-AT 
* #100000073799 ^designation[0].value = "Vaginalgel" 
* #100000073806 "Nasensalbe"
* #100000073806 ^designation[0].language = #de-AT 
* #100000073806 ^designation[0].value = "Nasensalbe" 
* #100000073808 "Nasenpulver"
* #100000073808 ^designation[0].language = #de-AT 
* #100000073808 ^designation[0].value = "Nasenpulver" 
* #100000073809 "Nasenspray, Suspension"
* #100000073809 ^designation[0].language = #de-AT 
* #100000073809 ^designation[0].value = "Nasenspray, Suspension" 
* #100000073811 "Vaginalcreme"
* #100000073811 ^designation[0].language = #de-AT 
* #100000073811 ^designation[0].value = "Vaginalcreme" 
* #100000073813 "Vaginallösung"
* #100000073813 ^designation[0].language = #de-AT 
* #100000073813 ^designation[0].value = "Vaginallösung" 
* #100000073814 "Vaginalemulsion"
* #100000073814 ^designation[0].language = #de-AT 
* #100000073814 ^designation[0].value = "Vaginalemulsion" 
* #100000073815 "Vaginalzäpfchen"
* #100000073815 ^designation[0].language = #de-AT 
* #100000073815 ^designation[0].value = "Vaginalzäpfchen" 
* #100000073816 "Weichkapsel zur vaginalen Anwendung"
* #100000073816 ^designation[0].language = #de-AT 
* #100000073816 ^designation[0].value = "Weichkapsel zur vaginalen Anwendung" 
* #100000073818 "vaginales Wirkstofffreisetzungssystem"
* #100000073818 ^designation[0].language = #de-AT 
* #100000073818 ^designation[0].value = "vaginales Wirkstofffreisetzungssystem" 
* #100000073819 "Rektalcreme"
* #100000073819 ^designation[0].language = #de-AT 
* #100000073819 ^designation[0].value = "Rektalcreme" 
* #100000073820 "Rektalschaum"
* #100000073820 ^designation[0].language = #de-AT 
* #100000073820 ^designation[0].value = "Rektalschaum" 
* #100000073823 "Hartkapsel zur vaginalen Anwendung"
* #100000073823 ^designation[0].language = #de-AT 
* #100000073823 ^designation[0].value = "Hartkapsel zur vaginalen Anwendung" 
* #100000073824 "Vaginaltablette"
* #100000073824 ^designation[0].language = #de-AT 
* #100000073824 ^designation[0].value = "Vaginaltablette" 
* #100000073828 "Rektalsalbe"
* #100000073828 ^designation[0].language = #de-AT 
* #100000073828 ^designation[0].value = "Rektalsalbe" 
* #100000073829 "Rektallösung"
* #100000073829 ^designation[0].language = #de-AT 
* #100000073829 ^designation[0].value = "Rektallösung" 
* #100000073830 "Rektalsuspension"
* #100000073830 ^designation[0].language = #de-AT 
* #100000073830 ^designation[0].value = "Rektalsuspension" 
* #100000073831 "Pulver zur Herstellung einer Rektallösung"
* #100000073831 ^designation[0].language = #de-AT 
* #100000073831 ^designation[0].value = "Pulver zur Herstellung einer Rektallösung" 
* #100000073832 "Pulver zur Herstellung einer Rektalsuspension"
* #100000073832 ^designation[0].language = #de-AT 
* #100000073832 ^designation[0].value = "Pulver zur Herstellung einer Rektalsuspension" 
* #100000073835 "Lösung für einen Vernebler"
* #100000073835 ^designation[0].language = #de-AT 
* #100000073835 ^designation[0].value = "Lösung für einen Vernebler" 
* #100000073838 "Druckgasinhalation, Suspension"
* #100000073838 ^designation[0].language = #de-AT 
* #100000073838 ^designation[0].value = "Druckgasinhalation, Suspension" 
* #100000073839 "Pulver zur Inhalation"
* #100000073839 ^designation[0].language = #de-AT 
* #100000073839 ^designation[0].value = "Pulver zur Inhalation" 
* #100000073840 "einzeldosiertes Pulver zur Inhalation"
* #100000073840 ^designation[0].language = #de-AT 
* #100000073840 ^designation[0].value = "einzeldosiertes Pulver zur Inhalation" 
* #100000073843 "Zäpfchen"
* #100000073843 ^designation[0].language = #de-AT 
* #100000073843 ^designation[0].value = "Zäpfchen" 
* #100000073846 "Suspension für einen Vernebler"
* #100000073846 ^designation[0].language = #de-AT 
* #100000073846 ^designation[0].value = "Suspension für einen Vernebler" 
* #100000073847 "Pulver zur Herstellung einer Lösung für einen Vernebler"
* #100000073847 ^designation[0].language = #de-AT 
* #100000073847 ^designation[0].value = "Pulver zur Herstellung einer Lösung für einen Vernebler" 
* #100000073848 "Druckgasinhalation, Lösung"
* #100000073848 ^designation[0].language = #de-AT 
* #100000073848 ^designation[0].value = "Druckgasinhalation, Lösung" 
* #100000073850 "Hartkapsel mit Pulver zur Inhalation"
* #100000073850 ^designation[0].language = #de-AT 
* #100000073850 ^designation[0].value = "Hartkapsel mit Pulver zur Inhalation" 
* #100000073854 "Flüssigkeit zur Herstellung eines Dampfs zur Inhalation"
* #100000073854 ^designation[0].language = #de-AT 
* #100000073854 ^designation[0].value = "Flüssigkeit zur Herstellung eines Dampfs zur Inhalation" 
* #100000073856 "Emulsion zur Injektion"
* #100000073856 ^designation[0].language = #de-AT 
* #100000073856 ^designation[0].value = "Emulsion zur Injektion" 
* #100000073857 "Konzentrat zur Herstellung einer Injektionslösung"
* #100000073857 ^designation[0].language = #de-AT 
* #100000073857 ^designation[0].value = "Konzentrat zur Herstellung einer Injektionslösung" 
* #100000073858 "Emulsion zur Infusion"
* #100000073858 ^designation[0].language = #de-AT 
* #100000073858 ^designation[0].value = "Emulsion zur Infusion" 
* #100000073859 "Pulver zur Herstellung einer Infusionslösung"
* #100000073859 ^designation[0].language = #de-AT 
* #100000073859 ^designation[0].value = "Pulver zur Herstellung einer Infusionslösung" 
* #100000073860 "Pulver und Lösungsmittel zur Herstellung einer Infusionslösung"
* #100000073860 ^designation[0].language = #de-AT 
* #100000073860 ^designation[0].value = "Pulver und Lösungsmittel zur Herstellung einer Infusionslösung" 
* #100000073862 "Salbe zur Herstellung eines Dampfs zur Inhalation"
* #100000073862 ^designation[0].language = #de-AT 
* #100000073862 ^designation[0].value = "Salbe zur Herstellung eines Dampfs zur Inhalation" 
* #100000073863 "Injektionslösung"
* #100000073863 ^designation[0].language = #de-AT 
* #100000073863 ^designation[0].value = "Injektionslösung" 
* #100000073864 "Injektionssuspension"
* #100000073864 ^designation[0].language = #de-AT 
* #100000073864 ^designation[0].value = "Injektionssuspension" 
* #100000073866 "Pulver zur Herstellung einer Injektionslösung"
* #100000073866 ^designation[0].language = #de-AT 
* #100000073866 ^designation[0].value = "Pulver zur Herstellung einer Injektionslösung" 
* #100000073867 "Pulver zur Herstellung einer Injektionssuspension"
* #100000073867 ^designation[0].language = #de-AT 
* #100000073867 ^designation[0].value = "Pulver zur Herstellung einer Injektionssuspension" 
* #100000073868 "Pulver und Lösungsmittel zur Herstellung einer Injektionslösung"
* #100000073868 ^designation[0].language = #de-AT 
* #100000073868 ^designation[0].value = "Pulver und Lösungsmittel zur Herstellung einer Injektionslösung" 
* #100000073869 "Pulver und Lösungsmittel zur Herstellung einer Injektionssuspension"
* #100000073869 ^designation[0].language = #de-AT 
* #100000073869 ^designation[0].value = "Pulver und Lösungsmittel zur Herstellung einer Injektionssuspension" 
* #100000073870 "Infusionslösung"
* #100000073870 ^designation[0].language = #de-AT 
* #100000073870 ^designation[0].value = "Infusionslösung" 
* #100000073871 "Konzentrat zur Herstellung einer Infusionslösung"
* #100000073871 ^designation[0].language = #de-AT 
* #100000073871 ^designation[0].value = "Konzentrat zur Herstellung einer Infusionslösung" 
* #100000073872 "Lyophilisat zur Herstellung einer Infusionslösung"
* #100000073872 ^designation[0].language = #de-AT 
* #100000073872 ^designation[0].value = "Lyophilisat zur Herstellung einer Infusionslösung" 
* #100000073873 "Lyophilisat zur Herstellung einer Injektionslösung"
* #100000073873 ^designation[0].language = #de-AT 
* #100000073873 ^designation[0].value = "Lyophilisat zur Herstellung einer Injektionslösung" 
* #100000073874 "Implantat"
* #100000073874 ^designation[0].language = #de-AT 
* #100000073874 ^designation[0].value = "Implantat" 
* #100000073875 "Kette zur Implantation"
* #100000073875 ^designation[0].language = #de-AT 
* #100000073875 ^designation[0].value = "Kette zur Implantation" 
* #100000073877 "Hämodiafiltrationslösung"
* #100000073877 ^designation[0].language = #de-AT 
* #100000073877 ^designation[0].value = "Hämodiafiltrationslösung" 
* #100000073879 "Blasenspüllösung"
* #100000073879 ^designation[0].language = #de-AT 
* #100000073879 ^designation[0].value = "Blasenspüllösung" 
* #100000073880 "Gel zur Anwendung in der Harnröhre"
* #100000073880 ^designation[0].language = #de-AT 
* #100000073880 ^designation[0].value = "Gel zur Anwendung in der Harnröhre" 
* #100000073881 "Lösungsmittel zur Herstellung von Parenteralia"
* #100000073881 ^designation[0].language = #de-AT 
* #100000073881 ^designation[0].value = "Lösungsmittel zur Herstellung von Parenteralia" 
* #100000073885 "Hämofiltrationslösung"
* #100000073885 ^designation[0].language = #de-AT 
* #100000073885 ^designation[0].value = "Hämofiltrationslösung" 
* #100000073886 "Hämodialyselösung"
* #100000073886 ^designation[0].language = #de-AT 
* #100000073886 ^designation[0].value = "Hämodialyselösung" 
* #100000073887 "Lösung zur intravesikalen Anwendung"
* #100000073887 ^designation[0].language = #de-AT 
* #100000073887 ^designation[0].value = "Lösung zur intravesikalen Anwendung" 
* #100000073889 "Stäbchen zur Anwendung in der Harnröhre"
* #100000073889 ^designation[0].language = #de-AT 
* #100000073889 ^designation[0].value = "Stäbchen zur Anwendung in der Harnröhre" 
* #100000073895 "Intrauterines Wirkstofffreisetzungssystem"
* #100000073895 ^designation[0].language = #de-AT 
* #100000073895 ^designation[0].value = "Intrauterines Wirkstofffreisetzungssystem" 
* #100000073902 "Suspension zur endotracheopulmonalen Instillation"
* #100000073902 ^designation[0].language = #de-AT 
* #100000073902 ^designation[0].value = "Suspension zur endotracheopulmonalen Instillation" 
* #100000073903 "Gel zur endozervikalen Anwendung"
* #100000073903 ^designation[0].language = #de-AT 
* #100000073903 ^designation[0].value = "Gel zur endozervikalen Anwendung" 
* #100000073911 "Lösung für einen Vernebler (*)"
* #100000073911 ^designation[0].language = #de-AT 
* #100000073911 ^designation[0].value = "Lösung für einen Vernebler (*)" 
* #100000073913 "Lösung zur Modifikation einer Blutfraktion"
* #100000073913 ^designation[0].language = #de-AT 
* #100000073913 ^designation[0].value = "Lösung zur Modifikation einer Blutfraktion" 
* #100000073916 "Radionuklidgenerator"
* #100000073916 ^designation[0].language = #de-AT 
* #100000073916 ^designation[0].value = "Radionuklidgenerator" 
* #100000073917 "Lösung zur gastrointestinalen Anwendung"
* #100000073917 ^designation[0].language = #de-AT 
* #100000073917 ^designation[0].value = "Lösung zur gastrointestinalen Anwendung" 
* #100000073918 "Suspension zur gastrointestinalen Anwendung"
* #100000073918 ^designation[0].language = #de-AT 
* #100000073918 ^designation[0].value = "Suspension zur gastrointestinalen Anwendung" 
* #100000073919 "Organkonservierungslösung"
* #100000073919 ^designation[0].language = #de-AT 
* #100000073919 ^designation[0].value = "Organkonservierungslösung" 
* #100000073921 "Kit für ein radioaktives Arzneimittel"
* #100000073921 ^designation[0].language = #de-AT 
* #100000073921 ^designation[0].value = "Kit für ein radioaktives Arzneimittel" 
* #100000073922 "Dispersion"
* #100000073922 ^designation[0].language = #de-AT 
* #100000073922 ^designation[0].value = "Dispersion" 
* #100000073924 "Spüllösung"
* #100000073924 ^designation[0].language = #de-AT 
* #100000073924 ^designation[0].value = "Spüllösung" 
* #100000073925 "Gewebekleber"
* #100000073925 ^designation[0].language = #de-AT 
* #100000073925 ^designation[0].value = "Gewebekleber" 
* #100000073929 "Gas zur medizinischen Anwendung, druckverdichtet"
* #100000073929 ^designation[0].language = #de-AT 
* #100000073929 ^designation[0].value = "Gas zur medizinischen Anwendung, druckverdichtet" 
* #100000073930 "Gas zur medizinischen Anwendung, verflüssigt"
* #100000073930 ^designation[0].language = #de-AT 
* #100000073930 ^designation[0].value = "Gas zur medizinischen Anwendung, verflüssigt" 
* #100000073941 "Pulver und Lösungsmittel für einen Gewebekleber"
* #100000073941 ^designation[0].language = #de-AT 
* #100000073941 ^designation[0].value = "Pulver und Lösungsmittel für einen Gewebekleber" 
* #100000073942 "lebendes Gewebeäquivalent"
* #100000073942 ^designation[0].language = #de-AT 
* #100000073942 ^designation[0].value = "lebendes Gewebeäquivalent" 
* #100000073943 "Gel zur intestinalen Anwendung"
* #100000073943 ^designation[0].language = #de-AT 
* #100000073943 ^designation[0].value = "Gel zur intestinalen Anwendung" 
* #100000073945 "Gas zur medizinischen Anwendung, kälteverflüssigt"
* #100000073945 ^designation[0].language = #de-AT 
* #100000073945 ^designation[0].value = "Gas zur medizinischen Anwendung, kälteverflüssigt" 
* #100000073972 "Concentrate and solvent for concentrate for solution for infusion"
* #100000073972 ^designation[0].language = #de-AT 
* #100000073972 ^designation[0].value = "Concentrate and solvent for concentrate for solution for infusion" 
* #100000073990 "Konzentrat und Lösungsmittel zur Herstellung einer Injektionssuspension"
* #100000073990 ^designation[0].language = #de-AT 
* #100000073990 ^designation[0].value = "Konzentrat und Lösungsmittel zur Herstellung einer Injektionssuspension" 
* #100000073991 "Konzentrat zur Herstellung einer Lösung zum Einnehmen"
* #100000073991 ^designation[0].language = #de-AT 
* #100000073991 ^designation[0].value = "Konzentrat zur Herstellung einer Lösung zum Einnehmen" 
* #100000073996 "Emulsion for injection/infusion"
* #100000073996 ^designation[0].language = #de-AT 
* #100000073996 ^designation[0].value = "Emulsion for injection/infusion" 
* #100000073997 "Augentropfen, Lösung im Einzeldosisbehältnis"
* #100000073997 ^designation[0].language = #de-AT 
* #100000073997 ^designation[0].value = "Augentropfen, Lösung im Einzeldosisbehältnis" 
* #100000074010 "Modified-release film-coated tablet"
* #100000074010 ^designation[0].language = #de-AT 
* #100000074010 ^designation[0].value = "Modified-release film-coated tablet" 
* #100000074015 "Powder and solvent for concentrate for solution for infusion"
* #100000074015 ^designation[0].language = #de-AT 
* #100000074015 ^designation[0].value = "Powder and solvent for concentrate for solution for infusion" 
* #100000074018 "Pulver und Lösungsmittel zur Herstellung einer Depot-Injektionssuspension"
* #100000074018 ^designation[0].language = #de-AT 
* #100000074018 ^designation[0].value = "Pulver und Lösungsmittel zur Herstellung einer Depot-Injektionssuspension" 
* #100000074020 "Pulver zur Herstellung einer Suspension zur intravesikalen Anwendung"
* #100000074020 ^designation[0].language = #de-AT 
* #100000074020 ^designation[0].value = "Pulver zur Herstellung einer Suspension zur intravesikalen Anwendung" 
* #100000074021 "Inhalation powder, tablet"
* #100000074021 ^designation[0].language = #de-AT 
* #100000074021 ^designation[0].value = "Inhalation powder, tablet" 
* #100000074029 "Pulver für ein Konzentrat zur Herstellung einer Infusionslösung"
* #100000074029 ^designation[0].language = #de-AT 
* #100000074029 ^designation[0].value = "Pulver für ein Konzentrat zur Herstellung einer Infusionslösung" 
* #100000074033 "Pulver zur Herstellung einer Lösung zur intravesikalen Anwendung"
* #100000074033 ^designation[0].language = #de-AT 
* #100000074033 ^designation[0].value = "Pulver zur Herstellung einer Lösung zur intravesikalen Anwendung" 
* #100000074037 "Solution for haemodialysis/haemofiltration"
* #100000074037 ^designation[0].language = #de-AT 
* #100000074037 ^designation[0].value = "Solution for haemodialysis/haemofiltration" 
* #100000074038 "Injektions-/Infusionslösung"
* #100000074038 ^designation[0].language = #de-AT 
* #100000074038 ^designation[0].value = "Injektions-/Infusionslösung" 
* #100000074039 "Injektionslösung im Fertigpen"
* #100000074039 ^designation[0].language = #de-AT 
* #100000074039 ^designation[0].value = "Injektionslösung im Fertigpen" 
* #100000074040 "Lösung zur intraperitonealen Anwendung"
* #100000074040 ^designation[0].language = #de-AT 
* #100000074040 ^designation[0].value = "Lösung zur intraperitonealen Anwendung" 
* #100000074042 "Pulver zur Herstellung einer Injektions- bzw. Infusionslösung"
* #100000074042 ^designation[0].language = #de-AT 
* #100000074042 ^designation[0].value = "Pulver zur Herstellung einer Injektions- bzw. Infusionslösung" 
* #100000074046 "Solution for injection in cartridge"
* #100000074046 ^designation[0].language = #de-AT 
* #100000074046 ^designation[0].value = "Solution for injection in cartridge" 
* #100000074047 "Injektionslösung in einer Fertigspritze"
* #100000074047 ^designation[0].language = #de-AT 
* #100000074047 ^designation[0].value = "Injektionslösung in einer Fertigspritze" 
* #100000074048 "Suspension und Brausegranulat zur Herstellung einer Suspension zum Einnehmen"
* #100000074048 ^designation[0].language = #de-AT 
* #100000074048 ^designation[0].value = "Suspension und Brausegranulat zur Herstellung einer Suspension zum Einnehmen" 
* #100000074057 "Pulver und Lösungsmittel zur Herstellung einer Injektions- bzw. Infusionslösung"
* #100000074057 ^designation[0].language = #de-AT 
* #100000074057 ^designation[0].value = "Pulver und Lösungsmittel zur Herstellung einer Injektions- bzw. Infusionslösung" 
* #100000074063 "Suspension for injection in pre-filled syringe"
* #100000074063 ^designation[0].language = #de-AT 
* #100000074063 ^designation[0].value = "Suspension for injection in pre-filled syringe" 
* #100000074068 "Injektionsdispersion"
* #100000074068 ^designation[0].language = #de-AT 
* #100000074068 ^designation[0].value = "Injektionsdispersion" 
* #100000074069 "Konzentrat zur Herstellung einer Injektions- /Infusionslösung"
* #100000074069 ^designation[0].language = #de-AT 
* #100000074069 ^designation[0].value = "Konzentrat zur Herstellung einer Injektions- /Infusionslösung" 
* #100000074070 "Inhalationslösung"
* #100000074070 ^designation[0].language = #de-AT 
* #100000074070 ^designation[0].value = "Inhalationslösung" 
* #100000075573 "Schmelzfilm"
* #100000075573 ^designation[0].language = #de-AT 
* #100000075573 ^designation[0].value = "Schmelzfilm" 
* #100000075574 "Buccalfilm"
* #100000075574 ^designation[0].language = #de-AT 
* #100000075574 ^designation[0].value = "Buccalfilm" 
* #100000075576 "Suspension zur Implantation"
* #100000075576 ^designation[0].language = #de-AT 
* #100000075576 ^designation[0].value = "Suspension zur Implantation" 
* #100000075584 "Powder and solvent for dispersion for injection"
* #100000075584 ^designation[0].language = #de-AT 
* #100000075584 ^designation[0].value = "Powder and solvent for dispersion for injection" 
* #100000075585 "Powder and solvent for solution for injection in pre-filled syringe"
* #100000075585 ^designation[0].language = #de-AT 
* #100000075585 ^designation[0].value = "Powder and solvent for solution for injection in pre-filled syringe" 
* #100000075588 "Solution for injection/infusion in pre-filled syringe"
* #100000075588 ^designation[0].language = #de-AT 
* #100000075588 ^designation[0].value = "Solution for injection/infusion in pre-filled syringe" 
* #100000111534 "Concentrate for dispersion for infusion"
* #100000111534 ^designation[0].language = #de-AT 
* #100000111534 ^designation[0].value = "Concentrate for dispersion for infusion" 
* #100000116131 "Salbe zur Anwendung in der Mundhöhle"
* #100000116131 ^designation[0].language = #de-AT 
* #100000116131 ^designation[0].value = "Salbe zur Anwendung in der Mundhöhle" 
* #100000116134 "Transdermal gel"
* #100000116134 ^designation[0].language = #de-AT 
* #100000116134 ^designation[0].value = "Transdermal gel" 
* #100000116137 "Lyophilisat und Lösungsmittel zur Herstellung einer Injektionslösung"
* #100000116137 ^designation[0].language = #de-AT 
* #100000116137 ^designation[0].value = "Lyophilisat und Lösungsmittel zur Herstellung einer Injektionslösung" 
* #100000116151 "Ear/eye/nasal drops, solution"
* #100000116151 ^designation[0].language = #de-AT 
* #100000116151 ^designation[0].value = "Ear/eye/nasal drops, solution" 
* #100000116154 "magensaftresistente Retardtablette"
* #100000116154 ^designation[0].language = #de-AT 
* #100000116154 ^designation[0].value = "magensaftresistente Retardtablette" 
* #100000116157 "Hard capsule with gastro-resistant pellets"
* #100000116157 ^designation[0].language = #de-AT 
* #100000116157 ^designation[0].value = "Hard capsule with gastro-resistant pellets" 
* #100000116169 "Tropfen zum Einnehmen, Flüssigkeit"
* #100000116169 ^designation[0].language = #de-AT 
* #100000116169 ^designation[0].value = "Tropfen zum Einnehmen, Flüssigkeit" 
* #100000116172 "Powder and gel for gel"
* #100000116172 ^designation[0].language = #de-AT 
* #100000116172 ^designation[0].value = "Powder and gel for gel" 
* #100000116175 "Pulver und Lösungsmittel zur Herstellung einer Lösung zur intravesikalen Anwendung"
* #100000116175 ^designation[0].language = #de-AT 
* #100000116175 ^designation[0].value = "Pulver und Lösungsmittel zur Herstellung einer Lösung zur intravesikalen Anwendung" 
* #100000116176 "Powder and solvent for intravesical suspension"
* #100000116176 ^designation[0].language = #de-AT 
* #100000116176 ^designation[0].value = "Powder and solvent for intravesical suspension" 
* #100000116178 "Powder and solvent for suspension for injection in pre-filled syringe"
* #100000116178 ^designation[0].language = #de-AT 
* #100000116178 ^designation[0].value = "Powder and solvent for suspension for injection in pre-filled syringe" 
* #100000116183 "Pulver für ein Konzentrat zur Herstellung einer Injektions-/Infusionslösung"
* #100000116183 ^designation[0].language = #de-AT 
* #100000116183 ^designation[0].value = "Pulver für ein Konzentrat zur Herstellung einer Injektions-/Infusionslösung" 
* #100000116186 "Pulver zur Herstellung einer Injektions-/Infusionslösung"
* #100000116186 ^designation[0].language = #de-AT 
* #100000116186 ^designation[0].value = "Pulver zur Herstellung einer Injektions-/Infusionslösung" 
* #100000125706 "Spray zur Anwendung in der Mundhöhle, Lösung"
* #100000125706 ^designation[0].language = #de-AT 
* #100000125706 ^designation[0].value = "Spray zur Anwendung in der Mundhöhle, Lösung" 
* #100000125736 "Prolonged-release suspension for injection"
* #100000125736 ^designation[0].language = #de-AT 
* #100000125736 ^designation[0].value = "Prolonged-release suspension for injection" 
* #100000125743 "Granules in sachet"
* #100000125743 ^designation[0].language = #de-AT 
* #100000125743 ^designation[0].value = "Granules in sachet" 
* #100000125752 "Powder for oral solution in sachet"
* #100000125752 ^designation[0].language = #de-AT 
* #100000125752 ^designation[0].value = "Powder for oral solution in sachet" 
* #100000125756 "Oral solution in sachet"
* #100000125756 ^designation[0].language = #de-AT 
* #100000125756 ^designation[0].value = "Oral solution in sachet" 
* #100000125757 "Oral solution in single-dose container"
* #100000125757 ^designation[0].language = #de-AT 
* #100000125757 ^designation[0].value = "Oral solution in single-dose container" 
* #100000125775 "Prolonged-release suspension for injection in pre-filled syringe"
* #100000125775 ^designation[0].language = #de-AT 
* #100000125775 ^designation[0].value = "Prolonged-release suspension for injection in pre-filled syringe" 
* #100000143523 "Pulver zur Herstellung einer Infusionsdispersion"
* #100000143523 ^designation[0].language = #de-AT 
* #100000143523 ^designation[0].value = "Pulver zur Herstellung einer Infusionsdispersion" 
* #100000156088 "Versiegelungsmatrix"
* #100000156088 ^designation[0].language = #de-AT 
* #100000156088 ^designation[0].value = "Versiegelungsmatrix" 
* #100000169806 "Depot-Injektionslösung"
* #100000169806 ^designation[0].language = #de-AT 
* #100000169806 ^designation[0].value = "Depot-Injektionslösung" 
* #100000173999 "Konzentrat zur Herstellung einer Injektionssuspension"
* #100000173999 ^designation[0].language = #de-AT 
* #100000173999 ^designation[0].value = "Konzentrat zur Herstellung einer Injektionssuspension" 
* #100000174015 "Lösungsmittel zur Herstellung..."
* #100000174015 ^designation[0].language = #de-AT 
* #100000174015 ^designation[0].value = "Lösungsmittel zur Herstellung..." 
* #100000174019 "Suspension zur Herstellung einer Injektionssuspension"
* #100000174019 ^designation[0].language = #de-AT 
* #100000174019 ^designation[0].value = "Suspension zur Herstellung einer Injektionssuspension" 
* #100000174052 "Tablette zur Herstellung einer Lösung zur Anwendung auf der Haut"
* #100000174052 ^designation[0].language = #de-AT 
* #100000174052 ^designation[0].value = "Tablette zur Herstellung einer Lösung zur Anwendung auf der Haut" 
* #200000002229 "Granulat zur Entnahme aus Kapseln"
* #200000002229 ^designation[0].language = #de-AT 
* #200000002229 ^designation[0].value = "Granulat zur Entnahme aus Kapseln" 
* #200000003971 "Gel zur intrauterinen Anwendung"
* #200000003971 ^designation[0].language = #de-AT 
* #200000003971 ^designation[0].value = "Gel zur intrauterinen Anwendung" 
* #200000003974 "magensaftresistentes Retardgranulat"
* #200000003974 ^designation[0].language = #de-AT 
* #200000003974 ^designation[0].value = "magensaftresistentes Retardgranulat" 
* #200000010620 "Suspension and solution for suspension for injection"
* #200000010620 ^designation[0].language = #de-AT 
* #200000010620 ^designation[0].value = "Suspension and solution for suspension for injection" 
* #200000010684 "Lyophilisat zur sublingualen Anwendung"
* #200000010684 ^designation[0].language = #de-AT 
* #200000010684 ^designation[0].value = "Lyophilisat zur sublingualen Anwendung" 
* #200000015705 "Konzentrat zur Herstellung einer Injektionsdispersion"
* #200000015705 ^designation[0].language = #de-AT 
* #200000015705 ^designation[0].value = "Konzentrat zur Herstellung einer Injektionsdispersion" 
* #200000016453 "Spray zur Anwendung auf der Haut"
* #200000016453 ^designation[0].language = #de-AT 
* #200000016453 ^designation[0].value = "Spray zur Anwendung auf der Haut" 
* #900000000006 "Pulver für ein Konzentrat zur Herstellung einer Injektionslösung"
* #900000000006 ^designation[0].language = #de-AT 
* #900000000006 ^designation[0].value = "Pulver für ein Konzentrat zur Herstellung einer Injektionslösung" 
* #900000000008 "Konzentrat für eine orale / rektale Suspension"
* #900000000008 ^designation[0].language = #de-AT 
* #900000000008 ^designation[0].value = "Konzentrat für eine orale / rektale Suspension" 
* #900000000012 "Pulver zur Herstellung einer Infusionssuspension"
* #900000000012 ^designation[0].language = #de-AT 
* #900000000012 ^designation[0].value = "Pulver zur Herstellung einer Infusionssuspension" 
* #900000000026 "Tablette zur Herstellung einer Rektaldispersion"
* #900000000026 ^designation[0].language = #de-AT 
* #900000000026 ^designation[0].value = "Tablette zur Herstellung einer Rektaldispersion" 
* #900000000034 "Dentalpaste"
* #900000000034 ^designation[0].language = #de-AT 
* #900000000034 ^designation[0].value = "Dentalpaste" 
* #900000000036 "Emulsion zur urethralen Instillation"
* #900000000036 ^designation[0].language = #de-AT 
* #900000000036 ^designation[0].value = "Emulsion zur urethralen Instillation" 
* #900000000037 "nicht definiert"
* #900000000037 ^designation[0].language = #de-AT 
* #900000000037 ^designation[0].value = "nicht definiert" 
* #900000000043 "Augen- und Nasensalbe"
* #900000000043 ^designation[0].language = #de-AT 
* #900000000043 ^designation[0].value = "Augen- und Nasensalbe" 
