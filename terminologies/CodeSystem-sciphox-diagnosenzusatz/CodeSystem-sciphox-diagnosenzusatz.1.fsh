Instance: sciphox-diagnosenzusatz 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-sciphox-diagnosenzusatz" 
* name = "sciphox-diagnosenzusatz" 
* title = "Sciphox Diagnosenzusatz" 
* status = #active 
* content = #complete 
* description = "**Beschreibung:** Vocabulary Domain für Sicherheit/Verlässlichkeit" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.3.7.1.8" 
* date = "2015-07-15" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://wiki.hl7.de/index.php/cdaab2:ICD-Diagnose_Entry_%28Template%29" 
* count = 4 
* property[0].code = #hints 
* property[0].type = #string 
* concept[0].code = #A
* concept[0].display = "Ausgeschlossene Erkrankung"
* concept[0].definition = "Ausgeschlossene Erkrankung, gleichzeitig ist dies in Level 3 mittels negationInd anzugeben (siehe auch Hinweis im Text)"
* concept[0].designation[0].language = #de-AT 
* concept[0].designation[0].value = "Ausgeschlossene Erkrankung" 
* concept[0].property[0].code = #hints 
* concept[0].property[0].valueString = "negationInd = true" 
* concept[1].code = #G
* concept[1].display = "Gesichert"
* concept[1].definition = "Gesicherte Diagnose"
* concept[1].designation[0].language = #de-AT 
* concept[1].designation[0].value = "Gesichert" 
* concept[2].code = #V
* concept[2].display = "Verdacht auf"
* concept[2].definition = "Verdachtsdiagnose"
* concept[2].designation[0].language = #de-AT 
* concept[2].designation[0].value = "Verdacht auf" 
* concept[2].property[0].code = #hints 
* concept[2].property[0].valueString = "uncertaintyCode = UN" 
* concept[3].code = #Z
* concept[3].display = "Zustand nach"
* concept[3].definition = "Zustand nach"
* concept[3].designation[0].language = #de-AT 
* concept[3].designation[0].value = "Zustand nach" 
