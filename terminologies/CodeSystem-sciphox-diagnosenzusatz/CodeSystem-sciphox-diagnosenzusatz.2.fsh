Instance: sciphox-diagnosenzusatz 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-sciphox-diagnosenzusatz" 
* name = "sciphox-diagnosenzusatz" 
* title = "Sciphox Diagnosenzusatz" 
* status = #active 
* content = #complete 
* description = "**Beschreibung:** Vocabulary Domain für Sicherheit/Verlässlichkeit" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.3.7.1.8" 
* date = "2015-07-15" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://wiki.hl7.de/index.php/cdaab2:ICD-Diagnose_Entry_%28Template%29" 
* count = 4 
* property[0].code = #hints 
* property[0].type = #string 
* #A "Ausgeschlossene Erkrankung"
* #A ^definition = Ausgeschlossene Erkrankung, gleichzeitig ist dies in Level 3 mittels negationInd anzugeben (siehe auch Hinweis im Text)
* #A ^designation[0].language = #de-AT 
* #A ^designation[0].value = "Ausgeschlossene Erkrankung" 
* #A ^property[0].code = #hints 
* #A ^property[0].valueString = "negationInd = true" 
* #G "Gesichert"
* #G ^definition = Gesicherte Diagnose
* #G ^designation[0].language = #de-AT 
* #G ^designation[0].value = "Gesichert" 
* #V "Verdacht auf"
* #V ^definition = Verdachtsdiagnose
* #V ^designation[0].language = #de-AT 
* #V ^designation[0].value = "Verdacht auf" 
* #V ^property[0].code = #hints 
* #V ^property[0].valueString = "uncertaintyCode = UN" 
* #Z "Zustand nach"
* #Z ^definition = Zustand nach
* #Z ^designation[0].language = #de-AT 
* #Z ^designation[0].value = "Zustand nach" 
