Instance: geschlechtercodes-iso-iec-5218 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-geschlechtercodes-iso-iec-5218" 
* name = "geschlechtercodes-iso-iec-5218" 
* title = "Geschlechtercodes - ISO IEC 5218" 
* status = #active 
* content = #complete 
* version = "Corrected Version 2004" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.0.5218" 
* date = "2015-04-01" 
* count = 4 
* concept[0].code = #0
* concept[0].display = "unbekannt"
* concept[1].code = #1
* concept[1].display = "männlich"
* concept[2].code = #2
* concept[2].display = "weiblich"
* concept[3].code = #9
* concept[3].display = "nicht zutreffend"
