Instance: geschlechtercodes-iso-iec-5218 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-geschlechtercodes-iso-iec-5218" 
* name = "geschlechtercodes-iso-iec-5218" 
* title = "Geschlechtercodes - ISO IEC 5218" 
* status = #active 
* content = #complete 
* version = "Corrected Version 2004" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.0.5218" 
* date = "2015-04-01" 
* count = 4 
* #0 "unbekannt"
* #1 "männlich"
* #2 "weiblich"
* #9 "nicht zutreffend"
