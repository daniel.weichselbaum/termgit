Instance: dgc-vaccine 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-dgc-vaccine" 
* name = "dgc-vaccine" 
* title = "DGC_Vaccine" 
* status = #active 
* version = "202104" 
* description = "**Description:** To be used in certificate 2" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.56" 
* date = "2021-04-29" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-atc-deutsch-wido"
* compose.include[0].concept[0].code = "J07BX03"
* compose.include[0].concept[0].display = "Covid-19-Impfstoffe"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "Covid-19 Vakzine" 
* compose.include[0].concept[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[0].designation[1].value = "covid-19 vaccines" 
* compose.include[0].concept[0].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#relationships "relationships" 
* compose.include[0].concept[0].designation[2].value = "2.16.840.1.113883.6.73:J07BX03" 
