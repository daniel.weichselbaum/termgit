Instance: elga-conditionverificationstatus 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-conditionverificationstatus" 
* name = "elga-conditionverificationstatus" 
* title = "ELGA_ConditionVerificationStatus" 
* status = #active 
* version = "202002" 
* description = "**Description:** Clinical Status of Allergies Or Intolerances 

**Beschreibung:** Klinischer Status von Allergien und Intoleranzen" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.184" 
* date = "2020-02-04" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-ips-conditionverificationstatus"
* compose.include[0].concept[0].code = #unconfirmed
* compose.include[0].concept[0].display = "unbestätigt"
* compose.include[0].concept[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[0].designation[0].value = "IPSConditionVerificationStatus" 
* compose.include[0].concept[1].code = #provisional
* compose.include[0].concept[1].display = "vorläufig"
* compose.include[0].concept[1].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[1].designation[0].value = "IPSConditionVerificationStatus" 
* compose.include[0].concept[2].code = #differential
* compose.include[0].concept[2].display = "alternativ möglich (Differentialdiagnose)"
* compose.include[0].concept[2].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[2].designation[0].value = "IPSConditionVerificationStatus" 
* compose.include[0].concept[3].code = #confirmed
* compose.include[0].concept[3].display = "bestätigt"
* compose.include[0].concept[3].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[3].designation[0].value = "IPSConditionVerificationStatus" 
* compose.include[0].concept[4].code = #refuted
* compose.include[0].concept[4].display = "ausgeschlossen"
* compose.include[0].concept[4].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[4].designation[0].value = "IPSConditionVerificationStatus" 
* compose.include[0].concept[5].code = #unknown
* compose.include[0].concept[5].display = "unbekannt"
* compose.include[0].concept[5].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[5].designation[0].value = "IPSConditionVerificationStatus" 

* expansion.timestamp = "2022-09-13T14:16:55.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem-ips-conditionverificationstatus"
* expansion.contains[0].code = #unconfirmed
* expansion.contains[0].display = "unbestätigt"
* expansion.contains[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[0].designation[0].value = "IPSConditionVerificationStatus" 
* expansion.contains[0].contains[0].system = "https://termgit.elga.gv.at/CodeSystem-ips-conditionverificationstatus"
* expansion.contains[0].contains[0].code = #provisional
* expansion.contains[0].contains[0].display = "vorläufig"
* expansion.contains[0].contains[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[0].contains[0].designation[0].value = "IPSConditionVerificationStatus" 
* expansion.contains[0].contains[1].system = "https://termgit.elga.gv.at/CodeSystem-ips-conditionverificationstatus"
* expansion.contains[0].contains[1].code = #differential
* expansion.contains[0].contains[1].display = "alternativ möglich (Differentialdiagnose)"
* expansion.contains[0].contains[1].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[0].contains[1].designation[0].value = "IPSConditionVerificationStatus" 
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem-ips-conditionverificationstatus"
* expansion.contains[1].code = #confirmed
* expansion.contains[1].display = "bestätigt"
* expansion.contains[1].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[1].designation[0].value = "IPSConditionVerificationStatus" 
* expansion.contains[2].system = "https://termgit.elga.gv.at/CodeSystem-ips-conditionverificationstatus"
* expansion.contains[2].code = #refuted
* expansion.contains[2].display = "ausgeschlossen"
* expansion.contains[2].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[2].designation[0].value = "IPSConditionVerificationStatus" 
* expansion.contains[3].system = "https://termgit.elga.gv.at/CodeSystem-ips-conditionverificationstatus"
* expansion.contains[3].code = #unknown
* expansion.contains[3].display = "unbekannt"
* expansion.contains[3].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[3].designation[0].value = "IPSConditionVerificationStatus" 
