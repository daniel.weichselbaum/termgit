Instance: elga-laendercodes 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-laendercodes" 
* name = "elga-laendercodes" 
* title = "ELGA_Laendercodes" 
* status = #active 
* version = "4.0" 
* description = "**Description:** Value Set for ISO 3166-1 alpha 3 used in ELGA

**Beschreibung:** Value Set für ISO 3166-1 alpha 3 zur Verwendung in ELGA" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.172" 
* date = "2016-08-01" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-iso-3166-1-alpha-3"
* compose.include[0].concept[0].code = "ABW"
* compose.include[0].concept[0].display = "Aruba"
* compose.include[0].concept[1].code = "AFG"
* compose.include[0].concept[1].display = "Afghanistan"
* compose.include[0].concept[2].code = "AGO"
* compose.include[0].concept[2].display = "Angola"
* compose.include[0].concept[3].code = "AIA"
* compose.include[0].concept[3].display = "Anguilla"
* compose.include[0].concept[4].code = "ALA"
* compose.include[0].concept[4].display = "Åland"
* compose.include[0].concept[5].code = "ALB"
* compose.include[0].concept[5].display = "Albanien"
* compose.include[0].concept[6].code = "AND"
* compose.include[0].concept[6].display = "Andorra"
* compose.include[0].concept[7].code = "ANT"
* compose.include[0].concept[7].display = "Niederländische Antillen"
* compose.include[0].concept[8].code = "ARE"
* compose.include[0].concept[8].display = "Vereinigte Arabische Emirate"
* compose.include[0].concept[9].code = "ARG"
* compose.include[0].concept[9].display = "Argentinien"
* compose.include[0].concept[10].code = "ARM"
* compose.include[0].concept[10].display = "Armenien"
* compose.include[0].concept[11].code = "ASC"
* compose.include[0].concept[11].display = "Ascension (verwaltet von St. Helena)"
* compose.include[0].concept[11].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[11].designation[0].value = "Nicht offiziell" 
* compose.include[0].concept[12].code = "ASM"
* compose.include[0].concept[12].display = "Amerikanisch-Samoa"
* compose.include[0].concept[13].code = "ATA"
* compose.include[0].concept[13].display = "Antarktis (Sonderstatus durch Antarktis-Vertrag)"
* compose.include[0].concept[14].code = "ATF"
* compose.include[0].concept[14].display = "Französische Süd- und Antarktisgebiete"
* compose.include[0].concept[15].code = "ATG"
* compose.include[0].concept[15].display = "Antigua und Barbuda"
* compose.include[0].concept[16].code = "AUS"
* compose.include[0].concept[16].display = "Australien"
* compose.include[0].concept[17].code = "AUT"
* compose.include[0].concept[17].display = "Österreich"
* compose.include[0].concept[18].code = "AZE"
* compose.include[0].concept[18].display = "Aserbaidschan"
* compose.include[0].concept[19].code = "BDI"
* compose.include[0].concept[19].display = "Burundi"
* compose.include[0].concept[20].code = "BEL"
* compose.include[0].concept[20].display = "Belgien"
* compose.include[0].concept[21].code = "BEN"
* compose.include[0].concept[21].display = "Benin"
* compose.include[0].concept[22].code = "BES"
* compose.include[0].concept[22].display = "Bonaire, Sint Eustatius und Saba (Niederlande"
* compose.include[0].concept[23].code = "BFA"
* compose.include[0].concept[23].display = "Burkina Faso"
* compose.include[0].concept[24].code = "BGD"
* compose.include[0].concept[24].display = "Bangladesch"
* compose.include[0].concept[25].code = "BGR"
* compose.include[0].concept[25].display = "Bulgarien"
* compose.include[0].concept[26].code = "BHR"
* compose.include[0].concept[26].display = "Bahrain"
* compose.include[0].concept[27].code = "BHS"
* compose.include[0].concept[27].display = "Bahamas"
* compose.include[0].concept[28].code = "BIH"
* compose.include[0].concept[28].display = "Bosnien und Herzegowina"
* compose.include[0].concept[29].code = "BLM"
* compose.include[0].concept[29].display = "Saint-Barthélemy"
* compose.include[0].concept[30].code = "BLR"
* compose.include[0].concept[30].display = "Belarus (Weißrussland)"
* compose.include[0].concept[31].code = "BLZ"
* compose.include[0].concept[31].display = "Belize"
* compose.include[0].concept[32].code = "BMU"
* compose.include[0].concept[32].display = "Bermuda"
* compose.include[0].concept[33].code = "BOL"
* compose.include[0].concept[33].display = "Bolivien"
* compose.include[0].concept[34].code = "BRA"
* compose.include[0].concept[34].display = "Brasilien"
* compose.include[0].concept[35].code = "BRB"
* compose.include[0].concept[35].display = "Barbados"
* compose.include[0].concept[36].code = "BRN"
* compose.include[0].concept[36].display = "Brunei Darussalam"
* compose.include[0].concept[37].code = "BTN"
* compose.include[0].concept[37].display = "Bhutan"
* compose.include[0].concept[38].code = "BUR"
* compose.include[0].concept[38].display = "Burma (jetzt Myanmar)"
* compose.include[0].concept[39].code = "BVT"
* compose.include[0].concept[39].display = "Bouvetinsel"
* compose.include[0].concept[40].code = "BWA"
* compose.include[0].concept[40].display = "Botsuana"
* compose.include[0].concept[41].code = "CAF"
* compose.include[0].concept[41].display = "Zentralafrikanische Republik"
* compose.include[0].concept[42].code = "CAN"
* compose.include[0].concept[42].display = "Kanada"
* compose.include[0].concept[43].code = "CCK"
* compose.include[0].concept[43].display = "Kokosinseln"
* compose.include[0].concept[44].code = "CHE"
* compose.include[0].concept[44].display = "Schweiz (Confoederatio Helvetica)"
* compose.include[0].concept[45].code = "CHL"
* compose.include[0].concept[45].display = "Chile"
* compose.include[0].concept[46].code = "CHN"
* compose.include[0].concept[46].display = "China"
* compose.include[0].concept[47].code = "CIV"
* compose.include[0].concept[47].display = "Côte d'Ivoire (Elfenbeinküste)"
* compose.include[0].concept[47].designation[0].language = #de-AT 
* compose.include[0].concept[47].designation[0].value = "Côte dIvoire (Elfenbeinküste)" 
* compose.include[0].concept[48].code = "CMR"
* compose.include[0].concept[48].display = "Kamerun"
* compose.include[0].concept[49].code = "COD"
* compose.include[0].concept[49].display = "Kongo"
* compose.include[0].concept[50].code = "COG"
* compose.include[0].concept[50].display = "Republik Kongo"
* compose.include[0].concept[51].code = "COK"
* compose.include[0].concept[51].display = "Cookinseln"
* compose.include[0].concept[52].code = "COL"
* compose.include[0].concept[52].display = "Kolumbien"
* compose.include[0].concept[53].code = "COM"
* compose.include[0].concept[53].display = "Komoren"
* compose.include[0].concept[54].code = "CPT"
* compose.include[0].concept[54].display = "Clipperton (reserviert für ITU)"
* compose.include[0].concept[54].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[54].designation[0].value = "Nicht offiziell" 
* compose.include[0].concept[55].code = "CPV"
* compose.include[0].concept[55].display = "Kap Verde"
* compose.include[0].concept[56].code = "CRI"
* compose.include[0].concept[56].display = "Costa Rica"
* compose.include[0].concept[57].code = "CSK"
* compose.include[0].concept[57].display = "Tschechoslowakei (ehemalig)"
* compose.include[0].concept[58].code = "CUB"
* compose.include[0].concept[58].display = "Kuba"
* compose.include[0].concept[59].code = "CUW"
* compose.include[0].concept[59].display = "Curaçao"
* compose.include[0].concept[60].code = "CXR"
* compose.include[0].concept[60].display = "Weihnachtsinsel"
* compose.include[0].concept[61].code = "CYM"
* compose.include[0].concept[61].display = "Kaimaninseln"
* compose.include[0].concept[62].code = "CYP"
* compose.include[0].concept[62].display = "Zypern"
* compose.include[0].concept[63].code = "CZE"
* compose.include[0].concept[63].display = "Tschechische Republik"
* compose.include[0].concept[64].code = "DEU"
* compose.include[0].concept[64].display = "Deutschland"
* compose.include[0].concept[65].code = "DGA"
* compose.include[0].concept[65].display = "Diego Garcia (reserviert für ITU)"
* compose.include[0].concept[65].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[65].designation[0].value = "Nicht offiziell" 
* compose.include[0].concept[66].code = "DJI"
* compose.include[0].concept[66].display = "Dschibuti"
* compose.include[0].concept[67].code = "DMA"
* compose.include[0].concept[67].display = "Dominica"
* compose.include[0].concept[68].code = "DNK"
* compose.include[0].concept[68].display = "Dänemark"
* compose.include[0].concept[69].code = "DOM"
* compose.include[0].concept[69].display = "Dominikanische Republik"
* compose.include[0].concept[70].code = "DZA"
* compose.include[0].concept[70].display = "Algerien"
* compose.include[0].concept[71].code = "ECU"
* compose.include[0].concept[71].display = "Ecuador"
* compose.include[0].concept[72].code = "EGY"
* compose.include[0].concept[72].display = "Ägypten"
* compose.include[0].concept[73].code = "ERI"
* compose.include[0].concept[73].display = "Eritrea"
* compose.include[0].concept[74].code = "ESH"
* compose.include[0].concept[74].display = "Westsahara"
* compose.include[0].concept[75].code = "ESP"
* compose.include[0].concept[75].display = "Spanien"
* compose.include[0].concept[76].code = "EST"
* compose.include[0].concept[76].display = "Estland"
* compose.include[0].concept[77].code = "ETH"
* compose.include[0].concept[77].display = "Äthiopien"
* compose.include[0].concept[78].code = "FIN"
* compose.include[0].concept[78].display = "Finnland"
* compose.include[0].concept[79].code = "FJI"
* compose.include[0].concept[79].display = "Fidschi"
* compose.include[0].concept[80].code = "FLK"
* compose.include[0].concept[80].display = "Falklandinseln"
* compose.include[0].concept[81].code = "FRA"
* compose.include[0].concept[81].display = "Frankreich"
* compose.include[0].concept[82].code = "FRO"
* compose.include[0].concept[82].display = "Färöer"
* compose.include[0].concept[83].code = "FSM"
* compose.include[0].concept[83].display = "Mikronesien"
* compose.include[0].concept[84].code = "FXX"
* compose.include[0].concept[84].display = "(europ. Frankreich ohne Übersee-Départements)"
* compose.include[0].concept[85].code = "GAB"
* compose.include[0].concept[85].display = "Gabun"
* compose.include[0].concept[86].code = "GBR"
* compose.include[0].concept[86].display = "Vereinigtes Königreich Großbritannien und Nordirland"
* compose.include[0].concept[87].code = "GEO"
* compose.include[0].concept[87].display = "Georgien"
* compose.include[0].concept[88].code = "GGY"
* compose.include[0].concept[88].display = "Guernsey (Kanalinsel)"
* compose.include[0].concept[89].code = "GHA"
* compose.include[0].concept[89].display = "Ghana"
* compose.include[0].concept[90].code = "GIB"
* compose.include[0].concept[90].display = "Gibraltar"
* compose.include[0].concept[91].code = "GIN"
* compose.include[0].concept[91].display = "Guinea"
* compose.include[0].concept[92].code = "GLP"
* compose.include[0].concept[92].display = "Guadeloupe"
* compose.include[0].concept[93].code = "GMB"
* compose.include[0].concept[93].display = "Gambia"
* compose.include[0].concept[94].code = "GNB"
* compose.include[0].concept[94].display = "Guinea-Bissau"
* compose.include[0].concept[95].code = "GNQ"
* compose.include[0].concept[95].display = "Äquatorialguinea"
* compose.include[0].concept[96].code = "GRC"
* compose.include[0].concept[96].display = "Griechenland"
* compose.include[0].concept[97].code = "GRD"
* compose.include[0].concept[97].display = "Grenada"
* compose.include[0].concept[98].code = "GRL"
* compose.include[0].concept[98].display = "Grönland"
* compose.include[0].concept[99].code = "GTM"
* compose.include[0].concept[99].display = "Guatemala"
* compose.include[0].concept[100].code = "GUF"
* compose.include[0].concept[100].display = "Französisch-Guayana"
* compose.include[0].concept[101].code = "GUM"
* compose.include[0].concept[101].display = "Guam"
* compose.include[0].concept[102].code = "GUY"
* compose.include[0].concept[102].display = "Guyana"
* compose.include[0].concept[103].code = "HKG"
* compose.include[0].concept[103].display = "Hongkong"
* compose.include[0].concept[104].code = "HMD"
* compose.include[0].concept[104].display = "Heard- und McDonald-Inseln"
* compose.include[0].concept[105].code = "HND"
* compose.include[0].concept[105].display = "Honduras"
* compose.include[0].concept[106].code = "HRV"
* compose.include[0].concept[106].display = "Kroatien"
* compose.include[0].concept[107].code = "HTI"
* compose.include[0].concept[107].display = "Haiti"
* compose.include[0].concept[108].code = "HUN"
* compose.include[0].concept[108].display = "Ungarn"
* compose.include[0].concept[109].code = "IDN"
* compose.include[0].concept[109].display = "Indonesien"
* compose.include[0].concept[110].code = "IMN"
* compose.include[0].concept[110].display = "Insel Man"
* compose.include[0].concept[111].code = "IND"
* compose.include[0].concept[111].display = "Indien"
* compose.include[0].concept[112].code = "IOT"
* compose.include[0].concept[112].display = "Britisches Territorium im Indischen Ozean"
* compose.include[0].concept[113].code = "IRL"
* compose.include[0].concept[113].display = "Irland"
* compose.include[0].concept[114].code = "IRN"
* compose.include[0].concept[114].display = "Iran"
* compose.include[0].concept[115].code = "IRQ"
* compose.include[0].concept[115].display = "Irak"
* compose.include[0].concept[116].code = "ISL"
* compose.include[0].concept[116].display = "Island"
* compose.include[0].concept[117].code = "ISR"
* compose.include[0].concept[117].display = "Israel"
* compose.include[0].concept[118].code = "ITA"
* compose.include[0].concept[118].display = "Italien"
* compose.include[0].concept[119].code = "JAM"
* compose.include[0].concept[119].display = "Jamaika"
* compose.include[0].concept[120].code = "JEY"
* compose.include[0].concept[120].display = "Jersey (Kanalinsel)"
* compose.include[0].concept[121].code = "JOR"
* compose.include[0].concept[121].display = "Jordanien"
* compose.include[0].concept[122].code = "JPN"
* compose.include[0].concept[122].display = "Japan"
* compose.include[0].concept[123].code = "KAZ"
* compose.include[0].concept[123].display = "Kasachstan"
* compose.include[0].concept[124].code = "KEN"
* compose.include[0].concept[124].display = "Kenia"
* compose.include[0].concept[125].code = "KGZ"
* compose.include[0].concept[125].display = "Kirgisistan"
* compose.include[0].concept[126].code = "KHM"
* compose.include[0].concept[126].display = "Kambodscha"
* compose.include[0].concept[127].code = "KIR"
* compose.include[0].concept[127].display = "Kiribati"
* compose.include[0].concept[128].code = "KNA"
* compose.include[0].concept[128].display = "St. Kitts und Nevis"
* compose.include[0].concept[129].code = "KOR"
* compose.include[0].concept[129].display = "Korea"
* compose.include[0].concept[130].code = "KOS"
* compose.include[0].concept[130].display = "Kosovo, Republik"
* compose.include[0].concept[131].code = "KWT"
* compose.include[0].concept[131].display = "Kuwait"
* compose.include[0].concept[132].code = "LAO"
* compose.include[0].concept[132].display = "Laos"
* compose.include[0].concept[133].code = "LBN"
* compose.include[0].concept[133].display = "Libanon"
* compose.include[0].concept[134].code = "LBR"
* compose.include[0].concept[134].display = "Liberia"
* compose.include[0].concept[135].code = "LBY"
* compose.include[0].concept[135].display = "Libysch-Arabische Dschamahirija (Libyen)"
* compose.include[0].concept[136].code = "LCA"
* compose.include[0].concept[136].display = "St. Lucia"
* compose.include[0].concept[137].code = "LIE"
* compose.include[0].concept[137].display = "Liechtenstein"
* compose.include[0].concept[138].code = "LKA"
* compose.include[0].concept[138].display = "Sri Lanka"
* compose.include[0].concept[139].code = "LSO"
* compose.include[0].concept[139].display = "Lesotho"
* compose.include[0].concept[140].code = "LTU"
* compose.include[0].concept[140].display = "Litauen"
* compose.include[0].concept[141].code = "LUX"
* compose.include[0].concept[141].display = "Luxemburg"
* compose.include[0].concept[142].code = "LVA"
* compose.include[0].concept[142].display = "Lettland"
* compose.include[0].concept[143].code = "MAC"
* compose.include[0].concept[143].display = "Macao"
* compose.include[0].concept[144].code = "MAF"
* compose.include[0].concept[144].display = "Saint-Martin (franz. Teil)"
* compose.include[0].concept[145].code = "MAR"
* compose.include[0].concept[145].display = "Marokko"
* compose.include[0].concept[146].code = "MCO"
* compose.include[0].concept[146].display = "Monaco"
* compose.include[0].concept[147].code = "MDA"
* compose.include[0].concept[147].display = "Moldawien (Republik Moldau)"
* compose.include[0].concept[148].code = "MDG"
* compose.include[0].concept[148].display = "Madagaskar"
* compose.include[0].concept[149].code = "MDV"
* compose.include[0].concept[149].display = "Malediven"
* compose.include[0].concept[150].code = "MEX"
* compose.include[0].concept[150].display = "Mexiko"
* compose.include[0].concept[151].code = "MHL"
* compose.include[0].concept[151].display = "Marshallinseln"
* compose.include[0].concept[152].code = "MKD"
* compose.include[0].concept[152].display = "Mazedonien"
* compose.include[0].concept[153].code = "MLI"
* compose.include[0].concept[153].display = "Mali"
* compose.include[0].concept[154].code = "MLT"
* compose.include[0].concept[154].display = "Malta"
* compose.include[0].concept[155].code = "MMR"
* compose.include[0].concept[155].display = "Myanmar (Burma)"
* compose.include[0].concept[156].code = "MNE"
* compose.include[0].concept[156].display = "Montenegro"
* compose.include[0].concept[157].code = "MNG"
* compose.include[0].concept[157].display = "Mongolei"
* compose.include[0].concept[158].code = "MNP"
* compose.include[0].concept[158].display = "Nördliche Marianen"
* compose.include[0].concept[159].code = "MOZ"
* compose.include[0].concept[159].display = "Mosambik"
* compose.include[0].concept[160].code = "MRT"
* compose.include[0].concept[160].display = "Mauretanien"
* compose.include[0].concept[161].code = "MSR"
* compose.include[0].concept[161].display = "Montserrat"
* compose.include[0].concept[162].code = "MTQ"
* compose.include[0].concept[162].display = "Martinique"
* compose.include[0].concept[163].code = "MUS"
* compose.include[0].concept[163].display = "Mauritius"
* compose.include[0].concept[164].code = "MWI"
* compose.include[0].concept[164].display = "Malawi"
* compose.include[0].concept[165].code = "MYS"
* compose.include[0].concept[165].display = "Malaysia"
* compose.include[0].concept[166].code = "MYT"
* compose.include[0].concept[166].display = "Mayotte"
* compose.include[0].concept[167].code = "NAM"
* compose.include[0].concept[167].display = "Namibia"
* compose.include[0].concept[168].code = "NCL"
* compose.include[0].concept[168].display = "Neukaledonien"
* compose.include[0].concept[169].code = "NER"
* compose.include[0].concept[169].display = "Niger"
* compose.include[0].concept[170].code = "NFK"
* compose.include[0].concept[170].display = "Norfolkinsel"
* compose.include[0].concept[171].code = "NGA"
* compose.include[0].concept[171].display = "Nigeria"
* compose.include[0].concept[172].code = "NIC"
* compose.include[0].concept[172].display = "Nicaragua"
* compose.include[0].concept[173].code = "NIU"
* compose.include[0].concept[173].display = "Niue"
* compose.include[0].concept[174].code = "NLD"
* compose.include[0].concept[174].display = "Niederlande"
* compose.include[0].concept[175].code = "NOR"
* compose.include[0].concept[175].display = "Norwegen"
* compose.include[0].concept[176].code = "NPL"
* compose.include[0].concept[176].display = "Nepal"
* compose.include[0].concept[177].code = "NRU"
* compose.include[0].concept[177].display = "Nauru"
* compose.include[0].concept[178].code = "NTZ"
* compose.include[0].concept[178].display = "Neutrale Zone (Saudi-Arabien und Irak)"
* compose.include[0].concept[179].code = "NZL"
* compose.include[0].concept[179].display = "Neuseeland"
* compose.include[0].concept[180].code = "OMN"
* compose.include[0].concept[180].display = "Oman"
* compose.include[0].concept[181].code = "PAK"
* compose.include[0].concept[181].display = "Pakistan"
* compose.include[0].concept[182].code = "PAN"
* compose.include[0].concept[182].display = "Panama"
* compose.include[0].concept[183].code = "PCN"
* compose.include[0].concept[183].display = "Pitcairninseln"
* compose.include[0].concept[184].code = "PER"
* compose.include[0].concept[184].display = "Peru"
* compose.include[0].concept[185].code = "PHL"
* compose.include[0].concept[185].display = "Philippinen"
* compose.include[0].concept[186].code = "PLW"
* compose.include[0].concept[186].display = "Palau"
* compose.include[0].concept[187].code = "PNG"
* compose.include[0].concept[187].display = "Papua-Neuguinea"
* compose.include[0].concept[188].code = "POL"
* compose.include[0].concept[188].display = "Polen"
* compose.include[0].concept[189].code = "PRI"
* compose.include[0].concept[189].display = "Puerto Rico"
* compose.include[0].concept[190].code = "PRK"
* compose.include[0].concept[190].display = "Korea"
* compose.include[0].concept[191].code = "PRT"
* compose.include[0].concept[191].display = "Portugal"
* compose.include[0].concept[192].code = "PRY"
* compose.include[0].concept[192].display = "Paraguay"
* compose.include[0].concept[193].code = "PSE"
* compose.include[0].concept[193].display = "Palästinensische Autonomiegebiete"
* compose.include[0].concept[194].code = "PYF"
* compose.include[0].concept[194].display = "Französisch-Polynesien"
* compose.include[0].concept[195].code = "QAT"
* compose.include[0].concept[195].display = "Katar"
* compose.include[0].concept[196].code = "REU"
* compose.include[0].concept[196].display = "Réunion"
* compose.include[0].concept[197].code = "ROU"
* compose.include[0].concept[197].display = "Rumänien"
* compose.include[0].concept[198].code = "RUS"
* compose.include[0].concept[198].display = "Russische Föderation"
* compose.include[0].concept[199].code = "RWA"
* compose.include[0].concept[199].display = "Ruanda"
* compose.include[0].concept[200].code = "SAU"
* compose.include[0].concept[200].display = "Saudi-Arabien"
* compose.include[0].concept[201].code = "SCG"
* compose.include[0].concept[201].display = "Serbien und Montenegro (ehemalig)"
* compose.include[0].concept[202].code = "SDN"
* compose.include[0].concept[202].display = "Sudan"
* compose.include[0].concept[203].code = "SEN"
* compose.include[0].concept[203].display = "Senegal"
* compose.include[0].concept[204].code = "SGP"
* compose.include[0].concept[204].display = "Singapur"
* compose.include[0].concept[205].code = "SGS"
* compose.include[0].concept[205].display = "Südgeorgien und die Südlichen Sandwichinseln"
* compose.include[0].concept[206].code = "SHN"
* compose.include[0].concept[206].display = "St. Helena"
* compose.include[0].concept[207].code = "SJM"
* compose.include[0].concept[207].display = "Svalbard und Jan Mayen"
* compose.include[0].concept[208].code = "SLB"
* compose.include[0].concept[208].display = "Salomonen"
* compose.include[0].concept[209].code = "SLE"
* compose.include[0].concept[209].display = "Sierra Leone"
* compose.include[0].concept[210].code = "SLV"
* compose.include[0].concept[210].display = "El Salvador"
* compose.include[0].concept[211].code = "SMR"
* compose.include[0].concept[211].display = "San Marino"
* compose.include[0].concept[212].code = "SOM"
* compose.include[0].concept[212].display = "Somalia"
* compose.include[0].concept[213].code = "SPM"
* compose.include[0].concept[213].display = "Saint-Pierre und Miquelon"
* compose.include[0].concept[214].code = "SRB"
* compose.include[0].concept[214].display = "Serbien"
* compose.include[0].concept[215].code = "SSD"
* compose.include[0].concept[215].display = "Südsudan"
* compose.include[0].concept[216].code = "STP"
* compose.include[0].concept[216].display = "São Tomé und Príncipe"
* compose.include[0].concept[217].code = "SUN"
* compose.include[0].concept[217].display = "UdSSR (jetzt: Russische Föderation)"
* compose.include[0].concept[218].code = "SUR"
* compose.include[0].concept[218].display = "Suriname"
* compose.include[0].concept[219].code = "SVK"
* compose.include[0].concept[219].display = "Slowakei"
* compose.include[0].concept[220].code = "SVN"
* compose.include[0].concept[220].display = "Slowenien"
* compose.include[0].concept[221].code = "SWE"
* compose.include[0].concept[221].display = "Schweden"
* compose.include[0].concept[222].code = "SWZ"
* compose.include[0].concept[222].display = "Swasiland"
* compose.include[0].concept[223].code = "SXM"
* compose.include[0].concept[223].display = "Sint Maarten (niederl. Teil)"
* compose.include[0].concept[224].code = "SYC"
* compose.include[0].concept[224].display = "Seychellen"
* compose.include[0].concept[225].code = "SYR"
* compose.include[0].concept[225].display = "Syrien"
* compose.include[0].concept[226].code = "TAA"
* compose.include[0].concept[226].display = "Tristan da Cunha (verwaltet von St. Helena)"
* compose.include[0].concept[226].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[226].designation[0].value = "Nicht offiziell" 
* compose.include[0].concept[227].code = "TCA"
* compose.include[0].concept[227].display = "Turks- und Caicosinseln"
* compose.include[0].concept[228].code = "TCD"
* compose.include[0].concept[228].display = "Tschad"
* compose.include[0].concept[229].code = "TGO"
* compose.include[0].concept[229].display = "Togo"
* compose.include[0].concept[230].code = "THA"
* compose.include[0].concept[230].display = "Thailand"
* compose.include[0].concept[231].code = "TJK"
* compose.include[0].concept[231].display = "Tadschikistan"
* compose.include[0].concept[232].code = "TKL"
* compose.include[0].concept[232].display = "Tokelau"
* compose.include[0].concept[233].code = "TKM"
* compose.include[0].concept[233].display = "Turkmenistan"
* compose.include[0].concept[234].code = "TLS"
* compose.include[0].concept[234].display = "Osttimor (Timor-L'este)"
* compose.include[0].concept[234].designation[0].language = #de-AT 
* compose.include[0].concept[234].designation[0].value = "Osttimor (Timor-Leste)" 
* compose.include[0].concept[235].code = "TMP"
* compose.include[0].concept[235].display = "Osttimor (alt)"
* compose.include[0].concept[235].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[235].designation[0].value = "Nicht offiziell" 
* compose.include[0].concept[236].code = "TON"
* compose.include[0].concept[236].display = "Tonga"
* compose.include[0].concept[237].code = "TTO"
* compose.include[0].concept[237].display = "Trinidad und Tobago"
* compose.include[0].concept[238].code = "TUN"
* compose.include[0].concept[238].display = "Tunesien"
* compose.include[0].concept[239].code = "TUR"
* compose.include[0].concept[239].display = "Türkei"
* compose.include[0].concept[240].code = "TUV"
* compose.include[0].concept[240].display = "Tuvalu"
* compose.include[0].concept[241].code = "TWN"
* compose.include[0].concept[241].display = "Republik China (Taiwan)"
* compose.include[0].concept[242].code = "TZA"
* compose.include[0].concept[242].display = "Tansania"
* compose.include[0].concept[243].code = "UGA"
* compose.include[0].concept[243].display = "Uganda"
* compose.include[0].concept[244].code = "UKR"
* compose.include[0].concept[244].display = "Ukraine"
* compose.include[0].concept[245].code = "UMI"
* compose.include[0].concept[245].display = "United States Minor Outlying Islands"
* compose.include[0].concept[246].code = "URY"
* compose.include[0].concept[246].display = "Uruguay"
* compose.include[0].concept[247].code = "USA"
* compose.include[0].concept[247].display = "Vereinigte Staaten von Amerika"
* compose.include[0].concept[248].code = "UZB"
* compose.include[0].concept[248].display = "Usbekistan"
* compose.include[0].concept[249].code = "VAT"
* compose.include[0].concept[249].display = "Vatikanstadt"
* compose.include[0].concept[250].code = "VCT"
* compose.include[0].concept[250].display = "St. Vincent und die Grenadinen"
* compose.include[0].concept[251].code = "VEN"
* compose.include[0].concept[251].display = "Venezuela"
* compose.include[0].concept[252].code = "VGB"
* compose.include[0].concept[252].display = "Britische Jungferninseln"
* compose.include[0].concept[253].code = "VIR"
* compose.include[0].concept[253].display = "Amerikanische Jungferninseln"
* compose.include[0].concept[254].code = "VNM"
* compose.include[0].concept[254].display = "Vietnam"
* compose.include[0].concept[255].code = "VUT"
* compose.include[0].concept[255].display = "Vanuatu"
* compose.include[0].concept[256].code = "WLF"
* compose.include[0].concept[256].display = "Wallis und Futuna"
* compose.include[0].concept[257].code = "WSM"
* compose.include[0].concept[257].display = "Samoa"
* compose.include[0].concept[258].code = "XKS"
* compose.include[0].concept[258].display = "Kosovo"
* compose.include[0].concept[259].code = "XXA"
* compose.include[0].concept[259].display = "Staatenlos"
* compose.include[0].concept[259].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[259].designation[0].value = "Part D Codes for persons without a defined nationality. User-reserved code element" 
* compose.include[0].concept[260].code = "XXB"
* compose.include[0].concept[260].display = "Flüchtling gemäß Genfer Flüchtlingskonvention (GFK) von 1951"
* compose.include[0].concept[260].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[260].designation[0].value = "Part D Codes for persons without a defined nationality. User-reserved code element" 
* compose.include[0].concept[261].code = "XXC"
* compose.include[0].concept[261].display = "Anderer Flüchtling"
* compose.include[0].concept[261].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[261].designation[0].value = "Part D Codes for persons without a defined nationality. User-reserved code element" 
* compose.include[0].concept[262].code = "XXX"
* compose.include[0].concept[262].display = "Unbekannt"
* compose.include[0].concept[262].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[262].designation[0].value = "Nationaltät unbestimmt, unabhängig von der tatsächlichen Staatszugehörigkeit. Kann Personen mit Staatenzugehörigkeit inkludieren, die sich legal im Land aufenthalten, deren Staatenzugehörigkeit derzeit aber unbekannt ist" 
* compose.include[0].concept[262].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[262].designation[1].value = "Part D Codes for persons without a defined nationality. User-reserved code element" 
* compose.include[0].concept[263].code = "YEM"
* compose.include[0].concept[263].display = "Jemen"
* compose.include[0].concept[264].code = "YUG"
* compose.include[0].concept[264].display = "Jugoslawien (ehemalig)"
* compose.include[0].concept[265].code = "ZAF"
* compose.include[0].concept[265].display = "Südafrika"
* compose.include[0].concept[266].code = "ZAR"
* compose.include[0].concept[266].display = "Zaire (jetzt Demokratische Republik Kongo)"
* compose.include[0].concept[267].code = "ZMB"
* compose.include[0].concept[267].display = "Sambia"
* compose.include[0].concept[268].code = "ZWE"
* compose.include[0].concept[268].display = "Simbabwe"
