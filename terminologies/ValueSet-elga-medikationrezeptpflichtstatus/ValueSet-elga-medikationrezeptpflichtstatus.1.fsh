Instance: elga-medikationrezeptpflichtstatus 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-medikationrezeptpflichtstatus" 
* name = "elga-medikationrezeptpflichtstatus" 
* title = "ELGA_MedikationRezeptpflichtstatus" 
* status = #active 
* version = "20211126" 
* description = "**Description:** ELGA ValueSet for e-Medication

**Beschreibung:** ELGA ValueSet für Rezeptpflichtstatus" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.74" 
* date = "2021-11-26" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-medikationrezeptpflichtstatus"
* compose.include[0].concept[0].code = #100000072076
* compose.include[0].concept[0].display = "Arzneimittel zur Abgabe ohne aerztliche Verschreibung"
* compose.include[0].concept[1].code = #100000072077
* compose.include[0].concept[1].display = "Arzneimittel zur einmaligen oder wiederholten Abgabe gegen aerztliche Verschreibung"
* compose.include[0].concept[2].code = #100000072078
* compose.include[0].concept[2].display = "Arzneimittel zur eingeschraenkten Abgabe gegen aerztliche Verschreibung, deren Anwendung definierten Spezialisten vorbehalten ist"
* compose.include[0].concept[3].code = #100000072079
* compose.include[0].concept[3].display = "Arzneimittel zur einmaligen Abgabe auf aerztliche Verschreibung"
* compose.include[0].concept[4].code = #100000072084
* compose.include[0].concept[4].display = "Arzneimittel zur Abgabe gegen aerztliche Verschreibung"
* compose.include[0].concept[5].code = #100000072086
* compose.include[0].concept[5].display = "Arzneimittel zur wiederholten Abgabe gegen aerztliche Verschreibung"
* compose.include[0].concept[6].code = #100000157313
* compose.include[0].concept[6].display = "Arzneimittel zur Abgabe gegen besondere aerztliche Verschreibung, Suchtgifte"
* compose.include[0].concept[7].code = #900000000002
* compose.include[0].concept[7].display = "Arzneimittel zur Abgabe gegen besondere aerztliche Verschreibung, Substitution"

* expansion.timestamp = "2022-09-13T14:18:17.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem-medikationrezeptpflichtstatus"
* expansion.contains[0].code = #100000072076
* expansion.contains[0].display = "Arzneimittel zur Abgabe ohne aerztliche Verschreibung"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem-medikationrezeptpflichtstatus"
* expansion.contains[1].code = #100000072077
* expansion.contains[1].display = "Arzneimittel zur einmaligen oder wiederholten Abgabe gegen aerztliche Verschreibung"
* expansion.contains[2].system = "https://termgit.elga.gv.at/CodeSystem-medikationrezeptpflichtstatus"
* expansion.contains[2].code = #100000072078
* expansion.contains[2].display = "Arzneimittel zur eingeschraenkten Abgabe gegen aerztliche Verschreibung, deren Anwendung definierten Spezialisten vorbehalten ist"
* expansion.contains[3].system = "https://termgit.elga.gv.at/CodeSystem-medikationrezeptpflichtstatus"
* expansion.contains[3].code = #100000072079
* expansion.contains[3].display = "Arzneimittel zur einmaligen Abgabe auf aerztliche Verschreibung"
* expansion.contains[4].system = "https://termgit.elga.gv.at/CodeSystem-medikationrezeptpflichtstatus"
* expansion.contains[4].code = #100000072084
* expansion.contains[4].display = "Arzneimittel zur Abgabe gegen aerztliche Verschreibung"
* expansion.contains[5].system = "https://termgit.elga.gv.at/CodeSystem-medikationrezeptpflichtstatus"
* expansion.contains[5].code = #100000072086
* expansion.contains[5].display = "Arzneimittel zur wiederholten Abgabe gegen aerztliche Verschreibung"
* expansion.contains[6].system = "https://termgit.elga.gv.at/CodeSystem-medikationrezeptpflichtstatus"
* expansion.contains[6].code = #100000157313
* expansion.contains[6].display = "Arzneimittel zur Abgabe gegen besondere aerztliche Verschreibung, Suchtgifte"
* expansion.contains[7].system = "https://termgit.elga.gv.at/CodeSystem-medikationrezeptpflichtstatus"
* expansion.contains[7].code = #900000000002
* expansion.contains[7].display = "Arzneimittel zur Abgabe gegen besondere aerztliche Verschreibung, Substitution"
