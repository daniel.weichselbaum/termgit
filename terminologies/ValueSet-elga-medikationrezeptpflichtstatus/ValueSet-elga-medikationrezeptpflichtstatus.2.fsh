Instance: elga-medikationrezeptpflichtstatus 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-medikationrezeptpflichtstatus" 
* name = "elga-medikationrezeptpflichtstatus" 
* title = "ELGA_MedikationRezeptpflichtstatus" 
* status = #active 
* version = "20211126" 
* description = "**Description:** ELGA ValueSet for e-Medication

**Beschreibung:** ELGA ValueSet für Rezeptpflichtstatus" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.74" 
* date = "2021-11-26" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-medikationrezeptpflichtstatus"
* compose.include[0].concept[0].code = "100000072076"
* compose.include[0].concept[0].display = "Arzneimittel zur Abgabe ohne aerztliche Verschreibung"
* compose.include[0].concept[1].code = "100000072077"
* compose.include[0].concept[1].display = "Arzneimittel zur einmaligen oder wiederholten Abgabe gegen aerztliche Verschreibung"
* compose.include[0].concept[2].code = "100000072078"
* compose.include[0].concept[2].display = "Arzneimittel zur eingeschraenkten Abgabe gegen aerztliche Verschreibung, deren Anwendung definierten Spezialisten vorbehalten ist"
* compose.include[0].concept[3].code = "100000072079"
* compose.include[0].concept[3].display = "Arzneimittel zur einmaligen Abgabe auf aerztliche Verschreibung"
* compose.include[0].concept[4].code = "100000072084"
* compose.include[0].concept[4].display = "Arzneimittel zur Abgabe gegen aerztliche Verschreibung"
* compose.include[0].concept[5].code = "100000072086"
* compose.include[0].concept[5].display = "Arzneimittel zur wiederholten Abgabe gegen aerztliche Verschreibung"
* compose.include[0].concept[6].code = "100000157313"
* compose.include[0].concept[6].display = "Arzneimittel zur Abgabe gegen besondere aerztliche Verschreibung, Suchtgifte"
* compose.include[0].concept[7].code = "900000000002"
* compose.include[0].concept[7].display = "Arzneimittel zur Abgabe gegen besondere aerztliche Verschreibung, Substitution"
