Instance: elga-kontakttypen 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-kontakttypen" 
* name = "elga-kontakttypen" 
* title = "ELGA_Kontakttypen" 
* status = #active 
* content = #complete 
* version = "3.0" 
* description = "**Beschreibung:** ELGA_Codeliste für Kontakttypen (relevant für das Berechtigungssystem)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.161" 
* date = "2015-03-31" 
* count = 4 
* #K101 "Stationär"
* #K102 "Ambulant"
* #K103 "Entlassung"
* #K104 "Delegiert"
