Instance: elga-kontakttypen 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-kontakttypen" 
* name = "elga-kontakttypen" 
* title = "ELGA_Kontakttypen" 
* status = #active 
* content = #complete 
* version = "3.0" 
* description = "**Beschreibung:** ELGA_Codeliste für Kontakttypen (relevant für das Berechtigungssystem)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.161" 
* date = "2015-03-31" 
* count = 4 
* concept[0].code = #K101
* concept[0].display = "Stationär"
* concept[1].code = #K102
* concept[1].display = "Ambulant"
* concept[2].code = #K103
* concept[2].display = "Entlassung"
* concept[3].code = #K104
* concept[3].display = "Delegiert"
