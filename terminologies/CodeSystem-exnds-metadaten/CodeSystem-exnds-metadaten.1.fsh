Instance: exnds-metadaten 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-exnds-metadaten" 
* name = "exnds-metadaten" 
* title = "EXNDS_Metadaten" 
* status = #active 
* content = #complete 
* version = "201301" 
* description = "**Beschreibung:** EXNDS Codes für XDM Metadaten " 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.198" 
* date = "2013-01-10" 
* count = 2 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* concept[0].code = #classCode
* concept[0].display = "classCode"
* concept[0].property[0].code = #child 
* concept[0].property[0].valueCode = #TerminEXNDS 
* concept[1].code = #TerminEXNDS
* concept[1].display = "Termin EXNDS"
* concept[1].property[0].code = #parent 
* concept[1].property[0].valueCode = #classCode 
