Instance: exnds-metadaten 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-exnds-metadaten" 
* name = "exnds-metadaten" 
* title = "EXNDS_Metadaten" 
* status = #active 
* content = #complete 
* version = "201301" 
* description = "**Beschreibung:** EXNDS Codes für XDM Metadaten " 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.198" 
* date = "2013-01-10" 
* count = 2 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #classCode "classCode"
* #classCode ^property[0].code = #child 
* #classCode ^property[0].valueCode = #TerminEXNDS 
* #TerminEXNDS "Termin EXNDS"
* #TerminEXNDS ^property[0].code = #parent 
* #TerminEXNDS ^property[0].valueCode = #classCode 
