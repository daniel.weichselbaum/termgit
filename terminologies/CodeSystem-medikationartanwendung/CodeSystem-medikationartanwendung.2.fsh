Instance: medikationartanwendung 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-medikationartanwendung" 
* name = "medikationartanwendung" 
* title = "MedikationArtAnwendung" 
* status = #active 
* content = #complete 
* version = "20211126" 
* description = "**Description:** Art der Anwendung

**Beschreibung:** The EDQM grants permission to AGES to reproduce Standard Terms data on their website. However, the following conditions apply to this permission:
It is clearly stated that the data is from the EDQM Standard Terms database (http://standardterms.edqm.eu), and that it is reproduced with the permission of the European Directorate for the Quality of Medicines & HealthCare, Council of Europe (EDQM). The Standard Terms data is not modified, and no other data is represented as being part of Standard Terms. If any additional data is presented alongside, it must be clear that it does not originate from Standard Terms. The date on which the data was retrieved from the Standard Terms database is clearly stated, with a notice that the EDQM Standard Terms database is not a static list and content can change over time" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.10.1.4.3.4.3.4" 
* date = "2021-11-26" 
* count = 69 
* #100000073564 "Anwendung am Ohr"
* #100000073564 ^designation[0].language = #de-AT 
* #100000073564 ^designation[0].value = "Anwendung am Ohr" 
* #100000073566 "Anwendung auf der Haut"
* #100000073566 ^designation[0].language = #de-AT 
* #100000073566 ^designation[0].value = "Anwendung auf der Haut" 
* #100000073567 "dentale Anwendung"
* #100000073567 ^designation[0].language = #de-AT 
* #100000073567 ^designation[0].value = "dentale Anwendung" 
* #100000073568 "Badebehandlung"
* #100000073568 ^designation[0].language = #de-AT 
* #100000073568 ^designation[0].value = "Badebehandlung" 
* #100000073570 "Anwendung in den Nebenhöhlen"
* #100000073570 ^designation[0].language = #de-AT 
* #100000073570 ^designation[0].value = "Anwendung in den Nebenhöhlen" 
* #100000073571 "endotracheopulmonale Anwendung"
* #100000073571 ^designation[0].language = #de-AT 
* #100000073571 ^designation[0].value = "endotracheopulmonale Anwendung" 
* #100000073572 "epidurale Anwendung"
* #100000073572 ^designation[0].language = #de-AT 
* #100000073572 ^designation[0].value = "epidurale Anwendung" 
* #100000073573 "zum Auftragen auf die Wunde"
* #100000073573 ^designation[0].language = #de-AT 
* #100000073573 ^designation[0].value = "zum Auftragen auf die Wunde" 
* #100000073577 "zur Anwendung mittels Magensonde"
* #100000073577 ^designation[0].language = #de-AT 
* #100000073577 ^designation[0].value = "zur Anwendung mittels Magensonde" 
* #100000073578 "Anwendung am Zahnfleisch"
* #100000073578 ^designation[0].language = #de-AT 
* #100000073578 ^designation[0].value = "Anwendung am Zahnfleisch" 
* #100000073579 "Hämodialyse"
* #100000073579 ^designation[0].language = #de-AT 
* #100000073579 ^designation[0].value = "Hämodialyse" 
* #100000073584 "zur Inhalation"
* #100000073584 ^designation[0].language = #de-AT 
* #100000073584 ^designation[0].value = "zur Inhalation" 
* #100000073585 "intestinale Anwendung"
* #100000073585 ^designation[0].language = #de-AT 
* #100000073585 ^designation[0].value = "intestinale Anwendung" 
* #100000073587 "intraarterielle Anwendung"
* #100000073587 ^designation[0].language = #de-AT 
* #100000073587 ^designation[0].value = "intraarterielle Anwendung" 
* #100000073588 "intraartikuläre Anwendung"
* #100000073588 ^designation[0].language = #de-AT 
* #100000073588 ^designation[0].value = "intraartikuläre Anwendung" 
* #100000073589 "intrabursale Anwendung"
* #100000073589 ^designation[0].language = #de-AT 
* #100000073589 ^designation[0].value = "intrabursale Anwendung" 
* #100000073590 "intracardiale Anwendung"
* #100000073590 ^designation[0].language = #de-AT 
* #100000073590 ^designation[0].value = "intracardiale Anwendung" 
* #100000073591 "intrakavernöse Anwendung"
* #100000073591 ^designation[0].language = #de-AT 
* #100000073591 ^designation[0].value = "intrakavernöse Anwendung" 
* #100000073592 "Intrazerebrale Anwendung"
* #100000073592 ^designation[0].language = #de-AT 
* #100000073592 ^designation[0].value = "Intrazerebrale Anwendung" 
* #100000073593 "intrazervikale Anwendung"
* #100000073593 ^designation[0].language = #de-AT 
* #100000073593 ^designation[0].value = "intrazervikale Anwendung" 
* #100000073595 "intradermale Anwendung"
* #100000073595 ^designation[0].language = #de-AT 
* #100000073595 ^designation[0].value = "intradermale Anwendung" 
* #100000073597 "intraläsionale Anwendung"
* #100000073597 ^designation[0].language = #de-AT 
* #100000073597 ^designation[0].value = "intraläsionale Anwendung" 
* #100000073598 "intralymphatische Anwendung"
* #100000073598 ^designation[0].language = #de-AT 
* #100000073598 ^designation[0].value = "intralymphatische Anwendung" 
* #100000073600 "intramuskuläre Anwendung"
* #100000073600 ^designation[0].language = #de-AT 
* #100000073600 ^designation[0].value = "intramuskuläre Anwendung" 
* #100000073602 "intraossäre Anwendung"
* #100000073602 ^designation[0].language = #de-AT 
* #100000073602 ^designation[0].value = "intraossäre Anwendung" 
* #100000073604 "intraperitoneale Anwendung"
* #100000073604 ^designation[0].language = #de-AT 
* #100000073604 ^designation[0].value = "intraperitoneale Anwendung" 
* #100000073608 "intrathekale Anwendung"
* #100000073608 ^designation[0].language = #de-AT 
* #100000073608 ^designation[0].value = "intrathekale Anwendung" 
* #100000073609 "intratumorale Anwendung"
* #100000073609 ^designation[0].language = #de-AT 
* #100000073609 ^designation[0].value = "intratumorale Anwendung" 
* #100000073610 "intrauterine Anwendung"
* #100000073610 ^designation[0].language = #de-AT 
* #100000073610 ^designation[0].value = "intrauterine Anwendung" 
* #100000073611 "intravenöse Anwendung"
* #100000073611 ^designation[0].language = #de-AT 
* #100000073611 ^designation[0].value = "intravenöse Anwendung" 
* #100000073612 "intravesikale Anwendung"
* #100000073612 ^designation[0].language = #de-AT 
* #100000073612 ^designation[0].value = "intravesikale Anwendung" 
* #100000073613 "Intravitreal"
* #100000073613 ^designation[0].language = #de-AT 
* #100000073613 ^designation[0].value = "Intravitreal" 
* #100000073615 "nasale Anwendung"
* #100000073615 ^designation[0].language = #de-AT 
* #100000073615 ^designation[0].value = "nasale Anwendung" 
* #100000073616 "zum Vernebeln"
* #100000073616 ^designation[0].language = #de-AT 
* #100000073616 ^designation[0].value = "zum Vernebeln" 
* #100000073617 "Anwendung am Auge"
* #100000073617 ^designation[0].language = #de-AT 
* #100000073617 ^designation[0].value = "Anwendung am Auge" 
* #100000073619 "zum Einnehmen"
* #100000073619 ^designation[0].language = #de-AT 
* #100000073619 ^designation[0].value = "zum Einnehmen" 
* #100000073620 "Anwendung in der Mundhöhle"
* #100000073620 ^designation[0].language = #de-AT 
* #100000073620 ^designation[0].value = "Anwendung in der Mundhöhle" 
* #100000073621 "zur Anwendung im Mund- und Rachenraum"
* #100000073621 ^designation[0].language = #de-AT 
* #100000073621 ^designation[0].value = "zur Anwendung im Mund- und Rachenraum" 
* #100000073623 "periartikuläre Anwendung"
* #100000073623 ^designation[0].language = #de-AT 
* #100000073623 ^designation[0].value = "periartikuläre Anwendung" 
* #100000073624 "perineurale Anwendung"
* #100000073624 ^designation[0].language = #de-AT 
* #100000073624 ^designation[0].value = "perineurale Anwendung" 
* #100000073625 "zur periodontalen Anwendung"
* #100000073625 ^designation[0].language = #de-AT 
* #100000073625 ^designation[0].value = "zur periodontalen Anwendung" 
* #100000073628 "rektale Anwendung"
* #100000073628 ^designation[0].language = #de-AT 
* #100000073628 ^designation[0].value = "rektale Anwendung" 
* #100000073629 "Art der Anwendung nicht spezifizierbar"
* #100000073629 ^designation[0].language = #de-AT 
* #100000073629 ^designation[0].value = "Art der Anwendung nicht spezifizierbar" 
* #100000073632 "subkonjunktivale Anwendung"
* #100000073632 ^designation[0].language = #de-AT 
* #100000073632 ^designation[0].value = "subkonjunktivale Anwendung" 
* #100000073633 "subkutane Anwendung"
* #100000073633 ^designation[0].language = #de-AT 
* #100000073633 ^designation[0].value = "subkutane Anwendung" 
* #100000073634 "Sublingual"
* #100000073634 ^designation[0].language = #de-AT 
* #100000073634 ^designation[0].value = "Sublingual" 
* #100000073637 "transdermale Anwendung"
* #100000073637 ^designation[0].language = #de-AT 
* #100000073637 ^designation[0].value = "transdermale Anwendung" 
* #100000073638 "Anwendung in der Harnröhre"
* #100000073638 ^designation[0].language = #de-AT 
* #100000073638 ^designation[0].value = "Anwendung in der Harnröhre" 
* #100000073639 "vaginale Anwendung"
* #100000073639 ^designation[0].language = #de-AT 
* #100000073639 ^designation[0].value = "vaginale Anwendung" 
* #100000075243 "zur lokalen Anwendung"
* #100000075243 ^designation[0].language = #de-AT 
* #100000075243 ^designation[0].value = "zur lokalen Anwendung" 
* #100000075248 "Infiltration"
* #100000075248 ^designation[0].language = #de-AT 
* #100000075248 ^designation[0].value = "Infiltration" 
* #100000075249 "zur Implantation"
* #100000075249 ^designation[0].language = #de-AT 
* #100000075249 ^designation[0].value = "zur Implantation" 
* #100000075264 "retrobulbäre Anwendung"
* #100000075264 ^designation[0].language = #de-AT 
* #100000075264 ^designation[0].value = "retrobulbäre Anwendung" 
* #100000075275 "Buccale Anwendung"
* #100000075275 ^designation[0].language = #de-AT 
* #100000075275 ^designation[0].value = "Buccale Anwendung" 
* #100000075554 "submuköse Anwendung"
* #100000075554 ^designation[0].language = #de-AT 
* #100000075554 ^designation[0].value = "submuköse Anwendung" 
* #100000075556 "extrakorporale Anwendung"
* #100000075556 ^designation[0].language = #de-AT 
* #100000075556 ^designation[0].value = "extrakorporale Anwendung" 
* #100000075557 "periossäre Anwendung"
* #100000075557 ^designation[0].language = #de-AT 
* #100000075557 ^designation[0].value = "periossäre Anwendung" 
* #100000125763 "intrakamerale Anwendung"
* #100000125763 ^designation[0].language = #de-AT 
* #100000125763 ^designation[0].value = "intrakamerale Anwendung" 
* #100000157797 "intraglanduläre Anwendung"
* #100000157797 ^designation[0].language = #de-AT 
* #100000157797 ^designation[0].value = "intraglanduläre Anwendung" 
* #900000000002 "subläsionale Anwendung"
* #900000000002 ^designation[0].language = #de-AT 
* #900000000002 ^designation[0].value = "subläsionale Anwendung" 
* #900000000003 "perivasale Anwendung"
* #900000000003 ^designation[0].language = #de-AT 
* #900000000003 ^designation[0].value = "perivasale Anwendung" 
* #900000000007 "intratendineale Anwendung"
* #900000000007 ^designation[0].language = #de-AT 
* #900000000007 ^designation[0].value = "intratendineale Anwendung" 
* #900000000008 "subepitheliale Anwendung"
* #900000000008 ^designation[0].language = #de-AT 
* #900000000008 ^designation[0].value = "subepitheliale Anwendung" 
* #900000000009 "intrakavitäre Anwendung"
* #900000000009 ^designation[0].language = #de-AT 
* #900000000009 ^designation[0].value = "intrakavitäre Anwendung" 
* #900000000010 "peridurale Anwendung"
* #900000000010 ^designation[0].language = #de-AT 
* #900000000010 ^designation[0].value = "peridurale Anwendung" 
* #900000000012 "Hämofiltration"
* #900000000012 ^designation[0].language = #de-AT 
* #900000000012 ^designation[0].value = "Hämofiltration" 
* #900000000013 "parenterale Perfusion"
* #900000000013 ^designation[0].language = #de-AT 
* #900000000013 ^designation[0].value = "parenterale Perfusion" 
* #900000000014 "zur Instillation"
* #900000000014 ^designation[0].language = #de-AT 
* #900000000014 ^designation[0].value = "zur Instillation" 
* #900000000015 "zur Wurzelkanalbehandlung"
* #900000000015 ^designation[0].language = #de-AT 
* #900000000015 ^designation[0].value = "zur Wurzelkanalbehandlung" 
