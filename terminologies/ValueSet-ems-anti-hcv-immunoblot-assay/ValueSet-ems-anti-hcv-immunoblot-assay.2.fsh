Instance: ems-anti-hcv-immunoblot-assay 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-ems-anti-hcv-immunoblot-assay" 
* name = "ems-anti-hcv-immunoblot-assay" 
* title = "EMS_Anti-HCV-Immunoblot_Assay" 
* status = #active 
* version = "201902" 
* description = "**Beschreibung:** Dieses Valueset wird verwendet um das Ergebnis des Anti-HCV Immunoblot Assays zu bewerten. Verwendung im Zuge vom EMS" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.91" 
* date = "2019-01-01" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-ems-posneg"
* compose.include[0].concept[0].code = "NAW"
* compose.include[0].concept[0].display = "nicht auswertbar"
* compose.include[0].concept[1].code = "NEG"
* compose.include[0].concept[1].display = "negativ"
* compose.include[0].concept[2].code = "NOTEST"
* compose.include[0].concept[2].display = "nicht durchgeführt"
* compose.include[0].concept[3].code = "POS"
* compose.include[0].concept[3].display = "positiv"
* compose.include[0].concept[4].code = "UB"
* compose.include[0].concept[4].display = "unbestimmt"
