Instance: oeaek-spezialisierung 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-oeaek-spezialisierung" 
* name = "oeaek-spezialisierung" 
* title = "OEAEK_Spezialisierung" 
* status = #active 
* content = #complete 
* version = "1.0" 
* description = "**Description:** Code List of all specialization diplomas valid in Austria issued by the Medical Chamber of Austria

**Beschreibung:** Code Liste aller in Österreich gültigen Spezialisierungen" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.3.1.10.60" 
* date = "2018-07-01" 
* count = 6 
* #1 "Spez. Geriatrie"
* #1 ^designation[0].language = #de-AT 
* #1 ^designation[0].value = "Spez. Geriatrie" 
* #2 "Spez. Phoniatrie"
* #2 ^designation[0].language = #de-AT 
* #2 ^designation[0].value = "Spez. Phoniatrie" 
* #3 "Spez. Handchirurgie"
* #3 ^designation[0].language = #de-AT 
* #3 ^designation[0].value = "Spez. Handchirurgie" 
* #4 "Spez. Palliativmedizin"
* #4 ^designation[0].language = #de-AT 
* #4 ^designation[0].value = "Spez. Palliativmedizin" 
* #5 "Spez. Dermatohistopathologie"
* #5 ^designation[0].language = #de-AT 
* #5 ^designation[0].value = "Spez. Dermatohistopathologie" 
* #6 "Spez. in fachspezifischer psychosomatischer Medizin"
* #6 ^designation[0].language = #de-AT 
* #6 ^designation[0].value = "Spez. in fachspezifischer psychosomatischer Medizin" 
