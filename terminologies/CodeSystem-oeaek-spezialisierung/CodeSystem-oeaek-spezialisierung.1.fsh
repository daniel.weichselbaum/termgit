Instance: oeaek-spezialisierung 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-oeaek-spezialisierung" 
* name = "oeaek-spezialisierung" 
* title = "OEAEK_Spezialisierung" 
* status = #active 
* content = #complete 
* version = "1.0" 
* description = "**Description:** Code List of all specialization diplomas valid in Austria issued by the Medical Chamber of Austria

**Beschreibung:** Code Liste aller in Österreich gültigen Spezialisierungen" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.3.1.10.60" 
* date = "2018-07-01" 
* count = 6 
* concept[0].code = #1
* concept[0].display = "Spez. Geriatrie"
* concept[0].designation[0].language = #de-AT 
* concept[0].designation[0].value = "Spez. Geriatrie" 
* concept[1].code = #2
* concept[1].display = "Spez. Phoniatrie"
* concept[1].designation[0].language = #de-AT 
* concept[1].designation[0].value = "Spez. Phoniatrie" 
* concept[2].code = #3
* concept[2].display = "Spez. Handchirurgie"
* concept[2].designation[0].language = #de-AT 
* concept[2].designation[0].value = "Spez. Handchirurgie" 
* concept[3].code = #4
* concept[3].display = "Spez. Palliativmedizin"
* concept[3].designation[0].language = #de-AT 
* concept[3].designation[0].value = "Spez. Palliativmedizin" 
* concept[4].code = #5
* concept[4].display = "Spez. Dermatohistopathologie"
* concept[4].designation[0].language = #de-AT 
* concept[4].designation[0].value = "Spez. Dermatohistopathologie" 
* concept[5].code = #6
* concept[5].display = "Spez. in fachspezifischer psychosomatischer Medizin"
* concept[5].designation[0].language = #de-AT 
* concept[5].designation[0].value = "Spez. in fachspezifischer psychosomatischer Medizin" 
