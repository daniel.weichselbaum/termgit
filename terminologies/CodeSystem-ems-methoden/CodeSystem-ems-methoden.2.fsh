Instance: ems-methoden 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-methoden" 
* name = "ems-methoden" 
* title = "EMS_Methoden" 
* status = #active 
* content = #complete 
* description = "**Beschreibung:** Liste der Methoden welche im Zuge einer EMS Meldung codiert werden müssen" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.102" 
* date = "2017-01-26" 
* count = 208 
* #AGGLUAUTO "Autoagglutinationstest"
* #AGGLUAUTO ^definition = Autoagglutinationstest
* #AGGLUGEL "Geldiffusionstest"
* #AGGLUGEL ^definition = Geldiffusionstest
* #AGGLUKO "Koagglutination"
* #AGGLUKO ^definition = Koagglutination
* #AGGLULATEX "Latexagglutinationstest"
* #AGGLULATEX ^definition = Latexagglutinationstest
* #AGGLUOBJEKT "Objektträgeragglutination"
* #AGGLUOBJEKT ^definition = Objektträgeragglutination
* #AGGLUPNEUMOTEST "Pneumotest©"
* #AGGLUPNEUMOTEST ^definition = Pneumotest©
* #AGGLUQUELL "Quellungsreaktion"
* #AGGLUQUELL ^definition = Quellungsreaktion
* #AGGLUSERO "Serotyp Agglutination"
* #AGGLUSERO ^definition = Serotyp Agglutination | Serotyp-Agglutination
* #AGGLUSEROVAR "Serovarbestimmung (Agglutination)"
* #AGGLUSEROVAR ^definition = Serovarbestimmung (Agglutination)
* #ANTIGEN "Antigennachweis"
* #ANTIGEN ^definition = Antigennachweis | Antigen-Nachweis | Antigennachweis (z.B. ELISA) | Antigennachweis (z.B. ELISA, einschließlich Schnelltest)
* #ANTIGENATEMLUNG "Antigen-Nachweis in Atemwegssekreten oder Lungengewebe"
* #ANTIGENATEMLUNG ^definition = Antigen-Nachweis in Atemwegssekreten oder Lungengewebe
* #ANTIGENDFA "Antigen-Nachweis durch DFA in klinischer Probe (Verw. masernspezif.monoklonaler Antikörper)"
* #ANTIGENDFA ^definition = Antigen-Nachweis durch DFA in klinischer Probe (Verw. masernspezif.monoklonaler Antikörper)
* #ANTIGENELISA "Antigen-ELISA"
* #ANTIGENELISA ^definition = Antigen-ELISA | EIA | ELISA
* #ANTIGENF1 "Yersinia-pestis-Anti-F1-Antigen-spezifische Antikörperreaktion"
* #ANTIGENF1 ^definition = Yersinia-pestis-Anti-F1-Antigen-spezifische Antikörperreaktion
* #ANTIGENHBE "Hepatitis-B-e-Antigen (HBeAg)"
* #ANTIGENHBE ^definition = Hepatitis-B-e-Antigen (HBeAg)
* #ANTIGENHBS "HBsAg"
* #ANTIGENHBS ^definition = HBsAg | Oberflächenantigen der Hepatitis B (HBsAg)
* #ANTIGENHCV "Nachweis von Kernantigen des Hepatitis-C-Virus (HCV-Kern)"
* #ANTIGENHCV ^definition = Nachweis von Kernantigen des Hepatitis-C-Virus (HCV-Kern)
* #ANTIGENIFT "Antigennachweis mittels IFT nur in Gewebeproben (z.B. Milz, Lunge)"
* #ANTIGENIFT ^definition = Antigennachweis mittels IFT nur in Gewebeproben (z.B. Milz, Lunge)
* #ANTIGENO1 "Nachweis von O1- oder O139-Antigen im Isolat"
* #ANTIGENO1 ^definition = Nachweis von O1- oder O139-Antigen im Isolat
* #ANTIGENST "Antigennachweis im Stuhl"
* #ANTIGENST ^definition = Antigennachweis im Stuhl
* #ANTIGENSTERILE "Antigennachweis aus einer normalerweise sterilen Probe"
* #ANTIGENSTERILE ^definition = Antigennachweis aus einer normalerweise sterilen Probe | Antigennachweis in einer normalerweise sterilen Probe | Antigen-Nachweis in einer normalerweise sterilen Probe
* #ANTIGENSTU "Spezies-spezifischer Antigennachweis (E. histolytica-Koproantigen) im Stuhl"
* #ANTIGENSTU ^definition = Spezies-spezifischer Antigennachweis (E. histolytica-Koproantigen) im Stuhl
* #ANTIGENU "Antigennachweis im Urin"
* #ANTIGENU ^definition = Antigennachweis im Urin
* #ANTIGENVDFA "Nachweis viraler  Antigene durch DFA in einer klinischen Probe"
* #ANTIGENVDFA ^definition = Nachweis viraler  Antigene durch DFA in einer klinischen Probe
* #ANTIK "deutliche Änderung zwischen zwei Proben beim Antikörpernachweis mittels KBR"
* #ANTIK ^definition = deutliche Änderung zwischen zwei Proben beim Antikörpernachweis mittels KBR
* #ANTIKAVT "IgG-AVT"
* #ANTIKAVT ^definition = IgG-AVT
* #ANTIKCSF "spezifische Antikörperreaktion durch Virusneutralisierungsassay in Liquor cerebrospinalis (CSF)"
* #ANTIKCSF ^definition = spezifische Antikörperreaktion durch Virusneutralisierungsassay in Liquor cerebrospinalis (CSF)
* #ANTIKDENG "deutl.Änderung zw. 2 Proben beim IgG-Antikörpernachweis gegen Antigene e.d. 4 Dengue-Serotypen"
* #ANTIKDENG ^definition = deutl.Änderung zw. 2 Proben beim IgG-Antikörpernachweis gegen Antigene e.d. 4 Dengue-Serotypen
* #ANTIKECOL "E-coli-Serogruppen-spezifische Antikörperreaktion"
* #ANTIKECOL ^definition = E-coli-Serogruppen-spezifische Antikörperreaktion
* #ANTIKELISAIGG "IgG Elisa"
* #ANTIKELISAIGG ^definition = IgG Elisa
* #ANTIKELISAIGM "IgM-ELisa"
* #ANTIKELISAIGM ^definition = IgM-ELisa
* #ANTIKFSME "Nachweis intrathekal gebildeter FSME-spezifischer Antikörper (erhöhter Liquor/Serum-Index)"
* #ANTIKFSME ^definition = Nachweis intrathekal gebildeter FSME-spezifischer Antikörper (erhöhter Liquor/Serum-Index)
* #ANTIKHAV "Anti-HAV-IgM"
* #ANTIKHAV ^definition = Anti-HAV-IgM
* #ANTIKHBC "Anti-HBc-IgM"
* #ANTIKHBC ^definition = Anti-HBc-IgM | IgM-Antikörper gegen das HBc-Antigen (Anti-HBc IgM)
* #ANTIKHCV "HCV spezifische Antikörperreaktion bestätigt durch einen Antikörpertest"
* #ANTIKHCV ^definition = HCV spezifische Antikörperreaktion bestätigt durch einen Antikörpertest
* #ANTIKHCVPCR "HCV spezifische Antikörperreaktion bestätigt durch Nachweis von Nukleinsäure des HCV im Serum bei Personen >18 Monate ohne Nachweis einer abgeklungenen Infektion"
* #ANTIKHCVPCR ^definition = HCV spezifische Antikörperreaktion bestätigt durch Nachweis von Nukleinsäure des HCV im Serum bei Personen >18 Monate ohne Nachweis einer abgeklungenen Infektion
* #ANTIKHEV "Anti-HEV-IgM"
* #ANTIKHEV ^definition = Anti-HEV-IgM
* #ANTIKIFA "spezifische Antikörperreaktion (IFA-Test, ELISA oder Western Blot)"
* #ANTIKIFA ^definition = spezifische Antikörperreaktion (IFA-Test, ELISA oder Western Blot)
* #ANTIKIGG "IgM od IgA-Antikörpernachweis bestätigt durch IgG-Antikörpernachweis"
* #ANTIKIGG ^definition = IgM od IgA-Antikörpernachweis bestätigt durch IgG-Antikörpernachweis
* #ANTIKIGM "IgM-Antikörpernachweis"
* #ANTIKIGM ^definition = IgM-Antikörpernachweis | IgM-Antikörpernachweis (z.B. µCapture-ELISA) | IgM-Antikörpernachweis (z.B. ELISA) | spezifische Antikörperreaktion (IgM)
* #ANTIKIGMDENG "IgM-Antikörpernachweis gegen Antigene eines der vier Denue-Serotypen"
* #ANTIKIGMDENG ^definition = IgM-Antikörpernachweis gegen Antigene eines der vier Denue-Serotypen
* #ANTIKIGMIGG "IgM und IgG Antikörpernachweis"
* #ANTIKIGMIGG ^definition = IgM und IgG Antikörpernachweis
* #ANTIKIGMIGGBL "IgM- und IgG-Antikörpernachweis (einmalig deutlich erhöhter Wert, z.B. ELISA, NT) nur in Blut oder Liquor"
* #ANTIKIGMIGGBL ^definition = IgM- und IgG-Antikörpernachweis (einmalig deutlich erhöhter Wert, z.B. ELISA, NT) nur in Blut oder Liquor
* #ANTIKINFL "vierfacher Anstieg der für das neuartige Influenza-Virus des Typs A(H1N1) spezifischen neutralisierenden Antikörper"
* #ANTIKINFL ^definition = vierfacher Anstieg der für das neuartige Influenza-Virus des Typs A(H1N1) spezifischen neutralisierenden Antikörper
* #ANTIKKBR "Chlamydiales-Antikörpernachweis mittels KBR (deutliche Änderung zwischen 2 Proben ODER einmaliger deutlich erhöhter Wert) nur bei Kontakt mit potentiell infizierten Vögeln oder ihren Ausscheidungen unter Berücksichtigung der Inkubationszeit"
* #ANTIKKBR ^definition = Chlamydiales-Antikörpernachweis mittels KBR (deutliche Änderung zwischen 2 Proben ODER einmaliger deutlich erhöhter Wert) nur bei Kontakt mit potentiell infizierten Vögeln oder ihren Ausscheidungen unter Berücksichtigung der Inkubationszeit
* #ANTIKKONVERS "spezifische Antikörperreaktion: Serokonversion d.ELISA od.IFA im parallel getesteten Serum d.akuten"
* #ANTIKKONVERS ^definition = spezifische Antikörperreaktion: Serokonversion d.ELISA od.IFA im parallel getesteten Serum d.akuten
* #ANTIKLEGIGEPAART "spezifische Antikörperreaktion gegen L.pneumophila der Serumgruppe 1 in gepaarten Serumproben"
* #ANTIKLEGIGEPAART ^definition = spezifische Antikörperreaktion gegen L.pneumophila der Serumgruppe 1 in gepaarten Serumproben
* #ANTIKLEGISG "einmalige erhöhte Aktivität spezifischer Serum-Antikörper gegen Legionella pneumophila der Serumgruppe 1"
* #ANTIKLEGISG ^definition = einmalige erhöhte Aktivität spezifischer Serum-Antikörper gegen Legionella pneumophila der Serumgruppe 1
* #ANTIKLEGISGAND "signifikante Zunahme der spezifischen Antikörper gegen andere L.pneumophila als Serumgruppe 1 od.andere Legionella spp. in gepaarten Serumproben"
* #ANTIKLEGISGAND ^definition = signifikante Zunahme der spezifischen Antikörper gegen andere L.pneumophila als Serumgruppe 1 od.andere Legionella spp. in gepaarten Serumproben
* #ANTIKLPS "Antikörpernachweis von LPS (lipopolysaccarides) in HUS-Fällen"
* #ANTIKLPS ^definition = Antikörpernachweis von LPS (lipopolysaccarides) in HUS-Fällen
* #ANTIKMEASLE "akute Infektion: charakteristische Masernvirus-spezif.Antikörperreaktion in Serum od.Speichel"
* #ANTIKMEASLE ^definition = akute Infektion: charakteristische Masernvirus-spezif.Antikörperreaktion in Serum od.Speichel
* #ANTIKMIF "Antikörpernachweis mittels C.-psittaci-spezifischer MIF"
* #ANTIKMIF ^definition = Antikörpernachweis mittels C.-psittaci-spezifischer MIF
* #ANTIKMIKROTITER "Mikrotiterverfahren"
* #ANTIKMIKROTITER ^definition = Mikrotiterverfahren
* #ANTIKNACHW "Nachweis einer Antikörperreaktion"
* #ANTIKNACHW ^definition = Nachweis einer Antikörperreaktion
* #ANTIKOERPER "Antikörperbestimmung / Antikörpernachweis"
* #ANTIKOERPER ^definition = Antikörperbestimmung
* #ANTIKPGL "PGL-I-Antikörpernachweis (einmaliger deutlich erhöhter Wert)"
* #ANTIKPGL ^definition = PGL-I-Antikörpernachweis (einmaliger deutlich erhöhter Wert)
* #ANTIKPOS "ein einziger positiver Antikörpertest"
* #ANTIKPOS ^definition = ein einziger positiver Antikörpertest
* #ANTIKSERO "Serum-Antikörper durch serolog. Test mit hoher Sensitivität UND Bestätigung durch serologischen Test"
* #ANTIKSERO ^definition = Serum-Antikörper durch serolog. Test mit hoher Sensitivität UND Bestätigung durch serologischen Test | Serum-Antikörper durch serologischen Test mit hoher Sensitivität UND Bestätigung durch serologischen Test
* #ANTIKSERUM "spezifische Antikörperreaktion (IgG) im Serum oder Speichel"
* #ANTIKSERUM ^definition = spezifische Antikörperreaktion (IgG) im Serum oder Speichel
* #ANTIKSPEZ "spezifische Antikörperreaktion"
* #ANTIKSPEZ ^definition = spezifische Antikörperreaktion
* #ANTIKTITER "spezifische Antikörperreaktion. 4facher od.höherer Anstieg im Antikörpertitet zw.den parallel getest"
* #ANTIKTITER ^definition = spezifische Antikörperreaktion. 4facher od.höherer Anstieg im Antikörpertitet zw.den parallel getest
* #ANTIKVIERFACH "spezifische Antikörperreaktion (vierfach oder höher Anstieg oder ein einziger erhöhter Titer)"
* #ANTIKVIERFACH ^definition = spezifische Antikörperreaktion (vierfach oder höher Anstieg oder ein einziger erhöhter Titer)
* #ANTIKVIRUS "spezifische Antikörperreaktion durch Virusneutralisierungsassay im Serum"
* #ANTIKVIRUS ^definition = spezifische Antikörperreaktion durch Virusneutralisierungsassay im Serum
* #ANTIKZWEI "deutl.Änderung zw.2 Proben beim IgG-Antikörpernachweis"
* #ANTIKZWEI ^definition = deutl.Änderung zw.2 Proben beim IgG-Antikörpernachweis | deutliche Änderung zw. 2 Proben beim IgG-Antikörpernachweis | deutliche Änderung zwischen zwei Proben beim IgG-Antikörpernachweis | deutliche Änderung  zwischen zwei Proben beim IgG-Antikörpernachweis (z.B. ELISA, IFT) | deutliche Änderung zwischen zwei Proben beim IgG-Antikörpernachweis (z.B. ELISA, NT)
* #ANZUCHTBIOCHEM "Anzüchtung und biochemische Identifizierung"
* #ANZUCHTBIOCHEM ^definition = Anzüchtung und biochemische Identifizierung
* #ANZUCHTCIN "Anzucht auf CIN-Agar (Schiemann)"
* #ANZUCHTCIN ^definition = Anzucht auf CIN-Agar (Schiemann)
* #ANZUCHTKEIM "Keimanzucht"
* #ANZUCHTKEIM ^definition = Keimanzucht
* #ANZUCHTSTUHL "Anzucht mittels Stuhlkultur"
* #ANZUCHTSTUHL ^definition = Anzucht mittels Stuhlkultur
* #BILDGEBCJDEEG "keine typ.Erscheinungsform sporadischer CJD im EEG während frühen Stadien d.Erkr."
* #BILDGEBCJDEEG ^definition = keine typ.Erscheinungsform sporadischer CJD im EEG während frühen Stadien d.Erkr.
* #BILDGEBCJDSTAD "keine typische Erscheinungsform sporadischer CJD im EEG während der frühen Stadien der Erkrankung"
* #BILDGEBCJDSTAD ^definition = keine typische Erscheinungsform sporadischer CJD im EEG während der frühen Stadien der Erkrankung
* #BILDGEBMORPH "Nachweis durch pathognomonische makroskopische Morphologie v.Zysten in chirurgischen Proben"
* #BILDGEBMORPH ^definition = Nachweis durch pathognomonische makroskopische Morphologie v.Zysten in chirurgischen Proben
* #BILDGEBMRT "beidseitig hohes Pulvinar-Signal im MRT"
* #BILDGEBMRT ^definition = beidseitig hohes Pulvinar-Signal im MRT
* #BILDGEBORGAN "Nachweis typischer Organläsionen in bildgebenden Verfahren UND Bestätigung durch serologischen Test"
* #BILDGEBORGAN ^definition = Nachweis typischer Organläsionen in bildgebenden Verfahren UND Bestätigung durch serologischen Test
* #BILDGEBSONO "Bildgebende Verfahren (z.B. Sonographie, CT)"
* #BILDGEBSONO ^definition = Bildgebende Verfahren (z.B. Sonographie, CT)
* #BIOCHEMAPI "api 20 E"
* #BIOCHEMAPI ^definition = api 20 E
* #BIOCHEMBIOVAR "Biovarbestimmung (\"bunte Reihe\")"
* #BIOCHEMBIOVAR ^definition = Biovarbestimmung ("bunte Reihe")
* #BIOCHEMTEST "biochemische Tests"
* #BIOCHEMTEST ^definition = biochemische Tests
* #CULT "Kultur"
* #CULT ^definition = Kultur
* #ELEKTEST "Elek-Test"
* #ELEKTEST ^definition = Elek-Test
* #ERREGER "Erregerisolierung"
* #ERREGER ^definition = Erregerisolierung
* #ERREGERATEM "Erregerisolierung aus Atemwegssekreten oder normalerweise sterilen Proben"
* #ERREGERATEM ^definition = Erregerisolierung aus Atemwegssekreten oder normalerweise sterilen Proben
* #ERREGERBACI "Isolierung des Bacillus anthracis aus einer klinischen Probe"
* #ERREGERBACI ^definition = Isolierung des Bacillus anthracis aus einer klinischen Probe
* #ERREGERBLUT "Erregerisolierung aus Blut"
* #ERREGERBLUT ^definition = Erregerisolierung aus Blut
* #ERREGERBOTU "Erregerisolierung bei Kinderbotulismus (Stuhl) ODER Wundbotulismus (Wunde)"
* #ERREGERBOTU ^definition = Erregerisolierung bei Kinderbotulismus (Stuhl) ODER Wundbotulismus (Wunde)
* #ERREGERCSF "Erregerisolierung aus normalerweise sterilen Körperflüssigkeiten und -geweben (z. B.CSF, Knochen, Synovialflüssigkeit)"
* #ERREGERCSF ^definition = Erregerisolierung aus normalerweise sterilen Körperflüssigkeiten und -geweben (z. B.CSF, Knochen, Synovialflüssigkeit)
* #ERREGERCULT "Erregerisolierung (kulturell)"
* #ERREGERCULT ^definition = Erregerisolierung (kulturell)
* #ERREGERECOLI "Erregerisolierung (kulturell) nur aus Stuhl und Zuordnung des Isolats zu einem E.-coli-Pathovar (EPEC; ETEC; EIEC; EAggEC; DAEC)"
* #ERREGERECOLI ^definition = Erregerisolierung (kulturell) nur aus Stuhl und Zuordnung des Isolats zu einem E.-coli-Pathovar (EPEC; ETEC; EIEC; EAggEC; DAEC)
* #ERREGERHAUTBLUT "Erregerisolierung aus einer normalerweise sterilen Probe oder aus Hautblutungen"
* #ERREGERHAUTBLUT ^definition = Erregerisolierung aus einer normalerweise sterilen Probe oder aus Hautblutungen
* #ERREGERISOKLIN "Isolierung aus klinischer Probe"
* #ERREGERISOKLIN ^definition = Isolierung aus klinischer Probe
* #ERREGERKLIN "Erregerisolierung aus einer klinischen Probe"
* #ERREGERKLIN ^definition = Erregerisolierung aus einer klinischen Probe
* #ERREGERKOERPER "Erregerisolierung aus einer Körperstelle (z. B. einer infizierten Wunde)"
* #ERREGERKOERPER ^definition = Erregerisolierung aus einer Körperstelle (z. B. einer infizierten Wunde)
* #ERREGERLISTKIND "Erregerisolierung aus einer normalerweise sterilen Probe (Fötus, Totgeboren, Neugeboren)"
* #ERREGERLISTKIND ^definition = Erregerisolierung aus einer normalerweise sterilen Probe (Fötus, Totgeboren, Neugeboren)
* #ERREGERLISTMUTTER "Erregerisolierung aus einer normalerweise sterilen Probe (Mutter bei Geburt od.innerhalb v.24h nachher"
* #ERREGERLISTMUTTER ^definition = Erregerisolierung aus einer normalerweise sterilen Probe (Mutter bei Geburt od.innerhalb v.24h nachher
* #ERREGERNACHW "Erreger-Nachweis"
* #ERREGERNACHW ^definition = Erreger-Nachweis
* #ERREGERNACHWBLUT "Erregernachweis im Blut"
* #ERREGERNACHWBLUT ^definition = Erregernachweis im Blut
* #ERREGERPERT "Isolierung B.pertussis aus einer klinischen Probe"
* #ERREGERPERT ^definition = Isolierung B.pertussis aus einer klinischen Probe
* #ERREGERSEQU "Isolierung aus einer klinischen Probe mit anschließender Sequenzierung"
* #ERREGERSEQU ^definition = Isolierung aus einer klinischen Probe mit anschließender Sequenzierung
* #ERREGERSTERIL "Erregerisolierung aus einer normalerweise sterilen Probe"
* #ERREGERSTERIL ^definition = Erregerisolierung aus einer normalerweise sterilen Probe
* #ERREGERSTUHL "Erregerisolierung aus Stuhl"
* #ERREGERSTUHL ^definition = Erregerisolierung aus Stuhl
* #ERREGERSTX "Isolierung eines Shigatoxin (Stx) produzierenden oder (ein) stx1- oder stx2-Gen(e) tragenden Escherichia coli- Stammes"
* #ERREGERSTX ^definition = Isolierung eines Shigatoxin (Stx) produzierenden oder (ein) stx1- oder stx2-Gen(e) tragenden Escherichia coli- Stammes
* #ERREGERSTX0157 "Isolierung von nicht sorbitol-fermentierenden Escherichia coli O157 (ohne Test der Stx- oder stx-Gene)"
* #ERREGERSTX0157 ^definition = Isolierung von nicht sorbitol-fermentierenden Escherichia coli O157 (ohne Test der Stx- oder stx-Gene)
* #ERREGERTROPHO "Nachweis von Trophozoiten im Biopsiematerial"
* #ERREGERTROPHO ^definition = Nachweis von Trophozoiten im Biopsiematerial
* #ERREGERURIN "Erregerisolierung aus Urin"
* #ERREGERURIN ^definition = Erregerisolierung aus Urin
* #HHT "Hämagglutinations-Hemmtest (HHT)"
* #HHT ^definition = Hämagglutinations-Hemmtest (HHT)
* #HHTINDIREKT "indirekter Hämagglutinationstest (IHA)"
* #HHTINDIREKT ^definition = indirekter Hämagglutinationstest (IHA)
* #HISTOPATHOGEWEB "charakteristische histologische Veränderungen in Gewebeproben"
* #HISTOPATHOGEWEB ^definition = charakteristische histologische Veränderungen in Gewebeproben
* #HISTOPATHOPARASIT "Histopathologie oder Parasitologie (z.B. direkte Sichtbarkeit des Protoskolex i.d.Zystenflüssigkeit)"
* #HISTOPATHOPARASIT ^definition = Histopathologie oder Parasitologie (z.B. direkte Sichtbarkeit des Protoskolex i.d.Zystenflüssigkeit)
* #HISTOPATHOPOST "Nachweis typischer Läsionen bei der Post-mortem-Leberhistopathologie"
* #HISTOPATHOPOST ^definition = Nachweis typischer Läsionen bei der Post-mortem-Leberhistopathologie
* #HISTOPATHOTONSILLEN "positiver Befund der Tonsillenbiopsie"
* #HISTOPATHOTONSILLEN ^definition = positiver Befund der Tonsillenbiopsie
* #HISTOPATHOVCJK "Hirnbiopsie oder -autopsie: für vCJK typische Histopathologie oder Immunhistopathologie"
* #HISTOPATHOVCJK ^definition = Hirnbiopsie oder -autopsie: für vCJK typische Histopathologie oder Immunhistopathologie
* #IGMUNEUTZIKA "Nachweise von Zika Virus spezifische IgM Antikörper UND Bestätigung durch Neutralizationstest (bestätigt)"
* #IGMUNEUTZIKA ^definition = Nachweise von Zika Virus spezifische IgM Antikörper UND Bestätigung durch Neutralizationstest (bestätigt)
* #IMMUNFLUORDNA "DNA-Analyse"
* #IMMUNFLUORDNA ^definition = DNA-Analyse
* #IMMUNFLUORDNACHW "DNA-Nachweis"
* #IMMUNFLUORDNACHW ^definition = DNA-Nachweis
* #IMMUNFLUORIFT "Immunfluoreszenztest (IFT)"
* #IMMUNFLUORIFT ^definition = Immunfluoreszenztest (IFT)
* #IMMUNFLUORKLIN "Nachweis in einer klinischen Probe durch Immunfluoreszenz"
* #IMMUNFLUORKLIN ^definition = Nachweis in einer klinischen Probe durch Immunfluoreszenz
* #IMMUNFLUORTEST "immundiagnostische Tests"
* #IMMUNFLUORTEST ^definition = immundiagnostische Tests
* #IMMUNFLUORTOXINA "Immunoenzymatischer Test, Toxin A"
* #IMMUNFLUORTOXINA ^definition = Immunoenzymatischer Test, Toxin A
* #IMMUNFLUORTOXINAB "Immunoenzymatischer Test, Toxin A und B"
* #IMMUNFLUORTOXINAB ^definition = Immunoenzymatischer Test, Toxin A und B
* #KBRKBR "KBR"
* #KBRKBR ^definition = KBR
* #KBRREAKT "Komplementbindungsreaktion (KBR)"
* #KBRREAKT ^definition = Komplementbindungsreaktion (KBR)
* #MIKROSKOPIEBIOPS "Elektronenmikroskopie (z.B. Leberbiopsien, post mortem)"
* #MIKROSKOPIEBIOPS ^definition = Elektronenmikroskopie (z.B. Leberbiopsien, post mortem)
* #MIKROSKOPIEDIPLO "Nachweis von gram-negativem gefärbten Diplokokkus im CSF"
* #MIKROSKOPIEDIPLO ^definition = Nachweis von gram-negativem gefärbten Diplokokkus im CSF
* #MIKROSKOPIEDUNKEL "mikroskopischer Erregernachweis im Dunkelfeld, Phasenkontrast oder im gefärbten Ausstrich"
* #MIKROSKOPIEDUNKEL ^definition = mikroskopischer Erregernachweis im Dunkelfeld, Phasenkontrast oder im gefärbten Ausstrich
* #MIKROSKOPIEELEKTR "Elektronenmikroskopie"
* #MIKROSKOPIEELEKTR ^definition = Elektronenmikroskopie
* #MIKROSKOPIEHYSTOLYTICA "Mikroskopischer Nachweis von erythrophagen E. histolytica-Trophozoiten (im Stuhl)"
* #MIKROSKOPIEHYSTOLYTICA ^definition = Mikroskopischer Nachweis von erythrophagen E. histolytica-Trophozoiten (im Stuhl)
* #MIKROSKOPIEMALA "Nachweis v.Malaria-Parasiten im Blutausstrich durch Lichtmikroskop"
* #MIKROSKOPIEMALA ^definition = Nachweis v.Malaria-Parasiten im Blutausstrich durch Lichtmikroskop
* #MIKROSKOPIEMIKRO "Mikroskopie"
* #MIKROSKOPIEMIKRO ^definition = Mikroskopie
* #MIKROSKOPIEORTHO "Identifizierung von Orthopox-Virus-Partikeln im EM"
* #MIKROSKOPIEORTHO ^definition = Identifizierung von Orthopox-Virus-Partikeln im EM
* #MIKROSKOPIESTAEBCHEN "mikroskopischer Nachweis säurefester Stäbchen"
* #MIKROSKOPIESTAEBCHEN ^definition = mikroskopischer Nachweis säurefester Stäbchen
* #MIKROSKOPIETRICHI "Nachweis von Trichinella-Larven in durch Muskelbiopsie gewonnenem Gewebe"
* #MIKROSKOPIETRICHI ^definition = Nachweis von Trichinella-Larven in durch Muskelbiopsie gewonnenem Gewebe
* #MIKROSKOPIEZYST "Nachweis von Zysten oder Vegetativformen im Stuhl"
* #MIKROSKOPIEZYST ^definition = Nachweis von Zysten oder Vegetativformen im Stuhl
* #MOLEKULARBIOAMPLI "DNA-Sequenzanalyse der Amplifikationsprodukte"
* #MOLEKULARBIOAMPLI ^definition = DNA-Sequenzanalyse der Amplifikationsprodukte
* #MOLEKULARBIONACHW "molekularbiologischer Nachweis"
* #MOLEKULARBIONACHW ^definition = molekularbiologischer Nachweis
* #MOLEKULARBIOTYP "molekularbiologischer Nachweis und Typisierung"
* #MOLEKULARBIOTYP ^definition = molekularbiologischer Nachweis und Typisierung
* #NICHTDURCH "nicht durchgeführt"
* #NICHTDURCH ^definition = Nicht durchgeführt
* #NUCLACIDATEM "Nukleinsäure-Nachweis in Atemwegssekreten, Lungengewebe oder einer klinischen Probe"
* #NUCLACIDATEM ^definition = Nukleinsäure-Nachweis in Atemwegssekreten, Lungengewebe oder einer klinischen Probe
* #NUCLACIDBAC "Nachweis der Nukleinsäure von Bacillus anthracis in einer klinischen Probe"
* #NUCLACIDBAC ^definition = Nachweis der Nukleinsäure von Bacillus anthracis in einer klinischen Probe
* #NUCLACIDBLUT "Nukleinsäure-Nachweis im Blut"
* #NUCLACIDBLUT ^definition = Nukleinsäure-Nachweis im Blut
* #NUCLACIDDIREKT "direkter Erregernachweis mittels PCR"
* #NUCLACIDDIREKT ^definition = direkter Erregernachweis mittels PCR
* #NUCLACIDF1 "Nukleinsäure-Nachweis in einer klinischen Probe (z.B.F1-Antigen)"
* #NUCLACIDF1 ^definition = Nukleinsäure-Nachweis in einer klinischen Probe (z.B.F1-Antigen)
* #NUCLACIDGEN "Nukleinsäure-Nachweis eines Gens f.etablierte Virulenzfaktoren nur in Mischkultur, Stuhlanreicherungskultur oder E.-coli-Isolat UND Zuordnung zu E.-coli-Pathovar"
* #NUCLACIDGEN ^definition = Nukleinsäure-Nachweis eines Gens f.etablierte Virulenzfaktoren nur in Mischkultur, Stuhlanreicherungskultur oder E.-coli-Isolat UND Zuordnung zu E.-coli-Pathovar
* #NUCLACIDHAUTBL "Nukleinsäure-Nachweis aus einer normalerweise sterilen Probe oder aus Hautblutungen"
* #NUCLACIDHAUTBL ^definition = Nukleinsäure-Nachweis aus einer normalerweise sterilen Probe oder aus Hautblutungen
* #NUCLACIDHBV "Hepatitis-B-Nukleinsäure (HBV-DNA)"
* #NUCLACIDHBV ^definition = Hepatitis-B-Nukleinsäure (HBV-DNA)
* #NUCLACIDHCV "HCV-Nukleinsäure-Nachweis"
* #NUCLACIDHCV ^definition = HCV-Nukleinsäure-Nachweis
* #NUCLACIDHCVRNA "Nukleinsäure-Nachweis im Serum (HCV-RNA)"
* #NUCLACIDHCVRNA ^definition = Nukleinsäure-Nachweis im Serum (HCV-RNA)
* #NUCLACIDHEV "HEV-Nukleinsäure-Nachweis"
* #NUCLACIDHEV ^definition = HEV-Nukleinsäure-Nachweis
* #NUCLACIDKLIN "ein pos.PCR-Ergebnis an einer einzigen klinischen Probe u.in einem einzigen Test"
* #NUCLACIDKLIN ^definition = ein pos.PCR-Ergebnis an einer einzigen klinischen Probe u.in einem einzigen Test
* #NUCLACIDKLINPROB "Nachweis von Nukleinsäure in einer klinischen Probe"
* #NUCLACIDKLINPROB ^definition = Nachweis von Nukleinsäure in einer klinischen Probe | Nukleinsäure-Nachweis aus einer klinischen Probe | Nukleinsäure-Nachweis in einer klinischen Probe | Nukleinsäure-Nachweis in klinischer Probe
* #NUCLACIDMOMP "Nukleinsäure-Nachweis (z.B. PCR) des MOMP1-Gens"
* #NUCLACIDMOMP ^definition = Nukleinsäure-Nachweis (z.B. PCR) des MOMP1-Gens
* #NUCLACIDMULTI "Multiplex PCR"
* #NUCLACIDMULTI ^definition = Multiplex PCR
* #NUCLACIDNACHW "Nukleinsäurenachweis"
* #NUCLACIDNACHW ^definition = Nukleinsäurenachweis | Nukleinsäure-Nachweis
* #NUCLACIDPCR "Nukleinsäurenachweis (z.B. PCR)"
* #NUCLACIDPCR ^definition = Nukleinsäurenachweis (z.B. PCR)
* #NUCLACIDPLASMA "Nukleinsäure-Nachweis (z.B. PCR) nur in Serum/Plasma oder Stuhl"
* #NUCLACIDPLASMA ^definition = Nukleinsäure-Nachweis (z.B. PCR) nur in Serum/Plasma oder Stuhl
* #NUCLACIDPOLIO "Vakzine-abgeleitetes Poliovirus (VDPV) (bei mind. 85% Ähnlichkeit d.VDPV m.Vakzinevirus i.d.Nukleotidsequenz i. VP1-Abschnitt)"
* #NUCLACIDPOLIO ^definition = Vakzine-abgeleitetes Poliovirus (VDPV) (bei mind. 85% Ähnlichkeit d.VDPV m.Vakzinevirus i.d.Nukleotidsequenz i. VP1-Abschnitt)
* #NUCLACIDPOST "Nukleinsäure-Nachweis (z.B. PCR) nur in Blut oder Liquor, post mortem in Organgewebe"
* #NUCLACIDPOST ^definition = Nukleinsäure-Nachweis (z.B. PCR) nur in Blut oder Liquor, post mortem in Organgewebe
* #NUCLACIDQUAL "PCR qual/quant"
* #NUCLACIDQUAL ^definition = PCR qual/quant
* #NUCLACIDRT "real-time PCR"
* #NUCLACIDRT ^definition = real-time PCR | realtime RT-PCR | RT-PCR
* #NUCLACIDSARS2 "Nukleinsäure-Nachweis SARS-CoV bei d.gl.klin.Probe, 2x od.öfter während KH-Aufenthalt entnommen"
* #NUCLACIDSARS2 ^definition = Nukleinsäure-Nachweis SARS-CoV bei d.gl.klin.Probe, 2x od.öfter während KH-Aufenthalt entnommen
* #NUCLACIDSARSKLIN "Nukleinsäure-Nachweis SARS-CoV bei mind. 2 verschiedenen klinischen Proben"
* #NUCLACIDSARSKLIN ^definition = Nukleinsäure-Nachweis SARS-CoV bei mind. 2 verschiedenen klinischen Proben
* #NUCLACIDSARSRNA "Nukleinsäure-Nachweis SARS-CoV: 2 versch.Tests od.wiederho.RT-PCR u.Verw.e.neuen RNA-Extrakts a.ursp"
* #NUCLACIDSARSRNA ^definition = Nukleinsäure-Nachweis SARS-CoV: 2 versch.Tests od.wiederho.RT-PCR u.Verw.e.neuen RNA-Extrakts a.ursp
* #NUCLACIDSARSZELL "Virusisolierung in Zellkultur aus klin.Probe u.Ident.v.SARS-CoV unter Verw.eines Verfahren wie RT-PC"
* #NUCLACIDSARSZELL ^definition = Virusisolierung in Zellkultur aus klin.Probe u.Ident.v.SARS-CoV unter Verw.eines Verfahren wie RT-PC
* #NUCLACIDSEQU "Nukleinsäure-Nachweis in einer klinischen Probe mit anschließender Sequenzierung"
* #NUCLACIDSEQU ^definition = Nukleinsäure-Nachweis in einer klinischen Probe mit anschließender Sequenzierung
* #NUCLACIDSTERIL "Nukleinsäurenachweis aus einer normalerweise sterilen Probe"
* #NUCLACIDSTERIL ^definition = Nukleinsäurenachweis aus einer normalerweise sterilen Probe | Nukleinsäurenachweis aus normalerweise sterilen Probe | Nukleinsäure-Nachweis in einer normalerweise sterilen Probe | Nukleinsäure-Nachweis in normalerweise sterilen Proben | Nukleisäurenachweis aus einer normalerweise sterilen Probe
* #NUCLACIDSTUHL "Nukleinsäure-Nachweis im Serum oder  im Stuhl"
* #NUCLACIDSTUHL ^definition = Nukleinsäure-Nachweis im Serum oder  im Stuhl
* #NUCLACIDSTX "direkter Nukleinsäure-Nachweis von stx1 oder stx2-Gen(en) (ohne Stammisolierung)"
* #NUCLACIDSTX ^definition = direkter Nukleinsäure-Nachweis von stx1 oder stx2-Gen(en) (ohne Stammisolierung)
* #OTHER "andere"
* #OTHER ^definition = andere
* #PATHOTYPEAE "Isolierung und Charakterisierung nach eae-Genen"
* #PATHOTYPEAE ^definition = Isolierung und Charakterisierung nach eae-Genen
* #PATHOTYPGS "Genotypisierung/Sequenzierung"
* #PATHOTYPGS ^definition = Genotypisierung/Sequenzierung
* #PATHOTYPPH "Isolierung und Charakterisierung nach Phagentyp"
* #PATHOTYPPH ^definition = Isolierung und Charakterisierung nach Phagentyp
* #PATHOTYPS "Isolierung und Charakterisierung nach Serotyp"
* #PATHOTYPS ^definition = Isolierung und Charakterisierung nach Serotyp
* #PATHOTYPSABIN "Sabin-artiges Poliovirus: Typdifferenzierung d.WHO-akkreditiertes Polio-Labor (> als 1% -15% VP1-Sequenzunterschied d.VDPV i. Vergleich z.Vakzinevirus d.gl.Serotyps)"
* #PATHOTYPSABIN ^definition = Sabin-artiges Poliovirus: Typdifferenzierung d.WHO-akkreditiertes Polio-Labor (> als 1% -15% VP1-Sequenzunterschied d.VDPV i. Vergleich z.Vakzinevirus d.gl.Serotyps)
* #PATHOTYPSERO "Serotypisierung"
* #PATHOTYPSERO ^definition = Serotypisierung
* #PATHOTYPSG "Sequenzierung/Genotypisierung"
* #PATHOTYPSG ^definition = Sequenzierung/Genotypisierung
* #PATHOTYPSPEZIES "Speziesdifferenzierung und Subtypisierung"
* #PATHOTYPSPEZIES ^definition = Speziesdifferenzierung und Subtypisierung
* #PATHOTYPSS "Serogruppe/Serotyp"
* #PATHOTYPSS ^definition = Serogruppe/Serotyp
* #PATHOTYPTYP "Typisierung"
* #PATHOTYPTYP ^definition = Typisierung
* #PATHOTYPVIRUSISO "Virusisolierung und Differenzierung mit monoklonalen Antikörpern"
* #PATHOTYPVIRUSISO ^definition = Virusisolierung und Differenzierung mit monoklonalen Antikörpern
* #RESISTENZAMR "Antibiotika Resistenztestung"
* #RESISTENZAMR ^definition = Antibiotika Resistenztestung | Antibiotika-Resistenztestung
* #RESISTENZAMRDIFF "Antibiotika-Resistenztest (Diffusionstest)"
* #RESISTENZAMRDIFF ^definition = Antibiotika-Resistenztest (Diffusionstest)
* #RESISTENZANTI "Antibiotikaresistenzbestimmung"
* #RESISTENZANTI ^definition = Antibiotikaresistenzbestimmung
* #RESISTENZAUTOMAT "Automatisierte MHK Testung (Vitek© etc.)"
* #RESISTENZAUTOMAT ^definition = Automatisierte MHK Testung (Vitek© etc.)
* #RESISTENZBEST "Resistenzbestimmung"
* #RESISTENZBEST ^definition = Resistenzbestimmung
* #RESISTENZDIFF "Agar Diffusionstest"
* #RESISTENZDIFF ^definition = Agar Diffusionstest | Agardiffusionstest
* #RESISTENZGRAD "Antibiotikumgradiententest (E-test©, etc.)"
* #RESISTENZGRAD ^definition = Antibiotikumgradiententest (E-test©, etc.)
* #RESISTENZMHK "Antibiotika Resistenztestung (MHK)"
* #RESISTENZMHK ^definition = Antibiotika Resistenztestung (MHK) | Antibiotika Resistenztestung MHK | Antibiotika-Resistenztest (MHK-Bestimmung)
* #RESISTENZTEST "Resistenztestung"
* #RESISTENZTEST ^definition = Resistenztestung
* #RESISTENZTYP "Typisierung und Antibiotikaresistenzbestimmung"
* #RESISTENZTYP ^definition = Typisierung und Antibiotikaresistenzbestimmung
* #RESISTENZTYPDIFF "Typisierung und Antibiotika-Resistenztestung (Diffusionstest)"
* #RESISTENZTYPDIFF ^definition = Typisierung und Antibiotika-Resistenztestung (Diffusionstest)
* #RESISTENZTYPMHK "Typisierung und Antibiotika-Resistenztestung (MHK-Bestimmung)"
* #RESISTENZTYPMHK ^definition = Typisierung und Antibiotika-Resistenztestung (MHK-Bestimmung)
* #SEROLOGIEDIAG "Serodiagnostik"
* #SEROLOGIEDIAG ^definition = Serodiagnostik
* #SEROLOGIELOGY "Serology"
* #SEROLOGIELOGY ^definition = Serology
* #SEROLOGIENACHW "serologischer Nachweis"
* #SEROLOGIENACHW ^definition = serologischer Nachweis
* #SEROLOGIERACHEN "Rachenabstrich"
* #SEROLOGIERACHEN ^definition = Rachenabstrich
* #SEROLOGIESERO "Serologie"
* #SEROLOGIESERO ^definition = Serologie
* #TOXINBOTU "Nachweis von Botulinum-Toxin in einer klinischen Probe"
* #TOXINBOTU ^definition = Nachweis von Botulinum-Toxin in einer klinischen Probe
* #TOXINCHOL "Nachweis von Cholera-Enterotoxin oder des Cholera-Enterotoxin-Gens im Isolat"
* #TOXINCHOL ^definition = Nachweis von Cholera-Enterotoxin oder des Cholera-Enterotoxin-Gens im Isolat
* #TOXINCLOST "direkter Nachweis von Clostridientoxinen im Stuhl"
* #TOXINCLOST ^definition = direkter Nachweis von Clostridientoxinen im Stuhl
* #TOXINNW "Toxinnachweis"
* #TOXINNW ^definition = Toxinnachweis
* #TOXINSHIGA "direkter Nachweis freier Shigatoxine im Stuhl (ohne Stammisolierung)"
* #TOXINSHIGA ^definition = direkter Nachweis freier Shigatoxine im Stuhl (ohne Stammisolierung)
* #TOXINZYTO "Zytotoxizitätstest mittels Zellkultur"
* #TOXINZYTO ^definition = Zytotoxizitätstest mittels Zellkultur
* #UNBEKANNT "unbekannt"
* #UNBEKANNT ^definition = unbekannt
* #VIRUSISOBSL3 "Viruskultur (erfordert BSL-3-Einrichtungen)"
* #VIRUSISOBSL3 ^definition = Viruskultur (erfordert BSL-3-Einrichtungen)
* #VIRUSISOISO "Virusisolation"
* #VIRUSISOISO ^definition = Virusisolation
* #VIRUSISOKLIN "Virusisolierung aus einer klinischen Probe"
* #VIRUSISOKLIN ^definition = Virusisolierung aus einer klinischen Probe
* #VIRUSISOKLINPROB "Virusisolierung aus klinischer Probe"
* #VIRUSISOKLINPROB ^definition = Virusisolierung aus klinischer Probe
* #VIRUSISOOSI "Virusisolierung"
* #VIRUSISOOSI ^definition = Virusisolierung
* #VIRUSISOWPV "Virusisolierung und Typdifferenzierung - wildes Poliovirus (WPV)"
* #VIRUSISOWPV ^definition = Virusisolierung und Typdifferenzierung - wildes Poliovirus (WPV)
* #WESTERNBLOT "Westernblot"
* #WESTERNBLOT ^definition = Westernblot
