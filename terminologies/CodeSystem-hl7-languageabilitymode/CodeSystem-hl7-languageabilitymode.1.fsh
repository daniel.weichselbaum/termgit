Instance: hl7-languageabilitymode 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-languageabilitymode" 
* name = "hl7-languageabilitymode" 
* title = "HL7 LanguageAbilityMode" 
* status = #active 
* content = #complete 
* description = "**Description:** List of codes representing the method of expression of the language

**Beschreibung:** Codes zur Angabe der Sprachfähigkeit von Patienten (Methode zum Ausdruck von Spache)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.5.60" 
* date = "2016-11-24" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.hl7.org/documentcenter/public_temp_ED0BB625-1C23-BA17-0CE4EA7EB5B49D1F/standards/vocabulary/vocabulary_tables/infrastructure/vocabulary/LanguageAbilityMode.html" 
* count = 6 
* concept[0].code = #ESGN
* concept[0].display = "Expressed signed"
* concept[1].code = #ESP
* concept[1].display = "Expressed spoken"
* concept[2].code = #EWR
* concept[2].display = "Expressed written"
* concept[3].code = #RSGN
* concept[3].display = "Received signed"
* concept[4].code = #RSP
* concept[4].display = "Received spoken"
* concept[5].code = #RWR
* concept[5].display = "Received written"
