Instance: elga-formatcode 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-formatcode" 
* name = "elga-formatcode" 
* title = "ELGA_FormatCode" 
* status = #active 
* content = #complete 
* version = "202205" 
* description = "**Description:** Allowed values for the FormatCode in the XDS-Matadata, within ELGA. In case of an ELGA CDA document with the interoperability level ''Enhanced'' or ''Full Support'' and additionally containing self-defined machine readable elements, this needs to be labeled using ''+'' (Plus-Sign) within the Formatcode. E.g.: ''urn:elga:dissum:2013:EIS_FullSupport+''     

**Beschreibung:** Zur Verwendung in ELGA erlaubte Werte für den FormatCode in den XDS-Metadaten. Liegt ein CDA Dokument in ELGA Interoperabilitätsstufe ''Enhanced'' oder ''Full Support'' vor und enthält das Dokument zusätzliche selbst-definierte maschinenlesbare Elemente, so ist dies durch ein ''+'' (Plus-Zeichen) als Zusatz im Formatcode gekennzeichnet werden. ZB: ''urn:elga:dissum:2013:EIS_FullSupport+''" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.37" 
* date = "2022-05-16" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* count = 97 
* property[0].code = #hints 
* property[0].type = #string 
* #urn:elga:2011:EIS_Basic "ELGA Dokument"
* #urn:elga:2011:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:2011:EIS_Basic ^designation[0].value = "ELGA Dokument" 
* #urn:elga:2013:EIS_Basic "ELGA Dokument"
* #urn:elga:2013:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:2013:EIS_Basic ^designation[0].value = "ELGA Dokument" 
* #urn:elga:2014:EIS_Basic "ELGA Dokument"
* #urn:elga:2014:EIS_Basic ^definition = CDA Dokument entsprechend den Vorgaben des Allgemeinen Implementierungsleitfadens für ELGA Dokumente (EIS Basic)
* #urn:elga:2014:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:2014:EIS_Basic ^designation[0].value = "ELGA Dokument" 
* #urn:elga:2015-v2.06:EIS_Basic "ELGA Dokument v2.06"
* #urn:elga:2015-v2.06:EIS_Basic ^definition = CDA Dokument entsprechend den Vorgaben des Allgemeinen Implementierungsleitfadens für ELGA Dokumente (EIS Basic) v2-06
* #urn:elga:2015-v2.06:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:2015-v2.06:EIS_Basic ^designation[0].value = "ELGA Dokument v2.06" 
* #urn:elga:2015:EIS_Basic "ELGA Dokument"
* #urn:elga:2015:EIS_Basic ^definition = CDA Dokument entsprechend den Vorgaben des Allgemeinen Implementierungsleitfadens für ELGA Dokumente (EIS Basic)
* #urn:elga:2015:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:2015:EIS_Basic ^designation[0].value = "ELGA Dokument" 
* #urn:elga:2016-v2.06.1:EIS_Basic "ELGA Dokument v2.06.1"
* #urn:elga:2016-v2.06.1:EIS_Basic ^definition = CDA Dokument entsprechend den Vorgaben des Allgemeinen Implementierungsleitfadens für ELGA Dokumente (EIS Basic) v2-06.1
* #urn:elga:2016-v2.06.1:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:2016-v2.06.1:EIS_Basic ^designation[0].value = "ELGA Dokument v2.06" 
* #urn:elga:2017-v2.06.2:EIS_Basic "ELGA Dokument v2.06.2"
* #urn:elga:2017-v2.06.2:EIS_Basic ^definition = CDA Dokument entsprechend den Vorgaben des Allgemeinen Implementierungsleitfadens für ELGA Dokumente (EIS Basic) v2-06.2
* #urn:elga:2017-v2.06.2:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:2017-v2.06.2:EIS_Basic ^designation[0].value = "ELGA Dokument v2.06" 
* #urn:elga:dissum-n:2014:EIS_Basic "ELGA Entlassungsbrief Pflege, EIS Basic"
* #urn:elga:dissum-n:2014:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:dissum-n:2014:EIS_Basic ^designation[0].value = "ELGA Entlassungsbrief Pflege, EIS Basic" 
* #urn:elga:dissum-n:2014:EIS_Basic ^property[0].code = #hints 
* #urn:elga:dissum-n:2014:EIS_Basic ^property[0].valueString = "Gilt für EIS Basic und Structured" 
* #urn:elga:dissum-n:2014:EIS_Enhanced "ELGA Entlassungsbrief Pflege, EIS Enhanced"
* #urn:elga:dissum-n:2014:EIS_Enhanced ^designation[0].language = #de-AT 
* #urn:elga:dissum-n:2014:EIS_Enhanced ^designation[0].value = "ELGA Entlassungsbrief Pflege, EIS Enhanced" 
* #urn:elga:dissum-n:2014:EIS_FullSupport "ELGA Entlassungsbrief Pflege, EIS Full Support"
* #urn:elga:dissum-n:2014:EIS_FullSupport ^designation[0].language = #de-AT 
* #urn:elga:dissum-n:2014:EIS_FullSupport ^designation[0].value = "ELGA Entlassungsbrief Pflege, EIS Full Support" 
* #urn:elga:dissum-n:2015-v2.06:EIS_Basic "ELGA Entlassungsbrief Pflege, EIS Basic v2.06"
* #urn:elga:dissum-n:2015-v2.06:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:dissum-n:2015-v2.06:EIS_Basic ^designation[0].value = "ELGA Entlassungsbrief Pflege, EIS Basic v2.06" 
* #urn:elga:dissum-n:2015-v2.06:EIS_Basic ^property[0].code = #hints 
* #urn:elga:dissum-n:2015-v2.06:EIS_Basic ^property[0].valueString = "Gilt für EIS Basic und Structured" 
* #urn:elga:dissum-n:2015-v2.06:EIS_Enhanced "ELGA Entlassungsbrief Pflege, EIS Enhanced v2.06"
* #urn:elga:dissum-n:2015-v2.06:EIS_Enhanced ^designation[0].language = #de-AT 
* #urn:elga:dissum-n:2015-v2.06:EIS_Enhanced ^designation[0].value = "ELGA Entlassungsbrief Pflege, EIS Enhanced v2.06" 
* #urn:elga:dissum-n:2015-v2.06:EIS_FullSupport "ELGA Entlassungsbrief Pflege, EIS Full Support v2.06"
* #urn:elga:dissum-n:2015-v2.06:EIS_FullSupport ^designation[0].language = #de-AT 
* #urn:elga:dissum-n:2015-v2.06:EIS_FullSupport ^designation[0].value = "ELGA Entlassungsbrief Pflege, EIS Full Support v2.06" 
* #urn:elga:dissum-n:2015:EIS_Basic "ELGA Entlassungsbrief Pflege, EIS Basic"
* #urn:elga:dissum-n:2015:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:dissum-n:2015:EIS_Basic ^designation[0].value = "ELGA Entlassungsbrief Pflege, EIS Basic" 
* #urn:elga:dissum-n:2015:EIS_Basic ^property[0].code = #hints 
* #urn:elga:dissum-n:2015:EIS_Basic ^property[0].valueString = "Gilt für EIS Basic und Structured" 
* #urn:elga:dissum-n:2015:EIS_Enhanced "ELGA Entlassungsbrief Pflege, EIS Enhanced"
* #urn:elga:dissum-n:2015:EIS_Enhanced ^designation[0].language = #de-AT 
* #urn:elga:dissum-n:2015:EIS_Enhanced ^designation[0].value = "ELGA Entlassungsbrief Pflege, EIS Enhanced" 
* #urn:elga:dissum-n:2015:EIS_FullSupport "ELGA Entlassungsbrief Pflege, EIS Full Support"
* #urn:elga:dissum-n:2015:EIS_FullSupport ^designation[0].language = #de-AT 
* #urn:elga:dissum-n:2015:EIS_FullSupport ^designation[0].value = "ELGA Entlassungsbrief Pflege, EIS Full Support" 
* #urn:elga:dissum-n:2017-v2.06.2:EIS_Basic "ELGA Entlassungsbrief Pflege, EIS Basic v2.06.2"
* #urn:elga:dissum-n:2017-v2.06.2:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:dissum-n:2017-v2.06.2:EIS_Basic ^designation[0].value = "ELGA Entlassungsbrief Pflege, EIS Basic v2.06.2" 
* #urn:elga:dissum-n:2017-v2.06.2:EIS_Basic ^property[0].code = #hints 
* #urn:elga:dissum-n:2017-v2.06.2:EIS_Basic ^property[0].valueString = "Gilt für EIS Basic und Structured" 
* #urn:elga:dissum-n:2017-v2.06.2:EIS_Enhanced "ELGA Entlassungsbrief Pflege, EIS Enhanced v2.06.2"
* #urn:elga:dissum-n:2017-v2.06.2:EIS_Enhanced ^designation[0].language = #de-AT 
* #urn:elga:dissum-n:2017-v2.06.2:EIS_Enhanced ^designation[0].value = "ELGA Entlassungsbrief Pflege, EIS Enhanced v2.06.2" 
* #urn:elga:dissum-n:2017-v2.06.2:EIS_FullSupport "ELGA Entlassungsbrief Pflege, EIS Full Support v2.06.2"
* #urn:elga:dissum-n:2017-v2.06.2:EIS_FullSupport ^designation[0].language = #de-AT 
* #urn:elga:dissum-n:2017-v2.06.2:EIS_FullSupport ^designation[0].value = "ELGA Entlassungsbrief Pflege, EIS Full Support v2.06.2" 
* #urn:elga:dissum:2011:EIS_Basic "ELGA Entlassungsbrief"
* #urn:elga:dissum:2011:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:dissum:2011:EIS_Basic ^designation[0].value = "ELGA Entlassungsbrief" 
* #urn:elga:dissum:2011:EIS_Basic ^property[0].code = #hints 
* #urn:elga:dissum:2011:EIS_Basic ^property[0].valueString = "Gilt für EIS Basic und Structured" 
* #urn:elga:dissum:2011:EIS_Enhanced "ELGA Entlassungsbrief"
* #urn:elga:dissum:2011:EIS_Enhanced ^designation[0].language = #de-AT 
* #urn:elga:dissum:2011:EIS_Enhanced ^designation[0].value = "ELGA Entlassungsbrief" 
* #urn:elga:dissum:2011:EIS_FullSupport "ELGA Entlassungsbrief"
* #urn:elga:dissum:2011:EIS_FullSupport ^designation[0].language = #de-AT 
* #urn:elga:dissum:2011:EIS_FullSupport ^designation[0].value = "ELGA Entlassungsbrief" 
* #urn:elga:dissum:2013:EIS_Basic "ELGA Entlassungsbrief"
* #urn:elga:dissum:2013:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:dissum:2013:EIS_Basic ^designation[0].value = "ELGA Entlassungsbrief" 
* #urn:elga:dissum:2013:EIS_Basic ^property[0].code = #hints 
* #urn:elga:dissum:2013:EIS_Basic ^property[0].valueString = "Gilt für EIS Basic und Structured" 
* #urn:elga:dissum:2013:EIS_Enhanced "ELGA Entlassungsbrief"
* #urn:elga:dissum:2013:EIS_Enhanced ^designation[0].language = #de-AT 
* #urn:elga:dissum:2013:EIS_Enhanced ^designation[0].value = "ELGA Entlassungsbrief" 
* #urn:elga:dissum:2013:EIS_FullSupport "ELGA Entlassungsbrief"
* #urn:elga:dissum:2013:EIS_FullSupport ^designation[0].language = #de-AT 
* #urn:elga:dissum:2013:EIS_FullSupport ^designation[0].value = "ELGA Entlassungsbrief" 
* #urn:elga:dissum:2014:EIS_Basic "ELGA Entlassungsbrief Ärztlich, EIS Basic"
* #urn:elga:dissum:2014:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:dissum:2014:EIS_Basic ^designation[0].value = "ELGA Entlassungsbrief Ärztlich, EIS Basic" 
* #urn:elga:dissum:2014:EIS_Basic ^property[0].code = #hints 
* #urn:elga:dissum:2014:EIS_Basic ^property[0].valueString = "Gilt für EIS Basic und Structured" 
* #urn:elga:dissum:2014:EIS_Enhanced "ELGA Entlassungsbrief Ärztlich, EIS Enhanced"
* #urn:elga:dissum:2014:EIS_Enhanced ^designation[0].language = #de-AT 
* #urn:elga:dissum:2014:EIS_Enhanced ^designation[0].value = "ELGA Entlassungsbrief Ärztlich, EIS Enhanced" 
* #urn:elga:dissum:2014:EIS_FullSupport "ELGA Entlassungsbrief Ärztlich, EIS Full Support"
* #urn:elga:dissum:2014:EIS_FullSupport ^designation[0].language = #de-AT 
* #urn:elga:dissum:2014:EIS_FullSupport ^designation[0].value = "ELGA Entlassungsbrief Ärztlich, EIS Full Support" 
* #urn:elga:dissum:2015-v2.06:EIS_Basic "ELGA Entlassungsbrief Ärztlich, EIS Basic v2.06"
* #urn:elga:dissum:2015-v2.06:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:dissum:2015-v2.06:EIS_Basic ^designation[0].value = "ELGA Entlassungsbrief Ärztlich, EIS Basic v2.06" 
* #urn:elga:dissum:2015-v2.06:EIS_Basic ^property[0].code = #hints 
* #urn:elga:dissum:2015-v2.06:EIS_Basic ^property[0].valueString = "Gilt für EIS Basic und Structured" 
* #urn:elga:dissum:2015-v2.06:EIS_Enhanced "ELGA Entlassungsbrief Ärztlich, EIS Enhanced v2.06"
* #urn:elga:dissum:2015-v2.06:EIS_Enhanced ^designation[0].language = #de-AT 
* #urn:elga:dissum:2015-v2.06:EIS_Enhanced ^designation[0].value = "ELGA Entlassungsbrief Ärztlich, EIS Enhanced v2.06" 
* #urn:elga:dissum:2015-v2.06:EIS_FullSupport "ELGA Entlassungsbrief Ärztlich, EIS Full Support v2.06"
* #urn:elga:dissum:2015-v2.06:EIS_FullSupport ^designation[0].language = #de-AT 
* #urn:elga:dissum:2015-v2.06:EIS_FullSupport ^designation[0].value = "ELGA Entlassungsbrief Ärztlich, EIS Full Support v2.06" 
* #urn:elga:dissum:2015:EIS_Basic "ELGA Entlassungsbrief Ärztlich, EIS Basic"
* #urn:elga:dissum:2015:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:dissum:2015:EIS_Basic ^designation[0].value = "ELGA Entlassungsbrief Ärztlich, EIS Basic" 
* #urn:elga:dissum:2015:EIS_Basic ^property[0].code = #hints 
* #urn:elga:dissum:2015:EIS_Basic ^property[0].valueString = "Gilt für EIS Basic und Structured" 
* #urn:elga:dissum:2015:EIS_Enhanced "ELGA Entlassungsbrief Ärztlich, EIS Enhanced"
* #urn:elga:dissum:2015:EIS_Enhanced ^designation[0].language = #de-AT 
* #urn:elga:dissum:2015:EIS_Enhanced ^designation[0].value = "ELGA Entlassungsbrief Ärztlich, EIS Enhanced" 
* #urn:elga:dissum:2015:EIS_FullSupport "ELGA Entlassungsbrief Ärztlich, EIS Full Support"
* #urn:elga:dissum:2015:EIS_FullSupport ^designation[0].language = #de-AT 
* #urn:elga:dissum:2015:EIS_FullSupport ^designation[0].value = "ELGA Entlassungsbrief Ärztlich, EIS Full Support" 
* #urn:elga:dissum:2017-v2.06.2:EIS_Basic "ELGA Entlassungsbrief Ärztlich, EIS Basic v2.06.2"
* #urn:elga:dissum:2017-v2.06.2:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:dissum:2017-v2.06.2:EIS_Basic ^designation[0].value = "ELGA Entlassungsbrief Ärztlich, EIS Basic v2.06.2" 
* #urn:elga:dissum:2017-v2.06.2:EIS_Basic ^property[0].code = #hints 
* #urn:elga:dissum:2017-v2.06.2:EIS_Basic ^property[0].valueString = "Gilt für EIS Basic und Structured" 
* #urn:elga:dissum:2017-v2.06.2:EIS_Enhanced "ELGA Entlassungsbrief Ärztlich, EIS Enhanced v2.06.2"
* #urn:elga:dissum:2017-v2.06.2:EIS_Enhanced ^designation[0].language = #de-AT 
* #urn:elga:dissum:2017-v2.06.2:EIS_Enhanced ^designation[0].value = "ELGA Entlassungsbrief Ärztlich, EIS Enhanced v2.06.2" 
* #urn:elga:dissum:2017-v2.06.2:EIS_FullSupport "ELGA Entlassungsbrief Ärztlich, EIS Full Support v2.06.2"
* #urn:elga:dissum:2017-v2.06.2:EIS_FullSupport ^designation[0].language = #de-AT 
* #urn:elga:dissum:2017-v2.06.2:EIS_FullSupport ^designation[0].value = "ELGA Entlassungsbrief Ärztlich, EIS Full Support v2.06.2" 
* #urn:elga:emedat:2014:EIS_FullSupport "ELGA e-Medikation, EIS Full Support"
* #urn:elga:emedat:2014:EIS_FullSupport ^designation[0].language = #de-AT 
* #urn:elga:emedat:2014:EIS_FullSupport ^designation[0].value = "ELGA e-Medikation, EIS Full Support" 
* #urn:elga:emedat:2015 "ELGA e-Medikation"
* #urn:elga:emedat:2015 ^designation[0].language = #de-AT 
* #urn:elga:emedat:2015 ^designation[0].value = "ELGA e-Medikation" 
* #urn:elga:emedat:2015 ^property[0].code = #hints 
* #urn:elga:emedat:2015 ^property[0].valueString = "CDA für eMedikation sind nur in EIS Full Support zulässig" 
* #urn:elga:emedat:2015-v2.06 "ELGA e-Medikation v2.06"
* #urn:elga:emedat:2015-v2.06 ^designation[0].language = #de-AT 
* #urn:elga:emedat:2015-v2.06 ^designation[0].value = "ELGA e-Medikation v2.06" 
* #urn:elga:lab:2011:EIS_Basic "ELGA Befundbericht Labor"
* #urn:elga:lab:2011:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:lab:2011:EIS_Basic ^designation[0].value = "ELGA Befundbericht Labor" 
* #urn:elga:lab:2011:EIS_Basic ^property[0].code = #hints 
* #urn:elga:lab:2011:EIS_Basic ^property[0].valueString = "Gilt für EIS Basic und Structured" 
* #urn:elga:lab:2011:EIS_Enhanced "ELGA Befundbericht Labor"
* #urn:elga:lab:2011:EIS_Enhanced ^designation[0].language = #de-AT 
* #urn:elga:lab:2011:EIS_Enhanced ^designation[0].value = "ELGA Befundbericht Labor" 
* #urn:elga:lab:2011:EIS_FullSupport "ELGA Befundbericht Labor"
* #urn:elga:lab:2011:EIS_FullSupport ^designation[0].language = #de-AT 
* #urn:elga:lab:2011:EIS_FullSupport ^designation[0].value = "ELGA Befundbericht Labor" 
* #urn:elga:lab:2013:EIS_Basic "ELGA Befundbericht Labor"
* #urn:elga:lab:2013:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:lab:2013:EIS_Basic ^designation[0].value = "ELGA Befundbericht Labor" 
* #urn:elga:lab:2013:EIS_Basic ^property[0].code = #hints 
* #urn:elga:lab:2013:EIS_Basic ^property[0].valueString = "Gilt für EIS Basic und Structured" 
* #urn:elga:lab:2013:EIS_Enhanced "ELGA Befundbericht Labor"
* #urn:elga:lab:2013:EIS_Enhanced ^designation[0].language = #de-AT 
* #urn:elga:lab:2013:EIS_Enhanced ^designation[0].value = "ELGA Befundbericht Labor" 
* #urn:elga:lab:2013:EIS_FullSupport "ELGA Befundbericht Labor"
* #urn:elga:lab:2013:EIS_FullSupport ^designation[0].language = #de-AT 
* #urn:elga:lab:2013:EIS_FullSupport ^designation[0].value = "ELGA Befundbericht Labor" 
* #urn:elga:lab:2014:EIS_Basic "ELGA Laborbefund, EIS Basic"
* #urn:elga:lab:2014:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:lab:2014:EIS_Basic ^designation[0].value = "ELGA Laborbefund, EIS Basic" 
* #urn:elga:lab:2014:EIS_Basic ^property[0].code = #hints 
* #urn:elga:lab:2014:EIS_Basic ^property[0].valueString = "Gilt für EIS Basic und Structured" 
* #urn:elga:lab:2014:EIS_Enhanced "ELGA Laborbefund, EIS Enhanced"
* #urn:elga:lab:2014:EIS_Enhanced ^designation[0].language = #de-AT 
* #urn:elga:lab:2014:EIS_Enhanced ^designation[0].value = "ELGA Laborbefund, EIS Enhanced" 
* #urn:elga:lab:2014:EIS_FullSupport "ELGA Laborbefund, EIS Full Support"
* #urn:elga:lab:2014:EIS_FullSupport ^designation[0].language = #de-AT 
* #urn:elga:lab:2014:EIS_FullSupport ^designation[0].value = "ELGA Laborbefund, EIS Full Support" 
* #urn:elga:lab:2015-v2.06:EIS_Basic "ELGA Laborbefund, EIS Basic v2.06"
* #urn:elga:lab:2015-v2.06:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:lab:2015-v2.06:EIS_Basic ^designation[0].value = "ELGA Laborbefund, EIS Basic v2.06" 
* #urn:elga:lab:2015-v2.06:EIS_Basic ^property[0].code = #hints 
* #urn:elga:lab:2015-v2.06:EIS_Basic ^property[0].valueString = "Gilt für EIS Basic und Structured" 
* #urn:elga:lab:2015-v2.06:EIS_Enhanced "ELGA Laborbefund, EIS Enhanced v2.06"
* #urn:elga:lab:2015-v2.06:EIS_Enhanced ^designation[0].language = #de-AT 
* #urn:elga:lab:2015-v2.06:EIS_Enhanced ^designation[0].value = "ELGA Laborbefund, EIS Enhanced v2.06" 
* #urn:elga:lab:2015-v2.06:EIS_FullSupport "ELGA Laborbefund, EIS Full Support v2.06"
* #urn:elga:lab:2015-v2.06:EIS_FullSupport ^designation[0].language = #de-AT 
* #urn:elga:lab:2015-v2.06:EIS_FullSupport ^designation[0].value = "ELGA Laborbefund, EIS Full Support v2.06" 
* #urn:elga:lab:2015:EIS_Basic "ELGA Laborbefund, EIS Basic"
* #urn:elga:lab:2015:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:lab:2015:EIS_Basic ^designation[0].value = "ELGA Laborbefund, EIS Basic" 
* #urn:elga:lab:2015:EIS_Basic ^property[0].code = #hints 
* #urn:elga:lab:2015:EIS_Basic ^property[0].valueString = "Gilt für EIS Basic und Structured" 
* #urn:elga:lab:2015:EIS_Enhanced "ELGA Laborbefund, EIS Enhanced"
* #urn:elga:lab:2015:EIS_Enhanced ^designation[0].language = #de-AT 
* #urn:elga:lab:2015:EIS_Enhanced ^designation[0].value = "ELGA Laborbefund, EIS Enhanced" 
* #urn:elga:lab:2015:EIS_FullSupport "ELGA Laborbefund, EIS Full Support"
* #urn:elga:lab:2015:EIS_FullSupport ^designation[0].language = #de-AT 
* #urn:elga:lab:2015:EIS_FullSupport ^designation[0].value = "ELGA Laborbefund, EIS Full Support" 
* #urn:elga:lab:2017-v2.06.2:EIS_Basic "ELGA Laborbefund, EIS Basic v2.06.2"
* #urn:elga:lab:2017-v2.06.2:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:lab:2017-v2.06.2:EIS_Basic ^designation[0].value = "ELGA Laborbefund, EIS Basic v2.06.2" 
* #urn:elga:lab:2017-v2.06.2:EIS_Basic ^property[0].code = #hints 
* #urn:elga:lab:2017-v2.06.2:EIS_Basic ^property[0].valueString = "Gilt für EIS Basic und Structured" 
* #urn:elga:lab:2017-v2.06.2:EIS_Enhanced "ELGA Laborbefund, EIS Enhanced v2.06.2"
* #urn:elga:lab:2017-v2.06.2:EIS_Enhanced ^designation[0].language = #de-AT 
* #urn:elga:lab:2017-v2.06.2:EIS_Enhanced ^designation[0].value = "ELGA Laborbefund, EIS Enhanced v2.06.2" 
* #urn:elga:lab:2017-v2.06.2:EIS_FullSupport "ELGA Laborbefund, EIS Full Support v2.06.2"
* #urn:elga:lab:2017-v2.06.2:EIS_FullSupport ^designation[0].language = #de-AT 
* #urn:elga:lab:2017-v2.06.2:EIS_FullSupport ^designation[0].value = "ELGA Laborbefund, EIS Full Support v2.06.2" 
* #urn:elga:nurse-tf:2015-v2.06:EIS_Basic "ELGA Pflegesituationsbericht, EIS Basic v2.06"
* #urn:elga:nurse-tf:2015-v2.06:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:nurse-tf:2015-v2.06:EIS_Basic ^designation[0].value = "ELGA Pflegesituationsbericht, EIS Basic v2.06" 
* #urn:elga:nurse-tf:2015-v2.06:EIS_Enhanced "ELGA Pflegesituationsbericht, EIS Enhanced v2.06"
* #urn:elga:nurse-tf:2015-v2.06:EIS_Enhanced ^designation[0].language = #de-AT 
* #urn:elga:nurse-tf:2015-v2.06:EIS_Enhanced ^designation[0].value = "ELGA Pflegesituationsbericht, EIS Enhanced v2.06" 
* #urn:elga:nurse-tf:2015-v2.06:EIS_FullSupport "ELGA Pflegesituationsbericht, EIS Full Support v2.06"
* #urn:elga:nurse-tf:2015-v2.06:EIS_FullSupport ^designation[0].language = #de-AT 
* #urn:elga:nurse-tf:2015-v2.06:EIS_FullSupport ^designation[0].value = "ELGA Pflegesituationsbericht, EIS Full Support v2.06" 
* #urn:elga:nurse-tf:2017-v2.06.2:EIS_Basic "ELGA Pflegesituationsbericht, EIS Basic v2.06.2"
* #urn:elga:nurse-tf:2017-v2.06.2:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:nurse-tf:2017-v2.06.2:EIS_Basic ^designation[0].value = "ELGA Pflegesituationsbericht, EIS Basic v2.06.2" 
* #urn:elga:nurse-tf:2017-v2.06.2:EIS_Enhanced "ELGA Pflegesituationsbericht, EIS Enhanced v2.06.2"
* #urn:elga:nurse-tf:2017-v2.06.2:EIS_Enhanced ^designation[0].language = #de-AT 
* #urn:elga:nurse-tf:2017-v2.06.2:EIS_Enhanced ^designation[0].value = "ELGA Pflegesituationsbericht, EIS Enhanced v2.06.2" 
* #urn:elga:nurse-tf:2017-v2.06.2:EIS_FullSupport "ELGA Pflegesituationsbericht, EIS Full Support v2.06.2"
* #urn:elga:nurse-tf:2017-v2.06.2:EIS_FullSupport ^designation[0].language = #de-AT 
* #urn:elga:nurse-tf:2017-v2.06.2:EIS_FullSupport ^designation[0].value = "ELGA Pflegesituationsbericht, EIS Full Support v2.06.2" 
* #urn:elga:radio:2011:EIS_Basic "ELGA Befundbericht Radiologie"
* #urn:elga:radio:2011:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:radio:2011:EIS_Basic ^designation[0].value = "ELGA Befundbericht Radiologie" 
* #urn:elga:radio:2011:EIS_Basic ^property[0].code = #hints 
* #urn:elga:radio:2011:EIS_Basic ^property[0].valueString = "Gilt für EIS Basic und Structured" 
* #urn:elga:radio:2011:EIS_Enhanced "ELGA Befundbericht Radiologie"
* #urn:elga:radio:2011:EIS_Enhanced ^designation[0].language = #de-AT 
* #urn:elga:radio:2011:EIS_Enhanced ^designation[0].value = "ELGA Befundbericht Radiologie" 
* #urn:elga:radio:2011:EIS_FullSupport "ELGA Befundbericht Radiologie"
* #urn:elga:radio:2011:EIS_FullSupport ^designation[0].language = #de-AT 
* #urn:elga:radio:2011:EIS_FullSupport ^designation[0].value = "ELGA Befundbericht Radiologie" 
* #urn:elga:radio:2013:EIS_Basic "ELGA Befundbericht Radiologie"
* #urn:elga:radio:2013:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:radio:2013:EIS_Basic ^designation[0].value = "ELGA Befundbericht Radiologie" 
* #urn:elga:radio:2013:EIS_Basic ^property[0].code = #hints 
* #urn:elga:radio:2013:EIS_Basic ^property[0].valueString = "Gilt für EIS Basic und Structured" 
* #urn:elga:radio:2013:EIS_Enhanced "ELGA Befundbericht Radiologie"
* #urn:elga:radio:2013:EIS_Enhanced ^designation[0].language = #de-AT 
* #urn:elga:radio:2013:EIS_Enhanced ^designation[0].value = "ELGA Befundbericht Radiologie" 
* #urn:elga:radio:2013:EIS_FullSupport "ELGA Befundbericht Radiologie"
* #urn:elga:radio:2013:EIS_FullSupport ^designation[0].language = #de-AT 
* #urn:elga:radio:2013:EIS_FullSupport ^designation[0].value = "ELGA Befundbericht Radiologie" 
* #urn:elga:radio:2014:EIS_Basic "ELGA Befund bildgebende Diagnostik, EIS Basic"
* #urn:elga:radio:2014:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:radio:2014:EIS_Basic ^designation[0].value = "ELGA Befund bildgebende Diagnostik, EIS Basic" 
* #urn:elga:radio:2014:EIS_Basic ^property[0].code = #hints 
* #urn:elga:radio:2014:EIS_Basic ^property[0].valueString = "Gilt für EIS Basic und Structured" 
* #urn:elga:radio:2014:EIS_Enhanced "ELGA Befund bildgebende Diagnostik, EIS Enhanced"
* #urn:elga:radio:2014:EIS_Enhanced ^designation[0].language = #de-AT 
* #urn:elga:radio:2014:EIS_Enhanced ^designation[0].value = "ELGA Befund bildgebende Diagnostik, EIS Enhanced" 
* #urn:elga:radio:2014:EIS_FullSupport "ELGA Befund bildgebende Diagnostik,EIS Full Support"
* #urn:elga:radio:2014:EIS_FullSupport ^designation[0].language = #de-AT 
* #urn:elga:radio:2014:EIS_FullSupport ^designation[0].value = "ELGA Befund bildgebende Diagnostik,EIS Full Support" 
* #urn:elga:radio:2015-v2.06:EIS_Basic "ELGA Befund bildgebende Diagnostik, EIS Basic v2.06"
* #urn:elga:radio:2015-v2.06:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:radio:2015-v2.06:EIS_Basic ^designation[0].value = "ELGA Befund bildgebende Diagnostik, EIS Basic v2.06" 
* #urn:elga:radio:2015-v2.06:EIS_Basic ^property[0].code = #hints 
* #urn:elga:radio:2015-v2.06:EIS_Basic ^property[0].valueString = "Gilt für EIS Basic und Structured" 
* #urn:elga:radio:2015-v2.06:EIS_Enhanced "ELGA Befund bildgebende Diagnostik, EIS Enhanced v2.06"
* #urn:elga:radio:2015-v2.06:EIS_Enhanced ^designation[0].language = #de-AT 
* #urn:elga:radio:2015-v2.06:EIS_Enhanced ^designation[0].value = "ELGA Befund bildgebende Diagnostik, EIS Enhanced v2.06" 
* #urn:elga:radio:2015-v2.06:EIS_FullSupport "ELGA Befund bildgebende Diagnostik, EIS Full Support v2.06"
* #urn:elga:radio:2015-v2.06:EIS_FullSupport ^designation[0].language = #de-AT 
* #urn:elga:radio:2015-v2.06:EIS_FullSupport ^designation[0].value = "ELGA Befund bildgebende Diagnostik, EIS Full Support v2.06" 
* #urn:elga:radio:2015:EIS_Basic "ELGA Befund bildgebende Diagnostik, EIS Basic"
* #urn:elga:radio:2015:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:radio:2015:EIS_Basic ^designation[0].value = "ELGA Befund bildgebende Diagnostik, EIS Basic" 
* #urn:elga:radio:2015:EIS_Basic ^property[0].code = #hints 
* #urn:elga:radio:2015:EIS_Basic ^property[0].valueString = "Gilt für EIS Basic und Structured" 
* #urn:elga:radio:2015:EIS_Enhanced "ELGA Befund bildgebende Diagnostik, EIS Enhanced"
* #urn:elga:radio:2015:EIS_Enhanced ^designation[0].language = #de-AT 
* #urn:elga:radio:2015:EIS_Enhanced ^designation[0].value = "ELGA Befund bildgebende Diagnostik, EIS Enhanced" 
* #urn:elga:radio:2015:EIS_FullSupport "ELGA Befund bildgebende Diagnostik,EIS Full Support"
* #urn:elga:radio:2015:EIS_FullSupport ^designation[0].language = #de-AT 
* #urn:elga:radio:2015:EIS_FullSupport ^designation[0].value = "ELGA Befund bildgebende Diagnostik,EIS Full Support" 
* #urn:elga:radio:2017-v2.06.2:EIS_Basic "ELGA Befund bildgebende Diagnostik, EIS Basic v2.06.2"
* #urn:elga:radio:2017-v2.06.2:EIS_Basic ^designation[0].language = #de-AT 
* #urn:elga:radio:2017-v2.06.2:EIS_Basic ^designation[0].value = "ELGA Befund bildgebende Diagnostik, EIS Basic v2.06.2" 
* #urn:elga:radio:2017-v2.06.2:EIS_Basic ^property[0].code = #hints 
* #urn:elga:radio:2017-v2.06.2:EIS_Basic ^property[0].valueString = "Gilt für EIS Basic und Structured" 
* #urn:elga:radio:2017-v2.06.2:EIS_Enhanced "ELGA Befund bildgebende Diagnostik, EIS Enhanced v2.06.2"
* #urn:elga:radio:2017-v2.06.2:EIS_Enhanced ^designation[0].language = #de-AT 
* #urn:elga:radio:2017-v2.06.2:EIS_Enhanced ^designation[0].value = "ELGA Befund bildgebende Diagnostik, EIS Enhanced v2.06.2" 
* #urn:elga:radio:2017-v2.06.2:EIS_FullSupport "ELGA Befund bildgebende Diagnostik, EIS Full Support v2.06.2"
* #urn:elga:radio:2017-v2.06.2:EIS_FullSupport ^designation[0].language = #de-AT 
* #urn:elga:radio:2017-v2.06.2:EIS_FullSupport ^designation[0].value = "ELGA Befund bildgebende Diagnostik, EIS Full Support v2.06.2" 
* #urn:hl7-at:arztb:1.2.0+20210304:EIS_Enhanced "HL7 Austria Arztbrief 1.2.0+20210304, EIS Enhanced"
* #urn:hl7-at:arztb:1.2.0+20210304:EIS_Enhanced ^designation[0].language = #de-AT 
* #urn:hl7-at:arztb:1.2.0+20210304:EIS_Enhanced ^designation[0].value = "HL7 Austria Arztbrief 1.2.0+20210304, EIS Enhanced" 
* #urn:hl7-at:arztb:1.2.0+20210304:EIS_Enhanced+ "HL7 Austria Arztbrief 1.2.0+20210304, EIS Enhanced+"
* #urn:hl7-at:arztb:1.2.0+20210304:EIS_Enhanced+ ^designation[0].language = #de-AT 
* #urn:hl7-at:arztb:1.2.0+20210304:EIS_Enhanced+ ^designation[0].value = "HL7 Austria Arztbrief 1.2.0+20210304, EIS Enhanced+" 
* #urn:hl7-at:arztb:1.2.0+20210304:EIS_FullSupport "HL7 Austria Arztbrief 1.2.0+20210304, EIS FullSupport"
* #urn:hl7-at:arztb:1.2.0+20210304:EIS_FullSupport ^designation[0].language = #de-AT 
* #urn:hl7-at:arztb:1.2.0+20210304:EIS_FullSupport ^designation[0].value = "HL7 Austria Arztbrief 1.2.0+20210304, EIS FullSupport" 
* #urn:hl7-at:arztb:1.2.0+20210304:EIS_FullSupport+ "HL7 Austria Arztbrief 1.2.0+20210304, EIS FullSupport+"
* #urn:hl7-at:arztb:1.2.0+20210304:EIS_FullSupport+ ^designation[0].language = #de-AT 
* #urn:hl7-at:arztb:1.2.0+20210304:EIS_FullSupport+ ^designation[0].value = "HL7 Austria Arztbrief 1.2.0+20210304, EIS FullSupport+" 
* #urn:hl7-at:arztb:1.3.0+20220209:EIS_Enhanced "HL7 Austria Arztbrief 1.3.0+20220209, EIS Enhanced"
* #urn:hl7-at:arztb:1.3.0+20220209:EIS_Enhanced ^designation[0].language = #de-AT 
* #urn:hl7-at:arztb:1.3.0+20220209:EIS_Enhanced ^designation[0].value = "HL7 Austria Arztbrief 1.3.0+20220209, EIS Enhanced" 
* #urn:hl7-at:arztb:1.3.0+20220209:EIS_Enhanced+ "HL7 Austria Arztbrief 1.3.0+20220209, EIS Enhanced+"
* #urn:hl7-at:arztb:1.3.0+20220209:EIS_Enhanced+ ^designation[0].language = #de-AT 
* #urn:hl7-at:arztb:1.3.0+20220209:EIS_Enhanced+ ^designation[0].value = "HL7 Austria Arztbrief 1.3.0+20220209, EIS Enhanced+" 
* #urn:hl7-at:arztb:1.3.0+20220209:EIS_FullSupport "HL7 Austria Arztbrief 1.3.0+20220209, EIS FullSupport"
* #urn:hl7-at:arztb:1.3.0+20220209:EIS_FullSupport ^designation[0].language = #de-AT 
* #urn:hl7-at:arztb:1.3.0+20220209:EIS_FullSupport ^designation[0].value = "HL7 Austria Arztbrief 1.3.0+20220209, EIS FullSupport" 
* #urn:hl7-at:arztb:1.3.0+20220209:EIS_FullSupport+ "HL7 Austria Arztbrief 1.3.0+20220209, EIS FullSupport+"
* #urn:hl7-at:arztb:1.3.0+20220209:EIS_FullSupport+ ^designation[0].language = #de-AT 
* #urn:hl7-at:arztb:1.3.0+20220209:EIS_FullSupport+ ^designation[0].value = "HL7 Austria Arztbrief 1.3.0+20220209, EIS FullSupport+" 
* #urn:hl7-at:eImpf:1.1.0+20210512 "HL7 Austria e-Impfpass 1.1.0+20210512"
* #urn:hl7-at:eImpf:1.1.0+20210512 ^designation[0].language = #de-AT 
* #urn:hl7-at:eImpf:1.1.0+20210512 ^designation[0].value = "HL7 Austria e-Impfpass 1.1.0+20210512" 
* #urn:hl7-at:eImpf:1.1.1+20210601 "HL7 Austria e-Impfpass 1.1.1+20210601"
* #urn:hl7-at:eImpf:1.1.1+20210601 ^designation[0].language = #de-AT 
* #urn:hl7-at:eImpf:1.1.1+20210601 ^designation[0].value = "HL7 Austria e-Impfpass 1.1.1+20210601" 
* #urn:hl7-at:eImpf:1.2.0+20220103 "HL7 Austria e-Impfpass 1.2.0+20220103"
* #urn:hl7-at:eImpf:1.2.0+20220103 ^designation[0].language = #de-AT 
* #urn:hl7-at:eImpf:1.2.0+20220103 ^designation[0].value = "HL7 Austria e-Impfpass 1.2.0+20220103" 
* #urn:hl7-at:eImpf:1.3.0+20220127 "HL7 Austria e-Impfpass 1.3.0+20220127"
* #urn:hl7-at:eImpf:1.3.0+20220127 ^designation[0].language = #de-AT 
* #urn:hl7-at:eImpf:1.3.0+20220127 ^designation[0].value = "HL7 Austria e-Impfpass 1.3.0+20220127" 
* #urn:hl7-at:eImpf:2019 "HL7 Austria e-Impfpass 2019"
* #urn:hl7-at:eImpf:2019 ^designation[0].language = #de-AT 
* #urn:hl7-at:eImpf:2019 ^designation[0].value = "HL7 Austria e-Impfpass 2019" 
* #urn:hl7-at:lab:3.0.0+20211214 "HL7 Austria Labor- und Mikrobiologiebefund 3.0.0+20211214"
* #urn:hl7-at:lab:3.0.0+20211214 ^designation[0].language = #de-AT 
* #urn:hl7-at:lab:3.0.0+20211214 ^designation[0].value = "HL7 Austria Labor- und Mikrobiologiebefund 3.0.0+20211214" 
* #urn:hl7-at:telemon-epi:1.2.0+20210304 "HL7 Austria Telemonitoring Episodenbericht 1.2.0+20210304"
* #urn:hl7-at:telemon-epi:1.2.0+20210304 ^designation[0].language = #de-AT 
* #urn:hl7-at:telemon-epi:1.2.0+20210304 ^designation[0].value = "HL7 Austria Telemonitoring Episodenbericht 1.2.0+20210304" 
