Instance: elga-serviceeventsentlassbr 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-serviceeventsentlassbr" 
* name = "elga-serviceeventsentlassbr" 
* title = "ELGA_ServiceEventsEntlassbr" 
* status = #active 
* version = "3.1" 
* description = "**Description:** Codes to distinguish discharge service events

**Beschreibung:** Value Set zur Unterscheidung der Service Events im Bereich Entlassung" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.57" 
* date = "2015-09-01" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-elga-serviceeventsentlassbrief"
* compose.include[0].concept[0].code = #GDLPUB
* compose.include[0].concept[0].display = "Gesundheitsdienstleistung Pflege und Betreuung"
* compose.include[0].concept[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[0].designation[0].value = "Verwendung im Pflegesituationsbericht" 
* compose.include[0].concept[1].code = #GDLSTATAUF
* compose.include[0].concept[1].display = "Gesundheitsdienstleistung im Rahmen eines stationären Aufenthalts"

* expansion.timestamp = "2022-09-13T14:16:37.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem-elga-serviceeventsentlassbrief"
* expansion.contains[0].code = #GDLPUB
* expansion.contains[0].display = "Gesundheitsdienstleistung Pflege und Betreuung"
* expansion.contains[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* expansion.contains[0].designation[0].value = "Verwendung im Pflegesituationsbericht" 
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem-elga-serviceeventsentlassbrief"
* expansion.contains[1].code = #GDLSTATAUF
* expansion.contains[1].display = "Gesundheitsdienstleistung im Rahmen eines stationären Aufenthalts"
