Instance: elga-problemarten 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-problemarten" 
* name = "elga-problemarten" 
* title = "ELGA_Problemarten" 
* status = #active 
* version = "2.6" 
* description = "**Description:** Describes the process of establishing a problem. Contains information protected by copyright of SNOMED International. Any use of SNOMED CT in Austria requires a valid affiliate license or sublicense. The corresponding license is free of charge, provided that the use only takes place in Austria and fulfills the conditions of the Affiliate License Agreement. Affiliate licenses can be requested directly from the respective NRC via the [Member Licensing and Distribution Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS).

**Beschreibung:** Beschreibt die möglichen Problemarten. Enthält durch SNOMED International urheberrechtlich geschützte Information. Jede Verwendung von SNOMED CT in Österreich erfordert eine aufrechte Affiliate Lizenz oder eine Sublizenz. Die entsprechende Lizenz ist kostenlos, vorausgesetzt die Verwendung findet nur in Österreich statt und erfüllt die Bedingungen des Affiliate License Agreements. Affiliate Lizenzen können über das [Mitglieder-Lizenzierungs- und Distributions-Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS) direkt beim jeweiligen NRC beantragt werden." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.35" 
* date = "2013-01-10" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://wiki.ihe.net/index.php?title=1.3.6.1.4.1.19376.1.5.3.1.4.5" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* compose.include[0].concept[0].code = "160245001"
* compose.include[0].concept[0].display = "no current problems or disability"
* compose.include[0].concept[1].code = "248536006"
* compose.include[0].concept[1].display = "Functional limitation"
* compose.include[0].concept[1].designation[0].language = #de-AT 
* compose.include[0].concept[1].designation[0].value = "Funktionellen Einschränkung" 
* compose.include[0].concept[2].code = "282291009"
* compose.include[0].concept[2].display = "Diagnosis"
* compose.include[0].concept[2].designation[0].language = #de-AT 
* compose.include[0].concept[2].designation[0].value = "Diagnose" 
* compose.include[0].concept[3].code = "404684003"
* compose.include[0].concept[3].display = "Finding"
* compose.include[0].concept[3].designation[0].language = #de-AT 
* compose.include[0].concept[3].designation[0].value = "Befund" 
* compose.include[0].concept[4].code = "409586006"
* compose.include[0].concept[4].display = "Complaint"
* compose.include[0].concept[4].designation[0].language = #de-AT 
* compose.include[0].concept[4].designation[0].value = "Beschwerde" 
* compose.include[0].concept[5].code = "418799008"
* compose.include[0].concept[5].display = "Symptom"
* compose.include[0].concept[6].code = "55607006"
* compose.include[0].concept[6].display = "Problem"
* compose.include[0].concept[7].code = "64572001"
* compose.include[0].concept[7].display = "Condition"
* compose.include[0].concept[7].designation[0].language = #de-AT 
* compose.include[0].concept[7].designation[0].value = "Zustand" 
