Instance: elga-actcode-patinfo 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-actcode-patinfo" 
* name = "elga-actcode-patinfo" 
* title = "ELGA_ActCode_PatInfo" 
* status = #active 
* version = "3.0" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.161" 
* date = "2015-03-31" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-elga-actcode"
* compose.include[0].concept[0].code = #ALTEIN
* compose.include[0].concept[0].display = "Informationen zur alternativen Einnahme"
* compose.include[0].concept[1].code = #ARZNEIINFO
* compose.include[0].concept[1].display = "Informationen zur Arznei"
* compose.include[0].concept[2].code = #ZINFO
* compose.include[0].concept[2].display = "Zusatzinformationen für den Patienten"

* expansion.timestamp = "2022-09-13T14:15:13.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem-elga-actcode"
* expansion.contains[0].code = #ALTEIN
* expansion.contains[0].display = "Informationen zur alternativen Einnahme"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem-elga-actcode"
* expansion.contains[1].code = #ARZNEIINFO
* expansion.contains[1].display = "Informationen zur Arznei"
* expansion.contains[2].system = "https://termgit.elga.gv.at/CodeSystem-elga-actcode"
* expansion.contains[2].code = #ZINFO
* expansion.contains[2].display = "Zusatzinformationen für den Patienten"
