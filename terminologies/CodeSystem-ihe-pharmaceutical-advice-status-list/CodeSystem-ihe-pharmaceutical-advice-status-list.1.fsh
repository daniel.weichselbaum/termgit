Instance: ihe-pharmaceutical-advice-status-list 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ihe-pharmaceutical-advice-status-list" 
* name = "ihe-pharmaceutical-advice-status-list" 
* title = "IHE Pharmaceutical Advice Status List" 
* status = #active 
* content = #complete 
* version = "3.0" 
* description = "**Description:** IHE Codelist for Pharmaceutical Advice status

**Beschreibung:** IHE Codeliste für Pharmazeutische Empfehlung Status" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.3.6.1.4.1.19376.1.9.2.1" 
* date = "2015-03-31" 
* count = 2 
* concept[0].code = #CANCEL
* concept[0].display = "Storno/Absetzen"
* concept[0].designation[0].language = #de-AT 
* concept[0].designation[0].value = "Storno/Absetzen" 
* concept[1].code = #CHANGE
* concept[1].display = "Änderung"
* concept[1].designation[0].language = #de-AT 
* concept[1].designation[0].value = "Änderung" 
