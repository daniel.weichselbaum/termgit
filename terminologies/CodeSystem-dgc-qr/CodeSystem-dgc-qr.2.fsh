Instance: dgc-qr 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-dgc-qr" 
* name = "dgc-qr" 
* title = "DGC_QR" 
* status = #active 
* content = #complete 
* version = "202209" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.202" 
* date = "2022-09-02" 
* copyright = "Enthält durch SNOMED International urheberrechtlich geschützte Information. Jede Verwendung von SNOMED CT in Österreich erfordert eine aufrechte Affiliate Lizenz oder eine Sublizenz. Die entsprechende Lizenz ist kostenlos, vorausgesetzt die Verwendung findet nur in Österreich statt und erfüllt die Bedingungen des Affiliate License Agreements. Affiliate Lizenzen können über das [Mitglieder-Lizenzierungs- und Distributions-Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS) direkt beim jeweiligen NRC beantragt werden. Contains information protected by copyright of SNOMED International. Any use of SNOMED CT in Austria requires a valid affiliate license or sublicense. The corresponding license is free of charge, provided that the use only takes place in Austria and fulfills the conditions of the Affiliate License Agreement. Affiliate licenses can be requested directly from the respective NRC via the [Member Licensing and Distribution Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS)." 
* count = 552 
* property[0].code = #Relationships 
* property[0].type = #string 
* property[1].code = #hints 
* property[1].type = #string 
* property[2].code = #status 
* property[2].type = #code 
* #1065 "Becton Dickinson, BD Veritor System for Rapid Detection of SARS CoV 2"
* #1065 ^definition = Becton Dickinson, BD Veritor System for Rapid Detection of SARS CoV 2
* #1065 ^designation[0].language = #de-AT 
* #1065 ^designation[0].value = "Becton Dickinson, BD Veritor System for Rapid Detection of SARS CoV 2" 
* #1065 ^property[0].code = #Relationships 
* #1065 ^property[0].valueString = "$.t..ma" 
* #1065 ^property[1].code = #hints 
* #1065 ^property[1].valueString = "T^E^B" 
* #1097 "Quidel Corporation, Sofia 2 SARS Antigen FIA"
* #1097 ^definition = Quidel Corporation, Sofia 2 SARS Antigen FIA
* #1097 ^designation[0].language = #de-AT 
* #1097 ^designation[0].value = "Quidel Corporation, Sofia 2 SARS Antigen FIA" 
* #1097 ^property[0].code = #Relationships 
* #1097 ^property[0].valueString = "$.t..ma" 
* #1097 ^property[1].code = #hints 
* #1097 ^property[1].valueString = "T^E^B" 
* #1114 "Sugentech, Inc., SGTi-flex COVID-19 Ag"
* #1114 ^definition = Sugentech, Inc., SGTi-flex COVID-19 Ag
* #1114 ^designation[0].language = #de-AT 
* #1114 ^designation[0].value = "Sugentech, Inc., SGTi-flex COVID-19 Ag" 
* #1114 ^property[0].code = #Relationships 
* #1114 ^property[0].valueString = "$.t..ma" 
* #1114 ^property[1].code = #hints 
* #1114 ^property[1].valueString = "T^E^B" 
* #1119305005 "SARS-CoV-2 antigen vaccine"
* #1119305005 ^definition = SARS-CoV-2 antigen vaccine
* #1119305005 ^property[0].code = #Relationships 
* #1119305005 ^property[0].valueString = "$.v..vp" 
* #1119305005 ^property[1].code = #hints 
* #1119305005 ^property[1].valueString = "T^E^B" 
* #1119349007 "SARS-CoV-2 mRNA vaccine"
* #1119349007 ^definition = SARS-CoV-2 mRNA vaccine
* #1119349007 ^property[0].code = #Relationships 
* #1119349007 ^property[0].valueString = "$.v..vp" 
* #1119349007 ^property[1].code = #hints 
* #1119349007 ^property[1].valueString = "T^E^B" 
* #1144 "Green Cross Medical Science Corp., GENEDIA W COVID-19 Ag"
* #1144 ^definition = Green Cross Medical Science Corp., GENEDIA W COVID-19 Ag
* #1144 ^designation[0].language = #de-AT 
* #1144 ^designation[0].value = "Green Cross Medical Science Corp., GENEDIA W COVID-19 Ag" 
* #1144 ^property[0].code = #Relationships 
* #1144 ^property[0].valueString = "$.t..ma" 
* #1144 ^property[1].code = #hints 
* #1144 ^property[1].valueString = "T^E^B" 
* #1173 "CerTest Biotec, CerTest SARS-CoV-2 Card test"
* #1173 ^definition = CerTest Biotec, CerTest SARS-CoV-2 Card test
* #1173 ^designation[0].language = #de-AT 
* #1173 ^designation[0].value = "CerTest Biotec, CerTest SARS-CoV-2 Card test" 
* #1173 ^property[0].code = #Relationships 
* #1173 ^property[0].valueString = "$.t..ma" 
* #1173 ^property[1].code = #hints 
* #1173 ^property[1].valueString = "T^E^B" 
* #1178 "Shenzhen Microprofit Biotech Co., Ltd., SARS-CoV-2 Spike Protein Test Kit (Colloidal Gold Chromatographic Immunoassay)"
* #1178 ^definition = Shenzhen Microprofit Biotech Co., Ltd., SARS-CoV-2 Spike Protein Test Kit (Colloidal Gold Chromatographic Immunoassay)
* #1178 ^designation[0].language = #de-AT 
* #1178 ^designation[0].value = "Shenzhen Microprofit Biotech Co., Ltd., SARS-CoV-2 Spike Protein Test Kit (Colloidal Gold Chromatographic Immunoassay)" 
* #1178 ^property[0].code = #Relationships 
* #1178 ^property[0].valueString = "$.t..ma" 
* #1178 ^property[1].code = #hints 
* #1178 ^property[1].valueString = "T^E^B" 
* #1180 "MEDsan GmbH, MEDsan SARS-CoV-2 Antigen Rapid Test"
* #1180 ^definition = MEDsan GmbH, MEDsan SARS-CoV-2 Antigen Rapid Test
* #1180 ^designation[0].language = #de-AT 
* #1180 ^designation[0].value = "MEDsan GmbH, MEDsan SARS-CoV-2 Antigen Rapid Test" 
* #1180 ^property[0].code = #Relationships 
* #1180 ^property[0].valueString = "$.t..ma" 
* #1180 ^property[1].code = #hints 
* #1180 ^property[1].valueString = "T^E^B" 
* #1190 "möLab, COVID-19 Rapid Antigen Test"
* #1190 ^definition = möLab, COVID-19 Rapid Antigen Test
* #1190 ^designation[0].language = #de-AT 
* #1190 ^designation[0].value = "möLab, COVID-19 Rapid Antigen Test" 
* #1190 ^property[0].code = #Relationships 
* #1190 ^property[0].valueString = "$.t..ma" 
* #1190 ^property[1].code = #hints 
* #1190 ^property[1].valueString = "T^E^B" 
* #1197 "Goldsite Diagnostic Inc., SARS-CoV-2 Antigen Kit (Colloidal Gold)"
* #1197 ^definition = Goldsite Diagnostic Inc., SARS-CoV-2 Antigen Kit (Colloidal Gold)
* #1197 ^designation[0].language = #de-AT 
* #1197 ^designation[0].value = "Goldsite Diagnostic Inc., SARS-CoV-2 Antigen Kit (Colloidal Gold)" 
* #1197 ^property[0].code = #Relationships 
* #1197 ^property[0].valueString = "$.t..ma" 
* #1197 ^property[1].code = #hints 
* #1197 ^property[1].valueString = "T^E^B" 
* #1199 "Oncosem Onkolojik Sistemler San. ve Tic. A.S., CAT"
* #1199 ^definition = Oncosem Onkolojik Sistemler San. ve Tic. A.S., CAT
* #1199 ^designation[0].language = #de-AT 
* #1199 ^designation[0].value = "Oncosem Onkolojik Sistemler San. ve Tic. A.S., CAT" 
* #1199 ^property[0].code = #Relationships 
* #1199 ^property[0].valueString = "$.t..ma" 
* #1199 ^property[1].code = #hints 
* #1199 ^property[1].valueString = "T^E^B" 
* #1201 "ScheBo Biotech AG, ScheBo SARS CoV-2 Quick Antigen"
* #1201 ^definition = ScheBo Biotech AG, ScheBo SARS CoV-2 Quick Antigen
* #1201 ^designation[0].language = #de-AT 
* #1201 ^designation[0].value = "ScheBo Biotech AG, ScheBo SARS CoV-2 Quick Antigen" 
* #1201 ^property[0].code = #Relationships 
* #1201 ^property[0].valueString = "$.t..ma" 
* #1201 ^property[1].code = #hints 
* #1201 ^property[1].valueString = "T^E^B" 
* #1215 "Hangzhou Laihe Biotech Co., LYHER Novel Coronavirus (COVID-19) Antigen Test Kit (Colloidal Gold)"
* #1215 ^definition = Hangzhou Laihe Biotech Co., LYHER Novel Coronavirus (COVID-19) Antigen Test Kit (Colloidal Gold)
* #1215 ^designation[0].language = #de-AT 
* #1215 ^designation[0].value = "Hangzhou Laihe Biotech Co., LYHER Novel Coronavirus (COVID-19) Antigen Test Kit (Colloidal Gold)" 
* #1215 ^property[0].code = #Relationships 
* #1215 ^property[0].valueString = "$.t..ma" 
* #1215 ^property[1].code = #hints 
* #1215 ^property[1].valueString = "T^E^B" 
* #1216 "Guangdong Longsee Biomedical Co., Ltd., COVID-2019-nCoV Ag Rapid TestDetection Kit(Immuno-Chromatography)"
* #1216 ^definition = Guangdong Longsee Biomedical Co., Ltd., COVID-2019-nCoV Ag Rapid TestDetection Kit(Immuno-Chromatography)
* #1216 ^designation[0].language = #de-AT 
* #1216 ^designation[0].value = "Guangdong Longsee Biomedical Co., Ltd., COVID-2019-nCoV Ag Rapid TestDetection Kit(Immuno-Chromatography)" 
* #1216 ^property[0].code = #Relationships 
* #1216 ^property[0].valueString = "$.t..ma" 
* #1216 ^property[1].code = #hints 
* #1216 ^property[1].valueString = "T^E^B" 
* #1218 "Siemens Healthineers, CLINITEST Rapid COVID-19 Antigen Test"
* #1218 ^definition = Siemens Healthineers, CLINITEST Rapid COVID-19 Antigen Test
* #1218 ^designation[0].language = #de-AT 
* #1218 ^designation[0].value = "Siemens Healthineers, CLINITEST Rapid COVID-19 Antigen Test" 
* #1218 ^property[0].code = #Relationships 
* #1218 ^property[0].valueString = "$.t..ma" 
* #1218 ^property[1].code = #hints 
* #1218 ^property[1].valueString = "T^E^B" 
* #1223 "BIOSYNEX SA , BIOSYNEX COVID-19 Ag BSS"
* #1223 ^definition = BIOSYNEX SA , BIOSYNEX COVID-19 Ag BSS
* #1223 ^designation[0].language = #de-AT 
* #1223 ^designation[0].value = "BIOSYNEX SA , BIOSYNEX COVID-19 Ag BSS" 
* #1223 ^property[0].code = #Relationships 
* #1223 ^property[0].valueString = "$.t..ma" 
* #1223 ^property[1].code = #hints 
* #1223 ^property[1].valueString = "T^E^B" 
* #1225 "DDS DIAGNOSTIC, Test Rapid Covid-19 Antigen (tampon nazofaringian)"
* #1225 ^definition = DDS DIAGNOSTIC, Test Rapid Covid-19 Antigen (tampon nazofaringian)
* #1225 ^designation[0].language = #de-AT 
* #1225 ^designation[0].value = "DDS DIAGNOSTIC, Test Rapid Covid-19 Antigen (tampon nazofaringian)" 
* #1225 ^property[0].code = #Relationships 
* #1225 ^property[0].valueString = "$.t..ma" 
* #1225 ^property[1].code = #hints 
* #1225 ^property[1].valueString = "T^E^B" 
* #1228 "Shenzhen Microprofit Biotech Co., Ltd, SARS-CoV-2 Spike Protein Test Kit (Fluorescence Immunoassay)"
* #1228 ^definition = Shenzhen Microprofit Biotech Co., Ltd, SARS-CoV-2 Spike Protein Test Kit (Fluorescence Immunoassay)
* #1228 ^designation[0].language = #de-AT 
* #1228 ^designation[0].value = "Shenzhen Microprofit Biotech Co., Ltd, SARS-CoV-2 Spike Protein Test Kit (Fluorescence Immunoassay)" 
* #1228 ^property[0].code = #Relationships 
* #1228 ^property[0].valueString = "$.t..ma" 
* #1228 ^property[1].code = #hints 
* #1228 ^property[1].valueString = "T^E^B" 
* #1232 "Abbott Rapid Diagnostics, Panbio COVID-19 Ag Rapid Test"
* #1232 ^definition = Abbott Rapid Diagnostics, Panbio COVID-19 Ag Rapid Test
* #1232 ^designation[0].language = #de-AT 
* #1232 ^designation[0].value = "Abbott Rapid Diagnostics, Panbio COVID-19 Ag Rapid Test " 
* #1232 ^property[0].code = #Relationships 
* #1232 ^property[0].valueString = "$.t..ma" 
* #1232 ^property[1].code = #hints 
* #1232 ^property[1].valueString = "T^E^B" 
* #1236 "BTNX Inc., Rapid Response COVID-19 Antigen Rapid Test Device"
* #1236 ^definition = BTNX Inc., Rapid Response COVID-19 Antigen Rapid Test Device
* #1236 ^designation[0].language = #de-AT 
* #1236 ^designation[0].value = "BTNX Inc., Rapid Response COVID-19 Antigen Rapid Test Device" 
* #1236 ^property[0].code = #Relationships 
* #1236 ^property[0].valueString = "$.t..ma" 
* #1236 ^property[1].code = #hints 
* #1236 ^property[1].valueString = "T^E^B" 
* #1242 "BIONOTE, NowCheck COVID-19 Ag Test"
* #1242 ^definition = BIONOTE, NowCheck COVID-19 Ag Test
* #1242 ^designation[0].language = #de-AT 
* #1242 ^designation[0].value = "BIONOTE, NowCheck COVID-19 Ag Test" 
* #1242 ^property[0].code = #Relationships 
* #1242 ^property[0].valueString = "$.t..ma" 
* #1242 ^property[1].code = #hints 
* #1242 ^property[1].valueString = "T^E^B" 
* #1243 "Edinburgh Genetics Limited, Edinburgh Genetics ActivXpress+ COVID-19 Antigen Complete Testing Kit"
* #1243 ^definition = Edinburgh Genetics Limited, Edinburgh Genetics ActivXpress+ COVID-19 Antigen Complete Testing Kit
* #1243 ^designation[0].language = #de-AT 
* #1243 ^designation[0].value = "Edinburgh Genetics Limited, Edinburgh Genetics ActivXpress+ COVID-19 Antigen Complete Testing Kit" 
* #1243 ^property[0].code = #Relationships 
* #1243 ^property[0].valueString = "$.t..ma" 
* #1243 ^property[1].code = #hints 
* #1243 ^property[1].valueString = "T^E^B" 
* #1244 "GenBody Inc, GenBody COVID-19 Ag Test"
* #1244 ^definition = GenBody Inc, GenBody COVID-19 Ag Test
* #1244 ^designation[0].language = #de-AT 
* #1244 ^designation[0].value = "GenBody Inc, GenBody COVID-19 Ag Test" 
* #1244 ^property[0].code = #Relationships 
* #1244 ^property[0].valueString = "$.t..ma" 
* #1244 ^property[1].code = #hints 
* #1244 ^property[1].valueString = "T^E^B" 
* #1253 "GenSure Biotech Inc, Gensure COVID-19 Antigen Rapid Test Kit (REF: P2004) (DIA-COVID -19 Ag Rapid Test)"
* #1253 ^definition = GenSure Biotech Inc, Gensure COVID-19 Antigen Rapid Test Kit (REF: P2004) (DIA-COVID -19 Ag Rapid Test)
* #1253 ^designation[0].language = #de-AT 
* #1253 ^designation[0].value = "GenSure Biotech Inc, Gensure COVID-19 Antigen Rapid Test Kit (REF: P2004) (DIA-COVID -19 Ag Rapid Test)" 
* #1253 ^property[0].code = #Relationships 
* #1253 ^property[0].valueString = "$.t..ma" 
* #1253 ^property[1].code = #hints 
* #1253 ^property[1].valueString = "T^E^B" 
* #1257 "Hangzhou AllTest Biotech Co., Ltd, COVID-19 Antigen Rapid Test"
* #1257 ^definition = Hangzhou AllTest Biotech Co., Ltd, COVID-19 Antigen Rapid Test
* #1257 ^designation[0].language = #de-AT 
* #1257 ^designation[0].value = "Hangzhou AllTest Biotech Co., Ltd, COVID-19 Antigen Rapid Test  " 
* #1257 ^property[0].code = #Relationships 
* #1257 ^property[0].valueString = "$.t..ma" 
* #1257 ^property[1].code = #hints 
* #1257 ^property[1].valueString = "T^E^B" 
* #1266 "Labnovation Technologies Inc., SARS-CoV-2 Antigen Rapid Test Kit"
* #1266 ^definition = Labnovation Technologies Inc., SARS-CoV-2 Antigen Rapid Test Kit
* #1266 ^designation[0].language = #de-AT 
* #1266 ^designation[0].value = "Labnovation Technologies Inc., SARS-CoV-2 Antigen Rapid Test Kit" 
* #1266 ^property[0].code = #Relationships 
* #1266 ^property[0].valueString = "$.t..ma" 
* #1266 ^property[1].code = #hints 
* #1266 ^property[1].valueString = "T^E^B" 
* #1267 "LumiQuick Diagnostics Inc., QuickProfil COVID-19 ANTIGEN Test"
* #1267 ^definition = LumiQuick Diagnostics Inc., QuickProfile? COVID-19 ANTIGEN Test
* #1267 ^designation[0].language = #de-AT 
* #1267 ^designation[0].value = "LumiQuick Diagnostics Inc., QuickProfile? COVID-19 ANTIGEN Test" 
* #1267 ^property[0].code = #Relationships 
* #1267 ^property[0].valueString = "$.t..ma" 
* #1267 ^property[1].code = #hints 
* #1267 ^property[1].valueString = "T^E^B" 
* #1268 "LumiraDX , LumiraDx SARS-CoV-2 Ag Test"
* #1268 ^definition = LumiraDX , LumiraDx SARS-CoV-2 Ag Test
* #1268 ^designation[0].language = #de-AT 
* #1268 ^designation[0].value = "LumiraDX , LumiraDx SARS-CoV-2 Ag Test " 
* #1268 ^property[0].code = #Relationships 
* #1268 ^property[0].valueString = "$.t..ma" 
* #1268 ^property[1].code = #hints 
* #1268 ^property[1].valueString = "T^E^B" 
* #1271 "Precision Biosensor Inc., Exdia COVI-19 Ag Test"
* #1271 ^definition = Precision Biosensor Inc., Exdia COVI-19 Ag Test
* #1271 ^designation[0].language = #de-AT 
* #1271 ^designation[0].value = "Precision Biosensor Inc., Exdia COVI-19 Ag Test" 
* #1271 ^property[0].code = #Relationships 
* #1271 ^property[0].valueString = "$.t..ma" 
* #1271 ^property[1].code = #hints 
* #1271 ^property[1].valueString = "T^E^B" 
* #1276 "Willi Fox GmbH, Willi Fox COVID-19 Antigen rapid test"
* #1276 ^definition = Willi Fox GmbH, Willi Fox COVID-19 Antigen rapid test
* #1276 ^designation[0].language = #de-AT 
* #1276 ^designation[0].value = "Willi Fox GmbH, Willi Fox COVID-19 Antigen rapid test" 
* #1276 ^property[0].code = #Relationships 
* #1276 ^property[0].valueString = "$.t..ma" 
* #1276 ^property[1].code = #hints 
* #1276 ^property[1].valueString = "T^E^B" 
* #1278 "Xiamen Boson Biotech Co, Rapid SARS-CoV-2 Antigen Test card"
* #1278 ^definition = Xiamen Boson Biotech Co, Rapid SARS-CoV-2 Antigen Test card
* #1278 ^designation[0].language = #de-AT 
* #1278 ^designation[0].value = "Xiamen Boson Biotech Co, Rapid SARS-CoV-2 Antigen Test card " 
* #1278 ^property[0].code = #Relationships 
* #1278 ^property[0].valueString = "$.t..ma" 
* #1278 ^property[1].code = #hints 
* #1278 ^property[1].valueString = "T^E^B" 
* #1286 "BIOHIT HealthCcare (Hefei) Co., Ltd., SARS-CoV-2 Antigen Rapid Test Kit (Fluorescence Immunochromatography)"
* #1286 ^definition = BIOHIT HealthCcare (Hefei) Co., Ltd., SARS-CoV-2 Antigen Rapid Test Kit (Fluorescence Immunochromatography)
* #1286 ^designation[0].language = #de-AT 
* #1286 ^designation[0].value = "BIOHIT HealthCcare (Hefei) Co., Ltd., SARS-CoV-2 Antigen Rapid Test Kit (Fluorescence Immunochromatography)" 
* #1286 ^property[0].code = #Relationships 
* #1286 ^property[0].valueString = "$.t..ma" 
* #1286 ^property[1].code = #hints 
* #1286 ^property[1].valueString = "T^E^B" 
* #1295 "Zhejiang Anji Saianfu Biotech Co.., Ltd., reOpenTest COVID-19  Antigen Rapid Test"
* #1295 ^definition = Zhejiang Anji Saianfu Biotech Co.., Ltd., reOpenTest COVID-19  Antigen Rapid Test
* #1295 ^designation[0].language = #de-AT 
* #1295 ^designation[0].value = "Zhejiang Anji Saianfu Biotech Co.., Ltd., reOpenTest COVID-19  Antigen Rapid Test" 
* #1295 ^property[0].code = #Relationships 
* #1295 ^property[0].valueString = "$.t..ma" 
* #1295 ^property[1].code = #hints 
* #1295 ^property[1].valueString = "T^E^B" 
* #1296 "Zhejiang Anji Saianfu Biotech Co.., Ltd., AndLucky COVID-19 Antigen Rapid Test"
* #1296 ^definition = Zhejiang Anji Saianfu Biotech Co.., Ltd., AndLucky COVID-19 Antigen Rapid Test
* #1296 ^designation[0].language = #de-AT 
* #1296 ^designation[0].value = "Zhejiang Anji Saianfu Biotech Co.., Ltd., AndLucky COVID-19 Antigen Rapid Test" 
* #1296 ^property[0].code = #Relationships 
* #1296 ^property[0].valueString = "$.t..ma" 
* #1296 ^property[1].code = #hints 
* #1296 ^property[1].valueString = "T^E^B" 
* #1304 "AMEDA Labordiagnostik GmbH, AMP Rapid Test SARS-CoV-2 Ag"
* #1304 ^definition = AMEDA Labordiagnostik GmbH, AMP Rapid Test SARS-CoV-2 Ag
* #1304 ^designation[0].language = #de-AT 
* #1304 ^designation[0].value = "AMEDA Labordiagnostik GmbH, AMP Rapid Test SARS-CoV-2 Ag" 
* #1304 ^property[0].code = #Relationships 
* #1304 ^property[0].valueString = "$.t..ma" 
* #1304 ^property[1].code = #hints 
* #1304 ^property[1].valueString = "T^E^B" 
* #1319 "SGA Medikal, V-Chek SARS-CoV2- Rapid Ag Tets (Coloidal Gold)"
* #1319 ^definition = SGA Medikal, V-Chek SARS-CoV2- Rapid Ag Tets (Coloidal Gold)
* #1319 ^designation[0].language = #de-AT 
* #1319 ^designation[0].value = "SGA Medikal, V-Chek SARS-CoV2- Rapid Ag Tets (Coloidal Gold)" 
* #1319 ^property[0].code = #Relationships 
* #1319 ^property[0].valueString = "$.t..ma" 
* #1319 ^property[1].code = #hints 
* #1319 ^property[1].valueString = "T^E^B" 
* #1331 "Beijing Lepu Medical Technology, SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold immunochromatography)"
* #1331 ^definition = Beijing Lepu Medical Technology, SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold immunochromatography)
* #1331 ^designation[0].language = #de-AT 
* #1331 ^designation[0].value = "Beijing Lepu Medical Technology, SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold immunochromatography)" 
* #1331 ^property[0].code = #Relationships 
* #1331 ^property[0].valueString = "$.t..ma" 
* #1331 ^property[1].code = #hints 
* #1331 ^property[1].valueString = "T^E^B" 
* #1333 "Joinstar Biomedical Technology Co. Ltd, COVID-19 Antigen Rapid Test (Colloidal Gold)"
* #1333 ^definition = Joinstar Biomedical Technology Co. Ltd, COVID-19 Antigen Rapid Test (Colloidal Gold)
* #1333 ^designation[0].language = #de-AT 
* #1333 ^designation[0].value = "Joinstar Biomedical Technology Co. Ltd, COVID-19 Antigen Rapid Test (Colloidal Gold)" 
* #1333 ^property[0].code = #Relationships 
* #1333 ^property[0].valueString = "$.t..ma" 
* #1333 ^property[1].code = #hints 
* #1333 ^property[1].valueString = "T^E^B" 
* #1341 "Qingdao Hightop Biotech Co. Ltd, SARS-CoV-2 Antigen Rapid Test"
* #1341 ^definition = Qingdao Hightop Biotech Co. Ltd, SARS-CoV-2 Antigen Rapid Test
* #1341 ^designation[0].language = #de-AT 
* #1341 ^designation[0].value = "Qingdao Hightop Biotech Co. Ltd, SARS-CoV-2 Antigen Rapid Test" 
* #1341 ^property[0].code = #Relationships 
* #1341 ^property[0].valueString = "$.t..ma" 
* #1341 ^property[1].code = #hints 
* #1341 ^property[1].valueString = "T^E^B" 
* #1343 "Zhejiang Orient Gene Biotech Co., Ltd, Coronavirus Ag Rapid Test Cassette (Swab)"
* #1343 ^definition = Zhejiang Orient Gene Biotech Co., Ltd, Coronavirus Ag Rapid Test Cassette (Swab)
* #1343 ^designation[0].language = #de-AT 
* #1343 ^designation[0].value = "Zhejiang Orient Gene Biotech Co., Ltd, Coronavirus Ag Rapid Test Cassette (Swab)" 
* #1343 ^property[0].code = #Relationships 
* #1343 ^property[0].valueString = "$.t..ma" 
* #1343 ^property[1].code = #hints 
* #1343 ^property[1].valueString = "T^E^B" 
* #1347 "Shenzhen YHLO Biotech Co., Ltd., GLINE-2019-nCoV Ag"
* #1347 ^definition = Shenzhen YHLO Biotech Co., Ltd., GLINE-2019-nCoV Ag
* #1347 ^designation[0].language = #de-AT 
* #1347 ^designation[0].value = "Shenzhen YHLO Biotech Co., Ltd., GLINE-2019-nCoV Ag" 
* #1347 ^property[0].code = #Relationships 
* #1347 ^property[0].valueString = "$.t..ma" 
* #1347 ^property[1].code = #hints 
* #1347 ^property[1].valueString = "T^E^B" 
* #1353 "LINKCARE (NANTONG DIAGNOS BIO), COVID-19 Antigen Test Kit (Colloidal Gold)"
* #1353 ^definition = LINKCARE (NANTONG DIAGNOS BIO), COVID-19 Antigen Test Kit (Colloidal Gold)
* #1353 ^designation[0].language = #de-AT 
* #1353 ^designation[0].value = "LINKCARE (NANTONG DIAGNOS BIO), COVID-19 Antigen Test Kit (Colloidal Gold)" 
* #1353 ^property[0].code = #Relationships 
* #1353 ^property[0].valueString = "$.t..ma" 
* #1353 ^property[1].code = #hints 
* #1353 ^property[1].valueString = "T^E^B" 
* #1357 "SGA Medikal, V-Chek SARS-CoV-2 Rapid Ag Test (colloidal gold)"
* #1357 ^definition = SGA Medikal, V-Chek SARS-CoV-2 Rapid Ag Test (colloidal gold)
* #1357 ^designation[0].language = #de-AT 
* #1357 ^designation[0].value = "SGA Medikal, V-Chek SARS-CoV-2 Rapid Ag Test (colloidal gold)" 
* #1357 ^property[0].code = #Relationships 
* #1357 ^property[0].valueString = "$.t..ma" 
* #1357 ^property[1].code = #hints 
* #1357 ^property[1].valueString = "T^E^B" 
* #1360 "Guangdong Wesail Biotech Co. Ltd, COVID-19 AG Test Kit"
* #1360 ^definition = Guangdong Wesail Biotech Co. Ltd, COVID-19 AG Test Kit
* #1360 ^designation[0].language = #de-AT 
* #1360 ^designation[0].value = "Guangdong Wesail Biotech Co. Ltd, COVID-19 AG Test Kit " 
* #1360 ^property[0].code = #Relationships 
* #1360 ^property[0].valueString = "$.t..ma" 
* #1360 ^property[1].code = #hints 
* #1360 ^property[1].valueString = "T^E^B" 
* #1363 "Hangzhou Clongene Biotech Co., Ltd., COVID-19 Antigen Rapid Test Kit"
* #1363 ^definition = Hangzhou Clongene Biotech Co., Ltd., COVID-19 Antigen Rapid Test Kit
* #1363 ^designation[0].language = #de-AT 
* #1363 ^designation[0].value = "Hangzhou Clongene Biotech Co., Ltd., COVID-19 Antigen Rapid Test Kit" 
* #1363 ^property[0].code = #Relationships 
* #1363 ^property[0].valueString = "$.t..ma" 
* #1363 ^property[1].code = #hints 
* #1363 ^property[1].valueString = "T^E^B" 
* #1365 "Hangzhou Clongene Biotech Co., Ltd., COVID-19/Influenza A+B Antigen Combo Rapid Test"
* #1365 ^definition = Hangzhou Clongene Biotech Co., Ltd., COVID-19/Influenza A+B Antigen Combo Rapid Test
* #1365 ^designation[0].language = #de-AT 
* #1365 ^designation[0].value = "Hangzhou Clongene Biotech Co., Ltd., COVID-19/Influenza A+B Antigen Combo Rapid Test" 
* #1365 ^property[0].code = #Relationships 
* #1365 ^property[0].valueString = "$.t..ma" 
* #1365 ^property[1].code = #hints 
* #1365 ^property[1].valueString = "T^E^B" 
* #1375 "DIALAB GmbH, DIAQUICK COVID -19 Ag Cassette"
* #1375 ^definition = DIALAB GmbH, DIAQUICK COVID -19 Ag Cassette
* #1375 ^designation[0].language = #de-AT 
* #1375 ^designation[0].value = "DIALAB GmbH, DIAQUICK COVID -19 Ag Cassette" 
* #1375 ^property[0].code = #Relationships 
* #1375 ^property[0].valueString = "$.t..ma" 
* #1375 ^property[1].code = #hints 
* #1375 ^property[1].valueString = "T^E^B" 
* #1392 "Hangzhou Testsea Biotechnology Co., Ltd., Testsealabs Covid-19 Antigen Rapid Test Cassette"
* #1392 ^definition = Hangzhou Testsea Biotechnology Co., Ltd., Testsealabs Covid-19 Antigen Rapid Test Cassette
* #1392 ^designation[0].language = #de-AT 
* #1392 ^designation[0].value = "Hangzhou Testsea Biotechnology Co., Ltd., Testsealabs Covid-19 Antigen Rapid Test Cassette" 
* #1392 ^property[0].code = #Relationships 
* #1392 ^property[0].valueString = "$.t..ma" 
* #1392 ^property[1].code = #hints 
* #1392 ^property[1].valueString = "T^E^B" 
* #1443 "Vitrosens Biotechnology Co. Ltd, RapidFor SARS-CoV-2 Ag Test Kit"
* #1443 ^definition = Vitrosens Biotechnology Co. Ltd, RapidFor SARS-CoV-2 Ag Test Kit
* #1443 ^designation[0].language = #de-AT 
* #1443 ^designation[0].value = "Vitrosens Biotechnology Co. Ltd, RapidFor SARS-CoV-2 Ag Test Kit" 
* #1443 ^property[0].code = #Relationships 
* #1443 ^property[0].valueString = "$.t..ma" 
* #1443 ^property[1].code = #hints 
* #1443 ^property[1].valueString = "T^E^B" 
* #1457 "Acon Biotech (Hangzhou) Co., Ltd, SARS-CoV-2 Antigen Rapid Test"
* #1457 ^definition = Acon Biotech (Hangzhou) Co., Ltd, SARS-CoV-2 Antigen Rapid Test
* #1457 ^designation[0].language = #de-AT 
* #1457 ^designation[0].value = "Acon Biotech (Hangzhou) Co., Ltd, SARS-CoV-2 Antigen Rapid Test" 
* #1457 ^property[0].code = #Relationships 
* #1457 ^property[0].valueString = "$.t..ma" 
* #1457 ^property[1].code = #hints 
* #1457 ^property[1].valueString = "T^E^B" 
* #1465 "Triplex International Biosciences Co., Ltd, China, SARS-CoV-2 Antigen Rapid Test Kit"
* #1465 ^definition = Triplex International Biosciences Co., Ltd, China, SARS-CoV-2 Antigen Rapid Test Kit
* #1465 ^designation[0].language = #de-AT 
* #1465 ^designation[0].value = "Triplex International Biosciences Co., Ltd, China, SARS-CoV-2 Antigen Rapid Test Kit" 
* #1465 ^property[0].code = #Relationships 
* #1465 ^property[0].valueString = "$.t..ma" 
* #1465 ^property[1].code = #hints 
* #1465 ^property[1].valueString = "T^E^B" 
* #1466 "TODA PHARMA, TODA CORONADIAG Ag"
* #1466 ^definition = TODA PHARMA, TODA CORONADIAG Ag
* #1466 ^designation[0].language = #de-AT 
* #1466 ^designation[0].value = "TODA PHARMA, TODA CORONADIAG Ag" 
* #1466 ^property[0].code = #Relationships 
* #1466 ^property[0].valueString = "$.t..ma" 
* #1466 ^property[1].code = #hints 
* #1466 ^property[1].valueString = "T^E^B" 
* #1468 "ACON Laboratories, Inc, Flowflex SARS-CoV-2 Antigen Rapid Test"
* #1468 ^definition = ACON Laboratories, Inc, Flowflex SARS-CoV-2 Antigen Rapid Test
* #1468 ^designation[0].language = #de-AT 
* #1468 ^designation[0].value = "ACON Laboratories, Inc, Flowflex SARS-CoV-2 Antigen Rapid Test " 
* #1468 ^property[0].code = #Relationships 
* #1468 ^property[0].valueString = "$.t..ma" 
* #1468 ^property[1].code = #hints 
* #1468 ^property[1].valueString = "T^E^B" 
* #1481 "MP Biomedicals , Rapid SARS-CoV-2 Antigen Test Card"
* #1481 ^definition = MP Biomedicals , Rapid SARS-CoV-2 Antigen Test Card
* #1481 ^designation[0].language = #de-AT 
* #1481 ^designation[0].value = "MP Biomedicals , Rapid SARS-CoV-2 Antigen Test Card" 
* #1481 ^property[0].code = #Relationships 
* #1481 ^property[0].valueString = "$.t..ma" 
* #1481 ^property[1].code = #hints 
* #1481 ^property[1].valueString = "T^E^B" 
* #1485 "Beijing Wantai Biological Pharmacy Enterprise Co., Ltd, Wantai SARS-CoV-2 Ag Rapid Test (colloidal gold)"
* #1485 ^definition = Beijing Wantai Biological Pharmacy Enterprise Co., Ltd, Wantai SARS-CoV-2 Ag Rapid Test (colloidal gold)
* #1485 ^designation[0].language = #de-AT 
* #1485 ^designation[0].value = "Beijing Wantai Biological Pharmacy Enterprise Co., Ltd, Wantai SARS-CoV-2 Ag Rapid Test (colloidal gold)" 
* #1485 ^property[0].code = #Relationships 
* #1485 ^property[0].valueString = "$.t..ma" 
* #1485 ^property[1].code = #hints 
* #1485 ^property[1].valueString = "T^E^B" 
* #1489 "Safecare Biotech Hangzhou Co, COVID-19 Antigen Rapid Test Kit (Swab)"
* #1489 ^definition = Safecare Biotech Hangzhou Co, COVID-19 Antigen Rapid Test Kit (Swab)
* #1489 ^designation[0].language = #de-AT 
* #1489 ^designation[0].value = "Safecare Biotech Hangzhou Co, COVID-19 Antigen Rapid Test Kit (Swab)" 
* #1489 ^property[0].code = #Relationships 
* #1489 ^property[0].valueString = "$.t..ma" 
* #1489 ^property[1].code = #hints 
* #1489 ^property[1].valueString = "T^E^B" 
* #1490 "Safecare Biotech Hangzhou Co, Multi-Respiratory Virus Antigen Test Kit (Swab) (Influenza A+B/COVID-19)"
* #1490 ^definition = Safecare Biotech Hangzhou Co, Multi-Respiratory Virus Antigen Test Kit (Swab) (Influenza A+B/COVID-19)
* #1490 ^designation[0].language = #de-AT 
* #1490 ^designation[0].value = "Safecare Biotech Hangzhou Co, Multi-Respiratory Virus Antigen Test Kit (Swab) (Influenza A+B/COVID-19)" 
* #1490 ^property[0].code = #Relationships 
* #1490 ^property[0].valueString = "$.t..ma" 
* #1490 ^property[1].code = #hints 
* #1490 ^property[1].valueString = "T^E^B" 
* #1494 "BIOSYNEX SA, BIOSYNEX COVID-19 Ag+ BSS"
* #1494 ^definition = BIOSYNEX SA, BIOSYNEX COVID-19 Ag+ BSS
* #1494 ^designation[0].language = #de-AT 
* #1494 ^designation[0].value = "BIOSYNEX SA, BIOSYNEX COVID-19 Ag+ BSS" 
* #1494 ^property[0].code = #Relationships 
* #1494 ^property[0].valueString = "$.t..ma" 
* #1494 ^property[1].code = #hints 
* #1494 ^property[1].valueString = "T^E^B" 
* #1495 "Prognosis Biotech, Rapid Test Ag 2019-nCov"
* #1495 ^definition = Prognosis Biotech, Rapid Test Ag 2019-nCov
* #1495 ^designation[0].language = #de-AT 
* #1495 ^designation[0].value = "Prognosis Biotech, Rapid Test Ag 2019-nCov" 
* #1495 ^property[0].code = #Relationships 
* #1495 ^property[0].valueString = "$.t..ma" 
* #1495 ^property[1].code = #hints 
* #1495 ^property[1].valueString = "T^E^B" 
* #1501 "New Gene (Hangzhou) Bioengineering Co., Ltd., COVID-19 Antigen Detection Kit"
* #1501 ^definition = New Gene (Hangzhou) Bioengineering Co., Ltd., COVID-19 Antigen Detection Kit
* #1501 ^designation[0].language = #de-AT 
* #1501 ^designation[0].value = "New Gene (Hangzhou) Bioengineering Co., Ltd., COVID-19 Antigen Detection Kit" 
* #1501 ^property[0].code = #Relationships 
* #1501 ^property[0].valueString = "$.t..ma" 
* #1501 ^property[1].code = #hints 
* #1501 ^property[1].valueString = "T^E^B" 
* #1573 "Nantong Egens Biotechnology Co.,Ltd, COVID-19 Antigen Rapid Test Kit"
* #1573 ^definition = Nantong Egens Biotechnology Co.,Ltd, COVID-19 Antigen Rapid Test Kit
* #1573 ^designation[0].language = #de-AT 
* #1573 ^designation[0].value = "Nantong Egens Biotechnology Co.,Ltd, COVID-19 Antigen Rapid Test Kit" 
* #1573 ^property[0].code = #Relationships 
* #1573 ^property[0].valueString = "$.t..ma" 
* #1573 ^property[1].code = #hints 
* #1573 ^property[1].valueString = "T^E^B" 
* #1581 "CTK Biotech, Inc, OnSite COVID-19 Ag Rapid Test"
* #1581 ^definition = CTK Biotech, Inc, OnSite COVID-19 Ag Rapid Test
* #1581 ^designation[0].language = #de-AT 
* #1581 ^designation[0].value = "CTK Biotech, Inc, OnSite COVID-19 Ag Rapid Test" 
* #1581 ^property[0].code = #Relationships 
* #1581 ^property[0].valueString = "$.t..ma" 
* #1581 ^property[1].code = #hints 
* #1581 ^property[1].valueString = "T^E^B" 
* #1592 "Shenzhen Lifotronic Technology Co., Ltd., Antigen Rapid Test Ag SARS-CoV-2"
* #1592 ^definition = Shenzhen Lifotronic Technology Co., Ltd., Antigen Rapid Test Ag SARS-CoV-2
* #1592 ^designation[0].language = #de-AT 
* #1592 ^designation[0].value = "Shenzhen Lifotronic Technology Co., Ltd., Antigen Rapid Test Ag SARS-CoV-2" 
* #1592 ^property[0].code = #Relationships 
* #1592 ^property[0].valueString = "$.t..ma" 
* #1592 ^property[1].code = #hints 
* #1592 ^property[1].valueString = "T^E^B" 
* #1593 "OSANG Healthcare Co., Ltd., GeneFinder COVID-19 Ag Rapid Test"
* #1593 ^definition = OSANG Healthcare Co., Ltd., GeneFinder COVID-19 Ag Rapid Test
* #1593 ^designation[0].language = #de-AT 
* #1593 ^designation[0].value = "OSANG Healthcare Co., Ltd., GeneFinder COVID-19 Ag Rapid Test" 
* #1593 ^property[0].code = #Relationships 
* #1593 ^property[0].valueString = "$.t..ma" 
* #1593 ^property[1].code = #hints 
* #1593 ^property[1].valueString = "T^E^B" 
* #1599 "Biomerica Inc., Biomerica COVID-19 Antigen Rapid Test (nasopharyngeal swab)"
* #1599 ^definition = Biomerica Inc., Biomerica COVID-19 Antigen Rapid Test (nasopharyngeal swab)
* #1599 ^designation[0].language = #de-AT 
* #1599 ^designation[0].value = "Biomerica Inc., Biomerica COVID-19 Antigen Rapid Test (nasopharyngeal swab)" 
* #1599 ^property[0].code = #Relationships 
* #1599 ^property[0].valueString = "$.t..ma" 
* #1599 ^property[1].code = #hints 
* #1599 ^property[1].valueString = "T^E^B" 
* #1604 "Roche (SD BIOSENSOR), SARS-CoV-2 Antigen Rapid Test"
* #1604 ^definition = Roche (SD BIOSENSOR), SARS-CoV-2 Antigen Rapid Test
* #1604 ^designation[0].language = #de-AT 
* #1604 ^designation[0].value = "Roche (SD BIOSENSOR), SARS-CoV-2 Antigen Rapid Test" 
* #1604 ^property[0].code = #Relationships 
* #1604 ^property[0].valueString = "$.t..ma" 
* #1604 ^property[1].code = #hints 
* #1604 ^property[1].valueString = "T^E^B" 
* #1610 "Hangzhou Clongene Biotech Co., Ltd, COVID-19 Antigen Rapid Test Casette"
* #1610 ^definition = Hangzhou Clongene Biotech Co., Ltd, COVID-19 Antigen Rapid Test Casette
* #1610 ^designation[0].language = #de-AT 
* #1610 ^designation[0].value = "Hangzhou Clongene Biotech Co., Ltd, COVID-19 Antigen Rapid Test Casette" 
* #1610 ^property[0].code = #Relationships 
* #1610 ^property[0].valueString = "$.t..ma" 
* #1610 ^property[1].code = #hints 
* #1610 ^property[1].valueString = "T^E^B" 
* #1618 "Artron Laboratories Inc., Artron COVID-19 Antigen Test"
* #1618 ^definition = Artron Laboratories Inc., Artron COVID-19 Antigen Test
* #1618 ^designation[0].language = #de-AT 
* #1618 ^designation[0].value = "Artron Laboratories Inc., Artron COVID-19 Antigen Test" 
* #1618 ^property[0].code = #Relationships 
* #1618 ^property[0].valueString = "$.t..ma" 
* #1618 ^property[1].code = #hints 
* #1618 ^property[1].valueString = "T^E^B" 
* #1647 "CALTH Inc., AllCheck COVID19 Ag"
* #1647 ^definition = CALTH Inc., AllCheck COVID19 Ag
* #1647 ^designation[0].language = #de-AT 
* #1647 ^designation[0].value = "CALTH Inc., AllCheck COVID19 Ag" 
* #1647 ^property[0].code = #Relationships 
* #1647 ^property[0].valueString = "$.t..ma" 
* #1647 ^property[1].code = #hints 
* #1647 ^property[1].valueString = "T^E^B" 
* #1654 "Asan Pharmaceutical CO., LTD, Asan Easy Test COVID-19 Ag"
* #1654 ^definition = Asan Pharmaceutical CO., LTD, Asan Easy Test COVID-19 Ag
* #1654 ^designation[0].language = #de-AT 
* #1654 ^designation[0].value = "Asan Pharmaceutical CO., LTD, Asan Easy Test COVID-19 Ag" 
* #1654 ^property[0].code = #Relationships 
* #1654 ^property[0].valueString = "$.t..ma" 
* #1654 ^property[1].code = #hints 
* #1654 ^property[1].valueString = "T^E^B" 
* #1689 "TÜRKLAB TIBBI MALZEMELER SAN. ve TIC. A.S., Covid-19 Ag Test"
* #1689 ^definition = TÜRKLAB TIBBI MALZEMELER SAN. ve TIC. A.S., Covid-19 Ag Test
* #1689 ^designation[0].language = #de-AT 
* #1689 ^designation[0].value = "TÜRKLAB TIBBI MALZEMELER SAN. ve TIC. A.S., Covid-19 Ag Test" 
* #1689 ^property[0].code = #Relationships 
* #1689 ^property[0].valueString = "$.t..ma" 
* #1689 ^property[1].code = #hints 
* #1689 ^property[1].valueString = "T^E^B" 
* #1691 "Chil T?bbi Malzeme Sanayi ve Ticaret Limited Sirketi, CHIL COVID-19 Antigen Rapid Test (Nasopharyngeal / Oropharyngeal Swab-Casette)"
* #1691 ^definition = Chil T?bbi Malzeme Sanayi ve Ticaret Limited Sirketi, CHIL COVID-19 Antigen Rapid Test (Nasopharyngeal / Oropharyngeal Swab-Casette)
* #1691 ^designation[0].language = #de-AT 
* #1691 ^designation[0].value = "Chil T?bbi Malzeme Sanayi ve Ticaret Limited Sirketi, CHIL COVID-19 Antigen Rapid Test (Nasopharyngeal / Oropharyngeal Swab-Casette)" 
* #1691 ^property[0].code = #Relationships 
* #1691 ^property[0].valueString = "$.t..ma" 
* #1691 ^property[1].code = #hints 
* #1691 ^property[1].valueString = "T^E^B" 
* #1722 "TÜRKLAB TIBBI MALZEMELER SAN. ve TIC. A.S., TOYO Covid-19 Ag Tes"
* #1722 ^definition = TÜRKLAB TIBBI MALZEMELER SAN. ve TIC. A.S., TOYO Covid-19 Ag Tes
* #1722 ^designation[0].language = #de-AT 
* #1722 ^designation[0].value = "TÜRKLAB TIBBI MALZEMELER SAN. ve TIC. A.S., TOYO Covid-19 Ag Tes" 
* #1722 ^property[0].code = #Relationships 
* #1722 ^property[0].valueString = "$.t..ma" 
* #1722 ^property[1].code = #hints 
* #1722 ^property[1].valueString = "T^E^B" 
* #1736 "Anhui Deep Blue Medical Technology Co. Ltd, COVID-19 (SARS-CoV-2) Antigen Test Kit (Colloidal Gold)"
* #1736 ^definition = Anhui Deep Blue Medical Technology Co. Ltd, COVID-19 (SARS-CoV-2) Antigen Test Kit (Colloidal Gold)
* #1736 ^designation[0].language = #de-AT 
* #1736 ^designation[0].value = "Anhui Deep Blue Medical Technology Co. Ltd, COVID-19 (SARS-CoV-2) Antigen Test Kit (Colloidal Gold)" 
* #1736 ^property[0].code = #Relationships 
* #1736 ^property[0].valueString = "$.t..ma" 
* #1736 ^property[1].code = #hints 
* #1736 ^property[1].valueString = "T^E^B" 
* #1739 "Eurobio Scientific, EBS SARS-CoV-2 Ag Rapid Test"
* #1739 ^definition = Eurobio Scientific, EBS SARS-CoV-2 Ag Rapid Test
* #1739 ^designation[0].language = #de-AT 
* #1739 ^designation[0].value = "Eurobio Scientific, EBS SARS-CoV-2 Ag Rapid Test" 
* #1739 ^property[0].code = #Relationships 
* #1739 ^property[0].valueString = "$.t..ma" 
* #1739 ^property[1].code = #hints 
* #1739 ^property[1].valueString = "T^E^B" 
* #1747 "Guangdong Hecin Scientific, Inc., 2019-nCoV Antigen Test Kit (colloidal gold method)"
* #1747 ^definition = Guangdong Hecin Scientific, Inc., 2019-nCoV Antigen Test Kit (colloidal gold method)
* #1747 ^designation[0].language = #de-AT 
* #1747 ^designation[0].value = "Guangdong Hecin Scientific, Inc., 2019-nCoV Antigen Test Kit (colloidal gold method)" 
* #1747 ^property[0].code = #Relationships 
* #1747 ^property[0].valueString = "$.t..ma" 
* #1747 ^property[1].code = #hints 
* #1747 ^property[1].valueString = "T^E^B" 
* #1751 "TÜRKLAB TIBBI MALZEMELER SAN. ve TIC. A.S., RAPIDAN TESTER Covid-19 Ag Test"
* #1751 ^definition = TÜRKLAB TIBBI MALZEMELER SAN. ve TIC. A.S., RAPIDAN TESTER Covid-19 Ag Test
* #1751 ^designation[0].language = #de-AT 
* #1751 ^designation[0].value = "TÜRKLAB TIBBI MALZEMELER SAN. ve TIC. A.S., RAPIDAN TESTER Covid-19 Ag Test" 
* #1751 ^property[0].code = #Relationships 
* #1751 ^property[0].valueString = "$.t..ma" 
* #1751 ^property[1].code = #hints 
* #1751 ^property[1].valueString = "T^E^B" 
* #1759 "Hubei Jinjian Biology Co., Ltd, SARS-CoV-2 Antigen Test Kit"
* #1759 ^definition = Hubei Jinjian Biology Co., Ltd, SARS-CoV-2 Antigen Test Kit
* #1759 ^designation[0].language = #de-AT 
* #1759 ^designation[0].value = "Hubei Jinjian Biology Co., Ltd, SARS-CoV-2 Antigen Test Kit" 
* #1759 ^property[0].code = #Relationships 
* #1759 ^property[0].valueString = "$.t..ma" 
* #1759 ^property[1].code = #hints 
* #1759 ^property[1].valueString = "T^E^B" 
* #1762 "Novatech, SARS-CoV-2 Antigen Rapid Test"
* #1762 ^definition = Novatech, SARS-CoV-2 Antigen Rapid Test
* #1762 ^designation[0].language = #de-AT 
* #1762 ^designation[0].value = "Novatech, SARS-CoV-2 Antigen Rapid Test" 
* #1762 ^property[0].code = #Relationships 
* #1762 ^property[0].valueString = "$.t..ma" 
* #1762 ^property[1].code = #hints 
* #1762 ^property[1].valueString = "T^E^B" 
* #1763 "Xiamen AmonMed Biotechnology Co., Ltd., COVID-19 Antigen Rapid Test Kit (Colloidal Gold)"
* #1763 ^definition = Xiamen AmonMed Biotechnology Co., Ltd., COVID-19 Antigen Rapid Test Kit (Colloidal Gold)
* #1763 ^designation[0].language = #de-AT 
* #1763 ^designation[0].value = "Xiamen AmonMed Biotechnology Co., Ltd., COVID-19 Antigen Rapid Test Kit (Colloidal Gold)  " 
* #1763 ^property[0].code = #Relationships 
* #1763 ^property[0].valueString = "$.t..ma" 
* #1763 ^property[1].code = #hints 
* #1763 ^property[1].valueString = "T^E^B" 
* #1764 "JOYSBIO (Tianjin) Biotechnology Co., Ltd., SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold immunochromatography)"
* #1764 ^definition = JOYSBIO (Tianjin) Biotechnology Co., Ltd., SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold immunochromatography)
* #1764 ^designation[0].language = #de-AT 
* #1764 ^designation[0].value = "JOYSBIO (Tianjin) Biotechnology Co., Ltd., SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold immunochromatography)" 
* #1764 ^property[0].code = #Relationships 
* #1764 ^property[0].valueString = "$.t..ma" 
* #1764 ^property[1].code = #hints 
* #1764 ^property[1].valueString = "T^E^B" 
* #1767 "Healgen Scientific Limited, Coronavirus Ag Rapid Test Cassette (Swab)"
* #1767 ^definition = Healgen Scientific Limited, Coronavirus Ag Rapid Test Cassette (Swab)
* #1767 ^designation[0].language = #de-AT 
* #1767 ^designation[0].value = "Healgen Scientific Limited, Coronavirus Ag Rapid Test Cassette (Swab)" 
* #1767 ^property[0].code = #Relationships 
* #1767 ^property[0].valueString = "$.t..ma" 
* #1767 ^property[1].code = #hints 
* #1767 ^property[1].valueString = "T^E^B" 
* #1768 "Shenzhen Watmind Medical Co., Ltd, SARS-CoV-2 Ag Diagnostic Test Kit (Immuno-fluorescence)"
* #1768 ^definition = Shenzhen Watmind Medical Co., Ltd, SARS-CoV-2 Ag Diagnostic Test Kit (Immuno-fluorescence)
* #1768 ^designation[0].language = #de-AT 
* #1768 ^designation[0].value = "Shenzhen Watmind Medical Co., Ltd, SARS-CoV-2 Ag Diagnostic Test Kit (Immuno-fluorescence)" 
* #1768 ^property[0].code = #Relationships 
* #1768 ^property[0].valueString = "$.t..ma" 
* #1768 ^property[1].code = #hints 
* #1768 ^property[1].valueString = "T^E^B" 
* #1769 "Shenzhen Watmind Medical Co., Ltd, SARS-CoV-2 Ag Diagnostic Test Kit (Colloidal Gold)"
* #1769 ^definition = Shenzhen Watmind Medical Co., Ltd, SARS-CoV-2 Ag Diagnostic Test Kit (Colloidal Gold)
* #1769 ^designation[0].language = #de-AT 
* #1769 ^designation[0].value = "Shenzhen Watmind Medical Co., Ltd, SARS-CoV-2 Ag Diagnostic Test Kit (Colloidal Gold)" 
* #1769 ^property[0].code = #Relationships 
* #1769 ^property[0].valueString = "$.t..ma" 
* #1769 ^property[1].code = #hints 
* #1769 ^property[1].valueString = "T^E^B" 
* #1773 "Wuhan Life Origin Biotech Joint Stock Co., Ltd., SARS-CoV-2 Antigen Assay Kit (Immunochromatography)"
* #1773 ^definition = Wuhan Life Origin Biotech Joint Stock Co., Ltd., SARS-CoV-2 Antigen Assay Kit (Immunochromatography)
* #1773 ^designation[0].language = #de-AT 
* #1773 ^designation[0].value = "Wuhan Life Origin Biotech Joint Stock Co., Ltd., SARS-CoV-2 Antigen Assay Kit (Immunochromatography)" 
* #1773 ^property[0].code = #Relationships 
* #1773 ^property[0].valueString = "$.t..ma" 
* #1773 ^property[1].code = #hints 
* #1773 ^property[1].valueString = "T^E^B" 
* #1775 "MEXACARE GmbH, MEXACARE COVID-19 Antigen Rapid Test"
* #1775 ^definition = MEXACARE GmbH, MEXACARE COVID-19 Antigen Rapid Test
* #1775 ^designation[0].language = #de-AT 
* #1775 ^designation[0].value = "MEXACARE GmbH, MEXACARE COVID-19 Antigen Rapid Test" 
* #1775 ^property[0].code = #Relationships 
* #1775 ^property[0].valueString = "$.t..ma" 
* #1775 ^property[1].code = #hints 
* #1775 ^property[1].valueString = "T^E^B" 
* #1778 "Beijing Kewei Clinical Diagnostic Reagent Inc, COVID19 Antigen Rapid Test Kit"
* #1778 ^definition = Beijing Kewei Clinical Diagnostic Reagent Inc, COVID19 Antigen Rapid Test Kit
* #1778 ^designation[0].language = #de-AT 
* #1778 ^designation[0].value = "Beijing Kewei Clinical Diagnostic Reagent Inc, COVID19 Antigen Rapid Test Kit" 
* #1778 ^property[0].code = #Relationships 
* #1778 ^property[0].valueString = "$.t..ma" 
* #1778 ^property[1].code = #hints 
* #1778 ^property[1].valueString = "T^E^B" 
* #1780 "Spring Healthcare Services AG, SARS-Cov-2 Antigen Rapid Test Cassette (swab)"
* #1780 ^definition = Spring Healthcare Services AG, SARS-Cov-2 Antigen Rapid Test Cassette (swab)
* #1780 ^designation[0].language = #de-AT 
* #1780 ^designation[0].value = "Spring Healthcare Services AG, SARS-Cov-2 Antigen Rapid Test Cassette (swab)" 
* #1780 ^property[0].code = #Relationships 
* #1780 ^property[0].valueString = "$.t..ma" 
* #1780 ^property[1].code = #hints 
* #1780 ^property[1].valueString = "T^E^B" 
* #1783 "InTec PRODUCTS, INC, Rapid SARS-CoV-2 Antigen Test (nasopharyngeal/nasal specimen)"
* #1783 ^definition = InTec PRODUCTS, INC, Rapid SARS-CoV-2 Antigen Test (nasopharyngeal/nasal specimen)
* #1783 ^designation[0].language = #de-AT 
* #1783 ^designation[0].value = "InTec PRODUCTS, INC, Rapid SARS-CoV-2 Antigen Test (nasopharyngeal/nasal specimen)" 
* #1783 ^property[0].code = #Relationships 
* #1783 ^property[0].valueString = "$.t..ma" 
* #1783 ^property[1].code = #hints 
* #1783 ^property[1].valueString = "T^E^B" 
* #1791 "Immunospark s.r.l., Rapid SARS-Cov2 Antigen Test"
* #1791 ^definition = Immunospark s.r.l., Rapid SARS-Cov2 Antigen Test
* #1791 ^designation[0].language = #de-AT 
* #1791 ^designation[0].value = "Immunospark s.r.l., Rapid SARS-Cov2 Antigen Test" 
* #1791 ^property[0].code = #Relationships 
* #1791 ^property[0].valueString = "$.t..ma" 
* #1791 ^property[1].code = #hints 
* #1791 ^property[1].valueString = "T^E^B" 
* #1800 "Avalun, Ksmart SARS-COV2 Antigen Rapid Test"
* #1800 ^definition = Avalun, Ksmart SARS-COV2 Antigen Rapid Test
* #1800 ^designation[0].language = #de-AT 
* #1800 ^designation[0].value = "Avalun, Ksmart SARS-COV2 Antigen Rapid Test" 
* #1800 ^property[0].code = #Relationships 
* #1800 ^property[0].valueString = "$.t..ma" 
* #1800 ^property[1].code = #hints 
* #1800 ^property[1].valueString = "T^E^B" 
* #1801 "Innova Medical Group.Inc, Innova SARS-CoV-2 Antigen Rapid Qualitative Test"
* #1801 ^definition = Innova Medical Group.Inc, Innova SARS-CoV-2 Antigen Rapid Qualitative Test
* #1801 ^designation[0].language = #de-AT 
* #1801 ^designation[0].value = "Innova Medical Group.Inc, Innova SARS-CoV-2 Antigen Rapid Qualitative Test" 
* #1801 ^property[0].code = #Relationships 
* #1801 ^property[0].valueString = "$.t..ma" 
* #1801 ^property[1].code = #hints 
* #1801 ^property[1].valueString = "T^E^B" 
* #1813 "Shenzhen Kisshealth Biotechnology Co., Ltd, SARS-CoV-2 Antigen Test Kit (GICA)"
* #1813 ^definition = Shenzhen Kisshealth Biotechnology Co., Ltd, SARS-CoV-2 Antigen Test Kit (GICA)
* #1813 ^designation[0].language = #de-AT 
* #1813 ^designation[0].value = "Shenzhen Kisshealth Biotechnology Co., Ltd, SARS-CoV-2 Antigen Test Kit (GICA)" 
* #1813 ^property[0].code = #Relationships 
* #1813 ^property[0].valueString = "$.t..ma" 
* #1813 ^property[1].code = #hints 
* #1813 ^property[1].valueString = "T^E^B" 
* #1815 "Anhui DeepBlue Medical Technology Co. Ltd, COVID-19 (SARS-CoV-2) Antigen Test Kit (Colloidal Gold) ? Nasal swab"
* #1815 ^definition = Anhui DeepBlue Medical Technology Co. Ltd, COVID-19 (SARS-CoV-2) Antigen Test Kit (Colloidal Gold) ? Nasal swab
* #1815 ^designation[0].language = #de-AT 
* #1815 ^designation[0].value = "Anhui DeepBlue Medical Technology Co. Ltd, COVID-19 (SARS-CoV-2) Antigen Test Kit (Colloidal Gold) ? Nasal swab" 
* #1815 ^property[0].code = #Relationships 
* #1815 ^property[0].valueString = "$.t..ma" 
* #1815 ^property[1].code = #hints 
* #1815 ^property[1].valueString = "T^E^B" 
* #1820 "Getein Biotech, Inc, SARS-CoV-2 Antigen (Colloidal Gold)"
* #1820 ^definition = Getein Biotech, Inc, SARS-CoV-2 Antigen (Colloidal Gold)
* #1820 ^designation[0].language = #de-AT 
* #1820 ^designation[0].value = "Getein Biotech, Inc, SARS-CoV-2 Antigen (Colloidal Gold)" 
* #1820 ^property[0].code = #Relationships 
* #1820 ^property[0].valueString = "$.t..ma" 
* #1820 ^property[1].code = #hints 
* #1820 ^property[1].valueString = "T^E^B" 
* #1822 "Anbio (Xiamen) Biotechnology Co., Ltd., Rapid COVID-19 Antigen-Test (colloidal Gold)"
* #1822 ^definition = Anbio (Xiamen) Biotechnology Co., Ltd., Rapid COVID-19 Antigen-Test (colloidal Gold)
* #1822 ^designation[0].language = #de-AT 
* #1822 ^designation[0].value = "Anbio (Xiamen) Biotechnology Co., Ltd., Rapid COVID-19 Antigen-Test (colloidal Gold)" 
* #1822 ^property[0].code = #Relationships 
* #1822 ^property[0].valueString = "$.t..ma" 
* #1822 ^property[1].code = #hints 
* #1822 ^property[1].valueString = "T^E^B" 
* #1833 "AAZ-LMB, COVID-VIRO Rapid antigen test COVID-19"
* #1833 ^definition = AAZ-LMB, COVID-VIRO Rapid antigen test COVID-19
* #1833 ^designation[0].language = #de-AT 
* #1833 ^designation[0].value = "AAZ-LMB, COVID-VIRO Rapid antigen test COVID-19" 
* #1833 ^property[0].code = #Relationships 
* #1833 ^property[0].valueString = "$.t..ma" 
* #1833 ^property[1].code = #hints 
* #1833 ^property[1].valueString = "T^E^B" 
* #1855 "GA Generic Assays GmbH, GA CoV-2 Antigen Rapid Test"
* #1855 ^definition = GA Generic Assays GmbH, GA CoV-2 Antigen Rapid Test
* #1855 ^designation[0].language = #de-AT 
* #1855 ^designation[0].value = "GA Generic Assays GmbH, GA CoV-2 Antigen Rapid Test" 
* #1855 ^property[0].code = #Relationships 
* #1855 ^property[0].valueString = "$.t..ma" 
* #1855 ^property[1].code = #hints 
* #1855 ^property[1].valueString = "T^E^B" 
* #1865 "ACON Biotech(Hangzhou) Co., Ltd., Flowflex SARS-CoV-2 Antigen Rapid Test (Nasal/Saliva)"
* #1865 ^definition = ACON Biotech(Hangzhou) Co., Ltd., Flowflex SARS-CoV-2 Antigen Rapid Test (Nasal/Saliva)
* #1865 ^designation[0].language = #de-AT 
* #1865 ^designation[0].value = "ACON Biotech(Hangzhou) Co., Ltd., Flowflex SARS-CoV-2 Antigen Rapid Test (Nasal/Saliva)" 
* #1865 ^property[0].code = #Relationships 
* #1865 ^property[0].valueString = "$.t..ma" 
* #1865 ^property[1].code = #hints 
* #1865 ^property[1].valueString = "T^E^B" 
* #1870 "Beijing Hotgen Biotech Co., Ltd., Novel Coronavirus 2019-nCoV Antigen Test (Colloidal Gold)"
* #1870 ^definition = Beijing Hotgen Biotech Co., Ltd., Novel Coronavirus 2019-nCoV Antigen Test (Colloidal Gold)
* #1870 ^designation[0].language = #de-AT 
* #1870 ^designation[0].value = "Beijing Hotgen Biotech Co., Ltd., Novel Coronavirus 2019-nCoV Antigen Test (Colloidal Gold)" 
* #1870 ^property[0].code = #Relationships 
* #1870 ^property[0].valueString = "$.t..ma" 
* #1870 ^property[1].code = #hints 
* #1870 ^property[1].valueString = "T^E^B" 
* #1876 "Hangzhou Biotest Biotech Co., Ltd, COVID-19 Antigen Rapid Test Cassette(Nasal Swab)"
* #1876 ^definition = Hangzhou Biotest Biotech Co., Ltd, COVID-19 Antigen Rapid Test Cassette(Nasal Swab)
* #1876 ^designation[0].language = #de-AT 
* #1876 ^designation[0].value = "Hangzhou Biotest Biotech Co., Ltd, COVID-19 Antigen Rapid Test Cassette(Nasal Swab)" 
* #1876 ^property[0].code = #Relationships 
* #1876 ^property[0].valueString = "$.t..ma" 
* #1876 ^property[1].code = #hints 
* #1876 ^property[1].valueString = "T^E^B" 
* #1880 "NG Biotech, Ninonasal"
* #1880 ^definition = NG Biotech, Ninonasal
* #1880 ^designation[0].language = #de-AT 
* #1880 ^designation[0].value = "NG Biotech, Ninonasal" 
* #1880 ^property[0].code = #Relationships 
* #1880 ^property[0].valueString = "$.t..ma" 
* #1880 ^property[1].code = #hints 
* #1880 ^property[1].valueString = "T^E^B" 
* #1899 "Jiangsu Konsung Bio-Medical Science and Technology Co., H+W_COVID-19 Antigen Rapid Test Kit (Colloidal Gold)"
* #1899 ^definition = Jiangsu Konsung Bio-Medical Science and Technology Co., H+W_COVID-19 Antigen Rapid Test Kit (Colloidal Gold)
* #1899 ^designation[0].language = #de-AT 
* #1899 ^designation[0].value = "Jiangsu Konsung Bio-Medical Science and Technology Co., H+W_COVID-19 Antigen Rapid Test Kit (Colloidal Gold)" 
* #1899 ^property[0].code = #Relationships 
* #1899 ^property[0].valueString = "$.t..ma" 
* #1899 ^property[1].code = #hints 
* #1899 ^property[1].valueString = "T^E^B" 
* #1902 "Zhuhai Encode Medical Engineering Co.,Ltd, ENCODE SARS-COV-2 Antigen Rapid Test Device"
* #1902 ^definition = Zhuhai Encode Medical Engineering Co.,Ltd, ENCODE SARS-COV-2 Antigen Rapid Test Device
* #1902 ^designation[0].language = #de-AT 
* #1902 ^designation[0].value = "Zhuhai Encode Medical Engineering Co.,Ltd, ENCODE SARS-COV-2 Antigen Rapid Test Device" 
* #1902 ^property[0].code = #Relationships 
* #1902 ^property[0].valueString = "$.t..ma" 
* #1902 ^property[1].code = #hints 
* #1902 ^property[1].valueString = "T^E^B" 
* #1920 "Jiangsu Diagnostics Biotechnology Co., Ltd, COVID-19 Antigen Rapid Test Cassette (Colloidal Gold)"
* #1920 ^definition = Jiangsu Diagnostics Biotechnology Co., Ltd, COVID-19 Antigen Rapid Test Cassette (Colloidal Gold)
* #1920 ^designation[0].language = #de-AT 
* #1920 ^designation[0].value = "Jiangsu Diagnostics Biotechnology Co., Ltd, COVID-19 Antigen Rapid Test Cassette (Colloidal Gold)" 
* #1920 ^property[0].code = #Relationships 
* #1920 ^property[0].valueString = "$.t..ma" 
* #1920 ^property[1].code = #hints 
* #1920 ^property[1].valueString = "T^E^B" 
* #1926 "ARISTA Biotech Pte.LTD., ARISTA COVID-19 Antigen Rapid Test"
* #1926 ^definition = ARISTA Biotech Pte.LTD., ARISTA COVID-19 Antigen Rapid Test
* #1926 ^designation[0].language = #de-AT 
* #1926 ^designation[0].value = "ARISTA Biotech Pte.LTD., ARISTA COVID-19 Antigen Rapid Test" 
* #1926 ^property[0].code = #Relationships 
* #1926 ^property[0].valueString = "$.t..ma" 
* #1926 ^property[1].code = #hints 
* #1926 ^property[1].valueString = "T^E^B" 
* #1929 "Hoyotek Biomedical Co.,Ltd., Corona Virus (COVID-19) Antigen Rapid Test (Colloidal Gold)"
* #1929 ^definition = Hoyotek Biomedical Co.,Ltd., Corona Virus (COVID-19) Antigen Rapid Test (Colloidal Gold)
* #1929 ^designation[0].language = #de-AT 
* #1929 ^designation[0].value = "Hoyotek Biomedical Co.,Ltd., Corona Virus (COVID-19) Antigen Rapid Test (Colloidal Gold)" 
* #1929 ^property[0].code = #Relationships 
* #1929 ^property[0].valueString = "$.t..ma" 
* #1929 ^property[1].code = #hints 
* #1929 ^property[1].valueString = "T^E^B" 
* #1942 "Surge Medical Inc., COVID-19 Antigen Test Kit"
* #1942 ^definition = Surge Medical Inc., COVID-19 Antigen Test Kit
* #1942 ^designation[0].language = #de-AT 
* #1942 ^designation[0].value = "Surge Medical Inc., COVID-19 Antigen Test Kit" 
* #1942 ^property[0].code = #Relationships 
* #1942 ^property[0].valueString = "$.t..ma" 
* #1942 ^property[1].code = #hints 
* #1942 ^property[1].valueString = "T^E^B" 
* #1945 "Hangzhou Sejoy Electronics & Instruments Co.Ltd, SARS-CoV-2 Antigen Rapid Test Cassette"
* #1945 ^definition = Hangzhou Sejoy Electronics & Instruments Co.Ltd, SARS-CoV-2 Antigen Rapid Test Cassette
* #1945 ^designation[0].language = #de-AT 
* #1945 ^designation[0].value = "Hangzhou Sejoy Electronics & Instruments Co.Ltd, SARS-CoV-2 Antigen Rapid Test Cassette" 
* #1945 ^property[0].code = #Relationships 
* #1945 ^property[0].valueString = "$.t..ma" 
* #1945 ^property[1].code = #hints 
* #1945 ^property[1].valueString = "T^E^B" 
* #1952 "Hangzhou Sejoy Electronics & Instruments Co.Ltd, SARS-CoV-2 Antigen Rapid Test Cassette (nasal, nasopharyngeal, oropharyngeal, saliva)"
* #1952 ^definition = Hangzhou Sejoy Electronics & Instruments Co.Ltd, SARS-CoV-2 Antigen Rapid Test Cassette (nasal, nasopharyngeal, oropharyngeal, saliva)
* #1952 ^designation[0].language = #de-AT 
* #1952 ^designation[0].value = "Hangzhou Sejoy Electronics & Instruments Co.Ltd, SARS-CoV-2 Antigen Rapid Test Cassette (nasal, nasopharyngeal, oropharyngeal, saliva)" 
* #1952 ^property[0].code = #Relationships 
* #1952 ^property[0].valueString = "$.t..ma" 
* #1952 ^property[1].code = #hints 
* #1952 ^property[1].valueString = "T^E^B" 
* #1957 "Zhuhai Lituo Biotechnology Co., Ltd., COVID-19 Antigen Detection Kit (Colloidal Gold)"
* #1957 ^definition = Zhuhai Lituo Biotechnology Co., Ltd., COVID-19 Antigen Detection Kit (Colloidal Gold)
* #1957 ^designation[0].language = #de-AT 
* #1957 ^designation[0].value = "Zhuhai Lituo Biotechnology Co., Ltd., COVID-19 Antigen Detection Kit (Colloidal Gold)" 
* #1957 ^property[0].code = #Relationships 
* #1957 ^property[0].valueString = "$.t..ma" 
* #1957 ^property[1].code = #hints 
* #1957 ^property[1].valueString = "T^E^B" 
* #1967 "Shenzhen Microprofit Biotech Co., Ltd, SARS-CoV-2 Antigen Test Kit (Colloidal Gold Chromatographic Immunoassay)"
* #1967 ^definition = Shenzhen Microprofit Biotech Co., Ltd, SARS-CoV-2 Antigen Test Kit (Colloidal Gold Chromatographic Immunoassay)
* #1967 ^designation[0].language = #de-AT 
* #1967 ^designation[0].value = "Shenzhen Microprofit Biotech Co., Ltd, SARS-CoV-2 Antigen Test Kit (Colloidal Gold Chromatographic Immunoassay)" 
* #1967 ^property[0].code = #Relationships 
* #1967 ^property[0].valueString = "$.t..ma" 
* #1967 ^property[1].code = #hints 
* #1967 ^property[1].valueString = "T^E^B" 
* #1988 "Inzek International Trading B.V., Biozek covid-19 Antigen Rapidtest BCOV-502"
* #1988 ^definition = Inzek International Trading B.V., Biozek covid-19 Antigen Rapidtest BCOV-502
* #1988 ^designation[0].language = #de-AT 
* #1988 ^designation[0].value = "Inzek International Trading B.V., Biozek covid-19 Antigen Rapidtest BCOV-502" 
* #1988 ^property[0].code = #Relationships 
* #1988 ^property[0].valueString = "$.t..ma" 
* #1988 ^property[1].code = #hints 
* #1988 ^property[1].valueString = "T^E^B" 
* #1989 "Boditech Med Inc, AFIAS COVID-19 Ag"
* #1989 ^definition = Boditech Med Inc, AFIAS COVID-19 Ag
* #1989 ^designation[0].language = #de-AT 
* #1989 ^designation[0].value = "Boditech Med Inc, AFIAS COVID-19 Ag" 
* #1989 ^property[0].code = #Relationships 
* #1989 ^property[0].valueString = "$.t..ma" 
* #1989 ^property[1].code = #hints 
* #1989 ^property[1].valueString = "T^E^B" 
* #2006 "Jiangsu Medomics medical technology Co.,Ltd., SARS-CoV-2 antigen Test Kit (LFIA)"
* #2006 ^definition = Jiangsu Medomics medical technology Co.,Ltd., SARS-CoV-2 antigen Test Kit (LFIA)
* #2006 ^designation[0].language = #de-AT 
* #2006 ^designation[0].value = "Jiangsu Medomics medical technology Co.,Ltd., SARS-CoV-2 antigen Test Kit (LFIA)" 
* #2006 ^property[0].code = #Relationships 
* #2006 ^property[0].valueString = "$.t..ma" 
* #2006 ^property[1].code = #hints 
* #2006 ^property[1].valueString = "T^E^B" 
* #2012 "Genrui Biotech Inc, SARS-CoV-2 Antigen Test Kit (Colloidal Gold)"
* #2012 ^definition = Genrui Biotech Inc, SARS-CoV-2 Antigen Test Kit (Colloidal Gold)
* #2012 ^designation[0].language = #de-AT 
* #2012 ^designation[0].value = "Genrui Biotech Inc, SARS-CoV-2 Antigen Test Kit (Colloidal Gold)" 
* #2012 ^property[0].code = #Relationships 
* #2012 ^property[0].valueString = "$.t..ma" 
* #2012 ^property[1].code = #hints 
* #2012 ^property[1].valueString = "T^E^B" 
* #2013 "Biotical Health S.L.U.BIOTICAL HEALTH S.L.U, biotical SARS-CoV-2 Ag Card"
* #2013 ^definition = Biotical Health S.L.U.BIOTICAL HEALTH S.L.U, biotical SARS-CoV-2 Ag Card
* #2013 ^designation[0].language = #de-AT 
* #2013 ^designation[0].value = "Biotical Health S.L.U.BIOTICAL HEALTH S.L.U, biotical SARS-CoV-2 Ag Card" 
* #2013 ^property[0].code = #Relationships 
* #2013 ^property[0].valueString = "$.t..ma" 
* #2013 ^property[1].code = #hints 
* #2013 ^property[1].valueString = "T^E^B" 
* #2017 "Shenzen Ultra-Diagnostics Biotec Co., SARS-CoV-2 Antigen test Kit (colloidal gold)"
* #2017 ^definition = Shenzen Ultra-Diagnostics Biotec Co., SARS-CoV-2 Antigen test Kit (colloidal gold)
* #2017 ^designation[0].language = #de-AT 
* #2017 ^designation[0].value = "Shenzen Ultra-Diagnostics Biotec Co., SARS-CoV-2 Antigen test Kit (colloidal gold)" 
* #2017 ^property[0].code = #Relationships 
* #2017 ^property[0].valueString = "$.t..ma" 
* #2017 ^property[1].code = #hints 
* #2017 ^property[1].valueString = "T^E^B" 
* #2026 "Shenzhen Reagent Technology Co.,Ltd, SARS-CoV-2 antigen IVD kit SWAB"
* #2026 ^definition = Shenzhen Reagent Technology Co.,Ltd, SARS-CoV-2 antigen IVD kit SWAB
* #2026 ^designation[0].language = #de-AT 
* #2026 ^designation[0].value = "Shenzhen Reagent Technology Co.,Ltd, SARS-CoV-2 antigen IVD kit SWAB" 
* #2026 ^property[0].code = #Relationships 
* #2026 ^property[0].valueString = "$.t..ma" 
* #2026 ^property[1].code = #hints 
* #2026 ^property[1].valueString = "T^E^B" 
* #2029 "Merlin Biomedical (Xiamen) Co., Ltd., SARS-CoV-2 Antigen Rapid Test Cassette"
* #2029 ^definition = Merlin Biomedical (Xiamen) Co., Ltd., SARS-CoV-2 Antigen Rapid Test Cassette
* #2029 ^designation[0].language = #de-AT 
* #2029 ^designation[0].value = "Merlin Biomedical (Xiamen) Co., Ltd., SARS-CoV-2 Antigen Rapid Test Cassette" 
* #2029 ^property[0].code = #Relationships 
* #2029 ^property[0].valueString = "$.t..ma" 
* #2029 ^property[1].code = #hints 
* #2029 ^property[1].valueString = "T^E^B" 
* #2031 "BIO-RAD, CORONAVIRUS AG RAPID TEST CASSETTE"
* #2031 ^definition = BIO-RAD, CORONAVIRUS AG RAPID TEST CASSETTE
* #2031 ^designation[0].language = #de-AT 
* #2031 ^designation[0].value = "BIO-RAD, CORONAVIRUS AG RAPID TEST CASSETTE" 
* #2031 ^property[0].code = #Relationships 
* #2031 ^property[0].valueString = "$.t..ma" 
* #2031 ^property[1].code = #hints 
* #2031 ^property[1].valueString = "T^E^B" 
* #2035 "BioMaxima SA, SARS-CoV-2 Ag Rapid Test"
* #2035 ^definition = BioMaxima SA, SARS-CoV-2 Ag Rapid Test
* #2035 ^designation[0].language = #de-AT 
* #2035 ^designation[0].value = "BioMaxima SA, SARS-CoV-2 Ag Rapid Test" 
* #2035 ^property[0].code = #Relationships 
* #2035 ^property[0].valueString = "$.t..ma" 
* #2035 ^property[1].code = #hints 
* #2035 ^property[1].valueString = "T^E^B" 
* #2038 "Koch Biotechnology (Beijing) Co., Ltd, COVID-19 Antigen Rapid Test Kit"
* #2038 ^definition = Koch Biotechnology (Beijing) Co., Ltd, COVID-19 Antigen Rapid Test Kit
* #2038 ^designation[0].language = #de-AT 
* #2038 ^designation[0].value = "Koch Biotechnology (Beijing) Co., Ltd, COVID-19 Antigen Rapid Test Kit " 
* #2038 ^property[0].code = #Relationships 
* #2038 ^property[0].valueString = "$.t..ma" 
* #2038 ^property[1].code = #hints 
* #2038 ^property[1].valueString = "T^E^B" 
* #2052 "SD BIOSENSOR Inc. (manufacturer), STANDARD Q COVID-19 Ag Test Nasal"
* #2052 ^definition = SD BIOSENSOR Inc. (manufacturer), STANDARD Q COVID-19 Ag Test Nasal
* #2052 ^designation[0].language = #de-AT 
* #2052 ^designation[0].value = "SD BIOSENSOR Inc. (manufacturer), STANDARD Q COVID-19 Ag Test Nasal" 
* #2052 ^property[0].code = #Relationships 
* #2052 ^property[0].valueString = "$.t..ma" 
* #2052 ^property[1].code = #hints 
* #2052 ^property[1].valueString = "T^E^B" 
* #2067 "BIOTEKE CORPORATION (WUXI) CO., LTD, SARS-CoV-2 Antigen Test Kit (colloidal gold method)"
* #2067 ^definition = BIOTEKE CORPORATION (WUXI) CO., LTD, SARS-CoV-2 Antigen Test Kit (colloidal gold method)
* #2067 ^designation[0].language = #de-AT 
* #2067 ^designation[0].value = "BIOTEKE CORPORATION (WUXI) CO., LTD, SARS-CoV-2 Antigen Test Kit (colloidal gold method)" 
* #2067 ^property[0].code = #Relationships 
* #2067 ^property[0].valueString = "$.t..ma" 
* #2067 ^property[1].code = #hints 
* #2067 ^property[1].valueString = "T^E^B" 
* #2072 "Beijing Jinwofu Bioengineering Technology Co.,Ltd., Novel Coronavirus (SARS-CoV-2) Antigen Rapid Test Kit"
* #2072 ^definition = Beijing Jinwofu Bioengineering Technology Co.,Ltd., Novel Coronavirus (SARS-CoV-2) Antigen Rapid Test Kit
* #2072 ^designation[0].language = #de-AT 
* #2072 ^designation[0].value = "Beijing Jinwofu Bioengineering Technology Co.,Ltd., Novel Coronavirus (SARS-CoV-2) Antigen Rapid Test Kit" 
* #2072 ^property[0].code = #Relationships 
* #2072 ^property[0].valueString = "$.t..ma" 
* #2072 ^property[1].code = #hints 
* #2072 ^property[1].valueString = "T^E^B" 
* #2074 "Triplex International Biosciences Co., Ltd, SARS-CoV-2 Antigen Rapid Test Kit"
* #2074 ^definition = Triplex International Biosciences Co., Ltd, SARS-CoV-2 Antigen Rapid Test Kit
* #2074 ^designation[0].language = #de-AT 
* #2074 ^designation[0].value = "Triplex International Biosciences Co., Ltd, SARS-CoV-2 Antigen Rapid Test Kit" 
* #2074 ^property[0].code = #Relationships 
* #2074 ^property[0].valueString = "$.t..ma" 
* #2074 ^property[1].code = #hints 
* #2074 ^property[1].valueString = "T^E^B" 
* #2078 "ArcDia International Oy Ltd, mariPOC Respi+"
* #2078 ^definition = ArcDia International Oy Ltd, mariPOC Respi+
* #2078 ^designation[0].language = #de-AT 
* #2078 ^designation[0].value = "ArcDia International Oy Ltd, mariPOC Respi+" 
* #2078 ^property[0].code = #Relationships 
* #2078 ^property[0].valueString = "$.t..ma" 
* #2078 ^property[1].code = #hints 
* #2078 ^property[1].valueString = "T^E^B" 
* #2079 "ArcDia International Oy Ltd, mariPOC Quick Flu+"
* #2079 ^definition = ArcDia International Oy Ltd, mariPOC Quick Flu+
* #2079 ^designation[0].language = #de-AT 
* #2079 ^designation[0].value = "ArcDia International Oy Ltd, mariPOC Quick Flu+" 
* #2079 ^property[0].code = #Relationships 
* #2079 ^property[0].valueString = "$.t..ma" 
* #2079 ^property[1].code = #hints 
* #2079 ^property[1].valueString = "T^E^B" 
* #2089 "Anhui Formaster Biosci Co., Ltd., New Coronavirus (COVID-19) Antigen Rapid Test"
* #2089 ^definition = Anhui Formaster Biosci Co., Ltd., New Coronavirus (COVID-19) Antigen Rapid Test
* #2089 ^designation[0].language = #de-AT 
* #2089 ^designation[0].value = "Anhui Formaster Biosci Co., Ltd., New Coronavirus (COVID-19) Antigen Rapid Test" 
* #2089 ^property[0].code = #Relationships 
* #2089 ^property[0].valueString = "$.t..ma" 
* #2089 ^property[1].code = #hints 
* #2089 ^property[1].valueString = "T^E^B" 
* #2090 "Wuhan UNscience Biotechnology Co., Ltd., SARS-CoV-2 Antigen Rapid Test Kit"
* #2090 ^definition = Wuhan UNscience Biotechnology Co., Ltd., SARS-CoV-2 Antigen Rapid Test Kit
* #2090 ^designation[0].language = #de-AT 
* #2090 ^designation[0].value = "Wuhan UNscience Biotechnology Co., Ltd., SARS-CoV-2 Antigen Rapid Test Kit" 
* #2090 ^property[0].code = #Relationships 
* #2090 ^property[0].valueString = "$.t..ma" 
* #2090 ^property[1].code = #hints 
* #2090 ^property[1].valueString = "T^E^B" 
* #2097 "Sansure Biotech Inc, SARS-CoV-2 Rapid Antigen Test (Colloidal Gold Method)"
* #2097 ^definition = Sansure Biotech Inc, SARS-CoV-2 Rapid Antigen Test (Colloidal Gold Method)
* #2097 ^designation[0].language = #de-AT 
* #2097 ^designation[0].value = "Sansure Biotech Inc, SARS-CoV-2 Rapid Antigen Test (Colloidal Gold Method)" 
* #2097 ^property[0].code = #Relationships 
* #2097 ^property[0].valueString = "$.t..ma" 
* #2097 ^property[1].code = #hints 
* #2097 ^property[1].valueString = "T^E^B" 
* #2098 "Wuhan EasyDiagnosis Biomedicine Co., Ltd., COVID-19  (SARS-CoV-2) Antigen-Test kit"
* #2098 ^definition = Wuhan EasyDiagnosis Biomedicine Co., Ltd., COVID-19  (SARS-CoV-2) Antigen-Test kit
* #2098 ^designation[0].language = #de-AT 
* #2098 ^designation[0].value = "Wuhan EasyDiagnosis Biomedicine Co., Ltd., COVID-19  (SARS-CoV-2) Antigen-Test kit " 
* #2098 ^property[0].code = #Relationships 
* #2098 ^property[0].valueString = "$.t..ma" 
* #2098 ^property[1].code = #hints 
* #2098 ^property[1].valueString = "T^E^B" 
* #2100 "VivaChek Biotech (Hangzhou) Co., Ltd, China, Verino Pro SARS CoV 2 Ag Rapid Test"
* #2100 ^definition = VivaChek Biotech (Hangzhou) Co., Ltd, China, Verino Pro SARS CoV 2 Ag Rapid Test
* #2100 ^designation[0].language = #de-AT 
* #2100 ^designation[0].value = "VivaChek Biotech (Hangzhou) Co., Ltd, China, Verino Pro SARS CoV 2 Ag Rapid Test" 
* #2100 ^property[0].code = #Relationships 
* #2100 ^property[0].valueString = "$.t..ma" 
* #2100 ^property[1].code = #hints 
* #2100 ^property[1].valueString = "T^E^B" 
* #2101 "AXIOM Gesellschaft für Diagnostica und Biochemica mbH, COVID-19 Antigen Rapid Test"
* #2101 ^definition = AXIOM Gesellschaft für Diagnostica und Biochemica mbH, COVID-19 Antigen Rapid Test
* #2101 ^designation[0].language = #de-AT 
* #2101 ^designation[0].value = "AXIOM Gesellschaft für Diagnostica und Biochemica mbH, COVID-19 Antigen Rapid Test" 
* #2101 ^property[0].code = #Relationships 
* #2101 ^property[0].valueString = "$.t..ma" 
* #2101 ^property[1].code = #hints 
* #2101 ^property[1].valueString = "T^E^B" 
* #2107 "Jiangsu Bioperfectus Technologies Co., Ltd., Novel Corona Virus (SARS-CoV-2) Ag Rapid Test Kit"
* #2107 ^definition = Jiangsu Bioperfectus Technologies Co., Ltd., Novel Corona Virus (SARS-CoV-2) Ag Rapid Test Kit
* #2107 ^designation[0].language = #de-AT 
* #2107 ^designation[0].value = "Jiangsu Bioperfectus Technologies Co., Ltd., Novel Corona Virus (SARS-CoV-2) Ag Rapid Test Kit" 
* #2107 ^property[0].code = #Relationships 
* #2107 ^property[0].valueString = "$.t..ma" 
* #2107 ^property[1].code = #hints 
* #2107 ^property[1].valueString = "T^E^B" 
* #2109 "Shenzhen Lvshiyuan Biotechnology Co., Ltd., Green Spring SARS-CoV-2 Antigen-Rapid test-Set"
* #2109 ^definition = Shenzhen Lvshiyuan Biotechnology Co., Ltd., Green Spring SARS-CoV-2 Antigen-Rapid test-Set
* #2109 ^designation[0].language = #de-AT 
* #2109 ^designation[0].value = "Shenzhen Lvshiyuan Biotechnology Co., Ltd., Green Spring SARS-CoV-2 Antigen-Rapid test-Set" 
* #2109 ^property[0].code = #Relationships 
* #2109 ^property[0].valueString = "$.t..ma" 
* #2109 ^property[1].code = #hints 
* #2109 ^property[1].valueString = "T^E^B" 
* #2111 "VivaChek Biotech (Hangzhou) Co., Ltd, SARS-CoV-2 Ag Rapid Test"
* #2111 ^definition = VivaChek Biotech (Hangzhou) Co., Ltd, SARS-CoV-2 Ag Rapid Test
* #2111 ^designation[0].language = #de-AT 
* #2111 ^designation[0].value = "VivaChek Biotech (Hangzhou) Co., Ltd, SARS-CoV-2 Ag Rapid Test" 
* #2111 ^property[0].code = #Relationships 
* #2111 ^property[0].valueString = "$.t..ma" 
* #2111 ^property[1].code = #hints 
* #2111 ^property[1].valueString = "T^E^B" 
* #2116 "PerGrande Biotech Development Co., Ltd., SARS-CoV-2 Antigen Detection Kit (Colloidal Gold Immunochromato-graphic assay)"
* #2116 ^definition = PerGrande Biotech Development Co., Ltd., SARS-CoV-2 Antigen Detection Kit (Colloidal Gold Immunochromato-graphic assay)
* #2116 ^designation[0].language = #de-AT 
* #2116 ^designation[0].value = "PerGrande Biotech Development Co., Ltd., SARS-CoV-2 Antigen Detection Kit (Colloidal Gold Immunochromato-graphic assay)" 
* #2116 ^property[0].code = #Relationships 
* #2116 ^property[0].valueString = "$.t..ma" 
* #2116 ^property[1].code = #hints 
* #2116 ^property[1].valueString = "T^E^B" 
* #2124 "Fujirebio, Lumipulse G SARS-CoV-2 Ag"
* #2124 ^definition = Fujirebio, Lumipulse G SARS-CoV-2 Ag
* #2124 ^designation[0].language = #de-AT 
* #2124 ^designation[0].value = "Fujirebio, Lumipulse G SARS-CoV-2 Ag" 
* #2124 ^property[0].code = #Relationships 
* #2124 ^property[0].valueString = "$.t..ma" 
* #2124 ^property[1].code = #hints 
* #2124 ^property[1].valueString = "T^E^B" 
* #2128 "Lumigenex (Suzhou) Co., Ltd, PocRoc SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold)"
* #2128 ^definition = Lumigenex (Suzhou) Co., Ltd, PocRoc SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold)
* #2128 ^designation[0].language = #de-AT 
* #2128 ^designation[0].value = "Lumigenex (Suzhou) Co., Ltd, PocRoc SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold)" 
* #2128 ^property[0].code = #Relationships 
* #2128 ^property[0].valueString = "$.t..ma" 
* #2128 ^property[1].code = #hints 
* #2128 ^property[1].valueString = "T^E^B" 
* #2130 "Affimedix Inc., TestNOW - COVID-19 Antigen Test"
* #2130 ^definition = Affimedix Inc., TestNOW - COVID-19 Antigen Test
* #2130 ^designation[0].language = #de-AT 
* #2130 ^designation[0].value = "Affimedix Inc., TestNOW - COVID-19 Antigen Test" 
* #2130 ^property[0].code = #Relationships 
* #2130 ^property[0].valueString = "$.t..ma" 
* #2130 ^property[1].code = #hints 
* #2130 ^property[1].valueString = "T^E^B" 
* #2139 "Hangzhou Lysun Biotechnology Co., Ltd., COVID-19 antigen Rapid Test Device (Colloidal Gold)"
* #2139 ^definition = Hangzhou Lysun Biotechnology Co., Ltd., COVID-19 antigen Rapid Test Device (Colloidal Gold)
* #2139 ^designation[0].language = #de-AT 
* #2139 ^designation[0].value = "Hangzhou Lysun Biotechnology Co., Ltd., COVID-19 antigen Rapid Test Device (Colloidal Gold)" 
* #2139 ^property[0].code = #Relationships 
* #2139 ^property[0].valueString = "$.t..ma" 
* #2139 ^property[1].code = #hints 
* #2139 ^property[1].valueString = "T^E^B" 
* #2143 "Wuxi Biohermes Bio & Medical Technology Co., Ltd, SARS-CoV-2 Antigen Test Kit (Lateral Flow Assay)"
* #2143 ^definition = Wuxi Biohermes Bio & Medical Technology Co., Ltd, SARS-CoV-2 Antigen Test Kit (Lateral Flow Assay)
* #2143 ^designation[0].language = #de-AT 
* #2143 ^designation[0].value = "Wuxi Biohermes Bio & Medical Technology Co., Ltd, SARS-CoV-2 Antigen Test Kit (Lateral Flow Assay)" 
* #2143 ^property[0].code = #Relationships 
* #2143 ^property[0].valueString = "$.t..ma" 
* #2143 ^property[1].code = #hints 
* #2143 ^property[1].valueString = "T^E^B" 
* #2144 "Jiangsu Well Biotech Co., Ltd., COVID-19 Ag Rapid Test Device"
* #2144 ^definition = Jiangsu Well Biotech Co., Ltd., COVID-19 Ag Rapid Test Device
* #2144 ^designation[0].language = #de-AT 
* #2144 ^designation[0].value = "Jiangsu Well Biotech Co., Ltd., COVID-19 Ag Rapid Test Device" 
* #2144 ^property[0].code = #Relationships 
* #2144 ^property[0].valueString = "$.t..ma" 
* #2144 ^property[1].code = #hints 
* #2144 ^property[1].valueString = "T^E^B" 
* #2147 "Fujirebio , ESPLINE SARS-CoV-2"
* #2147 ^definition = Fujirebio , ESPLINE SARS-CoV-2
* #2147 ^designation[0].language = #de-AT 
* #2147 ^designation[0].value = "Fujirebio , ESPLINE SARS-CoV-2" 
* #2147 ^property[0].code = #Relationships 
* #2147 ^property[0].valueString = "$.t..ma" 
* #2147 ^property[1].code = #hints 
* #2147 ^property[1].valueString = "T^E^B" 
* #2150 "Chongqing M&D Biotechnology Co. Ltd, 2019-nCoV Antigen Test Kit"
* #2150 ^definition = Chongqing M&D Biotechnology Co. Ltd, 2019-nCoV Antigen Test Kit
* #2150 ^designation[0].language = #de-AT 
* #2150 ^designation[0].value = "Chongqing M&D Biotechnology Co. Ltd, 2019-nCoV Antigen Test Kit" 
* #2150 ^property[0].code = #Relationships 
* #2150 ^property[0].valueString = "$.t..ma" 
* #2150 ^property[1].code = #hints 
* #2150 ^property[1].valueString = "T^E^B" 
* #2152 "Shenzhen CAS-Envision Medical Technology Co., Ltd., SARS-CoV-2-Antigen Rapid Detection Kit"
* #2152 ^definition = Shenzhen CAS-Envision Medical Technology Co., Ltd., SARS-CoV-2-Antigen Rapid Detection Kit
* #2152 ^designation[0].language = #de-AT 
* #2152 ^designation[0].value = "Shenzhen CAS-Envision Medical Technology Co., Ltd., SARS-CoV-2-Antigen Rapid Detection Kit" 
* #2152 ^property[0].code = #Relationships 
* #2152 ^property[0].valueString = "$.t..ma" 
* #2152 ^property[1].code = #hints 
* #2152 ^property[1].valueString = "T^E^B" 
* #2164 "Nanjing Synthgene Medical Technology Co., Ltd., SARS-COV-2 Nucleocapsid (N) Antigen Rapid Detection Kit (Colloidal gold method)"
* #2164 ^definition = Nanjing Synthgene Medical Technology Co., Ltd., SARS-COV-2 Nucleocapsid (N) Antigen Rapid Detection Kit (Colloidal gold method)
* #2164 ^designation[0].language = #de-AT 
* #2164 ^designation[0].value = "Nanjing Synthgene Medical Technology Co., Ltd., SARS-COV-2 Nucleocapsid (N) Antigen Rapid Detection Kit (Colloidal gold method)" 
* #2164 ^property[0].code = #Relationships 
* #2164 ^property[0].valueString = "$.t..ma" 
* #2164 ^property[1].code = #hints 
* #2164 ^property[1].valueString = "T^E^B" 
* #2183 "Getein Biotech, Inc., One Step Test for SARS-CoV-2 Antigen (Colloidal Gold)"
* #2183 ^definition = Getein Biotech, Inc., One Step Test for SARS-CoV-2 Antigen (Colloidal Gold)
* #2183 ^designation[0].language = #de-AT 
* #2183 ^designation[0].value = "Getein Biotech, Inc., One Step Test for SARS-CoV-2 Antigen (Colloidal Gold)" 
* #2183 ^property[0].code = #Relationships 
* #2183 ^property[0].valueString = "$.t..ma" 
* #2183 ^property[1].code = #hints 
* #2183 ^property[1].valueString = "T^E^B" 
* #2200 "NanoRepro AG, NanoRepro SARS-CoV-2 Antigen Rapid Test"
* #2200 ^definition = NanoRepro AG, NanoRepro SARS-CoV-2 Antigen Rapid Test
* #2200 ^designation[0].language = #de-AT 
* #2200 ^designation[0].value = "NanoRepro AG, NanoRepro SARS-CoV-2 Antigen Rapid Test" 
* #2200 ^property[0].code = #Relationships 
* #2200 ^property[0].valueString = "$.t..ma" 
* #2200 ^property[1].code = #hints 
* #2200 ^property[1].valueString = "T^E^B" 
* #2201 "Zybio Inc., SARS-CoV-2 Antigen Assay Kit (Colloidal Gold Method)"
* #2201 ^definition = Zybio Inc., SARS-CoV-2 Antigen Assay Kit (Colloidal Gold Method)
* #2201 ^designation[0].language = #de-AT 
* #2201 ^designation[0].value = "Zybio Inc., SARS-CoV-2 Antigen Assay Kit (Colloidal Gold Method)" 
* #2201 ^property[0].code = #Relationships 
* #2201 ^property[0].valueString = "$.t..ma" 
* #2201 ^property[1].code = #hints 
* #2201 ^property[1].valueString = "T^E^B" 
* #2228 "Roche (SD BIOSENSOR), SARS-CoV-2 Rapid Antigen Test Nasal"
* #2228 ^definition = Roche (SD BIOSENSOR), SARS-CoV-2 Rapid Antigen Test Nasal
* #2228 ^designation[0].language = #de-AT 
* #2228 ^designation[0].value = "Roche (SD BIOSENSOR), SARS-CoV-2 Rapid Antigen Test Nasal" 
* #2228 ^property[0].code = #Relationships 
* #2228 ^property[0].valueString = "$.t..ma" 
* #2228 ^property[1].code = #hints 
* #2228 ^property[1].valueString = "T^E^B" 
* #2230 "BIOHIT HealthCcare (Hefei) Co., Ltd., SARS-CoV-2 Antigen Rapid Test (Colloidal Gold Method)"
* #2230 ^definition = BIOHIT HealthCcare (Hefei) Co., Ltd., SARS-CoV-2 Antigen Rapid Test (Colloidal Gold Method)
* #2230 ^designation[0].language = #de-AT 
* #2230 ^designation[0].value = "BIOHIT HealthCcare (Hefei) Co., Ltd., SARS-CoV-2 Antigen Rapid Test (Colloidal Gold Method)" 
* #2230 ^property[0].code = #Relationships 
* #2230 ^property[0].valueString = "$.t..ma" 
* #2230 ^property[1].code = #hints 
* #2230 ^property[1].valueString = "T^E^B" 
* #2241 "NESAPOR EUROPA SL, MARESKIT COVID-19 ANTIGEN RAPID TEST KIT"
* #2241 ^definition = NESAPOR EUROPA SL, MARESKIT COVID-19 ANTIGEN RAPID TEST KIT
* #2241 ^designation[0].language = #de-AT 
* #2241 ^designation[0].value = "NESAPOR EUROPA SL, MARESKIT COVID-19 ANTIGEN RAPID TEST KIT" 
* #2241 ^property[0].code = #Relationships 
* #2241 ^property[0].valueString = "$.t..ma" 
* #2241 ^property[1].code = #hints 
* #2241 ^property[1].valueString = "T^E^B" 
* #2242 "DNA Diagnostic, COVID-19 Antigen Detection Kit"
* #2242 ^definition = DNA Diagnostic, COVID-19 Antigen Detection Kit
* #2242 ^designation[0].language = #de-AT 
* #2242 ^designation[0].value = "DNA Diagnostic, COVID-19 Antigen Detection Kit" 
* #2242 ^property[0].code = #Relationships 
* #2242 ^property[0].valueString = "$.t..ma" 
* #2242 ^property[1].code = #hints 
* #2242 ^property[1].valueString = "T^E^B" 
* #2247 "BioGnost Ltd, CoviGnost AG Test Device 1x20"
* #2247 ^definition = BioGnost Ltd, CoviGnost AG Test Device 1x20
* #2247 ^designation[0].language = #de-AT 
* #2247 ^designation[0].value = "BioGnost Ltd, CoviGnost AG Test Device 1x20" 
* #2247 ^property[0].code = #Relationships 
* #2247 ^property[0].valueString = "$.t..ma" 
* #2247 ^property[1].code = #hints 
* #2247 ^property[1].valueString = "T^E^B" 
* #2257 "Hangzhou AllTest Biotech Co., Ltd, SARS-CoV-2 Antigen Rapid Test (Nasal Swab)"
* #2257 ^definition = Hangzhou AllTest Biotech Co., Ltd, SARS-CoV-2 Antigen Rapid Test (Nasal Swab)
* #2257 ^designation[0].language = #de-AT 
* #2257 ^designation[0].value = "Hangzhou AllTest Biotech Co., Ltd, SARS-CoV-2 Antigen Rapid Test (Nasal Swab)" 
* #2257 ^property[0].code = #Relationships 
* #2257 ^property[0].valueString = "$.t..ma" 
* #2257 ^property[1].code = #hints 
* #2257 ^property[1].valueString = "T^E^B" 
* #2260 "Multi-G bvba, Covid19Check-NAS"
* #2260 ^definition = Multi-G bvba, Covid19Check-NAS
* #2260 ^designation[0].language = #de-AT 
* #2260 ^designation[0].value = "Multi-G bvba, Covid19Check-NAS" 
* #2260 ^property[0].code = #Relationships 
* #2260 ^property[0].valueString = "$.t..ma" 
* #2260 ^property[1].code = #hints 
* #2260 ^property[1].valueString = "T^E^B" 
* #2271 "Pantest SA, Pantest Coronavirus Ag (Nasopharyngeal Swab)"
* #2271 ^definition = Pantest SA, Pantest Coronavirus Ag (Nasopharyngeal Swab)
* #2271 ^designation[0].language = #de-AT 
* #2271 ^designation[0].value = "Pantest SA, Pantest Coronavirus Ag (Nasopharyngeal Swab)" 
* #2271 ^property[0].code = #Relationships 
* #2271 ^property[0].valueString = "$.t..ma" 
* #2271 ^property[1].code = #hints 
* #2271 ^property[1].valueString = "T^E^B" 
* #2273 "Dräger Safety AG & Co. KGaA, Dräger Antigen Test SARS-CoV-2"
* #2273 ^definition = Dräger Safety AG & Co. KGaA, Dräger Antigen Test SARS-CoV-2
* #2273 ^designation[0].language = #de-AT 
* #2273 ^designation[0].value = "Dräger Safety AG & Co. KGaA, Dräger Antigen Test SARS-CoV-2" 
* #2273 ^property[0].code = #Relationships 
* #2273 ^property[0].valueString = "$.t..ma" 
* #2273 ^property[1].code = #hints 
* #2273 ^property[1].valueString = "T^E^B" 
* #2278 "Innovation Biotech(Beijing) Co.Ltd, Coronavirus (SARS-Cov-2) Antigen Rapid Test Cassette (Nasal swab)"
* #2278 ^definition = Innovation Biotech(Beijing) Co.Ltd, Coronavirus (SARS-Cov-2) Antigen Rapid Test Cassette (Nasal swab)
* #2278 ^designation[0].language = #de-AT 
* #2278 ^designation[0].value = "Innovation Biotech(Beijing) Co.Ltd, Coronavirus (SARS-Cov-2) Antigen Rapid Test Cassette (Nasal swab)" 
* #2278 ^property[0].code = #Relationships 
* #2278 ^property[0].valueString = "$.t..ma" 
* #2278 ^property[1].code = #hints 
* #2278 ^property[1].valueString = "T^E^B" 
* #2282 "Becton Dickinson, BD Kit for Rapid Detection of SARS-CoV-2"
* #2282 ^definition = Becton Dickinson, BD Kit for Rapid Detection of SARS-CoV-2
* #2282 ^designation[0].language = #de-AT 
* #2282 ^designation[0].value = "Becton Dickinson, BD Kit for Rapid Detection of SARS-CoV-2" 
* #2282 ^property[0].code = #Relationships 
* #2282 ^property[0].valueString = "$.t..ma" 
* #2282 ^property[1].code = #hints 
* #2282 ^property[1].valueString = "T^E^B" 
* #2290 "Rapid Pathogen Screening, Inc, LIAISON Quick Detect Covid Ag Assay"
* #2290 ^definition = Rapid Pathogen Screening, Inc, LIAISON Quick Detect Covid Ag Assay
* #2290 ^designation[0].language = #de-AT 
* #2290 ^designation[0].value = "Rapid Pathogen Screening, Inc, LIAISON Quick Detect Covid Ag Assay" 
* #2290 ^property[0].code = #Relationships 
* #2290 ^property[0].valueString = "$.t..ma" 
* #2290 ^property[1].code = #hints 
* #2290 ^property[1].valueString = "T^E^B" 
* #2297 "SureScreen Diagnostics, SARS-CoV-2 Rapid Antigen Test Cassette"
* #2297 ^definition = SureScreen Diagnostics, SARS-CoV-2 Rapid Antigen Test Cassette
* #2297 ^designation[0].language = #de-AT 
* #2297 ^designation[0].value = "SureScreen Diagnostics, SARS-CoV-2 Rapid Antigen Test Cassette" 
* #2297 ^property[0].code = #Relationships 
* #2297 ^property[0].valueString = "$.t..ma" 
* #2297 ^property[1].code = #hints 
* #2297 ^property[1].valueString = "T^E^B" 
* #2301 "Nanjing Liming Bio-Products Co., Ltd., StrongStep SARS-CoV-2 Antigen Rapid Test"
* #2301 ^definition = Nanjing Liming Bio-Products Co., Ltd., StrongStep SARS-CoV-2 Antigen Rapid Test
* #2301 ^designation[0].language = #de-AT 
* #2301 ^designation[0].value = "Nanjing Liming Bio-Products Co., Ltd., StrongStep SARS-CoV-2 Antigen Rapid Test" 
* #2301 ^property[0].code = #Relationships 
* #2301 ^property[0].valueString = "$.t..ma" 
* #2301 ^property[1].code = #hints 
* #2301 ^property[1].valueString = "T^E^B" 
* #2302 "Hangzhou AllTest Biotech Co., Ltd, COVID-19 Antigen Test Cassette (Nasopharyngeal Swab)(FIA)"
* #2302 ^definition = Hangzhou AllTest Biotech Co., Ltd, COVID-19 Antigen Test Cassette (Nasopharyngeal Swab)(FIA)
* #2302 ^designation[0].language = #de-AT 
* #2302 ^designation[0].value = "Hangzhou AllTest Biotech Co., Ltd, COVID-19 Antigen Test Cassette (Nasopharyngeal Swab)(FIA)" 
* #2302 ^property[0].code = #Relationships 
* #2302 ^property[0].valueString = "$.t..ma" 
* #2302 ^property[1].code = #hints 
* #2302 ^property[1].valueString = "T^E^B" 
* #2319 "Hangzhou AllTest Biotech Co., Ltd, CVAG4080A ? GSD NovaGen SARS-CoV-2 Ag Rapid Test (NP Swab)"
* #2319 ^definition = Hangzhou AllTest Biotech Co., Ltd, CVAG4080A ? GSD NovaGen SARS-CoV-2 Ag Rapid Test (NP Swab)
* #2319 ^designation[0].language = #de-AT 
* #2319 ^designation[0].value = "Hangzhou AllTest Biotech Co., Ltd, CVAG4080A ? GSD NovaGen SARS-CoV-2 Ag Rapid Test (NP Swab)" 
* #2319 ^property[0].code = #Relationships 
* #2319 ^property[0].valueString = "$.t..ma" 
* #2319 ^property[1].code = #hints 
* #2319 ^property[1].valueString = "T^E^B" 
* #2325 "Hangzhou AllTest Biotech Co., Ltd, GSD NovaGen SARS-CoV-2 Ag Rapid Test (Nasal Swab)"
* #2325 ^definition = Hangzhou AllTest Biotech Co., Ltd, GSD NovaGen SARS-CoV-2 Ag Rapid Test (Nasal Swab)
* #2325 ^designation[0].language = #de-AT 
* #2325 ^designation[0].value = "Hangzhou AllTest Biotech Co., Ltd, GSD NovaGen SARS-CoV-2 Ag Rapid Test (Nasal Swab)" 
* #2325 ^property[0].code = #Relationships 
* #2325 ^property[0].valueString = "$.t..ma" 
* #2325 ^property[1].code = #hints 
* #2325 ^property[1].valueString = "T^E^B" 
* #2350 "Assure Tech. (Hangzhou) Co., Ltd., ECOTEST COVID-19 Antigen Rapid Test Device"
* #2350 ^definition = Assure Tech. (Hangzhou) Co., Ltd., ECOTEST COVID-19 Antigen Rapid Test Device
* #2350 ^designation[0].language = #de-AT 
* #2350 ^designation[0].value = "Assure Tech. (Hangzhou) Co., Ltd., ECOTEST COVID-19 Antigen Rapid Test Device" 
* #2350 ^property[0].code = #Relationships 
* #2350 ^property[0].valueString = "$.t..ma" 
* #2350 ^property[1].code = #hints 
* #2350 ^property[1].valueString = "T^E^B" 
* #2374 "ABIOTEQ, Cora Gentest-19"
* #2374 ^definition = ABIOTEQ, Cora Gentest-19
* #2374 ^designation[0].language = #de-AT 
* #2374 ^designation[0].value = "ABIOTEQ, Cora Gentest-19" 
* #2374 ^property[0].code = #Relationships 
* #2374 ^property[0].valueString = "$.t..ma" 
* #2374 ^property[1].code = #hints 
* #2374 ^property[1].valueString = "T^E^B" 
* #2380 "BioSpeedia International, COVID19Speed-Antigen Test BSD_0503"
* #2380 ^definition = BioSpeedia International, COVID19Speed-Antigen Test BSD_0503
* #2380 ^designation[0].language = #de-AT 
* #2380 ^designation[0].value = "BioSpeedia International, COVID19Speed-Antigen Test BSD_0503 " 
* #2380 ^property[0].code = #Relationships 
* #2380 ^property[0].valueString = "$.t..ma" 
* #2380 ^property[1].code = #hints 
* #2380 ^property[1].valueString = "T^E^B" 
* #2414 "Shenzhen Huian Biosci Technology Co., Ltd., SARS-CoV-2 Antigen Test Kit (Colloidal Gold)"
* #2414 ^definition = Shenzhen Huian Biosci Technology Co., Ltd., SARS-CoV-2 Antigen Test Kit (Colloidal Gold)
* #2414 ^designation[0].language = #de-AT 
* #2414 ^designation[0].value = "Shenzhen Huian Biosci Technology Co., Ltd., SARS-CoV-2 Antigen Test Kit (Colloidal Gold)" 
* #2414 ^property[0].code = #Relationships 
* #2414 ^property[0].valueString = "$.t..ma" 
* #2414 ^property[1].code = #hints 
* #2414 ^property[1].valueString = "T^E^B" 
* #2415 "Shenzhen Dymind Biotechnology Co., Ltd, SARS-CoV-2 Antigen Test Kit (Colloidal Gold)"
* #2415 ^definition = Shenzhen Dymind Biotechnology Co., Ltd, SARS-CoV-2 Antigen Test Kit (Colloidal Gold)
* #2415 ^designation[0].language = #de-AT 
* #2415 ^designation[0].value = "Shenzhen Dymind Biotechnology Co., Ltd, SARS-CoV-2 Antigen Test Kit (Colloidal Gold)" 
* #2415 ^property[0].code = #Relationships 
* #2415 ^property[0].valueString = "$.t..ma" 
* #2415 ^property[1].code = #hints 
* #2415 ^property[1].valueString = "T^E^B" 
* #2419 "InTec PRODUCTS, INC., Rapid SARS-CoV-2 Antigen Test (nasopharyngeal specimen)"
* #2419 ^definition = InTec PRODUCTS, INC., Rapid SARS-CoV-2 Antigen Test (nasopharyngeal specimen)
* #2419 ^designation[0].language = #de-AT 
* #2419 ^designation[0].value = "InTec PRODUCTS, INC., Rapid SARS-CoV-2 Antigen Test (nasopharyngeal specimen)" 
* #2419 ^property[0].code = #Relationships 
* #2419 ^property[0].valueString = "$.t..ma" 
* #2419 ^property[1].code = #hints 
* #2419 ^property[1].valueString = "T^E^B" 
* #2449 "Citest Diagnostics Inc., COVID-19 Antigen Rapid Test (Nasal Swab)"
* #2449 ^definition = Citest Diagnostics Inc., COVID-19 Antigen Rapid Test (Nasal Swab)
* #2449 ^designation[0].language = #de-AT 
* #2449 ^designation[0].value = "Citest Diagnostics Inc., COVID-19 Antigen Rapid Test (Nasal Swab)" 
* #2449 ^property[0].code = #Relationships 
* #2449 ^property[0].valueString = "$.t..ma" 
* #2449 ^property[1].code = #hints 
* #2449 ^property[1].valueString = "T^E^B" 
* #2494 "Beijing O&D Biotech Co., Ltd., COVID-19 Antigen Rapid Test"
* #2494 ^definition = Beijing O&D Biotech Co., Ltd., COVID-19 Antigen Rapid Test
* #2494 ^designation[0].language = #de-AT 
* #2494 ^designation[0].value = "Beijing O&D Biotech Co., Ltd., COVID-19 Antigen Rapid Test" 
* #2494 ^property[0].code = #Relationships 
* #2494 ^property[0].valueString = "$.t..ma" 
* #2494 ^property[1].code = #hints 
* #2494 ^property[1].valueString = "T^E^B" 
* #2506 "Nanjing Norman Biological Technology Co., Ltd., Novel Coronavirus (2019-nCoV) Antigen Testing Kit (Colloidal Gold)"
* #2506 ^definition = Nanjing Norman Biological Technology Co., Ltd., Novel Coronavirus (2019-nCoV) Antigen Testing Kit (Colloidal Gold)
* #2506 ^designation[0].language = #de-AT 
* #2506 ^designation[0].value = "Nanjing Norman Biological Technology Co., Ltd., Novel Coronavirus (2019-nCoV) Antigen Testing Kit (Colloidal Gold)" 
* #2506 ^property[0].code = #Relationships 
* #2506 ^property[0].valueString = "$.t..ma" 
* #2506 ^property[1].code = #hints 
* #2506 ^property[1].valueString = "T^E^B" 
* #2519 "BIOLAN HEALTH, S.L, COVID-19 Antigen Rapid Test (Colloidal Gold Method)"
* #2519 ^definition = BIOLAN HEALTH, S.L, COVID-19 Antigen Rapid Test (Colloidal Gold Method)
* #2519 ^designation[0].language = #de-AT 
* #2519 ^designation[0].value = "BIOLAN HEALTH, S.L, COVID-19 Antigen Rapid Test (Colloidal Gold Method)" 
* #2519 ^property[0].code = #Relationships 
* #2519 ^property[0].valueString = "$.t..ma" 
* #2519 ^property[1].code = #hints 
* #2519 ^property[1].valueString = "T^E^B" 
* #2533 "Dynamiker Biotechnolgy(Tianjin) Co., Ltd., Dynamiker SARS-CoV-2 Ag Rapid Test"
* #2533 ^definition = Dynamiker Biotechnolgy(Tianjin) Co., Ltd., Dynamiker SARS-CoV-2 Ag Rapid Test
* #2533 ^designation[0].language = #de-AT 
* #2533 ^designation[0].value = "Dynamiker Biotechnolgy(Tianjin) Co., Ltd., Dynamiker SARS-CoV-2 Ag Rapid Test" 
* #2533 ^property[0].code = #Relationships 
* #2533 ^property[0].valueString = "$.t..ma" 
* #2533 ^property[1].code = #hints 
* #2533 ^property[1].valueString = "T^E^B" 
* #2555 "IEDAU INTERNATIONAL GMBH, Covid-19 Antigen Schnelltest (Colloidales Gold)"
* #2555 ^definition = IEDAU INTERNATIONAL GMBH, Covid-19 Antigen Schnelltest (Colloidales Gold)
* #2555 ^designation[0].language = #de-AT 
* #2555 ^designation[0].value = "IEDAU INTERNATIONAL GMBH, Covid-19 Antigen Schnelltest (Colloidales Gold)" 
* #2555 ^property[0].code = #Relationships 
* #2555 ^property[0].valueString = "$.t..ma" 
* #2555 ^property[1].code = #hints 
* #2555 ^property[1].valueString = "T^E^B" 
* #2579 "AccuBioTech Co.,Ltd, Accu-Tell SARS-CoV-2 Ag Cassette"
* #2579 ^definition = AccuBioTech Co.,Ltd, Accu-Tell SARS-CoV-2 Ag Cassette
* #2579 ^designation[0].language = #de-AT 
* #2579 ^designation[0].value = "AccuBioTech Co.,Ltd, Accu-Tell SARS-CoV-2 Ag Cassette" 
* #2579 ^property[0].code = #Relationships 
* #2579 ^property[0].valueString = "$.t..ma" 
* #2579 ^property[1].code = #hints 
* #2579 ^property[1].valueString = "T^E^B" 
* #2584 "TÜRKLAB TIBBI MALZEMELER SAN. ve TIC. A.S., INFO Covid-19 Ag Test"
* #2584 ^definition = TÜRKLAB TIBBI MALZEMELER SAN. ve TIC. A.S., INFO Covid-19 Ag Test
* #2584 ^designation[0].language = #de-AT 
* #2584 ^designation[0].value = "TÜRKLAB TIBBI MALZEMELER SAN. ve TIC. A.S., INFO Covid-19 Ag Test" 
* #2584 ^property[0].code = #Relationships 
* #2584 ^property[0].valueString = "$.t..ma" 
* #2584 ^property[1].code = #hints 
* #2584 ^property[1].valueString = "T^E^B" 
* #2586 "Jiangsu Mole Bioscience CO., LTD., SARS-CoV-2 Antigen Test Cassette"
* #2586 ^definition = Jiangsu Mole Bioscience CO., LTD., SARS-CoV-2 Antigen Test Cassette
* #2586 ^designation[0].language = #de-AT 
* #2586 ^designation[0].value = "Jiangsu Mole Bioscience CO., LTD., SARS-CoV-2 Antigen Test Cassette" 
* #2586 ^property[0].code = #Relationships 
* #2586 ^property[0].valueString = "$.t..ma" 
* #2586 ^property[1].code = #hints 
* #2586 ^property[1].valueString = "T^E^B" 
* #2588 "Changzhou Biowin Pharmaceutical Co.,Ltd., Novel Coronavirus(COVID-19) Antigen Test Kit (Colloidal Gold)"
* #2588 ^definition = Changzhou Biowin Pharmaceutical Co.,Ltd., Novel Coronavirus(COVID-19) Antigen Test Kit (Colloidal Gold)
* #2588 ^designation[0].language = #de-AT 
* #2588 ^designation[0].value = "Changzhou Biowin Pharmaceutical Co.,Ltd., Novel Coronavirus(COVID-19) Antigen Test Kit (Colloidal Gold)" 
* #2588 ^property[0].code = #Relationships 
* #2588 ^property[0].valueString = "$.t..ma" 
* #2588 ^property[1].code = #hints 
* #2588 ^property[1].valueString = "T^E^B" 
* #260373001 "detected"
* #260373001 ^definition = detected
* #260373001 ^designation[0].language = #de-AT 
* #260373001 ^designation[0].value = "nachgewiesen" 
* #260373001 ^property[0].code = #Relationships 
* #260373001 ^property[0].valueString = "$.t..tr" 
* #260415000 "not detected"
* #260415000 ^definition = not detected
* #260415000 ^designation[0].language = #de-AT 
* #260415000 ^designation[0].value = "nicht nachgewiesen" 
* #260415000 ^property[0].code = #Relationships 
* #260415000 ^property[0].valueString = "$.t..tr" 
* #260415000 ^property[1].code = #hints 
* #260415000 ^property[1].valueString = "T^E^B" 
* #2608 "Neo-nostics (Suzhou) Bioengineering Co., Ltd., COVID 19 Antigen Test Kit (Colloidal Gold Method)"
* #2608 ^definition = Neo-nostics (Suzhou) Bioengineering Co., Ltd., COVID 19 Antigen Test Kit (Colloidal Gold Method)
* #2608 ^designation[0].language = #de-AT 
* #2608 ^designation[0].value = "Neo-nostics (Suzhou) Bioengineering Co., Ltd., COVID 19 Antigen Test Kit (Colloidal Gold Method)" 
* #2608 ^property[0].code = #Relationships 
* #2608 ^property[0].valueString = "$.t..ma" 
* #2608 ^property[1].code = #hints 
* #2608 ^property[1].valueString = "T^E^B" 
* #2629 "Hangzhou DIAN Biotechnology Co., Ltd., COVID-19 Antigen Test Cassette"
* #2629 ^definition = Hangzhou DIAN Biotechnology Co., Ltd., COVID-19 Antigen Test Cassette
* #2629 ^designation[0].language = #de-AT 
* #2629 ^designation[0].value = "Hangzhou DIAN Biotechnology Co., Ltd., COVID-19 Antigen Test Cassette" 
* #2629 ^property[0].code = #Relationships 
* #2629 ^property[0].valueString = "$.t..ma" 
* #2629 ^property[1].code = #hints 
* #2629 ^property[1].valueString = "T^E^B" 
* #2640 "Mologic Ltd, COVIOS Ag COVID-19 Antigen Rapid Diagnostic Test"
* #2640 ^definition = Mologic Ltd, COVIOS Ag COVID-19 Antigen Rapid Diagnostic Test
* #2640 ^designation[0].language = #de-AT 
* #2640 ^designation[0].value = "Mologic Ltd, COVIOS Ag COVID-19 Antigen Rapid Diagnostic Test" 
* #2640 ^property[0].code = #Relationships 
* #2640 ^property[0].valueString = "$.t..ma" 
* #2640 ^property[1].code = #hints 
* #2640 ^property[1].valueString = "T^E^B" 
* #2642 "Genobio Pharmaceutical Co., Ltd., Virusee SARS-CoV-2 Antigen Rapid Test (Colloidal Gold)"
* #2642 ^definition = Genobio Pharmaceutical Co., Ltd., Virusee SARS-CoV-2 Antigen Rapid Test (Colloidal Gold)
* #2642 ^designation[0].language = #de-AT 
* #2642 ^designation[0].value = "Genobio Pharmaceutical Co., Ltd., Virusee SARS-CoV-2 Antigen Rapid Test (Colloidal Gold)" 
* #2642 ^property[0].code = #Relationships 
* #2642 ^property[0].valueString = "$.t..ma" 
* #2642 ^property[1].code = #hints 
* #2642 ^property[1].valueString = "T^E^B" 
* #2672 "Pierenkemper GmbH, (SARS-CoV-2) Antigen Rapid Test COVIDENT (SWAB) COVID-19"
* #2672 ^definition = Pierenkemper GmbH, (SARS-CoV-2) Antigen Rapid Test COVIDENT (SWAB) COVID-19
* #2672 ^designation[0].language = #de-AT 
* #2672 ^designation[0].value = "Pierenkemper GmbH, (SARS-CoV-2) Antigen Rapid Test COVIDENT (SWAB) COVID-19" 
* #2672 ^property[0].code = #Relationships 
* #2672 ^property[0].valueString = "$.t..ma" 
* #2672 ^property[1].code = #hints 
* #2672 ^property[1].valueString = "T^E^B" 
* #2678 "NDFOS Co., Ltd., ND COVID-19 Ag Test"
* #2678 ^definition = NDFOS Co., Ltd., ND COVID-19 Ag Test
* #2678 ^designation[0].language = #de-AT 
* #2678 ^designation[0].value = "NDFOS Co., Ltd., ND COVID-19 Ag Test" 
* #2678 ^property[0].code = #Relationships 
* #2678 ^property[0].valueString = "$.t..ma" 
* #2678 ^property[1].code = #hints 
* #2678 ^property[1].valueString = "T^E^B" 
* #2684 "Zhejiang GENE SCIENCE Co., Ltd, Novel Coronavirus (COVID-19) Antigen Detection Kit (Swab)"
* #2684 ^definition = Zhejiang GENE SCIENCE Co., Ltd, Novel Coronavirus (COVID-19) Antigen Detection Kit (Swab)
* #2684 ^designation[0].language = #de-AT 
* #2684 ^designation[0].value = "Zhejiang GENE SCIENCE Co., Ltd, Novel Coronavirus (COVID-19) Antigen Detection Kit (Swab)" 
* #2684 ^property[0].code = #Relationships 
* #2684 ^property[0].valueString = "$.t..ma" 
* #2684 ^property[1].code = #hints 
* #2684 ^property[1].valueString = "T^E^B" 
* #2685 "PRIMA Lab SA, COVID-19 Antigen Rapid Test"
* #2685 ^definition = PRIMA Lab SA, COVID-19 Antigen Rapid Test
* #2685 ^designation[0].language = #de-AT 
* #2685 ^designation[0].value = "PRIMA Lab SA, COVID-19 Antigen Rapid Test" 
* #2685 ^property[0].code = #Relationships 
* #2685 ^property[0].valueString = "$.t..ma" 
* #2685 ^property[1].code = #hints 
* #2685 ^property[1].valueString = "T^E^B" 
* #2687 "Zephyr Biomedicals - Tulip Diagnostics, PerkinElmer COVID 19 Antigen Test (NS, NP)"
* #2687 ^definition = Zephyr Biomedicals - Tulip Diagnostics, PerkinElmer COVID 19 Antigen Test (NS, NP)
* #2687 ^designation[0].language = #de-AT 
* #2687 ^designation[0].value = "Zephyr Biomedicals - Tulip Diagnostics, PerkinElmer COVID 19 Antigen Test (NS, NP)" 
* #2687 ^property[0].code = #Relationships 
* #2687 ^property[0].valueString = "$.t..ma" 
* #2687 ^property[1].code = #hints 
* #2687 ^property[1].valueString = "T^E^B" 
* #2691 "CALTH Inc., AllCheck COVID19 Ag Nasal"
* #2691 ^definition = CALTH Inc., AllCheck COVID19 Ag Nasal
* #2691 ^designation[0].language = #de-AT 
* #2691 ^designation[0].value = "CALTH Inc., AllCheck COVID19 Ag Nasal" 
* #2691 ^property[0].code = #Relationships 
* #2691 ^property[0].valueString = "$.t..ma" 
* #2691 ^property[1].code = #hints 
* #2691 ^property[1].valueString = "T^E^B" 
* #2695 "Glallergen CO., LTD., Novel Coronavirus (2019-nCoV) Antigen Test Kit (Colloidal gold immunochromatography)"
* #2695 ^definition = Glallergen CO., LTD., Novel Coronavirus (2019-nCoV) Antigen Test Kit (Colloidal gold immunochromatography)
* #2695 ^designation[0].language = #de-AT 
* #2695 ^designation[0].value = "Glallergen CO., LTD., Novel Coronavirus (2019-nCoV) Antigen Test Kit (Colloidal gold immunochromatography)" 
* #2695 ^property[0].code = #Relationships 
* #2695 ^property[0].valueString = "$.t..ma" 
* #2695 ^property[1].code = #hints 
* #2695 ^property[1].valueString = "T^E^B" 
* #2696 "Cesna Biyoteknoloji Arast?rma Gelistirme Laboratuvar Sist.Ins.Müh.Dan.San.Tic.Ltd.Sti., CHECK UP SARS-COV-2 NASAL ANTIGEN RAPID TEST"
* #2696 ^definition = Cesna Biyoteknoloji Arast?rma Gelistirme Laboratuvar Sist.Ins.Müh.Dan.San.Tic.Ltd.Sti., CHECK UP SARS-COV-2 NASAL ANTIGEN RAPID TEST
* #2696 ^designation[0].language = #de-AT 
* #2696 ^designation[0].value = "Cesna Biyoteknoloji Arast?rma Gelistirme Laboratuvar Sist.Ins.Müh.Dan.San.Tic.Ltd.Sti., CHECK UP SARS-COV-2 NASAL ANTIGEN RAPID TEST" 
* #2696 ^property[0].code = #Relationships 
* #2696 ^property[0].valueString = "$.t..ma" 
* #2696 ^property[1].code = #hints 
* #2696 ^property[1].valueString = "T^E^B" 
* #2724 "Fosun Diagnostics (Shanghai) Co.,Ltd., China, Fosun Covid-19 Ag Card"
* #2724 ^definition = Fosun Diagnostics (Shanghai) Co.,Ltd., China, Fosun Covid-19 Ag Card
* #2724 ^designation[0].language = #de-AT 
* #2724 ^designation[0].value = "Fosun Diagnostics (Shanghai) Co.,Ltd., China, Fosun Covid-19 Ag Card" 
* #2724 ^property[0].code = #Relationships 
* #2724 ^property[0].valueString = "$.t..ma" 
* #2724 ^property[1].code = #hints 
* #2724 ^property[1].valueString = "T^E^B" 
* #2741 "OSANG Healthcare Co., Ltd., GeneFinder COVID-19 Ag Plus Rapid Test"
* #2741 ^definition = OSANG Healthcare Co., Ltd., GeneFinder COVID-19 Ag Plus Rapid Test
* #2741 ^designation[0].language = #de-AT 
* #2741 ^designation[0].value = "OSANG Healthcare Co., Ltd., GeneFinder COVID-19 Ag Plus Rapid Test" 
* #2741 ^property[0].code = #Relationships 
* #2741 ^property[0].valueString = "$.t..ma" 
* #2741 ^property[1].code = #hints 
* #2741 ^property[1].valueString = "T^E^B" 
* #2742 "Wuhan HealthCare Biotechnology Co. Ltd., SARS-CoV-2 Antigen Test Kit (Colloidal Gold)"
* #2742 ^definition = Wuhan HealthCare Biotechnology Co. Ltd., SARS-CoV-2 Antigen Test Kit (Colloidal Gold)
* #2742 ^designation[0].language = #de-AT 
* #2742 ^designation[0].value = "Wuhan HealthCare Biotechnology Co. Ltd., SARS-CoV-2 Antigen Test Kit (Colloidal Gold)" 
* #2742 ^property[0].code = #Relationships 
* #2742 ^property[0].valueString = "$.t..ma" 
* #2742 ^property[1].code = #hints 
* #2742 ^property[1].valueString = "T^E^B" 
* #2746 "Cesna Biyoteknoloji Arast?rma Gelistirme Laboratuvar Sist.Ins.Müh.Dan.San.Tic.Ltd.Sti., CHECK UP SARS-COV-2 NASOPHARYNGEAL RAPID ANTIGEN TEST"
* #2746 ^definition = Cesna Biyoteknoloji Arast?rma Gelistirme Laboratuvar Sist.Ins.Müh.Dan.San.Tic.Ltd.Sti., CHECK UP SARS-COV-2 NASOPHARYNGEAL RAPID ANTIGEN TEST
* #2746 ^designation[0].language = #de-AT 
* #2746 ^designation[0].value = "Cesna Biyoteknoloji Arast?rma Gelistirme Laboratuvar Sist.Ins.Müh.Dan.San.Tic.Ltd.Sti., CHECK UP SARS-COV-2 NASOPHARYNGEAL RAPID ANTIGEN TEST" 
* #2746 ^property[0].code = #Relationships 
* #2746 ^property[0].valueString = "$.t..ma" 
* #2746 ^property[1].code = #hints 
* #2746 ^property[1].valueString = "T^E^B" 
* #2754 "Qingdao Hightop Biotech Co., Ltd., SARS-CoV-2/Flu A+B/RSV Antigen Rapid Test"
* #2754 ^definition = Qingdao Hightop Biotech Co., Ltd., SARS-CoV-2/Flu A+B/RSV Antigen Rapid Test
* #2754 ^designation[0].language = #de-AT 
* #2754 ^designation[0].value = "Qingdao Hightop Biotech Co., Ltd., SARS-CoV-2/Flu A+B/RSV Antigen Rapid Test" 
* #2754 ^property[0].code = #Relationships 
* #2754 ^property[0].valueString = "$.t..ma" 
* #2754 ^property[1].code = #hints 
* #2754 ^property[1].valueString = "T^E^B" 
* #2756 "DNA Diagnostic, SARS-CoV-2 Antigen Rapid Test"
* #2756 ^definition = DNA Diagnostic, SARS-CoV-2 Antigen Rapid Test
* #2756 ^designation[0].language = #de-AT 
* #2756 ^designation[0].value = "DNA Diagnostic, SARS-CoV-2 Antigen Rapid Test" 
* #2756 ^property[0].code = #Relationships 
* #2756 ^property[0].valueString = "$.t..ma" 
* #2756 ^property[1].code = #hints 
* #2756 ^property[1].valueString = "T^E^B" 
* #2763 "ScheBo Biotech, ScheBo SARS CoV-2 Quick ANTIGEN (Colloidal Gold Method)"
* #2763 ^definition = ScheBo Biotech, ScheBo SARS CoV-2 Quick ANTIGEN (Colloidal Gold Method)
* #2763 ^designation[0].language = #de-AT 
* #2763 ^designation[0].value = "ScheBo Biotech, ScheBo SARS CoV-2 Quick ANTIGEN (Colloidal Gold Method)" 
* #2763 ^property[0].code = #Relationships 
* #2763 ^property[0].valueString = "$.t..ma" 
* #2763 ^property[1].code = #hints 
* #2763 ^property[1].valueString = "T^E^B" 
* #2807 "Beijing Hotgen Biotech Co., Ltd, Coronavirus (2019-nCoV)-Antigentest"
* #2807 ^definition = Beijing Hotgen Biotech Co., Ltd, Coronavirus (2019-nCoV)-Antigentest
* #2807 ^designation[0].language = #de-AT 
* #2807 ^designation[0].value = "Beijing Hotgen Biotech Co., Ltd, Coronavirus (2019-nCoV)-Antigentest" 
* #2807 ^property[0].code = #Relationships 
* #2807 ^property[0].valueString = "$.t..ma" 
* #2807 ^property[1].code = #hints 
* #2807 ^property[1].valueString = "T^E^B" 
* #2812 "Shenzhen Huaree Technology Co.,Ltd, SARS-CoV-2 Antigen Rapid Test Kit (Immunochromatography)"
* #2812 ^definition = Shenzhen Huaree Technology Co.,Ltd, SARS-CoV-2 Antigen Rapid Test Kit (Immunochromatography)
* #2812 ^designation[0].language = #de-AT 
* #2812 ^designation[0].value = "Shenzhen Huaree Technology Co.,Ltd, SARS-CoV-2 Antigen Rapid Test Kit (Immunochromatography)" 
* #2812 ^property[0].code = #Relationships 
* #2812 ^property[0].valueString = "$.t..ma" 
* #2812 ^property[1].code = #hints 
* #2812 ^property[1].valueString = "T^E^B" 
* #2853 "GenSure Biotech Inc., GenSure COVID-19 Antigen Rapid Test Kit"
* #2853 ^definition = GenSure Biotech Inc., GenSure COVID-19 Antigen Rapid Test Kit
* #2853 ^designation[0].language = #de-AT 
* #2853 ^designation[0].value = "GenSure Biotech Inc., GenSure COVID-19 Antigen Rapid Test Kit" 
* #2853 ^property[0].code = #Relationships 
* #2853 ^property[0].valueString = "$.t..ma" 
* #2853 ^property[1].code = #hints 
* #2853 ^property[1].valueString = "T^E^B" 
* #2858 "Bioscience (Tianjin) Diagnostic Technology Co.,Ltd, Novel Coronavirus (2019-nCoV) Antigen Rapid Detection"
* #2858 ^definition = Bioscience (Tianjin) Diagnostic Technology Co.,Ltd, Novel Coronavirus (2019-nCoV) Antigen Rapid Detection
* #2858 ^designation[0].language = #de-AT 
* #2858 ^designation[0].value = "Bioscience (Tianjin) Diagnostic Technology Co.,Ltd, Novel Coronavirus (2019-nCoV) Antigen Rapid Detection" 
* #2858 ^property[0].code = #Relationships 
* #2858 ^property[0].valueString = "$.t..ma" 
* #2858 ^property[1].code = #hints 
* #2858 ^property[1].valueString = "T^E^B" 
* #2862 "Hangzhou Funworld Biotech Co., Ltd, SARS-CoV-2 Antigen Rapid Test Device"
* #2862 ^definition = Hangzhou Funworld Biotech Co., Ltd, SARS-CoV-2 Antigen Rapid Test Device
* #2862 ^designation[0].language = #de-AT 
* #2862 ^designation[0].value = "Hangzhou Funworld Biotech Co., Ltd, SARS-CoV-2 Antigen Rapid Test Device" 
* #2862 ^property[0].code = #Relationships 
* #2862 ^property[0].valueString = "$.t..ma" 
* #2862 ^property[1].code = #hints 
* #2862 ^property[1].valueString = "T^E^B" 
* #2866 "Lifecosm Biotech Limited, COVID-19 Antigen Test Cassette"
* #2866 ^definition = Lifecosm Biotech Limited, COVID-19 Antigen Test Cassette
* #2866 ^designation[0].language = #de-AT 
* #2866 ^designation[0].value = "Lifecosm Biotech Limited, COVID-19 Antigen Test Cassette" 
* #2866 ^property[0].code = #Relationships 
* #2866 ^property[0].valueString = "$.t..ma" 
* #2866 ^property[1].code = #hints 
* #2866 ^property[1].valueString = "T^E^B" 
* #2885 "Hangzhou GENESIS Biodetection and Biocontrol CO.,LTD, KaiBiLi COVID-19 Antigen Pro"
* #2885 ^definition = Hangzhou GENESIS Biodetection and Biocontrol CO.,LTD, KaiBiLi COVID-19 Antigen Pro
* #2885 ^designation[0].language = #de-AT 
* #2885 ^designation[0].value = "Hangzhou GENESIS Biodetection and Biocontrol CO.,LTD, KaiBiLi COVID-19 Antigen Pro" 
* #2885 ^property[0].code = #Relationships 
* #2885 ^property[0].valueString = "$.t..ma" 
* #2885 ^property[1].code = #hints 
* #2885 ^property[1].valueString = "T^E^B" 
* #2941 "Shenzhen Kingfocus Biomedical Engineering Co., Ltd., COVID-19 Antigen Detection Kit (Quantum Dots-Based Immunofluorescence Chromatography)"
* #2941 ^definition = Shenzhen Kingfocus Biomedical Engineering Co., Ltd., COVID-19 Antigen Detection Kit (Quantum Dots-Based Immunofluorescence Chromatography)
* #2941 ^designation[0].language = #de-AT 
* #2941 ^designation[0].value = "Shenzhen Kingfocus Biomedical Engineering Co., Ltd., COVID-19 Antigen Detection Kit (Quantum Dots-Based Immunofluorescence Chromatography)" 
* #2941 ^property[0].code = #Relationships 
* #2941 ^property[0].valueString = "$.t..ma" 
* #2941 ^property[1].code = #hints 
* #2941 ^property[1].valueString = "T^E^B" 
* #2942 "Hangzhou Zheda Dixun Biological Gene Engineering Co., Ltd., SARS-CoV-2 Nucleocapsid (N) Antigen Rapid Test Cassette (Swab)"
* #2942 ^definition = Hangzhou Zheda Dixun Biological Gene Engineering Co., Ltd., SARS-CoV-2 Nucleocapsid (N) Antigen Rapid Test Cassette (Swab)
* #2942 ^designation[0].language = #de-AT 
* #2942 ^designation[0].value = "Hangzhou Zheda Dixun Biological Gene Engineering Co., Ltd., SARS-CoV-2 Nucleocapsid (N) Antigen Rapid Test Cassette (Swab)" 
* #2942 ^property[0].code = #Relationships 
* #2942 ^property[0].valueString = "$.t..ma" 
* #2942 ^property[1].code = #hints 
* #2942 ^property[1].valueString = "T^E^B" 
* #2963 "Jiangxi Province JinHuan Medical Instrument Co., LTD., DREHA Novel Coronavirus (SARS-CoV-2) Antigen Rapid Detection Kit"
* #2963 ^definition = Jiangxi Province JinHuan Medical Instrument Co., LTD., DREHA Novel Coronavirus (SARS-CoV-2) Antigen Rapid Detection Kit
* #2963 ^designation[0].language = #de-AT 
* #2963 ^designation[0].value = "Jiangxi Province JinHuan Medical Instrument Co., LTD., DREHA Novel Coronavirus (SARS-CoV-2) Antigen Rapid Detection Kit" 
* #2963 ^property[0].code = #Relationships 
* #2963 ^property[0].valueString = "$.t..ma" 
* #2963 ^property[1].code = #hints 
* #2963 ^property[1].valueString = "T^E^B" 
* #2979 "Hangzhou Jucheng Medical Products Co., Ltd, SARS-CoV-2 Ag Rapid Test Kit"
* #2979 ^definition = Hangzhou Jucheng Medical Products Co., Ltd, SARS-CoV-2 Ag Rapid Test Kit
* #2979 ^designation[0].language = #de-AT 
* #2979 ^designation[0].value = "Hangzhou Jucheng Medical Products Co., Ltd, SARS-CoV-2 Ag Rapid Test Kit" 
* #2979 ^property[0].code = #Relationships 
* #2979 ^property[0].valueString = "$.t..ma" 
* #2979 ^property[1].code = #hints 
* #2979 ^property[1].valueString = "T^E^B" 
* #3015 "Suzhou Soochow University Saier Immuno Biotech Co., Ltd., InstantSure Covid-19 Ag CARD"
* #3015 ^definition = Suzhou Soochow University Saier Immuno Biotech Co., Ltd., InstantSure Covid-19 Ag CARD
* #3015 ^designation[0].language = #de-AT 
* #3015 ^designation[0].value = "Suzhou Soochow University Saier Immuno Biotech Co., Ltd., InstantSure Covid-19 Ag CARD" 
* #3015 ^property[0].code = #Relationships 
* #3015 ^property[0].valueString = "$.t..ma" 
* #3015 ^property[1].code = #hints 
* #3015 ^property[1].valueString = "T^E^B" 
* #3093 "TBG BIOTECHNOLOGY XIAMEN INC., SARS-CoV-2 Antigen Rapid Test (Nasal Swab)"
* #3093 ^definition = TBG BIOTECHNOLOGY XIAMEN INC., SARS-CoV-2 Antigen Rapid Test (Nasal Swab)
* #3093 ^designation[0].language = #de-AT 
* #3093 ^designation[0].value = "TBG BIOTECHNOLOGY XIAMEN INC., SARS-CoV-2 Antigen Rapid Test (Nasal Swab)" 
* #3093 ^property[0].code = #Relationships 
* #3093 ^property[0].valueString = "$.t..ma" 
* #3093 ^property[1].code = #hints 
* #3093 ^property[1].valueString = "T^E^B" 
* #3099 "BioDetect (Xiamen) Biotechnology Co., Ltd., RAPID SARS-COV-2 ANTIGEN TEST CARD"
* #3099 ^definition = BioDetect (Xiamen) Biotechnology Co., Ltd., RAPID SARS-COV-2 ANTIGEN TEST CARD
* #3099 ^designation[0].language = #de-AT 
* #3099 ^designation[0].value = "BioDetect (Xiamen) Biotechnology Co., Ltd., RAPID SARS-COV-2 ANTIGEN TEST CARD" 
* #3099 ^property[0].code = #Relationships 
* #3099 ^property[0].valueString = "$.t..ma" 
* #3099 ^property[1].code = #hints 
* #3099 ^property[1].valueString = "T^E^B" 
* #344 "SD BIOSENSOR Inc., STANDARD F COVID-19 Ag FIA"
* #344 ^definition = SD BIOSENSOR Inc., STANDARD F COVID-19 Ag FIA
* #344 ^designation[0].language = #de-AT 
* #344 ^designation[0].value = "SD BIOSENSOR Inc., STANDARD F COVID-19 Ag FIA" 
* #344 ^property[0].code = #Relationships 
* #344 ^property[0].valueString = "$.t..ma" 
* #344 ^property[1].code = #hints 
* #344 ^property[1].valueString = "T^E^B" 
* #345 "SD BIOSENSOR Inc., STANDARD Q COVID-19 Ag Test"
* #345 ^definition = SD BIOSENSOR Inc., STANDARD Q COVID-19 Ag Test
* #345 ^designation[0].language = #de-AT 
* #345 ^designation[0].value = "SD BIOSENSOR Inc., STANDARD Q COVID-19 Ag Test" 
* #345 ^property[0].code = #Relationships 
* #345 ^property[0].valueString = "$.t..ma" 
* #345 ^property[1].code = #hints 
* #345 ^property[1].valueString = "T^E^B" 
* #768 "ArcDia International Ltd, mariPOC SARS-CoV-2"
* #768 ^definition = ArcDia International Ltd, mariPOC SARS-CoV-2
* #768 ^designation[0].language = #de-AT 
* #768 ^designation[0].value = "ArcDia International Ltd, mariPOC SARS-CoV-2" 
* #768 ^property[0].code = #Relationships 
* #768 ^property[0].valueString = "$.t..ma" 
* #768 ^property[1].code = #hints 
* #768 ^property[1].valueString = "T^E^B" 
* #770 "Assure Tech. (Hangzhou) Co., Ltd., ECOTEST COVID-19 Antigen Rapid Test Device"
* #770 ^definition = Assure Tech. (Hangzhou) Co., Ltd., ECOTEST COVID-19 Antigen Rapid Test Device
* #770 ^designation[0].language = #de-AT 
* #770 ^designation[0].value = "Assure Tech. (Hangzhou) Co., Ltd., ECOTEST COVID-19 Antigen Rapid Test Device" 
* #770 ^property[0].code = #Relationships 
* #770 ^property[0].valueString = "$.t..ma" 
* #770 ^property[1].code = #hints 
* #770 ^property[1].valueString = "T^E^B" 
* #82334004 "Indeterminate"
* #82334004 ^definition = Indeterminate
* #82334004 ^designation[0].language = #de-AT 
* #82334004 ^designation[0].value = "nicht bestimmbar" 
* #82334004 ^property[0].code = #Relationships 
* #82334004 ^property[0].valueString = "$.t..tr" 
* #840539006 "SARSCOV2"
* #840539006 ^definition = COVID-19
* #840539006 ^designation[0].language = #de-AT 
* #840539006 ^designation[0].value = "COVID-19" 
* #840539006 ^property[0].code = #Relationships 
* #840539006 ^property[0].valueString = "$.tg" 
* #840539006 ^property[1].code = #hints 
* #840539006 ^property[1].valueString = "T^E^B" 
* #AC "Ascension Island"
* #AC ^definition = Ascension Island
* #AC ^designation[0].language = #de-AT 
* #AC ^designation[0].value = "Ascension (verwaltet von St. Helena, reserviert für UPU und ITU)" 
* #AC ^property[0].code = #Relationships 
* #AC ^property[0].valueString = "$.co" 
* #AC ^property[1].code = #hints 
* #AC ^property[1].valueString = "T^E^B" 
* #AD "Andorra"
* #AD ^definition = Andorra
* #AD ^designation[0].language = #de-AT 
* #AD ^designation[0].value = "Andorra" 
* #AD ^property[0].code = #Relationships 
* #AD ^property[0].valueString = "$.co" 
* #AD ^property[1].code = #hints 
* #AD ^property[1].valueString = "T^E^B" 
* #AE "United Arab Emirates (the)"
* #AE ^definition = United Arab Emirates (the)
* #AE ^designation[0].language = #de-AT 
* #AE ^designation[0].value = "Vereinigte Arab.Emirate" 
* #AE ^property[0].code = #Relationships 
* #AE ^property[0].valueString = "$.co" 
* #AE ^property[1].code = #hints 
* #AE ^property[1].valueString = "T^E^B" 
* #AF "Afghanistan"
* #AF ^definition = Afghanistan
* #AF ^designation[0].language = #de-AT 
* #AF ^designation[0].value = "Afghanistan" 
* #AF ^property[0].code = #Relationships 
* #AF ^property[0].valueString = "$.co" 
* #AF ^property[1].code = #hints 
* #AF ^property[1].valueString = "T^E^B" 
* #AG "Antigua and Barbuda"
* #AG ^definition = Antigua and Barbuda
* #AG ^designation[0].language = #de-AT 
* #AG ^designation[0].value = "Antigua und Barbuda" 
* #AG ^property[0].code = #Relationships 
* #AG ^property[0].valueString = "$.co" 
* #AG ^property[1].code = #hints 
* #AG ^property[1].valueString = "T^E^B" 
* #AI "Anguilla"
* #AI ^definition = Anguilla
* #AI ^designation[0].language = #de-AT 
* #AI ^designation[0].value = "Anguilla" 
* #AI ^property[0].code = #Relationships 
* #AI ^property[0].valueString = "$.co" 
* #AI ^property[1].code = #hints 
* #AI ^property[1].valueString = "T^E^B" 
* #AL "Albania"
* #AL ^definition = Albania
* #AL ^designation[0].language = #de-AT 
* #AL ^designation[0].value = "Albanien" 
* #AL ^property[0].code = #Relationships 
* #AL ^property[0].valueString = "$.co" 
* #AL ^property[1].code = #hints 
* #AL ^property[1].valueString = "T^E^B" 
* #AM "Armenia"
* #AM ^definition = Armenia
* #AM ^designation[0].language = #de-AT 
* #AM ^designation[0].value = "Armenien" 
* #AM ^property[0].code = #Relationships 
* #AM ^property[0].valueString = "$.co" 
* #AM ^property[1].code = #hints 
* #AM ^property[1].valueString = "T^E^B" 
* #AN "Netherlands Antilles"
* #AN ^definition = Netherlands Antilles
* #AN ^designation[0].language = #de-AT 
* #AN ^designation[0].value = "Niedl.Antillen" 
* #AN ^property[0].code = #Relationships 
* #AN ^property[0].valueString = "$.co" 
* #AN ^property[1].code = #hints 
* #AN ^property[1].valueString = "T^E^B" 
* #AO "Angola"
* #AO ^definition = Angola
* #AO ^designation[0].language = #de-AT 
* #AO ^designation[0].value = "Angola" 
* #AO ^property[0].code = #Relationships 
* #AO ^property[0].valueString = "$.co" 
* #AO ^property[1].code = #hints 
* #AO ^property[1].valueString = "T^E^B" 
* #AQ "Antarctica"
* #AQ ^definition = Antarctica
* #AQ ^designation[0].language = #de-AT 
* #AQ ^designation[0].value = "Antarktis" 
* #AQ ^property[0].code = #Relationships 
* #AQ ^property[0].valueString = "$.co" 
* #AQ ^property[1].code = #hints 
* #AQ ^property[1].valueString = "T^E^B" 
* #AR "Argentina"
* #AR ^definition = Argentina
* #AR ^designation[0].language = #de-AT 
* #AR ^designation[0].value = "Argentinien" 
* #AR ^property[0].code = #Relationships 
* #AR ^property[0].valueString = "$.co" 
* #AR ^property[1].code = #hints 
* #AR ^property[1].valueString = "T^E^B" 
* #AS "American Samoa"
* #AS ^definition = American Samoa
* #AS ^designation[0].language = #de-AT 
* #AS ^designation[0].value = "Amerikan.Samoa" 
* #AS ^property[0].code = #Relationships 
* #AS ^property[0].valueString = "$.co" 
* #AS ^property[1].code = #hints 
* #AS ^property[1].valueString = "T^E^B" 
* #AT "Austria"
* #AT ^definition = Austria
* #AT ^designation[0].language = #de-AT 
* #AT ^designation[0].value = "Österreich" 
* #AT ^property[0].code = #Relationships 
* #AT ^property[0].valueString = "$.co" 
* #AT ^property[1].code = #hints 
* #AT ^property[1].valueString = "T^E^B" 
* #AU "Australia"
* #AU ^definition = Australia
* #AU ^designation[0].language = #de-AT 
* #AU ^designation[0].value = "Australien" 
* #AU ^property[0].code = #Relationships 
* #AU ^property[0].valueString = "$.co" 
* #AU ^property[1].code = #hints 
* #AU ^property[1].valueString = "T^E^B" 
* #AW "Aruba"
* #AW ^definition = Aruba
* #AW ^designation[0].language = #de-AT 
* #AW ^designation[0].value = "Aruba" 
* #AW ^property[0].code = #Relationships 
* #AW ^property[0].valueString = "$.co" 
* #AW ^property[1].code = #hints 
* #AW ^property[1].valueString = "T^E^B" 
* #AX "Åland Islands"
* #AX ^definition = Åland Islands
* #AX ^designation[0].language = #de-AT 
* #AX ^designation[0].value = "Alandinseln" 
* #AX ^property[0].code = #Relationships 
* #AX ^property[0].valueString = "$.co" 
* #AX ^property[1].code = #hints 
* #AX ^property[1].valueString = "T^E^B" 
* #AZ "Azerbaijan"
* #AZ ^definition = Azerbaijan
* #AZ ^designation[0].language = #de-AT 
* #AZ ^designation[0].value = "Aserbaidschan" 
* #AZ ^property[0].code = #Relationships 
* #AZ ^property[0].valueString = "$.co" 
* #AZ ^property[1].code = #hints 
* #AZ ^property[1].valueString = "T^E^B" 
* #AZD2816 "AZD2816"
* #AZD2816 ^definition = AZD2816
* #AZD2816 ^designation[0].language = #de-AT 
* #AZD2816 ^designation[0].value = "COVID-19-Impfstoff AZD2816 (AstraZeneca AB)" 
* #AZD2816 ^property[0].code = #Relationships 
* #AZD2816 ^property[0].valueString = "$.v..mp" 
* #AZD2816 ^property[1].code = #hints 
* #AZD2816 ^property[1].valueString = "T^E^B" 
* #Abdala "Abdala"
* #Abdala ^definition = Abdala
* #Abdala ^designation[0].language = #de-AT 
* #Abdala ^designation[0].value = "COVID-19-Impfstoff Abdala (Center for Genetic Engineering and Biotechnology)" 
* #Abdala ^property[0].code = #Relationships 
* #Abdala ^property[0].valueString = "$.v..mp" 
* #Abdala ^property[1].code = #hints 
* #Abdala ^property[1].valueString = "T^E^B" 
* #BA "Bosnia and Herzegovina"
* #BA ^definition = Bosnia and Herzegovina
* #BA ^designation[0].language = #de-AT 
* #BA ^designation[0].value = "Bosnien und Herzegowina" 
* #BA ^property[0].code = #Relationships 
* #BA ^property[0].valueString = "$.co" 
* #BA ^property[1].code = #hints 
* #BA ^property[1].valueString = "T^E^B" 
* #BB "Barbados"
* #BB ^definition = Barbados
* #BB ^designation[0].language = #de-AT 
* #BB ^designation[0].value = "Barbados" 
* #BB ^property[0].code = #Relationships 
* #BB ^property[0].valueString = "$.co" 
* #BB ^property[1].code = #hints 
* #BB ^property[1].valueString = "T^E^B" 
* #BBIBP-CorV "BBIBP-CorV"
* #BBIBP-CorV ^definition = BBIBP-CorV
* #BBIBP-CorV ^designation[0].language = #de-AT 
* #BBIBP-CorV ^designation[0].value = "COVID-19-Impfstoff BBIBP-CorV (Sinopharm)" 
* #BBIBP-CorV ^property[0].code = #Relationships 
* #BBIBP-CorV ^property[0].valueString = "$.v..mp" 
* #BBIBP-CorV ^property[1].code = #hints 
* #BBIBP-CorV ^property[1].valueString = "T^E^B" 
* #BD "Bangladesh"
* #BD ^definition = Bangladesh
* #BD ^designation[0].language = #de-AT 
* #BD ^designation[0].value = "Bangladesch" 
* #BD ^property[0].code = #Relationships 
* #BD ^property[0].valueString = "$.co" 
* #BD ^property[1].code = #hints 
* #BD ^property[1].valueString = "T^E^B" 
* #BE "Belgium"
* #BE ^definition = Belgium
* #BE ^designation[0].language = #de-AT 
* #BE ^designation[0].value = "Belgien" 
* #BE ^property[0].code = #Relationships 
* #BE ^property[0].valueString = "$.co" 
* #BE ^property[1].code = #hints 
* #BE ^property[1].valueString = "T^E^B" 
* #BF "Burkina Faso"
* #BF ^definition = Burkina Faso
* #BF ^designation[0].language = #de-AT 
* #BF ^designation[0].value = "Burkina Faso" 
* #BF ^property[0].code = #Relationships 
* #BF ^property[0].valueString = "$.co" 
* #BF ^property[1].code = #hints 
* #BF ^property[1].valueString = "T^E^B" 
* #BG "Bulgaria"
* #BG ^definition = Bulgaria
* #BG ^designation[0].language = #de-AT 
* #BG ^designation[0].value = "Bulgarien" 
* #BG ^property[0].code = #Relationships 
* #BG ^property[0].valueString = "$.co" 
* #BG ^property[1].code = #hints 
* #BG ^property[1].valueString = "T^E^B" 
* #BH "Bahrain"
* #BH ^definition = Bahrain
* #BH ^designation[0].language = #de-AT 
* #BH ^designation[0].value = "Bahrain" 
* #BH ^property[0].code = #Relationships 
* #BH ^property[0].valueString = "$.co" 
* #BH ^property[1].code = #hints 
* #BH ^property[1].valueString = "T^E^B" 
* #BI "Burundi"
* #BI ^definition = Burundi
* #BI ^designation[0].language = #de-AT 
* #BI ^designation[0].value = "Burundi" 
* #BI ^property[0].code = #Relationships 
* #BI ^property[0].valueString = "$.co" 
* #BI ^property[1].code = #hints 
* #BI ^property[1].valueString = "T^E^B" 
* #BJ "Benin"
* #BJ ^definition = Benin
* #BJ ^designation[0].language = #de-AT 
* #BJ ^designation[0].value = "Benin" 
* #BJ ^property[0].code = #Relationships 
* #BJ ^property[0].valueString = "$.co" 
* #BJ ^property[1].code = #hints 
* #BJ ^property[1].valueString = "T^E^B" 
* #BL "Saint Barthélemy"
* #BL ^definition = Saint Barthélemy
* #BL ^designation[0].language = #de-AT 
* #BL ^designation[0].value = "Saint-Barthélemy" 
* #BL ^property[0].code = #Relationships 
* #BL ^property[0].valueString = "$.co" 
* #BL ^property[1].code = #hints 
* #BL ^property[1].valueString = "T^E^B" 
* #BM "Bermuda"
* #BM ^definition = Bermuda
* #BM ^designation[0].language = #de-AT 
* #BM ^designation[0].value = "Bermuda" 
* #BM ^property[0].code = #Relationships 
* #BM ^property[0].valueString = "$.co" 
* #BM ^property[1].code = #hints 
* #BM ^property[1].valueString = "T^E^B" 
* #BN "Brunei Darussalam"
* #BN ^definition = Brunei Darussalam
* #BN ^designation[0].language = #de-AT 
* #BN ^designation[0].value = "Brunei Darussalam" 
* #BN ^property[0].code = #Relationships 
* #BN ^property[0].valueString = "$.co" 
* #BN ^property[1].code = #hints 
* #BN ^property[1].valueString = "T^E^B" 
* #BO "Bolivia (Plurinational State of)"
* #BO ^definition = Bolivia (Plurinational State of)
* #BO ^designation[0].language = #de-AT 
* #BO ^designation[0].value = "Bolivien" 
* #BO ^property[0].code = #Relationships 
* #BO ^property[0].valueString = "$.co" 
* #BO ^property[1].code = #hints 
* #BO ^property[1].valueString = "T^E^B" 
* #BQ "Bonaire, Sint Eustatius and Saba"
* #BQ ^definition = Bonaire, Sint Eustatius and Saba
* #BQ ^designation[0].language = #de-AT 
* #BQ ^designation[0].value = "Bonaire,  Saba,  Sint Eustatius" 
* #BQ ^property[0].code = #Relationships 
* #BQ ^property[0].valueString = "$.co" 
* #BQ ^property[1].code = #hints 
* #BQ ^property[1].valueString = "T^E^B" 
* #BR "Brazil"
* #BR ^definition = Brazil
* #BR ^designation[0].language = #de-AT 
* #BR ^designation[0].value = "Brasilien" 
* #BR ^property[0].code = #Relationships 
* #BR ^property[0].valueString = "$.co" 
* #BR ^property[1].code = #hints 
* #BR ^property[1].valueString = "T^E^B" 
* #BS "Bahamas (the)"
* #BS ^definition = Bahamas (the)
* #BS ^designation[0].language = #de-AT 
* #BS ^designation[0].value = "Bahamas" 
* #BS ^property[0].code = #Relationships 
* #BS ^property[0].valueString = "$.co" 
* #BS ^property[1].code = #hints 
* #BS ^property[1].valueString = "T^E^B" 
* #BT "Bhutan"
* #BT ^definition = Bhutan
* #BT ^designation[0].language = #de-AT 
* #BT ^designation[0].value = "Bhutan" 
* #BT ^property[0].code = #Relationships 
* #BT ^property[0].valueString = "$.co" 
* #BT ^property[1].code = #hints 
* #BT ^property[1].valueString = "T^E^B" 
* #BV "Bouvet Island"
* #BV ^definition = Bouvet Island
* #BV ^designation[0].language = #de-AT 
* #BV ^designation[0].value = "Bouvetinsel" 
* #BV ^property[0].code = #Relationships 
* #BV ^property[0].valueString = "$.co" 
* #BV ^property[1].code = #hints 
* #BV ^property[1].valueString = "T^E^B" 
* #BW "Botswana"
* #BW ^definition = Botswana
* #BW ^designation[0].language = #de-AT 
* #BW ^designation[0].value = "Botsuana" 
* #BW ^property[0].code = #Relationships 
* #BW ^property[0].valueString = "$.co" 
* #BW ^property[1].code = #hints 
* #BW ^property[1].valueString = "T^E^B" 
* #BY "Belarus"
* #BY ^definition = Belarus
* #BY ^designation[0].language = #de-AT 
* #BY ^designation[0].value = "Belarus" 
* #BY ^property[0].code = #Relationships 
* #BY ^property[0].valueString = "$.co" 
* #BY ^property[1].code = #hints 
* #BY ^property[1].valueString = "T^E^B" 
* #BZ "Belize"
* #BZ ^definition = Belize
* #BZ ^designation[0].language = #de-AT 
* #BZ ^designation[0].value = "Belize" 
* #BZ ^property[0].code = #Relationships 
* #BZ ^property[0].valueString = "$.co" 
* #BZ ^property[1].code = #hints 
* #BZ ^property[1].valueString = "T^E^B" 
* #Bharat-Biotech "Bharat Biotech"
* #Bharat-Biotech ^definition = Bharat Biotech
* #Bharat-Biotech ^designation[0].language = #de-AT 
* #Bharat-Biotech ^designation[0].value = "Bharat Biotech" 
* #Bharat-Biotech ^property[0].code = #Relationships 
* #Bharat-Biotech ^property[0].valueString = "$.v..ma" 
* #Bharat-Biotech ^property[1].code = #hints 
* #Bharat-Biotech ^property[1].valueString = "T^E^B" 
* #CA "Canada"
* #CA ^definition = Canada
* #CA ^designation[0].language = #de-AT 
* #CA ^designation[0].value = "Kanada" 
* #CA ^property[0].code = #Relationships 
* #CA ^property[0].valueString = "$.co" 
* #CA ^property[1].code = #hints 
* #CA ^property[1].valueString = "T^E^B" 
* #CC "Cocos (Keeling) Islands (the)"
* #CC ^definition = Cocos (Keeling) Islands (the)
* #CC ^designation[0].language = #de-AT 
* #CC ^designation[0].value = "Kokosinseln(Au)" 
* #CC ^property[0].code = #Relationships 
* #CC ^property[0].valueString = "$.co" 
* #CC ^property[1].code = #hints 
* #CC ^property[1].valueString = "T^E^B" 
* #CD "Congo (the Democratic Republic of the)"
* #CD ^definition = Congo (the Democratic Republic of the)
* #CD ^designation[0].language = #de-AT 
* #CD ^designation[0].value = "Kongo,Dem.Rep.(Kinshasa)" 
* #CD ^property[0].code = #Relationships 
* #CD ^property[0].valueString = "$.co" 
* #CD ^property[1].code = #hints 
* #CD ^property[1].valueString = "T^E^B" 
* #CF "Central African Republic (the)"
* #CF ^definition = Central African Republic (the)
* #CF ^designation[0].language = #de-AT 
* #CF ^designation[0].value = "Zentralafrikan.Republik" 
* #CF ^property[0].code = #Relationships 
* #CF ^property[0].valueString = "$.co" 
* #CF ^property[1].code = #hints 
* #CF ^property[1].valueString = "T^E^B" 
* #CG "Congo (the)"
* #CG ^definition = Congo (the)
* #CG ^designation[0].language = #de-AT 
* #CG ^designation[0].value = "Kongo,Rep. (Brazzaville)" 
* #CG ^property[0].code = #Relationships 
* #CG ^property[0].valueString = "$.co" 
* #CG ^property[1].code = #hints 
* #CG ^property[1].valueString = "T^E^B" 
* #CH "Switzerland"
* #CH ^definition = Switzerland
* #CH ^designation[0].language = #de-AT 
* #CH ^designation[0].value = "Schweiz" 
* #CH ^property[0].code = #Relationships 
* #CH ^property[0].valueString = "$.co" 
* #CH ^property[1].code = #hints 
* #CH ^property[1].valueString = "T^E^B" 
* #CI "Côte d'Ivoire"
* #CI ^definition = Côte d'Ivoire
* #CI ^designation[0].language = #de-AT 
* #CI ^designation[0].value = "Cote dIvoire" 
* #CI ^property[0].code = #Relationships 
* #CI ^property[0].valueString = "$.co" 
* #CI ^property[1].code = #hints 
* #CI ^property[1].valueString = "T^E^B" 
* #CIGB "Center for Genetic Engineering and Biotechnology (CIGB)"
* #CIGB ^definition = Center for Genetic Engineering and Biotechnology (CIGB)
* #CIGB ^designation[0].language = #de-AT 
* #CIGB ^designation[0].value = "Center for Genetic Engineering and Biotechnology (CIGB)" 
* #CIGB ^property[0].code = #Relationships 
* #CIGB ^property[0].valueString = "$.v..ma" 
* #CIGB ^property[1].code = #hints 
* #CIGB ^property[1].valueString = "T^E^B" 
* #CK "Cook Islands (the)"
* #CK ^definition = Cook Islands (the)
* #CK ^designation[0].language = #de-AT 
* #CK ^designation[0].value = "Cookinseln" 
* #CK ^property[0].code = #Relationships 
* #CK ^property[0].valueString = "$.co" 
* #CK ^property[1].code = #hints 
* #CK ^property[1].valueString = "T^E^B" 
* #CL "Chile"
* #CL ^definition = Chile
* #CL ^designation[0].language = #de-AT 
* #CL ^designation[0].value = "Chile" 
* #CL ^property[0].code = #Relationships 
* #CL ^property[0].valueString = "$.co" 
* #CL ^property[1].code = #hints 
* #CL ^property[1].valueString = "T^E^B" 
* #CM "Cameroon"
* #CM ^definition = Cameroon
* #CM ^designation[0].language = #de-AT 
* #CM ^designation[0].value = "Kamerun" 
* #CM ^property[0].code = #Relationships 
* #CM ^property[0].valueString = "$.co" 
* #CM ^property[1].code = #hints 
* #CM ^property[1].valueString = "T^E^B" 
* #CN "China"
* #CN ^definition = China
* #CN ^designation[0].language = #de-AT 
* #CN ^designation[0].value = "China" 
* #CN ^property[0].code = #Relationships 
* #CN ^property[0].valueString = "$.co" 
* #CN ^property[1].code = #hints 
* #CN ^property[1].valueString = "T^E^B" 
* #CO "Colombia"
* #CO ^definition = Colombia
* #CO ^designation[0].language = #de-AT 
* #CO ^designation[0].value = "Kolumbien" 
* #CO ^property[0].code = #Relationships 
* #CO ^property[0].valueString = "$.co" 
* #CO ^property[1].code = #hints 
* #CO ^property[1].valueString = "T^E^B" 
* #CR "Costa Rica"
* #CR ^definition = Costa Rica
* #CR ^designation[0].language = #de-AT 
* #CR ^designation[0].value = "Costa Rica" 
* #CR ^property[0].code = #Relationships 
* #CR ^property[0].valueString = "$.co" 
* #CR ^property[1].code = #hints 
* #CR ^property[1].valueString = "T^E^B" 
* #CU "Cuba"
* #CU ^definition = Cuba
* #CU ^designation[0].language = #de-AT 
* #CU ^designation[0].value = "Kuba" 
* #CU ^property[0].code = #Relationships 
* #CU ^property[0].valueString = "$.co" 
* #CU ^property[1].code = #hints 
* #CU ^property[1].valueString = "T^E^B" 
* #CV "Cabo Verde"
* #CV ^definition = Cabo Verde
* #CV ^designation[0].language = #de-AT 
* #CV ^designation[0].value = "Kap Verde" 
* #CV ^property[0].code = #Relationships 
* #CV ^property[0].valueString = "$.co" 
* #CV ^property[1].code = #hints 
* #CV ^property[1].valueString = "T^E^B" 
* #CVnCoV "CVnCoV"
* #CVnCoV ^definition = CVnCoV
* #CVnCoV ^designation[0].language = #de-AT 
* #CVnCoV ^designation[0].value = "COVID-19-Impfstoff CVnCoV (CureVac)" 
* #CVnCoV ^property[0].code = #Relationships 
* #CVnCoV ^property[0].valueString = "$.v..mp" 
* #CVnCoV ^property[1].code = #hints 
* #CVnCoV ^property[1].valueString = "T^E^B" 
* #CX "Christmas Island"
* #CX ^definition = Christmas Island
* #CX ^designation[0].language = #de-AT 
* #CX ^designation[0].value = "Weihnachtsinsel" 
* #CX ^property[0].code = #Relationships 
* #CX ^property[0].valueString = "$.co" 
* #CX ^property[1].code = #hints 
* #CX ^property[1].valueString = "T^E^B" 
* #CY "Cyprus"
* #CY ^definition = Cyprus
* #CY ^designation[0].language = #de-AT 
* #CY ^designation[0].value = "Zypern" 
* #CY ^property[0].code = #Relationships 
* #CY ^property[0].valueString = "$.co" 
* #CY ^property[1].code = #hints 
* #CY ^property[1].valueString = "T^E^B" 
* #CZ "Czechia"
* #CZ ^definition = Czechia
* #CZ ^designation[0].language = #de-AT 
* #CZ ^designation[0].value = "Tschechische Republik" 
* #CZ ^property[0].code = #Relationships 
* #CZ ^property[0].valueString = "$.co" 
* #CZ ^property[1].code = #hints 
* #CZ ^property[1].valueString = "T^E^B" 
* #Chumakov-Federal-Scientific-Center "Chumakov Federal Scientific Center for Research and Development of Immune-and-Biological Products"
* #Chumakov-Federal-Scientific-Center ^definition = Chumakov Federal Scientific Center for Research and Development of Immune-and-Biological Products
* #Chumakov-Federal-Scientific-Center ^designation[0].language = #de-AT 
* #Chumakov-Federal-Scientific-Center ^designation[0].value = "Chumakov Federal Scientific Center for Research and Development of Immune-and-Biological Products" 
* #Chumakov-Federal-Scientific-Center ^property[0].code = #Relationships 
* #Chumakov-Federal-Scientific-Center ^property[0].valueString = "$.v..ma" 
* #Chumakov-Federal-Scientific-Center ^property[1].code = #hints 
* #Chumakov-Federal-Scientific-Center ^property[1].valueString = "T^E^B" 
* #Convidecia "Convidecia"
* #Convidecia ^definition = Convidecia
* #Convidecia ^designation[0].language = #de-AT 
* #Convidecia ^designation[0].value = "COVID-19-Impfstoff Convidecia (CanSino Biologics)" 
* #Convidecia ^property[0].code = #Relationships 
* #Convidecia ^property[0].valueString = "$.v..mp" 
* #Convidecia ^property[1].code = #hints 
* #Convidecia ^property[1].valueString = "T^E^B" 
* #CoronaVac "CoronaVac"
* #CoronaVac ^definition = CoronaVac
* #CoronaVac ^designation[0].language = #de-AT 
* #CoronaVac ^designation[0].value = "COVID-19-Impfstoff CoronaVac (Sinovac Biotech)" 
* #CoronaVac ^property[0].code = #Relationships 
* #CoronaVac ^property[0].valueString = "$.v..mp" 
* #CoronaVac ^property[1].code = #hints 
* #CoronaVac ^property[1].valueString = "T^E^B" 
* #Covaxin "Covaxin (also known as BBV152 A, B, C)"
* #Covaxin ^definition = Covaxin (also known as BBV152 A, B, C)
* #Covaxin ^designation[0].language = #de-AT 
* #Covaxin ^designation[0].value = "COVID-19-Impfstoff Covaxin (Bharat Biotech)" 
* #Covaxin ^property[0].code = #Relationships 
* #Covaxin ^property[0].valueString = "$.v..mp" 
* #Covaxin ^property[1].code = #hints 
* #Covaxin ^property[1].valueString = "T^E^B" 
* #CoviVac "CoviVac"
* #CoviVac ^definition = CoviVac
* #CoviVac ^designation[0].language = #de-AT 
* #CoviVac ^designation[0].value = "COVID-19-Impfstoff CoviVac (Chumakov-Federal-Scientific-Center)" 
* #CoviVac ^property[0].code = #Relationships 
* #CoviVac ^property[0].valueString = "$.v..mp" 
* #CoviVac ^property[1].code = #hints 
* #CoviVac ^property[1].valueString = "T^E^B" 
* #Covid-19-adsorvida-inativada "Vacina adsorvida covid-19 (inativada)"
* #Covid-19-adsorvida-inativada ^definition = Vacina adsorvida covid-19 (inativada)
* #Covid-19-adsorvida-inativada ^designation[0].language = #de-AT 
* #Covid-19-adsorvida-inativada ^designation[0].value = "COVID-19-Impfstoff Vacina adsorvida covid-19 (inativada) (Instituto Butantan)" 
* #Covid-19-adsorvida-inativada ^property[0].code = #Relationships 
* #Covid-19-adsorvida-inativada ^property[0].valueString = "$.v..mp" 
* #Covid-19-adsorvida-inativada ^property[1].code = #hints 
* #Covid-19-adsorvida-inativada ^property[1].valueString = "T^E^B" 
* #Covid-19-recombinant "Covid-19 (recombinant)"
* #Covid-19-recombinant ^definition = Covid-19 (recombinant)
* #Covid-19-recombinant ^designation[0].language = #de-AT 
* #Covid-19-recombinant ^designation[0].value = "COVID-19-Impfstoff Covid-19 (recombinant) (Fiocruz)" 
* #Covid-19-recombinant ^property[0].code = #Relationships 
* #Covid-19-recombinant ^property[0].valueString = "$.v..mp" 
* #Covid-19-recombinant ^property[1].code = #hints 
* #Covid-19-recombinant ^property[1].valueString = "T^E^B" 
* #Covifenz "Covifenz"
* #Covifenz ^definition = Covifenz
* #Covifenz ^designation[0].language = #de-AT 
* #Covifenz ^designation[0].value = "COVID-19-Impfstoff Covifenz (Medicago Inc.)" 
* #Covifenz ^property[0].code = #Relationships 
* #Covifenz ^property[0].valueString = "$.v..mp" 
* #Covifenz ^property[1].code = #hints 
* #Covifenz ^property[1].valueString = "T^E^B" 
* #Covishield "Covishield (ChAdOx1_nCoV-19)"
* #Covishield ^definition = Covishield (ChAdOx1_nCoV-19)
* #Covishield ^designation[0].language = #de-AT 
* #Covishield ^designation[0].value = "COVID-19-Impfstoff Covishield (Serum Institute of India)" 
* #Covishield ^property[0].code = #Relationships 
* #Covishield ^property[0].valueString = "$.v..mp" 
* #Covishield ^property[1].code = #hints 
* #Covishield ^property[1].valueString = "T^E^B" 
* #Covovax "Covovax"
* #Covovax ^definition = Covovax
* #Covovax ^designation[0].language = #de-AT 
* #Covovax ^designation[0].value = "COVID-19-Impfstoff Covovax (Serum Institute Of India Private Limited)" 
* #Covovax ^property[0].code = #Relationships 
* #Covovax ^property[0].valueString = "$.v..mp" 
* #Covovax ^property[1].code = #hints 
* #Covovax ^property[1].valueString = "T^E^B" 
* #DE "Germany"
* #DE ^definition = Germany
* #DE ^designation[0].language = #de-AT 
* #DE ^designation[0].value = "Deutschland" 
* #DE ^property[0].code = #Relationships 
* #DE ^property[0].valueString = "$.co" 
* #DE ^property[1].code = #hints 
* #DE ^property[1].valueString = "T^E^B" 
* #DJ "Djibouti"
* #DJ ^definition = Djibouti
* #DJ ^designation[0].language = #de-AT 
* #DJ ^designation[0].value = "Dschibuti" 
* #DJ ^property[0].code = #Relationships 
* #DJ ^property[0].valueString = "$.co" 
* #DJ ^property[1].code = #hints 
* #DJ ^property[1].valueString = "T^E^B" 
* #DK "Denmark"
* #DK ^definition = Denmark
* #DK ^designation[0].language = #de-AT 
* #DK ^designation[0].value = "Dänemark" 
* #DK ^property[0].code = #Relationships 
* #DK ^property[0].valueString = "$.co" 
* #DK ^property[1].code = #hints 
* #DK ^property[1].valueString = "T^E^B" 
* #DM "Dominica"
* #DM ^definition = Dominica
* #DM ^designation[0].language = #de-AT 
* #DM ^designation[0].value = "Dominica" 
* #DM ^property[0].code = #Relationships 
* #DM ^property[0].valueString = "$.co" 
* #DM ^property[1].code = #hints 
* #DM ^property[1].valueString = "T^E^B" 
* #DO "Dominican Republic (the)"
* #DO ^definition = Dominican Republic (the)
* #DO ^designation[0].language = #de-AT 
* #DO ^designation[0].value = "Dominikanische Republik" 
* #DO ^property[0].code = #Relationships 
* #DO ^property[0].valueString = "$.co" 
* #DO ^property[1].code = #hints 
* #DO ^property[1].valueString = "T^E^B" 
* #DZ "Algeria"
* #DZ ^definition = Algeria
* #DZ ^designation[0].language = #de-AT 
* #DZ ^designation[0].value = "Algerien" 
* #DZ ^property[0].code = #Relationships 
* #DZ ^property[0].valueString = "$.co" 
* #DZ ^property[1].code = #hints 
* #DZ ^property[1].valueString = "T^E^B" 
* #EC "Ecuador"
* #EC ^definition = Ecuador
* #EC ^designation[0].language = #de-AT 
* #EC ^designation[0].value = "Ecuador" 
* #EC ^property[0].code = #Relationships 
* #EC ^property[0].valueString = "$.co" 
* #EC ^property[1].code = #hints 
* #EC ^property[1].valueString = "T^E^B" 
* #EE "Estonia"
* #EE ^definition = Estonia
* #EE ^designation[0].language = #de-AT 
* #EE ^designation[0].value = "Estland" 
* #EE ^property[0].code = #Relationships 
* #EE ^property[0].valueString = "$.co" 
* #EE ^property[1].code = #hints 
* #EE ^property[1].valueString = "T^E^B" 
* #EG "Egypt"
* #EG ^definition = Egypt
* #EG ^designation[0].language = #de-AT 
* #EG ^designation[0].value = "Ägypten" 
* #EG ^property[0].code = #Relationships 
* #EG ^property[0].valueString = "$.co" 
* #EG ^property[1].code = #hints 
* #EG ^property[1].valueString = "T^E^B" 
* #EH "Western Sahara"
* #EH ^definition = Western Sahara
* #EH ^designation[0].language = #de-AT 
* #EH ^designation[0].value = "Westsahara" 
* #EH ^property[0].code = #Relationships 
* #EH ^property[0].valueString = "$.co" 
* #EH ^property[1].code = #hints 
* #EH ^property[1].valueString = "T^E^B" 
* #ER "Eritrea"
* #ER ^definition = Eritrea
* #ER ^designation[0].language = #de-AT 
* #ER ^designation[0].value = "Eritrea" 
* #ER ^property[0].code = #Relationships 
* #ER ^property[0].valueString = "$.co" 
* #ER ^property[1].code = #hints 
* #ER ^property[1].valueString = "T^E^B" 
* #ES "Spain"
* #ES ^definition = Spain
* #ES ^designation[0].language = #de-AT 
* #ES ^designation[0].value = "Spanien" 
* #ES ^property[0].code = #Relationships 
* #ES ^property[0].valueString = "$.co" 
* #ES ^property[1].code = #hints 
* #ES ^property[1].valueString = "T^E^B" 
* #ET "Ethiopia"
* #ET ^definition = Ethiopia
* #ET ^designation[0].language = #de-AT 
* #ET ^designation[0].value = "Äthiopien" 
* #ET ^property[0].code = #Relationships 
* #ET ^property[0].valueString = "$.co" 
* #ET ^property[1].code = #hints 
* #ET ^property[1].valueString = "T^E^B" 
* #EU/1/20/1507 "Spikevax"
* #EU/1/20/1507 ^definition = Spikevax
* #EU/1/20/1507 ^designation[0].language = #de-AT 
* #EU/1/20/1507 ^designation[0].value = "COVID-19 Vaccine Moderna, Injektionsdispersion, COVID-19-mRNA-Impfstoff (Nukleosid-modifiziert)" 
* #EU/1/20/1507 ^property[0].code = #Relationships 
* #EU/1/20/1507 ^property[0].valueString = "$.v..mp" 
* #EU/1/20/1507 ^property[1].code = #hints 
* #EU/1/20/1507 ^property[1].valueString = "T^E^B" 
* #EU/1/20/1525 "Jcovden"
* #EU/1/20/1525 ^definition = Jcovden
* #EU/1/20/1525 ^designation[0].language = #de-AT 
* #EU/1/20/1525 ^designation[0].value = "COVID-19 Vaccine Janssen Injektionssuspension, COVID-19-Impfstoff (Ad26.COV2-S [rekombinant])" 
* #EU/1/20/1525 ^property[0].code = #Relationships 
* #EU/1/20/1525 ^property[0].valueString = "$.v..mp" 
* #EU/1/20/1525 ^property[1].code = #hints 
* #EU/1/20/1525 ^property[1].valueString = "T^E^B" 
* #EU/1/20/1528 "Comirnaty"
* #EU/1/20/1528 ^definition = Comirnaty
* #EU/1/20/1528 ^designation[0].language = #de-AT 
* #EU/1/20/1528 ^designation[0].value = "Comirnaty Konzentrat zur Herstellung einer Injektionsdispersion, COVID-19-mRNA-Impfstoff (Nukleosid-modifiziert)" 
* #EU/1/20/1528 ^property[0].code = #Relationships 
* #EU/1/20/1528 ^property[0].valueString = "$.v..mp" 
* #EU/1/20/1528 ^property[1].code = #hints 
* #EU/1/20/1528 ^property[1].valueString = "T^E^B" 
* #EU/1/21/1529 "Vaxzevria"
* #EU/1/21/1529 ^definition = Vaxzevria
* #EU/1/21/1529 ^designation[0].language = #de-AT 
* #EU/1/21/1529 ^designation[0].value = "COVID-19 Vaccine AstraZeneca Injektionssuspension, COVID-19-Impfstoff (ChAdOx1-S [rekombinant])" 
* #EU/1/21/1529 ^property[0].code = #Relationships 
* #EU/1/21/1529 ^property[0].valueString = "$.v..mp" 
* #EU/1/21/1529 ^property[1].code = #hints 
* #EU/1/21/1529 ^property[1].valueString = "T^E^B" 
* #EU/1/21/1618 "Nuvaxovid"
* #EU/1/21/1618 ^definition = Nuvaxovid
* #EU/1/21/1618 ^designation[0].language = #de-AT 
* #EU/1/21/1618 ^designation[0].value = "Nuvaxovid Injektionsdispersion; COVID-19 Impfstoff (rekombinant, adjuvantiert)" 
* #EU/1/21/1618 ^property[0].code = #Relationships 
* #EU/1/21/1618 ^property[0].valueString = "$.v..mp" 
* #EU/1/21/1618 ^property[1].code = #hints 
* #EU/1/21/1618 ^property[1].valueString = "T^E^B" 
* #EU/1/21/1624 "Valneva"
* #EU/1/21/1624 ^definition = Valneva
* #EU/1/21/1624 ^designation[0].language = #de-AT 
* #EU/1/21/1624 ^designation[0].value = "COVID-19-Impfstoff (inaktiviert, adjuvantiert) Valneva Injektionssuspension; COVID-19-Impfstoff (inaktiviert, adjuvantiert, adsorbiert)" 
* #EU/1/21/1624 ^property[0].code = #Relationships 
* #EU/1/21/1624 ^property[0].valueString = "$.v..mp" 
* #EU/1/21/1624 ^property[1].code = #hints 
* #EU/1/21/1624 ^property[1].valueString = "T^E^B" 
* #EpiVacCorona "EpiVacCorona"
* #EpiVacCorona ^definition = EpiVacCorona
* #EpiVacCorona ^designation[0].language = #de-AT 
* #EpiVacCorona ^designation[0].value = "COVID-19-Impfstoff EpiVacCorona (Vektor-Institut)" 
* #EpiVacCorona ^property[0].code = #Relationships 
* #EpiVacCorona ^property[0].valueString = "$.v..mp" 
* #EpiVacCorona ^property[1].code = #hints 
* #EpiVacCorona ^property[1].valueString = "T^E^B" 
* #EpiVacCorona-N "EpiVacCorona-N"
* #EpiVacCorona-N ^definition = EpiVacCorona-N
* #EpiVacCorona-N ^designation[0].language = #de-AT 
* #EpiVacCorona-N ^designation[0].value = "COVID-19-Impfstoff EpiVacCorona-N (Vector Institute)" 
* #EpiVacCorona-N ^property[0].code = #Relationships 
* #EpiVacCorona-N ^property[0].valueString = "$.v..mp" 
* #EpiVacCorona-N ^property[1].code = #hints 
* #EpiVacCorona-N ^property[1].valueString = "T^E^B" 
* #FI "Finland"
* #FI ^definition = Finland
* #FI ^designation[0].language = #de-AT 
* #FI ^designation[0].value = "Finnland" 
* #FI ^property[0].code = #Relationships 
* #FI ^property[0].valueString = "$.co" 
* #FI ^property[1].code = #hints 
* #FI ^property[1].valueString = "T^E^B" 
* #FJ "Fiji"
* #FJ ^definition = Fiji
* #FJ ^designation[0].language = #de-AT 
* #FJ ^designation[0].value = "Fidschi" 
* #FJ ^property[0].code = #Relationships 
* #FJ ^property[0].valueString = "$.co" 
* #FJ ^property[1].code = #hints 
* #FJ ^property[1].valueString = "T^E^B" 
* #FK "Falkland Islands (the) [Malvinas]"
* #FK ^definition = Falkland Islands (the) [Malvinas]
* #FK ^designation[0].language = #de-AT 
* #FK ^designation[0].value = "Falklandinseln" 
* #FK ^property[0].code = #Relationships 
* #FK ^property[0].valueString = "$.co" 
* #FK ^property[1].code = #hints 
* #FK ^property[1].valueString = "T^E^B" 
* #FM "Micronesia (Federated States of)"
* #FM ^definition = Micronesia (Federated States of)
* #FM ^designation[0].language = #de-AT 
* #FM ^designation[0].value = "Mikronesien,Föd.Staat.von" 
* #FM ^property[0].code = #Relationships 
* #FM ^property[0].valueString = "$.co" 
* #FM ^property[1].code = #hints 
* #FM ^property[1].valueString = "T^E^B" 
* #FO "Faroe Islands (the)"
* #FO ^definition = Faroe Islands (the)
* #FO ^designation[0].language = #de-AT 
* #FO ^designation[0].value = "Färöer" 
* #FO ^property[0].code = #Relationships 
* #FO ^property[0].valueString = "$.co" 
* #FO ^property[1].code = #hints 
* #FO ^property[1].valueString = "T^E^B" 
* #FR "France"
* #FR ^definition = France
* #FR ^designation[0].language = #de-AT 
* #FR ^designation[0].value = "Frankreich" 
* #FR ^property[0].code = #Relationships 
* #FR ^property[0].valueString = "$.co" 
* #FR ^property[1].code = #hints 
* #FR ^property[1].valueString = "T^E^B" 
* #Finlay-Institute "Finlay Institute of Vaccines"
* #Finlay-Institute ^definition = Finlay Institute of Vaccines
* #Finlay-Institute ^designation[0].language = #de-AT 
* #Finlay-Institute ^designation[0].value = "Finlay Institute of Vaccines" 
* #Finlay-Institute ^property[0].code = #Relationships 
* #Finlay-Institute ^property[0].valueString = "$.v..ma" 
* #Finlay-Institute ^property[1].code = #hints 
* #Finlay-Institute ^property[1].valueString = "T^E^B" 
* #Fiocruz "Fiocruz"
* #Fiocruz ^definition = Fiocruz
* #Fiocruz ^designation[0].language = #de-AT 
* #Fiocruz ^designation[0].value = "Fiocruz" 
* #Fiocruz ^property[0].code = #Relationships 
* #Fiocruz ^property[0].valueString = "$.v..ma" 
* #Fiocruz ^property[1].code = #hints 
* #Fiocruz ^property[1].valueString = "T^E^B" 
* #GA "Gabon"
* #GA ^definition = Gabon
* #GA ^designation[0].language = #de-AT 
* #GA ^designation[0].value = "Gabun" 
* #GA ^property[0].code = #Relationships 
* #GA ^property[0].valueString = "$.co" 
* #GA ^property[1].code = #hints 
* #GA ^property[1].valueString = "T^E^B" 
* #GB "United Kingdom of Great Britain and Northern Ireland (the)"
* #GB ^definition = United Kingdom of Great Britain and Northern Ireland (the)
* #GB ^designation[0].language = #de-AT 
* #GB ^designation[0].value = "Vereinigtes Königreich" 
* #GB ^property[0].code = #Relationships 
* #GB ^property[0].valueString = "$.co" 
* #GB ^property[1].code = #hints 
* #GB ^property[1].valueString = "T^E^B" 
* #GD "Grenada"
* #GD ^definition = Grenada
* #GD ^designation[0].language = #de-AT 
* #GD ^designation[0].value = "Grenada" 
* #GD ^property[0].code = #Relationships 
* #GD ^property[0].valueString = "$.co" 
* #GD ^property[1].code = #hints 
* #GD ^property[1].valueString = "T^E^B" 
* #GE "Georgia"
* #GE ^definition = Georgia
* #GE ^designation[0].language = #de-AT 
* #GE ^designation[0].value = "Georgien" 
* #GE ^property[0].code = #Relationships 
* #GE ^property[0].valueString = "$.co" 
* #GE ^property[1].code = #hints 
* #GE ^property[1].valueString = "T^E^B" 
* #GF "French Guiana"
* #GF ^definition = French Guiana
* #GF ^designation[0].language = #de-AT 
* #GF ^designation[0].value = "Franz.Guayana" 
* #GF ^property[0].code = #Relationships 
* #GF ^property[0].valueString = "$.co" 
* #GF ^property[1].code = #hints 
* #GF ^property[1].valueString = "T^E^B" 
* #GG "Guernsey"
* #GG ^definition = Guernsey
* #GG ^designation[0].language = #de-AT 
* #GG ^designation[0].value = "Guernsey (nur SV)" 
* #GG ^property[0].code = #Relationships 
* #GG ^property[0].valueString = "$.co" 
* #GG ^property[1].code = #hints 
* #GG ^property[1].valueString = "T^E^B" 
* #GH "Ghana"
* #GH ^definition = Ghana
* #GH ^designation[0].language = #de-AT 
* #GH ^designation[0].value = "Ghana" 
* #GH ^property[0].code = #Relationships 
* #GH ^property[0].valueString = "$.co" 
* #GH ^property[1].code = #hints 
* #GH ^property[1].valueString = "T^E^B" 
* #GI "Gibraltar"
* #GI ^definition = Gibraltar
* #GI ^designation[0].language = #de-AT 
* #GI ^designation[0].value = "Gibraltar" 
* #GI ^property[0].code = #Relationships 
* #GI ^property[0].valueString = "$.co" 
* #GI ^property[1].code = #hints 
* #GI ^property[1].valueString = "T^E^B" 
* #GL "Greenland"
* #GL ^definition = Greenland
* #GL ^designation[0].language = #de-AT 
* #GL ^designation[0].value = "Grönland" 
* #GL ^property[0].code = #Relationships 
* #GL ^property[0].valueString = "$.co" 
* #GL ^property[1].code = #hints 
* #GL ^property[1].valueString = "T^E^B" 
* #GM "Gambia (the)"
* #GM ^definition = Gambia (the)
* #GM ^designation[0].language = #de-AT 
* #GM ^designation[0].value = "Gambia" 
* #GM ^property[0].code = #Relationships 
* #GM ^property[0].valueString = "$.co" 
* #GM ^property[1].code = #hints 
* #GM ^property[1].valueString = "T^E^B" 
* #GN "Guinea"
* #GN ^definition = Guinea
* #GN ^designation[0].language = #de-AT 
* #GN ^designation[0].value = "Guinea" 
* #GN ^property[0].code = #Relationships 
* #GN ^property[0].valueString = "$.co" 
* #GN ^property[1].code = #hints 
* #GN ^property[1].valueString = "T^E^B" 
* #GP "Guadeloupe"
* #GP ^definition = Guadeloupe
* #GP ^designation[0].language = #de-AT 
* #GP ^designation[0].value = "Guadeloupe" 
* #GP ^property[0].code = #Relationships 
* #GP ^property[0].valueString = "$.co" 
* #GP ^property[1].code = #hints 
* #GP ^property[1].valueString = "T^E^B" 
* #GQ "Equatorial Guinea"
* #GQ ^definition = Equatorial Guinea
* #GQ ^designation[0].language = #de-AT 
* #GQ ^designation[0].value = "Äquatorialguinea" 
* #GQ ^property[0].code = #Relationships 
* #GQ ^property[0].valueString = "$.co" 
* #GQ ^property[1].code = #hints 
* #GQ ^property[1].valueString = "T^E^B" 
* #GR "Greece"
* #GR ^definition = Greece
* #GR ^designation[0].language = #de-AT 
* #GR ^designation[0].value = "Griechenland" 
* #GR ^property[0].code = #Relationships 
* #GR ^property[0].valueString = "$.co" 
* #GR ^property[1].code = #hints 
* #GR ^property[1].valueString = "T^E^B" 
* #GT "Guatemala"
* #GT ^definition = Guatemala
* #GT ^designation[0].language = #de-AT 
* #GT ^designation[0].value = "Guatemala" 
* #GT ^property[0].code = #Relationships 
* #GT ^property[0].valueString = "$.co" 
* #GT ^property[1].code = #hints 
* #GT ^property[1].valueString = "T^E^B" 
* #GU "Guam"
* #GU ^definition = Guam
* #GU ^designation[0].language = #de-AT 
* #GU ^designation[0].value = "Guam" 
* #GU ^property[0].code = #Relationships 
* #GU ^property[0].valueString = "$.co" 
* #GU ^property[1].code = #hints 
* #GU ^property[1].valueString = "T^E^B" 
* #GW "Guinea-Bissau"
* #GW ^definition = Guinea-Bissau
* #GW ^designation[0].language = #de-AT 
* #GW ^designation[0].value = "Guinea-Bissau" 
* #GW ^property[0].code = #Relationships 
* #GW ^property[0].valueString = "$.co" 
* #GW ^property[1].code = #hints 
* #GW ^property[1].valueString = "T^E^B" 
* #GY "Guyana"
* #GY ^definition = Guyana
* #GY ^designation[0].language = #de-AT 
* #GY ^designation[0].value = "Guyana" 
* #GY ^property[0].code = #Relationships 
* #GY ^property[0].valueString = "$.co" 
* #GY ^property[1].code = #hints 
* #GY ^property[1].valueString = "T^E^B" 
* #Gamaleya-Research-Institute "Gamaleya Research Institute"
* #Gamaleya-Research-Institute ^definition = Gamaleya Research Institute
* #Gamaleya-Research-Institute ^designation[0].language = #de-AT 
* #Gamaleya-Research-Institute ^designation[0].value = "Gamaleya Research Institute" 
* #Gamaleya-Research-Institute ^property[0].code = #Relationships 
* #Gamaleya-Research-Institute ^property[0].valueString = "$.v..ma" 
* #Gamaleya-Research-Institute ^property[1].code = #hints 
* #Gamaleya-Research-Institute ^property[1].valueString = "T^E^B" 
* #HK "Hong Kong"
* #HK ^definition = Hong Kong
* #HK ^designation[0].language = #de-AT 
* #HK ^designation[0].value = "Hongkong" 
* #HK ^property[0].code = #Relationships 
* #HK ^property[0].valueString = "$.co" 
* #HK ^property[1].code = #hints 
* #HK ^property[1].valueString = "T^E^B" 
* #HL "Saint Helena"
* #HL ^definition = Saint Helena
* #HL ^designation[0].language = #de-AT 
* #HL ^designation[0].value = "St. Helena" 
* #HL ^property[0].code = #Relationships 
* #HL ^property[0].valueString = "$.co" 
* #HL ^property[1].code = #hints 
* #HL ^property[1].valueString = "T^E^B" 
* #HM "Heard Island and McDonald Islands"
* #HM ^definition = Heard Island and McDonald Islands
* #HM ^designation[0].language = #de-AT 
* #HM ^designation[0].value = "Heard/McDonaldi" 
* #HM ^property[0].code = #Relationships 
* #HM ^property[0].valueString = "$.co" 
* #HM ^property[1].code = #hints 
* #HM ^property[1].valueString = "T^E^B" 
* #HN "Honduras"
* #HN ^definition = Honduras
* #HN ^designation[0].language = #de-AT 
* #HN ^designation[0].value = "Honduras" 
* #HN ^property[0].code = #Relationships 
* #HN ^property[0].valueString = "$.co" 
* #HN ^property[1].code = #hints 
* #HN ^property[1].valueString = "T^E^B" 
* #HR "Croatia"
* #HR ^definition = Croatia
* #HR ^designation[0].language = #de-AT 
* #HR ^designation[0].value = "Kroatien" 
* #HR ^property[0].code = #Relationships 
* #HR ^property[0].valueString = "$.co" 
* #HR ^property[1].code = #hints 
* #HR ^property[1].valueString = "T^E^B" 
* #HT "Haiti"
* #HT ^definition = Haiti
* #HT ^designation[0].language = #de-AT 
* #HT ^designation[0].value = "Haiti" 
* #HT ^property[0].code = #Relationships 
* #HT ^property[0].valueString = "$.co" 
* #HT ^property[1].code = #hints 
* #HT ^property[1].valueString = "T^E^B" 
* #HU "Hungary"
* #HU ^definition = Hungary
* #HU ^designation[0].language = #de-AT 
* #HU ^designation[0].value = "Ungarn" 
* #HU ^property[0].code = #Relationships 
* #HU ^property[0].valueString = "$.co" 
* #HU ^property[1].code = #hints 
* #HU ^property[1].valueString = "T^E^B" 
* #Hayat-Vax "Hayat-Vax"
* #Hayat-Vax ^definition = Hayat-Vax
* #Hayat-Vax ^designation[0].language = #de-AT 
* #Hayat-Vax ^designation[0].value = "COVID-19-Impfstoff Hayat-Vax (Gulf Pharmaceutical Industries)" 
* #Hayat-Vax ^property[0].code = #Relationships 
* #Hayat-Vax ^property[0].valueString = "$.v..mp" 
* #Hayat-Vax ^property[1].code = #hints 
* #Hayat-Vax ^property[1].valueString = "T^E^B" 
* #ID "Indonesia"
* #ID ^definition = Indonesia
* #ID ^designation[0].language = #de-AT 
* #ID ^designation[0].value = "Indonesien" 
* #ID ^property[0].code = #Relationships 
* #ID ^property[0].valueString = "$.co" 
* #ID ^property[1].code = #hints 
* #ID ^property[1].valueString = "T^E^B" 
* #IE "Ireland"
* #IE ^definition = Ireland
* #IE ^designation[0].language = #de-AT 
* #IE ^designation[0].value = "Irland" 
* #IE ^property[0].code = #Relationships 
* #IE ^property[0].valueString = "$.co" 
* #IE ^property[1].code = #hints 
* #IE ^property[1].valueString = "T^E^B" 
* #IL "Israel"
* #IL ^definition = Israel
* #IL ^designation[0].language = #de-AT 
* #IL ^designation[0].value = "Israel" 
* #IL ^property[0].code = #Relationships 
* #IL ^property[0].valueString = "$.co" 
* #IL ^property[1].code = #hints 
* #IL ^property[1].valueString = "T^E^B" 
* #IM "Isle of Man"
* #IM ^definition = Isle of Man
* #IM ^designation[0].language = #de-AT 
* #IM ^designation[0].value = "Isle of Man (nur SV)" 
* #IM ^property[0].code = #Relationships 
* #IM ^property[0].valueString = "$.co" 
* #IM ^property[1].code = #hints 
* #IM ^property[1].valueString = "T^E^B" 
* #IN "India"
* #IN ^definition = India
* #IN ^designation[0].language = #de-AT 
* #IN ^designation[0].value = "Indien" 
* #IN ^property[0].code = #Relationships 
* #IN ^property[0].valueString = "$.co" 
* #IN ^property[1].code = #hints 
* #IN ^property[1].valueString = "T^E^B" 
* #IO "British Indian Ocean Territory (the)"
* #IO ^definition = British Indian Ocean Territory (the)
* #IO ^designation[0].language = #de-AT 
* #IO ^designation[0].value = "Br.Terr/Ind.Ozn" 
* #IO ^property[0].code = #Relationships 
* #IO ^property[0].valueString = "$.co" 
* #IO ^property[1].code = #hints 
* #IO ^property[1].valueString = "T^E^B" 
* #IQ "Iraq"
* #IQ ^definition = Iraq
* #IQ ^designation[0].language = #de-AT 
* #IQ ^designation[0].value = "Irak" 
* #IQ ^property[0].code = #Relationships 
* #IQ ^property[0].valueString = "$.co" 
* #IQ ^property[1].code = #hints 
* #IQ ^property[1].valueString = "T^E^B" 
* #IR "Iran (Islamic Republic of)"
* #IR ^definition = Iran (Islamic Republic of)
* #IR ^designation[0].language = #de-AT 
* #IR ^designation[0].value = "Iran,Islamische Republik" 
* #IR ^property[0].code = #Relationships 
* #IR ^property[0].valueString = "$.co" 
* #IR ^property[1].code = #hints 
* #IR ^property[1].valueString = "T^E^B" 
* #IS "Iceland"
* #IS ^definition = Iceland
* #IS ^designation[0].language = #de-AT 
* #IS ^designation[0].value = "Island" 
* #IS ^property[0].code = #Relationships 
* #IS ^property[0].valueString = "$.co" 
* #IS ^property[1].code = #hints 
* #IS ^property[1].valueString = "T^E^B" 
* #IT "Italy"
* #IT ^definition = Italy
* #IT ^designation[0].language = #de-AT 
* #IT ^designation[0].value = "Italien" 
* #IT ^property[0].code = #Relationships 
* #IT ^property[0].valueString = "$.co" 
* #IT ^property[1].code = #hints 
* #IT ^property[1].valueString = "T^E^B" 
* #Inactivated-SARS-CoV-2-Vero-Cell "Inactivated SARS-CoV-2 (Vero Cell)"
* #Inactivated-SARS-CoV-2-Vero-Cell ^definition = Inactivated SARS-CoV-2 (Vero Cell)
* #Inactivated-SARS-CoV-2-Vero-Cell ^designation[0].language = #de-AT 
* #Inactivated-SARS-CoV-2-Vero-Cell ^designation[0].value = "COVID-19-Impfstoff Inactivated-SARS-CoV-2-Vero-Cell" 
* #Inactivated-SARS-CoV-2-Vero-Cell ^property[0].code = #status 
* #Inactivated-SARS-CoV-2-Vero-Cell ^property[0].valueCode = #retired 
* #Inactivated-SARS-CoV-2-Vero-Cell ^property[1].code = #Relationships 
* #Inactivated-SARS-CoV-2-Vero-Cell ^property[1].valueString = "$.v..mp" 
* #Inactivated-SARS-CoV-2-Vero-Cell ^property[2].code = #hints 
* #Inactivated-SARS-CoV-2-Vero-Cell ^property[2].valueString = "DEPRECATED" 
* #Instituto-Butantan "Instituto Butantan"
* #Instituto-Butantan ^definition = Instituto Butantan
* #Instituto-Butantan ^designation[0].language = #de-AT 
* #Instituto-Butantan ^designation[0].value = "Instituto Butantan" 
* #Instituto-Butantan ^property[0].code = #Relationships 
* #Instituto-Butantan ^property[0].valueString = "$.v..ma" 
* #Instituto-Butantan ^property[1].code = #hints 
* #Instituto-Butantan ^property[1].valueString = "T^E^B" 
* #J07BX03 "covid-19 vaccines"
* #J07BX03 ^definition = covid-19 vaccines
* #J07BX03 ^designation[0].language = #de-AT 
* #J07BX03 ^designation[0].value = "Covid-19 Vakzine" 
* #J07BX03 ^property[0].code = #Relationships 
* #J07BX03 ^property[0].valueString = "$.v..vp" 
* #J07BX03 ^property[1].code = #hints 
* #J07BX03 ^property[1].valueString = "T^E^B" 
* #JE "Jersey"
* #JE ^definition = Jersey
* #JE ^designation[0].language = #de-AT 
* #JE ^designation[0].value = "Jersey (nur SV)" 
* #JE ^property[0].code = #Relationships 
* #JE ^property[0].valueString = "$.co" 
* #JE ^property[1].code = #hints 
* #JE ^property[1].valueString = "T^E^B" 
* #JM "Jamaica"
* #JM ^definition = Jamaica
* #JM ^designation[0].language = #de-AT 
* #JM ^designation[0].value = "Jamaika" 
* #JM ^property[0].code = #Relationships 
* #JM ^property[0].valueString = "$.co" 
* #JM ^property[1].code = #hints 
* #JM ^property[1].valueString = "T^E^B" 
* #JO "Jordan"
* #JO ^definition = Jordan
* #JO ^designation[0].language = #de-AT 
* #JO ^designation[0].value = "Jordanien" 
* #JO ^property[0].code = #Relationships 
* #JO ^property[0].valueString = "$.co" 
* #JO ^property[1].code = #hints 
* #JO ^property[1].valueString = "T^E^B" 
* #JP "Japan"
* #JP ^definition = Japan
* #JP ^designation[0].language = #de-AT 
* #JP ^designation[0].value = "Japan" 
* #JP ^property[0].code = #Relationships 
* #JP ^property[0].valueString = "$.co" 
* #JP ^property[1].code = #hints 
* #JP ^property[1].valueString = "T^E^B" 
* #KE "Kenya"
* #KE ^definition = Kenya
* #KE ^designation[0].language = #de-AT 
* #KE ^designation[0].value = "Kenia" 
* #KE ^property[0].code = #Relationships 
* #KE ^property[0].valueString = "$.co" 
* #KE ^property[1].code = #hints 
* #KE ^property[1].valueString = "T^E^B" 
* #KG "Kyrgyzstan"
* #KG ^definition = Kyrgyzstan
* #KG ^designation[0].language = #de-AT 
* #KG ^designation[0].value = "Kirgisistan" 
* #KG ^property[0].code = #Relationships 
* #KG ^property[0].valueString = "$.co" 
* #KG ^property[1].code = #hints 
* #KG ^property[1].valueString = "T^E^B" 
* #KH "Cambodia"
* #KH ^definition = Cambodia
* #KH ^designation[0].language = #de-AT 
* #KH ^designation[0].value = "Kambodscha" 
* #KH ^property[0].code = #Relationships 
* #KH ^property[0].valueString = "$.co" 
* #KH ^property[1].code = #hints 
* #KH ^property[1].valueString = "T^E^B" 
* #KI "Kiribati"
* #KI ^definition = Kiribati
* #KI ^designation[0].language = #de-AT 
* #KI ^designation[0].value = "Kiribati" 
* #KI ^property[0].code = #Relationships 
* #KI ^property[0].valueString = "$.co" 
* #KI ^property[1].code = #hints 
* #KI ^property[1].valueString = "T^E^B" 
* #KM "Comoros (the)"
* #KM ^definition = Comoros (the)
* #KM ^designation[0].language = #de-AT 
* #KM ^designation[0].value = "Komoren" 
* #KM ^property[0].code = #Relationships 
* #KM ^property[0].valueString = "$.co" 
* #KM ^property[1].code = #hints 
* #KM ^property[1].valueString = "T^E^B" 
* #KN "Saint Kitts and Nevis"
* #KN ^definition = Saint Kitts and Nevis
* #KN ^designation[0].language = #de-AT 
* #KN ^designation[0].value = "St.Kitts und Nevis" 
* #KN ^property[0].code = #Relationships 
* #KN ^property[0].valueString = "$.co" 
* #KN ^property[1].code = #hints 
* #KN ^property[1].valueString = "T^E^B" 
* #KP "Korea (the Democratic People's Republic of)"
* #KP ^definition = Korea (the Democratic People's Republic of)
* #KP ^designation[0].language = #de-AT 
* #KP ^designation[0].value = "Korea,Dem.Volksrepublik" 
* #KP ^property[0].code = #Relationships 
* #KP ^property[0].valueString = "$.co" 
* #KP ^property[1].code = #hints 
* #KP ^property[1].valueString = "T^E^B" 
* #KR "Korea (the Republic of)"
* #KR ^definition = Korea (the Republic of)
* #KR ^designation[0].language = #de-AT 
* #KR ^designation[0].value = "Korea,Republik" 
* #KR ^property[0].code = #Relationships 
* #KR ^property[0].valueString = "$.co" 
* #KR ^property[1].code = #hints 
* #KR ^property[1].valueString = "T^E^B" 
* #KS "Kosovo (Republic of)"
* #KS ^definition = Kosovo (Republic of)
* #KS ^designation[0].language = #de-AT 
* #KS ^designation[0].value = "Kosovo, Republik" 
* #KS ^property[0].code = #Relationships 
* #KS ^property[0].valueString = "$.co" 
* #KS ^property[1].code = #hints 
* #KS ^property[1].valueString = "T^E^B" 
* #KW "Kuwait"
* #KW ^definition = Kuwait
* #KW ^designation[0].language = #de-AT 
* #KW ^designation[0].value = "Kuwait" 
* #KW ^property[0].code = #Relationships 
* #KW ^property[0].valueString = "$.co" 
* #KW ^property[1].code = #hints 
* #KW ^property[1].valueString = "T^E^B" 
* #KY "Cayman Islands (the)"
* #KY ^definition = Cayman Islands (the)
* #KY ^designation[0].language = #de-AT 
* #KY ^designation[0].value = "Kaimaninseln" 
* #KY ^property[0].code = #Relationships 
* #KY ^property[0].valueString = "$.co" 
* #KY ^property[1].code = #hints 
* #KY ^property[1].valueString = "T^E^B" 
* #KZ "Kazakhstan"
* #KZ ^definition = Kazakhstan
* #KZ ^designation[0].language = #de-AT 
* #KZ ^designation[0].value = "Kasachstan" 
* #KZ ^property[0].code = #Relationships 
* #KZ ^property[0].valueString = "$.co" 
* #KZ ^property[1].code = #hints 
* #KZ ^property[1].valueString = "T^E^B" 
* #LA "Lao People's Democratic Republic (the)"
* #LA ^definition = Lao People's Democratic Republic (the)
* #LA ^designation[0].language = #de-AT 
* #LA ^designation[0].value = "Laos,Demokr.Volksrepublik" 
* #LA ^property[0].code = #Relationships 
* #LA ^property[0].valueString = "$.co" 
* #LA ^property[1].code = #hints 
* #LA ^property[1].valueString = "T^E^B" 
* #LB "Lebanon"
* #LB ^definition = Lebanon
* #LB ^designation[0].language = #de-AT 
* #LB ^designation[0].value = "Libanon" 
* #LB ^property[0].code = #Relationships 
* #LB ^property[0].valueString = "$.co" 
* #LB ^property[1].code = #hints 
* #LB ^property[1].valueString = "T^E^B" 
* #LC "Saint Lucia"
* #LC ^definition = Saint Lucia
* #LC ^designation[0].language = #de-AT 
* #LC ^designation[0].value = "St.Lucia" 
* #LC ^property[0].code = #Relationships 
* #LC ^property[0].valueString = "$.co" 
* #LC ^property[1].code = #hints 
* #LC ^property[1].valueString = "T^E^B" 
* #LI "Liechtenstein"
* #LI ^definition = Liechtenstein
* #LI ^designation[0].language = #de-AT 
* #LI ^designation[0].value = "Liechtenstein" 
* #LI ^property[0].code = #Relationships 
* #LI ^property[0].valueString = "$.co" 
* #LI ^property[1].code = #hints 
* #LI ^property[1].valueString = "T^E^B" 
* #LK "Sri Lanka"
* #LK ^definition = Sri Lanka
* #LK ^designation[0].language = #de-AT 
* #LK ^designation[0].value = "Sri Lanka" 
* #LK ^property[0].code = #Relationships 
* #LK ^property[0].valueString = "$.co" 
* #LK ^property[1].code = #hints 
* #LK ^property[1].valueString = "T^E^B" 
* #LP217197-5 "ANTIKOERPER"
* #LP217197-5 ^definition = Rapid immunoassay
* #LP217197-5 ^designation[0].language = #de-AT 
* #LP217197-5 ^designation[0].value = "Antikörpernachweis auf SARS-CoV-2" 
* #LP217197-5 ^property[0].code = #Relationships 
* #LP217197-5 ^property[0].valueString = "$.t..tt" 
* #LP217198-3 "ANTIGEN"
* #LP217198-3 ^definition = Rapid immunoassay
* #LP217198-3 ^designation[0].language = #de-AT 
* #LP217198-3 ^designation[0].value = "Antigen-Test auf SARS-CoV-2" 
* #LP217198-3 ^property[0].code = #Relationships 
* #LP217198-3 ^property[0].valueString = "$.t..tt" 
* #LP217198-3 ^property[1].code = #hints 
* #LP217198-3 ^property[1].valueString = "T^E^B" 
* #LP6464-4 "PCR"
* #LP6464-4 ^definition = Nucleic acid amplification with probe detection
* #LP6464-4 ^designation[0].language = #de-AT 
* #LP6464-4 ^designation[0].value = "PCR-Test auf SARS-CoV-2" 
* #LP6464-4 ^property[0].code = #Relationships 
* #LP6464-4 ^property[0].valueString = "$.t..tt" 
* #LP6464-4 ^property[1].code = #hints 
* #LP6464-4 ^property[1].valueString = "T^E^B" 
* #LR "Liberia"
* #LR ^definition = Liberia
* #LR ^designation[0].language = #de-AT 
* #LR ^designation[0].value = "Liberia" 
* #LR ^property[0].code = #Relationships 
* #LR ^property[0].valueString = "$.co" 
* #LR ^property[1].code = #hints 
* #LR ^property[1].valueString = "T^E^B" 
* #LS "Lesotho"
* #LS ^definition = Lesotho
* #LS ^designation[0].language = #de-AT 
* #LS ^designation[0].value = "Lesotho" 
* #LS ^property[0].code = #Relationships 
* #LS ^property[0].valueString = "$.co" 
* #LS ^property[1].code = #hints 
* #LS ^property[1].valueString = "T^E^B" 
* #LT "Lithuania"
* #LT ^definition = Lithuania
* #LT ^designation[0].language = #de-AT 
* #LT ^designation[0].value = "Litauen" 
* #LT ^property[0].code = #Relationships 
* #LT ^property[0].valueString = "$.co" 
* #LT ^property[1].code = #hints 
* #LT ^property[1].valueString = "T^E^B" 
* #LU "Luxembourg"
* #LU ^definition = Luxembourg
* #LU ^designation[0].language = #de-AT 
* #LU ^designation[0].value = "Luxemburg" 
* #LU ^property[0].code = #Relationships 
* #LU ^property[0].valueString = "$.co" 
* #LU ^property[1].code = #hints 
* #LU ^property[1].valueString = "T^E^B" 
* #LV "Latvia"
* #LV ^definition = Latvia
* #LV ^designation[0].language = #de-AT 
* #LV ^designation[0].value = "Lettland" 
* #LV ^property[0].code = #Relationships 
* #LV ^property[0].valueString = "$.co" 
* #LV ^property[1].code = #hints 
* #LV ^property[1].valueString = "T^E^B" 
* #LY "Libya"
* #LY ^definition = Libya
* #LY ^designation[0].language = #de-AT 
* #LY ^designation[0].value = "Libysch-Arab.Dschamahir." 
* #LY ^property[0].code = #Relationships 
* #LY ^property[0].valueString = "$.co" 
* #LY ^property[1].code = #hints 
* #LY ^property[1].valueString = "T^E^B" 
* #MA "Morocco"
* #MA ^definition = Morocco
* #MA ^designation[0].language = #de-AT 
* #MA ^designation[0].value = "Marokko" 
* #MA ^property[0].code = #Relationships 
* #MA ^property[0].valueString = "$.co" 
* #MA ^property[1].code = #hints 
* #MA ^property[1].valueString = "T^E^B" 
* #MC "Monaco"
* #MC ^definition = Monaco
* #MC ^designation[0].language = #de-AT 
* #MC ^designation[0].value = "Monaco" 
* #MC ^property[0].code = #Relationships 
* #MC ^property[0].valueString = "$.co" 
* #MC ^property[1].code = #hints 
* #MC ^property[1].valueString = "T^E^B" 
* #MD "Moldova (the Republic of)"
* #MD ^definition = Moldova (the Republic of)
* #MD ^designation[0].language = #de-AT 
* #MD ^designation[0].value = "Moldau" 
* #MD ^property[0].code = #Relationships 
* #MD ^property[0].valueString = "$.co" 
* #MD ^property[1].code = #hints 
* #MD ^property[1].valueString = "T^E^B" 
* #ME "Montenegro"
* #ME ^definition = Montenegro
* #ME ^designation[0].language = #de-AT 
* #ME ^designation[0].value = "Montenegro" 
* #ME ^property[0].code = #Relationships 
* #ME ^property[0].valueString = "$.co" 
* #ME ^property[1].code = #hints 
* #ME ^property[1].valueString = "T^E^B" 
* #MG "Madagascar"
* #MG ^definition = Madagascar
* #MG ^designation[0].language = #de-AT 
* #MG ^designation[0].value = "Madagaskar" 
* #MG ^property[0].code = #Relationships 
* #MG ^property[0].valueString = "$.co" 
* #MG ^property[1].code = #hints 
* #MG ^property[1].valueString = "T^E^B" 
* #MH "Marshall Islands (the)"
* #MH ^definition = Marshall Islands (the)
* #MH ^designation[0].language = #de-AT 
* #MH ^designation[0].value = "Marshallinseln" 
* #MH ^property[0].code = #Relationships 
* #MH ^property[0].valueString = "$.co" 
* #MH ^property[1].code = #hints 
* #MH ^property[1].valueString = "T^E^B" 
* #MK "Republic of North Macedonia"
* #MK ^definition = Republic of North Macedonia
* #MK ^designation[0].language = #de-AT 
* #MK ^designation[0].value = "Mazedonien,ehem.jug.Rep." 
* #MK ^property[0].code = #Relationships 
* #MK ^property[0].valueString = "$.co" 
* #MK ^property[1].code = #hints 
* #MK ^property[1].valueString = "T^E^B" 
* #ML "Mali"
* #ML ^definition = Mali
* #ML ^designation[0].language = #de-AT 
* #ML ^designation[0].value = "Mali" 
* #ML ^property[0].code = #Relationships 
* #ML ^property[0].valueString = "$.co" 
* #ML ^property[1].code = #hints 
* #ML ^property[1].valueString = "T^E^B" 
* #MM "Myanmar"
* #MM ^definition = Myanmar
* #MM ^designation[0].language = #de-AT 
* #MM ^designation[0].value = "Myanmar (früher:Birma)" 
* #MM ^property[0].code = #Relationships 
* #MM ^property[0].valueString = "$.co" 
* #MM ^property[1].code = #hints 
* #MM ^property[1].valueString = "T^E^B" 
* #MN "Mongolia"
* #MN ^definition = Mongolia
* #MN ^designation[0].language = #de-AT 
* #MN ^designation[0].value = "Mongolei" 
* #MN ^property[0].code = #Relationships 
* #MN ^property[0].valueString = "$.co" 
* #MN ^property[1].code = #hints 
* #MN ^property[1].valueString = "T^E^B" 
* #MO "Macao"
* #MO ^definition = Macao
* #MO ^designation[0].language = #de-AT 
* #MO ^designation[0].value = "Macau" 
* #MO ^property[0].code = #Relationships 
* #MO ^property[0].valueString = "$.co" 
* #MO ^property[1].code = #hints 
* #MO ^property[1].valueString = "T^E^B" 
* #MP "Northern Mariana Islands (the)"
* #MP ^definition = Northern Mariana Islands (the)
* #MP ^designation[0].language = #de-AT 
* #MP ^designation[0].value = "Marianen,Nördl." 
* #MP ^property[0].code = #Relationships 
* #MP ^property[0].valueString = "$.co" 
* #MP ^property[1].code = #hints 
* #MP ^property[1].valueString = "T^E^B" 
* #MQ "Martinique"
* #MQ ^definition = Martinique
* #MQ ^designation[0].language = #de-AT 
* #MQ ^designation[0].value = "Martinique" 
* #MQ ^property[0].code = #Relationships 
* #MQ ^property[0].valueString = "$.co" 
* #MQ ^property[1].code = #hints 
* #MQ ^property[1].valueString = "T^E^B" 
* #MR "Mauritania"
* #MR ^definition = Mauritania
* #MR ^designation[0].language = #de-AT 
* #MR ^designation[0].value = "Mauretanien" 
* #MR ^property[0].code = #Relationships 
* #MR ^property[0].valueString = "$.co" 
* #MR ^property[1].code = #hints 
* #MR ^property[1].valueString = "T^E^B" 
* #MS "Montserrat"
* #MS ^definition = Montserrat
* #MS ^designation[0].language = #de-AT 
* #MS ^designation[0].value = "Montserrat" 
* #MS ^property[0].code = #Relationships 
* #MS ^property[0].valueString = "$.co" 
* #MS ^property[1].code = #hints 
* #MS ^property[1].valueString = "T^E^B" 
* #MT "Malta"
* #MT ^definition = Malta
* #MT ^designation[0].language = #de-AT 
* #MT ^designation[0].value = "Malta" 
* #MT ^property[0].code = #Relationships 
* #MT ^property[0].valueString = "$.co" 
* #MT ^property[1].code = #hints 
* #MT ^property[1].valueString = "T^E^B" 
* #MU "Mauritius"
* #MU ^definition = Mauritius
* #MU ^designation[0].language = #de-AT 
* #MU ^designation[0].value = "Mauritius" 
* #MU ^property[0].code = #Relationships 
* #MU ^property[0].valueString = "$.co" 
* #MU ^property[1].code = #hints 
* #MU ^property[1].valueString = "T^E^B" 
* #MV "Maldives"
* #MV ^definition = Maldives
* #MV ^designation[0].language = #de-AT 
* #MV ^designation[0].value = "Malediven" 
* #MV ^property[0].code = #Relationships 
* #MV ^property[0].valueString = "$.co" 
* #MV ^property[1].code = #hints 
* #MV ^property[1].valueString = "T^E^B" 
* #MVC-COV1901 "MVC COVID-19 vaccine"
* #MVC-COV1901 ^definition = MVC COVID-19 vaccine
* #MVC-COV1901 ^designation[0].language = #de-AT 
* #MVC-COV1901 ^designation[0].value = "COVID-19-Impfstoff MVC COVID-19 vaccine (Medigen Vaccine Biologics Corporation)" 
* #MVC-COV1901 ^property[0].code = #Relationships 
* #MVC-COV1901 ^property[0].valueString = "$.v..mp" 
* #MVC-COV1901 ^property[1].code = #hints 
* #MVC-COV1901 ^property[1].valueString = "T^E^B" 
* #MW "Malawi"
* #MW ^definition = Malawi
* #MW ^designation[0].language = #de-AT 
* #MW ^designation[0].value = "Malawi" 
* #MW ^property[0].code = #Relationships 
* #MW ^property[0].valueString = "$.co" 
* #MW ^property[1].code = #hints 
* #MW ^property[1].valueString = "T^E^B" 
* #MX "Mexico"
* #MX ^definition = Mexico
* #MX ^designation[0].language = #de-AT 
* #MX ^designation[0].value = "Mexiko" 
* #MX ^property[0].code = #Relationships 
* #MX ^property[0].valueString = "$.co" 
* #MX ^property[1].code = #hints 
* #MX ^property[1].valueString = "T^E^B" 
* #MY "Malaysia"
* #MY ^definition = Malaysia
* #MY ^designation[0].language = #de-AT 
* #MY ^designation[0].value = "Malaysia" 
* #MY ^property[0].code = #Relationships 
* #MY ^property[0].valueString = "$.co" 
* #MY ^property[1].code = #hints 
* #MY ^property[1].valueString = "T^E^B" 
* #MZ "Mozambique"
* #MZ ^definition = Mozambique
* #MZ ^designation[0].language = #de-AT 
* #MZ ^designation[0].value = "Mosambik" 
* #MZ ^property[0].code = #Relationships 
* #MZ ^property[0].valueString = "$.co" 
* #MZ ^property[1].code = #hints 
* #MZ ^property[1].valueString = "T^E^B" 
* #NA "Namibia"
* #NA ^definition = Namibia
* #NA ^designation[0].language = #de-AT 
* #NA ^designation[0].value = "Namibia" 
* #NA ^property[0].code = #Relationships 
* #NA ^property[0].valueString = "$.co" 
* #NA ^property[1].code = #hints 
* #NA ^property[1].valueString = "T^E^B" 
* #NC "New Caledonia"
* #NC ^definition = New Caledonia
* #NC ^designation[0].language = #de-AT 
* #NC ^designation[0].value = "Neukaledonien" 
* #NC ^property[0].code = #Relationships 
* #NC ^property[0].valueString = "$.co" 
* #NC ^property[1].code = #hints 
* #NC ^property[1].valueString = "T^E^B" 
* #NE "Niger (the)"
* #NE ^definition = Niger (the)
* #NE ^designation[0].language = #de-AT 
* #NE ^designation[0].value = "Niger" 
* #NE ^property[0].code = #Relationships 
* #NE ^property[0].valueString = "$.co" 
* #NE ^property[1].code = #hints 
* #NE ^property[1].valueString = "T^E^B" 
* #NF "Norfolk Island"
* #NF ^definition = Norfolk Island
* #NF ^designation[0].language = #de-AT 
* #NF ^designation[0].value = "Norfolkinsel" 
* #NF ^property[0].code = #Relationships 
* #NF ^property[0].valueString = "$.co" 
* #NF ^property[1].code = #hints 
* #NF ^property[1].valueString = "T^E^B" 
* #NG "Nigeria"
* #NG ^definition = Nigeria
* #NG ^designation[0].language = #de-AT 
* #NG ^designation[0].value = "Nigeria" 
* #NG ^property[0].code = #Relationships 
* #NG ^property[0].valueString = "$.co" 
* #NG ^property[1].code = #hints 
* #NG ^property[1].valueString = "T^E^B" 
* #NI "Nicaragua"
* #NI ^definition = Nicaragua
* #NI ^designation[0].language = #de-AT 
* #NI ^designation[0].value = "Nicaragua" 
* #NI ^property[0].code = #Relationships 
* #NI ^property[0].valueString = "$.co" 
* #NI ^property[1].code = #hints 
* #NI ^property[1].valueString = "T^E^B" 
* #NL "Netherlands (the)"
* #NL ^definition = Netherlands (the)
* #NL ^designation[0].language = #de-AT 
* #NL ^designation[0].value = "Niederlande" 
* #NL ^property[0].code = #Relationships 
* #NL ^property[0].valueString = "$.co" 
* #NL ^property[1].code = #hints 
* #NL ^property[1].valueString = "T^E^B" 
* #NO "Norway"
* #NO ^definition = Norway
* #NO ^designation[0].language = #de-AT 
* #NO ^designation[0].value = "Norwegen" 
* #NO ^property[0].code = #Relationships 
* #NO ^property[0].valueString = "$.co" 
* #NO ^property[1].code = #hints 
* #NO ^property[1].valueString = "T^E^B" 
* #NP "Nepal"
* #NP ^definition = Nepal
* #NP ^designation[0].language = #de-AT 
* #NP ^designation[0].value = "Nepal" 
* #NP ^property[0].code = #Relationships 
* #NP ^property[0].valueString = "$.co" 
* #NP ^property[1].code = #hints 
* #NP ^property[1].valueString = "T^E^B" 
* #NR "Nauru"
* #NR ^definition = Nauru
* #NR ^designation[0].language = #de-AT 
* #NR ^designation[0].value = "Nauru" 
* #NR ^property[0].code = #Relationships 
* #NR ^property[0].valueString = "$.co" 
* #NR ^property[1].code = #hints 
* #NR ^property[1].valueString = "T^E^B" 
* #NU "Niue"
* #NU ^definition = Niue
* #NU ^designation[0].language = #de-AT 
* #NU ^designation[0].value = "Niue" 
* #NU ^property[0].code = #Relationships 
* #NU ^property[0].valueString = "$.co" 
* #NU ^property[1].code = #hints 
* #NU ^property[1].valueString = "T^E^B" 
* #NVSI "National Vaccine and Serum Institute, China"
* #NVSI ^definition = National Vaccine and Serum Institute, China
* #NVSI ^designation[0].language = #de-AT 
* #NVSI ^designation[0].value = "National Vaccine and Serum Institute, China" 
* #NVSI ^property[0].code = #Relationships 
* #NVSI ^property[0].valueString = "$.v..ma" 
* #NVSI ^property[1].code = #hints 
* #NVSI ^property[1].valueString = "T^E^B" 
* #NVSI-06-08 "NVSI-06-08"
* #NVSI-06-08 ^definition = NVSI-06-08
* #NVSI-06-08 ^designation[0].language = #de-AT 
* #NVSI-06-08 ^designation[0].value = "COVID-19-Impfstoff NVSI-06-08 (National Vaccine and Serum Institute, China)" 
* #NVSI-06-08 ^property[0].code = #Relationships 
* #NVSI-06-08 ^property[0].valueString = "$.v..mp" 
* #NVSI-06-08 ^property[1].code = #hints 
* #NVSI-06-08 ^property[1].valueString = "T^E^B" 
* #NVX-CoV2373 "NVX-CoV2373"
* #NVX-CoV2373 ^definition = NVX-CoV2373
* #NVX-CoV2373 ^designation[0].language = #de-AT 
* #NVX-CoV2373 ^designation[0].value = "COVID-19-Impfstoff NVX-CoV2373 (Novavax)" 
* #NVX-CoV2373 ^property[0].code = #status 
* #NVX-CoV2373 ^property[0].valueCode = #retired 
* #NVX-CoV2373 ^property[1].code = #Relationships 
* #NVX-CoV2373 ^property[1].valueString = "$.v..mp" 
* #NVX-CoV2373 ^property[2].code = #hints 
* #NVX-CoV2373 ^property[2].valueString = "DEPRECATED" 
* #NZ "New Zealand"
* #NZ ^definition = New Zealand
* #NZ ^designation[0].language = #de-AT 
* #NZ ^designation[0].value = "Neuseeland" 
* #NZ ^property[0].code = #Relationships 
* #NZ ^property[0].valueString = "$.co" 
* #NZ ^property[1].code = #hints 
* #NZ ^property[1].valueString = "T^E^B" 
* #OM "Oman"
* #OM ^definition = Oman
* #OM ^designation[0].language = #de-AT 
* #OM ^designation[0].value = "Oman" 
* #OM ^property[0].code = #Relationships 
* #OM ^property[0].valueString = "$.co" 
* #OM ^property[1].code = #hints 
* #OM ^property[1].valueString = "T^E^B" 
* #ORG-100000788 "Sanofi Pasteur"
* #ORG-100000788 ^definition = Sanofi Pasteur
* #ORG-100000788 ^designation[0].language = #de-AT 
* #ORG-100000788 ^designation[0].value = "Sanofi Pasteur" 
* #ORG-100000788 ^property[0].code = #Relationships 
* #ORG-100000788 ^property[0].valueString = "$.v..ma" 
* #ORG-100000788 ^property[1].code = #hints 
* #ORG-100000788 ^property[1].valueString = "T^E^B" 
* #ORG-100001417 "Janssen-Cilag International"
* #ORG-100001417 ^definition = Janssen-Cilag International
* #ORG-100001417 ^designation[0].language = #de-AT 
* #ORG-100001417 ^designation[0].value = "Janssen-Cilag International" 
* #ORG-100001417 ^property[0].code = #Relationships 
* #ORG-100001417 ^property[0].valueString = "$.v..ma" 
* #ORG-100001417 ^property[1].code = #hints 
* #ORG-100001417 ^property[1].valueString = "T^E^B" 
* #ORG-100001699 "AstraZeneca AB"
* #ORG-100001699 ^definition = AstraZeneca AB
* #ORG-100001699 ^designation[0].language = #de-AT 
* #ORG-100001699 ^designation[0].value = "AstraZeneca AB" 
* #ORG-100001699 ^property[0].code = #Relationships 
* #ORG-100001699 ^property[0].valueString = "$.v..ma" 
* #ORG-100001699 ^property[1].code = #hints 
* #ORG-100001699 ^property[1].valueString = "T^E^B" 
* #ORG-100001981 "Serum Institute Of India Private Limited"
* #ORG-100001981 ^definition = Serum Institute Of India Private Limited
* #ORG-100001981 ^designation[0].language = #de-AT 
* #ORG-100001981 ^designation[0].value = "Serum Institute Of India Private Limited" 
* #ORG-100001981 ^property[0].code = #Relationships 
* #ORG-100001981 ^property[0].valueString = "$.v..ma" 
* #ORG-100001981 ^property[1].code = #hints 
* #ORG-100001981 ^property[1].valueString = "T^E^B" 
* #ORG-100006270 "Curevac AG"
* #ORG-100006270 ^definition = Curevac AG
* #ORG-100006270 ^designation[0].language = #de-AT 
* #ORG-100006270 ^designation[0].value = "Curevac AG" 
* #ORG-100006270 ^property[0].code = #Relationships 
* #ORG-100006270 ^property[0].valueString = "$.v..ma" 
* #ORG-100006270 ^property[1].code = #hints 
* #ORG-100006270 ^property[1].valueString = "T^E^B" 
* #ORG-100007893 "R-Pharm CJSC"
* #ORG-100007893 ^definition = R-Pharm CJSC
* #ORG-100007893 ^designation[0].language = #de-AT 
* #ORG-100007893 ^designation[0].value = "R-Pharm CJSC" 
* #ORG-100007893 ^property[0].code = #Relationships 
* #ORG-100007893 ^property[0].valueString = "$.v..ma" 
* #ORG-100007893 ^property[1].code = #hints 
* #ORG-100007893 ^property[1].valueString = "T^E^B" 
* #ORG-100008549 "Medicago Inc."
* #ORG-100008549 ^definition = Medicago Inc.
* #ORG-100008549 ^designation[0].language = #de-AT 
* #ORG-100008549 ^designation[0].value = "Medicago Inc." 
* #ORG-100008549 ^property[0].code = #Relationships 
* #ORG-100008549 ^property[0].valueString = "$.v..ma" 
* #ORG-100008549 ^property[1].code = #hints 
* #ORG-100008549 ^property[1].valueString = "T^E^B" 
* #ORG-100010771 "Sinopharm Weiqida Europe Pharmaceutical s.r.o. - Prague location"
* #ORG-100010771 ^definition = Sinopharm Weiqida Europe Pharmaceutical s.r.o. - Prague location
* #ORG-100010771 ^designation[0].language = #de-AT 
* #ORG-100010771 ^designation[0].value = "Sinopharm Weiqida Europe Pharmaceutical s.r.o. - Prague location" 
* #ORG-100010771 ^property[0].code = #Relationships 
* #ORG-100010771 ^property[0].valueString = "$.v..ma" 
* #ORG-100010771 ^property[1].code = #hints 
* #ORG-100010771 ^property[1].valueString = "T^E^B" 
* #ORG-100013793 "CanSino Biologics"
* #ORG-100013793 ^definition = CanSino Biologics
* #ORG-100013793 ^designation[0].language = #de-AT 
* #ORG-100013793 ^designation[0].value = "CanSino Biologics" 
* #ORG-100013793 ^property[0].code = #Relationships 
* #ORG-100013793 ^property[0].valueString = "$.v..ma" 
* #ORG-100013793 ^property[1].code = #hints 
* #ORG-100013793 ^property[1].valueString = "T^E^B" 
* #ORG-100020693 "China Sinopharm International Corp. - Beijing location"
* #ORG-100020693 ^definition = China Sinopharm International Corp. - Beijing location
* #ORG-100020693 ^designation[0].language = #de-AT 
* #ORG-100020693 ^designation[0].value = "China Sinopharm International Corp. - Beijing location" 
* #ORG-100020693 ^property[0].code = #Relationships 
* #ORG-100020693 ^property[0].valueString = "$.v..ma" 
* #ORG-100020693 ^property[1].code = #hints 
* #ORG-100020693 ^property[1].valueString = "T^E^B" 
* #ORG-100023050 "Gulf Pharmaceutical Industries"
* #ORG-100023050 ^definition = Gulf Pharmaceutical Industries
* #ORG-100023050 ^designation[0].language = #de-AT 
* #ORG-100023050 ^designation[0].value = "Gulf Pharmaceutical Industries" 
* #ORG-100023050 ^property[0].code = #Relationships 
* #ORG-100023050 ^property[0].valueString = "$.v..ma" 
* #ORG-100023050 ^property[1].code = #hints 
* #ORG-100023050 ^property[1].valueString = "T^E^B" 
* #ORG-100024420 "Sinopharm Zhijun (Shenzhen) Pharmaceutical Co. Ltd. - Shenzhen location"
* #ORG-100024420 ^definition = Sinopharm Zhijun (Shenzhen) Pharmaceutical Co. Ltd. - Shenzhen location
* #ORG-100024420 ^designation[0].language = #de-AT 
* #ORG-100024420 ^designation[0].value = "Sinopharm Zhijun (Shenzhen) Pharmaceutical Co. Ltd. - Shenzhen location" 
* #ORG-100024420 ^property[0].code = #Relationships 
* #ORG-100024420 ^property[0].valueString = "$.v..ma" 
* #ORG-100024420 ^property[1].code = #hints 
* #ORG-100024420 ^property[1].valueString = "T^E^B" 
* #ORG-100026614 "Sinocelltech Ltd."
* #ORG-100026614 ^definition = Sinocelltech Ltd.
* #ORG-100026614 ^designation[0].language = #de-AT 
* #ORG-100026614 ^designation[0].value = "Sinocelltech Ltd." 
* #ORG-100026614 ^property[0].code = #Relationships 
* #ORG-100026614 ^property[0].valueString = "$.v..ma" 
* #ORG-100026614 ^property[1].code = #hints 
* #ORG-100026614 ^property[1].valueString = "T^E^B" 
* #ORG-100030215 "Biontech Manufacturing GmbH"
* #ORG-100030215 ^definition = Biontech Manufacturing GmbH
* #ORG-100030215 ^designation[0].language = #de-AT 
* #ORG-100030215 ^designation[0].value = "Biontech Manufacturing GmbH" 
* #ORG-100030215 ^property[0].code = #Relationships 
* #ORG-100030215 ^property[0].valueString = "$.v..ma" 
* #ORG-100030215 ^property[1].code = #hints 
* #ORG-100030215 ^property[1].valueString = "T^E^B" 
* #ORG-100031184 "Moderna Biotech Spain S.L."
* #ORG-100031184 ^definition = Moderna Biotech Spain S.L.
* #ORG-100031184 ^designation[0].language = #de-AT 
* #ORG-100031184 ^designation[0].value = "Moderna Biotech Spain S.L." 
* #ORG-100031184 ^property[0].code = #Relationships 
* #ORG-100031184 ^property[0].valueString = "$.v..ma" 
* #ORG-100031184 ^property[1].code = #hints 
* #ORG-100031184 ^property[1].valueString = "T^E^B" 
* #ORG-100032020 "Novavax CZ, a.s."
* #ORG-100032020 ^definition = Novavax CZ, a.s.
* #ORG-100032020 ^designation[0].language = #de-AT 
* #ORG-100032020 ^designation[0].value = "Novavax CZ, a.s." 
* #ORG-100032020 ^property[0].code = #Relationships 
* #ORG-100032020 ^property[0].valueString = "$.v..ma" 
* #ORG-100032020 ^property[1].code = #hints 
* #ORG-100032020 ^property[1].valueString = "T^E^B" 
* #ORG-100033914 "Medigen Vaccine Biologics Corporation"
* #ORG-100033914 ^definition = Medigen Vaccine Biologics Corporation
* #ORG-100033914 ^designation[0].language = #de-AT 
* #ORG-100033914 ^designation[0].value = "Medigen Vaccine Biologics Corporation" 
* #ORG-100033914 ^property[0].code = #Relationships 
* #ORG-100033914 ^property[0].valueString = "$.v..ma" 
* #ORG-100033914 ^property[1].code = #hints 
* #ORG-100033914 ^property[1].valueString = "T^E^B" 
* #ORG-100036422 "Valneva France"
* #ORG-100036422 ^definition = Valneva France
* #ORG-100036422 ^designation[0].language = #de-AT 
* #ORG-100036422 ^designation[0].value = "Valneva France" 
* #ORG-100036422 ^property[0].code = #Relationships 
* #ORG-100036422 ^property[0].valueString = "$.v..ma" 
* #ORG-100036422 ^property[1].code = #hints 
* #ORG-100036422 ^property[1].valueString = "T^E^B" 
* #PA "Panama"
* #PA ^definition = Panama
* #PA ^designation[0].language = #de-AT 
* #PA ^designation[0].value = "Panama" 
* #PA ^property[0].code = #Relationships 
* #PA ^property[0].valueString = "$.co" 
* #PA ^property[1].code = #hints 
* #PA ^property[1].valueString = "T^E^B" 
* #PE "Peru"
* #PE ^definition = Peru
* #PE ^designation[0].language = #de-AT 
* #PE ^designation[0].value = "Peru" 
* #PE ^property[0].code = #Relationships 
* #PE ^property[0].valueString = "$.co" 
* #PE ^property[1].code = #hints 
* #PE ^property[1].valueString = "T^E^B" 
* #PF "French Polynesia"
* #PF ^definition = French Polynesia
* #PF ^designation[0].language = #de-AT 
* #PF ^designation[0].value = "Franz.Polynesien" 
* #PF ^property[0].code = #Relationships 
* #PF ^property[0].valueString = "$.co" 
* #PF ^property[1].code = #hints 
* #PF ^property[1].valueString = "T^E^B" 
* #PG "Papua New Guinea"
* #PG ^definition = Papua New Guinea
* #PG ^designation[0].language = #de-AT 
* #PG ^designation[0].value = "Papua-Neuguinea" 
* #PG ^property[0].code = #Relationships 
* #PG ^property[0].valueString = "$.co" 
* #PG ^property[1].code = #hints 
* #PG ^property[1].valueString = "T^E^B" 
* #PH "Philippines (the)"
* #PH ^definition = Philippines (the)
* #PH ^designation[0].language = #de-AT 
* #PH ^designation[0].value = "Philippinen" 
* #PH ^property[0].code = #Relationships 
* #PH ^property[0].valueString = "$.co" 
* #PH ^property[1].code = #hints 
* #PH ^property[1].valueString = "T^E^B" 
* #PK "Pakistan"
* #PK ^definition = Pakistan
* #PK ^designation[0].language = #de-AT 
* #PK ^designation[0].value = "Pakistan" 
* #PK ^property[0].code = #Relationships 
* #PK ^property[0].valueString = "$.co" 
* #PK ^property[1].code = #hints 
* #PK ^property[1].valueString = "T^E^B" 
* #PL "Poland"
* #PL ^definition = Poland
* #PL ^designation[0].language = #de-AT 
* #PL ^designation[0].value = "Polen" 
* #PL ^property[0].code = #Relationships 
* #PL ^property[0].valueString = "$.co" 
* #PL ^property[1].code = #hints 
* #PL ^property[1].valueString = "T^E^B" 
* #PM "Saint Pierre and Miquelon"
* #PM ^definition = Saint Pierre and Miquelon
* #PM ^designation[0].language = #de-AT 
* #PM ^designation[0].value = "St.Pierre/Miquel" 
* #PM ^property[0].code = #Relationships 
* #PM ^property[0].valueString = "$.co" 
* #PM ^property[1].code = #hints 
* #PM ^property[1].valueString = "T^E^B" 
* #PN "Pitcairn"
* #PN ^definition = Pitcairn
* #PN ^designation[0].language = #de-AT 
* #PN ^designation[0].value = "Pitcairninseln" 
* #PN ^property[0].code = #Relationships 
* #PN ^property[0].valueString = "$.co" 
* #PN ^property[1].code = #hints 
* #PN ^property[1].valueString = "T^E^B" 
* #PR "Puerto Rico"
* #PR ^definition = Puerto Rico
* #PR ^designation[0].language = #de-AT 
* #PR ^designation[0].value = "Puerto Rico" 
* #PR ^property[0].code = #Relationships 
* #PR ^property[0].valueString = "$.co" 
* #PR ^property[1].code = #hints 
* #PR ^property[1].valueString = "T^E^B" 
* #PS "Palestine, State of"
* #PS ^definition = Palestine, State of
* #PS ^designation[0].language = #de-AT 
* #PS ^designation[0].value = "Paläst/Wjld-Gaza" 
* #PS ^property[0].code = #Relationships 
* #PS ^property[0].valueString = "$.co" 
* #PS ^property[1].code = #hints 
* #PS ^property[1].valueString = "T^E^B" 
* #PT "Portugal"
* #PT ^definition = Portugal
* #PT ^designation[0].language = #de-AT 
* #PT ^designation[0].value = "Portugal" 
* #PT ^property[0].code = #Relationships 
* #PT ^property[0].valueString = "$.co" 
* #PT ^property[1].code = #hints 
* #PT ^property[1].valueString = "T^E^B" 
* #PW "Palau"
* #PW ^definition = Palau
* #PW ^designation[0].language = #de-AT 
* #PW ^designation[0].value = "Palau" 
* #PW ^property[0].code = #Relationships 
* #PW ^property[0].valueString = "$.co" 
* #PW ^property[1].code = #hints 
* #PW ^property[1].valueString = "T^E^B" 
* #PY "Paraguay"
* #PY ^definition = Paraguay
* #PY ^designation[0].language = #de-AT 
* #PY ^designation[0].value = "Paraguay" 
* #PY ^property[0].code = #Relationships 
* #PY ^property[0].valueString = "$.co" 
* #PY ^property[1].code = #hints 
* #PY ^property[1].valueString = "T^E^B" 
* #QA "Qatar"
* #QA ^definition = Qatar
* #QA ^designation[0].language = #de-AT 
* #QA ^designation[0].value = "Katar" 
* #QA ^property[0].code = #Relationships 
* #QA ^property[0].valueString = "$.co" 
* #QA ^property[1].code = #hints 
* #QA ^property[1].valueString = "T^E^B" 
* #R-COVI "R-COVI"
* #R-COVI ^definition = R-COVI
* #R-COVI ^designation[0].language = #de-AT 
* #R-COVI ^designation[0].value = "COVID-19-Impfstoff R-COVI (R-Pharm CJSC)" 
* #R-COVI ^property[0].code = #Relationships 
* #R-COVI ^property[0].valueString = "$.v..mp" 
* #R-COVI ^property[1].code = #hints 
* #R-COVI ^property[1].valueString = "T^E^B" 
* #RE "Réunion"
* #RE ^definition = Réunion
* #RE ^designation[0].language = #de-AT 
* #RE ^designation[0].value = "Reunion" 
* #RE ^property[0].code = #Relationships 
* #RE ^property[0].valueString = "$.co" 
* #RE ^property[1].code = #hints 
* #RE ^property[1].valueString = "T^E^B" 
* #RO "Romania"
* #RO ^definition = Romania
* #RO ^designation[0].language = #de-AT 
* #RO ^designation[0].value = "Rumänien" 
* #RO ^property[0].code = #Relationships 
* #RO ^property[0].valueString = "$.co" 
* #RO ^property[1].code = #hints 
* #RO ^property[1].valueString = "T^E^B" 
* #RS "Serbia"
* #RS ^definition = Serbia
* #RS ^designation[0].language = #de-AT 
* #RS ^designation[0].value = "Serbien" 
* #RS ^property[0].code = #Relationships 
* #RS ^property[0].valueString = "$.co" 
* #RS ^property[1].code = #hints 
* #RS ^property[1].valueString = "T^E^B" 
* #RU "Russian Federation (the)"
* #RU ^definition = Russian Federation (the)
* #RU ^designation[0].language = #de-AT 
* #RU ^designation[0].value = "Russische Föderation" 
* #RU ^property[0].code = #Relationships 
* #RU ^property[0].valueString = "$.co" 
* #RU ^property[1].code = #hints 
* #RU ^property[1].valueString = "T^E^B" 
* #RW "Rwanda"
* #RW ^definition = Rwanda
* #RW ^designation[0].language = #de-AT 
* #RW ^designation[0].value = "Ruanda" 
* #RW ^property[0].code = #Relationships 
* #RW ^property[0].valueString = "$.co" 
* #RW ^property[1].code = #hints 
* #RW ^property[1].valueString = "T^E^B" 
* #SA "Saudi Arabia"
* #SA ^definition = Saudi Arabia
* #SA ^designation[0].language = #de-AT 
* #SA ^designation[0].value = "Saudi-Arabien" 
* #SA ^property[0].code = #Relationships 
* #SA ^property[0].valueString = "$.co" 
* #SA ^property[1].code = #hints 
* #SA ^property[1].valueString = "T^E^B" 
* #SB "Solomon Islands"
* #SB ^definition = Solomon Islands
* #SB ^designation[0].language = #de-AT 
* #SB ^designation[0].value = "Salomonen" 
* #SB ^property[0].code = #Relationships 
* #SB ^property[0].valueString = "$.co" 
* #SB ^property[1].code = #hints 
* #SB ^property[1].valueString = "T^E^B" 
* #SC "Seychelles"
* #SC ^definition = Seychelles
* #SC ^designation[0].language = #de-AT 
* #SC ^designation[0].value = "Seychellen" 
* #SC ^property[0].code = #Relationships 
* #SC ^property[0].valueString = "$.co" 
* #SC ^property[1].code = #hints 
* #SC ^property[1].valueString = "T^E^B" 
* #SCTV01C "SCTV01C"
* #SCTV01C ^definition = SCTV01C
* #SCTV01C ^designation[0].language = #de-AT 
* #SCTV01C ^designation[0].value = "COVID-19-Impfstoff SCTV01C (Sinocelltech Ltd.)" 
* #SCTV01C ^property[0].code = #Relationships 
* #SCTV01C ^property[0].valueString = "$.v..mp" 
* #SCTV01C ^property[1].code = #hints 
* #SCTV01C ^property[1].valueString = "T^E^B" 
* #SD "Sudan (the)"
* #SD ^definition = Sudan (the)
* #SD ^designation[0].language = #de-AT 
* #SD ^designation[0].value = "Sudan" 
* #SD ^property[0].code = #Relationships 
* #SD ^property[0].valueString = "$.co" 
* #SD ^property[1].code = #hints 
* #SD ^property[1].valueString = "T^E^B" 
* #SE "Sweden"
* #SE ^definition = Sweden
* #SE ^designation[0].language = #de-AT 
* #SE ^designation[0].value = "Schweden" 
* #SE ^property[0].code = #Relationships 
* #SE ^property[0].valueString = "$.co" 
* #SE ^property[1].code = #hints 
* #SE ^property[1].valueString = "T^E^B" 
* #SG "Singapore"
* #SG ^definition = Singapore
* #SG ^designation[0].language = #de-AT 
* #SG ^designation[0].value = "Singapur" 
* #SG ^property[0].code = #Relationships 
* #SG ^property[0].valueString = "$.co" 
* #SG ^property[1].code = #hints 
* #SG ^property[1].valueString = "T^E^B" 
* #SH "Saint Helena, Ascension and Tristan da Cunha"
* #SH ^definition = Saint Helena, Ascension and Tristan da Cunha
* #SH ^designation[0].language = #de-AT 
* #SH ^designation[0].value = "Sankt Helena" 
* #SH ^property[0].code = #Relationships 
* #SH ^property[0].valueString = "$.co" 
* #SH ^property[1].code = #hints 
* #SH ^property[1].valueString = "T^E^B" 
* #SI "Slovenia"
* #SI ^definition = Slovenia
* #SI ^designation[0].language = #de-AT 
* #SI ^designation[0].value = "Slowenien" 
* #SI ^property[0].code = #Relationships 
* #SI ^property[0].valueString = "$.co" 
* #SI ^property[1].code = #hints 
* #SI ^property[1].valueString = "T^E^B" 
* #SJ "Svalbard and Jan Mayen"
* #SJ ^definition = Svalbard and Jan Mayen
* #SJ ^designation[0].language = #de-AT 
* #SJ ^designation[0].value = "Svalbard/JanMay" 
* #SJ ^property[0].code = #Relationships 
* #SJ ^property[0].valueString = "$.co" 
* #SJ ^property[1].code = #hints 
* #SJ ^property[1].valueString = "T^E^B" 
* #SK "Slovakia"
* #SK ^definition = Slovakia
* #SK ^designation[0].language = #de-AT 
* #SK ^designation[0].value = "Slowakei" 
* #SK ^property[0].code = #Relationships 
* #SK ^property[0].valueString = "$.co" 
* #SK ^property[1].code = #hints 
* #SK ^property[1].valueString = "T^E^B" 
* #SL "Sierra Leone"
* #SL ^definition = Sierra Leone
* #SL ^designation[0].language = #de-AT 
* #SL ^designation[0].value = "Sierra Leone" 
* #SL ^property[0].code = #Relationships 
* #SL ^property[0].valueString = "$.co" 
* #SL ^property[1].code = #hints 
* #SL ^property[1].valueString = "T^E^B" 
* #SM "San Marino"
* #SM ^definition = San Marino
* #SM ^designation[0].language = #de-AT 
* #SM ^designation[0].value = "San Marino" 
* #SM ^property[0].code = #Relationships 
* #SM ^property[0].valueString = "$.co" 
* #SM ^property[1].code = #hints 
* #SM ^property[1].valueString = "T^E^B" 
* #SN "Senegal"
* #SN ^definition = Senegal
* #SN ^designation[0].language = #de-AT 
* #SN ^designation[0].value = "Senegal" 
* #SN ^property[0].code = #Relationships 
* #SN ^property[0].valueString = "$.co" 
* #SN ^property[1].code = #hints 
* #SN ^property[1].valueString = "T^E^B" 
* #SO "Somalia"
* #SO ^definition = Somalia
* #SO ^designation[0].language = #de-AT 
* #SO ^designation[0].value = "Somalia" 
* #SO ^property[0].code = #Relationships 
* #SO ^property[0].valueString = "$.co" 
* #SO ^property[1].code = #hints 
* #SO ^property[1].valueString = "T^E^B" 
* #SR "Suriname"
* #SR ^definition = Suriname
* #SR ^designation[0].language = #de-AT 
* #SR ^designation[0].value = "Suriname" 
* #SR ^property[0].code = #Relationships 
* #SR ^property[0].valueString = "$.co" 
* #SR ^property[1].code = #hints 
* #SR ^property[1].valueString = "T^E^B" 
* #ST "Sao Tome and Principe"
* #ST ^definition = Sao Tome and Principe
* #ST ^designation[0].language = #de-AT 
* #ST ^designation[0].value = "Sao Tome und Principe" 
* #ST ^property[0].code = #Relationships 
* #ST ^property[0].valueString = "$.co" 
* #ST ^property[1].code = #hints 
* #ST ^property[1].valueString = "T^E^B" 
* #SU "Soviet Union"
* #SU ^definition = Soviet Union
* #SU ^designation[0].language = #de-AT 
* #SU ^designation[0].value = "eheml. Sowjetunion" 
* #SU ^property[0].code = #Relationships 
* #SU ^property[0].valueString = "$.co" 
* #SU ^property[1].code = #hints 
* #SU ^property[1].valueString = "T^E^B" 
* #SV "El Salvador"
* #SV ^definition = El Salvador
* #SV ^designation[0].language = #de-AT 
* #SV ^designation[0].value = "El Salvador" 
* #SV ^property[0].code = #Relationships 
* #SV ^property[0].valueString = "$.co" 
* #SV ^property[1].code = #hints 
* #SV ^property[1].valueString = "T^E^B" 
* #SY "Syrian Arab Republic"
* #SY ^definition = Syrian Arab Republic
* #SY ^designation[0].language = #de-AT 
* #SY ^designation[0].value = "Syrien,Arabische Republik" 
* #SY ^property[0].code = #Relationships 
* #SY ^property[0].valueString = "$.co" 
* #SY ^property[1].code = #hints 
* #SY ^property[1].valueString = "T^E^B" 
* #SZ "Eswatini"
* #SZ ^definition = Eswatini
* #SZ ^designation[0].language = #de-AT 
* #SZ ^designation[0].value = "Swasiland" 
* #SZ ^property[0].code = #Relationships 
* #SZ ^property[0].valueString = "$.co" 
* #SZ ^property[1].code = #hints 
* #SZ ^property[1].valueString = "T^E^B" 
* #Sinopharm-WIBP "Sinopharm - Wuhan Institute of Biological Products"
* #Sinopharm-WIBP ^definition = Sinopharm - Wuhan Institute of Biological Products
* #Sinopharm-WIBP ^designation[0].language = #de-AT 
* #Sinopharm-WIBP ^designation[0].value = "Sinopharm - Wuhan Institute of Biological Products" 
* #Sinopharm-WIBP ^property[0].code = #Relationships 
* #Sinopharm-WIBP ^property[0].valueString = "$.v..ma" 
* #Sinopharm-WIBP ^property[1].code = #hints 
* #Sinopharm-WIBP ^property[1].valueString = "T^E^B" 
* #Sinovac-Biotech "Sinovac Biotech"
* #Sinovac-Biotech ^definition = Sinovac Biotech
* #Sinovac-Biotech ^designation[0].language = #de-AT 
* #Sinovac-Biotech ^designation[0].value = "Sinovac Biotech" 
* #Sinovac-Biotech ^property[0].code = #Relationships 
* #Sinovac-Biotech ^property[0].valueString = "$.v..ma" 
* #Sinovac-Biotech ^property[1].code = #hints 
* #Sinovac-Biotech ^property[1].valueString = "T^E^B" 
* #Soberana-02 "Soberana 02"
* #Soberana-02 ^definition = Soberana 02
* #Soberana-02 ^designation[0].language = #de-AT 
* #Soberana-02 ^designation[0].value = "COVID-19-Impfstoff Soberana 02 (Finlay-Institute)" 
* #Soberana-02 ^property[0].code = #Relationships 
* #Soberana-02 ^property[0].valueString = "$.v..mp" 
* #Soberana-02 ^property[1].code = #hints 
* #Soberana-02 ^property[1].valueString = "T^E^B" 
* #Soberana-Plus "Soberana Plus"
* #Soberana-Plus ^definition = Soberana Plus
* #Soberana-Plus ^designation[0].language = #de-AT 
* #Soberana-Plus ^designation[0].value = "COVID-19-Impfstoff Soberana Plus (Finlay-Institute)" 
* #Soberana-Plus ^property[0].code = #Relationships 
* #Soberana-Plus ^property[0].valueString = "$.v..mp" 
* #Soberana-Plus ^property[1].code = #hints 
* #Soberana-Plus ^property[1].valueString = "T^E^B" 
* #Sputnik-Light "Sputnik Light"
* #Sputnik-Light ^definition = Sputnik Light
* #Sputnik-Light ^designation[0].language = #de-AT 
* #Sputnik-Light ^designation[0].value = "COVID-19-Impfstoff Sputnik-Light (Gamaleya)" 
* #Sputnik-Light ^property[0].code = #Relationships 
* #Sputnik-Light ^property[0].valueString = "$.v..mp" 
* #Sputnik-Light ^property[1].code = #hints 
* #Sputnik-Light ^property[1].valueString = "T^E^B" 
* #Sputnik-M "Sputnik-M"
* #Sputnik-M ^definition = Sputnik-M
* #Sputnik-M ^designation[0].language = #de-AT 
* #Sputnik-M ^designation[0].value = "COVID-19-Impfstoff Sputnik-M (Gamaleya Research Institute)" 
* #Sputnik-M ^property[0].code = #Relationships 
* #Sputnik-M ^property[0].valueString = "$.v..mp" 
* #Sputnik-M ^property[1].code = #hints 
* #Sputnik-M ^property[1].valueString = "T^E^B" 
* #Sputnik-V "Sputnik-V"
* #Sputnik-V ^definition = Sputnik-V
* #Sputnik-V ^designation[0].language = #de-AT 
* #Sputnik-V ^designation[0].value = "COVID-19-Impfstoff Sputnik-V (Gamaleya)" 
* #Sputnik-V ^property[0].code = #Relationships 
* #Sputnik-V ^property[0].valueString = "$.v..mp" 
* #Sputnik-V ^property[1].code = #hints 
* #Sputnik-V ^property[1].valueString = "T^E^B" 
* #TA "Tristan da Cunha"
* #TA ^definition = Tristan da Cunha
* #TA ^designation[0].language = #de-AT 
* #TA ^designation[0].value = "Tristan da Cunha" 
* #TA ^property[0].code = #Relationships 
* #TA ^property[0].valueString = "$.co" 
* #TA ^property[1].code = #hints 
* #TA ^property[1].valueString = "T^E^B" 
* #TC "Turks and Caicos Islands (the)"
* #TC ^definition = Turks and Caicos Islands (the)
* #TC ^designation[0].language = #de-AT 
* #TC ^designation[0].value = "Turks/Caicosin" 
* #TC ^property[0].code = #Relationships 
* #TC ^property[0].valueString = "$.co" 
* #TC ^property[1].code = #hints 
* #TC ^property[1].valueString = "T^E^B" 
* #TD "Chad"
* #TD ^definition = Chad
* #TD ^designation[0].language = #de-AT 
* #TD ^designation[0].value = "Tschad" 
* #TD ^property[0].code = #Relationships 
* #TD ^property[0].valueString = "$.co" 
* #TD ^property[1].code = #hints 
* #TD ^property[1].valueString = "T^E^B" 
* #TF "French Southern Territories (the)"
* #TF ^definition = French Southern Territories (the)
* #TF ^designation[0].language = #de-AT 
* #TF ^designation[0].value = "Fr.Südgebiete" 
* #TF ^property[0].code = #Relationships 
* #TF ^property[0].valueString = "$.co" 
* #TF ^property[1].code = #hints 
* #TF ^property[1].valueString = "T^E^B" 
* #TG "Togo"
* #TG ^definition = Togo
* #TG ^designation[0].language = #de-AT 
* #TG ^designation[0].value = "Togo" 
* #TG ^property[0].code = #Relationships 
* #TG ^property[0].valueString = "$.co" 
* #TG ^property[1].code = #hints 
* #TG ^property[1].valueString = "T^E^B" 
* #TH "Thailand"
* #TH ^definition = Thailand
* #TH ^designation[0].language = #de-AT 
* #TH ^designation[0].value = "Thailand" 
* #TH ^property[0].code = #Relationships 
* #TH ^property[0].valueString = "$.co" 
* #TH ^property[1].code = #hints 
* #TH ^property[1].valueString = "T^E^B" 
* #TJ "Tajikistan"
* #TJ ^definition = Tajikistan
* #TJ ^designation[0].language = #de-AT 
* #TJ ^designation[0].value = "Tadschikistan" 
* #TJ ^property[0].code = #Relationships 
* #TJ ^property[0].valueString = "$.co" 
* #TJ ^property[1].code = #hints 
* #TJ ^property[1].valueString = "T^E^B" 
* #TK "Tokelau"
* #TK ^definition = Tokelau
* #TK ^designation[0].language = #de-AT 
* #TK ^designation[0].value = "Tokelau" 
* #TK ^property[0].code = #Relationships 
* #TK ^property[0].valueString = "$.co" 
* #TK ^property[1].code = #hints 
* #TK ^property[1].valueString = "T^E^B" 
* #TL "Timor-Leste"
* #TL ^definition = Timor-Leste
* #TL ^designation[0].language = #de-AT 
* #TL ^designation[0].value = "Timor-Leste" 
* #TL ^property[0].code = #Relationships 
* #TL ^property[0].valueString = "$.co" 
* #TL ^property[1].code = #hints 
* #TL ^property[1].valueString = "T^E^B" 
* #TM "Turkmenistan"
* #TM ^definition = Turkmenistan
* #TM ^designation[0].language = #de-AT 
* #TM ^designation[0].value = "Turkmenistan" 
* #TM ^property[0].code = #Relationships 
* #TM ^property[0].valueString = "$.co" 
* #TM ^property[1].code = #hints 
* #TM ^property[1].valueString = "T^E^B" 
* #TN "Tunisia"
* #TN ^definition = Tunisia
* #TN ^designation[0].language = #de-AT 
* #TN ^designation[0].value = "Tunesien" 
* #TN ^property[0].code = #Relationships 
* #TN ^property[0].valueString = "$.co" 
* #TN ^property[1].code = #hints 
* #TN ^property[1].valueString = "T^E^B" 
* #TO "Tonga"
* #TO ^definition = Tonga
* #TO ^designation[0].language = #de-AT 
* #TO ^designation[0].value = "Tonga" 
* #TO ^property[0].code = #Relationships 
* #TO ^property[0].valueString = "$.co" 
* #TO ^property[1].code = #hints 
* #TO ^property[1].valueString = "T^E^B" 
* #TR "Turkey"
* #TR ^definition = Turkey
* #TR ^designation[0].language = #de-AT 
* #TR ^designation[0].value = "Türkei" 
* #TR ^property[0].code = #Relationships 
* #TR ^property[0].valueString = "$.co" 
* #TR ^property[1].code = #hints 
* #TR ^property[1].valueString = "T^E^B" 
* #TT "Trinidad and Tobago"
* #TT ^definition = Trinidad and Tobago
* #TT ^designation[0].language = #de-AT 
* #TT ^designation[0].value = "Trinidad und Tobago" 
* #TT ^property[0].code = #Relationships 
* #TT ^property[0].valueString = "$.co" 
* #TT ^property[1].code = #hints 
* #TT ^property[1].valueString = "T^E^B" 
* #TV "Tuvalu"
* #TV ^definition = Tuvalu
* #TV ^designation[0].language = #de-AT 
* #TV ^designation[0].value = "Tuvalu" 
* #TV ^property[0].code = #Relationships 
* #TV ^property[0].valueString = "$.co" 
* #TV ^property[1].code = #hints 
* #TV ^property[1].valueString = "T^E^B" 
* #TW "Taiwan (Province of China)"
* #TW ^definition = Taiwan (Province of China)
* #TW ^designation[0].language = #de-AT 
* #TW ^designation[0].value = "Taiwan" 
* #TW ^property[0].code = #Relationships 
* #TW ^property[0].valueString = "$.co" 
* #TW ^property[1].code = #hints 
* #TW ^property[1].valueString = "T^E^B" 
* #TZ "Tanzania, United Republic of"
* #TZ ^definition = Tanzania, United Republic of
* #TZ ^designation[0].language = #de-AT 
* #TZ ^designation[0].value = "Tansania,Vereinigte Rep." 
* #TZ ^property[0].code = #Relationships 
* #TZ ^property[0].valueString = "$.co" 
* #TZ ^property[1].code = #hints 
* #TZ ^property[1].valueString = "T^E^B" 
* #UA "Ukraine"
* #UA ^definition = Ukraine
* #UA ^designation[0].language = #de-AT 
* #UA ^designation[0].value = "Ukraine" 
* #UA ^property[0].code = #Relationships 
* #UA ^property[0].valueString = "$.co" 
* #UA ^property[1].code = #hints 
* #UA ^property[1].valueString = "T^E^B" 
* #UG "Uganda"
* #UG ^definition = Uganda
* #UG ^designation[0].language = #de-AT 
* #UG ^designation[0].value = "Uganda" 
* #UG ^property[0].code = #Relationships 
* #UG ^property[0].valueString = "$.co" 
* #UG ^property[1].code = #hints 
* #UG ^property[1].valueString = "T^E^B" 
* #UM "United States Minor Outlying Islands (the)"
* #UM ^definition = United States Minor Outlying Islands (the)
* #UM ^designation[0].language = #de-AT 
* #UM ^designation[0].value = "Amerik.Ozeanien" 
* #UM ^property[0].code = #Relationships 
* #UM ^property[0].valueString = "$.co" 
* #UM ^property[1].code = #hints 
* #UM ^property[1].valueString = "T^E^B" 
* #US "United States of America (the)"
* #US ^definition = United States of America (the)
* #US ^designation[0].language = #de-AT 
* #US ^designation[0].value = "Vereinigte Staaten" 
* #US ^property[0].code = #Relationships 
* #US ^property[0].valueString = "$.co" 
* #US ^property[1].code = #hints 
* #US ^property[1].valueString = "T^E^B" 
* #UY "Uruguay"
* #UY ^definition = Uruguay
* #UY ^designation[0].language = #de-AT 
* #UY ^designation[0].value = "Uruguay" 
* #UY ^property[0].code = #Relationships 
* #UY ^property[0].valueString = "$.co" 
* #UY ^property[1].code = #hints 
* #UY ^property[1].valueString = "T^E^B" 
* #UZ "Uzbekistan"
* #UZ ^definition = Uzbekistan
* #UZ ^designation[0].language = #de-AT 
* #UZ ^designation[0].value = "Usbekistan" 
* #UZ ^property[0].code = #Relationships 
* #UZ ^property[0].valueString = "$.co" 
* #UZ ^property[1].code = #hints 
* #UZ ^property[1].valueString = "T^E^B" 
* #VA "Holy See (the)"
* #VA ^definition = Holy See (the)
* #VA ^designation[0].language = #de-AT 
* #VA ^designation[0].value = "Vatikanstadt" 
* #VA ^property[0].code = #Relationships 
* #VA ^property[0].valueString = "$.co" 
* #VA ^property[1].code = #hints 
* #VA ^property[1].valueString = "T^E^B" 
* #VC "Saint Vincent and the Grenadines"
* #VC ^definition = Saint Vincent and the Grenadines
* #VC ^designation[0].language = #de-AT 
* #VC ^designation[0].value = "St.Vincent u.d.Grenadinen" 
* #VC ^property[0].code = #Relationships 
* #VC ^property[0].valueString = "$.co" 
* #VC ^property[1].code = #hints 
* #VC ^property[1].valueString = "T^E^B" 
* #VE "Venezuela (Bolivarian Republic of)"
* #VE ^definition = Venezuela (Bolivarian Republic of)
* #VE ^designation[0].language = #de-AT 
* #VE ^designation[0].value = "Venezuela" 
* #VE ^property[0].code = #Relationships 
* #VE ^property[0].valueString = "$.co" 
* #VE ^property[1].code = #hints 
* #VE ^property[1].valueString = "T^E^B" 
* #VG "Virgin Islands (British)"
* #VG ^definition = Virgin Islands (British)
* #VG ^designation[0].language = #de-AT 
* #VG ^designation[0].value = "Br.Jungfernin." 
* #VG ^property[0].code = #Relationships 
* #VG ^property[0].valueString = "$.co" 
* #VG ^property[1].code = #hints 
* #VG ^property[1].valueString = "T^E^B" 
* #VI "Virgin Islands (U.S.)"
* #VI ^definition = Virgin Islands (U.S.)
* #VI ^designation[0].language = #de-AT 
* #VI ^designation[0].value = "Am.Jungfernin." 
* #VI ^property[0].code = #Relationships 
* #VI ^property[0].valueString = "$.co" 
* #VI ^property[1].code = #hints 
* #VI ^property[1].valueString = "T^E^B" 
* #VLA2001 "VLA2001"
* #VLA2001 ^definition = VLA2001
* #VLA2001 ^designation[0].language = #de-AT 
* #VLA2001 ^designation[0].value = "COVID-19-Impfstoff VLA2001 (Valneva France)" 
* #VLA2001 ^property[0].code = #Relationships 
* #VLA2001 ^property[0].valueString = "$.v..mp" 
* #VLA2001 ^property[1].code = #hints 
* #VLA2001 ^property[1].valueString = "T^E^B" 
* #VN "Viet Nam"
* #VN ^definition = Viet Nam
* #VN ^designation[0].language = #de-AT 
* #VN ^designation[0].value = "Vietnam" 
* #VN ^property[0].code = #Relationships 
* #VN ^property[0].valueString = "$.co" 
* #VN ^property[1].code = #hints 
* #VN ^property[1].valueString = "T^E^B" 
* #VU "Vanuatu"
* #VU ^definition = Vanuatu
* #VU ^designation[0].language = #de-AT 
* #VU ^designation[0].value = "Vanuatu" 
* #VU ^property[0].code = #Relationships 
* #VU ^property[0].valueString = "$.co" 
* #VU ^property[1].code = #hints 
* #VU ^property[1].valueString = "T^E^B" 
* #Vector-Institute "Vector Institute"
* #Vector-Institute ^definition = Vector Institute
* #Vector-Institute ^designation[0].language = #de-AT 
* #Vector-Institute ^designation[0].value = "Vector Institute" 
* #Vector-Institute ^property[0].code = #Relationships 
* #Vector-Institute ^property[0].valueString = "$.v..ma" 
* #Vector-Institute ^property[1].code = #hints 
* #Vector-Institute ^property[1].valueString = "T^E^B" 
* #Vidprevtyn "Vidprevtyn"
* #Vidprevtyn ^definition = Vidprevtyn
* #Vidprevtyn ^designation[0].language = #de-AT 
* #Vidprevtyn ^designation[0].value = "COVID-19-Impfstoff Vidprevtyn (Sanofi Pasteur)" 
* #Vidprevtyn ^property[0].code = #Relationships 
* #Vidprevtyn ^property[0].valueString = "$.v..mp" 
* #Vidprevtyn ^property[1].code = #hints 
* #Vidprevtyn ^property[1].valueString = "T^E^B" 
* #WF "Wallis and Futuna"
* #WF ^definition = Wallis and Futuna
* #WF ^designation[0].language = #de-AT 
* #WF ^designation[0].value = "Wallis/Futuna" 
* #WF ^property[0].code = #Relationships 
* #WF ^property[0].valueString = "$.co" 
* #WF ^property[1].code = #hints 
* #WF ^property[1].valueString = "T^E^B" 
* #WIBP-CorV "WIBP-CorV"
* #WIBP-CorV ^definition = WIBP-CorV
* #WIBP-CorV ^designation[0].language = #de-AT 
* #WIBP-CorV ^designation[0].value = "COVID-19-Impfstoff WIBP-CorV (Sinopharm - Wuhan Institute of Biological Products)" 
* #WIBP-CorV ^property[0].code = #Relationships 
* #WIBP-CorV ^property[0].valueString = "$.v..mp" 
* #WIBP-CorV ^property[1].code = #hints 
* #WIBP-CorV ^property[1].valueString = "T^E^B" 
* #WS "Samoa"
* #WS ^definition = Samoa
* #WS ^designation[0].language = #de-AT 
* #WS ^designation[0].value = "Samoa" 
* #WS ^property[0].code = #Relationships 
* #WS ^property[0].valueString = "$.co" 
* #WS ^property[1].code = #hints 
* #WS ^property[1].valueString = "T^E^B" 
* #XK "Republic of Kosovo"
* #XK ^definition = Republic of Kosovo
* #XK ^designation[0].language = #de-AT 
* #XK ^designation[0].value = "Republic of Kosovo" 
* #XK ^property[0].code = #Relationships 
* #XK ^property[0].valueString = "$.co" 
* #XK ^property[1].code = #hints 
* #XK ^property[1].valueString = "T^E^B" 
* #XX "Stateless"
* #XX ^definition = Stateless
* #XX ^designation[0].language = #de-AT 
* #XX ^designation[0].value = "Staatenlos" 
* #XX ^property[0].code = #Relationships 
* #XX ^property[0].valueString = "$.co" 
* #XX ^property[1].code = #hints 
* #XX ^property[1].valueString = "T^E^B" 
* #YE "Yemen"
* #YE ^definition = Yemen
* #YE ^designation[0].language = #de-AT 
* #YE ^designation[0].value = "Jemen" 
* #YE ^property[0].code = #Relationships 
* #YE ^property[0].valueString = "$.co" 
* #YE ^property[1].code = #hints 
* #YE ^property[1].valueString = "T^E^B" 
* #YS-SC2-010 "YS-SC2-010"
* #YS-SC2-010 ^definition = YS-SC2-010
* #YS-SC2-010 ^designation[0].language = #de-AT 
* #YS-SC2-010 ^designation[0].value = "COVID-19-Impfstoff YS-SC2-010 (Yisheng Biopharma)" 
* #YS-SC2-010 ^property[0].code = #Relationships 
* #YS-SC2-010 ^property[0].valueString = "$.v..mp" 
* #YS-SC2-010 ^property[1].code = #hints 
* #YS-SC2-010 ^property[1].valueString = "T^E^B" 
* #YT "Mayotte"
* #YT ^definition = Mayotte
* #YT ^designation[0].language = #de-AT 
* #YT ^designation[0].value = "Mayotte" 
* #YT ^property[0].code = #Relationships 
* #YT ^property[0].valueString = "$.co" 
* #YT ^property[1].code = #hints 
* #YT ^property[1].valueString = "T^E^B" 
* #YU "Yugoslavia"
* #YU ^definition = Yugoslavia
* #YU ^designation[0].language = #de-AT 
* #YU ^designation[0].value = "eheml. Yugoslawien" 
* #YU ^property[0].code = #Relationships 
* #YU ^property[0].valueString = "$.co" 
* #YU ^property[1].code = #hints 
* #YU ^property[1].valueString = "T^E^B" 
* #YY "Unknown"
* #YY ^definition = Unknown
* #YY ^designation[0].language = #de-AT 
* #YY ^designation[0].value = "Unbekannt" 
* #YY ^property[0].code = #Relationships 
* #YY ^property[0].valueString = "$.co" 
* #YY ^property[1].code = #hints 
* #YY ^property[1].valueString = "T^E^B" 
* #Yisheng-Biopharma "Yisheng Biopharma"
* #Yisheng-Biopharma ^definition = Yisheng Biopharma
* #Yisheng-Biopharma ^designation[0].language = #de-AT 
* #Yisheng-Biopharma ^designation[0].value = "Yisheng Biopharma" 
* #Yisheng-Biopharma ^property[0].code = #Relationships 
* #Yisheng-Biopharma ^property[0].valueString = "$.v..ma" 
* #Yisheng-Biopharma ^property[1].code = #hints 
* #Yisheng-Biopharma ^property[1].valueString = "T^E^B" 
* #ZA "South Africa"
* #ZA ^definition = South Africa
* #ZA ^designation[0].language = #de-AT 
* #ZA ^designation[0].value = "Südafrika" 
* #ZA ^property[0].code = #Relationships 
* #ZA ^property[0].valueString = "$.co" 
* #ZA ^property[1].code = #hints 
* #ZA ^property[1].valueString = "T^E^B" 
* #ZM "Zambia"
* #ZM ^definition = Zambia
* #ZM ^designation[0].language = #de-AT 
* #ZM ^designation[0].value = "Sambia" 
* #ZM ^property[0].code = #Relationships 
* #ZM ^property[0].valueString = "$.co" 
* #ZM ^property[1].code = #hints 
* #ZM ^property[1].valueString = "T^E^B" 
* #ZW "Zimbabwe"
* #ZW ^definition = Zimbabwe
* #ZW ^designation[0].language = #de-AT 
* #ZW ^designation[0].value = "Simbabwe" 
* #ZW ^property[0].code = #Relationships 
* #ZW ^property[0].valueString = "$.co" 
* #ZW ^property[1].code = #hints 
* #ZW ^property[1].valueString = "T^E^B" 
