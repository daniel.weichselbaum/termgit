Instance: elga-auditeventtypea-arr 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-auditeventtypea-arr" 
* name = "elga-auditeventtypea-arr" 
* title = "ELGA_AuditEventTypeA-ARR" 
* status = #active 
* content = #complete 
* version = "202005" 
* description = "**Beschreibung:** Codeliste der ELGA spezifischen Audit Event Type Codes (Transaktionen) für A-ARR. Verwendung in IHE konformen und ELGA BeS definierten Audit Events für A-ARR" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.165" 
* date = "2020-05-28" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* count = 15 
* #25 "BürgerIn Anmeldung"
* #25 ^designation[0].language = #de-AT 
* #25 ^designation[0].value = "BürgerIn Anmeldung" 
* #26 "BürgerIn Anmeldung in Vertretung"
* #26 ^designation[0].language = #de-AT 
* #26 ^designation[0].value = "BürgerIn Anmeldung in Vertretung" 
* #27 "BürgerIn Anmeldung der OBST in Vertretung"
* #27 ^designation[0].language = #de-AT 
* #27 ^designation[0].value = "BürgerIn Anmeldung der OBST in Vertretung" 
* #31 "Dokument einbringen"
* #31 ^designation[0].language = #de-AT 
* #31 ^designation[0].value = "e-Befund bereitgestellt" 
* #35 "Dokument suchen"
* #35 ^designation[0].language = #de-AT 
* #35 ^designation[0].value = "ELGA-Abfrage durchgeführt" 
* #36 "Dokument abrufen"
* #36 ^designation[0].language = #de-AT 
* #36 ^designation[0].value = "e-Befund aufgerufen" 
* #4 "Submit Consent"
* #4 ^designation[0].language = #de-AT 
* #4 ^designation[0].value = "Berechtigungsänderung durchgeführt" 
* #42 "Medikationsdaten ändern"
* #42 ^designation[0].language = #de-AT 
* #42 ^designation[0].value = "e-Medikationsdaten geändert" 
* #43 "Medikationsdaten lesen"
* #43 ^designation[0].language = #de-AT 
* #43 ^designation[0].value = "e-Medikationsdaten aufgerufen" 
* #51 "Notify ELGA XAD-PID Link Change"
* #51 ^definition = Transaktion ELGA-1
* #51 ^designation[0].language = #de-AT 
* #51 ^designation[0].value = "Technische Datenkorrektur" 
* #52 "Registrieren von Dokumenten im Rahmen eines Clearings"
* #52 ^designation[0].language = #de-AT 
* #52 ^designation[0].value = "Dieser e-Befund wurde nach einer Datenrichtigstellung nachträglich bereitgestellt" 
* #53 "Stornieren von Dokumenten im Rahmen eines Clearings"
* #53 ^designation[0].language = #de-AT 
* #53 ^designation[0].value = "Im Rahmen einer Datenrichtigstellung wurde dieser e-Befund storniert" 
* #54 "Dokumentenbereinigung im Auftrag des GDA"
* #54 ^designation[0].language = #de-AT 
* #54 ^designation[0].value = "Dokumentenbereinigung im Auftrag des GDA" 
* #61 "Immunisierungsdaten geändert (e-Impfpass)"
* #61 ^designation[0].language = #de-AT 
* #61 ^designation[0].value = "e-Impfpassdaten geändert" 
* #62 "Immunisierungsdaten aufgerufen (e-Impfpass)"
* #62 ^designation[0].language = #de-AT 
* #62 ^designation[0].value = "e-Impfpassdaten aufgerufen" 
