Instance: elga-auditparticipantobjecttype 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-auditparticipantobjecttype" 
* name = "elga-auditparticipantobjecttype" 
* title = "ELGA_AuditParticipantObjectType" 
* status = #active 
* content = #complete 
* description = "**Beschreibung:** Codeliste der ELGA spezifischen Audit Participant Object Type Codes. Der Audit Participant Object Type beschreibt die Art eines Objekts, welches im Audit Event referenziert ist." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.153" 
* date = "2015-06-01" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* count = 5 
* #0 "ELGA Objekt"
* #0 ^designation[0].language = #de-AT 
* #0 ^designation[0].value = "ELGA Objekt" 
* #100 "Policy"
* #100 ^designation[0].language = #de-AT 
* #100 ^designation[0].value = "Policy" 
* #110 "Kontaktbestätigung"
* #110 ^designation[0].language = #de-AT 
* #110 ^designation[0].value = "Kontaktbestätigung" 
* #120 "Security Token"
* #120 ^designation[0].language = #de-AT 
* #120 ^designation[0].value = "Security Token" 
* #130 "Vollmacht"
* #130 ^designation[0].language = #de-AT 
* #130 ^designation[0].value = "Vollmacht" 
