Instance: elga-roleclass 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-roleclass" 
* name = "elga-roleclass" 
* title = "ELGA_RoleClass" 
* status = #active 
* version = "4.0" 
* description = "**Description:** Represents an association or relationshsip between two entities

**Beschreibung:** Unterscheidung zwischen Notfallkontakt und Angehörigem" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.19" 
* date = "2018-06-20" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.hl7.org" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-hl7-role-class"
* compose.include[0].concept[0].code = #ECON
* compose.include[0].concept[0].display = "emergency contact"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "Notfallkontakt" 
* compose.include[0].concept[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[0].designation[1].value = "An entity to be contacted in the event of an emergency." 
* compose.include[0].concept[1].code = #NOK
* compose.include[0].concept[1].display = "next of kin"
* compose.include[0].concept[1].designation[0].language = #de-AT 
* compose.include[0].concept[1].designation[0].value = "Angehörige(r)" 
* compose.include[0].concept[1].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[1].designation[1].value = "An individual designated for notification as the next of kin for a given entity." 

* expansion.timestamp = "2022-09-13T14:17:43.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem-hl7-role-class"
* expansion.contains[0].code = #ECON
* expansion.contains[0].display = "emergency contact"
* expansion.contains[0].designation[0].language = #de-AT 
* expansion.contains[0].designation[0].value = "Notfallkontakt" 
* expansion.contains[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[0].designation[1].value = "An entity to be contacted in the event of an emergency." 
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem-hl7-role-class"
* expansion.contains[1].code = #NOK
* expansion.contains[1].display = "next of kin"
* expansion.contains[1].designation[0].language = #de-AT 
* expansion.contains[1].designation[0].value = "Angehörige(r)" 
* expansion.contains[1].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[1].designation[1].value = "An individual designated for notification as the next of kin for a given entity." 
