Instance: eimpf-ergaenzung 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-eimpf-ergaenzung" 
* name = "eimpf-ergaenzung" 
* title = "eImpf_Ergaenzung" 
* status = #active 
* content = #complete 
* version = "1.1.0+20221007" 
* description = "**Description:** ELGA code list for temporary codes that will be represented in the national SNOMED extension in the future

**Beschreibung:** ELGA Codeliste für temporäre Codes, die in Zukunft in der nationale SNOMED Extension abgebildet werden sollen" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.183" 
* date = "2022-10-07" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* count = 142 
* property[0].code = #hints 
* property[0].type = #string 
* property[1].code = #status 
* property[1].type = #code 
* #B "Auffrischung"
* #B ^property[0].code = #hints 
* #B ^property[0].valueString = "Impfdosis" 
* #B.I "Auffrischungsserie Teil 1"
* #B.I ^property[0].code = #hints 
* #B.I ^property[0].valueString = "Impfdosis" 
* #B.II "Auffrischungsserie Teil 2"
* #B.II ^property[0].code = #hints 
* #B.II ^property[0].valueString = "Impfdosis" 
* #D1 "Dosis 1"
* #D1 ^property[0].code = #hints 
* #D1 ^property[0].valueString = "Impfdosis" 
* #D2 "Dosis 2"
* #D2 ^property[0].code = #hints 
* #D2 ^property[0].valueString = "Impfdosis" 
* #D3 "Dosis 3"
* #D3 ^property[0].code = #hints 
* #D3 ^property[0].valueString = "Impfdosis" 
* #D4 "Dosis 4"
* #D4 ^property[0].code = #hints 
* #D4 ^property[0].valueString = "Impfdosis" 
* #D5 "Dosis 5"
* #D5 ^property[0].code = #hints 
* #D5 ^property[0].valueString = "Impfdosis" 
* #D6 "Dosis 6"
* #D6 ^property[0].code = #hints 
* #D6 ^property[0].valueString = "Impfdosis" 
* #D7 "Dosis 7"
* #D7 ^property[0].code = #hints 
* #D7 ^property[0].valueString = "Impfdosis" 
* #D8 "Dosis 8"
* #D8 ^property[0].code = #hints 
* #D8 ^property[0].valueString = "Impfdosis" 
* #D9 "Dosis 9"
* #D9 ^property[0].code = #hints 
* #D9 ^property[0].valueString = "Impfdosis" 
* #DGC-R "Genesungs-Zertifikat"
* #DGC-T "Test-Zertifikat"
* #DGC-V "Impf-Zertifikat"
* #IG1 "Indikationsimpfung für Risikogruppe"
* #IG1 ^property[0].code = #hints 
* #IG1 ^property[0].valueString = "Impfgrund" 
* #IG2 "Wiederholungsimpfung aufgrund medizinischer Indikation"
* #IG2 ^property[0].code = #hints 
* #IG2 ^property[0].valueString = "Impfgrund" 
* #IS001 "Bildungseinrichtung"
* #IS001 ^property[0].code = #hints 
* #IS001 ^property[0].valueString = "Impfsetting" 
* #IS002 "Arbeitsplatz/Betriebe"
* #IS002 ^property[0].code = #hints 
* #IS002 ^property[0].valueString = "Impfsetting" 
* #IS003 "Krankenhaus inkl. Kur- und Rehaeinrichtungen"
* #IS003 ^property[0].code = #status 
* #IS003 ^property[0].valueCode = #retired 
* #IS003 ^property[1].code = #hints 
* #IS003 ^property[1].valueString = "Impfsetting" 
* #IS004 "Ordination"
* #IS004 ^property[0].code = #status 
* #IS004 ^property[0].valueCode = #retired 
* #IS004 ^property[1].code = #hints 
* #IS004 ^property[1].valueString = "Impfsetting" 
* #IS005 "Öffentliche Impfstelle/Impfstraße"
* #IS005 ^property[0].code = #hints 
* #IS005 ^property[0].valueString = "Impfsetting" 
* #IS006 "Wohnbereich und Betreuungseinrichtungen"
* #IS006 ^property[0].code = #hints 
* #IS006 ^property[0].valueString = "Impfsetting" 
* #SCHEMA001 "Kombinationsschema Di-Te-Pert-HiB-IPV-HepB, Erstimpfung bis 1 Jahr"
* #SCHEMA001 ^property[0].code = #hints 
* #SCHEMA001 ^property[0].valueString = "Impfschema" 
* #SCHEMA002 "Kombinationsschema Di-Te-Pert-HiB-IPV-HepB, Erstimpfung ab 1 Jahr"
* #SCHEMA002 ^property[0].code = #hints 
* #SCHEMA002 ^property[0].valueString = "Impfschema" 
* #SCHEMA003 "Kombinationsschema Di-Te-Pert-IPV-HepB, Erstimpfung ab 6 Jahren"
* #SCHEMA003 ^property[0].code = #hints 
* #SCHEMA003 ^property[0].valueString = "Impfschema" 
* #SCHEMA004 "Kombinationsschema Di-Te-Pert-HiB-IPV-HepB, Indikation"
* #SCHEMA004 ^property[0].code = #hints 
* #SCHEMA004 ^property[0].valueString = "Impfschema" 
* #SCHEMA005 "FSME Grundschema, FSME-Immun"
* #SCHEMA005 ^property[0].code = #hints 
* #SCHEMA005 ^property[0].valueString = "Impfschema" 
* #SCHEMA006 "FSME Grundschema, Encepur"
* #SCHEMA006 ^property[0].code = #hints 
* #SCHEMA006 ^property[0].valueString = "Impfschema" 
* #SCHEMA007 "FSME Grundschema, FSME-Immun, Erstimpfung bis 1 Jahr"
* #SCHEMA008 "FSME Grundschema, Encepur, Erstimpfung bis 1 Jahr"
* #SCHEMA009 "FSME Schnellschema, FSME-Immun"
* #SCHEMA009 ^property[0].code = #hints 
* #SCHEMA009 ^property[0].valueString = "Impfschema" 
* #SCHEMA010 "FSME Schnellschema, Encepur"
* #SCHEMA010 ^property[0].code = #hints 
* #SCHEMA010 ^property[0].valueString = "Impfschema" 
* #SCHEMA011 "Haemophilus influenzae Typ B Indikationsschema"
* #SCHEMA011 ^property[0].code = #hints 
* #SCHEMA011 ^property[0].valueString = "Impfschema" 
* #SCHEMA012 "Hepatitis A Monokomponente Indikationsschema"
* #SCHEMA012 ^property[0].code = #hints 
* #SCHEMA012 ^property[0].valueString = "Impfschema" 
* #SCHEMA013 "Hepatitis AB Grundschema"
* #SCHEMA013 ^property[0].code = #hints 
* #SCHEMA013 ^property[0].valueString = "Impfschema" 
* #SCHEMA014 "Hepatitis A Monokomponente Grundschema"
* #SCHEMA014 ^property[0].code = #hints 
* #SCHEMA014 ^property[0].valueString = "Impfschema" 
* #SCHEMA015 "Hepatitis AB Schnellschema"
* #SCHEMA015 ^property[0].code = #hints 
* #SCHEMA015 ^property[0].valueString = "Impfschema" 
* #SCHEMA016 "Hepatitis A & Typhus Grundschema"
* #SCHEMA016 ^property[0].code = #hints 
* #SCHEMA016 ^property[0].valueString = "Impfschema" 
* #SCHEMA017 "Hepatitis B Monokomponente Grundschema"
* #SCHEMA017 ^property[0].code = #hints 
* #SCHEMA017 ^property[0].valueString = "Impfschema" 
* #SCHEMA019 "Hepatitis B erhoehte Antigenmenge Grundschema"
* #SCHEMA019 ^property[0].code = #hints 
* #SCHEMA019 ^property[0].valueString = "Impfschema" 
* #SCHEMA020 "HPV Grundschema, bis 18 Jahre"
* #SCHEMA020 ^property[0].code = #hints 
* #SCHEMA020 ^property[0].valueString = "Impfschema" 
* #SCHEMA021 "HPV Grundschema, ab 18 Jahren"
* #SCHEMA021 ^property[0].code = #hints 
* #SCHEMA021 ^property[0].valueString = "Impfschema" 
* #SCHEMA022 "HPV Indikationsschema"
* #SCHEMA022 ^property[0].code = #hints 
* #SCHEMA022 ^property[0].valueString = "Impfschema" 
* #SCHEMA023 "Influenza Grundschema, Erstimpfung Kinder"
* #SCHEMA023 ^property[0].code = #hints 
* #SCHEMA023 ^property[0].valueString = "Impfschema" 
* #SCHEMA024 "Influenza Grundschema, Einmalimpfung"
* #SCHEMA024 ^property[0].code = #hints 
* #SCHEMA024 ^property[0].valueString = "Impfschema" 
* #SCHEMA025 "MMR Grundschema ab 9 Monaten"
* #SCHEMA025 ^property[0].code = #hints 
* #SCHEMA025 ^property[0].valueString = "Impfschema" 
* #SCHEMA027 "MMR Indikationsschema, Erstimpfung 6-8 Monate"
* #SCHEMA027 ^property[0].code = #hints 
* #SCHEMA027 ^property[0].valueString = "Impfschema" 
* #SCHEMA028 "Meningokokken B Grundschema, Erstimpfung 2-5 Monate, 4 Dosen"
* #SCHEMA028 ^property[0].code = #status 
* #SCHEMA028 ^property[0].valueCode = #retired 
* #SCHEMA028 ^property[1].code = #hints 
* #SCHEMA028 ^property[1].valueString = "DEPRECATED" 
* #SCHEMA029 "Meningokokken B Grundschema, Erstimpfung 2-5 Monate"
* #SCHEMA029 ^property[0].code = #hints 
* #SCHEMA029 ^property[0].valueString = "Impfschema" 
* #SCHEMA030 "Meningokokken B Grundschema, Erstimpfung 6-11 Monate"
* #SCHEMA030 ^property[0].code = #hints 
* #SCHEMA030 ^property[0].valueString = "Impfschema" 
* #SCHEMA031 "Meningokokken B Grundschema, Erstimpfung 12-23 Monate"
* #SCHEMA031 ^property[0].code = #hints 
* #SCHEMA031 ^property[0].valueString = "Impfschema" 
* #SCHEMA032 "Meningokokken B Grundschema, Erstimpfung 2-18 Jahre, Bexsero"
* #SCHEMA032 ^property[0].code = #hints 
* #SCHEMA032 ^property[0].valueString = "Impfschema" 
* #SCHEMA033 "Meningokokken B Indikationsschema, ab 25 Jahren, Bexsero"
* #SCHEMA033 ^property[0].code = #hints 
* #SCHEMA033 ^property[0].valueString = "Impfschema" 
* #SCHEMA034 "Meningokokken B Indikationsschema, ab 25 Jahren, Trumenba"
* #SCHEMA034 ^property[0].code = #hints 
* #SCHEMA034 ^property[0].valueString = "Impfschema" 
* #SCHEMA035 "Meningokokken C Indikationsschema, 2-4 Monate, Neisvac C"
* #SCHEMA035 ^property[0].code = #hints 
* #SCHEMA035 ^property[0].valueString = "Impfschema" 
* #SCHEMA036 "Meningokokken C Indikationsschema, 2-12 Monate, Menjugate"
* #SCHEMA036 ^property[0].code = #hints 
* #SCHEMA036 ^property[0].valueString = "Impfschema" 
* #SCHEMA037 "Meningokokken C Indikationsschema, 4-12 Monate, Neisvac C"
* #SCHEMA037 ^property[0].code = #hints 
* #SCHEMA037 ^property[0].valueString = "Impfschema" 
* #SCHEMA038 "Meningokokken C Grundschema, ab 1 Jahr"
* #SCHEMA038 ^property[0].code = #hints 
* #SCHEMA038 ^property[0].valueString = "Impfschema" 
* #SCHEMA039 "Meningokokken ACWY Grundschema, ab 10 Jahren"
* #SCHEMA039 ^property[0].code = #hints 
* #SCHEMA039 ^property[0].valueString = "Impfschema" 
* #SCHEMA040 "Meningokokken ACWY Indikationsschema, ab 1 Jahr"
* #SCHEMA040 ^property[0].code = #status 
* #SCHEMA040 ^property[0].valueCode = #retired 
* #SCHEMA040 ^property[1].code = #hints 
* #SCHEMA040 ^property[1].valueString = "DEPRECATED" 
* #SCHEMA041 "Meningokokken ACWY Indikationsschema, Erstimpfung bis 1 Jahr"
* #SCHEMA041 ^property[0].code = #status 
* #SCHEMA041 ^property[0].valueCode = #retired 
* #SCHEMA041 ^property[1].code = #hints 
* #SCHEMA041 ^property[1].valueString = "DEPRECATED" 
* #SCHEMA042 "Pneumokokken Grundschema, Erstimpfung bis 1 Jahr"
* #SCHEMA042 ^property[0].code = #hints 
* #SCHEMA042 ^property[0].valueString = "Impfschema" 
* #SCHEMA043 "Pneumokokken Grundschema, Erstimpfung 1-2 Jahre"
* #SCHEMA043 ^property[0].code = #hints 
* #SCHEMA043 ^property[0].valueString = "Impfschema" 
* #SCHEMA044 "Pneumokokken Indikationsschema (hohes Risiko), Erstimpfung bis 1 Jahr"
* #SCHEMA044 ^property[0].code = #hints 
* #SCHEMA044 ^property[0].valueString = "Impfschema" 
* #SCHEMA045 "Pneumokokken Indikationsschema (hohes Risiko), Erstimpfung im 2. Lebensjahr"
* #SCHEMA045 ^property[0].code = #hints 
* #SCHEMA045 ^property[0].valueString = "Impfschema" 
* #SCHEMA046 "Pneumokokken Indikationsschema (hohes Risiko), Erstimpfung ab 3 Jahren"
* #SCHEMA046 ^property[0].code = #hints 
* #SCHEMA046 ^property[0].valueString = "Impfschema" 
* #SCHEMA050 "Rotavirus Grundschema, 2 Dosen, Erstimpfung 6.- 19. Woche, Rotarix"
* #SCHEMA050 ^property[0].code = #hints 
* #SCHEMA050 ^property[0].valueString = "Impfschema" 
* #SCHEMA051 "Rotavirus Grundschema, 2 Dosen, Erstimpfung 6.- 27. Woche, Rotateq"
* #SCHEMA051 ^property[0].code = #hints 
* #SCHEMA051 ^property[0].valueString = "Impfschema" 
* #SCHEMA052 "Rotavirus Grundschema, 1 Dosis, Erstimpfung 19. - 23. Woche, Rotarix"
* #SCHEMA052 ^property[0].code = #hints 
* #SCHEMA052 ^property[0].valueString = "Impfschema" 
* #SCHEMA053 "Rotavirus Grundschema, 1 Dosis, Erstimpfung 28. - 32. Woche, Rotateq"
* #SCHEMA053 ^property[0].code = #hints 
* #SCHEMA053 ^property[0].valueString = "Impfschema" 
* #SCHEMA054 "Rotavirus Grundschema, 3 Dosen, Erstimpfung 6. - 23. Woche, Rotateq"
* #SCHEMA054 ^property[0].code = #hints 
* #SCHEMA054 ^property[0].valueString = "Impfschema" 
* #SCHEMA060 "Varizellen Grundschema, ab 1 Jahr"
* #SCHEMA060 ^property[0].code = #hints 
* #SCHEMA060 ^property[0].valueString = "Impfschema" 
* #SCHEMA061 "Kombinationsschema MMRV, ab 9 Monaten, MMR - MMRV - V"
* #SCHEMA061 ^property[0].code = #hints 
* #SCHEMA061 ^property[0].valueString = "Impfschema" 
* #SCHEMA062 "Kombinationsschema MMRV, ab 12 Monaten, MMR - MMRV - V"
* #SCHEMA062 ^property[0].code = #hints 
* #SCHEMA062 ^property[0].valueString = "Impfschema" 
* #SCHEMA063 "Varizellen Indikationsschema, Erstimpfung bis 1 Jahr"
* #SCHEMA063 ^property[0].code = #hints 
* #SCHEMA063 ^property[0].valueString = "Impfschema" 
* #SCHEMA064 "Herpes Zoster Grundschema, ab 50 Jahren"
* #SCHEMA064 ^property[0].code = #hints 
* #SCHEMA064 ^property[0].valueString = "Impfschema" 
* #SCHEMA065 "Gelbfieber Grundschema, ab 1 Jahr"
* #SCHEMA065 ^property[0].code = #hints 
* #SCHEMA065 ^property[0].valueString = "Impfschema" 
* #SCHEMA066 "Japanische Enzephalitis Grundschema, ab 2 Monaten"
* #SCHEMA066 ^property[0].code = #hints 
* #SCHEMA066 ^property[0].valueString = "Impfschema" 
* #SCHEMA067 "Japanische Enzephalitis Schnellschema, ab 18 Jahren"
* #SCHEMA067 ^property[0].code = #hints 
* #SCHEMA067 ^property[0].valueString = "Impfschema" 
* #SCHEMA068 "Typhus Grundschema, Injektion"
* #SCHEMA068 ^property[0].code = #hints 
* #SCHEMA068 ^property[0].valueString = "Impfschema" 
* #SCHEMA069 "Typhus Grundschema, Oral"
* #SCHEMA069 ^property[0].code = #hints 
* #SCHEMA069 ^property[0].valueString = "Impfschema" 
* #SCHEMA070 "Tollwut Grundschema"
* #SCHEMA070 ^property[0].code = #hints 
* #SCHEMA070 ^property[0].valueString = "Impfschema" 
* #SCHEMA071 "Tollwut Schnellschema"
* #SCHEMA071 ^property[0].code = #hints 
* #SCHEMA071 ^property[0].valueString = "Impfschema" 
* #SCHEMA072 "Tollwut postexpositionelles Schema, Essen 4 Dosen"
* #SCHEMA072 ^property[0].code = #hints 
* #SCHEMA072 ^property[0].valueString = "Impfschema" 
* #SCHEMA073 "Tollwut postexpositionelles Schema, Essen 5 Dosen"
* #SCHEMA073 ^property[0].code = #hints 
* #SCHEMA073 ^property[0].valueString = "Impfschema" 
* #SCHEMA074 "Tollwut postexpositionelles Schema, Zagreb"
* #SCHEMA074 ^property[0].code = #hints 
* #SCHEMA074 ^property[0].valueString = "Impfschema" 
* #SCHEMA075 "Tollwut postexpositionelles Schema, mit Tollwut-Vorimpfung"
* #SCHEMA075 ^property[0].code = #hints 
* #SCHEMA075 ^property[0].valueString = "Impfschema" 
* #SCHEMA076 "Cholera Grundschema, Erstimpfung 2-6 Jahre"
* #SCHEMA076 ^property[0].code = #hints 
* #SCHEMA076 ^property[0].valueString = "Impfschema" 
* #SCHEMA078 "Cholera Grundschema, Erstimpfung ab 6 Jahren"
* #SCHEMA078 ^property[0].code = #hints 
* #SCHEMA078 ^property[0].valueString = "Impfschema" 
* #SCHEMA079 "Meningokokken B Grundschema, Erstimpfung 11-18 Jahre, Trumenba"
* #SCHEMA079 ^property[0].code = #hints 
* #SCHEMA079 ^property[0].valueString = "Impfschema" 
* #SCHEMA080 "Kombinationsschema Di-Te-Pert-IPV"
* #SCHEMA080 ^property[0].code = #hints 
* #SCHEMA080 ^property[0].valueString = "Impfschema" 
* #SCHEMA081 "Kombinationsschema Di-Te-Pert"
* #SCHEMA081 ^property[0].code = #hints 
* #SCHEMA081 ^property[0].valueString = "Impfschema" 
* #SCHEMA082 "Kombinationsschema Di-Te"
* #SCHEMA082 ^property[0].code = #hints 
* #SCHEMA082 ^property[0].valueString = "Impfschema" 
* #SCHEMA083 "Tetanus Monokomponente"
* #SCHEMA083 ^property[0].code = #hints 
* #SCHEMA083 ^property[0].valueString = "Impfschema" 
* #SCHEMA084 "Poliomyelitis Monokomponente"
* #SCHEMA084 ^property[0].code = #hints 
* #SCHEMA084 ^property[0].valueString = "Impfschema" 
* #SCHEMA085 "Hepatitis B Monokomponente Indikationsschema"
* #SCHEMA085 ^property[0].code = #hints 
* #SCHEMA085 ^property[0].valueString = "Impfschema" 
* #SCHEMA086 "SARS-CoV-2 Grundschema, Comirnaty"
* #SCHEMA086 ^property[0].code = #hints 
* #SCHEMA086 ^property[0].valueString = "Impfschema" 
* #SCHEMA087 "SARS-CoV-2 Grundschema, Moderna"
* #SCHEMA087 ^property[0].code = #hints 
* #SCHEMA087 ^property[0].valueString = "Impfschema" 
* #SCHEMA088 "SARS-CoV-2 Grundschema, AstraZeneca"
* #SCHEMA088 ^property[0].code = #hints 
* #SCHEMA088 ^property[0].valueString = "Impfschema" 
* #SCHEMA089 "SARS-CoV-2 Grundschema, Janssen"
* #SCHEMA089 ^property[0].code = #hints 
* #SCHEMA089 ^property[0].valueString = "Impfschema" 
* #SCHEMA090 "SARS-CoV-2 Grundschema, Auslandsimpfung"
* #SCHEMA090 ^property[0].code = #hints 
* #SCHEMA090 ^property[0].valueString = "Impfschema" 
* #SCHEMA091 "Herpes Zoster Grundschema, nach Zostavax"
* #SCHEMA091 ^property[0].code = #hints 
* #SCHEMA091 ^property[0].valueString = "Impfschema" 
* #SCHEMA092 "Herpes Zoster Indikationsschema, ab 18 Jahren"
* #SCHEMA092 ^property[0].code = #hints 
* #SCHEMA092 ^property[0].valueString = "Impfschema" 
* #SCHEMA093 "MMR Grundschema, ab 1 Jahr"
* #SCHEMA093 ^property[0].code = #hints 
* #SCHEMA093 ^property[0].valueString = "Impfschema" 
* #SCHEMA094 "SARS-CoV-2 Indikationsschema, Comirnaty"
* #SCHEMA094 ^property[0].code = #status 
* #SCHEMA094 ^property[0].valueCode = #retired 
* #SCHEMA094 ^property[1].code = #hints 
* #SCHEMA094 ^property[1].valueString = "DEPRECATED" 
* #SCHEMA095 "SARS-CoV-2 Indikationsschema, Moderna"
* #SCHEMA095 ^property[0].code = #status 
* #SCHEMA095 ^property[0].valueCode = #retired 
* #SCHEMA095 ^property[1].code = #hints 
* #SCHEMA095 ^property[1].valueString = "DEPRECATED" 
* #SCHEMA096 "SARS-CoV-2 heterologes Schema (Impfstoffwechsel)"
* #SCHEMA096 ^property[0].code = #hints 
* #SCHEMA096 ^property[0].valueString = "Impfschema" 
* #SCHEMA097 "Pneumokokken Grundschema, Erstimpfung 2-5 Jahre"
* #SCHEMA097 ^property[0].code = #hints 
* #SCHEMA097 ^property[0].valueString = "Impfschema" 
* #SCHEMA098 "Pneumokokken Grundschema, ab 60 Jahre"
* #SCHEMA098 ^property[0].code = #hints 
* #SCHEMA098 ^property[0].valueString = "Impfschema" 
* #SCHEMA099 "SARS-CoV-2 Grundschema, Nuvaxovid"
* #SCHEMA099 ^property[0].code = #hints 
* #SCHEMA099 ^property[0].valueString = "Impfschema" 
* #SCHEMA100 "Kombinationsschema MMRV, ab 9 Monaten, MMRV - MMRV"
* #SCHEMA100 ^property[0].code = #hints 
* #SCHEMA100 ^property[0].valueString = "Impfschema" 
* #SCHEMA101 "Kombinationsschema MMRV, ab 12 Monaten, MMRV - MMRV"
* #SCHEMA101 ^property[0].code = #hints 
* #SCHEMA101 ^property[0].valueString = "Impfschema" 
* #SCHEMA102 "Hepatitis B Monokomponente Schnellschema durch Indikation"
* #SCHEMA102 ^property[0].code = #hints 
* #SCHEMA102 ^property[0].valueString = "Impfschema" 
* #SCHEMA103 "Hepatitis B Prophylaxe Neugeborener Indikationsschema"
* #SCHEMA103 ^property[0].code = #hints 
* #SCHEMA103 ^property[0].valueString = "Impfschema" 
* #SCHEMA104 "Influenza Indikationsschema"
* #SCHEMA104 ^property[0].code = #hints 
* #SCHEMA104 ^property[0].valueString = "Impfschema" 
* #SCHEMA105 "Kombinationsschema Di-Te-IPV"
* #SCHEMA105 ^property[0].code = #hints 
* #SCHEMA105 ^property[0].valueString = "Impfschema" 
* #SCHEMA106 "Meningokokken ACWY Indikationsschema, Nimenrix bis 6 Monate"
* #SCHEMA106 ^property[0].code = #hints 
* #SCHEMA106 ^property[0].valueString = "Impfschema" 
* #SCHEMA107 "Meningokokken ACWY Indikationsschema, Nimenrix 6-12 Monate"
* #SCHEMA107 ^property[0].code = #hints 
* #SCHEMA107 ^property[0].valueString = "Impfschema" 
* #SCHEMA108 "Meningokokken ACWY Indikationsschema, Nimenrix ab 1 Jahr"
* #SCHEMA108 ^property[0].code = #hints 
* #SCHEMA108 ^property[0].valueString = "Impfschema" 
* #SCHEMA109 "Meningokokken ACWY Indikationsschema, Menveo ab 2 Jahre"
* #SCHEMA109 ^property[0].code = #hints 
* #SCHEMA109 ^property[0].valueString = "Impfschema" 
* #SCHEMA110 "SARS-CoV-2 Indikationsschema, 4 Dosen"
* #SCHEMA110 ^property[0].code = #hints 
* #SCHEMA110 ^property[0].valueString = "Impfschema" 
* #SCHEMA111 "(Affen-)Pocken Indikationsschema"
* #SCHEMA111 ^property[0].code = #hints 
* #SCHEMA111 ^property[0].valueString = "Impfschema" 
* #SCHEMA112 "SARS-CoV-2 Grundschema, Valneva"
* #SCHEMA112 ^property[0].code = #hints 
* #SCHEMA112 ^property[0].valueString = "Impfschema" 
* #SCHEMA113 "Kombinationsschema MMRV, ab 9 Monaten, MMRV - MMR - V"
* #SCHEMA113 ^property[0].code = #hints 
* #SCHEMA113 ^property[0].valueString = "Impfschema" 
* #SCHEMA114 "Kombinationsschema MMRV, ab 12 Monaten, MMRV - MMR - V"
* #SCHEMA114 ^property[0].code = #hints 
* #SCHEMA114 ^property[0].valueString = "Impfschema" 
* #SCHEMA115 "(Affen-)Pocken Indikationsschema, einmalige Impfung"
* #SCHEMA116 "Pneumokokken Indikationsschema (erhöhtes Risiko), ab 50 Jahren"
* #_AllgemeineBevölkerung "Allgemeine Bevölkerung"
* #_AllgemeineBevölkerung ^property[0].code = #hints 
* #_AllgemeineBevölkerung ^property[0].valueString = "Exponierte Personengruppen" 
* #_Ausnahme "Ausnahme"
* #_Ausnahme ^property[0].code = #hints 
* #_Ausnahme ^property[0].valueString = "SpecialCaseVaccination" 
* #_Blau "Blau"
* #_Blau ^property[0].code = #hints 
* #_Blau ^property[0].valueString = "SpecialCaseVaccination" 
* #_Einzel "Impfungen"
* #_Einzel ^property[0].code = #hints 
* #_Einzel ^property[0].valueString = "Immunizationtarget" 
* #_Empfohlene "Empfohlene Impfungen"
* #_Empfohlene ^property[0].code = #hints 
* #_Empfohlene ^property[0].valueString = "Immunizationtarget" 
* #_Gelb "Gelb"
* #_Gelb ^property[0].code = #hints 
* #_Gelb ^property[0].valueString = "SpecialCaseVaccination" 
* #_Grau "Grau"
* #_Grau ^property[0].code = #hints 
* #_Grau ^property[0].valueString = "SpecialCaseVaccination" 
* #_Grün "Grün"
* #_Grün ^property[0].code = #hints 
* #_Grün ^property[0].valueString = "SpecialCaseVaccination" 
* #_Kombinationen "Kombinationsimpfungen"
* #_Kombinationen ^property[0].code = #hints 
* #_Kombinationen ^property[0].valueString = "Immunizationtarget" 
* #_Reise "Reiseimpfungen"
* #_Reise ^property[0].code = #hints 
* #_Reise ^property[0].valueString = "Immunizationtarget" 
* #_Rot "Rot"
* #_Rot ^property[0].code = #hints 
* #_Rot ^property[0].valueString = "SpecialCaseVaccination" 
* #_SpezielleRisikogruppefür "Spezielle Risikogruppe für"
* #_SpezielleRisikogruppefür ^property[0].code = #hints 
* #_SpezielleRisikogruppefür ^property[0].valueString = "SpecialSituationIndication" 
* #_Weitere "Weitere Impfungen"
* #_Weitere ^property[0].code = #hints 
* #_Weitere ^property[0].valueString = "Immunizationtarget" 
* #off-label-use "off-label-use"
