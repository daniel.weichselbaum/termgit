Instance: ems-wurdekrankheitimportiert 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-wurdekrankheitimportiert" 
* name = "ems-wurdekrankheitimportiert" 
* title = "EMS_WurdeKrankheitImportiert" 
* status = #active 
* content = #complete 
* version = "202006" 
* description = "**Description:** Codelist for the epidemiological reporting system Austria coding whether the illness was imported

**Beschreibung:** Codeliste epidemiologisches Meldesystem: wurde die Krankheit importiert" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.110" 
* date = "2020-06-22" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* count = 4 
* concept[0].code = #ICASE
* concept[0].display = "infiziert durch importierten Fall"
* concept[1].code = #IMP
* concept[1].display = "importiert"
* concept[2].code = #NIMP
* concept[2].display = "nicht importiert"
* concept[3].code = #UNK
* concept[3].display = "unbekannt"
