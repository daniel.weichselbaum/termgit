Instance: elga-auditsourcetype 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-auditsourcetype" 
* name = "elga-auditsourcetype" 
* title = "ELGA_AuditSourceType" 
* status = #active 
* content = #complete 
* version = "202202" 
* description = "**Beschreibung:** ELGA Codeliste für autonome ELGA-Anwendungen und Services (relevant für das Berechtigungssystem)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.152" 
* date = "2022-02-23" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* count = 15 
* #0 "PAP"
* #0 ^definition = ELGA PAP
* #0 ^designation[0].language = #de-AT 
* #0 ^designation[0].value = "PAP" 
* #1 "KBS"
* #1 ^definition = ELGA Kontaktbestätigungsservice
* #1 ^designation[0].language = #de-AT 
* #1 ^designation[0].value = "KBS" 
* #10 "CDM"
* #10 ^definition = Zentrales Content Delete Management Service
* #10 ^designation[0].language = #de-AT 
* #10 ^designation[0].value = "CDM" 
* #11 "CDD"
* #11 ^definition = Dezentraler Content Delete Management Daemon
* #11 ^designation[0].language = #de-AT 
* #11 ^designation[0].value = "CDD" 
* #12 "e-Medikation"
* #12 ^designation[0].language = #de-AT 
* #12 ^designation[0].value = "e-Medikation" 
* #13 "EMS Epidemiologisches Meldesystem/EPI-Service"
* #13 ^designation[0].language = #de-AT 
* #13 ^designation[0].value = "EMS Epidemiologisches Meldesystem/EPI-Service" 
* #14 "DSUB Broker"
* #2 "ETS"
* #2 ^definition = ELGA Token Service
* #2 ^designation[0].language = #de-AT 
* #2 ^designation[0].value = "ETS" 
* #3 "Policy Repository"
* #3 ^designation[0].language = #de-AT 
* #3 ^designation[0].value = "Policy Repository" 
* #4 "Bürgerportal"
* #4 ^designation[0].language = #de-AT 
* #4 ^designation[0].value = "Bürgerportal" 
* #5 "Policy Administraion"
* #5 ^definition = Tool für die Verwaltung allgemeiner Policies
* #5 ^designation[0].language = #de-AT 
* #5 ^designation[0].value = "Policy Administraion" 
* #6 "GDA"
* #6 ^definition = GDA System
* #6 ^designation[0].language = #de-AT 
* #6 ^designation[0].value = "GDA" 
* #7 "Gateway"
* #7 ^definition = Zwischengeschaltetes System, Proxy,...
* #7 ^designation[0].language = #de-AT 
* #7 ^designation[0].value = "Gateway" 
* #8 "ZGF"
* #8 ^definition = Zugriffssteuerungsfassade eines ELGA Bereichs
* #8 ^designation[0].language = #de-AT 
* #8 ^designation[0].value = "ZGF" 
* #9 "A-ARR"
* #9 ^definition = Aggregiertes Audit Record Repository
* #9 ^designation[0].language = #de-AT 
* #9 ^designation[0].value = "A-ARR" 
