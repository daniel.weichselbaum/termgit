Instance: elga-seitenlokalisation 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-seitenlokalisation" 
* name = "elga-seitenlokalisation" 
* title = "ELGA_Seitenlokalisation" 
* status = #active 
* version = "4.0" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.176" 
* date = "2017-02-15" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-sciphox-seitenlokalisation"
* compose.include[0].concept[0].code = #B
* compose.include[0].concept[0].display = "beidseits"
* compose.include[0].concept[1].code = #L
* compose.include[0].concept[1].display = "links"
* compose.include[0].concept[2].code = #M
* compose.include[0].concept[2].display = "Mittellinienzone"
* compose.include[0].concept[3].code = #R
* compose.include[0].concept[3].display = "rechts"

* expansion.timestamp = "2022-09-13T14:16:52.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem-sciphox-seitenlokalisation"
* expansion.contains[0].code = #B
* expansion.contains[0].display = "beidseits"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem-sciphox-seitenlokalisation"
* expansion.contains[1].code = #L
* expansion.contains[1].display = "links"
* expansion.contains[2].system = "https://termgit.elga.gv.at/CodeSystem-sciphox-seitenlokalisation"
* expansion.contains[2].code = #M
* expansion.contains[2].display = "Mittellinienzone"
* expansion.contains[3].system = "https://termgit.elga.gv.at/CodeSystem-sciphox-seitenlokalisation"
* expansion.contains[3].code = #R
* expansion.contains[3].display = "rechts"
