Instance: ems-aviditaet 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-aviditaet" 
* name = "ems-aviditaet" 
* title = "EMS_Aviditaet" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Liste Avidität" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.69" 
* date = "2017-01-26" 
* count = 3 
* #HIGH "hochavid"
* #LOW "niederavid"
* #NOTEST "nicht getestet"
