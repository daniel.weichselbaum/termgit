Instance: elga-typeofproblem 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-typeofproblem" 
* name = "elga-typeofproblem" 
* title = "ELGA_TypeOfProblem" 
* status = #active 
* version = "202108" 
* description = "**Description:** Contains information protected by copyright of SNOMED International. Any use of SNOMED CT in Austria requires a valid affiliate license or sublicense. The corresponding license is free of charge, provided that the use only takes place in Austria and fulfills the conditions of the Affiliate License Agreement. Affiliate licenses can be requested directly from the respective NRC via the [Member Licensing and Distribution Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS).

**Beschreibung:** Enthält durch SNOMED International urheberrechtlich geschützte Information. Jede Verwendung von SNOMED CT in Österreich erfordert eine aufrechte Affiliate Lizenz oder eine Sublizenz. Die entsprechende Lizenz ist kostenlos, vorausgesetzt die Verwendung findet nur in Österreich statt und erfüllt die Bedingungen des Affiliate License Agreements. Affiliate Lizenzen können über das [Mitglieder-Lizenzierungs- und Distributions-Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS) direkt beim jeweiligen NRC beantragt werden." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.205" 
* date = "2021-08-10" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* compose.include[0].concept[0].code = "55607006"
* compose.include[0].concept[0].display = "Problem"
* compose.include[0].concept[1].code = "160245001"
* compose.include[0].concept[1].display = "no current problems or disability"
