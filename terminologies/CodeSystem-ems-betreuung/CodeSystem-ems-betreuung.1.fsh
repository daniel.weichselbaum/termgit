Instance: ems-betreuung 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-betreuung" 
* name = "ems-betreuung" 
* title = "EMS_Betreuung" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Betreuung" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.196" 
* date = "2020-10-27" 
* count = 4 
* concept[0].code = #Kindergarten
* concept[0].display = "Kindergarten"
* concept[1].code = #Pflegeheim
* concept[1].display = "Pflegeheim"
* concept[2].code = #Schule
* concept[2].display = "Schule"
* concept[3].code = #Seniorenheim
* concept[3].display = "Seniorenheim"
