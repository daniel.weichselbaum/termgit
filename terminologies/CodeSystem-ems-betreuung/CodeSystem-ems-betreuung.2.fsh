Instance: ems-betreuung 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-betreuung" 
* name = "ems-betreuung" 
* title = "EMS_Betreuung" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Betreuung" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.196" 
* date = "2020-10-27" 
* count = 4 
* #Kindergarten "Kindergarten"
* #Pflegeheim "Pflegeheim"
* #Schule "Schule"
* #Seniorenheim "Seniorenheim"
