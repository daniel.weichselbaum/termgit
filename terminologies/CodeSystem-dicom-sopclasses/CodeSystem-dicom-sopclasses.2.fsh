Instance: dicom-sopclasses 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-dicom-sopclasses" 
* name = "dicom-sopclasses" 
* title = "DICOM_SOPClasses" 
* status = #active 
* content = #complete 
* version = "3.0" 
* description = "**Description:** DICOM Standard SOP Classes

**Beschreibung:** DICOM Standard SOP Classes, siehe DICOM Part 4: Service Class Specification" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.840.10008.2.6.1" 
* date = "2015-03-31" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://medical.nema.org/standard.html" 
* count = 113 
* #1.2.840.10008.5.1.4.1.1.1 "Computed Radiography Image Storage"
* #1.2.840.10008.5.1.4.1.1.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.1 ^designation[0].value = "Computed Radiography Image Storage" 
* #1.2.840.10008.5.1.4.1.1.1.1 "Digital X-Ray Image Storage - For Presentation"
* #1.2.840.10008.5.1.4.1.1.1.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.1.1 ^designation[0].value = "Digital X-Ray Image Storage - For Presentation" 
* #1.2.840.10008.5.1.4.1.1.1.1.1 "Digital X-Ray Image Storage - For Processing"
* #1.2.840.10008.5.1.4.1.1.1.1.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.1.1.1 ^designation[0].value = "Digital X-Ray Image Storage - For Processing" 
* #1.2.840.10008.5.1.4.1.1.1.2 "Digital Mammography X-Ray Image Storage- For Presentation"
* #1.2.840.10008.5.1.4.1.1.1.2 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.1.2 ^designation[0].value = "Digital Mammography X-Ray Image Storage- For Presentation" 
* #1.2.840.10008.5.1.4.1.1.1.2.1 "Digital Mammography X-Ray Image Storage- For Processing"
* #1.2.840.10008.5.1.4.1.1.1.2.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.1.2.1 ^designation[0].value = "Digital Mammography X-Ray Image Storage- For Processing" 
* #1.2.840.10008.5.1.4.1.1.1.3 "Digital Intra-Oral X-Ray Image Storage - For Presentation"
* #1.2.840.10008.5.1.4.1.1.1.3 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.1.3 ^designation[0].value = "Digital Intra-Oral X-Ray Image Storage - For Presentation" 
* #1.2.840.10008.5.1.4.1.1.1.3.1 "Digital Intra-Oral X-Ray Image Storage - For Processing"
* #1.2.840.10008.5.1.4.1.1.1.3.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.1.3.1 ^designation[0].value = "Digital Intra-Oral X-Ray Image Storage - For Processing" 
* #1.2.840.10008.5.1.4.1.1.104.1 "Encapsulated PDF Storage"
* #1.2.840.10008.5.1.4.1.1.104.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.104.1 ^designation[0].value = "Encapsulated PDF Storage" 
* #1.2.840.10008.5.1.4.1.1.104.2 "Encapsulated CDA Storage"
* #1.2.840.10008.5.1.4.1.1.104.2 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.104.2 ^designation[0].value = "Encapsulated CDA Storage" 
* #1.2.840.10008.5.1.4.1.1.11.1 "Grayscale Softcopy Presentation State Storage"
* #1.2.840.10008.5.1.4.1.1.11.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.11.1 ^designation[0].value = "Grayscale Softcopy Presentation State Storage" 
* #1.2.840.10008.5.1.4.1.1.11.2 "Color Softcopy Presentation State Storage"
* #1.2.840.10008.5.1.4.1.1.11.2 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.11.2 ^designation[0].value = "Color Softcopy Presentation State Storage" 
* #1.2.840.10008.5.1.4.1.1.11.3 "Pseudo-Color Softcopy Presentation State Storage"
* #1.2.840.10008.5.1.4.1.1.11.3 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.11.3 ^designation[0].value = "Pseudo-Color Softcopy Presentation State Storage" 
* #1.2.840.10008.5.1.4.1.1.11.4 "Blending Softcopy Presentation State Storage"
* #1.2.840.10008.5.1.4.1.1.11.4 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.11.4 ^designation[0].value = "Blending Softcopy Presentation State Storage" 
* #1.2.840.10008.5.1.4.1.1.11.5 "XA/XRF Grayscale Softcopy PresentationState Storage"
* #1.2.840.10008.5.1.4.1.1.11.5 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.11.5 ^designation[0].value = "XA/XRF Grayscale Softcopy PresentationState Storage" 
* #1.2.840.10008.5.1.4.1.1.12.1 "X-Ray Angiographic Image Storage"
* #1.2.840.10008.5.1.4.1.1.12.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.12.1 ^designation[0].value = "X-Ray Angiographic Image Storage" 
* #1.2.840.10008.5.1.4.1.1.12.1.1 "Enhanced XA Image Storage"
* #1.2.840.10008.5.1.4.1.1.12.1.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.12.1.1 ^designation[0].value = "Enhanced XA Image Storage" 
* #1.2.840.10008.5.1.4.1.1.12.2 "X-Ray Radiofluoroscopic Image Storage"
* #1.2.840.10008.5.1.4.1.1.12.2 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.12.2 ^designation[0].value = "X-Ray Radiofluoroscopic Image Storage" 
* #1.2.840.10008.5.1.4.1.1.12.2.1 "Enhanced XRF Image Storage"
* #1.2.840.10008.5.1.4.1.1.12.2.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.12.2.1 ^designation[0].value = "Enhanced XRF Image Storage" 
* #1.2.840.10008.5.1.4.1.1.128 "Positron Emission Tomography Image Storage"
* #1.2.840.10008.5.1.4.1.1.128 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.128 ^designation[0].value = "Positron Emission Tomography Image Storage" 
* #1.2.840.10008.5.1.4.1.1.128.1 "Legacy Converted Enhanced PET Image Storage"
* #1.2.840.10008.5.1.4.1.1.128.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.128.1 ^designation[0].value = "Legacy Converted Enhanced PET Image Storage" 
* #1.2.840.10008.5.1.4.1.1.13.1.1 "X-Ray 3D Angiographic Image Storage"
* #1.2.840.10008.5.1.4.1.1.13.1.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.13.1.1 ^designation[0].value = "X-Ray 3D Angiographic Image Storage" 
* #1.2.840.10008.5.1.4.1.1.13.1.2 "X-Ray 3D Craniofacial Image Storage"
* #1.2.840.10008.5.1.4.1.1.13.1.2 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.13.1.2 ^designation[0].value = "X-Ray 3D Craniofacial Image Storage" 
* #1.2.840.10008.5.1.4.1.1.13.1.3 "Breast Tomosynthesis Image Storage"
* #1.2.840.10008.5.1.4.1.1.13.1.3 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.13.1.3 ^designation[0].value = "Breast Tomosynthesis Image Storage" 
* #1.2.840.10008.5.1.4.1.1.13.1.4 "Breast Projection X-Ray Image Storage - For Presentation"
* #1.2.840.10008.5.1.4.1.1.13.1.4 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.13.1.4 ^designation[0].value = "Breast Projection X-Ray Image Storage - For Presentation" 
* #1.2.840.10008.5.1.4.1.1.13.1.5 "Breast Projection X-Ray Image Storage - For Processing"
* #1.2.840.10008.5.1.4.1.1.13.1.5 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.13.1.5 ^designation[0].value = "Breast Projection X-Ray Image Storage - For Processing" 
* #1.2.840.10008.5.1.4.1.1.130 "Enhanced PET Image Storage"
* #1.2.840.10008.5.1.4.1.1.130 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.130 ^designation[0].value = "Enhanced PET Image Storage" 
* #1.2.840.10008.5.1.4.1.1.131 "Basic Structured Display Storage"
* #1.2.840.10008.5.1.4.1.1.131 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.131 ^designation[0].value = "Basic Structured Display Storage" 
* #1.2.840.10008.5.1.4.1.1.14.1 "Intravascular Optical Coherence Tomography Image Storage - For Presentation"
* #1.2.840.10008.5.1.4.1.1.14.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.14.1 ^designation[0].value = "Intravascular Optical Coherence Tomography Image Storage - For Presentation" 
* #1.2.840.10008.5.1.4.1.1.14.2 "Intravascular Optical Coherence Tomography Image Storage - For Processing"
* #1.2.840.10008.5.1.4.1.1.14.2 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.14.2 ^designation[0].value = "Intravascular Optical Coherence Tomography Image Storage - For Processing" 
* #1.2.840.10008.5.1.4.1.1.2 "CT Image Storage"
* #1.2.840.10008.5.1.4.1.1.2 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.2 ^designation[0].value = "CT Image Storage" 
* #1.2.840.10008.5.1.4.1.1.2.1 "Enhanced CT Image Storage"
* #1.2.840.10008.5.1.4.1.1.2.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.2.1 ^designation[0].value = "Enhanced CT Image Storage" 
* #1.2.840.10008.5.1.4.1.1.2.2 "Legacy Converted Enhanced CT Image Storage"
* #1.2.840.10008.5.1.4.1.1.2.2 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.2.2 ^designation[0].value = "Legacy Converted Enhanced CT Image Storage" 
* #1.2.840.10008.5.1.4.1.1.20 "Nuclear Medicine Image Storage"
* #1.2.840.10008.5.1.4.1.1.20 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.20 ^designation[0].value = "Nuclear Medicine Image Storage" 
* #1.2.840.10008.5.1.4.1.1.3.1 "Ultrasound Multi-frame Image Storage"
* #1.2.840.10008.5.1.4.1.1.3.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.3.1 ^designation[0].value = "Ultrasound Multi-frame Image Storage" 
* #1.2.840.10008.5.1.4.1.1.4 "MR Image Storage"
* #1.2.840.10008.5.1.4.1.1.4 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.4 ^designation[0].value = "MR Image Storage" 
* #1.2.840.10008.5.1.4.1.1.4.1 "Enhanced MR Image Storage"
* #1.2.840.10008.5.1.4.1.1.4.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.4.1 ^designation[0].value = "Enhanced MR Image Storage" 
* #1.2.840.10008.5.1.4.1.1.4.2 "MR Spectroscopy Storage"
* #1.2.840.10008.5.1.4.1.1.4.2 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.4.2 ^designation[0].value = "MR Spectroscopy Storage" 
* #1.2.840.10008.5.1.4.1.1.4.3 "Enhanced MR Color Image Storage"
* #1.2.840.10008.5.1.4.1.1.4.3 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.4.3 ^designation[0].value = "Enhanced MR Color Image Storage" 
* #1.2.840.10008.5.1.4.1.1.4.4 "Legacy Converted Enhanced MR ImageStorage"
* #1.2.840.10008.5.1.4.1.1.4.4 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.4.4 ^designation[0].value = "Legacy Converted Enhanced MR ImageStorage" 
* #1.2.840.10008.5.1.4.1.1.481.1 "RT Image Storage"
* #1.2.840.10008.5.1.4.1.1.481.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.481.1 ^designation[0].value = "RT Image Storage" 
* #1.2.840.10008.5.1.4.1.1.481.2 "RT Dose Storage"
* #1.2.840.10008.5.1.4.1.1.481.2 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.481.2 ^designation[0].value = "RT Dose Storage" 
* #1.2.840.10008.5.1.4.1.1.481.3 "RT Structure Set Storage"
* #1.2.840.10008.5.1.4.1.1.481.3 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.481.3 ^designation[0].value = "RT Structure Set Storage" 
* #1.2.840.10008.5.1.4.1.1.481.4 "RT Beams Treatment Record Storage"
* #1.2.840.10008.5.1.4.1.1.481.4 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.481.4 ^designation[0].value = "RT Beams Treatment Record Storage" 
* #1.2.840.10008.5.1.4.1.1.481.5 "RT Plan Storage"
* #1.2.840.10008.5.1.4.1.1.481.5 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.481.5 ^designation[0].value = "RT Plan Storage" 
* #1.2.840.10008.5.1.4.1.1.481.6 "RT Brachy Treatment Record Storage"
* #1.2.840.10008.5.1.4.1.1.481.6 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.481.6 ^designation[0].value = "RT Brachy Treatment Record Storage" 
* #1.2.840.10008.5.1.4.1.1.481.7 "RT Treatment Summary Record Storage"
* #1.2.840.10008.5.1.4.1.1.481.7 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.481.7 ^designation[0].value = "RT Treatment Summary Record Storage" 
* #1.2.840.10008.5.1.4.1.1.481.8 "RT Ion Plan Storage"
* #1.2.840.10008.5.1.4.1.1.481.8 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.481.8 ^designation[0].value = "RT Ion Plan Storage" 
* #1.2.840.10008.5.1.4.1.1.481.9 "RT Ion Beams Treatment Record Storage"
* #1.2.840.10008.5.1.4.1.1.481.9 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.481.9 ^designation[0].value = "RT Ion Beams Treatment Record Storage" 
* #1.2.840.10008.5.1.4.1.1.6.1 "Ultrasound Image Storage"
* #1.2.840.10008.5.1.4.1.1.6.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.6.1 ^designation[0].value = "Ultrasound Image Storage" 
* #1.2.840.10008.5.1.4.1.1.6.2 "Enhanced US Volume Storage"
* #1.2.840.10008.5.1.4.1.1.6.2 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.6.2 ^designation[0].value = "Enhanced US Volume Storage" 
* #1.2.840.10008.5.1.4.1.1.66 "Raw Data Storage"
* #1.2.840.10008.5.1.4.1.1.66 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.66 ^designation[0].value = "Raw Data Storage" 
* #1.2.840.10008.5.1.4.1.1.66.1 "Spatial Registration Storage"
* #1.2.840.10008.5.1.4.1.1.66.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.66.1 ^designation[0].value = "Spatial Registration Storage" 
* #1.2.840.10008.5.1.4.1.1.66.2 "Spatial Fiducials Storage"
* #1.2.840.10008.5.1.4.1.1.66.2 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.66.2 ^designation[0].value = "Spatial Fiducials Storage" 
* #1.2.840.10008.5.1.4.1.1.66.3 "Deformable Spatial Registration Storage"
* #1.2.840.10008.5.1.4.1.1.66.3 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.66.3 ^designation[0].value = "Deformable Spatial Registration Storage" 
* #1.2.840.10008.5.1.4.1.1.66.4 "Segmentation Storage"
* #1.2.840.10008.5.1.4.1.1.66.4 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.66.4 ^designation[0].value = "Segmentation Storage" 
* #1.2.840.10008.5.1.4.1.1.66.5 "Surface Segmentation Storage"
* #1.2.840.10008.5.1.4.1.1.66.5 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.66.5 ^designation[0].value = "Surface Segmentation Storage" 
* #1.2.840.10008.5.1.4.1.1.67 "Real World Value Mapping Storage"
* #1.2.840.10008.5.1.4.1.1.67 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.67 ^designation[0].value = "Real World Value Mapping Storage" 
* #1.2.840.10008.5.1.4.1.1.68.1 "Surface Scan Mesh Storage"
* #1.2.840.10008.5.1.4.1.1.68.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.68.1 ^designation[0].value = "Surface Scan Mesh Storage" 
* #1.2.840.10008.5.1.4.1.1.68.2 "Surface Scan Point Cloud Storage"
* #1.2.840.10008.5.1.4.1.1.68.2 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.68.2 ^designation[0].value = "Surface Scan Point Cloud Storage" 
* #1.2.840.10008.5.1.4.1.1.7 "Secondary Capture Image Storage"
* #1.2.840.10008.5.1.4.1.1.7 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.7 ^designation[0].value = "Secondary Capture Image Storage" 
* #1.2.840.10008.5.1.4.1.1.7.1 "Multi-frame Single Bit Secondary Capture Image Storage"
* #1.2.840.10008.5.1.4.1.1.7.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.7.1 ^designation[0].value = "Multi-frame Single Bit Secondary Capture Image Storage" 
* #1.2.840.10008.5.1.4.1.1.7.2 "Multi-frame Grayscale Byte Secondary Capture Image Storage"
* #1.2.840.10008.5.1.4.1.1.7.2 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.7.2 ^designation[0].value = "Multi-frame Grayscale Byte Secondary Capture Image Storage" 
* #1.2.840.10008.5.1.4.1.1.7.3 "Multi-frame Grayscale Word SecondaryCapture Image Storage"
* #1.2.840.10008.5.1.4.1.1.7.3 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.7.3 ^designation[0].value = "Multi-frame Grayscale Word SecondaryCapture Image Storage" 
* #1.2.840.10008.5.1.4.1.1.7.4 "Multi-frame True Color Secondary Capture Image Storage"
* #1.2.840.10008.5.1.4.1.1.7.4 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.7.4 ^designation[0].value = "Multi-frame True Color Secondary Capture Image Storage" 
* #1.2.840.10008.5.1.4.1.1.77.1.1 "VL Endoscopic Image Storage"
* #1.2.840.10008.5.1.4.1.1.77.1.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.77.1.1 ^designation[0].value = "VL Endoscopic Image Storage" 
* #1.2.840.10008.5.1.4.1.1.77.1.1.1 "Video Endoscopic Image Storage"
* #1.2.840.10008.5.1.4.1.1.77.1.1.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.77.1.1.1 ^designation[0].value = "Video Endoscopic Image Storage" 
* #1.2.840.10008.5.1.4.1.1.77.1.2 "VL Microscopic Image Storage"
* #1.2.840.10008.5.1.4.1.1.77.1.2 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.77.1.2 ^designation[0].value = "VL Microscopic Image Storage" 
* #1.2.840.10008.5.1.4.1.1.77.1.2.1 "Video Microscopic Image Storage"
* #1.2.840.10008.5.1.4.1.1.77.1.2.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.77.1.2.1 ^designation[0].value = "Video Microscopic Image Storage" 
* #1.2.840.10008.5.1.4.1.1.77.1.3 "VL Slide-Coordinates Microscopic Image Storage"
* #1.2.840.10008.5.1.4.1.1.77.1.3 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.77.1.3 ^designation[0].value = "VL Slide-Coordinates Microscopic Image Storage" 
* #1.2.840.10008.5.1.4.1.1.77.1.4 "VL Photographic Image Storage"
* #1.2.840.10008.5.1.4.1.1.77.1.4 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.77.1.4 ^designation[0].value = "VL Photographic Image Storage" 
* #1.2.840.10008.5.1.4.1.1.77.1.4.1 "Video Photographic Image Storage"
* #1.2.840.10008.5.1.4.1.1.77.1.4.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.77.1.4.1 ^designation[0].value = "Video Photographic Image Storage" 
* #1.2.840.10008.5.1.4.1.1.77.1.5.1 "Ophthalmic Photography 8 Bit Image Storage"
* #1.2.840.10008.5.1.4.1.1.77.1.5.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.77.1.5.1 ^designation[0].value = "Ophthalmic Photography 8 Bit Image Storage" 
* #1.2.840.10008.5.1.4.1.1.77.1.5.2 "Ophthalmic Photography 16 Bit Image Storage"
* #1.2.840.10008.5.1.4.1.1.77.1.5.2 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.77.1.5.2 ^designation[0].value = "Ophthalmic Photography 16 Bit Image Storage" 
* #1.2.840.10008.5.1.4.1.1.77.1.5.3 "Stereometric Relationship Storage"
* #1.2.840.10008.5.1.4.1.1.77.1.5.3 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.77.1.5.3 ^designation[0].value = "Stereometric Relationship Storage" 
* #1.2.840.10008.5.1.4.1.1.77.1.5.4 "Ophthalmic Tomography Image Storage"
* #1.2.840.10008.5.1.4.1.1.77.1.5.4 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.77.1.5.4 ^designation[0].value = "Ophthalmic Tomography Image Storage" 
* #1.2.840.10008.5.1.4.1.1.77.1.6 "VL Whole Slide Microscopy Image Storage"
* #1.2.840.10008.5.1.4.1.1.77.1.6 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.77.1.6 ^designation[0].value = "VL Whole Slide Microscopy Image Storage" 
* #1.2.840.10008.5.1.4.1.1.78.1 "Lensometry Measurements Storage"
* #1.2.840.10008.5.1.4.1.1.78.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.78.1 ^designation[0].value = "Lensometry Measurements Storage" 
* #1.2.840.10008.5.1.4.1.1.78.2 "Autorefraction Measurements Storage"
* #1.2.840.10008.5.1.4.1.1.78.2 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.78.2 ^designation[0].value = "Autorefraction Measurements Storage" 
* #1.2.840.10008.5.1.4.1.1.78.3 "Keratometry Measurements Storage"
* #1.2.840.10008.5.1.4.1.1.78.3 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.78.3 ^designation[0].value = "Keratometry Measurements Storage" 
* #1.2.840.10008.5.1.4.1.1.78.4 "Subjective Refraction Measurements Storage"
* #1.2.840.10008.5.1.4.1.1.78.4 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.78.4 ^designation[0].value = "Subjective Refraction Measurements Storage" 
* #1.2.840.10008.5.1.4.1.1.78.5 "Visual Acuity Measurements Storage"
* #1.2.840.10008.5.1.4.1.1.78.5 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.78.5 ^designation[0].value = "Visual Acuity Measurements Storage" 
* #1.2.840.10008.5.1.4.1.1.78.6 "Spectacle Prescription Report Storage"
* #1.2.840.10008.5.1.4.1.1.78.6 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.78.6 ^designation[0].value = "Spectacle Prescription Report Storage" 
* #1.2.840.10008.5.1.4.1.1.78.7 "Ophthalmic Axial Measurements Storage"
* #1.2.840.10008.5.1.4.1.1.78.7 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.78.7 ^designation[0].value = "Ophthalmic Axial Measurements Storage" 
* #1.2.840.10008.5.1.4.1.1.78.8 "Intraocular Lens Calculations Storage"
* #1.2.840.10008.5.1.4.1.1.78.8 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.78.8 ^designation[0].value = "Intraocular Lens Calculations Storage" 
* #1.2.840.10008.5.1.4.1.1.79.1 "Macular Grid Thickness and Volume Report"
* #1.2.840.10008.5.1.4.1.1.79.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.79.1 ^designation[0].value = "Macular Grid Thickness and Volume Report" 
* #1.2.840.10008.5.1.4.1.1.80.1 "Ophthalmic Visual Field Static Perimetry Measurements Storage"
* #1.2.840.10008.5.1.4.1.1.80.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.80.1 ^designation[0].value = "Ophthalmic Visual Field Static Perimetry Measurements Storage" 
* #1.2.840.10008.5.1.4.1.1.81.1 "Ophthalmic Thickness Map Storage"
* #1.2.840.10008.5.1.4.1.1.81.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.81.1 ^designation[0].value = "Ophthalmic Thickness Map Storage" 
* #1.2.840.10008.5.1.4.1.1.82.1 "Corneal Topography Map Storage"
* #1.2.840.10008.5.1.4.1.1.82.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.82.1 ^designation[0].value = "Corneal Topography Map Storage" 
* #1.2.840.10008.5.1.4.1.1.88.11 "Basic Text SR"
* #1.2.840.10008.5.1.4.1.1.88.11 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.88.11 ^designation[0].value = "Basic Text SR" 
* #1.2.840.10008.5.1.4.1.1.88.22 "Enhanced SR"
* #1.2.840.10008.5.1.4.1.1.88.22 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.88.22 ^designation[0].value = "Enhanced SR" 
* #1.2.840.10008.5.1.4.1.1.88.33 "Comprehensive SR"
* #1.2.840.10008.5.1.4.1.1.88.33 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.88.33 ^designation[0].value = "Comprehensive SR" 
* #1.2.840.10008.5.1.4.1.1.88.34 "Comprehensive 3D SR"
* #1.2.840.10008.5.1.4.1.1.88.34 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.88.34 ^designation[0].value = "Comprehensive 3D SR" 
* #1.2.840.10008.5.1.4.1.1.88.40 "Procedure Log"
* #1.2.840.10008.5.1.4.1.1.88.40 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.88.40 ^designation[0].value = "Procedure Log" 
* #1.2.840.10008.5.1.4.1.1.88.50 "Mammography CAD SR"
* #1.2.840.10008.5.1.4.1.1.88.50 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.88.50 ^designation[0].value = "Mammography CAD SR" 
* #1.2.840.10008.5.1.4.1.1.88.59 "Key Object Selection"
* #1.2.840.10008.5.1.4.1.1.88.59 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.88.59 ^designation[0].value = "Key Object Selection" 
* #1.2.840.10008.5.1.4.1.1.88.65 "Chest CAD SR"
* #1.2.840.10008.5.1.4.1.1.88.65 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.88.65 ^designation[0].value = "Chest CAD SR" 
* #1.2.840.10008.5.1.4.1.1.88.67 "X-Ray Radiation Dose SR"
* #1.2.840.10008.5.1.4.1.1.88.67 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.88.67 ^designation[0].value = "X-Ray Radiation Dose SR" 
* #1.2.840.10008.5.1.4.1.1.88.68 "Radiopharmaceutical Radiation Dose SR"
* #1.2.840.10008.5.1.4.1.1.88.68 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.88.68 ^designation[0].value = "Radiopharmaceutical Radiation Dose SR" 
* #1.2.840.10008.5.1.4.1.1.88.69 "Colon CAD SR"
* #1.2.840.10008.5.1.4.1.1.88.69 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.88.69 ^designation[0].value = "Colon CAD SR" 
* #1.2.840.10008.5.1.4.1.1.88.70 "Implantation Plan SR Document Storage"
* #1.2.840.10008.5.1.4.1.1.88.70 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.88.70 ^designation[0].value = "Implantation Plan SR Document Storage" 
* #1.2.840.10008.5.1.4.1.1.9.1.1 "12-lead ECG Waveform Storage"
* #1.2.840.10008.5.1.4.1.1.9.1.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.9.1.1 ^designation[0].value = "12-lead ECG Waveform Storage" 
* #1.2.840.10008.5.1.4.1.1.9.1.2 "General ECG Waveform Storage"
* #1.2.840.10008.5.1.4.1.1.9.1.2 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.9.1.2 ^designation[0].value = "General ECG Waveform Storage" 
* #1.2.840.10008.5.1.4.1.1.9.1.3 "Ambulatory ECG Waveform Storage"
* #1.2.840.10008.5.1.4.1.1.9.1.3 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.9.1.3 ^designation[0].value = "Ambulatory ECG Waveform Storage" 
* #1.2.840.10008.5.1.4.1.1.9.2.1 "Hemodynamic Waveform Storage"
* #1.2.840.10008.5.1.4.1.1.9.2.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.9.2.1 ^designation[0].value = "Hemodynamic Waveform Storage" 
* #1.2.840.10008.5.1.4.1.1.9.3.1 "Cardiac Electrophysiology Waveform Storage"
* #1.2.840.10008.5.1.4.1.1.9.3.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.9.3.1 ^designation[0].value = "Cardiac Electrophysiology Waveform Storage" 
* #1.2.840.10008.5.1.4.1.1.9.4.1 "Basic Voice Audio Waveform Storage"
* #1.2.840.10008.5.1.4.1.1.9.4.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.9.4.1 ^designation[0].value = "Basic Voice Audio Waveform Storage" 
* #1.2.840.10008.5.1.4.1.1.9.4.2 "General Audio Waveform Storage"
* #1.2.840.10008.5.1.4.1.1.9.4.2 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.9.4.2 ^designation[0].value = "General Audio Waveform Storage" 
* #1.2.840.10008.5.1.4.1.1.9.5.1 "Arterial Pulse Waveform Storage"
* #1.2.840.10008.5.1.4.1.1.9.5.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.9.5.1 ^designation[0].value = "Arterial Pulse Waveform Storage" 
* #1.2.840.10008.5.1.4.1.1.9.6.1 "Respiratory Waveform Storage"
* #1.2.840.10008.5.1.4.1.1.9.6.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.1.1.9.6.1 ^designation[0].value = "Respiratory Waveform Storage" 
* #1.2.840.10008.5.1.4.34.7 "RT Beams Delivery Instruction Storage"
* #1.2.840.10008.5.1.4.34.7 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.34.7 ^designation[0].value = "RT Beams Delivery Instruction Storage" 
* #1.2.840.10008.5.1.4.43.1 "Generic Implant Template Storage"
* #1.2.840.10008.5.1.4.43.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.43.1 ^designation[0].value = "Generic Implant Template Storage" 
* #1.2.840.10008.5.1.4.44.1 "Implant Assembly Template Storage"
* #1.2.840.10008.5.1.4.44.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.44.1 ^designation[0].value = "Implant Assembly Template Storage" 
* #1.2.840.10008.5.1.4.45.1 "Implant Template Group Storage"
* #1.2.840.10008.5.1.4.45.1 ^designation[0].language = #de-AT 
* #1.2.840.10008.5.1.4.45.1 ^designation[0].value = "Implant Template Group Storage" 
