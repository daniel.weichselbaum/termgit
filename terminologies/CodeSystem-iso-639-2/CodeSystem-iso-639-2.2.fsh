Instance: iso-639-2 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-iso-639-2" 
* name = "iso-639-2" 
* title = "ISO 639-2" 
* status = #active 
* content = #complete 
* description = "**Description:** ISO 639-2 is the alpha-3 code in Codes for the representation of names of languages-- Part 2. There are 21 languages that have alternative codes for bibliographic or terminology purposes. In those cases, each is listed separately and they are designated as 'DEPRECATED' (bibliographic) or 'PREFERRED' (terminology). In all other cases there is only one ISO 639-2 code. Multiple codes assigned to the same language are to be considered synonyms. ISO 639-1 is the alpha-2 code" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.0.639.2" 
* date = "2016-05-18" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.loc.gov/standards/iso639-2/php/code_list.php" 
* count = 506 
* property[0].code = #hints 
* property[0].type = #string 
* #aar "Afar"
* #aar ^designation[0].language = #de-AT 
* #aar ^designation[0].value = "Danakil-Sprache" 
* #aar ^property[0].code = #hints 
* #aar ^property[0].valueString = "aa" 
* #abk "Abkhazian"
* #abk ^designation[0].language = #de-AT 
* #abk ^designation[0].value = "Abchasisch" 
* #abk ^property[0].code = #hints 
* #abk ^property[0].valueString = "ab" 
* #ace "Achinese"
* #ace ^designation[0].language = #de-AT 
* #ace ^designation[0].value = "Aceh-Sprache" 
* #ach "Acoli"
* #ach ^designation[0].language = #de-AT 
* #ach ^designation[0].value = "Acholi-Sprache" 
* #ada "Adangme"
* #ada ^designation[0].language = #de-AT 
* #ada ^designation[0].value = "Adangme-Sprache" 
* #ady "Adyghe; Adygei"
* #ady ^designation[0].language = #de-AT 
* #ady ^designation[0].value = "Adygisch" 
* #afa "Afro-Asiatic languages"
* #afa ^designation[0].language = #de-AT 
* #afa ^designation[0].value = "Hamitosemitische Sprachen (Andere)" 
* #afh "Afrihili"
* #afh ^designation[0].language = #de-AT 
* #afh ^designation[0].value = "Afrihili" 
* #afr "Afrikaans"
* #afr ^designation[0].language = #de-AT 
* #afr ^designation[0].value = "Afrikaans" 
* #afr ^property[0].code = #hints 
* #afr ^property[0].valueString = "af" 
* #ain "Ainu"
* #ain ^designation[0].language = #de-AT 
* #ain ^designation[0].value = "Ainu-Sprache" 
* #aka "Akan"
* #aka ^designation[0].language = #de-AT 
* #aka ^designation[0].value = "Akan-Sprache" 
* #aka ^property[0].code = #hints 
* #aka ^property[0].valueString = "ak" 
* #akk "Akkadian"
* #akk ^designation[0].language = #de-AT 
* #akk ^designation[0].value = "Akkadisch" 
* #alb "Albanian"
* #alb ^designation[0].language = #de-AT 
* #alb ^designation[0].value = "Albanisch" 
* #alb ^property[0].code = #hints 
* #alb ^property[0].valueString = "sq" 
* #ale "Aleut"
* #ale ^designation[0].language = #de-AT 
* #ale ^designation[0].value = "Aleutisch" 
* #alg "Algonquian languages"
* #alg ^designation[0].language = #de-AT 
* #alg ^designation[0].value = "Algonkin-Sprachen (Andere)" 
* #alt "Southern Altai"
* #alt ^designation[0].language = #de-AT 
* #alt ^designation[0].value = "Altaisch" 
* #amh "Amharic"
* #amh ^designation[0].language = #de-AT 
* #amh ^designation[0].value = "Amharisch" 
* #amh ^property[0].code = #hints 
* #amh ^property[0].valueString = "am" 
* #ang "English, Old (ca.450-1100)"
* #ang ^designation[0].language = #de-AT 
* #ang ^designation[0].value = "Altenglisch" 
* #anp "Angika"
* #anp ^designation[0].language = #de-AT 
* #anp ^designation[0].value = "Anga-Sprache" 
* #apa "Apache languages"
* #apa ^designation[0].language = #de-AT 
* #apa ^designation[0].value = "Apachen-Sprachen" 
* #ara "Arabic"
* #ara ^designation[0].language = #de-AT 
* #ara ^designation[0].value = "Arabisch" 
* #ara ^property[0].code = #hints 
* #ara ^property[0].valueString = "ar" 
* #arc "Official Aramaic (700-300 BCE); Imperial Aramaic (700-300 BCE)"
* #arc ^designation[0].language = #de-AT 
* #arc ^designation[0].value = "Aramäisch" 
* #arg "Aragonese"
* #arg ^designation[0].language = #de-AT 
* #arg ^designation[0].value = "Aragonesisch" 
* #arg ^property[0].code = #hints 
* #arg ^property[0].valueString = "an" 
* #arm "Armenian"
* #arm ^designation[0].language = #de-AT 
* #arm ^designation[0].value = "Armenisch" 
* #arm ^property[0].code = #hints 
* #arm ^property[0].valueString = "hy" 
* #arn "Mapudungun; Mapuche"
* #arn ^designation[0].language = #de-AT 
* #arn ^designation[0].value = "Arauka-Sprachen" 
* #arp "Arapaho"
* #arp ^designation[0].language = #de-AT 
* #arp ^designation[0].value = "Arapaho-Sprache" 
* #art "Artificial languages"
* #art ^designation[0].language = #de-AT 
* #art ^designation[0].value = "Kunstsprachen (Andere)" 
* #arw "Arawak"
* #arw ^designation[0].language = #de-AT 
* #arw ^designation[0].value = "Arawak-Sprachen" 
* #asm "Assamese"
* #asm ^designation[0].language = #de-AT 
* #asm ^designation[0].value = "Assamesisch" 
* #asm ^property[0].code = #hints 
* #asm ^property[0].valueString = "as" 
* #ast "Asturian; Bable; Leonese; Asturleonese"
* #ast ^designation[0].language = #de-AT 
* #ast ^designation[0].value = "Asturisch" 
* #ath "Athapascan languages"
* #ath ^designation[0].language = #de-AT 
* #ath ^designation[0].value = "Athapaskische Sprachen (Andere)" 
* #aus "Australian languages"
* #aus ^designation[0].language = #de-AT 
* #aus ^designation[0].value = "Australische Sprachen" 
* #ava "Avaric"
* #ava ^designation[0].language = #de-AT 
* #ava ^designation[0].value = "Awarisch" 
* #ava ^property[0].code = #hints 
* #ava ^property[0].valueString = "av" 
* #ave "Avestan"
* #ave ^designation[0].language = #de-AT 
* #ave ^designation[0].value = "Avestisch" 
* #ave ^property[0].code = #hints 
* #ave ^property[0].valueString = "ae" 
* #awa "Awadhi"
* #awa ^designation[0].language = #de-AT 
* #awa ^designation[0].value = "Awadhi" 
* #aym "Aymara"
* #aym ^designation[0].language = #de-AT 
* #aym ^designation[0].value = "Aymará-Sprache" 
* #aym ^property[0].code = #hints 
* #aym ^property[0].valueString = "ay" 
* #aze "Azerbaijani"
* #aze ^designation[0].language = #de-AT 
* #aze ^designation[0].value = "Aserbeidschanisch" 
* #aze ^property[0].code = #hints 
* #aze ^property[0].valueString = "az" 
* #bad "Banda languages"
* #bad ^designation[0].language = #de-AT 
* #bad ^designation[0].value = "Banda-Sprachen (Ubangi-Sprachen)" 
* #bai "Bamileke languages"
* #bai ^designation[0].language = #de-AT 
* #bai ^designation[0].value = "Bamileke-Sprachen" 
* #bak "Bashkir"
* #bak ^designation[0].language = #de-AT 
* #bak ^designation[0].value = "Baschkirisch" 
* #bak ^property[0].code = #hints 
* #bak ^property[0].valueString = "ba" 
* #bal "Baluchi"
* #bal ^designation[0].language = #de-AT 
* #bal ^designation[0].value = "Belutschisch" 
* #bam "Bambara"
* #bam ^designation[0].language = #de-AT 
* #bam ^designation[0].value = "Bambara-Sprache" 
* #bam ^property[0].code = #hints 
* #bam ^property[0].valueString = "bm" 
* #ban "Balinese"
* #ban ^designation[0].language = #de-AT 
* #ban ^designation[0].value = "Balinesisch" 
* #baq "Basque"
* #baq ^designation[0].language = #de-AT 
* #baq ^designation[0].value = "Baskisch" 
* #baq ^property[0].code = #hints 
* #baq ^property[0].valueString = "eu" 
* #bas "Basa"
* #bas ^designation[0].language = #de-AT 
* #bas ^designation[0].value = "Basaa-Sprache" 
* #bat "Baltic languages"
* #bat ^designation[0].language = #de-AT 
* #bat ^designation[0].value = "Baltische Sprachen (Andere)" 
* #bej "Beja; Bedawiyet"
* #bej ^designation[0].language = #de-AT 
* #bej ^designation[0].value = "Bedauye" 
* #bel "Belarusian"
* #bel ^designation[0].language = #de-AT 
* #bel ^designation[0].value = "Weißrussisch" 
* #bel ^property[0].code = #hints 
* #bel ^property[0].valueString = "be" 
* #bem "Bemba"
* #bem ^designation[0].language = #de-AT 
* #bem ^designation[0].value = "Bemba-Sprache" 
* #ben "Bengali"
* #ben ^designation[0].language = #de-AT 
* #ben ^designation[0].value = "Bengali" 
* #ben ^property[0].code = #hints 
* #ben ^property[0].valueString = "bn" 
* #ber "Berber languages"
* #ber ^designation[0].language = #de-AT 
* #ber ^designation[0].value = "Berbersprachen (Andere)" 
* #bho "Bhojpuri"
* #bho ^designation[0].language = #de-AT 
* #bho ^designation[0].value = "Bhojpuri" 
* #bih "Bihari languages"
* #bih ^designation[0].language = #de-AT 
* #bih ^designation[0].value = "Bihari (Andere)" 
* #bih ^property[0].code = #hints 
* #bih ^property[0].valueString = "bh" 
* #bik "Bikol"
* #bik ^designation[0].language = #de-AT 
* #bik ^designation[0].value = "Bikol-Sprache" 
* #bin "Bini; Edo"
* #bin ^designation[0].language = #de-AT 
* #bin ^designation[0].value = "Edo-Sprache" 
* #bis "Bislama"
* #bis ^designation[0].language = #de-AT 
* #bis ^designation[0].value = "Beach-la-mar" 
* #bis ^property[0].code = #hints 
* #bis ^property[0].valueString = "bi" 
* #bla "Siksika"
* #bla ^designation[0].language = #de-AT 
* #bla ^designation[0].value = "Blackfoot-Sprache" 
* #bnt "Bantu languages"
* #bnt ^designation[0].language = #de-AT 
* #bnt ^designation[0].value = "Bantusprachen (Andere)" 
* #bod "Tibetan"
* #bod ^designation[0].language = #de-AT 
* #bod ^designation[0].value = "Tibetisch" 
* #bod ^property[0].code = #hints 
* #bod ^property[0].valueString = "bo" 
* #bos "Bosnian"
* #bos ^designation[0].language = #de-AT 
* #bos ^designation[0].value = "Bosnisch" 
* #bos ^property[0].code = #hints 
* #bos ^property[0].valueString = "bs" 
* #bra "Braj"
* #bra ^designation[0].language = #de-AT 
* #bra ^designation[0].value = "Braj-Bhakha" 
* #bre "Breton"
* #bre ^designation[0].language = #de-AT 
* #bre ^designation[0].value = "Bretonisch" 
* #bre ^property[0].code = #hints 
* #bre ^property[0].valueString = "br" 
* #btk "Batak languages"
* #btk ^designation[0].language = #de-AT 
* #btk ^designation[0].value = "Batak-Sprache" 
* #bua "Buriat"
* #bua ^designation[0].language = #de-AT 
* #bua ^designation[0].value = "Burjatisch" 
* #bug "Buginese"
* #bug ^designation[0].language = #de-AT 
* #bug ^designation[0].value = "Bugi-Sprache" 
* #bul "Bulgarian"
* #bul ^designation[0].language = #de-AT 
* #bul ^designation[0].value = "Bulgarisch" 
* #bul ^property[0].code = #hints 
* #bul ^property[0].valueString = "bg" 
* #bur "Burmese"
* #bur ^designation[0].language = #de-AT 
* #bur ^designation[0].value = "Birmanisch" 
* #bur ^property[0].code = #hints 
* #bur ^property[0].valueString = "my" 
* #byn "Blin; Bilin"
* #byn ^designation[0].language = #de-AT 
* #byn ^designation[0].value = "Bilin-Sprache" 
* #cad "Caddo"
* #cad ^designation[0].language = #de-AT 
* #cad ^designation[0].value = "Caddo-Sprachen" 
* #cai "Central American Indian languages"
* #cai ^designation[0].language = #de-AT 
* #cai ^designation[0].value = "Indianersprachen, Zentralamerika (Andere)" 
* #car "Galibi Carib"
* #car ^designation[0].language = #de-AT 
* #car ^designation[0].value = "Karibische Sprachen" 
* #cat "Catalan; Valencian"
* #cat ^designation[0].language = #de-AT 
* #cat ^designation[0].value = "Katalanisch" 
* #cat ^property[0].code = #hints 
* #cat ^property[0].valueString = "ca" 
* #cau "Caucasian languages"
* #cau ^designation[0].language = #de-AT 
* #cau ^designation[0].value = "Kaukasische Sprachen (Andere)" 
* #ceb "Cebuano"
* #ceb ^designation[0].language = #de-AT 
* #ceb ^designation[0].value = "Cebuano" 
* #cel "Celtic languages"
* #cel ^designation[0].language = #de-AT 
* #cel ^designation[0].value = "Keltische Sprachen (Andere)" 
* #ces "Czech"
* #ces ^designation[0].language = #de-AT 
* #ces ^designation[0].value = "Tschechisch" 
* #ces ^property[0].code = #hints 
* #ces ^property[0].valueString = "cs" 
* #cha "Chamorro"
* #cha ^designation[0].language = #de-AT 
* #cha ^designation[0].value = "Chamorro-Sprache" 
* #cha ^property[0].code = #hints 
* #cha ^property[0].valueString = "ch" 
* #chb "Chibcha"
* #chb ^designation[0].language = #de-AT 
* #chb ^designation[0].value = "Chibcha-Sprachen" 
* #che "Chechen"
* #che ^designation[0].language = #de-AT 
* #che ^designation[0].value = "Tschetschenisch" 
* #che ^property[0].code = #hints 
* #che ^property[0].valueString = "ce" 
* #chg "Chagatai"
* #chg ^designation[0].language = #de-AT 
* #chg ^designation[0].value = "Tschagataisch" 
* #chi "Chinese"
* #chi ^designation[0].language = #de-AT 
* #chi ^designation[0].value = "Chinesisch" 
* #chi ^property[0].code = #hints 
* #chi ^property[0].valueString = "zh" 
* #chk "Chuukese"
* #chk ^designation[0].language = #de-AT 
* #chk ^designation[0].value = "Trukesisch" 
* #chm "Mari"
* #chm ^designation[0].language = #de-AT 
* #chm ^designation[0].value = "Tscheremissisch" 
* #chn "Chinook jargon"
* #chn ^designation[0].language = #de-AT 
* #chn ^designation[0].value = "Chinook-Jargon" 
* #cho "Choctaw"
* #cho ^designation[0].language = #de-AT 
* #cho ^designation[0].value = "Choctaw-Sprache" 
* #chp "Chipewyan; Dene Suline"
* #chp ^designation[0].language = #de-AT 
* #chp ^designation[0].value = "Chipewyan-Sprache" 
* #chr "Cherokee"
* #chr ^designation[0].language = #de-AT 
* #chr ^designation[0].value = "Cherokee-Sprache" 
* #chu "Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic"
* #chu ^designation[0].language = #de-AT 
* #chu ^designation[0].value = "Kirchenslawisch" 
* #chu ^property[0].code = #hints 
* #chu ^property[0].valueString = "cu" 
* #chv "Chuvash"
* #chv ^designation[0].language = #de-AT 
* #chv ^designation[0].value = "Tschuwaschisch" 
* #chv ^property[0].code = #hints 
* #chv ^property[0].valueString = "cv" 
* #chy "Cheyenne"
* #chy ^designation[0].language = #de-AT 
* #chy ^designation[0].value = "Cheyenne-Sprache" 
* #cmc "Chamic languages"
* #cmc ^designation[0].language = #de-AT 
* #cmc ^designation[0].value = "Cham-Sprachen" 
* #cop "Coptic"
* #cop ^designation[0].language = #de-AT 
* #cop ^designation[0].value = "Koptisch" 
* #cor "Cornish"
* #cor ^designation[0].language = #de-AT 
* #cor ^designation[0].value = "Kornisch" 
* #cor ^property[0].code = #hints 
* #cor ^property[0].valueString = "kw" 
* #cos "Corsican"
* #cos ^designation[0].language = #de-AT 
* #cos ^designation[0].value = "Korsisch" 
* #cos ^property[0].code = #hints 
* #cos ^property[0].valueString = "co" 
* #cpe "Creoles and pidgins, English based"
* #cpe ^designation[0].language = #de-AT 
* #cpe ^designation[0].value = "Kreolisch-Englisch (Andere)" 
* #cpf "Creoles and pidgins, French-based"
* #cpf ^designation[0].language = #de-AT 
* #cpf ^designation[0].value = "Kreolisch-Französisch (Andere)" 
* #cpp "Creoles and pidgins, Portuguese-based"
* #cpp ^designation[0].language = #de-AT 
* #cpp ^designation[0].value = "Kreolisch-Portugiesisch (Andere)" 
* #cre "Cree"
* #cre ^designation[0].language = #de-AT 
* #cre ^designation[0].value = "Cree-Sprache" 
* #cre ^property[0].code = #hints 
* #cre ^property[0].valueString = "cr" 
* #crh "Crimean Tatar; Crimean Turkish"
* #crh ^designation[0].language = #de-AT 
* #crh ^designation[0].value = "Krimtatarisch" 
* #crp "Creoles and pidgins"
* #crp ^designation[0].language = #de-AT 
* #crp ^designation[0].value = "Kreolische Sprachen; Pidginsprachen (Andere)" 
* #csb "Kashubian"
* #csb ^designation[0].language = #de-AT 
* #csb ^designation[0].value = "Kaschubisch" 
* #cus "Cushitic languages"
* #cus ^designation[0].language = #de-AT 
* #cus ^designation[0].value = "Kuschitische Sprachen (Andere)" 
* #cym "Welsh"
* #cym ^designation[0].language = #de-AT 
* #cym ^designation[0].value = "Kymrisch" 
* #cym ^property[0].code = #hints 
* #cym ^property[0].valueString = "cy" 
* #cze "Czech"
* #cze ^designation[0].language = #de-AT 
* #cze ^designation[0].value = "Tschechisch" 
* #cze ^property[0].code = #hints 
* #cze ^property[0].valueString = "cs" 
* #dak "Dakota"
* #dak ^designation[0].language = #de-AT 
* #dak ^designation[0].value = "Dakota-Sprache" 
* #dan "Danish"
* #dan ^designation[0].language = #de-AT 
* #dan ^designation[0].value = "Dänisch" 
* #dan ^property[0].code = #hints 
* #dan ^property[0].valueString = "da" 
* #dar "Dargwa"
* #dar ^designation[0].language = #de-AT 
* #dar ^designation[0].value = "Darginisch" 
* #day "Land Dayak languages"
* #day ^designation[0].language = #de-AT 
* #day ^designation[0].value = "Dajakisch" 
* #del "Delaware"
* #del ^designation[0].language = #de-AT 
* #del ^designation[0].value = "Delaware-Sprache" 
* #den "Slave (Athapascan)"
* #den ^designation[0].language = #de-AT 
* #den ^designation[0].value = "Slave-Sprache" 
* #deu "German"
* #deu ^designation[0].language = #de-AT 
* #deu ^designation[0].value = "Deutsch" 
* #deu ^property[0].code = #hints 
* #deu ^property[0].valueString = "de" 
* #dgr "Dogrib"
* #dgr ^designation[0].language = #de-AT 
* #dgr ^designation[0].value = "Dogrib-Sprache" 
* #din "Dinka"
* #din ^designation[0].language = #de-AT 
* #din ^designation[0].value = "Dinka-Sprache" 
* #div "Divehi; Dhivehi; Maldivian"
* #div ^designation[0].language = #de-AT 
* #div ^designation[0].value = "Maledivisch" 
* #div ^property[0].code = #hints 
* #div ^property[0].valueString = "dv" 
* #doi "Dogri"
* #doi ^designation[0].language = #de-AT 
* #doi ^designation[0].value = "Dogri" 
* #dra "Dravidian languages"
* #dra ^designation[0].language = #de-AT 
* #dra ^designation[0].value = "Drawidische Sprachen (Andere)" 
* #dsb "Lower Sorbian"
* #dsb ^designation[0].language = #de-AT 
* #dsb ^designation[0].value = "Niedersorbisch" 
* #dua "Duala"
* #dua ^designation[0].language = #de-AT 
* #dua ^designation[0].value = "Duala-Sprachen" 
* #dum "Dutch, Middle (ca.1050-1350)"
* #dum ^designation[0].language = #de-AT 
* #dum ^designation[0].value = "Mittelniederländisch" 
* #dut "Dutch; Flemish"
* #dut ^designation[0].language = #de-AT 
* #dut ^designation[0].value = "Niederländisch" 
* #dut ^property[0].code = #hints 
* #dut ^property[0].valueString = "nl" 
* #dyu "Dyula"
* #dyu ^designation[0].language = #de-AT 
* #dyu ^designation[0].value = "Dyula-Sprache" 
* #dzo "Dzongkha"
* #dzo ^designation[0].language = #de-AT 
* #dzo ^designation[0].value = "Dzongkha" 
* #dzo ^property[0].code = #hints 
* #dzo ^property[0].valueString = "dz" 
* #efi "Efik"
* #efi ^designation[0].language = #de-AT 
* #efi ^designation[0].value = "Efik" 
* #egy "Egyptian (Ancient)"
* #egy ^designation[0].language = #de-AT 
* #egy ^designation[0].value = "Ägyptisch" 
* #eka "Ekajuk"
* #eka ^designation[0].language = #de-AT 
* #eka ^designation[0].value = "Ekajuk" 
* #ell "Greek, Modern (1453-)"
* #ell ^designation[0].language = #de-AT 
* #ell ^designation[0].value = "Neugriechisch" 
* #ell ^property[0].code = #hints 
* #ell ^property[0].valueString = "el" 
* #elx "Elamite"
* #elx ^designation[0].language = #de-AT 
* #elx ^designation[0].value = "Elamisch" 
* #eng "English"
* #eng ^designation[0].language = #de-AT 
* #eng ^designation[0].value = "Englisch" 
* #eng ^property[0].code = #hints 
* #eng ^property[0].valueString = "en" 
* #enm "English, Middle (1100-1500)"
* #enm ^designation[0].language = #de-AT 
* #enm ^designation[0].value = "Mittelenglisch" 
* #epo "Esperanto"
* #epo ^designation[0].language = #de-AT 
* #epo ^designation[0].value = "Esperanto" 
* #epo ^property[0].code = #hints 
* #epo ^property[0].valueString = "eo" 
* #est "Estonian"
* #est ^designation[0].language = #de-AT 
* #est ^designation[0].value = "Estnisch" 
* #est ^property[0].code = #hints 
* #est ^property[0].valueString = "et" 
* #eus "Basque"
* #eus ^designation[0].language = #de-AT 
* #eus ^designation[0].value = "Baskisch" 
* #eus ^property[0].code = #hints 
* #eus ^property[0].valueString = "eu" 
* #ewe "Ewe"
* #ewe ^designation[0].language = #de-AT 
* #ewe ^designation[0].value = "Ewe-Sprache" 
* #ewe ^property[0].code = #hints 
* #ewe ^property[0].valueString = "ee" 
* #ewo "Ewondo"
* #ewo ^designation[0].language = #de-AT 
* #ewo ^designation[0].value = "Ewondo" 
* #fan "Fang"
* #fan ^designation[0].language = #de-AT 
* #fan ^designation[0].value = "Pangwe-Sprache" 
* #fao "Faroese"
* #fao ^designation[0].language = #de-AT 
* #fao ^designation[0].value = "Färöisch" 
* #fao ^property[0].code = #hints 
* #fao ^property[0].valueString = "fo" 
* #fas "Persian"
* #fas ^designation[0].language = #de-AT 
* #fas ^designation[0].value = "Persisch" 
* #fas ^property[0].code = #hints 
* #fas ^property[0].valueString = "fa" 
* #fat "Fanti"
* #fat ^designation[0].language = #de-AT 
* #fat ^designation[0].value = "Fante-Sprache" 
* #fij "Fijian"
* #fij ^designation[0].language = #de-AT 
* #fij ^designation[0].value = "Fidschi-Sprache" 
* #fij ^property[0].code = #hints 
* #fij ^property[0].valueString = "fj" 
* #fil "Filipino; Pilipino"
* #fil ^designation[0].language = #de-AT 
* #fil ^designation[0].value = "Pilipino" 
* #fin "Finnish"
* #fin ^designation[0].language = #de-AT 
* #fin ^designation[0].value = "Finnisch" 
* #fin ^property[0].code = #hints 
* #fin ^property[0].valueString = "fi" 
* #fiu "Finno-Ugrian languages"
* #fiu ^designation[0].language = #de-AT 
* #fiu ^designation[0].value = "Finnougrische Sprachen (Andere)" 
* #fon "Fon"
* #fon ^designation[0].language = #de-AT 
* #fon ^designation[0].value = "Fon-Sprache" 
* #fra "French"
* #fra ^designation[0].language = #de-AT 
* #fra ^designation[0].value = "Französisch" 
* #fra ^property[0].code = #hints 
* #fra ^property[0].valueString = "fr" 
* #fre "French"
* #fre ^designation[0].language = #de-AT 
* #fre ^designation[0].value = "Französisch" 
* #fre ^property[0].code = #hints 
* #fre ^property[0].valueString = "fr" 
* #frm "French, Middle (ca.1400-1600)"
* #frm ^designation[0].language = #de-AT 
* #frm ^designation[0].value = "Mittelfranzösisch" 
* #fro "French, Old (842-ca.1400)"
* #fro ^designation[0].language = #de-AT 
* #fro ^designation[0].value = "Altfranzösisch" 
* #frr "Northern Frisian"
* #frr ^designation[0].language = #de-AT 
* #frr ^designation[0].value = "Nordfriesisch" 
* #frs "Eastern Frisian"
* #frs ^designation[0].language = #de-AT 
* #frs ^designation[0].value = "Ostfriesisch" 
* #fry "Western Frisian"
* #fry ^designation[0].language = #de-AT 
* #fry ^designation[0].value = "Friesisch" 
* #fry ^property[0].code = #hints 
* #fry ^property[0].valueString = "fy" 
* #ful "Fulah"
* #ful ^designation[0].language = #de-AT 
* #ful ^designation[0].value = "Ful" 
* #ful ^property[0].code = #hints 
* #ful ^property[0].valueString = "ff" 
* #fur "Friulian"
* #fur ^designation[0].language = #de-AT 
* #fur ^designation[0].value = "Friulisch" 
* #gaa "Ga"
* #gaa ^designation[0].language = #de-AT 
* #gaa ^designation[0].value = "Ga-Sprache" 
* #gay "Gayo"
* #gay ^designation[0].language = #de-AT 
* #gay ^designation[0].value = "Gayo-Sprache" 
* #gba "Gbaya"
* #gba ^designation[0].language = #de-AT 
* #gba ^designation[0].value = "Gbaya-Sprache" 
* #gem "Germanic languages"
* #gem ^designation[0].language = #de-AT 
* #gem ^designation[0].value = "Germanische Sprachen (Andere)" 
* #geo "Georgian"
* #geo ^designation[0].language = #de-AT 
* #geo ^designation[0].value = "Georgisch" 
* #geo ^property[0].code = #hints 
* #geo ^property[0].valueString = "ka" 
* #ger "German"
* #ger ^designation[0].language = #de-AT 
* #ger ^designation[0].value = "Deutsch" 
* #ger ^property[0].code = #hints 
* #ger ^property[0].valueString = "de" 
* #gez "Geez"
* #gez ^designation[0].language = #de-AT 
* #gez ^designation[0].value = "Altäthiopisch" 
* #gil "Gilbertese"
* #gil ^designation[0].language = #de-AT 
* #gil ^designation[0].value = "Gilbertesisch" 
* #gla "Gaelic; Scottish Gaelic"
* #gla ^designation[0].language = #de-AT 
* #gla ^designation[0].value = "Gälisch-Schottisch" 
* #gla ^property[0].code = #hints 
* #gla ^property[0].valueString = "gd" 
* #gle "Irish"
* #gle ^designation[0].language = #de-AT 
* #gle ^designation[0].value = "Irisch" 
* #gle ^property[0].code = #hints 
* #gle ^property[0].valueString = "ga" 
* #glg "Galician"
* #glg ^designation[0].language = #de-AT 
* #glg ^designation[0].value = "Galicisch" 
* #glg ^property[0].code = #hints 
* #glg ^property[0].valueString = "gl" 
* #glv "Manx"
* #glv ^designation[0].language = #de-AT 
* #glv ^designation[0].value = "Manx" 
* #glv ^property[0].code = #hints 
* #glv ^property[0].valueString = "gv" 
* #gmh "German, Middle High (ca.1050-1500)"
* #gmh ^designation[0].language = #de-AT 
* #gmh ^designation[0].value = "Mittelhochdeutsch" 
* #goh "German, Old High (ca.750-1050)"
* #goh ^designation[0].language = #de-AT 
* #goh ^designation[0].value = "Althochdeutsch" 
* #gon "Gondi"
* #gon ^designation[0].language = #de-AT 
* #gon ^designation[0].value = "Gondi-Sprache" 
* #gor "Gorontalo"
* #gor ^designation[0].language = #de-AT 
* #gor ^designation[0].value = "Gorontalesisch" 
* #got "Gothic"
* #got ^designation[0].language = #de-AT 
* #got ^designation[0].value = "Gotisch" 
* #grb "Grebo"
* #grb ^designation[0].language = #de-AT 
* #grb ^designation[0].value = "Grebo-Sprache" 
* #grc "Greek, Ancient (to 1453)"
* #grc ^designation[0].language = #de-AT 
* #grc ^designation[0].value = "Griechisch" 
* #gre "Greek, Modern (1453-)"
* #gre ^designation[0].language = #de-AT 
* #gre ^designation[0].value = "Neugriechisch" 
* #gre ^property[0].code = #hints 
* #gre ^property[0].valueString = "el" 
* #grn "Guarani"
* #grn ^designation[0].language = #de-AT 
* #grn ^designation[0].value = "Guaraní-Sprache" 
* #grn ^property[0].code = #hints 
* #grn ^property[0].valueString = "gn" 
* #gsw "Swiss German; Alemannic; Alsatian"
* #gsw ^designation[0].language = #de-AT 
* #gsw ^designation[0].value = "Schweizerdeutsch" 
* #guj "Gujarati"
* #guj ^designation[0].language = #de-AT 
* #guj ^designation[0].value = "Gujarati-Sprache" 
* #guj ^property[0].code = #hints 
* #guj ^property[0].valueString = "gu" 
* #gwi "Gwich'in"
* #gwi ^designation[0].language = #de-AT 
* #gwi ^designation[0].value = "Kutchin-Sprache" 
* #hai "Haida"
* #hai ^designation[0].language = #de-AT 
* #hai ^designation[0].value = "Haida-Sprache" 
* #hat "Haitian; Haitian Creole"
* #hat ^designation[0].language = #de-AT 
* #hat ^designation[0].value = "Haïtien (Haiti-Kreolisch)" 
* #hat ^property[0].code = #hints 
* #hat ^property[0].valueString = "ht" 
* #hau "Hausa"
* #hau ^designation[0].language = #de-AT 
* #hau ^designation[0].value = "Haussa-Sprache" 
* #hau ^property[0].code = #hints 
* #hau ^property[0].valueString = "ha" 
* #haw "Hawaiian"
* #haw ^designation[0].language = #de-AT 
* #haw ^designation[0].value = "Hawaiisch" 
* #heb "Hebrew"
* #heb ^designation[0].language = #de-AT 
* #heb ^designation[0].value = "Hebräisch" 
* #heb ^property[0].code = #hints 
* #heb ^property[0].valueString = "he" 
* #her "Herero"
* #her ^designation[0].language = #de-AT 
* #her ^designation[0].value = "Herero-Sprache" 
* #her ^property[0].code = #hints 
* #her ^property[0].valueString = "hz" 
* #hil "Hiligaynon"
* #hil ^designation[0].language = #de-AT 
* #hil ^designation[0].value = "Hiligaynon-Sprache" 
* #him "Himachali languages; Western Pahari languages"
* #him ^designation[0].language = #de-AT 
* #him ^designation[0].value = "Himachali" 
* #hin "Hindi"
* #hin ^designation[0].language = #de-AT 
* #hin ^designation[0].value = "Hindi" 
* #hin ^property[0].code = #hints 
* #hin ^property[0].valueString = "hi" 
* #hit "Hittite"
* #hit ^designation[0].language = #de-AT 
* #hit ^designation[0].value = "Hethitisch" 
* #hmn "Hmong; Mong"
* #hmn ^designation[0].language = #de-AT 
* #hmn ^designation[0].value = "Miao-Sprachen" 
* #hmo "Hiri Motu"
* #hmo ^designation[0].language = #de-AT 
* #hmo ^designation[0].value = "Hiri-Motu" 
* #hmo ^property[0].code = #hints 
* #hmo ^property[0].valueString = "ho" 
* #hrv "Croatian"
* #hrv ^designation[0].language = #de-AT 
* #hrv ^designation[0].value = "Kroatisch" 
* #hrv ^property[0].code = #hints 
* #hrv ^property[0].valueString = "hr" 
* #hsb "Upper Sorbian"
* #hsb ^designation[0].language = #de-AT 
* #hsb ^designation[0].value = "Obersorbisch" 
* #hun "Hungarian"
* #hun ^designation[0].language = #de-AT 
* #hun ^designation[0].value = "Ungarisch" 
* #hun ^property[0].code = #hints 
* #hun ^property[0].valueString = "hu" 
* #hup "Hupa"
* #hup ^designation[0].language = #de-AT 
* #hup ^designation[0].value = "Hupa-Sprache" 
* #hye "Armenian"
* #hye ^designation[0].language = #de-AT 
* #hye ^designation[0].value = "Armenisch" 
* #hye ^property[0].code = #hints 
* #hye ^property[0].valueString = "hy" 
* #iba "Iban"
* #iba ^designation[0].language = #de-AT 
* #iba ^designation[0].value = "Iban-Sprache" 
* #ibo "Igbo"
* #ibo ^designation[0].language = #de-AT 
* #ibo ^designation[0].value = "Ibo-Sprache" 
* #ibo ^property[0].code = #hints 
* #ibo ^property[0].valueString = "ig" 
* #ice "Icelandic"
* #ice ^designation[0].language = #de-AT 
* #ice ^designation[0].value = "Isländisch" 
* #ice ^property[0].code = #hints 
* #ice ^property[0].valueString = "is" 
* #ido "Ido"
* #ido ^designation[0].language = #de-AT 
* #ido ^designation[0].value = "Ido" 
* #ido ^property[0].code = #hints 
* #ido ^property[0].valueString = "io" 
* #iii "Sichuan Yi; Nuosu"
* #iii ^designation[0].language = #de-AT 
* #iii ^designation[0].value = "Lalo-Sprache" 
* #iii ^property[0].code = #hints 
* #iii ^property[0].valueString = "ii" 
* #ijo "Ijo languages"
* #ijo ^designation[0].language = #de-AT 
* #ijo ^designation[0].value = "Ijo-Sprache" 
* #iku "Inuktitut"
* #iku ^designation[0].language = #de-AT 
* #iku ^designation[0].value = "Inuktitut" 
* #iku ^property[0].code = #hints 
* #iku ^property[0].valueString = "iu" 
* #ile "Interlingue; Occidental"
* #ile ^designation[0].language = #de-AT 
* #ile ^designation[0].value = "Interlingue" 
* #ile ^property[0].code = #hints 
* #ile ^property[0].valueString = "ie" 
* #ilo "Iloko"
* #ilo ^designation[0].language = #de-AT 
* #ilo ^designation[0].value = "Ilokano-Sprache" 
* #ina "Interlingua (International Auxiliary Language Association)"
* #ina ^designation[0].language = #de-AT 
* #ina ^designation[0].value = "Interlingua" 
* #ina ^property[0].code = #hints 
* #ina ^property[0].valueString = "ia" 
* #inc "Indic languages"
* #inc ^designation[0].language = #de-AT 
* #inc ^designation[0].value = "Indoarische Sprachen (Andere)" 
* #ind "Indonesian"
* #ind ^designation[0].language = #de-AT 
* #ind ^designation[0].value = "Bahasa Indonesia" 
* #ind ^property[0].code = #hints 
* #ind ^property[0].valueString = "id" 
* #ine "Indo-European languages"
* #ine ^designation[0].language = #de-AT 
* #ine ^designation[0].value = "Indogermanische Sprachen (Andere)" 
* #inh "Ingush"
* #inh ^designation[0].language = #de-AT 
* #inh ^designation[0].value = "Inguschisch" 
* #ipk "Inupiaq"
* #ipk ^designation[0].language = #de-AT 
* #ipk ^designation[0].value = "Inupik" 
* #ipk ^property[0].code = #hints 
* #ipk ^property[0].valueString = "ik" 
* #ira "Iranian languages"
* #ira ^designation[0].language = #de-AT 
* #ira ^designation[0].value = "Iranische Sprachen (Andere)" 
* #iro "Iroquoian languages"
* #iro ^designation[0].language = #de-AT 
* #iro ^designation[0].value = "Irokesische Sprachen" 
* #isl "Icelandic"
* #isl ^designation[0].language = #de-AT 
* #isl ^designation[0].value = "Isländisch" 
* #isl ^property[0].code = #hints 
* #isl ^property[0].valueString = "is" 
* #ita "Italian"
* #ita ^designation[0].language = #de-AT 
* #ita ^designation[0].value = "Italienisch" 
* #ita ^property[0].code = #hints 
* #ita ^property[0].valueString = "it" 
* #jav "Javanese"
* #jav ^designation[0].language = #de-AT 
* #jav ^designation[0].value = "Javanisch" 
* #jav ^property[0].code = #hints 
* #jav ^property[0].valueString = "jv" 
* #jbo "Lojban"
* #jbo ^designation[0].language = #de-AT 
* #jbo ^designation[0].value = "Lojban" 
* #jpn "Japanese"
* #jpn ^designation[0].language = #de-AT 
* #jpn ^designation[0].value = "Japanisch" 
* #jpn ^property[0].code = #hints 
* #jpn ^property[0].valueString = "ja" 
* #jpr "Judeo-Persian"
* #jpr ^designation[0].language = #de-AT 
* #jpr ^designation[0].value = "Jüdisch-Persisch" 
* #jrb "Judeo-Arabic"
* #jrb ^designation[0].language = #de-AT 
* #jrb ^designation[0].value = "Jüdisch-Arabisch" 
* #kaa "Kara-Kalpak"
* #kaa ^designation[0].language = #de-AT 
* #kaa ^designation[0].value = "Karakalpakisch" 
* #kab "Kabyle"
* #kab ^designation[0].language = #de-AT 
* #kab ^designation[0].value = "Kabylisch" 
* #kac "Kachin; Jingpho"
* #kac ^designation[0].language = #de-AT 
* #kac ^designation[0].value = "Kachin-Sprache" 
* #kal "Kalaallisut; Greenlandic"
* #kal ^designation[0].language = #de-AT 
* #kal ^designation[0].value = "Grönländisch" 
* #kal ^property[0].code = #hints 
* #kal ^property[0].valueString = "kl" 
* #kam "Kamba"
* #kam ^designation[0].language = #de-AT 
* #kam ^designation[0].value = "Kamba-Sprache" 
* #kan "Kannada"
* #kan ^designation[0].language = #de-AT 
* #kan ^designation[0].value = "Kannada" 
* #kan ^property[0].code = #hints 
* #kan ^property[0].valueString = "kn" 
* #kar "Karen languages"
* #kar ^designation[0].language = #de-AT 
* #kar ^designation[0].value = "Karenisch" 
* #kas "Kashmiri"
* #kas ^designation[0].language = #de-AT 
* #kas ^designation[0].value = "Kaschmiri" 
* #kas ^property[0].code = #hints 
* #kas ^property[0].valueString = "ks" 
* #kat "Georgian"
* #kat ^designation[0].language = #de-AT 
* #kat ^designation[0].value = "Georgisch" 
* #kat ^property[0].code = #hints 
* #kat ^property[0].valueString = "ka" 
* #kau "Kanuri"
* #kau ^designation[0].language = #de-AT 
* #kau ^designation[0].value = "Kanuri-Sprache" 
* #kau ^property[0].code = #hints 
* #kau ^property[0].valueString = "kr" 
* #kaw "Kawi"
* #kaw ^designation[0].language = #de-AT 
* #kaw ^designation[0].value = "Kawi" 
* #kaz "Kazakh"
* #kaz ^designation[0].language = #de-AT 
* #kaz ^designation[0].value = "Kasachisch" 
* #kaz ^property[0].code = #hints 
* #kaz ^property[0].valueString = "kk" 
* #kbd "Kabardian"
* #kbd ^designation[0].language = #de-AT 
* #kbd ^designation[0].value = "Kabardinisch" 
* #kha "Khasi"
* #kha ^designation[0].language = #de-AT 
* #kha ^designation[0].value = "Khasi-Sprache" 
* #khi "Khoisan languages"
* #khi ^designation[0].language = #de-AT 
* #khi ^designation[0].value = "Khoisan-Sprachen (Andere)" 
* #khm "Central Khmer"
* #khm ^designation[0].language = #de-AT 
* #khm ^designation[0].value = "Kambodschanisch" 
* #khm ^property[0].code = #hints 
* #khm ^property[0].valueString = "km" 
* #kho "Khotanese; Sakan"
* #kho ^designation[0].language = #de-AT 
* #kho ^designation[0].value = "Sakisch" 
* #kik "Kikuyu; Gikuyu"
* #kik ^designation[0].language = #de-AT 
* #kik ^designation[0].value = "Kikuyu-Sprache" 
* #kik ^property[0].code = #hints 
* #kik ^property[0].valueString = "ki" 
* #kin "Kinyarwanda"
* #kin ^designation[0].language = #de-AT 
* #kin ^designation[0].value = "Rwanda-Sprache" 
* #kin ^property[0].code = #hints 
* #kin ^property[0].valueString = "rw" 
* #kir "Kirghiz; Kyrgyz"
* #kir ^designation[0].language = #de-AT 
* #kir ^designation[0].value = "Kirgisisch" 
* #kir ^property[0].code = #hints 
* #kir ^property[0].valueString = "ky" 
* #kmb "Kimbundu"
* #kmb ^designation[0].language = #de-AT 
* #kmb ^designation[0].value = "Kimbundu-Sprache" 
* #kok "Konkani"
* #kok ^designation[0].language = #de-AT 
* #kok ^designation[0].value = "Konkani" 
* #kom "Komi"
* #kom ^designation[0].language = #de-AT 
* #kom ^designation[0].value = "Komi-Sprache" 
* #kom ^property[0].code = #hints 
* #kom ^property[0].valueString = "kv" 
* #kon "Kongo"
* #kon ^designation[0].language = #de-AT 
* #kon ^designation[0].value = "Kongo-Sprache" 
* #kon ^property[0].code = #hints 
* #kon ^property[0].valueString = "kg" 
* #kor "Korean"
* #kor ^designation[0].language = #de-AT 
* #kor ^designation[0].value = "Koreanisch" 
* #kor ^property[0].code = #hints 
* #kor ^property[0].valueString = "ko" 
* #kos "Kosraean"
* #kos ^designation[0].language = #de-AT 
* #kos ^designation[0].value = "Kosraeanisch" 
* #kpe "Kpelle"
* #kpe ^designation[0].language = #de-AT 
* #kpe ^designation[0].value = "Kpelle-Sprache" 
* #krc "Karachay-Balkar"
* #krc ^designation[0].language = #de-AT 
* #krc ^designation[0].value = "Karatschaiisch-Balkarisch" 
* #krl "Karelian"
* #krl ^designation[0].language = #de-AT 
* #krl ^designation[0].value = "Karelisch" 
* #kro "Kru languages"
* #kro ^designation[0].language = #de-AT 
* #kro ^designation[0].value = "Kru-Sprachen (Andere)" 
* #kru "Kurukh"
* #kru ^designation[0].language = #de-AT 
* #kru ^designation[0].value = "Oraon-Sprache" 
* #kua "Kuanyama; Kwanyama"
* #kua ^designation[0].language = #de-AT 
* #kua ^designation[0].value = "Kwanyama-Sprache" 
* #kua ^property[0].code = #hints 
* #kua ^property[0].valueString = "kj" 
* #kum "Kumyk"
* #kum ^designation[0].language = #de-AT 
* #kum ^designation[0].value = "Kumükisch" 
* #kur "Kurdish"
* #kur ^designation[0].language = #de-AT 
* #kur ^designation[0].value = "Kurdisch" 
* #kur ^property[0].code = #hints 
* #kur ^property[0].valueString = "ku" 
* #kut "Kutenai"
* #kut ^designation[0].language = #de-AT 
* #kut ^designation[0].value = "Kutenai-Sprache" 
* #lad "Ladino"
* #lad ^designation[0].language = #de-AT 
* #lad ^designation[0].value = "Judenspanisch" 
* #lah "Lahnda"
* #lah ^designation[0].language = #de-AT 
* #lah ^designation[0].value = "Lahnda" 
* #lam "Lamba"
* #lam ^designation[0].language = #de-AT 
* #lam ^designation[0].value = "Lamba-Sprache (Bantusprache)" 
* #lao "Lao"
* #lao ^designation[0].language = #de-AT 
* #lao ^designation[0].value = "Laotisch" 
* #lao ^property[0].code = #hints 
* #lao ^property[0].valueString = "lo" 
* #lat "Latin"
* #lat ^designation[0].language = #de-AT 
* #lat ^designation[0].value = "Latein" 
* #lat ^property[0].code = #hints 
* #lat ^property[0].valueString = "la" 
* #lav "Latvian"
* #lav ^designation[0].language = #de-AT 
* #lav ^designation[0].value = "Lettisch" 
* #lav ^property[0].code = #hints 
* #lav ^property[0].valueString = "lv" 
* #lez "Lezghian"
* #lez ^designation[0].language = #de-AT 
* #lez ^designation[0].value = "Lesgisch" 
* #lim "Limburgan; Limburger; Limburgish"
* #lim ^designation[0].language = #de-AT 
* #lim ^designation[0].value = "Limburgisch" 
* #lim ^property[0].code = #hints 
* #lim ^property[0].valueString = "li" 
* #lin "Lingala"
* #lin ^designation[0].language = #de-AT 
* #lin ^designation[0].value = "Lingala" 
* #lin ^property[0].code = #hints 
* #lin ^property[0].valueString = "ln" 
* #lit "Lithuanian"
* #lit ^designation[0].language = #de-AT 
* #lit ^designation[0].value = "Litauisch" 
* #lit ^property[0].code = #hints 
* #lit ^property[0].valueString = "lt" 
* #lol "Mongo"
* #lol ^designation[0].language = #de-AT 
* #lol ^designation[0].value = "Mongo-Sprache" 
* #loz "Lozi"
* #loz ^designation[0].language = #de-AT 
* #loz ^designation[0].value = "Rotse-Sprache" 
* #ltz "Luxembourgish; Letzeburgesch"
* #ltz ^designation[0].language = #de-AT 
* #ltz ^designation[0].value = "Luxemburgisch" 
* #ltz ^property[0].code = #hints 
* #ltz ^property[0].valueString = "lb" 
* #lua "Luba-Lulua"
* #lua ^designation[0].language = #de-AT 
* #lua ^designation[0].value = "Lulua-Sprache" 
* #lub "Luba-Katanga"
* #lub ^designation[0].language = #de-AT 
* #lub ^designation[0].value = "Luba-Katanga-Sprache" 
* #lub ^property[0].code = #hints 
* #lub ^property[0].valueString = "lu" 
* #lug "Ganda"
* #lug ^designation[0].language = #de-AT 
* #lug ^designation[0].value = "Ganda-Sprache" 
* #lug ^property[0].code = #hints 
* #lug ^property[0].valueString = "lg" 
* #lui "Luiseno"
* #lui ^designation[0].language = #de-AT 
* #lui ^designation[0].value = "Luiseño-Sprache" 
* #lun "Lunda"
* #lun ^designation[0].language = #de-AT 
* #lun ^designation[0].value = "Lunda-Sprache" 
* #luo "Luo (Kenya and Tanzania)"
* #luo ^designation[0].language = #de-AT 
* #luo ^designation[0].value = "Luo-Sprache" 
* #lus "Lushai"
* #lus ^designation[0].language = #de-AT 
* #lus ^designation[0].value = "Lushai-Sprache" 
* #mac "Macedonian"
* #mac ^designation[0].language = #de-AT 
* #mac ^designation[0].value = "Makedonisch" 
* #mac ^property[0].code = #hints 
* #mac ^property[0].valueString = "mk" 
* #mad "Madurese"
* #mad ^designation[0].language = #de-AT 
* #mad ^designation[0].value = "Maduresisch" 
* #mag "Magahi"
* #mag ^designation[0].language = #de-AT 
* #mag ^designation[0].value = "Khotta" 
* #mah "Marshallese"
* #mah ^designation[0].language = #de-AT 
* #mah ^designation[0].value = "Marschallesisch" 
* #mah ^property[0].code = #hints 
* #mah ^property[0].valueString = "mh" 
* #mai "Maithili"
* #mai ^designation[0].language = #de-AT 
* #mai ^designation[0].value = "Maithili" 
* #mak "Makasar"
* #mak ^designation[0].language = #de-AT 
* #mak ^designation[0].value = "Makassarisch" 
* #mal "Malayalam"
* #mal ^designation[0].language = #de-AT 
* #mal ^designation[0].value = "Malayalam" 
* #mal ^property[0].code = #hints 
* #mal ^property[0].valueString = "ml" 
* #man "Mandingo"
* #man ^designation[0].language = #de-AT 
* #man ^designation[0].value = "Malinke-Sprache" 
* #mao "Maori"
* #mao ^designation[0].language = #de-AT 
* #mao ^designation[0].value = "Maori-Sprache" 
* #mao ^property[0].code = #hints 
* #mao ^property[0].valueString = "mi" 
* #map "Austronesian languages"
* #map ^designation[0].language = #de-AT 
* #map ^designation[0].value = "Austronesische Sprachen (Andere)" 
* #mar "Marathi"
* #mar ^designation[0].language = #de-AT 
* #mar ^designation[0].value = "Marathi" 
* #mar ^property[0].code = #hints 
* #mar ^property[0].valueString = "mr" 
* #mas "Masai"
* #mas ^designation[0].language = #de-AT 
* #mas ^designation[0].value = "Massai-Sprache" 
* #may "Malay"
* #may ^designation[0].language = #de-AT 
* #may ^designation[0].value = "Malaiisch" 
* #may ^property[0].code = #hints 
* #may ^property[0].valueString = "ms" 
* #mdf "Moksha"
* #mdf ^designation[0].language = #de-AT 
* #mdf ^designation[0].value = "Mokscha-Sprache" 
* #mdr "Mandar"
* #mdr ^designation[0].language = #de-AT 
* #mdr ^designation[0].value = "Mandaresisch" 
* #men "Mende"
* #men ^designation[0].language = #de-AT 
* #men ^designation[0].value = "Mende-Sprache" 
* #mga "Irish, Middle (900-1200)"
* #mga ^designation[0].language = #de-AT 
* #mga ^designation[0].value = "Mittelirisch" 
* #mic "Mi'kmaq; Micmac"
* #mic ^designation[0].language = #de-AT 
* #mic ^designation[0].value = "Micmac-Sprache" 
* #min "Minangkabau"
* #min ^designation[0].language = #de-AT 
* #min ^designation[0].value = "Minangkabau-Sprache" 
* #mis "Uncoded languages"
* #mis ^designation[0].language = #de-AT 
* #mis ^designation[0].value = "Einzelne andere Sprachen" 
* #mkd "Macedonian"
* #mkd ^designation[0].language = #de-AT 
* #mkd ^designation[0].value = "Makedonisch" 
* #mkd ^property[0].code = #hints 
* #mkd ^property[0].valueString = "mk" 
* #mkh "Mon-Khmer languages"
* #mkh ^designation[0].language = #de-AT 
* #mkh ^designation[0].value = "Mon-Khmer-Sprachen (Andere)" 
* #mlg "Malagasy"
* #mlg ^designation[0].language = #de-AT 
* #mlg ^designation[0].value = "Malagassi-Sprache" 
* #mlg ^property[0].code = #hints 
* #mlg ^property[0].valueString = "mg" 
* #mlt "Maltese"
* #mlt ^designation[0].language = #de-AT 
* #mlt ^designation[0].value = "Maltesisch" 
* #mlt ^property[0].code = #hints 
* #mlt ^property[0].valueString = "mt" 
* #mnc "Manchu"
* #mnc ^designation[0].language = #de-AT 
* #mnc ^designation[0].value = "Mandschurisch" 
* #mni "Manipuri"
* #mni ^designation[0].language = #de-AT 
* #mni ^designation[0].value = "Meithei-Sprache" 
* #mno "Manobo languages"
* #mno ^designation[0].language = #de-AT 
* #mno ^designation[0].value = "Manobo-Sprachen" 
* #moh "Mohawk"
* #moh ^designation[0].language = #de-AT 
* #moh ^designation[0].value = "Mohawk-Sprache" 
* #mon "Mongolian"
* #mon ^designation[0].language = #de-AT 
* #mon ^designation[0].value = "Mongolisch" 
* #mon ^property[0].code = #hints 
* #mon ^property[0].valueString = "mn" 
* #mos "Mossi"
* #mos ^designation[0].language = #de-AT 
* #mos ^designation[0].value = "Mossi-Sprache" 
* #mri "Maori"
* #mri ^designation[0].language = #de-AT 
* #mri ^designation[0].value = "Maori-Sprache" 
* #mri ^property[0].code = #hints 
* #mri ^property[0].valueString = "mi" 
* #msa "Malay"
* #msa ^designation[0].language = #de-AT 
* #msa ^designation[0].value = "Malaiisch" 
* #msa ^property[0].code = #hints 
* #msa ^property[0].valueString = "ms" 
* #mul "Multiple languages"
* #mul ^designation[0].language = #de-AT 
* #mul ^designation[0].value = "Mehrere Sprachen" 
* #mun "Munda languages"
* #mun ^designation[0].language = #de-AT 
* #mun ^designation[0].value = "Mundasprachen (Andere)" 
* #mus "Creek"
* #mus ^designation[0].language = #de-AT 
* #mus ^designation[0].value = "Muskogisch" 
* #mwl "Mirandese"
* #mwl ^designation[0].language = #de-AT 
* #mwl ^designation[0].value = "Mirandesisch" 
* #mwr "Marwari"
* #mwr ^designation[0].language = #de-AT 
* #mwr ^designation[0].value = "Marwari" 
* #mya "Burmese"
* #mya ^designation[0].language = #de-AT 
* #mya ^designation[0].value = "Birmanisch" 
* #mya ^property[0].code = #hints 
* #mya ^property[0].valueString = "my" 
* #myn "Mayan languages"
* #myn ^designation[0].language = #de-AT 
* #myn ^designation[0].value = "Maya-Sprachen" 
* #myv "Erzya"
* #myv ^designation[0].language = #de-AT 
* #myv ^designation[0].value = "Erza-Mordwinisch" 
* #nah "Nahuatl languages"
* #nah ^designation[0].language = #de-AT 
* #nah ^designation[0].value = "Nahuatl" 
* #nai "North American Indian languages"
* #nai ^designation[0].language = #de-AT 
* #nai ^designation[0].value = "Indianersprachen, Nordamerika (Andere)" 
* #nap "Neapolitan"
* #nap ^designation[0].language = #de-AT 
* #nap ^designation[0].value = "Neapel / Mundart" 
* #nau "Nauru"
* #nau ^designation[0].language = #de-AT 
* #nau ^designation[0].value = "Nauruanisch" 
* #nau ^property[0].code = #hints 
* #nau ^property[0].valueString = "na" 
* #nav "Navajo; Navaho"
* #nav ^designation[0].language = #de-AT 
* #nav ^designation[0].value = "Navajo-Sprache" 
* #nav ^property[0].code = #hints 
* #nav ^property[0].valueString = "nv" 
* #nbl "Ndebele, South; South Ndebele"
* #nbl ^designation[0].language = #de-AT 
* #nbl ^designation[0].value = "Ndebele-Sprache (Transvaal)" 
* #nbl ^property[0].code = #hints 
* #nbl ^property[0].valueString = "nr" 
* #nde "Ndebele, North; North Ndebele"
* #nde ^designation[0].language = #de-AT 
* #nde ^designation[0].value = "Ndebele-Sprache (Simbabwe)" 
* #nde ^property[0].code = #hints 
* #nde ^property[0].valueString = "nd" 
* #ndo "Ndonga"
* #ndo ^designation[0].language = #de-AT 
* #ndo ^designation[0].value = "Ndonga" 
* #ndo ^property[0].code = #hints 
* #ndo ^property[0].valueString = "ng" 
* #nds "Low German; Low Saxon; German, Low; Saxon, Low"
* #nds ^designation[0].language = #de-AT 
* #nds ^designation[0].value = "Niederdeutsch" 
* #nep "Nepali"
* #nep ^designation[0].language = #de-AT 
* #nep ^designation[0].value = "Nepali" 
* #nep ^property[0].code = #hints 
* #nep ^property[0].valueString = "ne" 
* #new "Nepal Bhasa; Newari"
* #new ^designation[0].language = #de-AT 
* #new ^designation[0].value = "Newari" 
* #nia "Nias"
* #nia ^designation[0].language = #de-AT 
* #nia ^designation[0].value = "Nias-Sprache" 
* #nic "Niger-Kordofanian languages"
* #nic ^designation[0].language = #de-AT 
* #nic ^designation[0].value = "Nigerkordofanische Sprachen (Andere)" 
* #niu "Niuean"
* #niu ^designation[0].language = #de-AT 
* #niu ^designation[0].value = "Niue-Sprache" 
* #nld "Dutch; Flemish"
* #nld ^designation[0].language = #de-AT 
* #nld ^designation[0].value = "Niederländisch" 
* #nld ^property[0].code = #hints 
* #nld ^property[0].valueString = "nl" 
* #nno "Norwegian Nynorsk; Nynorsk, Norwegian"
* #nno ^designation[0].language = #de-AT 
* #nno ^designation[0].value = "Nynorsk" 
* #nno ^property[0].code = #hints 
* #nno ^property[0].valueString = "nn" 
* #nob "Bokmål, Norwegian; Norwegian Bokmål"
* #nob ^designation[0].language = #de-AT 
* #nob ^designation[0].value = "Bokmål" 
* #nob ^property[0].code = #hints 
* #nob ^property[0].valueString = "nb" 
* #nog "Nogai"
* #nog ^designation[0].language = #de-AT 
* #nog ^designation[0].value = "Nogaisch" 
* #non "Norse, Old"
* #non ^designation[0].language = #de-AT 
* #non ^designation[0].value = "Altnorwegisch" 
* #nor "Norwegian"
* #nor ^designation[0].language = #de-AT 
* #nor ^designation[0].value = "Norwegisch" 
* #nor ^property[0].code = #hints 
* #nor ^property[0].valueString = "no" 
* #nqo "N'Ko"
* #nqo ^designation[0].language = #de-AT 
* #nqo ^designation[0].value = "N'Ko" 
* #nso "Pedi; Sepedi; Northern Sotho"
* #nso ^designation[0].language = #de-AT 
* #nso ^designation[0].value = "Pedi-Sprache" 
* #nub "Nubian languages"
* #nub ^designation[0].language = #de-AT 
* #nub ^designation[0].value = "Nubische Sprachen" 
* #nwc "Classical Newari; Old Newari; Classical Nepal Bhasa"
* #nwc ^designation[0].language = #de-AT 
* #nwc ^designation[0].value = "Alt-Newari" 
* #nya "Chichewa; Chewa; Nyanja"
* #nya ^designation[0].language = #de-AT 
* #nya ^designation[0].value = "Nyanja-Sprache" 
* #nya ^property[0].code = #hints 
* #nya ^property[0].valueString = "ny" 
* #nym "Nyamwezi"
* #nym ^designation[0].language = #de-AT 
* #nym ^designation[0].value = "Nyamwezi-Sprache" 
* #nyn "Nyankole"
* #nyn ^designation[0].language = #de-AT 
* #nyn ^designation[0].value = "Nkole-Sprache" 
* #nyo "Nyoro"
* #nyo ^designation[0].language = #de-AT 
* #nyo ^designation[0].value = "Nyoro-Sprache" 
* #nzi "Nzima"
* #nzi ^designation[0].language = #de-AT 
* #nzi ^designation[0].value = "Nzima-Sprache" 
* #oci "Occitan (post 1500)"
* #oci ^designation[0].language = #de-AT 
* #oci ^designation[0].value = "Okzitanisch" 
* #oci ^property[0].code = #hints 
* #oci ^property[0].valueString = "oc" 
* #oji "Ojibwa"
* #oji ^designation[0].language = #de-AT 
* #oji ^designation[0].value = "Ojibwa-Sprache" 
* #oji ^property[0].code = #hints 
* #oji ^property[0].valueString = "oj" 
* #ori "Oriya"
* #ori ^designation[0].language = #de-AT 
* #ori ^designation[0].value = "Oriya-Sprache" 
* #ori ^property[0].code = #hints 
* #ori ^property[0].valueString = "or" 
* #orm "Oromo"
* #orm ^designation[0].language = #de-AT 
* #orm ^designation[0].value = "Galla-Sprache" 
* #orm ^property[0].code = #hints 
* #orm ^property[0].valueString = "om" 
* #osa "Osage"
* #osa ^designation[0].language = #de-AT 
* #osa ^designation[0].value = "Osage-Sprache" 
* #oss "Ossetian; Ossetic"
* #oss ^designation[0].language = #de-AT 
* #oss ^designation[0].value = "Ossetisch" 
* #oss ^property[0].code = #hints 
* #oss ^property[0].valueString = "os" 
* #ota "Turkish, Ottoman (1500-1928)"
* #ota ^designation[0].language = #de-AT 
* #ota ^designation[0].value = "Osmanisch" 
* #oto "Otomian languages"
* #oto ^designation[0].language = #de-AT 
* #oto ^designation[0].value = "Otomangue-Sprachen" 
* #paa "Papuan languages"
* #paa ^designation[0].language = #de-AT 
* #paa ^designation[0].value = "Papuasprachen (Andere)" 
* #pag "Pangasinan"
* #pag ^designation[0].language = #de-AT 
* #pag ^designation[0].value = "Pangasinan-Sprache" 
* #pal "Pahlavi"
* #pal ^designation[0].language = #de-AT 
* #pal ^designation[0].value = "Mittelpersisch" 
* #pam "Pampanga; Kapampangan"
* #pam ^designation[0].language = #de-AT 
* #pam ^designation[0].value = "Pampanggan-Sprache" 
* #pan "Panjabi; Punjabi"
* #pan ^designation[0].language = #de-AT 
* #pan ^designation[0].value = "Pandschabi-Sprache" 
* #pan ^property[0].code = #hints 
* #pan ^property[0].valueString = "pa" 
* #pap "Papiamento"
* #pap ^designation[0].language = #de-AT 
* #pap ^designation[0].value = "Papiamento" 
* #pau "Palauan"
* #pau ^designation[0].language = #de-AT 
* #pau ^designation[0].value = "Palau-Sprache" 
* #peo "Persian, Old (ca.600-400 B.C.)"
* #peo ^designation[0].language = #de-AT 
* #peo ^designation[0].value = "Altpersisch" 
* #per "Persian"
* #per ^designation[0].language = #de-AT 
* #per ^designation[0].value = "Persisch" 
* #per ^property[0].code = #hints 
* #per ^property[0].valueString = "fa" 
* #phi "Philippine languages"
* #phi ^designation[0].language = #de-AT 
* #phi ^designation[0].value = "Philippinisch-Austronesisch (Andere)" 
* #phn "Phoenician"
* #phn ^designation[0].language = #de-AT 
* #phn ^designation[0].value = "Phönikisch" 
* #pli "Pali"
* #pli ^designation[0].language = #de-AT 
* #pli ^designation[0].value = "Pali" 
* #pli ^property[0].code = #hints 
* #pli ^property[0].valueString = "pi" 
* #pol "Polish"
* #pol ^designation[0].language = #de-AT 
* #pol ^designation[0].value = "Polnisch" 
* #pol ^property[0].code = #hints 
* #pol ^property[0].valueString = "pl" 
* #pon "Pohnpeian"
* #pon ^designation[0].language = #de-AT 
* #pon ^designation[0].value = "Ponapeanisch" 
* #por "Portuguese"
* #por ^designation[0].language = #de-AT 
* #por ^designation[0].value = "Portugiesisch" 
* #por ^property[0].code = #hints 
* #por ^property[0].valueString = "pt" 
* #pra "Prakrit languages"
* #pra ^designation[0].language = #de-AT 
* #pra ^designation[0].value = "Prakrit" 
* #pro "Provençal, Old (to 1500);Occitan, Old (to 1500)"
* #pro ^designation[0].language = #de-AT 
* #pro ^designation[0].value = "Altokzitanisch" 
* #pus "Pushto; Pashto"
* #pus ^designation[0].language = #de-AT 
* #pus ^designation[0].value = "Paschtu" 
* #pus ^property[0].code = #hints 
* #pus ^property[0].valueString = "ps" 
* #qaa "Reserved for local use"
* #qaa ^designation[0].language = #de-AT 
* #qaa ^designation[0].value = "Reserviert für lokale Verwendung" 
* #que "Quechua"
* #que ^designation[0].language = #de-AT 
* #que ^designation[0].value = "Quechua-Sprache" 
* #que ^property[0].code = #hints 
* #que ^property[0].valueString = "qu" 
* #raj "Rajasthani"
* #raj ^designation[0].language = #de-AT 
* #raj ^designation[0].value = "Rajasthani" 
* #rap "Rapanui"
* #rap ^designation[0].language = #de-AT 
* #rap ^designation[0].value = "Osterinsel-Sprache" 
* #rar "Rarotongan; Cook Islands Maori"
* #rar ^designation[0].language = #de-AT 
* #rar ^designation[0].value = "Rarotonganisch" 
* #roa "Romance languages"
* #roa ^designation[0].language = #de-AT 
* #roa ^designation[0].value = "Romanische Sprachen (Andere)" 
* #roh "Romansh"
* #roh ^designation[0].language = #de-AT 
* #roh ^designation[0].value = "Rätoromanisch" 
* #roh ^property[0].code = #hints 
* #roh ^property[0].valueString = "rm" 
* #rom "Romany"
* #rom ^designation[0].language = #de-AT 
* #rom ^designation[0].value = "Romani (Sprache)" 
* #ron "Romanian; Moldavian; Moldovan"
* #ron ^designation[0].language = #de-AT 
* #ron ^designation[0].value = "Rumänisch" 
* #ron ^property[0].code = #hints 
* #ron ^property[0].valueString = "ro" 
* #rum "Romanian; Moldavian; Moldovan"
* #rum ^designation[0].language = #de-AT 
* #rum ^designation[0].value = "Rumänisch" 
* #rum ^property[0].code = #hints 
* #rum ^property[0].valueString = "ro" 
* #run "Rundi"
* #run ^designation[0].language = #de-AT 
* #run ^designation[0].value = "Rundi-Sprache" 
* #run ^property[0].code = #hints 
* #run ^property[0].valueString = "rn" 
* #rup "Aromanian; Arumanian; Macedo-Romanian"
* #rup ^designation[0].language = #de-AT 
* #rup ^designation[0].value = "Aromunisch" 
* #rus "Russian"
* #rus ^designation[0].language = #de-AT 
* #rus ^designation[0].value = "Russisch" 
* #rus ^property[0].code = #hints 
* #rus ^property[0].valueString = "ru" 
* #sad "Sandawe"
* #sad ^designation[0].language = #de-AT 
* #sad ^designation[0].value = "Sandawe-Sprache" 
* #sag "Sango"
* #sag ^designation[0].language = #de-AT 
* #sag ^designation[0].value = "Sango-Sprache" 
* #sag ^property[0].code = #hints 
* #sag ^property[0].valueString = "sg" 
* #sah "Yakut"
* #sah ^designation[0].language = #de-AT 
* #sah ^designation[0].value = "Jakutisch" 
* #sai "South American Indian languages"
* #sai ^designation[0].language = #de-AT 
* #sai ^designation[0].value = "Indianersprachen, Südamerika (Andere)" 
* #sal "Salishan languages"
* #sal ^designation[0].language = #de-AT 
* #sal ^designation[0].value = "Salish-Sprache" 
* #sam "Samaritan Aramaic"
* #sam ^designation[0].language = #de-AT 
* #sam ^designation[0].value = "Samaritanisch" 
* #san "Sanskrit"
* #san ^designation[0].language = #de-AT 
* #san ^designation[0].value = "Sanskrit" 
* #san ^property[0].code = #hints 
* #san ^property[0].valueString = "sa" 
* #sas "Sasak"
* #sas ^designation[0].language = #de-AT 
* #sas ^designation[0].value = "Sasak" 
* #sat "Santali"
* #sat ^designation[0].language = #de-AT 
* #sat ^designation[0].value = "Santali" 
* #scn "Sicilian"
* #scn ^designation[0].language = #de-AT 
* #scn ^designation[0].value = "Sizilianisch" 
* #sco "Scots"
* #sco ^designation[0].language = #de-AT 
* #sco ^designation[0].value = "Schottisch" 
* #sel "Selkup"
* #sel ^designation[0].language = #de-AT 
* #sel ^designation[0].value = "Selkupisch" 
* #sem "Semitic languages"
* #sem ^designation[0].language = #de-AT 
* #sem ^designation[0].value = "Semitische Sprachen (Andere)" 
* #sga "Irish, Old (to 900)"
* #sga ^designation[0].language = #de-AT 
* #sga ^designation[0].value = "Altirisch" 
* #sgn "Sign Languages"
* #sgn ^designation[0].language = #de-AT 
* #sgn ^designation[0].value = "Zeichensprachen" 
* #shn "Shan"
* #shn ^designation[0].language = #de-AT 
* #shn ^designation[0].value = "Schan-Sprache" 
* #sid "Sidamo"
* #sid ^designation[0].language = #de-AT 
* #sid ^designation[0].value = "Sidamo-Sprache" 
* #sin "Sinhala; Sinhalese"
* #sin ^designation[0].language = #de-AT 
* #sin ^designation[0].value = "Singhalesisch" 
* #sin ^property[0].code = #hints 
* #sin ^property[0].valueString = "si" 
* #sio "Siouan languages"
* #sio ^designation[0].language = #de-AT 
* #sio ^designation[0].value = "Sioux-Sprachen (Andere)" 
* #sit "Sino-Tibetan languages"
* #sit ^designation[0].language = #de-AT 
* #sit ^designation[0].value = "Sinotibetische Sprachen (Andere)" 
* #sla "Slavic languages"
* #sla ^designation[0].language = #de-AT 
* #sla ^designation[0].value = "Slawische Sprachen (Andere)" 
* #slk "Slovak"
* #slk ^designation[0].language = #de-AT 
* #slk ^designation[0].value = "Slowakisch" 
* #slk ^property[0].code = #hints 
* #slk ^property[0].valueString = "sk" 
* #slo "Slovak"
* #slo ^designation[0].language = #de-AT 
* #slo ^designation[0].value = "Slowakisch" 
* #slo ^property[0].code = #hints 
* #slo ^property[0].valueString = "sk" 
* #slv "Slovenian"
* #slv ^designation[0].language = #de-AT 
* #slv ^designation[0].value = "Slowenisch" 
* #slv ^property[0].code = #hints 
* #slv ^property[0].valueString = "sl" 
* #sma "Southern Sami"
* #sma ^designation[0].language = #de-AT 
* #sma ^designation[0].value = "Südsaamisch" 
* #sme "Northern Sami"
* #sme ^designation[0].language = #de-AT 
* #sme ^designation[0].value = "Nordsaamisch" 
* #sme ^property[0].code = #hints 
* #sme ^property[0].valueString = "se" 
* #smi "Sami languages"
* #smi ^designation[0].language = #de-AT 
* #smi ^designation[0].value = "Saamisch" 
* #smj "Lule Sami"
* #smj ^designation[0].language = #de-AT 
* #smj ^designation[0].value = "Lulesaamisch" 
* #smn "Inari Sami"
* #smn ^designation[0].language = #de-AT 
* #smn ^designation[0].value = "Inarisaamisch" 
* #smo "Samoan"
* #smo ^designation[0].language = #de-AT 
* #smo ^designation[0].value = "Samoanisch" 
* #smo ^property[0].code = #hints 
* #smo ^property[0].valueString = "sm" 
* #sms "Skolt Sami"
* #sms ^designation[0].language = #de-AT 
* #sms ^designation[0].value = "Skoltsaamisch" 
* #sna "Shona"
* #sna ^designation[0].language = #de-AT 
* #sna ^designation[0].value = "Schona-Sprache" 
* #sna ^property[0].code = #hints 
* #sna ^property[0].valueString = "sn" 
* #snd "Sindhi"
* #snd ^designation[0].language = #de-AT 
* #snd ^designation[0].value = "Sindhi-Sprache" 
* #snd ^property[0].code = #hints 
* #snd ^property[0].valueString = "sd" 
* #snk "Soninke"
* #snk ^designation[0].language = #de-AT 
* #snk ^designation[0].value = "Soninke-Sprache" 
* #sog "Sogdian"
* #sog ^designation[0].language = #de-AT 
* #sog ^designation[0].value = "Sogdisch" 
* #som "Somali"
* #som ^designation[0].language = #de-AT 
* #som ^designation[0].value = "Somali" 
* #som ^property[0].code = #hints 
* #som ^property[0].valueString = "so" 
* #son "Songhai languages"
* #son ^designation[0].language = #de-AT 
* #son ^designation[0].value = "Songhai-Sprache" 
* #sot "Sotho, Southern"
* #sot ^designation[0].language = #de-AT 
* #sot ^designation[0].value = "Süd-Sotho-Sprache" 
* #sot ^property[0].code = #hints 
* #sot ^property[0].valueString = "st" 
* #spa "Spanish; Castilian"
* #spa ^designation[0].language = #de-AT 
* #spa ^designation[0].value = "Spanisch" 
* #spa ^property[0].code = #hints 
* #spa ^property[0].valueString = "es" 
* #sqi "Albanian"
* #sqi ^designation[0].language = #de-AT 
* #sqi ^designation[0].value = "Albanisch" 
* #sqi ^property[0].code = #hints 
* #sqi ^property[0].valueString = "sq" 
* #srd "Sardinian"
* #srd ^designation[0].language = #de-AT 
* #srd ^designation[0].value = "Sardisch" 
* #srd ^property[0].code = #hints 
* #srd ^property[0].valueString = "sc" 
* #srn "Sranan Tongo"
* #srn ^designation[0].language = #de-AT 
* #srn ^designation[0].value = "Sranantongo" 
* #srp "Serbian"
* #srp ^designation[0].language = #de-AT 
* #srp ^designation[0].value = "Serbisch" 
* #srp ^property[0].code = #hints 
* #srp ^property[0].valueString = "sr" 
* #srr "Serer"
* #srr ^designation[0].language = #de-AT 
* #srr ^designation[0].value = "Serer-Sprache" 
* #ssa "Nilo-Saharan languages"
* #ssa ^designation[0].language = #de-AT 
* #ssa ^designation[0].value = "Nilosaharanische Sprachen (Andere)" 
* #ssw "Swati"
* #ssw ^designation[0].language = #de-AT 
* #ssw ^designation[0].value = "Swasi-Sprache" 
* #ssw ^property[0].code = #hints 
* #ssw ^property[0].valueString = "ss" 
* #suk "Sukuma"
* #suk ^designation[0].language = #de-AT 
* #suk ^designation[0].value = "Sukuma-Sprache" 
* #sun "Sundanese"
* #sun ^designation[0].language = #de-AT 
* #sun ^designation[0].value = "Sundanesisch" 
* #sun ^property[0].code = #hints 
* #sun ^property[0].valueString = "su" 
* #sus "Susu"
* #sus ^designation[0].language = #de-AT 
* #sus ^designation[0].value = "Susu" 
* #sux "Sumerian"
* #sux ^designation[0].language = #de-AT 
* #sux ^designation[0].value = "Sumerisch" 
* #swa "Swahili"
* #swa ^designation[0].language = #de-AT 
* #swa ^designation[0].value = "Swahili" 
* #swa ^property[0].code = #hints 
* #swa ^property[0].valueString = "sw" 
* #swe "Swedish"
* #swe ^designation[0].language = #de-AT 
* #swe ^designation[0].value = "Schwedisch" 
* #swe ^property[0].code = #hints 
* #swe ^property[0].valueString = "sv" 
* #syc "Classical Syriac"
* #syc ^designation[0].language = #de-AT 
* #syc ^designation[0].value = "Syrisch" 
* #syr "Syriac"
* #syr ^designation[0].language = #de-AT 
* #syr ^designation[0].value = "Neuostaramäisch" 
* #tah "Tahitian"
* #tah ^designation[0].language = #de-AT 
* #tah ^designation[0].value = "Tahitisch" 
* #tah ^property[0].code = #hints 
* #tah ^property[0].valueString = "ty" 
* #tai "Tai languages"
* #tai ^designation[0].language = #de-AT 
* #tai ^designation[0].value = "Thaisprachen (Andere)" 
* #tam "Tamil"
* #tam ^designation[0].language = #de-AT 
* #tam ^designation[0].value = "Tamil" 
* #tam ^property[0].code = #hints 
* #tam ^property[0].valueString = "ta" 
* #tat "Tatar"
* #tat ^designation[0].language = #de-AT 
* #tat ^designation[0].value = "Tatarisch" 
* #tat ^property[0].code = #hints 
* #tat ^property[0].valueString = "tt" 
* #tel "Telugu"
* #tel ^designation[0].language = #de-AT 
* #tel ^designation[0].value = "Telugu-Sprache" 
* #tel ^property[0].code = #hints 
* #tel ^property[0].valueString = "te" 
* #tem "Timne"
* #tem ^designation[0].language = #de-AT 
* #tem ^designation[0].value = "Temne-Sprache" 
* #ter "Tereno"
* #ter ^designation[0].language = #de-AT 
* #ter ^designation[0].value = "Tereno-Sprache" 
* #tet "Tetum"
* #tet ^designation[0].language = #de-AT 
* #tet ^designation[0].value = "Tetum-Sprache" 
* #tgk "Tajik"
* #tgk ^designation[0].language = #de-AT 
* #tgk ^designation[0].value = "Tadschikisch" 
* #tgk ^property[0].code = #hints 
* #tgk ^property[0].valueString = "tg" 
* #tgl "Tagalog"
* #tgl ^designation[0].language = #de-AT 
* #tgl ^designation[0].value = "Tagalog" 
* #tgl ^property[0].code = #hints 
* #tgl ^property[0].valueString = "tl" 
* #tha "Thai"
* #tha ^designation[0].language = #de-AT 
* #tha ^designation[0].value = "Thailändisch" 
* #tha ^property[0].code = #hints 
* #tha ^property[0].valueString = "th" 
* #tib "Tibetan"
* #tib ^designation[0].language = #de-AT 
* #tib ^designation[0].value = "Tibetisch" 
* #tib ^property[0].code = #hints 
* #tib ^property[0].valueString = "bo" 
* #tig "Tigre"
* #tig ^designation[0].language = #de-AT 
* #tig ^designation[0].value = "Tigre-Sprache" 
* #tir "Tigrinya"
* #tir ^designation[0].language = #de-AT 
* #tir ^designation[0].value = "Tigrinja-Sprache" 
* #tir ^property[0].code = #hints 
* #tir ^property[0].valueString = "ti" 
* #tiv "Tiv"
* #tiv ^designation[0].language = #de-AT 
* #tiv ^designation[0].value = "Tiv-Sprache" 
* #tkl "Tokelau"
* #tkl ^designation[0].language = #de-AT 
* #tkl ^designation[0].value = "Tokelauanisch" 
* #tlh "Klingon; tlhIngan-Hol"
* #tlh ^designation[0].language = #de-AT 
* #tlh ^designation[0].value = "Klingonisch" 
* #tli "Tlingit"
* #tli ^designation[0].language = #de-AT 
* #tli ^designation[0].value = "Tlingit-Sprache" 
* #tmh "Tamashek"
* #tmh ^designation[0].language = #de-AT 
* #tmh ^designation[0].value = "Tamaeq" 
* #tog "Tonga (Nyasa)"
* #tog ^designation[0].language = #de-AT 
* #tog ^designation[0].value = "Tonga (Bantusprache, Sambia)" 
* #ton "Tonga (Tonga Islands)"
* #ton ^designation[0].language = #de-AT 
* #ton ^designation[0].value = "Tongaisch" 
* #ton ^property[0].code = #hints 
* #ton ^property[0].valueString = "to" 
* #tpi "Tok Pisin"
* #tpi ^designation[0].language = #de-AT 
* #tpi ^designation[0].value = "Neumelanesisch" 
* #tsi "Tsimshian"
* #tsi ^designation[0].language = #de-AT 
* #tsi ^designation[0].value = "Tsimshian-Sprache" 
* #tsn "Tswana"
* #tsn ^designation[0].language = #de-AT 
* #tsn ^designation[0].value = "Tswana-Sprache" 
* #tsn ^property[0].code = #hints 
* #tsn ^property[0].valueString = "tn" 
* #tso "Tsonga"
* #tso ^designation[0].language = #de-AT 
* #tso ^designation[0].value = "Tsonga-Sprache" 
* #tso ^property[0].code = #hints 
* #tso ^property[0].valueString = "ts" 
* #tuk "Turkmen"
* #tuk ^designation[0].language = #de-AT 
* #tuk ^designation[0].value = "Turkmenisch" 
* #tuk ^property[0].code = #hints 
* #tuk ^property[0].valueString = "tk" 
* #tum "Tumbuka"
* #tum ^designation[0].language = #de-AT 
* #tum ^designation[0].value = "Tumbuka-Sprache" 
* #tup "Tupi languages"
* #tup ^designation[0].language = #de-AT 
* #tup ^designation[0].value = "Tupi-Sprache" 
* #tur "Turkish"
* #tur ^designation[0].language = #de-AT 
* #tur ^designation[0].value = "Türkisch" 
* #tur ^property[0].code = #hints 
* #tur ^property[0].valueString = "tr" 
* #tut "Altaic languages"
* #tut ^designation[0].language = #de-AT 
* #tut ^designation[0].value = "Altaische Sprachen (Andere)" 
* #tvl "Tuvalu"
* #tvl ^designation[0].language = #de-AT 
* #tvl ^designation[0].value = "Elliceanisch" 
* #twi "Twi"
* #twi ^designation[0].language = #de-AT 
* #twi ^designation[0].value = "Twi-Sprache" 
* #twi ^property[0].code = #hints 
* #twi ^property[0].valueString = "tw" 
* #tyv "Tuvinian"
* #tyv ^designation[0].language = #de-AT 
* #tyv ^designation[0].value = "Tuwinisch" 
* #udm "Udmurt"
* #udm ^designation[0].language = #de-AT 
* #udm ^designation[0].value = "Udmurtisch" 
* #uga "Ugaritic"
* #uga ^designation[0].language = #de-AT 
* #uga ^designation[0].value = "Ugaritisch" 
* #uig "Uighur; Uyghur"
* #uig ^designation[0].language = #de-AT 
* #uig ^designation[0].value = "Uigurisch" 
* #uig ^property[0].code = #hints 
* #uig ^property[0].valueString = "ug" 
* #ukr "Ukrainian"
* #ukr ^designation[0].language = #de-AT 
* #ukr ^designation[0].value = "Ukrainisch" 
* #ukr ^property[0].code = #hints 
* #ukr ^property[0].valueString = "uk" 
* #umb "Umbundu"
* #umb ^designation[0].language = #de-AT 
* #umb ^designation[0].value = "Mbundu-Sprache" 
* #und "Undetermined"
* #und ^designation[0].language = #de-AT 
* #und ^designation[0].value = "Nicht zu entscheiden" 
* #urd "Urdu"
* #urd ^designation[0].language = #de-AT 
* #urd ^designation[0].value = "Urdu" 
* #urd ^property[0].code = #hints 
* #urd ^property[0].valueString = "ur" 
* #uzb "Uzbek"
* #uzb ^designation[0].language = #de-AT 
* #uzb ^designation[0].value = "Usbekisch" 
* #uzb ^property[0].code = #hints 
* #uzb ^property[0].valueString = "uz" 
* #vai "Vai"
* #vai ^designation[0].language = #de-AT 
* #vai ^designation[0].value = "Vai-Sprache" 
* #ven "Venda"
* #ven ^designation[0].language = #de-AT 
* #ven ^designation[0].value = "Venda-Sprache" 
* #ven ^property[0].code = #hints 
* #ven ^property[0].valueString = "ve" 
* #vie "Vietnamese"
* #vie ^designation[0].language = #de-AT 
* #vie ^designation[0].value = "Vietnamesisch" 
* #vie ^property[0].code = #hints 
* #vie ^property[0].valueString = "vi" 
* #vol "Volapük"
* #vol ^designation[0].language = #de-AT 
* #vol ^designation[0].value = "Volapük" 
* #vol ^property[0].code = #hints 
* #vol ^property[0].valueString = "vo" 
* #vot "Votic"
* #vot ^designation[0].language = #de-AT 
* #vot ^designation[0].value = "Wotisch" 
* #wak "Wakashan languages"
* #wak ^designation[0].language = #de-AT 
* #wak ^designation[0].value = "Wakash-Sprachen" 
* #wal "Wolaitta; Wolaytta"
* #wal ^designation[0].language = #de-AT 
* #wal ^designation[0].value = "Walamo-Sprache" 
* #war "Waray"
* #war ^designation[0].language = #de-AT 
* #war ^designation[0].value = "Waray" 
* #was "Washo"
* #was ^designation[0].language = #de-AT 
* #was ^designation[0].value = "Washo-Sprache" 
* #wel "Welsh"
* #wel ^designation[0].language = #de-AT 
* #wel ^designation[0].value = "Kymrisch" 
* #wel ^property[0].code = #hints 
* #wel ^property[0].valueString = "cy" 
* #wen "Sorbian languages"
* #wen ^designation[0].language = #de-AT 
* #wen ^designation[0].value = "Sorbisch (Andere)" 
* #wln "Walloon"
* #wln ^designation[0].language = #de-AT 
* #wln ^designation[0].value = "Wallonisch" 
* #wln ^property[0].code = #hints 
* #wln ^property[0].valueString = "wa" 
* #wol "Wolof"
* #wol ^designation[0].language = #de-AT 
* #wol ^designation[0].value = "Wolof-Sprache" 
* #wol ^property[0].code = #hints 
* #wol ^property[0].valueString = "wo" 
* #xal "Kalmyk; Oirat"
* #xal ^designation[0].language = #de-AT 
* #xal ^designation[0].value = "Kalmückisch" 
* #xho "Xhosa"
* #xho ^designation[0].language = #de-AT 
* #xho ^designation[0].value = "Xhosa-Sprache" 
* #xho ^property[0].code = #hints 
* #xho ^property[0].valueString = "xh" 
* #yao "Yao"
* #yao ^designation[0].language = #de-AT 
* #yao ^designation[0].value = "Yao-Sprache (Bantusprache)" 
* #yap "Yapese"
* #yap ^designation[0].language = #de-AT 
* #yap ^designation[0].value = "Yapesisch" 
* #yid "Yiddish"
* #yid ^designation[0].language = #de-AT 
* #yid ^designation[0].value = "Jiddisch" 
* #yid ^property[0].code = #hints 
* #yid ^property[0].valueString = "yi" 
* #yor "Yoruba"
* #yor ^designation[0].language = #de-AT 
* #yor ^designation[0].value = "Yoruba-Sprache" 
* #yor ^property[0].code = #hints 
* #yor ^property[0].valueString = "yo" 
* #ypk "Yupik languages"
* #ypk ^designation[0].language = #de-AT 
* #ypk ^designation[0].value = "Ypik-Sprachen" 
* #zap "Zapotec"
* #zap ^designation[0].language = #de-AT 
* #zap ^designation[0].value = "Zapotekisch" 
* #zbl "Blissymbols; Blissymbolics; Bliss"
* #zbl ^designation[0].language = #de-AT 
* #zbl ^designation[0].value = "Bliss-Symbol" 
* #zen "Zenaga"
* #zen ^designation[0].language = #de-AT 
* #zen ^designation[0].value = "Zenaga" 
* #zgh "Standard Moroccan Tamazight"
* #zgh ^designation[0].language = #de-AT 
* #zgh ^designation[0].value = "Standard Moroccan Tamazight" 
* #zha "Zhuang; Chuang"
* #zha ^designation[0].language = #de-AT 
* #zha ^designation[0].value = "Zhuang" 
* #zha ^property[0].code = #hints 
* #zha ^property[0].valueString = "za" 
* #zho "Chinese"
* #zho ^designation[0].language = #de-AT 
* #zho ^designation[0].value = "Chinesisch" 
* #zho ^property[0].code = #hints 
* #zho ^property[0].valueString = "zh" 
* #znd "Zande languages"
* #znd ^designation[0].language = #de-AT 
* #znd ^designation[0].value = "Zande-Sprachen" 
* #zul "Zulu"
* #zul ^designation[0].language = #de-AT 
* #zul ^designation[0].value = "Zulu-Sprache" 
* #zul ^property[0].code = #hints 
* #zul ^property[0].valueString = "zu" 
* #zun "Zuni"
* #zun ^designation[0].language = #de-AT 
* #zun ^designation[0].value = "Zuñi-Sprache" 
* #zxx "No linguistic content; Not applicable"
* #zxx ^designation[0].language = #de-AT 
* #zxx ^designation[0].value = "Kein linguistischer Inhalt" 
* #zza "Zaza; Dimili; Dimli; Kirdki; Kirmanjki; Zazaki"
* #zza ^designation[0].language = #de-AT 
* #zza ^designation[0].value = "Zazaki" 
