Instance: lkf-diagnose-statauf 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-lkf-diagnose-statauf" 
* name = "lkf-diagnose-statauf" 
* title = "LKF_Diagnose-statAuf" 
* status = #active 
* version = "202107" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.68" 
* date = "2021-07-23" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-lkf-diagnose-statauf"
* compose.include[0].concept[0].code = "J"
* compose.include[0].concept[0].display = "Ja"
* compose.include[0].concept[1].code = "N"
* compose.include[0].concept[1].display = "Nein"
