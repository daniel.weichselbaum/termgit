Instance: rast-klassen 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-rast-klassen" 
* name = "rast-klassen" 
* title = "RAST-Klassen" 
* status = #active 
* content = #complete 
* version = "4.0" 
* description = "**Description:** Allergo-Sorbent-Test Score (RAST-Score)

**Beschreibung:** Radio-Allergo-Sorbent-Test Klassen (RAST-Klassen)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.104" 
* date = "2018-06-21" 
* count = 8 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* property[2].code = #hints 
* property[2].type = #string 
* #_RAST "RAST"
* #_RAST ^property[0].code = #child 
* #_RAST ^property[0].valueCode = #RAST0 
* #_RAST ^property[1].code = #child 
* #_RAST ^property[1].valueCode = #RAST1 
* #_RAST ^property[2].code = #child 
* #_RAST ^property[2].valueCode = #RAST2 
* #_RAST ^property[3].code = #child 
* #_RAST ^property[3].valueCode = #RAST3 
* #_RAST ^property[4].code = #child 
* #_RAST ^property[4].valueCode = #RAST4 
* #_RAST ^property[5].code = #child 
* #_RAST ^property[5].valueCode = #RAST5 
* #_RAST ^property[6].code = #child 
* #_RAST ^property[6].valueCode = #RAST6 
* #RAST0 "< 0.35  kU/l (absent or undetectable allergen specific IgE)"
* #RAST0 ^definition = < 0.35  kU/l (absent or undetectable allergen specific IgE)
* #RAST0 ^designation[0].language = #de-AT 
* #RAST0 ^designation[0].value = "negativ, keine spezifischen IgE-Antikörper nachweisbar" 
* #RAST0 ^property[0].code = #parent 
* #RAST0 ^property[0].valueCode = #_RAST 
* #RAST0 ^property[1].code = #hints 
* #RAST0 ^property[1].valueString = "RAST 0" 
* #RAST1 "0.35 - 0.69  kU/l (low level of allergen specific IgE)"
* #RAST1 ^definition = 0.35 - 0.69  kU/l (low level of allergen specific IgE)
* #RAST1 ^designation[0].language = #de-AT 
* #RAST1 ^designation[0].value = "schwach positiv, geringe Konzentration spezifischer IgE-AK nachweisbar" 
* #RAST1 ^property[0].code = #parent 
* #RAST1 ^property[0].valueCode = #_RAST 
* #RAST1 ^property[1].code = #hints 
* #RAST1 ^property[1].valueString = "RAST 1" 
* #RAST2 "0.70 - 3.49  kU/l (moderate level of allergen specific IgE)"
* #RAST2 ^definition = 0.70 - 3.49  kU/l (moderate level of allergen specific IgE)
* #RAST2 ^designation[0].language = #de-AT 
* #RAST2 ^designation[0].value = "positiv" 
* #RAST2 ^property[0].code = #parent 
* #RAST2 ^property[0].valueCode = #_RAST 
* #RAST2 ^property[1].code = #hints 
* #RAST2 ^property[1].valueString = "RAST 2" 
* #RAST3 "3.50 - 17.49  kU/l (high level of allergen specific IgE)"
* #RAST3 ^definition = 3.50 - 17.49  kU/l (high level of allergen specific IgE)
* #RAST3 ^designation[0].language = #de-AT 
* #RAST3 ^designation[0].value = "deutlich positiv" 
* #RAST3 ^property[0].code = #parent 
* #RAST3 ^property[0].valueCode = #_RAST 
* #RAST3 ^property[1].code = #hints 
* #RAST3 ^property[1].valueString = "RAST 3" 
* #RAST4 "17.50 - 49.99  kU/l (very high level of allergen specific IgE)"
* #RAST4 ^definition = 17.50 - 49.99  kU/l (very high level of allergen specific IgE)
* #RAST4 ^designation[0].language = #de-AT 
* #RAST4 ^designation[0].value = "stark positiv, hohe Konzentration spezifischer IgE-AK nachweisbar" 
* #RAST4 ^property[0].code = #parent 
* #RAST4 ^property[0].valueCode = #_RAST 
* #RAST4 ^property[1].code = #hints 
* #RAST4 ^property[1].valueString = "RAST 4" 
* #RAST5 "50.00 - 100.00  kU/l (ultra high level of allergen specific IgE)"
* #RAST5 ^definition = 50.00 - 100.00  kU/l (ultra high level of allergen specific IgE)
* #RAST5 ^designation[0].language = #de-AT 
* #RAST5 ^designation[0].value = "sehr stark positiv, sehr hohe Konzentration spezifischer IgE-AK nachweisbar" 
* #RAST5 ^property[0].code = #parent 
* #RAST5 ^property[0].valueCode = #_RAST 
* #RAST5 ^property[1].code = #hints 
* #RAST5 ^property[1].valueString = "RAST 5" 
* #RAST6 "> 100.00  kU/l (extremely high level of allergen specific ige)"
* #RAST6 ^definition = > 100.00  kU/l (extremely high level of allergen specific ige)
* #RAST6 ^designation[0].language = #de-AT 
* #RAST6 ^designation[0].value = "hochgradig positiv, extrem hohe Konzentration spezifischer IgE-AK nachweisbar" 
* #RAST6 ^property[0].code = #parent 
* #RAST6 ^property[0].valueCode = #_RAST 
* #RAST6 ^property[1].code = #hints 
* #RAST6 ^property[1].valueString = "RAST 6" 
