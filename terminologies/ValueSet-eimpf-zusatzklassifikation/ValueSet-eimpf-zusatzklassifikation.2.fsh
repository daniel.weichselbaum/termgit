Instance: eimpf-zusatzklassifikation 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-eimpf-zusatzklassifikation" 
* name = "eimpf-zusatzklassifikation" 
* title = "eImpf_Zusatzklassifikation" 
* status = #active 
* version = "202110" 
* description = "**Description:** Contains information protected by copyright of SNOMED International. Any use of SNOMED CT in Austria requires a valid affiliate license or sublicense. The corresponding license is free of charge, provided that the use only takes place in Austria and fulfills the conditions of the Affiliate License Agreement. Affiliate licenses can be requested directly from the respective NRC via the [Member Licensing and Distribution Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS).

**Beschreibung:** Enthält durch SNOMED International urheberrechtlich geschützte Information. Jede Verwendung von SNOMED CT in Österreich erfordert eine aufrechte Affiliate Lizenz oder eine Sublizenz. Die entsprechende Lizenz ist kostenlos, vorausgesetzt die Verwendung findet nur in Österreich statt und erfüllt die Bedingungen des Affiliate License Agreements. Affiliate Lizenzen können über das [Mitglieder-Lizenzierungs- und Distributions-Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS) direkt beim jeweiligen NRC beantragt werden." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.62" 
* date = "2021-10-04" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* compose.include[0].concept[0].code = "46224007"
* compose.include[0].concept[0].display = "Impfstelle (Impfsetting)"
* compose.include[1].system = "https://termgit.elga.gv.at/CodeSystem-eimpf-ergaenzung"
* compose.include[1].concept[0].code = "IS001"
* compose.include[1].concept[0].display = "Bildungseinrichtung"
* compose.include[1].concept[1].code = "IS002"
* compose.include[1].concept[1].display = "Arbeitsplatz/Betriebe"
* compose.include[1].concept[2].code = "IS003"
* compose.include[1].concept[2].display = "Krankenhaus inkl. Kur- und Rehaeinrichtungen"
* compose.include[1].concept[3].code = "IS004"
* compose.include[1].concept[3].display = "Ordination"
* compose.include[1].concept[4].code = "IS005"
* compose.include[1].concept[4].display = "Öffentliche Impfstelle/Impfstraße"
* compose.include[1].concept[5].code = "IS006"
* compose.include[1].concept[5].display = "Wohnbereich und Betreuungseinrichtungen"
