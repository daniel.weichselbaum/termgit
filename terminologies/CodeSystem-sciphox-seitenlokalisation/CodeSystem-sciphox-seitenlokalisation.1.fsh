Instance: sciphox-seitenlokalisation 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-sciphox-seitenlokalisation" 
* name = "sciphox-seitenlokalisation" 
* title = "Sciphox_Seitenlokalisation" 
* status = #active 
* content = #complete 
* description = "**Beschreibung:** Vocabulary Domain für Seitenlokalisation" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.3.7.1.7" 
* date = "2016-02-17" 
* count = 4 
* concept[0].code = #B
* concept[0].display = "beidseits"
* concept[0].designation[0].language = #de-AT 
* concept[0].designation[0].value = "beidseits" 
* concept[1].code = #L
* concept[1].display = "links"
* concept[1].designation[0].language = #de-AT 
* concept[1].designation[0].value = "links" 
* concept[2].code = #M
* concept[2].display = "Mittellinienzone"
* concept[2].designation[0].language = #de-AT 
* concept[2].designation[0].value = "Mittellinienzone" 
* concept[3].code = #R
* concept[3].display = "rechts"
* concept[3].designation[0].language = #de-AT 
* concept[3].designation[0].value = "rechts" 
