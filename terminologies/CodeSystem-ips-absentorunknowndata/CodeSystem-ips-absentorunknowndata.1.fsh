Instance: ips-absentorunknowndata 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ips-absentorunknowndata" 
* name = "ips-absentorunknowndata" 
* title = "IPS_AbsentOrUnknownData" 
* status = #active 
* content = #complete 
* version = "202002" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.5.1150.1" 
* date = "2020-02-04" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* count = 2 
* concept[0].code = #no-allergy-info
* concept[0].display = "keine Information über Überempfindlichkeiten verfügbar"
* concept[1].code = #no-known-allergies
* concept[1].display = "keine Überempfindlichkeiten"
