Instance: elga-informationrecipienttype 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-informationrecipienttype" 
* name = "elga-informationrecipienttype" 
* title = "ELGA_InformationRecipientType" 
* status = #active 
* version = "2.8" 
* description = "**Description:** Describes the kinds of information recipients

**Beschreibung:** Beschreibt die Arten von Informationsempfängern " 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.29" 
* date = "2014-09-01" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.hl7.org" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-hl7-participation-type"
* compose.include[0].concept[0].code = #PRCP
* compose.include[0].concept[0].display = "primary information recipient"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "EmpfängerIn Primär" 
* compose.include[0].concept[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[0].designation[1].value = "Information recipient to whom an act statement is primarily directed. E.g., a primary care provider receiving a discharge letter from a hospitalist, a health department receiving information on a suspected case of infectious disease. Multiple of these participations may exist on the same act without requiring that recipients be ranked as primary vs. secondary." 
* compose.include[0].concept[1].code = #TRC
* compose.include[0].concept[1].display = "tracker"
* compose.include[0].concept[1].designation[0].language = #de-AT 
* compose.include[0].concept[1].designation[0].value = "EmpfängerIn Sekundär" 
* compose.include[0].concept[1].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[1].designation[1].value = "A secondary information recipient, who receives copies (e.g., a primary care provider receiving copies of results as ordered by specialist)." 

* expansion.timestamp = "2022-09-13T14:16:57.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem-hl7-participation-type"
* expansion.contains[0].code = #PRCP
* expansion.contains[0].display = "primary information recipient"
* expansion.contains[0].designation[0].language = #de-AT 
* expansion.contains[0].designation[0].value = "EmpfängerIn Primär" 
* expansion.contains[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[0].designation[1].value = "Information recipient to whom an act statement is primarily directed. E.g., a primary care provider receiving a discharge letter from a hospitalist, a health department receiving information on a suspected case of infectious disease. Multiple of these participations may exist on the same act without requiring that recipients be ranked as primary vs. secondary." 
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem-hl7-participation-type"
* expansion.contains[1].code = #TRC
* expansion.contains[1].display = "tracker"
* expansion.contains[1].designation[0].language = #de-AT 
* expansion.contains[1].designation[0].value = "EmpfängerIn Sekundär" 
* expansion.contains[1].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[1].designation[1].value = "A secondary information recipient, who receives copies (e.g., a primary care provider receiving copies of results as ordered by specialist)." 
