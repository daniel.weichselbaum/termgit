Instance: lkf-diagnose-art 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-lkf-diagnose-art" 
* name = "lkf-diagnose-art" 
* title = "LKF_Diagnose-Art" 
* status = #active 
* version = "202107" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.67" 
* date = "2021-07-30" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-lkf-diagnose-art"
* compose.include[0].concept[0].code = #D
* compose.include[0].concept[0].display = "Aktuelle/Behandelte Diagnose"
* compose.include[0].concept[1].code = #V
* compose.include[0].concept[1].display = "Verdachtsdiagnose"

* expansion.timestamp = "2022-09-13T14:16:42.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem-lkf-diagnose-art"
* expansion.contains[0].code = #D
* expansion.contains[0].display = "Aktuelle/Behandelte Diagnose"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem-lkf-diagnose-art"
* expansion.contains[1].code = #V
* expansion.contains[1].display = "Verdachtsdiagnose"
