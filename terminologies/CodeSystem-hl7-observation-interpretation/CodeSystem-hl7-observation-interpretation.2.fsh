Instance: hl7-observation-interpretation 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-observation-interpretation" 
* name = "hl7-observation-interpretation" 
* title = "HL7 Observation Interpretation" 
* status = #active 
* content = #complete 
* version = "HL7 ObservationInterpretation" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.5.83" 
* date = "2015-01-10" 
* count = 51 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #EX "outsidethreshold"
* #EX ^property[0].code = #child 
* #EX ^property[0].valueCode = #HX 
* #EX ^property[1].code = #child 
* #EX ^property[1].valueCode = #LX 
* #HX "abovehighthreshold"
* #HX ^property[0].code = #parent 
* #HX ^property[0].valueCode = #EX 
* #LX "belowlowthreshold"
* #LX ^property[0].code = #parent 
* #LX ^property[0].valueCode = #EX 
* #ObservationInterpretationDetection "ObservationInterpretationDetection"
* #ObservationInterpretationDetection ^property[0].code = #child 
* #ObservationInterpretationDetection ^property[0].valueCode = #IND 
* #ObservationInterpretationDetection ^property[1].code = #child 
* #ObservationInterpretationDetection ^property[1].valueCode = #NEG 
* #ObservationInterpretationDetection ^property[2].code = #child 
* #ObservationInterpretationDetection ^property[2].valueCode = #POS 
* #IND "Indeterminate"
* #IND ^property[0].code = #parent 
* #IND ^property[0].valueCode = #ObservationInterpretationDetection 
* #NEG "Negative"
* #NEG ^property[0].code = #parent 
* #NEG ^property[0].valueCode = #ObservationInterpretationDetection 
* #NEG ^property[1].code = #child 
* #NEG ^property[1].valueCode = #ND 
* #ND "Notdetected"
* #ND ^property[0].code = #parent 
* #ND ^property[0].valueCode = #NEG 
* #POS "Positive"
* #POS ^property[0].code = #parent 
* #POS ^property[0].valueCode = #ObservationInterpretationDetection 
* #POS ^property[1].code = #child 
* #POS ^property[1].valueCode = #DET 
* #DET "Detected"
* #DET ^property[0].code = #parent 
* #DET ^property[0].valueCode = #POS 
* #ReactivityObservationInterpretation "ReactivityObservationInterpretation"
* #ReactivityObservationInterpretation ^property[0].code = #child 
* #ReactivityObservationInterpretation ^property[0].valueCode = #NR 
* #ReactivityObservationInterpretation ^property[1].code = #child 
* #ReactivityObservationInterpretation ^property[1].valueCode = #RR 
* #NR "Non-reactive"
* #NR ^property[0].code = #parent 
* #NR ^property[0].valueCode = #ReactivityObservationInterpretation 
* #RR "Reactive"
* #RR ^property[0].code = #parent 
* #RR ^property[0].valueCode = #ReactivityObservationInterpretation 
* #RR ^property[1].code = #child 
* #RR ^property[1].valueCode = #WR 
* #WR "Weaklyreactive"
* #WR ^property[0].code = #parent 
* #WR ^property[0].valueCode = #RR 
* #_GeneticObservationInterpretation "GeneticObservationInterpretation"
* #_GeneticObservationInterpretation ^property[0].code = #child 
* #_GeneticObservationInterpretation ^property[0].valueCode = #CAR 
* #_GeneticObservationInterpretation ^property[1].code = #child 
* #_GeneticObservationInterpretation ^property[1].valueCode = #Carrier 
* #CAR "Carrier"
* #CAR ^property[0].code = #parent 
* #CAR ^property[0].valueCode = #_GeneticObservationInterpretation 
* #Carrier "Carrier"
* #Carrier ^property[0].code = #parent 
* #Carrier ^property[0].valueCode = #_GeneticObservationInterpretation 
* #_ObservationInterpretationChange "ObservationInterpretationChange"
* #_ObservationInterpretationChange ^property[0].code = #child 
* #_ObservationInterpretationChange ^property[0].valueCode = #B 
* #_ObservationInterpretationChange ^property[1].code = #child 
* #_ObservationInterpretationChange ^property[1].valueCode = #D 
* #_ObservationInterpretationChange ^property[2].code = #child 
* #_ObservationInterpretationChange ^property[2].valueCode = #U 
* #_ObservationInterpretationChange ^property[3].code = #child 
* #_ObservationInterpretationChange ^property[3].valueCode = #W 
* #B "Better"
* #B ^property[0].code = #parent 
* #B ^property[0].valueCode = #_ObservationInterpretationChange 
* #D "Significantchangedown"
* #D ^property[0].code = #parent 
* #D ^property[0].valueCode = #_ObservationInterpretationChange 
* #U "Significantchangeup"
* #U ^property[0].code = #parent 
* #U ^property[0].valueCode = #_ObservationInterpretationChange 
* #W "Worse"
* #W ^property[0].code = #parent 
* #W ^property[0].valueCode = #_ObservationInterpretationChange 
* #_ObservationInterpretationExceptions "ObservationInterpretationExceptions"
* #_ObservationInterpretationExceptions ^property[0].code = #child 
* #_ObservationInterpretationExceptions ^property[0].valueCode = #< 
* #_ObservationInterpretationExceptions ^property[1].code = #child 
* #_ObservationInterpretationExceptions ^property[1].valueCode = #> 
* #_ObservationInterpretationExceptions ^property[2].code = #child 
* #_ObservationInterpretationExceptions ^property[2].valueCode = #AC 
* #_ObservationInterpretationExceptions ^property[3].code = #child 
* #_ObservationInterpretationExceptions ^property[3].valueCode = #IE 
* #_ObservationInterpretationExceptions ^property[4].code = #child 
* #_ObservationInterpretationExceptions ^property[4].valueCode = #QCF 
* #_ObservationInterpretationExceptions ^property[5].code = #child 
* #_ObservationInterpretationExceptions ^property[5].valueCode = #TOX 
* #< "Offscalelow"
* #< ^property[0].code = #parent 
* #< ^property[0].valueCode = #_ObservationInterpretationExceptions 
* #> "Offscalehigh"
* #> ^property[0].code = #parent 
* #> ^property[0].valueCode = #_ObservationInterpretationExceptions 
* #AC "Anti-complementarysubstancespresent"
* #AC ^property[0].code = #parent 
* #AC ^property[0].valueCode = #_ObservationInterpretationExceptions 
* #IE "Insufficientevidence"
* #IE ^property[0].code = #parent 
* #IE ^property[0].valueCode = #_ObservationInterpretationSusceptibility 
* #QCF "Qualitycontrolfailure"
* #QCF ^property[0].code = #parent 
* #QCF ^property[0].valueCode = #_ObservationInterpretationExceptions 
* #TOX "Cytotoxicsubstancepresent"
* #TOX ^property[0].code = #parent 
* #TOX ^property[0].valueCode = #_ObservationInterpretationExceptions 
* #_ObservationInterpretationNormality "ObservationInterpretationNormality"
* #_ObservationInterpretationNormality ^property[0].code = #child 
* #_ObservationInterpretationNormality ^property[0].valueCode = #A 
* #_ObservationInterpretationNormality ^property[1].code = #child 
* #_ObservationInterpretationNormality ^property[1].valueCode = #N 
* #A "Abnormal"
* #A ^property[0].code = #parent 
* #A ^property[0].valueCode = #_ObservationInterpretationNormality 
* #A ^property[1].code = #child 
* #A ^property[1].valueCode = #AA 
* #A ^property[2].code = #child 
* #A ^property[2].valueCode = #H 
* #A ^property[3].code = #child 
* #A ^property[3].valueCode = #L 
* #AA "Criticallyabnormal"
* #AA ^property[0].code = #parent 
* #AA ^property[0].valueCode = #A 
* #AA ^property[1].code = #child 
* #AA ^property[1].valueCode = #HH 
* #AA ^property[2].code = #child 
* #AA ^property[2].valueCode = #LL 
* #HH "Criticallyhigh"
* #HH ^property[0].code = #parent 
* #HH ^property[0].valueCode = #H> 
* #LL "Criticallylow"
* #LL ^property[0].code = #parent 
* #LL ^property[0].valueCode = #L> 
* #H "High"
* #H ^property[0].code = #parent 
* #H ^property[0].valueCode = #A 
* #H ^property[1].code = #child 
* #H ^property[1].valueCode = #H> 
* #H> "Veryhigh"
* #H> ^property[0].code = #parent 
* #H> ^property[0].valueCode = #H 
* #H> ^property[1].code = #child 
* #H> ^property[1].valueCode = #HH 
* #L "Low"
* #L ^property[0].code = #parent 
* #L ^property[0].valueCode = #A 
* #L ^property[1].code = #child 
* #L ^property[1].valueCode = #L> 
* #L> "Verylow"
* #L> ^property[0].code = #parent 
* #L> ^property[0].valueCode = #L 
* #L> ^property[1].code = #child 
* #L> ^property[1].valueCode = #LL 
* #N "Normal"
* #N ^property[0].code = #parent 
* #N ^property[0].valueCode = #_ObservationInterpretationNormality 
* #_ObservationInterpretationSusceptibility "ObservationInterpretationSusceptibility"
* #_ObservationInterpretationSusceptibility ^property[0].code = #child 
* #_ObservationInterpretationSusceptibility ^property[0].valueCode = #I 
* #_ObservationInterpretationSusceptibility ^property[1].code = #child 
* #_ObservationInterpretationSusceptibility ^property[1].valueCode = #IE 
* #_ObservationInterpretationSusceptibility ^property[2].code = #child 
* #_ObservationInterpretationSusceptibility ^property[2].valueCode = #MS 
* #_ObservationInterpretationSusceptibility ^property[3].code = #child 
* #_ObservationInterpretationSusceptibility ^property[3].valueCode = #NS 
* #_ObservationInterpretationSusceptibility ^property[4].code = #child 
* #_ObservationInterpretationSusceptibility ^property[4].valueCode = #R 
* #_ObservationInterpretationSusceptibility ^property[5].code = #child 
* #_ObservationInterpretationSusceptibility ^property[5].valueCode = #S 
* #_ObservationInterpretationSusceptibility ^property[6].code = #child 
* #_ObservationInterpretationSusceptibility ^property[6].valueCode = #VS 
* #I "Intermediate"
* #I ^property[0].code = #parent 
* #I ^property[0].valueCode = #_ObservationInterpretationSusceptibility 
* #MS "moderatelysusceptible"
* #MS ^property[0].code = #parent 
* #MS ^property[0].valueCode = #_ObservationInterpretationSusceptibility 
* #NS "Non-susceptible"
* #NS ^property[0].code = #parent 
* #NS ^property[0].valueCode = #_ObservationInterpretationSusceptibility 
* #R "Resistant"
* #R ^property[0].code = #parent 
* #R ^property[0].valueCode = #_ObservationInterpretationSusceptibility 
* #R ^property[1].code = #child 
* #R ^property[1].valueCode = #SYN-R 
* #SYN-R "Synergy-resistant"
* #SYN-R ^property[0].code = #parent 
* #SYN-R ^property[0].valueCode = #R 
* #S "Susceptible"
* #S ^property[0].code = #parent 
* #S ^property[0].valueCode = #_ObservationInterpretationSusceptibility 
* #S ^property[1].code = #child 
* #S ^property[1].valueCode = #SDD 
* #S ^property[2].code = #child 
* #S ^property[2].valueCode = #SYN-S 
* #SDD "Susceptible-dosedependent"
* #SDD ^property[0].code = #parent 
* #SDD ^property[0].valueCode = #S 
* #SYN-S "Synergy-susceptible"
* #SYN-S ^property[0].code = #parent 
* #SYN-S ^property[0].valueCode = #S 
* #VS "verysusceptible"
* #VS ^property[0].code = #parent 
* #VS ^property[0].valueCode = #_ObservationInterpretationSusceptibility 
