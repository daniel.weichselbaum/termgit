Instance: exnds-sections 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-exnds-sections" 
* name = "exnds-sections" 
* title = "EXNDS_Sections" 
* status = #active 
* content = #complete 
* version = "201301" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.194" 
* date = "2013-01-10" 
* count = 10 
* #ABSDaten "ABS Daten"
* #Attachments "Attachments"
* #BEFUNDE "Befunde"
* #BSCHEIN "Behandlungsschein"
* #KARTEI_EINTRAGUNGEN "Karteieintragungen"
* #LabSpecContainer "Laboratory Speciality Container"
* #PAT_INFO_ADM "Weitere Patienteninformation - Administrativ"
* #PAT_INFO_MED "Weitere Patienteninformation - Medizinisch"
* #PAT_INFO_MED_MERK "Weitere Merkmale"
* #eCardKONS "eCard Konsultationsdaten"
