Instance: exnds-sections 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-exnds-sections" 
* name = "exnds-sections" 
* title = "EXNDS_Sections" 
* status = #active 
* content = #complete 
* version = "201301" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.194" 
* date = "2013-01-10" 
* count = 10 
* concept[0].code = #ABSDaten
* concept[0].display = "ABS Daten"
* concept[1].code = #Attachments
* concept[1].display = "Attachments"
* concept[2].code = #BEFUNDE
* concept[2].display = "Befunde"
* concept[3].code = #BSCHEIN
* concept[3].display = "Behandlungsschein"
* concept[4].code = #KARTEI_EINTRAGUNGEN
* concept[4].display = "Karteieintragungen"
* concept[5].code = #LabSpecContainer
* concept[5].display = "Laboratory Speciality Container"
* concept[6].code = #PAT_INFO_ADM
* concept[6].display = "Weitere Patienteninformation - Administrativ"
* concept[7].code = #PAT_INFO_MED
* concept[7].display = "Weitere Patienteninformation - Medizinisch"
* concept[8].code = #PAT_INFO_MED_MERK
* concept[8].display = "Weitere Merkmale"
* concept[9].code = #eCardKONS
* concept[9].display = "eCard Konsultationsdaten"
