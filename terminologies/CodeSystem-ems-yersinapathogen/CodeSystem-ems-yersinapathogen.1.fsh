Instance: ems-yersinapathogen 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-yersinapathogen" 
* name = "ems-yersinapathogen" 
* title = "EMS_YersinaPathogen" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Yersinia Pathogen" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.85" 
* date = "2017-01-26" 
* count = 4 
* concept[0].code = #YERENT
* concept[0].display = "Yersinia enterocolitica"
* concept[1].code = #YEROTHER
* concept[1].display = "andere Yersinia"
* concept[2].code = #YERPSE
* concept[2].display = "Yersinia pseudotuberculosis"
* concept[3].code = #YERSPP
* concept[3].display = "Yersinia unspezifiziert"
