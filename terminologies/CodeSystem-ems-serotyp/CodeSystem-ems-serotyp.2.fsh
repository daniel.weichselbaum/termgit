Instance: ems-serotyp 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-serotyp" 
* name = "ems-serotyp" 
* title = "EMS_Serotyp" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.92" 
* date = "2017-01-26" 
* count = 1852 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #HAEMOPHILUS "Haemophilus influenzae"
* #HAEMOPHILUS ^property[0].code = #child 
* #HAEMOPHILUS ^property[0].valueCode = #A 
* #HAEMOPHILUS ^property[1].code = #child 
* #HAEMOPHILUS ^property[1].valueCode = #B 
* #HAEMOPHILUS ^property[2].code = #child 
* #HAEMOPHILUS ^property[2].valueCode = #C 
* #HAEMOPHILUS ^property[3].code = #child 
* #HAEMOPHILUS ^property[3].valueCode = #D 
* #HAEMOPHILUS ^property[4].code = #child 
* #HAEMOPHILUS ^property[4].valueCode = #E 
* #HAEMOPHILUS ^property[5].code = #child 
* #HAEMOPHILUS ^property[5].valueCode = #F 
* #HAEMOPHILUS ^property[6].code = #child 
* #HAEMOPHILUS ^property[6].valueCode = #NON_B 
* #HAEMOPHILUS ^property[7].code = #child 
* #HAEMOPHILUS ^property[7].valueCode = #NON_CAPS 
* #A "Paratyphi A"
* #A ^property[0].code = #parent 
* #A ^property[0].valueCode = #PARATYPHUS 
* #B "Paratyphi B"
* #B ^property[0].code = #parent 
* #B ^property[0].valueCode = #PARATYPHUS 
* #C "Paratyphi C"
* #C ^property[0].code = #parent 
* #C ^property[0].valueCode = #PARATYPHUS 
* #D "H influenzae type d"
* #D ^property[0].code = #parent 
* #D ^property[0].valueCode = #HAEMOPHILUS 
* #E "H influenzae type e"
* #E ^property[0].code = #parent 
* #E ^property[0].valueCode = #HAEMOPHILUS 
* #F "H influenzae type f"
* #F ^property[0].code = #parent 
* #F ^property[0].valueCode = #HAEMOPHILUS 
* #NON_B "non-b H influenzae strain"
* #NON_B ^property[0].code = #parent 
* #NON_B ^property[0].valueCode = #HAEMOPHILUS 
* #NON_CAPS "non-capsulated H influenzae strain"
* #NON_CAPS ^property[0].code = #parent 
* #NON_CAPS ^property[0].valueCode = #HAEMOPHILUS 
* #LISTERIOSE "Listeriose"
* #LISTERIOSE ^property[0].code = #child 
* #LISTERIOSE ^property[0].valueCode = #1_2 
* #LISTERIOSE ^property[1].code = #child 
* #LISTERIOSE ^property[1].valueCode = #1_2A 
* #LISTERIOSE ^property[2].code = #child 
* #LISTERIOSE ^property[2].valueCode = #1_2B 
* #LISTERIOSE ^property[3].code = #child 
* #LISTERIOSE ^property[3].valueCode = #1_2C 
* #LISTERIOSE ^property[4].code = #child 
* #LISTERIOSE ^property[4].valueCode = #3A 
* #LISTERIOSE ^property[5].code = #child 
* #LISTERIOSE ^property[5].valueCode = #3B 
* #LISTERIOSE ^property[6].code = #child 
* #LISTERIOSE ^property[6].valueCode = #3C 
* #LISTERIOSE ^property[7].code = #child 
* #LISTERIOSE ^property[7].valueCode = #4 
* #LISTERIOSE ^property[8].code = #child 
* #LISTERIOSE ^property[8].valueCode = #4A 
* #LISTERIOSE ^property[9].code = #child 
* #LISTERIOSE ^property[9].valueCode = #4AB 
* #LISTERIOSE ^property[10].code = #child 
* #LISTERIOSE ^property[10].valueCode = #4B 
* #LISTERIOSE ^property[11].code = #child 
* #LISTERIOSE ^property[11].valueCode = #4C 
* #LISTERIOSE ^property[12].code = #child 
* #LISTERIOSE ^property[12].valueCode = #4D 
* #LISTERIOSE ^property[13].code = #child 
* #LISTERIOSE ^property[13].valueCode = #4E 
* #LISTERIOSE ^property[14].code = #child 
* #LISTERIOSE ^property[14].valueCode = #7 
* #1_2 "1/2"
* #1_2 ^property[0].code = #parent 
* #1_2 ^property[0].valueCode = #LISTERIOSE 
* #1_2A "1/2a"
* #1_2A ^property[0].code = #parent 
* #1_2A ^property[0].valueCode = #LISTERIOSE 
* #1_2B "1/2b"
* #1_2B ^property[0].code = #parent 
* #1_2B ^property[0].valueCode = #LISTERIOSE 
* #1_2C "1/2c"
* #1_2C ^property[0].code = #parent 
* #1_2C ^property[0].valueCode = #LISTERIOSE 
* #3A "3a"
* #3A ^property[0].code = #parent 
* #3A ^property[0].valueCode = #SHIGELLOSE 
* #3B "3b"
* #3B ^property[0].code = #parent 
* #3B ^property[0].valueCode = #SHIGELLOSE 
* #3C "3c"
* #3C ^property[0].code = #parent 
* #3C ^property[0].valueCode = #LISTERIOSE 
* #4 "4"
* #4 ^property[0].code = #parent 
* #4 ^property[0].valueCode = #SHIGELLOSE 
* #4A "4a"
* #4A ^property[0].code = #parent 
* #4A ^property[0].valueCode = #SHIGELLOSE 
* #4AB "4ab"
* #4AB ^property[0].code = #parent 
* #4AB ^property[0].valueCode = #LISTERIOSE 
* #4B "4b"
* #4B ^property[0].code = #parent 
* #4B ^property[0].valueCode = #SHIGELLOSE 
* #4C "4c"
* #4C ^property[0].code = #parent 
* #4C ^property[0].valueCode = #SHIGELLOSE 
* #4D "4d"
* #4D ^property[0].code = #parent 
* #4D ^property[0].valueCode = #LISTERIOSE 
* #4E "4e"
* #4E ^property[0].code = #parent 
* #4E ^property[0].valueCode = #LISTERIOSE 
* #7 "7"
* #7 ^property[0].code = #parent 
* #7 ^property[0].valueCode = #SHIGELLOSE 
* #MENINGOKOKKEN "Meningokokken-Erkrankung, invasiv"
* #MENINGOKOKKEN ^property[0].code = #child 
* #MENINGOKOKKEN ^property[0].valueCode = #NOTEST 
* #MENINGOKOKKEN ^property[1].code = #child 
* #MENINGOKOKKEN ^property[1].valueCode = #NOTYPE 
* #MENINGOKOKKEN ^property[2].code = #child 
* #MENINGOKOKKEN ^property[2].valueCode = #P2_2A 
* #MENINGOKOKKEN ^property[3].code = #child 
* #MENINGOKOKKEN ^property[3].valueCode = #P2_2B 
* #MENINGOKOKKEN ^property[4].code = #child 
* #MENINGOKOKKEN ^property[4].valueCode = #P2_2C 
* #MENINGOKOKKEN ^property[5].code = #child 
* #MENINGOKOKKEN ^property[5].valueCode = #P3_1 
* #MENINGOKOKKEN ^property[6].code = #child 
* #MENINGOKOKKEN ^property[6].valueCode = #P3_14 
* #MENINGOKOKKEN ^property[7].code = #child 
* #MENINGOKOKKEN ^property[7].valueCode = #P3_15 
* #MENINGOKOKKEN ^property[8].code = #child 
* #MENINGOKOKKEN ^property[8].valueCode = #P3_16 
* #MENINGOKOKKEN ^property[9].code = #child 
* #MENINGOKOKKEN ^property[9].valueCode = #P3_21 
* #MENINGOKOKKEN ^property[10].code = #child 
* #MENINGOKOKKEN ^property[10].valueCode = #P3_22 
* #MENINGOKOKKEN ^property[11].code = #child 
* #MENINGOKOKKEN ^property[11].valueCode = #P3_23 
* #MENINGOKOKKEN ^property[12].code = #child 
* #MENINGOKOKKEN ^property[12].valueCode = #P3_4 
* #NOTEST "not tested"
* #NOTEST ^property[0].code = #parent 
* #NOTEST ^property[0].valueCode = #PNEUMOKOKKEN 
* #NOTYPE "Not typeable"
* #NOTYPE ^property[0].code = #parent 
* #NOTYPE ^property[0].valueCode = #PNEUMOKOKKEN 
* #P2_2A "serotype P2.2a"
* #P2_2A ^property[0].code = #parent 
* #P2_2A ^property[0].valueCode = #MENINGOKOKKEN 
* #P2_2B "serotype P2.2b"
* #P2_2B ^property[0].code = #parent 
* #P2_2B ^property[0].valueCode = #MENINGOKOKKEN 
* #P2_2C "serotype P2.2c"
* #P2_2C ^property[0].code = #parent 
* #P2_2C ^property[0].valueCode = #MENINGOKOKKEN 
* #P3_1 "serotype P3.1"
* #P3_1 ^property[0].code = #parent 
* #P3_1 ^property[0].valueCode = #MENINGOKOKKEN 
* #P3_14 "serotype P3.14"
* #P3_14 ^property[0].code = #parent 
* #P3_14 ^property[0].valueCode = #MENINGOKOKKEN 
* #P3_15 "serotype P3.15"
* #P3_15 ^property[0].code = #parent 
* #P3_15 ^property[0].valueCode = #MENINGOKOKKEN 
* #P3_16 "serotype P3.16"
* #P3_16 ^property[0].code = #parent 
* #P3_16 ^property[0].valueCode = #MENINGOKOKKEN 
* #P3_21 "serotype P3.21"
* #P3_21 ^property[0].code = #parent 
* #P3_21 ^property[0].valueCode = #MENINGOKOKKEN 
* #P3_22 "serotype P3.22"
* #P3_22 ^property[0].code = #parent 
* #P3_22 ^property[0].valueCode = #MENINGOKOKKEN 
* #P3_23 "serotype P3.23"
* #P3_23 ^property[0].code = #parent 
* #P3_23 ^property[0].valueCode = #MENINGOKOKKEN 
* #P3_4 "serotype P3.4"
* #P3_4 ^property[0].code = #parent 
* #P3_4 ^property[0].valueCode = #MENINGOKOKKEN 
* #PARATYPHUS "Paratyphus"
* #PARATYPHUS ^property[0].code = #child 
* #PARATYPHUS ^property[0].valueCode = #A 
* #PARATYPHUS ^property[1].code = #child 
* #PARATYPHUS ^property[1].valueCode = #B 
* #PARATYPHUS ^property[2].code = #child 
* #PARATYPHUS ^property[2].valueCode = #C 
* #PNEUMOKOKKEN "Pneumokokken-Erkrankung, invasiv"
* #PNEUMOKOKKEN ^property[0].code = #child 
* #PNEUMOKOKKEN ^property[0].valueCode = #1 
* #PNEUMOKOKKEN ^property[1].code = #child 
* #PNEUMOKOKKEN ^property[1].valueCode = #10A 
* #PNEUMOKOKKEN ^property[2].code = #child 
* #PNEUMOKOKKEN ^property[2].valueCode = #10B 
* #PNEUMOKOKKEN ^property[3].code = #child 
* #PNEUMOKOKKEN ^property[3].valueCode = #10C 
* #PNEUMOKOKKEN ^property[4].code = #child 
* #PNEUMOKOKKEN ^property[4].valueCode = #10F 
* #PNEUMOKOKKEN ^property[5].code = #child 
* #PNEUMOKOKKEN ^property[5].valueCode = #11 
* #PNEUMOKOKKEN ^property[6].code = #child 
* #PNEUMOKOKKEN ^property[6].valueCode = #11A 
* #PNEUMOKOKKEN ^property[7].code = #child 
* #PNEUMOKOKKEN ^property[7].valueCode = #11B 
* #PNEUMOKOKKEN ^property[8].code = #child 
* #PNEUMOKOKKEN ^property[8].valueCode = #11C 
* #PNEUMOKOKKEN ^property[9].code = #child 
* #PNEUMOKOKKEN ^property[9].valueCode = #11D 
* #PNEUMOKOKKEN ^property[10].code = #child 
* #PNEUMOKOKKEN ^property[10].valueCode = #11F 
* #PNEUMOKOKKEN ^property[11].code = #child 
* #PNEUMOKOKKEN ^property[11].valueCode = #12 
* #PNEUMOKOKKEN ^property[12].code = #child 
* #PNEUMOKOKKEN ^property[12].valueCode = #12A 
* #PNEUMOKOKKEN ^property[13].code = #child 
* #PNEUMOKOKKEN ^property[13].valueCode = #12B 
* #PNEUMOKOKKEN ^property[14].code = #child 
* #PNEUMOKOKKEN ^property[14].valueCode = #12F 
* #PNEUMOKOKKEN ^property[15].code = #child 
* #PNEUMOKOKKEN ^property[15].valueCode = #13 
* #PNEUMOKOKKEN ^property[16].code = #child 
* #PNEUMOKOKKEN ^property[16].valueCode = #14 
* #PNEUMOKOKKEN ^property[17].code = #child 
* #PNEUMOKOKKEN ^property[17].valueCode = #15 
* #PNEUMOKOKKEN ^property[18].code = #child 
* #PNEUMOKOKKEN ^property[18].valueCode = #15A 
* #PNEUMOKOKKEN ^property[19].code = #child 
* #PNEUMOKOKKEN ^property[19].valueCode = #15B 
* #PNEUMOKOKKEN ^property[20].code = #child 
* #PNEUMOKOKKEN ^property[20].valueCode = #15B/C 
* #PNEUMOKOKKEN ^property[21].code = #child 
* #PNEUMOKOKKEN ^property[21].valueCode = #15C 
* #PNEUMOKOKKEN ^property[22].code = #child 
* #PNEUMOKOKKEN ^property[22].valueCode = #15F 
* #PNEUMOKOKKEN ^property[23].code = #child 
* #PNEUMOKOKKEN ^property[23].valueCode = #16 
* #PNEUMOKOKKEN ^property[24].code = #child 
* #PNEUMOKOKKEN ^property[24].valueCode = #16A 
* #PNEUMOKOKKEN ^property[25].code = #child 
* #PNEUMOKOKKEN ^property[25].valueCode = #16F 
* #PNEUMOKOKKEN ^property[26].code = #child 
* #PNEUMOKOKKEN ^property[26].valueCode = #17 
* #PNEUMOKOKKEN ^property[27].code = #child 
* #PNEUMOKOKKEN ^property[27].valueCode = #17A 
* #PNEUMOKOKKEN ^property[28].code = #child 
* #PNEUMOKOKKEN ^property[28].valueCode = #17F 
* #PNEUMOKOKKEN ^property[29].code = #child 
* #PNEUMOKOKKEN ^property[29].valueCode = #18 
* #PNEUMOKOKKEN ^property[30].code = #child 
* #PNEUMOKOKKEN ^property[30].valueCode = #18A 
* #PNEUMOKOKKEN ^property[31].code = #child 
* #PNEUMOKOKKEN ^property[31].valueCode = #18B 
* #PNEUMOKOKKEN ^property[32].code = #child 
* #PNEUMOKOKKEN ^property[32].valueCode = #18C 
* #PNEUMOKOKKEN ^property[33].code = #child 
* #PNEUMOKOKKEN ^property[33].valueCode = #18F 
* #PNEUMOKOKKEN ^property[34].code = #child 
* #PNEUMOKOKKEN ^property[34].valueCode = #19 
* #PNEUMOKOKKEN ^property[35].code = #child 
* #PNEUMOKOKKEN ^property[35].valueCode = #19A 
* #PNEUMOKOKKEN ^property[36].code = #child 
* #PNEUMOKOKKEN ^property[36].valueCode = #19B 
* #PNEUMOKOKKEN ^property[37].code = #child 
* #PNEUMOKOKKEN ^property[37].valueCode = #19C 
* #PNEUMOKOKKEN ^property[38].code = #child 
* #PNEUMOKOKKEN ^property[38].valueCode = #19F 
* #PNEUMOKOKKEN ^property[39].code = #child 
* #PNEUMOKOKKEN ^property[39].valueCode = #2 
* #PNEUMOKOKKEN ^property[40].code = #child 
* #PNEUMOKOKKEN ^property[40].valueCode = #20 
* #PNEUMOKOKKEN ^property[41].code = #child 
* #PNEUMOKOKKEN ^property[41].valueCode = #21 
* #PNEUMOKOKKEN ^property[42].code = #child 
* #PNEUMOKOKKEN ^property[42].valueCode = #22 
* #PNEUMOKOKKEN ^property[43].code = #child 
* #PNEUMOKOKKEN ^property[43].valueCode = #22A 
* #PNEUMOKOKKEN ^property[44].code = #child 
* #PNEUMOKOKKEN ^property[44].valueCode = #22F 
* #PNEUMOKOKKEN ^property[45].code = #child 
* #PNEUMOKOKKEN ^property[45].valueCode = #23 
* #PNEUMOKOKKEN ^property[46].code = #child 
* #PNEUMOKOKKEN ^property[46].valueCode = #23A 
* #PNEUMOKOKKEN ^property[47].code = #child 
* #PNEUMOKOKKEN ^property[47].valueCode = #23B 
* #PNEUMOKOKKEN ^property[48].code = #child 
* #PNEUMOKOKKEN ^property[48].valueCode = #23F 
* #PNEUMOKOKKEN ^property[49].code = #child 
* #PNEUMOKOKKEN ^property[49].valueCode = #24 
* #PNEUMOKOKKEN ^property[50].code = #child 
* #PNEUMOKOKKEN ^property[50].valueCode = #24A 
* #PNEUMOKOKKEN ^property[51].code = #child 
* #PNEUMOKOKKEN ^property[51].valueCode = #24B 
* #PNEUMOKOKKEN ^property[52].code = #child 
* #PNEUMOKOKKEN ^property[52].valueCode = #24F 
* #PNEUMOKOKKEN ^property[53].code = #child 
* #PNEUMOKOKKEN ^property[53].valueCode = #25 
* #PNEUMOKOKKEN ^property[54].code = #child 
* #PNEUMOKOKKEN ^property[54].valueCode = #25A 
* #PNEUMOKOKKEN ^property[55].code = #child 
* #PNEUMOKOKKEN ^property[55].valueCode = #25F 
* #PNEUMOKOKKEN ^property[56].code = #child 
* #PNEUMOKOKKEN ^property[56].valueCode = #27 
* #PNEUMOKOKKEN ^property[57].code = #child 
* #PNEUMOKOKKEN ^property[57].valueCode = #28 
* #PNEUMOKOKKEN ^property[58].code = #child 
* #PNEUMOKOKKEN ^property[58].valueCode = #28A 
* #PNEUMOKOKKEN ^property[59].code = #child 
* #PNEUMOKOKKEN ^property[59].valueCode = #28F 
* #PNEUMOKOKKEN ^property[60].code = #child 
* #PNEUMOKOKKEN ^property[60].valueCode = #29 
* #PNEUMOKOKKEN ^property[61].code = #child 
* #PNEUMOKOKKEN ^property[61].valueCode = #3 
* #PNEUMOKOKKEN ^property[62].code = #child 
* #PNEUMOKOKKEN ^property[62].valueCode = #31 
* #PNEUMOKOKKEN ^property[63].code = #child 
* #PNEUMOKOKKEN ^property[63].valueCode = #32 
* #PNEUMOKOKKEN ^property[64].code = #child 
* #PNEUMOKOKKEN ^property[64].valueCode = #32A 
* #PNEUMOKOKKEN ^property[65].code = #child 
* #PNEUMOKOKKEN ^property[65].valueCode = #32F 
* #PNEUMOKOKKEN ^property[66].code = #child 
* #PNEUMOKOKKEN ^property[66].valueCode = #33 
* #PNEUMOKOKKEN ^property[67].code = #child 
* #PNEUMOKOKKEN ^property[67].valueCode = #33A 
* #PNEUMOKOKKEN ^property[68].code = #child 
* #PNEUMOKOKKEN ^property[68].valueCode = #33B 
* #PNEUMOKOKKEN ^property[69].code = #child 
* #PNEUMOKOKKEN ^property[69].valueCode = #33C 
* #PNEUMOKOKKEN ^property[70].code = #child 
* #PNEUMOKOKKEN ^property[70].valueCode = #33D 
* #PNEUMOKOKKEN ^property[71].code = #child 
* #PNEUMOKOKKEN ^property[71].valueCode = #33F 
* #PNEUMOKOKKEN ^property[72].code = #child 
* #PNEUMOKOKKEN ^property[72].valueCode = #34 
* #PNEUMOKOKKEN ^property[73].code = #child 
* #PNEUMOKOKKEN ^property[73].valueCode = #35 
* #PNEUMOKOKKEN ^property[74].code = #child 
* #PNEUMOKOKKEN ^property[74].valueCode = #35A 
* #PNEUMOKOKKEN ^property[75].code = #child 
* #PNEUMOKOKKEN ^property[75].valueCode = #35B 
* #PNEUMOKOKKEN ^property[76].code = #child 
* #PNEUMOKOKKEN ^property[76].valueCode = #35C 
* #PNEUMOKOKKEN ^property[77].code = #child 
* #PNEUMOKOKKEN ^property[77].valueCode = #35F 
* #PNEUMOKOKKEN ^property[78].code = #child 
* #PNEUMOKOKKEN ^property[78].valueCode = #36 
* #PNEUMOKOKKEN ^property[79].code = #child 
* #PNEUMOKOKKEN ^property[79].valueCode = #37 
* #PNEUMOKOKKEN ^property[80].code = #child 
* #PNEUMOKOKKEN ^property[80].valueCode = #38 
* #PNEUMOKOKKEN ^property[81].code = #child 
* #PNEUMOKOKKEN ^property[81].valueCode = #39 
* #PNEUMOKOKKEN ^property[82].code = #child 
* #PNEUMOKOKKEN ^property[82].valueCode = #4 
* #PNEUMOKOKKEN ^property[83].code = #child 
* #PNEUMOKOKKEN ^property[83].valueCode = #40 
* #PNEUMOKOKKEN ^property[84].code = #child 
* #PNEUMOKOKKEN ^property[84].valueCode = #41 
* #PNEUMOKOKKEN ^property[85].code = #child 
* #PNEUMOKOKKEN ^property[85].valueCode = #41A 
* #PNEUMOKOKKEN ^property[86].code = #child 
* #PNEUMOKOKKEN ^property[86].valueCode = #41F 
* #PNEUMOKOKKEN ^property[87].code = #child 
* #PNEUMOKOKKEN ^property[87].valueCode = #42 
* #PNEUMOKOKKEN ^property[88].code = #child 
* #PNEUMOKOKKEN ^property[88].valueCode = #43 
* #PNEUMOKOKKEN ^property[89].code = #child 
* #PNEUMOKOKKEN ^property[89].valueCode = #44 
* #PNEUMOKOKKEN ^property[90].code = #child 
* #PNEUMOKOKKEN ^property[90].valueCode = #45 
* #PNEUMOKOKKEN ^property[91].code = #child 
* #PNEUMOKOKKEN ^property[91].valueCode = #46 
* #PNEUMOKOKKEN ^property[92].code = #child 
* #PNEUMOKOKKEN ^property[92].valueCode = #47 
* #PNEUMOKOKKEN ^property[93].code = #child 
* #PNEUMOKOKKEN ^property[93].valueCode = #47A 
* #PNEUMOKOKKEN ^property[94].code = #child 
* #PNEUMOKOKKEN ^property[94].valueCode = #47F 
* #PNEUMOKOKKEN ^property[95].code = #child 
* #PNEUMOKOKKEN ^property[95].valueCode = #48 
* #PNEUMOKOKKEN ^property[96].code = #child 
* #PNEUMOKOKKEN ^property[96].valueCode = #5 
* #PNEUMOKOKKEN ^property[97].code = #child 
* #PNEUMOKOKKEN ^property[97].valueCode = #6 
* #PNEUMOKOKKEN ^property[98].code = #child 
* #PNEUMOKOKKEN ^property[98].valueCode = #6A 
* #PNEUMOKOKKEN ^property[99].code = #child 
* #PNEUMOKOKKEN ^property[99].valueCode = #6B 
* #PNEUMOKOKKEN ^property[100].code = #child 
* #PNEUMOKOKKEN ^property[100].valueCode = #6C 
* #PNEUMOKOKKEN ^property[101].code = #child 
* #PNEUMOKOKKEN ^property[101].valueCode = #6D 
* #PNEUMOKOKKEN ^property[102].code = #child 
* #PNEUMOKOKKEN ^property[102].valueCode = #7 
* #PNEUMOKOKKEN ^property[103].code = #child 
* #PNEUMOKOKKEN ^property[103].valueCode = #7A 
* #PNEUMOKOKKEN ^property[104].code = #child 
* #PNEUMOKOKKEN ^property[104].valueCode = #7B 
* #PNEUMOKOKKEN ^property[105].code = #child 
* #PNEUMOKOKKEN ^property[105].valueCode = #7C 
* #PNEUMOKOKKEN ^property[106].code = #child 
* #PNEUMOKOKKEN ^property[106].valueCode = #7F 
* #PNEUMOKOKKEN ^property[107].code = #child 
* #PNEUMOKOKKEN ^property[107].valueCode = #8 
* #PNEUMOKOKKEN ^property[108].code = #child 
* #PNEUMOKOKKEN ^property[108].valueCode = #9 
* #PNEUMOKOKKEN ^property[109].code = #child 
* #PNEUMOKOKKEN ^property[109].valueCode = #9A 
* #PNEUMOKOKKEN ^property[110].code = #child 
* #PNEUMOKOKKEN ^property[110].valueCode = #9L 
* #PNEUMOKOKKEN ^property[111].code = #child 
* #PNEUMOKOKKEN ^property[111].valueCode = #9N 
* #PNEUMOKOKKEN ^property[112].code = #child 
* #PNEUMOKOKKEN ^property[112].valueCode = #9V 
* #PNEUMOKOKKEN ^property[113].code = #child 
* #PNEUMOKOKKEN ^property[113].valueCode = #NOTEST 
* #PNEUMOKOKKEN ^property[114].code = #child 
* #PNEUMOKOKKEN ^property[114].valueCode = #NOTYPE 
* #1 "1"
* #1 ^property[0].code = #parent 
* #1 ^property[0].valueCode = #SHIGELLOSE 
* #10A "10A"
* #10A ^property[0].code = #parent 
* #10A ^property[0].valueCode = #PNEUMOKOKKEN 
* #10B "10B"
* #10B ^property[0].code = #parent 
* #10B ^property[0].valueCode = #PNEUMOKOKKEN 
* #10C "10C"
* #10C ^property[0].code = #parent 
* #10C ^property[0].valueCode = #PNEUMOKOKKEN 
* #10F "10F"
* #10F ^property[0].code = #parent 
* #10F ^property[0].valueCode = #PNEUMOKOKKEN 
* #11 "11"
* #11 ^property[0].code = #parent 
* #11 ^property[0].valueCode = #SHIGELLOSE 
* #11A "11A"
* #11A ^property[0].code = #parent 
* #11A ^property[0].valueCode = #PNEUMOKOKKEN 
* #11B "11B"
* #11B ^property[0].code = #parent 
* #11B ^property[0].valueCode = #PNEUMOKOKKEN 
* #11C "11C"
* #11C ^property[0].code = #parent 
* #11C ^property[0].valueCode = #PNEUMOKOKKEN 
* #11D "11D"
* #11D ^property[0].code = #parent 
* #11D ^property[0].valueCode = #PNEUMOKOKKEN 
* #11F "11F"
* #11F ^property[0].code = #parent 
* #11F ^property[0].valueCode = #PNEUMOKOKKEN 
* #12 "12"
* #12 ^property[0].code = #parent 
* #12 ^property[0].valueCode = #SHIGELLOSE 
* #12A "12A"
* #12A ^property[0].code = #parent 
* #12A ^property[0].valueCode = #PNEUMOKOKKEN 
* #12B "12B"
* #12B ^property[0].code = #parent 
* #12B ^property[0].valueCode = #PNEUMOKOKKEN 
* #12F "12F"
* #12F ^property[0].code = #parent 
* #12F ^property[0].valueCode = #PNEUMOKOKKEN 
* #13 "13"
* #13 ^property[0].code = #parent 
* #13 ^property[0].valueCode = #SHIGELLOSE 
* #14 "14"
* #14 ^property[0].code = #parent 
* #14 ^property[0].valueCode = #SHIGELLOSE 
* #15 "15"
* #15 ^property[0].code = #parent 
* #15 ^property[0].valueCode = #SHIGELLOSE 
* #15A "15A"
* #15A ^property[0].code = #parent 
* #15A ^property[0].valueCode = #PNEUMOKOKKEN 
* #15B "15B"
* #15B ^property[0].code = #parent 
* #15B ^property[0].valueCode = #PNEUMOKOKKEN 
* #15B/C "15B/C"
* #15B/C ^property[0].code = #parent 
* #15B/C ^property[0].valueCode = #PNEUMOKOKKEN 
* #15C "15C"
* #15C ^property[0].code = #parent 
* #15C ^property[0].valueCode = #PNEUMOKOKKEN 
* #15F "15F"
* #15F ^property[0].code = #parent 
* #15F ^property[0].valueCode = #PNEUMOKOKKEN 
* #16 "16"
* #16 ^property[0].code = #parent 
* #16 ^property[0].valueCode = #SHIGELLOSE 
* #16A "16A"
* #16A ^property[0].code = #parent 
* #16A ^property[0].valueCode = #PNEUMOKOKKEN 
* #16F "16F"
* #16F ^property[0].code = #parent 
* #16F ^property[0].valueCode = #PNEUMOKOKKEN 
* #17 "17"
* #17 ^property[0].code = #parent 
* #17 ^property[0].valueCode = #SHIGELLOSE 
* #17A "17A"
* #17A ^property[0].code = #parent 
* #17A ^property[0].valueCode = #PNEUMOKOKKEN 
* #17F "17F"
* #17F ^property[0].code = #parent 
* #17F ^property[0].valueCode = #PNEUMOKOKKEN 
* #18 "18"
* #18 ^property[0].code = #parent 
* #18 ^property[0].valueCode = #SHIGELLOSE 
* #18A "18A"
* #18A ^property[0].code = #parent 
* #18A ^property[0].valueCode = #PNEUMOKOKKEN 
* #18B "18B"
* #18B ^property[0].code = #parent 
* #18B ^property[0].valueCode = #PNEUMOKOKKEN 
* #18C "18C"
* #18C ^property[0].code = #parent 
* #18C ^property[0].valueCode = #PNEUMOKOKKEN 
* #18F "18F"
* #18F ^property[0].code = #parent 
* #18F ^property[0].valueCode = #PNEUMOKOKKEN 
* #19 "19"
* #19 ^property[0].code = #parent 
* #19 ^property[0].valueCode = #SHIGELLOSE 
* #19A "19A"
* #19A ^property[0].code = #parent 
* #19A ^property[0].valueCode = #PNEUMOKOKKEN 
* #19B "19B"
* #19B ^property[0].code = #parent 
* #19B ^property[0].valueCode = #PNEUMOKOKKEN 
* #19C "19C"
* #19C ^property[0].code = #parent 
* #19C ^property[0].valueCode = #PNEUMOKOKKEN 
* #19F "19F"
* #19F ^property[0].code = #parent 
* #19F ^property[0].valueCode = #PNEUMOKOKKEN 
* #2 "2"
* #2 ^property[0].code = #parent 
* #2 ^property[0].valueCode = #SHIGELLOSE 
* #20 "20"
* #20 ^property[0].code = #parent 
* #20 ^property[0].valueCode = #PNEUMOKOKKEN 
* #21 "21"
* #21 ^property[0].code = #parent 
* #21 ^property[0].valueCode = #PNEUMOKOKKEN 
* #22 "22"
* #22 ^property[0].code = #parent 
* #22 ^property[0].valueCode = #PNEUMOKOKKEN 
* #22A "22A"
* #22A ^property[0].code = #parent 
* #22A ^property[0].valueCode = #PNEUMOKOKKEN 
* #22F "22F"
* #22F ^property[0].code = #parent 
* #22F ^property[0].valueCode = #PNEUMOKOKKEN 
* #23 "23"
* #23 ^property[0].code = #parent 
* #23 ^property[0].valueCode = #PNEUMOKOKKEN 
* #23A "23A"
* #23A ^property[0].code = #parent 
* #23A ^property[0].valueCode = #PNEUMOKOKKEN 
* #23B "23B"
* #23B ^property[0].code = #parent 
* #23B ^property[0].valueCode = #PNEUMOKOKKEN 
* #23F "23F"
* #23F ^property[0].code = #parent 
* #23F ^property[0].valueCode = #PNEUMOKOKKEN 
* #24 "24"
* #24 ^property[0].code = #parent 
* #24 ^property[0].valueCode = #PNEUMOKOKKEN 
* #24A "24A"
* #24A ^property[0].code = #parent 
* #24A ^property[0].valueCode = #PNEUMOKOKKEN 
* #24B "24B"
* #24B ^property[0].code = #parent 
* #24B ^property[0].valueCode = #PNEUMOKOKKEN 
* #24F "24F"
* #24F ^property[0].code = #parent 
* #24F ^property[0].valueCode = #PNEUMOKOKKEN 
* #25 "25"
* #25 ^property[0].code = #parent 
* #25 ^property[0].valueCode = #PNEUMOKOKKEN 
* #25A "25A"
* #25A ^property[0].code = #parent 
* #25A ^property[0].valueCode = #PNEUMOKOKKEN 
* #25F "25F"
* #25F ^property[0].code = #parent 
* #25F ^property[0].valueCode = #PNEUMOKOKKEN 
* #27 "27"
* #27 ^property[0].code = #parent 
* #27 ^property[0].valueCode = #PNEUMOKOKKEN 
* #28 "28"
* #28 ^property[0].code = #parent 
* #28 ^property[0].valueCode = #PNEUMOKOKKEN 
* #28A "28A"
* #28A ^property[0].code = #parent 
* #28A ^property[0].valueCode = #PNEUMOKOKKEN 
* #28F "28F"
* #28F ^property[0].code = #parent 
* #28F ^property[0].valueCode = #PNEUMOKOKKEN 
* #29 "29"
* #29 ^property[0].code = #parent 
* #29 ^property[0].valueCode = #PNEUMOKOKKEN 
* #3 "3"
* #3 ^property[0].code = #parent 
* #3 ^property[0].valueCode = #SHIGELLOSE 
* #31 "31"
* #31 ^property[0].code = #parent 
* #31 ^property[0].valueCode = #PNEUMOKOKKEN 
* #32 "32"
* #32 ^property[0].code = #parent 
* #32 ^property[0].valueCode = #PNEUMOKOKKEN 
* #32A "32A"
* #32A ^property[0].code = #parent 
* #32A ^property[0].valueCode = #PNEUMOKOKKEN 
* #32F "32F"
* #32F ^property[0].code = #parent 
* #32F ^property[0].valueCode = #PNEUMOKOKKEN 
* #33 "33"
* #33 ^property[0].code = #parent 
* #33 ^property[0].valueCode = #PNEUMOKOKKEN 
* #33A "33A"
* #33A ^property[0].code = #parent 
* #33A ^property[0].valueCode = #PNEUMOKOKKEN 
* #33B "33B"
* #33B ^property[0].code = #parent 
* #33B ^property[0].valueCode = #PNEUMOKOKKEN 
* #33C "33C"
* #33C ^property[0].code = #parent 
* #33C ^property[0].valueCode = #PNEUMOKOKKEN 
* #33D "33D"
* #33D ^property[0].code = #parent 
* #33D ^property[0].valueCode = #PNEUMOKOKKEN 
* #33F "33F"
* #33F ^property[0].code = #parent 
* #33F ^property[0].valueCode = #PNEUMOKOKKEN 
* #34 "34"
* #34 ^property[0].code = #parent 
* #34 ^property[0].valueCode = #PNEUMOKOKKEN 
* #35 "35"
* #35 ^property[0].code = #parent 
* #35 ^property[0].valueCode = #PNEUMOKOKKEN 
* #35A "35A"
* #35A ^property[0].code = #parent 
* #35A ^property[0].valueCode = #PNEUMOKOKKEN 
* #35B "35B"
* #35B ^property[0].code = #parent 
* #35B ^property[0].valueCode = #PNEUMOKOKKEN 
* #35C "35C"
* #35C ^property[0].code = #parent 
* #35C ^property[0].valueCode = #PNEUMOKOKKEN 
* #35F "35F"
* #35F ^property[0].code = #parent 
* #35F ^property[0].valueCode = #PNEUMOKOKKEN 
* #36 "36"
* #36 ^property[0].code = #parent 
* #36 ^property[0].valueCode = #PNEUMOKOKKEN 
* #37 "37"
* #37 ^property[0].code = #parent 
* #37 ^property[0].valueCode = #PNEUMOKOKKEN 
* #38 "38"
* #38 ^property[0].code = #parent 
* #38 ^property[0].valueCode = #PNEUMOKOKKEN 
* #39 "39"
* #39 ^property[0].code = #parent 
* #39 ^property[0].valueCode = #PNEUMOKOKKEN 
* #40 "40"
* #40 ^property[0].code = #parent 
* #40 ^property[0].valueCode = #PNEUMOKOKKEN 
* #41 "41"
* #41 ^property[0].code = #parent 
* #41 ^property[0].valueCode = #PNEUMOKOKKEN 
* #41A "41A"
* #41A ^property[0].code = #parent 
* #41A ^property[0].valueCode = #PNEUMOKOKKEN 
* #41F "41F"
* #41F ^property[0].code = #parent 
* #41F ^property[0].valueCode = #PNEUMOKOKKEN 
* #42 "42"
* #42 ^property[0].code = #parent 
* #42 ^property[0].valueCode = #PNEUMOKOKKEN 
* #43 "43"
* #43 ^property[0].code = #parent 
* #43 ^property[0].valueCode = #PNEUMOKOKKEN 
* #44 "44"
* #44 ^property[0].code = #parent 
* #44 ^property[0].valueCode = #PNEUMOKOKKEN 
* #45 "45"
* #45 ^property[0].code = #parent 
* #45 ^property[0].valueCode = #PNEUMOKOKKEN 
* #46 "46"
* #46 ^property[0].code = #parent 
* #46 ^property[0].valueCode = #PNEUMOKOKKEN 
* #47 "47"
* #47 ^property[0].code = #parent 
* #47 ^property[0].valueCode = #PNEUMOKOKKEN 
* #47A "47A"
* #47A ^property[0].code = #parent 
* #47A ^property[0].valueCode = #PNEUMOKOKKEN 
* #47F "47F"
* #47F ^property[0].code = #parent 
* #47F ^property[0].valueCode = #PNEUMOKOKKEN 
* #48 "48"
* #48 ^property[0].code = #parent 
* #48 ^property[0].valueCode = #PNEUMOKOKKEN 
* #5 "5"
* #5 ^property[0].code = #parent 
* #5 ^property[0].valueCode = #SHIGELLOSE 
* #6 "6"
* #6 ^property[0].code = #parent 
* #6 ^property[0].valueCode = #SHIGELLOSE 
* #6A "6A"
* #6A ^property[0].code = #parent 
* #6A ^property[0].valueCode = #PNEUMOKOKKEN 
* #6B "6B"
* #6B ^property[0].code = #parent 
* #6B ^property[0].valueCode = #PNEUMOKOKKEN 
* #6C "6C"
* #6C ^property[0].code = #parent 
* #6C ^property[0].valueCode = #PNEUMOKOKKEN 
* #6D "6D"
* #6D ^property[0].code = #parent 
* #6D ^property[0].valueCode = #PNEUMOKOKKEN 
* #7A "7A"
* #7A ^property[0].code = #parent 
* #7A ^property[0].valueCode = #PNEUMOKOKKEN 
* #7B "7B"
* #7B ^property[0].code = #parent 
* #7B ^property[0].valueCode = #PNEUMOKOKKEN 
* #7C "7C"
* #7C ^property[0].code = #parent 
* #7C ^property[0].valueCode = #PNEUMOKOKKEN 
* #7F "7F"
* #7F ^property[0].code = #parent 
* #7F ^property[0].valueCode = #PNEUMOKOKKEN 
* #8 "8"
* #8 ^property[0].code = #parent 
* #8 ^property[0].valueCode = #SHIGELLOSE 
* #9 "9"
* #9 ^property[0].code = #parent 
* #9 ^property[0].valueCode = #SHIGELLOSE 
* #9A "9A"
* #9A ^property[0].code = #parent 
* #9A ^property[0].valueCode = #PNEUMOKOKKEN 
* #9L "9L"
* #9L ^property[0].code = #parent 
* #9L ^property[0].valueCode = #PNEUMOKOKKEN 
* #9N "9N"
* #9N ^property[0].code = #parent 
* #9N ^property[0].valueCode = #PNEUMOKOKKEN 
* #9V "9V"
* #9V ^property[0].code = #parent 
* #9V ^property[0].valueCode = #PNEUMOKOKKEN 
* #SALMONELLA "Salmonella spp."
* #SALMONELLA ^property[0].code = #child 
* #SALMONELLA ^property[0].valueCode = #AACHEN 
* #SALMONELLA ^property[1].code = #child 
* #SALMONELLA ^property[1].valueCode = #AARHUS 
* #SALMONELLA ^property[2].code = #child 
* #SALMONELLA ^property[2].valueCode = #ABA 
* #SALMONELLA ^property[3].code = #child 
* #SALMONELLA ^property[3].valueCode = #ABADINA 
* #SALMONELLA ^property[4].code = #child 
* #SALMONELLA ^property[4].valueCode = #ABAETETUBA 
* #SALMONELLA ^property[5].code = #child 
* #SALMONELLA ^property[5].valueCode = #ABERDEEN 
* #SALMONELLA ^property[6].code = #child 
* #SALMONELLA ^property[6].valueCode = #ABIDJAN 
* #SALMONELLA ^property[7].code = #child 
* #SALMONELLA ^property[7].valueCode = #ABLOGAME 
* #SALMONELLA ^property[8].code = #child 
* #SALMONELLA ^property[8].valueCode = #ABOBO 
* #SALMONELLA ^property[9].code = #child 
* #SALMONELLA ^property[9].valueCode = #ABONY 
* #SALMONELLA ^property[10].code = #child 
* #SALMONELLA ^property[10].valueCode = #ABORTUSEQUI 
* #SALMONELLA ^property[11].code = #child 
* #SALMONELLA ^property[11].valueCode = #ABORTUSOVIS 
* #SALMONELLA ^property[12].code = #child 
* #SALMONELLA ^property[12].valueCode = #ABUJA 
* #SALMONELLA ^property[13].code = #child 
* #SALMONELLA ^property[13].valueCode = #ACCRA 
* #SALMONELLA ^property[14].code = #child 
* #SALMONELLA ^property[14].valueCode = #ACKWEPE 
* #SALMONELLA ^property[15].code = #child 
* #SALMONELLA ^property[15].valueCode = #ADABRAKA 
* #SALMONELLA ^property[16].code = #child 
* #SALMONELLA ^property[16].valueCode = #ADAMSTOWN 
* #SALMONELLA ^property[17].code = #child 
* #SALMONELLA ^property[17].valueCode = #ADAMSTUA 
* #SALMONELLA ^property[18].code = #child 
* #SALMONELLA ^property[18].valueCode = #ADANA 
* #SALMONELLA ^property[19].code = #child 
* #SALMONELLA ^property[19].valueCode = #ADELAIDE 
* #SALMONELLA ^property[20].code = #child 
* #SALMONELLA ^property[20].valueCode = #ADEOYO 
* #SALMONELLA ^property[21].code = #child 
* #SALMONELLA ^property[21].valueCode = #ADERIKE 
* #SALMONELLA ^property[22].code = #child 
* #SALMONELLA ^property[22].valueCode = #ADIME 
* #SALMONELLA ^property[23].code = #child 
* #SALMONELLA ^property[23].valueCode = #ADJAME 
* #SALMONELLA ^property[24].code = #child 
* #SALMONELLA ^property[24].valueCode = #AEQUATORIA 
* #SALMONELLA ^property[25].code = #child 
* #SALMONELLA ^property[25].valueCode = #AESCH 
* #SALMONELLA ^property[26].code = #child 
* #SALMONELLA ^property[26].valueCode = #AFLAO 
* #SALMONELLA ^property[27].code = #child 
* #SALMONELLA ^property[27].valueCode = #AFRICANA 
* #SALMONELLA ^property[28].code = #child 
* #SALMONELLA ^property[28].valueCode = #AFULA 
* #SALMONELLA ^property[29].code = #child 
* #SALMONELLA ^property[29].valueCode = #AGAMA 
* #SALMONELLA ^property[30].code = #child 
* #SALMONELLA ^property[30].valueCode = #AGBARA 
* #SALMONELLA ^property[31].code = #child 
* #SALMONELLA ^property[31].valueCode = #AGBENI 
* #SALMONELLA ^property[32].code = #child 
* #SALMONELLA ^property[32].valueCode = #AGEGE 
* #SALMONELLA ^property[33].code = #child 
* #SALMONELLA ^property[33].valueCode = #AGO 
* #SALMONELLA ^property[34].code = #child 
* #SALMONELLA ^property[34].valueCode = #AGODI 
* #SALMONELLA ^property[35].code = #child 
* #SALMONELLA ^property[35].valueCode = #AGONA 
* #SALMONELLA ^property[36].code = #child 
* #SALMONELLA ^property[36].valueCode = #AGOUEVE 
* #SALMONELLA ^property[37].code = #child 
* #SALMONELLA ^property[37].valueCode = #AHANOU 
* #SALMONELLA ^property[38].code = #child 
* #SALMONELLA ^property[38].valueCode = #AHEPE 
* #SALMONELLA ^property[39].code = #child 
* #SALMONELLA ^property[39].valueCode = #AHMADI 
* #SALMONELLA ^property[40].code = #child 
* #SALMONELLA ^property[40].valueCode = #AHOUTOUE 
* #SALMONELLA ^property[41].code = #child 
* #SALMONELLA ^property[41].valueCode = #AHUZA 
* #SALMONELLA ^property[42].code = #child 
* #SALMONELLA ^property[42].valueCode = #AJIOBO 
* #SALMONELLA ^property[43].code = #child 
* #SALMONELLA ^property[43].valueCode = #AKANJI 
* #SALMONELLA ^property[44].code = #child 
* #SALMONELLA ^property[44].valueCode = #AKUAFO 
* #SALMONELLA ^property[45].code = #child 
* #SALMONELLA ^property[45].valueCode = #ALABAMA 
* #SALMONELLA ^property[46].code = #child 
* #SALMONELLA ^property[46].valueCode = #ALACHUA 
* #SALMONELLA ^property[47].code = #child 
* #SALMONELLA ^property[47].valueCode = #ALAGBON 
* #SALMONELLA ^property[48].code = #child 
* #SALMONELLA ^property[48].valueCode = #ALAMO 
* #SALMONELLA ^property[49].code = #child 
* #SALMONELLA ^property[49].valueCode = #ALBANY 
* #SALMONELLA ^property[50].code = #child 
* #SALMONELLA ^property[50].valueCode = #ALBERT 
* #SALMONELLA ^property[51].code = #child 
* #SALMONELLA ^property[51].valueCode = #ALBERTBANJUL 
* #SALMONELLA ^property[52].code = #child 
* #SALMONELLA ^property[52].valueCode = #ALBERTSLUND 
* #SALMONELLA ^property[53].code = #child 
* #SALMONELLA ^property[53].valueCode = #ALBUQUERQUE 
* #SALMONELLA ^property[54].code = #child 
* #SALMONELLA ^property[54].valueCode = #ALEXANDERPLATZ 
* #SALMONELLA ^property[55].code = #child 
* #SALMONELLA ^property[55].valueCode = #ALEXANDERPOLDER 
* #SALMONELLA ^property[56].code = #child 
* #SALMONELLA ^property[56].valueCode = #ALFORT 
* #SALMONELLA ^property[57].code = #child 
* #SALMONELLA ^property[57].valueCode = #ALGER 
* #SALMONELLA ^property[58].code = #child 
* #SALMONELLA ^property[58].valueCode = #ALKMAAR 
* #SALMONELLA ^property[59].code = #child 
* #SALMONELLA ^property[59].valueCode = #ALLANDALE 
* #SALMONELLA ^property[60].code = #child 
* #SALMONELLA ^property[60].valueCode = #ALLERTON 
* #SALMONELLA ^property[61].code = #child 
* #SALMONELLA ^property[61].valueCode = #ALMA 
* #SALMONELLA ^property[62].code = #child 
* #SALMONELLA ^property[62].valueCode = #ALMINKO 
* #SALMONELLA ^property[63].code = #child 
* #SALMONELLA ^property[63].valueCode = #ALPENQUAI 
* #SALMONELLA ^property[64].code = #child 
* #SALMONELLA ^property[64].valueCode = #ALTENDORF 
* #SALMONELLA ^property[65].code = #child 
* #SALMONELLA ^property[65].valueCode = #ALTONA 
* #SALMONELLA ^property[66].code = #child 
* #SALMONELLA ^property[66].valueCode = #AMAGER 
* #SALMONELLA ^property[67].code = #child 
* #SALMONELLA ^property[67].valueCode = #AMAGER_VAR_15+ 
* #SALMONELLA ^property[68].code = #child 
* #SALMONELLA ^property[68].valueCode = #AMBA 
* #SALMONELLA ^property[69].code = #child 
* #SALMONELLA ^property[69].valueCode = #AMERSFOORT 
* #SALMONELLA ^property[70].code = #child 
* #SALMONELLA ^property[70].valueCode = #AMERSFOORT_VAR_14+ 
* #SALMONELLA ^property[71].code = #child 
* #SALMONELLA ^property[71].valueCode = #AMHERSTIANA 
* #SALMONELLA ^property[72].code = #child 
* #SALMONELLA ^property[72].valueCode = #AMINA 
* #SALMONELLA ^property[73].code = #child 
* #SALMONELLA ^property[73].valueCode = #AMINATU 
* #SALMONELLA ^property[74].code = #child 
* #SALMONELLA ^property[74].valueCode = #AMOUNDERNESS 
* #SALMONELLA ^property[75].code = #child 
* #SALMONELLA ^property[75].valueCode = #AMOUTIVE 
* #SALMONELLA ^property[76].code = #child 
* #SALMONELLA ^property[76].valueCode = #AMSTERDAM 
* #SALMONELLA ^property[77].code = #child 
* #SALMONELLA ^property[77].valueCode = #AMSTERDAM_VAR_15+ 
* #SALMONELLA ^property[78].code = #child 
* #SALMONELLA ^property[78].valueCode = #AMUNIGUN 
* #SALMONELLA ^property[79].code = #child 
* #SALMONELLA ^property[79].valueCode = #ANATUM 
* #SALMONELLA ^property[80].code = #child 
* #SALMONELLA ^property[80].valueCode = #ANATUM_VAR_15+_ 
* #SALMONELLA ^property[81].code = #child 
* #SALMONELLA ^property[81].valueCode = #ANATUM_VAR_15+_34+_ 
* #SALMONELLA ^property[82].code = #child 
* #SALMONELLA ^property[82].valueCode = #ANDERLECHT 
* #SALMONELLA ^property[83].code = #child 
* #SALMONELLA ^property[83].valueCode = #ANECHO 
* #SALMONELLA ^property[84].code = #child 
* #SALMONELLA ^property[84].valueCode = #ANFO 
* #SALMONELLA ^property[85].code = #child 
* #SALMONELLA ^property[85].valueCode = #ANGERS 
* #SALMONELLA ^property[86].code = #child 
* #SALMONELLA ^property[86].valueCode = #ANGODA 
* #SALMONELLA ^property[87].code = #child 
* #SALMONELLA ^property[87].valueCode = #ANGOULEME 
* #SALMONELLA ^property[88].code = #child 
* #SALMONELLA ^property[88].valueCode = #ANK 
* #SALMONELLA ^property[89].code = #child 
* #SALMONELLA ^property[89].valueCode = #ANNA 
* #SALMONELLA ^property[90].code = #child 
* #SALMONELLA ^property[90].valueCode = #ANNEDAL 
* #SALMONELLA ^property[91].code = #child 
* #SALMONELLA ^property[91].valueCode = #ANTARCTICA 
* #SALMONELLA ^property[92].code = #child 
* #SALMONELLA ^property[92].valueCode = #ANTONIO 
* #SALMONELLA ^property[93].code = #child 
* #SALMONELLA ^property[93].valueCode = #ANTSALOVA 
* #SALMONELLA ^property[94].code = #child 
* #SALMONELLA ^property[94].valueCode = #ANTWERPEN 
* #SALMONELLA ^property[95].code = #child 
* #SALMONELLA ^property[95].valueCode = #APAPA 
* #SALMONELLA ^property[96].code = #child 
* #SALMONELLA ^property[96].valueCode = #APEYEME 
* #SALMONELLA ^property[97].code = #child 
* #SALMONELLA ^property[97].valueCode = #APRAD 
* #SALMONELLA ^property[98].code = #child 
* #SALMONELLA ^property[98].valueCode = #AQUA 
* #SALMONELLA ^property[99].code = #child 
* #SALMONELLA ^property[99].valueCode = #ARAGUA 
* #SALMONELLA ^property[100].code = #child 
* #SALMONELLA ^property[100].valueCode = #ARAPAHOE 
* #SALMONELLA ^property[101].code = #child 
* #SALMONELLA ^property[101].valueCode = #ARECHAVALETA 
* #SALMONELLA ^property[102].code = #child 
* #SALMONELLA ^property[102].valueCode = #ARGENTEUIL 
* #SALMONELLA ^property[103].code = #child 
* #SALMONELLA ^property[103].valueCode = #ARIZONAE 
* #SALMONELLA ^property[104].code = #child 
* #SALMONELLA ^property[104].valueCode = #ARUSHA 
* #SALMONELLA ^property[105].code = #child 
* #SALMONELLA ^property[105].valueCode = #ASCHERSLEBEN 
* #SALMONELLA ^property[106].code = #child 
* #SALMONELLA ^property[106].valueCode = #ASHANTI 
* #SALMONELLA ^property[107].code = #child 
* #SALMONELLA ^property[107].valueCode = #ASSEN 
* #SALMONELLA ^property[108].code = #child 
* #SALMONELLA ^property[108].valueCode = #ASSINIE 
* #SALMONELLA ^property[109].code = #child 
* #SALMONELLA ^property[109].valueCode = #ASTRIDPLEIN 
* #SALMONELLA ^property[110].code = #child 
* #SALMONELLA ^property[110].valueCode = #ASYLANTA 
* #SALMONELLA ^property[111].code = #child 
* #SALMONELLA ^property[111].valueCode = #ATAKPAME 
* #SALMONELLA ^property[112].code = #child 
* #SALMONELLA ^property[112].valueCode = #ATENTO 
* #SALMONELLA ^property[113].code = #child 
* #SALMONELLA ^property[113].valueCode = #ATHENS 
* #SALMONELLA ^property[114].code = #child 
* #SALMONELLA ^property[114].valueCode = #ATHINAI 
* #SALMONELLA ^property[115].code = #child 
* #SALMONELLA ^property[115].valueCode = #ATI 
* #SALMONELLA ^property[116].code = #child 
* #SALMONELLA ^property[116].valueCode = #AUGUSTENBORG 
* #SALMONELLA ^property[117].code = #child 
* #SALMONELLA ^property[117].valueCode = #AURELIANIS 
* #SALMONELLA ^property[118].code = #child 
* #SALMONELLA ^property[118].valueCode = #AUSTIN 
* #SALMONELLA ^property[119].code = #child 
* #SALMONELLA ^property[119].valueCode = #AUSTRALIA 
* #SALMONELLA ^property[120].code = #child 
* #SALMONELLA ^property[120].valueCode = #AVIGNON 
* #SALMONELLA ^property[121].code = #child 
* #SALMONELLA ^property[121].valueCode = #AVONMOUTH 
* #SALMONELLA ^property[122].code = #child 
* #SALMONELLA ^property[122].valueCode = #AXIM 
* #SALMONELLA ^property[123].code = #child 
* #SALMONELLA ^property[123].valueCode = #AYINDE 
* #SALMONELLA ^property[124].code = #child 
* #SALMONELLA ^property[124].valueCode = #AYTON 
* #SALMONELLA ^property[125].code = #child 
* #SALMONELLA ^property[125].valueCode = #AZTECA 
* #SALMONELLA ^property[126].code = #child 
* #SALMONELLA ^property[126].valueCode = #BABELSBERG 
* #SALMONELLA ^property[127].code = #child 
* #SALMONELLA ^property[127].valueCode = #BABILI 
* #SALMONELLA ^property[128].code = #child 
* #SALMONELLA ^property[128].valueCode = #BADAGRY 
* #SALMONELLA ^property[129].code = #child 
* #SALMONELLA ^property[129].valueCode = #BAGUIDA 
* #SALMONELLA ^property[130].code = #child 
* #SALMONELLA ^property[130].valueCode = #BAGUIRMI 
* #SALMONELLA ^property[131].code = #child 
* #SALMONELLA ^property[131].valueCode = #BAHATI 
* #SALMONELLA ^property[132].code = #child 
* #SALMONELLA ^property[132].valueCode = #BAHRENFELD 
* #SALMONELLA ^property[133].code = #child 
* #SALMONELLA ^property[133].valueCode = #BAIBOUKOUM 
* #SALMONELLA ^property[134].code = #child 
* #SALMONELLA ^property[134].valueCode = #BAILDON 
* #SALMONELLA ^property[135].code = #child 
* #SALMONELLA ^property[135].valueCode = #BAKAU 
* #SALMONELLA ^property[136].code = #child 
* #SALMONELLA ^property[136].valueCode = #BALCONES 
* #SALMONELLA ^property[137].code = #child 
* #SALMONELLA ^property[137].valueCode = #BALL 
* #SALMONELLA ^property[138].code = #child 
* #SALMONELLA ^property[138].valueCode = #BAMA 
* #SALMONELLA ^property[139].code = #child 
* #SALMONELLA ^property[139].valueCode = #BAMBOYE 
* #SALMONELLA ^property[140].code = #child 
* #SALMONELLA ^property[140].valueCode = #BAMBYLOR 
* #SALMONELLA ^property[141].code = #child 
* #SALMONELLA ^property[141].valueCode = #BANALIA 
* #SALMONELLA ^property[142].code = #child 
* #SALMONELLA ^property[142].valueCode = #BANANA 
* #SALMONELLA ^property[143].code = #child 
* #SALMONELLA ^property[143].valueCode = #BANCO 
* #SALMONELLA ^property[144].code = #child 
* #SALMONELLA ^property[144].valueCode = #BANDIA 
* #SALMONELLA ^property[145].code = #child 
* #SALMONELLA ^property[145].valueCode = #BANDIM 
* #SALMONELLA ^property[146].code = #child 
* #SALMONELLA ^property[146].valueCode = #BANGKOK 
* #SALMONELLA ^property[147].code = #child 
* #SALMONELLA ^property[147].valueCode = #BANGUI 
* #SALMONELLA ^property[148].code = #child 
* #SALMONELLA ^property[148].valueCode = #BANJUL 
* #SALMONELLA ^property[149].code = #child 
* #SALMONELLA ^property[149].valueCode = #BARDO 
* #SALMONELLA ^property[150].code = #child 
* #SALMONELLA ^property[150].valueCode = #BAREILLY 
* #SALMONELLA ^property[151].code = #child 
* #SALMONELLA ^property[151].valueCode = #BARGNY 
* #SALMONELLA ^property[152].code = #child 
* #SALMONELLA ^property[152].valueCode = #BARMBEK 
* #SALMONELLA ^property[153].code = #child 
* #SALMONELLA ^property[153].valueCode = #BARRANQUILLA 
* #SALMONELLA ^property[154].code = #child 
* #SALMONELLA ^property[154].valueCode = #BARRY 
* #SALMONELLA ^property[155].code = #child 
* #SALMONELLA ^property[155].valueCode = #BASINGSTOKE 
* #SALMONELLA ^property[156].code = #child 
* #SALMONELLA ^property[156].valueCode = #BASSA 
* #SALMONELLA ^property[157].code = #child 
* #SALMONELLA ^property[157].valueCode = #BASSADJI 
* #SALMONELLA ^property[158].code = #child 
* #SALMONELLA ^property[158].valueCode = #BATA 
* #SALMONELLA ^property[159].code = #child 
* #SALMONELLA ^property[159].valueCode = #BATONROUGE 
* #SALMONELLA ^property[160].code = #child 
* #SALMONELLA ^property[160].valueCode = #BATTLE 
* #SALMONELLA ^property[161].code = #child 
* #SALMONELLA ^property[161].valueCode = #BAZENHEID 
* #SALMONELLA ^property[162].code = #child 
* #SALMONELLA ^property[162].valueCode = #BE 
* #SALMONELLA ^property[163].code = #child 
* #SALMONELLA ^property[163].valueCode = #BEAUDESERT 
* #SALMONELLA ^property[164].code = #child 
* #SALMONELLA ^property[164].valueCode = #BEDFORD 
* #SALMONELLA ^property[165].code = #child 
* #SALMONELLA ^property[165].valueCode = #BELEM 
* #SALMONELLA ^property[166].code = #child 
* #SALMONELLA ^property[166].valueCode = #BELFAST 
* #SALMONELLA ^property[167].code = #child 
* #SALMONELLA ^property[167].valueCode = #BELLEVUE 
* #SALMONELLA ^property[168].code = #child 
* #SALMONELLA ^property[168].valueCode = #BENFICA 
* #SALMONELLA ^property[169].code = #child 
* #SALMONELLA ^property[169].valueCode = #BENGUELLA 
* #SALMONELLA ^property[170].code = #child 
* #SALMONELLA ^property[170].valueCode = #BENIN 
* #SALMONELLA ^property[171].code = #child 
* #SALMONELLA ^property[171].valueCode = #BENUE 
* #SALMONELLA ^property[172].code = #child 
* #SALMONELLA ^property[172].valueCode = #BERE 
* #SALMONELLA ^property[173].code = #child 
* #SALMONELLA ^property[173].valueCode = #BERGEDORF 
* #SALMONELLA ^property[174].code = #child 
* #SALMONELLA ^property[174].valueCode = #BERGEN 
* #SALMONELLA ^property[175].code = #child 
* #SALMONELLA ^property[175].valueCode = #BERGUES 
* #SALMONELLA ^property[176].code = #child 
* #SALMONELLA ^property[176].valueCode = #BERKELEY 
* #SALMONELLA ^property[177].code = #child 
* #SALMONELLA ^property[177].valueCode = #BERLIN 
* #SALMONELLA ^property[178].code = #child 
* #SALMONELLA ^property[178].valueCode = #BERTA 
* #SALMONELLA ^property[179].code = #child 
* #SALMONELLA ^property[179].valueCode = #BESSI 
* #SALMONELLA ^property[180].code = #child 
* #SALMONELLA ^property[180].valueCode = #BETHUNE 
* #SALMONELLA ^property[181].code = #child 
* #SALMONELLA ^property[181].valueCode = #BIAFRA 
* #SALMONELLA ^property[182].code = #child 
* #SALMONELLA ^property[182].valueCode = #BIDA 
* #SALMONELLA ^property[183].code = #child 
* #SALMONELLA ^property[183].valueCode = #BIETRI 
* #SALMONELLA ^property[184].code = #child 
* #SALMONELLA ^property[184].valueCode = #BIGNONA 
* #SALMONELLA ^property[185].code = #child 
* #SALMONELLA ^property[185].valueCode = #BIJLMER 
* #SALMONELLA ^property[186].code = #child 
* #SALMONELLA ^property[186].valueCode = #BILU 
* #SALMONELLA ^property[187].code = #child 
* #SALMONELLA ^property[187].valueCode = #BINCHE 
* #SALMONELLA ^property[188].code = #child 
* #SALMONELLA ^property[188].valueCode = #BINGERVILLE 
* #SALMONELLA ^property[189].code = #child 
* #SALMONELLA ^property[189].valueCode = #BINNINGEN 
* #SALMONELLA ^property[190].code = #child 
* #SALMONELLA ^property[190].valueCode = #BIRKENHEAD 
* #SALMONELLA ^property[191].code = #child 
* #SALMONELLA ^property[191].valueCode = #BIRMINGHAM 
* #SALMONELLA ^property[192].code = #child 
* #SALMONELLA ^property[192].valueCode = #BISPEBJERG 
* #SALMONELLA ^property[193].code = #child 
* #SALMONELLA ^property[193].valueCode = #BISSAU 
* #SALMONELLA ^property[194].code = #child 
* #SALMONELLA ^property[194].valueCode = #BLANCMESNIL 
* #SALMONELLA ^property[195].code = #child 
* #SALMONELLA ^property[195].valueCode = #BLEGDAM 
* #SALMONELLA ^property[196].code = #child 
* #SALMONELLA ^property[196].valueCode = #BLIJDORP 
* #SALMONELLA ^property[197].code = #child 
* #SALMONELLA ^property[197].valueCode = #BLITTA 
* #SALMONELLA ^property[198].code = #child 
* #SALMONELLA ^property[198].valueCode = #BLOCKLEY 
* #SALMONELLA ^property[199].code = #child 
* #SALMONELLA ^property[199].valueCode = #BLOOMSBURY 
* #SALMONELLA ^property[200].code = #child 
* #SALMONELLA ^property[200].valueCode = #BLUKWA 
* #SALMONELLA ^property[201].code = #child 
* #SALMONELLA ^property[201].valueCode = #BOBO 
* #SALMONELLA ^property[202].code = #child 
* #SALMONELLA ^property[202].valueCode = #BOCHUM 
* #SALMONELLA ^property[203].code = #child 
* #SALMONELLA ^property[203].valueCode = #BODJONEGORO 
* #SALMONELLA ^property[204].code = #child 
* #SALMONELLA ^property[204].valueCode = #BOECKER 
* #SALMONELLA ^property[205].code = #child 
* #SALMONELLA ^property[205].valueCode = #BOFFLENS 
* #SALMONELLA ^property[206].code = #child 
* #SALMONELLA ^property[206].valueCode = #BOKANJAC 
* #SALMONELLA ^property[207].code = #child 
* #SALMONELLA ^property[207].valueCode = #BOLAMA 
* #SALMONELLA ^property[208].code = #child 
* #SALMONELLA ^property[208].valueCode = #BOLOMBO 
* #SALMONELLA ^property[209].code = #child 
* #SALMONELLA ^property[209].valueCode = #BOLTON 
* #SALMONELLA ^property[210].code = #child 
* #SALMONELLA ^property[210].valueCode = #BONAMES 
* #SALMONELLA ^property[211].code = #child 
* #SALMONELLA ^property[211].valueCode = #BONARIENSIS 
* #SALMONELLA ^property[212].code = #child 
* #SALMONELLA ^property[212].valueCode = #BONN 
* #SALMONELLA ^property[213].code = #child 
* #SALMONELLA ^property[213].valueCode = #BOOTLE 
* #SALMONELLA ^property[214].code = #child 
* #SALMONELLA ^property[214].valueCode = #BORBECK 
* #SALMONELLA ^property[215].code = #child 
* #SALMONELLA ^property[215].valueCode = #BORDEAUX 
* #SALMONELLA ^property[216].code = #child 
* #SALMONELLA ^property[216].valueCode = #BORREZE 
* #SALMONELLA ^property[217].code = #child 
* #SALMONELLA ^property[217].valueCode = #BORROMEA 
* #SALMONELLA ^property[218].code = #child 
* #SALMONELLA ^property[218].valueCode = #BOUAKE 
* #SALMONELLA ^property[219].code = #child 
* #SALMONELLA ^property[219].valueCode = #BOURNEMOUTH 
* #SALMONELLA ^property[220].code = #child 
* #SALMONELLA ^property[220].valueCode = #BOUSSO 
* #SALMONELLA ^property[221].code = #child 
* #SALMONELLA ^property[221].valueCode = #BOVISMORBIFICANS 
* #SALMONELLA ^property[222].code = #child 
* #SALMONELLA ^property[222].valueCode = #BRACKENRIDGE 
* #SALMONELLA ^property[223].code = #child 
* #SALMONELLA ^property[223].valueCode = #BRACKNELL 
* #SALMONELLA ^property[224].code = #child 
* #SALMONELLA ^property[224].valueCode = #BRADFORD 
* #SALMONELLA ^property[225].code = #child 
* #SALMONELLA ^property[225].valueCode = #BRAENDERUP 
* #SALMONELLA ^property[226].code = #child 
* #SALMONELLA ^property[226].valueCode = #BRANCASTER 
* #SALMONELLA ^property[227].code = #child 
* #SALMONELLA ^property[227].valueCode = #BRANDENBURG 
* #SALMONELLA ^property[228].code = #child 
* #SALMONELLA ^property[228].valueCode = #BRAZIL 
* #SALMONELLA ^property[229].code = #child 
* #SALMONELLA ^property[229].valueCode = #BRAZOS 
* #SALMONELLA ^property[230].code = #child 
* #SALMONELLA ^property[230].valueCode = #BRAZZAVILLE 
* #SALMONELLA ^property[231].code = #child 
* #SALMONELLA ^property[231].valueCode = #BREDA 
* #SALMONELLA ^property[232].code = #child 
* #SALMONELLA ^property[232].valueCode = #BREDENEY 
* #SALMONELLA ^property[233].code = #child 
* #SALMONELLA ^property[233].valueCode = #BREFET 
* #SALMONELLA ^property[234].code = #child 
* #SALMONELLA ^property[234].valueCode = #BREUKELEN 
* #SALMONELLA ^property[235].code = #child 
* #SALMONELLA ^property[235].valueCode = #BREVIK 
* #SALMONELLA ^property[236].code = #child 
* #SALMONELLA ^property[236].valueCode = #BREZANY 
* #SALMONELLA ^property[237].code = #child 
* #SALMONELLA ^property[237].valueCode = #BRIJBHUMI 
* #SALMONELLA ^property[238].code = #child 
* #SALMONELLA ^property[238].valueCode = #BRIKAMA 
* #SALMONELLA ^property[239].code = #child 
* #SALMONELLA ^property[239].valueCode = #BRINDISI 
* #SALMONELLA ^property[240].code = #child 
* #SALMONELLA ^property[240].valueCode = #BRISBANE 
* #SALMONELLA ^property[241].code = #child 
* #SALMONELLA ^property[241].valueCode = #BRISTOL 
* #SALMONELLA ^property[242].code = #child 
* #SALMONELLA ^property[242].valueCode = #BRIVE 
* #SALMONELLA ^property[243].code = #child 
* #SALMONELLA ^property[243].valueCode = #BROC 
* #SALMONELLA ^property[244].code = #child 
* #SALMONELLA ^property[244].valueCode = #BRON 
* #SALMONELLA ^property[245].code = #child 
* #SALMONELLA ^property[245].valueCode = #BRONX 
* #SALMONELLA ^property[246].code = #child 
* #SALMONELLA ^property[246].valueCode = #BROOKLYN 
* #SALMONELLA ^property[247].code = #child 
* #SALMONELLA ^property[247].valueCode = #BROUGHTON 
* #SALMONELLA ^property[248].code = #child 
* #SALMONELLA ^property[248].valueCode = #BRUCK 
* #SALMONELLA ^property[249].code = #child 
* #SALMONELLA ^property[249].valueCode = #BRUEBACH 
* #SALMONELLA ^property[250].code = #child 
* #SALMONELLA ^property[250].valueCode = #BRUNEI 
* #SALMONELLA ^property[251].code = #child 
* #SALMONELLA ^property[251].valueCode = #BRUNFLO 
* #SALMONELLA ^property[252].code = #child 
* #SALMONELLA ^property[252].valueCode = #BSILLA 
* #SALMONELLA ^property[253].code = #child 
* #SALMONELLA ^property[253].valueCode = #BUCKEYE 
* #SALMONELLA ^property[254].code = #child 
* #SALMONELLA ^property[254].valueCode = #BUDAPEST 
* #SALMONELLA ^property[255].code = #child 
* #SALMONELLA ^property[255].valueCode = #BUKAVU 
* #SALMONELLA ^property[256].code = #child 
* #SALMONELLA ^property[256].valueCode = #BUKURU 
* #SALMONELLA ^property[257].code = #child 
* #SALMONELLA ^property[257].valueCode = #BULGARIA 
* #SALMONELLA ^property[258].code = #child 
* #SALMONELLA ^property[258].valueCode = #BULLBAY 
* #SALMONELLA ^property[259].code = #child 
* #SALMONELLA ^property[259].valueCode = #BULOVKA 
* #SALMONELLA ^property[260].code = #child 
* #SALMONELLA ^property[260].valueCode = #BURGAS 
* #SALMONELLA ^property[261].code = #child 
* #SALMONELLA ^property[261].valueCode = #BURUNDI 
* #SALMONELLA ^property[262].code = #child 
* #SALMONELLA ^property[262].valueCode = #BURY 
* #SALMONELLA ^property[263].code = #child 
* #SALMONELLA ^property[263].valueCode = #BUSINGA 
* #SALMONELLA ^property[264].code = #child 
* #SALMONELLA ^property[264].valueCode = #BUTANTAN 
* #SALMONELLA ^property[265].code = #child 
* #SALMONELLA ^property[265].valueCode = #BUTANTAN_VAR_15+ 
* #SALMONELLA ^property[266].code = #child 
* #SALMONELLA ^property[266].valueCode = #BUTARE 
* #SALMONELLA ^property[267].code = #child 
* #SALMONELLA ^property[267].valueCode = #BUZU 
* #SALMONELLA ^property[268].code = #child 
* #SALMONELLA ^property[268].valueCode = #CAEN 
* #SALMONELLA ^property[269].code = #child 
* #SALMONELLA ^property[269].valueCode = #CAIRINA 
* #SALMONELLA ^property[270].code = #child 
* #SALMONELLA ^property[270].valueCode = #CAIRNS 
* #SALMONELLA ^property[271].code = #child 
* #SALMONELLA ^property[271].valueCode = #CALABAR 
* #SALMONELLA ^property[272].code = #child 
* #SALMONELLA ^property[272].valueCode = #CALIFORNIA 
* #SALMONELLA ^property[273].code = #child 
* #SALMONELLA ^property[273].valueCode = #CAMBERENE 
* #SALMONELLA ^property[274].code = #child 
* #SALMONELLA ^property[274].valueCode = #CAMBERWELL 
* #SALMONELLA ^property[275].code = #child 
* #SALMONELLA ^property[275].valueCode = #CAMPINENSE 
* #SALMONELLA ^property[276].code = #child 
* #SALMONELLA ^property[276].valueCode = #CANADA 
* #SALMONELLA ^property[277].code = #child 
* #SALMONELLA ^property[277].valueCode = #CANARY 
* #SALMONELLA ^property[278].code = #child 
* #SALMONELLA ^property[278].valueCode = #CANNOBIO 
* #SALMONELLA ^property[279].code = #child 
* #SALMONELLA ^property[279].valueCode = #CANNONHILL 
* #SALMONELLA ^property[280].code = #child 
* #SALMONELLA ^property[280].valueCode = #CANNSTATT 
* #SALMONELLA ^property[281].code = #child 
* #SALMONELLA ^property[281].valueCode = #CANTON 
* #SALMONELLA ^property[282].code = #child 
* #SALMONELLA ^property[282].valueCode = #CARACAS 
* #SALMONELLA ^property[283].code = #child 
* #SALMONELLA ^property[283].valueCode = #CARDONER 
* #SALMONELLA ^property[284].code = #child 
* #SALMONELLA ^property[284].valueCode = #CARMEL 
* #SALMONELLA ^property[285].code = #child 
* #SALMONELLA ^property[285].valueCode = #CARNAC 
* #SALMONELLA ^property[286].code = #child 
* #SALMONELLA ^property[286].valueCode = #CARNO 
* #SALMONELLA ^property[287].code = #child 
* #SALMONELLA ^property[287].valueCode = #CARPENTRAS 
* #SALMONELLA ^property[288].code = #child 
* #SALMONELLA ^property[288].valueCode = #CARRAU 
* #SALMONELLA ^property[289].code = #child 
* #SALMONELLA ^property[289].valueCode = #CARSWELL 
* #SALMONELLA ^property[290].code = #child 
* #SALMONELLA ^property[290].valueCode = #CASABLANCA 
* #SALMONELLA ^property[291].code = #child 
* #SALMONELLA ^property[291].valueCode = #CASAMANCE 
* #SALMONELLA ^property[292].code = #child 
* #SALMONELLA ^property[292].valueCode = #CATALUNIA 
* #SALMONELLA ^property[293].code = #child 
* #SALMONELLA ^property[293].valueCode = #CATANZARO 
* #SALMONELLA ^property[294].code = #child 
* #SALMONELLA ^property[294].valueCode = #CATUMAGOS 
* #SALMONELLA ^property[295].code = #child 
* #SALMONELLA ^property[295].valueCode = #CAYAR 
* #SALMONELLA ^property[296].code = #child 
* #SALMONELLA ^property[296].valueCode = #CERRO 
* #SALMONELLA ^property[297].code = #child 
* #SALMONELLA ^property[297].valueCode = #CERRO_VAR_14+ 
* #SALMONELLA ^property[298].code = #child 
* #SALMONELLA ^property[298].valueCode = #CEYCO 
* #SALMONELLA ^property[299].code = #child 
* #SALMONELLA ^property[299].valueCode = #CHAGOUA 
* #SALMONELLA ^property[300].code = #child 
* #SALMONELLA ^property[300].valueCode = #CHAILEY 
* #SALMONELLA ^property[301].code = #child 
* #SALMONELLA ^property[301].valueCode = #CHAMPAIGN 
* #SALMONELLA ^property[302].code = #child 
* #SALMONELLA ^property[302].valueCode = #CHANDANS 
* #SALMONELLA ^property[303].code = #child 
* #SALMONELLA ^property[303].valueCode = #CHARITY 
* #SALMONELLA ^property[304].code = #child 
* #SALMONELLA ^property[304].valueCode = #CHARLOTTENBURG 
* #SALMONELLA ^property[305].code = #child 
* #SALMONELLA ^property[305].valueCode = #CHARTRES 
* #SALMONELLA ^property[306].code = #child 
* #SALMONELLA ^property[306].valueCode = #CHELTENHAM 
* #SALMONELLA ^property[307].code = #child 
* #SALMONELLA ^property[307].valueCode = #CHENNAI 
* #SALMONELLA ^property[308].code = #child 
* #SALMONELLA ^property[308].valueCode = #CHESTER 
* #SALMONELLA ^property[309].code = #child 
* #SALMONELLA ^property[309].valueCode = #CHICAGO 
* #SALMONELLA ^property[310].code = #child 
* #SALMONELLA ^property[310].valueCode = #CHICHESTER 
* #SALMONELLA ^property[311].code = #child 
* #SALMONELLA ^property[311].valueCode = #CHICHIRI 
* #SALMONELLA ^property[312].code = #child 
* #SALMONELLA ^property[312].valueCode = #CHILE 
* #SALMONELLA ^property[313].code = #child 
* #SALMONELLA ^property[313].valueCode = #CHINCOL 
* #SALMONELLA ^property[314].code = #child 
* #SALMONELLA ^property[314].valueCode = #CHINGOLA 
* #SALMONELLA ^property[315].code = #child 
* #SALMONELLA ^property[315].valueCode = #CHIREDZI 
* #SALMONELLA ^property[316].code = #child 
* #SALMONELLA ^property[316].valueCode = #CHITTAGONG 
* #SALMONELLA ^property[317].code = #child 
* #SALMONELLA ^property[317].valueCode = #CHOLERAESUIS 
* #SALMONELLA ^property[318].code = #child 
* #SALMONELLA ^property[318].valueCode = #CHOLERAESUIS_VAR_DECATUR 
* #SALMONELLA ^property[319].code = #child 
* #SALMONELLA ^property[319].valueCode = #CHOLERAESUIS_VAR_KUNZENDORF 
* #SALMONELLA ^property[320].code = #child 
* #SALMONELLA ^property[320].valueCode = #CHOMEDEY 
* #SALMONELLA ^property[321].code = #child 
* #SALMONELLA ^property[321].valueCode = #CHRISTIANSBORG 
* #SALMONELLA ^property[322].code = #child 
* #SALMONELLA ^property[322].valueCode = #CLACKAMAS 
* #SALMONELLA ^property[323].code = #child 
* #SALMONELLA ^property[323].valueCode = #CLAIBORNEI 
* #SALMONELLA ^property[324].code = #child 
* #SALMONELLA ^property[324].valueCode = #CLANVILLIAN 
* #SALMONELLA ^property[325].code = #child 
* #SALMONELLA ^property[325].valueCode = #CLERKENWELL 
* #SALMONELLA ^property[326].code = #child 
* #SALMONELLA ^property[326].valueCode = #CLEVELAND 
* #SALMONELLA ^property[327].code = #child 
* #SALMONELLA ^property[327].valueCode = #CLONTARF 
* #SALMONELLA ^property[328].code = #child 
* #SALMONELLA ^property[328].valueCode = #COCHIN 
* #SALMONELLA ^property[329].code = #child 
* #SALMONELLA ^property[329].valueCode = #COCHISE 
* #SALMONELLA ^property[330].code = #child 
* #SALMONELLA ^property[330].valueCode = #COCODY 
* #SALMONELLA ^property[331].code = #child 
* #SALMONELLA ^property[331].valueCode = #COELN 
* #SALMONELLA ^property[332].code = #child 
* #SALMONELLA ^property[332].valueCode = #COLEYPARK 
* #SALMONELLA ^property[333].code = #child 
* #SALMONELLA ^property[333].valueCode = #COLINDALE 
* #SALMONELLA ^property[334].code = #child 
* #SALMONELLA ^property[334].valueCode = #COLOBANE 
* #SALMONELLA ^property[335].code = #child 
* #SALMONELLA ^property[335].valueCode = #COLOMBO 
* #SALMONELLA ^property[336].code = #child 
* #SALMONELLA ^property[336].valueCode = #COLORADO 
* #SALMONELLA ^property[337].code = #child 
* #SALMONELLA ^property[337].valueCode = #CONCORD 
* #SALMONELLA ^property[338].code = #child 
* #SALMONELLA ^property[338].valueCode = #CONNECTICUT 
* #SALMONELLA ^property[339].code = #child 
* #SALMONELLA ^property[339].valueCode = #COOGEE 
* #SALMONELLA ^property[340].code = #child 
* #SALMONELLA ^property[340].valueCode = #COQUILHATVILLE 
* #SALMONELLA ^property[341].code = #child 
* #SALMONELLA ^property[341].valueCode = #COROMANDEL 
* #SALMONELLA ^property[342].code = #child 
* #SALMONELLA ^property[342].valueCode = #CORVALLIS 
* #SALMONELLA ^property[343].code = #child 
* #SALMONELLA ^property[343].valueCode = #COTHAM 
* #SALMONELLA ^property[344].code = #child 
* #SALMONELLA ^property[344].valueCode = #COTIA 
* #SALMONELLA ^property[345].code = #child 
* #SALMONELLA ^property[345].valueCode = #COTONOU 
* #SALMONELLA ^property[346].code = #child 
* #SALMONELLA ^property[346].valueCode = #CREMIEU 
* #SALMONELLA ^property[347].code = #child 
* #SALMONELLA ^property[347].valueCode = #CREWE 
* #SALMONELLA ^property[348].code = #child 
* #SALMONELLA ^property[348].valueCode = #CROFT 
* #SALMONELLA ^property[349].code = #child 
* #SALMONELLA ^property[349].valueCode = #CROSSNESS 
* #SALMONELLA ^property[350].code = #child 
* #SALMONELLA ^property[350].valueCode = #CUBANA 
* #SALMONELLA ^property[351].code = #child 
* #SALMONELLA ^property[351].valueCode = #CUCKMERE 
* #SALMONELLA ^property[352].code = #child 
* #SALMONELLA ^property[352].valueCode = #CULLINGWORTH 
* #SALMONELLA ^property[353].code = #child 
* #SALMONELLA ^property[353].valueCode = #CUMBERLAND 
* #SALMONELLA ^property[354].code = #child 
* #SALMONELLA ^property[354].valueCode = #CURACAO 
* #SALMONELLA ^property[355].code = #child 
* #SALMONELLA ^property[355].valueCode = #CYPRUS 
* #SALMONELLA ^property[356].code = #child 
* #SALMONELLA ^property[356].valueCode = #CZERNYRING 
* #SALMONELLA ^property[357].code = #child 
* #SALMONELLA ^property[357].valueCode = #DAARLE 
* #SALMONELLA ^property[358].code = #child 
* #SALMONELLA ^property[358].valueCode = #DABOU 
* #SALMONELLA ^property[359].code = #child 
* #SALMONELLA ^property[359].valueCode = #DADZIE 
* #SALMONELLA ^property[360].code = #child 
* #SALMONELLA ^property[360].valueCode = #DAHLEM 
* #SALMONELLA ^property[361].code = #child 
* #SALMONELLA ^property[361].valueCode = #DAHOMEY 
* #SALMONELLA ^property[362].code = #child 
* #SALMONELLA ^property[362].valueCode = #DAHRA 
* #SALMONELLA ^property[363].code = #child 
* #SALMONELLA ^property[363].valueCode = #DAKAR 
* #SALMONELLA ^property[364].code = #child 
* #SALMONELLA ^property[364].valueCode = #DAKOTA 
* #SALMONELLA ^property[365].code = #child 
* #SALMONELLA ^property[365].valueCode = #DALLGOW 
* #SALMONELLA ^property[366].code = #child 
* #SALMONELLA ^property[366].valueCode = #DAMMAN 
* #SALMONELLA ^property[367].code = #child 
* #SALMONELLA ^property[367].valueCode = #DAN 
* #SALMONELLA ^property[368].code = #child 
* #SALMONELLA ^property[368].valueCode = #DAPANGO 
* #SALMONELLA ^property[369].code = #child 
* #SALMONELLA ^property[369].valueCode = #DAULA 
* #SALMONELLA ^property[370].code = #child 
* #SALMONELLA ^property[370].valueCode = #DAYTONA 
* #SALMONELLA ^property[371].code = #child 
* #SALMONELLA ^property[371].valueCode = #DECKSTEIN 
* #SALMONELLA ^property[372].code = #child 
* #SALMONELLA ^property[372].valueCode = #DELAN 
* #SALMONELLA ^property[373].code = #child 
* #SALMONELLA ^property[373].valueCode = #DELMENHORST 
* #SALMONELLA ^property[374].code = #child 
* #SALMONELLA ^property[374].valueCode = #DEMBE 
* #SALMONELLA ^property[375].code = #child 
* #SALMONELLA ^property[375].valueCode = #DEMERARA 
* #SALMONELLA ^property[376].code = #child 
* #SALMONELLA ^property[376].valueCode = #DENVER 
* #SALMONELLA ^property[377].code = #child 
* #SALMONELLA ^property[377].valueCode = #DERBY 
* #SALMONELLA ^property[378].code = #child 
* #SALMONELLA ^property[378].valueCode = #DERKLE 
* #SALMONELLA ^property[379].code = #child 
* #SALMONELLA ^property[379].valueCode = #DESSAU 
* #SALMONELLA ^property[380].code = #child 
* #SALMONELLA ^property[380].valueCode = #DETMOLD 
* #SALMONELLA ^property[381].code = #child 
* #SALMONELLA ^property[381].valueCode = #DEVERSOIR 
* #SALMONELLA ^property[382].code = #child 
* #SALMONELLA ^property[382].valueCode = #DIBRA 
* #SALMONELLA ^property[383].code = #child 
* #SALMONELLA ^property[383].valueCode = #DIETRICHSDORF 
* #SALMONELLA ^property[384].code = #child 
* #SALMONELLA ^property[384].valueCode = #DIEUPPEUL 
* #SALMONELLA ^property[385].code = #child 
* #SALMONELLA ^property[385].valueCode = #DIGUEL 
* #SALMONELLA ^property[386].code = #child 
* #SALMONELLA ^property[386].valueCode = #DINGIRI 
* #SALMONELLA ^property[387].code = #child 
* #SALMONELLA ^property[387].valueCode = #DIOGOYE 
* #SALMONELLA ^property[388].code = #child 
* #SALMONELLA ^property[388].valueCode = #DIOURBEL 
* #SALMONELLA ^property[389].code = #child 
* #SALMONELLA ^property[389].valueCode = #DJAKARTA 
* #SALMONELLA ^property[390].code = #child 
* #SALMONELLA ^property[390].valueCode = #DJAMA 
* #SALMONELLA ^property[391].code = #child 
* #SALMONELLA ^property[391].valueCode = #DJELFA 
* #SALMONELLA ^property[392].code = #child 
* #SALMONELLA ^property[392].valueCode = #DJERMAIA 
* #SALMONELLA ^property[393].code = #child 
* #SALMONELLA ^property[393].valueCode = #DJIBOUTI 
* #SALMONELLA ^property[394].code = #child 
* #SALMONELLA ^property[394].valueCode = #DJINTEN 
* #SALMONELLA ^property[395].code = #child 
* #SALMONELLA ^property[395].valueCode = #DJUGU 
* #SALMONELLA ^property[396].code = #child 
* #SALMONELLA ^property[396].valueCode = #DOBA 
* #SALMONELLA ^property[397].code = #child 
* #SALMONELLA ^property[397].valueCode = #DOEL 
* #SALMONELLA ^property[398].code = #child 
* #SALMONELLA ^property[398].valueCode = #DONCASTER 
* #SALMONELLA ^property[399].code = #child 
* #SALMONELLA ^property[399].valueCode = #DONNA 
* #SALMONELLA ^property[400].code = #child 
* #SALMONELLA ^property[400].valueCode = #DOORN 
* #SALMONELLA ^property[401].code = #child 
* #SALMONELLA ^property[401].valueCode = #DORTMUND 
* #SALMONELLA ^property[402].code = #child 
* #SALMONELLA ^property[402].valueCode = #DOUALA 
* #SALMONELLA ^property[403].code = #child 
* #SALMONELLA ^property[403].valueCode = #DOUGI 
* #SALMONELLA ^property[404].code = #child 
* #SALMONELLA ^property[404].valueCode = #DOULASSAME 
* #SALMONELLA ^property[405].code = #child 
* #SALMONELLA ^property[405].valueCode = #DRAC 
* #SALMONELLA ^property[406].code = #child 
* #SALMONELLA ^property[406].valueCode = #DRESDEN 
* #SALMONELLA ^property[407].code = #child 
* #SALMONELLA ^property[407].valueCode = #DRIFFIELD 
* #SALMONELLA ^property[408].code = #child 
* #SALMONELLA ^property[408].valueCode = #DROGANA 
* #SALMONELLA ^property[409].code = #child 
* #SALMONELLA ^property[409].valueCode = #DUBLIN 
* #SALMONELLA ^property[410].code = #child 
* #SALMONELLA ^property[410].valueCode = #DUESSELDORF 
* #SALMONELLA ^property[411].code = #child 
* #SALMONELLA ^property[411].valueCode = #DUGBE 
* #SALMONELLA ^property[412].code = #child 
* #SALMONELLA ^property[412].valueCode = #DUISBURG 
* #SALMONELLA ^property[413].code = #child 
* #SALMONELLA ^property[413].valueCode = #DUMFRIES 
* #SALMONELLA ^property[414].code = #child 
* #SALMONELLA ^property[414].valueCode = #DUNKWA 
* #SALMONELLA ^property[415].code = #child 
* #SALMONELLA ^property[415].valueCode = #DURBAN 
* #SALMONELLA ^property[416].code = #child 
* #SALMONELLA ^property[416].valueCode = #DURHAM 
* #SALMONELLA ^property[417].code = #child 
* #SALMONELLA ^property[417].valueCode = #DUVAL 
* #SALMONELLA ^property[418].code = #child 
* #SALMONELLA ^property[418].valueCode = #EALING 
* #SALMONELLA ^property[419].code = #child 
* #SALMONELLA ^property[419].valueCode = #EASTBOURNE 
* #SALMONELLA ^property[420].code = #child 
* #SALMONELLA ^property[420].valueCode = #EASTGLAM 
* #SALMONELLA ^property[421].code = #child 
* #SALMONELLA ^property[421].valueCode = #EAUBONNE 
* #SALMONELLA ^property[422].code = #child 
* #SALMONELLA ^property[422].valueCode = #EBERSWALDE 
* #SALMONELLA ^property[423].code = #child 
* #SALMONELLA ^property[423].valueCode = #EBOKO 
* #SALMONELLA ^property[424].code = #child 
* #SALMONELLA ^property[424].valueCode = #EBRIE 
* #SALMONELLA ^property[425].code = #child 
* #SALMONELLA ^property[425].valueCode = #ECHA 
* #SALMONELLA ^property[426].code = #child 
* #SALMONELLA ^property[426].valueCode = #EDE 
* #SALMONELLA ^property[427].code = #child 
* #SALMONELLA ^property[427].valueCode = #EDINBURG 
* #SALMONELLA ^property[428].code = #child 
* #SALMONELLA ^property[428].valueCode = #EDMONTON 
* #SALMONELLA ^property[429].code = #child 
* #SALMONELLA ^property[429].valueCode = #EGUSI 
* #SALMONELLA ^property[430].code = #child 
* #SALMONELLA ^property[430].valueCode = #EGUSITOO 
* #SALMONELLA ^property[431].code = #child 
* #SALMONELLA ^property[431].valueCode = #EINGEDI 
* #SALMONELLA ^property[432].code = #child 
* #SALMONELLA ^property[432].valueCode = #EKO 
* #SALMONELLA ^property[433].code = #child 
* #SALMONELLA ^property[433].valueCode = #EKOTEDO 
* #SALMONELLA ^property[434].code = #child 
* #SALMONELLA ^property[434].valueCode = #EKPOUI 
* #SALMONELLA ^property[435].code = #child 
* #SALMONELLA ^property[435].valueCode = #ELISABETHVILLE 
* #SALMONELLA ^property[436].code = #child 
* #SALMONELLA ^property[436].valueCode = #ELOKATE 
* #SALMONELLA ^property[437].code = #child 
* #SALMONELLA ^property[437].valueCode = #ELOMRANE 
* #SALMONELLA ^property[438].code = #child 
* #SALMONELLA ^property[438].valueCode = #EMEK 
* #SALMONELLA ^property[439].code = #child 
* #SALMONELLA ^property[439].valueCode = #EMMASTAD 
* #SALMONELLA ^property[440].code = #child 
* #SALMONELLA ^property[440].valueCode = #ENCINO 
* #SALMONELLA ^property[441].code = #child 
* #SALMONELLA ^property[441].valueCode = #ENSCHEDE 
* #SALMONELLA ^property[442].code = #child 
* #SALMONELLA ^property[442].valueCode = #ENTEBBE 
* #SALMONELLA ^property[443].code = #child 
* #SALMONELLA ^property[443].valueCode = #ENTERICA 
* #SALMONELLA ^property[444].code = #child 
* #SALMONELLA ^property[444].valueCode = #ENTERICA_MONO 
* #SALMONELLA ^property[445].code = #child 
* #SALMONELLA ^property[445].valueCode = #ENTERITIDIS 
* #SALMONELLA ^property[446].code = #child 
* #SALMONELLA ^property[446].valueCode = #ENUGU 
* #SALMONELLA ^property[447].code = #child 
* #SALMONELLA ^property[447].valueCode = #EPALINGES 
* #SALMONELLA ^property[448].code = #child 
* #SALMONELLA ^property[448].valueCode = #EPICRATES 
* #SALMONELLA ^property[449].code = #child 
* #SALMONELLA ^property[449].valueCode = #EPINAY 
* #SALMONELLA ^property[450].code = #child 
* #SALMONELLA ^property[450].valueCode = #EPPENDORF 
* #SALMONELLA ^property[451].code = #child 
* #SALMONELLA ^property[451].valueCode = #ERFURT 
* #SALMONELLA ^property[452].code = #child 
* #SALMONELLA ^property[452].valueCode = #ESCANABA 
* #SALMONELLA ^property[453].code = #child 
* #SALMONELLA ^property[453].valueCode = #ESCHBERG 
* #SALMONELLA ^property[454].code = #child 
* #SALMONELLA ^property[454].valueCode = #ESCHWEILER 
* #SALMONELLA ^property[455].code = #child 
* #SALMONELLA ^property[455].valueCode = #ESSEN 
* #SALMONELLA ^property[456].code = #child 
* #SALMONELLA ^property[456].valueCode = #ESSINGEN 
* #SALMONELLA ^property[457].code = #child 
* #SALMONELLA ^property[457].valueCode = #ETTERBEEK 
* #SALMONELLA ^property[458].code = #child 
* #SALMONELLA ^property[458].valueCode = #EUSTON 
* #SALMONELLA ^property[459].code = #child 
* #SALMONELLA ^property[459].valueCode = #EVERLEIGH 
* #SALMONELLA ^property[460].code = #child 
* #SALMONELLA ^property[460].valueCode = #EVRY 
* #SALMONELLA ^property[461].code = #child 
* #SALMONELLA ^property[461].valueCode = #EZRA 
* #SALMONELLA ^property[462].code = #child 
* #SALMONELLA ^property[462].valueCode = #FAIRFIELD 
* #SALMONELLA ^property[463].code = #child 
* #SALMONELLA ^property[463].valueCode = #FAJARA 
* #SALMONELLA ^property[464].code = #child 
* #SALMONELLA ^property[464].valueCode = #FAJI 
* #SALMONELLA ^property[465].code = #child 
* #SALMONELLA ^property[465].valueCode = #FALKENSEE 
* #SALMONELLA ^property[466].code = #child 
* #SALMONELLA ^property[466].valueCode = #FALLOWFIELD 
* #SALMONELLA ^property[467].code = #child 
* #SALMONELLA ^property[467].valueCode = #FANN 
* #SALMONELLA ^property[468].code = #child 
* #SALMONELLA ^property[468].valueCode = #FANTI 
* #SALMONELLA ^property[469].code = #child 
* #SALMONELLA ^property[469].valueCode = #FARAKAN 
* #SALMONELLA ^property[470].code = #child 
* #SALMONELLA ^property[470].valueCode = #FARCHA 
* #SALMONELLA ^property[471].code = #child 
* #SALMONELLA ^property[471].valueCode = #FAREHAM 
* #SALMONELLA ^property[472].code = #child 
* #SALMONELLA ^property[472].valueCode = #FARMINGDALE 
* #SALMONELLA ^property[473].code = #child 
* #SALMONELLA ^property[473].valueCode = #FARMSEN 
* #SALMONELLA ^property[474].code = #child 
* #SALMONELLA ^property[474].valueCode = #FARSTA 
* #SALMONELLA ^property[475].code = #child 
* #SALMONELLA ^property[475].valueCode = #FASS 
* #SALMONELLA ^property[476].code = #child 
* #SALMONELLA ^property[476].valueCode = #FAYED 
* #SALMONELLA ^property[477].code = #child 
* #SALMONELLA ^property[477].valueCode = #FEHRBELLIN 
* #SALMONELLA ^property[478].code = #child 
* #SALMONELLA ^property[478].valueCode = #FERLO 
* #SALMONELLA ^property[479].code = #child 
* #SALMONELLA ^property[479].valueCode = #FERRUCH 
* #SALMONELLA ^property[480].code = #child 
* #SALMONELLA ^property[480].valueCode = #FILLMORE 
* #SALMONELLA ^property[481].code = #child 
* #SALMONELLA ^property[481].valueCode = #FINAGHY 
* #SALMONELLA ^property[482].code = #child 
* #SALMONELLA ^property[482].valueCode = #FINDORFF 
* #SALMONELLA ^property[483].code = #child 
* #SALMONELLA ^property[483].valueCode = #FINKENWERDER 
* #SALMONELLA ^property[484].code = #child 
* #SALMONELLA ^property[484].valueCode = #FISCHERHUETTE 
* #SALMONELLA ^property[485].code = #child 
* #SALMONELLA ^property[485].valueCode = #FISCHERKIETZ 
* #SALMONELLA ^property[486].code = #child 
* #SALMONELLA ^property[486].valueCode = #FISCHERSTRASSE 
* #SALMONELLA ^property[487].code = #child 
* #SALMONELLA ^property[487].valueCode = #FITZROY 
* #SALMONELLA ^property[488].code = #child 
* #SALMONELLA ^property[488].valueCode = #FLORIAN 
* #SALMONELLA ^property[489].code = #child 
* #SALMONELLA ^property[489].valueCode = #FLORIDA 
* #SALMONELLA ^property[490].code = #child 
* #SALMONELLA ^property[490].valueCode = #FLOTTBEK 
* #SALMONELLA ^property[491].code = #child 
* #SALMONELLA ^property[491].valueCode = #FLUNTERN 
* #SALMONELLA ^property[492].code = #child 
* #SALMONELLA ^property[492].valueCode = #FOMECO 
* #SALMONELLA ^property[493].code = #child 
* #SALMONELLA ^property[493].valueCode = #FORTLAMY 
* #SALMONELLA ^property[494].code = #child 
* #SALMONELLA ^property[494].valueCode = #FORTUNE 
* #SALMONELLA ^property[495].code = #child 
* #SALMONELLA ^property[495].valueCode = #FRANKEN 
* #SALMONELLA ^property[496].code = #child 
* #SALMONELLA ^property[496].valueCode = #FRANKFURT 
* #SALMONELLA ^property[497].code = #child 
* #SALMONELLA ^property[497].valueCode = #FREDERIKSBERG 
* #SALMONELLA ^property[498].code = #child 
* #SALMONELLA ^property[498].valueCode = #FREEFALLS 
* #SALMONELLA ^property[499].code = #child 
* #SALMONELLA ^property[499].valueCode = #FREETOWN 
* #SALMONELLA ^property[500].code = #child 
* #SALMONELLA ^property[500].valueCode = #FREIBURG 
* #SALMONELLA ^property[501].code = #child 
* #SALMONELLA ^property[501].valueCode = #FRESNO 
* #SALMONELLA ^property[502].code = #child 
* #SALMONELLA ^property[502].valueCode = #FRIEDENAU 
* #SALMONELLA ^property[503].code = #child 
* #SALMONELLA ^property[503].valueCode = #FRIEDRICHSFELDE 
* #SALMONELLA ^property[504].code = #child 
* #SALMONELLA ^property[504].valueCode = #FRINTROP 
* #SALMONELLA ^property[505].code = #child 
* #SALMONELLA ^property[505].valueCode = #FUFU 
* #SALMONELLA ^property[506].code = #child 
* #SALMONELLA ^property[506].valueCode = #FULDA 
* #SALMONELLA ^property[507].code = #child 
* #SALMONELLA ^property[507].valueCode = #FULICA 
* #SALMONELLA ^property[508].code = #child 
* #SALMONELLA ^property[508].valueCode = #FYRIS 
* #SALMONELLA ^property[509].code = #child 
* #SALMONELLA ^property[509].valueCode = #GABON 
* #SALMONELLA ^property[510].code = #child 
* #SALMONELLA ^property[510].valueCode = #GAFSA 
* #SALMONELLA ^property[511].code = #child 
* #SALMONELLA ^property[511].valueCode = #GAILLAC 
* #SALMONELLA ^property[512].code = #child 
* #SALMONELLA ^property[512].valueCode = #GALIEMA 
* #SALMONELLA ^property[513].code = #child 
* #SALMONELLA ^property[513].valueCode = #GALIL 
* #SALMONELLA ^property[514].code = #child 
* #SALMONELLA ^property[514].valueCode = #GALLEN 
* #SALMONELLA ^property[515].code = #child 
* #SALMONELLA ^property[515].valueCode = #GALLINARUM_ 
* #SALMONELLA ^property[516].code = #child 
* #SALMONELLA ^property[516].valueCode = #GAMABA 
* #SALMONELLA ^property[517].code = #child 
* #SALMONELLA ^property[517].valueCode = #GAMBAGA 
* #SALMONELLA ^property[518].code = #child 
* #SALMONELLA ^property[518].valueCode = #GAMBIA 
* #SALMONELLA ^property[519].code = #child 
* #SALMONELLA ^property[519].valueCode = #GAMINARA 
* #SALMONELLA ^property[520].code = #child 
* #SALMONELLA ^property[520].valueCode = #GARBA 
* #SALMONELLA ^property[521].code = #child 
* #SALMONELLA ^property[521].valueCode = #GAROLI 
* #SALMONELLA ^property[522].code = #child 
* #SALMONELLA ^property[522].valueCode = #GASSI 
* #SALMONELLA ^property[523].code = #child 
* #SALMONELLA ^property[523].valueCode = #GATESHEAD 
* #SALMONELLA ^property[524].code = #child 
* #SALMONELLA ^property[524].valueCode = #GATINEAU 
* #SALMONELLA ^property[525].code = #child 
* #SALMONELLA ^property[525].valueCode = #GATOW 
* #SALMONELLA ^property[526].code = #child 
* #SALMONELLA ^property[526].valueCode = #GATUNI 
* #SALMONELLA ^property[527].code = #child 
* #SALMONELLA ^property[527].valueCode = #GBADAGO 
* #SALMONELLA ^property[528].code = #child 
* #SALMONELLA ^property[528].valueCode = #GDANSK 
* #SALMONELLA ^property[529].code = #child 
* #SALMONELLA ^property[529].valueCode = #GDANSK_VAR_14+ 
* #SALMONELLA ^property[530].code = #child 
* #SALMONELLA ^property[530].valueCode = #GEGE 
* #SALMONELLA ^property[531].code = #child 
* #SALMONELLA ^property[531].valueCode = #GEORGIA 
* #SALMONELLA ^property[532].code = #child 
* #SALMONELLA ^property[532].valueCode = #GERA 
* #SALMONELLA ^property[533].code = #child 
* #SALMONELLA ^property[533].valueCode = #GERALDTON 
* #SALMONELLA ^property[534].code = #child 
* #SALMONELLA ^property[534].valueCode = #GERLAND 
* #SALMONELLA ^property[535].code = #child 
* #SALMONELLA ^property[535].valueCode = #GHANA 
* #SALMONELLA ^property[536].code = #child 
* #SALMONELLA ^property[536].valueCode = #GIESSEN 
* #SALMONELLA ^property[537].code = #child 
* #SALMONELLA ^property[537].valueCode = #GIVE 
* #SALMONELLA ^property[538].code = #child 
* #SALMONELLA ^property[538].valueCode = #GIVE_VAR_15+ 
* #SALMONELLA ^property[539].code = #child 
* #SALMONELLA ^property[539].valueCode = #GIVE_VAR_15+_34+ 
* #SALMONELLA ^property[540].code = #child 
* #SALMONELLA ^property[540].valueCode = #GIZA 
* #SALMONELLA ^property[541].code = #child 
* #SALMONELLA ^property[541].valueCode = #GLASGOW 
* #SALMONELLA ^property[542].code = #child 
* #SALMONELLA ^property[542].valueCode = #GLIDJI 
* #SALMONELLA ^property[543].code = #child 
* #SALMONELLA ^property[543].valueCode = #GLOSTRUP 
* #SALMONELLA ^property[544].code = #child 
* #SALMONELLA ^property[544].valueCode = #GLOUCESTER 
* #SALMONELLA ^property[545].code = #child 
* #SALMONELLA ^property[545].valueCode = #GNESTA 
* #SALMONELLA ^property[546].code = #child 
* #SALMONELLA ^property[546].valueCode = #GODESBERG 
* #SALMONELLA ^property[547].code = #child 
* #SALMONELLA ^property[547].valueCode = #GOELZAU 
* #SALMONELLA ^property[548].code = #child 
* #SALMONELLA ^property[548].valueCode = #GOELZAU_VAR_15+ 
* #SALMONELLA ^property[549].code = #child 
* #SALMONELLA ^property[549].valueCode = #GOETEBORG 
* #SALMONELLA ^property[550].code = #child 
* #SALMONELLA ^property[550].valueCode = #GOETTINGEN 
* #SALMONELLA ^property[551].code = #child 
* #SALMONELLA ^property[551].valueCode = #GOKUL 
* #SALMONELLA ^property[552].code = #child 
* #SALMONELLA ^property[552].valueCode = #GOLDCOAST 
* #SALMONELLA ^property[553].code = #child 
* #SALMONELLA ^property[553].valueCode = #GOMA 
* #SALMONELLA ^property[554].code = #child 
* #SALMONELLA ^property[554].valueCode = #GOMBE 
* #SALMONELLA ^property[555].code = #child 
* #SALMONELLA ^property[555].valueCode = #GOOD 
* #SALMONELLA ^property[556].code = #child 
* #SALMONELLA ^property[556].valueCode = #GORI 
* #SALMONELLA ^property[557].code = #child 
* #SALMONELLA ^property[557].valueCode = #GOULFEY 
* #SALMONELLA ^property[558].code = #child 
* #SALMONELLA ^property[558].valueCode = #GOULOUMBO 
* #SALMONELLA ^property[559].code = #child 
* #SALMONELLA ^property[559].valueCode = #GOVERDHAN 
* #SALMONELLA ^property[560].code = #child 
* #SALMONELLA ^property[560].valueCode = #GOZO 
* #SALMONELLA ^property[561].code = #child 
* #SALMONELLA ^property[561].valueCode = #GRAMPIAN 
* #SALMONELLA ^property[562].code = #child 
* #SALMONELLA ^property[562].valueCode = #GRANCANARIA 
* #SALMONELLA ^property[563].code = #child 
* #SALMONELLA ^property[563].valueCode = #GRANDHAVEN 
* #SALMONELLA ^property[564].code = #child 
* #SALMONELLA ^property[564].valueCode = #GRANLO 
* #SALMONELLA ^property[565].code = #child 
* #SALMONELLA ^property[565].valueCode = #GRAZ 
* #SALMONELLA ^property[566].code = #child 
* #SALMONELLA ^property[566].valueCode = #GREIZ 
* #SALMONELLA ^property[567].code = #child 
* #SALMONELLA ^property[567].valueCode = #GROENEKAN 
* #SALMONELLA ^property[568].code = #child 
* #SALMONELLA ^property[568].valueCode = #GROUP11 
* #SALMONELLA ^property[569].code = #child 
* #SALMONELLA ^property[569].valueCode = #GROUP13 
* #SALMONELLA ^property[570].code = #child 
* #SALMONELLA ^property[570].valueCode = #GROUP16 
* #SALMONELLA ^property[571].code = #child 
* #SALMONELLA ^property[571].valueCode = #GROUP17 
* #SALMONELLA ^property[572].code = #child 
* #SALMONELLA ^property[572].valueCode = #GROUP18 
* #SALMONELLA ^property[573].code = #child 
* #SALMONELLA ^property[573].valueCode = #GROUP1_3_19 
* #SALMONELLA ^property[574].code = #child 
* #SALMONELLA ^property[574].valueCode = #GROUP2 
* #SALMONELLA ^property[575].code = #child 
* #SALMONELLA ^property[575].valueCode = #GROUP21 
* #SALMONELLA ^property[576].code = #child 
* #SALMONELLA ^property[576].valueCode = #GROUP28 
* #SALMONELLA ^property[577].code = #child 
* #SALMONELLA ^property[577].valueCode = #GROUP30 
* #SALMONELLA ^property[578].code = #child 
* #SALMONELLA ^property[578].valueCode = #GROUP35 
* #SALMONELLA ^property[579].code = #child 
* #SALMONELLA ^property[579].valueCode = #GROUP38 
* #SALMONELLA ^property[580].code = #child 
* #SALMONELLA ^property[580].valueCode = #GROUP39 
* #SALMONELLA ^property[581].code = #child 
* #SALMONELLA ^property[581].valueCode = #GROUP3_10 
* #SALMONELLA ^property[582].code = #child 
* #SALMONELLA ^property[582].valueCode = #GROUP4 
* #SALMONELLA ^property[583].code = #child 
* #SALMONELLA ^property[583].valueCode = #GROUP40 
* #SALMONELLA ^property[584].code = #child 
* #SALMONELLA ^property[584].valueCode = #GROUP41 
* #SALMONELLA ^property[585].code = #child 
* #SALMONELLA ^property[585].valueCode = #GROUP42 
* #SALMONELLA ^property[586].code = #child 
* #SALMONELLA ^property[586].valueCode = #GROUP43 
* #SALMONELLA ^property[587].code = #child 
* #SALMONELLA ^property[587].valueCode = #GROUP44 
* #SALMONELLA ^property[588].code = #child 
* #SALMONELLA ^property[588].valueCode = #GROUP45 
* #SALMONELLA ^property[589].code = #child 
* #SALMONELLA ^property[589].valueCode = #GROUP47 
* #SALMONELLA ^property[590].code = #child 
* #SALMONELLA ^property[590].valueCode = #GROUP48 
* #SALMONELLA ^property[591].code = #child 
* #SALMONELLA ^property[591].valueCode = #GROUP50 
* #SALMONELLA ^property[592].code = #child 
* #SALMONELLA ^property[592].valueCode = #GROUP51 
* #SALMONELLA ^property[593].code = #child 
* #SALMONELLA ^property[593].valueCode = #GROUP52 
* #SALMONELLA ^property[594].code = #child 
* #SALMONELLA ^property[594].valueCode = #GROUP53 
* #SALMONELLA ^property[595].code = #child 
* #SALMONELLA ^property[595].valueCode = #GROUP54 
* #SALMONELLA ^property[596].code = #child 
* #SALMONELLA ^property[596].valueCode = #GROUP55 
* #SALMONELLA ^property[597].code = #child 
* #SALMONELLA ^property[597].valueCode = #GROUP56 
* #SALMONELLA ^property[598].code = #child 
* #SALMONELLA ^property[598].valueCode = #GROUP57 
* #SALMONELLA ^property[599].code = #child 
* #SALMONELLA ^property[599].valueCode = #GROUP58 
* #SALMONELLA ^property[600].code = #child 
* #SALMONELLA ^property[600].valueCode = #GROUP59 
* #SALMONELLA ^property[601].code = #child 
* #SALMONELLA ^property[601].valueCode = #GROUP60 
* #SALMONELLA ^property[602].code = #child 
* #SALMONELLA ^property[602].valueCode = #GROUP61 
* #SALMONELLA ^property[603].code = #child 
* #SALMONELLA ^property[603].valueCode = #GROUP62 
* #SALMONELLA ^property[604].code = #child 
* #SALMONELLA ^property[604].valueCode = #GROUP63 
* #SALMONELLA ^property[605].code = #child 
* #SALMONELLA ^property[605].valueCode = #GROUP65 
* #SALMONELLA ^property[606].code = #child 
* #SALMONELLA ^property[606].valueCode = #GROUP66 
* #SALMONELLA ^property[607].code = #child 
* #SALMONELLA ^property[607].valueCode = #GROUP67 
* #SALMONELLA ^property[608].code = #child 
* #SALMONELLA ^property[608].valueCode = #GROUP6_14 
* #SALMONELLA ^property[609].code = #child 
* #SALMONELLA ^property[609].valueCode = #GROUP7 
* #SALMONELLA ^property[610].code = #child 
* #SALMONELLA ^property[610].valueCode = #GROUP8 
* #SALMONELLA ^property[611].code = #child 
* #SALMONELLA ^property[611].valueCode = #GROUP9 
* #SALMONELLA ^property[612].code = #child 
* #SALMONELLA ^property[612].valueCode = #GROUP9_46 
* #SALMONELLA ^property[613].code = #child 
* #SALMONELLA ^property[613].valueCode = #GROUP9_46_27 
* #SALMONELLA ^property[614].code = #child 
* #SALMONELLA ^property[614].valueCode = #GROUPA 
* #SALMONELLA ^property[615].code = #child 
* #SALMONELLA ^property[615].valueCode = #GROUPB 
* #SALMONELLA ^property[616].code = #child 
* #SALMONELLA ^property[616].valueCode = #GROUPC 
* #SALMONELLA ^property[617].code = #child 
* #SALMONELLA ^property[617].valueCode = #GROUPC1 
* #SALMONELLA ^property[618].code = #child 
* #SALMONELLA ^property[618].valueCode = #GROUPC2 
* #SALMONELLA ^property[619].code = #child 
* #SALMONELLA ^property[619].valueCode = #GROUPD 
* #SALMONELLA ^property[620].code = #child 
* #SALMONELLA ^property[620].valueCode = #GROUPD1 
* #SALMONELLA ^property[621].code = #child 
* #SALMONELLA ^property[621].valueCode = #GROUPD2 
* #SALMONELLA ^property[622].code = #child 
* #SALMONELLA ^property[622].valueCode = #GROUPD3 
* #SALMONELLA ^property[623].code = #child 
* #SALMONELLA ^property[623].valueCode = #GROUPE 
* #SALMONELLA ^property[624].code = #child 
* #SALMONELLA ^property[624].valueCode = #GROUPE1 
* #SALMONELLA ^property[625].code = #child 
* #SALMONELLA ^property[625].valueCode = #GROUPE4 
* #SALMONELLA ^property[626].code = #child 
* #SALMONELLA ^property[626].valueCode = #GROUPG 
* #SALMONELLA ^property[627].code = #child 
* #SALMONELLA ^property[627].valueCode = #GROUPH 
* #SALMONELLA ^property[628].code = #child 
* #SALMONELLA ^property[628].valueCode = #GROUPI 
* #SALMONELLA ^property[629].code = #child 
* #SALMONELLA ^property[629].valueCode = #GROUPJ 
* #SALMONELLA ^property[630].code = #child 
* #SALMONELLA ^property[630].valueCode = #GROUPK 
* #SALMONELLA ^property[631].code = #child 
* #SALMONELLA ^property[631].valueCode = #GROUPL 
* #SALMONELLA ^property[632].code = #child 
* #SALMONELLA ^property[632].valueCode = #GROUPM 
* #SALMONELLA ^property[633].code = #child 
* #SALMONELLA ^property[633].valueCode = #GROUPN 
* #SALMONELLA ^property[634].code = #child 
* #SALMONELLA ^property[634].valueCode = #GROUPO 
* #SALMONELLA ^property[635].code = #child 
* #SALMONELLA ^property[635].valueCode = #GROUPP 
* #SALMONELLA ^property[636].code = #child 
* #SALMONELLA ^property[636].valueCode = #GROUPQ 
* #SALMONELLA ^property[637].code = #child 
* #SALMONELLA ^property[637].valueCode = #GROUPR 
* #SALMONELLA ^property[638].code = #child 
* #SALMONELLA ^property[638].valueCode = #GROUPS 
* #SALMONELLA ^property[639].code = #child 
* #SALMONELLA ^property[639].valueCode = #GROUPT 
* #SALMONELLA ^property[640].code = #child 
* #SALMONELLA ^property[640].valueCode = #GROUPU 
* #SALMONELLA ^property[641].code = #child 
* #SALMONELLA ^property[641].valueCode = #GROUPV 
* #SALMONELLA ^property[642].code = #child 
* #SALMONELLA ^property[642].valueCode = #GROUPW 
* #SALMONELLA ^property[643].code = #child 
* #SALMONELLA ^property[643].valueCode = #GROUPX 
* #SALMONELLA ^property[644].code = #child 
* #SALMONELLA ^property[644].valueCode = #GROUPY 
* #SALMONELLA ^property[645].code = #child 
* #SALMONELLA ^property[645].valueCode = #GROUPZ 
* #SALMONELLA ^property[646].code = #child 
* #SALMONELLA ^property[646].valueCode = #GRUMPENSIS 
* #SALMONELLA ^property[647].code = #child 
* #SALMONELLA ^property[647].valueCode = #GUARAPIRANGA 
* #SALMONELLA ^property[648].code = #child 
* #SALMONELLA ^property[648].valueCode = #GUERIN 
* #SALMONELLA ^property[649].code = #child 
* #SALMONELLA ^property[649].valueCode = #GUEULETAPEE 
* #SALMONELLA ^property[650].code = #child 
* #SALMONELLA ^property[650].valueCode = #GUILDFORD 
* #SALMONELLA ^property[651].code = #child 
* #SALMONELLA ^property[651].valueCode = #GUINEA 
* #SALMONELLA ^property[652].code = #child 
* #SALMONELLA ^property[652].valueCode = #GUSTAVIA 
* #SALMONELLA ^property[653].code = #child 
* #SALMONELLA ^property[653].valueCode = #GWALE 
* #SALMONELLA ^property[654].code = #child 
* #SALMONELLA ^property[654].valueCode = #GWOZA 
* #SALMONELLA ^property[655].code = #child 
* #SALMONELLA ^property[655].valueCode = #HAARDT 
* #SALMONELLA ^property[656].code = #child 
* #SALMONELLA ^property[656].valueCode = #HADAR 
* #SALMONELLA ^property[657].code = #child 
* #SALMONELLA ^property[657].valueCode = #HADEJIA 
* #SALMONELLA ^property[658].code = #child 
* #SALMONELLA ^property[658].valueCode = #HADUNA 
* #SALMONELLA ^property[659].code = #child 
* #SALMONELLA ^property[659].valueCode = #HAELSINGBORG 
* #SALMONELLA ^property[660].code = #child 
* #SALMONELLA ^property[660].valueCode = #HAFERBREITE 
* #SALMONELLA ^property[661].code = #child 
* #SALMONELLA ^property[661].valueCode = #HAGA 
* #SALMONELLA ^property[662].code = #child 
* #SALMONELLA ^property[662].valueCode = #HAIFA 
* #SALMONELLA ^property[663].code = #child 
* #SALMONELLA ^property[663].valueCode = #HALLE 
* #SALMONELLA ^property[664].code = #child 
* #SALMONELLA ^property[664].valueCode = #HALLFOLD 
* #SALMONELLA ^property[665].code = #child 
* #SALMONELLA ^property[665].valueCode = #HANDEN 
* #SALMONELLA ^property[666].code = #child 
* #SALMONELLA ^property[666].valueCode = #HANN 
* #SALMONELLA ^property[667].code = #child 
* #SALMONELLA ^property[667].valueCode = #HANNOVER 
* #SALMONELLA ^property[668].code = #child 
* #SALMONELLA ^property[668].valueCode = #HAOUARIA 
* #SALMONELLA ^property[669].code = #child 
* #SALMONELLA ^property[669].valueCode = #HARBURG 
* #SALMONELLA ^property[670].code = #child 
* #SALMONELLA ^property[670].valueCode = #HARCOURT 
* #SALMONELLA ^property[671].code = #child 
* #SALMONELLA ^property[671].valueCode = #HARLEYSTREET 
* #SALMONELLA ^property[672].code = #child 
* #SALMONELLA ^property[672].valueCode = #HARRISONBURG 
* #SALMONELLA ^property[673].code = #child 
* #SALMONELLA ^property[673].valueCode = #HARTFORD 
* #SALMONELLA ^property[674].code = #child 
* #SALMONELLA ^property[674].valueCode = #HARVESTEHUDE 
* #SALMONELLA ^property[675].code = #child 
* #SALMONELLA ^property[675].valueCode = #HATFIELD 
* #SALMONELLA ^property[676].code = #child 
* #SALMONELLA ^property[676].valueCode = #HATO 
* #SALMONELLA ^property[677].code = #child 
* #SALMONELLA ^property[677].valueCode = #HAVANA 
* #SALMONELLA ^property[678].code = #child 
* #SALMONELLA ^property[678].valueCode = #HAYINDOGO 
* #SALMONELLA ^property[679].code = #child 
* #SALMONELLA ^property[679].valueCode = #HEERLEN 
* #SALMONELLA ^property[680].code = #child 
* #SALMONELLA ^property[680].valueCode = #HEGAU 
* #SALMONELLA ^property[681].code = #child 
* #SALMONELLA ^property[681].valueCode = #HEIDELBERG 
* #SALMONELLA ^property[682].code = #child 
* #SALMONELLA ^property[682].valueCode = #HEISTOPDENBERG 
* #SALMONELLA ^property[683].code = #child 
* #SALMONELLA ^property[683].valueCode = #HEMINGFORD 
* #SALMONELLA ^property[684].code = #child 
* #SALMONELLA ^property[684].valueCode = #HENNEKAMP 
* #SALMONELLA ^property[685].code = #child 
* #SALMONELLA ^property[685].valueCode = #HERMANNSWERDER 
* #SALMONELLA ^property[686].code = #child 
* #SALMONELLA ^property[686].valueCode = #HERON 
* #SALMONELLA ^property[687].code = #child 
* #SALMONELLA ^property[687].valueCode = #HERSTON 
* #SALMONELLA ^property[688].code = #child 
* #SALMONELLA ^property[688].valueCode = #HERZLIYA 
* #SALMONELLA ^property[689].code = #child 
* #SALMONELLA ^property[689].valueCode = #HESSAREK 
* #SALMONELLA ^property[690].code = #child 
* #SALMONELLA ^property[690].valueCode = #HIDALGO 
* #SALMONELLA ^property[691].code = #child 
* #SALMONELLA ^property[691].valueCode = #HIDUDDIFY 
* #SALMONELLA ^property[692].code = #child 
* #SALMONELLA ^property[692].valueCode = #HILLEGERSBERG 
* #SALMONELLA ^property[693].code = #child 
* #SALMONELLA ^property[693].valueCode = #HILLINGDON 
* #SALMONELLA ^property[694].code = #child 
* #SALMONELLA ^property[694].valueCode = #HILLSBOROUGH 
* #SALMONELLA ^property[695].code = #child 
* #SALMONELLA ^property[695].valueCode = #HILVERSUM 
* #SALMONELLA ^property[696].code = #child 
* #SALMONELLA ^property[696].valueCode = #HINDMARSH 
* #SALMONELLA ^property[697].code = #child 
* #SALMONELLA ^property[697].valueCode = #HISINGEN 
* #SALMONELLA ^property[698].code = #child 
* #SALMONELLA ^property[698].valueCode = #HISSAR 
* #SALMONELLA ^property[699].code = #child 
* #SALMONELLA ^property[699].valueCode = #HITHERGREEN 
* #SALMONELLA ^property[700].code = #child 
* #SALMONELLA ^property[700].valueCode = #HOBOKEN 
* #SALMONELLA ^property[701].code = #child 
* #SALMONELLA ^property[701].valueCode = #HOFIT 
* #SALMONELLA ^property[702].code = #child 
* #SALMONELLA ^property[702].valueCode = #HOGHTON 
* #SALMONELLA ^property[703].code = #child 
* #SALMONELLA ^property[703].valueCode = #HOHENTWIEL 
* #SALMONELLA ^property[704].code = #child 
* #SALMONELLA ^property[704].valueCode = #HOLCOMB 
* #SALMONELLA ^property[705].code = #child 
* #SALMONELLA ^property[705].valueCode = #HOMOSASSA 
* #SALMONELLA ^property[706].code = #child 
* #SALMONELLA ^property[706].valueCode = #HONELIS 
* #SALMONELLA ^property[707].code = #child 
* #SALMONELLA ^property[707].valueCode = #HONGKONG 
* #SALMONELLA ^property[708].code = #child 
* #SALMONELLA ^property[708].valueCode = #HORSHAM 
* #SALMONELLA ^property[709].code = #child 
* #SALMONELLA ^property[709].valueCode = #HOUSTON 
* #SALMONELLA ^property[710].code = #child 
* #SALMONELLA ^property[710].valueCode = #HUDDINGE 
* #SALMONELLA ^property[711].code = #child 
* #SALMONELLA ^property[711].valueCode = #HUETTWILEN 
* #SALMONELLA ^property[712].code = #child 
* #SALMONELLA ^property[712].valueCode = #HULL 
* #SALMONELLA ^property[713].code = #child 
* #SALMONELLA ^property[713].valueCode = #HUVUDSTA 
* #SALMONELLA ^property[714].code = #child 
* #SALMONELLA ^property[714].valueCode = #HVITTINGFOSS 
* #SALMONELLA ^property[715].code = #child 
* #SALMONELLA ^property[715].valueCode = #HYDRA 
* #SALMONELLA ^property[716].code = #child 
* #SALMONELLA ^property[716].valueCode = #IBADAN 
* #SALMONELLA ^property[717].code = #child 
* #SALMONELLA ^property[717].valueCode = #IBARAGI 
* #SALMONELLA ^property[718].code = #child 
* #SALMONELLA ^property[718].valueCode = #IDIKAN 
* #SALMONELLA ^property[719].code = #child 
* #SALMONELLA ^property[719].valueCode = #IKAYI 
* #SALMONELLA ^property[720].code = #child 
* #SALMONELLA ^property[720].valueCode = #IKEJA 
* #SALMONELLA ^property[721].code = #child 
* #SALMONELLA ^property[721].valueCode = #ILALA 
* #SALMONELLA ^property[722].code = #child 
* #SALMONELLA ^property[722].valueCode = #ILUGUN 
* #SALMONELLA ^property[723].code = #child 
* #SALMONELLA ^property[723].valueCode = #IMO 
* #SALMONELLA ^property[724].code = #child 
* #SALMONELLA ^property[724].valueCode = #INCHPARK 
* #SALMONELLA ^property[725].code = #child 
* #SALMONELLA ^property[725].valueCode = #INDIA 
* #SALMONELLA ^property[726].code = #child 
* #SALMONELLA ^property[726].valueCode = #INDIANA 
* #SALMONELLA ^property[727].code = #child 
* #SALMONELLA ^property[727].valueCode = #INFANTIS 
* #SALMONELLA ^property[728].code = #child 
* #SALMONELLA ^property[728].valueCode = #INGANDA 
* #SALMONELLA ^property[729].code = #child 
* #SALMONELLA ^property[729].valueCode = #INGLIS 
* #SALMONELLA ^property[730].code = #child 
* #SALMONELLA ^property[730].valueCode = #INPRAW 
* #SALMONELLA ^property[731].code = #child 
* #SALMONELLA ^property[731].valueCode = #INVERNESS 
* #SALMONELLA ^property[732].code = #child 
* #SALMONELLA ^property[732].valueCode = #IPEKO 
* #SALMONELLA ^property[733].code = #child 
* #SALMONELLA ^property[733].valueCode = #IPSWICH 
* #SALMONELLA ^property[734].code = #child 
* #SALMONELLA ^property[734].valueCode = #IRCHEL 
* #SALMONELLA ^property[735].code = #child 
* #SALMONELLA ^property[735].valueCode = #IRENEA 
* #SALMONELLA ^property[736].code = #child 
* #SALMONELLA ^property[736].valueCode = #IRIGNY 
* #SALMONELLA ^property[737].code = #child 
* #SALMONELLA ^property[737].valueCode = #IRUMU 
* #SALMONELLA ^property[738].code = #child 
* #SALMONELLA ^property[738].valueCode = #ISANGI 
* #SALMONELLA ^property[739].code = #child 
* #SALMONELLA ^property[739].valueCode = #ISASZEG 
* #SALMONELLA ^property[740].code = #child 
* #SALMONELLA ^property[740].valueCode = #ISRAEL 
* #SALMONELLA ^property[741].code = #child 
* #SALMONELLA ^property[741].valueCode = #ISTANBUL 
* #SALMONELLA ^property[742].code = #child 
* #SALMONELLA ^property[742].valueCode = #ISTORIA 
* #SALMONELLA ^property[743].code = #child 
* #SALMONELLA ^property[743].valueCode = #ISUGE 
* #SALMONELLA ^property[744].code = #child 
* #SALMONELLA ^property[744].valueCode = #ITAMI 
* #SALMONELLA ^property[745].code = #child 
* #SALMONELLA ^property[745].valueCode = #ITURI 
* #SALMONELLA ^property[746].code = #child 
* #SALMONELLA ^property[746].valueCode = #ITUTABA 
* #SALMONELLA ^property[747].code = #child 
* #SALMONELLA ^property[747].valueCode = #IVORY 
* #SALMONELLA ^property[748].code = #child 
* #SALMONELLA ^property[748].valueCode = #IVORYCOAST 
* #SALMONELLA ^property[749].code = #child 
* #SALMONELLA ^property[749].valueCode = #IVRYSURSEINE 
* #SALMONELLA ^property[750].code = #child 
* #SALMONELLA ^property[750].valueCode = #JAFFNA 
* #SALMONELLA ^property[751].code = #child 
* #SALMONELLA ^property[751].valueCode = #JALISCO 
* #SALMONELLA ^property[752].code = #child 
* #SALMONELLA ^property[752].valueCode = #JAMAICA 
* #SALMONELLA ^property[753].code = #child 
* #SALMONELLA ^property[753].valueCode = #JAMBUR 
* #SALMONELLA ^property[754].code = #child 
* #SALMONELLA ^property[754].valueCode = #JANGWANI 
* #SALMONELLA ^property[755].code = #child 
* #SALMONELLA ^property[755].valueCode = #JAVA 
* #SALMONELLA ^property[756].code = #child 
* #SALMONELLA ^property[756].valueCode = #JAVIANA 
* #SALMONELLA ^property[757].code = #child 
* #SALMONELLA ^property[757].valueCode = #JEDBURGH 
* #SALMONELLA ^property[758].code = #child 
* #SALMONELLA ^property[758].valueCode = #JERICHO 
* #SALMONELLA ^property[759].code = #child 
* #SALMONELLA ^property[759].valueCode = #JERUSALEM 
* #SALMONELLA ^property[760].code = #child 
* #SALMONELLA ^property[760].valueCode = #JOAL 
* #SALMONELLA ^property[761].code = #child 
* #SALMONELLA ^property[761].valueCode = #JODHPUR 
* #SALMONELLA ^property[762].code = #child 
* #SALMONELLA ^property[762].valueCode = #JOHANNESBURG 
* #SALMONELLA ^property[763].code = #child 
* #SALMONELLA ^property[763].valueCode = #JOS 
* #SALMONELLA ^property[764].code = #child 
* #SALMONELLA ^property[764].valueCode = #JUBA 
* #SALMONELLA ^property[765].code = #child 
* #SALMONELLA ^property[765].valueCode = #JUBILEE 
* #SALMONELLA ^property[766].code = #child 
* #SALMONELLA ^property[766].valueCode = #JUKESTOWN 
* #SALMONELLA ^property[767].code = #child 
* #SALMONELLA ^property[767].valueCode = #KAAPSTAD 
* #SALMONELLA ^property[768].code = #child 
* #SALMONELLA ^property[768].valueCode = #KABETE 
* #SALMONELLA ^property[769].code = #child 
* #SALMONELLA ^property[769].valueCode = #KADUNA 
* #SALMONELLA ^property[770].code = #child 
* #SALMONELLA ^property[770].valueCode = #KAEVLINGE 
* #SALMONELLA ^property[771].code = #child 
* #SALMONELLA ^property[771].valueCode = #KAHLA 
* #SALMONELLA ^property[772].code = #child 
* #SALMONELLA ^property[772].valueCode = #KAINJI 
* #SALMONELLA ^property[773].code = #child 
* #SALMONELLA ^property[773].valueCode = #KAITAAN 
* #SALMONELLA ^property[774].code = #child 
* #SALMONELLA ^property[774].valueCode = #KALAMU 
* #SALMONELLA ^property[775].code = #child 
* #SALMONELLA ^property[775].valueCode = #KALINA 
* #SALMONELLA ^property[776].code = #child 
* #SALMONELLA ^property[776].valueCode = #KALLO 
* #SALMONELLA ^property[777].code = #child 
* #SALMONELLA ^property[777].valueCode = #KALUMBURU 
* #SALMONELLA ^property[778].code = #child 
* #SALMONELLA ^property[778].valueCode = #KAMBOLE 
* #SALMONELLA ^property[779].code = #child 
* #SALMONELLA ^property[779].valueCode = #KAMORU 
* #SALMONELLA ^property[780].code = #child 
* #SALMONELLA ^property[780].valueCode = #KAMPALA 
* #SALMONELLA ^property[781].code = #child 
* #SALMONELLA ^property[781].valueCode = #KANDE 
* #SALMONELLA ^property[782].code = #child 
* #SALMONELLA ^property[782].valueCode = #KANDLA 
* #SALMONELLA ^property[783].code = #child 
* #SALMONELLA ^property[783].valueCode = #KANESHIE 
* #SALMONELLA ^property[784].code = #child 
* #SALMONELLA ^property[784].valueCode = #KANIFING 
* #SALMONELLA ^property[785].code = #child 
* #SALMONELLA ^property[785].valueCode = #KANO 
* #SALMONELLA ^property[786].code = #child 
* #SALMONELLA ^property[786].valueCode = #KAOLACK 
* #SALMONELLA ^property[787].code = #child 
* #SALMONELLA ^property[787].valueCode = #KAPEMBA 
* #SALMONELLA ^property[788].code = #child 
* #SALMONELLA ^property[788].valueCode = #KARACHI 
* #SALMONELLA ^property[789].code = #child 
* #SALMONELLA ^property[789].valueCode = #KARAMOJA 
* #SALMONELLA ^property[790].code = #child 
* #SALMONELLA ^property[790].valueCode = #KARAYA 
* #SALMONELLA ^property[791].code = #child 
* #SALMONELLA ^property[791].valueCode = #KARLSHAMN 
* #SALMONELLA ^property[792].code = #child 
* #SALMONELLA ^property[792].valueCode = #KASENYI 
* #SALMONELLA ^property[793].code = #child 
* #SALMONELLA ^property[793].valueCode = #KASSBERG 
* #SALMONELLA ^property[794].code = #child 
* #SALMONELLA ^property[794].valueCode = #KASSEL 
* #SALMONELLA ^property[795].code = #child 
* #SALMONELLA ^property[795].valueCode = #KASTRUP 
* #SALMONELLA ^property[796].code = #child 
* #SALMONELLA ^property[796].valueCode = #KEDOUGOU 
* #SALMONELLA ^property[797].code = #child 
* #SALMONELLA ^property[797].valueCode = #KENTUCKY 
* #SALMONELLA ^property[798].code = #child 
* #SALMONELLA ^property[798].valueCode = #KENYA 
* #SALMONELLA ^property[799].code = #child 
* #SALMONELLA ^property[799].valueCode = #KERMEL 
* #SALMONELLA ^property[800].code = #child 
* #SALMONELLA ^property[800].valueCode = #KETHIABARNY 
* #SALMONELLA ^property[801].code = #child 
* #SALMONELLA ^property[801].valueCode = #KEURMASSAR 
* #SALMONELLA ^property[802].code = #child 
* #SALMONELLA ^property[802].valueCode = #KEVE 
* #SALMONELLA ^property[803].code = #child 
* #SALMONELLA ^property[803].valueCode = #KIAMBU 
* #SALMONELLA ^property[804].code = #child 
* #SALMONELLA ^property[804].valueCode = #KIBI 
* #SALMONELLA ^property[805].code = #child 
* #SALMONELLA ^property[805].valueCode = #KIBUSI 
* #SALMONELLA ^property[806].code = #child 
* #SALMONELLA ^property[806].valueCode = #KIDDERMINSTER 
* #SALMONELLA ^property[807].code = #child 
* #SALMONELLA ^property[807].valueCode = #KIEL 
* #SALMONELLA ^property[808].code = #child 
* #SALMONELLA ^property[808].valueCode = #KIKOMA 
* #SALMONELLA ^property[809].code = #child 
* #SALMONELLA ^property[809].valueCode = #KIMBERLEY 
* #SALMONELLA ^property[810].code = #child 
* #SALMONELLA ^property[810].valueCode = #KIMPESE 
* #SALMONELLA ^property[811].code = #child 
* #SALMONELLA ^property[811].valueCode = #KIMUENZA 
* #SALMONELLA ^property[812].code = #child 
* #SALMONELLA ^property[812].valueCode = #KINDIA 
* #SALMONELLA ^property[813].code = #child 
* #SALMONELLA ^property[813].valueCode = #KINGABWA 
* #SALMONELLA ^property[814].code = #child 
* #SALMONELLA ^property[814].valueCode = #KINGSTON 
* #SALMONELLA ^property[815].code = #child 
* #SALMONELLA ^property[815].valueCode = #KINONDONI 
* #SALMONELLA ^property[816].code = #child 
* #SALMONELLA ^property[816].valueCode = #KINSON 
* #SALMONELLA ^property[817].code = #child 
* #SALMONELLA ^property[817].valueCode = #KINTAMBO 
* #SALMONELLA ^property[818].code = #child 
* #SALMONELLA ^property[818].valueCode = #KIRKEE 
* #SALMONELLA ^property[819].code = #child 
* #SALMONELLA ^property[819].valueCode = #KISANGANI 
* #SALMONELLA ^property[820].code = #child 
* #SALMONELLA ^property[820].valueCode = #KISARAWE 
* #SALMONELLA ^property[821].code = #child 
* #SALMONELLA ^property[821].valueCode = #KISII 
* #SALMONELLA ^property[822].code = #child 
* #SALMONELLA ^property[822].valueCode = #KITENGE 
* #SALMONELLA ^property[823].code = #child 
* #SALMONELLA ^property[823].valueCode = #KIVU 
* #SALMONELLA ^property[824].code = #child 
* #SALMONELLA ^property[824].valueCode = #KLOUTO 
* #SALMONELLA ^property[825].code = #child 
* #SALMONELLA ^property[825].valueCode = #KOBLENZ 
* #SALMONELLA ^property[826].code = #child 
* #SALMONELLA ^property[826].valueCode = #KODJOVI 
* #SALMONELLA ^property[827].code = #child 
* #SALMONELLA ^property[827].valueCode = #KOENIGSTUHL 
* #SALMONELLA ^property[828].code = #child 
* #SALMONELLA ^property[828].valueCode = #KOESSEN 
* #SALMONELLA ^property[829].code = #child 
* #SALMONELLA ^property[829].valueCode = #KOFANDOKA 
* #SALMONELLA ^property[830].code = #child 
* #SALMONELLA ^property[830].valueCode = #KOKETIME 
* #SALMONELLA ^property[831].code = #child 
* #SALMONELLA ^property[831].valueCode = #KOKOLI 
* #SALMONELLA ^property[832].code = #child 
* #SALMONELLA ^property[832].valueCode = #KOKOMLEMLE 
* #SALMONELLA ^property[833].code = #child 
* #SALMONELLA ^property[833].valueCode = #KOLAR 
* #SALMONELLA ^property[834].code = #child 
* #SALMONELLA ^property[834].valueCode = #KOLDA 
* #SALMONELLA ^property[835].code = #child 
* #SALMONELLA ^property[835].valueCode = #KONOLFINGEN 
* #SALMONELLA ^property[836].code = #child 
* #SALMONELLA ^property[836].valueCode = #KONONGO 
* #SALMONELLA ^property[837].code = #child 
* #SALMONELLA ^property[837].valueCode = #KONSTANZ 
* #SALMONELLA ^property[838].code = #child 
* #SALMONELLA ^property[838].valueCode = #KORBOL 
* #SALMONELLA ^property[839].code = #child 
* #SALMONELLA ^property[839].valueCode = #KORKEASAARI 
* #SALMONELLA ^property[840].code = #child 
* #SALMONELLA ^property[840].valueCode = #KORLEBU 
* #SALMONELLA ^property[841].code = #child 
* #SALMONELLA ^property[841].valueCode = #KOROVI 
* #SALMONELLA ^property[842].code = #child 
* #SALMONELLA ^property[842].valueCode = #KORTRIJK 
* #SALMONELLA ^property[843].code = #child 
* #SALMONELLA ^property[843].valueCode = #KOTTBUS 
* #SALMONELLA ^property[844].code = #child 
* #SALMONELLA ^property[844].valueCode = #KOTTE 
* #SALMONELLA ^property[845].code = #child 
* #SALMONELLA ^property[845].valueCode = #KOTU 
* #SALMONELLA ^property[846].code = #child 
* #SALMONELLA ^property[846].valueCode = #KOUKA 
* #SALMONELLA ^property[847].code = #child 
* #SALMONELLA ^property[847].valueCode = #KOUMRA 
* #SALMONELLA ^property[848].code = #child 
* #SALMONELLA ^property[848].valueCode = #KPEME 
* #SALMONELLA ^property[849].code = #child 
* #SALMONELLA ^property[849].valueCode = #KRALINGEN 
* #SALMONELLA ^property[850].code = #child 
* #SALMONELLA ^property[850].valueCode = #KREFELD 
* #SALMONELLA ^property[851].code = #child 
* #SALMONELLA ^property[851].valueCode = #KRISTIANSTAD 
* #SALMONELLA ^property[852].code = #child 
* #SALMONELLA ^property[852].valueCode = #KUA 
* #SALMONELLA ^property[853].code = #child 
* #SALMONELLA ^property[853].valueCode = #KUBACHA 
* #SALMONELLA ^property[854].code = #child 
* #SALMONELLA ^property[854].valueCode = #KUESSEL 
* #SALMONELLA ^property[855].code = #child 
* #SALMONELLA ^property[855].valueCode = #KUMASI 
* #SALMONELLA ^property[856].code = #child 
* #SALMONELLA ^property[856].valueCode = #KUNDUCHI 
* #SALMONELLA ^property[857].code = #child 
* #SALMONELLA ^property[857].valueCode = #KUNTAIR 
* #SALMONELLA ^property[858].code = #child 
* #SALMONELLA ^property[858].valueCode = #KURU 
* #SALMONELLA ^property[859].code = #child 
* #SALMONELLA ^property[859].valueCode = #LABADI 
* #SALMONELLA ^property[860].code = #child 
* #SALMONELLA ^property[860].valueCode = #LAGOS 
* #SALMONELLA ^property[861].code = #child 
* #SALMONELLA ^property[861].valueCode = #LAMBERHURST 
* #SALMONELLA ^property[862].code = #child 
* #SALMONELLA ^property[862].valueCode = #LAMIN 
* #SALMONELLA ^property[863].code = #child 
* #SALMONELLA ^property[863].valueCode = #LAMPHUN 
* #SALMONELLA ^property[864].code = #child 
* #SALMONELLA ^property[864].valueCode = #LANCASTER 
* #SALMONELLA ^property[865].code = #child 
* #SALMONELLA ^property[865].valueCode = #LANDALA 
* #SALMONELLA ^property[866].code = #child 
* #SALMONELLA ^property[866].valueCode = #LANDAU 
* #SALMONELLA ^property[867].code = #child 
* #SALMONELLA ^property[867].valueCode = #LANDWASSER 
* #SALMONELLA ^property[868].code = #child 
* #SALMONELLA ^property[868].valueCode = #LANGENHORN 
* #SALMONELLA ^property[869].code = #child 
* #SALMONELLA ^property[869].valueCode = #LANGENSALZA 
* #SALMONELLA ^property[870].code = #child 
* #SALMONELLA ^property[870].valueCode = #LANGEVELD 
* #SALMONELLA ^property[871].code = #child 
* #SALMONELLA ^property[871].valueCode = #LANGFORD 
* #SALMONELLA ^property[872].code = #child 
* #SALMONELLA ^property[872].valueCode = #LANSING 
* #SALMONELLA ^property[873].code = #child 
* #SALMONELLA ^property[873].valueCode = #LAREDO 
* #SALMONELLA ^property[874].code = #child 
* #SALMONELLA ^property[874].valueCode = #LAROCHELLE 
* #SALMONELLA ^property[875].code = #child 
* #SALMONELLA ^property[875].valueCode = #LAROSE 
* #SALMONELLA ^property[876].code = #child 
* #SALMONELLA ^property[876].valueCode = #LATTENKAMP 
* #SALMONELLA ^property[877].code = #child 
* #SALMONELLA ^property[877].valueCode = #LAWNDALE 
* #SALMONELLA ^property[878].code = #child 
* #SALMONELLA ^property[878].valueCode = #LAWRA 
* #SALMONELLA ^property[879].code = #child 
* #SALMONELLA ^property[879].valueCode = #LEATHERHEAD 
* #SALMONELLA ^property[880].code = #child 
* #SALMONELLA ^property[880].valueCode = #LECHLER 
* #SALMONELLA ^property[881].code = #child 
* #SALMONELLA ^property[881].valueCode = #LEDA 
* #SALMONELLA ^property[882].code = #child 
* #SALMONELLA ^property[882].valueCode = #LEER 
* #SALMONELLA ^property[883].code = #child 
* #SALMONELLA ^property[883].valueCode = #LEEUWARDEN 
* #SALMONELLA ^property[884].code = #child 
* #SALMONELLA ^property[884].valueCode = #LEGON 
* #SALMONELLA ^property[885].code = #child 
* #SALMONELLA ^property[885].valueCode = #LEHRTE 
* #SALMONELLA ^property[886].code = #child 
* #SALMONELLA ^property[886].valueCode = #LEIDEN 
* #SALMONELLA ^property[887].code = #child 
* #SALMONELLA ^property[887].valueCode = #LEIPZIG 
* #SALMONELLA ^property[888].code = #child 
* #SALMONELLA ^property[888].valueCode = #LEITH 
* #SALMONELLA ^property[889].code = #child 
* #SALMONELLA ^property[889].valueCode = #LEKKE 
* #SALMONELLA ^property[890].code = #child 
* #SALMONELLA ^property[890].valueCode = #LENE 
* #SALMONELLA ^property[891].code = #child 
* #SALMONELLA ^property[891].valueCode = #LEOBEN 
* #SALMONELLA ^property[892].code = #child 
* #SALMONELLA ^property[892].valueCode = #LEOPOLDVILLE 
* #SALMONELLA ^property[893].code = #child 
* #SALMONELLA ^property[893].valueCode = #LERUM 
* #SALMONELLA ^property[894].code = #child 
* #SALMONELLA ^property[894].valueCode = #LEXINGTON 
* #SALMONELLA ^property[895].code = #child 
* #SALMONELLA ^property[895].valueCode = #LEXINGTON_VAR_15+ 
* #SALMONELLA ^property[896].code = #child 
* #SALMONELLA ^property[896].valueCode = #LEXINGTON_VAR_15+_34+ 
* #SALMONELLA ^property[897].code = #child 
* #SALMONELLA ^property[897].valueCode = #LEZENNES 
* #SALMONELLA ^property[898].code = #child 
* #SALMONELLA ^property[898].valueCode = #LIBREVILLE 
* #SALMONELLA ^property[899].code = #child 
* #SALMONELLA ^property[899].valueCode = #LIGEO 
* #SALMONELLA ^property[900].code = #child 
* #SALMONELLA ^property[900].valueCode = #LIGNA 
* #SALMONELLA ^property[901].code = #child 
* #SALMONELLA ^property[901].valueCode = #LIKA 
* #SALMONELLA ^property[902].code = #child 
* #SALMONELLA ^property[902].valueCode = #LILLE 
* #SALMONELLA ^property[903].code = #child 
* #SALMONELLA ^property[903].valueCode = #LILLE_VAR_14+ 
* #SALMONELLA ^property[904].code = #child 
* #SALMONELLA ^property[904].valueCode = #LIMETE 
* #SALMONELLA ^property[905].code = #child 
* #SALMONELLA ^property[905].valueCode = #LINDENBURG 
* #SALMONELLA ^property[906].code = #child 
* #SALMONELLA ^property[906].valueCode = #LINDERN 
* #SALMONELLA ^property[907].code = #child 
* #SALMONELLA ^property[907].valueCode = #LINDI 
* #SALMONELLA ^property[908].code = #child 
* #SALMONELLA ^property[908].valueCode = #LINGUERE 
* #SALMONELLA ^property[909].code = #child 
* #SALMONELLA ^property[909].valueCode = #LINGWALA 
* #SALMONELLA ^property[910].code = #child 
* #SALMONELLA ^property[910].valueCode = #LINTON 
* #SALMONELLA ^property[911].code = #child 
* #SALMONELLA ^property[911].valueCode = #LISBOA 
* #SALMONELLA ^property[912].code = #child 
* #SALMONELLA ^property[912].valueCode = #LISHABI 
* #SALMONELLA ^property[913].code = #child 
* #SALMONELLA ^property[913].valueCode = #LITCHFIELD 
* #SALMONELLA ^property[914].code = #child 
* #SALMONELLA ^property[914].valueCode = #LIVERPOOL 
* #SALMONELLA ^property[915].code = #child 
* #SALMONELLA ^property[915].valueCode = #LIVINGSTONE 
* #SALMONELLA ^property[916].code = #child 
* #SALMONELLA ^property[916].valueCode = #LIVINGSTONE_VAR_14+ 
* #SALMONELLA ^property[917].code = #child 
* #SALMONELLA ^property[917].valueCode = #LIVULU 
* #SALMONELLA ^property[918].code = #child 
* #SALMONELLA ^property[918].valueCode = #LJUBLJANA 
* #SALMONELLA ^property[919].code = #child 
* #SALMONELLA ^property[919].valueCode = #LLANDOFF 
* #SALMONELLA ^property[920].code = #child 
* #SALMONELLA ^property[920].valueCode = #LLOBREGAT 
* #SALMONELLA ^property[921].code = #child 
* #SALMONELLA ^property[921].valueCode = #LOANDA 
* #SALMONELLA ^property[922].code = #child 
* #SALMONELLA ^property[922].valueCode = #LOCKLEAZE 
* #SALMONELLA ^property[923].code = #child 
* #SALMONELLA ^property[923].valueCode = #LODE 
* #SALMONELLA ^property[924].code = #child 
* #SALMONELLA ^property[924].valueCode = #LODZ 
* #SALMONELLA ^property[925].code = #child 
* #SALMONELLA ^property[925].valueCode = #LOENGA 
* #SALMONELLA ^property[926].code = #child 
* #SALMONELLA ^property[926].valueCode = #LOGONE 
* #SALMONELLA ^property[927].code = #child 
* #SALMONELLA ^property[927].valueCode = #LOKOMO 
* #SALMONELLA ^property[928].code = #child 
* #SALMONELLA ^property[928].valueCode = #LOKSTEDT 
* #SALMONELLA ^property[929].code = #child 
* #SALMONELLA ^property[929].valueCode = #LOMALINDA 
* #SALMONELLA ^property[930].code = #child 
* #SALMONELLA ^property[930].valueCode = #LOME 
* #SALMONELLA ^property[931].code = #child 
* #SALMONELLA ^property[931].valueCode = #LOMITA 
* #SALMONELLA ^property[932].code = #child 
* #SALMONELLA ^property[932].valueCode = #LOMNAVA 
* #SALMONELLA ^property[933].code = #child 
* #SALMONELLA ^property[933].valueCode = #LONDON 
* #SALMONELLA ^property[934].code = #child 
* #SALMONELLA ^property[934].valueCode = #LONDON_VAR_15+_ 
* #SALMONELLA ^property[935].code = #child 
* #SALMONELLA ^property[935].valueCode = #LONESTAR 
* #SALMONELLA ^property[936].code = #child 
* #SALMONELLA ^property[936].valueCode = #LOSANGELES 
* #SALMONELLA ^property[937].code = #child 
* #SALMONELLA ^property[937].valueCode = #LOUBOMO 
* #SALMONELLA ^property[938].code = #child 
* #SALMONELLA ^property[938].valueCode = #LOUGA 
* #SALMONELLA ^property[939].code = #child 
* #SALMONELLA ^property[939].valueCode = #LOUISIANA 
* #SALMONELLA ^property[940].code = #child 
* #SALMONELLA ^property[940].valueCode = #LOVELACE 
* #SALMONELLA ^property[941].code = #child 
* #SALMONELLA ^property[941].valueCode = #LOWESTOFT 
* #SALMONELLA ^property[942].code = #child 
* #SALMONELLA ^property[942].valueCode = #LUBUMBASHI 
* #SALMONELLA ^property[943].code = #child 
* #SALMONELLA ^property[943].valueCode = #LUCIANA 
* #SALMONELLA ^property[944].code = #child 
* #SALMONELLA ^property[944].valueCode = #LUCKENWALDE 
* #SALMONELLA ^property[945].code = #child 
* #SALMONELLA ^property[945].valueCode = #LUEDINGHAUSEN 
* #SALMONELLA ^property[946].code = #child 
* #SALMONELLA ^property[946].valueCode = #LUKE 
* #SALMONELLA ^property[947].code = #child 
* #SALMONELLA ^property[947].valueCode = #LUND 
* #SALMONELLA ^property[948].code = #child 
* #SALMONELLA ^property[948].valueCode = #LUTETIA 
* #SALMONELLA ^property[949].code = #child 
* #SALMONELLA ^property[949].valueCode = #LYON 
* #SALMONELLA ^property[950].code = #child 
* #SALMONELLA ^property[950].valueCode = #MAASTRICHT 
* #SALMONELLA ^property[951].code = #child 
* #SALMONELLA ^property[951].valueCode = #MACALLEN 
* #SALMONELLA ^property[952].code = #child 
* #SALMONELLA ^property[952].valueCode = #MACCLESFIELD 
* #SALMONELLA ^property[953].code = #child 
* #SALMONELLA ^property[953].valueCode = #MACHAGA 
* #SALMONELLA ^property[954].code = #child 
* #SALMONELLA ^property[954].valueCode = #MADELIA 
* #SALMONELLA ^property[955].code = #child 
* #SALMONELLA ^property[955].valueCode = #MADIAGO 
* #SALMONELLA ^property[956].code = #child 
* #SALMONELLA ^property[956].valueCode = #MADIGAN 
* #SALMONELLA ^property[957].code = #child 
* #SALMONELLA ^property[957].valueCode = #MADISON 
* #SALMONELLA ^property[958].code = #child 
* #SALMONELLA ^property[958].valueCode = #MADJORIO 
* #SALMONELLA ^property[959].code = #child 
* #SALMONELLA ^property[959].valueCode = #MADRAS 
* #SALMONELLA ^property[960].code = #child 
* #SALMONELLA ^property[960].valueCode = #MAGHERAFELT 
* #SALMONELLA ^property[961].code = #child 
* #SALMONELLA ^property[961].valueCode = #MAGUMERI 
* #SALMONELLA ^property[962].code = #child 
* #SALMONELLA ^property[962].valueCode = #MAGWA 
* #SALMONELLA ^property[963].code = #child 
* #SALMONELLA ^property[963].valueCode = #MAHINA 
* #SALMONELLA ^property[964].code = #child 
* #SALMONELLA ^property[964].valueCode = #MAIDUGURI 
* #SALMONELLA ^property[965].code = #child 
* #SALMONELLA ^property[965].valueCode = #MAKILING 
* #SALMONELLA ^property[966].code = #child 
* #SALMONELLA ^property[966].valueCode = #MAKISO 
* #SALMONELLA ^property[967].code = #child 
* #SALMONELLA ^property[967].valueCode = #MALAKAL 
* #SALMONELLA ^property[968].code = #child 
* #SALMONELLA ^property[968].valueCode = #MALAYSIA 
* #SALMONELLA ^property[969].code = #child 
* #SALMONELLA ^property[969].valueCode = #MALIKA 
* #SALMONELLA ^property[970].code = #child 
* #SALMONELLA ^property[970].valueCode = #MALMOE 
* #SALMONELLA ^property[971].code = #child 
* #SALMONELLA ^property[971].valueCode = #MALSTATT 
* #SALMONELLA ^property[972].code = #child 
* #SALMONELLA ^property[972].valueCode = #MAMPEZA 
* #SALMONELLA ^property[973].code = #child 
* #SALMONELLA ^property[973].valueCode = #MAMPONG 
* #SALMONELLA ^property[974].code = #child 
* #SALMONELLA ^property[974].valueCode = #MANA 
* #SALMONELLA ^property[975].code = #child 
* #SALMONELLA ^property[975].valueCode = #MANCHESTER 
* #SALMONELLA ^property[976].code = #child 
* #SALMONELLA ^property[976].valueCode = #MANDERA 
* #SALMONELLA ^property[977].code = #child 
* #SALMONELLA ^property[977].valueCode = #MANGO 
* #SALMONELLA ^property[978].code = #child 
* #SALMONELLA ^property[978].valueCode = #MANHATTAN 
* #SALMONELLA ^property[979].code = #child 
* #SALMONELLA ^property[979].valueCode = #MANNHEIM 
* #SALMONELLA ^property[980].code = #child 
* #SALMONELLA ^property[980].valueCode = #MAPO 
* #SALMONELLA ^property[981].code = #child 
* #SALMONELLA ^property[981].valueCode = #MARA 
* #SALMONELLA ^property[982].code = #child 
* #SALMONELLA ^property[982].valueCode = #MARACAIBO 
* #SALMONELLA ^property[983].code = #child 
* #SALMONELLA ^property[983].valueCode = #MARBURG 
* #SALMONELLA ^property[984].code = #child 
* #SALMONELLA ^property[984].valueCode = #MARICOPA 
* #SALMONELLA ^property[985].code = #child 
* #SALMONELLA ^property[985].valueCode = #MARIENTHAL 
* #SALMONELLA ^property[986].code = #child 
* #SALMONELLA ^property[986].valueCode = #MARITZBURG 
* #SALMONELLA ^property[987].code = #child 
* #SALMONELLA ^property[987].valueCode = #MARMANDE 
* #SALMONELLA ^property[988].code = #child 
* #SALMONELLA ^property[988].valueCode = #MARON 
* #SALMONELLA ^property[989].code = #child 
* #SALMONELLA ^property[989].valueCode = #MAROUA 
* #SALMONELLA ^property[990].code = #child 
* #SALMONELLA ^property[990].valueCode = #MARSABIT 
* #SALMONELLA ^property[991].code = #child 
* #SALMONELLA ^property[991].valueCode = #MARSEILLE 
* #SALMONELLA ^property[992].code = #child 
* #SALMONELLA ^property[992].valueCode = #MARSHALL 
* #SALMONELLA ^property[993].code = #child 
* #SALMONELLA ^property[993].valueCode = #MARTONOS 
* #SALMONELLA ^property[994].code = #child 
* #SALMONELLA ^property[994].valueCode = #MARYLAND 
* #SALMONELLA ^property[995].code = #child 
* #SALMONELLA ^property[995].valueCode = #MARYLEBONE 
* #SALMONELLA ^property[996].code = #child 
* #SALMONELLA ^property[996].valueCode = #MASEMBE 
* #SALMONELLA ^property[997].code = #child 
* #SALMONELLA ^property[997].valueCode = #MASKA 
* #SALMONELLA ^property[998].code = #child 
* #SALMONELLA ^property[998].valueCode = #MASSAKORY 
* #SALMONELLA ^property[999].code = #child 
* #SALMONELLA ^property[999].valueCode = #MASSENYA 
* #SALMONELLA ^property[1000].code = #child 
* #SALMONELLA ^property[1000].valueCode = #MASSILIA 
* #SALMONELLA ^property[1001].code = #child 
* #SALMONELLA ^property[1001].valueCode = #MATADI 
* #SALMONELLA ^property[1002].code = #child 
* #SALMONELLA ^property[1002].valueCode = #MATHURA 
* #SALMONELLA ^property[1003].code = #child 
* #SALMONELLA ^property[1003].valueCode = #MATOPENI 
* #SALMONELLA ^property[1004].code = #child 
* #SALMONELLA ^property[1004].valueCode = #MATTENHOF 
* #SALMONELLA ^property[1005].code = #child 
* #SALMONELLA ^property[1005].valueCode = #MAUMEE 
* #SALMONELLA ^property[1006].code = #child 
* #SALMONELLA ^property[1006].valueCode = #MAYDAY 
* #SALMONELLA ^property[1007].code = #child 
* #SALMONELLA ^property[1007].valueCode = #MBANDAKA 
* #SALMONELLA ^property[1008].code = #child 
* #SALMONELLA ^property[1008].valueCode = #MBAO 
* #SALMONELLA ^property[1009].code = #child 
* #SALMONELLA ^property[1009].valueCode = #MEEKATHARRA 
* #SALMONELLA ^property[1010].code = #child 
* #SALMONELLA ^property[1010].valueCode = #MELAKA 
* #SALMONELLA ^property[1011].code = #child 
* #SALMONELLA ^property[1011].valueCode = #MELBOURNE 
* #SALMONELLA ^property[1012].code = #child 
* #SALMONELLA ^property[1012].valueCode = #MELEAGRIDIS 
* #SALMONELLA ^property[1013].code = #child 
* #SALMONELLA ^property[1013].valueCode = #MELEAGRIDIS_VAR_15+ 
* #SALMONELLA ^property[1014].code = #child 
* #SALMONELLA ^property[1014].valueCode = #MELEAGRIDIS_VAR_15+_34+ 
* #SALMONELLA ^property[1015].code = #child 
* #SALMONELLA ^property[1015].valueCode = #MEMPHIS 
* #SALMONELLA ^property[1016].code = #child 
* #SALMONELLA ^property[1016].valueCode = #MENDEN 
* #SALMONELLA ^property[1017].code = #child 
* #SALMONELLA ^property[1017].valueCode = #MENDOZA 
* #SALMONELLA ^property[1018].code = #child 
* #SALMONELLA ^property[1018].valueCode = #MENSTON 
* #SALMONELLA ^property[1019].code = #child 
* #SALMONELLA ^property[1019].valueCode = #MESBIT 
* #SALMONELLA ^property[1020].code = #child 
* #SALMONELLA ^property[1020].valueCode = #MESKIN 
* #SALMONELLA ^property[1021].code = #child 
* #SALMONELLA ^property[1021].valueCode = #MESSINA 
* #SALMONELLA ^property[1022].code = #child 
* #SALMONELLA ^property[1022].valueCode = #MGULANI 
* #SALMONELLA ^property[1023].code = #child 
* #SALMONELLA ^property[1023].valueCode = #MIAMI 
* #SALMONELLA ^property[1024].code = #child 
* #SALMONELLA ^property[1024].valueCode = #MICHIGAN 
* #SALMONELLA ^property[1025].code = #child 
* #SALMONELLA ^property[1025].valueCode = #MIDDLESBROUGH 
* #SALMONELLA ^property[1026].code = #child 
* #SALMONELLA ^property[1026].valueCode = #MIDWAY 
* #SALMONELLA ^property[1027].code = #child 
* #SALMONELLA ^property[1027].valueCode = #MIKAWASIMA 
* #SALMONELLA ^property[1028].code = #child 
* #SALMONELLA ^property[1028].valueCode = #MILLESI 
* #SALMONELLA ^property[1029].code = #child 
* #SALMONELLA ^property[1029].valueCode = #MILWAUKEE 
* #SALMONELLA ^property[1030].code = #child 
* #SALMONELLA ^property[1030].valueCode = #MIM 
* #SALMONELLA ^property[1031].code = #child 
* #SALMONELLA ^property[1031].valueCode = #MINNA 
* #SALMONELLA ^property[1032].code = #child 
* #SALMONELLA ^property[1032].valueCode = #MINNESOTA 
* #SALMONELLA ^property[1033].code = #child 
* #SALMONELLA ^property[1033].valueCode = #MISHMARHAEMEK 
* #SALMONELLA ^property[1034].code = #child 
* #SALMONELLA ^property[1034].valueCode = #MISSISSIPPI 
* #SALMONELLA ^property[1035].code = #child 
* #SALMONELLA ^property[1035].valueCode = #MISSOURI 
* #SALMONELLA ^property[1036].code = #child 
* #SALMONELLA ^property[1036].valueCode = #MIYAZAKI 
* #SALMONELLA ^property[1037].code = #child 
* #SALMONELLA ^property[1037].valueCode = #MJORDAN 
* #SALMONELLA ^property[1038].code = #child 
* #SALMONELLA ^property[1038].valueCode = #MKAMBA 
* #SALMONELLA ^property[1039].code = #child 
* #SALMONELLA ^property[1039].valueCode = #MOABIT 
* #SALMONELLA ^property[1040].code = #child 
* #SALMONELLA ^property[1040].valueCode = #MOCAMEDES 
* #SALMONELLA ^property[1041].code = #child 
* #SALMONELLA ^property[1041].valueCode = #MOERO 
* #SALMONELLA ^property[1042].code = #child 
* #SALMONELLA ^property[1042].valueCode = #MOERS 
* #SALMONELLA ^property[1043].code = #child 
* #SALMONELLA ^property[1043].valueCode = #MOKOLA 
* #SALMONELLA ^property[1044].code = #child 
* #SALMONELLA ^property[1044].valueCode = #MOLADE 
* #SALMONELLA ^property[1045].code = #child 
* #SALMONELLA ^property[1045].valueCode = #MOLESEY 
* #SALMONELLA ^property[1046].code = #child 
* #SALMONELLA ^property[1046].valueCode = #MONO 
* #SALMONELLA ^property[1047].code = #child 
* #SALMONELLA ^property[1047].valueCode = #MONOPHASIC_14[5]12_I_- 
* #SALMONELLA ^property[1048].code = #child 
* #SALMONELLA ^property[1048].valueCode = #MONS 
* #SALMONELLA ^property[1049].code = #child 
* #SALMONELLA ^property[1049].valueCode = #MONSCHAUI 
* #SALMONELLA ^property[1050].code = #child 
* #SALMONELLA ^property[1050].valueCode = #MONTAIGU 
* #SALMONELLA ^property[1051].code = #child 
* #SALMONELLA ^property[1051].valueCode = #MONTEVIDEO 
* #SALMONELLA ^property[1052].code = #child 
* #SALMONELLA ^property[1052].valueCode = #MONTREAL 
* #SALMONELLA ^property[1053].code = #child 
* #SALMONELLA ^property[1053].valueCode = #MORBIHAN 
* #SALMONELLA ^property[1054].code = #child 
* #SALMONELLA ^property[1054].valueCode = #MOREHEAD 
* #SALMONELLA ^property[1055].code = #child 
* #SALMONELLA ^property[1055].valueCode = #MORILLONS 
* #SALMONELLA ^property[1056].code = #child 
* #SALMONELLA ^property[1056].valueCode = #MORNINGSIDE 
* #SALMONELLA ^property[1057].code = #child 
* #SALMONELLA ^property[1057].valueCode = #MORNINGTON 
* #SALMONELLA ^property[1058].code = #child 
* #SALMONELLA ^property[1058].valueCode = #MOROCCO 
* #SALMONELLA ^property[1059].code = #child 
* #SALMONELLA ^property[1059].valueCode = #MOROTAI 
* #SALMONELLA ^property[1060].code = #child 
* #SALMONELLA ^property[1060].valueCode = #MOROTO 
* #SALMONELLA ^property[1061].code = #child 
* #SALMONELLA ^property[1061].valueCode = #MOSCOW 
* #SALMONELLA ^property[1062].code = #child 
* #SALMONELLA ^property[1062].valueCode = #MOUALINE 
* #SALMONELLA ^property[1063].code = #child 
* #SALMONELLA ^property[1063].valueCode = #MOUNDOU 
* #SALMONELLA ^property[1064].code = #child 
* #SALMONELLA ^property[1064].valueCode = #MOUNTMAGNET 
* #SALMONELLA ^property[1065].code = #child 
* #SALMONELLA ^property[1065].valueCode = #MOUNTPLEASANT 
* #SALMONELLA ^property[1066].code = #child 
* #SALMONELLA ^property[1066].valueCode = #MOUSSORO 
* #SALMONELLA ^property[1067].code = #child 
* #SALMONELLA ^property[1067].valueCode = #MOWANJUM 
* #SALMONELLA ^property[1068].code = #child 
* #SALMONELLA ^property[1068].valueCode = #MPOUTO 
* #SALMONELLA ^property[1069].code = #child 
* #SALMONELLA ^property[1069].valueCode = #MUENCHEN 
* #SALMONELLA ^property[1070].code = #child 
* #SALMONELLA ^property[1070].valueCode = #MUENSTER 
* #SALMONELLA ^property[1071].code = #child 
* #SALMONELLA ^property[1071].valueCode = #MUENSTER_VAR_15+ 
* #SALMONELLA ^property[1072].code = #child 
* #SALMONELLA ^property[1072].valueCode = #MUENSTER_VAR_15+_34+_ 
* #SALMONELLA ^property[1073].code = #child 
* #SALMONELLA ^property[1073].valueCode = #MUGUGA 
* #SALMONELLA ^property[1074].code = #child 
* #SALMONELLA ^property[1074].valueCode = #MULHOUSE 
* #SALMONELLA ^property[1075].code = #child 
* #SALMONELLA ^property[1075].valueCode = #MUNDONOBO 
* #SALMONELLA ^property[1076].code = #child 
* #SALMONELLA ^property[1076].valueCode = #MUNDUBBERA 
* #SALMONELLA ^property[1077].code = #child 
* #SALMONELLA ^property[1077].valueCode = #MURA 
* #SALMONELLA ^property[1078].code = #child 
* #SALMONELLA ^property[1078].valueCode = #MYGDAL 
* #SALMONELLA ^property[1079].code = #child 
* #SALMONELLA ^property[1079].valueCode = #MYRRIA 
* #SALMONELLA ^property[1080].code = #child 
* #SALMONELLA ^property[1080].valueCode = #NAESTVED 
* #SALMONELLA ^property[1081].code = #child 
* #SALMONELLA ^property[1081].valueCode = #NAGOYA 
* #SALMONELLA ^property[1082].code = #child 
* #SALMONELLA ^property[1082].valueCode = #NAKURU 
* #SALMONELLA ^property[1083].code = #child 
* #SALMONELLA ^property[1083].valueCode = #NAMIBIA 
* #SALMONELLA ^property[1084].code = #child 
* #SALMONELLA ^property[1084].valueCode = #NAMODA 
* #SALMONELLA ^property[1085].code = #child 
* #SALMONELLA ^property[1085].valueCode = #NAMUR 
* #SALMONELLA ^property[1086].code = #child 
* #SALMONELLA ^property[1086].valueCode = #NANERGOU 
* #SALMONELLA ^property[1087].code = #child 
* #SALMONELLA ^property[1087].valueCode = #NANGA 
* #SALMONELLA ^property[1088].code = #child 
* #SALMONELLA ^property[1088].valueCode = #NANTES 
* #SALMONELLA ^property[1089].code = #child 
* #SALMONELLA ^property[1089].valueCode = #NAPOLI 
* #SALMONELLA ^property[1090].code = #child 
* #SALMONELLA ^property[1090].valueCode = #NARASHINO 
* #SALMONELLA ^property[1091].code = #child 
* #SALMONELLA ^property[1091].valueCode = #NASHUA 
* #SALMONELLA ^property[1092].code = #child 
* #SALMONELLA ^property[1092].valueCode = #NATAL 
* #SALMONELLA ^property[1093].code = #child 
* #SALMONELLA ^property[1093].valueCode = #NAWARE 
* #SALMONELLA ^property[1094].code = #child 
* #SALMONELLA ^property[1094].valueCode = #NCHANGA 
* #SALMONELLA ^property[1095].code = #child 
* #SALMONELLA ^property[1095].valueCode = #NCHANGA_VAR_15+ 
* #SALMONELLA ^property[1096].code = #child 
* #SALMONELLA ^property[1096].valueCode = #NDJAMENA 
* #SALMONELLA ^property[1097].code = #child 
* #SALMONELLA ^property[1097].valueCode = #NDOLO 
* #SALMONELLA ^property[1098].code = #child 
* #SALMONELLA ^property[1098].valueCode = #NEFTENBACH 
* #SALMONELLA ^property[1099].code = #child 
* #SALMONELLA ^property[1099].valueCode = #NESSA 
* #SALMONELLA ^property[1100].code = #child 
* #SALMONELLA ^property[1100].valueCode = #NESSZIONA 
* #SALMONELLA ^property[1101].code = #child 
* #SALMONELLA ^property[1101].valueCode = #NEUDORF 
* #SALMONELLA ^property[1102].code = #child 
* #SALMONELLA ^property[1102].valueCode = #NEUKOELLN 
* #SALMONELLA ^property[1103].code = #child 
* #SALMONELLA ^property[1103].valueCode = #NEUMUENSTER 
* #SALMONELLA ^property[1104].code = #child 
* #SALMONELLA ^property[1104].valueCode = #NEUNKIRCHEN 
* #SALMONELLA ^property[1105].code = #child 
* #SALMONELLA ^property[1105].valueCode = #NEWHOLLAND 
* #SALMONELLA ^property[1106].code = #child 
* #SALMONELLA ^property[1106].valueCode = #NEWJERSEY 
* #SALMONELLA ^property[1107].code = #child 
* #SALMONELLA ^property[1107].valueCode = #NEWLANDS 
* #SALMONELLA ^property[1108].code = #child 
* #SALMONELLA ^property[1108].valueCode = #NEWMEXICO 
* #SALMONELLA ^property[1109].code = #child 
* #SALMONELLA ^property[1109].valueCode = #NEWPORT 
* #SALMONELLA ^property[1110].code = #child 
* #SALMONELLA ^property[1110].valueCode = #NEWROCHELLE 
* #SALMONELLA ^property[1111].code = #child 
* #SALMONELLA ^property[1111].valueCode = #NEWYORK 
* #SALMONELLA ^property[1112].code = #child 
* #SALMONELLA ^property[1112].valueCode = #NGAPAROU 
* #SALMONELLA ^property[1113].code = #child 
* #SALMONELLA ^property[1113].valueCode = #NGILI 
* #SALMONELLA ^property[1114].code = #child 
* #SALMONELLA ^property[1114].valueCode = #NGOR 
* #SALMONELLA ^property[1115].code = #child 
* #SALMONELLA ^property[1115].valueCode = #NIAKHAR 
* #SALMONELLA ^property[1116].code = #child 
* #SALMONELLA ^property[1116].valueCode = #NIAMEY 
* #SALMONELLA ^property[1117].code = #child 
* #SALMONELLA ^property[1117].valueCode = #NIAREMBE 
* #SALMONELLA ^property[1118].code = #child 
* #SALMONELLA ^property[1118].valueCode = #NIEDERODERWITZ 
* #SALMONELLA ^property[1119].code = #child 
* #SALMONELLA ^property[1119].valueCode = #NIEUKERK 
* #SALMONELLA ^property[1120].code = #child 
* #SALMONELLA ^property[1120].valueCode = #NIGERIA 
* #SALMONELLA ^property[1121].code = #child 
* #SALMONELLA ^property[1121].valueCode = #NIJMEGEN 
* #SALMONELLA ^property[1122].code = #child 
* #SALMONELLA ^property[1122].valueCode = #NIKOLAIFLEET 
* #SALMONELLA ^property[1123].code = #child 
* #SALMONELLA ^property[1123].valueCode = #NILOESE 
* #SALMONELLA ^property[1124].code = #child 
* #SALMONELLA ^property[1124].valueCode = #NIMA 
* #SALMONELLA ^property[1125].code = #child 
* #SALMONELLA ^property[1125].valueCode = #NIMES 
* #SALMONELLA ^property[1126].code = #child 
* #SALMONELLA ^property[1126].valueCode = #NITRA 
* #SALMONELLA ^property[1127].code = #child 
* #SALMONELLA ^property[1127].valueCode = #NIUMI 
* #SALMONELLA ^property[1128].code = #child 
* #SALMONELLA ^property[1128].valueCode = #NJALA 
* #SALMONELLA ^property[1129].code = #child 
* #SALMONELLA ^property[1129].valueCode = #NOLA 
* #SALMONELLA ^property[1130].code = #child 
* #SALMONELLA ^property[1130].valueCode = #NORDRHEIN 
* #SALMONELLA ^property[1131].code = #child 
* #SALMONELLA ^property[1131].valueCode = #NORDUFER 
* #SALMONELLA ^property[1132].code = #child 
* #SALMONELLA ^property[1132].valueCode = #NORTON 
* #SALMONELLA ^property[1133].code = #child 
* #SALMONELLA ^property[1133].valueCode = #NORWICH 
* #SALMONELLA ^property[1134].code = #child 
* #SALMONELLA ^property[1134].valueCode = #NOTTINGHAM 
* #SALMONELLA ^property[1135].code = #child 
* #SALMONELLA ^property[1135].valueCode = #NOWAWES 
* #SALMONELLA ^property[1136].code = #child 
* #SALMONELLA ^property[1136].valueCode = #NOYA 
* #SALMONELLA ^property[1137].code = #child 
* #SALMONELLA ^property[1137].valueCode = #NUATJA 
* #SALMONELLA ^property[1138].code = #child 
* #SALMONELLA ^property[1138].valueCode = #NYANZA 
* #SALMONELLA ^property[1139].code = #child 
* #SALMONELLA ^property[1139].valueCode = #NYBORG 
* #SALMONELLA ^property[1140].code = #child 
* #SALMONELLA ^property[1140].valueCode = #NYBORG_VAR_15+ 
* #SALMONELLA ^property[1141].code = #child 
* #SALMONELLA ^property[1141].valueCode = #NYEKO 
* #SALMONELLA ^property[1142].code = #child 
* #SALMONELLA ^property[1142].valueCode = #OAKEY 
* #SALMONELLA ^property[1143].code = #child 
* #SALMONELLA ^property[1143].valueCode = #OAKLAND 
* #SALMONELLA ^property[1144].code = #child 
* #SALMONELLA ^property[1144].valueCode = #OBOGU 
* #SALMONELLA ^property[1145].code = #child 
* #SALMONELLA ^property[1145].valueCode = #OCHIOGU 
* #SALMONELLA ^property[1146].code = #child 
* #SALMONELLA ^property[1146].valueCode = #OCHSENWERDER 
* #SALMONELLA ^property[1147].code = #child 
* #SALMONELLA ^property[1147].valueCode = #OCKENHEIM 
* #SALMONELLA ^property[1148].code = #child 
* #SALMONELLA ^property[1148].valueCode = #ODIENNE 
* #SALMONELLA ^property[1149].code = #child 
* #SALMONELLA ^property[1149].valueCode = #ODOZI 
* #SALMONELLA ^property[1150].code = #child 
* #SALMONELLA ^property[1150].valueCode = #OERLIKON 
* #SALMONELLA ^property[1151].code = #child 
* #SALMONELLA ^property[1151].valueCode = #OESTERBRO 
* #SALMONELLA ^property[1152].code = #child 
* #SALMONELLA ^property[1152].valueCode = #OFFA 
* #SALMONELLA ^property[1153].code = #child 
* #SALMONELLA ^property[1153].valueCode = #OGBETE 
* #SALMONELLA ^property[1154].code = #child 
* #SALMONELLA ^property[1154].valueCode = #OHIO 
* #SALMONELLA ^property[1155].code = #child 
* #SALMONELLA ^property[1155].valueCode = #OHIO_VAR_14+ 
* #SALMONELLA ^property[1156].code = #child 
* #SALMONELLA ^property[1156].valueCode = #OHLSTEDT 
* #SALMONELLA ^property[1157].code = #child 
* #SALMONELLA ^property[1157].valueCode = #OKATIE 
* #SALMONELLA ^property[1158].code = #child 
* #SALMONELLA ^property[1158].valueCode = #OKEFOKO 
* #SALMONELLA ^property[1159].code = #child 
* #SALMONELLA ^property[1159].valueCode = #OKERARA 
* #SALMONELLA ^property[1160].code = #child 
* #SALMONELLA ^property[1160].valueCode = #OLDENBURG 
* #SALMONELLA ^property[1161].code = #child 
* #SALMONELLA ^property[1161].valueCode = #OLTEN 
* #SALMONELLA ^property[1162].code = #child 
* #SALMONELLA ^property[1162].valueCode = #OMIFISAN 
* #SALMONELLA ^property[1163].code = #child 
* #SALMONELLA ^property[1163].valueCode = #OMUNA 
* #SALMONELLA ^property[1164].code = #child 
* #SALMONELLA ^property[1164].valueCode = #ONA 
* #SALMONELLA ^property[1165].code = #child 
* #SALMONELLA ^property[1165].valueCode = #ONARIMON 
* #SALMONELLA ^property[1166].code = #child 
* #SALMONELLA ^property[1166].valueCode = #ONDERSTEPOORT 
* #SALMONELLA ^property[1167].code = #child 
* #SALMONELLA ^property[1167].valueCode = #ONIREKE 
* #SALMONELLA ^property[1168].code = #child 
* #SALMONELLA ^property[1168].valueCode = #ONTARIO 
* #SALMONELLA ^property[1169].code = #child 
* #SALMONELLA ^property[1169].valueCode = #ORAN 
* #SALMONELLA ^property[1170].code = #child 
* #SALMONELLA ^property[1170].valueCode = #ORANIENBURG 
* #SALMONELLA ^property[1171].code = #child 
* #SALMONELLA ^property[1171].valueCode = #ORANIENBURG_VAR_14+ 
* #SALMONELLA ^property[1172].code = #child 
* #SALMONELLA ^property[1172].valueCode = #ORBE 
* #SALMONELLA ^property[1173].code = #child 
* #SALMONELLA ^property[1173].valueCode = #ORD 
* #SALMONELLA ^property[1174].code = #child 
* #SALMONELLA ^property[1174].valueCode = #ORDONEZ 
* #SALMONELLA ^property[1175].code = #child 
* #SALMONELLA ^property[1175].valueCode = #ORIENTALIS 
* #SALMONELLA ^property[1176].code = #child 
* #SALMONELLA ^property[1176].valueCode = #ORION 
* #SALMONELLA ^property[1177].code = #child 
* #SALMONELLA ^property[1177].valueCode = #ORION_VAR_15+_ 
* #SALMONELLA ^property[1178].code = #child 
* #SALMONELLA ^property[1178].valueCode = #ORION_VAR_15+_34+ 
* #SALMONELLA ^property[1179].code = #child 
* #SALMONELLA ^property[1179].valueCode = #ORITAMERIN 
* #SALMONELLA ^property[1180].code = #child 
* #SALMONELLA ^property[1180].valueCode = #ORLANDO 
* #SALMONELLA ^property[1181].code = #child 
* #SALMONELLA ^property[1181].valueCode = #ORLEANS 
* #SALMONELLA ^property[1182].code = #child 
* #SALMONELLA ^property[1182].valueCode = #OS 
* #SALMONELLA ^property[1183].code = #child 
* #SALMONELLA ^property[1183].valueCode = #OSKARSHAMN 
* #SALMONELLA ^property[1184].code = #child 
* #SALMONELLA ^property[1184].valueCode = #OSLO 
* #SALMONELLA ^property[1185].code = #child 
* #SALMONELLA ^property[1185].valueCode = #OSNABRUECK 
* #SALMONELLA ^property[1186].code = #child 
* #SALMONELLA ^property[1186].valueCode = #OTHER 
* #SALMONELLA ^property[1187].code = #child 
* #SALMONELLA ^property[1187].valueCode = #OTHMARSCHEN 
* #SALMONELLA ^property[1188].code = #child 
* #SALMONELLA ^property[1188].valueCode = #OTTAWA 
* #SALMONELLA ^property[1189].code = #child 
* #SALMONELLA ^property[1189].valueCode = #OUAGADOUGOU 
* #SALMONELLA ^property[1190].code = #child 
* #SALMONELLA ^property[1190].valueCode = #OUAKAM 
* #SALMONELLA ^property[1191].code = #child 
* #SALMONELLA ^property[1191].valueCode = #OUDWIJK 
* #SALMONELLA ^property[1192].code = #child 
* #SALMONELLA ^property[1192].valueCode = #OVERCHURCH 
* #SALMONELLA ^property[1193].code = #child 
* #SALMONELLA ^property[1193].valueCode = #OVERSCHIE 
* #SALMONELLA ^property[1194].code = #child 
* #SALMONELLA ^property[1194].valueCode = #OVERVECHT 
* #SALMONELLA ^property[1195].code = #child 
* #SALMONELLA ^property[1195].valueCode = #OXFORD 
* #SALMONELLA ^property[1196].code = #child 
* #SALMONELLA ^property[1196].valueCode = #OXFORD_VAR_15+_ 
* #SALMONELLA ^property[1197].code = #child 
* #SALMONELLA ^property[1197].valueCode = #OXFORD_VAR_15+_34+ 
* #SALMONELLA ^property[1198].code = #child 
* #SALMONELLA ^property[1198].valueCode = #OYONNAX 
* #SALMONELLA ^property[1199].code = #child 
* #SALMONELLA ^property[1199].valueCode = #PAKISTAN 
* #SALMONELLA ^property[1200].code = #child 
* #SALMONELLA ^property[1200].valueCode = #PALAMANER 
* #SALMONELLA ^property[1201].code = #child 
* #SALMONELLA ^property[1201].valueCode = #PALIME 
* #SALMONELLA ^property[1202].code = #child 
* #SALMONELLA ^property[1202].valueCode = #PANAMA 
* #SALMONELLA ^property[1203].code = #child 
* #SALMONELLA ^property[1203].valueCode = #PAPUANA 
* #SALMONELLA ^property[1204].code = #child 
* #SALMONELLA ^property[1204].valueCode = #PARAKOU 
* #SALMONELLA ^property[1205].code = #child 
* #SALMONELLA ^property[1205].valueCode = #PARATYPHI 
* #SALMONELLA ^property[1206].code = #child 
* #SALMONELLA ^property[1206].valueCode = #PARATYPHI_B_VAR_L(+)_TARTRATE+ 
* #SALMONELLA ^property[1207].code = #child 
* #SALMONELLA ^property[1207].valueCode = #PARIS 
* #SALMONELLA ^property[1208].code = #child 
* #SALMONELLA ^property[1208].valueCode = #PARKROYAL 
* #SALMONELLA ^property[1209].code = #child 
* #SALMONELLA ^property[1209].valueCode = #PASING 
* #SALMONELLA ^property[1210].code = #child 
* #SALMONELLA ^property[1210].valueCode = #PATIENCE 
* #SALMONELLA ^property[1211].code = #child 
* #SALMONELLA ^property[1211].valueCode = #PENARTH 
* #SALMONELLA ^property[1212].code = #child 
* #SALMONELLA ^property[1212].valueCode = #PENILLA 
* #SALMONELLA ^property[1213].code = #child 
* #SALMONELLA ^property[1213].valueCode = #PENSACOLA 
* #SALMONELLA ^property[1214].code = #child 
* #SALMONELLA ^property[1214].valueCode = #PERTH 
* #SALMONELLA ^property[1215].code = #child 
* #SALMONELLA ^property[1215].valueCode = #PETAHTIKVE 
* #SALMONELLA ^property[1216].code = #child 
* #SALMONELLA ^property[1216].valueCode = #PHALIRON 
* #SALMONELLA ^property[1217].code = #child 
* #SALMONELLA ^property[1217].valueCode = #PHARR 
* #SALMONELLA ^property[1218].code = #child 
* #SALMONELLA ^property[1218].valueCode = #PICPUS 
* #SALMONELLA ^property[1219].code = #child 
* #SALMONELLA ^property[1219].valueCode = #PIETERSBURG 
* #SALMONELLA ^property[1220].code = #child 
* #SALMONELLA ^property[1220].valueCode = #PISA 
* #SALMONELLA ^property[1221].code = #child 
* #SALMONELLA ^property[1221].valueCode = #PLANCKENDAEL 
* #SALMONELLA ^property[1222].code = #child 
* #SALMONELLA ^property[1222].valueCode = #PLOUFRAGAN 
* #SALMONELLA ^property[1223].code = #child 
* #SALMONELLA ^property[1223].valueCode = #PLUMAUGAT 
* #SALMONELLA ^property[1224].code = #child 
* #SALMONELLA ^property[1224].valueCode = #PLYMOUTH 
* #SALMONELLA ^property[1225].code = #child 
* #SALMONELLA ^property[1225].valueCode = #POANO 
* #SALMONELLA ^property[1226].code = #child 
* #SALMONELLA ^property[1226].valueCode = #PODIENSIS 
* #SALMONELLA ^property[1227].code = #child 
* #SALMONELLA ^property[1227].valueCode = #POESELDORF 
* #SALMONELLA ^property[1228].code = #child 
* #SALMONELLA ^property[1228].valueCode = #POITIERS 
* #SALMONELLA ^property[1229].code = #child 
* #SALMONELLA ^property[1229].valueCode = #POMONA 
* #SALMONELLA ^property[1230].code = #child 
* #SALMONELLA ^property[1230].valueCode = #PONTYPRIDD 
* #SALMONELLA ^property[1231].code = #child 
* #SALMONELLA ^property[1231].valueCode = #POONA 
* #SALMONELLA ^property[1232].code = #child 
* #SALMONELLA ^property[1232].valueCode = #PORTANIGRA 
* #SALMONELLA ^property[1233].code = #child 
* #SALMONELLA ^property[1233].valueCode = #PORTLAND 
* #SALMONELLA ^property[1234].code = #child 
* #SALMONELLA ^property[1234].valueCode = #POTENGI 
* #SALMONELLA ^property[1235].code = #child 
* #SALMONELLA ^property[1235].valueCode = #POTOSI 
* #SALMONELLA ^property[1236].code = #child 
* #SALMONELLA ^property[1236].valueCode = #POTSDAM 
* #SALMONELLA ^property[1237].code = #child 
* #SALMONELLA ^property[1237].valueCode = #POTTO 
* #SALMONELLA ^property[1238].code = #child 
* #SALMONELLA ^property[1238].valueCode = #POWELL 
* #SALMONELLA ^property[1239].code = #child 
* #SALMONELLA ^property[1239].valueCode = #PRAHA 
* #SALMONELLA ^property[1240].code = #child 
* #SALMONELLA ^property[1240].valueCode = #PRAMISO 
* #SALMONELLA ^property[1241].code = #child 
* #SALMONELLA ^property[1241].valueCode = #PRESOV 
* #SALMONELLA ^property[1242].code = #child 
* #SALMONELLA ^property[1242].valueCode = #PRESTON 
* #SALMONELLA ^property[1243].code = #child 
* #SALMONELLA ^property[1243].valueCode = #PRETORIA 
* #SALMONELLA ^property[1244].code = #child 
* #SALMONELLA ^property[1244].valueCode = #PUTTEN 
* #SALMONELLA ^property[1245].code = #child 
* #SALMONELLA ^property[1245].valueCode = #QUEBEC 
* #SALMONELLA ^property[1246].code = #child 
* #SALMONELLA ^property[1246].valueCode = #QUENTIN 
* #SALMONELLA ^property[1247].code = #child 
* #SALMONELLA ^property[1247].valueCode = #QUINCY 
* #SALMONELLA ^property[1248].code = #child 
* #SALMONELLA ^property[1248].valueCode = #QUINHON 
* #SALMONELLA ^property[1249].code = #child 
* #SALMONELLA ^property[1249].valueCode = #QUINIELA 
* #SALMONELLA ^property[1250].code = #child 
* #SALMONELLA ^property[1250].valueCode = #RAMATGAN 
* #SALMONELLA ^property[1251].code = #child 
* #SALMONELLA ^property[1251].valueCode = #RAMSEY 
* #SALMONELLA ^property[1252].code = #child 
* #SALMONELLA ^property[1252].valueCode = #RATCHABURI 
* #SALMONELLA ^property[1253].code = #child 
* #SALMONELLA ^property[1253].valueCode = #RAUS 
* #SALMONELLA ^property[1254].code = #child 
* #SALMONELLA ^property[1254].valueCode = #RAWASH 
* #SALMONELLA ^property[1255].code = #child 
* #SALMONELLA ^property[1255].valueCode = #READING 
* #SALMONELLA ^property[1256].code = #child 
* #SALMONELLA ^property[1256].valueCode = #RECHOVOT 
* #SALMONELLA ^property[1257].code = #child 
* #SALMONELLA ^property[1257].valueCode = #REDBA 
* #SALMONELLA ^property[1258].code = #child 
* #SALMONELLA ^property[1258].valueCode = #REDHILL 
* #SALMONELLA ^property[1259].code = #child 
* #SALMONELLA ^property[1259].valueCode = #REDLANDS 
* #SALMONELLA ^property[1260].code = #child 
* #SALMONELLA ^property[1260].valueCode = #REGENT 
* #SALMONELLA ^property[1261].code = #child 
* #SALMONELLA ^property[1261].valueCode = #REINICKENDORF 
* #SALMONELLA ^property[1262].code = #child 
* #SALMONELLA ^property[1262].valueCode = #REMETE 
* #SALMONELLA ^property[1263].code = #child 
* #SALMONELLA ^property[1263].valueCode = #REMIREMONT 
* #SALMONELLA ^property[1264].code = #child 
* #SALMONELLA ^property[1264].valueCode = #REMO 
* #SALMONELLA ^property[1265].code = #child 
* #SALMONELLA ^property[1265].valueCode = #REUBEUSS 
* #SALMONELLA ^property[1266].code = #child 
* #SALMONELLA ^property[1266].valueCode = #RHONE 
* #SALMONELLA ^property[1267].code = #child 
* #SALMONELLA ^property[1267].valueCode = #RHYDYFELIN 
* #SALMONELLA ^property[1268].code = #child 
* #SALMONELLA ^property[1268].valueCode = #RICHMOND 
* #SALMONELLA ^property[1269].code = #child 
* #SALMONELLA ^property[1269].valueCode = #RIDEAU 
* #SALMONELLA ^property[1270].code = #child 
* #SALMONELLA ^property[1270].valueCode = #RIDGE 
* #SALMONELLA ^property[1271].code = #child 
* #SALMONELLA ^property[1271].valueCode = #RIED 
* #SALMONELLA ^property[1272].code = #child 
* #SALMONELLA ^property[1272].valueCode = #RIGGIL 
* #SALMONELLA ^property[1273].code = #child 
* #SALMONELLA ^property[1273].valueCode = #RIOGRANDE 
* #SALMONELLA ^property[1274].code = #child 
* #SALMONELLA ^property[1274].valueCode = #RISSEN 
* #SALMONELLA ^property[1275].code = #child 
* #SALMONELLA ^property[1275].valueCode = #RISSEN_VAR_14+ 
* #SALMONELLA ^property[1276].code = #child 
* #SALMONELLA ^property[1276].valueCode = #RITTERSBACH 
* #SALMONELLA ^property[1277].code = #child 
* #SALMONELLA ^property[1277].valueCode = #RIVERSIDE 
* #SALMONELLA ^property[1278].code = #child 
* #SALMONELLA ^property[1278].valueCode = #ROAN 
* #SALMONELLA ^property[1279].code = #child 
* #SALMONELLA ^property[1279].valueCode = #ROCHDALE 
* #SALMONELLA ^property[1280].code = #child 
* #SALMONELLA ^property[1280].valueCode = #ROGY 
* #SALMONELLA ^property[1281].code = #child 
* #SALMONELLA ^property[1281].valueCode = #ROMANBY 
* #SALMONELLA ^property[1282].code = #child 
* #SALMONELLA ^property[1282].valueCode = #ROODEPOORT 
* #SALMONELLA ^property[1283].code = #child 
* #SALMONELLA ^property[1283].valueCode = #ROSENBERG 
* #SALMONELLA ^property[1284].code = #child 
* #SALMONELLA ^property[1284].valueCode = #ROSSLEBEN 
* #SALMONELLA ^property[1285].code = #child 
* #SALMONELLA ^property[1285].valueCode = #ROSTOCK 
* #SALMONELLA ^property[1286].code = #child 
* #SALMONELLA ^property[1286].valueCode = #ROTHENBURGSORT 
* #SALMONELLA ^property[1287].code = #child 
* #SALMONELLA ^property[1287].valueCode = #ROTTNEST 
* #SALMONELLA ^property[1288].code = #child 
* #SALMONELLA ^property[1288].valueCode = #ROVANIEMI 
* #SALMONELLA ^property[1289].code = #child 
* #SALMONELLA ^property[1289].valueCode = #ROYAN 
* #SALMONELLA ^property[1290].code = #child 
* #SALMONELLA ^property[1290].valueCode = #RUANDA 
* #SALMONELLA ^property[1291].code = #child 
* #SALMONELLA ^property[1291].valueCode = #RUBISLAW 
* #SALMONELLA ^property[1292].code = #child 
* #SALMONELLA ^property[1292].valueCode = #RUIRU 
* #SALMONELLA ^property[1293].code = #child 
* #SALMONELLA ^property[1293].valueCode = #RUMFORD 
* #SALMONELLA ^property[1294].code = #child 
* #SALMONELLA ^property[1294].valueCode = #RUNBY 
* #SALMONELLA ^property[1295].code = #child 
* #SALMONELLA ^property[1295].valueCode = #RUZIZI 
* #SALMONELLA ^property[1296].code = #child 
* #SALMONELLA ^property[1296].valueCode = #SAARBRUECKEN 
* #SALMONELLA ^property[1297].code = #child 
* #SALMONELLA ^property[1297].valueCode = #SABOYA 
* #SALMONELLA ^property[1298].code = #child 
* #SALMONELLA ^property[1298].valueCode = #SADA 
* #SALMONELLA ^property[1299].code = #child 
* #SALMONELLA ^property[1299].valueCode = #SAINTEMARIE 
* #SALMONELLA ^property[1300].code = #child 
* #SALMONELLA ^property[1300].valueCode = #SAINTPAUL 
* #SALMONELLA ^property[1301].code = #child 
* #SALMONELLA ^property[1301].valueCode = #SALFORD 
* #SALMONELLA ^property[1302].code = #child 
* #SALMONELLA ^property[1302].valueCode = #SALINAS 
* #SALMONELLA ^property[1303].code = #child 
* #SALMONELLA ^property[1303].valueCode = #SALLY 
* #SALMONELLA ^property[1304].code = #child 
* #SALMONELLA ^property[1304].valueCode = #SALONIKI 
* #SALMONELLA ^property[1305].code = #child 
* #SALMONELLA ^property[1305].valueCode = #SAMARU 
* #SALMONELLA ^property[1306].code = #child 
* #SALMONELLA ^property[1306].valueCode = #SAMBRE 
* #SALMONELLA ^property[1307].code = #child 
* #SALMONELLA ^property[1307].valueCode = #SANDAGA 
* #SALMONELLA ^property[1308].code = #child 
* #SALMONELLA ^property[1308].valueCode = #SANDIEGO 
* #SALMONELLA ^property[1309].code = #child 
* #SALMONELLA ^property[1309].valueCode = #SANDOW 
* #SALMONELLA ^property[1310].code = #child 
* #SALMONELLA ^property[1310].valueCode = #SANGA 
* #SALMONELLA ^property[1311].code = #child 
* #SALMONELLA ^property[1311].valueCode = #SANGALKAM 
* #SALMONELLA ^property[1312].code = #child 
* #SALMONELLA ^property[1312].valueCode = #SANGERA 
* #SALMONELLA ^property[1313].code = #child 
* #SALMONELLA ^property[1313].valueCode = #SANJUAN 
* #SALMONELLA ^property[1314].code = #child 
* #SALMONELLA ^property[1314].valueCode = #SANKTGEORG 
* #SALMONELLA ^property[1315].code = #child 
* #SALMONELLA ^property[1315].valueCode = #SANKTJOHANN 
* #SALMONELLA ^property[1316].code = #child 
* #SALMONELLA ^property[1316].valueCode = #SANKTMARX 
* #SALMONELLA ^property[1317].code = #child 
* #SALMONELLA ^property[1317].valueCode = #SANTANDER 
* #SALMONELLA ^property[1318].code = #child 
* #SALMONELLA ^property[1318].valueCode = #SANTHIABA 
* #SALMONELLA ^property[1319].code = #child 
* #SALMONELLA ^property[1319].valueCode = #SANTIAGO 
* #SALMONELLA ^property[1320].code = #child 
* #SALMONELLA ^property[1320].valueCode = #SAO 
* #SALMONELLA ^property[1321].code = #child 
* #SALMONELLA ^property[1321].valueCode = #SAPELE 
* #SALMONELLA ^property[1322].code = #child 
* #SALMONELLA ^property[1322].valueCode = #SAPHRA 
* #SALMONELLA ^property[1323].code = #child 
* #SALMONELLA ^property[1323].valueCode = #SARA 
* #SALMONELLA ^property[1324].code = #child 
* #SALMONELLA ^property[1324].valueCode = #SARAJANE 
* #SALMONELLA ^property[1325].code = #child 
* #SALMONELLA ^property[1325].valueCode = #SAUGUS 
* #SALMONELLA ^property[1326].code = #child 
* #SALMONELLA ^property[1326].valueCode = #SCARBOROUGH 
* #SALMONELLA ^property[1327].code = #child 
* #SALMONELLA ^property[1327].valueCode = #SCHALKWIJK 
* #SALMONELLA ^property[1328].code = #child 
* #SALMONELLA ^property[1328].valueCode = #SCHLEISSHEIM 
* #SALMONELLA ^property[1329].code = #child 
* #SALMONELLA ^property[1329].valueCode = #SCHOENEBERG 
* #SALMONELLA ^property[1330].code = #child 
* #SALMONELLA ^property[1330].valueCode = #SCHWABACH 
* #SALMONELLA ^property[1331].code = #child 
* #SALMONELLA ^property[1331].valueCode = #SCHWARZENGRUND 
* #SALMONELLA ^property[1332].code = #child 
* #SALMONELLA ^property[1332].valueCode = #SCHWERIN 
* #SALMONELLA ^property[1333].code = #child 
* #SALMONELLA ^property[1333].valueCode = #SCULCOATES 
* #SALMONELLA ^property[1334].code = #child 
* #SALMONELLA ^property[1334].valueCode = #SEATTLE 
* #SALMONELLA ^property[1335].code = #child 
* #SALMONELLA ^property[1335].valueCode = #SEDGWICK 
* #SALMONELLA ^property[1336].code = #child 
* #SALMONELLA ^property[1336].valueCode = #SEEGEFELD 
* #SALMONELLA ^property[1337].code = #child 
* #SALMONELLA ^property[1337].valueCode = #SEKONDI 
* #SALMONELLA ^property[1338].code = #child 
* #SALMONELLA ^property[1338].valueCode = #SELBY 
* #SALMONELLA ^property[1339].code = #child 
* #SALMONELLA ^property[1339].valueCode = #SENDAI 
* #SALMONELLA ^property[1340].code = #child 
* #SALMONELLA ^property[1340].valueCode = #SENEGAL 
* #SALMONELLA ^property[1341].code = #child 
* #SALMONELLA ^property[1341].valueCode = #SENFTENBERG 
* #SALMONELLA ^property[1342].code = #child 
* #SALMONELLA ^property[1342].valueCode = #SENNEVILLE 
* #SALMONELLA ^property[1343].code = #child 
* #SALMONELLA ^property[1343].valueCode = #SEREMBAN 
* #SALMONELLA ^property[1344].code = #child 
* #SALMONELLA ^property[1344].valueCode = #SERREKUNDA 
* #SALMONELLA ^property[1345].code = #child 
* #SALMONELLA ^property[1345].valueCode = #SHAHALAM 
* #SALMONELLA ^property[1346].code = #child 
* #SALMONELLA ^property[1346].valueCode = #SHAMBA 
* #SALMONELLA ^property[1347].code = #child 
* #SALMONELLA ^property[1347].valueCode = #SHANGANI 
* #SALMONELLA ^property[1348].code = #child 
* #SALMONELLA ^property[1348].valueCode = #SHANGANI_VAR_15+ 
* #SALMONELLA ^property[1349].code = #child 
* #SALMONELLA ^property[1349].valueCode = #SHANGHAI 
* #SALMONELLA ^property[1350].code = #child 
* #SALMONELLA ^property[1350].valueCode = #SHANNON 
* #SALMONELLA ^property[1351].code = #child 
* #SALMONELLA ^property[1351].valueCode = #SHARON 
* #SALMONELLA ^property[1352].code = #child 
* #SALMONELLA ^property[1352].valueCode = #SHEFFIELD 
* #SALMONELLA ^property[1353].code = #child 
* #SALMONELLA ^property[1353].valueCode = #SHERBROOKE 
* #SALMONELLA ^property[1354].code = #child 
* #SALMONELLA ^property[1354].valueCode = #SHIKMONAH 
* #SALMONELLA ^property[1355].code = #child 
* #SALMONELLA ^property[1355].valueCode = #SHIPLEY 
* #SALMONELLA ^property[1356].code = #child 
* #SALMONELLA ^property[1356].valueCode = #SHOMOLU 
* #SALMONELLA ^property[1357].code = #child 
* #SALMONELLA ^property[1357].valueCode = #SHOREDITCH 
* #SALMONELLA ^property[1358].code = #child 
* #SALMONELLA ^property[1358].valueCode = #SHUBRA 
* #SALMONELLA ^property[1359].code = #child 
* #SALMONELLA ^property[1359].valueCode = #SICA 
* #SALMONELLA ^property[1360].code = #child 
* #SALMONELLA ^property[1360].valueCode = #SIMI 
* #SALMONELLA ^property[1361].code = #child 
* #SALMONELLA ^property[1361].valueCode = #SINCHEW 
* #SALMONELLA ^property[1362].code = #child 
* #SALMONELLA ^property[1362].valueCode = #SINDELFINGEN 
* #SALMONELLA ^property[1363].code = #child 
* #SALMONELLA ^property[1363].valueCode = #SINGAPORE 
* #SALMONELLA ^property[1364].code = #child 
* #SALMONELLA ^property[1364].valueCode = #SINSTORF 
* #SALMONELLA ^property[1365].code = #child 
* #SALMONELLA ^property[1365].valueCode = #SINTHIA 
* #SALMONELLA ^property[1366].code = #child 
* #SALMONELLA ^property[1366].valueCode = #SIPANE 
* #SALMONELLA ^property[1367].code = #child 
* #SALMONELLA ^property[1367].valueCode = #SKANSEN 
* #SALMONELLA ^property[1368].code = #child 
* #SALMONELLA ^property[1368].valueCode = #SLADE 
* #SALMONELLA ^property[1369].code = #child 
* #SALMONELLA ^property[1369].valueCode = #SLJEME 
* #SALMONELLA ^property[1370].code = #child 
* #SALMONELLA ^property[1370].valueCode = #SLOTERDIJK 
* #SALMONELLA ^property[1371].code = #child 
* #SALMONELLA ^property[1371].valueCode = #SOAHANINA 
* #SALMONELLA ^property[1372].code = #child 
* #SALMONELLA ^property[1372].valueCode = #SOERENGA 
* #SALMONELLA ^property[1373].code = #child 
* #SALMONELLA ^property[1373].valueCode = #SOKODE 
* #SALMONELLA ^property[1374].code = #child 
* #SALMONELLA ^property[1374].valueCode = #SOLNA 
* #SALMONELLA ^property[1375].code = #child 
* #SALMONELLA ^property[1375].valueCode = #SOLT 
* #SALMONELLA ^property[1376].code = #child 
* #SALMONELLA ^property[1376].valueCode = #SOMONE 
* #SALMONELLA ^property[1377].code = #child 
* #SALMONELLA ^property[1377].valueCode = #SONTHEIM 
* #SALMONELLA ^property[1378].code = #child 
* #SALMONELLA ^property[1378].valueCode = #SOUMBEDIOUNE 
* #SALMONELLA ^property[1379].code = #child 
* #SALMONELLA ^property[1379].valueCode = #SOUTHAMPTON 
* #SALMONELLA ^property[1380].code = #child 
* #SALMONELLA ^property[1380].valueCode = #SOUTHBANK 
* #SALMONELLA ^property[1381].code = #child 
* #SALMONELLA ^property[1381].valueCode = #SOUZA 
* #SALMONELLA ^property[1382].code = #child 
* #SALMONELLA ^property[1382].valueCode = #SOUZA_VAR_15+ 
* #SALMONELLA ^property[1383].code = #child 
* #SALMONELLA ^property[1383].valueCode = #SPALENTOR 
* #SALMONELLA ^property[1384].code = #child 
* #SALMONELLA ^property[1384].valueCode = #SPARTEL 
* #SALMONELLA ^property[1385].code = #child 
* #SALMONELLA ^property[1385].valueCode = #SPLOTT 
* #SALMONELLA ^property[1386].code = #child 
* #SALMONELLA ^property[1386].valueCode = #STACHUS 
* #SALMONELLA ^property[1387].code = #child 
* #SALMONELLA ^property[1387].valueCode = #STANLEY 
* #SALMONELLA ^property[1388].code = #child 
* #SALMONELLA ^property[1388].valueCode = #STANLEYVILLE 
* #SALMONELLA ^property[1389].code = #child 
* #SALMONELLA ^property[1389].valueCode = #STAOUELI 
* #SALMONELLA ^property[1390].code = #child 
* #SALMONELLA ^property[1390].valueCode = #STEINPLATZ 
* #SALMONELLA ^property[1391].code = #child 
* #SALMONELLA ^property[1391].valueCode = #STEINWERDER 
* #SALMONELLA ^property[1392].code = #child 
* #SALMONELLA ^property[1392].valueCode = #STELLINGEN 
* #SALMONELLA ^property[1393].code = #child 
* #SALMONELLA ^property[1393].valueCode = #STENDAL 
* #SALMONELLA ^property[1394].code = #child 
* #SALMONELLA ^property[1394].valueCode = #STERNSCHANZE 
* #SALMONELLA ^property[1395].code = #child 
* #SALMONELLA ^property[1395].valueCode = #STERRENBOS 
* #SALMONELLA ^property[1396].code = #child 
* #SALMONELLA ^property[1396].valueCode = #STOCKHOLM 
* #SALMONELLA ^property[1397].code = #child 
* #SALMONELLA ^property[1397].valueCode = #STOCKHOLM_VAR_15+ 
* #SALMONELLA ^property[1398].code = #child 
* #SALMONELLA ^property[1398].valueCode = #STONEFERRY 
* #SALMONELLA ^property[1399].code = #child 
* #SALMONELLA ^property[1399].valueCode = #STORMONT 
* #SALMONELLA ^property[1400].code = #child 
* #SALMONELLA ^property[1400].valueCode = #STOURBRIDGE 
* #SALMONELLA ^property[1401].code = #child 
* #SALMONELLA ^property[1401].valueCode = #STRAENGNAES 
* #SALMONELLA ^property[1402].code = #child 
* #SALMONELLA ^property[1402].valueCode = #STRASBOURG 
* #SALMONELLA ^property[1403].code = #child 
* #SALMONELLA ^property[1403].valueCode = #STRATFORD 
* #SALMONELLA ^property[1404].code = #child 
* #SALMONELLA ^property[1404].valueCode = #STRATHCONA 
* #SALMONELLA ^property[1405].code = #child 
* #SALMONELLA ^property[1405].valueCode = #STUIVENBERG 
* #SALMONELLA ^property[1406].code = #child 
* #SALMONELLA ^property[1406].valueCode = #STUTTGART 
* #SALMONELLA ^property[1407].code = #child 
* #SALMONELLA ^property[1407].valueCode = #SUBERU 
* #SALMONELLA ^property[1408].code = #child 
* #SALMONELLA ^property[1408].valueCode = #SUBSPIIIA 
* #SALMONELLA ^property[1409].code = #child 
* #SALMONELLA ^property[1409].valueCode = #SUBSPIIIB 
* #SALMONELLA ^property[1410].code = #child 
* #SALMONELLA ^property[1410].valueCode = #SUBSPII_ 
* #SALMONELLA ^property[1411].code = #child 
* #SALMONELLA ^property[1411].valueCode = #SUBSPIV 
* #SALMONELLA ^property[1412].code = #child 
* #SALMONELLA ^property[1412].valueCode = #SUBSPI_ 
* #SALMONELLA ^property[1413].code = #child 
* #SALMONELLA ^property[1413].valueCode = #SUBSPV 
* #SALMONELLA ^property[1414].code = #child 
* #SALMONELLA ^property[1414].valueCode = #SUBSPVI 
* #SALMONELLA ^property[1415].code = #child 
* #SALMONELLA ^property[1415].valueCode = #SUDAN 
* #SALMONELLA ^property[1416].code = #child 
* #SALMONELLA ^property[1416].valueCode = #SUELLDORF 
* #SALMONELLA ^property[1417].code = #child 
* #SALMONELLA ^property[1417].valueCode = #SUNDSVALL 
* #SALMONELLA ^property[1418].code = #child 
* #SALMONELLA ^property[1418].valueCode = #SUNNYCOVE 
* #SALMONELLA ^property[1419].code = #child 
* #SALMONELLA ^property[1419].valueCode = #SURAT 
* #SALMONELLA ^property[1420].code = #child 
* #SALMONELLA ^property[1420].valueCode = #SURREY 
* #SALMONELLA ^property[1421].code = #child 
* #SALMONELLA ^property[1421].valueCode = #SVEDVI 
* #SALMONELLA ^property[1422].code = #child 
* #SALMONELLA ^property[1422].valueCode = #SYA 
* #SALMONELLA ^property[1423].code = #child 
* #SALMONELLA ^property[1423].valueCode = #SYLVANIA 
* #SALMONELLA ^property[1424].code = #child 
* #SALMONELLA ^property[1424].valueCode = #SZENTES 
* #SALMONELLA ^property[1425].code = #child 
* #SALMONELLA ^property[1425].valueCode = #TABLIGBO 
* #SALMONELLA ^property[1426].code = #child 
* #SALMONELLA ^property[1426].valueCode = #TADO 
* #SALMONELLA ^property[1427].code = #child 
* #SALMONELLA ^property[1427].valueCode = #TAFO 
* #SALMONELLA ^property[1428].code = #child 
* #SALMONELLA ^property[1428].valueCode = #TAIPING 
* #SALMONELLA ^property[1429].code = #child 
* #SALMONELLA ^property[1429].valueCode = #TAKORADI 
* #SALMONELLA ^property[1430].code = #child 
* #SALMONELLA ^property[1430].valueCode = #TAKSONY 
* #SALMONELLA ^property[1431].code = #child 
* #SALMONELLA ^property[1431].valueCode = #TALLAHASSEE 
* #SALMONELLA ^property[1432].code = #child 
* #SALMONELLA ^property[1432].valueCode = #TAMALE 
* #SALMONELLA ^property[1433].code = #child 
* #SALMONELLA ^property[1433].valueCode = #TAMBACOUNDA 
* #SALMONELLA ^property[1434].code = #child 
* #SALMONELLA ^property[1434].valueCode = #TAMBERMA 
* #SALMONELLA ^property[1435].code = #child 
* #SALMONELLA ^property[1435].valueCode = #TAMILNADU 
* #SALMONELLA ^property[1436].code = #child 
* #SALMONELLA ^property[1436].valueCode = #TAMPICO 
* #SALMONELLA ^property[1437].code = #child 
* #SALMONELLA ^property[1437].valueCode = #TANANARIVE 
* #SALMONELLA ^property[1438].code = #child 
* #SALMONELLA ^property[1438].valueCode = #TANGER 
* #SALMONELLA ^property[1439].code = #child 
* #SALMONELLA ^property[1439].valueCode = #TANZANIA 
* #SALMONELLA ^property[1440].code = #child 
* #SALMONELLA ^property[1440].valueCode = #TARSHYNE 
* #SALMONELLA ^property[1441].code = #child 
* #SALMONELLA ^property[1441].valueCode = #TASET 
* #SALMONELLA ^property[1442].code = #child 
* #SALMONELLA ^property[1442].valueCode = #TAUNTON 
* #SALMONELLA ^property[1443].code = #child 
* #SALMONELLA ^property[1443].valueCode = #TAYLOR 
* #SALMONELLA ^property[1444].code = #child 
* #SALMONELLA ^property[1444].valueCode = #TCHAD 
* #SALMONELLA ^property[1445].code = #child 
* #SALMONELLA ^property[1445].valueCode = #TCHAMBA 
* #SALMONELLA ^property[1446].code = #child 
* #SALMONELLA ^property[1446].valueCode = #TECHIMANI 
* #SALMONELLA ^property[1447].code = #child 
* #SALMONELLA ^property[1447].valueCode = #TEDDINGTON 
* #SALMONELLA ^property[1448].code = #child 
* #SALMONELLA ^property[1448].valueCode = #TEES 
* #SALMONELLA ^property[1449].code = #child 
* #SALMONELLA ^property[1449].valueCode = #TEJAS 
* #SALMONELLA ^property[1450].code = #child 
* #SALMONELLA ^property[1450].valueCode = #TEKO 
* #SALMONELLA ^property[1451].code = #child 
* #SALMONELLA ^property[1451].valueCode = #TELAVIV 
* #SALMONELLA ^property[1452].code = #child 
* #SALMONELLA ^property[1452].valueCode = #TELELKEBIR 
* #SALMONELLA ^property[1453].code = #child 
* #SALMONELLA ^property[1453].valueCode = #TELHASHOMER 
* #SALMONELLA ^property[1454].code = #child 
* #SALMONELLA ^property[1454].valueCode = #TELTOW 
* #SALMONELLA ^property[1455].code = #child 
* #SALMONELLA ^property[1455].valueCode = #TEMA 
* #SALMONELLA ^property[1456].code = #child 
* #SALMONELLA ^property[1456].valueCode = #TEMPE 
* #SALMONELLA ^property[1457].code = #child 
* #SALMONELLA ^property[1457].valueCode = #TENDEBA 
* #SALMONELLA ^property[1458].code = #child 
* #SALMONELLA ^property[1458].valueCode = #TENNENLOHE 
* #SALMONELLA ^property[1459].code = #child 
* #SALMONELLA ^property[1459].valueCode = #TENNESSEE 
* #SALMONELLA ^property[1460].code = #child 
* #SALMONELLA ^property[1460].valueCode = #TENNYSON 
* #SALMONELLA ^property[1461].code = #child 
* #SALMONELLA ^property[1461].valueCode = #TESHIE 
* #SALMONELLA ^property[1462].code = #child 
* #SALMONELLA ^property[1462].valueCode = #TEXAS 
* #SALMONELLA ^property[1463].code = #child 
* #SALMONELLA ^property[1463].valueCode = #THAYNGEN 
* #SALMONELLA ^property[1464].code = #child 
* #SALMONELLA ^property[1464].valueCode = #THETFORD 
* #SALMONELLA ^property[1465].code = #child 
* #SALMONELLA ^property[1465].valueCode = #THIAROYE 
* #SALMONELLA ^property[1466].code = #child 
* #SALMONELLA ^property[1466].valueCode = #THIES 
* #SALMONELLA ^property[1467].code = #child 
* #SALMONELLA ^property[1467].valueCode = #THOMPSON 
* #SALMONELLA ^property[1468].code = #child 
* #SALMONELLA ^property[1468].valueCode = #TIBATI 
* #SALMONELLA ^property[1469].code = #child 
* #SALMONELLA ^property[1469].valueCode = #TIENBA 
* #SALMONELLA ^property[1470].code = #child 
* #SALMONELLA ^property[1470].valueCode = #TIERGARTEN 
* #SALMONELLA ^property[1471].code = #child 
* #SALMONELLA ^property[1471].valueCode = #TIKO 
* #SALMONELLA ^property[1472].code = #child 
* #SALMONELLA ^property[1472].valueCode = #TILBURG 
* #SALMONELLA ^property[1473].code = #child 
* #SALMONELLA ^property[1473].valueCode = #TILENE 
* #SALMONELLA ^property[1474].code = #child 
* #SALMONELLA ^property[1474].valueCode = #TINDA 
* #SALMONELLA ^property[1475].code = #child 
* #SALMONELLA ^property[1475].valueCode = #TIONE 
* #SALMONELLA ^property[1476].code = #child 
* #SALMONELLA ^property[1476].valueCode = #TOGBA 
* #SALMONELLA ^property[1477].code = #child 
* #SALMONELLA ^property[1477].valueCode = #TOGO 
* #SALMONELLA ^property[1478].code = #child 
* #SALMONELLA ^property[1478].valueCode = #TOKOIN 
* #SALMONELLA ^property[1479].code = #child 
* #SALMONELLA ^property[1479].valueCode = #TOMEGBE 
* #SALMONELLA ^property[1480].code = #child 
* #SALMONELLA ^property[1480].valueCode = #TOMELILLA 
* #SALMONELLA ^property[1481].code = #child 
* #SALMONELLA ^property[1481].valueCode = #TONEV 
* #SALMONELLA ^property[1482].code = #child 
* #SALMONELLA ^property[1482].valueCode = #TOOWONG 
* #SALMONELLA ^property[1483].code = #child 
* #SALMONELLA ^property[1483].valueCode = #TORHOUT 
* #SALMONELLA ^property[1484].code = #child 
* #SALMONELLA ^property[1484].valueCode = #TORICADA 
* #SALMONELLA ^property[1485].code = #child 
* #SALMONELLA ^property[1485].valueCode = #TORNOW 
* #SALMONELLA ^property[1486].code = #child 
* #SALMONELLA ^property[1486].valueCode = #TORONTO 
* #SALMONELLA ^property[1487].code = #child 
* #SALMONELLA ^property[1487].valueCode = #TOUCRA 
* #SALMONELLA ^property[1488].code = #child 
* #SALMONELLA ^property[1488].valueCode = #TOULON 
* #SALMONELLA ^property[1489].code = #child 
* #SALMONELLA ^property[1489].valueCode = #TOUNOUMA 
* #SALMONELLA ^property[1490].code = #child 
* #SALMONELLA ^property[1490].valueCode = #TOURS 
* #SALMONELLA ^property[1491].code = #child 
* #SALMONELLA ^property[1491].valueCode = #TRACHAU 
* #SALMONELLA ^property[1492].code = #child 
* #SALMONELLA ^property[1492].valueCode = #TRANSVAAL 
* #SALMONELLA ^property[1493].code = #child 
* #SALMONELLA ^property[1493].valueCode = #TRAVIS 
* #SALMONELLA ^property[1494].code = #child 
* #SALMONELLA ^property[1494].valueCode = #TREFOREST 
* #SALMONELLA ^property[1495].code = #child 
* #SALMONELLA ^property[1495].valueCode = #TREGUIER 
* #SALMONELLA ^property[1496].code = #child 
* #SALMONELLA ^property[1496].valueCode = #TRIER 
* #SALMONELLA ^property[1497].code = #child 
* #SALMONELLA ^property[1497].valueCode = #TRIMDON 
* #SALMONELLA ^property[1498].code = #child 
* #SALMONELLA ^property[1498].valueCode = #TRIPOLI 
* #SALMONELLA ^property[1499].code = #child 
* #SALMONELLA ^property[1499].valueCode = #TROTHA 
* #SALMONELLA ^property[1500].code = #child 
* #SALMONELLA ^property[1500].valueCode = #TROY 
* #SALMONELLA ^property[1501].code = #child 
* #SALMONELLA ^property[1501].valueCode = #TRURO 
* #SALMONELLA ^property[1502].code = #child 
* #SALMONELLA ^property[1502].valueCode = #TSCHANGU 
* #SALMONELLA ^property[1503].code = #child 
* #SALMONELLA ^property[1503].valueCode = #TSEVIE 
* #SALMONELLA ^property[1504].code = #child 
* #SALMONELLA ^property[1504].valueCode = #TSHIONGWE 
* #SALMONELLA ^property[1505].code = #child 
* #SALMONELLA ^property[1505].valueCode = #TUCSON 
* #SALMONELLA ^property[1506].code = #child 
* #SALMONELLA ^property[1506].valueCode = #TUDU 
* #SALMONELLA ^property[1507].code = #child 
* #SALMONELLA ^property[1507].valueCode = #TUMODI 
* #SALMONELLA ^property[1508].code = #child 
* #SALMONELLA ^property[1508].valueCode = #TUNIS 
* #SALMONELLA ^property[1509].code = #child 
* #SALMONELLA ^property[1509].valueCode = #TYPHIMURIUM 
* #SALMONELLA ^property[1510].code = #child 
* #SALMONELLA ^property[1510].valueCode = #TYPHIMURIUM_VAR_O_5- 
* #SALMONELLA ^property[1511].code = #child 
* #SALMONELLA ^property[1511].valueCode = #TYPHISUIS 
* #SALMONELLA ^property[1512].code = #child 
* #SALMONELLA ^property[1512].valueCode = #TYRESOE 
* #SALMONELLA ^property[1513].code = #child 
* #SALMONELLA ^property[1513].valueCode = #UCCLE 
* #SALMONELLA ^property[1514].code = #child 
* #SALMONELLA ^property[1514].valueCode = #UGANDA 
* #SALMONELLA ^property[1515].code = #child 
* #SALMONELLA ^property[1515].valueCode = #UGANDA_VAR_15+_ 
* #SALMONELLA ^property[1516].code = #child 
* #SALMONELLA ^property[1516].valueCode = #UGHELLI 
* #SALMONELLA ^property[1517].code = #child 
* #SALMONELLA ^property[1517].valueCode = #UHLENHORST 
* #SALMONELLA ^property[1518].code = #child 
* #SALMONELLA ^property[1518].valueCode = #UITHOF 
* #SALMONELLA ^property[1519].code = #child 
* #SALMONELLA ^property[1519].valueCode = #ULLEVI 
* #SALMONELLA ^property[1520].code = #child 
* #SALMONELLA ^property[1520].valueCode = #UMBADAH 
* #SALMONELLA ^property[1521].code = #child 
* #SALMONELLA ^property[1521].valueCode = #UMBILO 
* #SALMONELLA ^property[1522].code = #child 
* #SALMONELLA ^property[1522].valueCode = #UMHLALI 
* #SALMONELLA ^property[1523].code = #child 
* #SALMONELLA ^property[1523].valueCode = #UMHLATAZANA 
* #SALMONELLA ^property[1524].code = #child 
* #SALMONELLA ^property[1524].valueCode = #UNK 
* #SALMONELLA ^property[1525].code = #child 
* #SALMONELLA ^property[1525].valueCode = #UNO 
* #SALMONELLA ^property[1526].code = #child 
* #SALMONELLA ^property[1526].valueCode = #UPPSALA 
* #SALMONELLA ^property[1527].code = #child 
* #SALMONELLA ^property[1527].valueCode = #URBANA 
* #SALMONELLA ^property[1528].code = #child 
* #SALMONELLA ^property[1528].valueCode = #URSENBACH 
* #SALMONELLA ^property[1529].code = #child 
* #SALMONELLA ^property[1529].valueCode = #USUMBURA 
* #SALMONELLA ^property[1530].code = #child 
* #SALMONELLA ^property[1530].valueCode = #UTAH 
* #SALMONELLA ^property[1531].code = #child 
* #SALMONELLA ^property[1531].valueCode = #UTRECHT 
* #SALMONELLA ^property[1532].code = #child 
* #SALMONELLA ^property[1532].valueCode = #UZARAMO 
* #SALMONELLA ^property[1533].code = #child 
* #SALMONELLA ^property[1533].valueCode = #VAERTAN 
* #SALMONELLA ^property[1534].code = #child 
* #SALMONELLA ^property[1534].valueCode = #VALDOSTA 
* #SALMONELLA ^property[1535].code = #child 
* #SALMONELLA ^property[1535].valueCode = #VANCOUVER 
* #SALMONELLA ^property[1536].code = #child 
* #SALMONELLA ^property[1536].valueCode = #VANIER 
* #SALMONELLA ^property[1537].code = #child 
* #SALMONELLA ^property[1537].valueCode = #VAUGIRARD 
* #SALMONELLA ^property[1538].code = #child 
* #SALMONELLA ^property[1538].valueCode = #VEGESACK 
* #SALMONELLA ^property[1539].code = #child 
* #SALMONELLA ^property[1539].valueCode = #VEJLE 
* #SALMONELLA ^property[1540].code = #child 
* #SALMONELLA ^property[1540].valueCode = #VEJLE_VAR_15+ 
* #SALMONELLA ^property[1541].code = #child 
* #SALMONELLA ^property[1541].valueCode = #VELLORE 
* #SALMONELLA ^property[1542].code = #child 
* #SALMONELLA ^property[1542].valueCode = #VENEZIANA 
* #SALMONELLA ^property[1543].code = #child 
* #SALMONELLA ^property[1543].valueCode = #VERONA 
* #SALMONELLA ^property[1544].code = #child 
* #SALMONELLA ^property[1544].valueCode = #VERVIERS 
* #SALMONELLA ^property[1545].code = #child 
* #SALMONELLA ^property[1545].valueCode = #VICTORIA 
* #SALMONELLA ^property[1546].code = #child 
* #SALMONELLA ^property[1546].valueCode = #VICTORIABORG 
* #SALMONELLA ^property[1547].code = #child 
* #SALMONELLA ^property[1547].valueCode = #VIETNAM 
* #SALMONELLA ^property[1548].code = #child 
* #SALMONELLA ^property[1548].valueCode = #VILVOORDE 
* #SALMONELLA ^property[1549].code = #child 
* #SALMONELLA ^property[1549].valueCode = #VINOHRADY 
* #SALMONELLA ^property[1550].code = #child 
* #SALMONELLA ^property[1550].valueCode = #VIRCHOW 
* #SALMONELLA ^property[1551].code = #child 
* #SALMONELLA ^property[1551].valueCode = #VIRGINIA 
* #SALMONELLA ^property[1552].code = #child 
* #SALMONELLA ^property[1552].valueCode = #VISBY 
* #SALMONELLA ^property[1553].code = #child 
* #SALMONELLA ^property[1553].valueCode = #VITKIN 
* #SALMONELLA ^property[1554].code = #child 
* #SALMONELLA ^property[1554].valueCode = #VLEUTEN 
* #SALMONELLA ^property[1555].code = #child 
* #SALMONELLA ^property[1555].valueCode = #VOGAN 
* #SALMONELLA ^property[1556].code = #child 
* #SALMONELLA ^property[1556].valueCode = #VOLKSMARSDORF 
* #SALMONELLA ^property[1557].code = #child 
* #SALMONELLA ^property[1557].valueCode = #VOLTA 
* #SALMONELLA ^property[1558].code = #child 
* #SALMONELLA ^property[1558].valueCode = #VOM 
* #SALMONELLA ^property[1559].code = #child 
* #SALMONELLA ^property[1559].valueCode = #VOULTE 
* #SALMONELLA ^property[1560].code = #child 
* #SALMONELLA ^property[1560].valueCode = #VRIDI 
* #SALMONELLA ^property[1561].code = #child 
* #SALMONELLA ^property[1561].valueCode = #VUADENS 
* #SALMONELLA ^property[1562].code = #child 
* #SALMONELLA ^property[1562].valueCode = #WA 
* #SALMONELLA ^property[1563].code = #child 
* #SALMONELLA ^property[1563].valueCode = #WAEDENSWIL 
* #SALMONELLA ^property[1564].code = #child 
* #SALMONELLA ^property[1564].valueCode = #WAGADUGU 
* #SALMONELLA ^property[1565].code = #child 
* #SALMONELLA ^property[1565].valueCode = #WAGENIA 
* #SALMONELLA ^property[1566].code = #child 
* #SALMONELLA ^property[1566].valueCode = #WANATAH 
* #SALMONELLA ^property[1567].code = #child 
* #SALMONELLA ^property[1567].valueCode = #WANDSWORTH 
* #SALMONELLA ^property[1568].code = #child 
* #SALMONELLA ^property[1568].valueCode = #WANGATA 
* #SALMONELLA ^property[1569].code = #child 
* #SALMONELLA ^property[1569].valueCode = #WARAL 
* #SALMONELLA ^property[1570].code = #child 
* #SALMONELLA ^property[1570].valueCode = #WARENGO 
* #SALMONELLA ^property[1571].code = #child 
* #SALMONELLA ^property[1571].valueCode = #WARMSEN 
* #SALMONELLA ^property[1572].code = #child 
* #SALMONELLA ^property[1572].valueCode = #WARNEMUENDE 
* #SALMONELLA ^property[1573].code = #child 
* #SALMONELLA ^property[1573].valueCode = #WARNOW 
* #SALMONELLA ^property[1574].code = #child 
* #SALMONELLA ^property[1574].valueCode = #WARRAGUL 
* #SALMONELLA ^property[1575].code = #child 
* #SALMONELLA ^property[1575].valueCode = #WARRI 
* #SALMONELLA ^property[1576].code = #child 
* #SALMONELLA ^property[1576].valueCode = #WASHINGTON 
* #SALMONELLA ^property[1577].code = #child 
* #SALMONELLA ^property[1577].valueCode = #WAYCROSS 
* #SALMONELLA ^property[1578].code = #child 
* #SALMONELLA ^property[1578].valueCode = #WAYNE 
* #SALMONELLA ^property[1579].code = #child 
* #SALMONELLA ^property[1579].valueCode = #WEDDING 
* #SALMONELLA ^property[1580].code = #child 
* #SALMONELLA ^property[1580].valueCode = #WELIKADE 
* #SALMONELLA ^property[1581].code = #child 
* #SALMONELLA ^property[1581].valueCode = #WELTEVREDEN 
* #SALMONELLA ^property[1582].code = #child 
* #SALMONELLA ^property[1582].valueCode = #WELTEVREDEN_VAR_15+_ 
* #SALMONELLA ^property[1583].code = #child 
* #SALMONELLA ^property[1583].valueCode = #WENATCHEE 
* #SALMONELLA ^property[1584].code = #child 
* #SALMONELLA ^property[1584].valueCode = #WENTWORTH 
* #SALMONELLA ^property[1585].code = #child 
* #SALMONELLA ^property[1585].valueCode = #WERNIGERODE 
* #SALMONELLA ^property[1586].code = #child 
* #SALMONELLA ^property[1586].valueCode = #WESLACO 
* #SALMONELLA ^property[1587].code = #child 
* #SALMONELLA ^property[1587].valueCode = #WESTAFRICA 
* #SALMONELLA ^property[1588].code = #child 
* #SALMONELLA ^property[1588].valueCode = #WESTEINDE 
* #SALMONELLA ^property[1589].code = #child 
* #SALMONELLA ^property[1589].valueCode = #WESTERSTEDE 
* #SALMONELLA ^property[1590].code = #child 
* #SALMONELLA ^property[1590].valueCode = #WESTHAMPTON 
* #SALMONELLA ^property[1591].code = #child 
* #SALMONELLA ^property[1591].valueCode = #WESTHAMPTON_VAR_15+ 
* #SALMONELLA ^property[1592].code = #child 
* #SALMONELLA ^property[1592].valueCode = #WESTHAMPTON_VAR_15+_34+_ 
* #SALMONELLA ^property[1593].code = #child 
* #SALMONELLA ^property[1593].valueCode = #WESTMINSTER 
* #SALMONELLA ^property[1594].code = #child 
* #SALMONELLA ^property[1594].valueCode = #WESTON 
* #SALMONELLA ^property[1595].code = #child 
* #SALMONELLA ^property[1595].valueCode = #WESTPHALIA 
* #SALMONELLA ^property[1596].code = #child 
* #SALMONELLA ^property[1596].valueCode = #WEYBRIDGE 
* #SALMONELLA ^property[1597].code = #child 
* #SALMONELLA ^property[1597].valueCode = #WICHITA 
* #SALMONELLA ^property[1598].code = #child 
* #SALMONELLA ^property[1598].valueCode = #WIDEMARSH 
* #SALMONELLA ^property[1599].code = #child 
* #SALMONELLA ^property[1599].valueCode = #WIEN 
* #SALMONELLA ^property[1600].code = #child 
* #SALMONELLA ^property[1600].valueCode = #WIL 
* #SALMONELLA ^property[1601].code = #child 
* #SALMONELLA ^property[1601].valueCode = #WILHELMSBURG 
* #SALMONELLA ^property[1602].code = #child 
* #SALMONELLA ^property[1602].valueCode = #WILLAMETTE 
* #SALMONELLA ^property[1603].code = #child 
* #SALMONELLA ^property[1603].valueCode = #WILLEMSTAD 
* #SALMONELLA ^property[1604].code = #child 
* #SALMONELLA ^property[1604].valueCode = #WILMINGTON 
* #SALMONELLA ^property[1605].code = #child 
* #SALMONELLA ^property[1605].valueCode = #WIMBORNE 
* #SALMONELLA ^property[1606].code = #child 
* #SALMONELLA ^property[1606].valueCode = #WINDERMERE 
* #SALMONELLA ^property[1607].code = #child 
* #SALMONELLA ^property[1607].valueCode = #WINDSHEIM 
* #SALMONELLA ^property[1608].code = #child 
* #SALMONELLA ^property[1608].valueCode = #WINGROVE 
* #SALMONELLA ^property[1609].code = #child 
* #SALMONELLA ^property[1609].valueCode = #WINNEBA 
* #SALMONELLA ^property[1610].code = #child 
* #SALMONELLA ^property[1610].valueCode = #WINNIPEG 
* #SALMONELLA ^property[1611].code = #child 
* #SALMONELLA ^property[1611].valueCode = #WINSLOW 
* #SALMONELLA ^property[1612].code = #child 
* #SALMONELLA ^property[1612].valueCode = #WINSTON 
* #SALMONELLA ^property[1613].code = #child 
* #SALMONELLA ^property[1613].valueCode = #WINTERTHUR 
* #SALMONELLA ^property[1614].code = #child 
* #SALMONELLA ^property[1614].valueCode = #WIPPRA 
* #SALMONELLA ^property[1615].code = #child 
* #SALMONELLA ^property[1615].valueCode = #WISBECH 
* #SALMONELLA ^property[1616].code = #child 
* #SALMONELLA ^property[1616].valueCode = #WOHLEN 
* #SALMONELLA ^property[1617].code = #child 
* #SALMONELLA ^property[1617].valueCode = #WOODHULL 
* #SALMONELLA ^property[1618].code = #child 
* #SALMONELLA ^property[1618].valueCode = #WOODINVILLE 
* #SALMONELLA ^property[1619].code = #child 
* #SALMONELLA ^property[1619].valueCode = #WORB 
* #SALMONELLA ^property[1620].code = #child 
* #SALMONELLA ^property[1620].valueCode = #WORTHINGTON 
* #SALMONELLA ^property[1621].code = #child 
* #SALMONELLA ^property[1621].valueCode = #WOUMBOU 
* #SALMONELLA ^property[1622].code = #child 
* #SALMONELLA ^property[1622].valueCode = #WUITI 
* #SALMONELLA ^property[1623].code = #child 
* #SALMONELLA ^property[1623].valueCode = #WUPPERTAL 
* #SALMONELLA ^property[1624].code = #child 
* #SALMONELLA ^property[1624].valueCode = #WYLDEGREEN 
* #SALMONELLA ^property[1625].code = #child 
* #SALMONELLA ^property[1625].valueCode = #YABA 
* #SALMONELLA ^property[1626].code = #child 
* #SALMONELLA ^property[1626].valueCode = #YALDING 
* #SALMONELLA ^property[1627].code = #child 
* #SALMONELLA ^property[1627].valueCode = #YAOUNDE 
* #SALMONELLA ^property[1628].code = #child 
* #SALMONELLA ^property[1628].valueCode = #YARDLEY 
* #SALMONELLA ^property[1629].code = #child 
* #SALMONELLA ^property[1629].valueCode = #YARM 
* #SALMONELLA ^property[1630].code = #child 
* #SALMONELLA ^property[1630].valueCode = #YARRABAH 
* #SALMONELLA ^property[1631].code = #child 
* #SALMONELLA ^property[1631].valueCode = #YEERONGPILLY 
* #SALMONELLA ^property[1632].code = #child 
* #SALMONELLA ^property[1632].valueCode = #YEHUDA 
* #SALMONELLA ^property[1633].code = #child 
* #SALMONELLA ^property[1633].valueCode = #YEKEPA 
* #SALMONELLA ^property[1634].code = #child 
* #SALMONELLA ^property[1634].valueCode = #YELLOWKNIFE 
* #SALMONELLA ^property[1635].code = #child 
* #SALMONELLA ^property[1635].valueCode = #YENNE 
* #SALMONELLA ^property[1636].code = #child 
* #SALMONELLA ^property[1636].valueCode = #YERBA 
* #SALMONELLA ^property[1637].code = #child 
* #SALMONELLA ^property[1637].valueCode = #YOFF 
* #SALMONELLA ^property[1638].code = #child 
* #SALMONELLA ^property[1638].valueCode = #YOKOE 
* #SALMONELLA ^property[1639].code = #child 
* #SALMONELLA ^property[1639].valueCode = #YOLO 
* #SALMONELLA ^property[1640].code = #child 
* #SALMONELLA ^property[1640].valueCode = #YOMBESALI 
* #SALMONELLA ^property[1641].code = #child 
* #SALMONELLA ^property[1641].valueCode = #YOPOUGON 
* #SALMONELLA ^property[1642].code = #child 
* #SALMONELLA ^property[1642].valueCode = #YORK 
* #SALMONELLA ^property[1643].code = #child 
* #SALMONELLA ^property[1643].valueCode = #YORUBA 
* #SALMONELLA ^property[1644].code = #child 
* #SALMONELLA ^property[1644].valueCode = #YOVOKOME 
* #SALMONELLA ^property[1645].code = #child 
* #SALMONELLA ^property[1645].valueCode = #YUNDUM 
* #SALMONELLA ^property[1646].code = #child 
* #SALMONELLA ^property[1646].valueCode = #ZADAR 
* #SALMONELLA ^property[1647].code = #child 
* #SALMONELLA ^property[1647].valueCode = #ZAIMAN 
* #SALMONELLA ^property[1648].code = #child 
* #SALMONELLA ^property[1648].valueCode = #ZAIRE 
* #SALMONELLA ^property[1649].code = #child 
* #SALMONELLA ^property[1649].valueCode = #ZANZIBAR 
* #SALMONELLA ^property[1650].code = #child 
* #SALMONELLA ^property[1650].valueCode = #ZARIA 
* #SALMONELLA ^property[1651].code = #child 
* #SALMONELLA ^property[1651].valueCode = #ZEGA 
* #SALMONELLA ^property[1652].code = #child 
* #SALMONELLA ^property[1652].valueCode = #ZEHLENDORF 
* #SALMONELLA ^property[1653].code = #child 
* #SALMONELLA ^property[1653].valueCode = #ZERIFIN 
* #SALMONELLA ^property[1654].code = #child 
* #SALMONELLA ^property[1654].valueCode = #ZIGONG 
* #SALMONELLA ^property[1655].code = #child 
* #SALMONELLA ^property[1655].valueCode = #ZINDER 
* #SALMONELLA ^property[1656].code = #child 
* #SALMONELLA ^property[1656].valueCode = #ZONGO 
* #SALMONELLA ^property[1657].code = #child 
* #SALMONELLA ^property[1657].valueCode = #ZUILEN 
* #SALMONELLA ^property[1658].code = #child 
* #SALMONELLA ^property[1658].valueCode = #ZWICKAU 
* #AACHEN "Aachen"
* #AACHEN ^property[0].code = #parent 
* #AACHEN ^property[0].valueCode = #SALMONELLA 
* #AARHUS "Aarhus"
* #AARHUS ^property[0].code = #parent 
* #AARHUS ^property[0].valueCode = #SALMONELLA 
* #ABA "Aba"
* #ABA ^property[0].code = #parent 
* #ABA ^property[0].valueCode = #SALMONELLA 
* #ABADINA "Abadina"
* #ABADINA ^property[0].code = #parent 
* #ABADINA ^property[0].valueCode = #SALMONELLA 
* #ABAETETUBA "Abaetetuba"
* #ABAETETUBA ^property[0].code = #parent 
* #ABAETETUBA ^property[0].valueCode = #SALMONELLA 
* #ABERDEEN "Aberdeen"
* #ABERDEEN ^property[0].code = #parent 
* #ABERDEEN ^property[0].valueCode = #SALMONELLA 
* #ABIDJAN "Abidjan"
* #ABIDJAN ^property[0].code = #parent 
* #ABIDJAN ^property[0].valueCode = #SALMONELLA 
* #ABLOGAME "Ablogame"
* #ABLOGAME ^property[0].code = #parent 
* #ABLOGAME ^property[0].valueCode = #SALMONELLA 
* #ABOBO "Abobo"
* #ABOBO ^property[0].code = #parent 
* #ABOBO ^property[0].valueCode = #SALMONELLA 
* #ABONY "Abony"
* #ABONY ^property[0].code = #parent 
* #ABONY ^property[0].valueCode = #SALMONELLA 
* #ABORTUSEQUI "Abortusequi"
* #ABORTUSEQUI ^property[0].code = #parent 
* #ABORTUSEQUI ^property[0].valueCode = #SALMONELLA 
* #ABORTUSOVIS "Abortusovis"
* #ABORTUSOVIS ^property[0].code = #parent 
* #ABORTUSOVIS ^property[0].valueCode = #SALMONELLA 
* #ABUJA "Abuja"
* #ABUJA ^property[0].code = #parent 
* #ABUJA ^property[0].valueCode = #SALMONELLA 
* #ACCRA "Accra"
* #ACCRA ^property[0].code = #parent 
* #ACCRA ^property[0].valueCode = #SALMONELLA 
* #ACKWEPE "Ackwepe"
* #ACKWEPE ^property[0].code = #parent 
* #ACKWEPE ^property[0].valueCode = #SALMONELLA 
* #ADABRAKA "Adabraka"
* #ADABRAKA ^property[0].code = #parent 
* #ADABRAKA ^property[0].valueCode = #SALMONELLA 
* #ADAMSTOWN "Adamstown"
* #ADAMSTOWN ^property[0].code = #parent 
* #ADAMSTOWN ^property[0].valueCode = #SALMONELLA 
* #ADAMSTUA "Adamstua"
* #ADAMSTUA ^property[0].code = #parent 
* #ADAMSTUA ^property[0].valueCode = #SALMONELLA 
* #ADANA "Adana"
* #ADANA ^property[0].code = #parent 
* #ADANA ^property[0].valueCode = #SALMONELLA 
* #ADELAIDE "Adelaide"
* #ADELAIDE ^property[0].code = #parent 
* #ADELAIDE ^property[0].valueCode = #SALMONELLA 
* #ADEOYO "Adeoyo"
* #ADEOYO ^property[0].code = #parent 
* #ADEOYO ^property[0].valueCode = #SALMONELLA 
* #ADERIKE "Aderike"
* #ADERIKE ^property[0].code = #parent 
* #ADERIKE ^property[0].valueCode = #SALMONELLA 
* #ADIME "Adime"
* #ADIME ^property[0].code = #parent 
* #ADIME ^property[0].valueCode = #SALMONELLA 
* #ADJAME "Adjame"
* #ADJAME ^property[0].code = #parent 
* #ADJAME ^property[0].valueCode = #SALMONELLA 
* #AEQUATORIA "Aequatoria"
* #AEQUATORIA ^property[0].code = #parent 
* #AEQUATORIA ^property[0].valueCode = #SALMONELLA 
* #AESCH "Aesch"
* #AESCH ^property[0].code = #parent 
* #AESCH ^property[0].valueCode = #SALMONELLA 
* #AFLAO "Aflao"
* #AFLAO ^property[0].code = #parent 
* #AFLAO ^property[0].valueCode = #SALMONELLA 
* #AFRICANA "Africana"
* #AFRICANA ^property[0].code = #parent 
* #AFRICANA ^property[0].valueCode = #SALMONELLA 
* #AFULA "Afula"
* #AFULA ^property[0].code = #parent 
* #AFULA ^property[0].valueCode = #SALMONELLA 
* #AGAMA "Agama"
* #AGAMA ^property[0].code = #parent 
* #AGAMA ^property[0].valueCode = #SALMONELLA 
* #AGBARA "Agbara"
* #AGBARA ^property[0].code = #parent 
* #AGBARA ^property[0].valueCode = #SALMONELLA 
* #AGBENI "Agbeni"
* #AGBENI ^property[0].code = #parent 
* #AGBENI ^property[0].valueCode = #SALMONELLA 
* #AGEGE "Agege"
* #AGEGE ^property[0].code = #parent 
* #AGEGE ^property[0].valueCode = #SALMONELLA 
* #AGO "Ago"
* #AGO ^property[0].code = #parent 
* #AGO ^property[0].valueCode = #SALMONELLA 
* #AGODI "Agodi"
* #AGODI ^property[0].code = #parent 
* #AGODI ^property[0].valueCode = #SALMONELLA 
* #AGONA "Agona"
* #AGONA ^property[0].code = #parent 
* #AGONA ^property[0].valueCode = #SALMONELLA 
* #AGOUEVE "Agoueve"
* #AGOUEVE ^property[0].code = #parent 
* #AGOUEVE ^property[0].valueCode = #SALMONELLA 
* #AHANOU "Ahanou"
* #AHANOU ^property[0].code = #parent 
* #AHANOU ^property[0].valueCode = #SALMONELLA 
* #AHEPE "Ahepe"
* #AHEPE ^property[0].code = #parent 
* #AHEPE ^property[0].valueCode = #SALMONELLA 
* #AHMADI "Ahmadi"
* #AHMADI ^property[0].code = #parent 
* #AHMADI ^property[0].valueCode = #SALMONELLA 
* #AHOUTOUE "Ahoutoue"
* #AHOUTOUE ^property[0].code = #parent 
* #AHOUTOUE ^property[0].valueCode = #SALMONELLA 
* #AHUZA "Ahuza"
* #AHUZA ^property[0].code = #parent 
* #AHUZA ^property[0].valueCode = #SALMONELLA 
* #AJIOBO "Ajiobo"
* #AJIOBO ^property[0].code = #parent 
* #AJIOBO ^property[0].valueCode = #SALMONELLA 
* #AKANJI "Akanji"
* #AKANJI ^property[0].code = #parent 
* #AKANJI ^property[0].valueCode = #SALMONELLA 
* #AKUAFO "Akuafo"
* #AKUAFO ^property[0].code = #parent 
* #AKUAFO ^property[0].valueCode = #SALMONELLA 
* #ALABAMA "Alabama"
* #ALABAMA ^property[0].code = #parent 
* #ALABAMA ^property[0].valueCode = #SALMONELLA 
* #ALACHUA "Alachua"
* #ALACHUA ^property[0].code = #parent 
* #ALACHUA ^property[0].valueCode = #SALMONELLA 
* #ALAGBON "Alagbon"
* #ALAGBON ^property[0].code = #parent 
* #ALAGBON ^property[0].valueCode = #SALMONELLA 
* #ALAMO "Alamo"
* #ALAMO ^property[0].code = #parent 
* #ALAMO ^property[0].valueCode = #SALMONELLA 
* #ALBANY "Albany"
* #ALBANY ^property[0].code = #parent 
* #ALBANY ^property[0].valueCode = #SALMONELLA 
* #ALBERT "Albert"
* #ALBERT ^property[0].code = #parent 
* #ALBERT ^property[0].valueCode = #SALMONELLA 
* #ALBERTBANJUL "Albertbanjul"
* #ALBERTBANJUL ^property[0].code = #parent 
* #ALBERTBANJUL ^property[0].valueCode = #SALMONELLA 
* #ALBERTSLUND "Albertslund"
* #ALBERTSLUND ^property[0].code = #parent 
* #ALBERTSLUND ^property[0].valueCode = #SALMONELLA 
* #ALBUQUERQUE "Albuquerque"
* #ALBUQUERQUE ^property[0].code = #parent 
* #ALBUQUERQUE ^property[0].valueCode = #SALMONELLA 
* #ALEXANDERPLATZ "Alexanderplatz"
* #ALEXANDERPLATZ ^property[0].code = #parent 
* #ALEXANDERPLATZ ^property[0].valueCode = #SALMONELLA 
* #ALEXANDERPOLDER "Alexanderpolder"
* #ALEXANDERPOLDER ^property[0].code = #parent 
* #ALEXANDERPOLDER ^property[0].valueCode = #SALMONELLA 
* #ALFORT "Alfort"
* #ALFORT ^property[0].code = #parent 
* #ALFORT ^property[0].valueCode = #SALMONELLA 
* #ALGER "Alger"
* #ALGER ^property[0].code = #parent 
* #ALGER ^property[0].valueCode = #SALMONELLA 
* #ALKMAAR "Alkmaar"
* #ALKMAAR ^property[0].code = #parent 
* #ALKMAAR ^property[0].valueCode = #SALMONELLA 
* #ALLANDALE "Allandale"
* #ALLANDALE ^property[0].code = #parent 
* #ALLANDALE ^property[0].valueCode = #SALMONELLA 
* #ALLERTON "Allerton"
* #ALLERTON ^property[0].code = #parent 
* #ALLERTON ^property[0].valueCode = #SALMONELLA 
* #ALMA "Alma"
* #ALMA ^property[0].code = #parent 
* #ALMA ^property[0].valueCode = #SALMONELLA 
* #ALMINKO "Alminko"
* #ALMINKO ^property[0].code = #parent 
* #ALMINKO ^property[0].valueCode = #SALMONELLA 
* #ALPENQUAI "Alpenquai"
* #ALPENQUAI ^property[0].code = #parent 
* #ALPENQUAI ^property[0].valueCode = #SALMONELLA 
* #ALTENDORF "Altendorf"
* #ALTENDORF ^property[0].code = #parent 
* #ALTENDORF ^property[0].valueCode = #SALMONELLA 
* #ALTONA "Altona"
* #ALTONA ^property[0].code = #parent 
* #ALTONA ^property[0].valueCode = #SALMONELLA 
* #AMAGER "Amager"
* #AMAGER ^property[0].code = #parent 
* #AMAGER ^property[0].valueCode = #SALMONELLA 
* #AMAGER_VAR_15+ "Amager var. 15+"
* #AMAGER_VAR_15+ ^property[0].code = #parent 
* #AMAGER_VAR_15+ ^property[0].valueCode = #SALMONELLA 
* #AMBA "Amba"
* #AMBA ^property[0].code = #parent 
* #AMBA ^property[0].valueCode = #SALMONELLA 
* #AMERSFOORT "Amersfoort"
* #AMERSFOORT ^property[0].code = #parent 
* #AMERSFOORT ^property[0].valueCode = #SALMONELLA 
* #AMERSFOORT_VAR_14+ "Amersfoort var. 14+"
* #AMERSFOORT_VAR_14+ ^property[0].code = #parent 
* #AMERSFOORT_VAR_14+ ^property[0].valueCode = #SALMONELLA 
* #AMHERSTIANA "Amherstiana"
* #AMHERSTIANA ^property[0].code = #parent 
* #AMHERSTIANA ^property[0].valueCode = #SALMONELLA 
* #AMINA "Amina"
* #AMINA ^property[0].code = #parent 
* #AMINA ^property[0].valueCode = #SALMONELLA 
* #AMINATU "Aminatu"
* #AMINATU ^property[0].code = #parent 
* #AMINATU ^property[0].valueCode = #SALMONELLA 
* #AMOUNDERNESS "Amounderness"
* #AMOUNDERNESS ^property[0].code = #parent 
* #AMOUNDERNESS ^property[0].valueCode = #SALMONELLA 
* #AMOUTIVE "Amoutive"
* #AMOUTIVE ^property[0].code = #parent 
* #AMOUTIVE ^property[0].valueCode = #SALMONELLA 
* #AMSTERDAM "Amsterdam"
* #AMSTERDAM ^property[0].code = #parent 
* #AMSTERDAM ^property[0].valueCode = #SALMONELLA 
* #AMSTERDAM_VAR_15+ "Amsterdam var. 15+"
* #AMSTERDAM_VAR_15+ ^property[0].code = #parent 
* #AMSTERDAM_VAR_15+ ^property[0].valueCode = #SALMONELLA 
* #AMUNIGUN "Amunigun"
* #AMUNIGUN ^property[0].code = #parent 
* #AMUNIGUN ^property[0].valueCode = #SALMONELLA 
* #ANATUM "Anatum"
* #ANATUM ^property[0].code = #parent 
* #ANATUM ^property[0].valueCode = #SALMONELLA 
* #ANATUM_VAR_15+_ "Anatum var. 15+"
* #ANATUM_VAR_15+_ ^property[0].code = #parent 
* #ANATUM_VAR_15+_ ^property[0].valueCode = #SALMONELLA 
* #ANATUM_VAR_15+_34+_ "Anatum var. 15+, 34+"
* #ANATUM_VAR_15+_34+_ ^property[0].code = #parent 
* #ANATUM_VAR_15+_34+_ ^property[0].valueCode = #SALMONELLA 
* #ANDERLECHT "Anderlecht"
* #ANDERLECHT ^property[0].code = #parent 
* #ANDERLECHT ^property[0].valueCode = #SALMONELLA 
* #ANECHO "Anecho"
* #ANECHO ^property[0].code = #parent 
* #ANECHO ^property[0].valueCode = #SALMONELLA 
* #ANFO "Anfo"
* #ANFO ^property[0].code = #parent 
* #ANFO ^property[0].valueCode = #SALMONELLA 
* #ANGERS "Angers"
* #ANGERS ^property[0].code = #parent 
* #ANGERS ^property[0].valueCode = #SALMONELLA 
* #ANGODA "Angoda"
* #ANGODA ^property[0].code = #parent 
* #ANGODA ^property[0].valueCode = #SALMONELLA 
* #ANGOULEME "Angouleme"
* #ANGOULEME ^property[0].code = #parent 
* #ANGOULEME ^property[0].valueCode = #SALMONELLA 
* #ANK "Ank"
* #ANK ^property[0].code = #parent 
* #ANK ^property[0].valueCode = #SALMONELLA 
* #ANNA "Anna"
* #ANNA ^property[0].code = #parent 
* #ANNA ^property[0].valueCode = #SALMONELLA 
* #ANNEDAL "Annedal"
* #ANNEDAL ^property[0].code = #parent 
* #ANNEDAL ^property[0].valueCode = #SALMONELLA 
* #ANTARCTICA "Antarctica"
* #ANTARCTICA ^property[0].code = #parent 
* #ANTARCTICA ^property[0].valueCode = #SALMONELLA 
* #ANTONIO "Antonio"
* #ANTONIO ^property[0].code = #parent 
* #ANTONIO ^property[0].valueCode = #SALMONELLA 
* #ANTSALOVA "Antsalova"
* #ANTSALOVA ^property[0].code = #parent 
* #ANTSALOVA ^property[0].valueCode = #SALMONELLA 
* #ANTWERPEN "Antwerpen"
* #ANTWERPEN ^property[0].code = #parent 
* #ANTWERPEN ^property[0].valueCode = #SALMONELLA 
* #APAPA "Apapa"
* #APAPA ^property[0].code = #parent 
* #APAPA ^property[0].valueCode = #SALMONELLA 
* #APEYEME "Apeyeme"
* #APEYEME ^property[0].code = #parent 
* #APEYEME ^property[0].valueCode = #SALMONELLA 
* #APRAD "Aprad"
* #APRAD ^property[0].code = #parent 
* #APRAD ^property[0].valueCode = #SALMONELLA 
* #AQUA "Aqua"
* #AQUA ^property[0].code = #parent 
* #AQUA ^property[0].valueCode = #SALMONELLA 
* #ARAGUA "Aragua"
* #ARAGUA ^property[0].code = #parent 
* #ARAGUA ^property[0].valueCode = #SALMONELLA 
* #ARAPAHOE "Arapahoe"
* #ARAPAHOE ^property[0].code = #parent 
* #ARAPAHOE ^property[0].valueCode = #SALMONELLA 
* #ARECHAVALETA "Arechavaleta"
* #ARECHAVALETA ^property[0].code = #parent 
* #ARECHAVALETA ^property[0].valueCode = #SALMONELLA 
* #ARGENTEUIL "Argenteuil"
* #ARGENTEUIL ^property[0].code = #parent 
* #ARGENTEUIL ^property[0].valueCode = #SALMONELLA 
* #ARIZONAE "Arizonae (Subsp IIIa)"
* #ARIZONAE ^property[0].code = #parent 
* #ARIZONAE ^property[0].valueCode = #SALMONELLA 
* #ARUSHA "Arusha"
* #ARUSHA ^property[0].code = #parent 
* #ARUSHA ^property[0].valueCode = #SALMONELLA 
* #ASCHERSLEBEN "Aschersleben"
* #ASCHERSLEBEN ^property[0].code = #parent 
* #ASCHERSLEBEN ^property[0].valueCode = #SALMONELLA 
* #ASHANTI "Ashanti"
* #ASHANTI ^property[0].code = #parent 
* #ASHANTI ^property[0].valueCode = #SALMONELLA 
* #ASSEN "Assen"
* #ASSEN ^property[0].code = #parent 
* #ASSEN ^property[0].valueCode = #SALMONELLA 
* #ASSINIE "Assinie"
* #ASSINIE ^property[0].code = #parent 
* #ASSINIE ^property[0].valueCode = #SALMONELLA 
* #ASTRIDPLEIN "Astridplein"
* #ASTRIDPLEIN ^property[0].code = #parent 
* #ASTRIDPLEIN ^property[0].valueCode = #SALMONELLA 
* #ASYLANTA "Asylanta"
* #ASYLANTA ^property[0].code = #parent 
* #ASYLANTA ^property[0].valueCode = #SALMONELLA 
* #ATAKPAME "Atakpame"
* #ATAKPAME ^property[0].code = #parent 
* #ATAKPAME ^property[0].valueCode = #SALMONELLA 
* #ATENTO "Atento"
* #ATENTO ^property[0].code = #parent 
* #ATENTO ^property[0].valueCode = #SALMONELLA 
* #ATHENS "Athens"
* #ATHENS ^property[0].code = #parent 
* #ATHENS ^property[0].valueCode = #SALMONELLA 
* #ATHINAI "Athinai"
* #ATHINAI ^property[0].code = #parent 
* #ATHINAI ^property[0].valueCode = #SALMONELLA 
* #ATI "Ati"
* #ATI ^property[0].code = #parent 
* #ATI ^property[0].valueCode = #SALMONELLA 
* #AUGUSTENBORG "Augustenborg"
* #AUGUSTENBORG ^property[0].code = #parent 
* #AUGUSTENBORG ^property[0].valueCode = #SALMONELLA 
* #AURELIANIS "Aurelianis"
* #AURELIANIS ^property[0].code = #parent 
* #AURELIANIS ^property[0].valueCode = #SALMONELLA 
* #AUSTIN "Austin"
* #AUSTIN ^property[0].code = #parent 
* #AUSTIN ^property[0].valueCode = #SALMONELLA 
* #AUSTRALIA "Australia"
* #AUSTRALIA ^property[0].code = #parent 
* #AUSTRALIA ^property[0].valueCode = #SALMONELLA 
* #AVIGNON "Avignon"
* #AVIGNON ^property[0].code = #parent 
* #AVIGNON ^property[0].valueCode = #SALMONELLA 
* #AVONMOUTH "Avonmouth"
* #AVONMOUTH ^property[0].code = #parent 
* #AVONMOUTH ^property[0].valueCode = #SALMONELLA 
* #AXIM "Axim"
* #AXIM ^property[0].code = #parent 
* #AXIM ^property[0].valueCode = #SALMONELLA 
* #AYINDE "Ayinde"
* #AYINDE ^property[0].code = #parent 
* #AYINDE ^property[0].valueCode = #SALMONELLA 
* #AYTON "Ayton"
* #AYTON ^property[0].code = #parent 
* #AYTON ^property[0].valueCode = #SALMONELLA 
* #AZTECA "Azteca"
* #AZTECA ^property[0].code = #parent 
* #AZTECA ^property[0].valueCode = #SALMONELLA 
* #BABELSBERG "Babelsberg"
* #BABELSBERG ^property[0].code = #parent 
* #BABELSBERG ^property[0].valueCode = #SALMONELLA 
* #BABILI "Babili"
* #BABILI ^property[0].code = #parent 
* #BABILI ^property[0].valueCode = #SALMONELLA 
* #BADAGRY "Badagry"
* #BADAGRY ^property[0].code = #parent 
* #BADAGRY ^property[0].valueCode = #SALMONELLA 
* #BAGUIDA "Baguida"
* #BAGUIDA ^property[0].code = #parent 
* #BAGUIDA ^property[0].valueCode = #SALMONELLA 
* #BAGUIRMI "Baguirmi"
* #BAGUIRMI ^property[0].code = #parent 
* #BAGUIRMI ^property[0].valueCode = #SALMONELLA 
* #BAHATI "Bahati"
* #BAHATI ^property[0].code = #parent 
* #BAHATI ^property[0].valueCode = #SALMONELLA 
* #BAHRENFELD "Bahrenfeld"
* #BAHRENFELD ^property[0].code = #parent 
* #BAHRENFELD ^property[0].valueCode = #SALMONELLA 
* #BAIBOUKOUM "Baiboukoum"
* #BAIBOUKOUM ^property[0].code = #parent 
* #BAIBOUKOUM ^property[0].valueCode = #SALMONELLA 
* #BAILDON "Baildon"
* #BAILDON ^property[0].code = #parent 
* #BAILDON ^property[0].valueCode = #SALMONELLA 
* #BAKAU "Bakau"
* #BAKAU ^property[0].code = #parent 
* #BAKAU ^property[0].valueCode = #SALMONELLA 
* #BALCONES "Balcones"
* #BALCONES ^property[0].code = #parent 
* #BALCONES ^property[0].valueCode = #SALMONELLA 
* #BALL "Ball"
* #BALL ^property[0].code = #parent 
* #BALL ^property[0].valueCode = #SALMONELLA 
* #BAMA "Bama"
* #BAMA ^property[0].code = #parent 
* #BAMA ^property[0].valueCode = #SALMONELLA 
* #BAMBOYE "Bamboye"
* #BAMBOYE ^property[0].code = #parent 
* #BAMBOYE ^property[0].valueCode = #SALMONELLA 
* #BAMBYLOR "Bambylor"
* #BAMBYLOR ^property[0].code = #parent 
* #BAMBYLOR ^property[0].valueCode = #SALMONELLA 
* #BANALIA "Banalia"
* #BANALIA ^property[0].code = #parent 
* #BANALIA ^property[0].valueCode = #SALMONELLA 
* #BANANA "Banana"
* #BANANA ^property[0].code = #parent 
* #BANANA ^property[0].valueCode = #SALMONELLA 
* #BANCO "Banco"
* #BANCO ^property[0].code = #parent 
* #BANCO ^property[0].valueCode = #SALMONELLA 
* #BANDIA "Bandia"
* #BANDIA ^property[0].code = #parent 
* #BANDIA ^property[0].valueCode = #SALMONELLA 
* #BANDIM "Bandim"
* #BANDIM ^property[0].code = #parent 
* #BANDIM ^property[0].valueCode = #SALMONELLA 
* #BANGKOK "Bangkok"
* #BANGKOK ^property[0].code = #parent 
* #BANGKOK ^property[0].valueCode = #SALMONELLA 
* #BANGUI "Bangui"
* #BANGUI ^property[0].code = #parent 
* #BANGUI ^property[0].valueCode = #SALMONELLA 
* #BANJUL "Banjul"
* #BANJUL ^property[0].code = #parent 
* #BANJUL ^property[0].valueCode = #SALMONELLA 
* #BARDO "Bardo"
* #BARDO ^property[0].code = #parent 
* #BARDO ^property[0].valueCode = #SALMONELLA 
* #BAREILLY "Bareilly"
* #BAREILLY ^property[0].code = #parent 
* #BAREILLY ^property[0].valueCode = #SALMONELLA 
* #BARGNY "Bargny"
* #BARGNY ^property[0].code = #parent 
* #BARGNY ^property[0].valueCode = #SALMONELLA 
* #BARMBEK "Barmbek"
* #BARMBEK ^property[0].code = #parent 
* #BARMBEK ^property[0].valueCode = #SALMONELLA 
* #BARRANQUILLA "Barranquilla"
* #BARRANQUILLA ^property[0].code = #parent 
* #BARRANQUILLA ^property[0].valueCode = #SALMONELLA 
* #BARRY "Barry"
* #BARRY ^property[0].code = #parent 
* #BARRY ^property[0].valueCode = #SALMONELLA 
* #BASINGSTOKE "Basingstoke"
* #BASINGSTOKE ^property[0].code = #parent 
* #BASINGSTOKE ^property[0].valueCode = #SALMONELLA 
* #BASSA "Bassa"
* #BASSA ^property[0].code = #parent 
* #BASSA ^property[0].valueCode = #SALMONELLA 
* #BASSADJI "Bassadji"
* #BASSADJI ^property[0].code = #parent 
* #BASSADJI ^property[0].valueCode = #SALMONELLA 
* #BATA "Bata"
* #BATA ^property[0].code = #parent 
* #BATA ^property[0].valueCode = #SALMONELLA 
* #BATONROUGE "Batonrouge"
* #BATONROUGE ^property[0].code = #parent 
* #BATONROUGE ^property[0].valueCode = #SALMONELLA 
* #BATTLE "Battle"
* #BATTLE ^property[0].code = #parent 
* #BATTLE ^property[0].valueCode = #SALMONELLA 
* #BAZENHEID "Bazenheid"
* #BAZENHEID ^property[0].code = #parent 
* #BAZENHEID ^property[0].valueCode = #SALMONELLA 
* #BE "Be"
* #BE ^property[0].code = #parent 
* #BE ^property[0].valueCode = #SALMONELLA 
* #BEAUDESERT "Beaudesert"
* #BEAUDESERT ^property[0].code = #parent 
* #BEAUDESERT ^property[0].valueCode = #SALMONELLA 
* #BEDFORD "Bedford"
* #BEDFORD ^property[0].code = #parent 
* #BEDFORD ^property[0].valueCode = #SALMONELLA 
* #BELEM "Belem"
* #BELEM ^property[0].code = #parent 
* #BELEM ^property[0].valueCode = #SALMONELLA 
* #BELFAST "Belfast"
* #BELFAST ^property[0].code = #parent 
* #BELFAST ^property[0].valueCode = #SALMONELLA 
* #BELLEVUE "Bellevue"
* #BELLEVUE ^property[0].code = #parent 
* #BELLEVUE ^property[0].valueCode = #SALMONELLA 
* #BENFICA "Benfica"
* #BENFICA ^property[0].code = #parent 
* #BENFICA ^property[0].valueCode = #SALMONELLA 
* #BENGUELLA "Benguella"
* #BENGUELLA ^property[0].code = #parent 
* #BENGUELLA ^property[0].valueCode = #SALMONELLA 
* #BENIN "Benin"
* #BENIN ^property[0].code = #parent 
* #BENIN ^property[0].valueCode = #SALMONELLA 
* #BENUE "Benue"
* #BENUE ^property[0].code = #parent 
* #BENUE ^property[0].valueCode = #SALMONELLA 
* #BERE "Bere"
* #BERE ^property[0].code = #parent 
* #BERE ^property[0].valueCode = #SALMONELLA 
* #BERGEDORF "Bergedorf"
* #BERGEDORF ^property[0].code = #parent 
* #BERGEDORF ^property[0].valueCode = #SALMONELLA 
* #BERGEN "Bergen"
* #BERGEN ^property[0].code = #parent 
* #BERGEN ^property[0].valueCode = #SALMONELLA 
* #BERGUES "Bergues"
* #BERGUES ^property[0].code = #parent 
* #BERGUES ^property[0].valueCode = #SALMONELLA 
* #BERKELEY "Berkeley"
* #BERKELEY ^property[0].code = #parent 
* #BERKELEY ^property[0].valueCode = #SALMONELLA 
* #BERLIN "Berlin"
* #BERLIN ^property[0].code = #parent 
* #BERLIN ^property[0].valueCode = #SALMONELLA 
* #BERTA "Berta"
* #BERTA ^property[0].code = #parent 
* #BERTA ^property[0].valueCode = #SALMONELLA 
* #BESSI "Bessi"
* #BESSI ^property[0].code = #parent 
* #BESSI ^property[0].valueCode = #SALMONELLA 
* #BETHUNE "Bethune"
* #BETHUNE ^property[0].code = #parent 
* #BETHUNE ^property[0].valueCode = #SALMONELLA 
* #BIAFRA "Biafra"
* #BIAFRA ^property[0].code = #parent 
* #BIAFRA ^property[0].valueCode = #SALMONELLA 
* #BIDA "Bida"
* #BIDA ^property[0].code = #parent 
* #BIDA ^property[0].valueCode = #SALMONELLA 
* #BIETRI "Bietri"
* #BIETRI ^property[0].code = #parent 
* #BIETRI ^property[0].valueCode = #SALMONELLA 
* #BIGNONA "Bignona"
* #BIGNONA ^property[0].code = #parent 
* #BIGNONA ^property[0].valueCode = #SALMONELLA 
* #BIJLMER "Bijlmer"
* #BIJLMER ^property[0].code = #parent 
* #BIJLMER ^property[0].valueCode = #SALMONELLA 
* #BILU "Bilu"
* #BILU ^property[0].code = #parent 
* #BILU ^property[0].valueCode = #SALMONELLA 
* #BINCHE "Binche"
* #BINCHE ^property[0].code = #parent 
* #BINCHE ^property[0].valueCode = #SALMONELLA 
* #BINGERVILLE "Bingerville"
* #BINGERVILLE ^property[0].code = #parent 
* #BINGERVILLE ^property[0].valueCode = #SALMONELLA 
* #BINNINGEN "Binningen"
* #BINNINGEN ^property[0].code = #parent 
* #BINNINGEN ^property[0].valueCode = #SALMONELLA 
* #BIRKENHEAD "Birkenhead"
* #BIRKENHEAD ^property[0].code = #parent 
* #BIRKENHEAD ^property[0].valueCode = #SALMONELLA 
* #BIRMINGHAM "Birmingham"
* #BIRMINGHAM ^property[0].code = #parent 
* #BIRMINGHAM ^property[0].valueCode = #SALMONELLA 
* #BISPEBJERG "Bispebjerg"
* #BISPEBJERG ^property[0].code = #parent 
* #BISPEBJERG ^property[0].valueCode = #SALMONELLA 
* #BISSAU "Bissau"
* #BISSAU ^property[0].code = #parent 
* #BISSAU ^property[0].valueCode = #SALMONELLA 
* #BLANCMESNIL "Blancmesnil"
* #BLANCMESNIL ^property[0].code = #parent 
* #BLANCMESNIL ^property[0].valueCode = #SALMONELLA 
* #BLEGDAM "Blegdam"
* #BLEGDAM ^property[0].code = #parent 
* #BLEGDAM ^property[0].valueCode = #SALMONELLA 
* #BLIJDORP "Blijdorp"
* #BLIJDORP ^property[0].code = #parent 
* #BLIJDORP ^property[0].valueCode = #SALMONELLA 
* #BLITTA "Blitta"
* #BLITTA ^property[0].code = #parent 
* #BLITTA ^property[0].valueCode = #SALMONELLA 
* #BLOCKLEY "Blockley"
* #BLOCKLEY ^property[0].code = #parent 
* #BLOCKLEY ^property[0].valueCode = #SALMONELLA 
* #BLOOMSBURY "Bloomsbury"
* #BLOOMSBURY ^property[0].code = #parent 
* #BLOOMSBURY ^property[0].valueCode = #SALMONELLA 
* #BLUKWA "Blukwa"
* #BLUKWA ^property[0].code = #parent 
* #BLUKWA ^property[0].valueCode = #SALMONELLA 
* #BOBO "Bobo"
* #BOBO ^property[0].code = #parent 
* #BOBO ^property[0].valueCode = #SALMONELLA 
* #BOCHUM "Bochum"
* #BOCHUM ^property[0].code = #parent 
* #BOCHUM ^property[0].valueCode = #SALMONELLA 
* #BODJONEGORO "Bodjonegoro"
* #BODJONEGORO ^property[0].code = #parent 
* #BODJONEGORO ^property[0].valueCode = #SALMONELLA 
* #BOECKER "Boecker"
* #BOECKER ^property[0].code = #parent 
* #BOECKER ^property[0].valueCode = #SALMONELLA 
* #BOFFLENS "Bofflens"
* #BOFFLENS ^property[0].code = #parent 
* #BOFFLENS ^property[0].valueCode = #SALMONELLA 
* #BOKANJAC "Bokanjac"
* #BOKANJAC ^property[0].code = #parent 
* #BOKANJAC ^property[0].valueCode = #SALMONELLA 
* #BOLAMA "Bolama"
* #BOLAMA ^property[0].code = #parent 
* #BOLAMA ^property[0].valueCode = #SALMONELLA 
* #BOLOMBO "Bolombo"
* #BOLOMBO ^property[0].code = #parent 
* #BOLOMBO ^property[0].valueCode = #SALMONELLA 
* #BOLTON "Bolton"
* #BOLTON ^property[0].code = #parent 
* #BOLTON ^property[0].valueCode = #SALMONELLA 
* #BONAMES "Bonames"
* #BONAMES ^property[0].code = #parent 
* #BONAMES ^property[0].valueCode = #SALMONELLA 
* #BONARIENSIS "Bonariensis"
* #BONARIENSIS ^property[0].code = #parent 
* #BONARIENSIS ^property[0].valueCode = #SALMONELLA 
* #BONN "Bonn"
* #BONN ^property[0].code = #parent 
* #BONN ^property[0].valueCode = #SALMONELLA 
* #BOOTLE "Bootle"
* #BOOTLE ^property[0].code = #parent 
* #BOOTLE ^property[0].valueCode = #SALMONELLA 
* #BORBECK "Borbeck"
* #BORBECK ^property[0].code = #parent 
* #BORBECK ^property[0].valueCode = #SALMONELLA 
* #BORDEAUX "Bordeaux"
* #BORDEAUX ^property[0].code = #parent 
* #BORDEAUX ^property[0].valueCode = #SALMONELLA 
* #BORREZE "Borreze"
* #BORREZE ^property[0].code = #parent 
* #BORREZE ^property[0].valueCode = #SALMONELLA 
* #BORROMEA "Borromea"
* #BORROMEA ^property[0].code = #parent 
* #BORROMEA ^property[0].valueCode = #SALMONELLA 
* #BOUAKE "Bouake"
* #BOUAKE ^property[0].code = #parent 
* #BOUAKE ^property[0].valueCode = #SALMONELLA 
* #BOURNEMOUTH "Bournemouth"
* #BOURNEMOUTH ^property[0].code = #parent 
* #BOURNEMOUTH ^property[0].valueCode = #SALMONELLA 
* #BOUSSO "Bousso"
* #BOUSSO ^property[0].code = #parent 
* #BOUSSO ^property[0].valueCode = #SALMONELLA 
* #BOVISMORBIFICANS "Bovismorbificans"
* #BOVISMORBIFICANS ^property[0].code = #parent 
* #BOVISMORBIFICANS ^property[0].valueCode = #SALMONELLA 
* #BRACKENRIDGE "Brackenridge"
* #BRACKENRIDGE ^property[0].code = #parent 
* #BRACKENRIDGE ^property[0].valueCode = #SALMONELLA 
* #BRACKNELL "Bracknell"
* #BRACKNELL ^property[0].code = #parent 
* #BRACKNELL ^property[0].valueCode = #SALMONELLA 
* #BRADFORD "Bradford"
* #BRADFORD ^property[0].code = #parent 
* #BRADFORD ^property[0].valueCode = #SALMONELLA 
* #BRAENDERUP "Braenderup"
* #BRAENDERUP ^property[0].code = #parent 
* #BRAENDERUP ^property[0].valueCode = #SALMONELLA 
* #BRANCASTER "Brancaster"
* #BRANCASTER ^property[0].code = #parent 
* #BRANCASTER ^property[0].valueCode = #SALMONELLA 
* #BRANDENBURG "Brandenburg"
* #BRANDENBURG ^property[0].code = #parent 
* #BRANDENBURG ^property[0].valueCode = #SALMONELLA 
* #BRAZIL "Brazil"
* #BRAZIL ^property[0].code = #parent 
* #BRAZIL ^property[0].valueCode = #SALMONELLA 
* #BRAZOS "Brazos"
* #BRAZOS ^property[0].code = #parent 
* #BRAZOS ^property[0].valueCode = #SALMONELLA 
* #BRAZZAVILLE "Brazzaville"
* #BRAZZAVILLE ^property[0].code = #parent 
* #BRAZZAVILLE ^property[0].valueCode = #SALMONELLA 
* #BREDA "Breda"
* #BREDA ^property[0].code = #parent 
* #BREDA ^property[0].valueCode = #SALMONELLA 
* #BREDENEY "Bredeney"
* #BREDENEY ^property[0].code = #parent 
* #BREDENEY ^property[0].valueCode = #SALMONELLA 
* #BREFET "Brefet"
* #BREFET ^property[0].code = #parent 
* #BREFET ^property[0].valueCode = #SALMONELLA 
* #BREUKELEN "Breukelen"
* #BREUKELEN ^property[0].code = #parent 
* #BREUKELEN ^property[0].valueCode = #SALMONELLA 
* #BREVIK "Brevik"
* #BREVIK ^property[0].code = #parent 
* #BREVIK ^property[0].valueCode = #SALMONELLA 
* #BREZANY "Brezany"
* #BREZANY ^property[0].code = #parent 
* #BREZANY ^property[0].valueCode = #SALMONELLA 
* #BRIJBHUMI "Brijbhumi"
* #BRIJBHUMI ^property[0].code = #parent 
* #BRIJBHUMI ^property[0].valueCode = #SALMONELLA 
* #BRIKAMA "Brikama"
* #BRIKAMA ^property[0].code = #parent 
* #BRIKAMA ^property[0].valueCode = #SALMONELLA 
* #BRINDISI "Brindisi"
* #BRINDISI ^property[0].code = #parent 
* #BRINDISI ^property[0].valueCode = #SALMONELLA 
* #BRISBANE "Brisbane"
* #BRISBANE ^property[0].code = #parent 
* #BRISBANE ^property[0].valueCode = #SALMONELLA 
* #BRISTOL "Bristol"
* #BRISTOL ^property[0].code = #parent 
* #BRISTOL ^property[0].valueCode = #SALMONELLA 
* #BRIVE "Brive"
* #BRIVE ^property[0].code = #parent 
* #BRIVE ^property[0].valueCode = #SALMONELLA 
* #BROC "Broc"
* #BROC ^property[0].code = #parent 
* #BROC ^property[0].valueCode = #SALMONELLA 
* #BRON "Bron"
* #BRON ^property[0].code = #parent 
* #BRON ^property[0].valueCode = #SALMONELLA 
* #BRONX "Bronx"
* #BRONX ^property[0].code = #parent 
* #BRONX ^property[0].valueCode = #SALMONELLA 
* #BROOKLYN "Brooklyn"
* #BROOKLYN ^property[0].code = #parent 
* #BROOKLYN ^property[0].valueCode = #SALMONELLA 
* #BROUGHTON "Broughton"
* #BROUGHTON ^property[0].code = #parent 
* #BROUGHTON ^property[0].valueCode = #SALMONELLA 
* #BRUCK "Bruck"
* #BRUCK ^property[0].code = #parent 
* #BRUCK ^property[0].valueCode = #SALMONELLA 
* #BRUEBACH "Bruebach"
* #BRUEBACH ^property[0].code = #parent 
* #BRUEBACH ^property[0].valueCode = #SALMONELLA 
* #BRUNEI "Brunei"
* #BRUNEI ^property[0].code = #parent 
* #BRUNEI ^property[0].valueCode = #SALMONELLA 
* #BRUNFLO "Brunflo"
* #BRUNFLO ^property[0].code = #parent 
* #BRUNFLO ^property[0].valueCode = #SALMONELLA 
* #BSILLA "Bsilla"
* #BSILLA ^property[0].code = #parent 
* #BSILLA ^property[0].valueCode = #SALMONELLA 
* #BUCKEYE "Buckeye"
* #BUCKEYE ^property[0].code = #parent 
* #BUCKEYE ^property[0].valueCode = #SALMONELLA 
* #BUDAPEST "Budapest"
* #BUDAPEST ^property[0].code = #parent 
* #BUDAPEST ^property[0].valueCode = #SALMONELLA 
* #BUKAVU "Bukavu"
* #BUKAVU ^property[0].code = #parent 
* #BUKAVU ^property[0].valueCode = #SALMONELLA 
* #BUKURU "Bukuru"
* #BUKURU ^property[0].code = #parent 
* #BUKURU ^property[0].valueCode = #SALMONELLA 
* #BULGARIA "Bulgaria"
* #BULGARIA ^property[0].code = #parent 
* #BULGARIA ^property[0].valueCode = #SALMONELLA 
* #BULLBAY "Bullbay"
* #BULLBAY ^property[0].code = #parent 
* #BULLBAY ^property[0].valueCode = #SALMONELLA 
* #BULOVKA "Bulovka"
* #BULOVKA ^property[0].code = #parent 
* #BULOVKA ^property[0].valueCode = #SALMONELLA 
* #BURGAS "Burgas"
* #BURGAS ^property[0].code = #parent 
* #BURGAS ^property[0].valueCode = #SALMONELLA 
* #BURUNDI "Burundi"
* #BURUNDI ^property[0].code = #parent 
* #BURUNDI ^property[0].valueCode = #SALMONELLA 
* #BURY "Bury"
* #BURY ^property[0].code = #parent 
* #BURY ^property[0].valueCode = #SALMONELLA 
* #BUSINGA "Businga"
* #BUSINGA ^property[0].code = #parent 
* #BUSINGA ^property[0].valueCode = #SALMONELLA 
* #BUTANTAN "Butantan"
* #BUTANTAN ^property[0].code = #parent 
* #BUTANTAN ^property[0].valueCode = #SALMONELLA 
* #BUTANTAN_VAR_15+ "Butantan var. 15+"
* #BUTANTAN_VAR_15+ ^property[0].code = #parent 
* #BUTANTAN_VAR_15+ ^property[0].valueCode = #SALMONELLA 
* #BUTARE "Butare"
* #BUTARE ^property[0].code = #parent 
* #BUTARE ^property[0].valueCode = #SALMONELLA 
* #BUZU "Buzu"
* #BUZU ^property[0].code = #parent 
* #BUZU ^property[0].valueCode = #SALMONELLA 
* #CAEN "Caen"
* #CAEN ^property[0].code = #parent 
* #CAEN ^property[0].valueCode = #SALMONELLA 
* #CAIRINA "Cairina"
* #CAIRINA ^property[0].code = #parent 
* #CAIRINA ^property[0].valueCode = #SALMONELLA 
* #CAIRNS "Cairns"
* #CAIRNS ^property[0].code = #parent 
* #CAIRNS ^property[0].valueCode = #SALMONELLA 
* #CALABAR "Calabar"
* #CALABAR ^property[0].code = #parent 
* #CALABAR ^property[0].valueCode = #SALMONELLA 
* #CALIFORNIA "California"
* #CALIFORNIA ^property[0].code = #parent 
* #CALIFORNIA ^property[0].valueCode = #SALMONELLA 
* #CAMBERENE "Camberene"
* #CAMBERENE ^property[0].code = #parent 
* #CAMBERENE ^property[0].valueCode = #SALMONELLA 
* #CAMBERWELL "Camberwell"
* #CAMBERWELL ^property[0].code = #parent 
* #CAMBERWELL ^property[0].valueCode = #SALMONELLA 
* #CAMPINENSE "Campinense"
* #CAMPINENSE ^property[0].code = #parent 
* #CAMPINENSE ^property[0].valueCode = #SALMONELLA 
* #CANADA "Canada"
* #CANADA ^property[0].code = #parent 
* #CANADA ^property[0].valueCode = #SALMONELLA 
* #CANARY "Canary"
* #CANARY ^property[0].code = #parent 
* #CANARY ^property[0].valueCode = #SALMONELLA 
* #CANNOBIO "Cannobio"
* #CANNOBIO ^property[0].code = #parent 
* #CANNOBIO ^property[0].valueCode = #SALMONELLA 
* #CANNONHILL "Cannonhill"
* #CANNONHILL ^property[0].code = #parent 
* #CANNONHILL ^property[0].valueCode = #SALMONELLA 
* #CANNSTATT "Cannstatt"
* #CANNSTATT ^property[0].code = #parent 
* #CANNSTATT ^property[0].valueCode = #SALMONELLA 
* #CANTON "Canton"
* #CANTON ^property[0].code = #parent 
* #CANTON ^property[0].valueCode = #SALMONELLA 
* #CARACAS "Caracas"
* #CARACAS ^property[0].code = #parent 
* #CARACAS ^property[0].valueCode = #SALMONELLA 
* #CARDONER "Cardoner"
* #CARDONER ^property[0].code = #parent 
* #CARDONER ^property[0].valueCode = #SALMONELLA 
* #CARMEL "Carmel"
* #CARMEL ^property[0].code = #parent 
* #CARMEL ^property[0].valueCode = #SALMONELLA 
* #CARNAC "Carnac"
* #CARNAC ^property[0].code = #parent 
* #CARNAC ^property[0].valueCode = #SALMONELLA 
* #CARNO "Carno"
* #CARNO ^property[0].code = #parent 
* #CARNO ^property[0].valueCode = #SALMONELLA 
* #CARPENTRAS "Carpentras"
* #CARPENTRAS ^property[0].code = #parent 
* #CARPENTRAS ^property[0].valueCode = #SALMONELLA 
* #CARRAU "Carrau"
* #CARRAU ^property[0].code = #parent 
* #CARRAU ^property[0].valueCode = #SALMONELLA 
* #CARSWELL "Carswell"
* #CARSWELL ^property[0].code = #parent 
* #CARSWELL ^property[0].valueCode = #SALMONELLA 
* #CASABLANCA "Casablanca"
* #CASABLANCA ^property[0].code = #parent 
* #CASABLANCA ^property[0].valueCode = #SALMONELLA 
* #CASAMANCE "Casamance"
* #CASAMANCE ^property[0].code = #parent 
* #CASAMANCE ^property[0].valueCode = #SALMONELLA 
* #CATALUNIA "Catalunia"
* #CATALUNIA ^property[0].code = #parent 
* #CATALUNIA ^property[0].valueCode = #SALMONELLA 
* #CATANZARO "Catanzaro"
* #CATANZARO ^property[0].code = #parent 
* #CATANZARO ^property[0].valueCode = #SALMONELLA 
* #CATUMAGOS "Catumagos"
* #CATUMAGOS ^property[0].code = #parent 
* #CATUMAGOS ^property[0].valueCode = #SALMONELLA 
* #CAYAR "Cayar"
* #CAYAR ^property[0].code = #parent 
* #CAYAR ^property[0].valueCode = #SALMONELLA 
* #CERRO "Cerro"
* #CERRO ^property[0].code = #parent 
* #CERRO ^property[0].valueCode = #SALMONELLA 
* #CERRO_VAR_14+ "Cerro var. 14+"
* #CERRO_VAR_14+ ^property[0].code = #parent 
* #CERRO_VAR_14+ ^property[0].valueCode = #SALMONELLA 
* #CEYCO "Ceyco"
* #CEYCO ^property[0].code = #parent 
* #CEYCO ^property[0].valueCode = #SALMONELLA 
* #CHAGOUA "Chagoua"
* #CHAGOUA ^property[0].code = #parent 
* #CHAGOUA ^property[0].valueCode = #SALMONELLA 
* #CHAILEY "Chailey"
* #CHAILEY ^property[0].code = #parent 
* #CHAILEY ^property[0].valueCode = #SALMONELLA 
* #CHAMPAIGN "Champaign"
* #CHAMPAIGN ^property[0].code = #parent 
* #CHAMPAIGN ^property[0].valueCode = #SALMONELLA 
* #CHANDANS "Chandans"
* #CHANDANS ^property[0].code = #parent 
* #CHANDANS ^property[0].valueCode = #SALMONELLA 
* #CHARITY "Charity"
* #CHARITY ^property[0].code = #parent 
* #CHARITY ^property[0].valueCode = #SALMONELLA 
* #CHARLOTTENBURG "Charlottenburg"
* #CHARLOTTENBURG ^property[0].code = #parent 
* #CHARLOTTENBURG ^property[0].valueCode = #SALMONELLA 
* #CHARTRES "Chartres"
* #CHARTRES ^property[0].code = #parent 
* #CHARTRES ^property[0].valueCode = #SALMONELLA 
* #CHELTENHAM "Cheltenham"
* #CHELTENHAM ^property[0].code = #parent 
* #CHELTENHAM ^property[0].valueCode = #SALMONELLA 
* #CHENNAI "Chennai"
* #CHENNAI ^property[0].code = #parent 
* #CHENNAI ^property[0].valueCode = #SALMONELLA 
* #CHESTER "Chester"
* #CHESTER ^property[0].code = #parent 
* #CHESTER ^property[0].valueCode = #SALMONELLA 
* #CHICAGO "Chicago"
* #CHICAGO ^property[0].code = #parent 
* #CHICAGO ^property[0].valueCode = #SALMONELLA 
* #CHICHESTER "Chichester"
* #CHICHESTER ^property[0].code = #parent 
* #CHICHESTER ^property[0].valueCode = #SALMONELLA 
* #CHICHIRI "Chichiri"
* #CHICHIRI ^property[0].code = #parent 
* #CHICHIRI ^property[0].valueCode = #SALMONELLA 
* #CHILE "Chile"
* #CHILE ^property[0].code = #parent 
* #CHILE ^property[0].valueCode = #SALMONELLA 
* #CHINCOL "Chincol"
* #CHINCOL ^property[0].code = #parent 
* #CHINCOL ^property[0].valueCode = #SALMONELLA 
* #CHINGOLA "Chingola"
* #CHINGOLA ^property[0].code = #parent 
* #CHINGOLA ^property[0].valueCode = #SALMONELLA 
* #CHIREDZI "Chiredzi"
* #CHIREDZI ^property[0].code = #parent 
* #CHIREDZI ^property[0].valueCode = #SALMONELLA 
* #CHITTAGONG "Chittagong"
* #CHITTAGONG ^property[0].code = #parent 
* #CHITTAGONG ^property[0].valueCode = #SALMONELLA 
* #CHOLERAESUIS "Choleraesuis"
* #CHOLERAESUIS ^property[0].code = #parent 
* #CHOLERAESUIS ^property[0].valueCode = #SALMONELLA 
* #CHOLERAESUIS_VAR_DECATUR "Choleraesuis var. Decatur"
* #CHOLERAESUIS_VAR_DECATUR ^property[0].code = #parent 
* #CHOLERAESUIS_VAR_DECATUR ^property[0].valueCode = #SALMONELLA 
* #CHOLERAESUIS_VAR_KUNZENDORF "Choleraesuis var. Kunzendorf"
* #CHOLERAESUIS_VAR_KUNZENDORF ^property[0].code = #parent 
* #CHOLERAESUIS_VAR_KUNZENDORF ^property[0].valueCode = #SALMONELLA 
* #CHOMEDEY "Chomedey"
* #CHOMEDEY ^property[0].code = #parent 
* #CHOMEDEY ^property[0].valueCode = #SALMONELLA 
* #CHRISTIANSBORG "Christiansborg"
* #CHRISTIANSBORG ^property[0].code = #parent 
* #CHRISTIANSBORG ^property[0].valueCode = #SALMONELLA 
* #CLACKAMAS "Clackamas"
* #CLACKAMAS ^property[0].code = #parent 
* #CLACKAMAS ^property[0].valueCode = #SALMONELLA 
* #CLAIBORNEI "Claibornei"
* #CLAIBORNEI ^property[0].code = #parent 
* #CLAIBORNEI ^property[0].valueCode = #SALMONELLA 
* #CLANVILLIAN "Clanvillian"
* #CLANVILLIAN ^property[0].code = #parent 
* #CLANVILLIAN ^property[0].valueCode = #SALMONELLA 
* #CLERKENWELL "Clerkenwell"
* #CLERKENWELL ^property[0].code = #parent 
* #CLERKENWELL ^property[0].valueCode = #SALMONELLA 
* #CLEVELAND "Cleveland"
* #CLEVELAND ^property[0].code = #parent 
* #CLEVELAND ^property[0].valueCode = #SALMONELLA 
* #CLONTARF "Clontarf"
* #CLONTARF ^property[0].code = #parent 
* #CLONTARF ^property[0].valueCode = #SALMONELLA 
* #COCHIN "Cochin"
* #COCHIN ^property[0].code = #parent 
* #COCHIN ^property[0].valueCode = #SALMONELLA 
* #COCHISE "Cochise"
* #COCHISE ^property[0].code = #parent 
* #COCHISE ^property[0].valueCode = #SALMONELLA 
* #COCODY "Cocody"
* #COCODY ^property[0].code = #parent 
* #COCODY ^property[0].valueCode = #SALMONELLA 
* #COELN "Coeln"
* #COELN ^property[0].code = #parent 
* #COELN ^property[0].valueCode = #SALMONELLA 
* #COLEYPARK "Coleypark"
* #COLEYPARK ^property[0].code = #parent 
* #COLEYPARK ^property[0].valueCode = #SALMONELLA 
* #COLINDALE "Colindale"
* #COLINDALE ^property[0].code = #parent 
* #COLINDALE ^property[0].valueCode = #SALMONELLA 
* #COLOBANE "Colobane"
* #COLOBANE ^property[0].code = #parent 
* #COLOBANE ^property[0].valueCode = #SALMONELLA 
* #COLOMBO "Colombo"
* #COLOMBO ^property[0].code = #parent 
* #COLOMBO ^property[0].valueCode = #SALMONELLA 
* #COLORADO "Colorado"
* #COLORADO ^property[0].code = #parent 
* #COLORADO ^property[0].valueCode = #SALMONELLA 
* #CONCORD "Concord"
* #CONCORD ^property[0].code = #parent 
* #CONCORD ^property[0].valueCode = #SALMONELLA 
* #CONNECTICUT "Connecticut"
* #CONNECTICUT ^property[0].code = #parent 
* #CONNECTICUT ^property[0].valueCode = #SALMONELLA 
* #COOGEE "Coogee"
* #COOGEE ^property[0].code = #parent 
* #COOGEE ^property[0].valueCode = #SALMONELLA 
* #COQUILHATVILLE "Coquilhatville"
* #COQUILHATVILLE ^property[0].code = #parent 
* #COQUILHATVILLE ^property[0].valueCode = #SALMONELLA 
* #COROMANDEL "Coromandel"
* #COROMANDEL ^property[0].code = #parent 
* #COROMANDEL ^property[0].valueCode = #SALMONELLA 
* #CORVALLIS "Corvallis"
* #CORVALLIS ^property[0].code = #parent 
* #CORVALLIS ^property[0].valueCode = #SALMONELLA 
* #COTHAM "Cotham"
* #COTHAM ^property[0].code = #parent 
* #COTHAM ^property[0].valueCode = #SALMONELLA 
* #COTIA "Cotia"
* #COTIA ^property[0].code = #parent 
* #COTIA ^property[0].valueCode = #SALMONELLA 
* #COTONOU "Cotonou"
* #COTONOU ^property[0].code = #parent 
* #COTONOU ^property[0].valueCode = #SALMONELLA 
* #CREMIEU "Cremieu"
* #CREMIEU ^property[0].code = #parent 
* #CREMIEU ^property[0].valueCode = #SALMONELLA 
* #CREWE "Crewe"
* #CREWE ^property[0].code = #parent 
* #CREWE ^property[0].valueCode = #SALMONELLA 
* #CROFT "Croft"
* #CROFT ^property[0].code = #parent 
* #CROFT ^property[0].valueCode = #SALMONELLA 
* #CROSSNESS "Crossness"
* #CROSSNESS ^property[0].code = #parent 
* #CROSSNESS ^property[0].valueCode = #SALMONELLA 
* #CUBANA "Cubana"
* #CUBANA ^property[0].code = #parent 
* #CUBANA ^property[0].valueCode = #SALMONELLA 
* #CUCKMERE "Cuckmere"
* #CUCKMERE ^property[0].code = #parent 
* #CUCKMERE ^property[0].valueCode = #SALMONELLA 
* #CULLINGWORTH "Cullingworth"
* #CULLINGWORTH ^property[0].code = #parent 
* #CULLINGWORTH ^property[0].valueCode = #SALMONELLA 
* #CUMBERLAND "Cumberland"
* #CUMBERLAND ^property[0].code = #parent 
* #CUMBERLAND ^property[0].valueCode = #SALMONELLA 
* #CURACAO "Curacao"
* #CURACAO ^property[0].code = #parent 
* #CURACAO ^property[0].valueCode = #SALMONELLA 
* #CYPRUS "Cyprus"
* #CYPRUS ^property[0].code = #parent 
* #CYPRUS ^property[0].valueCode = #SALMONELLA 
* #CZERNYRING "Czernyring"
* #CZERNYRING ^property[0].code = #parent 
* #CZERNYRING ^property[0].valueCode = #SALMONELLA 
* #DAARLE "Daarle"
* #DAARLE ^property[0].code = #parent 
* #DAARLE ^property[0].valueCode = #SALMONELLA 
* #DABOU "Dabou"
* #DABOU ^property[0].code = #parent 
* #DABOU ^property[0].valueCode = #SALMONELLA 
* #DADZIE "Dadzie"
* #DADZIE ^property[0].code = #parent 
* #DADZIE ^property[0].valueCode = #SALMONELLA 
* #DAHLEM "Dahlem"
* #DAHLEM ^property[0].code = #parent 
* #DAHLEM ^property[0].valueCode = #SALMONELLA 
* #DAHOMEY "Dahomey"
* #DAHOMEY ^property[0].code = #parent 
* #DAHOMEY ^property[0].valueCode = #SALMONELLA 
* #DAHRA "Dahra"
* #DAHRA ^property[0].code = #parent 
* #DAHRA ^property[0].valueCode = #SALMONELLA 
* #DAKAR "Dakar"
* #DAKAR ^property[0].code = #parent 
* #DAKAR ^property[0].valueCode = #SALMONELLA 
* #DAKOTA "Dakota"
* #DAKOTA ^property[0].code = #parent 
* #DAKOTA ^property[0].valueCode = #SALMONELLA 
* #DALLGOW "Dallgow"
* #DALLGOW ^property[0].code = #parent 
* #DALLGOW ^property[0].valueCode = #SALMONELLA 
* #DAMMAN "Damman"
* #DAMMAN ^property[0].code = #parent 
* #DAMMAN ^property[0].valueCode = #SALMONELLA 
* #DAN "Dan"
* #DAN ^property[0].code = #parent 
* #DAN ^property[0].valueCode = #SALMONELLA 
* #DAPANGO "Dapango"
* #DAPANGO ^property[0].code = #parent 
* #DAPANGO ^property[0].valueCode = #SALMONELLA 
* #DAULA "Daula"
* #DAULA ^property[0].code = #parent 
* #DAULA ^property[0].valueCode = #SALMONELLA 
* #DAYTONA "Daytona"
* #DAYTONA ^property[0].code = #parent 
* #DAYTONA ^property[0].valueCode = #SALMONELLA 
* #DECKSTEIN "Deckstein"
* #DECKSTEIN ^property[0].code = #parent 
* #DECKSTEIN ^property[0].valueCode = #SALMONELLA 
* #DELAN "Delan"
* #DELAN ^property[0].code = #parent 
* #DELAN ^property[0].valueCode = #SALMONELLA 
* #DELMENHORST "Delmenhorst"
* #DELMENHORST ^property[0].code = #parent 
* #DELMENHORST ^property[0].valueCode = #SALMONELLA 
* #DEMBE "Dembe"
* #DEMBE ^property[0].code = #parent 
* #DEMBE ^property[0].valueCode = #SALMONELLA 
* #DEMERARA "Demerara"
* #DEMERARA ^property[0].code = #parent 
* #DEMERARA ^property[0].valueCode = #SALMONELLA 
* #DENVER "Denver"
* #DENVER ^property[0].code = #parent 
* #DENVER ^property[0].valueCode = #SALMONELLA 
* #DERBY "Derby"
* #DERBY ^property[0].code = #parent 
* #DERBY ^property[0].valueCode = #SALMONELLA 
* #DERKLE "Derkle"
* #DERKLE ^property[0].code = #parent 
* #DERKLE ^property[0].valueCode = #SALMONELLA 
* #DESSAU "Dessau"
* #DESSAU ^property[0].code = #parent 
* #DESSAU ^property[0].valueCode = #SALMONELLA 
* #DETMOLD "Detmold"
* #DETMOLD ^property[0].code = #parent 
* #DETMOLD ^property[0].valueCode = #SALMONELLA 
* #DEVERSOIR "Deversoir"
* #DEVERSOIR ^property[0].code = #parent 
* #DEVERSOIR ^property[0].valueCode = #SALMONELLA 
* #DIBRA "Dibra"
* #DIBRA ^property[0].code = #parent 
* #DIBRA ^property[0].valueCode = #SALMONELLA 
* #DIETRICHSDORF "Dietrichsdorf"
* #DIETRICHSDORF ^property[0].code = #parent 
* #DIETRICHSDORF ^property[0].valueCode = #SALMONELLA 
* #DIEUPPEUL "Dieuppeul"
* #DIEUPPEUL ^property[0].code = #parent 
* #DIEUPPEUL ^property[0].valueCode = #SALMONELLA 
* #DIGUEL "Diguel"
* #DIGUEL ^property[0].code = #parent 
* #DIGUEL ^property[0].valueCode = #SALMONELLA 
* #DINGIRI "Dingiri"
* #DINGIRI ^property[0].code = #parent 
* #DINGIRI ^property[0].valueCode = #SALMONELLA 
* #DIOGOYE "Diogoye"
* #DIOGOYE ^property[0].code = #parent 
* #DIOGOYE ^property[0].valueCode = #SALMONELLA 
* #DIOURBEL "Diourbel"
* #DIOURBEL ^property[0].code = #parent 
* #DIOURBEL ^property[0].valueCode = #SALMONELLA 
* #DJAKARTA "Djakarta"
* #DJAKARTA ^property[0].code = #parent 
* #DJAKARTA ^property[0].valueCode = #SALMONELLA 
* #DJAMA "Djama"
* #DJAMA ^property[0].code = #parent 
* #DJAMA ^property[0].valueCode = #SALMONELLA 
* #DJELFA "Djelfa"
* #DJELFA ^property[0].code = #parent 
* #DJELFA ^property[0].valueCode = #SALMONELLA 
* #DJERMAIA "Djermaia"
* #DJERMAIA ^property[0].code = #parent 
* #DJERMAIA ^property[0].valueCode = #SALMONELLA 
* #DJIBOUTI "Djibouti"
* #DJIBOUTI ^property[0].code = #parent 
* #DJIBOUTI ^property[0].valueCode = #SALMONELLA 
* #DJINTEN "Djinten"
* #DJINTEN ^property[0].code = #parent 
* #DJINTEN ^property[0].valueCode = #SALMONELLA 
* #DJUGU "Djugu"
* #DJUGU ^property[0].code = #parent 
* #DJUGU ^property[0].valueCode = #SALMONELLA 
* #DOBA "Doba"
* #DOBA ^property[0].code = #parent 
* #DOBA ^property[0].valueCode = #SALMONELLA 
* #DOEL "Doel"
* #DOEL ^property[0].code = #parent 
* #DOEL ^property[0].valueCode = #SALMONELLA 
* #DONCASTER "Doncaster"
* #DONCASTER ^property[0].code = #parent 
* #DONCASTER ^property[0].valueCode = #SALMONELLA 
* #DONNA "Donna"
* #DONNA ^property[0].code = #parent 
* #DONNA ^property[0].valueCode = #SALMONELLA 
* #DOORN "Doorn"
* #DOORN ^property[0].code = #parent 
* #DOORN ^property[0].valueCode = #SALMONELLA 
* #DORTMUND "Dortmund"
* #DORTMUND ^property[0].code = #parent 
* #DORTMUND ^property[0].valueCode = #SALMONELLA 
* #DOUALA "Douala"
* #DOUALA ^property[0].code = #parent 
* #DOUALA ^property[0].valueCode = #SALMONELLA 
* #DOUGI "Dougi"
* #DOUGI ^property[0].code = #parent 
* #DOUGI ^property[0].valueCode = #SALMONELLA 
* #DOULASSAME "Doulassame"
* #DOULASSAME ^property[0].code = #parent 
* #DOULASSAME ^property[0].valueCode = #SALMONELLA 
* #DRAC "Drac"
* #DRAC ^property[0].code = #parent 
* #DRAC ^property[0].valueCode = #SALMONELLA 
* #DRESDEN "Dresden"
* #DRESDEN ^property[0].code = #parent 
* #DRESDEN ^property[0].valueCode = #SALMONELLA 
* #DRIFFIELD "Driffield"
* #DRIFFIELD ^property[0].code = #parent 
* #DRIFFIELD ^property[0].valueCode = #SALMONELLA 
* #DROGANA "Drogana"
* #DROGANA ^property[0].code = #parent 
* #DROGANA ^property[0].valueCode = #SALMONELLA 
* #DUBLIN "Dublin"
* #DUBLIN ^property[0].code = #parent 
* #DUBLIN ^property[0].valueCode = #SALMONELLA 
* #DUESSELDORF "Duesseldorf"
* #DUESSELDORF ^property[0].code = #parent 
* #DUESSELDORF ^property[0].valueCode = #SALMONELLA 
* #DUGBE "Dugbe"
* #DUGBE ^property[0].code = #parent 
* #DUGBE ^property[0].valueCode = #SALMONELLA 
* #DUISBURG "Duisburg"
* #DUISBURG ^property[0].code = #parent 
* #DUISBURG ^property[0].valueCode = #SALMONELLA 
* #DUMFRIES "Dumfries"
* #DUMFRIES ^property[0].code = #parent 
* #DUMFRIES ^property[0].valueCode = #SALMONELLA 
* #DUNKWA "Dunkwa"
* #DUNKWA ^property[0].code = #parent 
* #DUNKWA ^property[0].valueCode = #SALMONELLA 
* #DURBAN "Durban"
* #DURBAN ^property[0].code = #parent 
* #DURBAN ^property[0].valueCode = #SALMONELLA 
* #DURHAM "Durham"
* #DURHAM ^property[0].code = #parent 
* #DURHAM ^property[0].valueCode = #SALMONELLA 
* #DUVAL "Duval"
* #DUVAL ^property[0].code = #parent 
* #DUVAL ^property[0].valueCode = #SALMONELLA 
* #EALING "Ealing"
* #EALING ^property[0].code = #parent 
* #EALING ^property[0].valueCode = #SALMONELLA 
* #EASTBOURNE "Eastbourne"
* #EASTBOURNE ^property[0].code = #parent 
* #EASTBOURNE ^property[0].valueCode = #SALMONELLA 
* #EASTGLAM "Eastglam"
* #EASTGLAM ^property[0].code = #parent 
* #EASTGLAM ^property[0].valueCode = #SALMONELLA 
* #EAUBONNE "Eaubonne"
* #EAUBONNE ^property[0].code = #parent 
* #EAUBONNE ^property[0].valueCode = #SALMONELLA 
* #EBERSWALDE "Eberswalde"
* #EBERSWALDE ^property[0].code = #parent 
* #EBERSWALDE ^property[0].valueCode = #SALMONELLA 
* #EBOKO "Eboko"
* #EBOKO ^property[0].code = #parent 
* #EBOKO ^property[0].valueCode = #SALMONELLA 
* #EBRIE "Ebrie"
* #EBRIE ^property[0].code = #parent 
* #EBRIE ^property[0].valueCode = #SALMONELLA 
* #ECHA "Echa"
* #ECHA ^property[0].code = #parent 
* #ECHA ^property[0].valueCode = #SALMONELLA 
* #EDE "Ede"
* #EDE ^property[0].code = #parent 
* #EDE ^property[0].valueCode = #SALMONELLA 
* #EDINBURG "Edinburg"
* #EDINBURG ^property[0].code = #parent 
* #EDINBURG ^property[0].valueCode = #SALMONELLA 
* #EDMONTON "Edmonton"
* #EDMONTON ^property[0].code = #parent 
* #EDMONTON ^property[0].valueCode = #SALMONELLA 
* #EGUSI "Egusi"
* #EGUSI ^property[0].code = #parent 
* #EGUSI ^property[0].valueCode = #SALMONELLA 
* #EGUSITOO "Egusitoo"
* #EGUSITOO ^property[0].code = #parent 
* #EGUSITOO ^property[0].valueCode = #SALMONELLA 
* #EINGEDI "Eingedi"
* #EINGEDI ^property[0].code = #parent 
* #EINGEDI ^property[0].valueCode = #SALMONELLA 
* #EKO "Eko"
* #EKO ^property[0].code = #parent 
* #EKO ^property[0].valueCode = #SALMONELLA 
* #EKOTEDO "Ekotedo"
* #EKOTEDO ^property[0].code = #parent 
* #EKOTEDO ^property[0].valueCode = #SALMONELLA 
* #EKPOUI "Ekpoui"
* #EKPOUI ^property[0].code = #parent 
* #EKPOUI ^property[0].valueCode = #SALMONELLA 
* #ELISABETHVILLE "Elisabethville"
* #ELISABETHVILLE ^property[0].code = #parent 
* #ELISABETHVILLE ^property[0].valueCode = #SALMONELLA 
* #ELOKATE "Elokate"
* #ELOKATE ^property[0].code = #parent 
* #ELOKATE ^property[0].valueCode = #SALMONELLA 
* #ELOMRANE "Elomrane"
* #ELOMRANE ^property[0].code = #parent 
* #ELOMRANE ^property[0].valueCode = #SALMONELLA 
* #EMEK "Emek"
* #EMEK ^property[0].code = #parent 
* #EMEK ^property[0].valueCode = #SALMONELLA 
* #EMMASTAD "Emmastad"
* #EMMASTAD ^property[0].code = #parent 
* #EMMASTAD ^property[0].valueCode = #SALMONELLA 
* #ENCINO "Encino"
* #ENCINO ^property[0].code = #parent 
* #ENCINO ^property[0].valueCode = #SALMONELLA 
* #ENSCHEDE "Enschede"
* #ENSCHEDE ^property[0].code = #parent 
* #ENSCHEDE ^property[0].valueCode = #SALMONELLA 
* #ENTEBBE "Entebbe"
* #ENTEBBE ^property[0].code = #parent 
* #ENTEBBE ^property[0].valueCode = #SALMONELLA 
* #ENTERICA "Enterica (Subsp I)"
* #ENTERICA ^property[0].code = #parent 
* #ENTERICA ^property[0].valueCode = #SALMONELLA 
* #ENTERICA_MONO "Enterica monophasic"
* #ENTERICA_MONO ^property[0].code = #parent 
* #ENTERICA_MONO ^property[0].valueCode = #SALMONELLA 
* #ENTERITIDIS "Enteritidis"
* #ENTERITIDIS ^property[0].code = #parent 
* #ENTERITIDIS ^property[0].valueCode = #SALMONELLA 
* #ENUGU "Enugu"
* #ENUGU ^property[0].code = #parent 
* #ENUGU ^property[0].valueCode = #SALMONELLA 
* #EPALINGES "Epalinges"
* #EPALINGES ^property[0].code = #parent 
* #EPALINGES ^property[0].valueCode = #SALMONELLA 
* #EPICRATES "Epicrates"
* #EPICRATES ^property[0].code = #parent 
* #EPICRATES ^property[0].valueCode = #SALMONELLA 
* #EPINAY "Epinay"
* #EPINAY ^property[0].code = #parent 
* #EPINAY ^property[0].valueCode = #SALMONELLA 
* #EPPENDORF "Eppendorf"
* #EPPENDORF ^property[0].code = #parent 
* #EPPENDORF ^property[0].valueCode = #SALMONELLA 
* #ERFURT "Erfurt"
* #ERFURT ^property[0].code = #parent 
* #ERFURT ^property[0].valueCode = #SALMONELLA 
* #ESCANABA "Escanaba"
* #ESCANABA ^property[0].code = #parent 
* #ESCANABA ^property[0].valueCode = #SALMONELLA 
* #ESCHBERG "Eschberg"
* #ESCHBERG ^property[0].code = #parent 
* #ESCHBERG ^property[0].valueCode = #SALMONELLA 
* #ESCHWEILER "Eschweiler"
* #ESCHWEILER ^property[0].code = #parent 
* #ESCHWEILER ^property[0].valueCode = #SALMONELLA 
* #ESSEN "Essen"
* #ESSEN ^property[0].code = #parent 
* #ESSEN ^property[0].valueCode = #SALMONELLA 
* #ESSINGEN "Essingen"
* #ESSINGEN ^property[0].code = #parent 
* #ESSINGEN ^property[0].valueCode = #SALMONELLA 
* #ETTERBEEK "Etterbeek"
* #ETTERBEEK ^property[0].code = #parent 
* #ETTERBEEK ^property[0].valueCode = #SALMONELLA 
* #EUSTON "Euston"
* #EUSTON ^property[0].code = #parent 
* #EUSTON ^property[0].valueCode = #SALMONELLA 
* #EVERLEIGH "Everleigh"
* #EVERLEIGH ^property[0].code = #parent 
* #EVERLEIGH ^property[0].valueCode = #SALMONELLA 
* #EVRY "Evry"
* #EVRY ^property[0].code = #parent 
* #EVRY ^property[0].valueCode = #SALMONELLA 
* #EZRA "Ezra"
* #EZRA ^property[0].code = #parent 
* #EZRA ^property[0].valueCode = #SALMONELLA 
* #FAIRFIELD "Fairfield"
* #FAIRFIELD ^property[0].code = #parent 
* #FAIRFIELD ^property[0].valueCode = #SALMONELLA 
* #FAJARA "Fajara"
* #FAJARA ^property[0].code = #parent 
* #FAJARA ^property[0].valueCode = #SALMONELLA 
* #FAJI "Faji"
* #FAJI ^property[0].code = #parent 
* #FAJI ^property[0].valueCode = #SALMONELLA 
* #FALKENSEE "Falkensee"
* #FALKENSEE ^property[0].code = #parent 
* #FALKENSEE ^property[0].valueCode = #SALMONELLA 
* #FALLOWFIELD "Fallowfield"
* #FALLOWFIELD ^property[0].code = #parent 
* #FALLOWFIELD ^property[0].valueCode = #SALMONELLA 
* #FANN "Fann"
* #FANN ^property[0].code = #parent 
* #FANN ^property[0].valueCode = #SALMONELLA 
* #FANTI "Fanti"
* #FANTI ^property[0].code = #parent 
* #FANTI ^property[0].valueCode = #SALMONELLA 
* #FARAKAN "Farakan"
* #FARAKAN ^property[0].code = #parent 
* #FARAKAN ^property[0].valueCode = #SALMONELLA 
* #FARCHA "Farcha"
* #FARCHA ^property[0].code = #parent 
* #FARCHA ^property[0].valueCode = #SALMONELLA 
* #FAREHAM "Fareham"
* #FAREHAM ^property[0].code = #parent 
* #FAREHAM ^property[0].valueCode = #SALMONELLA 
* #FARMINGDALE "Farmingdale"
* #FARMINGDALE ^property[0].code = #parent 
* #FARMINGDALE ^property[0].valueCode = #SALMONELLA 
* #FARMSEN "Farmsen"
* #FARMSEN ^property[0].code = #parent 
* #FARMSEN ^property[0].valueCode = #SALMONELLA 
* #FARSTA "Farsta"
* #FARSTA ^property[0].code = #parent 
* #FARSTA ^property[0].valueCode = #SALMONELLA 
* #FASS "Fass"
* #FASS ^property[0].code = #parent 
* #FASS ^property[0].valueCode = #SALMONELLA 
* #FAYED "Fayed"
* #FAYED ^property[0].code = #parent 
* #FAYED ^property[0].valueCode = #SALMONELLA 
* #FEHRBELLIN "Fehrbellin"
* #FEHRBELLIN ^property[0].code = #parent 
* #FEHRBELLIN ^property[0].valueCode = #SALMONELLA 
* #FERLO "Ferlo"
* #FERLO ^property[0].code = #parent 
* #FERLO ^property[0].valueCode = #SALMONELLA 
* #FERRUCH "Ferruch"
* #FERRUCH ^property[0].code = #parent 
* #FERRUCH ^property[0].valueCode = #SALMONELLA 
* #FILLMORE "Fillmore"
* #FILLMORE ^property[0].code = #parent 
* #FILLMORE ^property[0].valueCode = #SALMONELLA 
* #FINAGHY "Finaghy"
* #FINAGHY ^property[0].code = #parent 
* #FINAGHY ^property[0].valueCode = #SALMONELLA 
* #FINDORFF "Findorff"
* #FINDORFF ^property[0].code = #parent 
* #FINDORFF ^property[0].valueCode = #SALMONELLA 
* #FINKENWERDER "Finkenwerder"
* #FINKENWERDER ^property[0].code = #parent 
* #FINKENWERDER ^property[0].valueCode = #SALMONELLA 
* #FISCHERHUETTE "Fischerhuette"
* #FISCHERHUETTE ^property[0].code = #parent 
* #FISCHERHUETTE ^property[0].valueCode = #SALMONELLA 
* #FISCHERKIETZ "Fischerkietz"
* #FISCHERKIETZ ^property[0].code = #parent 
* #FISCHERKIETZ ^property[0].valueCode = #SALMONELLA 
* #FISCHERSTRASSE "Fischerstrasse"
* #FISCHERSTRASSE ^property[0].code = #parent 
* #FISCHERSTRASSE ^property[0].valueCode = #SALMONELLA 
* #FITZROY "Fitzroy"
* #FITZROY ^property[0].code = #parent 
* #FITZROY ^property[0].valueCode = #SALMONELLA 
* #FLORIAN "Florian"
* #FLORIAN ^property[0].code = #parent 
* #FLORIAN ^property[0].valueCode = #SALMONELLA 
* #FLORIDA "Florida"
* #FLORIDA ^property[0].code = #parent 
* #FLORIDA ^property[0].valueCode = #SALMONELLA 
* #FLOTTBEK "Flottbek"
* #FLOTTBEK ^property[0].code = #parent 
* #FLOTTBEK ^property[0].valueCode = #SALMONELLA 
* #FLUNTERN "Fluntern"
* #FLUNTERN ^property[0].code = #parent 
* #FLUNTERN ^property[0].valueCode = #SALMONELLA 
* #FOMECO "Fomeco"
* #FOMECO ^property[0].code = #parent 
* #FOMECO ^property[0].valueCode = #SALMONELLA 
* #FORTLAMY "Fortlamy"
* #FORTLAMY ^property[0].code = #parent 
* #FORTLAMY ^property[0].valueCode = #SALMONELLA 
* #FORTUNE "Fortune"
* #FORTUNE ^property[0].code = #parent 
* #FORTUNE ^property[0].valueCode = #SALMONELLA 
* #FRANKEN "Franken"
* #FRANKEN ^property[0].code = #parent 
* #FRANKEN ^property[0].valueCode = #SALMONELLA 
* #FRANKFURT "Frankfurt"
* #FRANKFURT ^property[0].code = #parent 
* #FRANKFURT ^property[0].valueCode = #SALMONELLA 
* #FREDERIKSBERG "Frederiksberg"
* #FREDERIKSBERG ^property[0].code = #parent 
* #FREDERIKSBERG ^property[0].valueCode = #SALMONELLA 
* #FREEFALLS "Freefalls"
* #FREEFALLS ^property[0].code = #parent 
* #FREEFALLS ^property[0].valueCode = #SALMONELLA 
* #FREETOWN "Freetown"
* #FREETOWN ^property[0].code = #parent 
* #FREETOWN ^property[0].valueCode = #SALMONELLA 
* #FREIBURG "Freiburg"
* #FREIBURG ^property[0].code = #parent 
* #FREIBURG ^property[0].valueCode = #SALMONELLA 
* #FRESNO "Fresno"
* #FRESNO ^property[0].code = #parent 
* #FRESNO ^property[0].valueCode = #SALMONELLA 
* #FRIEDENAU "Friedenau"
* #FRIEDENAU ^property[0].code = #parent 
* #FRIEDENAU ^property[0].valueCode = #SALMONELLA 
* #FRIEDRICHSFELDE "Friedrichsfelde"
* #FRIEDRICHSFELDE ^property[0].code = #parent 
* #FRIEDRICHSFELDE ^property[0].valueCode = #SALMONELLA 
* #FRINTROP "Frintrop"
* #FRINTROP ^property[0].code = #parent 
* #FRINTROP ^property[0].valueCode = #SALMONELLA 
* #FUFU "Fufu"
* #FUFU ^property[0].code = #parent 
* #FUFU ^property[0].valueCode = #SALMONELLA 
* #FULDA "Fulda"
* #FULDA ^property[0].code = #parent 
* #FULDA ^property[0].valueCode = #SALMONELLA 
* #FULICA "Fulica"
* #FULICA ^property[0].code = #parent 
* #FULICA ^property[0].valueCode = #SALMONELLA 
* #FYRIS "Fyris"
* #FYRIS ^property[0].code = #parent 
* #FYRIS ^property[0].valueCode = #SALMONELLA 
* #GABON "Gabon"
* #GABON ^property[0].code = #parent 
* #GABON ^property[0].valueCode = #SALMONELLA 
* #GAFSA "Gafsa"
* #GAFSA ^property[0].code = #parent 
* #GAFSA ^property[0].valueCode = #SALMONELLA 
* #GAILLAC "Gaillac"
* #GAILLAC ^property[0].code = #parent 
* #GAILLAC ^property[0].valueCode = #SALMONELLA 
* #GALIEMA "Galiema"
* #GALIEMA ^property[0].code = #parent 
* #GALIEMA ^property[0].valueCode = #SALMONELLA 
* #GALIL "Galil"
* #GALIL ^property[0].code = #parent 
* #GALIL ^property[0].valueCode = #SALMONELLA 
* #GALLEN "Gallen"
* #GALLEN ^property[0].code = #parent 
* #GALLEN ^property[0].valueCode = #SALMONELLA 
* #GALLINARUM_ "Gallinarum"
* #GALLINARUM_ ^property[0].code = #parent 
* #GALLINARUM_ ^property[0].valueCode = #SALMONELLA 
* #GAMABA "Gamaba"
* #GAMABA ^property[0].code = #parent 
* #GAMABA ^property[0].valueCode = #SALMONELLA 
* #GAMBAGA "Gambaga"
* #GAMBAGA ^property[0].code = #parent 
* #GAMBAGA ^property[0].valueCode = #SALMONELLA 
* #GAMBIA "Gambia"
* #GAMBIA ^property[0].code = #parent 
* #GAMBIA ^property[0].valueCode = #SALMONELLA 
* #GAMINARA "Gaminara"
* #GAMINARA ^property[0].code = #parent 
* #GAMINARA ^property[0].valueCode = #SALMONELLA 
* #GARBA "Garba"
* #GARBA ^property[0].code = #parent 
* #GARBA ^property[0].valueCode = #SALMONELLA 
* #GAROLI "Garoli"
* #GAROLI ^property[0].code = #parent 
* #GAROLI ^property[0].valueCode = #SALMONELLA 
* #GASSI "Gassi"
* #GASSI ^property[0].code = #parent 
* #GASSI ^property[0].valueCode = #SALMONELLA 
* #GATESHEAD "Gateshead"
* #GATESHEAD ^property[0].code = #parent 
* #GATESHEAD ^property[0].valueCode = #SALMONELLA 
* #GATINEAU "Gatineau"
* #GATINEAU ^property[0].code = #parent 
* #GATINEAU ^property[0].valueCode = #SALMONELLA 
* #GATOW "Gatow"
* #GATOW ^property[0].code = #parent 
* #GATOW ^property[0].valueCode = #SALMONELLA 
* #GATUNI "Gatuni"
* #GATUNI ^property[0].code = #parent 
* #GATUNI ^property[0].valueCode = #SALMONELLA 
* #GBADAGO "Gbadago"
* #GBADAGO ^property[0].code = #parent 
* #GBADAGO ^property[0].valueCode = #SALMONELLA 
* #GDANSK "Gdansk"
* #GDANSK ^property[0].code = #parent 
* #GDANSK ^property[0].valueCode = #SALMONELLA 
* #GDANSK_VAR_14+ "Gdansk var. 14+"
* #GDANSK_VAR_14+ ^property[0].code = #parent 
* #GDANSK_VAR_14+ ^property[0].valueCode = #SALMONELLA 
* #GEGE "Gege"
* #GEGE ^property[0].code = #parent 
* #GEGE ^property[0].valueCode = #SALMONELLA 
* #GEORGIA "Georgia"
* #GEORGIA ^property[0].code = #parent 
* #GEORGIA ^property[0].valueCode = #SALMONELLA 
* #GERA "Gera"
* #GERA ^property[0].code = #parent 
* #GERA ^property[0].valueCode = #SALMONELLA 
* #GERALDTON "Geraldton"
* #GERALDTON ^property[0].code = #parent 
* #GERALDTON ^property[0].valueCode = #SALMONELLA 
* #GERLAND "Gerland"
* #GERLAND ^property[0].code = #parent 
* #GERLAND ^property[0].valueCode = #SALMONELLA 
* #GHANA "Ghana"
* #GHANA ^property[0].code = #parent 
* #GHANA ^property[0].valueCode = #SALMONELLA 
* #GIESSEN "Giessen"
* #GIESSEN ^property[0].code = #parent 
* #GIESSEN ^property[0].valueCode = #SALMONELLA 
* #GIVE "Give"
* #GIVE ^property[0].code = #parent 
* #GIVE ^property[0].valueCode = #SALMONELLA 
* #GIVE_VAR_15+ "Give var. 15+"
* #GIVE_VAR_15+ ^property[0].code = #parent 
* #GIVE_VAR_15+ ^property[0].valueCode = #SALMONELLA 
* #GIVE_VAR_15+_34+ "Give var. 15+, 34+"
* #GIVE_VAR_15+_34+ ^property[0].code = #parent 
* #GIVE_VAR_15+_34+ ^property[0].valueCode = #SALMONELLA 
* #GIZA "Giza"
* #GIZA ^property[0].code = #parent 
* #GIZA ^property[0].valueCode = #SALMONELLA 
* #GLASGOW "Glasgow"
* #GLASGOW ^property[0].code = #parent 
* #GLASGOW ^property[0].valueCode = #SALMONELLA 
* #GLIDJI "Glidji"
* #GLIDJI ^property[0].code = #parent 
* #GLIDJI ^property[0].valueCode = #SALMONELLA 
* #GLOSTRUP "Glostrup"
* #GLOSTRUP ^property[0].code = #parent 
* #GLOSTRUP ^property[0].valueCode = #SALMONELLA 
* #GLOUCESTER "Gloucester"
* #GLOUCESTER ^property[0].code = #parent 
* #GLOUCESTER ^property[0].valueCode = #SALMONELLA 
* #GNESTA "Gnesta"
* #GNESTA ^property[0].code = #parent 
* #GNESTA ^property[0].valueCode = #SALMONELLA 
* #GODESBERG "Godesberg"
* #GODESBERG ^property[0].code = #parent 
* #GODESBERG ^property[0].valueCode = #SALMONELLA 
* #GOELZAU "Goelzau"
* #GOELZAU ^property[0].code = #parent 
* #GOELZAU ^property[0].valueCode = #SALMONELLA 
* #GOELZAU_VAR_15+ "Goelzau var. 15+"
* #GOELZAU_VAR_15+ ^property[0].code = #parent 
* #GOELZAU_VAR_15+ ^property[0].valueCode = #SALMONELLA 
* #GOETEBORG "Goeteborg"
* #GOETEBORG ^property[0].code = #parent 
* #GOETEBORG ^property[0].valueCode = #SALMONELLA 
* #GOETTINGEN "Goettingen"
* #GOETTINGEN ^property[0].code = #parent 
* #GOETTINGEN ^property[0].valueCode = #SALMONELLA 
* #GOKUL "Gokul"
* #GOKUL ^property[0].code = #parent 
* #GOKUL ^property[0].valueCode = #SALMONELLA 
* #GOLDCOAST "Goldcoast"
* #GOLDCOAST ^property[0].code = #parent 
* #GOLDCOAST ^property[0].valueCode = #SALMONELLA 
* #GOMA "Goma"
* #GOMA ^property[0].code = #parent 
* #GOMA ^property[0].valueCode = #SALMONELLA 
* #GOMBE "Gombe"
* #GOMBE ^property[0].code = #parent 
* #GOMBE ^property[0].valueCode = #SALMONELLA 
* #GOOD "Good"
* #GOOD ^property[0].code = #parent 
* #GOOD ^property[0].valueCode = #SALMONELLA 
* #GORI "Gori"
* #GORI ^property[0].code = #parent 
* #GORI ^property[0].valueCode = #SALMONELLA 
* #GOULFEY "Goulfey"
* #GOULFEY ^property[0].code = #parent 
* #GOULFEY ^property[0].valueCode = #SALMONELLA 
* #GOULOUMBO "Gouloumbo"
* #GOULOUMBO ^property[0].code = #parent 
* #GOULOUMBO ^property[0].valueCode = #SALMONELLA 
* #GOVERDHAN "Goverdhan"
* #GOVERDHAN ^property[0].code = #parent 
* #GOVERDHAN ^property[0].valueCode = #SALMONELLA 
* #GOZO "Gozo"
* #GOZO ^property[0].code = #parent 
* #GOZO ^property[0].valueCode = #SALMONELLA 
* #GRAMPIAN "Grampian"
* #GRAMPIAN ^property[0].code = #parent 
* #GRAMPIAN ^property[0].valueCode = #SALMONELLA 
* #GRANCANARIA "Grancanaria"
* #GRANCANARIA ^property[0].code = #parent 
* #GRANCANARIA ^property[0].valueCode = #SALMONELLA 
* #GRANDHAVEN "Grandhaven"
* #GRANDHAVEN ^property[0].code = #parent 
* #GRANDHAVEN ^property[0].valueCode = #SALMONELLA 
* #GRANLO "Granlo"
* #GRANLO ^property[0].code = #parent 
* #GRANLO ^property[0].valueCode = #SALMONELLA 
* #GRAZ "Graz"
* #GRAZ ^property[0].code = #parent 
* #GRAZ ^property[0].valueCode = #SALMONELLA 
* #GREIZ "Greiz"
* #GREIZ ^property[0].code = #parent 
* #GREIZ ^property[0].valueCode = #SALMONELLA 
* #GROENEKAN "Groenekan"
* #GROENEKAN ^property[0].code = #parent 
* #GROENEKAN ^property[0].valueCode = #SALMONELLA 
* #GROUP11 "Group O:11 (F)"
* #GROUP11 ^property[0].code = #parent 
* #GROUP11 ^property[0].valueCode = #SALMONELLA 
* #GROUP13 "Group O:13 (G)"
* #GROUP13 ^property[0].code = #parent 
* #GROUP13 ^property[0].valueCode = #SALMONELLA 
* #GROUP16 "Group O:16 (I)"
* #GROUP16 ^property[0].code = #parent 
* #GROUP16 ^property[0].valueCode = #SALMONELLA 
* #GROUP17 "Group O:17 (J)"
* #GROUP17 ^property[0].code = #parent 
* #GROUP17 ^property[0].valueCode = #SALMONELLA 
* #GROUP18 "Group O:18 (K)"
* #GROUP18 ^property[0].code = #parent 
* #GROUP18 ^property[0].valueCode = #SALMONELLA 
* #GROUP1_3_19 "Group (E4) O:1,3,19"
* #GROUP1_3_19 ^property[0].code = #parent 
* #GROUP1_3_19 ^property[0].valueCode = #SALMONELLA 
* #GROUP2 "Group O:2 (A)"
* #GROUP2 ^property[0].code = #parent 
* #GROUP2 ^property[0].valueCode = #SALMONELLA 
* #GROUP21 "Group O:21 (L)"
* #GROUP21 ^property[0].code = #parent 
* #GROUP21 ^property[0].valueCode = #SALMONELLA 
* #GROUP28 "Group O:28 (M)"
* #GROUP28 ^property[0].code = #parent 
* #GROUP28 ^property[0].valueCode = #SALMONELLA 
* #GROUP30 "Group O:30 (N)"
* #GROUP30 ^property[0].code = #parent 
* #GROUP30 ^property[0].valueCode = #SALMONELLA 
* #GROUP35 "Group O:35 (O)"
* #GROUP35 ^property[0].code = #parent 
* #GROUP35 ^property[0].valueCode = #SALMONELLA 
* #GROUP38 "Group O:38 (P)"
* #GROUP38 ^property[0].code = #parent 
* #GROUP38 ^property[0].valueCode = #SALMONELLA 
* #GROUP39 "Group O:39 (Q)"
* #GROUP39 ^property[0].code = #parent 
* #GROUP39 ^property[0].valueCode = #SALMONELLA 
* #GROUP3_10 "Group (E1) O:3,10"
* #GROUP3_10 ^property[0].code = #parent 
* #GROUP3_10 ^property[0].valueCode = #SALMONELLA 
* #GROUP4 "Group O:4 (B)"
* #GROUP4 ^property[0].code = #parent 
* #GROUP4 ^property[0].valueCode = #SALMONELLA 
* #GROUP40 "Group (R) O:40"
* #GROUP40 ^property[0].code = #parent 
* #GROUP40 ^property[0].valueCode = #SALMONELLA 
* #GROUP41 "Group O:41 (S)"
* #GROUP41 ^property[0].code = #parent 
* #GROUP41 ^property[0].valueCode = #SALMONELLA 
* #GROUP42 "Group O:42 (T)"
* #GROUP42 ^property[0].code = #parent 
* #GROUP42 ^property[0].valueCode = #SALMONELLA 
* #GROUP43 "Group O:43 (U)"
* #GROUP43 ^property[0].code = #parent 
* #GROUP43 ^property[0].valueCode = #SALMONELLA 
* #GROUP44 "Group O:44 (V)"
* #GROUP44 ^property[0].code = #parent 
* #GROUP44 ^property[0].valueCode = #SALMONELLA 
* #GROUP45 "Group O:45 (W)"
* #GROUP45 ^property[0].code = #parent 
* #GROUP45 ^property[0].valueCode = #SALMONELLA 
* #GROUP47 "Group O:47 (X)"
* #GROUP47 ^property[0].code = #parent 
* #GROUP47 ^property[0].valueCode = #SALMONELLA 
* #GROUP48 "Group O:48 (Y)"
* #GROUP48 ^property[0].code = #parent 
* #GROUP48 ^property[0].valueCode = #SALMONELLA 
* #GROUP50 "Group O:50 (Z)"
* #GROUP50 ^property[0].code = #parent 
* #GROUP50 ^property[0].valueCode = #SALMONELLA 
* #GROUP51 "Group O:51"
* #GROUP51 ^property[0].code = #parent 
* #GROUP51 ^property[0].valueCode = #SALMONELLA 
* #GROUP52 "Group O:52"
* #GROUP52 ^property[0].code = #parent 
* #GROUP52 ^property[0].valueCode = #SALMONELLA 
* #GROUP53 "Group O:53"
* #GROUP53 ^property[0].code = #parent 
* #GROUP53 ^property[0].valueCode = #SALMONELLA 
* #GROUP54 "Group O:54"
* #GROUP54 ^property[0].code = #parent 
* #GROUP54 ^property[0].valueCode = #SALMONELLA 
* #GROUP55 "Group O:55"
* #GROUP55 ^property[0].code = #parent 
* #GROUP55 ^property[0].valueCode = #SALMONELLA 
* #GROUP56 "Group O:56"
* #GROUP56 ^property[0].code = #parent 
* #GROUP56 ^property[0].valueCode = #SALMONELLA 
* #GROUP57 "Group O:57"
* #GROUP57 ^property[0].code = #parent 
* #GROUP57 ^property[0].valueCode = #SALMONELLA 
* #GROUP58 "Group O:58"
* #GROUP58 ^property[0].code = #parent 
* #GROUP58 ^property[0].valueCode = #SALMONELLA 
* #GROUP59 "Group O:59"
* #GROUP59 ^property[0].code = #parent 
* #GROUP59 ^property[0].valueCode = #SALMONELLA 
* #GROUP60 "Group O:60"
* #GROUP60 ^property[0].code = #parent 
* #GROUP60 ^property[0].valueCode = #SALMONELLA 
* #GROUP61 "Group O:61"
* #GROUP61 ^property[0].code = #parent 
* #GROUP61 ^property[0].valueCode = #SALMONELLA 
* #GROUP62 "Group O:62"
* #GROUP62 ^property[0].code = #parent 
* #GROUP62 ^property[0].valueCode = #SALMONELLA 
* #GROUP63 "Group O:63"
* #GROUP63 ^property[0].code = #parent 
* #GROUP63 ^property[0].valueCode = #SALMONELLA 
* #GROUP65 "Group O:65"
* #GROUP65 ^property[0].code = #parent 
* #GROUP65 ^property[0].valueCode = #SALMONELLA 
* #GROUP66 "Group O:66"
* #GROUP66 ^property[0].code = #parent 
* #GROUP66 ^property[0].valueCode = #SALMONELLA 
* #GROUP67 "Group O:67"
* #GROUP67 ^property[0].code = #parent 
* #GROUP67 ^property[0].valueCode = #SALMONELLA 
* #GROUP6_14 "Group (H) O:6,14"
* #GROUP6_14 ^property[0].code = #parent 
* #GROUP6_14 ^property[0].valueCode = #SALMONELLA 
* #GROUP7 "Group O:7 (C1)"
* #GROUP7 ^property[0].code = #parent 
* #GROUP7 ^property[0].valueCode = #SALMONELLA 
* #GROUP8 "Group O:8 (C2-C3)"
* #GROUP8 ^property[0].code = #parent 
* #GROUP8 ^property[0].valueCode = #SALMONELLA 
* #GROUP9 "Group O:9 (D1)"
* #GROUP9 ^property[0].code = #parent 
* #GROUP9 ^property[0].valueCode = #SALMONELLA 
* #GROUP9_46 "Group (D2) O:9,46"
* #GROUP9_46 ^property[0].code = #parent 
* #GROUP9_46 ^property[0].valueCode = #SALMONELLA 
* #GROUP9_46_27 "Group O:9,46,27 (D3)"
* #GROUP9_46_27 ^property[0].code = #parent 
* #GROUP9_46_27 ^property[0].valueCode = #SALMONELLA 
* #GROUPA "Group (A) O:2"
* #GROUPA ^property[0].code = #parent 
* #GROUPA ^property[0].valueCode = #SALMONELLA 
* #GROUPB "Group (B) O:4"
* #GROUPB ^property[0].code = #parent 
* #GROUPB ^property[0].valueCode = #SALMONELLA 
* #GROUPC "GroupC"
* #GROUPC ^property[0].code = #parent 
* #GROUPC ^property[0].valueCode = #SALMONELLA 
* #GROUPC1 "Group (C1) O:7"
* #GROUPC1 ^property[0].code = #parent 
* #GROUPC1 ^property[0].valueCode = #SALMONELLA 
* #GROUPC2 "Group (C2-C3) O:8"
* #GROUPC2 ^property[0].code = #parent 
* #GROUPC2 ^property[0].valueCode = #SALMONELLA 
* #GROUPD "GroupD"
* #GROUPD ^property[0].code = #parent 
* #GROUPD ^property[0].valueCode = #SALMONELLA 
* #GROUPD1 "Group (D1) O:9"
* #GROUPD1 ^property[0].code = #parent 
* #GROUPD1 ^property[0].valueCode = #SALMONELLA 
* #GROUPD2 "Group O:9,46 (D2)"
* #GROUPD2 ^property[0].code = #parent 
* #GROUPD2 ^property[0].valueCode = #SALMONELLA 
* #GROUPD3 "Group (D3) O:9,46,27"
* #GROUPD3 ^property[0].code = #parent 
* #GROUPD3 ^property[0].valueCode = #SALMONELLA 
* #GROUPE "GroupE"
* #GROUPE ^property[0].code = #parent 
* #GROUPE ^property[0].valueCode = #SALMONELLA 
* #GROUPE1 "Group O:3,10 (E1)"
* #GROUPE1 ^property[0].code = #parent 
* #GROUPE1 ^property[0].valueCode = #SALMONELLA 
* #GROUPE4 "Group O:1,3,19 (E4)"
* #GROUPE4 ^property[0].code = #parent 
* #GROUPE4 ^property[0].valueCode = #SALMONELLA 
* #GROUPG "Group (G) O:13"
* #GROUPG ^property[0].code = #parent 
* #GROUPG ^property[0].valueCode = #SALMONELLA 
* #GROUPH "Group O:6,14 (H)"
* #GROUPH ^property[0].code = #parent 
* #GROUPH ^property[0].valueCode = #SALMONELLA 
* #GROUPI "Group (I) O:16"
* #GROUPI ^property[0].code = #parent 
* #GROUPI ^property[0].valueCode = #SALMONELLA 
* #GROUPJ "Group (J) O:17"
* #GROUPJ ^property[0].code = #parent 
* #GROUPJ ^property[0].valueCode = #SALMONELLA 
* #GROUPK "Group (K) O:18"
* #GROUPK ^property[0].code = #parent 
* #GROUPK ^property[0].valueCode = #SALMONELLA 
* #GROUPL "Group (L) O:21"
* #GROUPL ^property[0].code = #parent 
* #GROUPL ^property[0].valueCode = #SALMONELLA 
* #GROUPM "Group (M) O:28"
* #GROUPM ^property[0].code = #parent 
* #GROUPM ^property[0].valueCode = #SALMONELLA 
* #GROUPN "Group (N) O:30"
* #GROUPN ^property[0].code = #parent 
* #GROUPN ^property[0].valueCode = #SALMONELLA 
* #GROUPO "Group (O) O:35"
* #GROUPO ^property[0].code = #parent 
* #GROUPO ^property[0].valueCode = #SALMONELLA 
* #GROUPP "Group (P) O:38"
* #GROUPP ^property[0].code = #parent 
* #GROUPP ^property[0].valueCode = #SALMONELLA 
* #GROUPQ "Group (Q) O:39"
* #GROUPQ ^property[0].code = #parent 
* #GROUPQ ^property[0].valueCode = #SALMONELLA 
* #GROUPR "Group O:40 (R)"
* #GROUPR ^property[0].code = #parent 
* #GROUPR ^property[0].valueCode = #SALMONELLA 
* #GROUPS "Group (S) O:41"
* #GROUPS ^property[0].code = #parent 
* #GROUPS ^property[0].valueCode = #SALMONELLA 
* #GROUPT "Group (T) O:42"
* #GROUPT ^property[0].code = #parent 
* #GROUPT ^property[0].valueCode = #SALMONELLA 
* #GROUPU "Group (U) O:43"
* #GROUPU ^property[0].code = #parent 
* #GROUPU ^property[0].valueCode = #SALMONELLA 
* #GROUPV "Group (V) O:44"
* #GROUPV ^property[0].code = #parent 
* #GROUPV ^property[0].valueCode = #SALMONELLA 
* #GROUPW "Group (W) O:45"
* #GROUPW ^property[0].code = #parent 
* #GROUPW ^property[0].valueCode = #SALMONELLA 
* #GROUPX "Group (X) O:47"
* #GROUPX ^property[0].code = #parent 
* #GROUPX ^property[0].valueCode = #SALMONELLA 
* #GROUPY "Group (Y) O:48"
* #GROUPY ^property[0].code = #parent 
* #GROUPY ^property[0].valueCode = #SALMONELLA 
* #GROUPZ "Group (Z) O:50"
* #GROUPZ ^property[0].code = #parent 
* #GROUPZ ^property[0].valueCode = #SALMONELLA 
* #GRUMPENSIS "Grumpensis"
* #GRUMPENSIS ^property[0].code = #parent 
* #GRUMPENSIS ^property[0].valueCode = #SALMONELLA 
* #GUARAPIRANGA "Guarapiranga"
* #GUARAPIRANGA ^property[0].code = #parent 
* #GUARAPIRANGA ^property[0].valueCode = #SALMONELLA 
* #GUERIN "Guerin"
* #GUERIN ^property[0].code = #parent 
* #GUERIN ^property[0].valueCode = #SALMONELLA 
* #GUEULETAPEE "Gueuletapee"
* #GUEULETAPEE ^property[0].code = #parent 
* #GUEULETAPEE ^property[0].valueCode = #SALMONELLA 
* #GUILDFORD "Guildford"
* #GUILDFORD ^property[0].code = #parent 
* #GUILDFORD ^property[0].valueCode = #SALMONELLA 
* #GUINEA "Guinea"
* #GUINEA ^property[0].code = #parent 
* #GUINEA ^property[0].valueCode = #SALMONELLA 
* #GUSTAVIA "Gustavia"
* #GUSTAVIA ^property[0].code = #parent 
* #GUSTAVIA ^property[0].valueCode = #SALMONELLA 
* #GWALE "Gwale"
* #GWALE ^property[0].code = #parent 
* #GWALE ^property[0].valueCode = #SALMONELLA 
* #GWOZA "Gwoza"
* #GWOZA ^property[0].code = #parent 
* #GWOZA ^property[0].valueCode = #SALMONELLA 
* #HAARDT "Haardt"
* #HAARDT ^property[0].code = #parent 
* #HAARDT ^property[0].valueCode = #SALMONELLA 
* #HADAR "Hadar"
* #HADAR ^property[0].code = #parent 
* #HADAR ^property[0].valueCode = #SALMONELLA 
* #HADEJIA "Hadejia"
* #HADEJIA ^property[0].code = #parent 
* #HADEJIA ^property[0].valueCode = #SALMONELLA 
* #HADUNA "Haduna"
* #HADUNA ^property[0].code = #parent 
* #HADUNA ^property[0].valueCode = #SALMONELLA 
* #HAELSINGBORG "Haelsingborg"
* #HAELSINGBORG ^property[0].code = #parent 
* #HAELSINGBORG ^property[0].valueCode = #SALMONELLA 
* #HAFERBREITE "Haferbreite"
* #HAFERBREITE ^property[0].code = #parent 
* #HAFERBREITE ^property[0].valueCode = #SALMONELLA 
* #HAGA "Haga"
* #HAGA ^property[0].code = #parent 
* #HAGA ^property[0].valueCode = #SALMONELLA 
* #HAIFA "Haifa"
* #HAIFA ^property[0].code = #parent 
* #HAIFA ^property[0].valueCode = #SALMONELLA 
* #HALLE "Halle"
* #HALLE ^property[0].code = #parent 
* #HALLE ^property[0].valueCode = #SALMONELLA 
* #HALLFOLD "Hallfold"
* #HALLFOLD ^property[0].code = #parent 
* #HALLFOLD ^property[0].valueCode = #SALMONELLA 
* #HANDEN "Handen"
* #HANDEN ^property[0].code = #parent 
* #HANDEN ^property[0].valueCode = #SALMONELLA 
* #HANN "Hann"
* #HANN ^property[0].code = #parent 
* #HANN ^property[0].valueCode = #SALMONELLA 
* #HANNOVER "Hannover"
* #HANNOVER ^property[0].code = #parent 
* #HANNOVER ^property[0].valueCode = #SALMONELLA 
* #HAOUARIA "Haouaria"
* #HAOUARIA ^property[0].code = #parent 
* #HAOUARIA ^property[0].valueCode = #SALMONELLA 
* #HARBURG "Harburg"
* #HARBURG ^property[0].code = #parent 
* #HARBURG ^property[0].valueCode = #SALMONELLA 
* #HARCOURT "Harcourt"
* #HARCOURT ^property[0].code = #parent 
* #HARCOURT ^property[0].valueCode = #SALMONELLA 
* #HARLEYSTREET "Harleystreet"
* #HARLEYSTREET ^property[0].code = #parent 
* #HARLEYSTREET ^property[0].valueCode = #SALMONELLA 
* #HARRISONBURG "Harrisonburg"
* #HARRISONBURG ^property[0].code = #parent 
* #HARRISONBURG ^property[0].valueCode = #SALMONELLA 
* #HARTFORD "Hartford"
* #HARTFORD ^property[0].code = #parent 
* #HARTFORD ^property[0].valueCode = #SALMONELLA 
* #HARVESTEHUDE "Harvestehude"
* #HARVESTEHUDE ^property[0].code = #parent 
* #HARVESTEHUDE ^property[0].valueCode = #SALMONELLA 
* #HATFIELD "Hatfield"
* #HATFIELD ^property[0].code = #parent 
* #HATFIELD ^property[0].valueCode = #SALMONELLA 
* #HATO "Hato"
* #HATO ^property[0].code = #parent 
* #HATO ^property[0].valueCode = #SALMONELLA 
* #HAVANA "Havana"
* #HAVANA ^property[0].code = #parent 
* #HAVANA ^property[0].valueCode = #SALMONELLA 
* #HAYINDOGO "Hayindogo"
* #HAYINDOGO ^property[0].code = #parent 
* #HAYINDOGO ^property[0].valueCode = #SALMONELLA 
* #HEERLEN "Heerlen"
* #HEERLEN ^property[0].code = #parent 
* #HEERLEN ^property[0].valueCode = #SALMONELLA 
* #HEGAU "Hegau"
* #HEGAU ^property[0].code = #parent 
* #HEGAU ^property[0].valueCode = #SALMONELLA 
* #HEIDELBERG "Heidelberg"
* #HEIDELBERG ^property[0].code = #parent 
* #HEIDELBERG ^property[0].valueCode = #SALMONELLA 
* #HEISTOPDENBERG "Heistopdenberg"
* #HEISTOPDENBERG ^property[0].code = #parent 
* #HEISTOPDENBERG ^property[0].valueCode = #SALMONELLA 
* #HEMINGFORD "Hemingford"
* #HEMINGFORD ^property[0].code = #parent 
* #HEMINGFORD ^property[0].valueCode = #SALMONELLA 
* #HENNEKAMP "Hennekamp"
* #HENNEKAMP ^property[0].code = #parent 
* #HENNEKAMP ^property[0].valueCode = #SALMONELLA 
* #HERMANNSWERDER "Hermannswerder"
* #HERMANNSWERDER ^property[0].code = #parent 
* #HERMANNSWERDER ^property[0].valueCode = #SALMONELLA 
* #HERON "Heron"
* #HERON ^property[0].code = #parent 
* #HERON ^property[0].valueCode = #SALMONELLA 
* #HERSTON "Herston"
* #HERSTON ^property[0].code = #parent 
* #HERSTON ^property[0].valueCode = #SALMONELLA 
* #HERZLIYA "Herzliya"
* #HERZLIYA ^property[0].code = #parent 
* #HERZLIYA ^property[0].valueCode = #SALMONELLA 
* #HESSAREK "Hessarek"
* #HESSAREK ^property[0].code = #parent 
* #HESSAREK ^property[0].valueCode = #SALMONELLA 
* #HIDALGO "Hidalgo"
* #HIDALGO ^property[0].code = #parent 
* #HIDALGO ^property[0].valueCode = #SALMONELLA 
* #HIDUDDIFY "Hiduddify"
* #HIDUDDIFY ^property[0].code = #parent 
* #HIDUDDIFY ^property[0].valueCode = #SALMONELLA 
* #HILLEGERSBERG "Hillegersberg"
* #HILLEGERSBERG ^property[0].code = #parent 
* #HILLEGERSBERG ^property[0].valueCode = #SALMONELLA 
* #HILLINGDON "Hillingdon"
* #HILLINGDON ^property[0].code = #parent 
* #HILLINGDON ^property[0].valueCode = #SALMONELLA 
* #HILLSBOROUGH "Hillsborough"
* #HILLSBOROUGH ^property[0].code = #parent 
* #HILLSBOROUGH ^property[0].valueCode = #SALMONELLA 
* #HILVERSUM "Hilversum"
* #HILVERSUM ^property[0].code = #parent 
* #HILVERSUM ^property[0].valueCode = #SALMONELLA 
* #HINDMARSH "Hindmarsh"
* #HINDMARSH ^property[0].code = #parent 
* #HINDMARSH ^property[0].valueCode = #SALMONELLA 
* #HISINGEN "Hisingen"
* #HISINGEN ^property[0].code = #parent 
* #HISINGEN ^property[0].valueCode = #SALMONELLA 
* #HISSAR "Hissar"
* #HISSAR ^property[0].code = #parent 
* #HISSAR ^property[0].valueCode = #SALMONELLA 
* #HITHERGREEN "Hithergreen"
* #HITHERGREEN ^property[0].code = #parent 
* #HITHERGREEN ^property[0].valueCode = #SALMONELLA 
* #HOBOKEN "Hoboken"
* #HOBOKEN ^property[0].code = #parent 
* #HOBOKEN ^property[0].valueCode = #SALMONELLA 
* #HOFIT "Hofit"
* #HOFIT ^property[0].code = #parent 
* #HOFIT ^property[0].valueCode = #SALMONELLA 
* #HOGHTON "Hoghton"
* #HOGHTON ^property[0].code = #parent 
* #HOGHTON ^property[0].valueCode = #SALMONELLA 
* #HOHENTWIEL "Hohentwiel"
* #HOHENTWIEL ^property[0].code = #parent 
* #HOHENTWIEL ^property[0].valueCode = #SALMONELLA 
* #HOLCOMB "Holcomb"
* #HOLCOMB ^property[0].code = #parent 
* #HOLCOMB ^property[0].valueCode = #SALMONELLA 
* #HOMOSASSA "Homosassa"
* #HOMOSASSA ^property[0].code = #parent 
* #HOMOSASSA ^property[0].valueCode = #SALMONELLA 
* #HONELIS "Honelis"
* #HONELIS ^property[0].code = #parent 
* #HONELIS ^property[0].valueCode = #SALMONELLA 
* #HONGKONG "Hongkong"
* #HONGKONG ^property[0].code = #parent 
* #HONGKONG ^property[0].valueCode = #SALMONELLA 
* #HORSHAM "Horsham"
* #HORSHAM ^property[0].code = #parent 
* #HORSHAM ^property[0].valueCode = #SALMONELLA 
* #HOUSTON "Houston"
* #HOUSTON ^property[0].code = #parent 
* #HOUSTON ^property[0].valueCode = #SALMONELLA 
* #HUDDINGE "Huddinge"
* #HUDDINGE ^property[0].code = #parent 
* #HUDDINGE ^property[0].valueCode = #SALMONELLA 
* #HUETTWILEN "Huettwilen"
* #HUETTWILEN ^property[0].code = #parent 
* #HUETTWILEN ^property[0].valueCode = #SALMONELLA 
* #HULL "Hull"
* #HULL ^property[0].code = #parent 
* #HULL ^property[0].valueCode = #SALMONELLA 
* #HUVUDSTA "Huvudsta"
* #HUVUDSTA ^property[0].code = #parent 
* #HUVUDSTA ^property[0].valueCode = #SALMONELLA 
* #HVITTINGFOSS "Hvittingfoss"
* #HVITTINGFOSS ^property[0].code = #parent 
* #HVITTINGFOSS ^property[0].valueCode = #SALMONELLA 
* #HYDRA "Hydra"
* #HYDRA ^property[0].code = #parent 
* #HYDRA ^property[0].valueCode = #SALMONELLA 
* #IBADAN "Ibadan"
* #IBADAN ^property[0].code = #parent 
* #IBADAN ^property[0].valueCode = #SALMONELLA 
* #IBARAGI "Ibaragi"
* #IBARAGI ^property[0].code = #parent 
* #IBARAGI ^property[0].valueCode = #SALMONELLA 
* #IDIKAN "Idikan"
* #IDIKAN ^property[0].code = #parent 
* #IDIKAN ^property[0].valueCode = #SALMONELLA 
* #IKAYI "Ikayi"
* #IKAYI ^property[0].code = #parent 
* #IKAYI ^property[0].valueCode = #SALMONELLA 
* #IKEJA "Ikeja"
* #IKEJA ^property[0].code = #parent 
* #IKEJA ^property[0].valueCode = #SALMONELLA 
* #ILALA "Ilala"
* #ILALA ^property[0].code = #parent 
* #ILALA ^property[0].valueCode = #SALMONELLA 
* #ILUGUN "Ilugun"
* #ILUGUN ^property[0].code = #parent 
* #ILUGUN ^property[0].valueCode = #SALMONELLA 
* #IMO "Imo"
* #IMO ^property[0].code = #parent 
* #IMO ^property[0].valueCode = #SALMONELLA 
* #INCHPARK "Inchpark"
* #INCHPARK ^property[0].code = #parent 
* #INCHPARK ^property[0].valueCode = #SALMONELLA 
* #INDIA "India"
* #INDIA ^property[0].code = #parent 
* #INDIA ^property[0].valueCode = #SALMONELLA 
* #INDIANA "Indiana"
* #INDIANA ^property[0].code = #parent 
* #INDIANA ^property[0].valueCode = #SALMONELLA 
* #INFANTIS "Infantis"
* #INFANTIS ^property[0].code = #parent 
* #INFANTIS ^property[0].valueCode = #SALMONELLA 
* #INGANDA "Inganda"
* #INGANDA ^property[0].code = #parent 
* #INGANDA ^property[0].valueCode = #SALMONELLA 
* #INGLIS "Inglis"
* #INGLIS ^property[0].code = #parent 
* #INGLIS ^property[0].valueCode = #SALMONELLA 
* #INPRAW "Inpraw"
* #INPRAW ^property[0].code = #parent 
* #INPRAW ^property[0].valueCode = #SALMONELLA 
* #INVERNESS "Inverness"
* #INVERNESS ^property[0].code = #parent 
* #INVERNESS ^property[0].valueCode = #SALMONELLA 
* #IPEKO "Ipeko"
* #IPEKO ^property[0].code = #parent 
* #IPEKO ^property[0].valueCode = #SALMONELLA 
* #IPSWICH "Ipswich"
* #IPSWICH ^property[0].code = #parent 
* #IPSWICH ^property[0].valueCode = #SALMONELLA 
* #IRCHEL "Irchel"
* #IRCHEL ^property[0].code = #parent 
* #IRCHEL ^property[0].valueCode = #SALMONELLA 
* #IRENEA "Irenea"
* #IRENEA ^property[0].code = #parent 
* #IRENEA ^property[0].valueCode = #SALMONELLA 
* #IRIGNY "Irigny"
* #IRIGNY ^property[0].code = #parent 
* #IRIGNY ^property[0].valueCode = #SALMONELLA 
* #IRUMU "Irumu"
* #IRUMU ^property[0].code = #parent 
* #IRUMU ^property[0].valueCode = #SALMONELLA 
* #ISANGI "Isangi"
* #ISANGI ^property[0].code = #parent 
* #ISANGI ^property[0].valueCode = #SALMONELLA 
* #ISASZEG "Isaszeg"
* #ISASZEG ^property[0].code = #parent 
* #ISASZEG ^property[0].valueCode = #SALMONELLA 
* #ISRAEL "Israel"
* #ISRAEL ^property[0].code = #parent 
* #ISRAEL ^property[0].valueCode = #SALMONELLA 
* #ISTANBUL "Istanbul"
* #ISTANBUL ^property[0].code = #parent 
* #ISTANBUL ^property[0].valueCode = #SALMONELLA 
* #ISTORIA "Istoria"
* #ISTORIA ^property[0].code = #parent 
* #ISTORIA ^property[0].valueCode = #SALMONELLA 
* #ISUGE "Isuge"
* #ISUGE ^property[0].code = #parent 
* #ISUGE ^property[0].valueCode = #SALMONELLA 
* #ITAMI "Itami"
* #ITAMI ^property[0].code = #parent 
* #ITAMI ^property[0].valueCode = #SALMONELLA 
* #ITURI "Ituri"
* #ITURI ^property[0].code = #parent 
* #ITURI ^property[0].valueCode = #SALMONELLA 
* #ITUTABA "Itutaba"
* #ITUTABA ^property[0].code = #parent 
* #ITUTABA ^property[0].valueCode = #SALMONELLA 
* #IVORY "Ivory"
* #IVORY ^property[0].code = #parent 
* #IVORY ^property[0].valueCode = #SALMONELLA 
* #IVORYCOAST "Ivorycoast"
* #IVORYCOAST ^property[0].code = #parent 
* #IVORYCOAST ^property[0].valueCode = #SALMONELLA 
* #IVRYSURSEINE "Ivrysurseine"
* #IVRYSURSEINE ^property[0].code = #parent 
* #IVRYSURSEINE ^property[0].valueCode = #SALMONELLA 
* #JAFFNA "Jaffna"
* #JAFFNA ^property[0].code = #parent 
* #JAFFNA ^property[0].valueCode = #SALMONELLA 
* #JALISCO "Jalisco"
* #JALISCO ^property[0].code = #parent 
* #JALISCO ^property[0].valueCode = #SALMONELLA 
* #JAMAICA "Jamaica"
* #JAMAICA ^property[0].code = #parent 
* #JAMAICA ^property[0].valueCode = #SALMONELLA 
* #JAMBUR "Jambur"
* #JAMBUR ^property[0].code = #parent 
* #JAMBUR ^property[0].valueCode = #SALMONELLA 
* #JANGWANI "Jangwani"
* #JANGWANI ^property[0].code = #parent 
* #JANGWANI ^property[0].valueCode = #SALMONELLA 
* #JAVA "Java"
* #JAVA ^property[0].code = #parent 
* #JAVA ^property[0].valueCode = #SALMONELLA 
* #JAVIANA "Javiana"
* #JAVIANA ^property[0].code = #parent 
* #JAVIANA ^property[0].valueCode = #SALMONELLA 
* #JEDBURGH "Jedburgh"
* #JEDBURGH ^property[0].code = #parent 
* #JEDBURGH ^property[0].valueCode = #SALMONELLA 
* #JERICHO "Jericho"
* #JERICHO ^property[0].code = #parent 
* #JERICHO ^property[0].valueCode = #SALMONELLA 
* #JERUSALEM "Jerusalem"
* #JERUSALEM ^property[0].code = #parent 
* #JERUSALEM ^property[0].valueCode = #SALMONELLA 
* #JOAL "Joal"
* #JOAL ^property[0].code = #parent 
* #JOAL ^property[0].valueCode = #SALMONELLA 
* #JODHPUR "Jodhpur"
* #JODHPUR ^property[0].code = #parent 
* #JODHPUR ^property[0].valueCode = #SALMONELLA 
* #JOHANNESBURG "Johannesburg"
* #JOHANNESBURG ^property[0].code = #parent 
* #JOHANNESBURG ^property[0].valueCode = #SALMONELLA 
* #JOS "Jos"
* #JOS ^property[0].code = #parent 
* #JOS ^property[0].valueCode = #SALMONELLA 
* #JUBA "Juba"
* #JUBA ^property[0].code = #parent 
* #JUBA ^property[0].valueCode = #SALMONELLA 
* #JUBILEE "Jubilee"
* #JUBILEE ^property[0].code = #parent 
* #JUBILEE ^property[0].valueCode = #SALMONELLA 
* #JUKESTOWN "Jukestown"
* #JUKESTOWN ^property[0].code = #parent 
* #JUKESTOWN ^property[0].valueCode = #SALMONELLA 
* #KAAPSTAD "Kaapstad"
* #KAAPSTAD ^property[0].code = #parent 
* #KAAPSTAD ^property[0].valueCode = #SALMONELLA 
* #KABETE "Kabete"
* #KABETE ^property[0].code = #parent 
* #KABETE ^property[0].valueCode = #SALMONELLA 
* #KADUNA "Kaduna"
* #KADUNA ^property[0].code = #parent 
* #KADUNA ^property[0].valueCode = #SALMONELLA 
* #KAEVLINGE "Kaevlinge"
* #KAEVLINGE ^property[0].code = #parent 
* #KAEVLINGE ^property[0].valueCode = #SALMONELLA 
* #KAHLA "Kahla"
* #KAHLA ^property[0].code = #parent 
* #KAHLA ^property[0].valueCode = #SALMONELLA 
* #KAINJI "Kainji"
* #KAINJI ^property[0].code = #parent 
* #KAINJI ^property[0].valueCode = #SALMONELLA 
* #KAITAAN "Kaitaan"
* #KAITAAN ^property[0].code = #parent 
* #KAITAAN ^property[0].valueCode = #SALMONELLA 
* #KALAMU "Kalamu"
* #KALAMU ^property[0].code = #parent 
* #KALAMU ^property[0].valueCode = #SALMONELLA 
* #KALINA "Kalina"
* #KALINA ^property[0].code = #parent 
* #KALINA ^property[0].valueCode = #SALMONELLA 
* #KALLO "Kallo"
* #KALLO ^property[0].code = #parent 
* #KALLO ^property[0].valueCode = #SALMONELLA 
* #KALUMBURU "Kalumburu"
* #KALUMBURU ^property[0].code = #parent 
* #KALUMBURU ^property[0].valueCode = #SALMONELLA 
* #KAMBOLE "Kambole"
* #KAMBOLE ^property[0].code = #parent 
* #KAMBOLE ^property[0].valueCode = #SALMONELLA 
* #KAMORU "Kamoru"
* #KAMORU ^property[0].code = #parent 
* #KAMORU ^property[0].valueCode = #SALMONELLA 
* #KAMPALA "Kampala"
* #KAMPALA ^property[0].code = #parent 
* #KAMPALA ^property[0].valueCode = #SALMONELLA 
* #KANDE "Kande"
* #KANDE ^property[0].code = #parent 
* #KANDE ^property[0].valueCode = #SALMONELLA 
* #KANDLA "Kandla"
* #KANDLA ^property[0].code = #parent 
* #KANDLA ^property[0].valueCode = #SALMONELLA 
* #KANESHIE "Kaneshie"
* #KANESHIE ^property[0].code = #parent 
* #KANESHIE ^property[0].valueCode = #SALMONELLA 
* #KANIFING "Kanifing"
* #KANIFING ^property[0].code = #parent 
* #KANIFING ^property[0].valueCode = #SALMONELLA 
* #KANO "Kano"
* #KANO ^property[0].code = #parent 
* #KANO ^property[0].valueCode = #SALMONELLA 
* #KAOLACK "Kaolack"
* #KAOLACK ^property[0].code = #parent 
* #KAOLACK ^property[0].valueCode = #SALMONELLA 
* #KAPEMBA "Kapemba"
* #KAPEMBA ^property[0].code = #parent 
* #KAPEMBA ^property[0].valueCode = #SALMONELLA 
* #KARACHI "Karachi"
* #KARACHI ^property[0].code = #parent 
* #KARACHI ^property[0].valueCode = #SALMONELLA 
* #KARAMOJA "Karamoja"
* #KARAMOJA ^property[0].code = #parent 
* #KARAMOJA ^property[0].valueCode = #SALMONELLA 
* #KARAYA "Karaya"
* #KARAYA ^property[0].code = #parent 
* #KARAYA ^property[0].valueCode = #SALMONELLA 
* #KARLSHAMN "Karlshamn"
* #KARLSHAMN ^property[0].code = #parent 
* #KARLSHAMN ^property[0].valueCode = #SALMONELLA 
* #KASENYI "Kasenyi"
* #KASENYI ^property[0].code = #parent 
* #KASENYI ^property[0].valueCode = #SALMONELLA 
* #KASSBERG "Kassberg"
* #KASSBERG ^property[0].code = #parent 
* #KASSBERG ^property[0].valueCode = #SALMONELLA 
* #KASSEL "Kassel"
* #KASSEL ^property[0].code = #parent 
* #KASSEL ^property[0].valueCode = #SALMONELLA 
* #KASTRUP "Kastrup"
* #KASTRUP ^property[0].code = #parent 
* #KASTRUP ^property[0].valueCode = #SALMONELLA 
* #KEDOUGOU "Kedougou"
* #KEDOUGOU ^property[0].code = #parent 
* #KEDOUGOU ^property[0].valueCode = #SALMONELLA 
* #KENTUCKY "Kentucky"
* #KENTUCKY ^property[0].code = #parent 
* #KENTUCKY ^property[0].valueCode = #SALMONELLA 
* #KENYA "Kenya"
* #KENYA ^property[0].code = #parent 
* #KENYA ^property[0].valueCode = #SALMONELLA 
* #KERMEL "Kermel"
* #KERMEL ^property[0].code = #parent 
* #KERMEL ^property[0].valueCode = #SALMONELLA 
* #KETHIABARNY "Kethiabarny"
* #KETHIABARNY ^property[0].code = #parent 
* #KETHIABARNY ^property[0].valueCode = #SALMONELLA 
* #KEURMASSAR "Keurmassar"
* #KEURMASSAR ^property[0].code = #parent 
* #KEURMASSAR ^property[0].valueCode = #SALMONELLA 
* #KEVE "Keve"
* #KEVE ^property[0].code = #parent 
* #KEVE ^property[0].valueCode = #SALMONELLA 
* #KIAMBU "Kiambu"
* #KIAMBU ^property[0].code = #parent 
* #KIAMBU ^property[0].valueCode = #SALMONELLA 
* #KIBI "Kibi"
* #KIBI ^property[0].code = #parent 
* #KIBI ^property[0].valueCode = #SALMONELLA 
* #KIBUSI "Kibusi"
* #KIBUSI ^property[0].code = #parent 
* #KIBUSI ^property[0].valueCode = #SALMONELLA 
* #KIDDERMINSTER "Kidderminster"
* #KIDDERMINSTER ^property[0].code = #parent 
* #KIDDERMINSTER ^property[0].valueCode = #SALMONELLA 
* #KIEL "Kiel"
* #KIEL ^property[0].code = #parent 
* #KIEL ^property[0].valueCode = #SALMONELLA 
* #KIKOMA "Kikoma"
* #KIKOMA ^property[0].code = #parent 
* #KIKOMA ^property[0].valueCode = #SALMONELLA 
* #KIMBERLEY "Kimberley"
* #KIMBERLEY ^property[0].code = #parent 
* #KIMBERLEY ^property[0].valueCode = #SALMONELLA 
* #KIMPESE "Kimpese"
* #KIMPESE ^property[0].code = #parent 
* #KIMPESE ^property[0].valueCode = #SALMONELLA 
* #KIMUENZA "Kimuenza"
* #KIMUENZA ^property[0].code = #parent 
* #KIMUENZA ^property[0].valueCode = #SALMONELLA 
* #KINDIA "Kindia"
* #KINDIA ^property[0].code = #parent 
* #KINDIA ^property[0].valueCode = #SALMONELLA 
* #KINGABWA "Kingabwa"
* #KINGABWA ^property[0].code = #parent 
* #KINGABWA ^property[0].valueCode = #SALMONELLA 
* #KINGSTON "Kingston"
* #KINGSTON ^property[0].code = #parent 
* #KINGSTON ^property[0].valueCode = #SALMONELLA 
* #KINONDONI "Kinondoni"
* #KINONDONI ^property[0].code = #parent 
* #KINONDONI ^property[0].valueCode = #SALMONELLA 
* #KINSON "Kinson"
* #KINSON ^property[0].code = #parent 
* #KINSON ^property[0].valueCode = #SALMONELLA 
* #KINTAMBO "Kintambo"
* #KINTAMBO ^property[0].code = #parent 
* #KINTAMBO ^property[0].valueCode = #SALMONELLA 
* #KIRKEE "Kirkee"
* #KIRKEE ^property[0].code = #parent 
* #KIRKEE ^property[0].valueCode = #SALMONELLA 
* #KISANGANI "Kisangani"
* #KISANGANI ^property[0].code = #parent 
* #KISANGANI ^property[0].valueCode = #SALMONELLA 
* #KISARAWE "Kisarawe"
* #KISARAWE ^property[0].code = #parent 
* #KISARAWE ^property[0].valueCode = #SALMONELLA 
* #KISII "Kisii"
* #KISII ^property[0].code = #parent 
* #KISII ^property[0].valueCode = #SALMONELLA 
* #KITENGE "Kitenge"
* #KITENGE ^property[0].code = #parent 
* #KITENGE ^property[0].valueCode = #SALMONELLA 
* #KIVU "Kivu"
* #KIVU ^property[0].code = #parent 
* #KIVU ^property[0].valueCode = #SALMONELLA 
* #KLOUTO "Klouto"
* #KLOUTO ^property[0].code = #parent 
* #KLOUTO ^property[0].valueCode = #SALMONELLA 
* #KOBLENZ "Koblenz"
* #KOBLENZ ^property[0].code = #parent 
* #KOBLENZ ^property[0].valueCode = #SALMONELLA 
* #KODJOVI "Kodjovi"
* #KODJOVI ^property[0].code = #parent 
* #KODJOVI ^property[0].valueCode = #SALMONELLA 
* #KOENIGSTUHL "Koenigstuhl"
* #KOENIGSTUHL ^property[0].code = #parent 
* #KOENIGSTUHL ^property[0].valueCode = #SALMONELLA 
* #KOESSEN "Koessen"
* #KOESSEN ^property[0].code = #parent 
* #KOESSEN ^property[0].valueCode = #SALMONELLA 
* #KOFANDOKA "Kofandoka"
* #KOFANDOKA ^property[0].code = #parent 
* #KOFANDOKA ^property[0].valueCode = #SALMONELLA 
* #KOKETIME "Koketime"
* #KOKETIME ^property[0].code = #parent 
* #KOKETIME ^property[0].valueCode = #SALMONELLA 
* #KOKOLI "Kokoli"
* #KOKOLI ^property[0].code = #parent 
* #KOKOLI ^property[0].valueCode = #SALMONELLA 
* #KOKOMLEMLE "Kokomlemle"
* #KOKOMLEMLE ^property[0].code = #parent 
* #KOKOMLEMLE ^property[0].valueCode = #SALMONELLA 
* #KOLAR "Kolar"
* #KOLAR ^property[0].code = #parent 
* #KOLAR ^property[0].valueCode = #SALMONELLA 
* #KOLDA "Kolda"
* #KOLDA ^property[0].code = #parent 
* #KOLDA ^property[0].valueCode = #SALMONELLA 
* #KONOLFINGEN "Konolfingen"
* #KONOLFINGEN ^property[0].code = #parent 
* #KONOLFINGEN ^property[0].valueCode = #SALMONELLA 
* #KONONGO "Konongo"
* #KONONGO ^property[0].code = #parent 
* #KONONGO ^property[0].valueCode = #SALMONELLA 
* #KONSTANZ "Konstanz"
* #KONSTANZ ^property[0].code = #parent 
* #KONSTANZ ^property[0].valueCode = #SALMONELLA 
* #KORBOL "Korbol"
* #KORBOL ^property[0].code = #parent 
* #KORBOL ^property[0].valueCode = #SALMONELLA 
* #KORKEASAARI "Korkeasaari"
* #KORKEASAARI ^property[0].code = #parent 
* #KORKEASAARI ^property[0].valueCode = #SALMONELLA 
* #KORLEBU "Korlebu"
* #KORLEBU ^property[0].code = #parent 
* #KORLEBU ^property[0].valueCode = #SALMONELLA 
* #KOROVI "Korovi"
* #KOROVI ^property[0].code = #parent 
* #KOROVI ^property[0].valueCode = #SALMONELLA 
* #KORTRIJK "Kortrijk"
* #KORTRIJK ^property[0].code = #parent 
* #KORTRIJK ^property[0].valueCode = #SALMONELLA 
* #KOTTBUS "Kottbus"
* #KOTTBUS ^property[0].code = #parent 
* #KOTTBUS ^property[0].valueCode = #SALMONELLA 
* #KOTTE "Kotte"
* #KOTTE ^property[0].code = #parent 
* #KOTTE ^property[0].valueCode = #SALMONELLA 
* #KOTU "Kotu"
* #KOTU ^property[0].code = #parent 
* #KOTU ^property[0].valueCode = #SALMONELLA 
* #KOUKA "Kouka"
* #KOUKA ^property[0].code = #parent 
* #KOUKA ^property[0].valueCode = #SALMONELLA 
* #KOUMRA "Koumra"
* #KOUMRA ^property[0].code = #parent 
* #KOUMRA ^property[0].valueCode = #SALMONELLA 
* #KPEME "Kpeme"
* #KPEME ^property[0].code = #parent 
* #KPEME ^property[0].valueCode = #SALMONELLA 
* #KRALINGEN "Kralingen"
* #KRALINGEN ^property[0].code = #parent 
* #KRALINGEN ^property[0].valueCode = #SALMONELLA 
* #KREFELD "Krefeld"
* #KREFELD ^property[0].code = #parent 
* #KREFELD ^property[0].valueCode = #SALMONELLA 
* #KRISTIANSTAD "Kristianstad"
* #KRISTIANSTAD ^property[0].code = #parent 
* #KRISTIANSTAD ^property[0].valueCode = #SALMONELLA 
* #KUA "Kua"
* #KUA ^property[0].code = #parent 
* #KUA ^property[0].valueCode = #SALMONELLA 
* #KUBACHA "Kubacha"
* #KUBACHA ^property[0].code = #parent 
* #KUBACHA ^property[0].valueCode = #SALMONELLA 
* #KUESSEL "Kuessel"
* #KUESSEL ^property[0].code = #parent 
* #KUESSEL ^property[0].valueCode = #SALMONELLA 
* #KUMASI "Kumasi"
* #KUMASI ^property[0].code = #parent 
* #KUMASI ^property[0].valueCode = #SALMONELLA 
* #KUNDUCHI "Kunduchi"
* #KUNDUCHI ^property[0].code = #parent 
* #KUNDUCHI ^property[0].valueCode = #SALMONELLA 
* #KUNTAIR "Kuntair"
* #KUNTAIR ^property[0].code = #parent 
* #KUNTAIR ^property[0].valueCode = #SALMONELLA 
* #KURU "Kuru"
* #KURU ^property[0].code = #parent 
* #KURU ^property[0].valueCode = #SALMONELLA 
* #LABADI "Labadi"
* #LABADI ^property[0].code = #parent 
* #LABADI ^property[0].valueCode = #SALMONELLA 
* #LAGOS "Lagos"
* #LAGOS ^property[0].code = #parent 
* #LAGOS ^property[0].valueCode = #SALMONELLA 
* #LAMBERHURST "Lamberhurst"
* #LAMBERHURST ^property[0].code = #parent 
* #LAMBERHURST ^property[0].valueCode = #SALMONELLA 
* #LAMIN "Lamin"
* #LAMIN ^property[0].code = #parent 
* #LAMIN ^property[0].valueCode = #SALMONELLA 
* #LAMPHUN "Lamphun"
* #LAMPHUN ^property[0].code = #parent 
* #LAMPHUN ^property[0].valueCode = #SALMONELLA 
* #LANCASTER "Lancaster"
* #LANCASTER ^property[0].code = #parent 
* #LANCASTER ^property[0].valueCode = #SALMONELLA 
* #LANDALA "Landala"
* #LANDALA ^property[0].code = #parent 
* #LANDALA ^property[0].valueCode = #SALMONELLA 
* #LANDAU "Landau"
* #LANDAU ^property[0].code = #parent 
* #LANDAU ^property[0].valueCode = #SALMONELLA 
* #LANDWASSER "Landwasser"
* #LANDWASSER ^property[0].code = #parent 
* #LANDWASSER ^property[0].valueCode = #SALMONELLA 
* #LANGENHORN "Langenhorn"
* #LANGENHORN ^property[0].code = #parent 
* #LANGENHORN ^property[0].valueCode = #SALMONELLA 
* #LANGENSALZA "Langensalza"
* #LANGENSALZA ^property[0].code = #parent 
* #LANGENSALZA ^property[0].valueCode = #SALMONELLA 
* #LANGEVELD "Langeveld"
* #LANGEVELD ^property[0].code = #parent 
* #LANGEVELD ^property[0].valueCode = #SALMONELLA 
* #LANGFORD "Langford"
* #LANGFORD ^property[0].code = #parent 
* #LANGFORD ^property[0].valueCode = #SALMONELLA 
* #LANSING "Lansing"
* #LANSING ^property[0].code = #parent 
* #LANSING ^property[0].valueCode = #SALMONELLA 
* #LAREDO "Laredo"
* #LAREDO ^property[0].code = #parent 
* #LAREDO ^property[0].valueCode = #SALMONELLA 
* #LAROCHELLE "Larochelle"
* #LAROCHELLE ^property[0].code = #parent 
* #LAROCHELLE ^property[0].valueCode = #SALMONELLA 
* #LAROSE "Larose"
* #LAROSE ^property[0].code = #parent 
* #LAROSE ^property[0].valueCode = #SALMONELLA 
* #LATTENKAMP "Lattenkamp"
* #LATTENKAMP ^property[0].code = #parent 
* #LATTENKAMP ^property[0].valueCode = #SALMONELLA 
* #LAWNDALE "Lawndale"
* #LAWNDALE ^property[0].code = #parent 
* #LAWNDALE ^property[0].valueCode = #SALMONELLA 
* #LAWRA "Lawra"
* #LAWRA ^property[0].code = #parent 
* #LAWRA ^property[0].valueCode = #SALMONELLA 
* #LEATHERHEAD "Leatherhead"
* #LEATHERHEAD ^property[0].code = #parent 
* #LEATHERHEAD ^property[0].valueCode = #SALMONELLA 
* #LECHLER "Lechler"
* #LECHLER ^property[0].code = #parent 
* #LECHLER ^property[0].valueCode = #SALMONELLA 
* #LEDA "Leda"
* #LEDA ^property[0].code = #parent 
* #LEDA ^property[0].valueCode = #SALMONELLA 
* #LEER "Leer"
* #LEER ^property[0].code = #parent 
* #LEER ^property[0].valueCode = #SALMONELLA 
* #LEEUWARDEN "Leeuwarden"
* #LEEUWARDEN ^property[0].code = #parent 
* #LEEUWARDEN ^property[0].valueCode = #SALMONELLA 
* #LEGON "Legon"
* #LEGON ^property[0].code = #parent 
* #LEGON ^property[0].valueCode = #SALMONELLA 
* #LEHRTE "Lehrte"
* #LEHRTE ^property[0].code = #parent 
* #LEHRTE ^property[0].valueCode = #SALMONELLA 
* #LEIDEN "Leiden"
* #LEIDEN ^property[0].code = #parent 
* #LEIDEN ^property[0].valueCode = #SALMONELLA 
* #LEIPZIG "Leipzig"
* #LEIPZIG ^property[0].code = #parent 
* #LEIPZIG ^property[0].valueCode = #SALMONELLA 
* #LEITH "Leith"
* #LEITH ^property[0].code = #parent 
* #LEITH ^property[0].valueCode = #SALMONELLA 
* #LEKKE "Lekke"
* #LEKKE ^property[0].code = #parent 
* #LEKKE ^property[0].valueCode = #SALMONELLA 
* #LENE "Lene"
* #LENE ^property[0].code = #parent 
* #LENE ^property[0].valueCode = #SALMONELLA 
* #LEOBEN "Leoben"
* #LEOBEN ^property[0].code = #parent 
* #LEOBEN ^property[0].valueCode = #SALMONELLA 
* #LEOPOLDVILLE "Leopoldville"
* #LEOPOLDVILLE ^property[0].code = #parent 
* #LEOPOLDVILLE ^property[0].valueCode = #SALMONELLA 
* #LERUM "Lerum"
* #LERUM ^property[0].code = #parent 
* #LERUM ^property[0].valueCode = #SALMONELLA 
* #LEXINGTON "Lexington"
* #LEXINGTON ^property[0].code = #parent 
* #LEXINGTON ^property[0].valueCode = #SALMONELLA 
* #LEXINGTON_VAR_15+ "Lexington var. 15+"
* #LEXINGTON_VAR_15+ ^property[0].code = #parent 
* #LEXINGTON_VAR_15+ ^property[0].valueCode = #SALMONELLA 
* #LEXINGTON_VAR_15+_34+ "Lexington var. 15+, 34+"
* #LEXINGTON_VAR_15+_34+ ^property[0].code = #parent 
* #LEXINGTON_VAR_15+_34+ ^property[0].valueCode = #SALMONELLA 
* #LEZENNES "Lezennes"
* #LEZENNES ^property[0].code = #parent 
* #LEZENNES ^property[0].valueCode = #SALMONELLA 
* #LIBREVILLE "Libreville"
* #LIBREVILLE ^property[0].code = #parent 
* #LIBREVILLE ^property[0].valueCode = #SALMONELLA 
* #LIGEO "Ligeo"
* #LIGEO ^property[0].code = #parent 
* #LIGEO ^property[0].valueCode = #SALMONELLA 
* #LIGNA "Ligna"
* #LIGNA ^property[0].code = #parent 
* #LIGNA ^property[0].valueCode = #SALMONELLA 
* #LIKA "Lika"
* #LIKA ^property[0].code = #parent 
* #LIKA ^property[0].valueCode = #SALMONELLA 
* #LILLE "Lille"
* #LILLE ^property[0].code = #parent 
* #LILLE ^property[0].valueCode = #SALMONELLA 
* #LILLE_VAR_14+ "Lille var. 14+"
* #LILLE_VAR_14+ ^property[0].code = #parent 
* #LILLE_VAR_14+ ^property[0].valueCode = #SALMONELLA 
* #LIMETE "Limete"
* #LIMETE ^property[0].code = #parent 
* #LIMETE ^property[0].valueCode = #SALMONELLA 
* #LINDENBURG "Lindenburg"
* #LINDENBURG ^property[0].code = #parent 
* #LINDENBURG ^property[0].valueCode = #SALMONELLA 
* #LINDERN "Lindern"
* #LINDERN ^property[0].code = #parent 
* #LINDERN ^property[0].valueCode = #SALMONELLA 
* #LINDI "Lindi"
* #LINDI ^property[0].code = #parent 
* #LINDI ^property[0].valueCode = #SALMONELLA 
* #LINGUERE "Linguere"
* #LINGUERE ^property[0].code = #parent 
* #LINGUERE ^property[0].valueCode = #SALMONELLA 
* #LINGWALA "Lingwala"
* #LINGWALA ^property[0].code = #parent 
* #LINGWALA ^property[0].valueCode = #SALMONELLA 
* #LINTON "Linton"
* #LINTON ^property[0].code = #parent 
* #LINTON ^property[0].valueCode = #SALMONELLA 
* #LISBOA "Lisboa"
* #LISBOA ^property[0].code = #parent 
* #LISBOA ^property[0].valueCode = #SALMONELLA 
* #LISHABI "Lishabi"
* #LISHABI ^property[0].code = #parent 
* #LISHABI ^property[0].valueCode = #SALMONELLA 
* #LITCHFIELD "Litchfield"
* #LITCHFIELD ^property[0].code = #parent 
* #LITCHFIELD ^property[0].valueCode = #SALMONELLA 
* #LIVERPOOL "Liverpool"
* #LIVERPOOL ^property[0].code = #parent 
* #LIVERPOOL ^property[0].valueCode = #SALMONELLA 
* #LIVINGSTONE "Livingstone"
* #LIVINGSTONE ^property[0].code = #parent 
* #LIVINGSTONE ^property[0].valueCode = #SALMONELLA 
* #LIVINGSTONE_VAR_14+ "Livingstone var. 14+"
* #LIVINGSTONE_VAR_14+ ^property[0].code = #parent 
* #LIVINGSTONE_VAR_14+ ^property[0].valueCode = #SALMONELLA 
* #LIVULU "Livulu"
* #LIVULU ^property[0].code = #parent 
* #LIVULU ^property[0].valueCode = #SALMONELLA 
* #LJUBLJANA "Ljubljana"
* #LJUBLJANA ^property[0].code = #parent 
* #LJUBLJANA ^property[0].valueCode = #SALMONELLA 
* #LLANDOFF "Llandoff"
* #LLANDOFF ^property[0].code = #parent 
* #LLANDOFF ^property[0].valueCode = #SALMONELLA 
* #LLOBREGAT "Llobregat"
* #LLOBREGAT ^property[0].code = #parent 
* #LLOBREGAT ^property[0].valueCode = #SALMONELLA 
* #LOANDA "Loanda"
* #LOANDA ^property[0].code = #parent 
* #LOANDA ^property[0].valueCode = #SALMONELLA 
* #LOCKLEAZE "Lockleaze"
* #LOCKLEAZE ^property[0].code = #parent 
* #LOCKLEAZE ^property[0].valueCode = #SALMONELLA 
* #LODE "Lode"
* #LODE ^property[0].code = #parent 
* #LODE ^property[0].valueCode = #SALMONELLA 
* #LODZ "Lodz"
* #LODZ ^property[0].code = #parent 
* #LODZ ^property[0].valueCode = #SALMONELLA 
* #LOENGA "Loenga"
* #LOENGA ^property[0].code = #parent 
* #LOENGA ^property[0].valueCode = #SALMONELLA 
* #LOGONE "Logone"
* #LOGONE ^property[0].code = #parent 
* #LOGONE ^property[0].valueCode = #SALMONELLA 
* #LOKOMO "Lokomo"
* #LOKOMO ^property[0].code = #parent 
* #LOKOMO ^property[0].valueCode = #SALMONELLA 
* #LOKSTEDT "Lokstedt"
* #LOKSTEDT ^property[0].code = #parent 
* #LOKSTEDT ^property[0].valueCode = #SALMONELLA 
* #LOMALINDA "Lomalinda"
* #LOMALINDA ^property[0].code = #parent 
* #LOMALINDA ^property[0].valueCode = #SALMONELLA 
* #LOME "Lome"
* #LOME ^property[0].code = #parent 
* #LOME ^property[0].valueCode = #SALMONELLA 
* #LOMITA "Lomita"
* #LOMITA ^property[0].code = #parent 
* #LOMITA ^property[0].valueCode = #SALMONELLA 
* #LOMNAVA "Lomnava"
* #LOMNAVA ^property[0].code = #parent 
* #LOMNAVA ^property[0].valueCode = #SALMONELLA 
* #LONDON "London"
* #LONDON ^property[0].code = #parent 
* #LONDON ^property[0].valueCode = #SALMONELLA 
* #LONDON_VAR_15+_ "London var. 15+"
* #LONDON_VAR_15+_ ^property[0].code = #parent 
* #LONDON_VAR_15+_ ^property[0].valueCode = #SALMONELLA 
* #LONESTAR "Lonestar"
* #LONESTAR ^property[0].code = #parent 
* #LONESTAR ^property[0].valueCode = #SALMONELLA 
* #LOSANGELES "Losangeles"
* #LOSANGELES ^property[0].code = #parent 
* #LOSANGELES ^property[0].valueCode = #SALMONELLA 
* #LOUBOMO "Loubomo"
* #LOUBOMO ^property[0].code = #parent 
* #LOUBOMO ^property[0].valueCode = #SALMONELLA 
* #LOUGA "Louga"
* #LOUGA ^property[0].code = #parent 
* #LOUGA ^property[0].valueCode = #SALMONELLA 
* #LOUISIANA "Louisiana"
* #LOUISIANA ^property[0].code = #parent 
* #LOUISIANA ^property[0].valueCode = #SALMONELLA 
* #LOVELACE "Lovelace"
* #LOVELACE ^property[0].code = #parent 
* #LOVELACE ^property[0].valueCode = #SALMONELLA 
* #LOWESTOFT "Lowestoft"
* #LOWESTOFT ^property[0].code = #parent 
* #LOWESTOFT ^property[0].valueCode = #SALMONELLA 
* #LUBUMBASHI "Lubumbashi"
* #LUBUMBASHI ^property[0].code = #parent 
* #LUBUMBASHI ^property[0].valueCode = #SALMONELLA 
* #LUCIANA "Luciana"
* #LUCIANA ^property[0].code = #parent 
* #LUCIANA ^property[0].valueCode = #SALMONELLA 
* #LUCKENWALDE "Luckenwalde"
* #LUCKENWALDE ^property[0].code = #parent 
* #LUCKENWALDE ^property[0].valueCode = #SALMONELLA 
* #LUEDINGHAUSEN "Luedinghausen"
* #LUEDINGHAUSEN ^property[0].code = #parent 
* #LUEDINGHAUSEN ^property[0].valueCode = #SALMONELLA 
* #LUKE "Luke"
* #LUKE ^property[0].code = #parent 
* #LUKE ^property[0].valueCode = #SALMONELLA 
* #LUND "Lund"
* #LUND ^property[0].code = #parent 
* #LUND ^property[0].valueCode = #SALMONELLA 
* #LUTETIA "Lutetia"
* #LUTETIA ^property[0].code = #parent 
* #LUTETIA ^property[0].valueCode = #SALMONELLA 
* #LYON "Lyon"
* #LYON ^property[0].code = #parent 
* #LYON ^property[0].valueCode = #SALMONELLA 
* #MAASTRICHT "Maastricht"
* #MAASTRICHT ^property[0].code = #parent 
* #MAASTRICHT ^property[0].valueCode = #SALMONELLA 
* #MACALLEN "Macallen"
* #MACALLEN ^property[0].code = #parent 
* #MACALLEN ^property[0].valueCode = #SALMONELLA 
* #MACCLESFIELD "Macclesfield"
* #MACCLESFIELD ^property[0].code = #parent 
* #MACCLESFIELD ^property[0].valueCode = #SALMONELLA 
* #MACHAGA "Machaga"
* #MACHAGA ^property[0].code = #parent 
* #MACHAGA ^property[0].valueCode = #SALMONELLA 
* #MADELIA "Madelia"
* #MADELIA ^property[0].code = #parent 
* #MADELIA ^property[0].valueCode = #SALMONELLA 
* #MADIAGO "Madiago"
* #MADIAGO ^property[0].code = #parent 
* #MADIAGO ^property[0].valueCode = #SALMONELLA 
* #MADIGAN "Madigan"
* #MADIGAN ^property[0].code = #parent 
* #MADIGAN ^property[0].valueCode = #SALMONELLA 
* #MADISON "Madison"
* #MADISON ^property[0].code = #parent 
* #MADISON ^property[0].valueCode = #SALMONELLA 
* #MADJORIO "Madjorio"
* #MADJORIO ^property[0].code = #parent 
* #MADJORIO ^property[0].valueCode = #SALMONELLA 
* #MADRAS "Madras"
* #MADRAS ^property[0].code = #parent 
* #MADRAS ^property[0].valueCode = #SALMONELLA 
* #MAGHERAFELT "Magherafelt"
* #MAGHERAFELT ^property[0].code = #parent 
* #MAGHERAFELT ^property[0].valueCode = #SALMONELLA 
* #MAGUMERI "Magumeri"
* #MAGUMERI ^property[0].code = #parent 
* #MAGUMERI ^property[0].valueCode = #SALMONELLA 
* #MAGWA "Magwa"
* #MAGWA ^property[0].code = #parent 
* #MAGWA ^property[0].valueCode = #SALMONELLA 
* #MAHINA "Mahina"
* #MAHINA ^property[0].code = #parent 
* #MAHINA ^property[0].valueCode = #SALMONELLA 
* #MAIDUGURI "Maiduguri"
* #MAIDUGURI ^property[0].code = #parent 
* #MAIDUGURI ^property[0].valueCode = #SALMONELLA 
* #MAKILING "Makiling"
* #MAKILING ^property[0].code = #parent 
* #MAKILING ^property[0].valueCode = #SALMONELLA 
* #MAKISO "Makiso"
* #MAKISO ^property[0].code = #parent 
* #MAKISO ^property[0].valueCode = #SALMONELLA 
* #MALAKAL "Malakal"
* #MALAKAL ^property[0].code = #parent 
* #MALAKAL ^property[0].valueCode = #SALMONELLA 
* #MALAYSIA "Malaysia"
* #MALAYSIA ^property[0].code = #parent 
* #MALAYSIA ^property[0].valueCode = #SALMONELLA 
* #MALIKA "Malika"
* #MALIKA ^property[0].code = #parent 
* #MALIKA ^property[0].valueCode = #SALMONELLA 
* #MALMOE "Malmoe"
* #MALMOE ^property[0].code = #parent 
* #MALMOE ^property[0].valueCode = #SALMONELLA 
* #MALSTATT "Malstatt"
* #MALSTATT ^property[0].code = #parent 
* #MALSTATT ^property[0].valueCode = #SALMONELLA 
* #MAMPEZA "Mampeza"
* #MAMPEZA ^property[0].code = #parent 
* #MAMPEZA ^property[0].valueCode = #SALMONELLA 
* #MAMPONG "Mampong"
* #MAMPONG ^property[0].code = #parent 
* #MAMPONG ^property[0].valueCode = #SALMONELLA 
* #MANA "Mana"
* #MANA ^property[0].code = #parent 
* #MANA ^property[0].valueCode = #SALMONELLA 
* #MANCHESTER "Manchester"
* #MANCHESTER ^property[0].code = #parent 
* #MANCHESTER ^property[0].valueCode = #SALMONELLA 
* #MANDERA "Mandera"
* #MANDERA ^property[0].code = #parent 
* #MANDERA ^property[0].valueCode = #SALMONELLA 
* #MANGO "Mango"
* #MANGO ^property[0].code = #parent 
* #MANGO ^property[0].valueCode = #SALMONELLA 
* #MANHATTAN "Manhattan"
* #MANHATTAN ^property[0].code = #parent 
* #MANHATTAN ^property[0].valueCode = #SALMONELLA 
* #MANNHEIM "Mannheim"
* #MANNHEIM ^property[0].code = #parent 
* #MANNHEIM ^property[0].valueCode = #SALMONELLA 
* #MAPO "Mapo"
* #MAPO ^property[0].code = #parent 
* #MAPO ^property[0].valueCode = #SALMONELLA 
* #MARA "Mara"
* #MARA ^property[0].code = #parent 
* #MARA ^property[0].valueCode = #SALMONELLA 
* #MARACAIBO "Maracaibo"
* #MARACAIBO ^property[0].code = #parent 
* #MARACAIBO ^property[0].valueCode = #SALMONELLA 
* #MARBURG "Marburg"
* #MARBURG ^property[0].code = #parent 
* #MARBURG ^property[0].valueCode = #SALMONELLA 
* #MARICOPA "Maricopa"
* #MARICOPA ^property[0].code = #parent 
* #MARICOPA ^property[0].valueCode = #SALMONELLA 
* #MARIENTHAL "Marienthal"
* #MARIENTHAL ^property[0].code = #parent 
* #MARIENTHAL ^property[0].valueCode = #SALMONELLA 
* #MARITZBURG "Maritzburg"
* #MARITZBURG ^property[0].code = #parent 
* #MARITZBURG ^property[0].valueCode = #SALMONELLA 
* #MARMANDE "Marmande"
* #MARMANDE ^property[0].code = #parent 
* #MARMANDE ^property[0].valueCode = #SALMONELLA 
* #MARON "Maron"
* #MARON ^property[0].code = #parent 
* #MARON ^property[0].valueCode = #SALMONELLA 
* #MAROUA "Maroua"
* #MAROUA ^property[0].code = #parent 
* #MAROUA ^property[0].valueCode = #SALMONELLA 
* #MARSABIT "Marsabit"
* #MARSABIT ^property[0].code = #parent 
* #MARSABIT ^property[0].valueCode = #SALMONELLA 
* #MARSEILLE "Marseille"
* #MARSEILLE ^property[0].code = #parent 
* #MARSEILLE ^property[0].valueCode = #SALMONELLA 
* #MARSHALL "Marshall"
* #MARSHALL ^property[0].code = #parent 
* #MARSHALL ^property[0].valueCode = #SALMONELLA 
* #MARTONOS "Martonos"
* #MARTONOS ^property[0].code = #parent 
* #MARTONOS ^property[0].valueCode = #SALMONELLA 
* #MARYLAND "Maryland"
* #MARYLAND ^property[0].code = #parent 
* #MARYLAND ^property[0].valueCode = #SALMONELLA 
* #MARYLEBONE "Marylebone"
* #MARYLEBONE ^property[0].code = #parent 
* #MARYLEBONE ^property[0].valueCode = #SALMONELLA 
* #MASEMBE "Masembe"
* #MASEMBE ^property[0].code = #parent 
* #MASEMBE ^property[0].valueCode = #SALMONELLA 
* #MASKA "Maska"
* #MASKA ^property[0].code = #parent 
* #MASKA ^property[0].valueCode = #SALMONELLA 
* #MASSAKORY "Massakory"
* #MASSAKORY ^property[0].code = #parent 
* #MASSAKORY ^property[0].valueCode = #SALMONELLA 
* #MASSENYA "Massenya"
* #MASSENYA ^property[0].code = #parent 
* #MASSENYA ^property[0].valueCode = #SALMONELLA 
* #MASSILIA "Massilia"
* #MASSILIA ^property[0].code = #parent 
* #MASSILIA ^property[0].valueCode = #SALMONELLA 
* #MATADI "Matadi"
* #MATADI ^property[0].code = #parent 
* #MATADI ^property[0].valueCode = #SALMONELLA 
* #MATHURA "Mathura"
* #MATHURA ^property[0].code = #parent 
* #MATHURA ^property[0].valueCode = #SALMONELLA 
* #MATOPENI "Matopeni"
* #MATOPENI ^property[0].code = #parent 
* #MATOPENI ^property[0].valueCode = #SALMONELLA 
* #MATTENHOF "Mattenhof"
* #MATTENHOF ^property[0].code = #parent 
* #MATTENHOF ^property[0].valueCode = #SALMONELLA 
* #MAUMEE "Maumee"
* #MAUMEE ^property[0].code = #parent 
* #MAUMEE ^property[0].valueCode = #SALMONELLA 
* #MAYDAY "Mayday"
* #MAYDAY ^property[0].code = #parent 
* #MAYDAY ^property[0].valueCode = #SALMONELLA 
* #MBANDAKA "Mbandaka"
* #MBANDAKA ^property[0].code = #parent 
* #MBANDAKA ^property[0].valueCode = #SALMONELLA 
* #MBAO "Mbao"
* #MBAO ^property[0].code = #parent 
* #MBAO ^property[0].valueCode = #SALMONELLA 
* #MEEKATHARRA "Meekatharra"
* #MEEKATHARRA ^property[0].code = #parent 
* #MEEKATHARRA ^property[0].valueCode = #SALMONELLA 
* #MELAKA "Melaka"
* #MELAKA ^property[0].code = #parent 
* #MELAKA ^property[0].valueCode = #SALMONELLA 
* #MELBOURNE "Melbourne"
* #MELBOURNE ^property[0].code = #parent 
* #MELBOURNE ^property[0].valueCode = #SALMONELLA 
* #MELEAGRIDIS "Meleagridis"
* #MELEAGRIDIS ^property[0].code = #parent 
* #MELEAGRIDIS ^property[0].valueCode = #SALMONELLA 
* #MELEAGRIDIS_VAR_15+ "Meleagridis var. 15+"
* #MELEAGRIDIS_VAR_15+ ^property[0].code = #parent 
* #MELEAGRIDIS_VAR_15+ ^property[0].valueCode = #SALMONELLA 
* #MELEAGRIDIS_VAR_15+_34+ "Meleagridis var. 15+, 34+"
* #MELEAGRIDIS_VAR_15+_34+ ^property[0].code = #parent 
* #MELEAGRIDIS_VAR_15+_34+ ^property[0].valueCode = #SALMONELLA 
* #MEMPHIS "Memphis"
* #MEMPHIS ^property[0].code = #parent 
* #MEMPHIS ^property[0].valueCode = #SALMONELLA 
* #MENDEN "Menden"
* #MENDEN ^property[0].code = #parent 
* #MENDEN ^property[0].valueCode = #SALMONELLA 
* #MENDOZA "Mendoza"
* #MENDOZA ^property[0].code = #parent 
* #MENDOZA ^property[0].valueCode = #SALMONELLA 
* #MENSTON "Menston"
* #MENSTON ^property[0].code = #parent 
* #MENSTON ^property[0].valueCode = #SALMONELLA 
* #MESBIT "Mesbit"
* #MESBIT ^property[0].code = #parent 
* #MESBIT ^property[0].valueCode = #SALMONELLA 
* #MESKIN "Meskin"
* #MESKIN ^property[0].code = #parent 
* #MESKIN ^property[0].valueCode = #SALMONELLA 
* #MESSINA "Messina"
* #MESSINA ^property[0].code = #parent 
* #MESSINA ^property[0].valueCode = #SALMONELLA 
* #MGULANI "Mgulani"
* #MGULANI ^property[0].code = #parent 
* #MGULANI ^property[0].valueCode = #SALMONELLA 
* #MIAMI "Miami"
* #MIAMI ^property[0].code = #parent 
* #MIAMI ^property[0].valueCode = #SALMONELLA 
* #MICHIGAN "Michigan"
* #MICHIGAN ^property[0].code = #parent 
* #MICHIGAN ^property[0].valueCode = #SALMONELLA 
* #MIDDLESBROUGH "Middlesbrough"
* #MIDDLESBROUGH ^property[0].code = #parent 
* #MIDDLESBROUGH ^property[0].valueCode = #SALMONELLA 
* #MIDWAY "Midway"
* #MIDWAY ^property[0].code = #parent 
* #MIDWAY ^property[0].valueCode = #SALMONELLA 
* #MIKAWASIMA "Mikawasima"
* #MIKAWASIMA ^property[0].code = #parent 
* #MIKAWASIMA ^property[0].valueCode = #SALMONELLA 
* #MILLESI "Millesi"
* #MILLESI ^property[0].code = #parent 
* #MILLESI ^property[0].valueCode = #SALMONELLA 
* #MILWAUKEE "Milwaukee"
* #MILWAUKEE ^property[0].code = #parent 
* #MILWAUKEE ^property[0].valueCode = #SALMONELLA 
* #MIM "Mim"
* #MIM ^property[0].code = #parent 
* #MIM ^property[0].valueCode = #SALMONELLA 
* #MINNA "Minna"
* #MINNA ^property[0].code = #parent 
* #MINNA ^property[0].valueCode = #SALMONELLA 
* #MINNESOTA "Minnesota"
* #MINNESOTA ^property[0].code = #parent 
* #MINNESOTA ^property[0].valueCode = #SALMONELLA 
* #MISHMARHAEMEK "Mishmarhaemek"
* #MISHMARHAEMEK ^property[0].code = #parent 
* #MISHMARHAEMEK ^property[0].valueCode = #SALMONELLA 
* #MISSISSIPPI "Mississippi"
* #MISSISSIPPI ^property[0].code = #parent 
* #MISSISSIPPI ^property[0].valueCode = #SALMONELLA 
* #MISSOURI "Missouri"
* #MISSOURI ^property[0].code = #parent 
* #MISSOURI ^property[0].valueCode = #SALMONELLA 
* #MIYAZAKI "Miyazaki"
* #MIYAZAKI ^property[0].code = #parent 
* #MIYAZAKI ^property[0].valueCode = #SALMONELLA 
* #MJORDAN "Mjordan"
* #MJORDAN ^property[0].code = #parent 
* #MJORDAN ^property[0].valueCode = #SALMONELLA 
* #MKAMBA "Mkamba"
* #MKAMBA ^property[0].code = #parent 
* #MKAMBA ^property[0].valueCode = #SALMONELLA 
* #MOABIT "Moabit"
* #MOABIT ^property[0].code = #parent 
* #MOABIT ^property[0].valueCode = #SALMONELLA 
* #MOCAMEDES "Mocamedes"
* #MOCAMEDES ^property[0].code = #parent 
* #MOCAMEDES ^property[0].valueCode = #SALMONELLA 
* #MOERO "Moero"
* #MOERO ^property[0].code = #parent 
* #MOERO ^property[0].valueCode = #SALMONELLA 
* #MOERS "Moers"
* #MOERS ^property[0].code = #parent 
* #MOERS ^property[0].valueCode = #SALMONELLA 
* #MOKOLA "Mokola"
* #MOKOLA ^property[0].code = #parent 
* #MOKOLA ^property[0].valueCode = #SALMONELLA 
* #MOLADE "Molade"
* #MOLADE ^property[0].code = #parent 
* #MOLADE ^property[0].valueCode = #SALMONELLA 
* #MOLESEY "Molesey"
* #MOLESEY ^property[0].code = #parent 
* #MOLESEY ^property[0].valueCode = #SALMONELLA 
* #MONO "Mono"
* #MONO ^property[0].code = #parent 
* #MONO ^property[0].valueCode = #SALMONELLA 
* #MONOPHASIC_14[5]12_I_- "Monophasic 1.4.[5].12:I:-"
* #MONOPHASIC_14[5]12_I_- ^property[0].code = #parent 
* #MONOPHASIC_14[5]12_I_- ^property[0].valueCode = #SALMONELLA 
* #MONS "Mons"
* #MONS ^property[0].code = #parent 
* #MONS ^property[0].valueCode = #SALMONELLA 
* #MONSCHAUI "Monschaui"
* #MONSCHAUI ^property[0].code = #parent 
* #MONSCHAUI ^property[0].valueCode = #SALMONELLA 
* #MONTAIGU "Montaigu"
* #MONTAIGU ^property[0].code = #parent 
* #MONTAIGU ^property[0].valueCode = #SALMONELLA 
* #MONTEVIDEO "Montevideo"
* #MONTEVIDEO ^property[0].code = #parent 
* #MONTEVIDEO ^property[0].valueCode = #SALMONELLA 
* #MONTREAL "Montreal"
* #MONTREAL ^property[0].code = #parent 
* #MONTREAL ^property[0].valueCode = #SALMONELLA 
* #MORBIHAN "Morbihan"
* #MORBIHAN ^property[0].code = #parent 
* #MORBIHAN ^property[0].valueCode = #SALMONELLA 
* #MOREHEAD "Morehead"
* #MOREHEAD ^property[0].code = #parent 
* #MOREHEAD ^property[0].valueCode = #SALMONELLA 
* #MORILLONS "Morillons"
* #MORILLONS ^property[0].code = #parent 
* #MORILLONS ^property[0].valueCode = #SALMONELLA 
* #MORNINGSIDE "Morningside"
* #MORNINGSIDE ^property[0].code = #parent 
* #MORNINGSIDE ^property[0].valueCode = #SALMONELLA 
* #MORNINGTON "Mornington"
* #MORNINGTON ^property[0].code = #parent 
* #MORNINGTON ^property[0].valueCode = #SALMONELLA 
* #MOROCCO "Morocco"
* #MOROCCO ^property[0].code = #parent 
* #MOROCCO ^property[0].valueCode = #SALMONELLA 
* #MOROTAI "Morotai"
* #MOROTAI ^property[0].code = #parent 
* #MOROTAI ^property[0].valueCode = #SALMONELLA 
* #MOROTO "Moroto"
* #MOROTO ^property[0].code = #parent 
* #MOROTO ^property[0].valueCode = #SALMONELLA 
* #MOSCOW "Moscow"
* #MOSCOW ^property[0].code = #parent 
* #MOSCOW ^property[0].valueCode = #SALMONELLA 
* #MOUALINE "Moualine"
* #MOUALINE ^property[0].code = #parent 
* #MOUALINE ^property[0].valueCode = #SALMONELLA 
* #MOUNDOU "Moundou"
* #MOUNDOU ^property[0].code = #parent 
* #MOUNDOU ^property[0].valueCode = #SALMONELLA 
* #MOUNTMAGNET "Mountmagnet"
* #MOUNTMAGNET ^property[0].code = #parent 
* #MOUNTMAGNET ^property[0].valueCode = #SALMONELLA 
* #MOUNTPLEASANT "Mountpleasant"
* #MOUNTPLEASANT ^property[0].code = #parent 
* #MOUNTPLEASANT ^property[0].valueCode = #SALMONELLA 
* #MOUSSORO "Moussoro"
* #MOUSSORO ^property[0].code = #parent 
* #MOUSSORO ^property[0].valueCode = #SALMONELLA 
* #MOWANJUM "Mowanjum"
* #MOWANJUM ^property[0].code = #parent 
* #MOWANJUM ^property[0].valueCode = #SALMONELLA 
* #MPOUTO "Mpouto"
* #MPOUTO ^property[0].code = #parent 
* #MPOUTO ^property[0].valueCode = #SALMONELLA 
* #MUENCHEN "Muenchen"
* #MUENCHEN ^property[0].code = #parent 
* #MUENCHEN ^property[0].valueCode = #SALMONELLA 
* #MUENSTER "Muenster"
* #MUENSTER ^property[0].code = #parent 
* #MUENSTER ^property[0].valueCode = #SALMONELLA 
* #MUENSTER_VAR_15+ "Muenster var. 15+"
* #MUENSTER_VAR_15+ ^property[0].code = #parent 
* #MUENSTER_VAR_15+ ^property[0].valueCode = #SALMONELLA 
* #MUENSTER_VAR_15+_34+_ "Muenster var. 15+, 34+"
* #MUENSTER_VAR_15+_34+_ ^property[0].code = #parent 
* #MUENSTER_VAR_15+_34+_ ^property[0].valueCode = #SALMONELLA 
* #MUGUGA "Muguga"
* #MUGUGA ^property[0].code = #parent 
* #MUGUGA ^property[0].valueCode = #SALMONELLA 
* #MULHOUSE "Mulhouse"
* #MULHOUSE ^property[0].code = #parent 
* #MULHOUSE ^property[0].valueCode = #SALMONELLA 
* #MUNDONOBO "Mundonobo"
* #MUNDONOBO ^property[0].code = #parent 
* #MUNDONOBO ^property[0].valueCode = #SALMONELLA 
* #MUNDUBBERA "Mundubbera"
* #MUNDUBBERA ^property[0].code = #parent 
* #MUNDUBBERA ^property[0].valueCode = #SALMONELLA 
* #MURA "Mura"
* #MURA ^property[0].code = #parent 
* #MURA ^property[0].valueCode = #SALMONELLA 
* #MYGDAL "Mygdal"
* #MYGDAL ^property[0].code = #parent 
* #MYGDAL ^property[0].valueCode = #SALMONELLA 
* #MYRRIA "Myrria"
* #MYRRIA ^property[0].code = #parent 
* #MYRRIA ^property[0].valueCode = #SALMONELLA 
* #NAESTVED "Naestved"
* #NAESTVED ^property[0].code = #parent 
* #NAESTVED ^property[0].valueCode = #SALMONELLA 
* #NAGOYA "Nagoya"
* #NAGOYA ^property[0].code = #parent 
* #NAGOYA ^property[0].valueCode = #SALMONELLA 
* #NAKURU "Nakuru"
* #NAKURU ^property[0].code = #parent 
* #NAKURU ^property[0].valueCode = #SALMONELLA 
* #NAMIBIA "Namibia"
* #NAMIBIA ^property[0].code = #parent 
* #NAMIBIA ^property[0].valueCode = #SALMONELLA 
* #NAMODA "Namoda"
* #NAMODA ^property[0].code = #parent 
* #NAMODA ^property[0].valueCode = #SALMONELLA 
* #NAMUR "Namur"
* #NAMUR ^property[0].code = #parent 
* #NAMUR ^property[0].valueCode = #SALMONELLA 
* #NANERGOU "Nanergou"
* #NANERGOU ^property[0].code = #parent 
* #NANERGOU ^property[0].valueCode = #SALMONELLA 
* #NANGA "Nanga"
* #NANGA ^property[0].code = #parent 
* #NANGA ^property[0].valueCode = #SALMONELLA 
* #NANTES "Nantes"
* #NANTES ^property[0].code = #parent 
* #NANTES ^property[0].valueCode = #SALMONELLA 
* #NAPOLI "Napoli"
* #NAPOLI ^property[0].code = #parent 
* #NAPOLI ^property[0].valueCode = #SALMONELLA 
* #NARASHINO "Narashino"
* #NARASHINO ^property[0].code = #parent 
* #NARASHINO ^property[0].valueCode = #SALMONELLA 
* #NASHUA "Nashua"
* #NASHUA ^property[0].code = #parent 
* #NASHUA ^property[0].valueCode = #SALMONELLA 
* #NATAL "Natal"
* #NATAL ^property[0].code = #parent 
* #NATAL ^property[0].valueCode = #SALMONELLA 
* #NAWARE "Naware"
* #NAWARE ^property[0].code = #parent 
* #NAWARE ^property[0].valueCode = #SALMONELLA 
* #NCHANGA "Nchanga"
* #NCHANGA ^property[0].code = #parent 
* #NCHANGA ^property[0].valueCode = #SALMONELLA 
* #NCHANGA_VAR_15+ "Nchanga var. 15+"
* #NCHANGA_VAR_15+ ^property[0].code = #parent 
* #NCHANGA_VAR_15+ ^property[0].valueCode = #SALMONELLA 
* #NDJAMENA "Ndjamena"
* #NDJAMENA ^property[0].code = #parent 
* #NDJAMENA ^property[0].valueCode = #SALMONELLA 
* #NDOLO "Ndolo"
* #NDOLO ^property[0].code = #parent 
* #NDOLO ^property[0].valueCode = #SALMONELLA 
* #NEFTENBACH "Neftenbach"
* #NEFTENBACH ^property[0].code = #parent 
* #NEFTENBACH ^property[0].valueCode = #SALMONELLA 
* #NESSA "Nessa"
* #NESSA ^property[0].code = #parent 
* #NESSA ^property[0].valueCode = #SALMONELLA 
* #NESSZIONA "Nessziona"
* #NESSZIONA ^property[0].code = #parent 
* #NESSZIONA ^property[0].valueCode = #SALMONELLA 
* #NEUDORF "Neudorf"
* #NEUDORF ^property[0].code = #parent 
* #NEUDORF ^property[0].valueCode = #SALMONELLA 
* #NEUKOELLN "Neukoelln"
* #NEUKOELLN ^property[0].code = #parent 
* #NEUKOELLN ^property[0].valueCode = #SALMONELLA 
* #NEUMUENSTER "Neumuenster"
* #NEUMUENSTER ^property[0].code = #parent 
* #NEUMUENSTER ^property[0].valueCode = #SALMONELLA 
* #NEUNKIRCHEN "Neunkirchen"
* #NEUNKIRCHEN ^property[0].code = #parent 
* #NEUNKIRCHEN ^property[0].valueCode = #SALMONELLA 
* #NEWHOLLAND "Newholland"
* #NEWHOLLAND ^property[0].code = #parent 
* #NEWHOLLAND ^property[0].valueCode = #SALMONELLA 
* #NEWJERSEY "Newjersey"
* #NEWJERSEY ^property[0].code = #parent 
* #NEWJERSEY ^property[0].valueCode = #SALMONELLA 
* #NEWLANDS "Newlands"
* #NEWLANDS ^property[0].code = #parent 
* #NEWLANDS ^property[0].valueCode = #SALMONELLA 
* #NEWMEXICO "Newmexico"
* #NEWMEXICO ^property[0].code = #parent 
* #NEWMEXICO ^property[0].valueCode = #SALMONELLA 
* #NEWPORT "Newport"
* #NEWPORT ^property[0].code = #parent 
* #NEWPORT ^property[0].valueCode = #SALMONELLA 
* #NEWROCHELLE "Newrochelle"
* #NEWROCHELLE ^property[0].code = #parent 
* #NEWROCHELLE ^property[0].valueCode = #SALMONELLA 
* #NEWYORK "Newyork"
* #NEWYORK ^property[0].code = #parent 
* #NEWYORK ^property[0].valueCode = #SALMONELLA 
* #NGAPAROU "Ngaparou"
* #NGAPAROU ^property[0].code = #parent 
* #NGAPAROU ^property[0].valueCode = #SALMONELLA 
* #NGILI "Ngili"
* #NGILI ^property[0].code = #parent 
* #NGILI ^property[0].valueCode = #SALMONELLA 
* #NGOR "Ngor"
* #NGOR ^property[0].code = #parent 
* #NGOR ^property[0].valueCode = #SALMONELLA 
* #NIAKHAR "Niakhar"
* #NIAKHAR ^property[0].code = #parent 
* #NIAKHAR ^property[0].valueCode = #SALMONELLA 
* #NIAMEY "Niamey"
* #NIAMEY ^property[0].code = #parent 
* #NIAMEY ^property[0].valueCode = #SALMONELLA 
* #NIAREMBE "Niarembe"
* #NIAREMBE ^property[0].code = #parent 
* #NIAREMBE ^property[0].valueCode = #SALMONELLA 
* #NIEDERODERWITZ "Niederoderwitz"
* #NIEDERODERWITZ ^property[0].code = #parent 
* #NIEDERODERWITZ ^property[0].valueCode = #SALMONELLA 
* #NIEUKERK "Nieukerk"
* #NIEUKERK ^property[0].code = #parent 
* #NIEUKERK ^property[0].valueCode = #SALMONELLA 
* #NIGERIA "Nigeria"
* #NIGERIA ^property[0].code = #parent 
* #NIGERIA ^property[0].valueCode = #SALMONELLA 
* #NIJMEGEN "Nijmegen"
* #NIJMEGEN ^property[0].code = #parent 
* #NIJMEGEN ^property[0].valueCode = #SALMONELLA 
* #NIKOLAIFLEET "Nikolaifleet"
* #NIKOLAIFLEET ^property[0].code = #parent 
* #NIKOLAIFLEET ^property[0].valueCode = #SALMONELLA 
* #NILOESE "Niloese"
* #NILOESE ^property[0].code = #parent 
* #NILOESE ^property[0].valueCode = #SALMONELLA 
* #NIMA "Nima"
* #NIMA ^property[0].code = #parent 
* #NIMA ^property[0].valueCode = #SALMONELLA 
* #NIMES "Nimes"
* #NIMES ^property[0].code = #parent 
* #NIMES ^property[0].valueCode = #SALMONELLA 
* #NITRA "Nitra"
* #NITRA ^property[0].code = #parent 
* #NITRA ^property[0].valueCode = #SALMONELLA 
* #NIUMI "Niumi"
* #NIUMI ^property[0].code = #parent 
* #NIUMI ^property[0].valueCode = #SALMONELLA 
* #NJALA "Njala"
* #NJALA ^property[0].code = #parent 
* #NJALA ^property[0].valueCode = #SALMONELLA 
* #NOLA "Nola"
* #NOLA ^property[0].code = #parent 
* #NOLA ^property[0].valueCode = #SALMONELLA 
* #NORDRHEIN "Nordrhein"
* #NORDRHEIN ^property[0].code = #parent 
* #NORDRHEIN ^property[0].valueCode = #SALMONELLA 
* #NORDUFER "Nordufer"
* #NORDUFER ^property[0].code = #parent 
* #NORDUFER ^property[0].valueCode = #SALMONELLA 
* #NORTON "Norton"
* #NORTON ^property[0].code = #parent 
* #NORTON ^property[0].valueCode = #SALMONELLA 
* #NORWICH "Norwich"
* #NORWICH ^property[0].code = #parent 
* #NORWICH ^property[0].valueCode = #SALMONELLA 
* #NOTTINGHAM "Nottingham"
* #NOTTINGHAM ^property[0].code = #parent 
* #NOTTINGHAM ^property[0].valueCode = #SALMONELLA 
* #NOWAWES "Nowawes"
* #NOWAWES ^property[0].code = #parent 
* #NOWAWES ^property[0].valueCode = #SALMONELLA 
* #NOYA "Noya"
* #NOYA ^property[0].code = #parent 
* #NOYA ^property[0].valueCode = #SALMONELLA 
* #NUATJA "Nuatja"
* #NUATJA ^property[0].code = #parent 
* #NUATJA ^property[0].valueCode = #SALMONELLA 
* #NYANZA "Nyanza"
* #NYANZA ^property[0].code = #parent 
* #NYANZA ^property[0].valueCode = #SALMONELLA 
* #NYBORG "Nyborg"
* #NYBORG ^property[0].code = #parent 
* #NYBORG ^property[0].valueCode = #SALMONELLA 
* #NYBORG_VAR_15+ "Nyborg var. 15+"
* #NYBORG_VAR_15+ ^property[0].code = #parent 
* #NYBORG_VAR_15+ ^property[0].valueCode = #SALMONELLA 
* #NYEKO "Nyeko"
* #NYEKO ^property[0].code = #parent 
* #NYEKO ^property[0].valueCode = #SALMONELLA 
* #OAKEY "Oakey"
* #OAKEY ^property[0].code = #parent 
* #OAKEY ^property[0].valueCode = #SALMONELLA 
* #OAKLAND "Oakland"
* #OAKLAND ^property[0].code = #parent 
* #OAKLAND ^property[0].valueCode = #SALMONELLA 
* #OBOGU "Obogu"
* #OBOGU ^property[0].code = #parent 
* #OBOGU ^property[0].valueCode = #SALMONELLA 
* #OCHIOGU "Ochiogu"
* #OCHIOGU ^property[0].code = #parent 
* #OCHIOGU ^property[0].valueCode = #SALMONELLA 
* #OCHSENWERDER "Ochsenwerder"
* #OCHSENWERDER ^property[0].code = #parent 
* #OCHSENWERDER ^property[0].valueCode = #SALMONELLA 
* #OCKENHEIM "Ockenheim"
* #OCKENHEIM ^property[0].code = #parent 
* #OCKENHEIM ^property[0].valueCode = #SALMONELLA 
* #ODIENNE "Odienne"
* #ODIENNE ^property[0].code = #parent 
* #ODIENNE ^property[0].valueCode = #SALMONELLA 
* #ODOZI "Odozi"
* #ODOZI ^property[0].code = #parent 
* #ODOZI ^property[0].valueCode = #SALMONELLA 
* #OERLIKON "Oerlikon"
* #OERLIKON ^property[0].code = #parent 
* #OERLIKON ^property[0].valueCode = #SALMONELLA 
* #OESTERBRO "Oesterbro"
* #OESTERBRO ^property[0].code = #parent 
* #OESTERBRO ^property[0].valueCode = #SALMONELLA 
* #OFFA "Offa"
* #OFFA ^property[0].code = #parent 
* #OFFA ^property[0].valueCode = #SALMONELLA 
* #OGBETE "Ogbete"
* #OGBETE ^property[0].code = #parent 
* #OGBETE ^property[0].valueCode = #SALMONELLA 
* #OHIO "Ohio"
* #OHIO ^property[0].code = #parent 
* #OHIO ^property[0].valueCode = #SALMONELLA 
* #OHIO_VAR_14+ "Ohio var. 14+"
* #OHIO_VAR_14+ ^property[0].code = #parent 
* #OHIO_VAR_14+ ^property[0].valueCode = #SALMONELLA 
* #OHLSTEDT "Ohlstedt"
* #OHLSTEDT ^property[0].code = #parent 
* #OHLSTEDT ^property[0].valueCode = #SALMONELLA 
* #OKATIE "Okatie"
* #OKATIE ^property[0].code = #parent 
* #OKATIE ^property[0].valueCode = #SALMONELLA 
* #OKEFOKO "Okefoko"
* #OKEFOKO ^property[0].code = #parent 
* #OKEFOKO ^property[0].valueCode = #SALMONELLA 
* #OKERARA "Okerara"
* #OKERARA ^property[0].code = #parent 
* #OKERARA ^property[0].valueCode = #SALMONELLA 
* #OLDENBURG "Oldenburg"
* #OLDENBURG ^property[0].code = #parent 
* #OLDENBURG ^property[0].valueCode = #SALMONELLA 
* #OLTEN "Olten"
* #OLTEN ^property[0].code = #parent 
* #OLTEN ^property[0].valueCode = #SALMONELLA 
* #OMIFISAN "Omifisan"
* #OMIFISAN ^property[0].code = #parent 
* #OMIFISAN ^property[0].valueCode = #SALMONELLA 
* #OMUNA "Omuna"
* #OMUNA ^property[0].code = #parent 
* #OMUNA ^property[0].valueCode = #SALMONELLA 
* #ONA "Ona"
* #ONA ^property[0].code = #parent 
* #ONA ^property[0].valueCode = #SALMONELLA 
* #ONARIMON "Onarimon"
* #ONARIMON ^property[0].code = #parent 
* #ONARIMON ^property[0].valueCode = #SALMONELLA 
* #ONDERSTEPOORT "Onderstepoort"
* #ONDERSTEPOORT ^property[0].code = #parent 
* #ONDERSTEPOORT ^property[0].valueCode = #SALMONELLA 
* #ONIREKE "Onireke"
* #ONIREKE ^property[0].code = #parent 
* #ONIREKE ^property[0].valueCode = #SALMONELLA 
* #ONTARIO "Ontario"
* #ONTARIO ^property[0].code = #parent 
* #ONTARIO ^property[0].valueCode = #SALMONELLA 
* #ORAN "Oran"
* #ORAN ^property[0].code = #parent 
* #ORAN ^property[0].valueCode = #SALMONELLA 
* #ORANIENBURG "Oranienburg"
* #ORANIENBURG ^property[0].code = #parent 
* #ORANIENBURG ^property[0].valueCode = #SALMONELLA 
* #ORANIENBURG_VAR_14+ "Oranienburg var. 14+"
* #ORANIENBURG_VAR_14+ ^property[0].code = #parent 
* #ORANIENBURG_VAR_14+ ^property[0].valueCode = #SALMONELLA 
* #ORBE "Orbe"
* #ORBE ^property[0].code = #parent 
* #ORBE ^property[0].valueCode = #SALMONELLA 
* #ORD "Ord"
* #ORD ^property[0].code = #parent 
* #ORD ^property[0].valueCode = #SALMONELLA 
* #ORDONEZ "Ordonez"
* #ORDONEZ ^property[0].code = #parent 
* #ORDONEZ ^property[0].valueCode = #SALMONELLA 
* #ORIENTALIS "Orientalis"
* #ORIENTALIS ^property[0].code = #parent 
* #ORIENTALIS ^property[0].valueCode = #SALMONELLA 
* #ORION "Orion"
* #ORION ^property[0].code = #parent 
* #ORION ^property[0].valueCode = #SALMONELLA 
* #ORION_VAR_15+_ "Orion var. 15+"
* #ORION_VAR_15+_ ^property[0].code = #parent 
* #ORION_VAR_15+_ ^property[0].valueCode = #SALMONELLA 
* #ORION_VAR_15+_34+ "Orion var. 15+, 34+"
* #ORION_VAR_15+_34+ ^property[0].code = #parent 
* #ORION_VAR_15+_34+ ^property[0].valueCode = #SALMONELLA 
* #ORITAMERIN "Oritamerin"
* #ORITAMERIN ^property[0].code = #parent 
* #ORITAMERIN ^property[0].valueCode = #SALMONELLA 
* #ORLANDO "Orlando"
* #ORLANDO ^property[0].code = #parent 
* #ORLANDO ^property[0].valueCode = #SALMONELLA 
* #ORLEANS "Orleans"
* #ORLEANS ^property[0].code = #parent 
* #ORLEANS ^property[0].valueCode = #SALMONELLA 
* #OS "Os"
* #OS ^property[0].code = #parent 
* #OS ^property[0].valueCode = #SALMONELLA 
* #OSKARSHAMN "Oskarshamn"
* #OSKARSHAMN ^property[0].code = #parent 
* #OSKARSHAMN ^property[0].valueCode = #SALMONELLA 
* #OSLO "Oslo"
* #OSLO ^property[0].code = #parent 
* #OSLO ^property[0].valueCode = #SALMONELLA 
* #OSNABRUECK "Osnabrueck"
* #OSNABRUECK ^property[0].code = #parent 
* #OSNABRUECK ^property[0].valueCode = #SALMONELLA 
* #OTHER "Other"
* #OTHER ^property[0].code = #parent 
* #OTHER ^property[0].valueCode = #SALMONELLA 
* #OTHMARSCHEN "Othmarschen"
* #OTHMARSCHEN ^property[0].code = #parent 
* #OTHMARSCHEN ^property[0].valueCode = #SALMONELLA 
* #OTTAWA "Ottawa"
* #OTTAWA ^property[0].code = #parent 
* #OTTAWA ^property[0].valueCode = #SALMONELLA 
* #OUAGADOUGOU "Ouagadougou"
* #OUAGADOUGOU ^property[0].code = #parent 
* #OUAGADOUGOU ^property[0].valueCode = #SALMONELLA 
* #OUAKAM "Ouakam"
* #OUAKAM ^property[0].code = #parent 
* #OUAKAM ^property[0].valueCode = #SALMONELLA 
* #OUDWIJK "Oudwijk"
* #OUDWIJK ^property[0].code = #parent 
* #OUDWIJK ^property[0].valueCode = #SALMONELLA 
* #OVERCHURCH "Overchurch"
* #OVERCHURCH ^property[0].code = #parent 
* #OVERCHURCH ^property[0].valueCode = #SALMONELLA 
* #OVERSCHIE "Overschie"
* #OVERSCHIE ^property[0].code = #parent 
* #OVERSCHIE ^property[0].valueCode = #SALMONELLA 
* #OVERVECHT "Overvecht"
* #OVERVECHT ^property[0].code = #parent 
* #OVERVECHT ^property[0].valueCode = #SALMONELLA 
* #OXFORD "Oxford"
* #OXFORD ^property[0].code = #parent 
* #OXFORD ^property[0].valueCode = #SALMONELLA 
* #OXFORD_VAR_15+_ "Oxford var. 15+"
* #OXFORD_VAR_15+_ ^property[0].code = #parent 
* #OXFORD_VAR_15+_ ^property[0].valueCode = #SALMONELLA 
* #OXFORD_VAR_15+_34+ "Oxford var. 15+, 34+"
* #OXFORD_VAR_15+_34+ ^property[0].code = #parent 
* #OXFORD_VAR_15+_34+ ^property[0].valueCode = #SALMONELLA 
* #OYONNAX "Oyonnax"
* #OYONNAX ^property[0].code = #parent 
* #OYONNAX ^property[0].valueCode = #SALMONELLA 
* #PAKISTAN "Pakistan"
* #PAKISTAN ^property[0].code = #parent 
* #PAKISTAN ^property[0].valueCode = #SALMONELLA 
* #PALAMANER "Palamaner"
* #PALAMANER ^property[0].code = #parent 
* #PALAMANER ^property[0].valueCode = #SALMONELLA 
* #PALIME "Palime"
* #PALIME ^property[0].code = #parent 
* #PALIME ^property[0].valueCode = #SALMONELLA 
* #PANAMA "Panama"
* #PANAMA ^property[0].code = #parent 
* #PANAMA ^property[0].valueCode = #SALMONELLA 
* #PAPUANA "Papuana"
* #PAPUANA ^property[0].code = #parent 
* #PAPUANA ^property[0].valueCode = #SALMONELLA 
* #PARAKOU "Parakou"
* #PARAKOU ^property[0].code = #parent 
* #PARAKOU ^property[0].valueCode = #SALMONELLA 
* #PARATYPHI "Salmonella Paratyphi"
* #PARATYPHI ^property[0].code = #parent 
* #PARATYPHI ^property[0].valueCode = #SALMONELLA 
* #PARATYPHI_B_VAR_L(+)_TARTRATE+ "Paratyphi B var. L(+) tartrate+ (variant Java)"
* #PARATYPHI_B_VAR_L(+)_TARTRATE+ ^property[0].code = #parent 
* #PARATYPHI_B_VAR_L(+)_TARTRATE+ ^property[0].valueCode = #SALMONELLA 
* #PARIS "Paris"
* #PARIS ^property[0].code = #parent 
* #PARIS ^property[0].valueCode = #SALMONELLA 
* #PARKROYAL "Parkroyal"
* #PARKROYAL ^property[0].code = #parent 
* #PARKROYAL ^property[0].valueCode = #SALMONELLA 
* #PASING "Pasing"
* #PASING ^property[0].code = #parent 
* #PASING ^property[0].valueCode = #SALMONELLA 
* #PATIENCE "Patience"
* #PATIENCE ^property[0].code = #parent 
* #PATIENCE ^property[0].valueCode = #SALMONELLA 
* #PENARTH "Penarth"
* #PENARTH ^property[0].code = #parent 
* #PENARTH ^property[0].valueCode = #SALMONELLA 
* #PENILLA "Penilla"
* #PENILLA ^property[0].code = #parent 
* #PENILLA ^property[0].valueCode = #SALMONELLA 
* #PENSACOLA "Pensacola"
* #PENSACOLA ^property[0].code = #parent 
* #PENSACOLA ^property[0].valueCode = #SALMONELLA 
* #PERTH "Perth"
* #PERTH ^property[0].code = #parent 
* #PERTH ^property[0].valueCode = #SALMONELLA 
* #PETAHTIKVE "Petahtikve"
* #PETAHTIKVE ^property[0].code = #parent 
* #PETAHTIKVE ^property[0].valueCode = #SALMONELLA 
* #PHALIRON "Phaliron"
* #PHALIRON ^property[0].code = #parent 
* #PHALIRON ^property[0].valueCode = #SALMONELLA 
* #PHARR "Pharr"
* #PHARR ^property[0].code = #parent 
* #PHARR ^property[0].valueCode = #SALMONELLA 
* #PICPUS "Picpus"
* #PICPUS ^property[0].code = #parent 
* #PICPUS ^property[0].valueCode = #SALMONELLA 
* #PIETERSBURG "Pietersburg"
* #PIETERSBURG ^property[0].code = #parent 
* #PIETERSBURG ^property[0].valueCode = #SALMONELLA 
* #PISA "Pisa"
* #PISA ^property[0].code = #parent 
* #PISA ^property[0].valueCode = #SALMONELLA 
* #PLANCKENDAEL "Planckendael"
* #PLANCKENDAEL ^property[0].code = #parent 
* #PLANCKENDAEL ^property[0].valueCode = #SALMONELLA 
* #PLOUFRAGAN "Ploufragan"
* #PLOUFRAGAN ^property[0].code = #parent 
* #PLOUFRAGAN ^property[0].valueCode = #SALMONELLA 
* #PLUMAUGAT "Plumaugat"
* #PLUMAUGAT ^property[0].code = #parent 
* #PLUMAUGAT ^property[0].valueCode = #SALMONELLA 
* #PLYMOUTH "Plymouth"
* #PLYMOUTH ^property[0].code = #parent 
* #PLYMOUTH ^property[0].valueCode = #SALMONELLA 
* #POANO "Poano"
* #POANO ^property[0].code = #parent 
* #POANO ^property[0].valueCode = #SALMONELLA 
* #PODIENSIS "Podiensis"
* #PODIENSIS ^property[0].code = #parent 
* #PODIENSIS ^property[0].valueCode = #SALMONELLA 
* #POESELDORF "Poeseldorf"
* #POESELDORF ^property[0].code = #parent 
* #POESELDORF ^property[0].valueCode = #SALMONELLA 
* #POITIERS "Poitiers"
* #POITIERS ^property[0].code = #parent 
* #POITIERS ^property[0].valueCode = #SALMONELLA 
* #POMONA "Pomona"
* #POMONA ^property[0].code = #parent 
* #POMONA ^property[0].valueCode = #SALMONELLA 
* #PONTYPRIDD "Pontypridd"
* #PONTYPRIDD ^property[0].code = #parent 
* #PONTYPRIDD ^property[0].valueCode = #SALMONELLA 
* #POONA "Poona"
* #POONA ^property[0].code = #parent 
* #POONA ^property[0].valueCode = #SALMONELLA 
* #PORTANIGRA "Portanigra"
* #PORTANIGRA ^property[0].code = #parent 
* #PORTANIGRA ^property[0].valueCode = #SALMONELLA 
* #PORTLAND "Portland"
* #PORTLAND ^property[0].code = #parent 
* #PORTLAND ^property[0].valueCode = #SALMONELLA 
* #POTENGI "Potengi"
* #POTENGI ^property[0].code = #parent 
* #POTENGI ^property[0].valueCode = #SALMONELLA 
* #POTOSI "Potosi"
* #POTOSI ^property[0].code = #parent 
* #POTOSI ^property[0].valueCode = #SALMONELLA 
* #POTSDAM "Potsdam"
* #POTSDAM ^property[0].code = #parent 
* #POTSDAM ^property[0].valueCode = #SALMONELLA 
* #POTTO "Potto"
* #POTTO ^property[0].code = #parent 
* #POTTO ^property[0].valueCode = #SALMONELLA 
* #POWELL "Powell"
* #POWELL ^property[0].code = #parent 
* #POWELL ^property[0].valueCode = #SALMONELLA 
* #PRAHA "Praha"
* #PRAHA ^property[0].code = #parent 
* #PRAHA ^property[0].valueCode = #SALMONELLA 
* #PRAMISO "Pramiso"
* #PRAMISO ^property[0].code = #parent 
* #PRAMISO ^property[0].valueCode = #SALMONELLA 
* #PRESOV "Presov"
* #PRESOV ^property[0].code = #parent 
* #PRESOV ^property[0].valueCode = #SALMONELLA 
* #PRESTON "Preston"
* #PRESTON ^property[0].code = #parent 
* #PRESTON ^property[0].valueCode = #SALMONELLA 
* #PRETORIA "Pretoria"
* #PRETORIA ^property[0].code = #parent 
* #PRETORIA ^property[0].valueCode = #SALMONELLA 
* #PUTTEN "Putten"
* #PUTTEN ^property[0].code = #parent 
* #PUTTEN ^property[0].valueCode = #SALMONELLA 
* #QUEBEC "Quebec"
* #QUEBEC ^property[0].code = #parent 
* #QUEBEC ^property[0].valueCode = #SALMONELLA 
* #QUENTIN "Quentin"
* #QUENTIN ^property[0].code = #parent 
* #QUENTIN ^property[0].valueCode = #SALMONELLA 
* #QUINCY "Quincy"
* #QUINCY ^property[0].code = #parent 
* #QUINCY ^property[0].valueCode = #SALMONELLA 
* #QUINHON "Quinhon"
* #QUINHON ^property[0].code = #parent 
* #QUINHON ^property[0].valueCode = #SALMONELLA 
* #QUINIELA "Quiniela"
* #QUINIELA ^property[0].code = #parent 
* #QUINIELA ^property[0].valueCode = #SALMONELLA 
* #RAMATGAN "Ramatgan"
* #RAMATGAN ^property[0].code = #parent 
* #RAMATGAN ^property[0].valueCode = #SALMONELLA 
* #RAMSEY "Ramsey"
* #RAMSEY ^property[0].code = #parent 
* #RAMSEY ^property[0].valueCode = #SALMONELLA 
* #RATCHABURI "Ratchaburi"
* #RATCHABURI ^property[0].code = #parent 
* #RATCHABURI ^property[0].valueCode = #SALMONELLA 
* #RAUS "Raus"
* #RAUS ^property[0].code = #parent 
* #RAUS ^property[0].valueCode = #SALMONELLA 
* #RAWASH "Rawash"
* #RAWASH ^property[0].code = #parent 
* #RAWASH ^property[0].valueCode = #SALMONELLA 
* #READING "Reading"
* #READING ^property[0].code = #parent 
* #READING ^property[0].valueCode = #SALMONELLA 
* #RECHOVOT "Rechovot"
* #RECHOVOT ^property[0].code = #parent 
* #RECHOVOT ^property[0].valueCode = #SALMONELLA 
* #REDBA "Redba"
* #REDBA ^property[0].code = #parent 
* #REDBA ^property[0].valueCode = #SALMONELLA 
* #REDHILL "Redhill"
* #REDHILL ^property[0].code = #parent 
* #REDHILL ^property[0].valueCode = #SALMONELLA 
* #REDLANDS "Redlands"
* #REDLANDS ^property[0].code = #parent 
* #REDLANDS ^property[0].valueCode = #SALMONELLA 
* #REGENT "Regent"
* #REGENT ^property[0].code = #parent 
* #REGENT ^property[0].valueCode = #SALMONELLA 
* #REINICKENDORF "Reinickendorf"
* #REINICKENDORF ^property[0].code = #parent 
* #REINICKENDORF ^property[0].valueCode = #SALMONELLA 
* #REMETE "Remete"
* #REMETE ^property[0].code = #parent 
* #REMETE ^property[0].valueCode = #SALMONELLA 
* #REMIREMONT "Remiremont"
* #REMIREMONT ^property[0].code = #parent 
* #REMIREMONT ^property[0].valueCode = #SALMONELLA 
* #REMO "Remo"
* #REMO ^property[0].code = #parent 
* #REMO ^property[0].valueCode = #SALMONELLA 
* #REUBEUSS "Reubeuss"
* #REUBEUSS ^property[0].code = #parent 
* #REUBEUSS ^property[0].valueCode = #SALMONELLA 
* #RHONE "Rhone"
* #RHONE ^property[0].code = #parent 
* #RHONE ^property[0].valueCode = #SALMONELLA 
* #RHYDYFELIN "Rhydyfelin"
* #RHYDYFELIN ^property[0].code = #parent 
* #RHYDYFELIN ^property[0].valueCode = #SALMONELLA 
* #RICHMOND "Richmond"
* #RICHMOND ^property[0].code = #parent 
* #RICHMOND ^property[0].valueCode = #SALMONELLA 
* #RIDEAU "Rideau"
* #RIDEAU ^property[0].code = #parent 
* #RIDEAU ^property[0].valueCode = #SALMONELLA 
* #RIDGE "Ridge"
* #RIDGE ^property[0].code = #parent 
* #RIDGE ^property[0].valueCode = #SALMONELLA 
* #RIED "Ried"
* #RIED ^property[0].code = #parent 
* #RIED ^property[0].valueCode = #SALMONELLA 
* #RIGGIL "Riggil"
* #RIGGIL ^property[0].code = #parent 
* #RIGGIL ^property[0].valueCode = #SALMONELLA 
* #RIOGRANDE "Riogrande"
* #RIOGRANDE ^property[0].code = #parent 
* #RIOGRANDE ^property[0].valueCode = #SALMONELLA 
* #RISSEN "Rissen"
* #RISSEN ^property[0].code = #parent 
* #RISSEN ^property[0].valueCode = #SALMONELLA 
* #RISSEN_VAR_14+ "Rissen var. 14+"
* #RISSEN_VAR_14+ ^property[0].code = #parent 
* #RISSEN_VAR_14+ ^property[0].valueCode = #SALMONELLA 
* #RITTERSBACH "Rittersbach"
* #RITTERSBACH ^property[0].code = #parent 
* #RITTERSBACH ^property[0].valueCode = #SALMONELLA 
* #RIVERSIDE "Riverside"
* #RIVERSIDE ^property[0].code = #parent 
* #RIVERSIDE ^property[0].valueCode = #SALMONELLA 
* #ROAN "Roan"
* #ROAN ^property[0].code = #parent 
* #ROAN ^property[0].valueCode = #SALMONELLA 
* #ROCHDALE "Rochdale"
* #ROCHDALE ^property[0].code = #parent 
* #ROCHDALE ^property[0].valueCode = #SALMONELLA 
* #ROGY "Rogy"
* #ROGY ^property[0].code = #parent 
* #ROGY ^property[0].valueCode = #SALMONELLA 
* #ROMANBY "Romanby"
* #ROMANBY ^property[0].code = #parent 
* #ROMANBY ^property[0].valueCode = #SALMONELLA 
* #ROODEPOORT "Roodepoort"
* #ROODEPOORT ^property[0].code = #parent 
* #ROODEPOORT ^property[0].valueCode = #SALMONELLA 
* #ROSENBERG "Rosenberg"
* #ROSENBERG ^property[0].code = #parent 
* #ROSENBERG ^property[0].valueCode = #SALMONELLA 
* #ROSSLEBEN "Rossleben"
* #ROSSLEBEN ^property[0].code = #parent 
* #ROSSLEBEN ^property[0].valueCode = #SALMONELLA 
* #ROSTOCK "Rostock"
* #ROSTOCK ^property[0].code = #parent 
* #ROSTOCK ^property[0].valueCode = #SALMONELLA 
* #ROTHENBURGSORT "Rothenburgsort"
* #ROTHENBURGSORT ^property[0].code = #parent 
* #ROTHENBURGSORT ^property[0].valueCode = #SALMONELLA 
* #ROTTNEST "Rottnest"
* #ROTTNEST ^property[0].code = #parent 
* #ROTTNEST ^property[0].valueCode = #SALMONELLA 
* #ROVANIEMI "Rovaniemi"
* #ROVANIEMI ^property[0].code = #parent 
* #ROVANIEMI ^property[0].valueCode = #SALMONELLA 
* #ROYAN "Royan"
* #ROYAN ^property[0].code = #parent 
* #ROYAN ^property[0].valueCode = #SALMONELLA 
* #RUANDA "Ruanda"
* #RUANDA ^property[0].code = #parent 
* #RUANDA ^property[0].valueCode = #SALMONELLA 
* #RUBISLAW "Rubislaw"
* #RUBISLAW ^property[0].code = #parent 
* #RUBISLAW ^property[0].valueCode = #SALMONELLA 
* #RUIRU "Ruiru"
* #RUIRU ^property[0].code = #parent 
* #RUIRU ^property[0].valueCode = #SALMONELLA 
* #RUMFORD "Rumford"
* #RUMFORD ^property[0].code = #parent 
* #RUMFORD ^property[0].valueCode = #SALMONELLA 
* #RUNBY "Runby"
* #RUNBY ^property[0].code = #parent 
* #RUNBY ^property[0].valueCode = #SALMONELLA 
* #RUZIZI "Ruzizi"
* #RUZIZI ^property[0].code = #parent 
* #RUZIZI ^property[0].valueCode = #SALMONELLA 
* #SAARBRUECKEN "Saarbruecken"
* #SAARBRUECKEN ^property[0].code = #parent 
* #SAARBRUECKEN ^property[0].valueCode = #SALMONELLA 
* #SABOYA "Saboya"
* #SABOYA ^property[0].code = #parent 
* #SABOYA ^property[0].valueCode = #SALMONELLA 
* #SADA "Sada"
* #SADA ^property[0].code = #parent 
* #SADA ^property[0].valueCode = #SALMONELLA 
* #SAINTEMARIE "Saintemarie"
* #SAINTEMARIE ^property[0].code = #parent 
* #SAINTEMARIE ^property[0].valueCode = #SALMONELLA 
* #SAINTPAUL "Saintpaul"
* #SAINTPAUL ^property[0].code = #parent 
* #SAINTPAUL ^property[0].valueCode = #SALMONELLA 
* #SALFORD "Salford"
* #SALFORD ^property[0].code = #parent 
* #SALFORD ^property[0].valueCode = #SALMONELLA 
* #SALINAS "Salinas"
* #SALINAS ^property[0].code = #parent 
* #SALINAS ^property[0].valueCode = #SALMONELLA 
* #SALLY "Sally"
* #SALLY ^property[0].code = #parent 
* #SALLY ^property[0].valueCode = #SALMONELLA 
* #SALONIKI "Saloniki"
* #SALONIKI ^property[0].code = #parent 
* #SALONIKI ^property[0].valueCode = #SALMONELLA 
* #SAMARU "Samaru"
* #SAMARU ^property[0].code = #parent 
* #SAMARU ^property[0].valueCode = #SALMONELLA 
* #SAMBRE "Sambre"
* #SAMBRE ^property[0].code = #parent 
* #SAMBRE ^property[0].valueCode = #SALMONELLA 
* #SANDAGA "Sandaga"
* #SANDAGA ^property[0].code = #parent 
* #SANDAGA ^property[0].valueCode = #SALMONELLA 
* #SANDIEGO "Sandiego"
* #SANDIEGO ^property[0].code = #parent 
* #SANDIEGO ^property[0].valueCode = #SALMONELLA 
* #SANDOW "Sandow"
* #SANDOW ^property[0].code = #parent 
* #SANDOW ^property[0].valueCode = #SALMONELLA 
* #SANGA "Sanga"
* #SANGA ^property[0].code = #parent 
* #SANGA ^property[0].valueCode = #SALMONELLA 
* #SANGALKAM "Sangalkam"
* #SANGALKAM ^property[0].code = #parent 
* #SANGALKAM ^property[0].valueCode = #SALMONELLA 
* #SANGERA "Sangera"
* #SANGERA ^property[0].code = #parent 
* #SANGERA ^property[0].valueCode = #SALMONELLA 
* #SANJUAN "Sanjuan"
* #SANJUAN ^property[0].code = #parent 
* #SANJUAN ^property[0].valueCode = #SALMONELLA 
* #SANKTGEORG "Sanktgeorg"
* #SANKTGEORG ^property[0].code = #parent 
* #SANKTGEORG ^property[0].valueCode = #SALMONELLA 
* #SANKTJOHANN "Sanktjohann"
* #SANKTJOHANN ^property[0].code = #parent 
* #SANKTJOHANN ^property[0].valueCode = #SALMONELLA 
* #SANKTMARX "Sanktmarx"
* #SANKTMARX ^property[0].code = #parent 
* #SANKTMARX ^property[0].valueCode = #SALMONELLA 
* #SANTANDER "Santander"
* #SANTANDER ^property[0].code = #parent 
* #SANTANDER ^property[0].valueCode = #SALMONELLA 
* #SANTHIABA "Santhiaba"
* #SANTHIABA ^property[0].code = #parent 
* #SANTHIABA ^property[0].valueCode = #SALMONELLA 
* #SANTIAGO "Santiago"
* #SANTIAGO ^property[0].code = #parent 
* #SANTIAGO ^property[0].valueCode = #SALMONELLA 
* #SAO "Sao"
* #SAO ^property[0].code = #parent 
* #SAO ^property[0].valueCode = #SALMONELLA 
* #SAPELE "Sapele"
* #SAPELE ^property[0].code = #parent 
* #SAPELE ^property[0].valueCode = #SALMONELLA 
* #SAPHRA "Saphra"
* #SAPHRA ^property[0].code = #parent 
* #SAPHRA ^property[0].valueCode = #SALMONELLA 
* #SARA "Sara"
* #SARA ^property[0].code = #parent 
* #SARA ^property[0].valueCode = #SALMONELLA 
* #SARAJANE "Sarajane"
* #SARAJANE ^property[0].code = #parent 
* #SARAJANE ^property[0].valueCode = #SALMONELLA 
* #SAUGUS "Saugus"
* #SAUGUS ^property[0].code = #parent 
* #SAUGUS ^property[0].valueCode = #SALMONELLA 
* #SCARBOROUGH "Scarborough"
* #SCARBOROUGH ^property[0].code = #parent 
* #SCARBOROUGH ^property[0].valueCode = #SALMONELLA 
* #SCHALKWIJK "Schalkwijk"
* #SCHALKWIJK ^property[0].code = #parent 
* #SCHALKWIJK ^property[0].valueCode = #SALMONELLA 
* #SCHLEISSHEIM "Schleissheim"
* #SCHLEISSHEIM ^property[0].code = #parent 
* #SCHLEISSHEIM ^property[0].valueCode = #SALMONELLA 
* #SCHOENEBERG "Schoeneberg"
* #SCHOENEBERG ^property[0].code = #parent 
* #SCHOENEBERG ^property[0].valueCode = #SALMONELLA 
* #SCHWABACH "Schwabach"
* #SCHWABACH ^property[0].code = #parent 
* #SCHWABACH ^property[0].valueCode = #SALMONELLA 
* #SCHWARZENGRUND "Schwarzengrund"
* #SCHWARZENGRUND ^property[0].code = #parent 
* #SCHWARZENGRUND ^property[0].valueCode = #SALMONELLA 
* #SCHWERIN "Schwerin"
* #SCHWERIN ^property[0].code = #parent 
* #SCHWERIN ^property[0].valueCode = #SALMONELLA 
* #SCULCOATES "Sculcoates"
* #SCULCOATES ^property[0].code = #parent 
* #SCULCOATES ^property[0].valueCode = #SALMONELLA 
* #SEATTLE "Seattle"
* #SEATTLE ^property[0].code = #parent 
* #SEATTLE ^property[0].valueCode = #SALMONELLA 
* #SEDGWICK "Sedgwick"
* #SEDGWICK ^property[0].code = #parent 
* #SEDGWICK ^property[0].valueCode = #SALMONELLA 
* #SEEGEFELD "Seegefeld"
* #SEEGEFELD ^property[0].code = #parent 
* #SEEGEFELD ^property[0].valueCode = #SALMONELLA 
* #SEKONDI "Sekondi"
* #SEKONDI ^property[0].code = #parent 
* #SEKONDI ^property[0].valueCode = #SALMONELLA 
* #SELBY "Selby"
* #SELBY ^property[0].code = #parent 
* #SELBY ^property[0].valueCode = #SALMONELLA 
* #SENDAI "Sendai"
* #SENDAI ^property[0].code = #parent 
* #SENDAI ^property[0].valueCode = #SALMONELLA 
* #SENEGAL "Senegal"
* #SENEGAL ^property[0].code = #parent 
* #SENEGAL ^property[0].valueCode = #SALMONELLA 
* #SENFTENBERG "Senftenberg"
* #SENFTENBERG ^property[0].code = #parent 
* #SENFTENBERG ^property[0].valueCode = #SALMONELLA 
* #SENNEVILLE "Senneville"
* #SENNEVILLE ^property[0].code = #parent 
* #SENNEVILLE ^property[0].valueCode = #SALMONELLA 
* #SEREMBAN "Seremban"
* #SEREMBAN ^property[0].code = #parent 
* #SEREMBAN ^property[0].valueCode = #SALMONELLA 
* #SERREKUNDA "Serrekunda"
* #SERREKUNDA ^property[0].code = #parent 
* #SERREKUNDA ^property[0].valueCode = #SALMONELLA 
* #SHAHALAM "Shahalam"
* #SHAHALAM ^property[0].code = #parent 
* #SHAHALAM ^property[0].valueCode = #SALMONELLA 
* #SHAMBA "Shamba"
* #SHAMBA ^property[0].code = #parent 
* #SHAMBA ^property[0].valueCode = #SALMONELLA 
* #SHANGANI "Shangani"
* #SHANGANI ^property[0].code = #parent 
* #SHANGANI ^property[0].valueCode = #SALMONELLA 
* #SHANGANI_VAR_15+ "Shangani var. 15+"
* #SHANGANI_VAR_15+ ^property[0].code = #parent 
* #SHANGANI_VAR_15+ ^property[0].valueCode = #SALMONELLA 
* #SHANGHAI "Shanghai"
* #SHANGHAI ^property[0].code = #parent 
* #SHANGHAI ^property[0].valueCode = #SALMONELLA 
* #SHANNON "Shannon"
* #SHANNON ^property[0].code = #parent 
* #SHANNON ^property[0].valueCode = #SALMONELLA 
* #SHARON "Sharon"
* #SHARON ^property[0].code = #parent 
* #SHARON ^property[0].valueCode = #SALMONELLA 
* #SHEFFIELD "Sheffield"
* #SHEFFIELD ^property[0].code = #parent 
* #SHEFFIELD ^property[0].valueCode = #SALMONELLA 
* #SHERBROOKE "Sherbrooke"
* #SHERBROOKE ^property[0].code = #parent 
* #SHERBROOKE ^property[0].valueCode = #SALMONELLA 
* #SHIKMONAH "Shikmonah"
* #SHIKMONAH ^property[0].code = #parent 
* #SHIKMONAH ^property[0].valueCode = #SALMONELLA 
* #SHIPLEY "Shipley"
* #SHIPLEY ^property[0].code = #parent 
* #SHIPLEY ^property[0].valueCode = #SALMONELLA 
* #SHOMOLU "Shomolu"
* #SHOMOLU ^property[0].code = #parent 
* #SHOMOLU ^property[0].valueCode = #SALMONELLA 
* #SHOREDITCH "Shoreditch"
* #SHOREDITCH ^property[0].code = #parent 
* #SHOREDITCH ^property[0].valueCode = #SALMONELLA 
* #SHUBRA "Shubra"
* #SHUBRA ^property[0].code = #parent 
* #SHUBRA ^property[0].valueCode = #SALMONELLA 
* #SICA "Sica"
* #SICA ^property[0].code = #parent 
* #SICA ^property[0].valueCode = #SALMONELLA 
* #SIMI "Simi"
* #SIMI ^property[0].code = #parent 
* #SIMI ^property[0].valueCode = #SALMONELLA 
* #SINCHEW "Sinchew"
* #SINCHEW ^property[0].code = #parent 
* #SINCHEW ^property[0].valueCode = #SALMONELLA 
* #SINDELFINGEN "Sindelfingen"
* #SINDELFINGEN ^property[0].code = #parent 
* #SINDELFINGEN ^property[0].valueCode = #SALMONELLA 
* #SINGAPORE "Singapore"
* #SINGAPORE ^property[0].code = #parent 
* #SINGAPORE ^property[0].valueCode = #SALMONELLA 
* #SINSTORF "Sinstorf"
* #SINSTORF ^property[0].code = #parent 
* #SINSTORF ^property[0].valueCode = #SALMONELLA 
* #SINTHIA "Sinthia"
* #SINTHIA ^property[0].code = #parent 
* #SINTHIA ^property[0].valueCode = #SALMONELLA 
* #SIPANE "Sipane"
* #SIPANE ^property[0].code = #parent 
* #SIPANE ^property[0].valueCode = #SALMONELLA 
* #SKANSEN "Skansen"
* #SKANSEN ^property[0].code = #parent 
* #SKANSEN ^property[0].valueCode = #SALMONELLA 
* #SLADE "Slade"
* #SLADE ^property[0].code = #parent 
* #SLADE ^property[0].valueCode = #SALMONELLA 
* #SLJEME "Sljeme"
* #SLJEME ^property[0].code = #parent 
* #SLJEME ^property[0].valueCode = #SALMONELLA 
* #SLOTERDIJK "Sloterdijk"
* #SLOTERDIJK ^property[0].code = #parent 
* #SLOTERDIJK ^property[0].valueCode = #SALMONELLA 
* #SOAHANINA "Soahanina"
* #SOAHANINA ^property[0].code = #parent 
* #SOAHANINA ^property[0].valueCode = #SALMONELLA 
* #SOERENGA "Soerenga"
* #SOERENGA ^property[0].code = #parent 
* #SOERENGA ^property[0].valueCode = #SALMONELLA 
* #SOKODE "Sokode"
* #SOKODE ^property[0].code = #parent 
* #SOKODE ^property[0].valueCode = #SALMONELLA 
* #SOLNA "Solna"
* #SOLNA ^property[0].code = #parent 
* #SOLNA ^property[0].valueCode = #SALMONELLA 
* #SOLT "Solt"
* #SOLT ^property[0].code = #parent 
* #SOLT ^property[0].valueCode = #SALMONELLA 
* #SOMONE "Somone"
* #SOMONE ^property[0].code = #parent 
* #SOMONE ^property[0].valueCode = #SALMONELLA 
* #SONTHEIM "Sontheim"
* #SONTHEIM ^property[0].code = #parent 
* #SONTHEIM ^property[0].valueCode = #SALMONELLA 
* #SOUMBEDIOUNE "Soumbedioune"
* #SOUMBEDIOUNE ^property[0].code = #parent 
* #SOUMBEDIOUNE ^property[0].valueCode = #SALMONELLA 
* #SOUTHAMPTON "Southampton"
* #SOUTHAMPTON ^property[0].code = #parent 
* #SOUTHAMPTON ^property[0].valueCode = #SALMONELLA 
* #SOUTHBANK "Southbank"
* #SOUTHBANK ^property[0].code = #parent 
* #SOUTHBANK ^property[0].valueCode = #SALMONELLA 
* #SOUZA "Souza"
* #SOUZA ^property[0].code = #parent 
* #SOUZA ^property[0].valueCode = #SALMONELLA 
* #SOUZA_VAR_15+ "Souza var. 15+"
* #SOUZA_VAR_15+ ^property[0].code = #parent 
* #SOUZA_VAR_15+ ^property[0].valueCode = #SALMONELLA 
* #SPALENTOR "Spalentor"
* #SPALENTOR ^property[0].code = #parent 
* #SPALENTOR ^property[0].valueCode = #SALMONELLA 
* #SPARTEL "Spartel"
* #SPARTEL ^property[0].code = #parent 
* #SPARTEL ^property[0].valueCode = #SALMONELLA 
* #SPLOTT "Splott"
* #SPLOTT ^property[0].code = #parent 
* #SPLOTT ^property[0].valueCode = #SALMONELLA 
* #STACHUS "Stachus"
* #STACHUS ^property[0].code = #parent 
* #STACHUS ^property[0].valueCode = #SALMONELLA 
* #STANLEY "Stanley"
* #STANLEY ^property[0].code = #parent 
* #STANLEY ^property[0].valueCode = #SALMONELLA 
* #STANLEYVILLE "Stanleyville"
* #STANLEYVILLE ^property[0].code = #parent 
* #STANLEYVILLE ^property[0].valueCode = #SALMONELLA 
* #STAOUELI "Staoueli"
* #STAOUELI ^property[0].code = #parent 
* #STAOUELI ^property[0].valueCode = #SALMONELLA 
* #STEINPLATZ "Steinplatz"
* #STEINPLATZ ^property[0].code = #parent 
* #STEINPLATZ ^property[0].valueCode = #SALMONELLA 
* #STEINWERDER "Steinwerder"
* #STEINWERDER ^property[0].code = #parent 
* #STEINWERDER ^property[0].valueCode = #SALMONELLA 
* #STELLINGEN "Stellingen"
* #STELLINGEN ^property[0].code = #parent 
* #STELLINGEN ^property[0].valueCode = #SALMONELLA 
* #STENDAL "Stendal"
* #STENDAL ^property[0].code = #parent 
* #STENDAL ^property[0].valueCode = #SALMONELLA 
* #STERNSCHANZE "Sternschanze"
* #STERNSCHANZE ^property[0].code = #parent 
* #STERNSCHANZE ^property[0].valueCode = #SALMONELLA 
* #STERRENBOS "Sterrenbos"
* #STERRENBOS ^property[0].code = #parent 
* #STERRENBOS ^property[0].valueCode = #SALMONELLA 
* #STOCKHOLM "Stockholm"
* #STOCKHOLM ^property[0].code = #parent 
* #STOCKHOLM ^property[0].valueCode = #SALMONELLA 
* #STOCKHOLM_VAR_15+ "Stockholm var. 15+"
* #STOCKHOLM_VAR_15+ ^property[0].code = #parent 
* #STOCKHOLM_VAR_15+ ^property[0].valueCode = #SALMONELLA 
* #STONEFERRY "Stoneferry"
* #STONEFERRY ^property[0].code = #parent 
* #STONEFERRY ^property[0].valueCode = #SALMONELLA 
* #STORMONT "Stormont"
* #STORMONT ^property[0].code = #parent 
* #STORMONT ^property[0].valueCode = #SALMONELLA 
* #STOURBRIDGE "Stourbridge"
* #STOURBRIDGE ^property[0].code = #parent 
* #STOURBRIDGE ^property[0].valueCode = #SALMONELLA 
* #STRAENGNAES "Straengnaes"
* #STRAENGNAES ^property[0].code = #parent 
* #STRAENGNAES ^property[0].valueCode = #SALMONELLA 
* #STRASBOURG "Strasbourg"
* #STRASBOURG ^property[0].code = #parent 
* #STRASBOURG ^property[0].valueCode = #SALMONELLA 
* #STRATFORD "Stratford"
* #STRATFORD ^property[0].code = #parent 
* #STRATFORD ^property[0].valueCode = #SALMONELLA 
* #STRATHCONA "Strathcona"
* #STRATHCONA ^property[0].code = #parent 
* #STRATHCONA ^property[0].valueCode = #SALMONELLA 
* #STUIVENBERG "Stuivenberg"
* #STUIVENBERG ^property[0].code = #parent 
* #STUIVENBERG ^property[0].valueCode = #SALMONELLA 
* #STUTTGART "Stuttgart"
* #STUTTGART ^property[0].code = #parent 
* #STUTTGART ^property[0].valueCode = #SALMONELLA 
* #SUBERU "Suberu"
* #SUBERU ^property[0].code = #parent 
* #SUBERU ^property[0].valueCode = #SALMONELLA 
* #SUBSPIIIA "Subspecies IIIa (arizonae)"
* #SUBSPIIIA ^property[0].code = #parent 
* #SUBSPIIIA ^property[0].valueCode = #SALMONELLA 
* #SUBSPIIIB "Subspecies IIIb (diarizonae)"
* #SUBSPIIIB ^property[0].code = #parent 
* #SUBSPIIIB ^property[0].valueCode = #SALMONELLA 
* #SUBSPII_ "Subspecies II (salamae)"
* #SUBSPII_ ^property[0].code = #parent 
* #SUBSPII_ ^property[0].valueCode = #SALMONELLA 
* #SUBSPIV "Subspecies IV (houtenae)"
* #SUBSPIV ^property[0].code = #parent 
* #SUBSPIV ^property[0].valueCode = #SALMONELLA 
* #SUBSPI_ "Subspecies I (enterica)"
* #SUBSPI_ ^property[0].code = #parent 
* #SUBSPI_ ^property[0].valueCode = #SALMONELLA 
* #SUBSPV "Subspecies V (bongori, own specie)"
* #SUBSPV ^property[0].code = #parent 
* #SUBSPV ^property[0].valueCode = #SALMONELLA 
* #SUBSPVI "Subspecies VI (indica)"
* #SUBSPVI ^property[0].code = #parent 
* #SUBSPVI ^property[0].valueCode = #SALMONELLA 
* #SUDAN "Sudan"
* #SUDAN ^property[0].code = #parent 
* #SUDAN ^property[0].valueCode = #SALMONELLA 
* #SUELLDORF "Suelldorf"
* #SUELLDORF ^property[0].code = #parent 
* #SUELLDORF ^property[0].valueCode = #SALMONELLA 
* #SUNDSVALL "Sundsvall"
* #SUNDSVALL ^property[0].code = #parent 
* #SUNDSVALL ^property[0].valueCode = #SALMONELLA 
* #SUNNYCOVE "Sunnycove"
* #SUNNYCOVE ^property[0].code = #parent 
* #SUNNYCOVE ^property[0].valueCode = #SALMONELLA 
* #SURAT "Surat"
* #SURAT ^property[0].code = #parent 
* #SURAT ^property[0].valueCode = #SALMONELLA 
* #SURREY "Surrey"
* #SURREY ^property[0].code = #parent 
* #SURREY ^property[0].valueCode = #SALMONELLA 
* #SVEDVI "Svedvi"
* #SVEDVI ^property[0].code = #parent 
* #SVEDVI ^property[0].valueCode = #SALMONELLA 
* #SYA "Sya"
* #SYA ^property[0].code = #parent 
* #SYA ^property[0].valueCode = #SALMONELLA 
* #SYLVANIA "Sylvania"
* #SYLVANIA ^property[0].code = #parent 
* #SYLVANIA ^property[0].valueCode = #SALMONELLA 
* #SZENTES "Szentes"
* #SZENTES ^property[0].code = #parent 
* #SZENTES ^property[0].valueCode = #SALMONELLA 
* #TABLIGBO "Tabligbo"
* #TABLIGBO ^property[0].code = #parent 
* #TABLIGBO ^property[0].valueCode = #SALMONELLA 
* #TADO "Tado"
* #TADO ^property[0].code = #parent 
* #TADO ^property[0].valueCode = #SALMONELLA 
* #TAFO "Tafo"
* #TAFO ^property[0].code = #parent 
* #TAFO ^property[0].valueCode = #SALMONELLA 
* #TAIPING "Taiping"
* #TAIPING ^property[0].code = #parent 
* #TAIPING ^property[0].valueCode = #SALMONELLA 
* #TAKORADI "Takoradi"
* #TAKORADI ^property[0].code = #parent 
* #TAKORADI ^property[0].valueCode = #SALMONELLA 
* #TAKSONY "Taksony"
* #TAKSONY ^property[0].code = #parent 
* #TAKSONY ^property[0].valueCode = #SALMONELLA 
* #TALLAHASSEE "Tallahassee"
* #TALLAHASSEE ^property[0].code = #parent 
* #TALLAHASSEE ^property[0].valueCode = #SALMONELLA 
* #TAMALE "Tamale"
* #TAMALE ^property[0].code = #parent 
* #TAMALE ^property[0].valueCode = #SALMONELLA 
* #TAMBACOUNDA "Tambacounda"
* #TAMBACOUNDA ^property[0].code = #parent 
* #TAMBACOUNDA ^property[0].valueCode = #SALMONELLA 
* #TAMBERMA "Tamberma"
* #TAMBERMA ^property[0].code = #parent 
* #TAMBERMA ^property[0].valueCode = #SALMONELLA 
* #TAMILNADU "Tamilnadu"
* #TAMILNADU ^property[0].code = #parent 
* #TAMILNADU ^property[0].valueCode = #SALMONELLA 
* #TAMPICO "Tampico"
* #TAMPICO ^property[0].code = #parent 
* #TAMPICO ^property[0].valueCode = #SALMONELLA 
* #TANANARIVE "Tananarive"
* #TANANARIVE ^property[0].code = #parent 
* #TANANARIVE ^property[0].valueCode = #SALMONELLA 
* #TANGER "Tanger"
* #TANGER ^property[0].code = #parent 
* #TANGER ^property[0].valueCode = #SALMONELLA 
* #TANZANIA "Tanzania"
* #TANZANIA ^property[0].code = #parent 
* #TANZANIA ^property[0].valueCode = #SALMONELLA 
* #TARSHYNE "Tarshyne"
* #TARSHYNE ^property[0].code = #parent 
* #TARSHYNE ^property[0].valueCode = #SALMONELLA 
* #TASET "Taset"
* #TASET ^property[0].code = #parent 
* #TASET ^property[0].valueCode = #SALMONELLA 
* #TAUNTON "Taunton"
* #TAUNTON ^property[0].code = #parent 
* #TAUNTON ^property[0].valueCode = #SALMONELLA 
* #TAYLOR "Taylor"
* #TAYLOR ^property[0].code = #parent 
* #TAYLOR ^property[0].valueCode = #SALMONELLA 
* #TCHAD "Tchad"
* #TCHAD ^property[0].code = #parent 
* #TCHAD ^property[0].valueCode = #SALMONELLA 
* #TCHAMBA "Tchamba"
* #TCHAMBA ^property[0].code = #parent 
* #TCHAMBA ^property[0].valueCode = #SALMONELLA 
* #TECHIMANI "Techimani"
* #TECHIMANI ^property[0].code = #parent 
* #TECHIMANI ^property[0].valueCode = #SALMONELLA 
* #TEDDINGTON "Teddington"
* #TEDDINGTON ^property[0].code = #parent 
* #TEDDINGTON ^property[0].valueCode = #SALMONELLA 
* #TEES "Tees"
* #TEES ^property[0].code = #parent 
* #TEES ^property[0].valueCode = #SALMONELLA 
* #TEJAS "Tejas"
* #TEJAS ^property[0].code = #parent 
* #TEJAS ^property[0].valueCode = #SALMONELLA 
* #TEKO "Teko"
* #TEKO ^property[0].code = #parent 
* #TEKO ^property[0].valueCode = #SALMONELLA 
* #TELAVIV "Telaviv"
* #TELAVIV ^property[0].code = #parent 
* #TELAVIV ^property[0].valueCode = #SALMONELLA 
* #TELELKEBIR "Telelkebir"
* #TELELKEBIR ^property[0].code = #parent 
* #TELELKEBIR ^property[0].valueCode = #SALMONELLA 
* #TELHASHOMER "Telhashomer"
* #TELHASHOMER ^property[0].code = #parent 
* #TELHASHOMER ^property[0].valueCode = #SALMONELLA 
* #TELTOW "Teltow"
* #TELTOW ^property[0].code = #parent 
* #TELTOW ^property[0].valueCode = #SALMONELLA 
* #TEMA "Tema"
* #TEMA ^property[0].code = #parent 
* #TEMA ^property[0].valueCode = #SALMONELLA 
* #TEMPE "Tempe"
* #TEMPE ^property[0].code = #parent 
* #TEMPE ^property[0].valueCode = #SALMONELLA 
* #TENDEBA "Tendeba"
* #TENDEBA ^property[0].code = #parent 
* #TENDEBA ^property[0].valueCode = #SALMONELLA 
* #TENNENLOHE "Tennenlohe"
* #TENNENLOHE ^property[0].code = #parent 
* #TENNENLOHE ^property[0].valueCode = #SALMONELLA 
* #TENNESSEE "Tennessee"
* #TENNESSEE ^property[0].code = #parent 
* #TENNESSEE ^property[0].valueCode = #SALMONELLA 
* #TENNYSON "Tennyson"
* #TENNYSON ^property[0].code = #parent 
* #TENNYSON ^property[0].valueCode = #SALMONELLA 
* #TESHIE "Teshie"
* #TESHIE ^property[0].code = #parent 
* #TESHIE ^property[0].valueCode = #SALMONELLA 
* #TEXAS "Texas"
* #TEXAS ^property[0].code = #parent 
* #TEXAS ^property[0].valueCode = #SALMONELLA 
* #THAYNGEN "Thayngen"
* #THAYNGEN ^property[0].code = #parent 
* #THAYNGEN ^property[0].valueCode = #SALMONELLA 
* #THETFORD "Thetford"
* #THETFORD ^property[0].code = #parent 
* #THETFORD ^property[0].valueCode = #SALMONELLA 
* #THIAROYE "Thiaroye"
* #THIAROYE ^property[0].code = #parent 
* #THIAROYE ^property[0].valueCode = #SALMONELLA 
* #THIES "Thies"
* #THIES ^property[0].code = #parent 
* #THIES ^property[0].valueCode = #SALMONELLA 
* #THOMPSON "Thompson"
* #THOMPSON ^property[0].code = #parent 
* #THOMPSON ^property[0].valueCode = #SALMONELLA 
* #TIBATI "Tibati"
* #TIBATI ^property[0].code = #parent 
* #TIBATI ^property[0].valueCode = #SALMONELLA 
* #TIENBA "Tienba"
* #TIENBA ^property[0].code = #parent 
* #TIENBA ^property[0].valueCode = #SALMONELLA 
* #TIERGARTEN "Tiergarten"
* #TIERGARTEN ^property[0].code = #parent 
* #TIERGARTEN ^property[0].valueCode = #SALMONELLA 
* #TIKO "Tiko"
* #TIKO ^property[0].code = #parent 
* #TIKO ^property[0].valueCode = #SALMONELLA 
* #TILBURG "Tilburg"
* #TILBURG ^property[0].code = #parent 
* #TILBURG ^property[0].valueCode = #SALMONELLA 
* #TILENE "Tilene"
* #TILENE ^property[0].code = #parent 
* #TILENE ^property[0].valueCode = #SALMONELLA 
* #TINDA "Tinda"
* #TINDA ^property[0].code = #parent 
* #TINDA ^property[0].valueCode = #SALMONELLA 
* #TIONE "Tione"
* #TIONE ^property[0].code = #parent 
* #TIONE ^property[0].valueCode = #SALMONELLA 
* #TOGBA "Togba"
* #TOGBA ^property[0].code = #parent 
* #TOGBA ^property[0].valueCode = #SALMONELLA 
* #TOGO "Togo"
* #TOGO ^property[0].code = #parent 
* #TOGO ^property[0].valueCode = #SALMONELLA 
* #TOKOIN "Tokoin"
* #TOKOIN ^property[0].code = #parent 
* #TOKOIN ^property[0].valueCode = #SALMONELLA 
* #TOMEGBE "Tomegbe"
* #TOMEGBE ^property[0].code = #parent 
* #TOMEGBE ^property[0].valueCode = #SALMONELLA 
* #TOMELILLA "Tomelilla"
* #TOMELILLA ^property[0].code = #parent 
* #TOMELILLA ^property[0].valueCode = #SALMONELLA 
* #TONEV "Tonev"
* #TONEV ^property[0].code = #parent 
* #TONEV ^property[0].valueCode = #SALMONELLA 
* #TOOWONG "Toowong"
* #TOOWONG ^property[0].code = #parent 
* #TOOWONG ^property[0].valueCode = #SALMONELLA 
* #TORHOUT "Torhout"
* #TORHOUT ^property[0].code = #parent 
* #TORHOUT ^property[0].valueCode = #SALMONELLA 
* #TORICADA "Toricada"
* #TORICADA ^property[0].code = #parent 
* #TORICADA ^property[0].valueCode = #SALMONELLA 
* #TORNOW "Tornow"
* #TORNOW ^property[0].code = #parent 
* #TORNOW ^property[0].valueCode = #SALMONELLA 
* #TORONTO "Toronto"
* #TORONTO ^property[0].code = #parent 
* #TORONTO ^property[0].valueCode = #SALMONELLA 
* #TOUCRA "Toucra"
* #TOUCRA ^property[0].code = #parent 
* #TOUCRA ^property[0].valueCode = #SALMONELLA 
* #TOULON "Toulon"
* #TOULON ^property[0].code = #parent 
* #TOULON ^property[0].valueCode = #SALMONELLA 
* #TOUNOUMA "Tounouma"
* #TOUNOUMA ^property[0].code = #parent 
* #TOUNOUMA ^property[0].valueCode = #SALMONELLA 
* #TOURS "Tours"
* #TOURS ^property[0].code = #parent 
* #TOURS ^property[0].valueCode = #SALMONELLA 
* #TRACHAU "Trachau"
* #TRACHAU ^property[0].code = #parent 
* #TRACHAU ^property[0].valueCode = #SALMONELLA 
* #TRANSVAAL "Transvaal"
* #TRANSVAAL ^property[0].code = #parent 
* #TRANSVAAL ^property[0].valueCode = #SALMONELLA 
* #TRAVIS "Travis"
* #TRAVIS ^property[0].code = #parent 
* #TRAVIS ^property[0].valueCode = #SALMONELLA 
* #TREFOREST "Treforest"
* #TREFOREST ^property[0].code = #parent 
* #TREFOREST ^property[0].valueCode = #SALMONELLA 
* #TREGUIER "Treguier"
* #TREGUIER ^property[0].code = #parent 
* #TREGUIER ^property[0].valueCode = #SALMONELLA 
* #TRIER "Trier"
* #TRIER ^property[0].code = #parent 
* #TRIER ^property[0].valueCode = #SALMONELLA 
* #TRIMDON "Trimdon"
* #TRIMDON ^property[0].code = #parent 
* #TRIMDON ^property[0].valueCode = #SALMONELLA 
* #TRIPOLI "Tripoli"
* #TRIPOLI ^property[0].code = #parent 
* #TRIPOLI ^property[0].valueCode = #SALMONELLA 
* #TROTHA "Trotha"
* #TROTHA ^property[0].code = #parent 
* #TROTHA ^property[0].valueCode = #SALMONELLA 
* #TROY "Troy"
* #TROY ^property[0].code = #parent 
* #TROY ^property[0].valueCode = #SALMONELLA 
* #TRURO "Truro"
* #TRURO ^property[0].code = #parent 
* #TRURO ^property[0].valueCode = #SALMONELLA 
* #TSCHANGU "Tschangu"
* #TSCHANGU ^property[0].code = #parent 
* #TSCHANGU ^property[0].valueCode = #SALMONELLA 
* #TSEVIE "Tsevie"
* #TSEVIE ^property[0].code = #parent 
* #TSEVIE ^property[0].valueCode = #SALMONELLA 
* #TSHIONGWE "Tshiongwe"
* #TSHIONGWE ^property[0].code = #parent 
* #TSHIONGWE ^property[0].valueCode = #SALMONELLA 
* #TUCSON "Tucson"
* #TUCSON ^property[0].code = #parent 
* #TUCSON ^property[0].valueCode = #SALMONELLA 
* #TUDU "Tudu"
* #TUDU ^property[0].code = #parent 
* #TUDU ^property[0].valueCode = #SALMONELLA 
* #TUMODI "Tumodi"
* #TUMODI ^property[0].code = #parent 
* #TUMODI ^property[0].valueCode = #SALMONELLA 
* #TUNIS "Tunis"
* #TUNIS ^property[0].code = #parent 
* #TUNIS ^property[0].valueCode = #SALMONELLA 
* #TYPHIMURIUM "Typhimurium"
* #TYPHIMURIUM ^property[0].code = #parent 
* #TYPHIMURIUM ^property[0].valueCode = #SALMONELLA 
* #TYPHIMURIUM_VAR_O_5- "Typhimurium var. O:5-"
* #TYPHIMURIUM_VAR_O_5- ^property[0].code = #parent 
* #TYPHIMURIUM_VAR_O_5- ^property[0].valueCode = #SALMONELLA 
* #TYPHISUIS "Typhisuis"
* #TYPHISUIS ^property[0].code = #parent 
* #TYPHISUIS ^property[0].valueCode = #SALMONELLA 
* #TYRESOE "Tyresoe"
* #TYRESOE ^property[0].code = #parent 
* #TYRESOE ^property[0].valueCode = #SALMONELLA 
* #UCCLE "Uccle"
* #UCCLE ^property[0].code = #parent 
* #UCCLE ^property[0].valueCode = #SALMONELLA 
* #UGANDA "Uganda"
* #UGANDA ^property[0].code = #parent 
* #UGANDA ^property[0].valueCode = #SALMONELLA 
* #UGANDA_VAR_15+_ "Uganda var. 15+"
* #UGANDA_VAR_15+_ ^property[0].code = #parent 
* #UGANDA_VAR_15+_ ^property[0].valueCode = #SALMONELLA 
* #UGHELLI "Ughelli"
* #UGHELLI ^property[0].code = #parent 
* #UGHELLI ^property[0].valueCode = #SALMONELLA 
* #UHLENHORST "Uhlenhorst"
* #UHLENHORST ^property[0].code = #parent 
* #UHLENHORST ^property[0].valueCode = #SALMONELLA 
* #UITHOF "Uithof"
* #UITHOF ^property[0].code = #parent 
* #UITHOF ^property[0].valueCode = #SALMONELLA 
* #ULLEVI "Ullevi"
* #ULLEVI ^property[0].code = #parent 
* #ULLEVI ^property[0].valueCode = #SALMONELLA 
* #UMBADAH "Umbadah"
* #UMBADAH ^property[0].code = #parent 
* #UMBADAH ^property[0].valueCode = #SALMONELLA 
* #UMBILO "Umbilo"
* #UMBILO ^property[0].code = #parent 
* #UMBILO ^property[0].valueCode = #SALMONELLA 
* #UMHLALI "Umhlali"
* #UMHLALI ^property[0].code = #parent 
* #UMHLALI ^property[0].valueCode = #SALMONELLA 
* #UMHLATAZANA "Umhlatazana"
* #UMHLATAZANA ^property[0].code = #parent 
* #UMHLATAZANA ^property[0].valueCode = #SALMONELLA 
* #UNK "Unknown"
* #UNK ^property[0].code = #parent 
* #UNK ^property[0].valueCode = #SALMONELLA 
* #UNO "Uno"
* #UNO ^property[0].code = #parent 
* #UNO ^property[0].valueCode = #SALMONELLA 
* #UPPSALA "Uppsala"
* #UPPSALA ^property[0].code = #parent 
* #UPPSALA ^property[0].valueCode = #SALMONELLA 
* #URBANA "Urbana"
* #URBANA ^property[0].code = #parent 
* #URBANA ^property[0].valueCode = #SALMONELLA 
* #URSENBACH "Ursenbach"
* #URSENBACH ^property[0].code = #parent 
* #URSENBACH ^property[0].valueCode = #SALMONELLA 
* #USUMBURA "Usumbura"
* #USUMBURA ^property[0].code = #parent 
* #USUMBURA ^property[0].valueCode = #SALMONELLA 
* #UTAH "Utah"
* #UTAH ^property[0].code = #parent 
* #UTAH ^property[0].valueCode = #SALMONELLA 
* #UTRECHT "Utrecht"
* #UTRECHT ^property[0].code = #parent 
* #UTRECHT ^property[0].valueCode = #SALMONELLA 
* #UZARAMO "Uzaramo"
* #UZARAMO ^property[0].code = #parent 
* #UZARAMO ^property[0].valueCode = #SALMONELLA 
* #VAERTAN "Vaertan"
* #VAERTAN ^property[0].code = #parent 
* #VAERTAN ^property[0].valueCode = #SALMONELLA 
* #VALDOSTA "Valdosta"
* #VALDOSTA ^property[0].code = #parent 
* #VALDOSTA ^property[0].valueCode = #SALMONELLA 
* #VANCOUVER "Vancouver"
* #VANCOUVER ^property[0].code = #parent 
* #VANCOUVER ^property[0].valueCode = #SALMONELLA 
* #VANIER "Vanier"
* #VANIER ^property[0].code = #parent 
* #VANIER ^property[0].valueCode = #SALMONELLA 
* #VAUGIRARD "Vaugirard"
* #VAUGIRARD ^property[0].code = #parent 
* #VAUGIRARD ^property[0].valueCode = #SALMONELLA 
* #VEGESACK "Vegesack"
* #VEGESACK ^property[0].code = #parent 
* #VEGESACK ^property[0].valueCode = #SALMONELLA 
* #VEJLE "Vejle"
* #VEJLE ^property[0].code = #parent 
* #VEJLE ^property[0].valueCode = #SALMONELLA 
* #VEJLE_VAR_15+ "Vejle var. 15+"
* #VEJLE_VAR_15+ ^property[0].code = #parent 
* #VEJLE_VAR_15+ ^property[0].valueCode = #SALMONELLA 
* #VELLORE "Vellore"
* #VELLORE ^property[0].code = #parent 
* #VELLORE ^property[0].valueCode = #SALMONELLA 
* #VENEZIANA "Veneziana"
* #VENEZIANA ^property[0].code = #parent 
* #VENEZIANA ^property[0].valueCode = #SALMONELLA 
* #VERONA "Verona"
* #VERONA ^property[0].code = #parent 
* #VERONA ^property[0].valueCode = #SALMONELLA 
* #VERVIERS "Verviers"
* #VERVIERS ^property[0].code = #parent 
* #VERVIERS ^property[0].valueCode = #SALMONELLA 
* #VICTORIA "Victoria"
* #VICTORIA ^property[0].code = #parent 
* #VICTORIA ^property[0].valueCode = #SALMONELLA 
* #VICTORIABORG "Victoriaborg"
* #VICTORIABORG ^property[0].code = #parent 
* #VICTORIABORG ^property[0].valueCode = #SALMONELLA 
* #VIETNAM "Vietnam"
* #VIETNAM ^property[0].code = #parent 
* #VIETNAM ^property[0].valueCode = #SALMONELLA 
* #VILVOORDE "Vilvoorde"
* #VILVOORDE ^property[0].code = #parent 
* #VILVOORDE ^property[0].valueCode = #SALMONELLA 
* #VINOHRADY "Vinohrady"
* #VINOHRADY ^property[0].code = #parent 
* #VINOHRADY ^property[0].valueCode = #SALMONELLA 
* #VIRCHOW "Virchow"
* #VIRCHOW ^property[0].code = #parent 
* #VIRCHOW ^property[0].valueCode = #SALMONELLA 
* #VIRGINIA "Virginia"
* #VIRGINIA ^property[0].code = #parent 
* #VIRGINIA ^property[0].valueCode = #SALMONELLA 
* #VISBY "Visby"
* #VISBY ^property[0].code = #parent 
* #VISBY ^property[0].valueCode = #SALMONELLA 
* #VITKIN "Vitkin"
* #VITKIN ^property[0].code = #parent 
* #VITKIN ^property[0].valueCode = #SALMONELLA 
* #VLEUTEN "Vleuten"
* #VLEUTEN ^property[0].code = #parent 
* #VLEUTEN ^property[0].valueCode = #SALMONELLA 
* #VOGAN "Vogan"
* #VOGAN ^property[0].code = #parent 
* #VOGAN ^property[0].valueCode = #SALMONELLA 
* #VOLKSMARSDORF "Volksmarsdorf"
* #VOLKSMARSDORF ^property[0].code = #parent 
* #VOLKSMARSDORF ^property[0].valueCode = #SALMONELLA 
* #VOLTA "Volta"
* #VOLTA ^property[0].code = #parent 
* #VOLTA ^property[0].valueCode = #SALMONELLA 
* #VOM "Vom"
* #VOM ^property[0].code = #parent 
* #VOM ^property[0].valueCode = #SALMONELLA 
* #VOULTE "Voulte"
* #VOULTE ^property[0].code = #parent 
* #VOULTE ^property[0].valueCode = #SALMONELLA 
* #VRIDI "Vridi"
* #VRIDI ^property[0].code = #parent 
* #VRIDI ^property[0].valueCode = #SALMONELLA 
* #VUADENS "Vuadens"
* #VUADENS ^property[0].code = #parent 
* #VUADENS ^property[0].valueCode = #SALMONELLA 
* #WA "Wa"
* #WA ^property[0].code = #parent 
* #WA ^property[0].valueCode = #SALMONELLA 
* #WAEDENSWIL "Waedenswil"
* #WAEDENSWIL ^property[0].code = #parent 
* #WAEDENSWIL ^property[0].valueCode = #SALMONELLA 
* #WAGADUGU "Wagadugu"
* #WAGADUGU ^property[0].code = #parent 
* #WAGADUGU ^property[0].valueCode = #SALMONELLA 
* #WAGENIA "Wagenia"
* #WAGENIA ^property[0].code = #parent 
* #WAGENIA ^property[0].valueCode = #SALMONELLA 
* #WANATAH "Wanatah"
* #WANATAH ^property[0].code = #parent 
* #WANATAH ^property[0].valueCode = #SALMONELLA 
* #WANDSWORTH "Wandsworth"
* #WANDSWORTH ^property[0].code = #parent 
* #WANDSWORTH ^property[0].valueCode = #SALMONELLA 
* #WANGATA "Wangata"
* #WANGATA ^property[0].code = #parent 
* #WANGATA ^property[0].valueCode = #SALMONELLA 
* #WARAL "Waral"
* #WARAL ^property[0].code = #parent 
* #WARAL ^property[0].valueCode = #SALMONELLA 
* #WARENGO "Warengo"
* #WARENGO ^property[0].code = #parent 
* #WARENGO ^property[0].valueCode = #SALMONELLA 
* #WARMSEN "Warmsen"
* #WARMSEN ^property[0].code = #parent 
* #WARMSEN ^property[0].valueCode = #SALMONELLA 
* #WARNEMUENDE "Warnemuende"
* #WARNEMUENDE ^property[0].code = #parent 
* #WARNEMUENDE ^property[0].valueCode = #SALMONELLA 
* #WARNOW "Warnow"
* #WARNOW ^property[0].code = #parent 
* #WARNOW ^property[0].valueCode = #SALMONELLA 
* #WARRAGUL "Warragul"
* #WARRAGUL ^property[0].code = #parent 
* #WARRAGUL ^property[0].valueCode = #SALMONELLA 
* #WARRI "Warri"
* #WARRI ^property[0].code = #parent 
* #WARRI ^property[0].valueCode = #SALMONELLA 
* #WASHINGTON "Washington"
* #WASHINGTON ^property[0].code = #parent 
* #WASHINGTON ^property[0].valueCode = #SALMONELLA 
* #WAYCROSS "Waycross"
* #WAYCROSS ^property[0].code = #parent 
* #WAYCROSS ^property[0].valueCode = #SALMONELLA 
* #WAYNE "Wayne"
* #WAYNE ^property[0].code = #parent 
* #WAYNE ^property[0].valueCode = #SALMONELLA 
* #WEDDING "Wedding"
* #WEDDING ^property[0].code = #parent 
* #WEDDING ^property[0].valueCode = #SALMONELLA 
* #WELIKADE "Welikade"
* #WELIKADE ^property[0].code = #parent 
* #WELIKADE ^property[0].valueCode = #SALMONELLA 
* #WELTEVREDEN "Weltevreden"
* #WELTEVREDEN ^property[0].code = #parent 
* #WELTEVREDEN ^property[0].valueCode = #SALMONELLA 
* #WELTEVREDEN_VAR_15+_ "Weltevreden var. 15+"
* #WELTEVREDEN_VAR_15+_ ^property[0].code = #parent 
* #WELTEVREDEN_VAR_15+_ ^property[0].valueCode = #SALMONELLA 
* #WENATCHEE "Wenatchee"
* #WENATCHEE ^property[0].code = #parent 
* #WENATCHEE ^property[0].valueCode = #SALMONELLA 
* #WENTWORTH "Wentworth"
* #WENTWORTH ^property[0].code = #parent 
* #WENTWORTH ^property[0].valueCode = #SALMONELLA 
* #WERNIGERODE "Wernigerode"
* #WERNIGERODE ^property[0].code = #parent 
* #WERNIGERODE ^property[0].valueCode = #SALMONELLA 
* #WESLACO "Weslaco"
* #WESLACO ^property[0].code = #parent 
* #WESLACO ^property[0].valueCode = #SALMONELLA 
* #WESTAFRICA "Westafrica"
* #WESTAFRICA ^property[0].code = #parent 
* #WESTAFRICA ^property[0].valueCode = #SALMONELLA 
* #WESTEINDE "Westeinde"
* #WESTEINDE ^property[0].code = #parent 
* #WESTEINDE ^property[0].valueCode = #SALMONELLA 
* #WESTERSTEDE "Westerstede"
* #WESTERSTEDE ^property[0].code = #parent 
* #WESTERSTEDE ^property[0].valueCode = #SALMONELLA 
* #WESTHAMPTON "Westhampton"
* #WESTHAMPTON ^property[0].code = #parent 
* #WESTHAMPTON ^property[0].valueCode = #SALMONELLA 
* #WESTHAMPTON_VAR_15+ "Westhampton var. 15+"
* #WESTHAMPTON_VAR_15+ ^property[0].code = #parent 
* #WESTHAMPTON_VAR_15+ ^property[0].valueCode = #SALMONELLA 
* #WESTHAMPTON_VAR_15+_34+_ "Westhampton var. 15+, 34+"
* #WESTHAMPTON_VAR_15+_34+_ ^property[0].code = #parent 
* #WESTHAMPTON_VAR_15+_34+_ ^property[0].valueCode = #SALMONELLA 
* #WESTMINSTER "Westminster"
* #WESTMINSTER ^property[0].code = #parent 
* #WESTMINSTER ^property[0].valueCode = #SALMONELLA 
* #WESTON "Weston"
* #WESTON ^property[0].code = #parent 
* #WESTON ^property[0].valueCode = #SALMONELLA 
* #WESTPHALIA "Westphalia"
* #WESTPHALIA ^property[0].code = #parent 
* #WESTPHALIA ^property[0].valueCode = #SALMONELLA 
* #WEYBRIDGE "Weybridge"
* #WEYBRIDGE ^property[0].code = #parent 
* #WEYBRIDGE ^property[0].valueCode = #SALMONELLA 
* #WICHITA "Wichita"
* #WICHITA ^property[0].code = #parent 
* #WICHITA ^property[0].valueCode = #SALMONELLA 
* #WIDEMARSH "Widemarsh"
* #WIDEMARSH ^property[0].code = #parent 
* #WIDEMARSH ^property[0].valueCode = #SALMONELLA 
* #WIEN "Wien"
* #WIEN ^property[0].code = #parent 
* #WIEN ^property[0].valueCode = #SALMONELLA 
* #WIL "Wil"
* #WIL ^property[0].code = #parent 
* #WIL ^property[0].valueCode = #SALMONELLA 
* #WILHELMSBURG "Wilhelmsburg"
* #WILHELMSBURG ^property[0].code = #parent 
* #WILHELMSBURG ^property[0].valueCode = #SALMONELLA 
* #WILLAMETTE "Willamette"
* #WILLAMETTE ^property[0].code = #parent 
* #WILLAMETTE ^property[0].valueCode = #SALMONELLA 
* #WILLEMSTAD "Willemstad"
* #WILLEMSTAD ^property[0].code = #parent 
* #WILLEMSTAD ^property[0].valueCode = #SALMONELLA 
* #WILMINGTON "Wilmington"
* #WILMINGTON ^property[0].code = #parent 
* #WILMINGTON ^property[0].valueCode = #SALMONELLA 
* #WIMBORNE "Wimborne"
* #WIMBORNE ^property[0].code = #parent 
* #WIMBORNE ^property[0].valueCode = #SALMONELLA 
* #WINDERMERE "Windermere"
* #WINDERMERE ^property[0].code = #parent 
* #WINDERMERE ^property[0].valueCode = #SALMONELLA 
* #WINDSHEIM "Windsheim"
* #WINDSHEIM ^property[0].code = #parent 
* #WINDSHEIM ^property[0].valueCode = #SALMONELLA 
* #WINGROVE "Wingrove"
* #WINGROVE ^property[0].code = #parent 
* #WINGROVE ^property[0].valueCode = #SALMONELLA 
* #WINNEBA "Winneba"
* #WINNEBA ^property[0].code = #parent 
* #WINNEBA ^property[0].valueCode = #SALMONELLA 
* #WINNIPEG "Winnipeg"
* #WINNIPEG ^property[0].code = #parent 
* #WINNIPEG ^property[0].valueCode = #SALMONELLA 
* #WINSLOW "Winslow"
* #WINSLOW ^property[0].code = #parent 
* #WINSLOW ^property[0].valueCode = #SALMONELLA 
* #WINSTON "Winston"
* #WINSTON ^property[0].code = #parent 
* #WINSTON ^property[0].valueCode = #SALMONELLA 
* #WINTERTHUR "Winterthur"
* #WINTERTHUR ^property[0].code = #parent 
* #WINTERTHUR ^property[0].valueCode = #SALMONELLA 
* #WIPPRA "Wippra"
* #WIPPRA ^property[0].code = #parent 
* #WIPPRA ^property[0].valueCode = #SALMONELLA 
* #WISBECH "Wisbech"
* #WISBECH ^property[0].code = #parent 
* #WISBECH ^property[0].valueCode = #SALMONELLA 
* #WOHLEN "Wohlen"
* #WOHLEN ^property[0].code = #parent 
* #WOHLEN ^property[0].valueCode = #SALMONELLA 
* #WOODHULL "Woodhull"
* #WOODHULL ^property[0].code = #parent 
* #WOODHULL ^property[0].valueCode = #SALMONELLA 
* #WOODINVILLE "Woodinville"
* #WOODINVILLE ^property[0].code = #parent 
* #WOODINVILLE ^property[0].valueCode = #SALMONELLA 
* #WORB "Worb"
* #WORB ^property[0].code = #parent 
* #WORB ^property[0].valueCode = #SALMONELLA 
* #WORTHINGTON "Worthington"
* #WORTHINGTON ^property[0].code = #parent 
* #WORTHINGTON ^property[0].valueCode = #SALMONELLA 
* #WOUMBOU "Woumbou"
* #WOUMBOU ^property[0].code = #parent 
* #WOUMBOU ^property[0].valueCode = #SALMONELLA 
* #WUITI "Wuiti"
* #WUITI ^property[0].code = #parent 
* #WUITI ^property[0].valueCode = #SALMONELLA 
* #WUPPERTAL "Wuppertal"
* #WUPPERTAL ^property[0].code = #parent 
* #WUPPERTAL ^property[0].valueCode = #SALMONELLA 
* #WYLDEGREEN "Wyldegreen"
* #WYLDEGREEN ^property[0].code = #parent 
* #WYLDEGREEN ^property[0].valueCode = #SALMONELLA 
* #YABA "Yaba"
* #YABA ^property[0].code = #parent 
* #YABA ^property[0].valueCode = #SALMONELLA 
* #YALDING "Yalding"
* #YALDING ^property[0].code = #parent 
* #YALDING ^property[0].valueCode = #SALMONELLA 
* #YAOUNDE "Yaounde"
* #YAOUNDE ^property[0].code = #parent 
* #YAOUNDE ^property[0].valueCode = #SALMONELLA 
* #YARDLEY "Yardley"
* #YARDLEY ^property[0].code = #parent 
* #YARDLEY ^property[0].valueCode = #SALMONELLA 
* #YARM "Yarm"
* #YARM ^property[0].code = #parent 
* #YARM ^property[0].valueCode = #SALMONELLA 
* #YARRABAH "Yarrabah"
* #YARRABAH ^property[0].code = #parent 
* #YARRABAH ^property[0].valueCode = #SALMONELLA 
* #YEERONGPILLY "Yeerongpilly"
* #YEERONGPILLY ^property[0].code = #parent 
* #YEERONGPILLY ^property[0].valueCode = #SALMONELLA 
* #YEHUDA "Yehuda"
* #YEHUDA ^property[0].code = #parent 
* #YEHUDA ^property[0].valueCode = #SALMONELLA 
* #YEKEPA "Yekepa"
* #YEKEPA ^property[0].code = #parent 
* #YEKEPA ^property[0].valueCode = #SALMONELLA 
* #YELLOWKNIFE "Yellowknife"
* #YELLOWKNIFE ^property[0].code = #parent 
* #YELLOWKNIFE ^property[0].valueCode = #SALMONELLA 
* #YENNE "Yenne"
* #YENNE ^property[0].code = #parent 
* #YENNE ^property[0].valueCode = #SALMONELLA 
* #YERBA "Yerba"
* #YERBA ^property[0].code = #parent 
* #YERBA ^property[0].valueCode = #SALMONELLA 
* #YOFF "Yoff"
* #YOFF ^property[0].code = #parent 
* #YOFF ^property[0].valueCode = #SALMONELLA 
* #YOKOE "Yokoe"
* #YOKOE ^property[0].code = #parent 
* #YOKOE ^property[0].valueCode = #SALMONELLA 
* #YOLO "Yolo"
* #YOLO ^property[0].code = #parent 
* #YOLO ^property[0].valueCode = #SALMONELLA 
* #YOMBESALI "Yombesali"
* #YOMBESALI ^property[0].code = #parent 
* #YOMBESALI ^property[0].valueCode = #SALMONELLA 
* #YOPOUGON "Yopougon"
* #YOPOUGON ^property[0].code = #parent 
* #YOPOUGON ^property[0].valueCode = #SALMONELLA 
* #YORK "York"
* #YORK ^property[0].code = #parent 
* #YORK ^property[0].valueCode = #SALMONELLA 
* #YORUBA "Yoruba"
* #YORUBA ^property[0].code = #parent 
* #YORUBA ^property[0].valueCode = #SALMONELLA 
* #YOVOKOME "Yovokome"
* #YOVOKOME ^property[0].code = #parent 
* #YOVOKOME ^property[0].valueCode = #SALMONELLA 
* #YUNDUM "Yundum"
* #YUNDUM ^property[0].code = #parent 
* #YUNDUM ^property[0].valueCode = #SALMONELLA 
* #ZADAR "Zadar"
* #ZADAR ^property[0].code = #parent 
* #ZADAR ^property[0].valueCode = #SALMONELLA 
* #ZAIMAN "Zaiman"
* #ZAIMAN ^property[0].code = #parent 
* #ZAIMAN ^property[0].valueCode = #SALMONELLA 
* #ZAIRE "Zaire"
* #ZAIRE ^property[0].code = #parent 
* #ZAIRE ^property[0].valueCode = #SALMONELLA 
* #ZANZIBAR "Zanzibar"
* #ZANZIBAR ^property[0].code = #parent 
* #ZANZIBAR ^property[0].valueCode = #SALMONELLA 
* #ZARIA "Zaria"
* #ZARIA ^property[0].code = #parent 
* #ZARIA ^property[0].valueCode = #SALMONELLA 
* #ZEGA "Zega"
* #ZEGA ^property[0].code = #parent 
* #ZEGA ^property[0].valueCode = #SALMONELLA 
* #ZEHLENDORF "Zehlendorf"
* #ZEHLENDORF ^property[0].code = #parent 
* #ZEHLENDORF ^property[0].valueCode = #SALMONELLA 
* #ZERIFIN "Zerifin"
* #ZERIFIN ^property[0].code = #parent 
* #ZERIFIN ^property[0].valueCode = #SALMONELLA 
* #ZIGONG "Zigong"
* #ZIGONG ^property[0].code = #parent 
* #ZIGONG ^property[0].valueCode = #SALMONELLA 
* #ZINDER "Zinder"
* #ZINDER ^property[0].code = #parent 
* #ZINDER ^property[0].valueCode = #SALMONELLA 
* #ZONGO "Zongo"
* #ZONGO ^property[0].code = #parent 
* #ZONGO ^property[0].valueCode = #SALMONELLA 
* #ZUILEN "Zuilen"
* #ZUILEN ^property[0].code = #parent 
* #ZUILEN ^property[0].valueCode = #SALMONELLA 
* #ZWICKAU "Zwickau"
* #ZWICKAU ^property[0].code = #parent 
* #ZWICKAU ^property[0].valueCode = #SALMONELLA 
* #SHIGELLOSE "Shigellose"
* #SHIGELLOSE ^property[0].code = #child 
* #SHIGELLOSE ^property[0].valueCode = #1 
* #SHIGELLOSE ^property[1].code = #child 
* #SHIGELLOSE ^property[1].valueCode = #10 
* #SHIGELLOSE ^property[2].code = #child 
* #SHIGELLOSE ^property[2].valueCode = #11 
* #SHIGELLOSE ^property[3].code = #child 
* #SHIGELLOSE ^property[3].valueCode = #12 
* #SHIGELLOSE ^property[4].code = #child 
* #SHIGELLOSE ^property[4].valueCode = #13 
* #SHIGELLOSE ^property[5].code = #child 
* #SHIGELLOSE ^property[5].valueCode = #14 
* #SHIGELLOSE ^property[6].code = #child 
* #SHIGELLOSE ^property[6].valueCode = #15 
* #SHIGELLOSE ^property[7].code = #child 
* #SHIGELLOSE ^property[7].valueCode = #16 
* #SHIGELLOSE ^property[8].code = #child 
* #SHIGELLOSE ^property[8].valueCode = #17 
* #SHIGELLOSE ^property[9].code = #child 
* #SHIGELLOSE ^property[9].valueCode = #18 
* #SHIGELLOSE ^property[10].code = #child 
* #SHIGELLOSE ^property[10].valueCode = #19 
* #SHIGELLOSE ^property[11].code = #child 
* #SHIGELLOSE ^property[11].valueCode = #1A 
* #SHIGELLOSE ^property[12].code = #child 
* #SHIGELLOSE ^property[12].valueCode = #1B 
* #SHIGELLOSE ^property[13].code = #child 
* #SHIGELLOSE ^property[13].valueCode = #2 
* #SHIGELLOSE ^property[14].code = #child 
* #SHIGELLOSE ^property[14].valueCode = #2A 
* #SHIGELLOSE ^property[15].code = #child 
* #SHIGELLOSE ^property[15].valueCode = #2B 
* #SHIGELLOSE ^property[16].code = #child 
* #SHIGELLOSE ^property[16].valueCode = #3 
* #SHIGELLOSE ^property[17].code = #child 
* #SHIGELLOSE ^property[17].valueCode = #3A 
* #SHIGELLOSE ^property[18].code = #child 
* #SHIGELLOSE ^property[18].valueCode = #3B 
* #SHIGELLOSE ^property[19].code = #child 
* #SHIGELLOSE ^property[19].valueCode = #4 
* #SHIGELLOSE ^property[20].code = #child 
* #SHIGELLOSE ^property[20].valueCode = #4A 
* #SHIGELLOSE ^property[21].code = #child 
* #SHIGELLOSE ^property[21].valueCode = #4B 
* #SHIGELLOSE ^property[22].code = #child 
* #SHIGELLOSE ^property[22].valueCode = #4C 
* #SHIGELLOSE ^property[23].code = #child 
* #SHIGELLOSE ^property[23].valueCode = #5 
* #SHIGELLOSE ^property[24].code = #child 
* #SHIGELLOSE ^property[24].valueCode = #5A 
* #SHIGELLOSE ^property[25].code = #child 
* #SHIGELLOSE ^property[25].valueCode = #5B 
* #SHIGELLOSE ^property[26].code = #child 
* #SHIGELLOSE ^property[26].valueCode = #6 
* #SHIGELLOSE ^property[27].code = #child 
* #SHIGELLOSE ^property[27].valueCode = #7 
* #SHIGELLOSE ^property[28].code = #child 
* #SHIGELLOSE ^property[28].valueCode = #8 
* #SHIGELLOSE ^property[29].code = #child 
* #SHIGELLOSE ^property[29].valueCode = #9 
* #SHIGELLOSE ^property[30].code = #child 
* #SHIGELLOSE ^property[30].valueCode = #X 
* #SHIGELLOSE ^property[31].code = #child 
* #SHIGELLOSE ^property[31].valueCode = #Y 
* #10 "10"
* #10 ^property[0].code = #parent 
* #10 ^property[0].valueCode = #SHIGELLOSE 
* #1A "1a"
* #1A ^property[0].code = #parent 
* #1A ^property[0].valueCode = #SHIGELLOSE 
* #1B "1b"
* #1B ^property[0].code = #parent 
* #1B ^property[0].valueCode = #SHIGELLOSE 
* #2A "2a"
* #2A ^property[0].code = #parent 
* #2A ^property[0].valueCode = #SHIGELLOSE 
* #2B "2b"
* #2B ^property[0].code = #parent 
* #2B ^property[0].valueCode = #SHIGELLOSE 
* #5A "5a"
* #5A ^property[0].code = #parent 
* #5A ^property[0].valueCode = #SHIGELLOSE 
* #5B "5b"
* #5B ^property[0].code = #parent 
* #5B ^property[0].valueCode = #SHIGELLOSE 
* #X "X"
* #X ^property[0].code = #parent 
* #X ^property[0].valueCode = #SHIGELLOSE 
* #Y "Y"
* #Y ^property[0].code = #parent 
* #Y ^property[0].valueCode = #SHIGELLOSE 
