Instance: ems-krankheitsmerkmale 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-krankheitsmerkmale" 
* name = "ems-krankheitsmerkmale" 
* title = "EMS_Krankheitsmerkmale" 
* status = #active 
* content = #complete 
* version = "202101" 
* description = "**Beschreibung:** Angaben weiterer Krankheitsmerkmale bei der Meldunge in das Epidemiologische Meldesystem vom BMG" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.105" 
* date = "2021-01-27" 
* count = 5 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #ASYMPTOMATISCH "Asymptomatisch"
* #AUSSCHEIDER "Ausscheider"
* #Verstorben "Verstorben"
* #Verstorben ^property[0].code = #child 
* #Verstorben ^property[0].valueCode = #TOD 
* #Verstorben ^property[1].code = #child 
* #Verstorben ^property[1].valueCode = #TOD_KRANKHEIT 
* #TOD "Tod"
* #TOD ^property[0].code = #parent 
* #TOD ^property[0].valueCode = #Verstorben 
* #TOD_KRANKHEIT "Krankheitsbezogener Tod"
* #TOD_KRANKHEIT ^property[0].code = #parent 
* #TOD_KRANKHEIT ^property[0].valueCode = #Verstorben 
