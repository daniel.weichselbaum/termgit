Instance: ems-krankheitsmerkmale 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-krankheitsmerkmale" 
* name = "ems-krankheitsmerkmale" 
* title = "EMS_Krankheitsmerkmale" 
* status = #active 
* content = #complete 
* version = "202101" 
* description = "**Beschreibung:** Angaben weiterer Krankheitsmerkmale bei der Meldunge in das Epidemiologische Meldesystem vom BMG" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.105" 
* date = "2021-01-27" 
* count = 5 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* concept[0].code = #ASYMPTOMATISCH
* concept[0].display = "Asymptomatisch"
* concept[1].code = #AUSSCHEIDER
* concept[1].display = "Ausscheider"
* concept[2].code = #Verstorben
* concept[2].display = "Verstorben"
* concept[2].property[0].code = #child 
* concept[2].property[0].valueCode = #TOD 
* concept[2].property[1].code = #child 
* concept[2].property[1].valueCode = #TOD_KRANKHEIT 
* concept[3].code = #TOD
* concept[3].display = "Tod"
* concept[3].property[0].code = #parent 
* concept[3].property[0].valueCode = #Verstorben 
* concept[4].code = #TOD_KRANKHEIT
* concept[4].display = "Krankheitsbezogener Tod"
* concept[4].property[0].code = #parent 
* concept[4].property[0].valueCode = #Verstorben 
