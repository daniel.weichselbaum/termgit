Instance: ems-testmethodemic-ipd 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-testmethodemic-ipd" 
* name = "ems-testmethodemic-ipd" 
* title = "EMS_TestMethodeMIC_IPD" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Liste Testmethoden MIC IPD: Verwendung bei Pneumokokken" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.79" 
* date = "2017-01-26" 
* count = 6 
* #AGGLUSERO "Serotyp Agglutination"
* #ANTIKMIKROTITER "Mikrotiterverfahren"
* #RESISTENZAUTOMAT "Automatisierte MHK Testung (Vitek© etc.)"
* #RESISTENZDIFF "Agar Diffusionstest"
* #RESISTENZGRAD "Antibiotikumgradiententest (E-test©, etc.)"
* #RESISTENZMHK "Antibiotika Resistenztestung MHK"
