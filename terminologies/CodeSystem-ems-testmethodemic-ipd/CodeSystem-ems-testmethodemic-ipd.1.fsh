Instance: ems-testmethodemic-ipd 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-testmethodemic-ipd" 
* name = "ems-testmethodemic-ipd" 
* title = "EMS_TestMethodeMIC_IPD" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Liste Testmethoden MIC IPD: Verwendung bei Pneumokokken" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.79" 
* date = "2017-01-26" 
* count = 6 
* concept[0].code = #AGGLUSERO
* concept[0].display = "Serotyp Agglutination"
* concept[1].code = #ANTIKMIKROTITER
* concept[1].display = "Mikrotiterverfahren"
* concept[2].code = #RESISTENZAUTOMAT
* concept[2].display = "Automatisierte MHK Testung (Vitek© etc.)"
* concept[3].code = #RESISTENZDIFF
* concept[3].display = "Agar Diffusionstest"
* concept[4].code = #RESISTENZGRAD
* concept[4].display = "Antibiotikumgradiententest (E-test©, etc.)"
* concept[5].code = #RESISTENZMHK
* concept[5].display = "Antibiotika Resistenztestung MHK"
