Instance: ems-artmalaria 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-artmalaria" 
* name = "ems-artmalaria" 
* title = "EMS_ArtMalaria" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Liste der Malariaart" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.62" 
* date = "2017-01-26" 
* count = 12 
* #PLAFAL "Pl. falciparum"
* #PLAFALMAL "Pl. falciparum/malariae"
* #PLAFALOVA "Pl. falciparum/ovale"
* #PLAFALVIV "Pl. falciparum/vivax"
* #PLAKNO "Pl. knowlesi"
* #PLAMAL "Pl. malariae"
* #PLAMALOVA "Pl. malariae/ovale"
* #PLAOVA "Pl. ovale"
* #PLASPP "nicht spezifiziert"
* #PLAVIV "Pl. vivax"
* #PLAVIVMAL "Pl. vivax/malariae"
* #PLAVIVOVA "Pl. vivax/ovale"
