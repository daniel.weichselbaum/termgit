Instance: elga-ergaenzungsliste 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-ergaenzungsliste" 
* name = "elga-ergaenzungsliste" 
* title = "ELGA_Ergaenzungsliste" 
* status = #active 
* content = #complete 
* version = "1.0" 
* description = "**Description:** Supplemental code list for codes needed in ELGA value sets

**Beschreibung:** Sammel-Codeliste von Konzepten, die als Ergänzung in ELGA-Value-Sets benötigt werden" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.179" 
* date = "2018-06-21" 
* count = 1 
* #Refna "Referenzbereich nicht anwendbar"
* #Refna ^definition = Ergänzung der ObservationInterpretation Codes für den Fall, dass ein Referenzbereich nicht anwendbar ist
