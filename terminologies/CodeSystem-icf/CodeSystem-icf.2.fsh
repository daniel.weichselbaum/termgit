Instance: icf 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-icf" 
* name = "icf" 
* title = "ICF" 
* status = #active 
* content = #complete 
* version = "2005" 
* description = "**Beschreibung:** Internationale Klassifikation der Funktionsfähigkeit Behinderung und Gesundheit

**Versions-Beschreibung:** a. Die Erstellung erfolgt unter Verwendung der maschinenlesbaren Fassung des Deutschen Instituts für Medizinische Dokumentation und Information (DIMDI).
b. ICF-Kodes, -Begriffe und -Texte Weltgesundheitsorganisation, übersetzt und herausgegeben durch das Deutsche Institut für Medizinische Dokumentation und Information von der International classification of functioning , disability and health - ICF, herausgegeben durch die Weltgesundheitsorganisation
c. Die ICF muss so genutzt werden, wie in der Klassifikation und in der Einführung und in den Anhängen zur ICF beschrieben. Die Nutzung muss insbesondere gemäß der in Anhang 3 'Mögliche Verwendung der Liste der Aktivitäten und Partizipationen' als Option 4 'Verwendung der gleichen Domänen sowohl für die Aktivitäten als auch für Partizipation [Teilhabe] mit einer umfänglichen Überlappung der Domänen' beschriebenen Weise erfolgen. Die Beurteilungsmerkmale sind entsprechend Anhang 2 'Kodierungsleitlinien für die ICF' anzuwenden, Skala und prozentuale Bereiche dürfen ohne Genehmigung der WHO nicht geändert werden." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.6.254-2005" 
* date = "2021-05-28" 
* copyright = "Copyright WHO, DIMDI, 2001-2019" 
* count = 1495 
* property[0].code = #None 
* property[0].type = #string 
* property[1].code = #child 
* property[1].type = #code 
* property[2].code = #parent 
* property[2].type = #code 
* #b "Körperfunktionen"
* #b ^property[0].code = #None 
* #b ^property[0].valueString = "Körperfunktionen sind die physiologischen Funktionen von Körpersystemen (einschließlich psychologische Funktionen)." 
* #b ^property[1].code = #None 
* #b ^property[1].valueString = "Schädigungen sind Beeinträchtigungen einer Körperfunktion oder ?struktur, wie z.B. eine wesentliche Abweichung oder ein Verlust." 
* #b ^property[2].code = #child 
* #b ^property[2].valueCode = #b1 
* #b ^property[3].code = #child 
* #b ^property[3].valueCode = #b2 
* #b ^property[4].code = #child 
* #b ^property[4].valueCode = #b3 
* #b ^property[5].code = #child 
* #b ^property[5].valueCode = #b4 
* #b ^property[6].code = #child 
* #b ^property[6].valueCode = #b5 
* #b ^property[7].code = #child 
* #b ^property[7].valueCode = #b6 
* #b ^property[8].code = #child 
* #b ^property[8].valueCode = #b7 
* #b ^property[9].code = #child 
* #b ^property[9].valueCode = #b8 
* #b1 "Mentale Funktionen"
* #b1 ^property[0].code = #None 
* #b1 ^property[0].valueString = "Dieses Kapitel befasst sich mit den Funktionen des Gehirns: den globalen mentalen Funktionen, wie Funktionen des Bewusstseins sowie den Funktionen der psychischen Energie und des Antriebs und den spezifischen mentalen Funktionen, wie Funktionen des Gedächtnisses, kognitiv-sprachlichen Funktionen und Funktionen des Rechenvermögens." 
* #b1 ^property[1].code = #parent 
* #b1 ^property[1].valueCode = #b 
* #b1 ^property[2].code = #child 
* #b1 ^property[2].valueCode = #b110-b139 
* #b1 ^property[3].code = #child 
* #b1 ^property[3].valueCode = #b140-b189 
* #b1 ^property[4].code = #child 
* #b1 ^property[4].valueCode = #b198 
* #b1 ^property[5].code = #child 
* #b1 ^property[5].valueCode = #b199 
* #b110-b139 "Globale mentale Funktionen"
* #b110-b139 ^property[0].code = #parent 
* #b110-b139 ^property[0].valueCode = #b1 
* #b110-b139 ^property[1].code = #child 
* #b110-b139 ^property[1].valueCode = #b110 
* #b110-b139 ^property[2].code = #child 
* #b110-b139 ^property[2].valueCode = #b114 
* #b110-b139 ^property[3].code = #child 
* #b110-b139 ^property[3].valueCode = #b117 
* #b110-b139 ^property[4].code = #child 
* #b110-b139 ^property[4].valueCode = #b122 
* #b110-b139 ^property[5].code = #child 
* #b110-b139 ^property[5].valueCode = #b126 
* #b110-b139 ^property[6].code = #child 
* #b110-b139 ^property[6].valueCode = #b130 
* #b110-b139 ^property[7].code = #child 
* #b110-b139 ^property[7].valueCode = #b134 
* #b110-b139 ^property[8].code = #child 
* #b110-b139 ^property[8].valueCode = #b139 
* #b110 "Funktionen des Bewusstseins"
* #b110 ^property[0].code = #None 
* #b110 ^property[0].valueString = "Allgemeine mentale Funktionen, die die bewusste Wahrnehmung und Wachheit einschließlich Klarheit und Kontinuität des Wachheitszustandes betreffen" 
* #b110 ^property[1].code = #None 
* #b110 ^property[1].valueString = "Funktionen, die Zustand, Kontinuität und Qualität des Bewusstseins betreffen; Bewusstseinsverlust, Koma, vegetativer Status (Apallisches Syndrom), Dämmerzustand (Fugue), Trance, Besessenheit, drogeninduzierte Bewusstseinsveränderungen, Delir, Stupor" 
* #b110 ^property[2].code = #None 
* #b110 ^property[2].valueString = "Funktionen der Orientierung" 
* #b110 ^property[3].code = #parent 
* #b110 ^property[3].valueCode = #b110-b139 
* #b110 ^property[4].code = #child 
* #b110 ^property[4].valueCode = #b1100 
* #b110 ^property[5].code = #child 
* #b110 ^property[5].valueCode = #b1101 
* #b110 ^property[6].code = #child 
* #b110 ^property[6].valueCode = #b1102 
* #b110 ^property[7].code = #child 
* #b110 ^property[7].valueCode = #b1108 
* #b110 ^property[8].code = #child 
* #b110 ^property[8].valueCode = #b1109 
* #b1100 "Bewusstseinszustand"
* #b1100 ^property[0].code = #None 
* #b1100 ^property[0].valueString = "Mentale Funktionen, die sich bei Veränderung als Zustände wie Bewusstseinstrübung, Stupor oder Koma äußern" 
* #b1100 ^property[1].code = #parent 
* #b1100 ^property[1].valueCode = #b110 
* #b1101 "Kontinuität des Bewusstseins"
* #b1101 ^property[0].code = #None 
* #b1101 ^property[0].valueString = "Mentale Funktionen, die sich in Erhalt der Wachheit, Aufmerksamkeit und bewusster Wahrnehmung äußern und die bei einer Störung zu Dämmerzustand (Fugue), Trance oder ähnlichen Zuständen führen können" 
* #b1101 ^property[1].code = #parent 
* #b1101 ^property[1].valueCode = #b110 
* #b1102 "Qualität des Bewusstseins"
* #b1102 ^property[0].code = #None 
* #b1102 ^property[0].valueString = "Mentale Funktionen, die sich bei Veränderungen auf die Art des Empfindens von Wachheit, Aufmerksamkeit und bewusster Wahrnehmung auswirken, wie drogeninduzierte Bewusstseinsveränderungen oder ein Delir" 
* #b1102 ^property[1].code = #parent 
* #b1102 ^property[1].valueCode = #b110 
* #b1108 "Funktionen des Bewusstseins, anders bezeichnet"
* #b1108 ^property[0].code = #parent 
* #b1108 ^property[0].valueCode = #b110 
* #b1109 "Funktionen des Bewusstseins, nicht näher bezeichnet"
* #b1109 ^property[0].code = #parent 
* #b1109 ^property[0].valueCode = #b110 
* #b114 "Funktionen der Orientierung"
* #b114 ^property[0].code = #None 
* #b114 ^property[0].valueString = "Allgemeine mentale Funktionen, die Selbstwahrnehmung, Ich-Bewusstsein und realistische Wahrnehmung anderer Personen sowie der Zeit und der Umgebung betreffen" 
* #b114 ^property[1].code = #None 
* #b114 ^property[1].valueString = "Funktionen der Orientierung zu Zeit, Ort und Person sowie der Orientierung zur eigenen Person und zu anderen Personen; Desorientierung zu Zeit, Ort und Person" 
* #b114 ^property[2].code = #None 
* #b114 ^property[2].valueString = "Funktionen des Bewusstseins" 
* #b114 ^property[3].code = #parent 
* #b114 ^property[3].valueCode = #b110-b139 
* #b114 ^property[4].code = #child 
* #b114 ^property[4].valueCode = #b1140 
* #b114 ^property[5].code = #child 
* #b114 ^property[5].valueCode = #b1141 
* #b114 ^property[6].code = #child 
* #b114 ^property[6].valueCode = #b1142 
* #b114 ^property[7].code = #child 
* #b114 ^property[7].valueCode = #b1148 
* #b114 ^property[8].code = #child 
* #b114 ^property[8].valueCode = #b1149 
* #b1140 "Orientierung zur Zeit"
* #b1140 ^property[0].code = #None 
* #b1140 ^property[0].valueString = "Mentale Funktionen, die sich im bewussten Gewahrsein von Wochentag, Datum, Tag,. Monat und Jahr äußern" 
* #b1140 ^property[1].code = #parent 
* #b1140 ^property[1].valueCode = #b114 
* #b1141 "Orientierung zum Ort"
* #b1141 ^property[0].code = #None 
* #b1141 ^property[0].valueString = "Mentale Funktionen, die sich im bewussten Gewahrsein der örtlichen Situation äußeren, z.B. in welcher unmittelbaren Umgebung, in welcher Stadt oder in welchem Land man sich befindet" 
* #b1141 ^property[1].code = #parent 
* #b1141 ^property[1].valueCode = #b114 
* #b1142 "Orientierung zur Person"
* #b1142 ^property[0].code = #None 
* #b1142 ^property[0].valueString = "Mentale Funktionen, die sich im bewussten Gewahrsein der eigenen Identität und von Personen in der unmittelbaren Umgebung äußern" 
* #b1142 ^property[1].code = #parent 
* #b1142 ^property[1].valueCode = #b114 
* #b1142 ^property[2].code = #child 
* #b1142 ^property[2].valueCode = #b11420 
* #b1142 ^property[3].code = #child 
* #b1142 ^property[3].valueCode = #b11421 
* #b1142 ^property[4].code = #child 
* #b1142 ^property[4].valueCode = #b11428 
* #b1142 ^property[5].code = #child 
* #b1142 ^property[5].valueCode = #b11429 
* #b11420 "Orientierung zum eigenen Selbst"
* #b11420 ^property[0].code = #None 
* #b11420 ^property[0].valueString = "Mentale Funktionen, die sich im bewussten Gewahrsein der eigenen Identität äußern" 
* #b11420 ^property[1].code = #parent 
* #b11420 ^property[1].valueCode = #b1142 
* #b11421 "Orientierung zu anderen Personen"
* #b11421 ^property[0].code = #None 
* #b11421 ^property[0].valueString = "Mentale Funktionen, die sich im bewussten Gewahrsein von Personen in der unmittelbaren Umgebung äußern" 
* #b11421 ^property[1].code = #parent 
* #b11421 ^property[1].valueCode = #b1142 
* #b11428 "Orientierung zu Personen, anders bezeichnet"
* #b11428 ^property[0].code = #parent 
* #b11428 ^property[0].valueCode = #b1142 
* #b11429 "Orientierung zu Personen, nicht näher bezeichnet"
* #b11429 ^property[0].code = #parent 
* #b11429 ^property[0].valueCode = #b1142 
* #b1148 "Funktionen der Orientierung, anders bezeichnet"
* #b1148 ^property[0].code = #parent 
* #b1148 ^property[0].valueCode = #b114 
* #b1149 "Funktionen der Orientierung, nicht näher bezeichnet"
* #b1149 ^property[0].code = #parent 
* #b1149 ^property[0].valueCode = #b114 
* #b117 "Funktionen der Intelligenz"
* #b117 ^property[0].code = #None 
* #b117 ^property[0].valueString = "Allgemeine mentale Funktionen, die erforderlich sind, die verschiedenen mentalen Funktionen einschließlich aller kognitiven Funktionen zu verstehen und konstruktiv zu integrieren sowie diese über die gesamte Lebensdauer hinweg fortzuentwickeln" 
* #b117 ^property[1].code = #None 
* #b117 ^property[1].valueString = "Die Intelligenzentwicklung betreffende Funktionen; intellektuelle und mentale Retardierung, Demenz" 
* #b117 ^property[2].code = #None 
* #b117 ^property[2].valueString = "Funktionen des Gedächtnisses" 
* #b117 ^property[3].code = #parent 
* #b117 ^property[3].valueCode = #b110-b139 
* #b122 "Globale psychosoziale Funktionen"
* #b122 ^property[0].code = #None 
* #b122 ^property[0].valueString = "Sich über das gesamte Leben entwickelnde allgemeine mentale Funktionen, die für das Verständnis und die konstruktive Integration jener mentalen Funktionen erforderlich sind, die zur Bildung interpersoneller Fähigkeiten führen, welche für den Aufbau reziproker sozialer Interaktionen, die sinnvoll und zweckmäßig sind, benötigt werden" 
* #b122 ^property[1].code = #None 
* #b122 ^property[1].valueString = "Störungen wie bei Autismus" 
* #b122 ^property[2].code = #parent 
* #b122 ^property[2].valueCode = #b110-b139 
* #b126 "Funktionen von Temperament und Persönlichkeit"
* #b126 ^property[0].code = #None 
* #b126 ^property[0].valueString = "Allgemeine mentale Funktionen, die das anlagebedingte Naturell einer Person betreffen, individuell auf Situationen zu reagieren, einschließlich der psychischen Charakteristika, die eine Person von einer anderen unterscheiden" 
* #b126 ^property[1].code = #None 
* #b126 ^property[1].valueString = "Funktionen, die Extraversion, Introversion, Umgänglichkeit, Gewissenhaftigkeit, psychische und emotionale Stabilität, Offenheit gegenüber Erfahrungen, Optimismus, Neugier, Vertrauen und Zuverlässigkeit betreffen" 
* #b126 ^property[2].code = #None 
* #b126 ^property[2].valueString = "Funktionen der Intelligenz" 
* #b126 ^property[3].code = #parent 
* #b126 ^property[3].valueCode = #b110-b139 
* #b126 ^property[4].code = #child 
* #b126 ^property[4].valueCode = #b1260 
* #b126 ^property[5].code = #child 
* #b126 ^property[5].valueCode = #b1261 
* #b126 ^property[6].code = #child 
* #b126 ^property[6].valueCode = #b1262 
* #b126 ^property[7].code = #child 
* #b126 ^property[7].valueCode = #b1263 
* #b126 ^property[8].code = #child 
* #b126 ^property[8].valueCode = #b1264 
* #b126 ^property[9].code = #child 
* #b126 ^property[9].valueCode = #b1265 
* #b126 ^property[10].code = #child 
* #b126 ^property[10].valueCode = #b1266 
* #b126 ^property[11].code = #child 
* #b126 ^property[11].valueCode = #b1267 
* #b126 ^property[12].code = #child 
* #b126 ^property[12].valueCode = #b1268 
* #b126 ^property[13].code = #child 
* #b126 ^property[13].valueCode = #b1269 
* #b1260 "Extraversion"
* #b1260 ^property[0].code = #None 
* #b1260 ^property[0].valueString = "Mentale Funktionen, die sich in einer Persönlichkeit äußern, die durch Kontaktfreudigkeit, Geselligkeit und (emotionale) Ausdrucksfähigkeit gekennzeichnet ist, im Gegensatz zu Schüchternheit, Zurückgezogenheit oder Gehemmtheit" 
* #b1260 ^property[1].code = #parent 
* #b1260 ^property[1].valueCode = #b126 
* #b1261 "Umgänglichkeit"
* #b1261 ^property[0].code = #None 
* #b1261 ^property[0].valueString = "Mentale Funktionen, die sich in einer Persönlichkeit äußern, die durch Kooperationsbereitschaft, Freundschaftlichkeit und Zuvorkommenheit gekennzeichnet ist, im Gegensatz zu Unfreundlichkeit, Streitbarkeit und Aufsässigkeit" 
* #b1261 ^property[1].code = #parent 
* #b1261 ^property[1].valueCode = #b126 
* #b1262 "Gewissenhaftigkeit"
* #b1262 ^property[0].code = #None 
* #b1262 ^property[0].valueString = "Mentale Funktionen, die sich in einer Persönlichkeit äußern, die durch Fleiß, Genauigkeit und Sorgfalt gekennzeichnet ist, im Gegensatz zu Faulheit, Unzuverlässigkeit und Verantwortungslosigkeit" 
* #b1262 ^property[1].code = #parent 
* #b1262 ^property[1].valueCode = #b126 
* #b1263 "Psychische Stabilität"
* #b1263 ^property[0].code = #None 
* #b1263 ^property[0].valueString = "Mentale Funktionen, die sich in einer Persönlichkeit äußern, die durch Ausgeglichenheit, Ruhe und Gefasstheit gekennzeichnet ist, im Gegensatz zu Reizbarkeit, Besorgtheit, Unbeständigkeit und Launenhaftigkeit" 
* #b1263 ^property[1].code = #parent 
* #b1263 ^property[1].valueCode = #b126 
* #b1264 "Offenheit gegenüber neuen Erfahrungen"
* #b1264 ^property[0].code = #None 
* #b1264 ^property[0].valueString = "Mentale Funktionen, die sich in einer Persönlichkeit äußern, die durch Neugier, Vorstellungsvermögen und Suche nach Erfahrungen gekennzeichnet ist, im Gegensatz zu Abgestumpftheit, Unaufmerksamkeit und emotionaler Ausdruckslosigkeit" 
* #b1264 ^property[1].code = #parent 
* #b1264 ^property[1].valueCode = #b126 
* #b1265 "Optimismus"
* #b1265 ^property[0].code = #None 
* #b1265 ^property[0].valueString = "Mentale Funktionen, die sich in einer Persönlichkeit äußern, die durch Heiterkeit, Lebhaftigkeit und Zuversichtlichkeit gekennzeichnet ist, im Gegensatz zu Niedergeschlagenheit, Trübsinn und Verzweiflung" 
* #b1265 ^property[1].code = #parent 
* #b1265 ^property[1].valueCode = #b126 
* #b1266 "Selbstvertrauen"
* #b1266 ^property[0].code = #None 
* #b1266 ^property[0].valueString = "Mentale Funktionen, die sich in einer Persönlichkeit äußern, die durch Selbstsicherheit, Mut und Durchsetzungsvermögen gekennzeichnet ist, im Gegensatz zu Zaghaftigkeit, Unsicherheit und Zurückhaltung" 
* #b1266 ^property[1].code = #parent 
* #b1266 ^property[1].valueCode = #b126 
* #b1267 "Zuverlässigkeit"
* #b1267 ^property[0].code = #None 
* #b1267 ^property[0].valueString = "Mentale Funktionen, die sich in einer Persönlichkeit äußern, die durch Verlässlichkeit und Prinzipientreue gekennzeichnet ist, im Gegensatz zu Hinterlistigkeit und unsozialem Verhalten" 
* #b1267 ^property[1].code = #parent 
* #b1267 ^property[1].valueCode = #b126 
* #b1268 "Funktionen von Temperament und Persönlichkeit, anders bezeichnet"
* #b1268 ^property[0].code = #parent 
* #b1268 ^property[0].valueCode = #b126 
* #b1269 "Funktionen von Temperament und Persönlichkeit, nicht näher bezeichnet"
* #b1269 ^property[0].code = #parent 
* #b1269 ^property[0].valueCode = #b126 
* #b130 "Funktionen der psychischen Energie und des Antriebs"
* #b130 ^property[0].code = #None 
* #b130 ^property[0].valueString = "Allgemeine mentale Funktionen, die physiologische und psychologische Vorgänge betreffen, welche bei einer Person ein nachhaltiges Streben nach Befriedigung bestimmter Bedürfnisse und die Verfolgung allgemeiner Ziele verursachen" 
* #b130 ^property[1].code = #None 
* #b130 ^property[1].valueString = "Funktionen des Bewusstseins" 
* #b130 ^property[2].code = #None 
* #b130 ^property[2].valueString = "Funktionen, die psychische Energie, Motivation, Appetit, Sucht (einschließlich Sucht nach Substanzen, die zu einer Abhängigkeit führen) und Impulskontrolle betreffen" 
* #b130 ^property[3].code = #parent 
* #b130 ^property[3].valueCode = #b110-b139 
* #b130 ^property[4].code = #child 
* #b130 ^property[4].valueCode = #b1300 
* #b130 ^property[5].code = #child 
* #b130 ^property[5].valueCode = #b1301 
* #b130 ^property[6].code = #child 
* #b130 ^property[6].valueCode = #b1302 
* #b130 ^property[7].code = #child 
* #b130 ^property[7].valueCode = #b1303 
* #b130 ^property[8].code = #child 
* #b130 ^property[8].valueCode = #b1304 
* #b130 ^property[9].code = #child 
* #b130 ^property[9].valueCode = #b1308 
* #b130 ^property[10].code = #child 
* #b130 ^property[10].valueCode = #b1309 
* #b1300 "Ausmaß der psychischen Energie"
* #b1300 ^property[0].code = #None 
* #b1300 ^property[0].valueString = "Mentale Funktionen, die sich in Durchsetzungskraft und Durchhaltevermögen äußern" 
* #b1300 ^property[1].code = #parent 
* #b1300 ^property[1].valueCode = #b130 
* #b1301 "Motivation"
* #b1301 ^property[0].code = #None 
* #b1301 ^property[0].valueString = "Mentale Funktionen, die sich in einem Anreiz zu handeln und in einer bewussten oder unbewussten Antriebskraft zu Handlungen äußern" 
* #b1301 ^property[1].code = #parent 
* #b1301 ^property[1].valueCode = #b130 
* #b1302 "Appetit"
* #b1302 ^property[0].code = #None 
* #b1302 ^property[0].valueString = "Mentale Funktionen, die sich in einem natürlichen Verlangen oder einem Wunsch äußern, insbesondere das natürliche und wiederkehrende Verlangen nach Essen und Trinken" 
* #b1302 ^property[1].code = #parent 
* #b1302 ^property[1].valueCode = #b130 
* #b1303 "Drang nach Suchtmitteln"
* #b1303 ^property[0].code = #None 
* #b1303 ^property[0].valueString = "Mentale Funktionen, die sich in einem Drang äußern, Substanzen zu konsumieren einschließlich solcher, die zu Missbrauch führen können" 
* #b1303 ^property[1].code = #parent 
* #b1303 ^property[1].valueCode = #b130 
* #b1304 "Impulskontrolle"
* #b1304 ^property[0].code = #None 
* #b1304 ^property[0].valueString = "Mentale Funktionen, die plötzliche intensive Handlungsimpulse regulieren und unterdrücken" 
* #b1304 ^property[1].code = #parent 
* #b1304 ^property[1].valueCode = #b130 
* #b1308 "Funktionen der psychischen Energie und des Antriebs, anders bezeichnet"
* #b1308 ^property[0].code = #parent 
* #b1308 ^property[0].valueCode = #b130 
* #b1309 "Funktionen der psychischen Energie und des Antriebs, nicht näher bezeichnet"
* #b1309 ^property[0].code = #parent 
* #b1309 ^property[0].valueCode = #b130 
* #b134 "Funktionen des Schlafes"
* #b134 ^property[0].code = #None 
* #b134 ^property[0].valueString = "Allgemeine mentale Funktionen, die sich in einer periodischen, reversiblen und selektiven physischen und mentalen Loslösung von der unmittelbaren Umgebung äußern, und die von charakteristischen physiologischen Veränderungen begleitet sind" 
* #b134 ^property[1].code = #None 
* #b134 ^property[1].valueString = "Funktionen, die Schlafdauer, Schlafbeginn, Aufrechterhaltung des Schlafs, Schlafqualität, Schlafzyklus betreffen, wie bei Insomnie, Hypersomnie, Narkolepsie" 
* #b134 ^property[2].code = #None 
* #b134 ^property[2].valueString = "Funktionen des Bewusstseins" 
* #b134 ^property[3].code = #parent 
* #b134 ^property[3].valueCode = #b110-b139 
* #b134 ^property[4].code = #child 
* #b134 ^property[4].valueCode = #b1340 
* #b134 ^property[5].code = #child 
* #b134 ^property[5].valueCode = #b1341 
* #b134 ^property[6].code = #child 
* #b134 ^property[6].valueCode = #b1342 
* #b134 ^property[7].code = #child 
* #b134 ^property[7].valueCode = #b1343 
* #b134 ^property[8].code = #child 
* #b134 ^property[8].valueCode = #b1344 
* #b134 ^property[9].code = #child 
* #b134 ^property[9].valueCode = #b1348 
* #b134 ^property[10].code = #child 
* #b134 ^property[10].valueCode = #b1349 
* #b1340 "Schlafdauer"
* #b1340 ^property[0].code = #None 
* #b1340 ^property[0].valueString = "Mentale Funktionen, die an der Zeit, die im diurnalen oder circadianen Zyklus im Schlaf verbracht wird, beteiligt sind" 
* #b1340 ^property[1].code = #parent 
* #b1340 ^property[1].valueCode = #b134 
* #b1341 "Schlafbeginn"
* #b1341 ^property[0].code = #None 
* #b1341 ^property[0].valueString = "Mentale Funktionen, die sich in einem Übergang zwischen Wachheit und Schlaf äußern" 
* #b1341 ^property[1].code = #parent 
* #b1341 ^property[1].valueCode = #b134 
* #b1342 "Aufrechterhaltung des Schlafes"
* #b1342 ^property[0].code = #None 
* #b1342 ^property[0].valueString = "Mentale Funktionen, die sich im Durchschlafvermögen äußern" 
* #b1342 ^property[1].code = #parent 
* #b1342 ^property[1].valueCode = #b134 
* #b1343 "Schlafqualität"
* #b1343 ^property[0].code = #None 
* #b1343 ^property[0].valueString = "Mentale Funktionen, die sich in einem natürlichen Schlaf mit einer optimalen physischen und geistigen Erholung und Entspannung äußern" 
* #b1343 ^property[1].code = #parent 
* #b1343 ^property[1].valueCode = #b134 
* #b1344 "Am Schlafzyklus beteiligte Funktionen"
* #b1344 ^property[0].code = #None 
* #b1344 ^property[0].valueString = "Mentale Funktionen, die sich in Rapid Eye Movements (REM)-Schlaf (verbunden mit Träumen) und in Non-Rapid Eye Movement Sleep (NREM) äußern. (Hier wird auf das traditionelle Konzept des Schlafes als einer Zeit verminderter physiologischer und psychologischer Aktivität Bezug genommen.)" 
* #b1344 ^property[1].code = #parent 
* #b1344 ^property[1].valueCode = #b134 
* #b1348 "Funktionen des Schlafes, anders bezeichnet"
* #b1348 ^property[0].code = #parent 
* #b1348 ^property[0].valueCode = #b134 
* #b1349 "Funktionen des Schlafes, nicht näher bezeichnet"
* #b1349 ^property[0].code = #parent 
* #b1349 ^property[0].valueCode = #b134 
* #b139 "Globale mentale Funktionen, anders oder nicht näher bezeichnet"
* #b139 ^property[0].code = #parent 
* #b139 ^property[0].valueCode = #b110-b139 
* #b140-b189 "Spezifische mentale Funktionen"
* #b140-b189 ^property[0].code = #parent 
* #b140-b189 ^property[0].valueCode = #b1 
* #b140-b189 ^property[1].code = #child 
* #b140-b189 ^property[1].valueCode = #b140 
* #b140-b189 ^property[2].code = #child 
* #b140-b189 ^property[2].valueCode = #b144 
* #b140-b189 ^property[3].code = #child 
* #b140-b189 ^property[3].valueCode = #b147 
* #b140-b189 ^property[4].code = #child 
* #b140-b189 ^property[4].valueCode = #b152 
* #b140-b189 ^property[5].code = #child 
* #b140-b189 ^property[5].valueCode = #b156 
* #b140-b189 ^property[6].code = #child 
* #b140-b189 ^property[6].valueCode = #b160 
* #b140-b189 ^property[7].code = #child 
* #b140-b189 ^property[7].valueCode = #b164 
* #b140-b189 ^property[8].code = #child 
* #b140-b189 ^property[8].valueCode = #b167 
* #b140-b189 ^property[9].code = #child 
* #b140-b189 ^property[9].valueCode = #b172 
* #b140-b189 ^property[10].code = #child 
* #b140-b189 ^property[10].valueCode = #b176 
* #b140-b189 ^property[11].code = #child 
* #b140-b189 ^property[11].valueCode = #b180 
* #b140-b189 ^property[12].code = #child 
* #b140-b189 ^property[12].valueCode = #b189 
* #b140 "Funktionen der Aufmerksamkeit"
* #b140 ^property[0].code = #None 
* #b140 ^property[0].valueString = "Spezifische mentale Funktionen, die die Fokussierung auf einen externen Reiz oder auf innere Vorgänge für eine geforderte Zeitspanne betreffen" 
* #b140 ^property[1].code = #None 
* #b140 ^property[1].valueString = "Funktionen, die Daueraufmerksamkeit, Wechsel der Aufmerksamkeit, geteilte Aufmerksamkeit, mit anderen geteilte Aufmerksamkeit, Konzentration und Ablenkbarkeit betreffen" 
* #b140 ^property[2].code = #None 
* #b140 ^property[2].valueString = "Funktionen des Bewusstseins" 
* #b140 ^property[3].code = #parent 
* #b140 ^property[3].valueCode = #b140-b189 
* #b140 ^property[4].code = #child 
* #b140 ^property[4].valueCode = #b1400 
* #b140 ^property[5].code = #child 
* #b140 ^property[5].valueCode = #b1401 
* #b140 ^property[6].code = #child 
* #b140 ^property[6].valueCode = #b1402 
* #b140 ^property[7].code = #child 
* #b140 ^property[7].valueCode = #b1403 
* #b140 ^property[8].code = #child 
* #b140 ^property[8].valueCode = #b1408 
* #b140 ^property[9].code = #child 
* #b140 ^property[9].valueCode = #b1409 
* #b1400 "Daueraufmerksamkeit"
* #b1400 ^property[0].code = #None 
* #b1400 ^property[0].valueString = "Mentale Funktionen, die sich in der Konzentration über eine geforderte Zeitspanne äußern" 
* #b1400 ^property[1].code = #parent 
* #b1400 ^property[1].valueCode = #b140 
* #b1401 "Wechsel oder Lenkung der Aufmerksamkeit"
* #b1401 ^property[0].code = #None 
* #b1401 ^property[0].valueString = "Mentale Funktionen, die die Umlenkung der Konzentration von einem Reiz auf einen anderen zulassen" 
* #b1401 ^property[1].code = #parent 
* #b1401 ^property[1].valueCode = #b140 
* #b1402 "Geteilte Aufmerksamkeit"
* #b1402 ^property[0].code = #None 
* #b1402 ^property[0].valueString = "Mentale Funktionen, die die gleichzeitige Fokussierung auf zwei oder mehr Reize zulassen" 
* #b1402 ^property[1].code = #parent 
* #b1402 ^property[1].valueCode = #b140 
* #b1403 "Mit anderen geteilte Aufmerksamkeit"
* #b1403 ^property[0].code = #None 
* #b1403 ^property[0].valueString = "Mentale Funktionen, die die Fokussierung auf denselben Reiz durch zwei oder mehr Personen zulassen, wenn z.B. ein Kind und ein Betreuer sich gemeinsam auf ein Spielzeug konzentrieren" 
* #b1403 ^property[1].code = #parent 
* #b1403 ^property[1].valueCode = #b140 
* #b1408 "Funktionen der Aufmerksamkeit, anders bezeichnet"
* #b1408 ^property[0].code = #parent 
* #b1408 ^property[0].valueCode = #b140 
* #b1409 "Funktionen der Aufmerksamkeit, nicht näher bezeichnet"
* #b1409 ^property[0].code = #parent 
* #b1409 ^property[0].valueCode = #b140 
* #b144 "Funktionen des Gedächtnisses"
* #b144 ^property[0].code = #None 
* #b144 ^property[0].valueString = "Spezifische mentale Funktionen, die die adäquate Registrierung, die Speicherung und den Abruf von Informationen betreffen" 
* #b144 ^property[1].code = #None 
* #b144 ^property[1].valueString = "Funktionen des Bewusstseins" 
* #b144 ^property[2].code = #None 
* #b144 ^property[2].valueString = "Funktionen, die Kurzzeitgedächtnis und Langzeitgedächtnis, Sofort-, Frisch- und Altgedächtnis, Gedächtnisspanne und Abrufen betreffen; Funktionen, die beim Wiedererkennen und Lernen benutzt werden, wie bei nominaler, selektiver und dissoziativer Amnesie" 
* #b144 ^property[3].code = #parent 
* #b144 ^property[3].valueCode = #b140-b189 
* #b144 ^property[4].code = #child 
* #b144 ^property[4].valueCode = #b1440 
* #b144 ^property[5].code = #child 
* #b144 ^property[5].valueCode = #b1441 
* #b144 ^property[6].code = #child 
* #b144 ^property[6].valueCode = #b1442 
* #b144 ^property[7].code = #child 
* #b144 ^property[7].valueCode = #b1448 
* #b144 ^property[8].code = #child 
* #b144 ^property[8].valueCode = #b1449 
* #b1440 "Kurzzeitgedächtnis"
* #b1440 ^property[0].code = #None 
* #b1440 ^property[0].valueString = "Mentale Funktionen, die sich in einer vorübergehenden, störbaren Gedächtnisspeicherung von etwa 30 Sekunden äußern. Aus diesem Speicher gehen Informationen verloren, wenn sie nicht im Langzeitgedächtnis verankert werden" 
* #b1440 ^property[1].code = #parent 
* #b1440 ^property[1].valueCode = #b144 
* #b1441 "Langzeitgedächtnis"
* #b1441 ^property[0].code = #None 
* #b1441 ^property[0].valueString = "Mentale Funktionen, die sich in einem Gedächtnissystem zur langzeitigen Übernahme von Informationen aus dem Kurzzeitgedächtnis und zum Abruf dieser Informationen äußern. Es gibt zwei unterschiedliche Formen des Langzeitgedächtnisses: ein autobiographisches (für Ereignisse der Vergangenheit) und semantisches (für Sprache und Sachverhalte)" 
* #b1441 ^property[1].code = #parent 
* #b1441 ^property[1].valueCode = #b144 
* #b1442 "Abrufen von Gedächtnisinhalten"
* #b1442 ^property[0].code = #None 
* #b1442 ^property[0].valueString = "Spezifische mentale Funktionen, die das Erinnern von Informationen aus dem Langzeitgedächtnis und zur Überleitung ins Bewusstsein betreffen" 
* #b1442 ^property[1].code = #parent 
* #b1442 ^property[1].valueCode = #b144 
* #b1448 "Funktionen des Gedächtnisses, anders bezeichnet"
* #b1448 ^property[0].code = #parent 
* #b1448 ^property[0].valueCode = #b144 
* #b1449 "Funktionen des Gedächtnisses, nicht näher bezeichnet"
* #b1449 ^property[0].code = #parent 
* #b1449 ^property[0].valueCode = #b144 
* #b147 "Psychomotorische Funktionen"
* #b147 ^property[0].code = #None 
* #b147 ^property[0].valueString = "Spezifische mentale Funktionen, die die Kontrolle über motorische und psychologische Vorgänge auf körperlicher Ebene betreffen" 
* #b147 ^property[1].code = #None 
* #b147 ^property[1].valueString = "Funktionen des Bewusstseins" 
* #b147 ^property[2].code = #None 
* #b147 ^property[2].valueString = "Funktionen, die die psychomotorische Kontrolle betreffen, wie bei psychomotorischer Retardierung, Erregung und Agitiertheit, Katatonie, Negativismus, Ambitendenz, Echopraxie und Echolalie; Qualität der psychomotorischen Funktionen" 
* #b147 ^property[3].code = #parent 
* #b147 ^property[3].valueCode = #b140-b189 
* #b147 ^property[4].code = #child 
* #b147 ^property[4].valueCode = #b1470 
* #b147 ^property[5].code = #child 
* #b147 ^property[5].valueCode = #b1471 
* #b147 ^property[6].code = #child 
* #b147 ^property[6].valueCode = #b1478 
* #b147 ^property[7].code = #child 
* #b147 ^property[7].valueCode = #b1479 
* #b1470 "Psychomotorische Kontrolle"
* #b1470 ^property[0].code = #None 
* #b1470 ^property[0].valueString = "Mentale Funktionen, die Tempo des Verhaltens oder Reaktionszeiten regulieren und an denen sowohl motorische als auch psychologische Komponenten beteiligt sind. Störungen der Kontrolle führen zu einer psychomotorischen Retardierung (langsames Sprechen und Bewegen, Verminderung von Gestik und spontanen Bewegungen). Störungen der Kontrolle können auch zu psychomotorischer Erregung führen (überschießendes Verhalten oder überschießende kognitive Aktivitäten, die im allgemeinen unproduktiv sind und auf einer inneren Anspannung beruhen. Beispiele sind Klopfen mit den Füßen, ständiges Händereiben, agitiertes Verhalten und Ruhelosigkeit)." 
* #b1470 ^property[1].code = #parent 
* #b1470 ^property[1].valueCode = #b147 
* #b1471 "Qualität der psychomotorischen Funktionen"
* #b1471 ^property[0].code = #None 
* #b1471 ^property[0].valueString = "Mentale Funktionen, die sich in einer angemessenen Abfolge und Art der Teilkomponenten nicht-verbalen Verhaltens äußern, wie Augen- und Handkoordination oder Gang" 
* #b1471 ^property[1].code = #parent 
* #b1471 ^property[1].valueCode = #b147 
* #b1478 "Psychomotorische Funktionen, anders bezeichnet"
* #b1478 ^property[0].code = #parent 
* #b1478 ^property[0].valueCode = #b147 
* #b1479 "Psychomotorische Funktionen, nicht näher bezeichnet"
* #b1479 ^property[0].code = #parent 
* #b1479 ^property[0].valueCode = #b147 
* #b152 "Emotionale Funktionen"
* #b152 ^property[0].code = #None 
* #b152 ^property[0].valueString = "Spezifische mentale Funktionen, die im Zusammenhang mit Gefühlen und den affektiven Komponenten von Bewusstseinsprozessen stehen" 
* #b152 ^property[1].code = #None 
* #b152 ^property[1].valueString = "Funktionen, die (Situations)Angemessenheit der Emotion, affektive Kontrolle und Schwingungsfähigkeit betreffen; Affekt; Trauer, Glück; Liebe, Furcht, Ärger, Hass, Anspannung, Angst, Freude, Sorgen; emotionale Labilität; Affektverflachung" 
* #b152 ^property[2].code = #None 
* #b152 ^property[2].valueString = "Funktionen von Temperament und Persönlichkeit" 
* #b152 ^property[3].code = #parent 
* #b152 ^property[3].valueCode = #b140-b189 
* #b152 ^property[4].code = #child 
* #b152 ^property[4].valueCode = #b1520 
* #b152 ^property[5].code = #child 
* #b152 ^property[5].valueCode = #b1521 
* #b152 ^property[6].code = #child 
* #b152 ^property[6].valueCode = #b1522 
* #b152 ^property[7].code = #child 
* #b152 ^property[7].valueCode = #b1528 
* #b152 ^property[8].code = #child 
* #b152 ^property[8].valueCode = #b1529 
* #b1520 "(Situations)Angemessenheit der Emotion"
* #b1520 ^property[0].code = #None 
* #b1520 ^property[0].valueString = "Mentale Funktionen, die sich in der Übereinstimmung des Gefühls oder des Affektes mit der Situation äußern, wie Glücksgefühl, wenn man gute Nachrichten erhält" 
* #b1520 ^property[1].code = #parent 
* #b1520 ^property[1].valueCode = #b152 
* #b1521 "Affektkontrolle"
* #b1521 ^property[0].code = #None 
* #b1521 ^property[0].valueString = "Mentale Funktion, die Erleben und Ausdruck von Affekten kontrolliert" 
* #b1521 ^property[1].code = #parent 
* #b1521 ^property[1].valueCode = #b152 
* #b1522 "Spannweite von Emotionen"
* #b1522 ^property[0].code = #None 
* #b1522 ^property[0].valueString = "Mentale Funktionen, die sich im Spektrum von Gefühlsregungen oder Gefühlen äußern, wie Liebe, Hass, Angst, Sorgen, Freude, Furcht und Ärger" 
* #b1522 ^property[1].code = #parent 
* #b1522 ^property[1].valueCode = #b152 
* #b1528 "Emotionale Funktionen, anders bezeichnet"
* #b1528 ^property[0].code = #parent 
* #b1528 ^property[0].valueCode = #b152 
* #b1529 "Emotionale Funktionen, nicht näher bezeichnet"
* #b1529 ^property[0].code = #parent 
* #b1529 ^property[0].valueCode = #b152 
* #b156 "Funktionen der Wahrnehmung"
* #b156 ^property[0].code = #None 
* #b156 ^property[0].valueString = "Spezifische mentale Funktionen, die die Erkennung und Interpretation sensorischer Reize betreffen" 
* #b156 ^property[1].code = #None 
* #b156 ^property[1].valueString = "Funktionen, die visuelle, auditive, olfaktorische, gustatorische, taktile und räumlich-visuelle Wahrnehmung betreffen, wie bei Halluzination oder Illusion" 
* #b156 ^property[2].code = #None 
* #b156 ^property[2].valueString = "Funktionen des Bewusstseins" 
* #b156 ^property[3].code = #parent 
* #b156 ^property[3].valueCode = #b140-b189 
* #b156 ^property[4].code = #child 
* #b156 ^property[4].valueCode = #b1560 
* #b156 ^property[5].code = #child 
* #b156 ^property[5].valueCode = #b1561 
* #b156 ^property[6].code = #child 
* #b156 ^property[6].valueCode = #b1562 
* #b156 ^property[7].code = #child 
* #b156 ^property[7].valueCode = #b1563 
* #b156 ^property[8].code = #child 
* #b156 ^property[8].valueCode = #b1564 
* #b156 ^property[9].code = #child 
* #b156 ^property[9].valueCode = #b1565 
* #b156 ^property[10].code = #child 
* #b156 ^property[10].valueCode = #b1568 
* #b156 ^property[11].code = #child 
* #b156 ^property[11].valueCode = #b1569 
* #b1560 "Auditive Wahrnehmung"
* #b1560 ^property[0].code = #None 
* #b1560 ^property[0].valueString = "Mentale Funktionen, die an der Unterscheidung von Geräuschen, Tönen, Tonhöhe und anderen auditiven Reizen beteiligt sind" 
* #b1560 ^property[1].code = #parent 
* #b1560 ^property[1].valueCode = #b156 
* #b1561 "Visuelle Wahrnehmung"
* #b1561 ^property[0].code = #None 
* #b1561 ^property[0].valueString = "Mentale Funktionen, die an der Unterscheidung von Form, Größe, Farbe und anderen visuellen Reizen beteiligt sind" 
* #b1561 ^property[1].code = #parent 
* #b1561 ^property[1].valueCode = #b156 
* #b1562 "Geruchswahrnehmung"
* #b1562 ^property[0].code = #None 
* #b1562 ^property[0].valueString = "Mentale Funktionen, die an der Erkennung unterschiedlicher Gerüche beteiligt sind" 
* #b1562 ^property[1].code = #parent 
* #b1562 ^property[1].valueCode = #b156 
* #b1563 "Geschmackswahrnehmung"
* #b1563 ^property[0].code = #None 
* #b1563 ^property[0].valueString = "Mentale Funktionen, die an der Unterscheidung von Geschmackseigenschaften wie süß, sauer, salzig und bitter auf der Zunge beteiligt sind" 
* #b1563 ^property[1].code = #parent 
* #b1563 ^property[1].valueCode = #b156 
* #b1564 "Taktile Wahrnehmung"
* #b1564 ^property[0].code = #None 
* #b1564 ^property[0].valueString = "Mentale Funktionen, die an der Differenzierung der Beschaffenheit von Oberflächen wie rau oder glatt durch Berührung beteiligt sind" 
* #b1564 ^property[1].code = #parent 
* #b1564 ^property[1].valueCode = #b156 
* #b1565 "Räumlich-visuelle Wahrnehmung"
* #b1565 ^property[0].code = #None 
* #b1565 ^property[0].valueString = "Mentale Funktion, die am visuellen Erkennen von räumlichen Bezügen der Objekte in der Umgebung zueinander oder zu einem selbst beteiligt sind" 
* #b1565 ^property[1].code = #parent 
* #b1565 ^property[1].valueCode = #b156 
* #b1568 "Funktionen der Wahrnehmung, anders bezeichnet"
* #b1568 ^property[0].code = #parent 
* #b1568 ^property[0].valueCode = #b156 
* #b1569 "Funktionen der Wahrnehmung, nicht näher bezeichnet"
* #b1569 ^property[0].code = #parent 
* #b1569 ^property[0].valueCode = #b156 
* #b160 "Funktionen des Denkens"
* #b160 ^property[0].code = #None 
* #b160 ^property[0].valueString = "Spezifische mentale Funktionen, die im Zusammenhang mit dem formalen und inhaltlichen Ablauf des Denkens stehen" 
* #b160 ^property[1].code = #None 
* #b160 ^property[1].valueString = "Funktionen, die Tempo, Form, Kontrolle und Inhalt des Denkens betreffen; Funktionen, die zielgerichtetes und nicht zielgerichtetes Denken betreffen; Funktionen, die logisches Denken betreffen, wie bei Gedankendruck, Ideenflüchtigkeit, Denkhemmung, inkohärentes Denken, Vorbeidenken/Vorbeireden, umständliches Denken, Wahn, Zwangsgedanken, Zwangshandlungen" 
* #b160 ^property[2].code = #None 
* #b160 ^property[2].valueString = "Funktionen der Intelligenz" 
* #b160 ^property[3].code = #parent 
* #b160 ^property[3].valueCode = #b140-b189 
* #b160 ^property[4].code = #child 
* #b160 ^property[4].valueCode = #b1600 
* #b160 ^property[5].code = #child 
* #b160 ^property[5].valueCode = #b1601 
* #b160 ^property[6].code = #child 
* #b160 ^property[6].valueCode = #b1602 
* #b160 ^property[7].code = #child 
* #b160 ^property[7].valueCode = #b1603 
* #b160 ^property[8].code = #child 
* #b160 ^property[8].valueCode = #b1608 
* #b160 ^property[9].code = #child 
* #b160 ^property[9].valueCode = #b1609 
* #b1600 "Denktempo"
* #b1600 ^property[0].code = #None 
* #b1600 ^property[0].valueString = "Mentale Funktionen, die sich in der Geschwindigkeit des Denkprozesses äußern" 
* #b1600 ^property[1].code = #parent 
* #b1600 ^property[1].valueCode = #b160 
* #b1601 "Form des Denkens"
* #b1601 ^property[0].code = #None 
* #b1601 ^property[0].valueString = "Mentale Funktionen, die Kohärenz und Logik des Denkprozesses gewährleisten (formales Denken)" 
* #b1601 ^property[1].code = #None 
* #b1601 ^property[1].valueString = "Störungen wie Perseveration, Vorbeidenken/Vorbeireden und Umständlichkeit" 
* #b1601 ^property[2].code = #parent 
* #b1601 ^property[2].valueCode = #b160 
* #b1602 "Inhalt des Denkens"
* #b1602 ^property[0].code = #None 
* #b1602 ^property[0].valueString = "Mentale Funktionen, die Ideen und Inhalte im Denkprozess und das, was konzeptualisiert wird, betreffen (inhaltliches Denken)" 
* #b1602 ^property[1].code = #None 
* #b1602 ^property[1].valueString = "Störungen wie Wahn, überwertige Ideen und Somatisierung" 
* #b1602 ^property[2].code = #parent 
* #b1602 ^property[2].valueCode = #b160 
* #b1603 "Kontrolle des Denkens"
* #b1603 ^property[0].code = #None 
* #b1603 ^property[0].valueString = "Mentale Funktionen, die die willkürliche Kontrolle über das Denken beinhalten und die als solche von der Person selbst erkannt werden" 
* #b1603 ^property[1].code = #None 
* #b1603 ^property[1].valueString = "Störungen wie Déjà-vu-Erleben, Zwang, Gedankenbeeinflussung und Gedankeneingebung" 
* #b1603 ^property[2].code = #parent 
* #b1603 ^property[2].valueCode = #b160 
* #b1608 "Funktionen des Denkens, anders bezeichnet"
* #b1608 ^property[0].code = #parent 
* #b1608 ^property[0].valueCode = #b160 
* #b1609 "Funktionen des Denkens, nicht näher bezeichnet"
* #b1609 ^property[0].code = #parent 
* #b1609 ^property[0].valueCode = #b160 
* #b164 "Höhere kognitive Funktionen"
* #b164 ^property[0].code = #None 
* #b164 ^property[0].valueString = "Spezifische mentale Funktionen, die insbesondere von den Frontallappen des Gehirns abhängen, einschließlich komplexe zielgerichtete Verhaltensweisen wie Entscheidungen treffen, abstrakt denken sowie einen Plan aufstellen und durchführen, mentale Flexibilität, sowie entscheiden, welche Verhaltensweisen unter welchen Umständen angemessen sind (häufig ?exekutive Funktionen? genannt)" 
* #b164 ^property[1].code = #None 
* #b164 ^property[1].valueString = "Funktionen des Gedächtnisses" 
* #b164 ^property[2].code = #None 
* #b164 ^property[2].valueString = "Funktionen, die Abstraktionsvermögen und Ordnen von Ideen betreffen; Zeitmanagement, Einsichts- und Urteilsvermögen; Konzeptbildung, Kategorisierung und kognitive Flexibilität" 
* #b164 ^property[3].code = #parent 
* #b164 ^property[3].valueCode = #b140-b189 
* #b164 ^property[4].code = #child 
* #b164 ^property[4].valueCode = #b1640 
* #b164 ^property[5].code = #child 
* #b164 ^property[5].valueCode = #b1641 
* #b164 ^property[6].code = #child 
* #b164 ^property[6].valueCode = #b1642 
* #b164 ^property[7].code = #child 
* #b164 ^property[7].valueCode = #b1643 
* #b164 ^property[8].code = #child 
* #b164 ^property[8].valueCode = #b1644 
* #b164 ^property[9].code = #child 
* #b164 ^property[9].valueCode = #b1645 
* #b164 ^property[10].code = #child 
* #b164 ^property[10].valueCode = #b1646 
* #b164 ^property[11].code = #child 
* #b164 ^property[11].valueCode = #b1648 
* #b164 ^property[12].code = #child 
* #b164 ^property[12].valueCode = #b1649 
* #b1640 "Das Abstraktionsvermögen betreffende Funktionen"
* #b1640 ^property[0].code = #None 
* #b1640 ^property[0].valueString = "Mentale Funktionen, die die Entwicklung von allgemeinen Vorstellungen, Qualitäten oder Charakteristiken betreffen, hervorgegangen aus und losgelöst von den konkreten Realitäten, spezifischen Gegenständen oder aktuellen Gegebenheiten" 
* #b1640 ^property[1].code = #parent 
* #b1640 ^property[1].valueCode = #b164 
* #b1641 "Das Organisieren und Planen betreffende Funktionen"
* #b1641 ^property[0].code = #None 
* #b1641 ^property[0].valueString = "Mentale Funktionen, die das Zusammenfügen von Teilen zu einem Ganzen und das Systematisieren betreffen; diese mentale Funktion trägt dazu bei, eine methodische Vorgehens- oder Handlungsweise zu entwickeln" 
* #b1641 ^property[1].code = #parent 
* #b1641 ^property[1].valueCode = #b164 
* #b1642 "Das Zeitmanagement betreffende Funktionen"
* #b1642 ^property[0].code = #None 
* #b1642 ^property[0].valueString = "Mentale Funktionen, die das Ordnen von Ereignissen in eine chronologische Reihenfolge und das Zuweisen von Zeiten zu Ereignissen und Aktivitäten betreffen" 
* #b1642 ^property[1].code = #parent 
* #b1642 ^property[1].valueCode = #b164 
* #b1643 "Kognitive Flexibilität"
* #b1643 ^property[0].code = #None 
* #b1643 ^property[0].valueString = "Mentale Funktionen, die das Ändern von Strategien oder Denkansätzen betreffen, insbesondere beim Problemlösen" 
* #b1643 ^property[1].code = #parent 
* #b1643 ^property[1].valueCode = #b164 
* #b1644 "Das Einsichtsvermögen betreffende Funktionen"
* #b1644 ^property[0].code = #None 
* #b1644 ^property[0].valueString = "Mentale Funktionen, die Bewusstsein und Verstehen der eigenen Person und des eigenen Verhaltens betreffen" 
* #b1644 ^property[1].code = #parent 
* #b1644 ^property[1].valueCode = #b164 
* #b1645 "Das Urteilsvermögen betreffende Funktionen"
* #b1645 ^property[0].code = #None 
* #b1645 ^property[0].valueString = "Mentale Funktionen, die daran beteiligt sind, zwischen verschiedenen Möglichkeiten zu unterscheiden und diese zu bewerten, wie solche, die an der Meinungsbildung beteiligt sind" 
* #b1645 ^property[1].code = #parent 
* #b1645 ^property[1].valueCode = #b164 
* #b1646 "Das Problemlösungsvermögen betreffende Funktionen"
* #b1646 ^property[0].code = #None 
* #b1646 ^property[0].valueString = "Mentale Funktionen, die Identifizieren, Analysieren und Integrieren nicht übereinstimmender oder sich widersprechender Informationen in eine Lösung betreffen" 
* #b1646 ^property[1].code = #parent 
* #b1646 ^property[1].valueCode = #b164 
* #b1648 "Höhere kognitive Funktionen, anders bezeichnet"
* #b1648 ^property[0].code = #parent 
* #b1648 ^property[0].valueCode = #b164 
* #b1649 "Höhere kognitive Funktionen, nicht näher bezeichnet"
* #b1649 ^property[0].code = #parent 
* #b1649 ^property[0].valueCode = #b164 
* #b167 "Kognitiv-sprachliche Funktionen"
* #b167 ^property[0].code = #None 
* #b167 ^property[0].valueString = "Spezifische mentale Funktionen, die das Erkennen und Verwenden von Zeichen, Symbolen und anderen Teilbereichen einer Sprache betreffen" 
* #b167 ^property[1].code = #None 
* #b167 ^property[1].valueString = "Funktionen der Aufmerksamkeit" 
* #b167 ^property[2].code = #None 
* #b167 ^property[2].valueString = "Funktionen, die Verständnis und Entschlüsselung von gesprochener, geschriebener oder anderer Formen von Sprache wie Gebärdensprache betreffen; Funktionen, die das Ausdrucksvermögen in gesprochener, geschriebener oder anderer Form von Sprache betreffen; integratives Sprachvermögen in Sprache und Schrift, wie sie an der sensorischen (rezeptiven), motorischen (expressiven), Broca-, Wernicke- und Leitungsaphasie beteiligt sind" 
* #b167 ^property[3].code = #parent 
* #b167 ^property[3].valueCode = #b140-b189 
* #b167 ^property[4].code = #child 
* #b167 ^property[4].valueCode = #b1670 
* #b167 ^property[5].code = #child 
* #b167 ^property[5].valueCode = #b1671 
* #b167 ^property[6].code = #child 
* #b167 ^property[6].valueCode = #b1672 
* #b167 ^property[7].code = #child 
* #b167 ^property[7].valueCode = #b1678 
* #b167 ^property[8].code = #child 
* #b167 ^property[8].valueCode = #b1679 
* #b1670 "Das Sprachverständnis betreffende Funktionen"
* #b1670 ^property[0].code = #None 
* #b1670 ^property[0].valueString = "Spezifische mentale Funktionen, die Verstehen und Erfassen der Bedeutung von Mitteilungen in gesprochener, geschriebener, symbolisierter oder anderer Form betreffen" 
* #b1670 ^property[1].code = #parent 
* #b1670 ^property[1].valueCode = #b167 
* #b1670 ^property[2].code = #child 
* #b1670 ^property[2].valueCode = #b16700 
* #b1670 ^property[3].code = #child 
* #b1670 ^property[3].valueCode = #b16701 
* #b1670 ^property[4].code = #child 
* #b1670 ^property[4].valueCode = #b16702 
* #b1670 ^property[5].code = #child 
* #b1670 ^property[5].valueCode = #b16708 
* #b1670 ^property[6].code = #child 
* #b1670 ^property[6].valueCode = #b16709 
* #b16700 "Das Verständnis gesprochener Sprache betreffende Funktionen"
* #b16700 ^property[0].code = #None 
* #b16700 ^property[0].valueString = "Mentale Funktionen, die Verstehen und Erfassen der Bedeutung von gesprochenen Mitteilungen betreffen" 
* #b16700 ^property[1].code = #parent 
* #b16700 ^property[1].valueCode = #b1670 
* #b16701 "Das Verständnis geschriebener Sprache betreffende Funktionen"
* #b16701 ^property[0].code = #None 
* #b16701 ^property[0].valueString = "Mentale Funktionen, die Verstehen und Erfassen der Bedeutung von schriftlichen Mitteilungen betreffen" 
* #b16701 ^property[1].code = #parent 
* #b16701 ^property[1].valueCode = #b1670 
* #b16702 "Das Verständnis der Gebärdensprache betreffende Funktionen"
* #b16702 ^property[0].code = #None 
* #b16702 ^property[0].valueString = "Mentale Funktionen, die das Verstehen und Erfassen der Bedeutung von Mitteilungen in Sprachen, die mittels Hand- und anderen Bewegungen erzeugte Zeichen benutzen, betreffen" 
* #b16702 ^property[1].code = #parent 
* #b16702 ^property[1].valueCode = #b1670 
* #b16708 "Das Sprachverständnis betreffende Funktionen, anders bezeichnet"
* #b16708 ^property[0].code = #parent 
* #b16708 ^property[0].valueCode = #b1670 
* #b16709 "Das Sprachverständnis betreffende Funktionen, nicht näher bezeichnet"
* #b16709 ^property[0].code = #parent 
* #b16709 ^property[0].valueCode = #b1670 
* #b1671 "Das sprachliche Ausdrucksvermögen betreffende Funktionen"
* #b1671 ^property[0].code = #None 
* #b1671 ^property[0].valueString = "Spezifische mentale Funktionen, die notwendig sind, um sinnvolle Mitteilungen in gesprochener, geschriebener, symbolischer oder anderer Form zu produzieren" 
* #b1671 ^property[1].code = #parent 
* #b1671 ^property[1].valueCode = #b167 
* #b1671 ^property[2].code = #child 
* #b1671 ^property[2].valueCode = #b16710 
* #b1671 ^property[3].code = #child 
* #b1671 ^property[3].valueCode = #b16711 
* #b1671 ^property[4].code = #child 
* #b1671 ^property[4].valueCode = #b16712 
* #b1671 ^property[5].code = #child 
* #b1671 ^property[5].valueCode = #b16718 
* #b1671 ^property[6].code = #child 
* #b1671 ^property[6].valueCode = #b16719 
* #b16710 "Das lautsprachliche Ausdrucksvermögen betreffende Funktionen"
* #b16710 ^property[0].code = #None 
* #b16710 ^property[0].valueString = "Mentale Funktionen, die notwendig sind, in der gesprochenen Sprache sinnvolle Mitteilungen auszudrücken" 
* #b16710 ^property[1].code = #parent 
* #b16710 ^property[1].valueCode = #b1671 
* #b16711 "Das schriftsprachliche Ausdrucksvermögen betreffende Funktionen"
* #b16711 ^property[0].code = #None 
* #b16711 ^property[0].valueString = "Mentale Funktionen, die notwendig sind, schriftsprachlich sinnvolle Mitteilungen zu verfassen" 
* #b16711 ^property[1].code = #parent 
* #b16711 ^property[1].valueCode = #b1671 
* #b16712 "Das Ausdrucksvermögen in Gebärdensprache betreffende Funktionen"
* #b16712 ^property[0].code = #None 
* #b16712 ^property[0].valueString = "Mentale Funktionen, die notwendig sind, sinnvolle Mitteilungen in Sprachen auszudrücken, die mittels Hand- und anderen Bewegungen erzeugte Zeichen verwenden" 
* #b16712 ^property[1].code = #parent 
* #b16712 ^property[1].valueCode = #b1671 
* #b16718 "Das sprachliche Ausdrucksvermögen betreffende Funktionen, anders bezeichnet"
* #b16718 ^property[0].code = #parent 
* #b16718 ^property[0].valueCode = #b1671 
* #b16719 "Das sprachliche Ausdrucksvermögen betreffende Funktionen, nicht näher bezeichnet"
* #b16719 ^property[0].code = #parent 
* #b16719 ^property[0].valueCode = #b1671 
* #b1672 "Integrative Sprachfunktionen"
* #b1672 ^property[0].code = #None 
* #b1672 ^property[0].valueString = "Mentale Funktionen, die semantische und symbolische Bedeutung, grammatische Struktur und Inhalte ordnen, um Mitteilungen in gesprochener, geschriebener oder anderer Form produzieren zu können" 
* #b1672 ^property[1].code = #parent 
* #b1672 ^property[1].valueCode = #b167 
* #b1678 "Kognitiv-sprachliche Funktionen, anders bezeichnet"
* #b1678 ^property[0].code = #parent 
* #b1678 ^property[0].valueCode = #b167 
* #b1679 "Kognitiv-sprachliche Funktionen, nicht näher bezeichnet"
* #b1679 ^property[0].code = #parent 
* #b1679 ^property[0].valueCode = #b167 
* #b172 "Das Rechnen betreffende Funktionen"
* #b172 ^property[0].code = #None 
* #b172 ^property[0].valueString = "Spezifische mentale Funktionen, die Bestimmung, Abschätzung von und Umgang mit mathematischen Symbolen und Verfahren betreffen" 
* #b172 ^property[1].code = #None 
* #b172 ^property[1].valueString = "Funktionen, die Addition, Subtraktion und andere einfache mathematische Rechenarten betreffen; Funktionen, die komplexe mathematische Operationen betreffen" 
* #b172 ^property[2].code = #None 
* #b172 ^property[2].valueString = "Funktionen der Aufmerksamkeit" 
* #b172 ^property[3].code = #parent 
* #b172 ^property[3].valueCode = #b140-b189 
* #b172 ^property[4].code = #child 
* #b172 ^property[4].valueCode = #b1720 
* #b172 ^property[5].code = #child 
* #b172 ^property[5].valueCode = #b1721 
* #b172 ^property[6].code = #child 
* #b172 ^property[6].valueCode = #b1728 
* #b172 ^property[7].code = #child 
* #b172 ^property[7].valueCode = #b1729 
* #b1720 "Das einfache Rechnen betreffende Funktionen"
* #b1720 ^property[0].code = #None 
* #b1720 ^property[0].valueString = "Mentale Funktionen, die Rechnen mit Zahlen betreffen, wie Addition, Subtraktion, Multiplikation und Division" 
* #b1720 ^property[1].code = #parent 
* #b1720 ^property[1].valueCode = #b172 
* #b1721 "Das komplexe Rechnen betreffende Funktionen"
* #b1721 ^property[0].code = #None 
* #b1721 ^property[0].valueString = "Mentale Funktionen, die Umsetzen von Textaufgaben in arithmetische Verfahren, Umsetzen von mathematischer Formeln in arithmetische Verfahren sowie andere komplexe Operationen im Zusammenhang mit Zahlen betreffen" 
* #b1721 ^property[1].code = #parent 
* #b1721 ^property[1].valueCode = #b172 
* #b1728 "Das Rechnen betreffende Funktionen, anders bezeichnet"
* #b1728 ^property[0].code = #parent 
* #b1728 ^property[0].valueCode = #b172 
* #b1729 "Das Rechnen betreffende Funktionen, nicht näher bezeichnet"
* #b1729 ^property[0].code = #parent 
* #b1729 ^property[0].valueCode = #b172 
* #b176 "Mentale Funktionen, die die Durchführung komplexer Bewegungshandlungen betreffen"
* #b176 ^property[0].code = #None 
* #b176 ^property[0].valueString = "Spezifische mentale Funktionen, die die Aufeinanderfolge und Koordination komplexer, zweckgerichteter Bewegungen betreffen" 
* #b176 ^property[1].code = #None 
* #b176 ^property[1].valueString = "Psychomotorische Funktionen" 
* #b176 ^property[2].code = #None 
* #b176 ^property[2].valueString = "Funktionsstörungen wie ideatorische, ideomotorische, Ankleide-, okulomotorische, Sprech-Apraxie" 
* #b176 ^property[3].code = #parent 
* #b176 ^property[3].valueCode = #b140-b189 
* #b180 "Die Selbstwahrnehmung und die Zeitwahrnehmung betreffende Funktionen"
* #b180 ^property[0].code = #None 
* #b180 ^property[0].valueString = "Spezifische mentale Funktionen, die im Zusammenhang mit der bewussten Wahrnehmung der eigenen Identität, des eigenen Körpers, der eigenen Position in der eigenen realen Umwelt sowie der Zeit stehen" 
* #b180 ^property[1].code = #None 
* #b180 ^property[1].valueString = "Funktionen, die Selbsterfahrung, Körperschema und Zeitwahrnehmung betreffen" 
* #b180 ^property[2].code = #parent 
* #b180 ^property[2].valueCode = #b140-b189 
* #b180 ^property[3].code = #child 
* #b180 ^property[3].valueCode = #b1800 
* #b180 ^property[4].code = #child 
* #b180 ^property[4].valueCode = #b1801 
* #b180 ^property[5].code = #child 
* #b180 ^property[5].valueCode = #b1802 
* #b180 ^property[6].code = #child 
* #b180 ^property[6].valueCode = #b1808 
* #b180 ^property[7].code = #child 
* #b180 ^property[7].valueCode = #b1809 
* #b1800 "Selbstwahrnehmung"
* #b1800 ^property[0].code = #None 
* #b1800 ^property[0].valueString = "Spezifische mentale Funktionen, die die bewusste Wahrnehmung der eigenen Identität und der eigenen Position in der eigenen realen Umwelt betreffen" 
* #b1800 ^property[1].code = #None 
* #b1800 ^property[1].valueString = "Funktionsstörungen wie bei Depersonalisation und Realitätsverlust" 
* #b1800 ^property[2].code = #parent 
* #b1800 ^property[2].valueCode = #b180 
* #b1801 "Körperschema"
* #b1801 ^property[0].code = #None 
* #b1801 ^property[0].valueString = "Spezifische mentale Funktionen, die im Zusammenhang mit dem Bild und dem Bewusstsein des eigenen Körpers stehen" 
* #b1801 ^property[1].code = #None 
* #b1801 ^property[1].valueString = "Funktionsstörungen wie Phantomgliedmaße und das Gefühl, zu dick oder zu dünn zu sein" 
* #b1801 ^property[2].code = #parent 
* #b1801 ^property[2].valueCode = #b180 
* #b1802 "Zeitwahrnehmung"
* #b1802 ^property[0].code = #None 
* #b1802 ^property[0].valueString = "Spezifische mentale Funktionen, die die subjektive Wahrnehmung in Bezug auf Länge und Verlauf von Zeit betreffen" 
* #b1802 ^property[1].code = #None 
* #b1802 ^property[1].valueString = "Funktionsstörungen wie Jamais-vu- und Déjà-vu-Erlebnisse" 
* #b1802 ^property[2].code = #parent 
* #b1802 ^property[2].valueCode = #b180 
* #b1808 "Die Selbstwahrnehmung und die Zeitwahrnehmung betreffende Funktionen, anders bezeichnet"
* #b1808 ^property[0].code = #parent 
* #b1808 ^property[0].valueCode = #b180 
* #b1809 "Die Selbstwahrnehmung und die Zeitwahrnehmung betreffende Funktionen, nicht näher bezeichnet"
* #b1809 ^property[0].code = #parent 
* #b1809 ^property[0].valueCode = #b180 
* #b189 "Spezielle mentale Funktionen, anders oder nicht näher bezeichnet"
* #b189 ^property[0].code = #parent 
* #b189 ^property[0].valueCode = #b140-b189 
* #b198 "Mentale Funktionen, anders bezeichnet"
* #b198 ^property[0].code = #parent 
* #b198 ^property[0].valueCode = #b1 
* #b199 "Mentale Funktionen, nicht näher bezeichnet"
* #b199 ^property[0].code = #parent 
* #b199 ^property[0].valueCode = #b1 
* #b2 "Sinnesfunktionen und Schmerz"
* #b2 ^property[0].code = #None 
* #b2 ^property[0].valueString = "Dieses Kapitel befasst sich mit den Funktionen der Sinne wie Sehen, Hören, Schmecken usw. sowie mit Schmerzempfindung" 
* #b2 ^property[1].code = #parent 
* #b2 ^property[1].valueCode = #b 
* #b2 ^property[2].code = #child 
* #b2 ^property[2].valueCode = #b210-b229 
* #b2 ^property[3].code = #child 
* #b2 ^property[3].valueCode = #b230-b249 
* #b2 ^property[4].code = #child 
* #b2 ^property[4].valueCode = #b250-b279 
* #b2 ^property[5].code = #child 
* #b2 ^property[5].valueCode = #b280-b289 
* #b2 ^property[6].code = #child 
* #b2 ^property[6].valueCode = #b298 
* #b2 ^property[7].code = #child 
* #b2 ^property[7].valueCode = #b299 
* #b210-b229 "Seh- und verwandte Funktionen"
* #b210-b229 ^property[0].code = #parent 
* #b210-b229 ^property[0].valueCode = #b2 
* #b210-b229 ^property[1].code = #child 
* #b210-b229 ^property[1].valueCode = #b210 
* #b210-b229 ^property[2].code = #child 
* #b210-b229 ^property[2].valueCode = #b215 
* #b210-b229 ^property[3].code = #child 
* #b210-b229 ^property[3].valueCode = #b220 
* #b210-b229 ^property[4].code = #child 
* #b210-b229 ^property[4].valueCode = #b229 
* #b210 "Funktionen des Sehens (Sehsinn)"
* #b210 ^property[0].code = #None 
* #b210 ^property[0].valueString = "Sinnesfunktionen bezüglich der Wahrnehmung von Licht sowie von Form, Größe, Gestalt und Farbe des visuellen Reizes" 
* #b210 ^property[1].code = #None 
* #b210 ^property[1].valueString = "Funktionen der Wahrnehmung" 
* #b210 ^property[2].code = #None 
* #b210 ^property[2].valueString = "Die Sehschärfe betreffende Funktionen; das Gesichtsfeld betreffende Funktionen; Qualität des Sehvermögens; Licht- und Farbwahrnehmung, Sehschärfe bei Weit- und Nahsicht, einäugiges (monokulares) und beidäugiges (binokulares) Sehen; Bildqualität; Funktionsstörungen wie Kurzsichtigkeit (Myopie), Weitsichtigkeit (Hypermetropie), Hornhautverkrümmung (Astigmatismus), Halbseitenblindheit (Hemianopsie), Farbenblindheit, Tunnelsehen, zentrale oder periphere Gesichtsfeldausfälle (Skotome), Doppelbilder (Diplopie), Nachtblindheit, Hell- Dunkeladaptation" 
* #b210 ^property[3].code = #parent 
* #b210 ^property[3].valueCode = #b210-b229 
* #b210 ^property[4].code = #child 
* #b210 ^property[4].valueCode = #b2100 
* #b210 ^property[5].code = #child 
* #b210 ^property[5].valueCode = #b2101 
* #b210 ^property[6].code = #child 
* #b210 ^property[6].valueCode = #b2102 
* #b210 ^property[7].code = #child 
* #b210 ^property[7].valueCode = #b2108 
* #b210 ^property[8].code = #child 
* #b210 ^property[8].valueCode = #b2109 
* #b2100 "Die Sehschärfe (Visus) betreffende Funktionen"
* #b2100 ^property[0].code = #None 
* #b2100 ^property[0].valueString = "Sehfunktionen, die die beidäugige (binokulare) und einäugige (monokulare) Wahrnehmung von Formen und Konturen im Nah- und Fernbereich betreffen" 
* #b2100 ^property[1].code = #parent 
* #b2100 ^property[1].valueCode = #b210 
* #b2100 ^property[2].code = #child 
* #b2100 ^property[2].valueCode = #b21000 
* #b2100 ^property[3].code = #child 
* #b2100 ^property[3].valueCode = #b21001 
* #b2100 ^property[4].code = #child 
* #b2100 ^property[4].valueCode = #b21002 
* #b2100 ^property[5].code = #child 
* #b2100 ^property[5].valueCode = #b21003 
* #b2100 ^property[6].code = #child 
* #b2100 ^property[6].valueCode = #b21008 
* #b2100 ^property[7].code = #child 
* #b2100 ^property[7].valueCode = #b21009 
* #b21000 "Binokulare (beidäugige) Sehschärfe in der Ferne"
* #b21000 ^property[0].code = #None 
* #b21000 ^property[0].valueString = "Sehfunktionen, die die Wahrnehmung von Größe, Form und Kontur eines entfernten Objektes mit beiden Augen betreffen" 
* #b21000 ^property[1].code = #parent 
* #b21000 ^property[1].valueCode = #b2100 
* #b21001 "Monokulare (einäugige) Sehschärfe in der Ferne"
* #b21001 ^property[0].code = #None 
* #b21001 ^property[0].valueString = "Sehfunktionen, die die Wahrnehmung von Größe, Form und Kontur eines entfernten Objektes entweder mit dem rechten oder mit dem linken Auge betreffen" 
* #b21001 ^property[1].code = #parent 
* #b21001 ^property[1].valueCode = #b2100 
* #b21002 "Sehschärfe im Nahbereich bei beidäugigem (binokularem) Sehen"
* #b21002 ^property[0].code = #None 
* #b21002 ^property[0].valueString = "Sehfunktionen, die die Wahrnehmung von Größe, Form und Kontur eines nahen Objektes mit beiden Augen betreffen" 
* #b21002 ^property[1].code = #parent 
* #b21002 ^property[1].valueCode = #b2100 
* #b21003 "Sehschärfe im Nahbereich bei einäugigem (monokularem) Sehen"
* #b21003 ^property[0].code = #None 
* #b21003 ^property[0].valueString = "Sehfunktionen, die die Wahrnehmung von Größe, Form und Kontur eines nahen Objektes entweder mit dem rechten oder mit dem linken Auge betreffen" 
* #b21003 ^property[1].code = #parent 
* #b21003 ^property[1].valueCode = #b2100 
* #b21008 "Die Sehschärfe (Visus) betreffende Funktionen, anders bezeichnet"
* #b21008 ^property[0].code = #parent 
* #b21008 ^property[0].valueCode = #b2100 
* #b21009 "Die Sehschärfe (Visus) betreffende Funktionen, nicht näher bezeichnet"
* #b21009 ^property[0].code = #parent 
* #b21009 ^property[0].valueCode = #b2100 
* #b2101 "Das Gesichtsfeld betreffende Funktionen"
* #b2101 ^property[0].code = #None 
* #b2101 ^property[0].valueString = "Sehfunktionen, die sich auf den gesamten Bereich, der mit fixiertem Blick gesehen werden kann, beziehen" 
* #b2101 ^property[1].code = #None 
* #b2101 ^property[1].valueString = "Funktionsstörungen wie Gesichtsfeldausfall, Tunnelblick, Anopsien" 
* #b2101 ^property[2].code = #parent 
* #b2101 ^property[2].valueCode = #b210 
* #b2102 "Qualität des Sehvermögens"
* #b2102 ^property[0].code = #None 
* #b2102 ^property[0].valueString = "Sehfunktionen, die an Lichtempfindung, Farbsehvermögen, Kontrastempfindung und allgemeiner Bildqualität beteiligt sind" 
* #b2102 ^property[1].code = #parent 
* #b2102 ^property[1].valueCode = #b210 
* #b2102 ^property[2].code = #child 
* #b2102 ^property[2].valueCode = #b21020 
* #b2102 ^property[3].code = #child 
* #b2102 ^property[3].valueCode = #b21021 
* #b2102 ^property[4].code = #child 
* #b2102 ^property[4].valueCode = #b21022 
* #b2102 ^property[5].code = #child 
* #b2102 ^property[5].valueCode = #b21023 
* #b2102 ^property[6].code = #child 
* #b2102 ^property[6].valueCode = #b21028 
* #b2102 ^property[7].code = #child 
* #b2102 ^property[7].valueCode = #b21029 
* #b21020 "Lichtempfindung (Lichtsinn)"
* #b21020 ^property[0].code = #None 
* #b21020 ^property[0].valueString = "Sehfunktionen, die die Wahrnehmung einer geringen Lichtintensität (Helligkeitsminimum) und eines minimalen Helligkeitskontrasts (Kontrastschwelle) betreffen" 
* #b21020 ^property[1].code = #None 
* #b21020 ^property[1].valueString = "Die Hell-Dunkeladaptation betreffende Funktionen; Funktionsstörungen wie Nachtblindheit (verminderte Empfindlichkeit gegenüber Licht) und Photophobie (Lichtscheu)" 
* #b21020 ^property[2].code = #parent 
* #b21020 ^property[2].valueCode = #b2102 
* #b21021 "Farbsehvermögen (Farbsinn)"
* #b21021 ^property[0].code = #None 
* #b21021 ^property[0].valueString = "Sehfunktionen, die das Unterscheiden und Vergleichen von Farben betreffen" 
* #b21021 ^property[1].code = #parent 
* #b21021 ^property[1].valueCode = #b2102 
* #b21022 "Kontrastempfindung"
* #b21022 ^property[0].code = #None 
* #b21022 ^property[0].valueString = "Sehfunktionen, die die Unterscheidung eines Objekts vom Hintergrund mit der geringsten Leuchtdichte, die dafür erforderlich ist, betreffen" 
* #b21022 ^property[1].code = #parent 
* #b21022 ^property[1].valueCode = #b2102 
* #b21023 "Visuelle Bildqualität"
* #b21023 ^property[0].code = #None 
* #b21023 ^property[0].valueString = "Sehfunktionen, die an der Qualität des Bildes beteiligt sind" 
* #b21023 ^property[1].code = #None 
* #b21023 ^property[1].valueString = "Funktionsstörungen wie Sehen von Streulicht, beeinträchtigte intraokulare Bildqualität (Mouches volantes - durch Glaskörpertrübungen bedingte mückenartige Wahrnehmungen - und Schleier); Bildverzerrung, Sehen von Sternen und Blitzen" 
* #b21023 ^property[2].code = #parent 
* #b21023 ^property[2].valueCode = #b2102 
* #b21028 "Qualität des Sehvermögens, anders bezeichnet"
* #b21028 ^property[0].code = #parent 
* #b21028 ^property[0].valueCode = #b2102 
* #b21029 "Qualität des Sehvermögens, nicht näher bezeichnet"
* #b21029 ^property[0].code = #parent 
* #b21029 ^property[0].valueCode = #b2102 
* #b2108 "Funktionen des Sehens, anders bezeichnet"
* #b2108 ^property[0].code = #parent 
* #b2108 ^property[0].valueCode = #b210 
* #b2109 "Funktionen des Sehens, nicht näher bezeichnet"
* #b2109 ^property[0].code = #parent 
* #b2109 ^property[0].valueCode = #b210 
* #b215 "Funktionen von Strukturen, die in Verbindung mit dem Auge stehen"
* #b215 ^property[0].code = #None 
* #b215 ^property[0].valueString = "Funktion der Strukturen im Auge und um das Auge herum, die das Sehen ermöglichen" 
* #b215 ^property[1].code = #None 
* #b215 ^property[1].valueString = "Funktion der inneren Augenmuskeln, des Augenlids, der äußeren Augenmuskeln einschließlich der willkürlichen Bewegungen des Auges, der Augenfolgebewegungen und der Fähigkeit zur Fixierung des Auges, Tränendrüsen, Fähigkeit des Auges zur Scharfeinstellung (Akkomodation), Pupillenreaktion; Funktionsstörungen wie unwillkürliche ruckartige Augenbewegungen (Nystagmus), Augentrockenheit (Xerophthalmie), Herabhängen des Augenlids (Ptosis)" 
* #b215 ^property[2].code = #None 
* #b215 ^property[2].valueString = "Funktionen des Sehens (Sehsinn)" 
* #b215 ^property[3].code = #parent 
* #b215 ^property[3].valueCode = #b210-b229 
* #b215 ^property[4].code = #child 
* #b215 ^property[4].valueCode = #b2150 
* #b215 ^property[5].code = #child 
* #b215 ^property[5].valueCode = #b2151 
* #b215 ^property[6].code = #child 
* #b215 ^property[6].valueCode = #b2152 
* #b215 ^property[7].code = #child 
* #b215 ^property[7].valueCode = #b2153 
* #b215 ^property[8].code = #child 
* #b215 ^property[8].valueCode = #b2158 
* #b215 ^property[9].code = #child 
* #b215 ^property[9].valueCode = #b2159 
* #b2150 "Funktionen der Augeninnenmuskeln"
* #b2150 ^property[0].code = #None 
* #b2150 ^property[0].valueString = "Funktionen, die die Muskeln im Auge (wie bei der Iris) betreffen, welche Form und Größe der Pupille und der Linse regulieren" 
* #b2150 ^property[1].code = #None 
* #b2150 ^property[1].valueString = "Funktionen, die die Scharfeinstellung (Akkomodation) betreffen; Pupillenreaktion" 
* #b2150 ^property[2].code = #parent 
* #b2150 ^property[2].valueCode = #b215 
* #b2151 "Funktionen des Augenlids"
* #b2151 ^property[0].code = #None 
* #b2151 ^property[0].valueString = "Funktionen des Augenlids, wie zum Beispiel der Schutzreflex" 
* #b2151 ^property[1].code = #parent 
* #b2151 ^property[1].valueCode = #b215 
* #b2152 "Funktionen der externen Augenmuskeln"
* #b2152 ^property[0].code = #None 
* #b2152 ^property[0].valueString = "Funktionen, die die Muskeln betreffen, welche benutzt werden, um die Blickrichtung zu ändern, um ein sich durch das Gesichtsfeld bewegendes Objekt mit den Augen zu verfolgen, um ruckartige Augenbewegungen zur Verfolgung bewegter Ziele (Sakkaden) durchzuführen und um das Auge zu fixieren" 
* #b2152 ^property[1].code = #None 
* #b2152 ^property[1].valueString = "unwillkürliche ruckartige Augenbewegungen (Nystagmus); Koordination beider Augen" 
* #b2152 ^property[2].code = #parent 
* #b2152 ^property[2].valueCode = #b215 
* #b2153 "Funktionen der Tränendrüsen"
* #b2153 ^property[0].code = #None 
* #b2153 ^property[0].valueString = "Funktionen der Tränendrüsen und Tränengänge" 
* #b2153 ^property[1].code = #parent 
* #b2153 ^property[1].valueCode = #b215 
* #b2158 "Funktionen von Strukturen, die in Verbindung mit dem Auge stehen, anders bezeichnet"
* #b2158 ^property[0].code = #parent 
* #b2158 ^property[0].valueCode = #b215 
* #b2159 "Funktionen von Strukturen, die in Verbindung mit dem Auge stehen, nicht näher bezeichnet"
* #b2159 ^property[0].code = #parent 
* #b2159 ^property[0].valueCode = #b215 
* #b220 "Mit dem Auge und angrenzenden Strukturen verbundene Empfindungen"
* #b220 ^property[0].code = #None 
* #b220 ^property[0].valueString = "Empfindungen von Augenermüdung, von trockenen, juckenden Augen oder ähnliche Gefühle" 
* #b220 ^property[1].code = #None 
* #b220 ^property[1].valueString = "Empfindungen von Druck hinter dem Auge, Fremdkörpergefühl, Überanstrengung der Augen, Augenbrennen oder Augenreizung" 
* #b220 ^property[2].code = #None 
* #b220 ^property[2].valueString = "Schmerz" 
* #b220 ^property[3].code = #parent 
* #b220 ^property[3].valueCode = #b210-b229 
* #b229 "Seh- und verwandte Funktionen, anders oder nicht näher bezeichnet"
* #b229 ^property[0].code = #parent 
* #b229 ^property[0].valueCode = #b210-b229 
* #b230-b249 "Hör- und Vestibularfunktionen"
* #b230-b249 ^property[0].code = #parent 
* #b230-b249 ^property[0].valueCode = #b2 
* #b230-b249 ^property[1].code = #child 
* #b230-b249 ^property[1].valueCode = #b230 
* #b230-b249 ^property[2].code = #child 
* #b230-b249 ^property[2].valueCode = #b235 
* #b230-b249 ^property[3].code = #child 
* #b230-b249 ^property[3].valueCode = #b240 
* #b230-b249 ^property[4].code = #child 
* #b230-b249 ^property[4].valueCode = #b249 
* #b230 "Funktionen des Hörens (Hörsinn)"
* #b230 ^property[0].code = #None 
* #b230 ^property[0].valueString = "Sinnesfunktionen bezüglich der Wahrnehmung von Tönen oder Geräuschen und der Unterscheidung von deren Herkunftsort, Tonhöhe, Lautstärke und Qualität" 
* #b230 ^property[1].code = #None 
* #b230 ^property[1].valueString = "Funktionen des Hörens, akustische Differenzierung, Ortung der Geräuschquelle, Richtungshören, Spracherkennung; Funktionsstörungen wie Taubheit, Schwerhörigkeit, Einschränkung des Hörvermögens, Hörverlust" 
* #b230 ^property[2].code = #None 
* #b230 ^property[2].valueString = "Funktionen der Wahrnehmung" 
* #b230 ^property[3].code = #parent 
* #b230 ^property[3].valueCode = #b230-b249 
* #b230 ^property[4].code = #child 
* #b230 ^property[4].valueCode = #b2300 
* #b230 ^property[5].code = #child 
* #b230 ^property[5].valueCode = #b2301 
* #b230 ^property[6].code = #child 
* #b230 ^property[6].valueCode = #b2302 
* #b230 ^property[7].code = #child 
* #b230 ^property[7].valueCode = #b2303 
* #b230 ^property[8].code = #child 
* #b230 ^property[8].valueCode = #b2304 
* #b230 ^property[9].code = #child 
* #b230 ^property[9].valueCode = #b2308 
* #b230 ^property[10].code = #child 
* #b230 ^property[10].valueCode = #b2309 
* #b2300 "Schallwahrnehmung"
* #b2300 ^property[0].code = #None 
* #b2300 ^property[0].valueString = "Sinnesfunktionen, die die Wahrnehmung von Tönen oder Geräuschen betreffen" 
* #b2300 ^property[1].code = #parent 
* #b2300 ^property[1].valueCode = #b230 
* #b2301 "Auditive Differenzierung"
* #b2301 ^property[0].code = #None 
* #b2301 ^property[0].valueString = "Sinnesfunktionen, die die Wahrnehmung von Tönen oder Geräuschen betreffen, deren Abgrenzung von Hintergrundgeräuschen, die Zusammenführung auf beide Ohren getrennt einwirkenden Schalls zu einem Ganzen (binaurale Synthese) sowie die Trennung und Mischung von Tönen und Geräuschen" 
* #b2301 ^property[1].code = #parent 
* #b2301 ^property[1].valueCode = #b230 
* #b2302 "Ortung der Schallquelle"
* #b2302 ^property[0].code = #None 
* #b2302 ^property[0].valueString = "Sinnesfunktionen, die die Feststellung der örtlichen Herkunft eines Tones oder Geräusches betreffen" 
* #b2302 ^property[1].code = #parent 
* #b2302 ^property[1].valueCode = #b230 
* #b2303 "Richtungshören"
* #b2303 ^property[0].code = #None 
* #b2303 ^property[0].valueString = "Sinnesfunktionen, die die Unterscheidung, ob ein Ton oder Geräusch von rechts oder von links kommt, betreffen" 
* #b2303 ^property[1].code = #parent 
* #b2303 ^property[1].valueCode = #b230 
* #b2304 "Sprachdifferenzierung"
* #b2304 ^property[0].code = #None 
* #b2304 ^property[0].valueString = "Sinnesfunktionen, die das Erkennen gesprochener Sprache und die Unterscheidung dieser von anderen Tönen oder Geräuschen betreffen" 
* #b2304 ^property[1].code = #parent 
* #b2304 ^property[1].valueCode = #b230 
* #b2308 "Funktionen des Hörens, anders bezeichnet"
* #b2308 ^property[0].code = #parent 
* #b2308 ^property[0].valueCode = #b230 
* #b2309 "Funktionen des Hörens, nicht näher bezeichnet"
* #b2309 ^property[0].code = #parent 
* #b2309 ^property[0].valueCode = #b230 
* #b235 "Vestibuläre Funktionen"
* #b235 ^property[0].code = #None 
* #b235 ^property[0].valueString = "Sinnesfunktionen des Innenohrs, die Lage, Gleichgewicht und Bewegung betreffen" 
* #b235 ^property[1].code = #None 
* #b235 ^property[1].valueString = "Funktionen, die die Position und den Lagesinn sowie das Körpergleichgewicht und die Bewegung betreffen" 
* #b235 ^property[2].code = #None 
* #b235 ^property[2].valueString = "Mit den Hör- und vestibulären Funktionen verbundene Empfindungen" 
* #b235 ^property[3].code = #parent 
* #b235 ^property[3].valueCode = #b230-b249 
* #b235 ^property[4].code = #child 
* #b235 ^property[4].valueCode = #b2350 
* #b235 ^property[5].code = #child 
* #b235 ^property[5].valueCode = #b2351 
* #b235 ^property[6].code = #child 
* #b235 ^property[6].valueCode = #b2352 
* #b235 ^property[7].code = #child 
* #b235 ^property[7].valueCode = #b2358 
* #b235 ^property[8].code = #child 
* #b235 ^property[8].valueCode = #b2359 
* #b2350 "Vestibulärer Lagesinn"
* #b2350 ^property[0].code = #None 
* #b2350 ^property[0].valueString = "Sinnesfunktionen des Innenohrs, die die Feststellung der Körperausrichtung im Raum betreffen" 
* #b2350 ^property[1].code = #parent 
* #b2350 ^property[1].valueCode = #b235 
* #b2351 "Gleichgewichtssinn"
* #b2351 ^property[0].code = #None 
* #b2351 ^property[0].valueString = "Sinnesfunktionen des Innenohrs, die die Feststellung des Körpergleichgewichts betreffen" 
* #b2351 ^property[1].code = #parent 
* #b2351 ^property[1].valueCode = #b235 
* #b2352 "Vestibulärer Bewegungssinn"
* #b2352 ^property[0].code = #None 
* #b2352 ^property[0].valueString = "Sinnesfunktionen des Innenohrs, die die Feststellung der Körperbewegung im Raum betreffen, einschließlich ihrer Richtung und Geschwindigkeit" 
* #b2352 ^property[1].code = #parent 
* #b2352 ^property[1].valueCode = #b235 
* #b2358 "Vestibuläre Funktionen, anders bezeichnet"
* #b2358 ^property[0].code = #parent 
* #b2358 ^property[0].valueCode = #b235 
* #b2359 "Vestibuläre Funktionen, nicht näher bezeichnet"
* #b2359 ^property[0].code = #parent 
* #b2359 ^property[0].valueCode = #b235 
* #b240 "Mit den Hör- und vestibulären Funktionen verbundene Empfindungen"
* #b240 ^property[0].code = #None 
* #b240 ^property[0].valueString = "Schwindelgefühl, Gefühl des Fallens, Ohrgeräusche (Tinnitus) und Schwindel (Vertigo)" 
* #b240 ^property[1].code = #None 
* #b240 ^property[1].valueString = "Ohrenklingeln, Reizgefühl im Ohr, Druck im Ohr, Übelkeit in Verbindung mit Schwindelgefühl oder Schwindel" 
* #b240 ^property[2].code = #None 
* #b240 ^property[2].valueString = "Vestibuläre Funktionen" 
* #b240 ^property[3].code = #parent 
* #b240 ^property[3].valueCode = #b230-b249 
* #b240 ^property[4].code = #child 
* #b240 ^property[4].valueCode = #b2400 
* #b240 ^property[5].code = #child 
* #b240 ^property[5].valueCode = #b2401 
* #b240 ^property[6].code = #child 
* #b240 ^property[6].valueCode = #b2402 
* #b240 ^property[7].code = #child 
* #b240 ^property[7].valueCode = #b2403 
* #b240 ^property[8].code = #child 
* #b240 ^property[8].valueCode = #b2404 
* #b240 ^property[9].code = #child 
* #b240 ^property[9].valueCode = #b2405 
* #b240 ^property[10].code = #child 
* #b240 ^property[10].valueCode = #b2408 
* #b240 ^property[11].code = #child 
* #b240 ^property[11].valueCode = #b2409 
* #b2400 "Ohrgeräusche oder Tinnitus"
* #b2400 ^property[0].code = #None 
* #b2400 ^property[0].valueString = "Empfindung von tiefen, rauschenden, pfeifenden oder klingelnden Geräuschen im Ohr" 
* #b2400 ^property[1].code = #parent 
* #b2400 ^property[1].valueCode = #b240 
* #b2401 "Schwindelgefühl"
* #b2401 ^property[0].code = #None 
* #b2401 ^property[0].valueString = "Gefühl von Bewegung, an der man selbst oder seine Umgebung beteiligt ist; Gefühl von Drehen, Schwanken oder Kippen" 
* #b2401 ^property[1].code = #parent 
* #b2401 ^property[1].valueCode = #b240 
* #b2402 "Gefühl des Fallens"
* #b2402 ^property[0].code = #None 
* #b2402 ^property[0].valueString = "Gefühl, den Boden unter den Füßen zu verlieren und zu fallen" 
* #b2402 ^property[1].code = #parent 
* #b2402 ^property[1].valueCode = #b240 
* #b2403 "Übelkeit in Verbindung mit Schwindelgefühl oder Schwindel (Vertigo)"
* #b2403 ^property[0].code = #None 
* #b2403 ^property[0].valueString = "Gefühl, sich übergeben zu müssen, ausgelöst durch Schwindelgefühl oder Schwindel (Vertigo)" 
* #b2403 ^property[1].code = #parent 
* #b2403 ^property[1].valueCode = #b240 
* #b2404 "Reizgefühl im Ohr"
* #b2404 ^property[0].code = #None 
* #b2404 ^property[0].valueString = "Juckreiz oder ähnliche Empfindungen im Ohr" 
* #b2404 ^property[1].code = #parent 
* #b2404 ^property[1].valueCode = #b240 
* #b2405 "Druck im Ohr"
* #b2405 ^property[0].code = #None 
* #b2405 ^property[0].valueString = "Gefühl von Ohrdruck" 
* #b2405 ^property[1].code = #parent 
* #b2405 ^property[1].valueCode = #b240 
* #b2408 "Mit den Hör- und vestibulären Funktionen verbundene Empfindungen, anders bezeichnet"
* #b2408 ^property[0].code = #parent 
* #b2408 ^property[0].valueCode = #b240 
* #b2409 "Mit den Hör- und vestibulären Funktionen verbundene Empfindungen, nicht näher bezeichnet"
* #b2409 ^property[0].code = #parent 
* #b2409 ^property[0].valueCode = #b240 
* #b249 "Hör- und Vestibularfunktionen, anders oder nicht näher bezeichnet"
* #b249 ^property[0].code = #parent 
* #b249 ^property[0].valueCode = #b230-b249 
* #b250-b279 "Weitere Sinnesfunktionen"
* #b250-b279 ^property[0].code = #parent 
* #b250-b279 ^property[0].valueCode = #b2 
* #b250-b279 ^property[1].code = #child 
* #b250-b279 ^property[1].valueCode = #b250 
* #b250-b279 ^property[2].code = #child 
* #b250-b279 ^property[2].valueCode = #b255 
* #b250-b279 ^property[3].code = #child 
* #b250-b279 ^property[3].valueCode = #b260 
* #b250-b279 ^property[4].code = #child 
* #b250-b279 ^property[4].valueCode = #b265 
* #b250-b279 ^property[5].code = #child 
* #b250-b279 ^property[5].valueCode = #b270 
* #b250-b279 ^property[6].code = #child 
* #b250-b279 ^property[6].valueCode = #b279 
* #b250 "Funktionen des Schmeckens (Geschmackssinn)"
* #b250 ^property[0].code = #None 
* #b250 ^property[0].valueString = "Sinnesfunktionen, die die Wahrnehmung der Geschmacksqualitäten bitter, süß, sauer und salzig betreffen" 
* #b250 ^property[1].code = #None 
* #b250 ^property[1].valueString = "Funktionen des Schmeckens, des Geschmackssinns; Funktionsstörungen wie Verlust des Geschmacksvermögens (Ageusie) und Verminderung des Geschmacksvermögens (Hypogeusie)" 
* #b250 ^property[2].code = #parent 
* #b250 ^property[2].valueCode = #b250-b279 
* #b255 "Funktionen des Riechens (Geruchssinn)"
* #b255 ^property[0].code = #None 
* #b255 ^property[0].valueString = "Sinnesfunktionen, die die Wahrnehmung von Gerüchen und Düften betreffen" 
* #b255 ^property[1].code = #None 
* #b255 ^property[1].valueString = "Funktionen des Riechens; Funktionsstörungen wie fehlendes Geruchsvermögen (Anosmie) oder vermindertes Geruchsvermögen (Hyposmie)" 
* #b255 ^property[2].code = #parent 
* #b255 ^property[2].valueCode = #b250-b279 
* #b260 "Die Propriozeption betreffende Funktionen"
* #b260 ^property[0].code = #None 
* #b260 ^property[0].valueString = "Sinnesfunktionen, die die Wahrnehmung der Position der einzelnen Körperteile in Relation zum Körper betreffen" 
* #b260 ^property[1].code = #None 
* #b260 ^property[1].valueString = "Funktionen der Wahrnehmung der Körperposition (Statästhesie) und einer Körperbewegung (Kinästhesie)" 
* #b260 ^property[2].code = #None 
* #b260 ^property[2].valueString = "Vestibuläre Funktionen" 
* #b260 ^property[3].code = #parent 
* #b260 ^property[3].valueCode = #b250-b279 
* #b265 "Funktionen des Tastens (Tastsinn)"
* #b265 ^property[0].code = #None 
* #b265 ^property[0].valueString = "Sinnesfunktionen, die das Erkennen von Oberflächen sowie deren Beschaffenheit oder Qualität betreffen" 
* #b265 ^property[1].code = #None 
* #b265 ^property[1].valueString = "Funktionen des Tastens; Funktionsstörungen wie Taubheitsgefühle, Berührungsunempfindlichkeit (Anästhesie), Kribbelparästhesien, Missempfindungen (Parästhesien), Überempfindlichkeiten (Hyperästhesien)" 
* #b265 ^property[2].code = #None 
* #b265 ^property[2].valueString = "Sinnesfunktionen bezüglich Temperatur und anderer Reize" 
* #b265 ^property[3].code = #parent 
* #b265 ^property[3].valueCode = #b250-b279 
* #b270 "Sinnesfunktionen bezüglich Temperatur und anderer Reize"
* #b270 ^property[0].code = #None 
* #b270 ^property[0].valueString = "Sinnesfunktionen, die die Wahrnehmung von Temperatur, Vibration, Druck und schädigenden Reizen betreffen" 
* #b270 ^property[1].code = #None 
* #b270 ^property[1].valueString = "Funktionen, die das Empfinden von Temperatur, Vibration, Erschütterung oder Schwingungen, oberflächlichem Druck, tiefem Druck, Brennen oder schädlichen Reizen betreffen" 
* #b270 ^property[2].code = #None 
* #b270 ^property[2].valueString = "Funktionen des Tastens (Tastsinn)" 
* #b270 ^property[3].code = #parent 
* #b270 ^property[3].valueCode = #b250-b279 
* #b270 ^property[4].code = #child 
* #b270 ^property[4].valueCode = #b2700 
* #b270 ^property[5].code = #child 
* #b270 ^property[5].valueCode = #b2701 
* #b270 ^property[6].code = #child 
* #b270 ^property[6].valueCode = #b2702 
* #b270 ^property[7].code = #child 
* #b270 ^property[7].valueCode = #b2703 
* #b270 ^property[8].code = #child 
* #b270 ^property[8].valueCode = #b2708 
* #b270 ^property[9].code = #child 
* #b270 ^property[9].valueCode = #b2709 
* #b2700 "Temperaturempfinden"
* #b2700 ^property[0].code = #None 
* #b2700 ^property[0].valueString = "Sinnesfunktionen, die die Wahrnehmung von kalt und heiß betreffen" 
* #b2700 ^property[1].code = #parent 
* #b2700 ^property[1].valueCode = #b270 
* #b2701 "Vibrationsempfinden"
* #b2701 ^property[0].code = #None 
* #b2701 ^property[0].valueString = "Sinnesfunktionen, die die Wahrnehmung von Erschütterungen oder Schwingungen betreffen" 
* #b2701 ^property[1].code = #parent 
* #b2701 ^property[1].valueCode = #b270 
* #b2702 "Druck- und Berührungsempfinden"
* #b2702 ^property[0].code = #None 
* #b2702 ^property[0].valueString = "Sinnesfunktionen, die die Wahrnehmung von Druck auf die Haut betreffen" 
* #b2702 ^property[1].code = #None 
* #b2702 ^property[1].valueString = "Funktionsstörungen wie Berührungsempfindlichkeit, Taubheit, verringerte (Hypästhesie) oder gesteigerte Empfindlichkeit (Hyperästhesie), Kribbelparästhesien und Jucken" 
* #b2702 ^property[2].code = #parent 
* #b2702 ^property[2].valueCode = #b270 
* #b2703 "Wahrnehmung schädlicher Reize"
* #b2703 ^property[0].code = #None 
* #b2703 ^property[0].valueString = "Sinnesfunktionen, die die Wahrnehmung schmerzhafter oder unangenehmer Reize betreffen" 
* #b2703 ^property[1].code = #None 
* #b2703 ^property[1].valueString = "Funktionsstörungen wie herabgesetztes oder gesteigertes Schmerzempfinden (Hypalgesie, Hyperpathie), verändertes Schmerzempfinden (Allodynie), aufgehobenes Schmerzempfinden (Analgesie), schmerzhafte Empfindungslosigkeit (Anästhesia dolorosa)" 
* #b2703 ^property[2].code = #parent 
* #b2703 ^property[2].valueCode = #b270 
* #b2708 "Sinnesfunktionen bezüglich Temperatur und anderer Reize, anders bezeichnet"
* #b2708 ^property[0].code = #parent 
* #b2708 ^property[0].valueCode = #b270 
* #b2709 "Sinnesfunktionen bezüglich Temperatur und anderer Reize, nicht näher bezeichnet"
* #b2709 ^property[0].code = #parent 
* #b2709 ^property[0].valueCode = #b270 
* #b279 "Weitere Sinnesfunktionen, anders oder nicht näher bezeichnet"
* #b279 ^property[0].code = #parent 
* #b279 ^property[0].valueCode = #b250-b279 
* #b280-b289 "Schmerz"
* #b280-b289 ^property[0].code = #parent 
* #b280-b289 ^property[0].valueCode = #b2 
* #b280-b289 ^property[1].code = #child 
* #b280-b289 ^property[1].valueCode = #b280 
* #b280-b289 ^property[2].code = #child 
* #b280-b289 ^property[2].valueCode = #b289 
* #b280 "Schmerz"
* #b280 ^property[0].code = #None 
* #b280 ^property[0].valueString = "Empfinden eines unangenehmen Gefühls, das mögliche oder tatsächliche Schäden einer Körperstruktur anzeigt" 
* #b280 ^property[1].code = #None 
* #b280 ^property[1].valueString = "Allgemeiner oder umschriebener Schmerz in einem oder mehreren Körperteilen, Schmerz in einem Dermatom, stechender, brennender, dumpfer, quälender Schmerz; Muskelschmerz (Myalgie), aufgehobene Schmerzempfindung (Analgesie), gesteigerte Schmerzempfindung (Hyperalgesie)" 
* #b280 ^property[2].code = #parent 
* #b280 ^property[2].valueCode = #b280-b289 
* #b280 ^property[3].code = #child 
* #b280 ^property[3].valueCode = #b2800 
* #b280 ^property[4].code = #child 
* #b280 ^property[4].valueCode = #b2801 
* #b280 ^property[5].code = #child 
* #b280 ^property[5].valueCode = #b2802 
* #b280 ^property[6].code = #child 
* #b280 ^property[6].valueCode = #b2803 
* #b280 ^property[7].code = #child 
* #b280 ^property[7].valueCode = #b2804 
* #b2800 "Generalisierter Schmerz"
* #b2800 ^property[0].code = #None 
* #b2800 ^property[0].valueString = "Empfinden eines unangenehmen Gefühls, das mögliche oder tatsächliche Schäden einer Körperstruktur anzeigt, am oder im gesamten Körper" 
* #b2800 ^property[1].code = #parent 
* #b2800 ^property[1].valueCode = #b280 
* #b2801 "Schmerz in einem Körperteil"
* #b2801 ^property[0].code = #None 
* #b2801 ^property[0].valueString = "Empfinden eines unangenehmen Gefühls, das mögliche oder tatsächliche Schäden einer Körperstruktur anzeigt, in einem bestimmten Körperteil oder in Körperteilen" 
* #b2801 ^property[1].code = #parent 
* #b2801 ^property[1].valueCode = #b280 
* #b2801 ^property[2].code = #child 
* #b2801 ^property[2].valueCode = #b28010 
* #b2801 ^property[3].code = #child 
* #b2801 ^property[3].valueCode = #b28011 
* #b2801 ^property[4].code = #child 
* #b2801 ^property[4].valueCode = #b28012 
* #b2801 ^property[5].code = #child 
* #b2801 ^property[5].valueCode = #b28013 
* #b2801 ^property[6].code = #child 
* #b2801 ^property[6].valueCode = #b28014 
* #b2801 ^property[7].code = #child 
* #b2801 ^property[7].valueCode = #b28015 
* #b2801 ^property[8].code = #child 
* #b2801 ^property[8].valueCode = #b28016 
* #b2801 ^property[9].code = #child 
* #b2801 ^property[9].valueCode = #b28018 
* #b2801 ^property[10].code = #child 
* #b2801 ^property[10].valueCode = #b28019 
* #b28010 "Kopf- und Nackenschmerz"
* #b28010 ^property[0].code = #None 
* #b28010 ^property[0].valueString = "Empfinden eines unangenehmen Gefühls, das mögliche oder tatsächliche Schäden einer Körperstruktur anzeigt, in Kopf oder Nacken" 
* #b28010 ^property[1].code = #parent 
* #b28010 ^property[1].valueCode = #b2801 
* #b28011 "Brustschmerz"
* #b28011 ^property[0].code = #None 
* #b28011 ^property[0].valueString = "Empfinden eines unangenehmen Gefühls, das mögliche oder tatsächliche Schäden einer Körperstruktur anzeigt, in der Brust" 
* #b28011 ^property[1].code = #parent 
* #b28011 ^property[1].valueCode = #b2801 
* #b28012 "Magen- oder Bauchschmerz"
* #b28012 ^property[0].code = #None 
* #b28012 ^property[0].valueString = "Empfinden eines unangenehmen Gefühls, das mögliche oder tatsächliche Schäden einer Körperstruktur anzeigt, in Magen oder Bauch" 
* #b28012 ^property[1].code = #parent 
* #b28012 ^property[1].valueCode = #b2801 
* #b28013 "Rückenschmerz"
* #b28013 ^property[0].code = #None 
* #b28013 ^property[0].valueString = "Empfinden eines unangenehmen Gefühls, das mögliche oder tatsächliche Schäden einer Körperstruktur anzeigt, im Rücken" 
* #b28013 ^property[1].code = #parent 
* #b28013 ^property[1].valueCode = #b2801 
* #b28014 "Schmerz in den oberen Gliedmaßen"
* #b28014 ^property[0].code = #None 
* #b28014 ^property[0].valueString = "Empfinden eines unangenehmen Gefühls, das mögliche oder tatsächliche Schäden einer Körperstruktur anzeigt, in einem oder beiden oberen Gliedmaßen, einschließlich der Hände" 
* #b28014 ^property[1].code = #parent 
* #b28014 ^property[1].valueCode = #b2801 
* #b28015 "Schmerz in den unteren Gliedmaßen"
* #b28015 ^property[0].code = #None 
* #b28015 ^property[0].valueString = "Empfinden eines unangenehmen Gefühls, das mögliche oder tatsächliche Schäden einer Körperstruktur anzeigt, in einem oder beiden unteren Gliedmaßen" 
* #b28015 ^property[1].code = #parent 
* #b28015 ^property[1].valueCode = #b2801 
* #b28016 "Gelenkschmerz"
* #b28016 ^property[0].code = #None 
* #b28016 ^property[0].valueString = "Empfinden eines unangenehmen Gefühls, das mögliche oder tatsächliche Schäden einer Körperstruktur anzeigt, in einem oder mehreren Gelenken einschließlich kleiner und großer" 
* #b28016 ^property[1].code = #None 
* #b28016 ^property[1].valueString = "Hüftschmerz; Schulterschmerz" 
* #b28016 ^property[2].code = #parent 
* #b28016 ^property[2].valueCode = #b2801 
* #b28018 "Schmerz in einem Körperteil, anders bezeichnet"
* #b28018 ^property[0].code = #parent 
* #b28018 ^property[0].valueCode = #b2801 
* #b28019 "Schmerz in einem Körperteil, nicht näher bezeichnet"
* #b28019 ^property[0].code = #parent 
* #b28019 ^property[0].valueCode = #b2801 
* #b2802 "Schmerz in mehreren Körperteilen"
* #b2802 ^property[0].code = #None 
* #b2802 ^property[0].valueString = "Empfinden eines unangenehmen Gefühls, das mögliche oder tatsächliche Schäden einer Körperstruktur anzeigt, in mehreren Körperteilen" 
* #b2802 ^property[1].code = #parent 
* #b2802 ^property[1].valueCode = #b280 
* #b2803 "In ein Dermatom ausstrahlender Schmerz"
* #b2803 ^property[0].code = #None 
* #b2803 ^property[0].valueString = "Empfinden eines unangenehmen Gefühls, das mögliche oder tatsächliche Schäden einer Körperstruktur anzeigt, in einem von derselben Nervenwurzel versorgten Hautareal" 
* #b2803 ^property[1].code = #parent 
* #b2803 ^property[1].valueCode = #b280 
* #b2804 "In ein Hautsegment oder ein Hautareal ausstrahlender Schmerz"
* #b2804 ^property[0].code = #None 
* #b2804 ^property[0].valueString = "Empfinden eines unangenehmen Gefühls, das mögliche oder tatsächliche Schäden einer Körperstruktur anzeigt, in nicht von derselben Nervenwurzel versorgten Hautgebieten verschiedener Körperteile" 
* #b2804 ^property[1].code = #parent 
* #b2804 ^property[1].valueCode = #b280 
* #b289 "Schmerz, anders oder nicht näher bezeichnet"
* #b289 ^property[0].code = #parent 
* #b289 ^property[0].valueCode = #b280-b289 
* #b298 "Sinnesfunktionen und Schmerz, anders bezeichnet"
* #b298 ^property[0].code = #parent 
* #b298 ^property[0].valueCode = #b2 
* #b299 "Sinnesfunktionen und Schmerz, nicht näher bezeichnet"
* #b299 ^property[0].code = #parent 
* #b299 ^property[0].valueCode = #b2 
* #b3 "Stimm- und Sprechfunktionen"
* #b3 ^property[0].code = #None 
* #b3 ^property[0].valueString = "Dieses Kapitel befasst sich mit Funktionen, die die Lauterzeugung und das Sprechen betreffen" 
* #b3 ^property[1].code = #parent 
* #b3 ^property[1].valueCode = #b 
* #b3 ^property[2].code = #child 
* #b3 ^property[2].valueCode = #b310 
* #b3 ^property[3].code = #child 
* #b3 ^property[3].valueCode = #b320 
* #b3 ^property[4].code = #child 
* #b3 ^property[4].valueCode = #b330 
* #b3 ^property[5].code = #child 
* #b3 ^property[5].valueCode = #b340 
* #b3 ^property[6].code = #child 
* #b3 ^property[6].valueCode = #b398 
* #b3 ^property[7].code = #child 
* #b3 ^property[7].valueCode = #b399 
* #b310 "Funktionen der Stimme"
* #b310 ^property[0].code = #None 
* #b310 ^property[0].valueString = "Funktionen, die die Bildung verschiedener Laute während der Luftpassage durch den Kehlkopf betreffen" 
* #b310 ^property[1].code = #None 
* #b310 ^property[1].valueString = "Funktionen der Stimmerzeugung und -qualität; Funktionen der Phonation, der Tonhöhe, der Lautstärke und anderer Stimmqualitäten; Funktionsstörungen wie bei Aphonie, Dysphonie, Heiserkeit, Hypernasalität, Hyponasalität" 
* #b310 ^property[2].code = #None 
* #b310 ^property[2].valueString = "Kognitiv-sprachliche Funktionen" 
* #b310 ^property[3].code = #parent 
* #b310 ^property[3].valueCode = #b3 
* #b310 ^property[4].code = #child 
* #b310 ^property[4].valueCode = #b3100 
* #b310 ^property[5].code = #child 
* #b310 ^property[5].valueCode = #b3101 
* #b310 ^property[6].code = #child 
* #b310 ^property[6].valueCode = #b3108 
* #b310 ^property[7].code = #child 
* #b310 ^property[7].valueCode = #b3109 
* #b3100 "Stimmbildung"
* #b3100 ^property[0].code = #None 
* #b3100 ^property[0].valueString = "Funktionen, die die Lautbildung durch die Koordination von Kehlkopf und umgebenden Muskeln mit dem Atmungssystem betreffen" 
* #b3100 ^property[1].code = #None 
* #b3100 ^property[1].valueString = "Funktionen der Phonation, Lautstärke; Funktionsstörungen bei Aphonie" 
* #b3100 ^property[2].code = #parent 
* #b3100 ^property[2].valueCode = #b310 
* #b3101 "Stimmqualität"
* #b3101 ^property[0].code = #None 
* #b3101 ^property[0].valueString = "Funktionen, die die Prägung der Stimmcharakteristika betreffen, einschließlich Tonhöhe, Resonanz und andere Merkmale" 
* #b3101 ^property[1].code = #None 
* #b3101 ^property[1].valueString = "Funktionen der Tonhöhe (hoch oder tief); Funktionsstörungen wie Hypernasalität, Hyponasalität, Dysphonie, Heiserkeit, Rauigkeit" 
* #b3101 ^property[2].code = #parent 
* #b3101 ^property[2].valueCode = #b310 
* #b3108 "Stimmfunktionen, anders bezeichnet"
* #b3108 ^property[0].code = #parent 
* #b3108 ^property[0].valueCode = #b310 
* #b3109 "Stimmfunktionen, nicht näher bezeichnet"
* #b3109 ^property[0].code = #parent 
* #b3109 ^property[0].valueCode = #b310 
* #b320 "Artikulationsfunktionen"
* #b320 ^property[0].code = #None 
* #b320 ^property[0].valueString = "Funktionen, die die Bildung der Sprechlaute betreffen" 
* #b320 ^property[1].code = #None 
* #b320 ^property[1].valueString = "Funktionen, die Aussprache und Lautartikulation betreffen; Funktionsstörungen wie spastische, ataktische, schlaffe Dysarthrie; Anarthrie" 
* #b320 ^property[2].code = #None 
* #b320 ^property[2].valueString = "Kognitiv-sprachliche Funktionen" 
* #b320 ^property[3].code = #parent 
* #b320 ^property[3].valueCode = #b3 
* #b330 "Funktionen des Redeflusses und Sprechrhythmus"
* #b330 ^property[0].code = #None 
* #b330 ^property[0].valueString = "Funktionen, die die Ausprägung des Sprechflusses und -tempos betreffen" 
* #b330 ^property[1].code = #None 
* #b330 ^property[1].valueString = "Funktionen des Flusses, des Rhythmus, der Geschwindigkeit und Melodie des Sprechens; Prosodie und Intonation; Funktionsstörungen wie Stottern, Stammeln, Poltern, Bradylalie und Tachylalie" 
* #b330 ^property[2].code = #None 
* #b330 ^property[2].valueString = "Kognitiv-sprachliche Funktionen" 
* #b330 ^property[3].code = #parent 
* #b330 ^property[3].valueCode = #b3 
* #b330 ^property[4].code = #child 
* #b330 ^property[4].valueCode = #b3300 
* #b330 ^property[5].code = #child 
* #b330 ^property[5].valueCode = #b3301 
* #b330 ^property[6].code = #child 
* #b330 ^property[6].valueCode = #b3302 
* #b330 ^property[7].code = #child 
* #b330 ^property[7].valueCode = #b3303 
* #b330 ^property[8].code = #child 
* #b330 ^property[8].valueCode = #b3308 
* #b330 ^property[9].code = #child 
* #b330 ^property[9].valueCode = #b3309 
* #b3300 "Sprechflüssigkeit"
* #b3300 ^property[0].code = #None 
* #b3300 ^property[0].valueString = "Funktionen, die die Erzeugung eines gleichmäßigen, kontinuierlichen Sprechflusses betreffen" 
* #b3300 ^property[1].code = #None 
* #b3300 ^property[1].valueString = "Funktionen der gleichmäßigen Sprachverbindung; Funktionsstörungen wie Stottern, Stammeln, Poltern, Redeflussstörung, Wiederholung von Lauten, Wörtern oder Wortteilen, unregelmäßige Sprechunterbrechungen" 
* #b3300 ^property[2].code = #parent 
* #b3300 ^property[2].valueCode = #b330 
* #b3301 "Sprechrhythmus"
* #b3301 ^property[0].code = #None 
* #b3301 ^property[0].valueString = "Funktionen, die die Modulation sowie das Geschwindigkeits- und Betonungsmuster beim Sprechen betreffen" 
* #b3301 ^property[1].code = #None 
* #b3301 ^property[1].valueString = "Funktionsstörungen wie stereotyper oder repetitiver Sprechrhythmus" 
* #b3301 ^property[2].code = #parent 
* #b3301 ^property[2].valueCode = #b330 
* #b3302 "Sprechtempo"
* #b3302 ^property[0].code = #None 
* #b3302 ^property[0].valueString = "Funktionen, die die Sprechgeschwindigkeit betreffen" 
* #b3302 ^property[1].code = #None 
* #b3302 ^property[1].valueString = "Funktionsstörungen wie Bradylalie, Tachylalie" 
* #b3302 ^property[2].code = #parent 
* #b3302 ^property[2].valueCode = #b330 
* #b3303 "Melodik des Sprechens"
* #b3303 ^property[0].code = #None 
* #b3303 ^property[0].valueString = "Funktionen, die die Modulation der Tonhöhe beim Sprechen betreffen" 
* #b3303 ^property[1].code = #None 
* #b3303 ^property[1].valueString = "Prosodie, Intonation; Melodie des Sprechens; Funktionsstörungen wie monotones Sprechen" 
* #b3303 ^property[2].code = #parent 
* #b3303 ^property[2].valueCode = #b330 
* #b3308 "Funktionen des Redeflusses und des Sprechrhythmus, anders bezeichnet"
* #b3308 ^property[0].code = #parent 
* #b3308 ^property[0].valueCode = #b330 
* #b3309 "Funktionen des Redeflusses und des Sprechrhythmus, nicht näher bezeichnet"
* #b3309 ^property[0].code = #parent 
* #b3309 ^property[0].valueCode = #b330 
* #b340 "Alternative stimmliche Äußerungen"
* #b340 ^property[0].code = #None 
* #b340 ^property[0].valueString = "Funktionen, die die Erzeugung anderer Arten stimmlicher Äußerungen betreffen" 
* #b340 ^property[1].code = #None 
* #b340 ^property[1].valueString = "Funktionen, die die Erzeugung von Tönen und die Variation lautlicher Äußerungen betreffen, wie beim Singen, Sprechgesang, Plappern, Summen; lautes Weinen und Schreien" 
* #b340 ^property[2].code = #None 
* #b340 ^property[2].valueString = "Kognitiv-sprachliche Funktionen" 
* #b340 ^property[3].code = #parent 
* #b340 ^property[3].valueCode = #b3 
* #b340 ^property[4].code = #child 
* #b340 ^property[4].valueCode = #b3400 
* #b340 ^property[5].code = #child 
* #b340 ^property[5].valueCode = #b3401 
* #b340 ^property[6].code = #child 
* #b340 ^property[6].valueCode = #b3408 
* #b340 ^property[7].code = #child 
* #b340 ^property[7].valueCode = #b3409 
* #b3400 "Erzeugung von Tönen"
* #b3400 ^property[0].code = #None 
* #b3400 ^property[0].valueString = "Funktionen, die die Bildung von musikbezogenen stimmlichen Äußerungen betreffen" 
* #b3400 ^property[1].code = #None 
* #b3400 ^property[1].valueString = "Halten, modulieren und beenden einzelner oder gebundener stimmlicher Äußerungen mit Variation der Tonhöhe wie beim Singen, Summen und Sprechgesang" 
* #b3400 ^property[2].code = #parent 
* #b3400 ^property[2].valueCode = #b340 
* #b3401 "Erzeugung einer Variation von stimmlichen Äußerungen"
* #b3401 ^property[0].code = #None 
* #b3401 ^property[0].valueString = "Funktionen, die die Erzeugung einer vielfältigen Stimmgebung betreffen" 
* #b3401 ^property[1].code = #None 
* #b3401 ^property[1].valueString = "Funktionen des Lallens bei Kindern" 
* #b3401 ^property[2].code = #parent 
* #b3401 ^property[2].valueCode = #b340 
* #b3408 "Alternative stimmliche Äußerungen, anders bezeichnet"
* #b3408 ^property[0].code = #parent 
* #b3408 ^property[0].valueCode = #b340 
* #b3409 "Alternative stimmliche Äußerungen, nicht näher bezeichnet"
* #b3409 ^property[0].code = #parent 
* #b3409 ^property[0].valueCode = #b340 
* #b398 "Stimm- und Sprechfunktionen, anders bezeichnet"
* #b398 ^property[0].code = #parent 
* #b398 ^property[0].valueCode = #b3 
* #b399 "Stimm- und Sprechfunktionen, nicht näher bezeichnet"
* #b399 ^property[0].code = #parent 
* #b399 ^property[0].valueCode = #b3 
* #b4 "Funktionen des kardiovaskulären, hämatologischen, Immun- und Atmungssystems"
* #b4 ^property[0].code = #None 
* #b4 ^property[0].valueString = "Dieses Kapitel befasst sich mit Funktionen, die am kardiovaskulären System (Funktionen des Herzens und der Blutgefäße), am hämatologischen und Immunsystem (Funktionen der Blutbildung und der Immunität) und am Atmungssystem (Funktionen des Atmens und Funktionen der kardiorespiratorischen Belastbarkeit) beteiligt sind." 
* #b4 ^property[1].code = #parent 
* #b4 ^property[1].valueCode = #b 
* #b4 ^property[2].code = #child 
* #b4 ^property[2].valueCode = #b410-b429 
* #b4 ^property[3].code = #child 
* #b4 ^property[3].valueCode = #b430-b439 
* #b4 ^property[4].code = #child 
* #b4 ^property[4].valueCode = #b440-b449 
* #b4 ^property[5].code = #child 
* #b4 ^property[5].valueCode = #b450-b469 
* #b4 ^property[6].code = #child 
* #b4 ^property[6].valueCode = #b498 
* #b4 ^property[7].code = #child 
* #b4 ^property[7].valueCode = #b499 
* #b410-b429 "Funktionen des kardiovaskulären Systems"
* #b410-b429 ^property[0].code = #parent 
* #b410-b429 ^property[0].valueCode = #b4 
* #b410-b429 ^property[1].code = #child 
* #b410-b429 ^property[1].valueCode = #b410 
* #b410-b429 ^property[2].code = #child 
* #b410-b429 ^property[2].valueCode = #b415 
* #b410-b429 ^property[3].code = #child 
* #b410-b429 ^property[3].valueCode = #b420 
* #b410-b429 ^property[4].code = #child 
* #b410-b429 ^property[4].valueCode = #b429 
* #b410 "Herzfunktionen"
* #b410 ^property[0].code = #None 
* #b410 ^property[0].valueString = "Pumpfunktionen des Herzens zur Sicherstellung der Blutzufuhr zum Körper mit adäquatem oder erforderlichem Volumen und Druck" 
* #b410 ^property[1].code = #None 
* #b410 ^property[1].valueString = "Funktionen von Herzfrequenz, Herzrhythmus und Herzminutenvolumen, Kontraktionskraft der Ventrikel, Herzklappenfunktion, Lungenkreislauf, Füllungsdynamik; Funktionsstörungen wie bei Herzinsuffizienz, Kardiomyopathie, Myokarditis, Koronarinsuffizienz, Tachykardie, Bradykardie, Herzrhythmusstörungen" 
* #b410 ^property[2].code = #None 
* #b410 ^property[2].valueString = "Blutgefäßfunktionen" 
* #b410 ^property[3].code = #parent 
* #b410 ^property[3].valueCode = #b410-b429 
* #b410 ^property[4].code = #child 
* #b410 ^property[4].valueCode = #b4100 
* #b410 ^property[5].code = #child 
* #b410 ^property[5].valueCode = #b4101 
* #b410 ^property[6].code = #child 
* #b410 ^property[6].valueCode = #b4102 
* #b410 ^property[7].code = #child 
* #b410 ^property[7].valueCode = #b4103 
* #b410 ^property[8].code = #child 
* #b410 ^property[8].valueCode = #b4108 
* #b410 ^property[9].code = #child 
* #b410 ^property[9].valueCode = #b4109 
* #b4100 "Herzfrequenz"
* #b4100 ^property[0].code = #None 
* #b4100 ^property[0].valueString = "Funktionen bezüglich der Herzschläge pro Minute" 
* #b4100 ^property[1].code = #None 
* #b4100 ^property[1].valueString = "Funktionsstörungen wie bei zu hoher (Tachykardie) oder zu niedriger (Bradykardie) Herzfrequenz" 
* #b4100 ^property[2].code = #parent 
* #b4100 ^property[2].valueCode = #b410 
* #b4101 "Herzrhythmus"
* #b4101 ^property[0].code = #None 
* #b4101 ^property[0].valueString = "Funktionen, bezüglich der Regelmäßigkeit des Herzschlags" 
* #b4101 ^property[1].code = #None 
* #b4101 ^property[1].valueString = "Funktionsstörungen wie Arrhythmien" 
* #b4101 ^property[2].code = #parent 
* #b4101 ^property[2].valueCode = #b410 
* #b4102 "Kontraktionskraft der Ventrikel"
* #b4102 ^property[0].code = #None 
* #b4102 ^property[0].valueString = "Funktionen, die das Blutvolumen, das von den Ventrikeln bei jeder Kontraktion gepumpt wird (Schlagvolumen), betreffen" 
* #b4102 ^property[1].code = #None 
* #b4102 ^property[1].valueString = "Funktionsstörungen wie vermindertes Herzminutenvolumen" 
* #b4102 ^property[2].code = #parent 
* #b4102 ^property[2].valueCode = #b410 
* #b4103 "Blutzufuhr zum Herzen"
* #b4103 ^property[0].code = #None 
* #b4103 ^property[0].valueString = "Funktionen, die das Blutvolumen, das dem Herzmuskel zu Verfügung steht, betreffen" 
* #b4103 ^property[1].code = #None 
* #b4103 ^property[1].valueString = "Funktionsstörungen wie koronare Ischämie" 
* #b4103 ^property[2].code = #parent 
* #b4103 ^property[2].valueCode = #b410 
* #b4108 "Herzfunktionen, anders bezeichnet"
* #b4108 ^property[0].code = #parent 
* #b4108 ^property[0].valueCode = #b410 
* #b4109 "Herzfunktionen, nicht näher bezeichnet"
* #b4109 ^property[0].code = #parent 
* #b4109 ^property[0].valueCode = #b410 
* #b415 "Blutgefäßfunktionen"
* #b415 ^property[0].code = #None 
* #b415 ^property[0].valueString = "Funktionen, die den Bluttransport durch den Körper betreffen" 
* #b415 ^property[1].code = #None 
* #b415 ^property[1].valueString = "Funktionen der Arterien, Kapillaren und Venen; Vasomotorik; Funktionen der pulmonalen Arterien, Kapillaren und Venen; Funktionen der Venenklappen; Funktionsstörungen wie Verschluss oder Stenose von Arterien; Atherosklerose; Arteriosklerose; Thromboembolie; Varizen" 
* #b415 ^property[2].code = #None 
* #b415 ^property[2].valueString = "Herzfunktionen" 
* #b415 ^property[3].code = #parent 
* #b415 ^property[3].valueCode = #b410-b429 
* #b415 ^property[4].code = #child 
* #b415 ^property[4].valueCode = #b4150 
* #b415 ^property[5].code = #child 
* #b415 ^property[5].valueCode = #b4151 
* #b415 ^property[6].code = #child 
* #b415 ^property[6].valueCode = #b4152 
* #b415 ^property[7].code = #child 
* #b415 ^property[7].valueCode = #b4158 
* #b415 ^property[8].code = #child 
* #b415 ^property[8].valueCode = #b4159 
* #b4150 "Funktionen der Arterien"
* #b4150 ^property[0].code = #None 
* #b4150 ^property[0].valueString = "Funktionen bezüglich des Blutflusses in den Arterien" 
* #b4150 ^property[1].code = #None 
* #b4150 ^property[1].valueString = "Funktionsstörungen wie Arteriendilatation; Arterienstenose wie bei Claudicatio intermittens" 
* #b4150 ^property[2].code = #parent 
* #b4150 ^property[2].valueCode = #b415 
* #b4151 "Funktionen der Kapillaren"
* #b4151 ^property[0].code = #None 
* #b4151 ^property[0].valueString = "Funktionen bezüglich des Blutflusses in den Kapillaren" 
* #b4151 ^property[1].code = #parent 
* #b4151 ^property[1].valueCode = #b415 
* #b4152 "Funktionen der Venen"
* #b4152 ^property[0].code = #None 
* #b4152 ^property[0].valueString = "Funktionen bezüglich des Blutflusses in den Venen und Funktionen der Venenklappen" 
* #b4152 ^property[1].code = #None 
* #b4152 ^property[1].valueString = "Funktionsstörungen wie Venendilatation; Venenstenosen; Venenklappeninsuffizienz wie bei Varikosis" 
* #b4152 ^property[2].code = #parent 
* #b4152 ^property[2].valueCode = #b415 
* #b4158 "Funktionen der Blutgefäße, anders bezeichnet"
* #b4158 ^property[0].code = #parent 
* #b4158 ^property[0].valueCode = #b415 
* #b4159 "Funktionen der Blutgefäße, nicht näher bezeichnet"
* #b4159 ^property[0].code = #parent 
* #b4159 ^property[0].valueCode = #b415 
* #b420 "Blutdruckfunktionen"
* #b420 ^property[0].code = #None 
* #b420 ^property[0].valueString = "Funktionen, die die Aufrechterhaltung des arteriellen Blutdrucks betreffen" 
* #b420 ^property[1].code = #None 
* #b420 ^property[1].valueString = "Blutdruckstabilität; erhöhter und erniedrigter Blutdruck; Funktionsstörungen wie bei Hypotonie, Hypertonie, orthostatischer Blutdruckabfall" 
* #b420 ^property[2].code = #None 
* #b420 ^property[2].valueString = "Herzfunktionen" 
* #b420 ^property[3].code = #parent 
* #b420 ^property[3].valueCode = #b410-b429 
* #b420 ^property[4].code = #child 
* #b420 ^property[4].valueCode = #b4200 
* #b420 ^property[5].code = #child 
* #b420 ^property[5].valueCode = #b4201 
* #b420 ^property[6].code = #child 
* #b420 ^property[6].valueCode = #b4202 
* #b420 ^property[7].code = #child 
* #b420 ^property[7].valueCode = #b4208 
* #b420 ^property[8].code = #child 
* #b420 ^property[8].valueCode = #b4209 
* #b4200 "Erhöhter Blutdruck"
* #b4200 ^property[0].code = #None 
* #b4200 ^property[0].valueString = "Funktionen, die den systolischen oder diastolischen Blutdruckanstieg über die Altersnorm betreffen (Anm. d. Übers.: vgl. WHO-Empfehlungen)" 
* #b4200 ^property[1].code = #parent 
* #b4200 ^property[1].valueCode = #b420 
* #b4201 "Erniedrigter Blutdruck"
* #b4201 ^property[0].code = #None 
* #b4201 ^property[0].valueString = "Funktionen bezüglich eines systolischen oder diastolischen Blutdruckabfalls unter die Altersnorm" 
* #b4201 ^property[1].code = #parent 
* #b4201 ^property[1].valueCode = #b420 
* #b4202 "Aufrechterhaltung des Blutdrucks"
* #b4202 ^property[0].code = #None 
* #b4202 ^property[0].valueString = "Funktionen bezüglich der Aufrechterhaltung eines angemessenen Blutdrucks bei Veränderungen im Körper" 
* #b4202 ^property[1].code = #parent 
* #b4202 ^property[1].valueCode = #b420 
* #b4208 "Funktionen des Blutdrucks, anders bezeichnet"
* #b4208 ^property[0].code = #parent 
* #b4208 ^property[0].valueCode = #b420 
* #b4209 "Funktionen des Blutdrucks, nicht näher bezeichnet"
* #b4209 ^property[0].code = #parent 
* #b4209 ^property[0].valueCode = #b420 
* #b429 "Funktionen des kardiovaskulären Systems, anders oder nicht näher bezeichnet"
* #b429 ^property[0].code = #parent 
* #b429 ^property[0].valueCode = #b410-b429 
* #b430-b439 "Funktionen des hämatologischen und des Immunsystems"
* #b430-b439 ^property[0].code = #parent 
* #b430-b439 ^property[0].valueCode = #b4 
* #b430-b439 ^property[1].code = #child 
* #b430-b439 ^property[1].valueCode = #b430 
* #b430-b439 ^property[2].code = #child 
* #b430-b439 ^property[2].valueCode = #b435 
* #b430-b439 ^property[3].code = #child 
* #b430-b439 ^property[3].valueCode = #b439 
* #b430 "Funktionen des hämatologischen Systems"
* #b430 ^property[0].code = #None 
* #b430 ^property[0].valueString = "Funktionen, die die Blutbildung, den Sauerstoff- und Metaboliten-Transport sowie die Blutgerinnung betreffen" 
* #b430 ^property[1].code = #None 
* #b430 ^property[1].valueString = "Funktionen der Blutbildung und des Knochenmarks; Sauerstofftransportfunktion des Blutes; Blutzellen-bezogene Milzfunktionen; Metaboliten-Transportfunktion des Blutes; Blutgerinnung; Funktionsstörungen wie Anämie, Hämophilie und andere Gerinnungsstörungen" 
* #b430 ^property[2].code = #None 
* #b430 ^property[2].valueString = "Funktionen des kardiovaskulären Systems" 
* #b430 ^property[3].code = #parent 
* #b430 ^property[3].valueCode = #b430-b439 
* #b430 ^property[4].code = #child 
* #b430 ^property[4].valueCode = #b4300 
* #b430 ^property[5].code = #child 
* #b430 ^property[5].valueCode = #b4301 
* #b430 ^property[6].code = #child 
* #b430 ^property[6].valueCode = #b4302 
* #b430 ^property[7].code = #child 
* #b430 ^property[7].valueCode = #b4303 
* #b430 ^property[8].code = #child 
* #b430 ^property[8].valueCode = #b4308 
* #b430 ^property[9].code = #child 
* #b430 ^property[9].valueCode = #b4309 
* #b4300 "Hämatopoese"
* #b4300 ^property[0].code = #None 
* #b4300 ^property[0].valueString = "Funktionen bezüglich der Blutbildung und ihrer gesamten Bestandteile" 
* #b4300 ^property[1].code = #parent 
* #b4300 ^property[1].valueCode = #b430 
* #b4301 "Sauerstofftransportfunktion des Blutes"
* #b4301 ^property[0].code = #None 
* #b4301 ^property[0].valueString = "Funktionen des Blutes bezüglich der Fähigkeit, Sauerstoff zu transportieren" 
* #b4301 ^property[1].code = #parent 
* #b4301 ^property[1].valueCode = #b430 
* #b4302 "Metabolittransport des Blutes"
* #b4302 ^property[0].code = #None 
* #b4302 ^property[0].valueString = "Funktionen, die die Metabolittransport-Kapazität betreffen" 
* #b4302 ^property[1].code = #parent 
* #b4302 ^property[1].valueCode = #b430 
* #b4303 "Gerinnungsfunktionen des Blutes"
* #b4303 ^property[0].code = #None 
* #b4303 ^property[0].valueString = "Funktionen bezüglich der Blutgerinnung wie bei einer Verletzung oder Wunde" 
* #b4303 ^property[1].code = #parent 
* #b4303 ^property[1].valueCode = #b430 
* #b4308 "Funktionen des hämatologischen Systems, anders bezeichnet"
* #b4308 ^property[0].code = #parent 
* #b4308 ^property[0].valueCode = #b430 
* #b4309 "Funktionen des hämatologischen Systems, nicht näher bezeichnet"
* #b4309 ^property[0].code = #parent 
* #b4309 ^property[0].valueCode = #b430 
* #b435 "Funktionen des Immunsystems"
* #b435 ^property[0].code = #None 
* #b435 ^property[0].valueString = "Schutzfunktionen des Körpers mittels spezifischer oder unspezifischer Immunantwort gegen Fremdsubstanzen, einschließlich Infektionen" 
* #b435 ^property[1].code = #None 
* #b435 ^property[1].valueString = "Immunantwort (spezifisch und unspezifisch); Hypersensitivität; Funktionen der Lymphknoten und -gefäße; Funktionen der zellulären und nicht-zellulären Immunität; Reaktion auf Immunisierung; Funktionsstörungen wie Autoimmunität; allergische Reaktionen; Lymphadenitis; Lymphödem" 
* #b435 ^property[2].code = #None 
* #b435 ^property[2].valueString = "Funktionen des hämatologischen Systems" 
* #b435 ^property[3].code = #parent 
* #b435 ^property[3].valueCode = #b430-b439 
* #b435 ^property[4].code = #child 
* #b435 ^property[4].valueCode = #b4350 
* #b435 ^property[5].code = #child 
* #b435 ^property[5].valueCode = #b4351 
* #b435 ^property[6].code = #child 
* #b435 ^property[6].valueCode = #b4352 
* #b435 ^property[7].code = #child 
* #b435 ^property[7].valueCode = #b4353 
* #b435 ^property[8].code = #child 
* #b435 ^property[8].valueCode = #b4358 
* #b435 ^property[9].code = #child 
* #b435 ^property[9].valueCode = #b4359 
* #b4350 "Immunantwort"
* #b4350 ^property[0].code = #None 
* #b4350 ^property[0].valueString = "Funktionen der Körperreaktionen, die die Sensibilisierung gegenüber Fremdsubstanzen einschließlich Infektionen betreffen" 
* #b4350 ^property[1].code = #parent 
* #b4350 ^property[1].valueCode = #b435 
* #b4350 ^property[2].code = #child 
* #b4350 ^property[2].valueCode = #b43500 
* #b4350 ^property[3].code = #child 
* #b4350 ^property[3].valueCode = #b43501 
* #b4350 ^property[4].code = #child 
* #b4350 ^property[4].valueCode = #b43508 
* #b4350 ^property[5].code = #child 
* #b4350 ^property[5].valueCode = #b43509 
* #b43500 "Spezifische Immunantwort"
* #b43500 ^property[0].code = #None 
* #b43500 ^property[0].valueString = "Funktionen der Körperreaktionen, die die Sensibilisierung gegenüber einer spezifischen Fremdsubstanz betreffen" 
* #b43500 ^property[1].code = #parent 
* #b43500 ^property[1].valueCode = #b4350 
* #b43501 "Unspezifische Immunantwort"
* #b43501 ^property[0].code = #None 
* #b43501 ^property[0].valueString = "Funktionen der allgemeinen Körperreaktionen, die die Sensibilisierung gegenüber Fremdsubstanzen einschließlich Infektionen betreffen" 
* #b43501 ^property[1].code = #parent 
* #b43501 ^property[1].valueCode = #b4350 
* #b43508 "Immunantwort, anders bezeichnet"
* #b43508 ^property[0].code = #parent 
* #b43508 ^property[0].valueCode = #b4350 
* #b43509 "Immunantwort, nicht näher bezeichnet"
* #b43509 ^property[0].code = #parent 
* #b43509 ^property[0].valueCode = #b4350 
* #b4351 "Hypersensibilitäts-Reaktionen"
* #b4351 ^property[0].code = #None 
* #b4351 ^property[0].valueString = "Funktionen der Körperreaktionen, die eine erhöhte Sensibilisierung gegen Fremdsubstanzen betreffen, wie bei Sensibilisierung gegenüber verschiedenen Antigenen" 
* #b4351 ^property[1].code = #None 
* #b4351 ^property[1].valueString = "Funktionsstörungen wie Hypersensibilitäten oder allergische Reaktionen" 
* #b4351 ^property[2].code = #None 
* #b4351 ^property[2].valueString = "Nahrungsmittelverträglichkeit" 
* #b4351 ^property[3].code = #parent 
* #b4351 ^property[3].valueCode = #b435 
* #b4352 "Funktionen der Lymphgefäße"
* #b4352 ^property[0].code = #None 
* #b4352 ^property[0].valueString = "Funktionen, die die Gefäßkanäle für den Lymphtransport betreffen" 
* #b4352 ^property[1].code = #parent 
* #b4352 ^property[1].valueCode = #b435 
* #b4353 "Funktionen der Lymphknoten"
* #b4353 ^property[0].code = #None 
* #b4353 ^property[0].valueString = "Funktionen, die die Lymphknoten im Verlauf der Lymphgefäße betreffen" 
* #b4353 ^property[1].code = #parent 
* #b4353 ^property[1].valueCode = #b435 
* #b4358 "Funktionen des Immunsystems, anders bezeichnet"
* #b4358 ^property[0].code = #parent 
* #b4358 ^property[0].valueCode = #b435 
* #b4359 "Funktionen des Immunsystems, nicht näher bezeichnet"
* #b4359 ^property[0].code = #parent 
* #b4359 ^property[0].valueCode = #b435 
* #b439 "Funktionen des hämatologischen und Immunsystems, anders oder nicht näher bezeichnet"
* #b439 ^property[0].code = #parent 
* #b439 ^property[0].valueCode = #b430-b439 
* #b440-b449 "Funktionen des Atmungssystems"
* #b440-b449 ^property[0].code = #parent 
* #b440-b449 ^property[0].valueCode = #b4 
* #b440-b449 ^property[1].code = #child 
* #b440-b449 ^property[1].valueCode = #b440 
* #b440-b449 ^property[2].code = #child 
* #b440-b449 ^property[2].valueCode = #b445 
* #b440-b449 ^property[3].code = #child 
* #b440-b449 ^property[3].valueCode = #b449 
* #b440 "Atmungsfunktionen"
* #b440 ^property[0].code = #None 
* #b440 ^property[0].valueString = "Funktionen, die Inspiration, Gasaustausch zwischen Luft und Blut sowie Exspiration betreffen" 
* #b440 ^property[1].code = #None 
* #b440 ^property[1].valueString = "Funktionen der Atemfrequenz, des Atemrhythmus und der Atemtiefe; Funktionsstörungen wie Apnoe; Hyperventilation; unregelmäßige Atmung; paradoxe Atmung; pulmonales Emphysem; Bronchospasmus" 
* #b440 ^property[2].code = #None 
* #b440 ^property[2].valueString = "Funktionen der Atemmuskulatur" 
* #b440 ^property[3].code = #parent 
* #b440 ^property[3].valueCode = #b440-b449 
* #b440 ^property[4].code = #child 
* #b440 ^property[4].valueCode = #b4400 
* #b440 ^property[5].code = #child 
* #b440 ^property[5].valueCode = #b4401 
* #b440 ^property[6].code = #child 
* #b440 ^property[6].valueCode = #b4402 
* #b440 ^property[7].code = #child 
* #b440 ^property[7].valueCode = #b4408 
* #b440 ^property[8].code = #child 
* #b440 ^property[8].valueCode = #b4409 
* #b4400 "Atemfrequenz"
* #b4400 ^property[0].code = #None 
* #b4400 ^property[0].valueString = "Funktionen, die die Anzahl der Atemzüge pro Minute betreffen" 
* #b4400 ^property[1].code = #None 
* #b4400 ^property[1].valueString = "Funktionsstörungen wie zu hohe (Tachypnoe) oder zu niedrige (Bradypnoe) Atemfrequenz" 
* #b4400 ^property[2].code = #parent 
* #b4400 ^property[2].valueCode = #b440 
* #b4401 "Atemrhythmus"
* #b4401 ^property[0].code = #None 
* #b4401 ^property[0].valueString = "Funktionen, die die Periodizität und Regelmäßigkeit der Atmung betreffen" 
* #b4401 ^property[1].code = #None 
* #b4401 ^property[1].valueString = "Funktionsstörungen wie unregelmäßige Atmung" 
* #b4401 ^property[2].code = #parent 
* #b4401 ^property[2].valueCode = #b440 
* #b4402 "Atemtiefe"
* #b4402 ^property[0].code = #None 
* #b4402 ^property[0].valueString = "Funktionen, die die Lungenkapazität während der Atembewegung betreffen" 
* #b4402 ^property[1].code = #None 
* #b4402 ^property[1].valueString = "Funktionsstörungen wie oberflächliche oder flache Atmung" 
* #b4402 ^property[2].code = #parent 
* #b4402 ^property[2].valueCode = #b440 
* #b4408 "Atmungsfunktionen, anders bezeichnet"
* #b4408 ^property[0].code = #parent 
* #b4408 ^property[0].valueCode = #b440 
* #b4409 "Atmungsfunktionen, nicht näher bezeichnet"
* #b4409 ^property[0].code = #parent 
* #b4409 ^property[0].valueCode = #b440 
* #b445 "Funktionen der Atemmuskulatur"
* #b445 ^property[0].code = #None 
* #b445 ^property[0].valueString = "Funktionen, die die an der Atmung beteiligten Muskeln betreffen" 
* #b445 ^property[1].code = #None 
* #b445 ^property[1].valueString = "Funktionen der thorakalen Atemmuskeln; Funktionen des Zwerchfells und Funktionen der Atemhilfsmuskulatur" 
* #b445 ^property[2].code = #None 
* #b445 ^property[2].valueString = "Atmungsfunktionen" 
* #b445 ^property[3].code = #parent 
* #b445 ^property[3].valueCode = #b440-b449 
* #b445 ^property[4].code = #child 
* #b445 ^property[4].valueCode = #b4450 
* #b445 ^property[5].code = #child 
* #b445 ^property[5].valueCode = #b4451 
* #b445 ^property[6].code = #child 
* #b445 ^property[6].valueCode = #b4452 
* #b445 ^property[7].code = #child 
* #b445 ^property[7].valueCode = #b4458 
* #b445 ^property[8].code = #child 
* #b445 ^property[8].valueCode = #b4459 
* #b4450 "Funktionen der thorakalen Atemmuskeln"
* #b4450 ^property[0].code = #None 
* #b4450 ^property[0].valueString = "Funktionen der thorakalen Muskeln, die an der Atmung beteiligt sind" 
* #b4450 ^property[1].code = #parent 
* #b4450 ^property[1].valueCode = #b445 
* #b4451 "Funktionen des Zwerchfells"
* #b4451 ^property[0].code = #None 
* #b4451 ^property[0].valueString = "Funktionen des Zwerchfells, die an der Atmung beteiligt sind" 
* #b4451 ^property[1].code = #parent 
* #b4451 ^property[1].valueCode = #b445 
* #b4452 "Funktionen der Atemhilfsmuskulatur"
* #b4452 ^property[0].code = #None 
* #b4452 ^property[0].valueString = "Funktionen der Hilfsmuskeln, die beim Atmen beteiligt sind" 
* #b4452 ^property[1].code = #parent 
* #b4452 ^property[1].valueCode = #b445 
* #b4458 "Atemmuskelfunktionen, anders bezeichnet"
* #b4458 ^property[0].code = #parent 
* #b4458 ^property[0].valueCode = #b445 
* #b4459 "Atemmuskelfunktionen, nicht näher bezeichnet"
* #b4459 ^property[0].code = #parent 
* #b4459 ^property[0].valueCode = #b445 
* #b449 "Funktionen des Atmungssystems, anders oder nicht näher bezeichnet"
* #b449 ^property[0].code = #parent 
* #b449 ^property[0].valueCode = #b440-b449 
* #b450-b469 "Weitere Funktionen und Empfindungen, die das kardiovaskuläre und Atmungssystem betreffen"
* #b450-b469 ^property[0].code = #parent 
* #b450-b469 ^property[0].valueCode = #b4 
* #b450-b469 ^property[1].code = #child 
* #b450-b469 ^property[1].valueCode = #b450 
* #b450-b469 ^property[2].code = #child 
* #b450-b469 ^property[2].valueCode = #b455 
* #b450-b469 ^property[3].code = #child 
* #b450-b469 ^property[3].valueCode = #b460 
* #b450-b469 ^property[4].code = #child 
* #b450-b469 ^property[4].valueCode = #b469 
* #b450 "Weitere Atmungsfunktionen"
* #b450 ^property[0].code = #None 
* #b450 ^property[0].valueString = "Weitere Funktionen, die die Atmung betreffen, wie Husten, Niesen und Gähnen" 
* #b450 ^property[1].code = #None 
* #b450 ^property[1].valueString = "Funktionen, die Keuchen, Giemen und Mundatmung betreffen" 
* #b450 ^property[2].code = #parent 
* #b450 ^property[2].valueCode = #b450-b469 
* #b455 "Funktionen der kardiorespiratorischen Belastbarkeit"
* #b455 ^property[0].code = #None 
* #b455 ^property[0].valueString = "Funktionen, die die Kapazität des respiratorischen und kardiovaskulären Systems zur Erbringung von Ausdauerleistungen betreffen" 
* #b455 ^property[1].code = #None 
* #b455 ^property[1].valueString = "Funktionen der Ausdauerleistung, der aeroben Kapazität, Belastbarkeit und Ermüdbarkeit" 
* #b455 ^property[2].code = #None 
* #b455 ^property[2].valueString = "Funktionen des kardiovaskulären Systems" 
* #b455 ^property[3].code = #parent 
* #b455 ^property[3].valueCode = #b450-b469 
* #b455 ^property[4].code = #child 
* #b455 ^property[4].valueCode = #b4550 
* #b455 ^property[5].code = #child 
* #b455 ^property[5].valueCode = #b4551 
* #b455 ^property[6].code = #child 
* #b455 ^property[6].valueCode = #b4552 
* #b455 ^property[7].code = #child 
* #b455 ^property[7].valueCode = #b4558 
* #b455 ^property[8].code = #child 
* #b455 ^property[8].valueCode = #b4559 
* #b4550 "Allgemeine Ausdauerleistung"
* #b4550 ^property[0].code = #None 
* #b4550 ^property[0].valueString = "Funktionen, die die allgemeine Toleranzschwelle für physische Belastungen oder Ausdauer betreffen" 
* #b4550 ^property[1].code = #parent 
* #b4550 ^property[1].valueCode = #b455 
* #b4551 "Aerobe Kapazität"
* #b4551 ^property[0].code = #None 
* #b4551 ^property[0].valueString = "Funktionen, die die Belastungsgrenze des aeroben Stoffwechsels betreffen" 
* #b4551 ^property[1].code = #parent 
* #b4551 ^property[1].valueCode = #b455 
* #b4552 "Ermüdbarkeit"
* #b4552 ^property[0].code = #None 
* #b4552 ^property[0].valueString = "Funktionen, die die Ermüdbarkeit bei jedem Belastungsgrad betreffen" 
* #b4552 ^property[1].code = #parent 
* #b4552 ^property[1].valueCode = #b455 
* #b4558 "Kardiorespiratorische Belastbarkeit, anders bezeichnet"
* #b4558 ^property[0].code = #parent 
* #b4558 ^property[0].valueCode = #b455 
* #b4559 "Kardiorespiratorische Belastbarkeit, nicht näher bezeichnet"
* #b4559 ^property[0].code = #parent 
* #b4559 ^property[0].valueCode = #b455 
* #b460 "Mit dem kardiovaskulären und Atmungssystem verbundene Empfindungen"
* #b460 ^property[0].code = #None 
* #b460 ^property[0].valueString = "Empfindungen wie bei Aussetzen des Herzschlages, Herzklopfen, Kurzatmigkeit" 
* #b460 ^property[1].code = #None 
* #b460 ^property[1].valueString = "Empfindung von Brustenge, Gefühl von unregelmäßigem Herzschlag, Dyspnoe, Luftnot; Erstickungsgefühle, Würgegefühl, Keuchen" 
* #b460 ^property[2].code = #None 
* #b460 ^property[2].valueString = "Schmerz" 
* #b460 ^property[3].code = #parent 
* #b460 ^property[3].valueCode = #b450-b469 
* #b469 "Weitere Funktionen und Empfindungen des kardiovaskulären und Atmungssystems, anders oder nicht näher bezeichnet"
* #b469 ^property[0].code = #parent 
* #b469 ^property[0].valueCode = #b450-b469 
* #b498 "Funktionen des kardiovaskulären, hämatologischen, Immun- und Atmungssystems, anders bezeichnet"
* #b498 ^property[0].code = #parent 
* #b498 ^property[0].valueCode = #b4 
* #b499 "Funktionen des kardiovaskulären, hämatologischen, Immun- und Atmungssystems, nicht näher bezeichnet"
* #b499 ^property[0].code = #parent 
* #b499 ^property[0].valueCode = #b4 
* #b5 "Funktionen des Verdauungs-, des Stoffwechsel- und des endokrinen Systems"
* #b5 ^property[0].code = #None 
* #b5 ^property[0].valueString = "Dieses Kapitel befasst sich mit Funktionen, die Nahrungsaufnahme, Verdauung und Ausscheidung betreffen sowie mit Funktionen, die am Stoffwechsel beteiligt sind, und mit Funktionen der endokrinen Drüsen" 
* #b5 ^property[1].code = #parent 
* #b5 ^property[1].valueCode = #b 
* #b5 ^property[2].code = #child 
* #b5 ^property[2].valueCode = #b510-b539 
* #b5 ^property[3].code = #child 
* #b5 ^property[3].valueCode = #b540-b559 
* #b5 ^property[4].code = #child 
* #b5 ^property[4].valueCode = #b598 
* #b5 ^property[5].code = #child 
* #b5 ^property[5].valueCode = #b599 
* #b510-b539 "Funktionen im Zusammenhang mit dem Verdauungssystem"
* #b510-b539 ^property[0].code = #parent 
* #b510-b539 ^property[0].valueCode = #b5 
* #b510-b539 ^property[1].code = #child 
* #b510-b539 ^property[1].valueCode = #b510 
* #b510-b539 ^property[2].code = #child 
* #b510-b539 ^property[2].valueCode = #b515 
* #b510-b539 ^property[3].code = #child 
* #b510-b539 ^property[3].valueCode = #b520 
* #b510-b539 ^property[4].code = #child 
* #b510-b539 ^property[4].valueCode = #b525 
* #b510-b539 ^property[5].code = #child 
* #b510-b539 ^property[5].valueCode = #b530 
* #b510-b539 ^property[6].code = #child 
* #b510-b539 ^property[6].valueCode = #b535 
* #b510-b539 ^property[7].code = #child 
* #b510-b539 ^property[7].valueCode = #b539 
* #b510 "Funktionen der Nahrungsaufnahme"
* #b510 ^property[0].code = #None 
* #b510 ^property[0].valueString = "Funktionen, die im Zusammenhang mit der Aufnahme und der Bearbeitung fester oder flüssiger Stoffe in den Körper durch den Mund stehen" 
* #b510 ^property[1].code = #None 
* #b510 ^property[1].valueString = "Funktionen des Saugens, Kauens und Beißens, der Handhabung der Speisen im Mund, des Einspeichelns, Schluckens, Aufstoßens, Regurgitierens, Spuckens und Erbrechens; Funktionsstörungen wie Dysphagie, Nahrungsmittelaspiration, Luftschlucken, Speichelüber- oder -unterproduktion, Sabbern und Mundtrockenheit" 
* #b510 ^property[2].code = #None 
* #b510 ^property[2].valueString = "Mit dem Verdauungssystem verbundene Empfindungen" 
* #b510 ^property[3].code = #parent 
* #b510 ^property[3].valueCode = #b510-b539 
* #b510 ^property[4].code = #child 
* #b510 ^property[4].valueCode = #b5100 
* #b510 ^property[5].code = #child 
* #b510 ^property[5].valueCode = #b5101 
* #b510 ^property[6].code = #child 
* #b510 ^property[6].valueCode = #b5102 
* #b510 ^property[7].code = #child 
* #b510 ^property[7].valueCode = #b5103 
* #b510 ^property[8].code = #child 
* #b510 ^property[8].valueCode = #b5104 
* #b510 ^property[9].code = #child 
* #b510 ^property[9].valueCode = #b5105 
* #b510 ^property[10].code = #child 
* #b510 ^property[10].valueCode = #b5106 
* #b510 ^property[11].code = #child 
* #b510 ^property[11].valueCode = #b5108 
* #b510 ^property[12].code = #child 
* #b510 ^property[12].valueCode = #b5109 
* #b5100 "Saugen"
* #b5100 ^property[0].code = #None 
* #b5100 ^property[0].valueString = "Funktionen, eine Flüssigkeit durch Bewegungen der Wangen, Lippen und Zunge in den Mund zu ziehen" 
* #b5100 ^property[1].code = #parent 
* #b5100 ^property[1].valueCode = #b510 
* #b5101 "Beißen"
* #b5101 ^property[0].code = #None 
* #b5101 ^property[0].valueString = "Funktionen, die das Schneiden, Zerteilen oder Abbeißen von Nahrungsmitteln mit den Schneidezähnen betreffen" 
* #b5101 ^property[1].code = #parent 
* #b5101 ^property[1].valueCode = #b510 
* #b5102 "Kauen"
* #b5102 ^property[0].code = #None 
* #b5102 ^property[0].valueString = "Funktionen, die das Zerkleinern und Bearbeiten von Speisen mit den Zähnen (z.B. Backenzähnen)betreffen" 
* #b5102 ^property[1].code = #parent 
* #b5102 ^property[1].valueCode = #b510 
* #b5103 "Handhabung von Speisen im Mund"
* #b5103 ^property[0].code = #None 
* #b5103 ^property[0].valueString = "Funktionen, die die Bewegung von Speisen mit Zähnen und Zunge im Mund betreffen" 
* #b5103 ^property[1].code = #parent 
* #b5103 ^property[1].valueCode = #b510 
* #b5104 "Speichelfluss"
* #b5104 ^property[0].code = #None 
* #b5104 ^property[0].valueString = "Funktionen, die die Mundspeichelproduktion betreffen" 
* #b5104 ^property[1].code = #parent 
* #b5104 ^property[1].valueCode = #b510 
* #b5105 "Schlucken"
* #b5105 ^property[0].code = #None 
* #b5105 ^property[0].valueString = "Funktionen, die die Beförderung von Speisen und Getränken über Mundhöhle, Rachen und Speiseröhre in den Magen in angemessener Menge und Geschwindigkeit betreffen" 
* #b5105 ^property[1].code = #None 
* #b5105 ^property[1].valueString = "Orale, pharyngeale oder oesophageale Schluckstörung; Funktionsstörungen der Speiseröhrenpassage" 
* #b5105 ^property[2].code = #parent 
* #b5105 ^property[2].valueCode = #b510 
* #b5105 ^property[3].code = #child 
* #b5105 ^property[3].valueCode = #b51050 
* #b5105 ^property[4].code = #child 
* #b5105 ^property[4].valueCode = #b51051 
* #b5105 ^property[5].code = #child 
* #b5105 ^property[5].valueCode = #b51052 
* #b5105 ^property[6].code = #child 
* #b5105 ^property[6].valueCode = #b51058 
* #b5105 ^property[7].code = #child 
* #b5105 ^property[7].valueCode = #b51059 
* #b51050 "Orales Schlucken"
* #b51050 ^property[0].code = #None 
* #b51050 ^property[0].valueString = "Funktionen, die die Beförderung von Speisen und Getränken über die Mundhöhle in geeigneter Menge und Geschwindigkeit betreffen" 
* #b51050 ^property[1].code = #parent 
* #b51050 ^property[1].valueCode = #b5105 
* #b51051 "Pharyngeales Schlucken"
* #b51051 ^property[0].code = #None 
* #b51051 ^property[0].valueString = "Funktionen, die die Beförderung von Speisen und Getränken über den Rachen in geeigneter Menge und Geschwindigkeit betreffen" 
* #b51051 ^property[1].code = #parent 
* #b51051 ^property[1].valueCode = #b5105 
* #b51052 "Ösophageales Schlucken"
* #b51052 ^property[0].code = #None 
* #b51052 ^property[0].valueString = "Funktionen, die die Beförderung von Speisen und Getränken über die Speiseröhre in geeigneter Menge und Geschwindigkeit betreffen" 
* #b51052 ^property[1].code = #parent 
* #b51052 ^property[1].valueCode = #b5105 
* #b51058 "Schlucken, anders bezeichnet"
* #b51058 ^property[0].code = #parent 
* #b51058 ^property[0].valueCode = #b5105 
* #b51059 "Schlucken, nicht näher bezeichnet"
* #b51059 ^property[0].code = #parent 
* #b51059 ^property[0].valueCode = #b5105 
* #b5106 "Regurgitation und Erbrechen"
* #b5106 ^property[0].code = #None 
* #b5106 ^property[0].valueString = "Funktionen, die die Rückbeförderung von Speisen oder Flüssigkeit nach Aufnahme aus dem Magen in die Speiseröhre, in den und aus dem Mund betreffen" 
* #b5106 ^property[1].code = #parent 
* #b5106 ^property[1].valueCode = #b510 
* #b5108 "Funktionen der Nahrungsaufnahme, anders bezeichnet"
* #b5108 ^property[0].code = #parent 
* #b5108 ^property[0].valueCode = #b510 
* #b5109 "Funktionen der Nahrungsaufnahme, nicht näher bezeichnet"
* #b5109 ^property[0].code = #parent 
* #b5109 ^property[0].valueCode = #b510 
* #b515 "Verdauungsfunktionen"
* #b515 ^property[0].code = #None 
* #b515 ^property[0].valueString = "Funktionen, die den Transport von Speisen durch den Verdauungskanal, die Aufschlüsselung und Absorption von Nährstoffen betreffen" 
* #b515 ^property[1].code = #None 
* #b515 ^property[1].valueString = "Funktionen, die den Transport von Nahrung durch den Magen betreffen, Peristaltik; Aufschlüsselung von Nahrung, Enzymproduktion und Bewegungen in Magen und Darm; Absorption von Nährstoffen und Nahrungsmittelverträglichkeit; Funktionsstörungen wie Hyperazidität des Magens, Malabsorption, Nahrungsmittelunverträglichkeit, Hypermotilität, Darmlähmung, Darmobstruktion, eingeschränkte Galleproduktion" 
* #b515 ^property[2].code = #None 
* #b515 ^property[2].valueString = "Funktionen der Nahrungsaufnahme" 
* #b515 ^property[3].code = #parent 
* #b515 ^property[3].valueCode = #b510-b539 
* #b515 ^property[4].code = #child 
* #b515 ^property[4].valueCode = #b5150 
* #b515 ^property[5].code = #child 
* #b515 ^property[5].valueCode = #b5151 
* #b515 ^property[6].code = #child 
* #b515 ^property[6].valueCode = #b5152 
* #b515 ^property[7].code = #child 
* #b515 ^property[7].valueCode = #b5153 
* #b515 ^property[8].code = #child 
* #b515 ^property[8].valueCode = #b5158 
* #b515 ^property[9].code = #child 
* #b515 ^property[9].valueCode = #b5159 
* #b5150 "Transport von Nahrung durch Magen und Darm"
* #b5150 ^property[0].code = #None 
* #b5150 ^property[0].valueString = "Peristaltik und entsprechende Funktionen, die Nahrung mechanisch durch Magen und Darm befördern" 
* #b5150 ^property[1].code = #parent 
* #b5150 ^property[1].valueCode = #b515 
* #b5151 "Aufschlüsselung von Nahrung"
* #b5151 ^property[0].code = #None 
* #b5151 ^property[0].valueString = "Funktionen, die die mechanische und chemische Zerkleinerung von Speisen im Verdauungstrakt betreffen" 
* #b5151 ^property[1].code = #parent 
* #b5151 ^property[1].valueCode = #b515 
* #b5152 "Absorption von Nährstoffen"
* #b5152 ^property[0].code = #None 
* #b5152 ^property[0].valueString = "Funktionen, die die Überführung von Nährstoffen aus Speisen und Getränken aus dem Intestinaltrakt ins Blut betreffen" 
* #b5152 ^property[1].code = #parent 
* #b5152 ^property[1].valueCode = #b515 
* #b5153 "Nahrungsmittelverträglichkeit"
* #b5153 ^property[0].code = #None 
* #b5153 ^property[0].valueString = "Funktionen des Körpers, die die Akzeptanz verträglicher Speisen und Getränke zur Verdauung sowie die Verweigerung von Unverträglichem betreffen" 
* #b5153 ^property[1].code = #None 
* #b5153 ^property[1].valueString = "Funktionsstörungen wie Überempfindlichkeiten, Glutenintoleranz" 
* #b5153 ^property[2].code = #parent 
* #b5153 ^property[2].valueCode = #b515 
* #b5158 "Verdauungsfunktionen, anders bezeichnet"
* #b5158 ^property[0].code = #parent 
* #b5158 ^property[0].valueCode = #b515 
* #b5159 "Verdauungsfunktionen, nicht näher bezeichnet"
* #b5159 ^property[0].code = #parent 
* #b5159 ^property[0].valueCode = #b515 
* #b520 "Funktionen der Nahrungsmittelassimilation"
* #b520 ^property[0].code = #None 
* #b520 ^property[0].valueString = "Funktionen, bei denen Nährstoffe in Komponenten des Stoffwechsels umgewandelt werden" 
* #b520 ^property[1].code = #None 
* #b520 ^property[1].valueString = "Funktionen der Nährstoffspeicherung im Körper" 
* #b520 ^property[2].code = #None 
* #b520 ^property[2].valueString = "Verdauungsfunktionen" 
* #b520 ^property[3].code = #parent 
* #b520 ^property[3].valueCode = #b510-b539 
* #b525 "Defäkationsfunktionen"
* #b525 ^property[0].code = #None 
* #b525 ^property[0].valueString = "Funktionen, die die Ausscheidung von Schlacken und unverdauten Speisen als Stuhl betreffen sowie entsprechende Funktionen" 
* #b525 ^property[1].code = #None 
* #b525 ^property[1].valueString = "Funktionen, die Stuhlentleerung, Stuhlkonsistenz, Stuhlfrequenz, Stuhlkontinenz, Flatulenz betreffen; Funktionsstörungen wie Verstopfung, Durchfall, wässriger Stuhl und Analsphinkterinsuffizienz" 
* #b525 ^property[2].code = #None 
* #b525 ^property[2].valueString = "Verdauungsfunktionen" 
* #b525 ^property[3].code = #parent 
* #b525 ^property[3].valueCode = #b510-b539 
* #b525 ^property[4].code = #child 
* #b525 ^property[4].valueCode = #b5250 
* #b525 ^property[5].code = #child 
* #b525 ^property[5].valueCode = #b5251 
* #b525 ^property[6].code = #child 
* #b525 ^property[6].valueCode = #b5252 
* #b525 ^property[7].code = #child 
* #b525 ^property[7].valueCode = #b5253 
* #b525 ^property[8].code = #child 
* #b525 ^property[8].valueCode = #b5254 
* #b525 ^property[9].code = #child 
* #b525 ^property[9].valueCode = #b5258 
* #b525 ^property[10].code = #child 
* #b525 ^property[10].valueCode = #b5259 
* #b5250 "Funktionen der Stuhlentleerung"
* #b5250 ^property[0].code = #None 
* #b5250 ^property[0].valueString = "Funktionen, die die Entleerung von Stuhl aus dem Enddarm betreffen einschließlich der Funktionen der Bauchpresse hierfür" 
* #b5250 ^property[1].code = #parent 
* #b5250 ^property[1].valueCode = #b525 
* #b5251 "Stuhlkonsistenz"
* #b5251 ^property[0].code = #None 
* #b5251 ^property[0].valueString = "Beschaffenheit des Stuhls wie hart, geformt, weich oder wässrig" 
* #b5251 ^property[1].code = #parent 
* #b5251 ^property[1].valueCode = #b525 
* #b5252 "Stuhlhäufigkeit"
* #b5252 ^property[0].code = #None 
* #b5252 ^property[0].valueString = "Funktionen, die an der Stuhlhäufigkeit beteiligt sind" 
* #b5252 ^property[1].code = #parent 
* #b5252 ^property[1].valueCode = #b525 
* #b5253 "Stuhlkontinenz"
* #b5253 ^property[0].code = #None 
* #b5253 ^property[0].valueString = "Funktionen, die an der Kontrolle der Stuhlausscheidung beteiligt sind" 
* #b5253 ^property[1].code = #parent 
* #b5253 ^property[1].valueCode = #b525 
* #b5254 "Flatulenz"
* #b5254 ^property[0].code = #None 
* #b5254 ^property[0].valueString = "Funktionen, die an der Ausscheidung vermehrter Mengen an Luft oder Gas aus dem Darm beteiligt sind" 
* #b5254 ^property[1].code = #parent 
* #b5254 ^property[1].valueCode = #b525 
* #b5258 "Defäkationsfunktionen, anders bezeichnet"
* #b5258 ^property[0].code = #parent 
* #b5258 ^property[0].valueCode = #b525 
* #b5259 "Defäkationsfunktionen, nicht näher bezeichnet"
* #b5259 ^property[0].code = #parent 
* #b5259 ^property[0].valueCode = #b525 
* #b530 "Funktionen der Aufrechterhaltung des Körpergewichts"
* #b530 ^property[0].code = #None 
* #b530 ^property[0].valueString = "Funktionen, die das Aufrechterhalten eines angemessenen Körpergewichts einschließlich Gewichtszunahme während der Körperentwicklung betreffen" 
* #b530 ^property[1].code = #None 
* #b530 ^property[1].valueString = "Funktionen des Aufrechterhaltens eines angemessenen Body Mass Index (BMI); Funktionsstörungen wie Untergewicht, Kachexie, Substanzverlust, Übergewicht, Abzehrung, primäre und sekundäre Adipositas" 
* #b530 ^property[2].code = #None 
* #b530 ^property[2].valueString = "Funktionen der Nahrungsmittelassimilation" 
* #b530 ^property[3].code = #parent 
* #b530 ^property[3].valueCode = #b510-b539 
* #b535 "Mit dem Verdauungssystem verbundene Empfindungen"
* #b535 ^property[0].code = #None 
* #b535 ^property[0].valueString = "Empfindungen, die durch Essen, Trinken und entsprechende Verdauungsfunktionen entstehen" 
* #b535 ^property[1].code = #None 
* #b535 ^property[1].valueString = "Übelkeit und Brechreiz, Blähungsgefühl, Bauchkrämpfe; Völlegefühl, Globusgefühl, Magenkrämpfe, Blähbauch, Sodbrennen" 
* #b535 ^property[2].code = #None 
* #b535 ^property[2].valueString = "Schmerz" 
* #b535 ^property[3].code = #parent 
* #b535 ^property[3].valueCode = #b510-b539 
* #b535 ^property[4].code = #child 
* #b535 ^property[4].valueCode = #b5350 
* #b535 ^property[5].code = #child 
* #b535 ^property[5].valueCode = #b5351 
* #b535 ^property[6].code = #child 
* #b535 ^property[6].valueCode = #b5352 
* #b535 ^property[7].code = #child 
* #b535 ^property[7].valueCode = #b5358 
* #b535 ^property[8].code = #child 
* #b535 ^property[8].valueCode = #b5359 
* #b5350 "Brechreiz und Übelkeit"
* #b5350 ^property[0].code = #None 
* #b5350 ^property[0].valueString = "Gefühl, erbrechen zu müssen" 
* #b5350 ^property[1].code = #parent 
* #b5350 ^property[1].valueCode = #b535 
* #b5351 "Blähungsgefühl"
* #b5351 ^property[0].code = #None 
* #b5351 ^property[0].valueString = "Gefühl eines aufgeblähten Magens oder Bauches" 
* #b5351 ^property[1].code = #parent 
* #b5351 ^property[1].valueCode = #b535 
* #b5352 "Bauchkrämpfe"
* #b5352 ^property[0].code = #None 
* #b5352 ^property[0].valueString = "Gefühl spastischer oder schmerzhafter Kontraktionen der glatten Muskeln des Magen-Darm-Traktes" 
* #b5352 ^property[1].code = #parent 
* #b5352 ^property[1].valueCode = #b535 
* #b5358 "Mit dem Verdauungssystem verbundene Empfindungen, anders bezeichnet"
* #b5358 ^property[0].code = #parent 
* #b5358 ^property[0].valueCode = #b535 
* #b5359 "Mit dem Verdauungssystem verbundene Empfindungen, nicht näher bezeichnet"
* #b5359 ^property[0].code = #parent 
* #b5359 ^property[0].valueCode = #b535 
* #b539 "Funktionen im Zusammenhang mit dem Verdauungssystem, anders oder nicht näher bezeichnet"
* #b539 ^property[0].code = #parent 
* #b539 ^property[0].valueCode = #b510-b539 
* #b540-b559 "Funktionen im Zusammenhang mit dem Stoffwechsel- und dem endokrinen System"
* #b540-b559 ^property[0].code = #parent 
* #b540-b559 ^property[0].valueCode = #b5 
* #b540-b559 ^property[1].code = #child 
* #b540-b559 ^property[1].valueCode = #b540 
* #b540-b559 ^property[2].code = #child 
* #b540-b559 ^property[2].valueCode = #b545 
* #b540-b559 ^property[3].code = #child 
* #b540-b559 ^property[3].valueCode = #b550 
* #b540-b559 ^property[4].code = #child 
* #b540-b559 ^property[4].valueCode = #b555 
* #b540-b559 ^property[5].code = #child 
* #b540-b559 ^property[5].valueCode = #b559 
* #b540 "Allgemeine Stoffwechselfunktionen"
* #b540 ^property[0].code = #None 
* #b540 ^property[0].valueString = "Funktionen, die die Regulierung der notwendigen Nahrungsbausteine wie Kohlenhydrate, Eiweiße und Fette sowie deren Umwandlung in Energie betreffen" 
* #b540 ^property[1].code = #None 
* #b540 ^property[1].valueString = "Funktionen des Stoffwechsels, Grundumsatz, Stoffwechsel von Kohlenhydraten, Eiweiß und Fett, Katabolismus, Anabolismus, Energieproduktion; Steigerung oder Absenkung des Grundumsatzes" 
* #b540 ^property[2].code = #None 
* #b540 ^property[2].valueString = "Funktionen der Nahrungsmittelassimilation" 
* #b540 ^property[3].code = #parent 
* #b540 ^property[3].valueCode = #b540-b559 
* #b540 ^property[4].code = #child 
* #b540 ^property[4].valueCode = #b5400 
* #b540 ^property[5].code = #child 
* #b540 ^property[5].valueCode = #b5401 
* #b540 ^property[6].code = #child 
* #b540 ^property[6].valueCode = #b5402 
* #b540 ^property[7].code = #child 
* #b540 ^property[7].valueCode = #b5403 
* #b540 ^property[8].code = #child 
* #b540 ^property[8].valueCode = #b5408 
* #b540 ^property[9].code = #child 
* #b540 ^property[9].valueCode = #b5409 
* #b5400 "Grundumsatz"
* #b5400 ^property[0].code = #None 
* #b5400 ^property[0].valueString = "Funktionen, die an der Sauerstoffaufnahme unter definierten Bedingungen der Ruhe und Temperatur beteiligt sind" 
* #b5400 ^property[1].code = #None 
* #b5400 ^property[1].valueString = "Steigerung oder Absenkung des Grundumsatzes; Funktionsstörungen wie bei Schilddrüsenüber- oder -unterfunktion" 
* #b5400 ^property[2].code = #parent 
* #b5400 ^property[2].valueCode = #b540 
* #b5401 "Kohlenhydratstoffwechsel"
* #b5401 ^property[0].code = #None 
* #b5401 ^property[0].valueString = "Funktionen, die an dem Prozess, bei dem Nahrungskohlenhydrate gespeichert oder zu Glukose umgewandelt werden und schließlich zu Kohlendioxid und Wasser abgebaut werden, beteiligt sind" 
* #b5401 ^property[1].code = #parent 
* #b5401 ^property[1].valueCode = #b540 
* #b5402 "Eiweißstoffwechsel"
* #b5402 ^property[0].code = #None 
* #b5402 ^property[0].valueString = "Funktionen, die an dem Prozess, bei dem Nahrungseiweiße in Aminosäuren aufgeschlüsselt und weiter metabolisiert werden, beteiligt sind" 
* #b5402 ^property[1].code = #parent 
* #b5402 ^property[1].valueCode = #b540 
* #b5403 "Fettstoffwechsel"
* #b5403 ^property[0].code = #None 
* #b5403 ^property[0].valueString = "Funktionen, die an dem Prozess, bei dem Nahrungsfette gespeichert oder metabolisiert werden, beteiligt sind" 
* #b5403 ^property[1].code = #parent 
* #b5403 ^property[1].valueCode = #b540 
* #b5408 "Allgemeine Stoffwechselfunktionen, anders bezeichnet"
* #b5408 ^property[0].code = #parent 
* #b5408 ^property[0].valueCode = #b540 
* #b5409 "Allgemeine Stoffwechselfunktionen, nicht näher bezeichnet"
* #b5409 ^property[0].code = #parent 
* #b5409 ^property[0].valueCode = #b540 
* #b545 "Funktionen des Wasser-, Mineral- und Elektrolythaushaltes"
* #b545 ^property[0].code = #None 
* #b545 ^property[0].valueString = "Funktionen, die die Regulation von Wasser, Mineralien und Elektrolyten im Körper betreffen" 
* #b545 ^property[1].code = #None 
* #b545 ^property[1].valueString = "Funktionen des Wasserhaushaltes, Haushalt der Mineralien wie Kalzium, Zink, Eisen, und Haushalt der Elektrolyte wie Natrium und Kalium; Funktionsstörungen wie Wasserretention, Dehydratation, Hyperkalzämie, Hypokalzämie, Eisenmangel, Hypernatriämie, Hyponatriämie, Hyperkaliämie und Hypokaliämie" 
* #b545 ^property[2].code = #None 
* #b545 ^property[2].valueString = "Funktionen des hämatologischen Systems" 
* #b545 ^property[3].code = #parent 
* #b545 ^property[3].valueCode = #b540-b559 
* #b545 ^property[4].code = #child 
* #b545 ^property[4].valueCode = #b5450 
* #b545 ^property[5].code = #child 
* #b545 ^property[5].valueCode = #b5451 
* #b545 ^property[6].code = #child 
* #b545 ^property[6].valueCode = #b5452 
* #b545 ^property[7].code = #child 
* #b545 ^property[7].valueCode = #b5458 
* #b545 ^property[8].code = #child 
* #b545 ^property[8].valueCode = #b5459 
* #b5450 "Funktionen des Wasserhaushaltes"
* #b5450 ^property[0].code = #None 
* #b5450 ^property[0].valueString = "Funktionen, die an der Konzentration und Gesamtmenge des Körperwassers beteiligt sind" 
* #b5450 ^property[1].code = #None 
* #b5450 ^property[1].valueString = "Funktionsstörungen wie bei Dehydratation und Rehydration" 
* #b5450 ^property[2].code = #parent 
* #b5450 ^property[2].valueCode = #b545 
* #b5450 ^property[3].code = #child 
* #b5450 ^property[3].valueCode = #b54500 
* #b5450 ^property[4].code = #child 
* #b5450 ^property[4].valueCode = #b54501 
* #b5450 ^property[5].code = #child 
* #b5450 ^property[5].valueCode = #b54508 
* #b5450 ^property[6].code = #child 
* #b5450 ^property[6].valueCode = #b54509 
* #b54500 "Wasserretention"
* #b54500 ^property[0].code = #None 
* #b54500 ^property[0].valueString = "Funktionen, die an der Zurückhaltung vermehrten Wassers im Körper beteiligt sind" 
* #b54500 ^property[1].code = #parent 
* #b54500 ^property[1].valueCode = #b5450 
* #b54501 "Aufrechterhaltung eines ausgeglichenen Wasserhaushaltes"
* #b54501 ^property[0].code = #None 
* #b54501 ^property[0].valueString = "Funktionen, die an der Aufrechterhaltung einer optimalen Menge an Körperwasser beteiligt sind" 
* #b54501 ^property[1].code = #parent 
* #b54501 ^property[1].valueCode = #b5450 
* #b54508 "Funktionen des Wasserhaushaltes, anders bezeichnet"
* #b54508 ^property[0].code = #parent 
* #b54508 ^property[0].valueCode = #b5450 
* #b54509 "Funktionen des Wasserhaushaltes, nicht näher bezeichnet"
* #b54509 ^property[0].code = #parent 
* #b54509 ^property[0].valueCode = #b5450 
* #b5451 "Mineralstoffhaushalt"
* #b5451 ^property[0].code = #None 
* #b5451 ^property[0].valueString = "Funktionen, die an der Aufrechterhaltung eines Gleichgewichts zwischen Aufnahme, Speicherung, Nutzung und Ausscheidung von Mineralien im Körper beteiligt sind" 
* #b5451 ^property[1].code = #parent 
* #b5451 ^property[1].valueCode = #b545 
* #b5452 "Elektrolythaushalt"
* #b5452 ^property[0].code = #None 
* #b5452 ^property[0].valueString = "Funktionen, die an der Aufrechterhaltung eines Gleichgewichts zwischen Aufnahme, Speicherung, Nutzung und Ausscheidung von Elektrolyten im Körper beteiligt sind" 
* #b5452 ^property[1].code = #parent 
* #b5452 ^property[1].valueCode = #b545 
* #b5458 "Funktionen des Wasser-, Mineral- und Elektrolythaushaltes, anders bezeichnet"
* #b5458 ^property[0].code = #parent 
* #b5458 ^property[0].valueCode = #b545 
* #b5459 "Funktionen des Wasser-, Mineral- und Elektrolythaushaltes, nicht näher bezeichnet"
* #b5459 ^property[0].code = #parent 
* #b5459 ^property[0].valueCode = #b545 
* #b550 "Funktionen der Wärmeregulation"
* #b550 ^property[0].code = #None 
* #b550 ^property[0].valueString = "Funktionen, die die Regulation der Körpertemperatur betreffen" 
* #b550 ^property[1].code = #None 
* #b550 ^property[1].valueString = "Funktionen der Aufrechterhaltung der Körpertemperatur; Funktionsstörungen wie bei Hypothermie, Hyperthermie" 
* #b550 ^property[2].code = #None 
* #b550 ^property[2].valueString = "Allgemeine Stoffwechselfunktionen" 
* #b550 ^property[3].code = #parent 
* #b550 ^property[3].valueCode = #b540-b559 
* #b550 ^property[4].code = #child 
* #b550 ^property[4].valueCode = #b5500 
* #b550 ^property[5].code = #child 
* #b550 ^property[5].valueCode = #b5501 
* #b550 ^property[6].code = #child 
* #b550 ^property[6].valueCode = #b5508 
* #b550 ^property[7].code = #child 
* #b550 ^property[7].valueCode = #b5509 
* #b5500 "Körpertemperatur"
* #b5500 ^property[0].code = #None 
* #b5500 ^property[0].valueString = "Funktionen, die an der Regulation der Körperkerntemperatur beteiligt sind" 
* #b5500 ^property[1].code = #None 
* #b5500 ^property[1].valueString = "Funktionsstörungen wie bei Hyperthermie oder Hypothermie" 
* #b5500 ^property[2].code = #parent 
* #b5500 ^property[2].valueCode = #b550 
* #b5501 "Aufrechterhaltung der Körpertemperatur"
* #b5501 ^property[0].code = #None 
* #b5501 ^property[0].valueString = "Funktionen, die an der Aufrechterhaltung einer optimalen Körpertemperatur bei wechselnden Umgebungstemperaturen beteiligt sind" 
* #b5501 ^property[1].code = #None 
* #b5501 ^property[1].valueString = "Hitze- oder Kältetoleranz" 
* #b5501 ^property[2].code = #parent 
* #b5501 ^property[2].valueCode = #b550 
* #b5508 "Wärmeregulationsfunktionen, anders bezeichnet"
* #b5508 ^property[0].code = #parent 
* #b5508 ^property[0].valueCode = #b550 
* #b5509 "Wärmeregulationsfunktionen, nicht näher bezeichnet"
* #b5509 ^property[0].code = #parent 
* #b5509 ^property[0].valueCode = #b550 
* #b555 "Funktionen der endokrinen Drüsen"
* #b555 ^property[0].code = #None 
* #b555 ^property[0].valueString = "Funktionen, die die Produktion und Regulation der Hormonspiegel im Körper einschließlich zyklischer Veränderungen betreffen" 
* #b555 ^property[1].code = #None 
* #b555 ^property[1].valueString = "Funktionen der Hormonbalance; Unter- und Überfunktion der Hypophyse, der Schilddrüse, der Nebenniere, der Nebenschilddrüse und der Gonaden" 
* #b555 ^property[2].code = #None 
* #b555 ^property[2].valueString = "Allgemeine Stoffwechselfunktionen" 
* #b555 ^property[3].code = #parent 
* #b555 ^property[3].valueCode = #b540-b559 
* #b559 "Funktionen im Zusammenhang mit dem Stoffwechsel- und dem endokrinen System, anders oder nicht näher bezeichnet"
* #b559 ^property[0].code = #parent 
* #b559 ^property[0].valueCode = #b540-b559 
* #b598 "Funktionen des Verdauungs-, Stoffwechsel- und des endokrinen Systems, anders bezeichnet"
* #b598 ^property[0].code = #parent 
* #b598 ^property[0].valueCode = #b5 
* #b599 "Funktionen des Verdauungs-, Stoffwechsel- und des endokrinen Systems, nicht näher bezeichnet"
* #b599 ^property[0].code = #parent 
* #b599 ^property[0].valueCode = #b5 
* #b6 "Funktionen des Urogenital- und reproduktiven Systems"
* #b6 ^property[0].code = #None 
* #b6 ^property[0].valueString = "Dieses Kapitel befasst sich mit Funktionen, die die Harnausscheidung und die Reproduktion betreffen, einschließlich der Sexual- und Fortpflanzungsfunktionen" 
* #b6 ^property[1].code = #parent 
* #b6 ^property[1].valueCode = #b 
* #b6 ^property[2].code = #child 
* #b6 ^property[2].valueCode = #b610-b639 
* #b6 ^property[3].code = #child 
* #b6 ^property[3].valueCode = #b640-b679 
* #b6 ^property[4].code = #child 
* #b6 ^property[4].valueCode = #b698 
* #b6 ^property[5].code = #child 
* #b6 ^property[5].valueCode = #b699 
* #b610-b639 "Funktionen der Harnbildung und Harnausscheidung"
* #b610-b639 ^property[0].code = #parent 
* #b610-b639 ^property[0].valueCode = #b6 
* #b610-b639 ^property[1].code = #child 
* #b610-b639 ^property[1].valueCode = #b610 
* #b610-b639 ^property[2].code = #child 
* #b610-b639 ^property[2].valueCode = #b620 
* #b610-b639 ^property[3].code = #child 
* #b610-b639 ^property[3].valueCode = #b630 
* #b610-b639 ^property[4].code = #child 
* #b610-b639 ^property[4].valueCode = #b639 
* #b610 "Harnbildungsfunktionen"
* #b610 ^property[0].code = #None 
* #b610 ^property[0].valueString = "Funktionen, die die Filtration und Sammlung des Harns betreffen" 
* #b610 ^property[1].code = #None 
* #b610 ^property[1].valueString = "Funktionen der Filtration und Sammlung des Harns; Funktionsstörungen wie bei Niereninsuffizienz, Anurie, Oligourie, Hydronephrose, hypotone Harnblase, Verschluss eines Ureters" 
* #b610 ^property[2].code = #None 
* #b610 ^property[2].valueString = "Miktionsfunktionen" 
* #b610 ^property[3].code = #parent 
* #b610 ^property[3].valueCode = #b610-b639 
* #b610 ^property[4].code = #child 
* #b610 ^property[4].valueCode = #b6100 
* #b610 ^property[5].code = #child 
* #b610 ^property[5].valueCode = #b6101 
* #b610 ^property[6].code = #child 
* #b610 ^property[6].valueCode = #b6108 
* #b610 ^property[7].code = #child 
* #b610 ^property[7].valueCode = #b6109 
* #b6100 "Filtration des Harns"
* #b6100 ^property[0].code = #None 
* #b6100 ^property[0].valueString = "Funktionen, die die Filtration des Harns durch die Nieren betreffen" 
* #b6100 ^property[1].code = #parent 
* #b6100 ^property[1].valueCode = #b610 
* #b6101 "Sammlung des Harns"
* #b6101 ^property[0].code = #None 
* #b6101 ^property[0].valueString = "Funktionen, die die Ableitung von Harn durch die Harnleiter und Sammlung des Harns in der Harnblase betreffen" 
* #b6101 ^property[1].code = #parent 
* #b6101 ^property[1].valueCode = #b610 
* #b6108 "Harnbildungsfunktionen, anders bezeichnet"
* #b6108 ^property[0].code = #parent 
* #b6108 ^property[0].valueCode = #b610 
* #b6109 "Harnbildungsfunktionen, nicht näher bezeichnet"
* #b6109 ^property[0].code = #parent 
* #b6109 ^property[0].valueCode = #b610 
* #b620 "Miktionsfunktionen"
* #b620 ^property[0].code = #None 
* #b620 ^property[0].valueString = "Funktionen, die die Beförderung des Urins aus der Harnblase nach außen betreffen" 
* #b620 ^property[1].code = #None 
* #b620 ^property[1].valueString = "Funktionen des Harnlassens, der Häufigkeit der Blasenentleerung, der Harnkontinenz; Funktionsstörungen wie Stressinkontinenz, Dranginkontinenz, Reflexinkontinenz, Überlaufinkontinenz, ständige Inkontinenz, Harntröpfeln, Blasenautonomie (?Rückenmarksblase?) Polyurie, Harnverhalt, Harndrang" 
* #b620 ^property[2].code = #None 
* #b620 ^property[2].valueString = "Harnbildungsfunktionen" 
* #b620 ^property[3].code = #parent 
* #b620 ^property[3].valueCode = #b610-b639 
* #b620 ^property[4].code = #child 
* #b620 ^property[4].valueCode = #b6200 
* #b620 ^property[5].code = #child 
* #b620 ^property[5].valueCode = #b6201 
* #b620 ^property[6].code = #child 
* #b620 ^property[6].valueCode = #b6202 
* #b620 ^property[7].code = #child 
* #b620 ^property[7].valueCode = #b6208 
* #b620 ^property[8].code = #child 
* #b620 ^property[8].valueCode = #b6209 
* #b6200 "Harnlassen"
* #b6200 ^property[0].code = #None 
* #b6200 ^property[0].valueString = "Funktionen, die die Leerung der Harnblase betreffen" 
* #b6200 ^property[1].code = #None 
* #b6200 ^property[1].valueString = "Funktionsstörungen wie Harnretention" 
* #b6200 ^property[2].code = #parent 
* #b6200 ^property[2].valueCode = #b620 
* #b6201 "Häufigkeit der Blasenentleerung"
* #b6201 ^property[0].code = #None 
* #b6201 ^property[0].valueString = "Funktionen, die an der Häufigkeit, mit der die Blasenentleerung erfolgt, beteiligt sind" 
* #b6201 ^property[1].code = #parent 
* #b6201 ^property[1].valueCode = #b620 
* #b6202 "Harnkontinenz"
* #b6202 ^property[0].code = #None 
* #b6202 ^property[0].valueString = "Funktionen, die an der Kontrolle über die Blasenentleerung beteiligt sind" 
* #b6202 ^property[1].code = #None 
* #b6202 ^property[1].valueString = "Funktionsstörungen wie Stress-, Drang-, Reflexinkontinenz, ständige und gemischte Inkontinenz" 
* #b6202 ^property[2].code = #parent 
* #b6202 ^property[2].valueCode = #b620 
* #b6208 "Miktionsfunktionen, anders bezeichnet"
* #b6208 ^property[0].code = #parent 
* #b6208 ^property[0].valueCode = #b620 
* #b6209 "Miktionsfunktionen, nicht näher bezeichnet"
* #b6209 ^property[0].code = #parent 
* #b6209 ^property[0].valueCode = #b620 
* #b630 "Mit der Harnbildung und -ausscheidung verbundene Empfindungen"
* #b630 ^property[0].code = #None 
* #b630 ^property[0].valueString = "Empfindungen, die durch die Entleerung und durch entsprechende Funktionen hervorgerufen werden" 
* #b630 ^property[1].code = #None 
* #b630 ^property[1].valueString = "Gefühl der unvollständigen Blasenentleerung, Gefühl der Blasenfüllung" 
* #b630 ^property[2].code = #None 
* #b630 ^property[2].valueString = "Schmerz" 
* #b630 ^property[3].code = #parent 
* #b630 ^property[3].valueCode = #b610-b639 
* #b639 "Funktionen der Harnbildung und Harnausscheidung, anders oder nicht näher bezeichnet"
* #b639 ^property[0].code = #parent 
* #b639 ^property[0].valueCode = #b610-b639 
* #b640-b679 "Genital- und reproduktive Funktionen"
* #b640-b679 ^property[0].code = #parent 
* #b640-b679 ^property[0].valueCode = #b6 
* #b640-b679 ^property[1].code = #child 
* #b640-b679 ^property[1].valueCode = #b640 
* #b640-b679 ^property[2].code = #child 
* #b640-b679 ^property[2].valueCode = #b650 
* #b640-b679 ^property[3].code = #child 
* #b640-b679 ^property[3].valueCode = #b660 
* #b640-b679 ^property[4].code = #child 
* #b640-b679 ^property[4].valueCode = #b670 
* #b640-b679 ^property[5].code = #child 
* #b640-b679 ^property[5].valueCode = #b679 
* #b640 "Sexuelle Funktionen"
* #b640 ^property[0].code = #None 
* #b640 ^property[0].valueString = "Mentale und physische Funktionen, die mit dem Geschlechtsakt einschließlich der Stadien der Erregung, des Vorspiels, des Orgasmus und der Entspannung im Zusammenhang stehen" 
* #b640 ^property[1].code = #None 
* #b640 ^property[1].valueString = "Funktionen, die die Phasen der sexuellen Erregung, des Vorspiels, des Orgasmus und der Entspannung betreffen; Funktionen im Zusammenhang mit sexuellem Interesse und seiner Umsetzung, mit Erektion von Penis und Klitoris, der Lubrikation, Ejakulation und Orgasmus; Funktionsstörungen wie Impotenz, Frigidität, Vaginismus, Ejaculatio praecox, verzögerte Ejakulation und bleibende Erektion (Priapismus)" 
* #b640 ^property[2].code = #None 
* #b640 ^property[2].valueString = "Fortpflanzungsfunktionen" 
* #b640 ^property[3].code = #parent 
* #b640 ^property[3].valueCode = #b640-b679 
* #b640 ^property[4].code = #child 
* #b640 ^property[4].valueCode = #b6400 
* #b640 ^property[5].code = #child 
* #b640 ^property[5].valueCode = #b6401 
* #b640 ^property[6].code = #child 
* #b640 ^property[6].valueCode = #b6402 
* #b640 ^property[7].code = #child 
* #b640 ^property[7].valueCode = #b6403 
* #b640 ^property[8].code = #child 
* #b640 ^property[8].valueCode = #b6408 
* #b640 ^property[9].code = #child 
* #b640 ^property[9].valueCode = #b6409 
* #b6400 "Funktionen der sexuellen Erregungsphase"
* #b6400 ^property[0].code = #None 
* #b6400 ^property[0].valueString = "Funktionen, die das sexuelle Interesse und die sexuelle Erregung betreffen" 
* #b6400 ^property[1].code = #parent 
* #b6400 ^property[1].valueCode = #b640 
* #b6401 "Funktionen der Vorspielphase"
* #b6401 ^property[0].code = #None 
* #b6401 ^property[0].valueString = "Funktionen, die die Vorbereitung des Geschlechtsverkehrs betreffen" 
* #b6401 ^property[1].code = #parent 
* #b6401 ^property[1].valueCode = #b640 
* #b6402 "Funktionen der Orgasmusphase"
* #b6402 ^property[0].code = #None 
* #b6402 ^property[0].valueString = "Funktionen, die das Erreichen eines Orgasmus betreffen" 
* #b6402 ^property[1].code = #parent 
* #b6402 ^property[1].valueCode = #b640 
* #b6403 "Funktionen der sexuellen Entspannungsphase"
* #b6403 ^property[0].code = #None 
* #b6403 ^property[0].valueString = "Funktionen, die die Befriedigung nach einem Orgasmus und damit einhergehende Entspannung betreffen" 
* #b6403 ^property[1].code = #None 
* #b6403 ^property[1].valueString = "Funktionsstörung wie unbefriedigender Orgasmus" 
* #b6403 ^property[2].code = #parent 
* #b6403 ^property[2].valueCode = #b640 
* #b6408 "Sexuelle Funktionen, anders bezeichnet"
* #b6408 ^property[0].code = #parent 
* #b6408 ^property[0].valueCode = #b640 
* #b6409 "Sexuelle Funktionen, nicht näher bezeichnet"
* #b6409 ^property[0].code = #parent 
* #b6409 ^property[0].valueCode = #b640 
* #b650 "Menstruationsfunktionen"
* #b650 ^property[0].code = #None 
* #b650 ^property[0].valueString = "Funktionen, die mit dem Menstruationszyklus einschließlich der Regulation der Menstruation und der Ausscheidung der Menstruationssekrete verbunden sind" 
* #b650 ^property[1].code = #None 
* #b650 ^property[1].valueString = "Funktionen der Regelmäßigkeit des Zyklus und des Menstruationsintervalls, der Stärke der Menstruationsblutung, Menarche, Menopause; Funktionsstörungen wie prämenstruelles Syndrom, primäre und sekundäre Amenorrhoe, Menorrhagie, Polymenorrhoe, retrograde Menstruation" 
* #b650 ^property[2].code = #None 
* #b650 ^property[2].valueString = "Sexuelle Funktionen" 
* #b650 ^property[3].code = #parent 
* #b650 ^property[3].valueCode = #b640-b679 
* #b650 ^property[4].code = #child 
* #b650 ^property[4].valueCode = #b6500 
* #b650 ^property[5].code = #child 
* #b650 ^property[5].valueCode = #b6501 
* #b650 ^property[6].code = #child 
* #b650 ^property[6].valueCode = #b6502 
* #b650 ^property[7].code = #child 
* #b650 ^property[7].valueCode = #b6508 
* #b650 ^property[8].code = #child 
* #b650 ^property[8].valueCode = #b6509 
* #b6500 "Regelmäßigkeit des Menstruationszyklus"
* #b6500 ^property[0].code = #None 
* #b6500 ^property[0].valueString = "Funktionen, die an der Regelmäßigkeit des Menstruationszyklus beteiligt sind" 
* #b6500 ^property[1].code = #None 
* #b6500 ^property[1].valueString = "Zu häufige oder zu seltene Menstruationen" 
* #b6500 ^property[2].code = #parent 
* #b6500 ^property[2].valueCode = #b650 
* #b6501 "Menstruationsintervall"
* #b6501 ^property[0].code = #None 
* #b6501 ^property[0].valueString = "Periode zwischen zwei Menstruationen" 
* #b6501 ^property[1].code = #parent 
* #b6501 ^property[1].valueCode = #b650 
* #b6502 "Stärke der Menstruationsblutung"
* #b6502 ^property[0].code = #None 
* #b6502 ^property[0].valueString = "Funktionen, die an der Menge des Menstruationssekrets beteiligt sind" 
* #b6502 ^property[1].code = #None 
* #b6502 ^property[1].valueString = "Zu geringe Menstruation (Hypomenorrhoe); zu starke Menstruation (Menorrhagie, Hypermenorrhoe)" 
* #b6502 ^property[2].code = #parent 
* #b6502 ^property[2].valueCode = #b650 
* #b6508 "Menstruationsfunktionen, anders bezeichnet"
* #b6508 ^property[0].code = #parent 
* #b6508 ^property[0].valueCode = #b650 
* #b6509 "Menstruationsfunktionen, nicht näher bezeichnet"
* #b6509 ^property[0].code = #parent 
* #b6509 ^property[0].valueCode = #b650 
* #b660 "Fortpflanzungsfunktionen"
* #b660 ^property[0].code = #None 
* #b660 ^property[0].valueString = "Funktionen, die mit der Fertilität, Schwangerschaft, Geburt und Laktation verbunden sind" 
* #b660 ^property[1].code = #None 
* #b660 ^property[1].valueString = "Funktionen der männlichen und weiblichen Fertilität, Schwangerschaft, Geburt und Laktation; Funktionsstörungen wie Subfertilität, Sterilität, Azoospermie, Oligozoospermie, Spontanabort, ektopische Schwangerschaft, Fehlgeburt, zu kleiner Fetus, Hydramnion und Frühgeburt, verzögerte Geburt, Galaktorrhoe, Agalaktorrhoe, Agalaktie" 
* #b660 ^property[2].code = #None 
* #b660 ^property[2].valueString = "Sexuelle Funktionen" 
* #b660 ^property[3].code = #parent 
* #b660 ^property[3].valueCode = #b640-b679 
* #b660 ^property[4].code = #child 
* #b660 ^property[4].valueCode = #b6600 
* #b660 ^property[5].code = #child 
* #b660 ^property[5].valueCode = #b6601 
* #b660 ^property[6].code = #child 
* #b660 ^property[6].valueCode = #b6602 
* #b660 ^property[7].code = #child 
* #b660 ^property[7].valueCode = #b6603 
* #b660 ^property[8].code = #child 
* #b660 ^property[8].valueCode = #b6608 
* #b660 ^property[9].code = #child 
* #b660 ^property[9].valueCode = #b6609 
* #b6600 "Funktionen im Zusammenhang mit der Fertilität"
* #b6600 ^property[0].code = #None 
* #b6600 ^property[0].valueString = "Funktionen, die im Zusammenhang mit der Fähigkeit zur Bildung von Keimzellen stehen" 
* #b6600 ^property[1].code = #None 
* #b6600 ^property[1].valueString = "Funktionsstörungen wie Subfertilität, Sterilität" 
* #b6600 ^property[2].code = #None 
* #b6600 ^property[2].valueString = "Sexuelle Funktionen" 
* #b6600 ^property[3].code = #parent 
* #b6600 ^property[3].valueCode = #b660 
* #b6601 "Funktionen, die an der Schwangerschaft beteiligt sind"
* #b6601 ^property[0].code = #None 
* #b6601 ^property[0].valueString = "Funktionen, die an der Fähigkeit, schwanger zu werden und zu bleiben, beteiligt sind" 
* #b6601 ^property[1].code = #parent 
* #b6601 ^property[1].valueCode = #b660 
* #b6602 "Funktionen im Zusammenhang mit der Geburt"
* #b6602 ^property[0].code = #None 
* #b6602 ^property[0].valueString = "Funktionen, die am Geburtsvorgang beteiligt sind" 
* #b6602 ^property[1].code = #parent 
* #b6602 ^property[1].valueCode = #b660 
* #b6603 "Laktation"
* #b6603 ^property[0].code = #None 
* #b6603 ^property[0].valueString = "Funktionen, die an der Produktion von Milch und deren Abgabe an das Kind beteiligt sind" 
* #b6603 ^property[1].code = #parent 
* #b6603 ^property[1].valueCode = #b660 
* #b6608 "Fortpflanzungsfunktionen, anders bezeichnet"
* #b6608 ^property[0].code = #parent 
* #b6608 ^property[0].valueCode = #b660 
* #b6609 "Fortpflanzungsfunktionen, nicht näher bezeichnet"
* #b6609 ^property[0].code = #parent 
* #b6609 ^property[0].valueCode = #b660 
* #b670 "Mit den Genital- und reproduktiven Funktionen verbundene Empfindungen"
* #b670 ^property[0].code = #None 
* #b670 ^property[0].valueString = "Empfindungen wie Unbehagen während des Geschlechtsverkehrs oder während des Menstruationszyklus" 
* #b670 ^property[1].code = #None 
* #b670 ^property[1].valueString = "Dyspareunie, Dysmenorrhoe, Hitzewallungen und nächtliche Schweißausbrüche während der Menopause" 
* #b670 ^property[2].code = #None 
* #b670 ^property[2].valueString = "Schmerz" 
* #b670 ^property[3].code = #parent 
* #b670 ^property[3].valueCode = #b640-b679 
* #b670 ^property[4].code = #child 
* #b670 ^property[4].valueCode = #b6700 
* #b670 ^property[5].code = #child 
* #b670 ^property[5].valueCode = #b6701 
* #b670 ^property[6].code = #child 
* #b670 ^property[6].valueCode = #b6702 
* #b670 ^property[7].code = #child 
* #b670 ^property[7].valueCode = #b6708 
* #b670 ^property[8].code = #child 
* #b670 ^property[8].valueCode = #b6709 
* #b6700 "Mit dem Geschlechtsverkehr verbundene Beschwerden"
* #b6700 ^property[0].code = #None 
* #b6700 ^property[0].valueString = "Empfindungen, die mit der sexuellen Erregung, dem Vorspiel, dem Geschlechtsverkehr, dem Orgasmus und der Entspannung verbunden sind" 
* #b6700 ^property[1].code = #parent 
* #b6700 ^property[1].valueCode = #b670 
* #b6701 "Mit dem Menstruationszyklus verbundene Beschwerden"
* #b6701 ^property[0].code = #None 
* #b6701 ^property[0].valueString = "Empfindungen, die an der Menstruation einschließlich der prae- und postmenstruellen Phase beteiligt sind" 
* #b6701 ^property[1].code = #parent 
* #b6701 ^property[1].valueCode = #b670 
* #b6702 "Mit der Menopause verbundene Beschwerden"
* #b6702 ^property[0].code = #None 
* #b6702 ^property[0].valueString = "Empfindungen, die mit dem Sistieren des Menstruationszyklus verbunden sind" 
* #b6702 ^property[1].code = #None 
* #b6702 ^property[1].valueString = "Hitzewallungen und nächtliche Schweißausbrüche während der Menopause" 
* #b6702 ^property[2].code = #parent 
* #b6702 ^property[2].valueCode = #b670 
* #b6708 "Mit den Genital- und reproduktiven Funktionen verbundene Empfindungen, anders bezeichnet"
* #b6708 ^property[0].code = #parent 
* #b6708 ^property[0].valueCode = #b670 
* #b6709 "Mit den Genital- und reproduktiven Funktionen verbundene Empfindungen, nicht näher bezeichnet"
* #b6709 ^property[0].code = #parent 
* #b6709 ^property[0].valueCode = #b670 
* #b679 "Genital- und reproduktive Funktionen, anders oder nicht näher bezeichnet"
* #b679 ^property[0].code = #parent 
* #b679 ^property[0].valueCode = #b640-b679 
* #b698 "Funktionen des Urogenitalsystems und der Reproduktion, anders bezeichnet"
* #b698 ^property[0].code = #parent 
* #b698 ^property[0].valueCode = #b6 
* #b699 "Funktionen des Urogenitalsystems und der Reproduktion, nicht näher bezeichnet"
* #b699 ^property[0].code = #parent 
* #b699 ^property[0].valueCode = #b6 
* #b7 "Neuromuskuloskeletale und bewegungsbezogene Funktionen"
* #b7 ^property[0].code = #None 
* #b7 ^property[0].valueString = "Dieses Kapitel befasst sich mit Funktionen, die Bewegung und Mobilität betreffen, einschließlich der Funktionen der Gelenke, Knochen, Reflexe und Muskeln" 
* #b7 ^property[1].code = #parent 
* #b7 ^property[1].valueCode = #b 
* #b7 ^property[2].code = #child 
* #b7 ^property[2].valueCode = #b710-b729 
* #b7 ^property[3].code = #child 
* #b7 ^property[3].valueCode = #b730-b749 
* #b7 ^property[4].code = #child 
* #b7 ^property[4].valueCode = #b750-b789 
* #b7 ^property[5].code = #child 
* #b7 ^property[5].valueCode = #b798 
* #b7 ^property[6].code = #child 
* #b7 ^property[6].valueCode = #b799 
* #b710-b729 "Funktionen der Gelenke und Knochen"
* #b710-b729 ^property[0].code = #parent 
* #b710-b729 ^property[0].valueCode = #b7 
* #b710-b729 ^property[1].code = #child 
* #b710-b729 ^property[1].valueCode = #b710 
* #b710-b729 ^property[2].code = #child 
* #b710-b729 ^property[2].valueCode = #b715 
* #b710-b729 ^property[3].code = #child 
* #b710-b729 ^property[3].valueCode = #b720 
* #b710-b729 ^property[4].code = #child 
* #b710-b729 ^property[4].valueCode = #b729 
* #b710 "Funktionen der Gelenkbeweglichkeit"
* #b710 ^property[0].code = #None 
* #b710 ^property[0].valueString = "Funktionen, die den Bewegungsumfang und die Leichtigkeit des Bewegungsablaufes betreffen" 
* #b710 ^property[1].code = #None 
* #b710 ^property[1].valueString = "Funktionen der Beweglichkeit eines einzelnen oder mehrerer Gelenke, der Wirbelsäule, Schulter, des Ellenbogens, Handgelenks, der Hüfte, des Knies, Sprunggelenks, der kleinen Gelenke der Hände und Füße; allgemeine Gelenkbeweglichkeit; Funktionsstörungen wie bei Hypermobilität der Gelenke, Gelenksteife, Schultersteife, Gelenkentzündung" 
* #b710 ^property[2].code = #None 
* #b710 ^property[2].valueString = "Funktionen der Gelenkstabilität" 
* #b710 ^property[3].code = #parent 
* #b710 ^property[3].valueCode = #b710-b729 
* #b710 ^property[4].code = #child 
* #b710 ^property[4].valueCode = #b7100 
* #b710 ^property[5].code = #child 
* #b710 ^property[5].valueCode = #b7101 
* #b710 ^property[6].code = #child 
* #b710 ^property[6].valueCode = #b7102 
* #b710 ^property[7].code = #child 
* #b710 ^property[7].valueCode = #b7108 
* #b710 ^property[8].code = #child 
* #b710 ^property[8].valueCode = #b7109 
* #b7100 "Beweglichkeit eines einzelnen Gelenkes"
* #b7100 ^property[0].code = #None 
* #b7100 ^property[0].valueString = "Funktionen, die den Bewegungsumfang und die Leichtigkeit der Bewegung eines einzelnen Gelenkes betreffen" 
* #b7100 ^property[1].code = #parent 
* #b7100 ^property[1].valueCode = #b710 
* #b7101 "Beweglichkeit mehrerer Gelenke"
* #b7101 ^property[0].code = #None 
* #b7101 ^property[0].valueString = "Funktionen, die den Bewegungsumfang und die Leichtigkeit der Bewegung mehrerer Gelenke betreffen" 
* #b7101 ^property[1].code = #parent 
* #b7101 ^property[1].valueCode = #b710 
* #b7102 "Allgemeine Gelenkbeweglichkeit"
* #b7102 ^property[0].code = #None 
* #b7102 ^property[0].valueString = "Funktionen, die den Bewegungsumfang und die Leichtigkeit der Bewegung sämtlicher Gelenke betreffen" 
* #b7102 ^property[1].code = #parent 
* #b7102 ^property[1].valueCode = #b710 
* #b7108 "Funktionen der Gelenkbeweglichkeit, anders bezeichnet"
* #b7108 ^property[0].code = #parent 
* #b7108 ^property[0].valueCode = #b710 
* #b7109 "Funktionen der Gelenkbeweglichkeit, nicht näher bezeichnet"
* #b7109 ^property[0].code = #parent 
* #b7109 ^property[0].valueCode = #b710 
* #b715 "Funktionen der Gelenkstabilität"
* #b715 ^property[0].code = #None 
* #b715 ^property[0].valueString = "Funktionen, die die Aufrechterhaltung der strukturellen Integrität der Gelenke betreffen" 
* #b715 ^property[1].code = #None 
* #b715 ^property[1].valueString = "Funktionen der Stabilität eines einzelnen Gelenks, mehrerer Gelenke und aller Gelenke; Funktionsstörungen wie Schulterinstabilität, Gelenkdislokation, Dislokation der Schulter und Hüfte" 
* #b715 ^property[2].code = #None 
* #b715 ^property[2].valueString = "Funktionen der Gelenkbeweglichkeit" 
* #b715 ^property[3].code = #parent 
* #b715 ^property[3].valueCode = #b710-b729 
* #b715 ^property[4].code = #child 
* #b715 ^property[4].valueCode = #b7150 
* #b715 ^property[5].code = #child 
* #b715 ^property[5].valueCode = #b7151 
* #b715 ^property[6].code = #child 
* #b715 ^property[6].valueCode = #b7152 
* #b715 ^property[7].code = #child 
* #b715 ^property[7].valueCode = #b7158 
* #b715 ^property[8].code = #child 
* #b715 ^property[8].valueCode = #b7159 
* #b7150 "Stabilität eines einzelnen Gelenkes"
* #b7150 ^property[0].code = #None 
* #b7150 ^property[0].valueString = "Funktionen, die die Aufrechterhaltung der strukturellen Integrität eines einzelnen Gelenks betreffen" 
* #b7150 ^property[1].code = #parent 
* #b7150 ^property[1].valueCode = #b715 
* #b7151 "Stabilität mehrerer Gelenke"
* #b7151 ^property[0].code = #None 
* #b7151 ^property[0].valueString = "Funktionen, die die Aufrechterhaltung der strukturellen Integrität von mehr als einem Gelenk betreffen" 
* #b7151 ^property[1].code = #parent 
* #b7151 ^property[1].valueCode = #b715 
* #b7152 "Allgemeine Gelenkstabilität"
* #b7152 ^property[0].code = #None 
* #b7152 ^property[0].valueString = "Funktionen, die die Aufrechterhaltung der strukturellen Integrität aller Gelenke betreffen" 
* #b7152 ^property[1].code = #parent 
* #b7152 ^property[1].valueCode = #b715 
* #b7158 "Funktionen der Gelenkstabilität, anders bezeichnet"
* #b7158 ^property[0].code = #parent 
* #b7158 ^property[0].valueCode = #b715 
* #b7159 "Funktionen der Gelenkstabilität, nicht näher bezeichnet"
* #b7159 ^property[0].code = #parent 
* #b7159 ^property[0].valueCode = #b715 
* #b720 "Funktionen der Beweglichkeit der Knochen"
* #b720 ^property[0].code = #None 
* #b720 ^property[0].valueString = "Funktionen, die den Bewegungsumfang und die Leichtigkeit der Bewegung des Schulterblatts, Beckens sowie der Handwurzel- und Fußwurzelknochen betreffen" 
* #b720 ^property[1].code = #None 
* #b720 ^property[1].valueString = "Funktionsstörungen wie Einschränkung der Beweglichkeit des Schulterblattes, Beckensteife" 
* #b720 ^property[2].code = #None 
* #b720 ^property[2].valueString = "Funktionen der Gelenkbeweglichkeit" 
* #b720 ^property[3].code = #parent 
* #b720 ^property[3].valueCode = #b710-b729 
* #b720 ^property[4].code = #child 
* #b720 ^property[4].valueCode = #b7200 
* #b720 ^property[5].code = #child 
* #b720 ^property[5].valueCode = #b7201 
* #b720 ^property[6].code = #child 
* #b720 ^property[6].valueCode = #b7202 
* #b720 ^property[7].code = #child 
* #b720 ^property[7].valueCode = #b7203 
* #b720 ^property[8].code = #child 
* #b720 ^property[8].valueCode = #b7208 
* #b720 ^property[9].code = #child 
* #b720 ^property[9].valueCode = #b7209 
* #b7200 "Beweglichkeit des Schulterblattes"
* #b7200 ^property[0].code = #None 
* #b7200 ^property[0].valueString = "Funktionen, die den Bewegungsumfang und die Leichtigkeit der Bewegung des Schulterblatts betreffen" 
* #b7200 ^property[1].code = #None 
* #b7200 ^property[1].valueString = "Funktionsstörungen wie Protraktion, Retrotraktion, Außenrotation und Innenrotation des Schulterblattes" 
* #b7200 ^property[2].code = #parent 
* #b7200 ^property[2].valueCode = #b720 
* #b7201 "Beweglichkeit des Beckens"
* #b7201 ^property[0].code = #None 
* #b7201 ^property[0].valueString = "Funktionen, die den Bewegungsumfang und die Leichtigkeit der Bewegung des Beckens betreffen" 
* #b7201 ^property[1].code = #None 
* #b7201 ^property[1].valueString = "Beckenrotation" 
* #b7201 ^property[2].code = #parent 
* #b7201 ^property[2].valueCode = #b720 
* #b7202 "Beweglichkeit der Handwurzel"
* #b7202 ^property[0].code = #None 
* #b7202 ^property[0].valueString = "Funktionen, die den Bewegungsumfang und die Leichtigkeit der Bewegung der Handwurzelknochen betreffen" 
* #b7202 ^property[1].code = #parent 
* #b7202 ^property[1].valueCode = #b720 
* #b7203 "Beweglichkeit der Fußwurzel"
* #b7203 ^property[0].code = #None 
* #b7203 ^property[0].valueString = "Funktionen, die den Bewegungsumfang und die Leichtigkeit der Bewegung der Fußwurzelknochen betreffen" 
* #b7203 ^property[1].code = #parent 
* #b7203 ^property[1].valueCode = #b720 
* #b7208 "Funktionen der Beweglichkeit der Knochen, anders bezeichnet"
* #b7208 ^property[0].code = #parent 
* #b7208 ^property[0].valueCode = #b720 
* #b7209 "Funktionen der Beweglichkeit der Knochen, nicht näher bezeichnet"
* #b7209 ^property[0].code = #parent 
* #b7209 ^property[0].valueCode = #b720 
* #b729 "Funktionen der Gelenke und Knochen, anders oder nicht näher bezeichnet"
* #b729 ^property[0].code = #parent 
* #b729 ^property[0].valueCode = #b710-b729 
* #b730-b749 "Funktionen der Muskeln"
* #b730-b749 ^property[0].code = #parent 
* #b730-b749 ^property[0].valueCode = #b7 
* #b730-b749 ^property[1].code = #child 
* #b730-b749 ^property[1].valueCode = #b730 
* #b730-b749 ^property[2].code = #child 
* #b730-b749 ^property[2].valueCode = #b735 
* #b730-b749 ^property[3].code = #child 
* #b730-b749 ^property[3].valueCode = #b740 
* #b730-b749 ^property[4].code = #child 
* #b730-b749 ^property[4].valueCode = #b749 
* #b730 "Funktionen der Muskelkraft"
* #b730 ^property[0].code = #None 
* #b730 ^property[0].valueString = "Funktionen, die im Zusammenhang mit der Kontraktionskraft eines Muskels oder von Muskelgruppen stehen" 
* #b730 ^property[1].code = #None 
* #b730 ^property[1].valueString = "Funktionen, die mit der Muskelkraft bestimmter Muskeln oder Muskelgruppen, Muskeln einer Extremität, einer Körperhälfte, der unteren Körperhälfte, aller Extremitäten, des Rumpfes und aller Muskeln des Körpers verbunden sind; Funktionsstörungen wie Schwäche der kleinen Muskeln der Hände und Füße, Muskelparese, Muskelparalyse, Monoplegie, Hemiplegie, Paraplegie, Tetraplegie und akinetischer Mutismus" 
* #b730 ^property[2].code = #None 
* #b730 ^property[2].valueString = "Funktionen des Muskeltonus" 
* #b730 ^property[3].code = #parent 
* #b730 ^property[3].valueCode = #b730-b749 
* #b730 ^property[4].code = #child 
* #b730 ^property[4].valueCode = #b7300 
* #b730 ^property[5].code = #child 
* #b730 ^property[5].valueCode = #b7301 
* #b730 ^property[6].code = #child 
* #b730 ^property[6].valueCode = #b7302 
* #b730 ^property[7].code = #child 
* #b730 ^property[7].valueCode = #b7303 
* #b730 ^property[8].code = #child 
* #b730 ^property[8].valueCode = #b7304 
* #b730 ^property[9].code = #child 
* #b730 ^property[9].valueCode = #b7305 
* #b730 ^property[10].code = #child 
* #b730 ^property[10].valueCode = #b7306 
* #b730 ^property[11].code = #child 
* #b730 ^property[11].valueCode = #b7308 
* #b730 ^property[12].code = #child 
* #b730 ^property[12].valueCode = #b7309 
* #b7300 "Kraft isolierter Muskeln oder von Muskelgruppen"
* #b7300 ^property[0].code = #None 
* #b7300 ^property[0].valueString = "Funktionen, die im Zusammenhang mit der Kontraktionskraft einzelner isolierter Muskeln oder Muskelgruppen stehen" 
* #b7300 ^property[1].code = #None 
* #b7300 ^property[1].valueString = "Funktionsstörungen wie Schwäche der kleinen Muskeln der Hände oder Füße" 
* #b7300 ^property[2].code = #parent 
* #b7300 ^property[2].valueCode = #b730 
* #b7301 "Kraft der Muskeln einer einzelnen Extremität"
* #b7301 ^property[0].code = #None 
* #b7301 ^property[0].valueString = "Funktionen, die im Zusammenhang mit der Kontraktionskraft der Muskeln und Muskelgruppen eines Armes oder Beines stehen" 
* #b7301 ^property[1].code = #None 
* #b7301 ^property[1].valueString = "Funktionsstörungen wie bei Monoparese und Monoplegie" 
* #b7301 ^property[2].code = #parent 
* #b7301 ^property[2].valueCode = #b730 
* #b7302 "Kraft der Muskeln einer Körperhälfte"
* #b7302 ^property[0].code = #None 
* #b7302 ^property[0].valueString = "Funktionen, die im Zusammenhang mit der Kontraktionskraft der Muskeln und Muskelgruppen der linken oder rechten Körperhälfte stehen" 
* #b7302 ^property[1].code = #None 
* #b7302 ^property[1].valueString = "Funktionsstörungen wie bei Hemiparese und Hemiplegie" 
* #b7302 ^property[2].code = #parent 
* #b7302 ^property[2].valueCode = #b730 
* #b7303 "Kraft der Muskeln der unteren Körperhälfte"
* #b7303 ^property[0].code = #None 
* #b7303 ^property[0].valueString = "Funktionen, die im Zusammenhang mit der Kontraktionskraft der Muskeln und Muskelgruppen der unteren Körperhälfte stehen" 
* #b7303 ^property[1].code = #None 
* #b7303 ^property[1].valueString = "Funktionsstörungen wie bei Paraparese und Paraplegie" 
* #b7303 ^property[2].code = #parent 
* #b7303 ^property[2].valueCode = #b730 
* #b7304 "Kraft der Muskeln aller Extremitäten"
* #b7304 ^property[0].code = #None 
* #b7304 ^property[0].valueString = "Funktionen, die im Zusammenhang mit der Kontraktionskraft der Muskeln und Muskelgruppen aller vier Extremitäten stehen" 
* #b7304 ^property[1].code = #None 
* #b7304 ^property[1].valueString = "Funktionsstörungen wie bei Tetraparese und Tetraplegie" 
* #b7304 ^property[2].code = #parent 
* #b7304 ^property[2].valueCode = #b730 
* #b7305 "Kraft der Rumpfmuskeln"
* #b7305 ^property[0].code = #None 
* #b7305 ^property[0].valueString = "Funktionen, die im Zusammenhang mit der Kontraktionskraft der Muskeln und Muskelgruppen des Rumpfes stehen" 
* #b7305 ^property[1].code = #parent 
* #b7305 ^property[1].valueCode = #b730 
* #b7306 "Kraft aller Muskeln des Körpers"
* #b7306 ^property[0].code = #None 
* #b7306 ^property[0].valueString = "Funktionen, die im Zusammenhang mit der Kontraktionskraft aller Muskeln und Muskelgruppen des Körpers stehen" 
* #b7306 ^property[1].code = #None 
* #b7306 ^property[1].valueString = "Funktionsstörungen wie bei akinetischem Mutismus" 
* #b7306 ^property[2].code = #parent 
* #b7306 ^property[2].valueCode = #b730 
* #b7308 "Funktionen der Muskelkraft, anders bezeichnet"
* #b7308 ^property[0].code = #parent 
* #b7308 ^property[0].valueCode = #b730 
* #b7309 "Funktionen der Muskelkraft, nicht näher bezeichnet"
* #b7309 ^property[0].code = #parent 
* #b7309 ^property[0].valueCode = #b730 
* #b735 "Funktionen des Muskeltonus"
* #b735 ^property[0].code = #None 
* #b735 ^property[0].valueString = "Funktionen, die im Zusammenhang mit dem Ruhetonus der Muskeln und dem Widerstand bei passiver Bewegung stehen" 
* #b735 ^property[1].code = #None 
* #b735 ^property[1].valueString = "Funktionen, die mit dem Tonus einzelner Muskeln und Muskelgruppen, Muskeln einer einzelnen Extremität, einer Körperhälfte, der unteren Körperhälfte, aller Extremitäten, des Rumpfes und aller Muskeln des Körpers verbunden sind; Funktionsstörungen wie verminderter Muskeltonus, erhöhter Muskeltonus, Spastik" 
* #b735 ^property[2].code = #None 
* #b735 ^property[2].valueString = "Funktionen der Muskelkraft" 
* #b735 ^property[3].code = #parent 
* #b735 ^property[3].valueCode = #b730-b749 
* #b735 ^property[4].code = #child 
* #b735 ^property[4].valueCode = #b7350 
* #b735 ^property[5].code = #child 
* #b735 ^property[5].valueCode = #b7351 
* #b735 ^property[6].code = #child 
* #b735 ^property[6].valueCode = #b7352 
* #b735 ^property[7].code = #child 
* #b735 ^property[7].valueCode = #b7353 
* #b735 ^property[8].code = #child 
* #b735 ^property[8].valueCode = #b7354 
* #b735 ^property[9].code = #child 
* #b735 ^property[9].valueCode = #b7355 
* #b735 ^property[10].code = #child 
* #b735 ^property[10].valueCode = #b7356 
* #b735 ^property[11].code = #child 
* #b735 ^property[11].valueCode = #b7358 
* #b735 ^property[12].code = #child 
* #b735 ^property[12].valueCode = #b7359 
* #b7350 "Tonus einzelner Muskeln und Muskelgruppen"
* #b7350 ^property[0].code = #None 
* #b7350 ^property[0].valueString = "Funktionen, die im Zusammenhang mit dem Ruhetonus einzelner Muskeln und Muskelgruppen und dem Widerstand bei passiver Bewegung dieser Muskeln stehen" 
* #b7350 ^property[1].code = #None 
* #b7350 ^property[1].valueString = "Funktionsstörungen wie bei fokaler Dystonie (Torticollis)" 
* #b7350 ^property[2].code = #parent 
* #b7350 ^property[2].valueCode = #b735 
* #b7351 "Tonus der Muskeln einer einzelnen Extremität"
* #b7351 ^property[0].code = #None 
* #b7351 ^property[0].valueString = "Funktionen, die im Zusammenhang mit dem Ruhetonus einzelner Muskeln und Muskelgruppen eines Armes oder Beines und dem Widerstand bei passiver Bewegung dieser Muskeln stehen" 
* #b7351 ^property[1].code = #None 
* #b7351 ^property[1].valueString = "Funktionsstörungen wie bei Monoparese und Monoplegie" 
* #b7351 ^property[2].code = #parent 
* #b7351 ^property[2].valueCode = #b735 
* #b7352 "Tonus der Muskeln einer Körperhälfte"
* #b7352 ^property[0].code = #None 
* #b7352 ^property[0].valueString = "Funktionen, die im Zusammenhang mit dem Ruhetonus einzelner Muskeln und Muskelgruppen der rechten oder linken Körperhälfte und dem Widerstand bei passiver Bewegung dieser Muskeln stehen" 
* #b7352 ^property[1].code = #None 
* #b7352 ^property[1].valueString = "Funktionsstörungen wie bei Hemiparese und Hemiplegie" 
* #b7352 ^property[2].code = #parent 
* #b7352 ^property[2].valueCode = #b735 
* #b7353 "Tonus der Muskeln der unteren Körperhälfte"
* #b7353 ^property[0].code = #None 
* #b7353 ^property[0].valueString = "Funktionen, die im Zusammenhang mit dem Ruhetonus einzelner Muskeln und Muskelgruppen der unteren Körperhälfte und dem Widerstand bei passiver Bewegung dieser Muskeln stehen" 
* #b7353 ^property[1].code = #None 
* #b7353 ^property[1].valueString = "Funktionsstörungen wie bei Paraparese und Paraplegie" 
* #b7353 ^property[2].code = #parent 
* #b7353 ^property[2].valueCode = #b735 
* #b7354 "Tonus der Muskeln aller Extremitäten"
* #b7354 ^property[0].code = #None 
* #b7354 ^property[0].valueString = "Funktionen, die im Zusammenhang mit dem Ruhetonus einzelner Muskeln und Muskelgruppen aller Extremitäten und dem Widerstand bei passiver Bewegung dieser Muskeln stehen" 
* #b7354 ^property[1].code = #None 
* #b7354 ^property[1].valueString = "Funktionsstörungen wie bei Tetraparese und Tetraplegie" 
* #b7354 ^property[2].code = #parent 
* #b7354 ^property[2].valueCode = #b735 
* #b7355 "Tonus der Muskeln des Rumpfes"
* #b7355 ^property[0].code = #None 
* #b7355 ^property[0].valueString = "Funktionen, die im Zusammenhang mit dem Ruhetonus einzelner Muskeln und Muskelgruppen des Rumpfes und dem Widerstand bei passiver Bewegung dieser Muskeln stehen" 
* #b7355 ^property[1].code = #parent 
* #b7355 ^property[1].valueCode = #b735 
* #b7356 "Tonus aller Muskeln des Körpers"
* #b7356 ^property[0].code = #None 
* #b7356 ^property[0].valueString = "Funktionen, die im Zusammenhang mit dem Ruhetonus einzelner Muskeln und Muskelgruppen aller Muskeln des Körpers und dem Widerstand bei passiver Bewegung dieser Muskeln stehen" 
* #b7356 ^property[1].code = #None 
* #b7356 ^property[1].valueString = "Funktionsstörungen wie bei generalisierter Dystonie und Morbus Parkinson oder bei genereller Paraparese und Paraplegie" 
* #b7356 ^property[2].code = #parent 
* #b7356 ^property[2].valueCode = #b735 
* #b7358 "Funktionen des Muskeltonus, anders bezeichnet"
* #b7358 ^property[0].code = #parent 
* #b7358 ^property[0].valueCode = #b735 
* #b7359 "Funktionen des Muskeltonus, nicht näher bezeichnet"
* #b7359 ^property[0].code = #parent 
* #b7359 ^property[0].valueCode = #b735 
* #b740 "Funktionen der Muskelausdauer"
* #b740 ^property[0].code = #None 
* #b740 ^property[0].valueString = "Funktionen, die im Zusammenhang mit der Aufrechterhaltung der Muskelkontraktion über einen geforderten Zeitraum stehen" 
* #b740 ^property[1].code = #None 
* #b740 ^property[1].valueString = "Funktionen, die mit der Aufrechterhaltung der Kontraktion einzelner Muskeln, von Muskelgruppen und aller Muskeln des Körpers verbunden sind; Funktionsstörungen wie Myasthenia gravis" 
* #b740 ^property[2].code = #None 
* #b740 ^property[2].valueString = "Funktionen der kardiorespiratorische Belastbarkeit" 
* #b740 ^property[3].code = #parent 
* #b740 ^property[3].valueCode = #b730-b749 
* #b740 ^property[4].code = #child 
* #b740 ^property[4].valueCode = #b7400 
* #b740 ^property[5].code = #child 
* #b740 ^property[5].valueCode = #b7401 
* #b740 ^property[6].code = #child 
* #b740 ^property[6].valueCode = #b7402 
* #b740 ^property[7].code = #child 
* #b740 ^property[7].valueCode = #b7408 
* #b740 ^property[8].code = #child 
* #b740 ^property[8].valueCode = #b7409 
* #b7400 "Ausdauer einzelner Muskeln"
* #b7400 ^property[0].code = #None 
* #b7400 ^property[0].valueString = "Funktionen, die im Zusammenhang mit der Aufrechterhaltung der Kontraktion einzelner Muskeln über einen geforderten Zeitraum stehen" 
* #b7400 ^property[1].code = #parent 
* #b7400 ^property[1].valueCode = #b740 
* #b7401 "Ausdauer von Muskelgruppen"
* #b7401 ^property[0].code = #None 
* #b7401 ^property[0].valueString = "Funktionen, die im Zusammenhang mit der Aufrechterhaltung der Kontraktion einzelner Muskelgruppen über einen geforderten Zeitraum stehen" 
* #b7401 ^property[1].code = #None 
* #b7401 ^property[1].valueString = "Funktionsstörungen wie bei Monoparese, Monoplegie, Hemiparese und Hemiplegie, Paraparese und Paraplegie" 
* #b7401 ^property[2].code = #parent 
* #b7401 ^property[2].valueCode = #b740 
* #b7402 "Ausdauer aller Muskeln des Körpers"
* #b7402 ^property[0].code = #None 
* #b7402 ^property[0].valueString = "Funktionen, die im Zusammenhang mit der Aufrechterhaltung der Kontraktion aller Muskeln über einen geforderten Zeitraum stehen" 
* #b7402 ^property[1].code = #None 
* #b7402 ^property[1].valueString = "Funktionsstörungen wie bei Tetraparese, Tetraplegie, generalisierter Parese und Paralyse" 
* #b7402 ^property[2].code = #parent 
* #b7402 ^property[2].valueCode = #b740 
* #b7408 "Funktionen der Muskelausdauer, anders bezeichnet"
* #b7408 ^property[0].code = #parent 
* #b7408 ^property[0].valueCode = #b740 
* #b7409 "Funktionen der Muskelausdauer, nicht näher bezeichnet"
* #b7409 ^property[0].code = #parent 
* #b7409 ^property[0].valueCode = #b740 
* #b749 "Funktionen der Muskeln, anders oder nicht näher bezeichnet"
* #b749 ^property[0].code = #parent 
* #b749 ^property[0].valueCode = #b730-b749 
* #b750-b789 "Funktionen der Bewegung"
* #b750-b789 ^property[0].code = #parent 
* #b750-b789 ^property[0].valueCode = #b7 
* #b750-b789 ^property[1].code = #child 
* #b750-b789 ^property[1].valueCode = #b750 
* #b750-b789 ^property[2].code = #child 
* #b750-b789 ^property[2].valueCode = #b755 
* #b750-b789 ^property[3].code = #child 
* #b750-b789 ^property[3].valueCode = #b760 
* #b750-b789 ^property[4].code = #child 
* #b750-b789 ^property[4].valueCode = #b765 
* #b750-b789 ^property[5].code = #child 
* #b750-b789 ^property[5].valueCode = #b770 
* #b750-b789 ^property[6].code = #child 
* #b750-b789 ^property[6].valueCode = #b780 
* #b750-b789 ^property[7].code = #child 
* #b750-b789 ^property[7].valueCode = #b789 
* #b750 "Funktionen der motorischen Reflexe"
* #b750 ^property[0].code = #None 
* #b750 ^property[0].valueString = "Funktionen, die unwillkürliche Muskelkontraktionen, ausgelöst durch spezifische Stimuli, betreffen" 
* #b750 ^property[1].code = #None 
* #b750 ^property[1].valueString = "Funktionen der Streckreflexe, der automatischen lokalen Reflexe, der Reflexe durch schädigende Stimuli und andere exterozeptive Stimuli; Schutzreflexe, Bizepssehnenreflex, Radius-Periost-Reflex, Quadrizepsreflex, Patellarsehnenreflex, Achillessehnenreflex" 
* #b750 ^property[2].code = #parent 
* #b750 ^property[2].valueCode = #b750-b789 
* #b750 ^property[3].code = #child 
* #b750 ^property[3].valueCode = #b7500 
* #b750 ^property[4].code = #child 
* #b750 ^property[4].valueCode = #b7501 
* #b750 ^property[5].code = #child 
* #b750 ^property[5].valueCode = #b7502 
* #b750 ^property[6].code = #child 
* #b750 ^property[6].valueCode = #b7508 
* #b750 ^property[7].code = #child 
* #b750 ^property[7].valueCode = #b7509 
* #b7500 "Streckreflexe"
* #b7500 ^property[0].code = #None 
* #b7500 ^property[0].valueString = "Funktionen, die durch Streckung hervorgerufene unwillkürliche Muskelkontraktionen betreffen" 
* #b7500 ^property[1].code = #parent 
* #b7500 ^property[1].valueCode = #b750 
* #b7501 "Reflexe durch schädigende Stimuli"
* #b7501 ^property[0].code = #None 
* #b7501 ^property[0].valueString = "Funktionen, die durch schmerzhafte oder andere schädigende Stimuli hervorgerufene unwillkürliche Muskelkontraktionen betreffen" 
* #b7501 ^property[1].code = #None 
* #b7501 ^property[1].valueString = "Schutzreflex" 
* #b7501 ^property[2].code = #parent 
* #b7501 ^property[2].valueCode = #b750 
* #b7502 "Reflexe durch andere exterozeptive Stimuli"
* #b7502 ^property[0].code = #None 
* #b7502 ^property[0].valueString = "Funktionen, die durch andere exterozeptive Stimuli hervorgerufene unwillkürliche Muskelkontraktionen betreffen" 
* #b7502 ^property[1].code = #parent 
* #b7502 ^property[1].valueCode = #b750 
* #b7508 "Funktionen der motorischen Reflexe, anders bezeichnet"
* #b7508 ^property[0].code = #parent 
* #b7508 ^property[0].valueCode = #b750 
* #b7509 "Funktionen der motorischen Reflexe, nicht näher bezeichnet"
* #b7509 ^property[0].code = #parent 
* #b7509 ^property[0].valueCode = #b750 
* #b755 "Funktionen der unwillkürlichen Bewegungsreaktionen"
* #b755 ^property[0].code = #None 
* #b755 ^property[0].valueString = "Funktionen, die unwillkürliche Kontraktionen großer Muskeln oder des ganzen Körpers, ausgelöst durch Körperhaltung, Gleichgewichts- und Schreckreaktionen, betreffen" 
* #b755 ^property[1].code = #None 
* #b755 ^property[1].valueString = "Funktionen der Reaktionen auf Lagewechsel, Aufrichtung, Körper-Anpassung sowie der Gleichgewichtsreaktionen, Stützreaktionen, Abwehrreaktionen" 
* #b755 ^property[2].code = #None 
* #b755 ^property[2].valueString = "Funktionen der motorischen Reflexe" 
* #b755 ^property[3].code = #parent 
* #b755 ^property[3].valueCode = #b750-b789 
* #b760 "Funktionen der Kontrolle von Willkürbewegungen"
* #b760 ^property[0].code = #None 
* #b760 ^property[0].valueString = "Funktionen, die mit der Kontrolle und Koordination von willkürlichen Bewegungen verbunden sind" 
* #b760 ^property[1].code = #None 
* #b760 ^property[1].valueString = "Funktionen der Kontrolle einfacher und komplexer Willkürbewegungen, der Koordination von Willkürbewegungen, Stützfunktionen der Arme oder Beine, motorische Rechts-Links-Koordination, Auge-Hand-Koordination, Auge-Fuß-Koordination; Funktionsstörungen wie Kontroll- und Koordinationsprobleme, z.B. Dysdiadochokinese" 
* #b760 ^property[2].code = #None 
* #b760 ^property[2].valueString = "Funktionen der Muskelkraft" 
* #b760 ^property[3].code = #parent 
* #b760 ^property[3].valueCode = #b750-b789 
* #b760 ^property[4].code = #child 
* #b760 ^property[4].valueCode = #b7600 
* #b760 ^property[5].code = #child 
* #b760 ^property[5].valueCode = #b7601 
* #b760 ^property[6].code = #child 
* #b760 ^property[6].valueCode = #b7602 
* #b760 ^property[7].code = #child 
* #b760 ^property[7].valueCode = #b7603 
* #b760 ^property[8].code = #child 
* #b760 ^property[8].valueCode = #b7608 
* #b760 ^property[9].code = #child 
* #b760 ^property[9].valueCode = #b7609 
* #b7600 "Kontrolle einfacher Willkürbewegungen"
* #b7600 ^property[0].code = #None 
* #b7600 ^property[0].valueString = "Funktionen, die mit der Kontrolle und Koordination einfacher oder isolierter Willkürbewegungen verbunden sind" 
* #b7600 ^property[1].code = #parent 
* #b7600 ^property[1].valueCode = #b760 
* #b7601 "Kontrolle komplexer Willkürbewegungen"
* #b7601 ^property[0].code = #None 
* #b7601 ^property[0].valueString = "Funktionen, die mit der Kontrolle und Koordination komplexer Willkürbewegungen verbunden sind" 
* #b7601 ^property[1].code = #parent 
* #b7601 ^property[1].valueCode = #b760 
* #b7602 "Koordination von Willkürbewegungen"
* #b7602 ^property[0].code = #None 
* #b7602 ^property[0].valueString = "Funktionen, die mit der Koordination einfacher oder komplexer Willkürbewegungen, Ausführung von Bewegungen in richtiger Kombination verbunden sind" 
* #b7602 ^property[1].code = #None 
* #b7602 ^property[1].valueString = "Rechts-Links-Koordination; Koordination visuell gesteuerter Bewegungen, wie z.B. Auge-Hand-Koordination und Auge-Fuß-Koordination; Funktionsstörungen wie Dysdiadochokinese" 
* #b7602 ^property[2].code = #parent 
* #b7602 ^property[2].valueCode = #b760 
* #b7603 "Stützbewegungen der Arme oder Beine"
* #b7603 ^property[0].code = #None 
* #b7603 ^property[0].valueString = "Funktionen, die mit der Kontrolle und Koordination von Willkürbewegungen durch Abstützung entweder durch die Arme (Ellenbogen oder Hände) oder die Beine (Knie oder Füße) verbunden sind" 
* #b7603 ^property[1].code = #parent 
* #b7603 ^property[1].valueCode = #b760 
* #b7608 "Funktionen der Kontrolle der Willkürbewegungen, anders bezeichnet"
* #b7608 ^property[0].code = #parent 
* #b7608 ^property[0].valueCode = #b760 
* #b7609 "Funktionen der Kontrolle der Willkürbewegungen, nicht näher bezeichnet"
* #b7609 ^property[0].code = #parent 
* #b7609 ^property[0].valueCode = #b760 
* #b765 "Funktionen der unwillkürlichen Bewegungen"
* #b765 ^property[0].code = #None 
* #b765 ^property[0].valueString = "Funktionen, die die unbeabsichtigten, nicht- oder halbzweckgerichteten unwillkürlichen Kontraktionen von Muskeln oder Muskelgruppen betreffen" 
* #b765 ^property[1].code = #None 
* #b765 ^property[1].valueString = "Unwillkürliche Muskelkontraktionen; Funktionsstörungen wie Tremor, Tics, Manierismen, Stereotypien, Perserverationen, Chorea, Athetose, Stimmtics, Dystonische Bewegungen, Dyskinesie" 
* #b765 ^property[2].code = #None 
* #b765 ^property[2].valueString = "Funktionen der Kontrolle von Willkürbewegungen" 
* #b765 ^property[3].code = #parent 
* #b765 ^property[3].valueCode = #b750-b789 
* #b765 ^property[4].code = #child 
* #b765 ^property[4].valueCode = #b7650 
* #b765 ^property[5].code = #child 
* #b765 ^property[5].valueCode = #b7651 
* #b765 ^property[6].code = #child 
* #b765 ^property[6].valueCode = #b7652 
* #b765 ^property[7].code = #child 
* #b765 ^property[7].valueCode = #b7653 
* #b765 ^property[8].code = #child 
* #b765 ^property[8].valueCode = #b7658 
* #b765 ^property[9].code = #child 
* #b765 ^property[9].valueCode = #b7659 
* #b7650 "Unwillkürliche Muskelkontraktionen"
* #b7650 ^property[0].code = #None 
* #b7650 ^property[0].valueString = "Funktionen, die die unbeabsichtigten, nicht- oder halbzweckgerichteten unwillkürlichen Kontraktionen eines Muskels oder von Muskelgruppen, wie teilweise bei psychischen Fehlleistungen, betreffen" 
* #b7650 ^property[1].code = #None 
* #b7650 ^property[1].valueString = "Funktionsstörungen wie choreatische und athetotische Bewegungen, schlafabhängige Bewegungsstörungen" 
* #b7650 ^property[2].code = #parent 
* #b7650 ^property[2].valueCode = #b765 
* #b7651 "Tremor"
* #b7651 ^property[0].code = #None 
* #b7651 ^property[0].valueString = "Funktionen, die die abwechselnde Anspannung und Entspannung von Muskelgruppen um ein Gelenk, die zu Zittern führen, betreffen" 
* #b7651 ^property[1].code = #parent 
* #b7651 ^property[1].valueCode = #b765 
* #b7652 "Tics und Manierismen"
* #b7652 ^property[0].code = #None 
* #b7652 ^property[0].valueString = "Funktionen, die sich wiederholende, scheinbar zweckgerichtete, unwillkürliche Kontraktionen von Muskelgruppen betreffen" 
* #b7652 ^property[1].code = #None 
* #b7652 ^property[1].valueString = "Funktionsstörungen wie Stimmtics, Koprolalie, Zähneknirschen" 
* #b7652 ^property[2].code = #parent 
* #b7652 ^property[2].valueCode = #b765 
* #b7653 "Stereotypien und motorische Perserverationen"
* #b7653 ^property[0].code = #None 
* #b7653 ^property[0].valueString = "Funktionen, die die spontanen, nicht zweckgerichteten Bewegungen wie Vor- und Zurückschaukeln und Kopfnicken oder Wackeln betreffen" 
* #b7653 ^property[1].code = #parent 
* #b7653 ^property[1].valueCode = #b765 
* #b7658 "Funktionen der unwillkürlichen Bewegungen, anders bezeichnet"
* #b7658 ^property[0].code = #parent 
* #b7658 ^property[0].valueCode = #b765 
* #b7659 "Funktionen der unwillkürlichen Bewegungen, nicht näher bezeichnet"
* #b7659 ^property[0].code = #parent 
* #b7659 ^property[0].valueCode = #b765 
* #b770 "Funktionen der Bewegungsmuster beim Gehen"
* #b770 ^property[0].code = #None 
* #b770 ^property[0].valueString = "Funktionen, die die Bewegungsmuster beim Gehen, Rennen oder anderen Bewegungsabläufen des gesamten Körpers betreffen" 
* #b770 ^property[1].code = #None 
* #b770 ^property[1].valueString = "Bewegungsmuster beim Gehen und Rennen; Funktionsstörungen wie spastisches, hemiplegisches, paraplegisches, asymmetrisches Gangbild, Hinken und steifes Gangbild" 
* #b770 ^property[2].code = #None 
* #b770 ^property[2].valueString = "Funktionen der Muskelkraft" 
* #b770 ^property[3].code = #parent 
* #b770 ^property[3].valueCode = #b750-b789 
* #b780 "Mit den Funktionen der Muskeln und der Bewegung in Zusammenhang stehende Empfindungen"
* #b780 ^property[0].code = #None 
* #b780 ^property[0].valueString = "Empfindungen, die mit den Muskeln oder Muskelgruppen des Körpers und ihren Bewegungen verbunden sind" 
* #b780 ^property[1].code = #None 
* #b780 ^property[1].valueString = "Empfindungen von Muskelsteifigkeit und Muskelverspannung, von Muskelkrämpfen oder von Muskelanspannung und Schweregefühl der Muskeln" 
* #b780 ^property[2].code = #None 
* #b780 ^property[2].valueString = "Schmerz" 
* #b780 ^property[3].code = #parent 
* #b780 ^property[3].valueCode = #b750-b789 
* #b780 ^property[4].code = #child 
* #b780 ^property[4].valueCode = #b7800 
* #b780 ^property[5].code = #child 
* #b780 ^property[5].valueCode = #b7801 
* #b780 ^property[6].code = #child 
* #b780 ^property[6].valueCode = #b7808 
* #b780 ^property[7].code = #child 
* #b780 ^property[7].valueCode = #b7809 
* #b7800 "Empfindung von Muskelsteifigkeit"
* #b7800 ^property[0].code = #None 
* #b7800 ^property[0].valueString = "Empfindung von Muskelverspannung oder -steifigkeit" 
* #b7800 ^property[1].code = #parent 
* #b7800 ^property[1].valueCode = #b780 
* #b7801 "Empfindung von Muskelspasmus"
* #b7801 ^property[0].code = #None 
* #b7801 ^property[0].valueString = "Empfindung einer unwillkürlichen Kontraktion von Muskeln oder Muskelgruppen" 
* #b7801 ^property[1].code = #parent 
* #b7801 ^property[1].valueCode = #b780 
* #b7808 "Mit den Funktionen der Muskeln und der Bewegung in Zusammenhang stehende Empfindungen, anders bezeichnet"
* #b7808 ^property[0].code = #parent 
* #b7808 ^property[0].valueCode = #b780 
* #b7809 "Mit den Funktionen der Muskeln und der Bewegung in Zusammenhang stehende Empfindungen, nicht näher bezeichnet"
* #b7809 ^property[0].code = #parent 
* #b7809 ^property[0].valueCode = #b780 
* #b789 "Funktionen der Bewegung, anders oder nicht näher bezeichnet"
* #b789 ^property[0].code = #parent 
* #b789 ^property[0].valueCode = #b750-b789 
* #b798 "Neuromuskuloskeletale und bewegungsbezogene Funktionen, anders bezeichnet"
* #b798 ^property[0].code = #parent 
* #b798 ^property[0].valueCode = #b7 
* #b799 "Neuromuskuloskeletale und bewegungsbezogene Funktionen, nicht näher bezeichnet"
* #b799 ^property[0].code = #parent 
* #b799 ^property[0].valueCode = #b7 
* #b8 "Funktionen der Haut und der Hautanhangsgebilde"
* #b8 ^property[0].code = #None 
* #b8 ^property[0].valueString = "Dieses Kapitel befasst sich mit den Funktionen, die die Haut, die Nägel und das Haar betreffen" 
* #b8 ^property[1].code = #parent 
* #b8 ^property[1].valueCode = #b 
* #b8 ^property[2].code = #child 
* #b8 ^property[2].valueCode = #b810-b849 
* #b8 ^property[3].code = #child 
* #b8 ^property[3].valueCode = #b850-b869 
* #b8 ^property[4].code = #child 
* #b8 ^property[4].valueCode = #b898 
* #b8 ^property[5].code = #child 
* #b8 ^property[5].valueCode = #b899 
* #b810-b849 "Funktionen der Haut"
* #b810-b849 ^property[0].code = #parent 
* #b810-b849 ^property[0].valueCode = #b8 
* #b810-b849 ^property[1].code = #child 
* #b810-b849 ^property[1].valueCode = #b810 
* #b810-b849 ^property[2].code = #child 
* #b810-b849 ^property[2].valueCode = #b820 
* #b810-b849 ^property[3].code = #child 
* #b810-b849 ^property[3].valueCode = #b830 
* #b810-b849 ^property[4].code = #child 
* #b810-b849 ^property[4].valueCode = #b840 
* #b810-b849 ^property[5].code = #child 
* #b810-b849 ^property[5].valueCode = #b849 
* #b810 "Schutzfunktionen der Haut"
* #b810 ^property[0].code = #None 
* #b810 ^property[0].valueString = "Funktionen der Haut zum Schutz des Körpers vor schädlichen physikalischen, chemischen und biologischen Einflüssen" 
* #b810 ^property[1].code = #None 
* #b810 ^property[1].valueString = "Schutz gegen Sonnenstrahlung und andere Strahlen, Lichtempfindlichkeit, Pigmentierung, Hauttyp; Fähigkeit der Wärmeregulierung, Narbenbildung, Induration; Funktionsstörungen wie Rissbildung, Geschwüre, Dekubitus, Atrophie" 
* #b810 ^property[2].code = #None 
* #b810 ^property[2].valueString = "Heilfunktion der Haut" 
* #b810 ^property[3].code = #parent 
* #b810 ^property[3].valueCode = #b810-b849 
* #b820 "Heilfunktion der Haut"
* #b820 ^property[0].code = #None 
* #b820 ^property[0].valueString = "Funktionen, die die Heilung von Wunden und anderen Schäden der Haut betreffen" 
* #b820 ^property[1].code = #None 
* #b820 ^property[1].valueString = "Funktionen der Krustenbildung, Heilung, Narbenbildung, Quetschung, Keloidbildung" 
* #b820 ^property[2].code = #None 
* #b820 ^property[2].valueString = "Schutzfunktionen der Haut" 
* #b820 ^property[3].code = #parent 
* #b820 ^property[3].valueCode = #b810-b849 
* #b830 "Andere Funktionen der Haut"
* #b830 ^property[0].code = #None 
* #b830 ^property[0].valueString = "Funktionen der Haut außer Schutz und Wiederherstellung, wie Kühlen und Schweißabsonderung" 
* #b830 ^property[1].code = #None 
* #b830 ^property[1].valueString = "Funktionen des Schwitzens, Funktionen der Hautdrüsen und sich daraus ergebender Körpergeruch" 
* #b830 ^property[2].code = #None 
* #b830 ^property[2].valueString = "Schutzfunktionen der Haut" 
* #b830 ^property[3].code = #parent 
* #b830 ^property[3].valueCode = #b810-b849 
* #b840 "Auf die Haut bezogene Empfindungen"
* #b840 ^property[0].code = #None 
* #b840 ^property[0].valueString = "Empfindungen im Zusammenhang mit der Haut, wie Juckreiz, brennende und stechende Empfindungen" 
* #b840 ^property[1].code = #None 
* #b840 ^property[1].valueString = "Funktionsstörungen wie Kribbelgefühl und \"Ameisenlaufen\"" 
* #b840 ^property[2].code = #None 
* #b840 ^property[2].valueString = "Schmerz" 
* #b840 ^property[3].code = #parent 
* #b840 ^property[3].valueCode = #b810-b849 
* #b849 "Funktionen der Haut, anders oder nicht näher bezeichnet"
* #b849 ^property[0].code = #parent 
* #b849 ^property[0].valueCode = #b810-b849 
* #b850-b869 "Funktionen des Haars und der Nägel"
* #b850-b869 ^property[0].code = #parent 
* #b850-b869 ^property[0].valueCode = #b8 
* #b850-b869 ^property[1].code = #child 
* #b850-b869 ^property[1].valueCode = #b850 
* #b850-b869 ^property[2].code = #child 
* #b850-b869 ^property[2].valueCode = #b860 
* #b850-b869 ^property[3].code = #child 
* #b850-b869 ^property[3].valueCode = #b869 
* #b850 "Funktionen des Haars"
* #b850 ^property[0].code = #None 
* #b850 ^property[0].valueString = "Funktionen, die das Haar betreffen, wie Schutz, Farbe und Aussehen" 
* #b850 ^property[1].code = #None 
* #b850 ^property[1].valueString = "Funktionen des Wachstums und der Pigmentierung des Haars, Lokalisation; Funktionsstörungen wie Haarverlust oder Alopezie" 
* #b850 ^property[2].code = #parent 
* #b850 ^property[2].valueCode = #b850-b869 
* #b860 "Funktionen der Nägel"
* #b860 ^property[0].code = #None 
* #b860 ^property[0].valueString = "Funktionen, die die Nägel betreffen, wie Schutz, Kratzen und Aussehen" 
* #b860 ^property[1].code = #None 
* #b860 ^property[1].valueString = "Wachstum und Pigmentierung der Nägel, Qualität der Nägel" 
* #b860 ^property[2].code = #parent 
* #b860 ^property[2].valueCode = #b850-b869 
* #b869 "Funktionen des Haars und der Nägel, anders oder nicht näher bezeichnet"
* #b869 ^property[0].code = #parent 
* #b869 ^property[0].valueCode = #b850-b869 
* #b898 "Funktionen der Haut und verwandter Strukturen, anders bezeichnet"
* #b898 ^property[0].code = #parent 
* #b898 ^property[0].valueCode = #b8 
* #b899 "Funktionen der Haut und verwandter Strukturen, nicht näher bezeichnet"
* #b899 ^property[0].code = #parent 
* #b899 ^property[0].valueCode = #b8 
* #d "Aktivitäten und Partizipation"
* #d ^property[0].code = #None 
* #d ^property[0].valueString = "Eine Aktivität ist die Durchführung einer Aufgabe oder einer Handlung (Aktion) durch einen Menschen." 
* #d ^property[1].code = #None 
* #d ^property[1].valueString = "Partizipation [Teilhabe] ist das Einbezogensein in eine Lebenssituation." 
* #d ^property[2].code = #None 
* #d ^property[2].valueString = "Eine Beeinträchtigung der Aktivität ist eine Schwierigkeit oder die Unmöglichkeit, die ein Mensch haben kann, die Aktivität durchzuführen." 
* #d ^property[3].code = #None 
* #d ^property[3].valueString = "Eine Beeinträchtigung der Partizipation [Teilhabe] ist ein Problem, das ein Mensch in Hinblick auf sein Einbezogensein in Lebenssituationen erleben kann." 
* #d ^property[4].code = #child 
* #d ^property[4].valueCode = #d1 
* #d ^property[5].code = #child 
* #d ^property[5].valueCode = #d2 
* #d ^property[6].code = #child 
* #d ^property[6].valueCode = #d3 
* #d ^property[7].code = #child 
* #d ^property[7].valueCode = #d4 
* #d ^property[8].code = #child 
* #d ^property[8].valueCode = #d5 
* #d ^property[9].code = #child 
* #d ^property[9].valueCode = #d6 
* #d ^property[10].code = #child 
* #d ^property[10].valueCode = #d7 
* #d ^property[11].code = #child 
* #d ^property[11].valueCode = #d8 
* #d ^property[12].code = #child 
* #d ^property[12].valueCode = #d9 
* #d1 "Lernen und Wissensanwendung"
* #d1 ^property[0].code = #None 
* #d1 ^property[0].valueString = "Dieses Kapitel befasst sich mit Lernen, Anwendung des Erlernten, Denken, Probleme lösen und Entscheidungen treffen." 
* #d1 ^property[1].code = #parent 
* #d1 ^property[1].valueCode = #d 
* #d1 ^property[2].code = #child 
* #d1 ^property[2].valueCode = #d110-d129 
* #d1 ^property[3].code = #child 
* #d1 ^property[3].valueCode = #d130-d159 
* #d1 ^property[4].code = #child 
* #d1 ^property[4].valueCode = #d160-d179 
* #d1 ^property[5].code = #child 
* #d1 ^property[5].valueCode = #d198 
* #d1 ^property[6].code = #child 
* #d1 ^property[6].valueCode = #d199 
* #d110-d129 "Bewusste sinnliche Wahrnehmungen"
* #d110-d129 ^property[0].code = #parent 
* #d110-d129 ^property[0].valueCode = #d1 
* #d110-d129 ^property[1].code = #child 
* #d110-d129 ^property[1].valueCode = #d110 
* #d110-d129 ^property[2].code = #child 
* #d110-d129 ^property[2].valueCode = #d115 
* #d110-d129 ^property[3].code = #child 
* #d110-d129 ^property[3].valueCode = #d120 
* #d110-d129 ^property[4].code = #child 
* #d110-d129 ^property[4].valueCode = #d129 
* #d110 "Zuschauen"
* #d110 ^property[0].code = #None 
* #d110 ^property[0].valueString = "Absichtsvoll den Sehsinn zu benutzen, um visuelle Reize wahrzunehmen, wie einer Sportveranstaltung oder dem Spiel von Kindern zuschauen" 
* #d110 ^property[1].code = #parent 
* #d110 ^property[1].valueCode = #d110-d129 
* #d115 "Zuhören"
* #d115 ^property[0].code = #None 
* #d115 ^property[0].valueString = "Absichtsvoll den Hörsinn zu benutzen, um akustische Reize wahrzunehmen, wie Radio, Musik oder einen Vortrag hören" 
* #d115 ^property[1].code = #parent 
* #d115 ^property[1].valueCode = #d110-d129 
* #d120 "Andere bewusste sinnliche Wahrnehmungen"
* #d120 ^property[0].code = #None 
* #d120 ^property[0].valueString = "Absichtsvoll andere elementare Sinne zu benutzen, um Reize wahrzunehmen, wie die materielle Struktur tasten und fühlen, Süßes schmecken oder Blumen riechen" 
* #d120 ^property[1].code = #parent 
* #d120 ^property[1].valueCode = #d110-d129 
* #d129 "Bewusste sinnliche Wahrnehmungen, anders oder nicht näher bezeichnet"
* #d129 ^property[0].code = #parent 
* #d129 ^property[0].valueCode = #d110-d129 
* #d130-d159 "Elementares Lernen"
* #d130-d159 ^property[0].code = #parent 
* #d130-d159 ^property[0].valueCode = #d1 
* #d130-d159 ^property[1].code = #child 
* #d130-d159 ^property[1].valueCode = #d130 
* #d130-d159 ^property[2].code = #child 
* #d130-d159 ^property[2].valueCode = #d135 
* #d130-d159 ^property[3].code = #child 
* #d130-d159 ^property[3].valueCode = #d140 
* #d130-d159 ^property[4].code = #child 
* #d130-d159 ^property[4].valueCode = #d145 
* #d130-d159 ^property[5].code = #child 
* #d130-d159 ^property[5].valueCode = #d150 
* #d130-d159 ^property[6].code = #child 
* #d130-d159 ^property[6].valueCode = #d155 
* #d130-d159 ^property[7].code = #child 
* #d130-d159 ^property[7].valueCode = #d159 
* #d130 "Nachmachen, nachahmen"
* #d130 ^property[0].code = #None 
* #d130 ^property[0].valueString = "Imitieren oder Nachahmen als elementare Bestandteile des Lernens, wie eine Geste, einen Laut oder einen Buchstaben des Alphabets nachmachen" 
* #d130 ^property[1].code = #parent 
* #d130 ^property[1].valueCode = #d130-d159 
* #d135 "Üben"
* #d135 ^property[0].code = #None 
* #d135 ^property[0].valueString = "Wiederholen einer Folge von Dingen oder Zeichen als elementarer Bestandteil des Lernens, wie in Zehnerfolgen zählen oder das Vortragen eines Gedichtes einüben" 
* #d135 ^property[1].code = #parent 
* #d135 ^property[1].valueCode = #d130-d159 
* #d140 "Lesen lernen"
* #d140 ^property[0].code = #None 
* #d140 ^property[0].valueString = "Die Fähigkeit zu entwickeln, Geschriebenes (einschließlich Braille) flüssig und richtig zu lesen, wie Zeichen und Buchstaben erkennen, Wörter in richtiger Betonung äußern sowie Wörter und Wendungen verstehen" 
* #d140 ^property[1].code = #parent 
* #d140 ^property[1].valueCode = #d130-d159 
* #d145 "Schreiben lernen"
* #d145 ^property[0].code = #None 
* #d145 ^property[0].valueString = "Die Fähigkeit zu entwickeln, Symbole zu produzieren, die der Darstellung von Lauten, Wörtern oder Wendungen dienen, um Bedeutungen zu vermitteln (einschließlich schreiben in Braille), wie richtig buchstabieren und die Grammatik korrekt verwenden" 
* #d145 ^property[1].code = #parent 
* #d145 ^property[1].valueCode = #d130-d159 
* #d150 "Rechnen lernen"
* #d150 ^property[0].code = #None 
* #d150 ^property[0].valueString = "Die Fähigkeit zu entwickeln, mit Zahlen umzugehen sowie einfache und komplexe mathematische Operationen auszuführen, wie mathematische Zeichen für Addition und Subtraktion benutzen sowie die richtige mathematische Operation auf ein Problem anwenden" 
* #d150 ^property[1].code = #parent 
* #d150 ^property[1].valueCode = #d130-d159 
* #d155 "Sich Fertigkeiten aneignen"
* #d155 ^property[0].code = #None 
* #d155 ^property[0].valueString = "Elementare und komplexe Fähigkeiten für integrierte Mengen von Handlungen und Aufgaben zu entwickeln, um die Aneignung einer Fertigkeit anzugehen und zu Ende zu bringen, wie Werkzeuge handhaben oder Spiele wie Schach spielen" 
* #d155 ^property[1].code = #None 
* #d155 ^property[1].valueString = "Sich elementare und komplexe Fähigkeiten aneignen" 
* #d155 ^property[2].code = #parent 
* #d155 ^property[2].valueCode = #d130-d159 
* #d155 ^property[3].code = #child 
* #d155 ^property[3].valueCode = #d1550 
* #d155 ^property[4].code = #child 
* #d155 ^property[4].valueCode = #d1551 
* #d155 ^property[5].code = #child 
* #d155 ^property[5].valueCode = #d1558 
* #d155 ^property[6].code = #child 
* #d155 ^property[6].valueCode = #d1559 
* #d1550 "Sich elementare Fertigkeiten aneignen"
* #d1550 ^property[0].code = #None 
* #d1550 ^property[0].valueString = "Elementare, bewusste Handlungen zu erlernen, wie mit Essutensilien, einem Bleistift oder einem einfachen Werkzeug umgehen lernen" 
* #d1550 ^property[1].code = #parent 
* #d1550 ^property[1].valueCode = #d155 
* #d1551 "Sich komplexe Fertigkeiten aneignen"
* #d1551 ^property[0].code = #None 
* #d1551 ^property[0].valueString = "Integrierte Mengen von Handlungen zu erlernen, um Regeln zu folgen sowie die eigenen Bewegungen korrekt aufeinander folgen zu lassen und zu koordinieren, wie Fußball spielen oder ein Bauwerkzeug benutzen lernen" 
* #d1551 ^property[1].code = #parent 
* #d1551 ^property[1].valueCode = #d155 
* #d1558 "Sich Fertigkeiten aneignen, anders bezeichnet"
* #d1558 ^property[0].code = #parent 
* #d1558 ^property[0].valueCode = #d155 
* #d1559 "Sich Fertigkeiten aneignen, nicht näher bezeichnet"
* #d1559 ^property[0].code = #parent 
* #d1559 ^property[0].valueCode = #d155 
* #d159 "Elementares Lernen, anders oder nicht näher bezeichnet"
* #d159 ^property[0].code = #parent 
* #d159 ^property[0].valueCode = #d130-d159 
* #d160-d179 "Wissensanwendung"
* #d160-d179 ^property[0].code = #parent 
* #d160-d179 ^property[0].valueCode = #d1 
* #d160-d179 ^property[1].code = #child 
* #d160-d179 ^property[1].valueCode = #d160 
* #d160-d179 ^property[2].code = #child 
* #d160-d179 ^property[2].valueCode = #d163 
* #d160-d179 ^property[3].code = #child 
* #d160-d179 ^property[3].valueCode = #d166 
* #d160-d179 ^property[4].code = #child 
* #d160-d179 ^property[4].valueCode = #d170 
* #d160-d179 ^property[5].code = #child 
* #d160-d179 ^property[5].valueCode = #d172 
* #d160-d179 ^property[6].code = #child 
* #d160-d179 ^property[6].valueCode = #d175 
* #d160-d179 ^property[7].code = #child 
* #d160-d179 ^property[7].valueCode = #d177 
* #d160-d179 ^property[8].code = #child 
* #d160-d179 ^property[8].valueCode = #d179 
* #d160 "Aufmerksamkeit fokussieren"
* #d160 ^property[0].code = #None 
* #d160 ^property[0].valueString = "Sich absichtsvoll auf einen bestimmten Reiz zu konzentrieren, wie ablenkende Geräusche filtern" 
* #d160 ^property[1].code = #parent 
* #d160 ^property[1].valueCode = #d160-d179 
* #d163 "Denken"
* #d163 ^property[0].code = #None 
* #d163 ^property[0].valueString = "Ideen, Konzepte und Vorstellungen - seien sie zielgerichtet oder nicht - zu formulieren und zu handhaben, allein oder mit anderen, wie eine Fiktion entwickeln, ein Theorem beweisen, mit Ideen spielen, Brainstorming betreiben, meditieren, Vor- und Nachteile abwägen, Vermutungen anstellen, überlegen" 
* #d163 ^property[1].code = #None 
* #d163 ^property[1].valueString = "Probleme lösen" 
* #d163 ^property[2].code = #parent 
* #d163 ^property[2].valueCode = #d160-d179 
* #d166 "Lesen"
* #d166 ^property[0].code = #None 
* #d166 ^property[0].valueString = "Aktivitäten im Zusammenhang mit der Erfassung und Interpretation von Texten (z.B. Bücher, Anweisungen oder Zeitungen - auch in Braille) durchzuführen, um allgemeines Wissen oder besondere Informationen zu erlangen" 
* #d166 ^property[1].code = #None 
* #d166 ^property[1].valueString = "Lesen lernen" 
* #d166 ^property[2].code = #parent 
* #d166 ^property[2].valueCode = #d160-d179 
* #d170 "Schreiben"
* #d170 ^property[0].code = #None 
* #d170 ^property[0].valueString = "Symbole oder Sprache zu verwenden oder zu produzieren, um Informationen zu vermitteln, wie schriftliche Aufzeichnungen von Ereignissen oder Ideen produzieren oder einen Brief entwerfen" 
* #d170 ^property[1].code = #None 
* #d170 ^property[1].valueString = "Schreiben lernen" 
* #d170 ^property[2].code = #parent 
* #d170 ^property[2].valueCode = #d160-d179 
* #d172 "Rechnen"
* #d172 ^property[0].code = #None 
* #d172 ^property[0].valueString = "Berechnungen unter Anwendung mathematischer Prinzipien durchzuführen, um in Worten beschriebene Probleme zu lösen und die Ergebnisse zu produzieren oder darzustellen, wie die Summe aus drei Zahlen berechnen oder das Ergebnis der Division einer Zahl durch eine andere finden" 
* #d172 ^property[1].code = #None 
* #d172 ^property[1].valueString = "Rechnen lernen" 
* #d172 ^property[2].code = #parent 
* #d172 ^property[2].valueCode = #d160-d179 
* #d175 "Probleme lösen"
* #d175 ^property[0].code = #None 
* #d175 ^property[0].valueString = "Lösungen für eine Frage oder Situation zu finden, indem das Problem identifiziert und analysiert wird, Lösungsmöglichkeiten entwickelt und die möglichen Auswirkungen der Lösungen abgeschätzt werden und die gewählte Lösung umgesetzt wird, wie die Auseinandersetzung zweier Personen schlichten" 
* #d175 ^property[1].code = #None 
* #d175 ^property[1].valueString = "Einfache oder komplexe Probleme lösen" 
* #d175 ^property[2].code = #None 
* #d175 ^property[2].valueString = "Denken" 
* #d175 ^property[3].code = #parent 
* #d175 ^property[3].valueCode = #d160-d179 
* #d175 ^property[4].code = #child 
* #d175 ^property[4].valueCode = #d1750 
* #d175 ^property[5].code = #child 
* #d175 ^property[5].valueCode = #d1751 
* #d175 ^property[6].code = #child 
* #d175 ^property[6].valueCode = #d1758 
* #d175 ^property[7].code = #child 
* #d175 ^property[7].valueCode = #d1759 
* #d1750 "Einfache Probleme lösen"
* #d1750 ^property[0].code = #None 
* #d1750 ^property[0].valueString = "Lösungen für ein einfaches Problem, das ein einzelnes Thema oder eine einzelne Frage betrifft, zu finden, indem das Problem identifiziert und analysiert wird, Lösungen entwickelt und die möglichen Auswirkungen der Lösungen abgeschätzt werden und die gewählte Lösung umgesetzt wird" 
* #d1750 ^property[1].code = #parent 
* #d1750 ^property[1].valueCode = #d175 
* #d1751 "Komplexe Probleme lösen"
* #d1751 ^property[0].code = #None 
* #d1751 ^property[0].valueString = "Lösungen für ein komplexes Problem, das multiple und voneinander abhängige Themen oder mehrere zusammenhängende Probleme betrifft, zu finden, indem das Problem identifiziert und analysiert wird, Lösungen entwickelt und die möglichen Auswirkungen der Lösungen abgeschätzt werden und die gewählte Lösung umgesetzt wird" 
* #d1751 ^property[1].code = #parent 
* #d1751 ^property[1].valueCode = #d175 
* #d1758 "Probleme lösen, anders bezeichnet"
* #d1758 ^property[0].code = #parent 
* #d1758 ^property[0].valueCode = #d175 
* #d1759 "Probleme lösen, nicht näher bezeichnet"
* #d1759 ^property[0].code = #parent 
* #d1759 ^property[0].valueCode = #d175 
* #d177 "Entscheidungen treffen"
* #d177 ^property[0].code = #None 
* #d177 ^property[0].valueString = "Eine Wahl zwischen Optionen zu treffen, diese umzusetzen und ihre Auswirkungen abzuschätzen, wie einen besonderen Gegenstand auswählen und kaufen, oder sich entscheiden, eine Aufgabe unter vielen, die erledigt werden müssen, übernehmen und diese ausführen" 
* #d177 ^property[1].code = #None 
* #d177 ^property[1].valueString = "Denken" 
* #d177 ^property[2].code = #parent 
* #d177 ^property[2].valueCode = #d160-d179 
* #d179 "Wissen anwenden, anders oder nicht näher bezeichnet"
* #d179 ^property[0].code = #parent 
* #d179 ^property[0].valueCode = #d160-d179 
* #d198 "Lernen und Wissen anwenden, anders bezeichnet"
* #d198 ^property[0].code = #parent 
* #d198 ^property[0].valueCode = #d1 
* #d199 "Lernen und Wissen anwenden, nicht näher bezeichnet"
* #d199 ^property[0].code = #parent 
* #d199 ^property[0].valueCode = #d1 
* #d2 "Allgemeine Aufgaben und Anforderungen"
* #d2 ^property[0].code = #None 
* #d2 ^property[0].valueString = "Dieses Kapitel befasst sich mit allgemeinen Aspekten der Ausführung von Einzel- und Mehrfachaufgaben, der Organisation von Routinen und dem Umgang mit Stress. Diese können in Verbindung mit spezifischeren Aufgaben und Handlungen verwendet werden, um die zugrunde liegenden Merkmale der Ausführung von Aufgaben unter verschiedenen Bedingungen zu ermitteln." 
* #d2 ^property[1].code = #parent 
* #d2 ^property[1].valueCode = #d 
* #d2 ^property[2].code = #child 
* #d2 ^property[2].valueCode = #d210 
* #d2 ^property[3].code = #child 
* #d2 ^property[3].valueCode = #d220 
* #d2 ^property[4].code = #child 
* #d2 ^property[4].valueCode = #d230 
* #d2 ^property[5].code = #child 
* #d2 ^property[5].valueCode = #d240 
* #d2 ^property[6].code = #child 
* #d2 ^property[6].valueCode = #d298 
* #d2 ^property[7].code = #child 
* #d2 ^property[7].valueCode = #d299 
* #d210 "Eine Einzelaufgabe übernehmen"
* #d210 ^property[0].code = #None 
* #d210 ^property[0].valueString = "Einfache oder komplexe und koordinierte Handlungen bezüglich der mentalen und physischen Bestandteile einer einzelnen Aufgabe auszuführen, wie eine Aufgabe angehen, Zeit, Räumlichkeit und Materialien für die Aufgabe organisieren, die Schritte der Durchführung festlegen, die Aufgabe ausführen und abschließen sowie eine Aufgabe durchstehen" 
* #d210 ^property[1].code = #None 
* #d210 ^property[1].valueString = "Eine einfache oder komplexe Aufgabe übernehmen; eine einzelne Aufgabe unabhängig oder in einer Gruppe übernehmen" 
* #d210 ^property[2].code = #None 
* #d210 ^property[2].valueString = "Sich Fertigkeiten aneignen" 
* #d210 ^property[3].code = #parent 
* #d210 ^property[3].valueCode = #d2 
* #d210 ^property[4].code = #child 
* #d210 ^property[4].valueCode = #d2100 
* #d210 ^property[5].code = #child 
* #d210 ^property[5].valueCode = #d2101 
* #d210 ^property[6].code = #child 
* #d210 ^property[6].valueCode = #d2102 
* #d210 ^property[7].code = #child 
* #d210 ^property[7].valueCode = #d2103 
* #d210 ^property[8].code = #child 
* #d210 ^property[8].valueCode = #d2108 
* #d210 ^property[9].code = #child 
* #d210 ^property[9].valueCode = #d2109 
* #d2100 "Eine einfache Aufgabe übernehmen"
* #d2100 ^property[0].code = #None 
* #d2100 ^property[0].valueString = "Die einfache Aufgabe vorzubereiten, anzugehen und sich um die erforderliche Zeit und Räumlichkeit zu kümmern; eine einfache Aufgabe mit einem einzelnen größeren Bestandteil auszuführen, wie ein Buch lesen, einen Brief schreiben oder sein Bett machen" 
* #d2100 ^property[1].code = #parent 
* #d2100 ^property[1].valueCode = #d210 
* #d2101 "Eine komplexe Aufgabe übernehmen"
* #d2101 ^property[0].code = #None 
* #d2101 ^property[0].valueString = "Die komplexe Aufgabe vorzubereiten, anzugehen und sich um die erforderliche Zeit und Räumlichkeit zu kümmern; eine komplexe Aufgabe mit mehr als einem Bestandteil auszuführen, wobei die Bearbeitung in aufeinander folgenden Schritten oder gleichzeitig erfolgen kann, wie die Möbel in seiner Wohnung anordnen oder seine Schularbeiten machen" 
* #d2101 ^property[1].code = #parent 
* #d2101 ^property[1].valueCode = #d210 
* #d2102 "Eine Einzelaufgabe unabhängig übernehmen"
* #d2102 ^property[0].code = #None 
* #d2102 ^property[0].valueString = "Die einfache oder komplexe Aufgabe vorzubereiten, anzugehen und sich um die erforderliche Zeit und Räumlichkeit zu kümmern; eine Aufgabe allein ohne Hilfe anderer zu handhaben und zu bearbeiten" 
* #d2102 ^property[1].code = #parent 
* #d2102 ^property[1].valueCode = #d210 
* #d2103 "Eine Einzelaufgabe in einer Gruppe bewältigen"
* #d2103 ^property[0].code = #None 
* #d2103 ^property[0].valueString = "Die einfache oder komplexe Aufgabe vorzubereiten, anzugehen und sich um die erforderliche Zeit und Räumlichkeit zu kümmern; eine Aufgabe mit anderen Personen, die in einigen oder allen Schritten der Aufgabe einbezogen sind, zu handhaben und zu bearbeiten" 
* #d2103 ^property[1].code = #parent 
* #d2103 ^property[1].valueCode = #d210 
* #d2108 "Einzelaufgaben übernehmen, anders bezeichnet"
* #d2108 ^property[0].code = #parent 
* #d2108 ^property[0].valueCode = #d210 
* #d2109 "Einzelaufgaben übernehmen, nicht näher bezeichnet"
* #d2109 ^property[0].code = #parent 
* #d2109 ^property[0].valueCode = #d210 
* #d220 "Mehrfachaufgaben übernehmen"
* #d220 ^property[0].code = #None 
* #d220 ^property[0].valueString = "Einfache oder komplexe und koordinierte Handlungen als Bestandteile einer multiplen, integrierten und komplexen Aufgabe in aufeinander folgenden Schritten oder gleichzeitig zu bearbeiten" 
* #d220 ^property[1].code = #None 
* #d220 ^property[1].valueString = "Mehrfachaufgaben zu Ende bringen; Mehrfachaufgaben unabhängig oder in einer Gruppe übernehmen" 
* #d220 ^property[2].code = #None 
* #d220 ^property[2].valueString = "Sich Fertigkeiten aneignen" 
* #d220 ^property[3].code = #parent 
* #d220 ^property[3].valueCode = #d2 
* #d220 ^property[4].code = #child 
* #d220 ^property[4].valueCode = #d2200 
* #d220 ^property[5].code = #child 
* #d220 ^property[5].valueCode = #d2201 
* #d220 ^property[6].code = #child 
* #d220 ^property[6].valueCode = #d2202 
* #d220 ^property[7].code = #child 
* #d220 ^property[7].valueCode = #d2203 
* #d220 ^property[8].code = #child 
* #d220 ^property[8].valueCode = #d2208 
* #d220 ^property[9].code = #child 
* #d220 ^property[9].valueCode = #d2209 
* #d2200 "Mehrfachaufgaben bearbeiten"
* #d2200 ^property[0].code = #None 
* #d2200 ^property[0].valueString = "Mehrere Aufgaben vorzubereiten, anzugehen und sich um die erforderliche Zeit und Räumlichkeit zu kümmern; mehrere Aufgaben in aufeinander folgenden Schritten oder gleichzeitig zu handhaben und zu bearbeiten" 
* #d2200 ^property[1].code = #parent 
* #d2200 ^property[1].valueCode = #d220 
* #d2201 "Mehrfachaufgaben abschließen"
* #d2201 ^property[0].code = #None 
* #d2201 ^property[0].valueString = "Mehrere Aufgaben in aufeinander folgenden Schritten oder gleichzeitig abzuschließen" 
* #d2201 ^property[1].code = #parent 
* #d2201 ^property[1].valueCode = #d220 
* #d2202 "Mehrfachaufgaben unabhängig übernehmen"
* #d2202 ^property[0].code = #None 
* #d2202 ^property[0].valueString = "Mehrere Aufgaben vorzubereiten, anzugehen und sich um die erforderliche Zeit und Räumlichkeit zu kümmern; mehrere Aufgaben in aufeinander folgenden Schritten oder gleichzeitig allein ohne die Hilfe anderer zu handhaben und zu bearbeiten" 
* #d2202 ^property[1].code = #parent 
* #d2202 ^property[1].valueCode = #d220 
* #d2203 "Mehrfachaufgaben in einer Gruppe übernehmen"
* #d2203 ^property[0].code = #None 
* #d2203 ^property[0].valueString = "Mehrfachaufgaben vorzubereiten, anzugehen und sich um die erforderliche Zeit und Räumlichkeit zu kümmern; mehrere Aufgaben mit anderen Personen, die in einigen oder allen Schritten der Mehrfachaufgaben einbezogen sind, in aufeinander folgenden Schritten oder gleichzeitig zu handhaben und zu bearbeiten" 
* #d2203 ^property[1].code = #parent 
* #d2203 ^property[1].valueCode = #d220 
* #d2208 "Mehrfachaufgaben übernehmen, anders bezeichnet"
* #d2208 ^property[0].code = #parent 
* #d2208 ^property[0].valueCode = #d220 
* #d2209 "Mehrfachaufgaben übernehmen, nicht näher bezeichnet"
* #d2209 ^property[0].code = #parent 
* #d2209 ^property[0].valueCode = #d220 
* #d230 "Die tägliche Routine durchführen"
* #d230 ^property[0].code = #None 
* #d230 ^property[0].valueString = "Einfache und komplexe und koordinierte Handlungen auszuführen, um die Anforderungen der alltäglichen Prozeduren oder Pflichten zu planen, zu handhaben und zu bewältigen, wie Zeit einplanen und den Tagesplan für die verschiedenen Aktivitäten aufstellen" 
* #d230 ^property[1].code = #None 
* #d230 ^property[1].valueString = "Die tägliche Routine handhaben und zu Ende bringen; das eigene Aktivitätsniveau handhaben" 
* #d230 ^property[2].code = #None 
* #d230 ^property[2].valueString = "Mehrfachaufgaben übernehmen" 
* #d230 ^property[3].code = #parent 
* #d230 ^property[3].valueCode = #d2 
* #d230 ^property[4].code = #child 
* #d230 ^property[4].valueCode = #d2301 
* #d230 ^property[5].code = #child 
* #d230 ^property[5].valueCode = #d2302 
* #d230 ^property[6].code = #child 
* #d230 ^property[6].valueCode = #d2303 
* #d230 ^property[7].code = #child 
* #d230 ^property[7].valueCode = #d2308 
* #d230 ^property[8].code = #child 
* #d230 ^property[8].valueCode = #d2309 
* #d2301 "Die tägliche Routine planen"
* #d2301 ^property[0].code = #None 
* #d2301 ^property[0].valueString = "Einfache und komplexe, koordinierte Handlungen auszuführen, um die Anforderungen der alltäglichen Prozeduren oder Pflichten zu planen und zu handhaben" 
* #d2301 ^property[1].code = #parent 
* #d2301 ^property[1].valueCode = #d230 
* #d2302 "Die tägliche Routine abschließen"
* #d2302 ^property[0].code = #None 
* #d2302 ^property[0].valueString = "Einfache und komplexe, koordinierte Handlungen, die eine Person durchführen muss, um die Anforderungen der alltäglichen Prozeduren oder Pflichten zu einem Abschluss zu bringen" 
* #d2302 ^property[1].code = #parent 
* #d2302 ^property[1].valueCode = #d230 
* #d2303 "Das eigene Aktivitätsniveau handhaben"
* #d2303 ^property[0].code = #None 
* #d2303 ^property[0].valueString = "Handlungen und Verhaltensweisen, die eine Person durchführen bzw. zeigen muss, um den Zeit- und Energiebedarf für die Anforderungen der alltäglichen Prozeduren oder Pflichten einzuplanen" 
* #d2303 ^property[1].code = #parent 
* #d2303 ^property[1].valueCode = #d230 
* #d2308 "Die tägliche Routine durchführen, anders bezeichnet"
* #d2308 ^property[0].code = #parent 
* #d2308 ^property[0].valueCode = #d230 
* #d2309 "Die tägliche Routine durchführen, nicht näher bezeichnet"
* #d2309 ^property[0].code = #parent 
* #d2309 ^property[0].valueCode = #d230 
* #d240 "Mit Stress und anderen psychischen Anforderungen umgehen"
* #d240 ^property[0].code = #None 
* #d240 ^property[0].valueString = "Einfache oder komplexe und koordinierte Handlungen durchzuführen, um die psychischen Anforderungen, die erforderlich sind, um Aufgaben, die besondere Verantwortung beinhalten sowie mit Stress, Störungen und Krisensituationen verbunden sind, zu handhaben und zu kontrollieren, wie ein Fahrzeug bei dichtem Verkehr fahren oder viele Kinder betreuen" 
* #d240 ^property[1].code = #None 
* #d240 ^property[1].valueString = "Mit Verantwortung umgehen; mit Stress und Krisensituationen umgehen" 
* #d240 ^property[2].code = #parent 
* #d240 ^property[2].valueCode = #d2 
* #d240 ^property[3].code = #child 
* #d240 ^property[3].valueCode = #d2400 
* #d240 ^property[4].code = #child 
* #d240 ^property[4].valueCode = #d2401 
* #d240 ^property[5].code = #child 
* #d240 ^property[5].valueCode = #d2402 
* #d240 ^property[6].code = #child 
* #d240 ^property[6].valueCode = #d2408 
* #d240 ^property[7].code = #child 
* #d240 ^property[7].valueCode = #d2409 
* #d2400 "Mit Verantwortung umgehen"
* #d2400 ^property[0].code = #None 
* #d2400 ^property[0].valueString = "Einfache oder komplexe, koordinierte Handlungen durchzuführen, um die Pflichten der Aufgabenerfüllung zu handhaben und die Anforderungen dieser Pflichten zu beurteilen" 
* #d2400 ^property[1].code = #parent 
* #d2400 ^property[1].valueCode = #d240 
* #d2401 "Mit Stress umgehen"
* #d2401 ^property[0].code = #None 
* #d2401 ^property[0].valueString = "Einfache oder komplexe, koordinierte Handlungen durchzuführen, um mit Druck, Notfallsituationen oder Stress im Zusammenhang mit der Aufgabenerfüllung umzugehen" 
* #d2401 ^property[1].code = #parent 
* #d2401 ^property[1].valueCode = #d240 
* #d2402 "Mit Krisensituationen umgehen"
* #d2402 ^property[0].code = #None 
* #d2402 ^property[0].valueString = "Einfache oder komplexe, koordinierte Handlungen durchzuführen, um entscheidende Wendepunkte in einer bestimmten Situation oder in Zeiten akuter Gefahr oder Schwierigkeit zu bewältigen" 
* #d2402 ^property[1].code = #parent 
* #d2402 ^property[1].valueCode = #d240 
* #d2408 "Mit Stress und anderen psychischen Anforderungen umgehen, anders bezeichnet"
* #d2408 ^property[0].code = #parent 
* #d2408 ^property[0].valueCode = #d240 
* #d2409 "Mit Stress und anderen psychischen Anforderungen umgehen, nicht näher bezeichnet"
* #d2409 ^property[0].code = #parent 
* #d2409 ^property[0].valueCode = #d240 
* #d298 "Allgemeine Aufgaben und Anforderungen, anders bezeichnet"
* #d298 ^property[0].code = #parent 
* #d298 ^property[0].valueCode = #d2 
* #d299 "Allgemeine Aufgaben und Anforderungen, nicht näher bezeichnet"
* #d299 ^property[0].code = #parent 
* #d299 ^property[0].valueCode = #d2 
* #d3 "Kommunikation"
* #d3 ^property[0].code = #None 
* #d3 ^property[0].valueString = "Dieses Kapitel befasst sich mit allgemeinen und spezifischen Merkmalen der Kommunikation mittels Sprache, Zeichen und Symbolen, einschließlich des Verstehens und Produzierens von Mitteilungen, sowie der Konversation und des Gebrauchs von Kommunikationsgeräten und -techniken." 
* #d3 ^property[1].code = #parent 
* #d3 ^property[1].valueCode = #d 
* #d3 ^property[2].code = #child 
* #d3 ^property[2].valueCode = #d310-d329 
* #d3 ^property[3].code = #child 
* #d3 ^property[3].valueCode = #d330-d349 
* #d3 ^property[4].code = #child 
* #d3 ^property[4].valueCode = #d350-d369 
* #d3 ^property[5].code = #child 
* #d3 ^property[5].valueCode = #d398 
* #d3 ^property[6].code = #child 
* #d3 ^property[6].valueCode = #d399 
* #d310-d329 "Kommunizieren als Empfänger"
* #d310-d329 ^property[0].code = #parent 
* #d310-d329 ^property[0].valueCode = #d3 
* #d310-d329 ^property[1].code = #child 
* #d310-d329 ^property[1].valueCode = #d310 
* #d310-d329 ^property[2].code = #child 
* #d310-d329 ^property[2].valueCode = #d315 
* #d310-d329 ^property[3].code = #child 
* #d310-d329 ^property[3].valueCode = #d320 
* #d310-d329 ^property[4].code = #child 
* #d310-d329 ^property[4].valueCode = #d325 
* #d310-d329 ^property[5].code = #child 
* #d310-d329 ^property[5].valueCode = #d329 
* #d310 "Kommunizieren als Empfänger gesprochener Mitteilungen"
* #d310 ^property[0].code = #None 
* #d310 ^property[0].valueString = "Die wörtliche und übertragene Bedeutung von gesprochenen Mitteilungen zu erfassen, wie verstehen, ob eine Aussage eine Tatsache behauptet oder ob sie eine idiomatische Wendung ist" 
* #d310 ^property[1].code = #parent 
* #d310 ^property[1].valueCode = #d310-d329 
* #d315 "Kommunizieren als Empfänger non-verbaler Mitteilungen"
* #d315 ^property[0].code = #None 
* #d315 ^property[0].valueString = "Die wörtliche und übertragene Bedeutung von durch Gesten, Symbole und Zeichnungen vermittelten Mitteilungen zu erfassen, wie erkennen, dass ein Kinde müde ist, wenn es seine Augen reibt, oder dass das Läuten einer Warnglocke Feuer bedeutet" 
* #d315 ^property[1].code = #None 
* #d315 ^property[1].valueString = "Kommunizieren als Empfänger von Körpergesten, allgemeinen Zeichen und Symbolen, Zeichnungen und Fotos" 
* #d315 ^property[2].code = #parent 
* #d315 ^property[2].valueCode = #d310-d329 
* #d315 ^property[3].code = #child 
* #d315 ^property[3].valueCode = #d3150 
* #d315 ^property[4].code = #child 
* #d315 ^property[4].valueCode = #d3151 
* #d315 ^property[5].code = #child 
* #d315 ^property[5].valueCode = #d3152 
* #d315 ^property[6].code = #child 
* #d315 ^property[6].valueCode = #d3158 
* #d315 ^property[7].code = #child 
* #d315 ^property[7].valueCode = #d3159 
* #d3150 "Kommunizieren als Empfänger von Gesten oder Gebärden"
* #d3150 ^property[0].code = #None 
* #d3150 ^property[0].valueString = "Die Bedeutung von Gesichtsausdruck, Handbewegungen oder -zeichen, Körperhaltung und anderen Formen der Körpersprache zu erfassen" 
* #d3150 ^property[1].code = #parent 
* #d3150 ^property[1].valueCode = #d315 
* #d3151 "Kommunizieren als Empfänger von allgemeinen Zeichen und Symbolen"
* #d3151 ^property[0].code = #None 
* #d3151 ^property[0].valueString = "Die Bedeutung von öffentlichen Zeichen und Symbolen zu erfassen wie Verkehrszeichen, Warnsymbole, Notationen (z. B. musikalische, mathematische und wissenschaftliche) sowie Bildsymbole" 
* #d3151 ^property[1].code = #parent 
* #d3151 ^property[1].valueCode = #d315 
* #d3152 "Kommunizieren als Empfänger von Zeichnungen und Fotos"
* #d3152 ^property[0].code = #None 
* #d3152 ^property[0].valueString = "Die in Zeichnungen und Fotos (z. B. Strichzeichnungen, grafische Entwürfe, Gemälde, dreidimensionale Darstellungen) sowie in grafischen Darstellungen, Diagrammen und Fotos vermittelte Bedeutung zu erfassen, wie z.B. verstehen, dass eine Aufwärtslinie in einem Größendiagramm anzeigt, dass ein Kind wächst" 
* #d3152 ^property[1].code = #parent 
* #d3152 ^property[1].valueCode = #d315 
* #d3158 "Kommunizieren als Empfänger non-verbaler Mitteilungen, anders bezeichnet"
* #d3158 ^property[0].code = #parent 
* #d3158 ^property[0].valueCode = #d315 
* #d3159 "Kommunizieren als Empfänger non-verbaler Mitteilungen, nicht näher bezeichnet"
* #d3159 ^property[0].code = #parent 
* #d3159 ^property[0].valueCode = #d315 
* #d320 "Kommunizieren als Empfänger von Mitteilungen in Gebärdensprache"
* #d320 ^property[0].code = #None 
* #d320 ^property[0].valueString = "Die wörtliche und übertragene Bedeutung von Mitteilungen in Gebärdensprache zu empfangen und zu erfassen" 
* #d320 ^property[1].code = #parent 
* #d320 ^property[1].valueCode = #d310-d329 
* #d325 "Kommunizieren als Empfänger schriftlicher Mitteilungen"
* #d325 ^property[0].code = #None 
* #d325 ^property[0].valueString = "Die wörtliche und übertragene Bedeutung schriftlicher Mitteilungen (einschließlich Braille) zu erfassen, wie politische Ereignisse in der Tagespresse verfolgen oder die Absicht einer religiösen Schrift verstehen" 
* #d325 ^property[1].code = #parent 
* #d325 ^property[1].valueCode = #d310-d329 
* #d329 "Kommunizieren als Empfänger, anders oder nicht näher bezeichnet"
* #d329 ^property[0].code = #parent 
* #d329 ^property[0].valueCode = #d310-d329 
* #d330-d349 "Kommunizieren als Sender"
* #d330-d349 ^property[0].code = #parent 
* #d330-d349 ^property[0].valueCode = #d3 
* #d330-d349 ^property[1].code = #child 
* #d330-d349 ^property[1].valueCode = #d330 
* #d330-d349 ^property[2].code = #child 
* #d330-d349 ^property[2].valueCode = #d335 
* #d330-d349 ^property[3].code = #child 
* #d330-d349 ^property[3].valueCode = #d340 
* #d330-d349 ^property[4].code = #child 
* #d330-d349 ^property[4].valueCode = #d345 
* #d330-d349 ^property[5].code = #child 
* #d330-d349 ^property[5].valueCode = #d349 
* #d330 "Sprechen"
* #d330 ^property[0].code = #None 
* #d330 ^property[0].valueString = "Wörter, Wendungen oder längere Passagen in mündlichen Mitteilungen mit wörtlicher und übertragener Bedeutung zu äußern, wie in gesprochener Sprache eine Tatsache ausdrücken oder eine Geschichte erzählen" 
* #d330 ^property[1].code = #parent 
* #d330 ^property[1].valueCode = #d330-d349 
* #d335 "Non-verbale Mitteilungen produzieren"
* #d335 ^property[0].code = #None 
* #d335 ^property[0].valueString = "Gesten, Symbole und Zeichnungen zur Vermittlung von Bedeutungen einzusetzen, wie seinen Kopf schütteln, um Uneinigkeit anzuzeigen, oder ein Bild oder Diagramm zeichnen, um eine Tatsache oder eine komplexe Vorstellung zu vermitteln" 
* #d335 ^property[1].code = #None 
* #d335 ^property[1].valueString = "Körpergesten, Zeichen, Symbole, Zeichnungen und Fotos produzieren" 
* #d335 ^property[2].code = #parent 
* #d335 ^property[2].valueCode = #d330-d349 
* #d335 ^property[3].code = #child 
* #d335 ^property[3].valueCode = #d3350 
* #d335 ^property[4].code = #child 
* #d335 ^property[4].valueCode = #d3351 
* #d335 ^property[5].code = #child 
* #d335 ^property[5].valueCode = #d3352 
* #d335 ^property[6].code = #child 
* #d335 ^property[6].valueCode = #d3358 
* #d335 ^property[7].code = #child 
* #d335 ^property[7].valueCode = #d3359 
* #d3350 "Körpersprache einsetzen"
* #d3350 ^property[0].code = #None 
* #d3350 ^property[0].valueString = "Eine Bedeutung mit Körperbewegungen vermitteln, wie mit Gesichtsausdruck (z.B. lächeln, Stirn runzeln, zusammenzucken), Bewegungen und Haltungen von Armen und Händen (z.B., wie beim Umarmen, um Zuneigung zu zeigen)" 
* #d3350 ^property[1].code = #parent 
* #d3350 ^property[1].valueCode = #d335 
* #d3351 "Zeichen und Symbole produzieren"
* #d3351 ^property[0].code = #None 
* #d3351 ^property[0].valueString = "Bedeutung durch Verwendung von Zeichen und Symbolen (z.B. Bildsymbole, Bliss-Tafeln, wissenschaftliche Symbole) und symbolischen Notationssystemen zu vermitteln, wie die Notenschrift zu benutzen, um eine Melodie zu schreiben" 
* #d3351 ^property[1].code = #parent 
* #d3351 ^property[1].valueCode = #d335 
* #d3352 "Zeichnungen und Fotos machen"
* #d3352 ^property[0].code = #None 
* #d3352 ^property[0].valueString = "Bedeutung durch Zeichnen, Malen, Skizzieren und Herstellen von Diagrammen, Bildern oder Fotos zu vermitteln, wie eine Karte zeichnen, um jemanden die Richtung zu einem Ort anzugeben" 
* #d3352 ^property[1].code = #parent 
* #d3352 ^property[1].valueCode = #d335 
* #d3358 "Non-verbale Mitteilungen produzieren, anders bezeichnet"
* #d3358 ^property[0].code = #parent 
* #d3358 ^property[0].valueCode = #d335 
* #d3359 "Non-verbale Mitteilungen produzieren, nicht näher bezeichnet"
* #d3359 ^property[0].code = #parent 
* #d3359 ^property[0].valueCode = #d335 
* #d340 "Mitteilungen in Gebärdensprache ausdrücken"
* #d340 ^property[0].code = #None 
* #d340 ^property[0].valueString = "Mitteilungen mit wörtlicher und übertragener Bedeutung in Gebärdensprache zu vermitteln" 
* #d340 ^property[1].code = #parent 
* #d340 ^property[1].valueCode = #d330-d349 
* #d345 "Mitteilungen schreiben"
* #d345 ^property[0].code = #None 
* #d345 ^property[0].valueString = "Die wörtliche und übertragene Bedeutung von Mitteilungen, die in geschriebener Sprache vermittelt sind, zu verfassen, wie einem Freund einen Brief schreiben" 
* #d345 ^property[1].code = #parent 
* #d345 ^property[1].valueCode = #d330-d349 
* #d349 "Kommunizieren als Sender, anders oder nicht näher bezeichnet"
* #d349 ^property[0].code = #parent 
* #d349 ^property[0].valueCode = #d330-d349 
* #d350-d369 "Konversation und Gebrauch von Kommunikationsgeräten und -techniken"
* #d350-d369 ^property[0].code = #parent 
* #d350-d369 ^property[0].valueCode = #d3 
* #d350-d369 ^property[1].code = #child 
* #d350-d369 ^property[1].valueCode = #d350 
* #d350-d369 ^property[2].code = #child 
* #d350-d369 ^property[2].valueCode = #d355 
* #d350-d369 ^property[3].code = #child 
* #d350-d369 ^property[3].valueCode = #d360 
* #d350-d369 ^property[4].code = #child 
* #d350-d369 ^property[4].valueCode = #d369 
* #d350 "Konversation"
* #d350 ^property[0].code = #None 
* #d350 ^property[0].valueString = "Einen Gedanken- und Ideenaustausch in mündlicher oder schriftlicher Form, in Gebärdensprache oder auf anderer sprachlicher Weise zu beginnen, aufrecht zu erhalten und zu beenden, mit einer oder mehreren Personen, Bekannten oder Fremden, in formeller oder informeller Form" 
* #d350 ^property[1].code = #None 
* #d350 ^property[1].valueString = "Eine Konversation beginnen, aufrecht erhalten und beenden; sich mit einer oder vielen Personen unterhalten" 
* #d350 ^property[2].code = #parent 
* #d350 ^property[2].valueCode = #d350-d369 
* #d350 ^property[3].code = #child 
* #d350 ^property[3].valueCode = #d3500 
* #d350 ^property[4].code = #child 
* #d350 ^property[4].valueCode = #d3501 
* #d350 ^property[5].code = #child 
* #d350 ^property[5].valueCode = #d3502 
* #d350 ^property[6].code = #child 
* #d350 ^property[6].valueCode = #d3503 
* #d350 ^property[7].code = #child 
* #d350 ^property[7].valueCode = #d3504 
* #d350 ^property[8].code = #child 
* #d350 ^property[8].valueCode = #d3508 
* #d350 ^property[9].code = #child 
* #d350 ^property[9].valueCode = #d3509 
* #d3500 "Eine Unterhaltung beginnen"
* #d3500 ^property[0].code = #None 
* #d3500 ^property[0].valueString = "Einen Dialog oder einen Gedankenaustausch zu eröffnen, wie sich selbst vorzustellen, die üblichen Grußformeln auszudrücken und in ein Thema einzuführen oder eine Frage zu stellen" 
* #d3500 ^property[1].code = #parent 
* #d3500 ^property[1].valueCode = #d350 
* #d3501 "Eine Unterhaltung aufrecht erhalten"
* #d3501 ^property[0].code = #None 
* #d3501 ^property[0].valueString = "Einen Dialog oder Gedankenaustausch durch zusätzliche Gedanken, Einführung eines neuen Themas oder Wiederaufnahme eines vorangegangenen Themas sowie durch abwechselndes Sprechen oder Geben von Zeichen fortzusetzen und zu gestalten" 
* #d3501 ^property[1].code = #parent 
* #d3501 ^property[1].valueCode = #d350 
* #d3502 "Eine Unterhaltung beenden"
* #d3502 ^property[0].code = #None 
* #d3502 ^property[0].valueString = "Einen Dialog oder einen Gedankenaustausch mit den üblichen abschließenden Äußerungen oder Bemerkungen und durch Abschluss des gegenwärtigen Themas zu beenden" 
* #d3502 ^property[1].code = #parent 
* #d3502 ^property[1].valueCode = #d350 
* #d3503 "Sich mit einer Person unterhalten"
* #d3503 ^property[0].code = #None 
* #d3503 ^property[0].valueString = "Mit einer Person einen Dialog oder einen Gedankenaustausch zu initiieren, aufrecht zu erhalten, zu gestalten und zu beenden, wie mit einem Freund über das Wetter zu sprechen" 
* #d3503 ^property[1].code = #parent 
* #d3503 ^property[1].valueCode = #d350 
* #d3504 "Eine Unterhaltung mit mehreren Personen führen"
* #d3504 ^property[0].code = #None 
* #d3504 ^property[0].valueString = "Mit mehr als einer Person einen Dialog oder einen Gedankenaustausch zu initiieren, aufrecht zu erhalten, zu gestalten und zu beenden, wie eine Gruppenunterhaltung zu beginnen und sich daran beteiligen" 
* #d3504 ^property[1].code = #parent 
* #d3504 ^property[1].valueCode = #d350 
* #d3508 "Konversation, anders bezeichnet"
* #d3508 ^property[0].code = #parent 
* #d3508 ^property[0].valueCode = #d350 
* #d3509 "Konversation, nicht näher bezeichnet"
* #d3509 ^property[0].code = #parent 
* #d3509 ^property[0].valueCode = #d350 
* #d355 "Diskussion"
* #d355 ^property[0].code = #None 
* #d355 ^property[0].valueString = "Eine Erörterung eines Sachverhaltes mit Pro- und Kontra-Argumenten oder eine Debatte in mündlicher oder schriftlicher Form, in Gebärdensprache oder auf andere sprachliche Weise zu beginnen, aufrecht zu erhalten und zu beenden, mit einer oder mehreren Personen, Bekannten oder Fremden, in formeller oder informeller Form" 
* #d355 ^property[1].code = #None 
* #d355 ^property[1].valueString = "Diskussion mit einer oder vielen Personen" 
* #d355 ^property[2].code = #parent 
* #d355 ^property[2].valueCode = #d350-d369 
* #d355 ^property[3].code = #child 
* #d355 ^property[3].valueCode = #d3550 
* #d355 ^property[4].code = #child 
* #d355 ^property[4].valueCode = #d3551 
* #d355 ^property[5].code = #child 
* #d355 ^property[5].valueCode = #d3558 
* #d355 ^property[6].code = #child 
* #d355 ^property[6].valueCode = #d3559 
* #d3550 "Diskussion mit einer Person"
* #d3550 ^property[0].code = #None 
* #d3550 ^property[0].valueString = "Mit einer Person eine Auseinandersetzung oder Debatte zu initiieren, aufrecht zu erhalten, zu gestalten und zu beenden" 
* #d3550 ^property[1].code = #parent 
* #d3550 ^property[1].valueCode = #d355 
* #d3551 "Diskussion mit vielen Menschen"
* #d3551 ^property[0].code = #None 
* #d3551 ^property[0].valueString = "Mit mehr als einer Person eine Auseinandersetzung oder Debatte zu initiieren, aufrecht zu erhalten, zu gestalten und zu beenden" 
* #d3551 ^property[1].code = #parent 
* #d3551 ^property[1].valueCode = #d355 
* #d3558 "Diskussion, anders bezeichnet"
* #d3558 ^property[0].code = #parent 
* #d3558 ^property[0].valueCode = #d355 
* #d3559 "Diskussion, nicht näher bezeichnet"
* #d3559 ^property[0].code = #parent 
* #d3559 ^property[0].valueCode = #d355 
* #d360 "Kommunikationsgeräte und -techniken benutzen"
* #d360 ^property[0].code = #None 
* #d360 ^property[0].valueString = "Kommunikationsgeräte, -techniken und andere Kommunikationsmittel verwenden, wie einen Freund per Telefon anrufen" 
* #d360 ^property[1].code = #None 
* #d360 ^property[1].valueString = "Telekommunikationsgeräte, Schreibmaschinen und Kommunikationstechniken verwenden" 
* #d360 ^property[2].code = #parent 
* #d360 ^property[2].valueCode = #d350-d369 
* #d360 ^property[3].code = #child 
* #d360 ^property[3].valueCode = #d3600 
* #d360 ^property[4].code = #child 
* #d360 ^property[4].valueCode = #d3601 
* #d360 ^property[5].code = #child 
* #d360 ^property[5].valueCode = #d3602 
* #d360 ^property[6].code = #child 
* #d360 ^property[6].valueCode = #d3608 
* #d360 ^property[7].code = #child 
* #d360 ^property[7].valueCode = #d3609 
* #d3600 "Telekommunikationsgeräte benutzen"
* #d3600 ^property[0].code = #None 
* #d3600 ^property[0].valueString = "Ein Telefon und andere Geräte wie Fax- oder Telex-Geräte als Kommunikationsmittel zu verwenden" 
* #d3600 ^property[1].code = #parent 
* #d3600 ^property[1].valueCode = #d360 
* #d3601 "Technische Schreibgeräte benutzen"
* #d3601 ^property[0].code = #None 
* #d3601 ^property[0].valueString = "Maschinen zum Schreiben wie Schreibmaschinen, Computer und Brailleschreiber als Kommunikationsmittel verwenden" 
* #d3601 ^property[1].code = #parent 
* #d3601 ^property[1].valueCode = #d360 
* #d3602 "Kommunikationsmethoden benutzen"
* #d3602 ^property[0].code = #None 
* #d3602 ^property[0].valueString = "Handlungen und Aufgaben, die bei Techniken der Kommunikation wie Lippenlesen beteiligt sind, verwenden" 
* #d3602 ^property[1].code = #parent 
* #d3602 ^property[1].valueCode = #d360 
* #d3608 "Kommunikationsgeräte und -techniken benutzen, anders bezeichnet"
* #d3608 ^property[0].code = #parent 
* #d3608 ^property[0].valueCode = #d360 
* #d3609 "Kommunikationsgeräte und -techniken benutzen, nicht näher bezeichnet"
* #d3609 ^property[0].code = #parent 
* #d3609 ^property[0].valueCode = #d360 
* #d369 "Konversation und Gebrauch von Kommunikationsgeräten und -techniken, anders oder nicht näher bezeichnet"
* #d369 ^property[0].code = #parent 
* #d369 ^property[0].valueCode = #d350-d369 
* #d398 "Kommunikation, anders bezeichnet"
* #d398 ^property[0].code = #parent 
* #d398 ^property[0].valueCode = #d3 
* #d399 "Kommunikation, nicht näher bezeichnet"
* #d399 ^property[0].code = #parent 
* #d399 ^property[0].valueCode = #d3 
* #d4 "Mobilität"
* #d4 ^property[0].code = #None 
* #d4 ^property[0].valueString = "Dieses Kapitel befasst sich mit der eigenen Bewegung durch Änderung der Körperposition oder -lage oder Verlagerung von einem Platz zu einem anderen, mit der Bewegung von Gegenständen durch Tragen, Bewegen oder Handhaben, mit der Fortbewegung durch Gehen, Rennen, Klettern oder Steigen sowie durch den Gebrauch verschiedener Transportmittel." 
* #d4 ^property[1].code = #parent 
* #d4 ^property[1].valueCode = #d 
* #d4 ^property[2].code = #child 
* #d4 ^property[2].valueCode = #d410-d429 
* #d4 ^property[3].code = #child 
* #d4 ^property[3].valueCode = #d430-d449 
* #d4 ^property[4].code = #child 
* #d4 ^property[4].valueCode = #d450-d469 
* #d4 ^property[5].code = #child 
* #d4 ^property[5].valueCode = #d470-d489 
* #d4 ^property[6].code = #child 
* #d4 ^property[6].valueCode = #d498 
* #d4 ^property[7].code = #child 
* #d4 ^property[7].valueCode = #d499 
* #d410-d429 "Die Körperposition ändern und aufrecht erhalten"
* #d410-d429 ^property[0].code = #parent 
* #d410-d429 ^property[0].valueCode = #d4 
* #d410-d429 ^property[1].code = #child 
* #d410-d429 ^property[1].valueCode = #d410 
* #d410-d429 ^property[2].code = #child 
* #d410-d429 ^property[2].valueCode = #d415 
* #d410-d429 ^property[3].code = #child 
* #d410-d429 ^property[3].valueCode = #d420 
* #d410-d429 ^property[4].code = #child 
* #d410-d429 ^property[4].valueCode = #d429 
* #d410 "Eine elementare Körperposition wechseln"
* #d410 ^property[0].code = #None 
* #d410 ^property[0].valueString = "In eine und aus einer Körperposition zu gelangen und sich von einem Ort zu einem anderen zu bewegen, wie von einem Stuhl aufstehen, um sich in ein Bett zu legen, in eine und aus einer knienden oder hockenden Position gelangen" 
* #d410 ^property[1].code = #None 
* #d410 ^property[1].valueString = "Seine Körperposition aus einer liegenden, knienden oder hockenden, sitzenden oder stehenden Position ändern, sich beugen und seinen Körperschwerpunkt verlagern" 
* #d410 ^property[2].code = #None 
* #d410 ^property[2].valueString = "Sich verlagern" 
* #d410 ^property[3].code = #parent 
* #d410 ^property[3].valueCode = #d410-d429 
* #d410 ^property[4].code = #child 
* #d410 ^property[4].valueCode = #d4100 
* #d410 ^property[5].code = #child 
* #d410 ^property[5].valueCode = #d4101 
* #d410 ^property[6].code = #child 
* #d410 ^property[6].valueCode = #d4102 
* #d410 ^property[7].code = #child 
* #d410 ^property[7].valueCode = #d4103 
* #d410 ^property[8].code = #child 
* #d410 ^property[8].valueCode = #d4104 
* #d410 ^property[9].code = #child 
* #d410 ^property[9].valueCode = #d4105 
* #d410 ^property[10].code = #child 
* #d410 ^property[10].valueCode = #d4106 
* #d410 ^property[11].code = #child 
* #d410 ^property[11].valueCode = #d4108 
* #d410 ^property[12].code = #child 
* #d410 ^property[12].valueCode = #d4109 
* #d4100 "Sich hinlegen"
* #d4100 ^property[0].code = #None 
* #d4100 ^property[0].valueString = "In oder aus einer liegenden Position zu gelangen oder die Körperposition von einer waagerechten in jede andere Position zu wechseln, wie aufstehen oder sich hinsetzen" 
* #d4100 ^property[1].code = #None 
* #d4100 ^property[1].valueString = "In eine ausgestreckte Position gelangen" 
* #d4100 ^property[2].code = #parent 
* #d4100 ^property[2].valueCode = #d410 
* #d4101 "Hocken"
* #d4101 ^property[0].code = #None 
* #d4101 ^property[0].valueString = "In eine oder aus einer mit angezogenen Knien auf dem Hinterteil oder den Fersen sitzenden oder kauernden Stellung zu gelangen, wie es bei ebenerdigen Toiletten notwendig sein kann, oder die Körperposition aus einer hockenden in jede andere Position zu wechseln, wie aufstehen" 
* #d4101 ^property[1].code = #parent 
* #d4101 ^property[1].valueCode = #d410 
* #d4102 "Knien"
* #d4102 ^property[0].code = #None 
* #d4102 ^property[0].valueString = "In oder aus einer Position zu gelangen, bei der der Körper durch die Knie bei gebeugten Beinen unterstützt wird, wie während des Betens, oder die Körperposition von einer knienden in jede andere Position zu wechseln, wie aufstehen" 
* #d4102 ^property[1].code = #parent 
* #d4102 ^property[1].valueCode = #d410 
* #d4103 "Sitzen"
* #d4103 ^property[0].code = #None 
* #d4103 ^property[0].valueString = "In oder aus einer sitzenden Position zu gelangen oder die Körperposition von einer sitzenden in jede andere Position zu wechseln, wie aufstehen oder sich hinlegen" 
* #d4103 ^property[1].code = #None 
* #d4103 ^property[1].valueString = "In eine sitzende Position mit gebeugten oder übergeschlagenen Beinen gelangen; in eine sitzende Position mit oder ohne Unterstützung der Füße gelangen" 
* #d4103 ^property[2].code = #parent 
* #d4103 ^property[2].valueCode = #d410 
* #d4104 "Stehen"
* #d4104 ^property[0].code = #None 
* #d4104 ^property[0].valueString = "In oder aus einer stehenden Position zu gelangen oder die Körperposition von einer stehenden in jede andere Position zu wechseln, wie sich hinlegen oder hinsetzen" 
* #d4104 ^property[1].code = #parent 
* #d4104 ^property[1].valueCode = #d410 
* #d4105 "Sich beugen"
* #d4105 ^property[0].code = #None 
* #d4105 ^property[0].valueString = "Den Rücken nach unten oder zur Seite zu beugen, wie beim Verbeugen oder beim Langen nach einem Gegenstand" 
* #d4105 ^property[1].code = #parent 
* #d4105 ^property[1].valueCode = #d410 
* #d4106 "Seinen Körperschwerpunkt verlagern"
* #d4106 ^property[0].code = #None 
* #d4106 ^property[0].valueString = "Während des Sitzens, Stehens oder Liegens sein Körpergewicht von einer Position zu einer anderen zu verlagern oder zu bewegen, wie während des Stehens sein Standbein wechseln" 
* #d4106 ^property[1].code = #None 
* #d4106 ^property[1].valueString = "Sich verlagern" 
* #d4106 ^property[2].code = #parent 
* #d4106 ^property[2].valueCode = #d410 
* #d4108 "Eine elementare Körperposition wechseln, anders bezeichnet"
* #d4108 ^property[0].code = #parent 
* #d4108 ^property[0].valueCode = #d410 
* #d4109 "Eine elementare Körperposition wechseln, nicht näher bezeichnet"
* #d4109 ^property[0].code = #parent 
* #d4109 ^property[0].valueCode = #d410 
* #d415 "In einer Körperposition verbleiben"
* #d415 ^property[0].code = #None 
* #d415 ^property[0].valueString = "In derselben erforderlichen Körperposition zu verbleiben, wie sitzen bleiben oder bei der Arbeit bzw. in der Schule stehen bleiben" 
* #d415 ^property[1].code = #None 
* #d415 ^property[1].valueString = "In liegender, hockender, kniender, sitzender oder stehender Position verbleiben" 
* #d415 ^property[2].code = #parent 
* #d415 ^property[2].valueCode = #d410-d429 
* #d415 ^property[3].code = #child 
* #d415 ^property[3].valueCode = #d4150 
* #d415 ^property[4].code = #child 
* #d415 ^property[4].valueCode = #d4151 
* #d415 ^property[5].code = #child 
* #d415 ^property[5].valueCode = #d4152 
* #d415 ^property[6].code = #child 
* #d415 ^property[6].valueCode = #d4153 
* #d415 ^property[7].code = #child 
* #d415 ^property[7].valueCode = #d4154 
* #d415 ^property[8].code = #child 
* #d415 ^property[8].valueCode = #d4158 
* #d415 ^property[9].code = #child 
* #d415 ^property[9].valueCode = #d4159 
* #d4150 "In liegender Position verbleiben"
* #d4150 ^property[0].code = #None 
* #d4150 ^property[0].valueString = "Für eine erforderliche Zeit in einer liegenden Position zu verbleiben, wie im Bett auf dem Bauch liegen" 
* #d4150 ^property[1].code = #None 
* #d4150 ^property[1].valueString = "In Bauchlage (Gesicht unten), in Rückenlage (Gesicht oben) oder in Seitenlage verbleiben" 
* #d4150 ^property[2].code = #parent 
* #d4150 ^property[2].valueCode = #d415 
* #d4151 "In hockender Position verbleiben"
* #d4151 ^property[0].code = #None 
* #d4151 ^property[0].valueString = "Für eine erforderliche Zeit in einer hockenden Position zu verbleiben, wie ohne Sitzgelegenheit auf dem Boden sitzen" 
* #d4151 ^property[1].code = #parent 
* #d4151 ^property[1].valueCode = #d415 
* #d4152 "In kniender Position verbleiben"
* #d4152 ^property[0].code = #None 
* #d4152 ^property[0].valueString = "Für eine erforderliche Zeit in einer knienden Position zu verbleiben, wobei der Körper durch die Knie bei gebeugten Beinen unterstützt wird, wie während des Betens in einer Kirche" 
* #d4152 ^property[1].code = #parent 
* #d4152 ^property[1].valueCode = #d415 
* #d4153 "In sitzender Position verbleiben"
* #d4153 ^property[0].code = #None 
* #d4153 ^property[0].valueString = "Für eine erforderliche Zeit in einer sitzenden Position auf einer Sitzgelegenheit oder dem Boden zu verbleiben, wie an einem Pult oder Tisch sitzen" 
* #d4153 ^property[1].code = #None 
* #d4153 ^property[1].valueString = "Mit ausgestreckten oder übergeschlagenen Beinen mit oder ohne Unterstützung der Füße in einer sitzenden Position verbleiben" 
* #d4153 ^property[2].code = #parent 
* #d4153 ^property[2].valueCode = #d415 
* #d4154 "In stehender Position verbleiben"
* #d4154 ^property[0].code = #None 
* #d4154 ^property[0].valueString = "Für eine erforderliche Zeit in einer stehenden Position zu verbleiben, wie in einer Schlange stehen" 
* #d4154 ^property[1].code = #None 
* #d4154 ^property[1].valueString = "Auf einer geneigten, rutschigen oder harten Oberfläche in stehender Position verbleiben" 
* #d4154 ^property[2].code = #parent 
* #d4154 ^property[2].valueCode = #d415 
* #d4158 "In einer Körperposition verbleiben, anders bezeichnet"
* #d4158 ^property[0].code = #parent 
* #d4158 ^property[0].valueCode = #d415 
* #d4159 "In einer Körperposition verbleiben, nicht näher bezeichnet"
* #d4159 ^property[0].code = #parent 
* #d4159 ^property[0].valueCode = #d415 
* #d420 "Sich verlagern"
* #d420 ^property[0].code = #None 
* #d420 ^property[0].valueString = "Sich von einer Oberfläche auf eine andere zu bewegen, wie auf einer Bank entlang gleiten oder sich ohne Änderung der Körperposition aus dem Bett auf einen Stuhl bewegen" 
* #d420 ^property[1].code = #None 
* #d420 ^property[1].valueString = "Sich während des Sitzens oder Liegens verlagern" 
* #d420 ^property[2].code = #None 
* #d420 ^property[2].valueString = "Eine elementare Körperposition wechseln" 
* #d420 ^property[3].code = #parent 
* #d420 ^property[3].valueCode = #d410-d429 
* #d420 ^property[4].code = #child 
* #d420 ^property[4].valueCode = #d4200 
* #d420 ^property[5].code = #child 
* #d420 ^property[5].valueCode = #d4201 
* #d420 ^property[6].code = #child 
* #d420 ^property[6].valueCode = #d4208 
* #d420 ^property[7].code = #child 
* #d420 ^property[7].valueCode = #d4209 
* #d4200 "Sich beim Sitzen verlagern"
* #d4200 ^property[0].code = #None 
* #d4200 ^property[0].valueString = "Sich aus sitzender Position auf einer Sitzgelegenheit auf eine andere gleichen oder unterschiedlichen Niveaus zu bewegen, wie sich von einem Stuhl in ein Bett bewegen" 
* #d4200 ^property[1].code = #None 
* #d4200 ^property[1].valueString = "Sich von einem Stuhl auf eine andere Sitzgelegenheit zu bewegen, wie auf einen Toilettensitz; sich von einem Rollstuhl auf einen Autositz bewegen" 
* #d4200 ^property[2].code = #None 
* #d4200 ^property[2].valueString = "Eine elementare Körperposition wechseln" 
* #d4200 ^property[3].code = #parent 
* #d4200 ^property[3].valueCode = #d420 
* #d4201 "Sich beim Liegen verlagern"
* #d4201 ^property[0].code = #None 
* #d4201 ^property[0].valueString = "Sich aus liegender Position in eine andere gleichen oder unterschiedlichen Niveaus zu bewegen, wie sich von einem Bett in ein anderes bewegen" 
* #d4201 ^property[1].code = #None 
* #d4201 ^property[1].valueString = "Eine elementare Körperposition wechseln" 
* #d4201 ^property[2].code = #parent 
* #d4201 ^property[2].valueCode = #d420 
* #d4208 "Sich verlagern, anders bezeichnet"
* #d4208 ^property[0].code = #parent 
* #d4208 ^property[0].valueCode = #d420 
* #d4209 "Sich verlagern, nicht näher bezeichnet"
* #d4209 ^property[0].code = #parent 
* #d4209 ^property[0].valueCode = #d420 
* #d429 "Die Körperposition ändern und aufrecht erhalten, anders oder nicht näher bezeichnet"
* #d429 ^property[0].code = #parent 
* #d429 ^property[0].valueCode = #d410-d429 
* #d430-d449 "Gegenstände tragen, bewegen und handhaben"
* #d430-d449 ^property[0].code = #parent 
* #d430-d449 ^property[0].valueCode = #d4 
* #d430-d449 ^property[1].code = #child 
* #d430-d449 ^property[1].valueCode = #d430 
* #d430-d449 ^property[2].code = #child 
* #d430-d449 ^property[2].valueCode = #d435 
* #d430-d449 ^property[3].code = #child 
* #d430-d449 ^property[3].valueCode = #d440 
* #d430-d449 ^property[4].code = #child 
* #d430-d449 ^property[4].valueCode = #d445 
* #d430-d449 ^property[5].code = #child 
* #d430-d449 ^property[5].valueCode = #d449 
* #d430 "Gegenstände anheben und tragen"
* #d430 ^property[0].code = #None 
* #d430 ^property[0].valueString = "Einen Gegenstand anzuheben oder etwas von einem Platz zu einem anderen zu tragen, wie eine Tasse anheben oder ein Kind von einem Zimmer in ein anderes tragen" 
* #d430 ^property[1].code = #None 
* #d430 ^property[1].valueString = "Mit den Händen, Armen, auf den Schultern, dem Kopf, dem Rücken oder der Hüfte anheben und absetzen" 
* #d430 ^property[2].code = #parent 
* #d430 ^property[2].valueCode = #d430-d449 
* #d430 ^property[3].code = #child 
* #d430 ^property[3].valueCode = #d4300 
* #d430 ^property[4].code = #child 
* #d430 ^property[4].valueCode = #d4301 
* #d430 ^property[5].code = #child 
* #d430 ^property[5].valueCode = #d4302 
* #d430 ^property[6].code = #child 
* #d430 ^property[6].valueCode = #d4303 
* #d430 ^property[7].code = #child 
* #d430 ^property[7].valueCode = #d4304 
* #d430 ^property[8].code = #child 
* #d430 ^property[8].valueCode = #d4305 
* #d430 ^property[9].code = #child 
* #d430 ^property[9].valueCode = #d4308 
* #d430 ^property[10].code = #child 
* #d430 ^property[10].valueCode = #d4309 
* #d4300 "Anheben"
* #d4300 ^property[0].code = #None 
* #d4300 ^property[0].valueString = "Einen Gegenstand anheben, um ihn von einem niedrigen Niveau auf ein höheres zu bewegen, wie ein Glas auf einem Tisch anheben" 
* #d4300 ^property[1].code = #parent 
* #d4300 ^property[1].valueCode = #d430 
* #d4301 "Mit den Händen tragen"
* #d4301 ^property[0].code = #None 
* #d4301 ^property[0].valueString = "Einen Gegenstand mit den Händen von einem Platz an einen anderen zu tragen oder zu transportieren, wie ein Trinkglas oder einen Koffer tragen" 
* #d4301 ^property[1].code = #parent 
* #d4301 ^property[1].valueCode = #d430 
* #d4302 "Mit den Armen tragen"
* #d4302 ^property[0].code = #None 
* #d4302 ^property[0].valueString = "Einen Gegenstand mit den Händen und Armen von einem Platz an einen anderen zu tragen oder zu transportieren, wie ein Kind tragen" 
* #d4302 ^property[1].code = #parent 
* #d4302 ^property[1].valueCode = #d430 
* #d4303 "Auf den Schultern, der Hüfte oder dem Rücken tragen"
* #d4303 ^property[0].code = #None 
* #d4303 ^property[0].valueString = "Einen Gegenstand auf Schultern, Hüfte oder Rücken oder in deren Kombination von einem Platz an einen anderen zu tragen oder zu transportieren, wie beim Tragen eines großen Paketes" 
* #d4303 ^property[1].code = #parent 
* #d4303 ^property[1].valueCode = #d430 
* #d4304 "Auf dem Kopf tragen"
* #d4304 ^property[0].code = #None 
* #d4304 ^property[0].valueString = "Einen Gegenstand auf dem Kopf von einem Platz an einen anderen zu tragen oder zu transportieren, wie ein Wassergefäß auf dem Kopf tragen" 
* #d4304 ^property[1].code = #parent 
* #d4304 ^property[1].valueCode = #d430 
* #d4305 "Gegenstände absetzen"
* #d4305 ^property[0].code = #None 
* #d4305 ^property[0].valueString = "Mit Händen, Armen oder anderen Körperteilen einen Gegenstand auf dem Boden an einem Platz absetzen, wie ein Wassergefäß auf dem Boden absetzen" 
* #d4305 ^property[1].code = #parent 
* #d4305 ^property[1].valueCode = #d430 
* #d4308 "Gegenstände anheben und tragen, anders bezeichnet"
* #d4308 ^property[0].code = #parent 
* #d4308 ^property[0].valueCode = #d430 
* #d4309 "Gegenstände anheben und tragen, nicht näher bezeichnet"
* #d4309 ^property[0].code = #parent 
* #d4309 ^property[0].valueCode = #d430 
* #d435 "Gegenstände mit den unteren Extremitäten bewegen"
* #d435 ^property[0].code = #None 
* #d435 ^property[0].valueString = "Koordinierte Handlungen mit dem Ziel auszuführen, einen Gegenstand mit Beinen und Füßen in Bewegung zu versetzen, wie einem Ball einen Tritt versetzen oder die Pedale eines Fahrrades treten" 
* #d435 ^property[1].code = #None 
* #d435 ^property[1].valueString = "Mit den unteren Extremitäten stoßen; treten" 
* #d435 ^property[2].code = #parent 
* #d435 ^property[2].valueCode = #d430-d449 
* #d435 ^property[3].code = #child 
* #d435 ^property[3].valueCode = #d4350 
* #d435 ^property[4].code = #child 
* #d435 ^property[4].valueCode = #d4351 
* #d435 ^property[5].code = #child 
* #d435 ^property[5].valueCode = #d4358 
* #d435 ^property[6].code = #child 
* #d435 ^property[6].valueCode = #d4359 
* #d4350 "Mit den unteren Extremitäten schieben"
* #d4350 ^property[0].code = #None 
* #d4350 ^property[0].valueString = "Mit Beinen und Füßen eine Kraft auf einen Gegenstand auszuüben, damit er sich wegbewegt, wie einen Stuhl mit dem Fuß wegschieben" 
* #d4350 ^property[1].code = #parent 
* #d4350 ^property[1].valueCode = #d435 
* #d4351 "Stoßen"
* #d4351 ^property[0].code = #None 
* #d4351 ^property[0].valueString = "Mit Beinen und Füßen einen Gegenstand wegstoßen, wie einen Fußball wegstoßen" 
* #d4351 ^property[1].code = #parent 
* #d4351 ^property[1].valueCode = #d435 
* #d4358 "Gegenstände mit den unteren Extremitäten bewegen, anders bezeichnet"
* #d4358 ^property[0].code = #parent 
* #d4358 ^property[0].valueCode = #d435 
* #d4359 "Gegenstände mit den unteren Extremitäten bewegen, nicht näher bezeichnet"
* #d4359 ^property[0].code = #parent 
* #d4359 ^property[0].valueCode = #d435 
* #d440 "Feinmotorischer Handgebrauch"
* #d440 ^property[0].code = #None 
* #d440 ^property[0].valueString = "Koordinierte Handlungen mit dem Ziel auszuführen, Gegenstände mit der Hand, den Fingern und dem Daumen aufzunehmen, zu handhaben und loszulassen, wie es für das Aufnehmen von Münzen von einem Tisch, für das Drehen einer Wählscheibe oder eines Knaufes erforderlich ist" 
* #d440 ^property[1].code = #None 
* #d440 ^property[1].valueString = "aufnehmen, ergreifen, handhaben, loslassen" 
* #d440 ^property[2].code = #None 
* #d440 ^property[2].valueString = "Gegenstände anheben und tragen" 
* #d440 ^property[3].code = #parent 
* #d440 ^property[3].valueCode = #d430-d449 
* #d440 ^property[4].code = #child 
* #d440 ^property[4].valueCode = #d4400 
* #d440 ^property[5].code = #child 
* #d440 ^property[5].valueCode = #d4401 
* #d440 ^property[6].code = #child 
* #d440 ^property[6].valueCode = #d4402 
* #d440 ^property[7].code = #child 
* #d440 ^property[7].valueCode = #d4403 
* #d440 ^property[8].code = #child 
* #d440 ^property[8].valueCode = #d4408 
* #d440 ^property[9].code = #child 
* #d440 ^property[9].valueCode = #d4409 
* #d4400 "Einen Gegenstand aufnehmen"
* #d4400 ^property[0].code = #None 
* #d4400 ^property[0].valueString = "Einen kleinen Gegenstand mit den Händen und Fingern aufnehmen oder aufheben, wie einen Bleistift aufnehmen" 
* #d4400 ^property[1].code = #parent 
* #d4400 ^property[1].valueCode = #d440 
* #d4401 "Einen Gegenstand ergreifen"
* #d4401 ^property[0].code = #None 
* #d4401 ^property[0].valueString = "Mit beiden Händen etwas ergreifen und halten, wie ein Werkzeug oder einen Türknauf ergreifen" 
* #d4401 ^property[1].code = #parent 
* #d4401 ^property[1].valueCode = #d440 
* #d4402 "Einen Gegenstand handhaben"
* #d4402 ^property[0].code = #None 
* #d4402 ^property[0].valueString = "Mit Fingern und Händen die Kontrolle über etwas auszuüben, es zu dirigieren oder zu führen, wie mit Münzen oder anderen kleinen Gegenständen hantieren" 
* #d4402 ^property[1].code = #parent 
* #d4402 ^property[1].valueCode = #d440 
* #d4403 "Einen Gegenstand loslassen"
* #d4403 ^property[0].code = #None 
* #d4403 ^property[0].valueString = "Mit Fingern und Händen etwas loslassen oder freigeben, so dass es fällt oder die Position ändert, wie ein Kleidungsstück fallen lassen" 
* #d4403 ^property[1].code = #parent 
* #d4403 ^property[1].valueCode = #d440 
* #d4408 "Feinmotorischer Handgebrauch, anders bezeichnet"
* #d4408 ^property[0].code = #parent 
* #d4408 ^property[0].valueCode = #d440 
* #d4409 "Feinmotorischer Handgebrauch, nicht näher bezeichnet"
* #d4409 ^property[0].code = #parent 
* #d4409 ^property[0].valueCode = #d440 
* #d445 "Hand- und Armgebrauch"
* #d445 ^property[0].code = #None 
* #d445 ^property[0].valueString = "Koordinierte Handlungen auszuführen, die erforderlich sind, Gegenstände mit Händen und Armen zu bewegen oder zu handhaben, wie beim Drehen eines Türgriffs oder dem Werfen oder Fangen eines Gegenstands" 
* #d445 ^property[1].code = #None 
* #d445 ^property[1].valueString = "Gegenstände ziehen oder schieben; nach etwas langen; Hände oder Arme drehen oder verdrehen; werfen; fangen" 
* #d445 ^property[2].code = #None 
* #d445 ^property[2].valueString = "Feinmotorischer Handgebrauch" 
* #d445 ^property[3].code = #parent 
* #d445 ^property[3].valueCode = #d430-d449 
* #d445 ^property[4].code = #child 
* #d445 ^property[4].valueCode = #d4450 
* #d445 ^property[5].code = #child 
* #d445 ^property[5].valueCode = #d4451 
* #d445 ^property[6].code = #child 
* #d445 ^property[6].valueCode = #d4452 
* #d445 ^property[7].code = #child 
* #d445 ^property[7].valueCode = #d4453 
* #d445 ^property[8].code = #child 
* #d445 ^property[8].valueCode = #d4454 
* #d445 ^property[9].code = #child 
* #d445 ^property[9].valueCode = #d4455 
* #d445 ^property[10].code = #child 
* #d445 ^property[10].valueCode = #d4458 
* #d445 ^property[11].code = #child 
* #d445 ^property[11].valueCode = #d4459 
* #d4450 "Ziehen"
* #d4450 ^property[0].code = #None 
* #d4450 ^property[0].valueString = "Einen Gegenstand mit Fingern, Händen und Armen zu sich hinzubringen oder ihn von einem Platz zu einem anderen zu bewegen, wie eine Tür zuziehen" 
* #d4450 ^property[1].code = #parent 
* #d4450 ^property[1].valueCode = #d445 
* #d4451 "Schieben"
* #d4451 ^property[0].code = #None 
* #d4451 ^property[0].valueString = "Einen Gegenstand mit Fingern, Händen und Armen von sich wegzubringen oder ihn von einem Platz zu einem anderen zu bewegen, wie ein Tier wegschubsen" 
* #d4451 ^property[1].code = #parent 
* #d4451 ^property[1].valueCode = #d445 
* #d4452 "Nach etwas langen"
* #d4452 ^property[0].code = #None 
* #d4452 ^property[0].valueString = "Hände und Arme ausstrecken, um etwas zu berühren und zu greifen, wie über einen Tisch oder Pult nach einem Buch langen" 
* #d4452 ^property[1].code = #parent 
* #d4452 ^property[1].valueCode = #d445 
* #d4453 "Hände oder Arme drehen oder verdrehen"
* #d4453 ^property[0].code = #None 
* #d4453 ^property[0].valueString = "Einen Gegenstand mit Fingern, Händen und Armen in Rotation zu versetzen, zu drehen oder zu wenden, wie es für den Gebrauch von Werkzeugen oder Küchenutensilien erforderlich ist" 
* #d4453 ^property[1].code = #parent 
* #d4453 ^property[1].valueCode = #d445 
* #d4454 "Werfen"
* #d4454 ^property[0].code = #None 
* #d4454 ^property[0].valueString = "Etwas mit Fingern, Händen und Armen aufzunehmen und es mit einiger Kraft durch die Luft zu schleudern, wie einen Ball hochwerfen" 
* #d4454 ^property[1].code = #parent 
* #d4454 ^property[1].valueCode = #d445 
* #d4455 "Fangen"
* #d4455 ^property[0].code = #None 
* #d4455 ^property[0].valueString = "Einen bewegten Gegenstand mit Fingern, Händen und Armen zu ergreifen, um ihn zu stoppen und zu halten, wie einen Ball fangen" 
* #d4455 ^property[1].code = #parent 
* #d4455 ^property[1].valueCode = #d445 
* #d4458 "Hand- und Armgebrauch, anders bezeichnet"
* #d4458 ^property[0].code = #parent 
* #d4458 ^property[0].valueCode = #d445 
* #d4459 "Hand- und Armgebrauch, nicht näher bezeichnet"
* #d4459 ^property[0].code = #parent 
* #d4459 ^property[0].valueCode = #d445 
* #d449 "Gegenstände tragen, bewegen und handhaben, anders oder nicht näher bezeichnet"
* #d449 ^property[0].code = #parent 
* #d449 ^property[0].valueCode = #d430-d449 
* #d450-d469 "Gehen und sich fortbewegen"
* #d450-d469 ^property[0].code = #parent 
* #d450-d469 ^property[0].valueCode = #d4 
* #d450-d469 ^property[1].code = #child 
* #d450-d469 ^property[1].valueCode = #d450 
* #d450-d469 ^property[2].code = #child 
* #d450-d469 ^property[2].valueCode = #d455 
* #d450-d469 ^property[3].code = #child 
* #d450-d469 ^property[3].valueCode = #d460 
* #d450-d469 ^property[4].code = #child 
* #d450-d469 ^property[4].valueCode = #d465 
* #d450-d469 ^property[5].code = #child 
* #d450-d469 ^property[5].valueCode = #d469 
* #d450 "Gehen"
* #d450 ^property[0].code = #None 
* #d450 ^property[0].valueString = "Sich zu Fuß auf einer Oberfläche Schritt für Schritt so fortzubewegen, dass stets wenigstens ein Fuß den Boden berührt, wie beim Spazieren, Schlendern, Vorwärts-, Rückwärts- oder Seitwärtsgehen" 
* #d450 ^property[1].code = #None 
* #d450 ^property[1].valueString = "Kurze oder weite Entfernungen gehen; auf unterschiedlichen Oberflächen gehen; Hindernisse umgehen" 
* #d450 ^property[2].code = #None 
* #d450 ^property[2].valueString = "Sich verlagern" 
* #d450 ^property[3].code = #parent 
* #d450 ^property[3].valueCode = #d450-d469 
* #d450 ^property[4].code = #child 
* #d450 ^property[4].valueCode = #d4500 
* #d450 ^property[5].code = #child 
* #d450 ^property[5].valueCode = #d4501 
* #d450 ^property[6].code = #child 
* #d450 ^property[6].valueCode = #d4502 
* #d450 ^property[7].code = #child 
* #d450 ^property[7].valueCode = #d4503 
* #d450 ^property[8].code = #child 
* #d450 ^property[8].valueCode = #d4508 
* #d450 ^property[9].code = #child 
* #d450 ^property[9].valueCode = #d4509 
* #d4500 "Kurze Entfernungen gehen"
* #d4500 ^property[0].code = #None 
* #d4500 ^property[0].valueString = "Weniger als einen Kilometer zu gehen, wie in Räumen umher oder auf Korridoren entlang gehen, innerhalb eines Gebäudes oder für kurze Entfernungen außerhalb" 
* #d4500 ^property[1].code = #parent 
* #d4500 ^property[1].valueCode = #d450 
* #d4501 "Lange Entfernungen gehen"
* #d4501 ^property[0].code = #None 
* #d4501 ^property[0].valueString = "Mehr als einen Kilometer zu gehen, wie durch ein Dorf oder eine Stadt, von einem Dorf zu einem anderen oder über Land gehen" 
* #d4501 ^property[1].code = #parent 
* #d4501 ^property[1].valueCode = #d450 
* #d4502 "Auf unterschiedlichen Oberflächen gehen"
* #d4502 ^property[0].code = #None 
* #d4502 ^property[0].valueString = "Auf ansteigenden oder abfallenden, unebenen oder sich bewegenden Oberflächen zu gehen, wie auf Gras, Kies, Eis oder Schnee gehen, oder auf einem Schiff, in einem Zug oder einem anderen Fahrzeug gehen" 
* #d4502 ^property[1].code = #parent 
* #d4502 ^property[1].valueCode = #d450 
* #d4503 "Hindernisse umgehen"
* #d4503 ^property[0].code = #None 
* #d4503 ^property[0].valueString = "In der Weise zu gehen, dass sich bewegenden oder festen Gegenstände, Menschen, Tieren und Fahrzeugen ausgewichen wird, wie auf einem Markt oder in einem Laden gehen, im Straßenverkehr gehen oder diesen umgehen oder in belebten Gegenden gehen" 
* #d4503 ^property[1].code = #parent 
* #d4503 ^property[1].valueCode = #d450 
* #d4508 "Gehen, anders bezeichnet"
* #d4508 ^property[0].code = #parent 
* #d4508 ^property[0].valueCode = #d450 
* #d4509 "Gehen, nicht näher bezeichnet"
* #d4509 ^property[0].code = #parent 
* #d4509 ^property[0].valueCode = #d450 
* #d455 "Sich auf andere Weise fortbewegen"
* #d455 ^property[0].code = #None 
* #d455 ^property[0].valueString = "Sich auf andere Weise als gehend von einem Ort zu einem anderen fortzubewegen, wie über einen Fels klettern oder eine Straße entlang rennen, springen, spurten, hüpfen, einen Purzelbaum schlagen oder um Hindernisse rennen" 
* #d455 ^property[1].code = #None 
* #d455 ^property[1].valueString = "Krabbeln/robben, klettern/steigen, rennen, joggen, springen und schwimmen" 
* #d455 ^property[2].code = #None 
* #d455 ^property[2].valueString = "Sich verlagern" 
* #d455 ^property[3].code = #parent 
* #d455 ^property[3].valueCode = #d450-d469 
* #d455 ^property[4].code = #child 
* #d455 ^property[4].valueCode = #d4550 
* #d455 ^property[5].code = #child 
* #d455 ^property[5].valueCode = #d4551 
* #d455 ^property[6].code = #child 
* #d455 ^property[6].valueCode = #d4552 
* #d455 ^property[7].code = #child 
* #d455 ^property[7].valueCode = #d4553 
* #d455 ^property[8].code = #child 
* #d455 ^property[8].valueCode = #d4554 
* #d455 ^property[9].code = #child 
* #d455 ^property[9].valueCode = #d4558 
* #d455 ^property[10].code = #child 
* #d455 ^property[10].valueCode = #d4559 
* #d4550 "Krabbeln/robben"
* #d4550 ^property[0].code = #None 
* #d4550 ^property[0].valueString = "Auf Händen oder Händen und Armen und Knien den ganzen Körper in Bauchlage von einem Platz zu einem anderen zu bewegen" 
* #d4550 ^property[1].code = #parent 
* #d4550 ^property[1].valueCode = #d455 
* #d4551 "Klettern/steigen"
* #d4551 ^property[0].code = #None 
* #d4551 ^property[0].valueString = "Den ganzen Körper über Oberflächen oder Objekte auf- oder abwärts zu bewegen, wie bei Stufen, Steinen/Felsen, Leitern, Treppen, Kantsteinen oder anderen Objekten" 
* #d4551 ^property[1].code = #parent 
* #d4551 ^property[1].valueCode = #d455 
* #d4552 "Rennen"
* #d4552 ^property[0].code = #None 
* #d4552 ^property[0].valueString = "Sich mit schnellen Schritten in der Weise zu bewegen, dass beide Füße gleichzeitig vom Boden abgehoben sind" 
* #d4552 ^property[1].code = #parent 
* #d4552 ^property[1].valueCode = #d455 
* #d4553 "Springen"
* #d4553 ^property[0].code = #None 
* #d4553 ^property[0].valueString = "Durch Beugen und Strecken der Beine den Boden verlassen, wie auf einem Bein springen, hopsen, hüpfen und ins Wasser springen" 
* #d4553 ^property[1].code = #parent 
* #d4553 ^property[1].valueCode = #d455 
* #d4554 "Schwimmen"
* #d4554 ^property[0].code = #None 
* #d4554 ^property[0].valueString = "Mit Bewegungen der Gliedmaßen und des Körpers den ganzen Körper durch das Wasser fortzubewegen, ohne Zuhilfenahme des Gewässergrundes" 
* #d4554 ^property[1].code = #parent 
* #d4554 ^property[1].valueCode = #d455 
* #d4558 "Sich auf andere Weise fortbewegen, anders bezeichnet"
* #d4558 ^property[0].code = #parent 
* #d4558 ^property[0].valueCode = #d455 
* #d4559 "Sich auf andere Weise fortbewegen, nicht näher bezeichnet"
* #d4559 ^property[0].code = #parent 
* #d4559 ^property[0].valueCode = #d455 
* #d460 "Sich in verschiedenen Umgebungen fortbewegen"
* #d460 ^property[0].code = #None 
* #d460 ^property[0].valueString = "In verschiedenen Orten und Situationen zu gehen und sich fortzubewegen, wie in einem Haus oder Gebäude von einem Raum in einen anderen gehen oder auf einer Straße einer Stadt gehen" 
* #d460 ^property[1].code = #None 
* #d460 ^property[1].valueString = "Sich in seiner Wohnung umherbewegen, in der Wohnung krabbeln oder (Treppen) steigen, in anderen Gebäuden als zu Hause bzw. außerhalb seiner Wohnung oder anderen Gebäuden gehen oder sich fortbewegen" 
* #d460 ^property[2].code = #parent 
* #d460 ^property[2].valueCode = #d450-d469 
* #d460 ^property[3].code = #child 
* #d460 ^property[3].valueCode = #d4600 
* #d460 ^property[4].code = #child 
* #d460 ^property[4].valueCode = #d4601 
* #d460 ^property[5].code = #child 
* #d460 ^property[5].valueCode = #d4602 
* #d460 ^property[6].code = #child 
* #d460 ^property[6].valueCode = #d4608 
* #d460 ^property[7].code = #child 
* #d460 ^property[7].valueCode = #d4609 
* #d4600 "Sich in seiner Wohnung umherbewegen"
* #d4600 ^property[0].code = #None 
* #d4600 ^property[0].valueString = "In seiner Wohnung umherzugehen und sich umherzubewegen, innerhalb eines Raumes und zwischen Räumen sowie innerhalb der ganzen Wohnung oder des Lebensbereichs" 
* #d4600 ^property[1].code = #None 
* #d4600 ^property[1].valueString = "Sich von Stockwerk zu Stockwerk, auf einem Balkon, auf dem Hof, auf der Veranda oder im Garten bewegen" 
* #d4600 ^property[2].code = #parent 
* #d4600 ^property[2].valueCode = #d460 
* #d4601 "Sich in anderen Gebäuden außerhalb der eigenen Wohnung umherbewegen"
* #d4601 ^property[0].code = #None 
* #d4601 ^property[0].valueString = "Innerhalb von Gebäuden außerhalb der eigenen Wohnung umherzugehen und sich umherzubewegen, wie sich in anderen Wohnungen, privaten, gemeindeeigenen oder öffentlichen Gebäuden und eingefriedeten Bereichen umherbewegen" 
* #d4601 ^property[1].code = #None 
* #d4601 ^property[1].valueString = "Sich in allen Teilen von privaten oder öffentlichen Gebäuden und eingefriedeten Bereichen bewegen, zwischen den Stockwerken, innerhalb, außerhalb und um Gebäude herum" 
* #d4601 ^property[2].code = #parent 
* #d4601 ^property[2].valueCode = #d460 
* #d4602 "Sich außerhalb der eigenen Wohnung und anderen Gebäuden umherbewegen"
* #d4602 ^property[0].code = #None 
* #d4602 ^property[0].valueString = "In der Nähe oder von der eigenen Wohnung oder anderen Gebäuden entfernt umherzugehen und sich fortzubewegen, ohne öffentliche oder private Transport- oder Verkehrsmittel zu benutzen, wie für kurze oder lange Entfernungen in einer Stadt oder einem Dorf herumzugehen" 
* #d4602 ^property[1].code = #None 
* #d4602 ^property[1].valueString = "Auf Straßen in der Nachbarschaft, der Stadt, des Dorfes oder der Großstadt entlang gehen oder sich auf ihnen fortzubewegen; sich ohne Transport- oder Verkehrsmittel zwischen Großstädten und für längere Entfernungen bewegen" 
* #d4602 ^property[2].code = #parent 
* #d4602 ^property[2].valueCode = #d460 
* #d4608 "Sich in verschiedenen Umgebungen fortbewegen, anders bezeichnet"
* #d4608 ^property[0].code = #parent 
* #d4608 ^property[0].valueCode = #d460 
* #d4609 "Sich in verschiedenen Umgebungen fortbewegen, nicht näher bezeichnet"
* #d4609 ^property[0].code = #parent 
* #d4609 ^property[0].valueCode = #d460 
* #d465 "Sich unter Verwendung von Geräten/Ausrüstung fortbewegen"
* #d465 ^property[0].code = #None 
* #d465 ^property[0].valueString = "Seinen ganzen Körper unter Verwendung von speziellen Geräten, die zur Erleichterung der Mobilität entworfen sind, oder anderen Hilfsvorrichtungen der Fortbewegung auf beliebigen Oberflächen oder in beliebigen Umgebungen von einem Ort zu einem anderen fortzubewegen, wie mit Schlittschuhen, mit Skiern oder mit einer Ausrüstung zum Gerätetauchen, oder sich auf einer Straße mit einem Rollstuhl oder Gehwagen fortbewegen" 
* #d465 ^property[1].code = #None 
* #d465 ^property[1].valueString = "Sich verlagern" 
* #d465 ^property[2].code = #parent 
* #d465 ^property[2].valueCode = #d450-d469 
* #d469 "Gehen und sich fortbewegen, anders oder nicht näher bezeichnet"
* #d469 ^property[0].code = #parent 
* #d469 ^property[0].valueCode = #d450-d469 
* #d470-d489 "Sich mit Transportmitteln fortbewegen"
* #d470-d489 ^property[0].code = #parent 
* #d470-d489 ^property[0].valueCode = #d4 
* #d470-d489 ^property[1].code = #child 
* #d470-d489 ^property[1].valueCode = #d470 
* #d470-d489 ^property[2].code = #child 
* #d470-d489 ^property[2].valueCode = #d475 
* #d470-d489 ^property[3].code = #child 
* #d470-d489 ^property[3].valueCode = #d480 
* #d470-d489 ^property[4].code = #child 
* #d470-d489 ^property[4].valueCode = #d489 
* #d470 "Transportmittel benutzen"
* #d470 ^property[0].code = #None 
* #d470 ^property[0].valueString = "Transportmittel zu benutzen, um sich als Fahrgast fortzubewegen, wie als Mitfahrer mit einem Auto oder Autobus, einer Rikscha, einem Ruderboot, einem von einem Tier angetriebenen Fahrzeug, mit einem privaten oder öffentlichen Taxi, Autobus, Zug, Straßenbahn, U-Bahn, Schiff oder Flugzeug" 
* #d470 ^property[1].code = #None 
* #d470 ^property[1].valueString = "Ein von Menschenkraft betriebenes Fahrzeug benutzen, private motorisierte oder öffentliche Transportmittel benutzen" 
* #d470 ^property[2].code = #None 
* #d470 ^property[2].valueString = "Sich unter Verwendung von Geräten/Ausrüstung fortbewegen" 
* #d470 ^property[3].code = #parent 
* #d470 ^property[3].valueCode = #d470-d489 
* #d470 ^property[4].code = #child 
* #d470 ^property[4].valueCode = #d4700 
* #d470 ^property[5].code = #child 
* #d470 ^property[5].valueCode = #d4701 
* #d470 ^property[6].code = #child 
* #d470 ^property[6].valueCode = #d4702 
* #d470 ^property[7].code = #child 
* #d470 ^property[7].valueCode = #d4708 
* #d470 ^property[8].code = #child 
* #d470 ^property[8].valueCode = #d4709 
* #d4700 "Ein von Menschenkraft betriebenes Fahrzeug benutzen"
* #d4700 ^property[0].code = #None 
* #d4700 ^property[0].valueString = "Als Fahrgast mit einem von Menschenkraft betriebenen Fahrzeug befördert zu werden, wie mit einer Rikscha oder einem Ruderboot" 
* #d4700 ^property[1].code = #parent 
* #d4700 ^property[1].valueCode = #d470 
* #d4701 "Ein privates, motorisiertes Fahrzeug benutzen"
* #d4701 ^property[0].code = #None 
* #d4701 ^property[0].valueString = "Als Fahrgast mit einem privaten, motorisierten Land-, Wasser- oder Luftfahrzeug befördert zu werden, wie mit einem Taxi oder einem privaten Flugzeug oder Boot" 
* #d4701 ^property[1].code = #parent 
* #d4701 ^property[1].valueCode = #d470 
* #d4702 "Ein öffentliches, motorisiertes Verkehrsmittel benutzen"
* #d4702 ^property[0].code = #None 
* #d4702 ^property[0].valueString = "Als Fahrgast mit einem öffentlichen, motorisierten Land-, Wasser- oder Luftverkehrsmittel befördert zu werden, wie als Fahrgast mit einem Bus, Zug, U-Bahn oder Flugzeug" 
* #d4702 ^property[1].code = #parent 
* #d4702 ^property[1].valueCode = #d470 
* #d4708 "Transportmittel benutzen, anders bezeichnet"
* #d4708 ^property[0].code = #parent 
* #d4708 ^property[0].valueCode = #d470 
* #d4709 "Transportmittel benutzen, nicht näher bezeichnet"
* #d4709 ^property[0].code = #parent 
* #d4709 ^property[0].valueCode = #d470 
* #d475 "Ein Fahrzeug fahren"
* #d475 ^property[0].code = #None 
* #d475 ^property[0].valueString = "Ein Fahrzeug oder das Tier, das es zieht, zu kontrollieren und zu bewegen, unter eigener Leitung zu reisen oder über ein beliebiges Fahrzeug zu verfügen wie ein Auto, Fahrrad, Boot oder ein von einem Tier angetriebenes Fahrzeug" 
* #d475 ^property[1].code = #None 
* #d475 ^property[1].valueString = "Ein mit Menschenkraft betriebenes Transportmittel, motorisierte und von einem Tier angetriebene Fahrzeuge fahren" 
* #d475 ^property[2].code = #None 
* #d475 ^property[2].valueString = "Sich unter Verwendung von Geräten/Ausrüstung fortbewegen" 
* #d475 ^property[3].code = #parent 
* #d475 ^property[3].valueCode = #d470-d489 
* #d475 ^property[4].code = #child 
* #d475 ^property[4].valueCode = #d4750 
* #d475 ^property[5].code = #child 
* #d475 ^property[5].valueCode = #d4751 
* #d475 ^property[6].code = #child 
* #d475 ^property[6].valueCode = #d4752 
* #d475 ^property[7].code = #child 
* #d475 ^property[7].valueCode = #d4758 
* #d475 ^property[8].code = #child 
* #d475 ^property[8].valueCode = #d4759 
* #d4750 "Ein von Menschenkraft betriebenes Fahrzeug fahren"
* #d4750 ^property[0].code = #None 
* #d4750 ^property[0].valueString = "Ein von Menschenkraft betriebenes Fahrzeug zu fahren, wie ein Fahrrad, Dreirad oder Ruderboot" 
* #d4750 ^property[1].code = #parent 
* #d4750 ^property[1].valueCode = #d475 
* #d4751 "Ein motorisiertes Fahrzeug fahren"
* #d4751 ^property[0].code = #None 
* #d4751 ^property[0].valueString = "Ein Fahrzeug mit einem Motor zu fahren, wie ein Auto, Motorrad, Motorboot oder Flugzeug" 
* #d4751 ^property[1].code = #parent 
* #d4751 ^property[1].valueCode = #d475 
* #d4752 "Ein von einem Tier angetriebenes Fahrzeug fahren"
* #d4752 ^property[0].code = #None 
* #d4752 ^property[0].valueString = "Ein von einem Tier angetriebenes Fahrzeug zu fahren, wie einen Pferdewagen oder eine Pferdekutsche" 
* #d4752 ^property[1].code = #parent 
* #d4752 ^property[1].valueCode = #d475 
* #d4758 "Ein Fahrzeug fahren, anders bezeichnet"
* #d4758 ^property[0].code = #parent 
* #d4758 ^property[0].valueCode = #d475 
* #d4759 "Ein Fahrzeug fahren, nicht näher bezeichnet"
* #d4759 ^property[0].code = #parent 
* #d4759 ^property[0].valueCode = #d475 
* #d480 "Tiere zu Transportzwecken reiten"
* #d480 ^property[0].code = #None 
* #d480 ^property[0].valueString = "Sich auf dem Rücken eines Tieres fortzubewegen, wie auf einem Pferd, Ochsen, Kamel oder Elefanten" 
* #d480 ^property[1].code = #None 
* #d480 ^property[1].valueString = "Ein Fahrzeug fahren" 
* #d480 ^property[2].code = #parent 
* #d480 ^property[2].valueCode = #d470-d489 
* #d489 "Sich mit Transportmitteln fortbewegen, anders oder nicht näher bezeichnet"
* #d489 ^property[0].code = #parent 
* #d489 ^property[0].valueCode = #d470-d489 
* #d498 "Mobilität, anders bezeichnet"
* #d498 ^property[0].code = #parent 
* #d498 ^property[0].valueCode = #d4 
* #d499 "Mobilität, nicht näher bezeichnet"
* #d499 ^property[0].code = #parent 
* #d499 ^property[0].valueCode = #d4 
* #d5 "Selbstversorgung"
* #d5 ^property[0].code = #None 
* #d5 ^property[0].valueString = "Dieses Kapitel befasst sich mit der eigenen Versorgung, dem Waschen, Abtrocknen und der Pflege des eigenen Körpers und seiner Teile, dem An- und Ablegen von Kleidung, dem Essen und Trinken und der Sorge um die eigene Gesundheit." 
* #d5 ^property[1].code = #parent 
* #d5 ^property[1].valueCode = #d 
* #d5 ^property[2].code = #child 
* #d5 ^property[2].valueCode = #d510 
* #d5 ^property[3].code = #child 
* #d5 ^property[3].valueCode = #d520 
* #d5 ^property[4].code = #child 
* #d5 ^property[4].valueCode = #d530 
* #d5 ^property[5].code = #child 
* #d5 ^property[5].valueCode = #d540 
* #d5 ^property[6].code = #child 
* #d5 ^property[6].valueCode = #d550 
* #d5 ^property[7].code = #child 
* #d5 ^property[7].valueCode = #d560 
* #d5 ^property[8].code = #child 
* #d5 ^property[8].valueCode = #d570 
* #d5 ^property[9].code = #child 
* #d5 ^property[9].valueCode = #d598 
* #d5 ^property[10].code = #child 
* #d5 ^property[10].valueCode = #d599 
* #d510 "Sich waschen"
* #d510 ^property[0].code = #None 
* #d510 ^property[0].valueString = "Den ganzen Körper oder Körperteile mit Wasser und geeigneten Reinigungs- und Abtrocknungsmaterialien oder -methoden zu waschen und abzutrocknen, wie baden, duschen, Hände, Füße, Gesicht und Haare waschen und mit einem Handtuch abtrocknen" 
* #d510 ^property[1].code = #None 
* #d510 ^property[1].valueString = "Körperteile und den ganzen Körper waschen; sich abtrocknen" 
* #d510 ^property[2].code = #None 
* #d510 ^property[2].valueString = "Seine Körperteile pflegen" 
* #d510 ^property[3].code = #parent 
* #d510 ^property[3].valueCode = #d5 
* #d510 ^property[4].code = #child 
* #d510 ^property[4].valueCode = #d5100 
* #d510 ^property[5].code = #child 
* #d510 ^property[5].valueCode = #d5101 
* #d510 ^property[6].code = #child 
* #d510 ^property[6].valueCode = #d5102 
* #d510 ^property[7].code = #child 
* #d510 ^property[7].valueCode = #d5108 
* #d510 ^property[8].code = #child 
* #d510 ^property[8].valueCode = #d5109 
* #d5100 "Körperteile waschen"
* #d5100 ^property[0].code = #None 
* #d5100 ^property[0].valueString = "Zur Reinigung seiner Körperteile, wie Hände, Gesicht, Füße, Haare oder Nägel, Wasser, Seife und andere Substanzen zu verwenden" 
* #d5100 ^property[1].code = #parent 
* #d5100 ^property[1].valueCode = #d510 
* #d5101 "Den ganzen Körper waschen"
* #d5101 ^property[0].code = #None 
* #d5101 ^property[0].valueString = "Zur Reinigung seines ganzen Körpers Wasser, Seife und andere Substanzen zu verwenden, wie baden oder duschen" 
* #d5101 ^property[1].code = #parent 
* #d5101 ^property[1].valueCode = #d510 
* #d5102 "Sich abtrocknen"
* #d5102 ^property[0].code = #None 
* #d5102 ^property[0].valueString = "Zum Abtrocknen eines Körperteils, von Körperteilen oder des ganzen Körpers ein Handtuch oder entsprechendes zu verwenden, wie nach dem Waschen" 
* #d5102 ^property[1].code = #parent 
* #d5102 ^property[1].valueCode = #d510 
* #d5108 "Sich waschen, anders bezeichnet"
* #d5108 ^property[0].code = #parent 
* #d5108 ^property[0].valueCode = #d510 
* #d5109 "Sich waschen, nicht näher bezeichnet"
* #d5109 ^property[0].code = #parent 
* #d5109 ^property[0].valueCode = #d510 
* #d520 "Seine Körperteile pflegen"
* #d520 ^property[0].code = #None 
* #d520 ^property[0].valueString = "Sich um seine Körperteile wie Haut, Gesicht, Zähne, Kopfhaut, Nägel und Genitalien über das Waschen und Abtrocknen hinaus zu kümmern" 
* #d520 ^property[1].code = #None 
* #d520 ^property[1].valueString = "Haut, Zähne, Haar, Finger, Zehennägel pflegen" 
* #d520 ^property[2].code = #None 
* #d520 ^property[2].valueString = "Sich waschen" 
* #d520 ^property[3].code = #parent 
* #d520 ^property[3].valueCode = #d5 
* #d520 ^property[4].code = #child 
* #d520 ^property[4].valueCode = #d5200 
* #d520 ^property[5].code = #child 
* #d520 ^property[5].valueCode = #d5201 
* #d520 ^property[6].code = #child 
* #d520 ^property[6].valueCode = #d5202 
* #d520 ^property[7].code = #child 
* #d520 ^property[7].valueCode = #d5203 
* #d520 ^property[8].code = #child 
* #d520 ^property[8].valueCode = #d5204 
* #d520 ^property[9].code = #child 
* #d520 ^property[9].valueCode = #d5208 
* #d520 ^property[10].code = #child 
* #d520 ^property[10].valueCode = #d5209 
* #d5200 "Die Haut pflegen"
* #d5200 ^property[0].code = #None 
* #d5200 ^property[0].valueString = "Sich um die Beschaffenheit und Feuchtigkeitszufuhr seiner Haut zu kümmern, wie Schwielen oder Hühneraugen entfernen und Feuchtigkeitslotionen oder Kosmetika benutzen" 
* #d5200 ^property[1].code = #parent 
* #d5200 ^property[1].valueCode = #d520 
* #d5201 "Die Zähne pflegen"
* #d5201 ^property[0].code = #None 
* #d5201 ^property[0].valueString = "Sich um Zahnpflege zu kümmern, wie die Zähne putzen, Zahnseide benutzen sowie Zahnprothesen oder -orthesen reinigen" 
* #d5201 ^property[1].code = #parent 
* #d5201 ^property[1].valueCode = #d520 
* #d5202 "Das Haar pflegen"
* #d5202 ^property[0].code = #None 
* #d5202 ^property[0].valueString = "Sich um sein Kopf- und Gesichtshaar zu kümmern, wie kämmen, frisieren, rasieren oder schneiden" 
* #d5202 ^property[1].code = #parent 
* #d5202 ^property[1].valueCode = #d520 
* #d5203 "Die Fingernägel pflegen"
* #d5203 ^property[0].code = #None 
* #d5203 ^property[0].valueString = "Die Fingernägel zu reinigen, zu schneiden oder zu polieren" 
* #d5203 ^property[1].code = #parent 
* #d5203 ^property[1].valueCode = #d520 
* #d5204 "Die Fußnägel pflegen"
* #d5204 ^property[0].code = #None 
* #d5204 ^property[0].valueString = "Die Fußnägel zu reinigen, zu schneiden oder zu polieren" 
* #d5204 ^property[1].code = #parent 
* #d5204 ^property[1].valueCode = #d520 
* #d5208 "Seine Körperteile pflegen, anders bezeichnet"
* #d5208 ^property[0].code = #parent 
* #d5208 ^property[0].valueCode = #d520 
* #d5209 "Seine Körperteile pflegen, nicht näher bezeichnet"
* #d5209 ^property[0].code = #parent 
* #d5209 ^property[0].valueCode = #d520 
* #d530 "Die Toilette benutzen"
* #d530 ^property[0].code = #None 
* #d530 ^property[0].valueString = "Die Beseitigung menschlicher Ausscheidungen (Menstruationssekrete, Urin, Stuhl) zu planen und durchzuführen sowie sich anschließend zu reinigen" 
* #d530 ^property[1].code = #None 
* #d530 ^property[1].valueString = "Die Belange der Blasen- und Darmentleerung sowie der Menstruation regulieren" 
* #d530 ^property[2].code = #None 
* #d530 ^property[2].valueString = "Sich waschen" 
* #d530 ^property[3].code = #parent 
* #d530 ^property[3].valueCode = #d5 
* #d530 ^property[4].code = #child 
* #d530 ^property[4].valueCode = #d5300 
* #d530 ^property[5].code = #child 
* #d530 ^property[5].valueCode = #d5301 
* #d530 ^property[6].code = #child 
* #d530 ^property[6].valueCode = #d5302 
* #d530 ^property[7].code = #child 
* #d530 ^property[7].valueCode = #d5308 
* #d530 ^property[8].code = #child 
* #d530 ^property[8].valueCode = #d5309 
* #d5300 "Die Belange der Blasenentleerung regulieren"
* #d5300 ^property[0].code = #None 
* #d5300 ^property[0].valueString = "Die Blasenentleerung zu koordinieren und zu handhaben, wie das Bedürfnis angeben, sich in eine geeignete Position begeben, einen angemessenen Ort zur Blasenentleerung wählen und aufsuchen, vor und nach der Blasenentleerung die Kleidung richten und sich nach der Blasenentleerung reinigen" 
* #d5300 ^property[1].code = #parent 
* #d5300 ^property[1].valueCode = #d530 
* #d5301 "Die Belange der Darmentleerung regulieren"
* #d5301 ^property[0].code = #None 
* #d5301 ^property[0].valueString = "Die Darmentleerung zu koordinieren und zu handhaben, wie das Bedürfnis angeben, sich in eine geeignete Position begeben, einen angemessenen Ort zur Darmentleerung wählen und aufsuchen, vor und nach der Darmentleerung die Kleidung richten und sich nach der Darmentleerung reinigen" 
* #d5301 ^property[1].code = #parent 
* #d5301 ^property[1].valueCode = #d530 
* #d5302 "Die Belange der Menstruation regulieren"
* #d5302 ^property[0].code = #None 
* #d5302 ^property[0].valueString = "Die Menstruation zu koordinieren, zu planen und sich um sie zu kümmern, wie den Eintritt der Menstruation voraussehen sowie Binden und entsprechende Artikel benutzen" 
* #d5302 ^property[1].code = #parent 
* #d5302 ^property[1].valueCode = #d530 
* #d5308 "Die Toilette benutzen, anders bezeichnet"
* #d5308 ^property[0].code = #parent 
* #d5308 ^property[0].valueCode = #d530 
* #d5309 "Die Toilette benutzen, nicht näher bezeichnet"
* #d5309 ^property[0].code = #parent 
* #d5309 ^property[0].valueCode = #d530 
* #d540 "Sich kleiden"
* #d540 ^property[0].code = #None 
* #d540 ^property[0].valueString = "Die koordinierten Handlungen und Aufgaben durchzuführen, welche das An- und Ausziehen von Kleidung und Schuhwerk in Abfolge und entsprechend den sozialen und klimatischen Bedingungen betreffen, wie Hemden, Röcke, Blusen, Hosen, Unterwäsche, Saris, Kimonos, Strumpfhosen, Hüte, Handschuhe, Mäntel, Schuhe, Stiefel, Sandalen oder Slipper anziehen, ordnen und ausziehen" 
* #d540 ^property[1].code = #None 
* #d540 ^property[1].valueString = "Kleidung und Schuhwerk an- und ausziehen sowie geeignete Kleidung auswählen" 
* #d540 ^property[2].code = #parent 
* #d540 ^property[2].valueCode = #d5 
* #d540 ^property[3].code = #child 
* #d540 ^property[3].valueCode = #d5400 
* #d540 ^property[4].code = #child 
* #d540 ^property[4].valueCode = #d5401 
* #d540 ^property[5].code = #child 
* #d540 ^property[5].valueCode = #d5402 
* #d540 ^property[6].code = #child 
* #d540 ^property[6].valueCode = #d5403 
* #d540 ^property[7].code = #child 
* #d540 ^property[7].valueCode = #d5404 
* #d540 ^property[8].code = #child 
* #d540 ^property[8].valueCode = #d5408 
* #d540 ^property[9].code = #child 
* #d540 ^property[9].valueCode = #d5409 
* #d5400 "Kleidung anziehen"
* #d5400 ^property[0].code = #None 
* #d5400 ^property[0].valueString = "Die koordinierten Handlungen und Aufgaben durchzuführen, welche das Anlegen von Kleidung an verschiedene Körperteile betreffen, wie Kleidung über den Kopf, über Arme und Schultern sowie an die untere und obere Körperhälfte anlegen; Handschuhe anziehen oder eine Kopfbedeckung aufsetzen" 
* #d5400 ^property[1].code = #parent 
* #d5400 ^property[1].valueCode = #d540 
* #d5401 "Kleidung ausziehen"
* #d5401 ^property[0].code = #None 
* #d5401 ^property[0].valueString = "Die koordinierten Handlungen und Aufgaben durchzuführen, welche das Ablegen von Kleidung von verschiedenen Körperteilen betreffen, wie Kleidung vom oder über den Kopf, von Armen und Schultern sowie von der unteren und oberen Körperhälfte ablegen; Handschuhe ausziehen oder eine Kopfbedeckung ablegen" 
* #d5401 ^property[1].code = #parent 
* #d5401 ^property[1].valueCode = #d540 
* #d5402 "Schuhwerk anziehen"
* #d5402 ^property[0].code = #None 
* #d5402 ^property[0].valueString = "Die koordinierten Handlungen und Aufgaben durchzuführen, welche das Anziehen von Socken, Strümpfen und Schuhwerk betreffen" 
* #d5402 ^property[1].code = #parent 
* #d5402 ^property[1].valueCode = #d540 
* #d5403 "Schuhwerk ausziehen"
* #d5403 ^property[0].code = #None 
* #d5403 ^property[0].valueString = "Die koordinierten Handlungen und Aufgaben durchzuführen, welche das Ausziehen von Socken, Strümpfen und Schuhwerk betreffen" 
* #d5403 ^property[1].code = #parent 
* #d5403 ^property[1].valueCode = #d540 
* #d5404 "Geeignete Kleidung auswählen"
* #d5404 ^property[0].code = #None 
* #d5404 ^property[0].valueString = "Den impliziten oder expliziten Kleiderregeln und -konventionen seiner Gesellschaft oder Kultur zu entsprechen und sich entsprechend der klimatischen Bedingungen zu kleiden" 
* #d5404 ^property[1].code = #parent 
* #d5404 ^property[1].valueCode = #d540 
* #d5408 "Sich kleiden, anders bezeichnet"
* #d5408 ^property[0].code = #parent 
* #d5408 ^property[0].valueCode = #d540 
* #d5409 "Sich kleiden, nicht näher bezeichnet"
* #d5409 ^property[0].code = #parent 
* #d5409 ^property[0].valueCode = #d540 
* #d550 "Essen"
* #d550 ^property[0].code = #None 
* #d550 ^property[0].valueString = "Die koordinierten Handlungen und Aufgaben durchzuführen, die das Essen servierter Speisen betreffen, sie zum Mund zu führen und auf kulturell akzeptierte Weise zu verzehren, Nahrungsmittel in Stücke zu schneiden oder zu brechen, Flaschen und Dosen zu öffnen, Essbesteck zu benutzen, Mahlzeiten einnehmen, zu schlemmen oder zu speisen" 
* #d550 ^property[1].code = #None 
* #d550 ^property[1].valueString = "Trinken" 
* #d550 ^property[2].code = #parent 
* #d550 ^property[2].valueCode = #d5 
* #d560 "Trinken"
* #d560 ^property[0].code = #None 
* #d560 ^property[0].valueString = "Ein Gefäß mit einem Getränk in die Hand zu nehmen, es zum Mund zu führen und den Inhalt in kulturell akzeptierter Weise zu trinken, Flüssigkeiten zum Trinken zu mischen, zu rühren, zu gießen, Flaschen und Dosen zu öffnen, mit einem Strohhalm zu trinken oder fließendes Wasser wie z. B. vom Wasserhahn oder aus einer Quelle zu trinken; trinken an der Brust (Säugling)" 
* #d560 ^property[1].code = #None 
* #d560 ^property[1].valueString = "Essen" 
* #d560 ^property[2].code = #parent 
* #d560 ^property[2].valueCode = #d5 
* #d570 "Auf seine Gesundheit achten"
* #d570 ^property[0].code = #None 
* #d570 ^property[0].valueString = "Für physischen Komfort, Gesundheit sowie für physisches und mentales Wohlbefinden zu sorgen, wie eine ausgewogene Ernährung und ein angemessenes Niveau körperlicher Aktivität aufrecht erhalten, sich warm oder kühl halten, Gesundheitsschäden vermeiden, sicheren Sex praktizieren einschließlich Kondome benutzen, für Impfschutz und regelmäßige ärztliche Untersuchungen sorgen" 
* #d570 ^property[1].code = #None 
* #d570 ^property[1].valueString = "Für physischen Komfort sorgen; Ernährung und Fitness handhaben; die eigene Gesundheit erhalten" 
* #d570 ^property[2].code = #parent 
* #d570 ^property[2].valueCode = #d5 
* #d570 ^property[3].code = #child 
* #d570 ^property[3].valueCode = #d5700 
* #d570 ^property[4].code = #child 
* #d570 ^property[4].valueCode = #d5701 
* #d570 ^property[5].code = #child 
* #d570 ^property[5].valueCode = #d5702 
* #d570 ^property[6].code = #child 
* #d570 ^property[6].valueCode = #d5708 
* #d570 ^property[7].code = #child 
* #d570 ^property[7].valueCode = #d5709 
* #d5700 "Für seinen physischen Komfort sorgen"
* #d5700 ^property[0].code = #None 
* #d5700 ^property[0].valueString = "Auf sich selbst zu achten, indem man für eine bequeme Körperposition, eine angenehme Körpertemperatur und geeignete Beleuchtung sorgt und man sich über diese Notwendigkeit im Klaren ist" 
* #d5700 ^property[1].code = #parent 
* #d5700 ^property[1].valueCode = #d570 
* #d5701 "Ernährung und Fitness handhaben"
* #d5701 ^property[0].code = #None 
* #d5701 ^property[0].valueString = "Auf sich selbst zu achten, indem man gesunde Lebensmittel auswählt und verzehrt, sich körperlich fit hält und man sich über diese Notwendigkeit im Klaren ist" 
* #d5701 ^property[1].code = #parent 
* #d5701 ^property[1].valueCode = #d570 
* #d5702 "Seine Gesundheit erhalten"
* #d5702 ^property[0].code = #None 
* #d5702 ^property[0].valueString = "Auf sich selbst zu achten, indem man das tut, was die eigene Gesundheit erfordert, und zwar im Hinblick auf Gesundheitsrisiken und Krankheitsverhütung, und man sich über diese Notwendigkeit im Klaren ist. Hierzu gehören professionelle Hilfe in Anspruch nehmen, medizinischem oder anderem gesundheitlichem Rat folgen, Gesundheitsrisiken vermeiden wie körperliche Verletzungen, ansteckende Krankheiten, Drogeneinnahme und sexuell übertragbare Krankheiten" 
* #d5702 ^property[1].code = #parent 
* #d5702 ^property[1].valueCode = #d570 
* #d5708 "Auf seine Gesundheit achten, anders bezeichnet"
* #d5708 ^property[0].code = #parent 
* #d5708 ^property[0].valueCode = #d570 
* #d5709 "Auf seine Gesundheit achten, nicht näher bezeichnet"
* #d5709 ^property[0].code = #parent 
* #d5709 ^property[0].valueCode = #d570 
* #d598 "Selbstversorgung, anders bezeichnet"
* #d598 ^property[0].code = #parent 
* #d598 ^property[0].valueCode = #d5 
* #d599 "Selbstversorgung, nicht näher bezeichnet"
* #d599 ^property[0].code = #parent 
* #d599 ^property[0].valueCode = #d5 
* #d6 "Häusliches Leben"
* #d6 ^property[0].code = #None 
* #d6 ^property[0].valueString = "Dieses Kapitel befasst sich mit der Ausführung von häuslichen und alltäglichen Handlungen und Aufgaben. Die Bereiche des häuslichen Lebens umfassen die Beschaffung einer Wohnung, von Lebensmitteln, Kleidung und anderen Notwendigkeiten, Reinigungs- und Reparaturarbeiten im Haushalt, die Pflege von persönlichen und anderen Haushaltsgegenständen und die Hilfe für andere." 
* #d6 ^property[1].code = #parent 
* #d6 ^property[1].valueCode = #d 
* #d6 ^property[2].code = #child 
* #d6 ^property[2].valueCode = #d610-d629 
* #d6 ^property[3].code = #child 
* #d6 ^property[3].valueCode = #d630-d649 
* #d6 ^property[4].code = #child 
* #d6 ^property[4].valueCode = #d650-d669 
* #d6 ^property[5].code = #child 
* #d6 ^property[5].valueCode = #d698 
* #d6 ^property[6].code = #child 
* #d6 ^property[6].valueCode = #d699 
* #d610-d629 "Beschaffung von Lebensnotwendigkeiten"
* #d610-d629 ^property[0].code = #parent 
* #d610-d629 ^property[0].valueCode = #d6 
* #d610-d629 ^property[1].code = #child 
* #d610-d629 ^property[1].valueCode = #d610 
* #d610-d629 ^property[2].code = #child 
* #d610-d629 ^property[2].valueCode = #d620 
* #d610-d629 ^property[3].code = #child 
* #d610-d629 ^property[3].valueCode = #d629 
* #d610 "Wohnraum beschaffen"
* #d610 ^property[0].code = #None 
* #d610 ^property[0].valueString = "Ein Haus, ein Appartement oder eine Wohnung zu kaufen, zu mieten, zu möblieren und die Möbel aufzustellen" 
* #d610 ^property[1].code = #None 
* #d610 ^property[1].valueString = "Wohnraum kaufen oder mieten und Wohnraum möblieren" 
* #d610 ^property[2].code = #None 
* #d610 ^property[2].valueString = "Waren und Dienstleistungen des täglichen Bedarfs beschaffen" 
* #d610 ^property[3].code = #parent 
* #d610 ^property[3].valueCode = #d610-d629 
* #d610 ^property[4].code = #child 
* #d610 ^property[4].valueCode = #d6100 
* #d610 ^property[5].code = #child 
* #d610 ^property[5].valueCode = #d6101 
* #d610 ^property[6].code = #child 
* #d610 ^property[6].valueCode = #d6102 
* #d610 ^property[7].code = #child 
* #d610 ^property[7].valueCode = #d6108 
* #d610 ^property[8].code = #child 
* #d610 ^property[8].valueCode = #d6109 
* #d6100 "Wohnraum kaufen"
* #d6100 ^property[0].code = #None 
* #d6100 ^property[0].valueString = "Das Eigentum eines Hauses, Appartements oder einer Wohnung zu erwerben" 
* #d6100 ^property[1].code = #parent 
* #d6100 ^property[1].valueCode = #d610 
* #d6101 "Wohnraum mieten"
* #d6101 ^property[0].code = #None 
* #d6101 ^property[0].valueString = "Die Benutzung eines Hauses, Appartements oder einer anderen Wohngelegenheit, die jeweils einem anderen gehört, gegen Entgelt zu erlangen" 
* #d6101 ^property[1].code = #parent 
* #d6101 ^property[1].valueCode = #d610 
* #d6102 "Wohnraum möblieren"
* #d6102 ^property[0].code = #None 
* #d6102 ^property[0].valueString = "Wohnraum mit Möbeln, Einbauten und anderen Ausstattungen auszurüsten und einzurichten sowie die Räume zu dekorieren" 
* #d6102 ^property[1].code = #parent 
* #d6102 ^property[1].valueCode = #d610 
* #d6108 "Wohnraum beschaffen, anders bezeichnet"
* #d6108 ^property[0].code = #parent 
* #d6108 ^property[0].valueCode = #d610 
* #d6109 "Wohnraum beschaffen, nicht näher bezeichnet"
* #d6109 ^property[0].code = #parent 
* #d6109 ^property[0].valueCode = #d610 
* #d620 "Waren und Dienstleistungen des täglichen Bedarfs beschaffen"
* #d620 ^property[0].code = #None 
* #d620 ^property[0].valueString = "Alle Waren und Dienstleistungen des täglichen Bedarfs auszuwählen, zu beschaffen und zu transportieren, wie Lebensmittel, Getränke, Kleidung, Reinigungsmaterial, Brennstoff, Haushaltsartikel, Utensilien, Kochgeschirr, häusliche Hilfsmittel und Werkzeuge auswählen, beschaffen, transportieren und lagern; Versorgungs- und andere Dienstleistungen für den Haushalt beschaffen" 
* #d620 ^property[1].code = #None 
* #d620 ^property[1].valueString = "Die täglichen Notwendigkeiten einkaufen und zusammentragen" 
* #d620 ^property[2].code = #None 
* #d620 ^property[2].valueString = "Wohnraum beschaffen" 
* #d620 ^property[3].code = #parent 
* #d620 ^property[3].valueCode = #d610-d629 
* #d620 ^property[4].code = #child 
* #d620 ^property[4].valueCode = #d6200 
* #d620 ^property[5].code = #child 
* #d620 ^property[5].valueCode = #d6201 
* #d620 ^property[6].code = #child 
* #d620 ^property[6].valueCode = #d6208 
* #d620 ^property[7].code = #child 
* #d620 ^property[7].valueCode = #d6209 
* #d6200 "Einkaufen"
* #d6200 ^property[0].code = #None 
* #d6200 ^property[0].valueString = "Waren und Dienstleistungen für das tägliche Leben gegen Geld zu erwerben (einschließlich einen für die Einkäufe Beauftragten anzuweisen und zu beaufsichtigen), wie Lebensmittel, Getränke, Reinigungsmaterial, Haushaltsartikel oder Kleidung in einem Geschäft oder auf dem Markt auswählen; Qualität und Preis der benötigten Artikel vergleichen, den Preis für die ausgewählten Waren und Dienstleistungen aushandeln und bezahlen sowie die Waren transportieren" 
* #d6200 ^property[1].code = #parent 
* #d6200 ^property[1].valueCode = #d620 
* #d6201 "Die täglichen Notwendigkeiten unentgeltlich besorgen"
* #d6201 ^property[0].code = #None 
* #d6201 ^property[0].valueString = "Waren und Dienstleistungen für das tägliche Leben unentgeltlich zu beschaffen (einschließlich einen für die Beschaffung Beauftragten anzuweisen und zu beaufsichtigen), wie Gemüse und Früchte ernten sowie Wasser und Brennstoff beschaffen" 
* #d6201 ^property[1].code = #parent 
* #d6201 ^property[1].valueCode = #d620 
* #d6208 "Waren und Dienstleistungen des täglichen Bedarfs beschaffen, anders bezeichnet"
* #d6208 ^property[0].code = #parent 
* #d6208 ^property[0].valueCode = #d620 
* #d6209 "Waren und Dienstleistungen des täglichen Bedarfs beschaffen, nicht näher bezeichnet"
* #d6209 ^property[0].code = #parent 
* #d6209 ^property[0].valueCode = #d620 
* #d629 "Beschaffung von Lebensnotwendigkeiten, anders oder nicht näher bezeichnet"
* #d629 ^property[0].code = #parent 
* #d629 ^property[0].valueCode = #d610-d629 
* #d630-d649 "Haushaltsaufgaben"
* #d630-d649 ^property[0].code = #parent 
* #d630-d649 ^property[0].valueCode = #d6 
* #d630-d649 ^property[1].code = #child 
* #d630-d649 ^property[1].valueCode = #d630 
* #d630-d649 ^property[2].code = #child 
* #d630-d649 ^property[2].valueCode = #d640 
* #d630-d649 ^property[3].code = #child 
* #d630-d649 ^property[3].valueCode = #d649 
* #d630 "Mahlzeiten vorbereiten"
* #d630 ^property[0].code = #None 
* #d630 ^property[0].valueString = "Einfache und komplexe Mahlzeiten für sich selbst und andere zu planen, zu organisieren, zu kochen und anzurichten, wie ein Menü zubereiten, genießbare Lebensmittel und Getränke auswählen, Zutaten für die Vorbereitung der Mahlzeit zusammenstellen, mit Wärme kochen sowie kalte Speisen und Getränke vorbereiten und die Speisen servieren" 
* #d630 ^property[1].code = #None 
* #d630 ^property[1].valueString = "Einfache und komplexe Mahlzeiten vorbereiten" 
* #d630 ^property[2].code = #None 
* #d630 ^property[2].valueString = "Essen" 
* #d630 ^property[3].code = #parent 
* #d630 ^property[3].valueCode = #d630-d649 
* #d630 ^property[4].code = #child 
* #d630 ^property[4].valueCode = #d6300 
* #d630 ^property[5].code = #child 
* #d630 ^property[5].valueCode = #d6301 
* #d630 ^property[6].code = #child 
* #d630 ^property[6].valueCode = #d6308 
* #d630 ^property[7].code = #child 
* #d630 ^property[7].valueCode = #d6309 
* #d6300 "Einfache Mahlzeiten vorbereiten"
* #d6300 ^property[0].code = #None 
* #d6300 ^property[0].valueString = "Mahlzeiten, die wenig Zutaten erfordern und mit einfachen Mitteln zubereitet und serviert werden können, zu kochen und zu servieren, wie einen Snack oder eine kleine Mahlzeit zubereiten, die Zutaten durch Schneiden oder Rühren bearbeiten und Lebensmittel wie Reis oder Kartoffeln kochen oder erhitzen" 
* #d6300 ^property[1].code = #parent 
* #d6300 ^property[1].valueCode = #d630 
* #d6301 "Komplexe Mahlzeiten vorbereiten"
* #d6301 ^property[0].code = #None 
* #d6301 ^property[0].valueString = "Mahlzeiten, die viele Zutaten erfordern und mit komplexen Mitteln zubereitet und serviert werden müssen, zu planen, zu organisieren, zu kochen und zu servieren, wie mehrgängige Mahlzeiten planen, die Zutaten durch kombinierte Handlungen wie schälen, in Scheiben oder Stücke schneiden, mixen, kneten und rühren bearbeiten und die Mahlzeit dem Anlass und der Kultur entsprechend zu servieren" 
* #d6301 ^property[1].code = #None 
* #d6301 ^property[1].valueString = "Haushaltsgeräte benutzen" 
* #d6301 ^property[2].code = #parent 
* #d6301 ^property[2].valueCode = #d630 
* #d6308 "Mahlzeiten vorbereiten, anders bezeichnet"
* #d6308 ^property[0].code = #parent 
* #d6308 ^property[0].valueCode = #d630 
* #d6309 "Mahlzeiten vorbereiten, nicht näher bezeichnet"
* #d6309 ^property[0].code = #parent 
* #d6309 ^property[0].valueCode = #d630 
* #d640 "Hausarbeiten erledigen"
* #d640 ^property[0].code = #None 
* #d640 ^property[0].valueString = "Einen Haushalt zu handhaben durch Reinigen des Hauses, Waschen von Kleidung, Benutzung von Haushaltsgeräten, Lagerung von Lebensmitteln, Entsorgung von Müll, wie fegen, moppen, Tische, Wände und andere Oberflächen reinigen; Haushaltsmüll zu sammeln und zu entsorgen; Zimmer, Toiletten und Schubladen in Ordnung zu halten; schmutzige Kleidung zu sammeln, zu waschen, zu trocknen, zusammenzulegen und zu bügeln; Schuhwerk zu reinigen; Besen, Bürsten und Staubsauger, Waschmaschinen, Trockner und Bügeleisen zu benutzen" 
* #d640 ^property[1].code = #None 
* #d640 ^property[1].valueString = "Kleidung und Wäsche waschen und trocknen; Küchenbereich und -utensilien reinigen; den Wohnraum reinigen; Haushaltsgeräte benutzen, die täglichen Lebensnotwendigkeiten lagern und Müll entsorgen" 
* #d640 ^property[2].code = #None 
* #d640 ^property[2].valueString = "Wohnraum beschaffen" 
* #d640 ^property[3].code = #parent 
* #d640 ^property[3].valueCode = #d630-d649 
* #d640 ^property[4].code = #child 
* #d640 ^property[4].valueCode = #d6400 
* #d640 ^property[5].code = #child 
* #d640 ^property[5].valueCode = #d6401 
* #d640 ^property[6].code = #child 
* #d640 ^property[6].valueCode = #d6402 
* #d640 ^property[7].code = #child 
* #d640 ^property[7].valueCode = #d6403 
* #d640 ^property[8].code = #child 
* #d640 ^property[8].valueCode = #d6404 
* #d640 ^property[9].code = #child 
* #d640 ^property[9].valueCode = #d6405 
* #d640 ^property[10].code = #child 
* #d640 ^property[10].valueCode = #d6408 
* #d640 ^property[11].code = #child 
* #d640 ^property[11].valueCode = #d6409 
* #d6400 "Kleidung und Wäsche waschen und trocknen"
* #d6400 ^property[0].code = #None 
* #d6400 ^property[0].valueString = "Kleidung und Wäsche mit der Hand zu waschen und sie zum Trocknen an der Luft aufzuhängen" 
* #d6400 ^property[1].code = #parent 
* #d6400 ^property[1].valueCode = #d640 
* #d6401 "Küchenbereich und -utensilien reinigen"
* #d6401 ^property[0].code = #None 
* #d6401 ^property[0].valueString = "Nach dem Kochen zu reinigen, wie Geschirr, Pfannen, Töpfe und Kochutensilien abwaschen sowie Tische und Böden des Koch- und Essbereichs reinigen" 
* #d6401 ^property[1].code = #parent 
* #d6401 ^property[1].valueCode = #d640 
* #d6402 "Den Wohnbereich reinigen"
* #d6402 ^property[0].code = #None 
* #d6402 ^property[0].valueString = "Den Wohnbereich eines Haushalts zu reinigen, wie aufräumen und Staub wischen; Fußböden fegen, wischen, moppen; Fenster und Wände reinigen; Badezimmer und Toiletten reinigen; Möbel reinigen" 
* #d6402 ^property[1].code = #parent 
* #d6402 ^property[1].valueCode = #d640 
* #d6403 "Haushaltsgeräte benutzen"
* #d6403 ^property[0].code = #None 
* #d6403 ^property[0].valueString = "Alle Arten von Haushaltsgeräten zu benutzen, wie Waschmaschinen, Trockner, Bügeleisen, Staubsauger und Spülmaschinen" 
* #d6403 ^property[1].code = #parent 
* #d6403 ^property[1].valueCode = #d640 
* #d6404 "Die täglichen Lebensnotwendigkeiten lagern"
* #d6404 ^property[0].code = #None 
* #d6404 ^property[0].valueString = "Lebensmittel, Getränke, Kleidung und andere für das tägliche Leben notwendigen Waren zu lagern, Lebensmittel für die Konservierung durch Einmachen, Salzen oder Einfrieren vorzubereiten, Lebensmittel frisch zu halten und für Tiere nicht erreichbar aufzubewahren" 
* #d6404 ^property[1].code = #parent 
* #d6404 ^property[1].valueCode = #d640 
* #d6405 "Müll entsorgen"
* #d6405 ^property[0].code = #None 
* #d6405 ^property[0].valueString = "Den Haushaltsmüll zu entsorgen, wie Abfall und Unrat um das Haus herum aufsammeln, Müll mit geeigneten Mitteln zur Entsorgung vorbereiten, Müll verbrennen" 
* #d6405 ^property[1].code = #parent 
* #d6405 ^property[1].valueCode = #d640 
* #d6408 "Hausarbeiten erledigen, anders bezeichnet"
* #d6408 ^property[0].code = #parent 
* #d6408 ^property[0].valueCode = #d640 
* #d6409 "Hausarbeiten erledigen, nicht näher bezeichnet"
* #d6409 ^property[0].code = #parent 
* #d6409 ^property[0].valueCode = #d640 
* #d649 "Haushaltsaufgaben, anders oder nicht näher bezeichnet"
* #d649 ^property[0].code = #parent 
* #d649 ^property[0].valueCode = #d630-d649 
* #d650-d669 "Haushaltsgegenstände pflegen und anderen helfen"
* #d650-d669 ^property[0].code = #parent 
* #d650-d669 ^property[0].valueCode = #d6 
* #d650-d669 ^property[1].code = #child 
* #d650-d669 ^property[1].valueCode = #d650 
* #d650-d669 ^property[2].code = #child 
* #d650-d669 ^property[2].valueCode = #d660 
* #d650-d669 ^property[3].code = #child 
* #d650-d669 ^property[3].valueCode = #d669 
* #d650 "Haushaltsgegenstände pflegen"
* #d650 ^property[0].code = #None 
* #d650 ^property[0].valueString = "Haushalts- und andere persönliche Gegenstände, einschließlich Haus und dessen Inhalt, Kleidung, Fahrzeuge und Hilfsmittel, instand halten und instand setzen sowie sich um Pflanzen und Tiere kümmern, wie Räume anstreichen und tapezieren, Einrichtungsgegenstände befestigen, Wasserleitungen instand setzen, die Funktionsfähigkeit von Fahrzeugen sicherstellen, Pflanzen gießen, Haus- und Nutztiere pflegen und füttern" 
* #d650 ^property[1].code = #None 
* #d650 ^property[1].valueString = "Kleidung herstellen und reparieren; Wohnung, Möbel und häusliche Geräte instand halten; Fahrzeuge instand halten; Hilfsmittel instand halten; Pflanzen (drinnen und draußen) und Tiere pflegen" 
* #d650 ^property[2].code = #None 
* #d650 ^property[2].valueString = "Wohnraum beschaffen" 
* #d650 ^property[3].code = #parent 
* #d650 ^property[3].valueCode = #d650-d669 
* #d650 ^property[4].code = #child 
* #d650 ^property[4].valueCode = #d6500 
* #d650 ^property[5].code = #child 
* #d650 ^property[5].valueCode = #d6501 
* #d650 ^property[6].code = #child 
* #d650 ^property[6].valueCode = #d6502 
* #d650 ^property[7].code = #child 
* #d650 ^property[7].valueCode = #d6503 
* #d650 ^property[8].code = #child 
* #d650 ^property[8].valueCode = #d6504 
* #d650 ^property[9].code = #child 
* #d650 ^property[9].valueCode = #d6505 
* #d650 ^property[10].code = #child 
* #d650 ^property[10].valueCode = #d6506 
* #d650 ^property[11].code = #child 
* #d650 ^property[11].valueCode = #d6508 
* #d650 ^property[12].code = #child 
* #d650 ^property[12].valueCode = #d6509 
* #d6500 "Kleidung herstellen und reparieren"
* #d6500 ^property[0].code = #None 
* #d6500 ^property[0].valueString = "Kleidung herzustellen und zu reparieren, wie nähen, Kleidung anfertigen oder ausbessern; Knöpfe und Verschlüsse wieder befestigen; Kleidungsstücke bügeln; Schuhwerk in Ordnung bringen und putzen" 
* #d6500 ^property[1].code = #None 
* #d6500 ^property[1].valueString = "Haushaltsgeräte benutzen" 
* #d6500 ^property[2].code = #parent 
* #d6500 ^property[2].valueCode = #d650 
* #d6501 "Wohnung und Möbel instand halten"
* #d6501 ^property[0].code = #None 
* #d6501 ^property[0].valueString = "Die Wohnung, deren Außenbereich und Innenbereich samt Inhalt instand zu setzen und zu halten, wie Einrichtungsgegenstände und Möbel streichen und reparieren sowie die erforderlichen Werkzeuge und Materialien für die Reparatur benutzen" 
* #d6501 ^property[1].code = #parent 
* #d6501 ^property[1].valueCode = #d650 
* #d6502 "Häusliche Geräte instand halten"
* #d6502 ^property[0].code = #None 
* #d6502 ^property[0].valueString = "Alle häuslichen Geräte zum Kochen, Reinigen und Reparieren instand zu setzen und zu halten, wie Werkzeuge ölen und reparieren sowie die Waschmaschine warten" 
* #d6502 ^property[1].code = #parent 
* #d6502 ^property[1].valueCode = #d650 
* #d6503 "Fahrzeuge instand halten"
* #d6503 ^property[0].code = #None 
* #d6503 ^property[0].valueString = "Motorisierte und nicht motorisierte Fahrzeuge für die persönliche Benutzung instand zu setzen und zu halten, einschließlich Fahrräder, Wagen, Autos und Boote" 
* #d6503 ^property[1].code = #parent 
* #d6503 ^property[1].valueCode = #d650 
* #d6504 "Hilfsmittel instand halten"
* #d6504 ^property[0].code = #None 
* #d6504 ^property[0].valueString = "Hilfsmittel instand zu setzen und zu halten, wie Prothesen, Orthesen, Spezialwerkzeuge und Hilfen für die Haushaltsführung und die persönliche Pflege, Hilfen für die persönliche Mobilität wie Gehstützen, Gehwagen, Rollstühle und Roller instand setzen und instand halten; Hilfen zur Kommunikation und Erholung instand halten" 
* #d6504 ^property[1].code = #parent 
* #d6504 ^property[1].valueCode = #d650 
* #d6505 "Innen- und Außenpflanzen pflegen"
* #d6505 ^property[0].code = #None 
* #d6505 ^property[0].valueString = "Pflanzen innerhalb und außerhalb des Hauses zu pflegen, wie Pflanzen anpflanzen, gießen und düngen, Gartenarbeit machen sowie Nutzpflanzen für den persönlichen Bedarf anpflanzen" 
* #d6505 ^property[1].code = #parent 
* #d6505 ^property[1].valueCode = #d650 
* #d6506 "Sich um Tiere kümmern"
* #d6506 ^property[0].code = #None 
* #d6506 ^property[0].valueString = "Sich um Nutz- und Haustiere zu kümmern, wie Haustiere füttern, pflegen, reinigen, bürsten sowie für deren Bewegung sorgen; auf die Gesundheit von Nutz- und Haustieren achten; die Pflege von Nutz- und Haustieren für den Fall der eigenen Abwesenheit planen" 
* #d6506 ^property[1].code = #parent 
* #d6506 ^property[1].valueCode = #d650 
* #d6508 "Haushaltsgegenstände pflegen, anders bezeichnet"
* #d6508 ^property[0].code = #parent 
* #d6508 ^property[0].valueCode = #d650 
* #d6509 "Haushaltsgegenstände pflegen, nicht näher bezeichnet"
* #d6509 ^property[0].code = #parent 
* #d6509 ^property[0].valueCode = #d650 
* #d660 "Anderen helfen"
* #d660 ^property[0].code = #None 
* #d660 ^property[0].valueString = "Haushaltsmitgliedern und anderen beim Lernen, Kommunizieren, der Selbstversorgung, der (Fort-)Bewegung innerhalb und außerhalb des Hauses zu helfen; sich dem Wohlbefinden der Haushaltsmitglieder und anderer widmen" 
* #d660 ^property[1].code = #None 
* #d660 ^property[1].valueString = "Anderen bei der Selbstversorgung, der (Fort)Bewegung, Kommunikation, den interpersonellen Beziehungen, der Ernährung und der Erhaltung der Gesundheit helfen" 
* #d660 ^property[2].code = #None 
* #d660 ^property[2].valueString = "Bezahlte Tätigkeit" 
* #d660 ^property[3].code = #parent 
* #d660 ^property[3].valueCode = #d650-d669 
* #d660 ^property[4].code = #child 
* #d660 ^property[4].valueCode = #d6600 
* #d660 ^property[5].code = #child 
* #d660 ^property[5].valueCode = #d6601 
* #d660 ^property[6].code = #child 
* #d660 ^property[6].valueCode = #d6602 
* #d660 ^property[7].code = #child 
* #d660 ^property[7].valueCode = #d6603 
* #d660 ^property[8].code = #child 
* #d660 ^property[8].valueCode = #d6604 
* #d660 ^property[9].code = #child 
* #d660 ^property[9].valueCode = #d6605 
* #d660 ^property[10].code = #child 
* #d660 ^property[10].valueCode = #d6608 
* #d660 ^property[11].code = #child 
* #d660 ^property[11].valueCode = #d6609 
* #d6600 "Anderen bei der Selbstversorgung helfen"
* #d6600 ^property[0].code = #None 
* #d6600 ^property[0].valueString = "Haushaltsmitgliedern und anderen bei der Selbstversorgung helfen, einschließlich anderen beim Essen, Baden sowie An- und Ausziehen helfen; sich um Kinder oder andere Haushaltsmitglieder kümmern, die krank sind oder Schwierigkeiten bei der elementaren Selbstversorgung haben; anderen bei Belangen der Toilette helfen" 
* #d6600 ^property[1].code = #parent 
* #d6600 ^property[1].valueCode = #d660 
* #d6601 "Anderen bei der (Fort)Bewegung helfen"
* #d6601 ^property[0].code = #None 
* #d6601 ^property[0].valueString = "Haushaltsmitgliedern und anderen bei ihren Bewegungen und beim Verlassen des Hauses zu helfen, wie bei ihrem Unterwegssein in der Nachbarschaft oder der Stadt, in die oder von der Schule, zum Arbeitsplatz oder zu einem anderen Ziel helfen" 
* #d6601 ^property[1].code = #parent 
* #d6601 ^property[1].valueCode = #d660 
* #d6602 "Anderen bei der Kommunikation helfen"
* #d6602 ^property[0].code = #None 
* #d6602 ^property[0].valueString = "Haushaltsmitgliedern und anderen bei ihrer Kommunikation zu helfen, wie beim Sprechen, Schreiben oder Lesen helfen" 
* #d6602 ^property[1].code = #parent 
* #d6602 ^property[1].valueCode = #d660 
* #d6603 "Anderen bei interpersonellen Beziehungen helfen"
* #d6603 ^property[0].code = #None 
* #d6603 ^property[0].valueString = "Haushaltsmitgliedern und anderen bei ihren interpersonellen Interaktionen zu helfen, wie ihnen beim Aufnehmen, Aufrechterhalten und Beenden von Beziehungen helfen" 
* #d6603 ^property[1].code = #parent 
* #d6603 ^property[1].valueCode = #d660 
* #d6604 "Anderen bei der Ernährung helfen"
* #d6604 ^property[0].code = #None 
* #d6604 ^property[0].valueString = "Haushaltsmitgliedern und anderen bei ihrer Ernährung zu helfen, wie ihnen beim Zubereiten von Mahlzeiten und Essen helfen" 
* #d6604 ^property[1].code = #parent 
* #d6604 ^property[1].valueCode = #d660 
* #d6605 "Anderen bei der Erhaltung ihrer Gesundheit helfen"
* #d6605 ^property[0].code = #None 
* #d6605 ^property[0].valueString = "Haushaltsmitgliedern und anderen bei ihrer medizinischen Versorgung durch Professionelle und Laien zu helfen, wie darauf achten, dass ein Kind regelmäßige medizinische Check-Ups erhält oder dass ein älterer Verwandter die erforderlichen Medikamente einnimmt" 
* #d6605 ^property[1].code = #parent 
* #d6605 ^property[1].valueCode = #d660 
* #d6608 "Anderen helfen, anders bezeichnet"
* #d6608 ^property[0].code = #parent 
* #d6608 ^property[0].valueCode = #d660 
* #d6609 "Anderen helfen, nicht näher bezeichnet"
* #d6609 ^property[0].code = #parent 
* #d6609 ^property[0].valueCode = #d660 
* #d669 "Haushaltsgegenstände pflegen und anderen helfen, anders oder nicht näher bezeichnet"
* #d669 ^property[0].code = #parent 
* #d669 ^property[0].valueCode = #d650-d669 
* #d698 "Häusliches Leben, anders bezeichnet"
* #d698 ^property[0].code = #parent 
* #d698 ^property[0].valueCode = #d6 
* #d699 "Häusliches Leben, nicht näher bezeichnet"
* #d699 ^property[0].code = #parent 
* #d699 ^property[0].valueCode = #d6 
* #d7 "Interpersonelle Interaktionen und Beziehungen"
* #d7 ^property[0].code = #None 
* #d7 ^property[0].valueString = "Dieses Kapitel befasst sich mit der Ausführung von Handlungen und Aufgaben, die für die elementaren und komplexen Interaktionen mit Menschen (Fremden, Freunden, Verwandten, Familienmitgliedern und Liebespartnern) in einer kontextuell und sozial angemessenen Weise erforderlich sind." 
* #d7 ^property[1].code = #parent 
* #d7 ^property[1].valueCode = #d 
* #d7 ^property[2].code = #child 
* #d7 ^property[2].valueCode = #d710-d729 
* #d7 ^property[3].code = #child 
* #d7 ^property[3].valueCode = #d730-d779 
* #d7 ^property[4].code = #child 
* #d7 ^property[4].valueCode = #d798 
* #d7 ^property[5].code = #child 
* #d7 ^property[5].valueCode = #d799 
* #d710-d729 "Allgemeine interpersonelle Interaktionen"
* #d710-d729 ^property[0].code = #parent 
* #d710-d729 ^property[0].valueCode = #d7 
* #d710-d729 ^property[1].code = #child 
* #d710-d729 ^property[1].valueCode = #d710 
* #d710-d729 ^property[2].code = #child 
* #d710-d729 ^property[2].valueCode = #d720 
* #d710-d729 ^property[3].code = #child 
* #d710-d729 ^property[3].valueCode = #d729 
* #d710 "Elementare interpersonelle Aktivitäten"
* #d710 ^property[0].code = #None 
* #d710 ^property[0].valueString = "Mit anderen in einer kontextuell und sozial angemessenen Weise zu interagieren, wie die erforderliche Rücksichtnahme und Wertschätzung zeigen oder auf Gefühle anderer reagieren" 
* #d710 ^property[1].code = #None 
* #d710 ^property[1].valueString = "Respekt, Wärme, Wertschätzung und Toleranz in Beziehungen zeigen; auf Kritik und soziale Zeichen in Beziehungen reagieren und angemessenen körperlichen Kontakt einzusetzen" 
* #d710 ^property[2].code = #parent 
* #d710 ^property[2].valueCode = #d710-d729 
* #d710 ^property[3].code = #child 
* #d710 ^property[3].valueCode = #d7100 
* #d710 ^property[4].code = #child 
* #d710 ^property[4].valueCode = #d7101 
* #d710 ^property[5].code = #child 
* #d710 ^property[5].valueCode = #d7102 
* #d710 ^property[6].code = #child 
* #d710 ^property[6].valueCode = #d7103 
* #d710 ^property[7].code = #child 
* #d710 ^property[7].valueCode = #d7104 
* #d710 ^property[8].code = #child 
* #d710 ^property[8].valueCode = #d7105 
* #d710 ^property[9].code = #child 
* #d710 ^property[9].valueCode = #d7108 
* #d710 ^property[10].code = #child 
* #d710 ^property[10].valueCode = #d7109 
* #d7100 "Respekt und Wärme in Beziehungen"
* #d7100 ^property[0].code = #None 
* #d7100 ^property[0].valueString = "In einer kontextuell und sozial angemessenen Weise Rücksichtnahme und Wertschätzung zu zeigen und darauf zu reagieren" 
* #d7100 ^property[1].code = #parent 
* #d7100 ^property[1].valueCode = #d710 
* #d7101 "Anerkennung in Beziehungen"
* #d7101 ^property[0].code = #None 
* #d7101 ^property[0].valueString = "In einer kontextuell und sozial angemessenen Weise Zufriedenheit und Dankbarkeit zu zeigen und darauf zu reagieren" 
* #d7101 ^property[1].code = #parent 
* #d7101 ^property[1].valueCode = #d710 
* #d7102 "Toleranz in Beziehungen"
* #d7102 ^property[0].code = #None 
* #d7102 ^property[0].valueString = "In einer kontextuell und sozial angemessenen Weise Verständnis und Akzeptanz für Verhalten zu zeigen und darauf zu reagieren" 
* #d7102 ^property[1].code = #parent 
* #d7102 ^property[1].valueCode = #d710 
* #d7103 "Kritik in Beziehungen"
* #d7103 ^property[0].code = #None 
* #d7103 ^property[0].valueString = "In einer kontextuell und sozial angemessenen Weise implizite und explizite Meinungsverschiedenheiten oder Uneinigkeit auszudrücken und darauf zu reagieren" 
* #d7103 ^property[1].code = #parent 
* #d7103 ^property[1].valueCode = #d710 
* #d7104 "Soziale Zeichen in Beziehungen"
* #d7104 ^property[0].code = #None 
* #d7104 ^property[0].valueString = "Zeichen und Hinweise, die bei sozialen Interaktionen vorkommen, in angemessener Weise zu geben und darauf zu reagieren" 
* #d7104 ^property[1].code = #parent 
* #d7104 ^property[1].valueCode = #d710 
* #d7105 "Körperlicher Kontakt in Beziehungen"
* #d7105 ^property[0].code = #None 
* #d7105 ^property[0].valueString = "In einer kontextuell und sozial angemessenen Weise mit anderen körperlichen Kontakt aufzunehmen und darauf zu reagieren" 
* #d7105 ^property[1].code = #parent 
* #d7105 ^property[1].valueCode = #d710 
* #d7108 "Elementare interpersonelle Aktivitäten, anders bezeichnet"
* #d7108 ^property[0].code = #parent 
* #d7108 ^property[0].valueCode = #d710 
* #d7109 "Elementare interpersonelle Aktivitäten, nicht näher bezeichnet"
* #d7109 ^property[0].code = #parent 
* #d7109 ^property[0].valueCode = #d710 
* #d720 "Komplexe interpersonelle Interaktionen"
* #d720 ^property[0].code = #None 
* #d720 ^property[0].valueString = "Die Interaktionen mit anderen in einer kontextuell und sozial angemessenen Weise aufrechtzuerhalten und zu handhaben, wie Gefühle und Impulse steuern, verbale und physische Aggressionen kontrollieren, bei sozialen Interaktionen unabhängig handeln und in Übereinstimmung mit sozialen Regeln und Konventionen handeln" 
* #d720 ^property[1].code = #None 
* #d720 ^property[1].valueString = "Beziehungen eingehen und beenden; Verhaltensweisen bei Interaktionen regulieren; sozialen Regeln gemäß interagieren und sozialen Abstand wahren" 
* #d720 ^property[2].code = #parent 
* #d720 ^property[2].valueCode = #d710-d729 
* #d720 ^property[3].code = #child 
* #d720 ^property[3].valueCode = #d7200 
* #d720 ^property[4].code = #child 
* #d720 ^property[4].valueCode = #d7201 
* #d720 ^property[5].code = #child 
* #d720 ^property[5].valueCode = #d7202 
* #d720 ^property[6].code = #child 
* #d720 ^property[6].valueCode = #d7203 
* #d720 ^property[7].code = #child 
* #d720 ^property[7].valueCode = #d7204 
* #d720 ^property[8].code = #child 
* #d720 ^property[8].valueCode = #d7208 
* #d720 ^property[9].code = #child 
* #d720 ^property[9].valueCode = #d7209 
* #d7200 "Beziehungen eingehen"
* #d7200 ^property[0].code = #None 
* #d7200 ^property[0].valueString = "In einer kontextuell und sozial angemessenen Weise mit anderen Interaktionen für kurze oder längere Zeiträume zu beginnen und aufrecht zu erhalten, wie sich vorstellen, Freundschaften schließen und berufliche Beziehungen herstellen, eine mögliche Dauer-, Liebes- oder intime Beziehung beginnen" 
* #d7200 ^property[1].code = #parent 
* #d7200 ^property[1].valueCode = #d720 
* #d7201 "Beziehungen beenden"
* #d7201 ^property[0].code = #None 
* #d7201 ^property[0].valueString = "In einer kontextuell und sozial angemessenen Weise Interaktionen zu beenden, wie zeitlich begrenzte Beziehungen am Ende eines Besuches beenden, längerfristige Beziehungen mit Freunden, die in eine andere Stadt ziehen, beenden oder Beziehungen mit Arbeits-, Berufskollegen oder Dienstleistungserbringern beenden sowie Liebes- oder intime Beziehungen beenden" 
* #d7201 ^property[1].code = #parent 
* #d7201 ^property[1].valueCode = #d720 
* #d7202 "Verhalten in Beziehungen regulieren"
* #d7202 ^property[0].code = #None 
* #d7202 ^property[0].valueString = "In einer kontextuell und sozial angemessenen Weise Gefühle und Impulse, verbale und physische Aggressionen zu regulieren" 
* #d7202 ^property[1].code = #parent 
* #d7202 ^property[1].valueCode = #d720 
* #d7203 "Sozialen Regeln gemäß interagieren"
* #d7203 ^property[0].code = #None 
* #d7203 ^property[0].valueString = "In sozialen Interaktionen unabhängig zu handeln und sich nach den sozialen Konventionen, die die eigene Rolle, Stellung oder einen anderen sozialen Status bei Interaktionen mit anderen bestimmen, zu richten" 
* #d7203 ^property[1].code = #parent 
* #d7203 ^property[1].valueCode = #d720 
* #d7204 "Sozialen Abstand wahren"
* #d7204 ^property[0].code = #None 
* #d7204 ^property[0].valueString = "In einer kontextuell, sozial und kulturell angemessenen Weise sich über den Abstand zwischen sich und anderen bewusst zu sein und diesen zu wahren" 
* #d7204 ^property[1].code = #parent 
* #d7204 ^property[1].valueCode = #d720 
* #d7208 "Komplexe interpersonelle Interaktionen, anders bezeichnet"
* #d7208 ^property[0].code = #parent 
* #d7208 ^property[0].valueCode = #d720 
* #d7209 "Komplexe interpersonelle Interaktionen, nicht näher bezeichnet"
* #d7209 ^property[0].code = #parent 
* #d7209 ^property[0].valueCode = #d720 
* #d729 "Allgemeine interpersonelle Interaktionen, anders oder nicht näher bezeichnet"
* #d729 ^property[0].code = #parent 
* #d729 ^property[0].valueCode = #d710-d729 
* #d730-d779 "Besondere interpersonelle Beziehungen"
* #d730-d779 ^property[0].code = #parent 
* #d730-d779 ^property[0].valueCode = #d7 
* #d730-d779 ^property[1].code = #child 
* #d730-d779 ^property[1].valueCode = #d730 
* #d730-d779 ^property[2].code = #child 
* #d730-d779 ^property[2].valueCode = #d740 
* #d730-d779 ^property[3].code = #child 
* #d730-d779 ^property[3].valueCode = #d750 
* #d730-d779 ^property[4].code = #child 
* #d730-d779 ^property[4].valueCode = #d760 
* #d730-d779 ^property[5].code = #child 
* #d730-d779 ^property[5].valueCode = #d770 
* #d730-d779 ^property[6].code = #child 
* #d730-d779 ^property[6].valueCode = #d779 
* #d730 "Mit Fremden umgehen"
* #d730 ^property[0].code = #None 
* #d730 ^property[0].valueString = "In befristeten Kontakten und Verbindungen mit Fremden zu bestimmten Zwecken zu stehen, wie beim Fragen nach einer Richtung oder einen Kauf tätigen" 
* #d730 ^property[1].code = #parent 
* #d730 ^property[1].valueCode = #d730-d779 
* #d740 "Formelle Beziehungen"
* #d740 ^property[0].code = #None 
* #d740 ^property[0].valueString = "Spezielle Beziehungen in formellen Rahmen aufzunehmen und aufrecht zu erhalten, wie mit Arbeitgebern, Fachleuten oder Dienstleistungserbringer" 
* #d740 ^property[1].code = #None 
* #d740 ^property[1].valueString = "Mit Autoritätspersonen, Untergebenen oder Gleichrangigen umgehen" 
* #d740 ^property[2].code = #parent 
* #d740 ^property[2].valueCode = #d730-d779 
* #d740 ^property[3].code = #child 
* #d740 ^property[3].valueCode = #d7400 
* #d740 ^property[4].code = #child 
* #d740 ^property[4].valueCode = #d7401 
* #d740 ^property[5].code = #child 
* #d740 ^property[5].valueCode = #d7402 
* #d740 ^property[6].code = #child 
* #d740 ^property[6].valueCode = #d7408 
* #d740 ^property[7].code = #child 
* #d740 ^property[7].valueCode = #d7409 
* #d7400 "Mit Autoritätspersonen umgehen"
* #d7400 ^property[0].code = #None 
* #d7400 ^property[0].valueString = "Formelle Beziehungen mit Menschen in Machtpositionen, höheren Ranges oder Prestiges als der eigenen sozialen Position aufzunehmen und aufrecht zu erhalten, wie mit einem Arbeitgeber" 
* #d7400 ^property[1].code = #parent 
* #d7400 ^property[1].valueCode = #d740 
* #d7401 "Mit Untergebenen umgehen"
* #d7401 ^property[0].code = #None 
* #d7401 ^property[0].valueString = "Formelle Beziehungen mit Menschen niedrigeren Ranges oder Prestiges als der eigenen sozialen Position aufzunehmen und aufrecht zu erhalten, wie mit einem Beschäftigten oder Bediensteten" 
* #d7401 ^property[1].code = #parent 
* #d7401 ^property[1].valueCode = #d740 
* #d7402 "Mit Gleichrangigen umgehen"
* #d7402 ^property[0].code = #None 
* #d7402 ^property[0].valueString = "Formelle Beziehungen mit Menschen gleichen Ranges, Prestiges oder gleicher Autorität wie der eigenen sozialen Position aufzunehmen und aufrecht zu erhalten" 
* #d7402 ^property[1].code = #parent 
* #d7402 ^property[1].valueCode = #d740 
* #d7408 "Formelle Beziehungen, anders bezeichnet"
* #d7408 ^property[0].code = #parent 
* #d7408 ^property[0].valueCode = #d740 
* #d7409 "Formelle Beziehungen, nicht näher bezeichnet"
* #d7409 ^property[0].code = #parent 
* #d7409 ^property[0].valueCode = #d740 
* #d750 "Informelle soziale Beziehungen"
* #d750 ^property[0].code = #None 
* #d750 ^property[0].valueString = "Mit anderen Kontakte aufzunehmen, wie bei gelegentlichen Beziehungen mit Leuten, die in derselben Gemeinschaft oder am selben Wohnsitz leben, oder mit Mitarbeitern, Schülern und Studenten, Spielkameraden oder mit Menschen ähnlichen Hintergrundes oder Berufs" 
* #d750 ^property[1].code = #None 
* #d750 ^property[1].valueString = "Informelle Beziehungen zu Freunden, Nachbarn, Bekannten, Mitbewohnern und Seinesgleichen (Peers)" 
* #d750 ^property[2].code = #parent 
* #d750 ^property[2].valueCode = #d730-d779 
* #d750 ^property[3].code = #child 
* #d750 ^property[3].valueCode = #d7500 
* #d750 ^property[4].code = #child 
* #d750 ^property[4].valueCode = #d7501 
* #d750 ^property[5].code = #child 
* #d750 ^property[5].valueCode = #d7502 
* #d750 ^property[6].code = #child 
* #d750 ^property[6].valueCode = #d7503 
* #d750 ^property[7].code = #child 
* #d750 ^property[7].valueCode = #d7504 
* #d750 ^property[8].code = #child 
* #d750 ^property[8].valueCode = #d7508 
* #d750 ^property[9].code = #child 
* #d750 ^property[9].valueCode = #d7509 
* #d7500 "Informelle Beziehungen zu Freunden"
* #d7500 ^property[0].code = #None 
* #d7500 ^property[0].valueString = "Freundschaftliche Beziehungen, die durch gegenseitige Wertschätzung und gemeinsame Interessen geprägt sind, aufzunehmen und aufrecht zu erhalten" 
* #d7500 ^property[1].code = #parent 
* #d7500 ^property[1].valueCode = #d750 
* #d7501 "Informelle Beziehungen zu Nachbarn"
* #d7501 ^property[0].code = #None 
* #d7501 ^property[0].valueString = "Informelle Beziehungen zu Menschen in der Nachbarschaft aufzunehmen und aufrecht zu erhalten" 
* #d7501 ^property[1].code = #parent 
* #d7501 ^property[1].valueCode = #d750 
* #d7502 "Informelle Beziehungen zu Bekannten"
* #d7502 ^property[0].code = #None 
* #d7502 ^property[0].valueString = "Informelle Beziehungen zu Menschen, die man kennt, die jedoch nicht zum engeren Freundeskreis zählen, aufzunehmen und aufrecht zu erhalten" 
* #d7502 ^property[1].code = #parent 
* #d7502 ^property[1].valueCode = #d750 
* #d7503 "Informelle Beziehungen zu Mitbewohnern"
* #d7503 ^property[0].code = #None 
* #d7503 ^property[0].valueString = "Informelle Beziehungen für jeden Zweck zu Menschen, die im gleichen privaten oder öffentlich geleiteten Haus oder anderen Wohnung leben, aufzunehmen und aufrecht zu erhalten" 
* #d7503 ^property[1].code = #parent 
* #d7503 ^property[1].valueCode = #d750 
* #d7504 "Informelle Beziehungen zu Seinesgleichen (Peers)"
* #d7504 ^property[0].code = #None 
* #d7504 ^property[0].valueString = "Informelle Beziehungen für jeden Zweck zu Menschen im gleichen Alter, mit gleichen Interessen oder anderen gemeinsamen Merkmalen aufzunehmen und aufrecht zu erhalten" 
* #d7504 ^property[1].code = #parent 
* #d7504 ^property[1].valueCode = #d750 
* #d7508 "Informelle soziale Beziehungen, anders bezeichnet"
* #d7508 ^property[0].code = #parent 
* #d7508 ^property[0].valueCode = #d750 
* #d7509 "Informelle soziale Beziehungen, nicht näher bezeichnet"
* #d7509 ^property[0].code = #parent 
* #d7509 ^property[0].valueCode = #d750 
* #d760 "Familienbeziehungen"
* #d760 ^property[0].code = #None 
* #d760 ^property[0].valueString = "Beziehungen zu Verwandten aufzubauen und aufrecht zu erhalten, wie mit Mitgliedern der Kernfamilie, des erweiterten Familienkreises, der Pflege- und angenommenen Familie sowie der Stieffamilie, mit entfernteren Verwandten wie mit Cousinen/Cousins zweiten Grades, oder zum Vormund" 
* #d760 ^property[1].code = #None 
* #d760 ^property[1].valueString = "Eltern-Kind- und Kind-Eltern-Beziehungen, Beziehungen unter Kindern und Beziehungen zum erweiterten Familienkreis" 
* #d760 ^property[2].code = #parent 
* #d760 ^property[2].valueCode = #d730-d779 
* #d760 ^property[3].code = #child 
* #d760 ^property[3].valueCode = #d7600 
* #d760 ^property[4].code = #child 
* #d760 ^property[4].valueCode = #d7601 
* #d760 ^property[5].code = #child 
* #d760 ^property[5].valueCode = #d7602 
* #d760 ^property[6].code = #child 
* #d760 ^property[6].valueCode = #d7603 
* #d760 ^property[7].code = #child 
* #d760 ^property[7].valueCode = #d7608 
* #d760 ^property[8].code = #child 
* #d760 ^property[8].valueCode = #d7609 
* #d7600 "Eltern-Kind-Beziehungen"
* #d7600 ^property[0].code = #None 
* #d7600 ^property[0].valueString = "Auf natürliche Weise oder durch Adoption zu einem Elternteil zu werden oder dies zu sein, wie ein Kind zu haben und mit ihm in elterlicher Beziehung stehen oder eine elterliche Beziehung mit einem Adoptivkind aufbauen und aufrecht erhalten sowie dem eigenen Kind oder dem Adoptivkind physische, intellektuelle und emotionale Zuwendung geben" 
* #d7600 ^property[1].code = #parent 
* #d7600 ^property[1].valueCode = #d760 
* #d7601 "Kind-Eltern-Beziehung"
* #d7601 ^property[0].code = #None 
* #d7601 ^property[0].valueString = "Mit seinen Eltern Beziehungen aufzubauen und aufrecht zu erhalten, wie als junges Kind seinen Eltern gehorchen und sich als erwachsenes Kind um seine alten Eltern kümmern" 
* #d7601 ^property[1].code = #parent 
* #d7601 ^property[1].valueCode = #d760 
* #d7602 "Beziehungen unter Geschwistern"
* #d7602 ^property[0].code = #None 
* #d7602 ^property[0].valueString = "Eine geschwisterliche Beziehung zu einer Person aufzubauen und aufrecht zu erhalten, die ein oder beide Elternteile kraft Geburt, durch Adoption oder Heirat gemeinsam hat" 
* #d7602 ^property[1].code = #parent 
* #d7602 ^property[1].valueCode = #d760 
* #d7603 "Beziehungen zum erweiterten Familienkreis"
* #d7603 ^property[0].code = #None 
* #d7603 ^property[0].valueString = "Eine Familienbeziehung zu Mitgliedern des eigenen erweiterten Familienkreises aufzubauen und aufrecht zu erhalten, wie mit Cousinen und Cousins, Tanten, Onkeln und Großeltern" 
* #d7603 ^property[1].code = #parent 
* #d7603 ^property[1].valueCode = #d760 
* #d7608 "Familienbeziehungen, anders bezeichnet"
* #d7608 ^property[0].code = #parent 
* #d7608 ^property[0].valueCode = #d760 
* #d7609 "Familienbeziehungen, nicht näher bezeichnet"
* #d7609 ^property[0].code = #parent 
* #d7609 ^property[0].valueCode = #d760 
* #d770 "Intime Beziehungen"
* #d770 ^property[0].code = #None 
* #d770 ^property[0].valueString = "Intime oder Liebesbeziehungen zwischen Individuen aufzubauen und aufrecht zu erhalten, wie zwischen Ehemann und -frau, sich Liebenden oder Sexualpartnern" 
* #d770 ^property[1].code = #None 
* #d770 ^property[1].valueString = "Liebes-, eheliche und Sexualbeziehungen" 
* #d770 ^property[2].code = #parent 
* #d770 ^property[2].valueCode = #d730-d779 
* #d770 ^property[3].code = #child 
* #d770 ^property[3].valueCode = #d7700 
* #d770 ^property[4].code = #child 
* #d770 ^property[4].valueCode = #d7701 
* #d770 ^property[5].code = #child 
* #d770 ^property[5].valueCode = #d7702 
* #d770 ^property[6].code = #child 
* #d770 ^property[6].valueCode = #d7708 
* #d770 ^property[7].code = #child 
* #d770 ^property[7].valueCode = #d7709 
* #d7700 "Liebesbeziehungen"
* #d7700 ^property[0].code = #None 
* #d7700 ^property[0].valueString = "Beziehungen auf der Grundlage emotionaler und physischer Anziehung, die zu längerfristigen engen Beziehungen führen können, aufzubauen und aufrecht zu erhalten" 
* #d7700 ^property[1].code = #parent 
* #d7700 ^property[1].valueCode = #d770 
* #d7701 "Eheliche Beziehungen"
* #d7701 ^property[0].code = #None 
* #d7701 ^property[0].valueString = "Eine intime Beziehung mit einer anderen Person auf rechtlicher Grundlage aufzubauen und aufrecht zu erhalten, wie in einer Ehe, einschließlich eine Ehefrau oder ein Ehemann oder eine rechtlich anerkannte Lebensgefährtin bzw. -gefährte zu werden und zu sein" 
* #d7701 ^property[1].code = #parent 
* #d7701 ^property[1].valueCode = #d770 
* #d7702 "Sexualbeziehungen"
* #d7702 ^property[0].code = #None 
* #d7702 ^property[0].valueString = "Mit dem Ehe- oder einem anderen Partner Beziehungen sexueller Art aufzunehmen und aufrecht zu erhalten" 
* #d7702 ^property[1].code = #parent 
* #d7702 ^property[1].valueCode = #d770 
* #d7708 "Intime Beziehungen, anders bezeichnet"
* #d7708 ^property[0].code = #parent 
* #d7708 ^property[0].valueCode = #d770 
* #d7709 "Intime Beziehungen, nicht näher bezeichnet"
* #d7709 ^property[0].code = #parent 
* #d7709 ^property[0].valueCode = #d770 
* #d779 "Besondere interpersonelle Beziehungen, anders oder nicht näher bezeichnet"
* #d779 ^property[0].code = #parent 
* #d779 ^property[0].valueCode = #d730-d779 
* #d798 "Interpersonelle Interaktionen und Beziehungen, anders bezeichnet"
* #d798 ^property[0].code = #parent 
* #d798 ^property[0].valueCode = #d7 
* #d799 "Interpersonelle Interaktionen und Beziehungen, nicht näher bezeichnet"
* #d799 ^property[0].code = #parent 
* #d799 ^property[0].valueCode = #d7 
* #d8 "Bedeutende Lebensbereiche"
* #d8 ^property[0].code = #None 
* #d8 ^property[0].valueString = "Dieses Kapitel befasst sich mit der Ausführung von Aufgaben und Handlungen, die für die Beteiligung an Erziehung/Bildung, Arbeit und Beschäftigung sowie für die Durchführung wirtschaftlicher Transaktionen erforderlich sind." 
* #d8 ^property[1].code = #parent 
* #d8 ^property[1].valueCode = #d 
* #d8 ^property[2].code = #child 
* #d8 ^property[2].valueCode = #d810-d839 
* #d8 ^property[3].code = #child 
* #d8 ^property[3].valueCode = #d840-d859 
* #d8 ^property[4].code = #child 
* #d8 ^property[4].valueCode = #d860-d879 
* #d8 ^property[5].code = #child 
* #d8 ^property[5].valueCode = #d898 
* #d8 ^property[6].code = #child 
* #d8 ^property[6].valueCode = #d899 
* #d810-d839 "Erziehung/Bildung"
* #d810-d839 ^property[0].code = #parent 
* #d810-d839 ^property[0].valueCode = #d8 
* #d810-d839 ^property[1].code = #child 
* #d810-d839 ^property[1].valueCode = #d810 
* #d810-d839 ^property[2].code = #child 
* #d810-d839 ^property[2].valueCode = #d815 
* #d810-d839 ^property[3].code = #child 
* #d810-d839 ^property[3].valueCode = #d820 
* #d810-d839 ^property[4].code = #child 
* #d810-d839 ^property[4].valueCode = #d825 
* #d810-d839 ^property[5].code = #child 
* #d810-d839 ^property[5].valueCode = #d830 
* #d810-d839 ^property[6].code = #child 
* #d810-d839 ^property[6].valueCode = #d839 
* #d810 "Informelle Bildung/Ausbildung"
* #d810 ^property[0].code = #None 
* #d810 ^property[0].valueString = "Zu Hause oder in einem anderen nicht-institutionellen Rahmen zu lernen, wie handwerkliche und andere Fertigkeiten von den Eltern oder Familienmitgliedern lernen, oder Privatunterricht erhalten" 
* #d810 ^property[1].code = #parent 
* #d810 ^property[1].valueCode = #d810-d839 
* #d815 "Vorschulerziehung"
* #d815 ^property[0].code = #None 
* #d815 ^property[0].valueString = "Auf einem Eingangsniveau organisierten Unterrichts zu lernen, der vornehmlich dazu dient, ein Kind auf die Schule und die obligatorische Bildung vorzubereiten, wie bei der Aneignung von Fertigkeiten in einer Tagesbetreuung oder in einem ähnlichen Rahmen als Vorbereitung für den Übergang zur Schule" 
* #d815 ^property[1].code = #parent 
* #d815 ^property[1].valueCode = #d810-d839 
* #d820 "Schulbildung"
* #d820 ^property[0].code = #None 
* #d820 ^property[0].valueString = "Die Zulassung zu Schule und Bildung zu erlangen, an allen schulbezogenen Pflichten und Rechten teilzuhaben und die Lehrgangsstoffe, -inhalte und andere curriculare Anforderungen der Programme der Primar- und Sekundarstufenbildung zu erlernen einschließlich regelmäßig am Unterricht teilzunehmen, mit anderen Schülern zusammenzuarbeiten, Anweisungen der Lehrer zu befolgen, die zugewiesenen Aufgaben und Projekte zu organisieren, zu lernen und abzuschließen und zu anderen Stufen der Bildung fortzuschreiten" 
* #d820 ^property[1].code = #parent 
* #d820 ^property[1].valueCode = #d810-d839 
* #d825 "Theoretische Berufsausbildung"
* #d825 ^property[0].code = #None 
* #d825 ^property[0].valueString = "Sich an allen Aktivitäten von Programmen der beruflichen Ausbildung zu beteiligen und die curricularen Stoffe für die Vorbereitung der Beschäftigung in einem Gewerbe, auf einem Arbeitsplatz oder in einem Fachberuf zu lernen" 
* #d825 ^property[1].code = #parent 
* #d825 ^property[1].valueCode = #d810-d839 
* #d830 "Höhere Bildung und Ausbildung"
* #d830 ^property[0].code = #None 
* #d830 ^property[0].valueString = "Sich an den Aktivitäten der weiterführenden Bildungs-/Ausbildungsprogramme an Universitäten, Fachhochschulen und Fachschulen zu beteiligen und alle curricularen Inhalte zu lernen, die für formale Grade, Diplome und andere Beglaubigungen erforderlich sind, wie einen Diplom- oder Promotionsstudiengang an einer Universität oder anderen anerkannten Fachbildungseinrichtung abschließen" 
* #d830 ^property[1].code = #parent 
* #d830 ^property[1].valueCode = #d810-d839 
* #d839 "Bildung/Ausbildung, anders oder nicht näher bezeichnet"
* #d839 ^property[0].code = #parent 
* #d839 ^property[0].valueCode = #d810-d839 
* #d840-d859 "Arbeit und Beschäftigung"
* #d840-d859 ^property[0].code = #parent 
* #d840-d859 ^property[0].valueCode = #d8 
* #d840-d859 ^property[1].code = #child 
* #d840-d859 ^property[1].valueCode = #d840 
* #d840-d859 ^property[2].code = #child 
* #d840-d859 ^property[2].valueCode = #d845 
* #d840-d859 ^property[3].code = #child 
* #d840-d859 ^property[3].valueCode = #d850 
* #d840-d859 ^property[4].code = #child 
* #d840-d859 ^property[4].valueCode = #d855 
* #d840-d859 ^property[5].code = #child 
* #d840-d859 ^property[5].valueCode = #d859 
* #d840 "Vorbereitung auf Erwerbstätigkeit"
* #d840 ^property[0].code = #None 
* #d840 ^property[0].valueString = "Sich an allen Programmen in Zusammenhang mit der Vorbereitung auf Beschäftigung zu beteiligen, wie die Aufgaben ausführen, die in Lehre, Praktika (einschließlich im Rahmen eines Hochschulstudiums) und ausbildungsbegleitendem Training gefordert werden" 
* #d840 ^property[1].code = #None 
* #d840 ^property[1].valueString = "Theoretische Berufsausbildung" 
* #d840 ^property[2].code = #parent 
* #d840 ^property[2].valueCode = #d840-d859 
* #d845 "Eine Arbeit erhalten, behalten und beenden"
* #d845 ^property[0].code = #None 
* #d845 ^property[0].valueString = "Eine Beschäftigung zu suchen, zu finden und auszuwählen, eine angebotene Arbeitsstelle anzunehmen, eine Anstellung, eine Gewerbetätigkeit, eine allgemeine oder eine gehobene berufliche Tätigkeit zu behalten und darin aufzusteigen sowie ein Arbeitsverhältnis in geeigneter Weise zu beenden" 
* #d845 ^property[1].code = #None 
* #d845 ^property[1].valueString = "Eine Arbeit suchen; einen Lebenslauf verfassen; Arbeitgeber kontaktieren und Bewerbungsgespräche vorbereiten; ein Arbeitsverhältnis aufrecht erhalten; seine eigene Arbeitsleistung überwachen; kündigen und ein Arbeitsverhältnis beenden" 
* #d845 ^property[2].code = #parent 
* #d845 ^property[2].valueCode = #d840-d859 
* #d845 ^property[3].code = #child 
* #d845 ^property[3].valueCode = #d8450 
* #d845 ^property[4].code = #child 
* #d845 ^property[4].valueCode = #d8451 
* #d845 ^property[5].code = #child 
* #d845 ^property[5].valueCode = #d8452 
* #d845 ^property[6].code = #child 
* #d845 ^property[6].valueCode = #d8458 
* #d845 ^property[7].code = #child 
* #d845 ^property[7].valueCode = #d8459 
* #d8450 "Arbeit suchen"
* #d8450 ^property[0].code = #None 
* #d8450 ^property[0].valueString = "Ein Arbeitsangebot in einem Gewerbe, Beruf oder eine andere Art von Beschäftigung herauszufinden und auszuwählen und die erforderlichen Aufgaben zu erledigen, um eingestellt zu werden, wie mit dem Arbeitgeber Kontakt aufnehmen oder an einem Vorstellungsgespräch teilnehmen" 
* #d8450 ^property[1].code = #parent 
* #d8450 ^property[1].valueCode = #d845 
* #d8451 "Ein Arbeitsverhältnis behalten"
* #d8451 ^property[0].code = #None 
* #d8451 ^property[0].valueString = "Die Aufgaben des Arbeitsplatzes zu erfüllen, um die Beschäftigung, die Gewerbetätigkeit, die berufliche Tätigkeit oder andere Form von Arbeit zu behalten und eine Beförderung oder andere Förderungen zu erhalten" 
* #d8451 ^property[1].code = #parent 
* #d8451 ^property[1].valueCode = #d845 
* #d8452 "Ein Arbeitsverhältnis beenden"
* #d8452 ^property[0].code = #None 
* #d8452 ^property[0].valueString = "In geeigneter Weise ein Arbeitsverhältnis aufzulösen oder zu kündigen" 
* #d8452 ^property[1].code = #parent 
* #d8452 ^property[1].valueCode = #d845 
* #d8458 "Ein Arbeitsverhältnis finden, behalten und beenden, anders bezeichnet"
* #d8458 ^property[0].code = #parent 
* #d8458 ^property[0].valueCode = #d845 
* #d8459 "Ein Arbeitsverhältnis finden, behalten und beenden, nicht näher bezeichnet"
* #d8459 ^property[0].code = #parent 
* #d8459 ^property[0].valueCode = #d845 
* #d850 "Bezahlte Tätigkeit"
* #d850 ^property[0].code = #None 
* #d850 ^property[0].valueString = "Sich an allen Aspekten bezahlter Arbeit in Form von Beschäftigung, Gewerbetätigkeit, beruflicher Tätigkeit oder anderer Art von Erwerbstätigkeit zu beteiligen, als Angestellter, in Voll- oder Teilzeitbeschäftigung oder als Selbständiger, wie Arbeit suchen und eine Arbeitsstelle erhalten, die geforderten Aufgaben der Arbeitsstelle erfüllen, rechtzeitig bei der Arbeit erscheinen, andere Arbeitnehmer überwachen oder selbst überwacht werden sowie die geforderten Aufgaben allein oder in Gruppen erledigen" 
* #d850 ^property[1].code = #None 
* #d850 ^property[1].valueString = "Selbständige Tätigkeit, Teil- oder Vollzeitbeschäftigung" 
* #d850 ^property[2].code = #parent 
* #d850 ^property[2].valueCode = #d840-d859 
* #d850 ^property[3].code = #child 
* #d850 ^property[3].valueCode = #d8500 
* #d850 ^property[4].code = #child 
* #d850 ^property[4].valueCode = #d8501 
* #d850 ^property[5].code = #child 
* #d850 ^property[5].valueCode = #d8502 
* #d850 ^property[6].code = #child 
* #d850 ^property[6].valueCode = #d8508 
* #d850 ^property[7].code = #child 
* #d850 ^property[7].valueCode = #d8509 
* #d8500 "Selbständige Tätigkeit"
* #d8500 ^property[0].code = #None 
* #d8500 ^property[0].valueString = "Selbständige Erwerbstätigkeit, die das Individuum selbst gesucht oder geschaffen hat, oder von anderen ohne formelles Arbeitsverhältnis vertraglich zugesichert wurde, auszuüben, wie landwirtschaftliche Wanderarbeit, Tätigkeit als freiberuflicher Autor oder Berater, kurzfristige Vertragsarbeit, Tätigkeit als Künstler oder Handwerker, ein Geschäft oder ein Unternehmen besitzen und führen" 
* #d8500 ^property[1].code = #None 
* #d8500 ^property[1].valueString = "Teilzeit- und Vollzeitbeschäftigung" 
* #d8500 ^property[2].code = #parent 
* #d8500 ^property[2].valueCode = #d850 
* #d8501 "Teilzeitbeschäftigung"
* #d8501 ^property[0].code = #None 
* #d8501 ^property[0].valueString = "Sich als Angestellter an allen Aspekten bezahlter Arbeit auf Teilzeitbasis zu beteiligen, wie Arbeit suchen und eine Arbeitsstelle erhalten, die geforderten Aufgaben der Arbeitsstelle erfüllen, rechtzeitig bei der Arbeit erscheinen, andere Arbeitnehmer überwachen oder selbst überwacht werden sowie die geforderten Aufgaben allein oder in Gruppen erledigen" 
* #d8501 ^property[1].code = #parent 
* #d8501 ^property[1].valueCode = #d850 
* #d8502 "Vollzeitbeschäftigung"
* #d8502 ^property[0].code = #None 
* #d8502 ^property[0].valueString = "Sich als Angestellter an allen Aspekten bezahlter Arbeit auf Vollzeitbasis zu beteiligen, wie Arbeit suchen und eine Tätigkeit erhalten, die geforderten Aufgaben der Tätigkeit erfüllen, rechtzeitig bei der Arbeit erscheinen, andere Arbeitnehmer überwachen oder selbst überwacht werden sowie die geforderten Aufgaben allein oder in Gruppen erledigen" 
* #d8502 ^property[1].code = #parent 
* #d8502 ^property[1].valueCode = #d850 
* #d8508 "Bezahlte Tätigkeit, anders bezeichnet"
* #d8508 ^property[0].code = #parent 
* #d8508 ^property[0].valueCode = #d850 
* #d8509 "Bezahlte Tätigkeit, nicht näher bezeichnet"
* #d8509 ^property[0].code = #parent 
* #d8509 ^property[0].valueCode = #d850 
* #d855 "Unbezahlte Tätigkeit"
* #d855 ^property[0].code = #None 
* #d855 ^property[0].valueString = "Sich an allen Aspekten der Voll- oder Teilzeitarbeit, für die eine Bezahlung nicht vorgesehen ist, zu beteiligen, einschließlich organisierter Arbeitsaktivitäten, die geforderten Aufgaben der Tätigkeit zu erfüllen, rechtzeitig bei der Arbeit zu erscheinen, andere Arbeitnehmer zu überwachen oder selbst überwacht zu werden sowie die geforderten Aufgaben allein oder in Gruppen zu erledigen, wie ehrenamtliche Tätigkeit, ohne Bezahlung für die Gemeinschaft, für religiöse Gruppen oder in der häuslichen Umgebung arbeiten" 
* #d855 ^property[1].code = #None 
* #d855 ^property[1].valueString = "Kapitel 6: Häusliches Leben" 
* #d855 ^property[2].code = #parent 
* #d855 ^property[2].valueCode = #d840-d859 
* #d859 "Arbeit und Beschäftigung, anders oder nicht näher bezeichnet"
* #d859 ^property[0].code = #parent 
* #d859 ^property[0].valueCode = #d840-d859 
* #d860-d879 "Wirtschaftliches Leben"
* #d860-d879 ^property[0].code = #parent 
* #d860-d879 ^property[0].valueCode = #d8 
* #d860-d879 ^property[1].code = #child 
* #d860-d879 ^property[1].valueCode = #d860 
* #d860-d879 ^property[2].code = #child 
* #d860-d879 ^property[2].valueCode = #d865 
* #d860-d879 ^property[3].code = #child 
* #d860-d879 ^property[3].valueCode = #d870 
* #d860-d879 ^property[4].code = #child 
* #d860-d879 ^property[4].valueCode = #d879 
* #d860 "Elementare wirtschaftliche Transaktionen"
* #d860 ^property[0].code = #None 
* #d860 ^property[0].valueString = "Sich an jeder Form einfacher wirtschaftlicher Transaktionen zu beteiligen, wie Geld zum Einkaufen von Nahrungsmitteln benutzen oder Tauschhandel treiben, Güter oder Dienstleistungen austauschen oder Geld sparen" 
* #d860 ^property[1].code = #parent 
* #d860 ^property[1].valueCode = #d860-d879 
* #d865 "Komplexe wirtschaftliche Transaktionen"
* #d865 ^property[0].code = #None 
* #d865 ^property[0].valueString = "Sich an jeder Art von komplexen wirtschaftlichen Transaktionen zu beteiligen, die den Austausch von Kapital oder Eigentum und die Erzielung von Gewinn oder anderen wirtschaftlichen Werten beinhalten, wie ein Geschäft, eine Fabrik oder eine Ausstattung kaufen, ein Bankkonto unterhalten oder mit Gebrauchsgegenständen handeln" 
* #d865 ^property[1].code = #parent 
* #d865 ^property[1].valueCode = #d860-d879 
* #d870 "Wirtschaftliche Eigenständigkeit"
* #d870 ^property[0].code = #None 
* #d870 ^property[0].valueString = "Die Verfügungsgewalt über wirtschaftliche Ressourcen aus privaten oder öffentlichen Quellen zu haben, um die wirtschaftliche Sicherheit für den gegenwärtigen und zukünftigen Bedarf zu gewährleisten" 
* #d870 ^property[1].code = #None 
* #d870 ^property[1].valueString = "Persönliche wirtschaftliche Ressourcen und öffentliche wirtschaftliche Ansprüche" 
* #d870 ^property[2].code = #parent 
* #d870 ^property[2].valueCode = #d860-d879 
* #d870 ^property[3].code = #child 
* #d870 ^property[3].valueCode = #d8700 
* #d870 ^property[4].code = #child 
* #d870 ^property[4].valueCode = #d8701 
* #d870 ^property[5].code = #child 
* #d870 ^property[5].valueCode = #d8708 
* #d870 ^property[6].code = #child 
* #d870 ^property[6].valueCode = #d8709 
* #d8700 "Persönliche wirtschaftliche Ressourcen"
* #d8700 ^property[0].code = #None 
* #d8700 ^property[0].valueString = "Die Verfügungsgewalt über persönliche oder private wirtschaftliche Ressourcen zu haben, um die wirtschaftliche Sicherheit für den gegenwärtigen und zukünftigen Bedarf zu gewährleisten" 
* #d8700 ^property[1].code = #parent 
* #d8700 ^property[1].valueCode = #d870 
* #d8701 "Öffentliche wirtschaftliche Ansprüche"
* #d8701 ^property[0].code = #None 
* #d8701 ^property[0].valueString = "Die Verfügungsgewalt über öffentliche wirtschaftliche Ressourcen zu haben, um die wirtschaftliche Sicherheit für den gegenwärtigen und zukünftigen Bedarf zu gewährleisten" 
* #d8701 ^property[1].code = #parent 
* #d8701 ^property[1].valueCode = #d870 
* #d8708 "Wirtschaftliche Eigenständigkeit, anders bezeichnet"
* #d8708 ^property[0].code = #parent 
* #d8708 ^property[0].valueCode = #d870 
* #d8709 "Wirtschaftliche Eigenständigkeit, nicht näher bezeichnet"
* #d8709 ^property[0].code = #parent 
* #d8709 ^property[0].valueCode = #d870 
* #d879 "Wirtschaftliches Leben, anders oder nicht näher bezeichnet"
* #d879 ^property[0].code = #parent 
* #d879 ^property[0].valueCode = #d860-d879 
* #d898 "Größere Lebensbereiche, anders bezeichnet"
* #d898 ^property[0].code = #parent 
* #d898 ^property[0].valueCode = #d8 
* #d899 "Größere Lebensbereiche, nicht näher bezeichnet"
* #d899 ^property[0].code = #parent 
* #d899 ^property[0].valueCode = #d8 
* #d9 "Gemeinschafts-, soziales und staatsbürgerliches Leben"
* #d9 ^property[0].code = #None 
* #d9 ^property[0].valueString = "Dieses Kapitel befasst sich mit Handlungen und Aufgaben, die für die Beteiligung am organisierten sozialen Leben außerhalb der Familie, in der Gemeinschaft sowie in verschiedenen sozialen und staatsbürgerlichen Lebensbereichen erforderlich sind." 
* #d9 ^property[1].code = #parent 
* #d9 ^property[1].valueCode = #d 
* #d9 ^property[2].code = #child 
* #d9 ^property[2].valueCode = #d910 
* #d9 ^property[3].code = #child 
* #d9 ^property[3].valueCode = #d920 
* #d9 ^property[4].code = #child 
* #d9 ^property[4].valueCode = #d930 
* #d9 ^property[5].code = #child 
* #d9 ^property[5].valueCode = #d940 
* #d9 ^property[6].code = #child 
* #d9 ^property[6].valueCode = #d950 
* #d9 ^property[7].code = #child 
* #d9 ^property[7].valueCode = #d998 
* #d9 ^property[8].code = #child 
* #d9 ^property[8].valueCode = #d999 
* #d910 "Gemeinschaftsleben"
* #d910 ^property[0].code = #None 
* #d910 ^property[0].valueString = "Sich an allen Aspekten des gemeinschaftlichen sozialen Lebens zu beteiligen, wie in Wohlfahrtsorganisationen, Dienstleistungsvereinigungen oder professionellen Sozialorganisationen mitzuwirken" 
* #d910 ^property[1].code = #None 
* #d910 ^property[1].valueString = "Informelle und formelle Vereinigungen; Feierlichkeiten" 
* #d910 ^property[2].code = #None 
* #d910 ^property[2].valueString = "Unbezahlte Tätigkeit" 
* #d910 ^property[3].code = #parent 
* #d910 ^property[3].valueCode = #d9 
* #d910 ^property[4].code = #child 
* #d910 ^property[4].valueCode = #d9100 
* #d910 ^property[5].code = #child 
* #d910 ^property[5].valueCode = #d9101 
* #d910 ^property[6].code = #child 
* #d910 ^property[6].valueCode = #d9102 
* #d910 ^property[7].code = #child 
* #d910 ^property[7].valueCode = #d9108 
* #d910 ^property[8].code = #child 
* #d910 ^property[8].valueCode = #d9109 
* #d9100 "Informelle Vereinigungen"
* #d9100 ^property[0].code = #None 
* #d9100 ^property[0].valueString = "Sich in sozialen oder gesellschaftlichen Vereinigungen, die von Menschen gleicher Interessen organisiert sind, zu beteiligen, wie lokale soziale Klubs oder ethnische Gruppen" 
* #d9100 ^property[1].code = #parent 
* #d9100 ^property[1].valueCode = #d910 
* #d9101 "Formelle Vereinigungen"
* #d9101 ^property[0].code = #None 
* #d9101 ^property[0].valueString = "Sich an professionellen oder anderen sozialen Fachgruppen zu beteiligen, wie Vereinigungen von Rechtsanwälten, Ärzten oder Akademikern" 
* #d9101 ^property[1].code = #parent 
* #d9101 ^property[1].valueCode = #d910 
* #d9102 "Feierlichkeiten"
* #d9102 ^property[0].code = #None 
* #d9102 ^property[0].valueString = "Sich an nichtreligiösen Riten oder gesellschaftlichen Feierlichkeiten zu beteiligen, wie an Hochzeiten, Beerdigungen oder Initialriten" 
* #d9102 ^property[1].code = #parent 
* #d9102 ^property[1].valueCode = #d910 
* #d9108 "Gemeinschaftsleben, anders bezeichnet"
* #d9108 ^property[0].code = #parent 
* #d9108 ^property[0].valueCode = #d910 
* #d9109 "Gemeinschaftsleben, nicht näher bezeichnet"
* #d9109 ^property[0].code = #parent 
* #d9109 ^property[0].valueCode = #d910 
* #d920 "Erholung und Freizeit"
* #d920 ^property[0].code = #None 
* #d920 ^property[0].valueString = "Sich an allen Formen des Spiels, von Freizeit- oder Erholungsaktivitäten zu beteiligen, wie an Spiel und Sport in informeller oder organisierter Form, Programmen für die körperliche Fitness, Entspannung, Unterhaltung oder Zerstreuung; Kunstgalerien, Museen, Kino oder Theater besuchen, Handarbeiten machen und Hobbys frönen, zur Erbauung lesen, Musikinstrumente spielen; Sehenswürdigkeiten besichtigen, Tourismus- und Vergnügungsreisen machen" 
* #d920 ^property[1].code = #None 
* #d920 ^property[1].valueString = "Tiere zu Transportzwecken reiten" 
* #d920 ^property[2].code = #None 
* #d920 ^property[2].valueString = "Spiel, Sport, Kunst und Kultur, Kunsthandwerk, Hobbys und Geselligkeit" 
* #d920 ^property[3].code = #parent 
* #d920 ^property[3].valueCode = #d9 
* #d920 ^property[4].code = #child 
* #d920 ^property[4].valueCode = #d9200 
* #d920 ^property[5].code = #child 
* #d920 ^property[5].valueCode = #d9201 
* #d920 ^property[6].code = #child 
* #d920 ^property[6].valueCode = #d9202 
* #d920 ^property[7].code = #child 
* #d920 ^property[7].valueCode = #d9203 
* #d920 ^property[8].code = #child 
* #d920 ^property[8].valueCode = #d9204 
* #d920 ^property[9].code = #child 
* #d920 ^property[9].valueCode = #d9205 
* #d920 ^property[10].code = #child 
* #d920 ^property[10].valueCode = #d9208 
* #d920 ^property[11].code = #child 
* #d920 ^property[11].valueCode = #d9209 
* #d9200 "Spiel"
* #d9200 ^property[0].code = #None 
* #d9200 ^property[0].valueString = "Sich an Spielen mit Regeln, unstrukturierten oder ungeregelten Spielen und Freizeitbeschäftigung zu beteiligen, wie Schach oder Karten spielen oder das Spiel von Kindern" 
* #d9200 ^property[1].code = #parent 
* #d9200 ^property[1].valueCode = #d920 
* #d9201 "Sport"
* #d9201 ^property[0].code = #None 
* #d9201 ^property[0].valueString = "Sich an informellen oder formell organisierten Wettkampfspielen oder athletischen Ereignissen, die allein oder in einer Gruppe durchgeführt werden, zu beteiligen, wie Bowling, Gymnastik oder Fußball" 
* #d9201 ^property[1].code = #parent 
* #d9201 ^property[1].valueCode = #d920 
* #d9202 "Kunst und Kultur"
* #d9202 ^property[0].code = #None 
* #d9202 ^property[0].valueString = "Sich an Ereignissen der schönen Künste oder der Kultur zu beteiligen oder an diesen Gefallen zu finden, wie ins Theater, Kino, Museum oder in Kunstgalerien gehen, in einem Stück als Schauspieler auftreten, zur eigenen Erbauung lesen oder ein Musikinstrument spielen" 
* #d9202 ^property[1].code = #parent 
* #d9202 ^property[1].valueCode = #d920 
* #d9203 "Kunsthandwerk"
* #d9203 ^property[0].code = #None 
* #d9203 ^property[0].valueString = "Sich an Handarbeiten zu beteiligen, wie Töpferei oder Stricken" 
* #d9203 ^property[1].code = #parent 
* #d9203 ^property[1].valueCode = #d920 
* #d9204 "Hobbys"
* #d9204 ^property[0].code = #None 
* #d9204 ^property[0].valueString = "Sich mit Lieblingsbeschäftigungen zu befassen, wie Briefmarken, Münzen oder Antiquitäten sammeln" 
* #d9204 ^property[1].code = #parent 
* #d9204 ^property[1].valueCode = #d920 
* #d9205 "Geselligkeit"
* #d9205 ^property[0].code = #None 
* #d9205 ^property[0].valueString = "Sich an informellen oder gelegentlichen Zusammenkünften beteiligen, wie Freunde oder Verwandte besuchen oder sich informell in der Öffentlichkeit zu treffen" 
* #d9205 ^property[1].code = #parent 
* #d9205 ^property[1].valueCode = #d920 
* #d9208 "Erholung und Freizeit, anders bezeichnet"
* #d9208 ^property[0].code = #parent 
* #d9208 ^property[0].valueCode = #d920 
* #d9209 "Erholung und Freizeit, nicht näher bezeichnet"
* #d9209 ^property[0].code = #parent 
* #d9209 ^property[0].valueCode = #d920 
* #d930 "Religion und Spiritualität"
* #d930 ^property[0].code = #None 
* #d930 ^property[0].valueString = "Sich an religiösen und spirituellen Aktivitäten, Organisationen oder Praktiken zur Selbsterfüllung, Bedeutungsfindung, für religiöse und spirituelle Werte sowie zur Bildung von Beziehung zu einer göttlichen Macht zu beteiligen, wie an religiösen Diensten in einer Kirche, einem Tempel, einer Moschee oder Synagoge teilnehmen, aus religiösen Gründen beten und singen; spirituelle Kontemplation" 
* #d930 ^property[1].code = #None 
* #d930 ^property[1].valueString = "Organisierte Religion und Spiritualität" 
* #d930 ^property[2].code = #parent 
* #d930 ^property[2].valueCode = #d9 
* #d930 ^property[3].code = #child 
* #d930 ^property[3].valueCode = #d9300 
* #d930 ^property[4].code = #child 
* #d930 ^property[4].valueCode = #d9301 
* #d930 ^property[5].code = #child 
* #d930 ^property[5].valueCode = #d9308 
* #d930 ^property[6].code = #child 
* #d930 ^property[6].valueCode = #d9309 
* #d9300 "Organisierte Religion"
* #d9300 ^property[0].code = #None 
* #d9300 ^property[0].valueString = "Sich an organisierten religiösen Zeremonien, Aktivitäten und Ereignissen beteiligen" 
* #d9300 ^property[1].code = #parent 
* #d9300 ^property[1].valueCode = #d930 
* #d9301 "Spiritualität"
* #d9301 ^property[0].code = #None 
* #d9301 ^property[0].valueString = "Sich außerhalb organisierter Religion an spirituellen Aktivitäten oder Ereignissen zu beteiligen" 
* #d9301 ^property[1].code = #parent 
* #d9301 ^property[1].valueCode = #d930 
* #d9308 "Religion und Spiritualität, anders bezeichnet"
* #d9308 ^property[0].code = #parent 
* #d9308 ^property[0].valueCode = #d930 
* #d9309 "Religion und Spiritualität, nicht näher bezeichnet"
* #d9309 ^property[0].code = #parent 
* #d9309 ^property[0].valueCode = #d930 
* #d940 "Menschenrechte"
* #d940 ^property[0].code = #None 
* #d940 ^property[0].valueString = "Die nationalen und internationalen anerkannten Rechte zu genießen, die Menschen allein aufgrund ihres Menschseins gewährt werden, wie die Menschenrechte der Menschenrechtsdeklaration der Vereinten Nation (1948) und die Rahmenbestimmungen für die Herstellung von Chancengleichheit von Personen mit Behinderungen (1993); das Recht auf Selbstbestimmung und Autonomie sowie das Recht, über sein Schicksal selbst zu bestimmen" 
* #d940 ^property[1].code = #None 
* #d940 ^property[1].valueString = "Politisches Leben und Staatsbürgerschaft" 
* #d940 ^property[2].code = #parent 
* #d940 ^property[2].valueCode = #d9 
* #d950 "Politisches Leben und Staatsbürgerschaft"
* #d950 ^property[0].code = #None 
* #d950 ^property[0].valueString = "Sich als Bürger am sozialen, politischen und staatlichen Leben zu beteiligen, der den rechtlichen Status als Staatsbürger besitzt und die damit verbundenen Rechte, den Schutz, die Vorteile und Pflichten genießt, wie das Wahlrecht wahrnehmen, für ein politisches Amt kandidieren, politische Vereinigungen gründen; die Rechte und die Freiheit eines Staatsbürgers zu genießen (wie das Recht auf Meinungs-, Versammlungs- und Religionsfreiheit, Schutz vor unverhältnismäßiger oder unrechtmäßiger Verfolgung und Gefangennahme, das Recht auf Rechtsberatung und Verteidigung, auf ein Gerichtsverfahren sowie andere Rechte und Schutz vor Diskriminierung); den rechtlichen Status als Staatsbürger haben" 
* #d950 ^property[1].code = #None 
* #d950 ^property[1].valueString = "Menschenrechte" 
* #d950 ^property[2].code = #parent 
* #d950 ^property[2].valueCode = #d9 
* #d998 "Leben in der Gemeinschaft, soziales und staatsbürgerliches Leben, anders bezeichnet"
* #d998 ^property[0].code = #parent 
* #d998 ^property[0].valueCode = #d9 
* #d999 "Leben in der Gemeinschaft, soziales und staatsbürgerliches Leben, nicht näher bezeichnet"
* #d999 ^property[0].code = #parent 
* #d999 ^property[0].valueCode = #d9 
* #e "Umweltfaktoren"
* #e ^property[0].code = #None 
* #e ^property[0].valueString = "Umweltfaktoren bilden die materielle, soziale und einstellungsbezogene Umwelt, in der Menschen leben und ihr Dasein entfalten." 
* #e ^property[1].code = #child 
* #e ^property[1].valueCode = #e1 
* #e ^property[2].code = #child 
* #e ^property[2].valueCode = #e2 
* #e ^property[3].code = #child 
* #e ^property[3].valueCode = #e3 
* #e ^property[4].code = #child 
* #e ^property[4].valueCode = #e4 
* #e ^property[5].code = #child 
* #e ^property[5].valueCode = #e5 
* #e1 "Produkte und Technologien"
* #e1 ^property[0].code = #None 
* #e1 ^property[0].valueString = "Dieses Kapitel befasst sich mit natürlichen oder vom Menschen hergestellten Produkten oder Produktsystemen, Ausrüstungen und Technologien in der unmittelbaren Umwelt eines Menschen, die gesammelt, geschaffen, produziert oder hergestellt sind. Die ISO 9999 Klassifikation der technischen Hilfen definiert diese als \"jedes von einer behinderten Person verwendete Produkt, Instrument, Ausrüstung oder technisches System, speziell produziert oder allgemein verfügbar, um Behinderung vorzubeugen, zu kompensieren, zu überwachen, zu lindern oder zu beheben\". Es ist anzumerken, dass alle Produkte und Technologien Hilfsfunktion haben können (siehe ISO 9999: Technische Hilfen für behinderte Menschen - Klassifikation und Terminologie (zweite Version); ISO/TC 173/SC 2, ISO/DIS 9999 (rev.). Für diese Klassifikation der Umweltfaktoren sind jedoch hilfebezogene Produkte und Technologien enger definiert als jedes Produkt, Instrument, Ausrüstung oder Technologie, das zur Verbesserung der Funktionsfähigkeit behinderter Menschen angepasst oder speziell entworfen ist." 
* #e1 ^property[1].code = #parent 
* #e1 ^property[1].valueCode = #e 
* #e1 ^property[2].code = #child 
* #e1 ^property[2].valueCode = #e110 
* #e1 ^property[3].code = #child 
* #e1 ^property[3].valueCode = #e115 
* #e1 ^property[4].code = #child 
* #e1 ^property[4].valueCode = #e120 
* #e1 ^property[5].code = #child 
* #e1 ^property[5].valueCode = #e125 
* #e1 ^property[6].code = #child 
* #e1 ^property[6].valueCode = #e130 
* #e1 ^property[7].code = #child 
* #e1 ^property[7].valueCode = #e135 
* #e1 ^property[8].code = #child 
* #e1 ^property[8].valueCode = #e140 
* #e1 ^property[9].code = #child 
* #e1 ^property[9].valueCode = #e145 
* #e1 ^property[10].code = #child 
* #e1 ^property[10].valueCode = #e150 
* #e1 ^property[11].code = #child 
* #e1 ^property[11].valueCode = #e155 
* #e1 ^property[12].code = #child 
* #e1 ^property[12].valueCode = #e160 
* #e1 ^property[13].code = #child 
* #e1 ^property[13].valueCode = #e165 
* #e1 ^property[14].code = #child 
* #e1 ^property[14].valueCode = #e198 
* #e1 ^property[15].code = #child 
* #e1 ^property[15].valueCode = #e199 
* #e110 "Produkte und Substanzen für den persönlichen Verbrauch"
* #e110 ^property[0].code = #None 
* #e110 ^property[0].valueString = "Alle natürlichen oder vom Menschen hergestellten Produkte oder Substanzen, für den persönlichen Verbrauch gesammelt, verarbeitet oder hergestellt" 
* #e110 ^property[1].code = #None 
* #e110 ^property[1].valueString = "Produkte wie Lebensmittel, Heilmittel/Medikamente" 
* #e110 ^property[2].code = #parent 
* #e110 ^property[2].valueCode = #e1 
* #e110 ^property[3].code = #child 
* #e110 ^property[3].valueCode = #e1100 
* #e110 ^property[4].code = #child 
* #e110 ^property[4].valueCode = #e1101 
* #e110 ^property[5].code = #child 
* #e110 ^property[5].valueCode = #e1108 
* #e110 ^property[6].code = #child 
* #e110 ^property[6].valueCode = #e1109 
* #e1100 "Lebensmittel"
* #e1100 ^property[0].code = #None 
* #e1100 ^property[0].valueString = "Alle natürlichen oder vom Menschen hergestellten Produkte oder Substanzen, zum Zweck des Verzehrs gesammelt, verarbeitet oder hergestellt, wie rohe, bearbeitete oder vorbereitete Speisen und Getränke unterschiedlicher Konsistenz, Kräuter und Mineralien (Vitamine und andere Nahrungsergänzungsstoffe)" 
* #e1100 ^property[1].code = #parent 
* #e1100 ^property[1].valueCode = #e110 
* #e1101 "Medikamente"
* #e1101 ^property[0].code = #None 
* #e1101 ^property[0].valueString = "Alle natürlichen oder vom Menschen hergestellten Produkte oder Substanzen, für medizinische Zwecke gesammelt, verarbeitet oder hergestellt, wie der heutigen Schulmedizin und der Naturheilkunde entsprechende Heilmittel/Medikamente" 
* #e1101 ^property[1].code = #parent 
* #e1101 ^property[1].valueCode = #e110 
* #e1108 "Produkte und Substanzen für den persönlichen Verbrauch, anders bezeichnet"
* #e1108 ^property[0].code = #parent 
* #e1108 ^property[0].valueCode = #e110 
* #e1109 "Produkte und Substanzen für den persönlichen Verbrauch, nicht näher bezeichnet"
* #e1109 ^property[0].code = #parent 
* #e1109 ^property[0].valueCode = #e110 
* #e115 "Produkte und Technologien zum persönlichen Gebrauch im täglichen Leben"
* #e115 ^property[0].code = #None 
* #e115 ^property[0].valueString = "Von Menschen für ihre täglichen Aktivitäten benutzte Ausrüstungsgegenstände, Produkte und Technologien, in oder nahe beim Körper getragen, einschließlich solcher, die angepasst oder speziell entworfen sind" 
* #e115 ^property[1].code = #None 
* #e115 ^property[1].valueString = "Allgemeine und unterstützende Produkte und Technologien für den persönlichen Gebrauch" 
* #e115 ^property[2].code = #parent 
* #e115 ^property[2].valueCode = #e1 
* #e115 ^property[3].code = #child 
* #e115 ^property[3].valueCode = #e1150 
* #e115 ^property[4].code = #child 
* #e115 ^property[4].valueCode = #e1151 
* #e115 ^property[5].code = #child 
* #e115 ^property[5].valueCode = #e1158 
* #e115 ^property[6].code = #child 
* #e115 ^property[6].valueCode = #e1159 
* #e1150 "Allgemeine Produkte zum persönlichen Gebrauch"
* #e1150 ^property[0].code = #None 
* #e1150 ^property[0].valueString = "Von Menschen für ihre täglichen Aktivitäten benutzte Ausrüstungsgegenstände, Produkte und Technologien, wie Kleidung, Textilien, Möbel, Geräte, Reinigungsmittel und Werkzeuge, weder angepasst noch speziell entworfen" 
* #e1150 ^property[1].code = #parent 
* #e1150 ^property[1].valueCode = #e115 
* #e1151 "Hilfsprodukte und unterstützende Technologien für den persönlichen Gebrauch im täglichen Leben"
* #e1151 ^property[0].code = #None 
* #e1151 ^property[0].valueString = "Angepasste oder speziell entworfene Ausrüstungsgegenstände, Produkte und Technologien, die Menschen im täglichen Leben helfen, wie Prothesen und Orthesen, Neuroprothesen (z.B. Geräte zur funktionalen Stimulation, die Darm, Blase, Atmung und Herzfrequenz steuern) sowie Umfeldkontrollgeräte, die es dem Individuum erleichtern, seine häusliche Umgebung zu kontrollieren (Abtastverfahren (Scanning), Fernbedienungen, sprachgesteuerte Systeme, Zeitschaltuhren)" 
* #e1151 ^property[1].code = #parent 
* #e1151 ^property[1].valueCode = #e115 
* #e1158 "Produkte und Technologien zum persönlichen Gebrauch im täglichen Leben, anders bezeichnet"
* #e1158 ^property[0].code = #parent 
* #e1158 ^property[0].valueCode = #e115 
* #e1159 "Produkte und Technologien zum persönlichen Gebrauch im täglichen Leben, nicht näher bezeichnet"
* #e1159 ^property[0].code = #parent 
* #e1159 ^property[0].valueCode = #e115 
* #e120 "Produkte und Technologien zur persönlichen Mobilität drinnen und draußen und zum Transport"
* #e120 ^property[0].code = #None 
* #e120 ^property[0].valueString = "Ausrüstungsgegenstände, Produkte und Technologien, die von Menschen für ihre Aktivitäten der Mobilität innerhalb und außerhalb von Gebäuden benutzt werden, einschließlich solcher, die angepasst oder speziell entworfen sind, und sich bei ihnen bzw. sich in ihrer Nähe befinden" 
* #e120 ^property[1].code = #None 
* #e120 ^property[1].valueString = "Allgemeine Hilfsprodukte und unterstützende Technologien für die persönliche Mobilität drinnen und draußen" 
* #e120 ^property[2].code = #parent 
* #e120 ^property[2].valueCode = #e1 
* #e120 ^property[3].code = #child 
* #e120 ^property[3].valueCode = #e1200 
* #e120 ^property[4].code = #child 
* #e120 ^property[4].valueCode = #e1201 
* #e120 ^property[5].code = #child 
* #e120 ^property[5].valueCode = #e1208 
* #e120 ^property[6].code = #child 
* #e120 ^property[6].valueCode = #e1209 
* #e1200 "Allgemeine Produkte und Technologien zur persönlichen Mobilität und zum Transport drinnen und draußen"
* #e1200 ^property[0].code = #None 
* #e1200 ^property[0].valueString = "Von Menschen für ihre Mobilitätsaktivitäten innerhalb und außerhalb von Gebäuden benutzte Ausrüstungsgegenstände, Produkte und Technologien, wie motorisierte und nicht motorisierte Fahrzeuge für den Land-, Wasser- und Lufttransport von Menschen (z.B. Busse, Autos, Vans, andere motorisierte und von Tieren bewegte Transportmittel), weder angepasst noch speziell entworfen" 
* #e1200 ^property[1].code = #parent 
* #e1200 ^property[1].valueCode = #e120 
* #e1201 "Hilfsprodukte und unterstützende Technologien zur persönlichen Mobilität drinnen und draußen und zum Transport"
* #e1201 ^property[0].code = #None 
* #e1201 ^property[0].valueString = "Angepasste oder speziell entworfene Ausrüstungsgegenstände, Produkte und Technologien, die Menschen helfen, sich drinnen und draußen zu bewegen, wie Gehhilfen, spezielle Autos oder Großraumlimousinen, Fahrzeuganpassungen, Rollstühle, Roller und Geräte für den Transfer" 
* #e1201 ^property[1].code = #parent 
* #e1201 ^property[1].valueCode = #e120 
* #e1208 "Produkte und Technologien zur persönlichen Mobilität drinnen und draußen und zum Transport, anders bezeichnet"
* #e1208 ^property[0].code = #parent 
* #e1208 ^property[0].valueCode = #e120 
* #e1209 "Produkte und Technologien zur persönlichen Mobilität drinnen und draußen und zum Transport, nicht näher bezeichnet"
* #e1209 ^property[0].code = #parent 
* #e1209 ^property[0].valueCode = #e120 
* #e125 "Produkte und Technologien zur Kommunikation"
* #e125 ^property[0].code = #None 
* #e125 ^property[0].valueString = "Von Menschen für ihre Aktivitäten des Sendens und Empfangens von Informationen benutzte Ausrüstungsgegenstände, Produkte und Technologien, die sich im oder am Körper des Benutzers oder in seiner Nähe befinden, einschließlich solcher, die angepasst oder speziell entworfen sind" 
* #e125 ^property[1].code = #None 
* #e125 ^property[1].valueString = "Allgemeine Hilfsprodukte und unterstützende Technologien für die Kommunikation" 
* #e125 ^property[2].code = #parent 
* #e125 ^property[2].valueCode = #e1 
* #e125 ^property[3].code = #child 
* #e125 ^property[3].valueCode = #e1250 
* #e125 ^property[4].code = #child 
* #e125 ^property[4].valueCode = #e1251 
* #e125 ^property[5].code = #child 
* #e125 ^property[5].valueCode = #e1258 
* #e125 ^property[6].code = #child 
* #e125 ^property[6].valueCode = #e1259 
* #e1250 "Allgemeine Produkte und Technologien für die Kommunikation"
* #e1250 ^property[0].code = #None 
* #e1250 ^property[0].valueString = "Von Menschen für ihre Aktivitäten des Sendens und Empfangens von Informationen benutzte Ausrüstungsgegenstände, Produkte und Technologien wie optische und akustische Geräte, Tonaufnahme- und Empfangsgeräte, Fernseh- und Videogeräte, Telefongeräte und Zubehör, Tonübertragungssysteme, Verständigungshilfen bei Nahkommunikation, weder angepasst noch speziell entworfen" 
* #e1250 ^property[1].code = #parent 
* #e1250 ^property[1].valueCode = #e125 
* #e1251 "Hilfsprodukte und unterstützende Technologien für die Kommunikation"
* #e1251 ^property[0].code = #None 
* #e1251 ^property[0].valueString = "Angepasste oder speziell entworfene Ausrüstungsgegenstände, Produkte und Technologien, die Menschen helfen, Informationen zu senden und zu empfangen, wie optische und optisch-elektronische Geräte, Spezialschreib-, -zeichen- oder -handschreibgeräte, Signalsysteme sowie spezielle Computersoftware und -hardware, Cochlear-Implantate, Hörgeräte, FM-Hörtrainer, Stimmprothesen, Kommunikationstafeln, Brillen und Kontaktlinsen" 
* #e1251 ^property[1].code = #parent 
* #e1251 ^property[1].valueCode = #e125 
* #e1258 "Produkte und Technologien zur Kommunikation, anders bezeichnet"
* #e1258 ^property[0].code = #parent 
* #e1258 ^property[0].valueCode = #e125 
* #e1259 "Produkte und Technologien zur Kommunikation, nicht näher bezeichnet"
* #e1259 ^property[0].code = #parent 
* #e1259 ^property[0].valueCode = #e125 
* #e130 "Produkte und Technologien für Bildung/Ausbildung"
* #e130 ^property[0].code = #None 
* #e130 ^property[0].valueString = "Von Menschen für den Erwerb von Wissen, Fachwissen oder Fertigkeiten benutzte Ausrüstungsgegenstände, Produkte, Verfahren, Methoden und Technologien, einschließlich solcher, die angepasst oder speziell entworfen sind" 
* #e130 ^property[1].code = #None 
* #e130 ^property[1].valueString = "Allgemeine Produkte und unterstützende Technologien für Bildung/ Ausbildung" 
* #e130 ^property[2].code = #parent 
* #e130 ^property[2].valueCode = #e1 
* #e130 ^property[3].code = #child 
* #e130 ^property[3].valueCode = #e1300 
* #e130 ^property[4].code = #child 
* #e130 ^property[4].valueCode = #e1301 
* #e130 ^property[5].code = #child 
* #e130 ^property[5].valueCode = #e1308 
* #e130 ^property[6].code = #child 
* #e130 ^property[6].valueCode = #e1309 
* #e1300 "Allgemeine Produkte und Technologien für Bildung/Ausbildung"
* #e1300 ^property[0].code = #None 
* #e1300 ^property[0].valueString = "Von Menschen für den Erwerb von Wissen, Fachwissen oder Fertigkeiten auf jedem Niveau benutzte Ausrüstungsgegenstände, Produkte, Verfahren, Methoden und Technologien wie Bücher, Handbücher, pädagogisches Spielzeug, Computerhardware oder -software, weder angepasst noch speziell entworfen" 
* #e1300 ^property[1].code = #parent 
* #e1300 ^property[1].valueCode = #e130 
* #e1301 "Hilfsprodukte und unterstützende Technologien für Bildung/ Ausbildung"
* #e1301 ^property[0].code = #None 
* #e1301 ^property[0].valueString = "Angepasste oder speziell entworfene Ausrüstungsgegenstände, Produkte, Verfahren, Methoden und Technologien, die Menschen helfen, Wissen, Fachwissen und Fertigkeiten zu erwerben, wie spezielle Computertechnologie" 
* #e1301 ^property[1].code = #parent 
* #e1301 ^property[1].valueCode = #e130 
* #e1308 "Produkte und Technologien für Bildung/Ausbildung, anders bezeichnet"
* #e1308 ^property[0].code = #parent 
* #e1308 ^property[0].valueCode = #e130 
* #e1309 "Produkte und Technologien für Bildung/Ausbildung, nicht näher bezeichnet"
* #e1309 ^property[0].code = #parent 
* #e1309 ^property[0].valueCode = #e130 
* #e135 "Produkte und Technologien für die Erwerbstätigkeit"
* #e135 ^property[0].code = #None 
* #e135 ^property[0].valueString = "Zur Ermöglichung der Arbeitsaktivitäten im Rahmen der Erwerbstätigkeit benutzte Ausrüstungsgegenstände, Produkte und Technologien" 
* #e135 ^property[1].code = #None 
* #e135 ^property[1].valueString = "Allgemeine und Hilfsprodukte und unterstützende Technologien für die Erwerbstätigkeit" 
* #e135 ^property[2].code = #parent 
* #e135 ^property[2].valueCode = #e1 
* #e135 ^property[3].code = #child 
* #e135 ^property[3].valueCode = #e1350 
* #e135 ^property[4].code = #child 
* #e135 ^property[4].valueCode = #e1351 
* #e135 ^property[5].code = #child 
* #e135 ^property[5].valueCode = #e1358 
* #e135 ^property[6].code = #child 
* #e135 ^property[6].valueCode = #e1359 
* #e1350 "Allgemeine Produkte und Technologien für die Erwerbstätigkeit"
* #e1350 ^property[0].code = #None 
* #e1350 ^property[0].valueString = "Zur Erleichterung der Arbeitsaktivitäten im Rahmen der Erwerbstätigkeit benutzte Ausrüstungsgegenstände, Produkte und Technologien wie Werkzeuge, Maschinen und Büroausstattung, weder angepasst noch speziell entworfen" 
* #e1350 ^property[1].code = #parent 
* #e1350 ^property[1].valueCode = #e135 
* #e1351 "Hilfsprodukte und unterstützende Technologien für die Erwerbstätigkeit"
* #e1351 ^property[0].code = #None 
* #e1351 ^property[0].valueString = "Angepasste oder speziell entworfene Ausrüstungsgegenstände, Produkte, und Technologien zur Ermöglichung der Arbeit im Rahmen der Erwerbstätigkeit, wie einstellbare Tische, Schreibtische und Aktenschränke; Fernbedienung von Büroeingängen und Ausgängen; Computerhardware und -software, Zubehör und Umfeldkontrollgeräte, die es einem Individuum ermöglichen sollen, seine arbeitsbezogenen Aufgaben zu erfüllen und die Arbeitsumgebung zu steuern (z.B. Scanning, Fernbedienungen, sprachgesteuerte Systeme und Zeitschaltuhren)" 
* #e1351 ^property[1].code = #parent 
* #e1351 ^property[1].valueCode = #e135 
* #e1358 "Produkte und Technologien für Beschäftigung, anders bezeichnet"
* #e1358 ^property[0].code = #parent 
* #e1358 ^property[0].valueCode = #e135 
* #e1359 "Produkte und Technologien für Beschäftigung, nicht näher bezeichnet"
* #e1359 ^property[0].code = #parent 
* #e1359 ^property[0].valueCode = #e135 
* #e140 "Produkte und Technologien für Kultur, Freizeit und Sport"
* #e140 ^property[0].code = #None 
* #e140 ^property[0].valueString = "Für die Durchführung und Verbesserung der Kultur-, Freizeit- und Sportaktivitäten benutzte Ausrüstungsgegenstände, Produkte und Technologien, einschließlich solcher, die angepasst oder speziell entworfen sind" 
* #e140 ^property[1].code = #None 
* #e140 ^property[1].valueString = "Allgemeine und Hilfsprodukte und unterstützende Technologien für Kultur, Freizeit und Sport" 
* #e140 ^property[2].code = #parent 
* #e140 ^property[2].valueCode = #e1 
* #e140 ^property[3].code = #child 
* #e140 ^property[3].valueCode = #e1400 
* #e140 ^property[4].code = #child 
* #e140 ^property[4].valueCode = #e1401 
* #e140 ^property[5].code = #child 
* #e140 ^property[5].valueCode = #e1408 
* #e140 ^property[6].code = #child 
* #e140 ^property[6].valueCode = #e1409 
* #e1400 "Allgemeine Produkte und Technologien für Kultur, Freizeit und Sport"
* #e1400 ^property[0].code = #None 
* #e1400 ^property[0].valueString = "Für die Durchführung und Verbesserung der Kultur-, Freizeit- und Sportaktivitäten benutzte Ausrüstungsgegenstände, Produkte und Technologien wie Spielzeug, Ski, Tennisbälle und Musikinstrumente, weder angepasst noch speziell entworfen" 
* #e1400 ^property[1].code = #parent 
* #e1400 ^property[1].valueCode = #e140 
* #e1401 "Hilfsprodukte und unterstützende Technologien für Kultur, Freizeit und Sport"
* #e1401 ^property[0].code = #None 
* #e1401 ^property[0].valueString = "Angepasste oder speziell entworfene Ausrüstungsgegenstände, Produkte, und Technologien, die zur Durchführung und Verbesserung von Kultur-, Freizeit- und Sportaktivitäten benutzt werden, wie modifizierte Mobilitätsgeräte für den Sport, Anpassungen für musikalische und andere künstlerische Darbietungen" 
* #e1401 ^property[1].code = #parent 
* #e1401 ^property[1].valueCode = #e140 
* #e1408 "Produkte und Technologien für Kultur, Freizeit und Sport, anders bezeichnet"
* #e1408 ^property[0].code = #parent 
* #e1408 ^property[0].valueCode = #e140 
* #e1409 "Produkte und Technologien für Kultur, Freizeit und Sport, nicht näher bezeichnet"
* #e1409 ^property[0].code = #parent 
* #e1409 ^property[0].valueCode = #e140 
* #e145 "Produkte und Technologien zur Ausübung von Religion und Spiritualität"
* #e145 ^property[0].code = #None 
* #e145 ^property[0].valueString = "Einzel- oder massenproduzierte Produkte und Technologien, denen im Zusammenhang mit der Ausübung von Religion oder Spiritualität eine symbolische Bedeutung zukommt oder gegeben wird, einschließlich solcher, die angepasst oder speziell entworfen sind" 
* #e145 ^property[1].code = #None 
* #e145 ^property[1].valueString = "Allgemeine und Hilfsprodukte und unterstützende Technologien zur Ausübung von Religion und Spiritualität" 
* #e145 ^property[2].code = #parent 
* #e145 ^property[2].valueCode = #e1 
* #e145 ^property[3].code = #child 
* #e145 ^property[3].valueCode = #e1450 
* #e145 ^property[4].code = #child 
* #e145 ^property[4].valueCode = #e1451 
* #e145 ^property[5].code = #child 
* #e145 ^property[5].valueCode = #e1458 
* #e145 ^property[6].code = #child 
* #e145 ^property[6].valueCode = #e1459 
* #e1450 "Allgemeine Produkte und Technologien zur Ausübung von Religion oder Spiritualität"
* #e1450 ^property[0].code = #None 
* #e1450 ^property[0].valueString = "Einzel- oder massenproduzierte Produkte und Technologien, denen im Zusammenhang mit der Ausübung von Religion oder Spiritualität eine symbolische Bedeutung zukommt oder gegeben wird, wie Gemeindehäuser, Maibäume und Kopfschmuck, Masken, Kruzifixe, Menorah und Gebetsteppiche, weder angepasst noch speziell entworfen" 
* #e1450 ^property[1].code = #parent 
* #e1450 ^property[1].valueCode = #e145 
* #e1451 "Hilfsprodukte und unterstützende Technologien zur Ausübung von Religion oder Spiritualität"
* #e1451 ^property[0].code = #None 
* #e1451 ^property[0].valueString = "Angepasste oder speziell entworfene Ausrüstungsgegenstände, Produkte, und Technologien, denen im Zusammenhang mit der Ausübung von Religion oder Spiritualität eine symbolische Bedeutung zukommt oder gegeben wird, wie religiöse Bücher in Braille-Schrift, Tarockkarten in Braille und Spezialschutz für Rollstuhlräder für den Zugang von Tempeln" 
* #e1451 ^property[1].code = #parent 
* #e1451 ^property[1].valueCode = #e145 
* #e1458 "Produkte und Technologien zur Ausübung von Religion und Spiritualität, anders bezeichnet"
* #e1458 ^property[0].code = #parent 
* #e1458 ^property[0].valueCode = #e145 
* #e1459 "Produkte und Technologien zur Ausübung von Religion und Spiritualität, nicht näher bezeichnet"
* #e1459 ^property[0].code = #parent 
* #e1459 ^property[0].valueCode = #e145 
* #e150 "Entwurf, Konstruktion sowie Bauprodukte und Technologien von öffentlichen Gebäuden"
* #e150 ^property[0].code = #None 
* #e150 ^property[0].valueString = "Produkte und Technologien, für den öffentlichen Zugang geplant und konstruiert, welche die bebaute Umgebung (Innen- und Außenbereiche) eines Individuums bilden, einschließlich solcher, die angepasst oder speziell entworfen sind" 
* #e150 ^property[1].code = #None 
* #e150 ^property[1].valueString = "Entwurf, Konstruktion und Bauprodukte und Technologien von Ein- und Ausgängen, Einrichtungen und Wegeführung" 
* #e150 ^property[2].code = #parent 
* #e150 ^property[2].valueCode = #e1 
* #e150 ^property[3].code = #child 
* #e150 ^property[3].valueCode = #e1500 
* #e150 ^property[4].code = #child 
* #e150 ^property[4].valueCode = #e1501 
* #e150 ^property[5].code = #child 
* #e150 ^property[5].valueCode = #e1502 
* #e150 ^property[6].code = #child 
* #e150 ^property[6].valueCode = #e1508 
* #e150 ^property[7].code = #child 
* #e150 ^property[7].valueCode = #e1509 
* #e1500 "Entwurf, Konstruktion sowie Bauprodukte und Technologien für Zu- und Ausgänge von öffentlichen Gebäuden"
* #e1500 ^property[0].code = #None 
* #e1500 ^property[0].valueString = "Produkte und Technologien von Ein- und Ausgängen von für den öffentlichen Zugang geplanter und entworfener bebauter Umgebung, wie Entwurf, Bau und Konstruktion von Ein- und Ausgängen von Gebäuden für den öffentlichen Zugang (z.B. Arbeitsstätten, Läden und Theater), öffentliche Gebäude, mobile und fest eingebaute Rampen, automatische Türöffner, verlängerte Türklinken und ebenerdige Türschwellen" 
* #e1500 ^property[1].code = #parent 
* #e1500 ^property[1].valueCode = #e150 
* #e1501 "Entwurf, Konstruktion sowie Bauprodukte und Technologien für den Zugang zu Einrichtungen innerhalb öffentlicher Gebäude"
* #e1501 ^property[0].code = #None 
* #e1501 ^property[0].valueString = "Produkte und Technologien für Inneneinrichtungen in Entwurf, Konstruktion und Bau für den öffentlichen Zugang, wie Waschräume/Toiletten, Telefone, Audioschleifen, Lifts oder Aufzüge, Rolltreppen, Thermostaten (zur Regulierung der Temperatur) und zugänglich verteilte Sitzplätze in Auditorien oder Stadien" 
* #e1501 ^property[1].code = #parent 
* #e1501 ^property[1].valueCode = #e150 
* #e1502 "Entwurf, Konstruktion sowie Bauprodukte und Technologien zur Wegefindung, für Wegeführungen und zur Bezeichnung von Stellen in öffentlichen Gebäuden"
* #e1502 ^property[0].code = #None 
* #e1502 ^property[0].valueString = "Produkte und Technologien für den Innen- und Außenbereich von öffentlichen Gebäuden, die Menschen helfen, ihren Weg innerhalb und unmittelbar außerhalb von Bauten zu finden, und Orte, die sie aufsuchen möchten, lokalisieren, wie Anzeigen in Schrift oder Braille, Größe der Korridore, Bodenoberflächen, zugängliche (Informations-) Kioske und andere Arten von Hinweisen" 
* #e1502 ^property[1].code = #parent 
* #e1502 ^property[1].valueCode = #e150 
* #e1508 "Entwurf, Konstruktion sowie Bauprodukte und Technologien von öffentlichen Gebäuden, anders bezeichnet"
* #e1508 ^property[0].code = #parent 
* #e1508 ^property[0].valueCode = #e150 
* #e1509 "Entwurf, Konstruktion sowie Bauprodukte und Technologien von öffentlichen Gebäuden, nicht näher bezeichnet"
* #e1509 ^property[0].code = #parent 
* #e1509 ^property[0].valueCode = #e150 
* #e155 "Entwurf, Konstruktion sowie Bauprodukte und Technologien von privaten Gebäuden"
* #e155 ^property[0].code = #None 
* #e155 ^property[0].valueString = "Produkte und Technologien, für die private Nutzung geplant und konstruiert, welche die bebaute Umgebung (Innen- und Außenbereiche) eines Individuums bilden, einschließlich solcher, die angepasst oder speziell entworfen sind" 
* #e155 ^property[1].code = #None 
* #e155 ^property[1].valueString = "Entwurf, Konstruktion und Bauprodukte und Technologien von Ein- und Ausgängen, Einrichtungen und Wegeführung" 
* #e155 ^property[2].code = #parent 
* #e155 ^property[2].valueCode = #e1 
* #e155 ^property[3].code = #child 
* #e155 ^property[3].valueCode = #e1550 
* #e155 ^property[4].code = #child 
* #e155 ^property[4].valueCode = #e1551 
* #e155 ^property[5].code = #child 
* #e155 ^property[5].valueCode = #e1552 
* #e155 ^property[6].code = #child 
* #e155 ^property[6].valueCode = #e1558 
* #e155 ^property[7].code = #child 
* #e155 ^property[7].valueCode = #e1559 
* #e1550 "Entwurf, Konstruktion sowie Bauprodukte und Technologien für Zu- und Ausgänge von privaten Gebäuden"
* #e1550 ^property[0].code = #None 
* #e1550 ^property[0].valueString = "Produkte und Technologien von Ein- und Ausgängen von für die private Nutzung geplanter und entworfener bebauter Umgebung, wie Entwurf, Bau und Konstruktion von Ein- und Ausgängen von privaten Wohnstätten; mobile und fest eingebaute Rampen, automatische Türöffner, verlängerte Türklinken und ebenerdige Türschwellen" 
* #e1550 ^property[1].code = #parent 
* #e1550 ^property[1].valueCode = #e155 
* #e1551 "Entwurf, Konstruktion sowie Bauprodukte und Technologien für den Zugang zu Einrichtungen innerhalb von privaten Gebäuden"
* #e1551 ^property[0].code = #None 
* #e1551 ^property[0].valueString = "Produkte und Technologien für Inneneinrichtungen mit Bezug auf Entwurf, Konstruktion und Bau für die private Nutzung, wie Waschräume/Toiletten, Telefone, Audioschleifen, Küchenschränke und elektronische Regelungs- und Steuerungsgeräten in privaten Häusern/Wohnungen" 
* #e1551 ^property[1].code = #parent 
* #e1551 ^property[1].valueCode = #e155 
* #e1552 "Entwurf, Konstruktion sowie Bauprodukte und Technologien zur Wegefindung, für Wegeführungen und zur Bezeichnung von Stellen in privaten Gebäuden"
* #e1552 ^property[0].code = #None 
* #e1552 ^property[0].valueString = "Produkte und Technologien für den Innen- und Außenbereich zur Wegeführung in privaten Gebäuden, die Menschen helfen, ihren Weg innerhalb und unmittelbar außerhalb von Gebäuden zu finden, und die Orte, die sie aufsuchen möchten, zu lokalisieren, wie Anzeigen in Schrift oder Braille, Größe der Korridore und Bodenoberflächen" 
* #e1552 ^property[1].code = #parent 
* #e1552 ^property[1].valueCode = #e155 
* #e1558 "Entwurf, Konstruktion sowie Bauprodukte und Technologien von privaten Gebäuden, anders bezeichnet"
* #e1558 ^property[0].code = #parent 
* #e1558 ^property[0].valueCode = #e155 
* #e1559 "Entwurf, Konstruktion sowie Bauprodukte und Technologien von privaten Gebäuden, nicht näher bezeichnet"
* #e1559 ^property[0].code = #parent 
* #e1559 ^property[0].valueCode = #e155 
* #e160 "Produkte und Technologien der Flächennutzung"
* #e160 ^property[0].code = #None 
* #e160 ^property[0].valueString = "Produkte und Technologien für den Außenbereich, soweit sie sich auf die äußere Umgebung eines Individuums durch Umsetzung von Flächennutzungspolitik sowie der Raumplanung und -entwicklung auswirken, einschließlich solcher, die angepasst oder speziell entworfen sind" 
* #e160 ^property[1].code = #None 
* #e160 ^property[1].valueString = "Produkte und Technologien für den Außenbereich, die durch die Umsetzung von Flächennutzungspolitik geregelt werden wie ländliche Gebiete, Vorortsgebiete, Stadtgebiete, Parks, Natur- und Wildschutzgebiete" 
* #e160 ^property[2].code = #parent 
* #e160 ^property[2].valueCode = #e1 
* #e160 ^property[3].code = #child 
* #e160 ^property[3].valueCode = #e1600 
* #e160 ^property[4].code = #child 
* #e160 ^property[4].valueCode = #e1601 
* #e160 ^property[5].code = #child 
* #e160 ^property[5].valueCode = #e1602 
* #e160 ^property[6].code = #child 
* #e160 ^property[6].valueCode = #e1603 
* #e160 ^property[7].code = #child 
* #e160 ^property[7].valueCode = #e1608 
* #e160 ^property[8].code = #child 
* #e160 ^property[8].valueCode = #e1609 
* #e1600 "Produkte und Technologien der ländlichen Flächenentwicklung"
* #e1600 ^property[0].code = #None 
* #e1600 ^property[0].valueString = "Produkte und Technologien in ländlichen Außengebieten, soweit sie sich auf die äußere Umgebung eines Individuums durch Umsetzung von ländlicher Flächennutzungspolitik sowie der Raumplanung und -entwicklung auswirken, wie landwirtschaftliche Flächen, Wege und Wegweiser" 
* #e1600 ^property[1].code = #parent 
* #e1600 ^property[1].valueCode = #e160 
* #e1601 "Produkte und Technologien der Flächenentwicklung von Vorortsgebieten"
* #e1601 ^property[0].code = #None 
* #e1601 ^property[0].valueString = "Produkte und Technologien in Vorortsgebieten, soweit sie sich auf die äußere Umgebung eines Individuums durch Umsetzung von Flächennutzungspolitik für Vorortsgebiete sowie der Raumplanung und -entwicklung auswirken, wie Bordsteinabsenkungen, Wege, Wegweiser und Straßenbeleuchtung" 
* #e1601 ^property[1].code = #parent 
* #e1601 ^property[1].valueCode = #e160 
* #e1602 "Produkte und Technologien der Flächenentwicklung von Stadtgebieten"
* #e1602 ^property[0].code = #None 
* #e1602 ^property[0].valueString = "Produkte und Technologien in Stadtgebieten, soweit sie sich auf die äußere Umgebung eines Individuums durch Umsetzung von Flächennutzungspolitik für Stadtgebiete sowie der Raumplanung und -entwicklung auswirken, wie Bordsteinabsenkungen, Rampen, Wegweiser und Straßenbeleuchtung" 
* #e1602 ^property[1].code = #parent 
* #e1602 ^property[1].valueCode = #e160 
* #e1603 "Produkte und Technologien von Parks, Natur- und Wildschutzgebieten"
* #e1603 ^property[0].code = #None 
* #e1603 ^property[0].valueString = "Produkte und Technologien in Parks, Landschafts- und Naturschutzgebieten, soweit sie sich auf die äußere Umgebung eines Individuums durch Umsetzung von Flächennutzungspolitik sowie der Raumplanung und -entwicklung auswirken, wie Parkbezeichnungen und Wanderwege zur Wildtierbesichtigung" 
* #e1603 ^property[1].code = #parent 
* #e1603 ^property[1].valueCode = #e160 
* #e1608 "Produkte und Technologien der Flächenentwicklung, anders bezeichnet"
* #e1608 ^property[0].code = #parent 
* #e1608 ^property[0].valueCode = #e160 
* #e1609 "Produkte und Technologien der Flächenentwicklung, nicht näher bezeichnet"
* #e1609 ^property[0].code = #parent 
* #e1609 ^property[0].valueCode = #e160 
* #e165 "Vermögenswerte"
* #e165 ^property[0].code = #None 
* #e165 ^property[0].valueString = "Produkte oder Gegenstände des wirtschaftlichen Handels wie Geld, Waren, Immobilien und andere Wertsachen, die einem Individuum gehören oder zu deren Verwendung es berechtigt ist" 
* #e165 ^property[1].code = #None 
* #e165 ^property[1].valueString = "Materielle und immaterielle Produkte und Güter, finanzielle Vermögenswerte" 
* #e165 ^property[2].code = #parent 
* #e165 ^property[2].valueCode = #e1 
* #e165 ^property[3].code = #child 
* #e165 ^property[3].valueCode = #e1650 
* #e165 ^property[4].code = #child 
* #e165 ^property[4].valueCode = #e1651 
* #e165 ^property[5].code = #child 
* #e165 ^property[5].valueCode = #e1652 
* #e165 ^property[6].code = #child 
* #e165 ^property[6].valueCode = #e1658 
* #e165 ^property[7].code = #child 
* #e165 ^property[7].valueCode = #e1659 
* #e1650 "Finanzielle Vermögenswerte"
* #e1650 ^property[0].code = #None 
* #e1650 ^property[0].valueString = "Produkte wie Geld oder andere Finanzinstrumente, die als Tauschmittel für Arbeit, Kapitalgüter und Dienstleistungen dienen" 
* #e1650 ^property[1].code = #parent 
* #e1650 ^property[1].valueCode = #e165 
* #e1651 "Materielle Mittel"
* #e1651 ^property[0].code = #None 
* #e1651 ^property[0].valueString = "Produkte und Gegenstände wie Häuser und Land, Kleidung, Lebensmittel und technische Güter, die als Tauschmittel für Arbeit, Kapitalgüter und Dienstleistungen dienen" 
* #e1651 ^property[1].code = #parent 
* #e1651 ^property[1].valueCode = #e165 
* #e1652 "Immaterielle Vermögenswerte"
* #e1652 ^property[0].code = #None 
* #e1652 ^property[0].valueString = "Produkte wie geistiges Eigentum, Wissen und Fertigkeiten, die als Tauschmittel für Arbeit, Kapitalgüter und Dienstleistungen dienen" 
* #e1652 ^property[1].code = #parent 
* #e1652 ^property[1].valueCode = #e165 
* #e1658 "Vermögenswerte, anders bezeichnet"
* #e1658 ^property[0].code = #parent 
* #e1658 ^property[0].valueCode = #e165 
* #e1659 "Vermögenswerte, nicht näher bezeichnet"
* #e1659 ^property[0].code = #parent 
* #e1659 ^property[0].valueCode = #e165 
* #e198 "Produkte und Technologien, anders bezeichnet"
* #e198 ^property[0].code = #parent 
* #e198 ^property[0].valueCode = #e1 
* #e199 "Produkte und Technologien, nicht näher bezeichnet"
* #e199 ^property[0].code = #parent 
* #e199 ^property[0].valueCode = #e1 
* #e2 "Natürliche und vom Menschen veränderte Umwelt"
* #e2 ^property[0].code = #None 
* #e2 ^property[0].valueString = "Dieses Kapitel befasst sich mit belebten oder unbelebten Elementen der natürlichen oder materiellen Umwelt, mit vom Menschen veränderten Bestandteilen dieser Umwelt sowie mit Merkmalen menschlicher Bevölkerungen in dieser Umwelt." 
* #e2 ^property[1].code = #parent 
* #e2 ^property[1].valueCode = #e 
* #e2 ^property[2].code = #child 
* #e2 ^property[2].valueCode = #e210 
* #e2 ^property[3].code = #child 
* #e2 ^property[3].valueCode = #e215 
* #e2 ^property[4].code = #child 
* #e2 ^property[4].valueCode = #e220 
* #e2 ^property[5].code = #child 
* #e2 ^property[5].valueCode = #e225 
* #e2 ^property[6].code = #child 
* #e2 ^property[6].valueCode = #e230 
* #e2 ^property[7].code = #child 
* #e2 ^property[7].valueCode = #e235 
* #e2 ^property[8].code = #child 
* #e2 ^property[8].valueCode = #e240 
* #e2 ^property[9].code = #child 
* #e2 ^property[9].valueCode = #e245 
* #e2 ^property[10].code = #child 
* #e2 ^property[10].valueCode = #e250 
* #e2 ^property[11].code = #child 
* #e2 ^property[11].valueCode = #e255 
* #e2 ^property[12].code = #child 
* #e2 ^property[12].valueCode = #e260 
* #e2 ^property[13].code = #child 
* #e2 ^property[13].valueCode = #e298 
* #e2 ^property[14].code = #child 
* #e2 ^property[14].valueCode = #e299 
* #e210 "Physikalische Geographie"
* #e210 ^property[0].code = #None 
* #e210 ^property[0].valueString = "Merkmale der Landformen und Gewässer" 
* #e210 ^property[1].code = #None 
* #e210 ^property[1].valueString = "Merkmale der Geographie bezüglich Orographie (Relief, Art und Ausmaß von Land und Landformen einschließlich Höhe) und Hydrographie (Gewässer)" 
* #e210 ^property[2].code = #parent 
* #e210 ^property[2].valueCode = #e2 
* #e210 ^property[3].code = #child 
* #e210 ^property[3].valueCode = #e2100 
* #e210 ^property[4].code = #child 
* #e210 ^property[4].valueCode = #e2101 
* #e210 ^property[5].code = #child 
* #e210 ^property[5].valueCode = #e2108 
* #e210 ^property[6].code = #child 
* #e210 ^property[6].valueCode = #e2109 
* #e2100 "Landformen"
* #e2100 ^property[0].code = #None 
* #e2100 ^property[0].valueString = "Merkmale der Landformen wie Berge, Hügel, Täler, Ebenen" 
* #e2100 ^property[1].code = #parent 
* #e2100 ^property[1].valueCode = #e210 
* #e2101 "Gewässer"
* #e2101 ^property[0].code = #None 
* #e2101 ^property[0].valueString = "Merkmale von Gewässern wie Seen, Dämme, Flüsse und Wasserläufe" 
* #e2101 ^property[1].code = #parent 
* #e2101 ^property[1].valueCode = #e210 
* #e2108 "Physikalische Geographie, anders bezeichnet"
* #e2108 ^property[0].code = #parent 
* #e2108 ^property[0].valueCode = #e210 
* #e2109 "Physikalische Geographie, nicht näher bezeichnet"
* #e2109 ^property[0].code = #parent 
* #e2109 ^property[0].valueCode = #e210 
* #e215 "Bevölkerung"
* #e215 ^property[0].code = #None 
* #e215 ^property[0].valueString = "Gruppen von Menschen, die in einer bestimmten Umwelt leben, und die gleiche Art von Umweltanpassung aufweisen" 
* #e215 ^property[1].code = #None 
* #e215 ^property[1].valueString = "Demographischer Wandel, Bevölkerungsdichte" 
* #e215 ^property[2].code = #parent 
* #e215 ^property[2].valueCode = #e2 
* #e215 ^property[3].code = #child 
* #e215 ^property[3].valueCode = #e2150 
* #e215 ^property[4].code = #child 
* #e215 ^property[4].valueCode = #e2151 
* #e215 ^property[5].code = #child 
* #e215 ^property[5].valueCode = #e2158 
* #e215 ^property[6].code = #child 
* #e215 ^property[6].valueCode = #e2159 
* #e2150 "Demographischer Wandel"
* #e2150 ^property[0].code = #None 
* #e2150 ^property[0].valueString = "Veränderungen innerhalb von Bevölkerungsgruppen, wie Zusammensetzung und Schwankungen der Gesamtzahl von Individuen eines Gebietes, die durch Geburten, Todesfälle und Altern der Bevölkerung sowie durch Wanderungsbewegungen verursacht werden" 
* #e2150 ^property[1].code = #parent 
* #e2150 ^property[1].valueCode = #e215 
* #e2151 "Bevölkerungsdichte"
* #e2151 ^property[0].code = #None 
* #e2151 ^property[0].valueString = "Anzahl der Individuen pro Flächeneinheit des Gebietes einschließlich Merkmale wie hohe und niedrige Dichte" 
* #e2151 ^property[1].code = #parent 
* #e2151 ^property[1].valueCode = #e215 
* #e2158 "Bevölkerung, anders bezeichnet"
* #e2158 ^property[0].code = #parent 
* #e2158 ^property[0].valueCode = #e215 
* #e2159 "Bevölkerung, nicht näher bezeichnet"
* #e2159 ^property[0].code = #parent 
* #e2159 ^property[0].valueCode = #e215 
* #e220 "Flora und Fauna"
* #e220 ^property[0].code = #None 
* #e220 ^property[0].valueString = "Pflanzen und Tiere" 
* #e220 ^property[1].code = #None 
* #e220 ^property[1].valueString = "Domestizierte Tiere" 
* #e220 ^property[2].code = #parent 
* #e220 ^property[2].valueCode = #e2 
* #e220 ^property[3].code = #child 
* #e220 ^property[3].valueCode = #e2200 
* #e220 ^property[4].code = #child 
* #e220 ^property[4].valueCode = #e2201 
* #e220 ^property[5].code = #child 
* #e220 ^property[5].valueCode = #e2208 
* #e220 ^property[6].code = #child 
* #e220 ^property[6].valueCode = #e2209 
* #e2200 "Pflanzen"
* #e2200 ^property[0].code = #None 
* #e2200 ^property[0].valueString = "Alle Arten von zur Photosynthese fähigen, eukaryotischen und multizellulären Organismen aus dem Pflanzenreich, die charakteristischerweise Embryonen produzieren, Chloroplasten enthalten sowie Zellwände aus Zellulose haben und sich nicht fortbewegen können, wie Bäume, Blumen, Sträucher und Rebengewächse" 
* #e2200 ^property[1].code = #parent 
* #e2200 ^property[1].valueCode = #e220 
* #e2201 "Tiere"
* #e2201 ^property[0].code = #None 
* #e2201 ^property[0].valueString = "Multizelluläre Organismen aus dem Tierreich, die sich von Pflanzen in gewissen typischen Charakteristiken unterscheiden, wie Fähigkeit der Bewegung, nicht-photosynthetischer Stoffwechsel, ausgeprägte Reaktion auf Reize, eingeschränktes Wachstum und festgelegte Körperstruktur, wie wilde oder Nutztiere, Reptilien, Vögel, Fische und Säugetiere" 
* #e2201 ^property[1].code = #None 
* #e2201 ^property[1].valueString = "Vermögenswerte" 
* #e2201 ^property[2].code = #parent 
* #e2201 ^property[2].valueCode = #e220 
* #e2208 "Pflanzen und Tiere, anders bezeichnet"
* #e2208 ^property[0].code = #parent 
* #e2208 ^property[0].valueCode = #e220 
* #e2209 "Pflanzen und Tiere, nicht näher bezeichnet"
* #e2209 ^property[0].code = #parent 
* #e2209 ^property[0].valueCode = #e220 
* #e225 "Klima"
* #e225 ^property[0].code = #None 
* #e225 ^property[0].valueString = "Meteorologische Merkmale und Ereignisse wie das Wetter" 
* #e225 ^property[1].code = #None 
* #e225 ^property[1].valueString = "Temperatur, Luftfeuchtigkeit, Luftdruck, Niederschlag, Wind und jahreszeitabhängige Veränderungen" 
* #e225 ^property[2].code = #parent 
* #e225 ^property[2].valueCode = #e2 
* #e225 ^property[3].code = #child 
* #e225 ^property[3].valueCode = #e2250 
* #e225 ^property[4].code = #child 
* #e225 ^property[4].valueCode = #e2251 
* #e225 ^property[5].code = #child 
* #e225 ^property[5].valueCode = #e2252 
* #e225 ^property[6].code = #child 
* #e225 ^property[6].valueCode = #e2253 
* #e225 ^property[7].code = #child 
* #e225 ^property[7].valueCode = #e2254 
* #e225 ^property[8].code = #child 
* #e225 ^property[8].valueCode = #e2255 
* #e225 ^property[9].code = #child 
* #e225 ^property[9].valueCode = #e2258 
* #e225 ^property[10].code = #child 
* #e225 ^property[10].valueCode = #e2259 
* #e2250 "Temperatur"
* #e2250 ^property[0].code = #None 
* #e2250 ^property[0].valueString = "Wärme- oder Kältegrade, wie hohe und niedrige Temperatur, normale oder extreme Temperaturen" 
* #e2250 ^property[1].code = #parent 
* #e2250 ^property[1].valueCode = #e225 
* #e2251 "Luftfeuchtigkeit"
* #e2251 ^property[0].code = #None 
* #e2251 ^property[0].valueString = "Feuchtigkeitsgrad der Luft, wie hohe und niedrige Feuchtigkeit" 
* #e2251 ^property[1].code = #parent 
* #e2251 ^property[1].valueCode = #e225 
* #e2252 "Luftdruck"
* #e2252 ^property[0].code = #None 
* #e2252 ^property[0].valueString = "Druck der umgebenen Luft, wie der Druck bezogen auf die Höhe über dem Meeresspiegel oder auf meteorologische Bedingungen" 
* #e2252 ^property[1].code = #parent 
* #e2252 ^property[1].valueCode = #e225 
* #e2253 "Niederschlag"
* #e2253 ^property[0].code = #None 
* #e2253 ^property[0].valueString = "Feuchtigkeitsniederschlag, wie Regen, Tau, Schnee, Schneeregen und Hagel" 
* #e2253 ^property[1].code = #parent 
* #e2253 ^property[1].valueCode = #e225 
* #e2254 "Wind"
* #e2254 ^property[0].code = #None 
* #e2254 ^property[0].valueString = "Luft in mehr oder weniger schneller natürlicher Bewegung, wie Brise, Sturm oder Bö" 
* #e2254 ^property[1].code = #parent 
* #e2254 ^property[1].valueCode = #e225 
* #e2255 "Jahreszeitliche Veränderungen"
* #e2255 ^property[0].code = #None 
* #e2255 ^property[0].valueString = "Natürliche, regelmäßige und voraussagbare Änderungen von einer Jahreszeit zur nächsten, wie Sommer, Herbst, Winter und Frühling" 
* #e2255 ^property[1].code = #parent 
* #e2255 ^property[1].valueCode = #e225 
* #e2258 "Klima, anders bezeichnet"
* #e2258 ^property[0].code = #parent 
* #e2258 ^property[0].valueCode = #e225 
* #e2259 "Klima, nicht näher bezeichnet"
* #e2259 ^property[0].code = #parent 
* #e2259 ^property[0].valueCode = #e225 
* #e230 "Natürliche Ereignisse"
* #e230 ^property[0].code = #None 
* #e230 ^property[0].valueString = "Regelmäßige oder unregelmäßige geographische und atmosphärische Veränderungen, die eine erhebliche Beeinträchtigung der Umwelt eines Individuums zur Folge haben, wie Erdbeben, Unwetter, z.B. Orkane, Tornados, Hurrikane, Überflutungen, Waldbrände, Eisstürme" 
* #e230 ^property[1].code = #parent 
* #e230 ^property[1].valueCode = #e2 
* #e235 "Vom Menschen verursachte Ereignisse"
* #e235 ^property[0].code = #None 
* #e235 ^property[0].valueString = "Vom Menschen verursachte Veränderungen oder Störungen der natürlichen Umwelt, die eine erhebliche Beeinträchtigung des täglichen Lebens der Menschen der Region zur Folge haben können, einschließlich Ereignisse oder Bedingungen im Zusammenhang mit Konflikten und Kriegen, wie Vertreibung von Menschen, Zerstörung der sozialen Infrastruktur, von Häusern und Land, Umweltkatastrophen sowie Land-, Wasser- und Luftverschmutzung (z.B. Freisetzung giftiger Substanzen)" 
* #e235 ^property[1].code = #parent 
* #e235 ^property[1].valueCode = #e2 
* #e240 "Licht"
* #e240 ^property[0].code = #None 
* #e240 ^property[0].valueString = "Elektromagnetische Strahlung, durch die Dinge sichtbar gemacht werden, entweder durch Sonnenlicht oder künstliches Licht (z.B. Kerzen, Öl- oder Petroleumlampen, Feuer und Elektrizität) und die nützliche oder verwirrende Informationen über die Welt liefern kann" 
* #e240 ^property[1].code = #None 
* #e240 ^property[1].valueString = "Lichtintensität, Lichtqualität, Farbkontraste" 
* #e240 ^property[2].code = #parent 
* #e240 ^property[2].valueCode = #e2 
* #e240 ^property[3].code = #child 
* #e240 ^property[3].valueCode = #e2400 
* #e240 ^property[4].code = #child 
* #e240 ^property[4].valueCode = #e2401 
* #e240 ^property[5].code = #child 
* #e240 ^property[5].valueCode = #e2408 
* #e240 ^property[6].code = #child 
* #e240 ^property[6].valueCode = #e2409 
* #e2400 "Lichtintensität"
* #e2400 ^property[0].code = #None 
* #e2400 ^property[0].valueString = "Energieniveau oder -betrag, der von einer natürlichen (z.B. Sonne) oder künstlichen Lichtquelle emittiert wird" 
* #e2400 ^property[1].code = #parent 
* #e2400 ^property[1].valueCode = #e240 
* #e2401 "Lichtqualität"
* #e2401 ^property[0].code = #None 
* #e2401 ^property[0].valueString = "Die Art des zur Verfügung stehenden Lichtes und die entsprechenden, in der sichtbaren Umgebung entstehenden Farbkontraste, die nützliche Informationen (z.B. visuelle Informationen über das Vorhandensein von Treppen oder einer Tür) oder verwirrende Informationen (z.B. zu viele visuelle Bilder) über die Welt liefern können" 
* #e2401 ^property[1].code = #parent 
* #e2401 ^property[1].valueCode = #e240 
* #e2408 "Licht, anders bezeichnet"
* #e2408 ^property[0].code = #parent 
* #e2408 ^property[0].valueCode = #e240 
* #e2409 "Licht, nicht näher bezeichnet"
* #e2409 ^property[0].code = #parent 
* #e2409 ^property[0].valueCode = #e240 
* #e245 "Zeitbezogene Veränderungen"
* #e245 ^property[0].code = #None 
* #e245 ^property[0].valueString = "Natürliche, regelmäßige oder vorhersagbare zeitliche Veränderungen" 
* #e245 ^property[1].code = #None 
* #e245 ^property[1].valueString = "Tag/Nacht-Zyklen und Mondphasen" 
* #e245 ^property[2].code = #parent 
* #e245 ^property[2].valueCode = #e2 
* #e245 ^property[3].code = #child 
* #e245 ^property[3].valueCode = #e2450 
* #e245 ^property[4].code = #child 
* #e245 ^property[4].valueCode = #e2451 
* #e245 ^property[5].code = #child 
* #e245 ^property[5].valueCode = #e2458 
* #e245 ^property[6].code = #child 
* #e245 ^property[6].valueCode = #e2459 
* #e2450 "Tag/Nacht-Zyklen"
* #e2450 ^property[0].code = #None 
* #e2450 ^property[0].valueString = "Natürliche, regelmäßige oder vorhersagbare Veränderungen vom Tag zur Nacht und wieder zum Tag, wie Tag, Nacht, Morgen- und Abenddämmerung" 
* #e2450 ^property[1].code = #parent 
* #e2450 ^property[1].valueCode = #e245 
* #e2451 "Mondphasen"
* #e2451 ^property[0].code = #None 
* #e2451 ^property[0].valueString = "Natürliche, regelmäßige oder vorhersagbare Veränderungen der Mondposition im Verhältnis zur Erde" 
* #e2451 ^property[1].code = #parent 
* #e2451 ^property[1].valueCode = #e245 
* #e2458 "Zeitbezogene Veränderungen, anders bezeichnet"
* #e2458 ^property[0].code = #parent 
* #e2458 ^property[0].valueCode = #e245 
* #e2459 "Zeitbezogene Veränderungen, nicht näher bezeichnet"
* #e2459 ^property[0].code = #parent 
* #e2459 ^property[0].valueCode = #e245 
* #e250 "Laute und Geräusche"
* #e250 ^property[0].code = #None 
* #e250 ^property[0].valueString = "Phänomene, die gehört werden oder gehört werden können, wie Knallen, Klingeln, Hämmern, Singen, Pfeifen, Schreien oder Brummen, in jeder Lautstärke, Tonhöhe oder Ton, und die nützliche oder verwirrende Informationen über die Welt liefern können" 
* #e250 ^property[1].code = #None 
* #e250 ^property[1].valueString = "Laut-/Geräuschintensität oder -stärke und Laut-/Geräuschqualität" 
* #e250 ^property[2].code = #parent 
* #e250 ^property[2].valueCode = #e2 
* #e250 ^property[3].code = #child 
* #e250 ^property[3].valueCode = #e2500 
* #e250 ^property[4].code = #child 
* #e250 ^property[4].valueCode = #e2501 
* #e250 ^property[5].code = #child 
* #e250 ^property[5].valueCode = #e2508 
* #e250 ^property[6].code = #child 
* #e250 ^property[6].valueCode = #e2509 
* #e2500 "Laut-/Geräuschintensität oder -stärke"
* #e2500 ^property[0].code = #None 
* #e2500 ^property[0].valueString = "Niveau oder Ausmaß eines hörbaren Phänomens, das durch die erzeugte Energiemenge bestimmt wird, wobei ein hohes Energieniveau als lautes Geräusch und ein niedriges Energieniveau als leises Geräusch wahrgenommen wird" 
* #e2500 ^property[1].code = #parent 
* #e2500 ^property[1].valueCode = #e250 
* #e2501 "Laut-/Geräuschqualität"
* #e2501 ^property[0].code = #None 
* #e2501 ^property[0].valueString = "Art eines Lautes oder Geräusches, das durch Wellenlänge und -muster des Lautes/Geräusches bestimmt ist und als Tonhöhe und Klang wahrgenommen wird, wie Schrillheit oder Klangfülle, und das nützliche Informationen (z.B. die Laute eines Hundes, der eine miauende Katze verbellt) oder verwirrende Informationen (z.B. Hintergrundlärm) über die Welt liefern kann" 
* #e2501 ^property[1].code = #parent 
* #e2501 ^property[1].valueCode = #e250 
* #e2508 "Laute und Geräusche, anders bezeichnet"
* #e2508 ^property[0].code = #parent 
* #e2508 ^property[0].valueCode = #e250 
* #e2509 "Laute und Geräusche, nicht näher bezeichnet"
* #e2509 ^property[0].code = #parent 
* #e2509 ^property[0].valueCode = #e250 
* #e255 "Schwingung"
* #e255 ^property[0].code = #None 
* #e255 ^property[0].valueString = "Regelmäßige oder unregelmäßige Hin- und Herbewegung eines Gegenstandes oder einer Person infolge einer physikalischen Störung, wie Schütteln, Beben, schnelle ruckartige Bewegungen von Dingen, Gebäuden oder Menschen, verursacht durch kleine oder große Ausrüstung, Luftfahrzeuge und Explosionen" 
* #e255 ^property[1].code = #None 
* #e255 ^property[1].valueString = "Natürliche Ereignisse" 
* #e255 ^property[2].code = #parent 
* #e255 ^property[2].valueCode = #e2 
* #e260 "Luftqualität"
* #e260 ^property[0].code = #None 
* #e260 ^property[0].valueString = "Eigenschaften der Atmosphäre (außerhalb von Gebäuden) oder der Luft in umschlossenen Räumen (innerhalb von Gebäuden), und die nützliche oder verwirrende Informationen über die Welt liefern können" 
* #e260 ^property[1].code = #None 
* #e260 ^property[1].valueString = "Luftqualität in Innen- oder Außenbereichen" 
* #e260 ^property[2].code = #parent 
* #e260 ^property[2].valueCode = #e2 
* #e260 ^property[3].code = #child 
* #e260 ^property[3].valueCode = #e2600 
* #e260 ^property[4].code = #child 
* #e260 ^property[4].valueCode = #e2601 
* #e260 ^property[5].code = #child 
* #e260 ^property[5].valueCode = #e2608 
* #e260 ^property[6].code = #child 
* #e260 ^property[6].valueCode = #e2609 
* #e2600 "Luftqualität in Innenbereichen"
* #e2600 ^property[0].code = #None 
* #e2600 ^property[0].valueString = "Art der Luft innerhalb von Gebäuden oder umschlossenen Räumen, wie sie durch Gerüche, Rauch, Feuchtigkeit, Klimatisierung (gesteuerte Luftqualität) oder ungesteuerte Luftqualität bestimmt wird, und die nützliche Informationen (z.B. Geruch von ausströmendem Gas) oder verwirrende Informationen (z.B. überwältigender Geruch von Parfüm) über die Welt liefern kann" 
* #e2600 ^property[1].code = #parent 
* #e2600 ^property[1].valueCode = #e260 
* #e2601 "Luftqualität in Außenbereichen"
* #e2601 ^property[0].code = #None 
* #e2601 ^property[0].valueString = "Art der Luft außerhalb von Gebäuden oder umschlossenen Räumen, wie sie durch Gerüche, Rauch, Feuchtigkeit, Ozongehalt der Luft und andere Eigenschaften der Atmosphäre bestimmt wird, und die nützliche Informationen (z.B. Geruch von Regen) oder verwirrende Informationen (z.B. giftige Gerüche) über die Welt liefern kann" 
* #e2601 ^property[1].code = #parent 
* #e2601 ^property[1].valueCode = #e260 
* #e2608 "Luftqualität, anders bezeichnet"
* #e2608 ^property[0].code = #parent 
* #e2608 ^property[0].valueCode = #e260 
* #e2609 "Luftqualität, nicht näher bezeichnet"
* #e2609 ^property[0].code = #parent 
* #e2609 ^property[0].valueCode = #e260 
* #e298 "Natürliche und vom Menschen veränderte Umwelt, anders bezeichnet"
* #e298 ^property[0].code = #parent 
* #e298 ^property[0].valueCode = #e2 
* #e299 "Natürliche und vom Menschen veränderte Umwelt, nicht näher bezeichnet"
* #e299 ^property[0].code = #parent 
* #e299 ^property[0].valueCode = #e2 
* #e3 "Unterstützung und Beziehungen"
* #e3 ^property[0].code = #None 
* #e3 ^property[0].valueString = "Dieses Kapitel befasst sich mit Personen oder Tieren, die praktische physische oder emotionale Unterstützung, Fürsorge, Schutz, Hilfe und Beziehungen zu anderen Personen geben, sowie mit Beziehungen zu anderen Personen in deren Wohnungen, am Arbeitsplatz, in der Schule, beim Spielen oder in anderen Bereichen ihrer alltäglichen Aktivitäten. Das Kapitel umfasst nicht die Einstellungen der Person oder der Menschen, die die Unterstützung leisten. Der hier beschriebene Umweltfaktor ist nicht die Person oder das Tier, sondern das Ausmaß an physischer und emotionaler Unterstützung, die die Person oder das Tier geben." 
* #e3 ^property[1].code = #parent 
* #e3 ^property[1].valueCode = #e 
* #e3 ^property[2].code = #child 
* #e3 ^property[2].valueCode = #e310 
* #e3 ^property[3].code = #child 
* #e3 ^property[3].valueCode = #e315 
* #e3 ^property[4].code = #child 
* #e3 ^property[4].valueCode = #e320 
* #e3 ^property[5].code = #child 
* #e3 ^property[5].valueCode = #e325 
* #e3 ^property[6].code = #child 
* #e3 ^property[6].valueCode = #e330 
* #e3 ^property[7].code = #child 
* #e3 ^property[7].valueCode = #e335 
* #e3 ^property[8].code = #child 
* #e3 ^property[8].valueCode = #e340 
* #e3 ^property[9].code = #child 
* #e3 ^property[9].valueCode = #e345 
* #e3 ^property[10].code = #child 
* #e3 ^property[10].valueCode = #e350 
* #e3 ^property[11].code = #child 
* #e3 ^property[11].valueCode = #e355 
* #e3 ^property[12].code = #child 
* #e3 ^property[12].valueCode = #e360 
* #e3 ^property[13].code = #child 
* #e3 ^property[13].valueCode = #e398 
* #e3 ^property[14].code = #child 
* #e3 ^property[14].valueCode = #e399 
* #e310 "Engster Familienkreis"
* #e310 ^property[0].code = #None 
* #e310 ^property[0].valueString = "Personen, die infolge Geburt oder Heirat verwandt sind oder andere Beziehungen, die von der Kultur als 'engster Familienkreis' anerkannt sind, wie Ehepartner, Lebensgefährten, Eltern, Geschwister, Kinder, Pflegeeltern, Adoptiveltern und Großeltern" 
* #e310 ^property[1].code = #None 
* #e310 ^property[1].valueString = "Erweiterter Familienkreis" 
* #e310 ^property[2].code = #parent 
* #e310 ^property[2].valueCode = #e3 
* #e315 "Erweiterter Familienkreis"
* #e315 ^property[0].code = #None 
* #e315 ^property[0].valueString = "Personen, die über Familie oder Heirat verwandt sind oder andere Beziehungen, die von der Kultur als 'erweiterter Familienkreis' anerkannt sind, wie Tanten, Onkel, Neffen, Nichten" 
* #e315 ^property[1].code = #None 
* #e315 ^property[1].valueString = "Engster Familienkreis" 
* #e315 ^property[2].code = #parent 
* #e315 ^property[2].valueCode = #e3 
* #e320 "Freunde"
* #e320 ^property[0].code = #None 
* #e320 ^property[0].valueString = "Personen, die sich nahe stehen und deren kontinuierliche Bekanntschaft durch Vertrauen und gegenseitige Unterstützung gekennzeichnet ist" 
* #e320 ^property[1].code = #parent 
* #e320 ^property[1].valueCode = #e3 
* #e325 "Bekannte, Seinesgleichen (Peers), Kollegen, Nachbarn und andere Gemeindemitglieder"
* #e325 ^property[0].code = #None 
* #e325 ^property[0].valueString = "Personen, die sich als Bekannte, Seinesgleichen, Kollegen, Nachbarn und als Gemeindemitglieder kennen, etwa von der Arbeit, Schule oder Freizeit, über Kommunikationssysteme wie Telefon, Fernschreiber, Internet, E-Mail oder über andere Möglichkeiten, und die demographische Eigenschaften wie Alter, Geschlecht, religiöses Bekenntnis, ethnische Zugehörigkeit oder gemeinsame Interesse teilen" 
* #e325 ^property[1].code = #None 
* #e325 ^property[1].valueString = "Dienste von Vereinigungen und Organisationen" 
* #e325 ^property[2].code = #parent 
* #e325 ^property[2].valueCode = #e3 
* #e330 "Autoritätspersonen"
* #e330 ^property[0].code = #None 
* #e330 ^property[0].valueString = "Personen mit Entscheidungsverantwortung für andere, die infolge ihrer sozialen, ökonomischen, kulturellen oder religiösen Rollen in der Gesellschaft sozial definierten Einfluss oder Befugnisse haben, wie Lehrer, Arbeitgeber, Supervisoren, religiöse Führer, Vertreter im Amt, Vormund, Treuhänder" 
* #e330 ^property[1].code = #parent 
* #e330 ^property[1].valueCode = #e3 
* #e335 "Untergebene"
* #e335 ^property[0].code = #None 
* #e335 ^property[0].valueString = "Personen, deren tägliches Leben bei der Arbeit, in der Schule oder in anderen Bereichen durch Autoritätspersonen beeinflusst wird, wie Schüler, Studenten, Arbeiter und Mitglieder religiöser Gruppen" 
* #e335 ^property[1].code = #None 
* #e335 ^property[1].valueString = "Engster Familienkreis" 
* #e335 ^property[2].code = #parent 
* #e335 ^property[2].valueCode = #e3 
* #e340 "Persönliche Hilfs- und Pflegepersonen"
* #e340 ^property[0].code = #None 
* #e340 ^property[0].valueString = "Personen, die Dienstleistungen erbringen, welche erforderlich sind, um Personen bei ihren täglichen Aktivitäten, bei der Erhaltung und Durchführung der Arbeit am Arbeitsplatz, im Bildungs-/Ausbildungsbereich oder in anderen Lebenssituationen zu unterstützen, wobei dieser Dienst entweder durch öffentliche oder private Träger erfolgt oder auf ehrenamtlicher Basis, wie Anbieter von Hilfen bei Hausarbeit und Haushaltsführung, personeller Assistenz, Assistenz beim Transport und anderen Unterstützungserfordernissen durch bezahlte Hilfen, Kindermädchen und andere, die vornehmlich Betreuungs- oder Pflegeleistungen erbringen" 
* #e340 ^property[1].code = #None 
* #e340 ^property[1].valueString = "Engster Familienkreis" 
* #e340 ^property[2].code = #parent 
* #e340 ^property[2].valueCode = #e3 
* #e345 "Fremde"
* #e345 ^property[0].code = #None 
* #e345 ^property[0].valueString = "Personen, die sich weder kennen noch verwandt sind oder die bisher weder eine Beziehung eingegangen sind noch Kontakt zueinander haben, einschließlich Personen, die einer bestimmten Person zwar unbekannt sind, die jedoch eine Lebenssituation mit ihr teilen, wie Vertretungslehrer, Mitarbeiter oder Pflegekräfte" 
* #e345 ^property[1].code = #parent 
* #e345 ^property[1].valueCode = #e3 
* #e350 "Domestizierte Tiere"
* #e350 ^property[0].code = #None 
* #e350 ^property[0].valueString = "Tiere, die physische, emotionale oder psychische Unterstützung geben, wie Haustiere (Hunde, Katzen, Vögel, Fische usw.) und Tiere für persönliche Mobilität und Transport" 
* #e350 ^property[1].code = #None 
* #e350 ^property[1].valueString = "Tiere" 
* #e350 ^property[2].code = #parent 
* #e350 ^property[2].valueCode = #e3 
* #e355 "Fachleute der Gesundheitsberufe"
* #e355 ^property[0].code = #None 
* #e355 ^property[0].valueString = "Alle Dienstleistungserbringer, die im Gesundheitssystem arbeiten, wie Ärzte, Pflegekräfte, Physiotherapeuten, Ergotherapeuten, Sprachtherapeuten, Audiologen, Hersteller von Orthesen und Prothesen, Sozialarbeiter im Gesundheitswesen" 
* #e355 ^property[1].code = #None 
* #e355 ^property[1].valueString = "Andere Fachleute" 
* #e355 ^property[2].code = #parent 
* #e355 ^property[2].valueCode = #e3 
* #e360 "Andere Fachleute"
* #e360 ^property[0].code = #None 
* #e360 ^property[0].valueString = "Alle Fachleute, die außerhalb des Gesundheitssystems arbeiten, einschließlich Sozialarbeiter, Rechtanwälte, Lehrer, Architekten und Konstrukteure" 
* #e360 ^property[1].code = #None 
* #e360 ^property[1].valueString = "Fachleute der Gesundheitsberufe" 
* #e360 ^property[2].code = #parent 
* #e360 ^property[2].valueCode = #e3 
* #e398 "Unterstützung und Beziehungen, anders bezeichnet"
* #e398 ^property[0].code = #parent 
* #e398 ^property[0].valueCode = #e3 
* #e399 "Unterstützung und Beziehungen, nicht näher bezeichnet"
* #e399 ^property[0].code = #parent 
* #e399 ^property[0].valueCode = #e3 
* #e4 "Einstellungen"
* #e4 ^property[0].code = #None 
* #e4 ^property[0].valueString = "Dieses Kapitel befasst sich mit Einstellungen, die beobachtbare Konsequenzen von Sitten, Bräuchen, Weltanschauungen, Werten, Normen, tatsächlichen oder religiösen Überzeugungen sind. Diese Einstellungen beeinflussen individuelles Verhalten und soziales Leben auf allen Ebenen, von zwischenmenschlichen Beziehungen, Kontakten in der Gemeinde, bis zu politischen, wirtschaftlichen und rechtlichen Strukturen. So können zum Beispiel individuelle oder gesellschaftliche Einstellungen zu Vertrauenswürdigkeit und Wert einer Person zu ehrenhaftem oder negativem und diskriminierendem Umgang (z.B. Stigmatisierung, Stereotypisierung und Marginalisierung oder Vernachlässigung der Person) motivieren. Die klassifizierten Einstellungen beziehen sich auf Personen des Umfeldes der zu beschreibenden Person und nicht auf die zu beschreibende Person selbst. Die individuellen Einstellungen sind bezüglich der Arten der Beziehungen, die in Kapitel 3 der Umweltfaktoren aufgelistet sind, kategorisiert. Werte und Überzeugungen sind nicht gesondert von den Einstellungen kodiert, weil angenommen wird, dass sie die treibenden Kräfte hinter den Einstellungen sind." 
* #e4 ^property[1].code = #parent 
* #e4 ^property[1].valueCode = #e 
* #e4 ^property[2].code = #child 
* #e4 ^property[2].valueCode = #e410 
* #e4 ^property[3].code = #child 
* #e4 ^property[3].valueCode = #e415 
* #e4 ^property[4].code = #child 
* #e4 ^property[4].valueCode = #e420 
* #e4 ^property[5].code = #child 
* #e4 ^property[5].valueCode = #e425 
* #e4 ^property[6].code = #child 
* #e4 ^property[6].valueCode = #e430 
* #e4 ^property[7].code = #child 
* #e4 ^property[7].valueCode = #e435 
* #e4 ^property[8].code = #child 
* #e4 ^property[8].valueCode = #e440 
* #e4 ^property[9].code = #child 
* #e4 ^property[9].valueCode = #e445 
* #e4 ^property[10].code = #child 
* #e4 ^property[10].valueCode = #e450 
* #e4 ^property[11].code = #child 
* #e4 ^property[11].valueCode = #e455 
* #e4 ^property[12].code = #child 
* #e4 ^property[12].valueCode = #e460 
* #e4 ^property[13].code = #child 
* #e4 ^property[13].valueCode = #e465 
* #e4 ^property[14].code = #child 
* #e4 ^property[14].valueCode = #e498 
* #e4 ^property[15].code = #child 
* #e4 ^property[15].valueCode = #e499 
* #e410 "Individuelle Einstellungen der Mitglieder des engsten Familienkreises"
* #e410 ^property[0].code = #None 
* #e410 ^property[0].valueString = "Allgemeine oder spezifische Meinungen und Überzeugungen der Mitglieder des engsten Familienkreises, die eine bestimmte Person oder andere Dinge (z.B. soziale, politische und ökonomische Themen) betreffen, und die individuelles Verhalten und Handlungen beeinflussen" 
* #e410 ^property[1].code = #parent 
* #e410 ^property[1].valueCode = #e4 
* #e415 "Individuelle Einstellungen der Mitglieder des erweiterten Familienkreises"
* #e415 ^property[0].code = #None 
* #e415 ^property[0].valueString = "Allgemeine oder spezifische Meinungen und Überzeugungen der Mitglieder des erweiterten Familienkreises, die eine bestimmte Person oder andere Dinge (z.B. soziale, politische und ökonomische Themen) betreffen, und die individuelles Verhalten und Handlungen beeinflussen" 
* #e415 ^property[1].code = #parent 
* #e415 ^property[1].valueCode = #e4 
* #e420 "Individuelle Einstellungen von Freunden"
* #e420 ^property[0].code = #None 
* #e420 ^property[0].valueString = "Allgemeine oder spezifische Meinungen und Überzeugungen von Freunden, die eine bestimmte Person oder andere Dinge (z.B. soziale, politische und ökonomische Themen) betreffen, und die individuelles Verhalten und Handlungen beeinflussen" 
* #e420 ^property[1].code = #parent 
* #e420 ^property[1].valueCode = #e4 
* #e425 "Individuelle Einstellungen von Bekannten, Seinesgleichen (Peers), Kollegen, Nachbarn und anderen Gemeindemitgliedern"
* #e425 ^property[0].code = #None 
* #e425 ^property[0].valueString = "Allgemeine oder spezifische Meinungen und Überzeugungen von Bekannten, Seinesgleichen (Peers), Kollegen, Nachbarn und anderen Gemeindemitgliedern, die eine bestimmte Person oder andere Dinge (z.B. soziale, politische und ökonomische Themen) betreffen, und die individuelles Verhalten und Handlungen beeinflussen" 
* #e425 ^property[1].code = #parent 
* #e425 ^property[1].valueCode = #e4 
* #e430 "Individuelle Einstellungen von Autoritätspersonen"
* #e430 ^property[0].code = #None 
* #e430 ^property[0].valueString = "Allgemeine oder spezifische Meinungen und Überzeugungen von Autoritätspersonen, die eine bestimmte Person oder andere Dinge (z.B. soziale, politische und ökonomische Themen) betreffen, und die individuelles Verhalten und Handlungen beeinflussen" 
* #e430 ^property[1].code = #parent 
* #e430 ^property[1].valueCode = #e4 
* #e435 "Individuelle Einstellungen von Untergebenen"
* #e435 ^property[0].code = #None 
* #e435 ^property[0].valueString = "Allgemeine oder spezifische Meinungen und Überzeugungen von Untergebenen, die eine bestimmte Person oder andere Dinge (z.B. soziale, politische und ökonomische Themen) betreffen, und die individuelles Verhalten und Handlungen beeinflussen" 
* #e435 ^property[1].code = #parent 
* #e435 ^property[1].valueCode = #e4 
* #e440 "Individuelle Einstellungen von persönlichen Hilfs- und Pflegepersonen"
* #e440 ^property[0].code = #None 
* #e440 ^property[0].valueString = "Allgemeine oder spezifische Meinungen und Überzeugungen von persönlichen Hilfs- und Pflegepersonen, die eine bestimmte Person oder andere Dinge (z.B. soziale, politische und ökonomische Themen) betreffen, und die individuelles Verhalten und Handlungen beeinflussen" 
* #e440 ^property[1].code = #parent 
* #e440 ^property[1].valueCode = #e4 
* #e445 "Individuelle Einstellungen von Fremden"
* #e445 ^property[0].code = #None 
* #e445 ^property[0].valueString = "Allgemeine oder spezifische Meinungen und Überzeugungen von Fremden, die eine bestimmte Person oder andere Dinge (z.B. soziale, politische und ökonomische Themen) betreffen, und die individuelles Verhalten und Handlungen beeinflussen" 
* #e445 ^property[1].code = #parent 
* #e445 ^property[1].valueCode = #e4 
* #e450 "Individuelle Einstellungen von Fachleuten der Gesundheitsberufe"
* #e450 ^property[0].code = #None 
* #e450 ^property[0].valueString = "Allgemeine oder spezifische Meinungen und Überzeugungen von Fachleuten der Gesundheitsberufe, die eine bestimmte Person oder andere Dinge (z.B. soziale, politische und ökonomische Themen) betreffen, und die individuelles Verhalten und Handlungen beeinflussen" 
* #e450 ^property[1].code = #parent 
* #e450 ^property[1].valueCode = #e4 
* #e455 "Individuelle Einstellungen von anderen Fachleuten"
* #e455 ^property[0].code = #None 
* #e455 ^property[0].valueString = "Allgemeine oder spezifische Meinungen und Überzeugungen von anderen Fachleuten, die eine bestimmte Person oder andere Dinge (z.B. soziale, politische und ökonomische Themen) betreffen, und die individuelles Verhalten und Handlungen beeinflussen" 
* #e455 ^property[1].code = #parent 
* #e455 ^property[1].valueCode = #e4 
* #e460 "Gesellschaftliche Einstellungen"
* #e460 ^property[0].code = #None 
* #e460 ^property[0].valueString = "Allgemeine oder spezifische Meinungen und Überzeugungen, die im allgemeinen von Mitgliedern einer Kultur, Gesellschaft oder subkulturellen oder anderen gesellschaftlichen Gruppen zu anderen Menschen oder zu sozialen, politischen und ökonomischen Themen vertreten werden, und die Verhaltensweisen oder Handlungen einer Einzelperson oder Personengruppe beeinflussen" 
* #e460 ^property[1].code = #parent 
* #e460 ^property[1].valueCode = #e4 
* #e465 "Gesellschaftliche Normen, Konventionen und Weltanschauungen"
* #e465 ^property[0].code = #None 
* #e465 ^property[0].valueString = "Sitten, Praktiken/Bräuche, Regeln sowie abstrakte Wertsysteme und normative Überzeugungen (z. B. Ideologien, normative Weltanschauungen und moralphilosophische Ansichten), welche innerhalb gesellschaftlicher Kontexte entstehen, und die gesellschaftliche und individuelle Gewohnheiten und Verhaltensweisen beeinflussen oder schaffen, wie gesellschaftliche Normen der Moral, der religiösen Verhaltensweisen oder Etikette; religiöse Lehren und daraus abgeleitete Normen und Konventionen; Normen, die Rituale oder das Zusammensein sozialer Gruppen bestimmen" 
* #e465 ^property[1].code = #parent 
* #e465 ^property[1].valueCode = #e4 
* #e498 "Einstellungen, anders bezeichnet"
* #e498 ^property[0].code = #parent 
* #e498 ^property[0].valueCode = #e4 
* #e499 "Einstellungen, nicht näher bezeichnet"
* #e499 ^property[0].code = #parent 
* #e499 ^property[0].valueCode = #e4 
* #e5 "Dienste, Systeme und Handlungsgrundsätze"
* #e5 ^property[0].code = #parent 
* #e5 ^property[0].valueCode = #e 
* #e5 ^property[1].code = #child 
* #e5 ^property[1].valueCode = #e510 
* #e5 ^property[2].code = #child 
* #e5 ^property[2].valueCode = #e515 
* #e5 ^property[3].code = #child 
* #e5 ^property[3].valueCode = #e520 
* #e5 ^property[4].code = #child 
* #e5 ^property[4].valueCode = #e525 
* #e5 ^property[5].code = #child 
* #e5 ^property[5].valueCode = #e530 
* #e5 ^property[6].code = #child 
* #e5 ^property[6].valueCode = #e535 
* #e5 ^property[7].code = #child 
* #e5 ^property[7].valueCode = #e540 
* #e5 ^property[8].code = #child 
* #e5 ^property[8].valueCode = #e545 
* #e5 ^property[9].code = #child 
* #e5 ^property[9].valueCode = #e550 
* #e5 ^property[10].code = #child 
* #e5 ^property[10].valueCode = #e555 
* #e5 ^property[11].code = #child 
* #e5 ^property[11].valueCode = #e560 
* #e5 ^property[12].code = #child 
* #e5 ^property[12].valueCode = #e565 
* #e5 ^property[13].code = #child 
* #e5 ^property[13].valueCode = #e570 
* #e5 ^property[14].code = #child 
* #e5 ^property[14].valueCode = #e575 
* #e5 ^property[15].code = #child 
* #e5 ^property[15].valueCode = #e580 
* #e5 ^property[16].code = #child 
* #e5 ^property[16].valueCode = #e585 
* #e5 ^property[17].code = #child 
* #e5 ^property[17].valueCode = #e590 
* #e5 ^property[18].code = #child 
* #e5 ^property[18].valueCode = #e595 
* #e5 ^property[19].code = #child 
* #e5 ^property[19].valueCode = #e598 
* #e5 ^property[20].code = #child 
* #e5 ^property[20].valueCode = #e599 
* #e510 "Dienste, Systeme und Handlungsgrundsätze für die Konsumgüterproduktion"
* #e510 ^property[0].code = #None 
* #e510 ^property[0].valueString = "Dienste, Systeme und Handlungsgrundsätze, welche die Grundlage bilden und Vorsorge treffen für die Produktion von Objekten und Erzeugnissen, die von Menschen verbraucht oder benutzt werden" 
* #e510 ^property[1].code = #parent 
* #e510 ^property[1].valueCode = #e5 
* #e510 ^property[2].code = #child 
* #e510 ^property[2].valueCode = #e5100 
* #e510 ^property[3].code = #child 
* #e510 ^property[3].valueCode = #e5101 
* #e510 ^property[4].code = #child 
* #e510 ^property[4].valueCode = #e5102 
* #e510 ^property[5].code = #child 
* #e510 ^property[5].valueCode = #e5108 
* #e510 ^property[6].code = #child 
* #e510 ^property[6].valueCode = #e5109 
* #e5100 "Dienste für die Konsumgüterproduktion"
* #e5100 ^property[0].code = #None 
* #e5100 ^property[0].valueString = "Dienste und Programme für die Sammlung, Schaffung, Produktion und Herstellung von Konsumgütern und Produkten wie Produkte und Technologien für Mobilität, Kommunikation, Bildung/Ausbildung, Transport, Beschäftigung und Hausarbeit, einschließlich derer, die diese Dienste erbringen" 
* #e5100 ^property[1].code = #None 
* #e5100 ^property[1].valueString = "Dienste des Bildungs- und Ausbildungswesens" 
* #e5100 ^property[2].code = #parent 
* #e5100 ^property[2].valueCode = #e510 
* #e5101 "Systeme für die Konsumgüterproduktion"
* #e5101 ^property[0].code = #None 
* #e5101 ^property[0].valueString = "Administrative Steuerungs- und Überwachungsmechanismen wie regionale, nationale und internationale Organisationen, die Standards vorgeben (z.B. Internationale Organisation für Standardisierung) und Verbraucherorganisationen, die die Sammlung, Schaffung, Produktion und Herstellung von Konsumgütern und Produkten regeln" 
* #e5101 ^property[1].code = #parent 
* #e5101 ^property[1].valueCode = #e510 
* #e5102 "Handlungsgrundsätze für die Konsumgüterproduktion"
* #e5102 ^property[0].code = #None 
* #e5102 ^property[0].valueString = "Gesetze, Vorschriften und Standards für die Sammlung, Schaffung, Produktion und Herstellung von Konsumgütern und Produkten, wie Entscheidungen darüber, welche Standards zu übernehmen sind" 
* #e5102 ^property[1].code = #parent 
* #e5102 ^property[1].valueCode = #e510 
* #e5108 "Dienste, Systeme und Handlungsgrundsätze für die Konsumgüterproduktion, anders bezeichnet"
* #e5108 ^property[0].code = #parent 
* #e5108 ^property[0].valueCode = #e510 
* #e5109 "Dienste, Systeme und Handlungsgrundsätze für die Konsumgüterproduktion, nicht näher bezeichnet"
* #e5109 ^property[0].code = #parent 
* #e5109 ^property[0].valueCode = #e510 
* #e515 "Dienste, Systeme und Handlungsgrundsätze des Architektur- und Bauwesens"
* #e515 ^property[0].code = #None 
* #e515 ^property[0].valueString = "Dienste, Systeme und Handlungsgrundsätze für Entwurf und Bau von öffentlichen und privaten Bauten" 
* #e515 ^property[1].code = #None 
* #e515 ^property[1].valueString = "Dienste, Systeme und Handlungsgrundsätze der Stadt- und Landschaftsplanung" 
* #e515 ^property[2].code = #parent 
* #e515 ^property[2].valueCode = #e5 
* #e515 ^property[3].code = #child 
* #e515 ^property[3].valueCode = #e5150 
* #e515 ^property[4].code = #child 
* #e515 ^property[4].valueCode = #e5151 
* #e515 ^property[5].code = #child 
* #e515 ^property[5].valueCode = #e5152 
* #e515 ^property[6].code = #child 
* #e515 ^property[6].valueCode = #e5158 
* #e515 ^property[7].code = #child 
* #e515 ^property[7].valueCode = #e5159 
* #e5150 "Dienste des Architektur- und Bauwesens"
* #e5150 ^property[0].code = #None 
* #e5150 ^property[0].valueString = "Dienste und Programme für Entwurf, Bau und Erhaltung von Wohn-, Geschäfts-, Industrie- und öffentlichen Gebäuden, wie Hausbau, Operationalisierung und Umsetzung von Entwurfsgrundsätzen, Baunormen, -vorschriften und -standards, einschließlich derer, die diese Dienste erbringen" 
* #e5150 ^property[1].code = #parent 
* #e5150 ^property[1].valueCode = #e515 
* #e5151 "Systeme des Architektur- und Bauwesens"
* #e5151 ^property[0].code = #None 
* #e5151 ^property[0].valueString = "Administrative Steuerungs- und Überwachungsmechanismen, die Planung, Entwurf, Bau und Erhaltung von Wohn-, Geschäfts-, Industrie- und öffentlichen Gebäuden regeln, wie die Umsetzung und Überwachung von Baunormen, Konstruktionsstandards sowie Brandschutz- und Sicherheitsstandards" 
* #e5151 ^property[1].code = #parent 
* #e5151 ^property[1].valueCode = #e515 
* #e5152 "Handlungsgrundsätze des Architektur- und Bauwesens"
* #e5152 ^property[0].code = #None 
* #e5152 ^property[0].valueString = "Gesetze, Vorschriften und Standards, die Planung, Entwurf, Konstruktion und Erhaltung von Wohn-, Geschäfts-, Industrie- und öffentlichen Gebäuden regeln, wie Vorschriften zu Baunormen, Konstruktionsstandards sowie Sicherheitsnormen zu Brand- und Lebensschutz" 
* #e5152 ^property[1].code = #parent 
* #e5152 ^property[1].valueCode = #e515 
* #e5158 "Dienste, Systeme und Handlungsgrundsätze des Architektur- und Bauwesens, anders bezeichnet"
* #e5158 ^property[0].code = #parent 
* #e5158 ^property[0].valueCode = #e515 
* #e5159 "Dienste, Systeme und Handlungsgrundsätze des Architektur- und Bauwesens, nicht näher bezeichnet"
* #e5159 ^property[0].code = #parent 
* #e5159 ^property[0].valueCode = #e515 
* #e520 "Dienste, Systeme und Handlungsgrundsätze der Stadt- und Landschaftsplanung"
* #e520 ^property[0].code = #None 
* #e520 ^property[0].valueString = "Dienste, Systeme und Handlungsgrundsätze für Planung, Entwurf, Entwicklung und Unterhaltung von öffentlichem Land (z.B. Parks, Forsten, Uferlinien, Feuchtgebiete) und privatem Grund im ländlichen, vorörtlichen und städtischen Zusammenhang" 
* #e520 ^property[1].code = #None 
* #e520 ^property[1].valueString = "Dienste, Systeme und Handlungsgrundsätze des Architektur- und Bauwesens" 
* #e520 ^property[2].code = #parent 
* #e520 ^property[2].valueCode = #e5 
* #e520 ^property[3].code = #child 
* #e520 ^property[3].valueCode = #e5200 
* #e520 ^property[4].code = #child 
* #e520 ^property[4].valueCode = #e5201 
* #e520 ^property[5].code = #child 
* #e520 ^property[5].valueCode = #e5202 
* #e520 ^property[6].code = #child 
* #e520 ^property[6].valueCode = #e5208 
* #e520 ^property[7].code = #child 
* #e520 ^property[7].valueCode = #e5209 
* #e5200 "Dienste der Stadt- und Landschaftsplanung"
* #e5200 ^property[0].code = #None 
* #e5200 ^property[0].valueString = "Dienste und Programme zur Planung, Schaffung und Erhaltung von städtischen, vorstädtischen und ländlichen Räumen, Erholungs- und Naturschutzgebieten, Freiflächen für Versammlungen und Märkte (Plätze, Freilichtmärkte) und Verkehrsnetze für Fußgänger und Fahrzeuge für bestimmte Zwecke, einschließlich derer, die diese Dienste und Leistungen anbieten" 
* #e5200 ^property[1].code = #None 
* #e5200 ^property[1].valueString = "Entwurf, Konstruktion sowie Bauprodukte und Technologien von öffentlichen" 
* #e5200 ^property[2].code = #parent 
* #e5200 ^property[2].valueCode = #e520 
* #e5201 "Systeme der Stadt- und Landschaftsplanung"
* #e5201 ^property[0].code = #None 
* #e5201 ^property[0].valueString = "Administrative Steuerungs- oder Überwachungsmechanismen wie für die Umsetzung von lokalen, regionalen oder nationalen Raumplanungsgesetzen, Entwurfsnormen, Richtlinien für Kultur- und Naturerbe sowie Vorschriften der Umweltplanungspolitik, welche Planung, Entwurf, Entwicklung und Erhaltung von Freiflächen einschließlich städtische, vorstädtische und ländliche Räume, Parks, Landschafts- und Tierschutzgebiete regeln" 
* #e5201 ^property[1].code = #parent 
* #e5201 ^property[1].valueCode = #e520 
* #e5202 "Handlungsgrundsätze der Stadt- und Landschaftsplanung"
* #e5202 ^property[0].code = #None 
* #e5202 ^property[0].valueString = "Gesetze, Vorschriften und Standards, die Planung, Entwurf, Entwicklung und Erhaltung von Freiflächen einschließlich ländlicher, vorstädtischer und städtischer Räume, Parks, Landschafts- und Tierschutzgebiete regeln, wie lokale, regionale oder nationale Raumplanungsgesetze, Entwurfsnormen, Handlungsgrundsätze des Kultur- und Naturerbes und der Umweltplanung" 
* #e5202 ^property[1].code = #parent 
* #e5202 ^property[1].valueCode = #e520 
* #e5208 "Dienste, Systeme und Handlungsgrundsätze der Stadt- und Landschaftsplanung, anders bezeichnet"
* #e5208 ^property[0].code = #parent 
* #e5208 ^property[0].valueCode = #e520 
* #e5209 "Dienste, Systeme und Handlungsgrundsätze der Stadt- und Landschaftsplanung, nicht näher bezeichnet"
* #e5209 ^property[0].code = #parent 
* #e5209 ^property[0].valueCode = #e520 
* #e525 "Dienste, Systeme und Handlungsgrundsätze des Wohnungswesens"
* #e525 ^property[0].code = #None 
* #e525 ^property[0].valueString = "Dienste, Systeme und Handlungsgrundsätze für Bereitstellung von Unterkünften, Wohnungen oder möblierten Zimmern für Menschen" 
* #e525 ^property[1].code = #parent 
* #e525 ^property[1].valueCode = #e5 
* #e525 ^property[2].code = #child 
* #e525 ^property[2].valueCode = #e5250 
* #e525 ^property[3].code = #child 
* #e525 ^property[3].valueCode = #e5251 
* #e525 ^property[4].code = #child 
* #e525 ^property[4].valueCode = #e5252 
* #e525 ^property[5].code = #child 
* #e525 ^property[5].valueCode = #e5258 
* #e525 ^property[6].code = #child 
* #e525 ^property[6].valueCode = #e5259 
* #e5250 "Dienste des Wohnungswesens"
* #e5250 ^property[0].code = #None 
* #e5250 ^property[0].valueString = "Dienste und Programme für Lokalisation, Bereitstellung und Erhaltung von Häusern oder Unterkünften für Wohnzwecke wie Immobilienmakler, Wohnungsorganisationen, Unterkunftsvermittlung für Obdachlose, einschließlich jener, die diese Dienstleistungen bereitstellen" 
* #e5250 ^property[1].code = #parent 
* #e5250 ^property[1].valueCode = #e525 
* #e5251 "Systeme des Wohnungswesens"
* #e5251 ^property[0].code = #None 
* #e5251 ^property[0].valueString = "Administrative Steuerungs- oder Überwachungsmechanismen, die Wohnungen oder Unterkünfte für Menschen regeln, wie Einrichtungen für die Umsetzung und Überwachung der Wohnungsregelungen" 
* #e5251 ^property[1].code = #parent 
* #e5251 ^property[1].valueCode = #e525 
* #e5252 "Handlungsgrundsätze des Wohnungswesens"
* #e5252 ^property[0].code = #None 
* #e5252 ^property[0].valueString = "Gesetze, Vorschriften und Normen, die das Wohnungswesen regeln, wie Gesetze und Handlungsgrundsätze, die die Berechtigung zur Inanspruchnahme von Wohnungen oder Unterkünften festlegen, Handlungsgrundsätze, die die Beteiligung des Staates an Bau und Erhaltung von Wohnungen betreffen, und Handlungsgrundsätze zu Art und Ort des Wohnungsbaus" 
* #e5252 ^property[1].code = #parent 
* #e5252 ^property[1].valueCode = #e525 
* #e5258 "Dienste, Systeme und Handlungsgrundsätze des Wohnungswesens, anders bezeichnet"
* #e5258 ^property[0].code = #parent 
* #e5258 ^property[0].valueCode = #e525 
* #e5259 "Dienste, Systeme und Handlungsgrundsätze des Wohnungswesens, nicht näher bezeichnet"
* #e5259 ^property[0].code = #parent 
* #e5259 ^property[0].valueCode = #e525 
* #e530 "Dienste, Systeme und Handlungsgrundsätze des Versorgungswesens"
* #e530 ^property[0].code = #None 
* #e530 ^property[0].valueString = "Dienste, öffentliche Einrichtungen und rechtliche Vorschriften für öffentlich bereit gestellte Versorgungsleistungen wie Wasser, Brennstoff, Elektrizität, Entsorgung, öffentlicher Transport und andere notwendige Dienste" 
* #e530 ^property[1].code = #None 
* #e530 ^property[1].valueString = "Dienste, Systeme und Handlungsgrundsätze für den zivilen Schutz und Sicherheit" 
* #e530 ^property[2].code = #parent 
* #e530 ^property[2].valueCode = #e5 
* #e530 ^property[3].code = #child 
* #e530 ^property[3].valueCode = #e5300 
* #e530 ^property[4].code = #child 
* #e530 ^property[4].valueCode = #e5301 
* #e530 ^property[5].code = #child 
* #e530 ^property[5].valueCode = #e5302 
* #e530 ^property[6].code = #child 
* #e530 ^property[6].valueCode = #e5308 
* #e530 ^property[7].code = #child 
* #e530 ^property[7].valueCode = #e5309 
* #e5300 "Dienste des Versorgungswesens"
* #e5300 ^property[0].code = #None 
* #e5300 ^property[0].valueString = "Dienste und Programme, die der Bevölkerung insgesamt die notwendige Energie (z.B. Brennstoff und Elektrizität), Entsorgung, Wasser sowie andere notwendige Dienste (z.B. Bereitschaftsdienste) für private und kommerzielle Abnehmer zur Verfügung stellen, einschließlich derer, die diese Dienste erbringen" 
* #e5300 ^property[1].code = #parent 
* #e5300 ^property[1].valueCode = #e530 
* #e5301 "Systeme des Versorgungswesens"
* #e5301 ^property[0].code = #None 
* #e5301 ^property[0].valueString = "Administrative Steuerungs- und Überwachungsmechanismen, die die Bereitstellung von Versorgungsleistungen regeln, wie Gesundheits- und Sicherheitskommissionen sowie Verbraucherverbände" 
* #e5301 ^property[1].code = #parent 
* #e5301 ^property[1].valueCode = #e530 
* #e5302 "Handlungsgrundsätze des Versorgungswesens"
* #e5302 ^property[0].code = #None 
* #e5302 ^property[0].valueString = "Gesetze, Vorschriften und Standards, die die Bereitstellung von Versorgungsdiensten regeln, wie Gesundheits- und Sicherheitsstandards für Lieferung und Versorgung mit Wasser und Brennstoff, Entsorgungsverfahren in den Kommunen und Handlungsgrundsätze für andere notwendige Dienste sowie die Versorgung in Notzeiten und bei Naturkatastrophen" 
* #e5302 ^property[1].code = #parent 
* #e5302 ^property[1].valueCode = #e530 
* #e5308 "Dienste, Systeme und Handlungsgrundsätze des Versorgungswesens, anders bezeichnet"
* #e5308 ^property[0].code = #parent 
* #e5308 ^property[0].valueCode = #e530 
* #e5309 "Dienste, Systeme und Handlungsgrundsätze des Versorgungswesens, nicht näher bezeichnet"
* #e5309 ^property[0].code = #parent 
* #e5309 ^property[0].valueCode = #e530 
* #e535 "Dienste, Systeme und Handlungsgrundsätze des Kommunikationswesens"
* #e535 ^property[0].code = #None 
* #e535 ^property[0].valueString = "Dienste, öffentliche Einrichtungen und rechtliche Vorschriften für Übermittlung und Austausch von Informationen" 
* #e535 ^property[1].code = #None 
* #e535 ^property[1].valueString = "Dienste, Systeme und Handlungsgrundsätze des Medienwesens" 
* #e535 ^property[2].code = #parent 
* #e535 ^property[2].valueCode = #e5 
* #e535 ^property[3].code = #child 
* #e535 ^property[3].valueCode = #e5350 
* #e535 ^property[4].code = #child 
* #e535 ^property[4].valueCode = #e5351 
* #e535 ^property[5].code = #child 
* #e535 ^property[5].valueCode = #e5352 
* #e535 ^property[6].code = #child 
* #e535 ^property[6].valueCode = #e5358 
* #e535 ^property[7].code = #child 
* #e535 ^property[7].valueCode = #e5359 
* #e5350 "Dienste des Kommunikationswesens"
* #e5350 ^property[0].code = #None 
* #e5350 ^property[0].valueString = "Dienste und Programme für die Übermittlung von Informationen mittels verschiedener Methoden wie Telefon, Telefax, Post und Luftpost, elektronische Post und anderer computergestützter Systeme (z.B. Fernmeldevermittlung, Fernschreiben, Teletext und Internetdienste, einschließlich jener, die diese Dienste und Leistungen bereitstellen" 
* #e5350 ^property[1].code = #None 
* #e5350 ^property[1].valueString = "Dienste des Medienwesens" 
* #e5350 ^property[2].code = #parent 
* #e5350 ^property[2].valueCode = #e535 
* #e5351 "Systeme des Kommunikationswesens"
* #e5351 ^property[0].code = #None 
* #e5351 ^property[0].valueString = "Administrative Steuerungs- oder Überwachungsmechanismen wie Behörden zur Regulierung der Telekommunikation und ähnliche Organisationen, die die Übermittlung von Informationen mittels verschiedener Methoden einschließlich Telefon, Telefax, Post und Luftpost, elektronische Post und anderer computergestützter Systeme regeln" 
* #e5351 ^property[1].code = #parent 
* #e5351 ^property[1].valueCode = #e535 
* #e5352 "Handlungsgrundsätze des Kommunikationswesens"
* #e5352 ^property[0].code = #None 
* #e5352 ^property[0].valueString = "Gesetze, Vorschriften und Standards, die die Übermittlung von Informationen mittels verschiedener Methoden einschließlich Telefon, Telefax, Post und Luftpost, elektronische Post und anderer computergestützter Systeme regeln, wie die Zugangsberechtigung zu Kommunikationsdiensten, Bedarf an Postadressen und Standards für die Bereitstellung von Telekommunikation" 
* #e5352 ^property[1].code = #parent 
* #e5352 ^property[1].valueCode = #e535 
* #e5358 "Dienste, Systeme und Handlungsgrundsätze des Kommunikationswesens, anders bezeichnet"
* #e5358 ^property[0].code = #parent 
* #e5358 ^property[0].valueCode = #e535 
* #e5359 "Dienste, Systeme und Handlungsgrundsätze des Kommunikationswesens, nicht näher bezeichnet"
* #e5359 ^property[0].code = #parent 
* #e5359 ^property[0].valueCode = #e535 
* #e540 "Dienste, Systeme und Handlungsgrundsätze des Transportwesens"
* #e540 ^property[0].code = #None 
* #e540 ^property[0].valueString = "Dienste, Systeme und Handlungsgrundsätze für die Beförderung von Menschen und Gütern von einem Ort zu einem anderen" 
* #e540 ^property[1].code = #parent 
* #e540 ^property[1].valueCode = #e5 
* #e540 ^property[2].code = #child 
* #e540 ^property[2].valueCode = #e5400 
* #e540 ^property[3].code = #child 
* #e540 ^property[3].valueCode = #e5401 
* #e540 ^property[4].code = #child 
* #e540 ^property[4].valueCode = #e5402 
* #e540 ^property[5].code = #child 
* #e540 ^property[5].valueCode = #e5408 
* #e540 ^property[6].code = #child 
* #e540 ^property[6].valueCode = #e5409 
* #e5400 "Dienste des Transportwesens"
* #e5400 ^property[0].code = #None 
* #e5400 ^property[0].valueString = "Dienste und Programme zur Beförderung von Personen oder Gütern auf Straßen, Wegen, Schienen, in der Luft oder auf dem Wasser mittels privater oder öffentlicher Transportmittel, einschließlich derer, die diese Dienste erbringen" 
* #e5400 ^property[1].code = #None 
* #e5400 ^property[1].valueString = "Produkte und Technologien zur persönlichen Mobilität drinnen und draußen und Transport" 
* #e5400 ^property[2].code = #parent 
* #e5400 ^property[2].valueCode = #e540 
* #e5401 "Systeme des Transportwesens"
* #e5401 ^property[0].code = #None 
* #e5401 ^property[0].valueString = "Administrative Steuerungs- oder Überwachungsmechanismen, die die Beförderung von Personen oder Gütern auf Straßen, Wegen, Schienen, in der Luft oder auf dem Wasser regeln, wie Einrichtungen, die Berechtigung für das Führen eines Fahrzeuges festlegen, und die Umsetzung und Überwachung von Gesundheits- und Sicherheitsstandards für die Benutzung der verschieden Arten von Transportmitteln überwachen" 
* #e5401 ^property[1].code = #None 
* #e5401 ^property[1].valueString = "Dienste, Systeme und Handlungsgrundsätze der sozialen Sicherheit" 
* #e5401 ^property[2].code = #parent 
* #e5401 ^property[2].valueCode = #e540 
* #e5402 "Handlungsgrundsätze des Transportwesens"
* #e5402 ^property[0].code = #None 
* #e5402 ^property[0].valueString = "Gesetze, Vorschriften und Standards, die die Beförderung von Personen oder Gütern auf Straßen, Wegen, Schienen, in der Luft oder auf dem Wasser regeln, wie Beförderungsplanungsgesetze und -Handlungsgrundsätze, Handlungsgrundsätze für die Bereitstellung und den Zugang zu öffentlichen Verkehrsmitteln" 
* #e5402 ^property[1].code = #parent 
* #e5402 ^property[1].valueCode = #e540 
* #e5408 "Dienste, Systeme und Handlungsgrundsätze des Transportwesens, anders bezeichnet"
* #e5408 ^property[0].code = #parent 
* #e5408 ^property[0].valueCode = #e540 
* #e5409 "Dienste, Systeme und Handlungsgrundsätze des Transportwesens, nicht näher bezeichnet"
* #e5409 ^property[0].code = #parent 
* #e5409 ^property[0].valueCode = #e540 
* #e545 "Dienste, Systeme und Handlungsgrundsätze für zivilen Schutz und Sicherheit"
* #e545 ^property[0].code = #None 
* #e545 ^property[0].valueString = "Dienste, öffentliche Einrichtungen und rechtliche Vorschriften zum Schutz von Person und Besitz" 
* #e545 ^property[1].code = #None 
* #e545 ^property[1].valueString = "Dienste, Systeme und Handlungsgrundsätze des Versorgungswesens" 
* #e545 ^property[2].code = #parent 
* #e545 ^property[2].valueCode = #e5 
* #e545 ^property[3].code = #child 
* #e545 ^property[3].valueCode = #e5450 
* #e545 ^property[4].code = #child 
* #e545 ^property[4].valueCode = #e5451 
* #e545 ^property[5].code = #child 
* #e545 ^property[5].valueCode = #e5452 
* #e545 ^property[6].code = #child 
* #e545 ^property[6].valueCode = #e5458 
* #e545 ^property[7].code = #child 
* #e545 ^property[7].valueCode = #e5459 
* #e5450 "Dienste für zivilen Schutz und Sicherheit"
* #e5450 ^property[0].code = #None 
* #e5450 ^property[0].valueString = "Von der Gemeinde organisierte Dienste und Programme zum Schutz von Personen und Besitz wie Feuerwehr, Polizei, Not- und Rettungsdienste, einschließlich derer, die diese Dienste erbringen" 
* #e5450 ^property[1].code = #parent 
* #e5450 ^property[1].valueCode = #e545 
* #e5451 "Systeme für zivilen Schutz und Sicherheit"
* #e5451 ^property[0].code = #None 
* #e5451 ^property[0].valueString = "Administrative Steuerungs- oder Überwachungsmechanismen, die den Schutz von Personen und Besitz regeln, wie Einrichtungen, durch die die Bereitstellung von Feuerwehr-, Polizei-, Not- und Rettungsdiensten organisiert wird" 
* #e5451 ^property[1].code = #parent 
* #e5451 ^property[1].valueCode = #e545 
* #e5452 "Handlungsgrundsätze für zivilen Schutz und Sicherheit"
* #e5452 ^property[0].code = #None 
* #e5452 ^property[0].valueString = "Gesetze, Vorschriften und Standards, die den Schutz von Personen und Besitz regeln, wie Handlungsgrundsätze für die Bereitstellung von Feuerwehr-, Polizei-, Not- und Rettungsdiensten" 
* #e5452 ^property[1].code = #parent 
* #e5452 ^property[1].valueCode = #e545 
* #e5458 "Dienste, Systeme und Handlungsgrundsätze für zivilen Schutz und Sicherheit, anders bezeichnet"
* #e5458 ^property[0].code = #parent 
* #e5458 ^property[0].valueCode = #e545 
* #e5459 "Dienste, Systeme und Handlungsgrundsätze für zivilen Schutz und Sicherheit, nicht näher bezeichnet"
* #e5459 ^property[0].code = #parent 
* #e5459 ^property[0].valueCode = #e545 
* #e550 "Dienste, Systeme und Handlungsgrundsätze der Rechtspflege"
* #e550 ^property[0].code = #None 
* #e550 ^property[0].valueString = "Dienste, öffentliche Einrichtungen und Handlungsgrundsätze, die die Gesetzgebung und andere Rechtsprechung eines Landes betreffen" 
* #e550 ^property[1].code = #parent 
* #e550 ^property[1].valueCode = #e5 
* #e550 ^property[2].code = #child 
* #e550 ^property[2].valueCode = #e5500 
* #e550 ^property[3].code = #child 
* #e550 ^property[3].valueCode = #e5501 
* #e550 ^property[4].code = #child 
* #e550 ^property[4].valueCode = #e5502 
* #e550 ^property[5].code = #child 
* #e550 ^property[5].valueCode = #e5508 
* #e550 ^property[6].code = #child 
* #e550 ^property[6].valueCode = #e5509 
* #e5500 "Dienste der Rechtspflege"
* #e5500 ^property[0].code = #None 
* #e5500 ^property[0].valueString = "Dienste und Programme zur Erfüllung der gesetzlich festgelegten hoheitlichen Aufgaben wie Gerichte, Tribunale und andere Einrichtungen für die Anhörung und Beilegung von zivilen Rechtsstreitigkeiten und Strafverfahren, Anwälte, juristische Dienste wie Notariate, Schieds- und Schlichtungsstellen, Erziehungsheime und Strafvollzugsanstalten, einschließlich derer, die diese Dienste erbringen" 
* #e5500 ^property[1].code = #parent 
* #e5500 ^property[1].valueCode = #e550 
* #e5501 "Systeme der Rechtspflege"
* #e5501 ^property[0].code = #None 
* #e5501 ^property[0].valueString = "Administrative Steuerungs- oder Überwachungsmechanismen, die die Rechtsprechung regeln, wie Einrichtungen für die Umsetzung und Überwachung formaler Regeln (z.B. Rechte, Vorschriften, Gewohnheitsrecht, religiöses Recht, internationales Recht und Konventionen" 
* #e5501 ^property[1].code = #parent 
* #e5501 ^property[1].valueCode = #e550 
* #e5502 "Handlungsgrundsätze der Rechtspflege"
* #e5502 ^property[0].code = #None 
* #e5502 ^property[0].valueString = "Gesetze, Vorschriften und Standards, wie Rechte, Vorschriften, Gewohnheitsrecht, religiöses Recht, internationales Recht und Konventionen, die die Rechtsprechung regeln" 
* #e5502 ^property[1].code = #parent 
* #e5502 ^property[1].valueCode = #e550 
* #e5508 "Dienste, Systeme und Handlungsgrundsätze der Rechtspflege, anders bezeichnet"
* #e5508 ^property[0].code = #parent 
* #e5508 ^property[0].valueCode = #e550 
* #e5509 "Dienste, Systeme und Handlungsgrundsätze der Rechtspflege, nicht näher bezeichnet"
* #e5509 ^property[0].code = #parent 
* #e5509 ^property[0].valueCode = #e550 
* #e555 "Dienste, Systeme und Handlungsgrundsätze von Vereinigungen und Organisationen"
* #e555 ^property[0].code = #None 
* #e555 ^property[0].valueString = "Dienste und Programme von Personen, die sich zwecks Verfolgung allgemeiner, nicht-kommerzieller Interessen mit anderen Personen mit gleichen Interessen zusammengeschlossen haben, wobei die Erbringung solcher Dienste an eine Mitgliedschaft gebunden sein kann" 
* #e555 ^property[1].code = #parent 
* #e555 ^property[1].valueCode = #e5 
* #e555 ^property[2].code = #child 
* #e555 ^property[2].valueCode = #e5550 
* #e555 ^property[3].code = #child 
* #e555 ^property[3].valueCode = #e5551 
* #e555 ^property[4].code = #child 
* #e555 ^property[4].valueCode = #e5552 
* #e555 ^property[5].code = #child 
* #e555 ^property[5].valueCode = #e5558 
* #e555 ^property[6].code = #child 
* #e555 ^property[6].valueCode = #e5559 
* #e5550 "Dienste von Vereinigungen und Organisationen"
* #e5550 ^property[0].code = #None 
* #e5550 ^property[0].valueString = "Dienste und Programme von Personen, die sich zwecks Verfolgung allgemeiner, nicht-kommerzieller Interessen mit anderen Personen mit gleichen Interessen zusammengeschlossen haben, wobei die Erbringung solcher Dienste an eine Mitgliedschaft gebunden sein kann, wie Vereinigungen und Organisationen, die Dienste für Freizeit und Hobby, Sport, Kultur, Religion sowie gegenseitiger Hilfe erbringen" 
* #e5550 ^property[1].code = #parent 
* #e5550 ^property[1].valueCode = #e555 
* #e5551 "Systeme der Vereinigungen und Organisationen"
* #e5551 ^property[0].code = #None 
* #e5551 ^property[0].valueString = "Administrative Steuerungs- und Überwachungsmechanismen, die die Beziehungen und Aktivitäten von Menschen, die aus allgemeinen, nicht-kommerziellen Interessen zusammenkommen, sowie die Gründung und Führung von Vereinigungen und Organisationen regeln, wie Organisationen für gegenseitige Hilfe, Organisationen für Freizeit und Hobby, kulturelle und religiöse Vereinigungen und Non-Profit-Organisationen" 
* #e5551 ^property[1].code = #parent 
* #e5551 ^property[1].valueCode = #e555 
* #e5552 "Handlungsgrundsätze der Vereinigungen und Organisationen"
* #e5552 ^property[0].code = #None 
* #e5552 ^property[0].valueString = "Gesetze, Vorschriften und Standards, die die Beziehungen und Aktivitäten von Menschen regeln, die aus allgemeinen, nicht-kommerziellen Interessen zusammenkommen, wie Handlungsgrundsätze für die Gründung und Führung von Vereinigungen und Organisationen wie Organisationen für gegenseitige Hilfe, Organisationen für Freizeit und Hobby, kulturelle und religiöse Vereinigungen und gemeinnützige Organisationen" 
* #e5552 ^property[1].code = #parent 
* #e5552 ^property[1].valueCode = #e555 
* #e5558 "Dienste, Systeme und Handlungsgrundsätze der Vereinigungen und Organisationen, anders bezeichnet"
* #e5558 ^property[0].code = #parent 
* #e5558 ^property[0].valueCode = #e555 
* #e5559 "Dienste, Systeme und Handlungsgrundsätze der Vereinigungen und Organisationen, nicht näher bezeichnet"
* #e5559 ^property[0].code = #parent 
* #e5559 ^property[0].valueCode = #e555 
* #e560 "Dienste, Systeme und Handlungsgrundsätze des Medienwesens"
* #e560 ^property[0].code = #None 
* #e560 ^property[0].valueString = "Dienste, Systeme und Handlungsgrundsätze für die Massenkommunikation über Radio, Fernsehen, Zeitungen und Internet" 
* #e560 ^property[1].code = #parent 
* #e560 ^property[1].valueCode = #e5 
* #e560 ^property[2].code = #child 
* #e560 ^property[2].valueCode = #e5600 
* #e560 ^property[3].code = #child 
* #e560 ^property[3].valueCode = #e5601 
* #e560 ^property[4].code = #child 
* #e560 ^property[4].valueCode = #e5602 
* #e560 ^property[5].code = #child 
* #e560 ^property[5].valueCode = #e5608 
* #e560 ^property[6].code = #child 
* #e560 ^property[6].valueCode = #e5609 
* #e5600 "Dienste des Medienwesens"
* #e5600 ^property[0].code = #None 
* #e5600 ^property[0].valueString = "Dienste und Programme für die Massenkommunikation wie Radio- und Fernsehdienste, Dienste für Filmuntertitelung, Dienste der Presseagenturen, Zeitungsdienste, Brailleschrift-Dienste und computergestützte Massenkommunikationsdienste (WWW, Internet), einschließlich derer, die diese Dienste erbringen" 
* #e5600 ^property[1].code = #None 
* #e5600 ^property[1].valueString = "Dienste des Kommunikationswesens" 
* #e5600 ^property[2].code = #parent 
* #e5600 ^property[2].valueCode = #e560 
* #e5601 "Systeme des Medienwesens"
* #e5601 ^property[0].code = #None 
* #e5601 ^property[0].valueString = "Administrative Steuerungs- und Überwachungsmechanismen, die die Versorgung der allgemeinen Öffentlichkeit mit Nachrichten und Informationen regeln, wie z.B. Standards, die Inhalt, Verteilung, Verbreitung, Zugänglichkeit und Methoden der Kommunikation über Radio, Fernsehen, Dienste der Presseagenturen, Zeitungen und computergestützte Massenkommunikationsdienste (WWW, Internet) regeln" 
* #e5601 ^property[1].code = #None 
* #e5601 ^property[1].valueString = "Bedarf an Untertitelung beim Fernsehen, Braille-Versionen von Zeitungen und anderen Veröffentlichungen sowie Teletext-Radiosendungen" 
* #e5601 ^property[2].code = #None 
* #e5601 ^property[2].valueString = "Systeme des Kommunikationswesens" 
* #e5601 ^property[3].code = #parent 
* #e5601 ^property[3].valueCode = #e560 
* #e5602 "Handlungsgrundsätze des Medienwesens"
* #e5602 ^property[0].code = #None 
* #e5602 ^property[0].valueString = "Gesetze, Vorschriften und Standards, die die Versorgung der allgemeinen Öffentlichkeit mit Nachrichten und Informationen regeln, wie Handlungsgrundsätze für Inhalt, Verteilung, Verbreitung, Zugänglichkeit und Methoden der Kommunikation über Radio, Fernsehen, Dienste der Presseagenturen, Zeitungen und computergestützte Massenkommunikationsdienste (WWW, Internet)" 
* #e5602 ^property[1].code = #None 
* #e5602 ^property[1].valueString = "Handlungsgrundsätze des Kommunikationswesens" 
* #e5602 ^property[2].code = #parent 
* #e5602 ^property[2].valueCode = #e560 
* #e5608 "Dienste, Systeme und Handlungsgrundsätze des Medienwesens, anders bezeichnet"
* #e5608 ^property[0].code = #parent 
* #e5608 ^property[0].valueCode = #e560 
* #e5609 "Dienste, Systeme und Handlungsgrundsätze des Medienwesens, nicht näher bezeichnet"
* #e5609 ^property[0].code = #parent 
* #e5609 ^property[0].valueCode = #e560 
* #e565 "Dienste, Systeme und Handlungsgrundsätze der Wirtschaft"
* #e565 ^property[0].code = #None 
* #e565 ^property[0].valueString = "Dienste und Programme zu Produktion, Verteilung, Verbrauch und Verwendung von Gütern und Dienstleistungen" 
* #e565 ^property[1].code = #None 
* #e565 ^property[1].valueString = "Dienste, Systeme und Handlungsgrundsätze der sozialen Sicherheit" 
* #e565 ^property[2].code = #parent 
* #e565 ^property[2].valueCode = #e5 
* #e565 ^property[3].code = #child 
* #e565 ^property[3].valueCode = #e5650 
* #e565 ^property[4].code = #child 
* #e565 ^property[4].valueCode = #e5651 
* #e565 ^property[5].code = #child 
* #e565 ^property[5].valueCode = #e5652 
* #e565 ^property[6].code = #child 
* #e565 ^property[6].valueCode = #e5658 
* #e565 ^property[7].code = #child 
* #e565 ^property[7].valueCode = #e5659 
* #e5650 "Dienste der Wirtschaft"
* #e5650 ^property[0].code = #None 
* #e5650 ^property[0].valueString = "Dienste und Programme zu Gesamtproduktion, Verteilung, Verbrauch und Verwendung von Gütern und Dienstleistungen wie der private kommerzielle Sektor (z. B. Betriebe, juristische Personen, private gewinnorientierte Unternehmungen), der staatliche Sektor (z.B. öffentliche, kommerzielle Dienste wie Genossenschaften und Verwaltungen), Geld- und Kreditinstitute (z.B. Banken und Versicherungen), einschließlich derer, die diese Dienste erbringen" 
* #e5650 ^property[1].code = #None 
* #e5650 ^property[1].valueString = "Dienste des Versorgungswesens" 
* #e5650 ^property[2].code = #parent 
* #e5650 ^property[2].valueCode = #e565 
* #e5651 "Systeme der Wirtschaft"
* #e5651 ^property[0].code = #None 
* #e5651 ^property[0].valueString = "Administrative Steuerungs- und Überwachungsmechanismen, die Produktion, Verteilung, Verbrauch und Verwendung von Gütern und Dienstleistungen regeln, wie Systeme für die Umsetzung und Überwachung der Handlungsgrundsätze der Wirtschaft" 
* #e5651 ^property[1].code = #None 
* #e5651 ^property[1].valueString = "Systeme des Versorgungswesens" 
* #e5651 ^property[2].code = #parent 
* #e5651 ^property[2].valueCode = #e565 
* #e5652 "Handlungsgrundsätze der Wirtschaft"
* #e5652 ^property[0].code = #None 
* #e5652 ^property[0].valueString = "Gesetze, Vorschriften und Standards, die Produktion, Verteilung, Verbrauch und Verwendung von Gütern und Dienstleistungen regeln, wie von staatlicher Seite angenommene und umgesetzte wirtschaftliche Grundsätze" 
* #e5652 ^property[1].code = #None 
* #e5652 ^property[1].valueString = "Handlungsgrundsätze des Versorgungswesens" 
* #e5652 ^property[2].code = #parent 
* #e5652 ^property[2].valueCode = #e565 
* #e5658 "Dienste, Systeme und Handlungsgrundsätze der Wirtschaft, anders bezeichnet"
* #e5658 ^property[0].code = #parent 
* #e5658 ^property[0].valueCode = #e565 
* #e5659 "Dienste, Systeme und Handlungsgrundsätze der Wirtschaft, nicht näher bezeichnet"
* #e5659 ^property[0].code = #parent 
* #e5659 ^property[0].valueCode = #e565 
* #e570 "Dienste, Systeme und Handlungsgrundsätze der sozialen Sicherheit"
* #e570 ^property[0].code = #None 
* #e570 ^property[0].valueString = "Dienste, Systeme und Handlungsgrundsätze für die finanzielle Unterstützung von Menschen, welche aufgrund von Alter, Armut, Arbeitslosigkeit, Gesundheitsproblemen oder Behinderung staatliche Unterstützung benötigen, die entweder durch Steueraufkommen oder Beitragssysteme finanziert wird" 
* #e570 ^property[1].code = #None 
* #e570 ^property[1].valueString = "Dienste, Systeme und Handlungsgrundsätze der Wirtschaft" 
* #e570 ^property[2].code = #parent 
* #e570 ^property[2].valueCode = #e5 
* #e570 ^property[3].code = #child 
* #e570 ^property[3].valueCode = #e5700 
* #e570 ^property[4].code = #child 
* #e570 ^property[4].valueCode = #e5701 
* #e570 ^property[5].code = #child 
* #e570 ^property[5].valueCode = #e5702 
* #e570 ^property[6].code = #child 
* #e570 ^property[6].valueCode = #e5708 
* #e570 ^property[7].code = #child 
* #e570 ^property[7].valueCode = #e5709 
* #e5700 "Dienste der sozialen Sicherheit"
* #e5700 ^property[0].code = #None 
* #e5700 ^property[0].valueString = "Dienste und Programme für die finanzielle Unterstützung von Menschen, welche aufgrund von Alter, Armut, Arbeitslosigkeit, Gesundheitsproblemen oder Behinderung staatliche Unterstützung benötigen, die entweder durch Steueraufkommen oder Beitragssysteme finanziert wird, wie Dienste zur Feststellung von Anspruchsberechtigungen, zur Auszahlung und Verteilung von Unterstützungsgeldern für die folgenden Programmarten: Programme der Sozialhilfe (z.B. nicht beitragspflichtige Sozialhilfe, armuts- oder andere bedürftigkeitsabhängige Ausgleichszahlungen), Programme der Sozialversicherung (z.B. beitragspflichtige Versicherung gegen Unfall oder Arbeitslosigkeit) sowie Rentensysteme für Invalidität/Erwerbsminderung und ähnliche Fährnisse (z.B. Einkommensersatzleistungen), einschließlich derer, die diese Dienste erbringen" 
* #e5700 ^property[1].code = #None 
* #e5700 ^property[1].valueString = "Dienste des Gesundheitswesens" 
* #e5700 ^property[2].code = #parent 
* #e5700 ^property[2].valueCode = #e570 
* #e5701 "Systeme der sozialen Sicherheit"
* #e5701 ^property[0].code = #None 
* #e5701 ^property[0].valueString = "Administrative Steuerungs- und Überwachungsmechanismen, die die Programme und Systeme regeln, welche finanzielle Unterstützung für Menschen gewähren, die aufgrund von Alter, Armut, Arbeitslosigkeit, Gesundheitsproblemen oder Behinderung staatliche Unterstützung benötigen, wie Systeme für die Umsetzung von Regeln und Vorschriften für die Feststellung der Anspruchsberechtigung für Zahlungen der Sozialhilfe und Wohlfahrt, der Arbeitslosenversicherung sowie für Leistungen aus Rentenversicherung (Altersrente, Rente wegen verminderter Erwerbsfähigkeit) und entsprechenden Versicherungen" 
* #e5701 ^property[1].code = #parent 
* #e5701 ^property[1].valueCode = #e570 
* #e5702 "Handlungsgrundsätze der sozialen Sicherheit"
* #e5702 ^property[0].code = #None 
* #e5702 ^property[0].valueString = "Gesetze, Vorschriften und Standards, die die Programme und Systeme regeln, die finanzielle Unterstützung für Menschen gewähren, welche aufgrund von Alter, Armut, Arbeitslosigkeit, Gesundheitsproblemen oder Behinderung staatliche Unterstützung benötigen, wie Gesetze und Vorschriften für die Feststellung der Anspruchsberechtigung für Zahlungen der Sozialhilfe und Wohlfahrt, der Arbeitslosenversicherung sowie für Leistungen aus Rentenversicherung (Altersrente, Rente wegen verminderter Erwerbsfähigkeit) und entsprechenden Versicherungen" 
* #e5702 ^property[1].code = #parent 
* #e5702 ^property[1].valueCode = #e570 
* #e5708 "Dienste, Systeme und Handlungsgrundsätze der sozialen Sicherheit, anders bezeichnet"
* #e5708 ^property[0].code = #parent 
* #e5708 ^property[0].valueCode = #e570 
* #e5709 "Dienste, Systeme und Handlungsgrundsätze der sozialen Sicherheit, nicht näher bezeichnet"
* #e5709 ^property[0].code = #parent 
* #e5709 ^property[0].valueCode = #e570 
* #e575 "Dienste, Systeme und Handlungsgrundsätze der allgemeinen sozialen Unterstützung"
* #e575 ^property[0].code = #None 
* #e575 ^property[0].valueString = "Dienste, Systeme und Handlungsgrundsätze für diejenigen, die Hilfe in Bereichen wie Einkaufen, Hausarbeit, Beförderung, Selbstversorgung und anderen benötigen, um eine vollständigere Partizipation [Teilhabe] am Leben in der Gesellschaft zu erlangen" 
* #e575 ^property[1].code = #None 
* #e575 ^property[1].valueString = "Dienste, Systeme und Handlungsgrundsätze der sozialen Sicherheit" 
* #e575 ^property[2].code = #parent 
* #e575 ^property[2].valueCode = #e5 
* #e575 ^property[3].code = #child 
* #e575 ^property[3].valueCode = #e5750 
* #e575 ^property[4].code = #child 
* #e575 ^property[4].valueCode = #e5751 
* #e575 ^property[5].code = #child 
* #e575 ^property[5].valueCode = #e5752 
* #e575 ^property[6].code = #child 
* #e575 ^property[6].valueCode = #e5758 
* #e575 ^property[7].code = #child 
* #e575 ^property[7].valueCode = #e5759 
* #e5750 "Dienste der allgemeinen sozialen Unterstützung"
* #e5750 ^property[0].code = #None 
* #e5750 ^property[0].valueString = "Dienste und Programme für die soziale Unterstützung von Menschen, die wegen Alters, Armut, Arbeitslosigkeit, Gesundheitsproblemen oder Behinderung öffentliche Hilfe in Bereichen wie Einkaufen, Hausarbeit, Beförderung, Selbstversorgung und Versorgung von Anderen benötigen, um eine vollständigere Partizipation [Teilhabe] am Leben in der Gesellschaft zu erlangen" 
* #e5750 ^property[1].code = #parent 
* #e5750 ^property[1].valueCode = #e575 
* #e5751 "Systeme der allgemeinen sozialen Unterstützung"
* #e5751 ^property[0].code = #None 
* #e5751 ^property[0].valueString = "Administrative Steuerungs- und Überwachungsmechanismen, die die Programme und Systeme für die soziale Unterstützung von Menschen, die wegen Alters, Armut, Arbeitslosigkeit, Gesundheitsproblemen oder Behinderung öffentliche Hilfe benötigen, einschließlich der Systeme für die Umsetzung von Regeln und Vorschriften für die Feststellung der Anspruchsberechtigung auf die Dienste der sozialen Unterstützung und die Erbringung dieser Dienste" 
* #e5751 ^property[1].code = #parent 
* #e5751 ^property[1].valueCode = #e575 
* #e5752 "Handlungsgrundsätze der allgemeinen sozialen Unterstützung"
* #e5752 ^property[0].code = #None 
* #e5752 ^property[0].valueString = "Gesetze, Vorschriften und Standards, die die Programme und Systeme für die soziale Unterstützung von Menschen, die wegen Alters, Armut, Arbeitslosigkeit, Gesundheitsproblemen oder Behinderung öffentliche Hilfe benötigen, regeln, einschließlich der Gesetze und Vorschriften für die Feststellung der Anspruchsberechtigung auf die Dienste der sozialen Unterstützung" 
* #e5752 ^property[1].code = #parent 
* #e5752 ^property[1].valueCode = #e575 
* #e5758 "Dienste, Systeme und Handlungsgrundsätze der allgemeinen sozialen Unterstützung, anders bezeichnet"
* #e5758 ^property[0].code = #parent 
* #e5758 ^property[0].valueCode = #e575 
* #e5759 "Dienste, Systeme und Handlungsgrundsätze der allgemeinen sozialen Unterstützung, nicht näher bezeichnet"
* #e5759 ^property[0].code = #parent 
* #e5759 ^property[0].valueCode = #e575 
* #e580 "Dienste, Systeme und Handlungsgrundsätze des Gesundheitswesens"
* #e580 ^property[0].code = #None 
* #e580 ^property[0].valueString = "Dienste, Systeme und Handlungsgrundsätze zur Vorbeugung und Behandlung von Gesundheitsproblemen, zur medizinischen Rehabilitation und zur Förderung einer gesunden Lebensführung" 
* #e580 ^property[1].code = #None 
* #e580 ^property[1].valueString = "Dienste, Systeme und Handlungsgrundsätze der allgemeinen sozialen Unterstützung" 
* #e580 ^property[2].code = #parent 
* #e580 ^property[2].valueCode = #e5 
* #e580 ^property[3].code = #child 
* #e580 ^property[3].valueCode = #e5800 
* #e580 ^property[4].code = #child 
* #e580 ^property[4].valueCode = #e5801 
* #e580 ^property[5].code = #child 
* #e580 ^property[5].valueCode = #e5802 
* #e580 ^property[6].code = #child 
* #e580 ^property[6].valueCode = #e5808 
* #e580 ^property[7].code = #child 
* #e580 ^property[7].valueCode = #e5809 
* #e5800 "Dienste des Gesundheitswesens"
* #e5800 ^property[0].code = #None 
* #e5800 ^property[0].valueString = "Dienste und Programme auf lokaler, auf kommunaler, regionaler, staatlicher bzw. nationaler Ebene, die Individuen Therapiemaßnahmen für ihr körperliches, geistiges, seelisches und soziales Wohl erbringen, wie Dienste für die Gesundheitsförderung und Krankheitsvorbeugung, die medizinische Grundversorgung, die Akutversorgung, die Rehabilitation und Langzeitpflege; öffentlich oder privat finanzierte Dienste, die kurzfristige, langfristige, periodische oder einmalige Leistungen erbringen, an unterschiedlichen Orten wie z.B. in der Gemeinde, zu Hause, in der Schule, am Arbeitsplatz, in allgemeinen Krankenhäusern, Fachkrankenhäusern, Kliniken, wohnortnahen und ?fernen Pflegeeinrichtungen, einschließlich derer, die diese Dienste erbringen" 
* #e5800 ^property[1].code = #parent 
* #e5800 ^property[1].valueCode = #e580 
* #e5801 "Systeme des Gesundheitswesens"
* #e5801 ^property[0].code = #None 
* #e5801 ^property[0].valueString = "Administrative Steuerungs- und Überwachungsmechanismen, die die Gesamtheit aller Dienste für das körperliche, geistige, seelische und soziale Wohl von Menschen regeln, in den verschiedenen Bereichen einschließlich in der Gemeinde, zu Hause, in der Schule, am Arbeitsplatz, in allgemeinen Krankenhäusern, Fachkrankenhäusern, Kliniken, wohnortnahen und -fernen Pflegeeinrichtungen, wie Systeme für die Umsetzung von Vorschriften und Standards für die Feststellung der Anspruchsberechtigung auf die Dienste, Versorgung mit Geräten, Hilfstechnologie und andere angepasste Ausrüstungsgegenstände, sowie Gesetze wie Gesundheitsgesetze, die die Merkmale eines Gesundheitssystems regeln wie Zugänglichkeit, Allgemeingültigkeit, Übertragbarkeit, öffentliche Finanzierung und Umfang" 
* #e5801 ^property[1].code = #parent 
* #e5801 ^property[1].valueCode = #e580 
* #e5802 "Handlungsgrundsätze des Gesundheitswesens"
* #e5802 ^property[0].code = #None 
* #e5802 ^property[0].valueString = "Gesetze, Vorschriften und Standards, die die Gesamtheit aller Dienste für das körperliche, geistige, seelische und soziale Wohl von Menschen regeln, in den verschiedenen Bereichen einschließlich Dienste in der Gemeinde, zu Hause, in der Schule, am Arbeitsplatz, in allgemeinen Krankenhäusern, Fachkrankenhäusern, Kliniken, wohnortnahen und -fernen Pflegeeinrichtungen, wie Handlungsgrundsätze und Standards für die Feststellung der Anspruchsberechtigung auf die Dienste, Versorgung mit Geräten, Hilfstechnologie und andere angepasste Ausrüstungsgegenstände, sowie Gesetze wie Gesundheitsgesetze, die die Merkmale eines Gesundheitssystems regeln wie Zugänglichkeit, Allgemeingültigkeit, Übertragbarkeit, öffentliche Finanzierung und Umfang" 
* #e5802 ^property[1].code = #parent 
* #e5802 ^property[1].valueCode = #e580 
* #e5808 "Dienste, Systeme und Handlungsgrundsätze des Gesundheitswesens, anders bezeichnet"
* #e5808 ^property[0].code = #parent 
* #e5808 ^property[0].valueCode = #e580 
* #e5809 "Dienste, Systeme und Handlungsgrundsätze des Gesundheitswesens, nicht näher bezeichnet"
* #e5809 ^property[0].code = #parent 
* #e5809 ^property[0].valueCode = #e580 
* #e585 "Dienste, Systeme und Handlungsgrundsätze des Bildungs- und Ausbildungswesens"
* #e585 ^property[0].code = #None 
* #e585 ^property[0].valueString = "Dienste, Systeme und Handlungsgrundsätze für die Aneignung, Erhaltung und Vergrößerung von Wissen, Fachkenntnissen und beruflichen oder künstlerischen Fertigkeiten. Siehe International Standard Classification of Education der UNESCO (ISCED-1997)" 
* #e585 ^property[1].code = #parent 
* #e585 ^property[1].valueCode = #e5 
* #e585 ^property[2].code = #child 
* #e585 ^property[2].valueCode = #e5850 
* #e585 ^property[3].code = #child 
* #e585 ^property[3].valueCode = #e5851 
* #e585 ^property[4].code = #child 
* #e585 ^property[4].valueCode = #e5852 
* #e585 ^property[5].code = #child 
* #e585 ^property[5].valueCode = #e5858 
* #e585 ^property[6].code = #child 
* #e585 ^property[6].valueCode = #e5859 
* #e5850 "Dienste des Bildungs- und Ausbildungswesens"
* #e5850 ^property[0].code = #None 
* #e5850 ^property[0].valueString = "Dienste und Programme, die sich mit Bildung/Ausbildung sowie Aneignung, Erhaltung und Vergrößerung von Wissen, Fachkenntnissen und beruflichen oder künstlerischen Fertigkeiten befassen, wie Dienstleistungen und Programme für verschiedene Ebenen der Bildung (z.B. Vorschuleinrichtungen, Primar- und Sekundarschulen I und II, Postsekundareinrichtungen, berufliche Bildungs-/Ausbildungsprogramme, Schulung von Fertigkeiten und Kenntnissen, Lehrlingsausbildung, Fort- und Weiterbildung), einschließlich derer, die diese Dienste erbringen" 
* #e5850 ^property[1].code = #parent 
* #e5850 ^property[1].valueCode = #e585 
* #e5851 "Systeme des Bildungs- und Ausbildungswesens"
* #e5851 ^property[0].code = #None 
* #e5851 ^property[0].valueString = "Administrative Steuerungs- und Überwachungsmechanismen, die die Bereitstellung von Bildungs-/Ausbildungsprogrammen regeln, wie Systeme für die Umsetzung von Handlungsgrundsätzen und Standards für die Feststellung der Zulassungsberechtigung für öffentliche oder private Bildungs- und spezielle bedarfsgerechte Programme; lokale, regionale und nationale Bildungsbehörden oder andere autorisierte Gremien, die die Merkmale der Bildungs-/Ausbildungssysteme regeln, einschließlich Curricula, Klassengrößen, Zahl der Schulen in einer Region, Gebühren und Stipendien, besondere Schulspeisungsprogramme und Betreuungsprogramme nach der Schule" 
* #e5851 ^property[1].code = #parent 
* #e5851 ^property[1].valueCode = #e585 
* #e5852 "Handlungsgrundsätze des Bildungs- und Ausbildungswesens"
* #e5852 ^property[0].code = #None 
* #e5852 ^property[0].valueString = "Gesetze, Vorschriften und Standards, die die Bereitstellung von Bildungs-/Ausbildungsprogrammen regeln, wie Systeme für die Umsetzung von Handlungsgrundsätzen und Standards für die Feststellung der Zulassungsberechtigung für öffentliche oder private Bildungs- und Sonderschulprogramme und die die Struktur der lokalen, regionalen und nationalen Bildungsbehörden oder anderer autorisierter Gremien vorschreiben, die die Merkmale der Bildungs- /Ausbildungssysteme regeln, einschließlich Curricula, Klassengrößen, Zahl der Schulen in einer Region, Gebühren und Stipendien, besondere Schulspeisungsprogramme und Betreuungsprogramme nach der Schule" 
* #e5852 ^property[1].code = #parent 
* #e5852 ^property[1].valueCode = #e585 
* #e5858 "Dienste, Systeme und Handlungsgrundsätze des Bildungs- und Ausbildungswesens, anders bezeichnet"
* #e5858 ^property[0].code = #parent 
* #e5858 ^property[0].valueCode = #e585 
* #e5859 "Dienste, Systeme und Handlungsgrundsätze des Bildungs- und Ausbildungswesens, nicht näher bezeichnet"
* #e5859 ^property[0].code = #parent 
* #e5859 ^property[0].valueCode = #e585 
* #e590 "Dienste, Systeme und Handlungsgrundsätze des Arbeits- und Beschäftigungswesens"
* #e590 ^property[0].code = #None 
* #e590 ^property[0].valueString = "Dienste, Systeme und Handlungsgrundsätze zur Vermittlung passender Arbeit für Personen, die arbeitslos sind oder den Arbeitsplatz wechseln wollen, oder zur Unterstützung von Arbeitnehmern, die einen Aufstieg beabsichtigen" 
* #e590 ^property[1].code = #None 
* #e590 ^property[1].valueString = "Dienste, Systeme und Handlungsgrundsätze der Wirtschaft" 
* #e590 ^property[2].code = #parent 
* #e590 ^property[2].valueCode = #e5 
* #e590 ^property[3].code = #child 
* #e590 ^property[3].valueCode = #e5900 
* #e590 ^property[4].code = #child 
* #e590 ^property[4].valueCode = #e5901 
* #e590 ^property[5].code = #child 
* #e590 ^property[5].valueCode = #e5902 
* #e590 ^property[6].code = #child 
* #e590 ^property[6].valueCode = #e5908 
* #e590 ^property[7].code = #child 
* #e590 ^property[7].valueCode = #e5909 
* #e5900 "Dienste des Arbeits- und Beschäftigungswesens"
* #e5900 ^property[0].code = #None 
* #e5900 ^property[0].valueString = "Dienste und Programme von kommunalen, regionalen oder staatlichen Verwaltungen oder von privaten Organisationen zur Vermittlung passender Arbeit für Personen, die arbeitslos sind oder den Arbeitsplatz wechseln wollen, oder zur Unterstützung von Arbeitnehmern, wie Einrichtungen für die Arbeitssuche und -vorbereitung, für die Wiederbeschäftigung, Stellenvermittlung, Berufsberatung, berufsbezogene Nachfolgesuche, berufliche Gesundheits- und Sicherheitsdienste, Arbeitsplatzgestaltung (z.B. Ergonomie, Personalmanagement, Betriebsrat und ähnliche Einrichtungen, Berufsverbände), einschließlich derer, die diese Dienstleistungen und Programme zur Verfügung stellen" 
* #e5900 ^property[1].code = #parent 
* #e5900 ^property[1].valueCode = #e590 
* #e5901 "Systeme des Arbeits- und Beschäftigungswesens"
* #e5901 ^property[0].code = #None 
* #e5901 ^property[0].valueString = "Administrative Steuerungs- und Überwachungsmechanismen, die die Vergabe von Arbeitsplätzen und anderen Arten entlohnter Arbeit in der Wirtschaft regeln, wie Systeme für die Umsetzung von Handlungsgrundsätzen und Standards für die Schaffung von Arbeitsplätzen, Arbeitsschutz, geschützte und freie Beschäftigung, Arbeitsstandards und ?recht sowie Gewerkschaften" 
* #e5901 ^property[1].code = #parent 
* #e5901 ^property[1].valueCode = #e590 
* #e5902 "Handlungsgrundsätze des Arbeits- und Beschäftigungswesens"
* #e5902 ^property[0].code = #None 
* #e5902 ^property[0].valueString = "Gesetze, Vorschriften und Standards, die die Vergabe von Beschäftigungsverhältnissen und anderen Arten entlohnter Arbeit in der Wirtschaft regeln, wie Systeme für die Umsetzung von Handlungsgrundsätzen und Standards für die Schaffung von Arbeitsplätzen, Arbeitsschutz, geschützte und freie Beschäftigung, Arbeitsstandards und -recht sowie Gewerkschaften" 
* #e5902 ^property[1].code = #parent 
* #e5902 ^property[1].valueCode = #e590 
* #e5908 "Dienste, Systeme und Handlungsgrundsätze des Arbeits- und Beschäftigungswesens, anders bezeichnet"
* #e5908 ^property[0].code = #parent 
* #e5908 ^property[0].valueCode = #e590 
* #e5909 "Dienste, Systeme und Handlungsgrundsätze des Arbeits- und Beschäftigungswesens, nicht näher bezeichnet"
* #e5909 ^property[0].code = #parent 
* #e5909 ^property[0].valueCode = #e590 
* #e595 "Dienste, Systeme und Handlungsgrundsätze der Politik"
* #e595 ^property[0].code = #None 
* #e595 ^property[0].valueString = "Dienste, Systeme und Handlungsgrundsätze, die Abstimmungen, Wahlen und Regieren von Ländern, Regionen, Kommunen sowie internationalen Organisationen betreffen" 
* #e595 ^property[1].code = #parent 
* #e595 ^property[1].valueCode = #e5 
* #e595 ^property[2].code = #child 
* #e595 ^property[2].valueCode = #e5950 
* #e595 ^property[3].code = #child 
* #e595 ^property[3].valueCode = #e5951 
* #e595 ^property[4].code = #child 
* #e595 ^property[4].valueCode = #e5952 
* #e595 ^property[5].code = #child 
* #e595 ^property[5].valueCode = #e5958 
* #e595 ^property[6].code = #child 
* #e595 ^property[6].valueCode = #e5959 
* #e5950 "Dienste der Politik"
* #e5950 ^property[0].code = #None 
* #e5950 ^property[0].valueString = "Dienste und Strukturen wie kommunale, regionale und nationale Regierungen, internationale Organisationen sowie Personen, die für Positionen innerhalb dieser Strukturen gewählt oder benannt sind, wie die Vereinten Nationen, Europäische Union, Regierungen, regionale Instanzen, kommunale Ortsbehörden, traditionelle Führer" 
* #e5950 ^property[1].code = #parent 
* #e5950 ^property[1].valueCode = #e595 
* #e5951 "Systeme der Politik"
* #e5951 ^property[0].code = #None 
* #e5951 ^property[0].valueString = "Strukturen und damit im Zusammenhang stehende Tätigkeiten, die die politische und wirtschaftliche Macht in der Gesellschaft organisieren, wie exekutive und legislative Bereiche der Regierung sowie die verfassungsmäßigen oder anderen Quellen, von denen sie ihre Legitimation ableiten, wie politisch-organisatorische Grundsätze, Verfassungen der exekutiven und legislativen Bereiche der Regierung, Militär" 
* #e5951 ^property[1].code = #parent 
* #e5951 ^property[1].valueCode = #e595 
* #e5952 "Handlungsgrundsätze der Politik"
* #e5952 ^property[0].code = #None 
* #e5952 ^property[0].valueString = "Gesetze und Handlungsgrundsätze für das politische System, die die Arbeitsfähigkeit des politischen Systems regeln, wie Handlungsgrundsätze für Wahlkampagnen, Registrierung politischer Parteien, Wahlen sowie die Benennung von Mitgliedern in internationalen politischen Organisationen einschließlich Verträge, Verfassungs- und anderes Recht, das Gesetze und Vorschriften regelt" 
* #e5952 ^property[1].code = #parent 
* #e5952 ^property[1].valueCode = #e595 
* #e5958 "Dienste, Systeme und Handlungsgrundsätze der Politik, anders bezeichnet"
* #e5958 ^property[0].code = #parent 
* #e5958 ^property[0].valueCode = #e595 
* #e5959 "Dienste, Systeme und Handlungsgrundsätze der Politik, nicht näher bezeichnet"
* #e5959 ^property[0].code = #parent 
* #e5959 ^property[0].valueCode = #e595 
* #e598 "Dienste, Systeme und Handlungsgrundsätze, anders bezeichnet"
* #e598 ^property[0].code = #parent 
* #e598 ^property[0].valueCode = #e5 
* #e599 "Dienste, Systeme und Handlungsgrundsätze, nicht näher bezeichnet"
* #e599 ^property[0].code = #parent 
* #e599 ^property[0].valueCode = #e5 
* #s "Körperstrukturen"
* #s ^property[0].code = #None 
* #s ^property[0].valueString = "Körperstrukturen sind anatomische Teile des Körpers, wie Organe, Gliedmaßen und ihre Bestandteile." 
* #s ^property[1].code = #None 
* #s ^property[1].valueString = "Schädigungen sind Beeinträchtigungen einer Körperfunktion oder -struktur, wie z.B. eine wesentliche Abweichung oder ein Verlust." 
* #s ^property[2].code = #child 
* #s ^property[2].valueCode = #s1 
* #s ^property[3].code = #child 
* #s ^property[3].valueCode = #s2 
* #s ^property[4].code = #child 
* #s ^property[4].valueCode = #s3 
* #s ^property[5].code = #child 
* #s ^property[5].valueCode = #s4 
* #s ^property[6].code = #child 
* #s ^property[6].valueCode = #s5 
* #s ^property[7].code = #child 
* #s ^property[7].valueCode = #s6 
* #s ^property[8].code = #child 
* #s ^property[8].valueCode = #s7 
* #s ^property[9].code = #child 
* #s ^property[9].valueCode = #s8 
* #s1 "Strukturen des Nervensystems"
* #s1 ^property[0].code = #parent 
* #s1 ^property[0].valueCode = #s 
* #s1 ^property[1].code = #child 
* #s1 ^property[1].valueCode = #s110 
* #s1 ^property[2].code = #child 
* #s1 ^property[2].valueCode = #s120 
* #s1 ^property[3].code = #child 
* #s1 ^property[3].valueCode = #s130 
* #s1 ^property[4].code = #child 
* #s1 ^property[4].valueCode = #s140 
* #s1 ^property[5].code = #child 
* #s1 ^property[5].valueCode = #s150 
* #s1 ^property[6].code = #child 
* #s1 ^property[6].valueCode = #s198 
* #s1 ^property[7].code = #child 
* #s1 ^property[7].valueCode = #s199 
* #s110 "Strukur des Gehirns"
* #s110 ^property[0].code = #parent 
* #s110 ^property[0].valueCode = #s1 
* #s110 ^property[1].code = #child 
* #s110 ^property[1].valueCode = #s1100 
* #s110 ^property[2].code = #child 
* #s110 ^property[2].valueCode = #s1101 
* #s110 ^property[3].code = #child 
* #s110 ^property[3].valueCode = #s1102 
* #s110 ^property[4].code = #child 
* #s110 ^property[4].valueCode = #s1103 
* #s110 ^property[5].code = #child 
* #s110 ^property[5].valueCode = #s1104 
* #s110 ^property[6].code = #child 
* #s110 ^property[6].valueCode = #s1105 
* #s110 ^property[7].code = #child 
* #s110 ^property[7].valueCode = #s1106 
* #s110 ^property[8].code = #child 
* #s110 ^property[8].valueCode = #s1108 
* #s110 ^property[9].code = #child 
* #s110 ^property[9].valueCode = #s1109 
* #s1100 "Struktur der Großhirnhälften"
* #s1100 ^property[0].code = #parent 
* #s1100 ^property[0].valueCode = #s110 
* #s1100 ^property[1].code = #child 
* #s1100 ^property[1].valueCode = #s11000 
* #s1100 ^property[2].code = #child 
* #s1100 ^property[2].valueCode = #s11001 
* #s1100 ^property[3].code = #child 
* #s1100 ^property[3].valueCode = #s11002 
* #s1100 ^property[4].code = #child 
* #s1100 ^property[4].valueCode = #s11003 
* #s1100 ^property[5].code = #child 
* #s1100 ^property[5].valueCode = #s11008 
* #s1100 ^property[6].code = #child 
* #s1100 ^property[6].valueCode = #s11009 
* #s11000 "Stirnlappen (Frontallappen)"
* #s11000 ^property[0].code = #parent 
* #s11000 ^property[0].valueCode = #s1100 
* #s11001 "Schläfenlappen (Temporallappen)"
* #s11001 ^property[0].code = #parent 
* #s11001 ^property[0].valueCode = #s1100 
* #s11002 "Scheitellappen (Parietallappen)"
* #s11002 ^property[0].code = #parent 
* #s11002 ^property[0].valueCode = #s1100 
* #s11003 "Hinterhauptslappen (Okzipitallappen)"
* #s11003 ^property[0].code = #parent 
* #s11003 ^property[0].valueCode = #s1100 
* #s11008 "Struktur des Großhirns, anders bezeichnet"
* #s11008 ^property[0].code = #parent 
* #s11008 ^property[0].valueCode = #s1100 
* #s11009 "Struktur des Großhirns, nicht näher bezeichnet"
* #s11009 ^property[0].code = #parent 
* #s11009 ^property[0].valueCode = #s1100 
* #s1101 "Struktur des Mittelhirns"
* #s1101 ^property[0].code = #parent 
* #s1101 ^property[0].valueCode = #s110 
* #s1102 "Struktur des Zwischenhirns"
* #s1102 ^property[0].code = #parent 
* #s1102 ^property[0].valueCode = #s110 
* #s1103 "Basalganglien und mit ihnen in Zusammenhang stehende Strukturen"
* #s1103 ^property[0].code = #parent 
* #s1103 ^property[0].valueCode = #s110 
* #s1104 "Struktur des Kleinhirns"
* #s1104 ^property[0].code = #parent 
* #s1104 ^property[0].valueCode = #s110 
* #s1105 "Struktur des Hirnstamms"
* #s1105 ^property[0].code = #parent 
* #s1105 ^property[0].valueCode = #s110 
* #s1105 ^property[1].code = #child 
* #s1105 ^property[1].valueCode = #s11050 
* #s1105 ^property[2].code = #child 
* #s1105 ^property[2].valueCode = #s11051 
* #s1105 ^property[3].code = #child 
* #s1105 ^property[3].valueCode = #s11058 
* #s1105 ^property[4].code = #child 
* #s1105 ^property[4].valueCode = #s11059 
* #s11050 "Medulla oblongata"
* #s11050 ^property[0].code = #parent 
* #s11050 ^property[0].valueCode = #s1105 
* #s11051 "Brücke (Pons)"
* #s11051 ^property[0].code = #parent 
* #s11051 ^property[0].valueCode = #s1105 
* #s11058 "Struktur des Hirnstamms, anders bezeichnet"
* #s11058 ^property[0].code = #parent 
* #s11058 ^property[0].valueCode = #s1105 
* #s11059 "Struktur des Hirnstamms, nicht näher bezeichnet"
* #s11059 ^property[0].code = #parent 
* #s11059 ^property[0].valueCode = #s1105 
* #s1106 "Struktur der Hirnnerven"
* #s1106 ^property[0].code = #parent 
* #s1106 ^property[0].valueCode = #s110 
* #s1108 "Strukur des Gehirns, anders bezeichnet"
* #s1108 ^property[0].code = #parent 
* #s1108 ^property[0].valueCode = #s110 
* #s1109 "Strukur des Gehirns, nicht näher bezeichnet"
* #s1109 ^property[0].code = #parent 
* #s1109 ^property[0].valueCode = #s110 
* #s120 "Struktur des Rückenmarks und mit ihm in Zusammenhang stehende Strukturen"
* #s120 ^property[0].code = #parent 
* #s120 ^property[0].valueCode = #s1 
* #s120 ^property[1].code = #child 
* #s120 ^property[1].valueCode = #s1200 
* #s120 ^property[2].code = #child 
* #s120 ^property[2].valueCode = #s1201 
* #s120 ^property[3].code = #child 
* #s120 ^property[3].valueCode = #s1208 
* #s120 ^property[4].code = #child 
* #s120 ^property[4].valueCode = #s1209 
* #s1200 "Struktur des Rückenmarks"
* #s1200 ^property[0].code = #parent 
* #s1200 ^property[0].valueCode = #s120 
* #s1200 ^property[1].code = #child 
* #s1200 ^property[1].valueCode = #s12000 
* #s1200 ^property[2].code = #child 
* #s1200 ^property[2].valueCode = #s12001 
* #s1200 ^property[3].code = #child 
* #s1200 ^property[3].valueCode = #s12002 
* #s1200 ^property[4].code = #child 
* #s1200 ^property[4].valueCode = #s12003 
* #s1200 ^property[5].code = #child 
* #s1200 ^property[5].valueCode = #s12008 
* #s1200 ^property[6].code = #child 
* #s1200 ^property[6].valueCode = #s12009 
* #s12000 "Halsmark (Zervicalmark)"
* #s12000 ^property[0].code = #parent 
* #s12000 ^property[0].valueCode = #s1200 
* #s12001 "Brustmark (Thorakalmark)"
* #s12001 ^property[0].code = #parent 
* #s12001 ^property[0].valueCode = #s1200 
* #s12002 "Lenden- und Kreuzmark (Lumbosakralmark)"
* #s12002 ^property[0].code = #parent 
* #s12002 ^property[0].valueCode = #s1200 
* #s12003 "Kaudafasern (Cauda equina)"
* #s12003 ^property[0].code = #parent 
* #s12003 ^property[0].valueCode = #s1200 
* #s12008 "Struktur des Rückenmarks, anders bezeichnet"
* #s12008 ^property[0].code = #parent 
* #s12008 ^property[0].valueCode = #s1200 
* #s12009 "Struktur des Rückenmarks, nicht näher bezeichnet"
* #s12009 ^property[0].code = #parent 
* #s12009 ^property[0].valueCode = #s1200 
* #s1201 "Spinalnerven"
* #s1201 ^property[0].code = #parent 
* #s1201 ^property[0].valueCode = #s120 
* #s1208 "Struktur des Rückenmarks und mit ihm in Zusammenhang stehende Strukturen, anders bezeichnet"
* #s1208 ^property[0].code = #parent 
* #s1208 ^property[0].valueCode = #s120 
* #s1209 "Struktur des Rückenmarks und mit ihm in Zusammenhang stehende Strukturen, nicht näher bezeichnet"
* #s1209 ^property[0].code = #parent 
* #s1209 ^property[0].valueCode = #s120 
* #s130 "Struktur der Hirnhaut"
* #s130 ^property[0].code = #parent 
* #s130 ^property[0].valueCode = #s1 
* #s140 "Struktur des sympathischen Nervensystems"
* #s140 ^property[0].code = #parent 
* #s140 ^property[0].valueCode = #s1 
* #s150 "Struktur des parasympathischen Nervensystems"
* #s150 ^property[0].code = #parent 
* #s150 ^property[0].valueCode = #s1 
* #s198 "Struktur des Nervensystems, anders bezeichnet"
* #s198 ^property[0].code = #parent 
* #s198 ^property[0].valueCode = #s1 
* #s199 "Struktur des Nervensystems, nicht näher bezeichnet"
* #s199 ^property[0].code = #parent 
* #s199 ^property[0].valueCode = #s1 
* #s2 "Das Auge, das Ohr und mit diesen in Zusammenhang stehende Strukturen"
* #s2 ^property[0].code = #parent 
* #s2 ^property[0].valueCode = #s 
* #s2 ^property[1].code = #child 
* #s2 ^property[1].valueCode = #s210 
* #s2 ^property[2].code = #child 
* #s2 ^property[2].valueCode = #s220 
* #s2 ^property[3].code = #child 
* #s2 ^property[3].valueCode = #s230 
* #s2 ^property[4].code = #child 
* #s2 ^property[4].valueCode = #s240 
* #s2 ^property[5].code = #child 
* #s2 ^property[5].valueCode = #s250 
* #s2 ^property[6].code = #child 
* #s2 ^property[6].valueCode = #s260 
* #s2 ^property[7].code = #child 
* #s2 ^property[7].valueCode = #s298 
* #s2 ^property[8].code = #child 
* #s2 ^property[8].valueCode = #s299 
* #s210 "Struktur der Augenhöhle (Orbita)"
* #s210 ^property[0].code = #parent 
* #s210 ^property[0].valueCode = #s2 
* #s220 "Struktur des Augapfels (Bulbus)"
* #s220 ^property[0].code = #parent 
* #s220 ^property[0].valueCode = #s2 
* #s220 ^property[1].code = #child 
* #s220 ^property[1].valueCode = #s2200 
* #s220 ^property[2].code = #child 
* #s220 ^property[2].valueCode = #s2201 
* #s220 ^property[3].code = #child 
* #s220 ^property[3].valueCode = #s2202 
* #s220 ^property[4].code = #child 
* #s220 ^property[4].valueCode = #s2203 
* #s220 ^property[5].code = #child 
* #s220 ^property[5].valueCode = #s2204 
* #s220 ^property[6].code = #child 
* #s220 ^property[6].valueCode = #s2205 
* #s220 ^property[7].code = #child 
* #s220 ^property[7].valueCode = #s2208 
* #s220 ^property[8].code = #child 
* #s220 ^property[8].valueCode = #s2209 
* #s2200 "Bindehaut (Konjunktiva), Lederhaut (Sklera), Aderhaut (Chorioidea)"
* #s2200 ^property[0].code = #parent 
* #s2200 ^property[0].valueCode = #s220 
* #s2201 "Hornhaut (Kornea)"
* #s2201 ^property[0].code = #parent 
* #s2201 ^property[0].valueCode = #s220 
* #s2202 "Regenbogenhaut (Iris)"
* #s2202 ^property[0].code = #parent 
* #s2202 ^property[0].valueCode = #s220 
* #s2203 "Netzhaut (Retina)"
* #s2203 ^property[0].code = #parent 
* #s2203 ^property[0].valueCode = #s220 
* #s2204 "Linse des Augapfels"
* #s2204 ^property[0].code = #parent 
* #s2204 ^property[0].valueCode = #s220 
* #s2205 "Glaskörper (Corpus vitreum)"
* #s2205 ^property[0].code = #parent 
* #s2205 ^property[0].valueCode = #s220 
* #s2208 "Struktur des Augapfels, anders bezeichnet"
* #s2208 ^property[0].code = #parent 
* #s2208 ^property[0].valueCode = #s220 
* #s2209 "Struktur des Augapfels, nicht näher bezeichnet"
* #s2209 ^property[0].code = #parent 
* #s2209 ^property[0].valueCode = #s220 
* #s230 "Strukturen um das Auge herum"
* #s230 ^property[0].code = #parent 
* #s230 ^property[0].valueCode = #s2 
* #s230 ^property[1].code = #child 
* #s230 ^property[1].valueCode = #s2300 
* #s230 ^property[2].code = #child 
* #s230 ^property[2].valueCode = #s2301 
* #s230 ^property[3].code = #child 
* #s230 ^property[3].valueCode = #s2302 
* #s230 ^property[4].code = #child 
* #s230 ^property[4].valueCode = #s2303 
* #s230 ^property[5].code = #child 
* #s230 ^property[5].valueCode = #s2308 
* #s230 ^property[6].code = #child 
* #s230 ^property[6].valueCode = #s2309 
* #s2300 "Tränendrüsen und mit ihnen in Zusammenhang stehende Strukturen"
* #s2300 ^property[0].code = #parent 
* #s2300 ^property[0].valueCode = #s230 
* #s2301 "Augenlid"
* #s2301 ^property[0].code = #parent 
* #s2301 ^property[0].valueCode = #s230 
* #s2302 "Augenbrauen"
* #s2302 ^property[0].code = #parent 
* #s2302 ^property[0].valueCode = #s230 
* #s2303 "Externe Augenmuskeln"
* #s2303 ^property[0].code = #parent 
* #s2303 ^property[0].valueCode = #s230 
* #s2308 "Strukturen um das Auge herum, anders bezeichnet"
* #s2308 ^property[0].code = #parent 
* #s2308 ^property[0].valueCode = #s230 
* #s2309 "Strukturen um das Auge herum, nicht näher bezeichnet"
* #s2309 ^property[0].code = #parent 
* #s2309 ^property[0].valueCode = #s230 
* #s240 "Struktur des äußeren Ohres"
* #s240 ^property[0].code = #parent 
* #s240 ^property[0].valueCode = #s2 
* #s250 "Struktur des Mittelohres"
* #s250 ^property[0].code = #parent 
* #s250 ^property[0].valueCode = #s2 
* #s250 ^property[1].code = #child 
* #s250 ^property[1].valueCode = #s2500 
* #s250 ^property[2].code = #child 
* #s250 ^property[2].valueCode = #s2501 
* #s250 ^property[3].code = #child 
* #s250 ^property[3].valueCode = #s2502 
* #s250 ^property[4].code = #child 
* #s250 ^property[4].valueCode = #s2508 
* #s250 ^property[5].code = #child 
* #s250 ^property[5].valueCode = #s2509 
* #s2500 "Trommelfell (Membrana tympani)"
* #s2500 ^property[0].code = #parent 
* #s2500 ^property[0].valueCode = #s250 
* #s2501 "Ohrtrompete (Tuba Eustachii)"
* #s2501 ^property[0].code = #parent 
* #s2501 ^property[0].valueCode = #s250 
* #s2502 "Gehörknöchelchen (Ossicula auditus)"
* #s2502 ^property[0].code = #parent 
* #s2502 ^property[0].valueCode = #s250 
* #s2508 "Struktur des Mittelohres, anders bezeichnet"
* #s2508 ^property[0].code = #parent 
* #s2508 ^property[0].valueCode = #s250 
* #s2509 "Struktur des Mittelohres, nicht näher bezeichnet"
* #s2509 ^property[0].code = #parent 
* #s2509 ^property[0].valueCode = #s250 
* #s260 "Struktur des Innenohres"
* #s260 ^property[0].code = #parent 
* #s260 ^property[0].valueCode = #s2 
* #s260 ^property[1].code = #child 
* #s260 ^property[1].valueCode = #s2600 
* #s260 ^property[2].code = #child 
* #s260 ^property[2].valueCode = #s2601 
* #s260 ^property[3].code = #child 
* #s260 ^property[3].valueCode = #s2602 
* #s260 ^property[4].code = #child 
* #s260 ^property[4].valueCode = #s2603 
* #s260 ^property[5].code = #child 
* #s260 ^property[5].valueCode = #s2608 
* #s260 ^property[6].code = #child 
* #s260 ^property[6].valueCode = #s2609 
* #s2600 "Schnecke (Cochlea)"
* #s2600 ^property[0].code = #parent 
* #s2600 ^property[0].valueCode = #s260 
* #s2601 "Vorhoflabyrinth (Labyrinthus vestibularis)"
* #s2601 ^property[0].code = #parent 
* #s2601 ^property[0].valueCode = #s260 
* #s2602 "Knöcherner Bogengang (Canalis semicircularis osseus)"
* #s2602 ^property[0].code = #parent 
* #s2602 ^property[0].valueCode = #s260 
* #s2603 "Innerer Gehörgang (Meatus acusticus internus)"
* #s2603 ^property[0].code = #parent 
* #s2603 ^property[0].valueCode = #s260 
* #s2608 "Struktur des Innenohres, anders bezeichnet"
* #s2608 ^property[0].code = #parent 
* #s2608 ^property[0].valueCode = #s260 
* #s2609 "Struktur des Innenohres, nicht näher bezeichnet"
* #s2609 ^property[0].code = #parent 
* #s2609 ^property[0].valueCode = #s260 
* #s298 "Strukturen des Auges, des Ohres und mit ihnen in Zusammenhang stehende Strukturen, anders bezeichnet"
* #s298 ^property[0].code = #parent 
* #s298 ^property[0].valueCode = #s2 
* #s299 "Strukturen des Auges, des Ohres und mit ihnen in Zusammenhang stehende Strukturen, nicht näher bezeichnet"
* #s299 ^property[0].code = #parent 
* #s299 ^property[0].valueCode = #s2 
* #s3 "Strukturen, die an der Stimme und dem Sprechen beteiligt sind"
* #s3 ^property[0].code = #parent 
* #s3 ^property[0].valueCode = #s 
* #s3 ^property[1].code = #child 
* #s3 ^property[1].valueCode = #s310 
* #s3 ^property[2].code = #child 
* #s3 ^property[2].valueCode = #s320 
* #s3 ^property[3].code = #child 
* #s3 ^property[3].valueCode = #s330 
* #s3 ^property[4].code = #child 
* #s3 ^property[4].valueCode = #s340 
* #s3 ^property[5].code = #child 
* #s3 ^property[5].valueCode = #s398 
* #s3 ^property[6].code = #child 
* #s3 ^property[6].valueCode = #s399 
* #s310 "Struktur der Nase"
* #s310 ^property[0].code = #parent 
* #s310 ^property[0].valueCode = #s3 
* #s310 ^property[1].code = #child 
* #s310 ^property[1].valueCode = #s3100 
* #s310 ^property[2].code = #child 
* #s310 ^property[2].valueCode = #s3101 
* #s310 ^property[3].code = #child 
* #s310 ^property[3].valueCode = #s3102 
* #s310 ^property[4].code = #child 
* #s310 ^property[4].valueCode = #s3108 
* #s310 ^property[5].code = #child 
* #s310 ^property[5].valueCode = #s3109 
* #s3100 "Äußere Nase"
* #s3100 ^property[0].code = #parent 
* #s3100 ^property[0].valueCode = #s310 
* #s3101 "Nasenseptum"
* #s3101 ^property[0].code = #parent 
* #s3101 ^property[0].valueCode = #s310 
* #s3102 "Nasenhöhle"
* #s3102 ^property[0].code = #parent 
* #s3102 ^property[0].valueCode = #s310 
* #s3108 "Struktur der Nase, anders bezeichnet"
* #s3108 ^property[0].code = #parent 
* #s3108 ^property[0].valueCode = #s310 
* #s3109 "Struktur der Nase, nicht näher bezeichnet"
* #s3109 ^property[0].code = #parent 
* #s3109 ^property[0].valueCode = #s310 
* #s320 "Struktur des Mundes"
* #s320 ^property[0].code = #parent 
* #s320 ^property[0].valueCode = #s3 
* #s320 ^property[1].code = #child 
* #s320 ^property[1].valueCode = #s3200 
* #s320 ^property[2].code = #child 
* #s320 ^property[2].valueCode = #s3201 
* #s320 ^property[3].code = #child 
* #s320 ^property[3].valueCode = #s3202 
* #s320 ^property[4].code = #child 
* #s320 ^property[4].valueCode = #s3203 
* #s320 ^property[5].code = #child 
* #s320 ^property[5].valueCode = #s3204 
* #s320 ^property[6].code = #child 
* #s320 ^property[6].valueCode = #s3208 
* #s320 ^property[7].code = #child 
* #s320 ^property[7].valueCode = #s3209 
* #s3200 "Zähne"
* #s3200 ^property[0].code = #parent 
* #s3200 ^property[0].valueCode = #s320 
* #s3201 "Zahnfleisch"
* #s3201 ^property[0].code = #parent 
* #s3201 ^property[0].valueCode = #s320 
* #s3202 "Struktur des Gaumens"
* #s3202 ^property[0].code = #parent 
* #s3202 ^property[0].valueCode = #s320 
* #s3202 ^property[1].code = #child 
* #s3202 ^property[1].valueCode = #s32020 
* #s3202 ^property[2].code = #child 
* #s3202 ^property[2].valueCode = #s32021 
* #s32020 "Harter Gaumen"
* #s32020 ^property[0].code = #parent 
* #s32020 ^property[0].valueCode = #s3202 
* #s32021 "Gaumensegel"
* #s32021 ^property[0].code = #parent 
* #s32021 ^property[0].valueCode = #s3202 
* #s3203 "Zunge"
* #s3203 ^property[0].code = #parent 
* #s3203 ^property[0].valueCode = #s320 
* #s3204 "Struktur der Lippen"
* #s3204 ^property[0].code = #parent 
* #s3204 ^property[0].valueCode = #s320 
* #s3204 ^property[1].code = #child 
* #s3204 ^property[1].valueCode = #s32040 
* #s3204 ^property[2].code = #child 
* #s3204 ^property[2].valueCode = #s32041 
* #s32040 "Oberlippe"
* #s32040 ^property[0].code = #parent 
* #s32040 ^property[0].valueCode = #s3204 
* #s32041 "Unterlippe"
* #s32041 ^property[0].code = #parent 
* #s32041 ^property[0].valueCode = #s3204 
* #s3208 "Struktur des Mundes, anders bezeichnet"
* #s3208 ^property[0].code = #parent 
* #s3208 ^property[0].valueCode = #s320 
* #s3209 "Struktur des Mundes, nicht näher bezeichnet"
* #s3209 ^property[0].code = #parent 
* #s3209 ^property[0].valueCode = #s320 
* #s330 "Struktur des Pharynx"
* #s330 ^property[0].code = #parent 
* #s330 ^property[0].valueCode = #s3 
* #s330 ^property[1].code = #child 
* #s330 ^property[1].valueCode = #s3300 
* #s330 ^property[2].code = #child 
* #s330 ^property[2].valueCode = #s3301 
* #s330 ^property[3].code = #child 
* #s330 ^property[3].valueCode = #s3308 
* #s330 ^property[4].code = #child 
* #s330 ^property[4].valueCode = #s3309 
* #s3300 "Nasopharynx"
* #s3300 ^property[0].code = #parent 
* #s3300 ^property[0].valueCode = #s330 
* #s3301 "Oropharynx"
* #s3301 ^property[0].code = #parent 
* #s3301 ^property[0].valueCode = #s330 
* #s3308 "Struktur des Pharynx, anders bezeichnet"
* #s3308 ^property[0].code = #parent 
* #s3308 ^property[0].valueCode = #s330 
* #s3309 "Struktur des Pharynx, nicht näher bezeichnet"
* #s3309 ^property[0].code = #parent 
* #s3309 ^property[0].valueCode = #s330 
* #s340 "Struktur des Kehlkopfes"
* #s340 ^property[0].code = #parent 
* #s340 ^property[0].valueCode = #s3 
* #s340 ^property[1].code = #child 
* #s340 ^property[1].valueCode = #s3400 
* #s340 ^property[2].code = #child 
* #s340 ^property[2].valueCode = #s3408 
* #s340 ^property[3].code = #child 
* #s340 ^property[3].valueCode = #s3409 
* #s3400 "Stimmbänder"
* #s3400 ^property[0].code = #parent 
* #s3400 ^property[0].valueCode = #s340 
* #s3408 "Struktur des Kehlkopfes, anders bezeichnet"
* #s3408 ^property[0].code = #parent 
* #s3408 ^property[0].valueCode = #s340 
* #s3409 "Struktur des Kehlkopfes, nicht näher bezeichnet"
* #s3409 ^property[0].code = #parent 
* #s3409 ^property[0].valueCode = #s340 
* #s398 "Strukturen, die an der Stimme und am Sprechen beteiligt sind, anders bezeichnet"
* #s398 ^property[0].code = #parent 
* #s398 ^property[0].valueCode = #s3 
* #s399 "Strukturen, die an der Stimme und am Sprechen beteiligt sind, nicht näher bezeichnet"
* #s399 ^property[0].code = #parent 
* #s399 ^property[0].valueCode = #s3 
* #s4 "Strukturen des kardiovaskulären, des Immun- und des Atmungssystems"
* #s4 ^property[0].code = #parent 
* #s4 ^property[0].valueCode = #s 
* #s4 ^property[1].code = #child 
* #s4 ^property[1].valueCode = #s410 
* #s4 ^property[2].code = #child 
* #s4 ^property[2].valueCode = #s420 
* #s4 ^property[3].code = #child 
* #s4 ^property[3].valueCode = #s430 
* #s4 ^property[4].code = #child 
* #s4 ^property[4].valueCode = #s498 
* #s4 ^property[5].code = #child 
* #s4 ^property[5].valueCode = #s499 
* #s410 "Struktur des kardiovaskulären Systems"
* #s410 ^property[0].code = #parent 
* #s410 ^property[0].valueCode = #s4 
* #s410 ^property[1].code = #child 
* #s410 ^property[1].valueCode = #s4100 
* #s410 ^property[2].code = #child 
* #s410 ^property[2].valueCode = #s4101 
* #s410 ^property[3].code = #child 
* #s410 ^property[3].valueCode = #s4102 
* #s410 ^property[4].code = #child 
* #s410 ^property[4].valueCode = #s4103 
* #s410 ^property[5].code = #child 
* #s410 ^property[5].valueCode = #s4108 
* #s410 ^property[6].code = #child 
* #s410 ^property[6].valueCode = #s4109 
* #s4100 "Herz"
* #s4100 ^property[0].code = #parent 
* #s4100 ^property[0].valueCode = #s410 
* #s4100 ^property[1].code = #child 
* #s4100 ^property[1].valueCode = #s41000 
* #s4100 ^property[2].code = #child 
* #s4100 ^property[2].valueCode = #s41001 
* #s4100 ^property[3].code = #child 
* #s4100 ^property[3].valueCode = #s41008 
* #s4100 ^property[4].code = #child 
* #s4100 ^property[4].valueCode = #s41009 
* #s41000 "Vorhöfe"
* #s41000 ^property[0].code = #parent 
* #s41000 ^property[0].valueCode = #s4100 
* #s41001 "Ventrikel"
* #s41001 ^property[0].code = #parent 
* #s41001 ^property[0].valueCode = #s4100 
* #s41008 "Struktur des Herzens, anders bezeichnet"
* #s41008 ^property[0].code = #parent 
* #s41008 ^property[0].valueCode = #s4100 
* #s41009 "Struktur des Herzens, nicht näher bezeichnet"
* #s41009 ^property[0].code = #parent 
* #s41009 ^property[0].valueCode = #s4100 
* #s4101 "Arterien"
* #s4101 ^property[0].code = #parent 
* #s4101 ^property[0].valueCode = #s410 
* #s4102 "Venen"
* #s4102 ^property[0].code = #parent 
* #s4102 ^property[0].valueCode = #s410 
* #s4103 "Kapillaren"
* #s4103 ^property[0].code = #parent 
* #s4103 ^property[0].valueCode = #s410 
* #s4108 "Struktur des kardiovaskulären Systems, anders bezeichnet"
* #s4108 ^property[0].code = #parent 
* #s4108 ^property[0].valueCode = #s410 
* #s4109 "Struktur des kardiovaskulären Systems, nicht näher bezeichnet"
* #s4109 ^property[0].code = #parent 
* #s4109 ^property[0].valueCode = #s410 
* #s420 "Struktur des Immunsystems"
* #s420 ^property[0].code = #parent 
* #s420 ^property[0].valueCode = #s4 
* #s420 ^property[1].code = #child 
* #s420 ^property[1].valueCode = #s4200 
* #s420 ^property[2].code = #child 
* #s420 ^property[2].valueCode = #s4201 
* #s420 ^property[3].code = #child 
* #s420 ^property[3].valueCode = #s4202 
* #s420 ^property[4].code = #child 
* #s420 ^property[4].valueCode = #s4203 
* #s420 ^property[5].code = #child 
* #s420 ^property[5].valueCode = #s4204 
* #s420 ^property[6].code = #child 
* #s420 ^property[6].valueCode = #s4208 
* #s420 ^property[7].code = #child 
* #s420 ^property[7].valueCode = #s4209 
* #s4200 "Lymphgefäße"
* #s4200 ^property[0].code = #parent 
* #s4200 ^property[0].valueCode = #s420 
* #s4201 "Lymphknoten"
* #s4201 ^property[0].code = #parent 
* #s4201 ^property[0].valueCode = #s420 
* #s4202 "Thymus"
* #s4202 ^property[0].code = #parent 
* #s4202 ^property[0].valueCode = #s420 
* #s4203 "Milz"
* #s4203 ^property[0].code = #parent 
* #s4203 ^property[0].valueCode = #s420 
* #s4204 "Knochenmark"
* #s4204 ^property[0].code = #parent 
* #s4204 ^property[0].valueCode = #s420 
* #s4208 "Struktur des Immunsystems, anders bezeichnet"
* #s4208 ^property[0].code = #parent 
* #s4208 ^property[0].valueCode = #s420 
* #s4209 "Struktur des Immunsystems, nicht näher bezeichnet"
* #s4209 ^property[0].code = #parent 
* #s4209 ^property[0].valueCode = #s420 
* #s430 "Struktur des Atmungssystems"
* #s430 ^property[0].code = #parent 
* #s430 ^property[0].valueCode = #s4 
* #s430 ^property[1].code = #child 
* #s430 ^property[1].valueCode = #s4300 
* #s430 ^property[2].code = #child 
* #s430 ^property[2].valueCode = #s4301 
* #s430 ^property[3].code = #child 
* #s430 ^property[3].valueCode = #s4302 
* #s430 ^property[4].code = #child 
* #s430 ^property[4].valueCode = #s4303 
* #s430 ^property[5].code = #child 
* #s430 ^property[5].valueCode = #s4308 
* #s430 ^property[6].code = #child 
* #s430 ^property[6].valueCode = #s4309 
* #s4300 "Trachea"
* #s4300 ^property[0].code = #parent 
* #s4300 ^property[0].valueCode = #s430 
* #s4301 "Lunge"
* #s4301 ^property[0].code = #parent 
* #s4301 ^property[0].valueCode = #s430 
* #s4301 ^property[1].code = #child 
* #s4301 ^property[1].valueCode = #s43010 
* #s4301 ^property[2].code = #child 
* #s4301 ^property[2].valueCode = #s43011 
* #s4301 ^property[3].code = #child 
* #s4301 ^property[3].valueCode = #s43018 
* #s4301 ^property[4].code = #child 
* #s4301 ^property[4].valueCode = #s43019 
* #s43010 "Bronchialbaum"
* #s43010 ^property[0].code = #parent 
* #s43010 ^property[0].valueCode = #s4301 
* #s43011 "Alveolen"
* #s43011 ^property[0].code = #parent 
* #s43011 ^property[0].valueCode = #s4301 
* #s43018 "Struktur der Lungen, anders bezeichnet"
* #s43018 ^property[0].code = #parent 
* #s43018 ^property[0].valueCode = #s4301 
* #s43019 "Struktur der Lungen, nicht näher bezeichnet"
* #s43019 ^property[0].code = #parent 
* #s43019 ^property[0].valueCode = #s4301 
* #s4302 "Brustkorb"
* #s4302 ^property[0].code = #parent 
* #s4302 ^property[0].valueCode = #s430 
* #s4303 "Atemmuskulatur"
* #s4303 ^property[0].code = #parent 
* #s4303 ^property[0].valueCode = #s430 
* #s4303 ^property[1].code = #child 
* #s4303 ^property[1].valueCode = #s43030 
* #s4303 ^property[2].code = #child 
* #s4303 ^property[2].valueCode = #s43031 
* #s4303 ^property[3].code = #child 
* #s4303 ^property[3].valueCode = #s43038 
* #s4303 ^property[4].code = #child 
* #s4303 ^property[4].valueCode = #s43039 
* #s43030 "Interkostalmuskulatur"
* #s43030 ^property[0].code = #parent 
* #s43030 ^property[0].valueCode = #s4303 
* #s43031 "Zwerchfell"
* #s43031 ^property[0].code = #parent 
* #s43031 ^property[0].valueCode = #s4303 
* #s43038 "Atemmuskeln, anders bezeichnet"
* #s43038 ^property[0].code = #parent 
* #s43038 ^property[0].valueCode = #s4303 
* #s43039 "Atemmuskeln, nicht näher bezeichnet"
* #s43039 ^property[0].code = #parent 
* #s43039 ^property[0].valueCode = #s4303 
* #s4308 "Struktur des Atmungssystems, anders bezeichnet"
* #s4308 ^property[0].code = #parent 
* #s4308 ^property[0].valueCode = #s430 
* #s4309 "Struktur des Atmungssystems, nicht näher bezeichnet"
* #s4309 ^property[0].code = #parent 
* #s4309 ^property[0].valueCode = #s430 
* #s498 "Strukturen des kardiovaskulären, des Immun- und des Atmungssystems, anders bezeichnet"
* #s498 ^property[0].code = #parent 
* #s498 ^property[0].valueCode = #s4 
* #s499 "Strukturen des kardiovaskulären, des Immun- und des Atmungssystems, nicht näher bezeichnet"
* #s499 ^property[0].code = #parent 
* #s499 ^property[0].valueCode = #s4 
* #s5 "Mit dem Verdauungs-, Stoffwechsel und endokrinen System in Zusammenhang stehende Strukturen"
* #s5 ^property[0].code = #parent 
* #s5 ^property[0].valueCode = #s 
* #s5 ^property[1].code = #child 
* #s5 ^property[1].valueCode = #s510 
* #s5 ^property[2].code = #child 
* #s5 ^property[2].valueCode = #s520 
* #s5 ^property[3].code = #child 
* #s5 ^property[3].valueCode = #s530 
* #s5 ^property[4].code = #child 
* #s5 ^property[4].valueCode = #s540 
* #s5 ^property[5].code = #child 
* #s5 ^property[5].valueCode = #s550 
* #s5 ^property[6].code = #child 
* #s5 ^property[6].valueCode = #s560 
* #s5 ^property[7].code = #child 
* #s5 ^property[7].valueCode = #s570 
* #s5 ^property[8].code = #child 
* #s5 ^property[8].valueCode = #s580 
* #s5 ^property[9].code = #child 
* #s5 ^property[9].valueCode = #s598 
* #s5 ^property[10].code = #child 
* #s5 ^property[10].valueCode = #s599 
* #s510 "Struktur der Speicheldrüsen"
* #s510 ^property[0].code = #parent 
* #s510 ^property[0].valueCode = #s5 
* #s520 "Struktur der Speiseröhre"
* #s520 ^property[0].code = #parent 
* #s520 ^property[0].valueCode = #s5 
* #s530 "Struktur des Magens"
* #s530 ^property[0].code = #parent 
* #s530 ^property[0].valueCode = #s5 
* #s540 "Struktur des Darms"
* #s540 ^property[0].code = #parent 
* #s540 ^property[0].valueCode = #s5 
* #s540 ^property[1].code = #child 
* #s540 ^property[1].valueCode = #s5400 
* #s540 ^property[2].code = #child 
* #s540 ^property[2].valueCode = #s5401 
* #s540 ^property[3].code = #child 
* #s540 ^property[3].valueCode = #s5408 
* #s540 ^property[4].code = #child 
* #s540 ^property[4].valueCode = #s5409 
* #s5400 "Dünndarm"
* #s5400 ^property[0].code = #parent 
* #s5400 ^property[0].valueCode = #s540 
* #s5401 "Dickdarm"
* #s5401 ^property[0].code = #parent 
* #s5401 ^property[0].valueCode = #s540 
* #s5408 "Struktur des Darms, anders bezeichnet"
* #s5408 ^property[0].code = #parent 
* #s5408 ^property[0].valueCode = #s540 
* #s5409 "Struktur des Darms, nicht näher bezeichnet"
* #s5409 ^property[0].code = #parent 
* #s5409 ^property[0].valueCode = #s540 
* #s550 "Struktur der Bauchspeicheldrüse"
* #s550 ^property[0].code = #parent 
* #s550 ^property[0].valueCode = #s5 
* #s560 "Struktur der Leber"
* #s560 ^property[0].code = #parent 
* #s560 ^property[0].valueCode = #s5 
* #s570 "Struktur der Gallenwege"
* #s570 ^property[0].code = #parent 
* #s570 ^property[0].valueCode = #s5 
* #s580 "Struktur der endokrinen Drüsen"
* #s580 ^property[0].code = #parent 
* #s580 ^property[0].valueCode = #s5 
* #s580 ^property[1].code = #child 
* #s580 ^property[1].valueCode = #s5800 
* #s580 ^property[2].code = #child 
* #s580 ^property[2].valueCode = #s5801 
* #s580 ^property[3].code = #child 
* #s580 ^property[3].valueCode = #s5802 
* #s580 ^property[4].code = #child 
* #s580 ^property[4].valueCode = #s5803 
* #s580 ^property[5].code = #child 
* #s580 ^property[5].valueCode = #s5808 
* #s580 ^property[6].code = #child 
* #s580 ^property[6].valueCode = #s5809 
* #s5800 "Hypophyse"
* #s5800 ^property[0].code = #parent 
* #s5800 ^property[0].valueCode = #s580 
* #s5801 "Schilddrüse"
* #s5801 ^property[0].code = #parent 
* #s5801 ^property[0].valueCode = #s580 
* #s5802 "Nebenschilddrüse"
* #s5802 ^property[0].code = #parent 
* #s5802 ^property[0].valueCode = #s580 
* #s5803 "Nebenniere"
* #s5803 ^property[0].code = #parent 
* #s5803 ^property[0].valueCode = #s580 
* #s5808 "Struktur der endokrinen Drüsen, anders bezeichnet"
* #s5808 ^property[0].code = #parent 
* #s5808 ^property[0].valueCode = #s580 
* #s5809 "Struktur der endokrinen Drüsen, nicht näher bezeichnet"
* #s5809 ^property[0].code = #parent 
* #s5809 ^property[0].valueCode = #s580 
* #s598 "Mit dem Verdauungs-, Stoffwechsel- und endokrinen System in Zusammenhang stehende Strukturen, anders bezeichnet"
* #s598 ^property[0].code = #parent 
* #s598 ^property[0].valueCode = #s5 
* #s599 "Mit dem Verdauungs-, Stoffwechsel- und endokrinen System in Zusammenhang stehende Strukturen, nicht näher bezeichnet"
* #s599 ^property[0].code = #parent 
* #s599 ^property[0].valueCode = #s5 
* #s6 "Mit dem Urogenital- und dem Reproduktionssystem in Zusammenhang stehende Strukturen"
* #s6 ^property[0].code = #parent 
* #s6 ^property[0].valueCode = #s 
* #s6 ^property[1].code = #child 
* #s6 ^property[1].valueCode = #s610 
* #s6 ^property[2].code = #child 
* #s6 ^property[2].valueCode = #s620 
* #s6 ^property[3].code = #child 
* #s6 ^property[3].valueCode = #s630 
* #s6 ^property[4].code = #child 
* #s6 ^property[4].valueCode = #s698 
* #s6 ^property[5].code = #child 
* #s6 ^property[5].valueCode = #s699 
* #s610 "Struktur der ableitenden Harnwege"
* #s610 ^property[0].code = #parent 
* #s610 ^property[0].valueCode = #s6 
* #s610 ^property[1].code = #child 
* #s610 ^property[1].valueCode = #s6100 
* #s610 ^property[2].code = #child 
* #s610 ^property[2].valueCode = #s6101 
* #s610 ^property[3].code = #child 
* #s610 ^property[3].valueCode = #s6102 
* #s610 ^property[4].code = #child 
* #s610 ^property[4].valueCode = #s6103 
* #s610 ^property[5].code = #child 
* #s610 ^property[5].valueCode = #s6108 
* #s610 ^property[6].code = #child 
* #s610 ^property[6].valueCode = #s6109 
* #s6100 "Niere"
* #s6100 ^property[0].code = #parent 
* #s6100 ^property[0].valueCode = #s610 
* #s6101 "Harnleiter"
* #s6101 ^property[0].code = #parent 
* #s6101 ^property[0].valueCode = #s610 
* #s6102 "Harnblase"
* #s6102 ^property[0].code = #parent 
* #s6102 ^property[0].valueCode = #s610 
* #s6103 "Harnröhre"
* #s6103 ^property[0].code = #parent 
* #s6103 ^property[0].valueCode = #s610 
* #s6108 "Struktur der ableitenden Harnwege, anders bezeichnet"
* #s6108 ^property[0].code = #parent 
* #s6108 ^property[0].valueCode = #s610 
* #s6109 "Struktur der ableitenden Harnwege, nicht näher bezeichnet"
* #s6109 ^property[0].code = #parent 
* #s6109 ^property[0].valueCode = #s610 
* #s620 "Struktur des Beckenbodens"
* #s620 ^property[0].code = #parent 
* #s620 ^property[0].valueCode = #s6 
* #s630 "Struktur der Geschlechtsorgane"
* #s630 ^property[0].code = #parent 
* #s630 ^property[0].valueCode = #s6 
* #s630 ^property[1].code = #child 
* #s630 ^property[1].valueCode = #s6300 
* #s630 ^property[2].code = #child 
* #s630 ^property[2].valueCode = #s6301 
* #s630 ^property[3].code = #child 
* #s630 ^property[3].valueCode = #s6302 
* #s630 ^property[4].code = #child 
* #s630 ^property[4].valueCode = #s6303 
* #s630 ^property[5].code = #child 
* #s630 ^property[5].valueCode = #s6304 
* #s630 ^property[6].code = #child 
* #s630 ^property[6].valueCode = #s6305 
* #s630 ^property[7].code = #child 
* #s630 ^property[7].valueCode = #s6306 
* #s630 ^property[8].code = #child 
* #s630 ^property[8].valueCode = #s6308 
* #s630 ^property[9].code = #child 
* #s630 ^property[9].valueCode = #s6309 
* #s6300 "Eierstöcke"
* #s6300 ^property[0].code = #parent 
* #s6300 ^property[0].valueCode = #s630 
* #s6301 "Gebärmutter"
* #s6301 ^property[0].code = #parent 
* #s6301 ^property[0].valueCode = #s630 
* #s6301 ^property[1].code = #child 
* #s6301 ^property[1].valueCode = #s63010 
* #s6301 ^property[2].code = #child 
* #s6301 ^property[2].valueCode = #s63011 
* #s6301 ^property[3].code = #child 
* #s6301 ^property[3].valueCode = #s63012 
* #s6301 ^property[4].code = #child 
* #s6301 ^property[4].valueCode = #s63018 
* #s6301 ^property[5].code = #child 
* #s6301 ^property[5].valueCode = #s63019 
* #s63010 "Gebärmutterkörper"
* #s63010 ^property[0].code = #parent 
* #s63010 ^property[0].valueCode = #s6301 
* #s63011 "Gebärmutterhals"
* #s63011 ^property[0].code = #parent 
* #s63011 ^property[0].valueCode = #s6301 
* #s63012 "Eileiter"
* #s63012 ^property[0].code = #parent 
* #s63012 ^property[0].valueCode = #s6301 
* #s63018 "Struktur der Gebärmutter, anders bezeichnet"
* #s63018 ^property[0].code = #parent 
* #s63018 ^property[0].valueCode = #s6301 
* #s63019 "Struktur der Gebärmutter, nicht näher bezeichnet"
* #s63019 ^property[0].code = #parent 
* #s63019 ^property[0].valueCode = #s6301 
* #s6302 "Brust und Brustwarzen"
* #s6302 ^property[0].code = #parent 
* #s6302 ^property[0].valueCode = #s630 
* #s6303 "Struktur der Vagina und Vulva"
* #s6303 ^property[0].code = #parent 
* #s6303 ^property[0].valueCode = #s630 
* #s6303 ^property[1].code = #child 
* #s6303 ^property[1].valueCode = #s63030 
* #s6303 ^property[2].code = #child 
* #s6303 ^property[2].valueCode = #s63031 
* #s6303 ^property[3].code = #child 
* #s6303 ^property[3].valueCode = #s63032 
* #s6303 ^property[4].code = #child 
* #s6303 ^property[4].valueCode = #s63033 
* #s63030 "Klitoris"
* #s63030 ^property[0].code = #parent 
* #s63030 ^property[0].valueCode = #s6303 
* #s63031 "Große Schamlippen (Labia majora)"
* #s63031 ^property[0].code = #parent 
* #s63031 ^property[0].valueCode = #s6303 
* #s63032 "Kleine Schamlippen (Labia minora)"
* #s63032 ^property[0].code = #parent 
* #s63032 ^property[0].valueCode = #s6303 
* #s63033 "Vagina"
* #s63033 ^property[0].code = #parent 
* #s63033 ^property[0].valueCode = #s6303 
* #s6304 "Hoden"
* #s6304 ^property[0].code = #parent 
* #s6304 ^property[0].valueCode = #s630 
* #s6305 "Struktur des Penis"
* #s6305 ^property[0].code = #parent 
* #s6305 ^property[0].valueCode = #s630 
* #s6305 ^property[1].code = #child 
* #s6305 ^property[1].valueCode = #s63050 
* #s6305 ^property[2].code = #child 
* #s6305 ^property[2].valueCode = #s63051 
* #s6305 ^property[3].code = #child 
* #s6305 ^property[3].valueCode = #s63058 
* #s6305 ^property[4].code = #child 
* #s6305 ^property[4].valueCode = #s63059 
* #s63050 "Glans penis"
* #s63050 ^property[0].code = #parent 
* #s63050 ^property[0].valueCode = #s6305 
* #s63051 "Penisschaft"
* #s63051 ^property[0].code = #parent 
* #s63051 ^property[0].valueCode = #s6305 
* #s63058 "Struktur des Penis, anders bezeichnet"
* #s63058 ^property[0].code = #parent 
* #s63058 ^property[0].valueCode = #s6305 
* #s63059 "Struktur des Penis, nicht näher bezeichnet"
* #s63059 ^property[0].code = #parent 
* #s63059 ^property[0].valueCode = #s6305 
* #s6306 "Prostata"
* #s6306 ^property[0].code = #parent 
* #s6306 ^property[0].valueCode = #s630 
* #s6308 "Strukturen der Geschlechtsorgane, anders bezeichnet"
* #s6308 ^property[0].code = #parent 
* #s6308 ^property[0].valueCode = #s630 
* #s6309 "Strukturen der Geschlechtsorgane, nicht näher bezeichnet"
* #s6309 ^property[0].code = #parent 
* #s6309 ^property[0].valueCode = #s630 
* #s698 "Strukturen im Zusammenhang mit dem Urogenitalsystem, anders bezeichnet"
* #s698 ^property[0].code = #parent 
* #s698 ^property[0].valueCode = #s6 
* #s699 "Strukturen im Zusammenhang mit dem Urogenitalsystem, nicht näher bezeichnet"
* #s699 ^property[0].code = #parent 
* #s699 ^property[0].valueCode = #s6 
* #s7 "Mit der Bewegung in Zusammenhang stehende Strukturen"
* #s7 ^property[0].code = #parent 
* #s7 ^property[0].valueCode = #s 
* #s7 ^property[1].code = #child 
* #s7 ^property[1].valueCode = #s710 
* #s7 ^property[2].code = #child 
* #s7 ^property[2].valueCode = #s720 
* #s7 ^property[3].code = #child 
* #s7 ^property[3].valueCode = #s730 
* #s7 ^property[4].code = #child 
* #s7 ^property[4].valueCode = #s740 
* #s7 ^property[5].code = #child 
* #s7 ^property[5].valueCode = #s750 
* #s7 ^property[6].code = #child 
* #s7 ^property[6].valueCode = #s760 
* #s7 ^property[7].code = #child 
* #s7 ^property[7].valueCode = #s770 
* #s7 ^property[8].code = #child 
* #s7 ^property[8].valueCode = #s798 
* #s7 ^property[9].code = #child 
* #s7 ^property[9].valueCode = #s799 
* #s710 "Struktur der Kopf- und Halsregion"
* #s710 ^property[0].code = #parent 
* #s710 ^property[0].valueCode = #s7 
* #s710 ^property[1].code = #child 
* #s710 ^property[1].valueCode = #s7100 
* #s710 ^property[2].code = #child 
* #s710 ^property[2].valueCode = #s7101 
* #s710 ^property[3].code = #child 
* #s710 ^property[3].valueCode = #s7102 
* #s710 ^property[4].code = #child 
* #s710 ^property[4].valueCode = #s7103 
* #s710 ^property[5].code = #child 
* #s710 ^property[5].valueCode = #s7104 
* #s710 ^property[6].code = #child 
* #s710 ^property[6].valueCode = #s7105 
* #s710 ^property[7].code = #child 
* #s710 ^property[7].valueCode = #s7108 
* #s710 ^property[8].code = #child 
* #s710 ^property[8].valueCode = #s7109 
* #s7100 "Schädelknochen"
* #s7100 ^property[0].code = #parent 
* #s7100 ^property[0].valueCode = #s710 
* #s7101 "Gesichtsknochen"
* #s7101 ^property[0].code = #parent 
* #s7101 ^property[0].valueCode = #s710 
* #s7102 "Knochen der Halsregion"
* #s7102 ^property[0].code = #parent 
* #s7102 ^property[0].valueCode = #s710 
* #s7103 "Gelenke des Kopfes und der Halsregion"
* #s7103 ^property[0].code = #parent 
* #s7103 ^property[0].valueCode = #s710 
* #s7104 "Muskeln des Kopfes und der Halsregion"
* #s7104 ^property[0].code = #parent 
* #s7104 ^property[0].valueCode = #s710 
* #s7105 "Bänder und Faszien des Kopfes und der Halsregion"
* #s7105 ^property[0].code = #parent 
* #s7105 ^property[0].valueCode = #s710 
* #s7108 "Struktur der Kopf- und Halsregion, anders bezeichnet"
* #s7108 ^property[0].code = #parent 
* #s7108 ^property[0].valueCode = #s710 
* #s7109 "Struktur der Kopf- und Halsregion, nicht näher bezeichnet"
* #s7109 ^property[0].code = #parent 
* #s7109 ^property[0].valueCode = #s710 
* #s720 "Struktur der Schulterregion"
* #s720 ^property[0].code = #parent 
* #s720 ^property[0].valueCode = #s7 
* #s720 ^property[1].code = #child 
* #s720 ^property[1].valueCode = #s7200 
* #s720 ^property[2].code = #child 
* #s720 ^property[2].valueCode = #s7201 
* #s720 ^property[3].code = #child 
* #s720 ^property[3].valueCode = #s7202 
* #s720 ^property[4].code = #child 
* #s720 ^property[4].valueCode = #s7203 
* #s720 ^property[5].code = #child 
* #s720 ^property[5].valueCode = #s7208 
* #s720 ^property[6].code = #child 
* #s720 ^property[6].valueCode = #s7209 
* #s7200 "Knochen der Schulterregion"
* #s7200 ^property[0].code = #parent 
* #s7200 ^property[0].valueCode = #s720 
* #s7201 "Gelenke der Schulterregion"
* #s7201 ^property[0].code = #parent 
* #s7201 ^property[0].valueCode = #s720 
* #s7202 "Muskeln der Schulterregion"
* #s7202 ^property[0].code = #parent 
* #s7202 ^property[0].valueCode = #s720 
* #s7203 "Bänder und Faszien der Schulterregion"
* #s7203 ^property[0].code = #parent 
* #s7203 ^property[0].valueCode = #s720 
* #s7208 "Struktur der Schulterregion, anders bezeichnet"
* #s7208 ^property[0].code = #parent 
* #s7208 ^property[0].valueCode = #s720 
* #s7209 "Struktur der Schulterregion, nicht näher bezeichnet"
* #s7209 ^property[0].code = #parent 
* #s7209 ^property[0].valueCode = #s720 
* #s730 "Struktur der oberen Extremitäten"
* #s730 ^property[0].code = #parent 
* #s730 ^property[0].valueCode = #s7 
* #s730 ^property[1].code = #child 
* #s730 ^property[1].valueCode = #s7300 
* #s730 ^property[2].code = #child 
* #s730 ^property[2].valueCode = #s7301 
* #s730 ^property[3].code = #child 
* #s730 ^property[3].valueCode = #s7302 
* #s730 ^property[4].code = #child 
* #s730 ^property[4].valueCode = #s7308 
* #s730 ^property[5].code = #child 
* #s730 ^property[5].valueCode = #s7309 
* #s7300 "Struktur des Oberarms"
* #s7300 ^property[0].code = #parent 
* #s7300 ^property[0].valueCode = #s730 
* #s7300 ^property[1].code = #child 
* #s7300 ^property[1].valueCode = #s73000 
* #s7300 ^property[2].code = #child 
* #s7300 ^property[2].valueCode = #s73001 
* #s7300 ^property[3].code = #child 
* #s7300 ^property[3].valueCode = #s73002 
* #s7300 ^property[4].code = #child 
* #s7300 ^property[4].valueCode = #s73003 
* #s7300 ^property[5].code = #child 
* #s7300 ^property[5].valueCode = #s73008 
* #s7300 ^property[6].code = #child 
* #s7300 ^property[6].valueCode = #s73009 
* #s73000 "Knochen des Oberarms"
* #s73000 ^property[0].code = #parent 
* #s73000 ^property[0].valueCode = #s7300 
* #s73001 "Ellbogengelenk"
* #s73001 ^property[0].code = #parent 
* #s73001 ^property[0].valueCode = #s7300 
* #s73002 "Muskeln des Oberarms"
* #s73002 ^property[0].code = #parent 
* #s73002 ^property[0].valueCode = #s7300 
* #s73003 "Bänder und Faszien des Oberarms"
* #s73003 ^property[0].code = #parent 
* #s73003 ^property[0].valueCode = #s7300 
* #s73008 "Struktur des Oberarms, anders bezeichnet"
* #s73008 ^property[0].code = #parent 
* #s73008 ^property[0].valueCode = #s7300 
* #s73009 "Struktur des Oberarms, nicht näher bezeichnet"
* #s73009 ^property[0].code = #parent 
* #s73009 ^property[0].valueCode = #s7300 
* #s7301 "Struktur des Unterarms"
* #s7301 ^property[0].code = #parent 
* #s7301 ^property[0].valueCode = #s730 
* #s7301 ^property[1].code = #child 
* #s7301 ^property[1].valueCode = #s73010 
* #s7301 ^property[2].code = #child 
* #s7301 ^property[2].valueCode = #s73011 
* #s7301 ^property[3].code = #child 
* #s7301 ^property[3].valueCode = #s73012 
* #s7301 ^property[4].code = #child 
* #s7301 ^property[4].valueCode = #s73013 
* #s7301 ^property[5].code = #child 
* #s7301 ^property[5].valueCode = #s73018 
* #s7301 ^property[6].code = #child 
* #s7301 ^property[6].valueCode = #s73019 
* #s73010 "Knochen des Unterarms"
* #s73010 ^property[0].code = #parent 
* #s73010 ^property[0].valueCode = #s7301 
* #s73011 "Handgelenk"
* #s73011 ^property[0].code = #parent 
* #s73011 ^property[0].valueCode = #s7301 
* #s73012 "Muskeln des Unterarms"
* #s73012 ^property[0].code = #parent 
* #s73012 ^property[0].valueCode = #s7301 
* #s73013 "Bänder und Faszien des Unterarms"
* #s73013 ^property[0].code = #parent 
* #s73013 ^property[0].valueCode = #s7301 
* #s73018 "Struktur des Unterarms, anders bezeichnet"
* #s73018 ^property[0].code = #parent 
* #s73018 ^property[0].valueCode = #s7301 
* #s73019 "Struktur des Unterarms, nicht näher bezeichnet"
* #s73019 ^property[0].code = #parent 
* #s73019 ^property[0].valueCode = #s7301 
* #s7302 "Struktur der Hand"
* #s7302 ^property[0].code = #parent 
* #s7302 ^property[0].valueCode = #s730 
* #s7302 ^property[1].code = #child 
* #s7302 ^property[1].valueCode = #s73020 
* #s7302 ^property[2].code = #child 
* #s7302 ^property[2].valueCode = #s73021 
* #s7302 ^property[3].code = #child 
* #s7302 ^property[3].valueCode = #s73022 
* #s7302 ^property[4].code = #child 
* #s7302 ^property[4].valueCode = #s73023 
* #s7302 ^property[5].code = #child 
* #s7302 ^property[5].valueCode = #s73028 
* #s7302 ^property[6].code = #child 
* #s7302 ^property[6].valueCode = #s73029 
* #s73020 "Knochen der Hand"
* #s73020 ^property[0].code = #parent 
* #s73020 ^property[0].valueCode = #s7302 
* #s73021 "Gelenke der Hand und Finger"
* #s73021 ^property[0].code = #parent 
* #s73021 ^property[0].valueCode = #s7302 
* #s73022 "Muskeln der Hand"
* #s73022 ^property[0].code = #parent 
* #s73022 ^property[0].valueCode = #s7302 
* #s73023 "Bänder und Faszien der Hand"
* #s73023 ^property[0].code = #parent 
* #s73023 ^property[0].valueCode = #s7302 
* #s73028 "Struktur der Hand, anders bezeichnet"
* #s73028 ^property[0].code = #parent 
* #s73028 ^property[0].valueCode = #s7302 
* #s73029 "Struktur der Hand, nicht näher bezeichnet"
* #s73029 ^property[0].code = #parent 
* #s73029 ^property[0].valueCode = #s7302 
* #s7308 "Struktur der oberen Extremitäten, anders bezeichnet"
* #s7308 ^property[0].code = #parent 
* #s7308 ^property[0].valueCode = #s730 
* #s7309 "Struktur der oberen Extremitäten, nicht näher bezeichnet"
* #s7309 ^property[0].code = #parent 
* #s7309 ^property[0].valueCode = #s730 
* #s740 "Struktur der Beckenregion"
* #s740 ^property[0].code = #parent 
* #s740 ^property[0].valueCode = #s7 
* #s740 ^property[1].code = #child 
* #s740 ^property[1].valueCode = #s7400 
* #s740 ^property[2].code = #child 
* #s740 ^property[2].valueCode = #s7401 
* #s740 ^property[3].code = #child 
* #s740 ^property[3].valueCode = #s7402 
* #s740 ^property[4].code = #child 
* #s740 ^property[4].valueCode = #s7403 
* #s740 ^property[5].code = #child 
* #s740 ^property[5].valueCode = #s7408 
* #s740 ^property[6].code = #child 
* #s740 ^property[6].valueCode = #s7409 
* #s7400 "Knochen der Beckenregion"
* #s7400 ^property[0].code = #parent 
* #s7400 ^property[0].valueCode = #s740 
* #s7401 "Gelenke der Beckenregion"
* #s7401 ^property[0].code = #parent 
* #s7401 ^property[0].valueCode = #s740 
* #s7402 "Muskeln der Beckenregion"
* #s7402 ^property[0].code = #parent 
* #s7402 ^property[0].valueCode = #s740 
* #s7403 "Bänder und Faszien der Beckenregion"
* #s7403 ^property[0].code = #parent 
* #s7403 ^property[0].valueCode = #s740 
* #s7408 "Struktur der Beckenregion, anders bezeichnet"
* #s7408 ^property[0].code = #parent 
* #s7408 ^property[0].valueCode = #s740 
* #s7409 "Struktur der Beckenregion, nicht näher bezeichnet"
* #s7409 ^property[0].code = #parent 
* #s7409 ^property[0].valueCode = #s740 
* #s750 "Struktur der unteren Extremitäten"
* #s750 ^property[0].code = #parent 
* #s750 ^property[0].valueCode = #s7 
* #s750 ^property[1].code = #child 
* #s750 ^property[1].valueCode = #s7500 
* #s750 ^property[2].code = #child 
* #s750 ^property[2].valueCode = #s7501 
* #s750 ^property[3].code = #child 
* #s750 ^property[3].valueCode = #s7502 
* #s750 ^property[4].code = #child 
* #s750 ^property[4].valueCode = #s7508 
* #s750 ^property[5].code = #child 
* #s750 ^property[5].valueCode = #s7509 
* #s7500 "Struktur des Oberschenkels"
* #s7500 ^property[0].code = #parent 
* #s7500 ^property[0].valueCode = #s750 
* #s7500 ^property[1].code = #child 
* #s7500 ^property[1].valueCode = #s75000 
* #s7500 ^property[2].code = #child 
* #s7500 ^property[2].valueCode = #s75001 
* #s7500 ^property[3].code = #child 
* #s7500 ^property[3].valueCode = #s75002 
* #s7500 ^property[4].code = #child 
* #s7500 ^property[4].valueCode = #s75003 
* #s7500 ^property[5].code = #child 
* #s7500 ^property[5].valueCode = #s75008 
* #s7500 ^property[6].code = #child 
* #s7500 ^property[6].valueCode = #s75009 
* #s75000 "Knochen des Oberschenkels"
* #s75000 ^property[0].code = #parent 
* #s75000 ^property[0].valueCode = #s7500 
* #s75001 "Hüftgelenk"
* #s75001 ^property[0].code = #parent 
* #s75001 ^property[0].valueCode = #s7500 
* #s75002 "Muskeln des Oberschenkels"
* #s75002 ^property[0].code = #parent 
* #s75002 ^property[0].valueCode = #s7500 
* #s75003 "Bänder und Faszien des Oberschenkels"
* #s75003 ^property[0].code = #parent 
* #s75003 ^property[0].valueCode = #s7500 
* #s75008 "Struktur des Oberschenkels, anders bezeichnet"
* #s75008 ^property[0].code = #parent 
* #s75008 ^property[0].valueCode = #s7500 
* #s75009 "Struktur des Oberschenkels, nicht näher bezeichnet"
* #s75009 ^property[0].code = #parent 
* #s75009 ^property[0].valueCode = #s7500 
* #s7501 "Struktur des Unterschenkels"
* #s7501 ^property[0].code = #parent 
* #s7501 ^property[0].valueCode = #s750 
* #s7501 ^property[1].code = #child 
* #s7501 ^property[1].valueCode = #s75010 
* #s7501 ^property[2].code = #child 
* #s7501 ^property[2].valueCode = #s75011 
* #s7501 ^property[3].code = #child 
* #s7501 ^property[3].valueCode = #s75012 
* #s7501 ^property[4].code = #child 
* #s7501 ^property[4].valueCode = #s75013 
* #s7501 ^property[5].code = #child 
* #s7501 ^property[5].valueCode = #s75018 
* #s7501 ^property[6].code = #child 
* #s7501 ^property[6].valueCode = #s75019 
* #s75010 "Knochen des Unterschenkels"
* #s75010 ^property[0].code = #parent 
* #s75010 ^property[0].valueCode = #s7501 
* #s75011 "Kniegelenk"
* #s75011 ^property[0].code = #parent 
* #s75011 ^property[0].valueCode = #s7501 
* #s75012 "Muskeln des Unterschenkels"
* #s75012 ^property[0].code = #parent 
* #s75012 ^property[0].valueCode = #s7501 
* #s75013 "Bänder und Faszien des Unterschenkels"
* #s75013 ^property[0].code = #parent 
* #s75013 ^property[0].valueCode = #s7501 
* #s75018 "Struktur des Unterschenkels, anders bezeichnet"
* #s75018 ^property[0].code = #parent 
* #s75018 ^property[0].valueCode = #s7501 
* #s75019 "Struktur des Unterschenkels, nicht näher bezeichnet"
* #s75019 ^property[0].code = #parent 
* #s75019 ^property[0].valueCode = #s7501 
* #s7502 "Struktur der Knöchelregion und des Fußes"
* #s7502 ^property[0].code = #parent 
* #s7502 ^property[0].valueCode = #s750 
* #s7502 ^property[1].code = #child 
* #s7502 ^property[1].valueCode = #s75020 
* #s7502 ^property[2].code = #child 
* #s7502 ^property[2].valueCode = #s75021 
* #s7502 ^property[3].code = #child 
* #s7502 ^property[3].valueCode = #s75022 
* #s7502 ^property[4].code = #child 
* #s7502 ^property[4].valueCode = #s75023 
* #s7502 ^property[5].code = #child 
* #s7502 ^property[5].valueCode = #s75028 
* #s7502 ^property[6].code = #child 
* #s7502 ^property[6].valueCode = #s75029 
* #s75020 "Knochen der Knöchelregion und des Fußes"
* #s75020 ^property[0].code = #parent 
* #s75020 ^property[0].valueCode = #s7502 
* #s75021 "Sprunggelenk und Gelenke des Fußes und der Zehen"
* #s75021 ^property[0].code = #parent 
* #s75021 ^property[0].valueCode = #s7502 
* #s75022 "Muskeln der Knöchelregion und des Fußes"
* #s75022 ^property[0].code = #parent 
* #s75022 ^property[0].valueCode = #s7502 
* #s75023 "Bänder und Faszien der Knöchelregion und des Fußes"
* #s75023 ^property[0].code = #parent 
* #s75023 ^property[0].valueCode = #s7502 
* #s75028 "Struktur der Knöchelregion und des Fußes, anders bezeichnet"
* #s75028 ^property[0].code = #parent 
* #s75028 ^property[0].valueCode = #s7502 
* #s75029 "Struktur der Knöchelregion und des Fußes, nicht näher bezeichnet"
* #s75029 ^property[0].code = #parent 
* #s75029 ^property[0].valueCode = #s7502 
* #s7508 "Struktur der unteren Extremitäten, anders bezeichnet"
* #s7508 ^property[0].code = #parent 
* #s7508 ^property[0].valueCode = #s750 
* #s7509 "Struktur der unteren Extremitäten, nicht näher bezeichnet"
* #s7509 ^property[0].code = #parent 
* #s7509 ^property[0].valueCode = #s750 
* #s760 "Struktur des Rumpfes"
* #s760 ^property[0].code = #parent 
* #s760 ^property[0].valueCode = #s7 
* #s760 ^property[1].code = #child 
* #s760 ^property[1].valueCode = #s7600 
* #s760 ^property[2].code = #child 
* #s760 ^property[2].valueCode = #s7601 
* #s760 ^property[3].code = #child 
* #s760 ^property[3].valueCode = #s7602 
* #s760 ^property[4].code = #child 
* #s760 ^property[4].valueCode = #s7608 
* #s760 ^property[5].code = #child 
* #s760 ^property[5].valueCode = #s7609 
* #s7600 "Struktur der Wirbelsäule"
* #s7600 ^property[0].code = #parent 
* #s7600 ^property[0].valueCode = #s760 
* #s7600 ^property[1].code = #child 
* #s7600 ^property[1].valueCode = #s76000 
* #s7600 ^property[2].code = #child 
* #s7600 ^property[2].valueCode = #s76001 
* #s7600 ^property[3].code = #child 
* #s7600 ^property[3].valueCode = #s76002 
* #s7600 ^property[4].code = #child 
* #s7600 ^property[4].valueCode = #s76003 
* #s7600 ^property[5].code = #child 
* #s7600 ^property[5].valueCode = #s76004 
* #s7600 ^property[6].code = #child 
* #s7600 ^property[6].valueCode = #s76008 
* #s7600 ^property[7].code = #child 
* #s7600 ^property[7].valueCode = #s76009 
* #s76000 "Halswirbelsäule"
* #s76000 ^property[0].code = #parent 
* #s76000 ^property[0].valueCode = #s7600 
* #s76001 "Brustwirbelsäule"
* #s76001 ^property[0].code = #parent 
* #s76001 ^property[0].valueCode = #s7600 
* #s76002 "Lendenwirbelsäule"
* #s76002 ^property[0].code = #parent 
* #s76002 ^property[0].valueCode = #s7600 
* #s76003 "Kreuzbein"
* #s76003 ^property[0].code = #parent 
* #s76003 ^property[0].valueCode = #s7600 
* #s76004 "Steißbein"
* #s76004 ^property[0].code = #parent 
* #s76004 ^property[0].valueCode = #s7600 
* #s76008 "Struktur der Wirbelsäule, anders bezeichnet"
* #s76008 ^property[0].code = #parent 
* #s76008 ^property[0].valueCode = #s7600 
* #s76009 "Struktur der Wirbelsäule, nicht näher bezeichnet"
* #s76009 ^property[0].code = #parent 
* #s76009 ^property[0].valueCode = #s7600 
* #s7601 "Muskeln des Rumpfes"
* #s7601 ^property[0].code = #parent 
* #s7601 ^property[0].valueCode = #s760 
* #s7602 "Bänder und Faszien des Rumpfes"
* #s7602 ^property[0].code = #parent 
* #s7602 ^property[0].valueCode = #s760 
* #s7608 "Struktur des Rumpfes, anders bezeichnet"
* #s7608 ^property[0].code = #parent 
* #s7608 ^property[0].valueCode = #s760 
* #s7609 "Struktur des Rumpfes, nicht näher bezeichnet"
* #s7609 ^property[0].code = #parent 
* #s7609 ^property[0].valueCode = #s760 
* #s770 "Weitere mit der Bewegung in Zusammenhang stehende muskuloskeletale Strukturen"
* #s770 ^property[0].code = #parent 
* #s770 ^property[0].valueCode = #s7 
* #s770 ^property[1].code = #child 
* #s770 ^property[1].valueCode = #s7700 
* #s770 ^property[2].code = #child 
* #s770 ^property[2].valueCode = #s7701 
* #s770 ^property[3].code = #child 
* #s770 ^property[3].valueCode = #s7702 
* #s770 ^property[4].code = #child 
* #s770 ^property[4].valueCode = #s7703 
* #s770 ^property[5].code = #child 
* #s770 ^property[5].valueCode = #s7708 
* #s770 ^property[6].code = #child 
* #s770 ^property[6].valueCode = #s7709 
* #s7700 "Knochen"
* #s7700 ^property[0].code = #parent 
* #s7700 ^property[0].valueCode = #s770 
* #s7701 "Gelenke"
* #s7701 ^property[0].code = #parent 
* #s7701 ^property[0].valueCode = #s770 
* #s7702 "Muskeln"
* #s7702 ^property[0].code = #parent 
* #s7702 ^property[0].valueCode = #s770 
* #s7703 "Extraartikuläre Bänder, Faszien, Aponeurosen, Retinakula, Septen, Bursen, nicht näher bezeichnet"
* #s7703 ^property[0].code = #parent 
* #s7703 ^property[0].valueCode = #s770 
* #s7708 "Weitere mit der Bewegung in Zusammenhang stehende muskuloskeletale Strukturen, anders bezeichnet"
* #s7708 ^property[0].code = #parent 
* #s7708 ^property[0].valueCode = #s770 
* #s7709 "Weitere mit der Bewegung in Zusammenhang stehende muskuloskeletale Strukturen, nicht näher bezeichnet"
* #s7709 ^property[0].code = #parent 
* #s7709 ^property[0].valueCode = #s770 
* #s798 "Strukturen im Zusammenhang mit der Bewegung, anders bezeichnet"
* #s798 ^property[0].code = #parent 
* #s798 ^property[0].valueCode = #s7 
* #s799 "Strukturen im Zusammenhang mit der Bewegung, nicht näher bezeichnet"
* #s799 ^property[0].code = #parent 
* #s799 ^property[0].valueCode = #s7 
* #s8 "Strukturen der Haut und Hautanhangsgebilde"
* #s8 ^property[0].code = #parent 
* #s8 ^property[0].valueCode = #s 
* #s8 ^property[1].code = #child 
* #s8 ^property[1].valueCode = #s810 
* #s8 ^property[2].code = #child 
* #s8 ^property[2].valueCode = #s820 
* #s8 ^property[3].code = #child 
* #s8 ^property[3].valueCode = #s830 
* #s8 ^property[4].code = #child 
* #s8 ^property[4].valueCode = #s840 
* #s8 ^property[5].code = #child 
* #s8 ^property[5].valueCode = #s898 
* #s8 ^property[6].code = #child 
* #s8 ^property[6].valueCode = #s899 
* #s810 "Struktur der Hautregionen"
* #s810 ^property[0].code = #parent 
* #s810 ^property[0].valueCode = #s8 
* #s810 ^property[1].code = #child 
* #s810 ^property[1].valueCode = #s8100 
* #s810 ^property[2].code = #child 
* #s810 ^property[2].valueCode = #s8101 
* #s810 ^property[3].code = #child 
* #s810 ^property[3].valueCode = #s8102 
* #s810 ^property[4].code = #child 
* #s810 ^property[4].valueCode = #s8103 
* #s810 ^property[5].code = #child 
* #s810 ^property[5].valueCode = #s8104 
* #s810 ^property[6].code = #child 
* #s810 ^property[6].valueCode = #s8105 
* #s810 ^property[7].code = #child 
* #s810 ^property[7].valueCode = #s8108 
* #s810 ^property[8].code = #child 
* #s810 ^property[8].valueCode = #s8109 
* #s8100 "Haut des Kopfes und der Halsregion"
* #s8100 ^property[0].code = #parent 
* #s8100 ^property[0].valueCode = #s810 
* #s8101 "Haut der Schulterregion"
* #s8101 ^property[0].code = #parent 
* #s8101 ^property[0].valueCode = #s810 
* #s8102 "Haut der oberen Extremitäten"
* #s8102 ^property[0].code = #parent 
* #s8102 ^property[0].valueCode = #s810 
* #s8103 "Haut der Beckenregion"
* #s8103 ^property[0].code = #parent 
* #s8103 ^property[0].valueCode = #s810 
* #s8104 "Haut der unteren Extremitäten"
* #s8104 ^property[0].code = #parent 
* #s8104 ^property[0].valueCode = #s810 
* #s8105 "Haut des Körperstammes und des Rückens"
* #s8105 ^property[0].code = #parent 
* #s8105 ^property[0].valueCode = #s810 
* #s8108 "Struktur der Hautregionen, anders bezeichnet"
* #s8108 ^property[0].code = #parent 
* #s8108 ^property[0].valueCode = #s810 
* #s8109 "Struktur der Hautregionen, nicht näher bezeichnet"
* #s8109 ^property[0].code = #parent 
* #s8109 ^property[0].valueCode = #s810 
* #s820 "Struktur der Hautanhangsgebilde"
* #s820 ^property[0].code = #parent 
* #s820 ^property[0].valueCode = #s8 
* #s820 ^property[1].code = #child 
* #s820 ^property[1].valueCode = #s8200 
* #s820 ^property[2].code = #child 
* #s820 ^property[2].valueCode = #s8201 
* #s820 ^property[3].code = #child 
* #s820 ^property[3].valueCode = #s8208 
* #s820 ^property[4].code = #child 
* #s820 ^property[4].valueCode = #s8209 
* #s8200 "Schweißdrüsen"
* #s8200 ^property[0].code = #parent 
* #s8200 ^property[0].valueCode = #s820 
* #s8201 "Talgdrüsen"
* #s8201 ^property[0].code = #parent 
* #s8201 ^property[0].valueCode = #s820 
* #s8208 "Struktur der Hautanhangsgebilde, anders bezeichnet"
* #s8208 ^property[0].code = #parent 
* #s8208 ^property[0].valueCode = #s820 
* #s8209 "Struktur der Hautanhangsgebilde, nicht näher bezeichnet"
* #s8209 ^property[0].code = #parent 
* #s8209 ^property[0].valueCode = #s820 
* #s830 "Struktur der Nägel"
* #s830 ^property[0].code = #parent 
* #s830 ^property[0].valueCode = #s8 
* #s830 ^property[1].code = #child 
* #s830 ^property[1].valueCode = #s8300 
* #s830 ^property[2].code = #child 
* #s830 ^property[2].valueCode = #s8301 
* #s830 ^property[3].code = #child 
* #s830 ^property[3].valueCode = #s8308 
* #s830 ^property[4].code = #child 
* #s830 ^property[4].valueCode = #s8309 
* #s8300 "Fingernägel"
* #s8300 ^property[0].code = #parent 
* #s8300 ^property[0].valueCode = #s830 
* #s8301 "Zehennägel"
* #s8301 ^property[0].code = #parent 
* #s8301 ^property[0].valueCode = #s830 
* #s8308 "Struktur der Nägel, anders bezeichnet"
* #s8308 ^property[0].code = #parent 
* #s8308 ^property[0].valueCode = #s830 
* #s8309 "Struktur der Nägel, nicht näher bezeichnet"
* #s8309 ^property[0].code = #parent 
* #s8309 ^property[0].valueCode = #s830 
* #s840 "Struktur der Haare"
* #s840 ^property[0].code = #parent 
* #s840 ^property[0].valueCode = #s8 
* #s898 "Strukturen im Zusammenhang mit der Haut, anders bezeichnet"
* #s898 ^property[0].code = #parent 
* #s898 ^property[0].valueCode = #s8 
* #s899 "Strukturen im Zusammenhang mit der Haut, nicht näher bezeichnet"
* #s899 ^property[0].code = #parent 
* #s899 ^property[0].valueCode = #s8 
