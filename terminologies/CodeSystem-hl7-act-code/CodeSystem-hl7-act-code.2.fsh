Instance: hl7-act-code 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-act-code" 
* name = "hl7-act-code" 
* title = "HL7 Act Code" 
* status = #active 
* content = #complete 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.5.4" 
* date = "2015-10-05" 
* count = 896 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #C "corrected"
* #DIET "Diet"
* #DIET ^property[0].code = #child 
* #DIET ^property[0].valueCode = #BR 
* #DIET ^property[1].code = #child 
* #DIET ^property[1].valueCode = #DM 
* #DIET ^property[2].code = #child 
* #DIET ^property[2].valueCode = #FAST 
* #DIET ^property[3].code = #child 
* #DIET ^property[3].valueCode = #GF 
* #DIET ^property[4].code = #child 
* #DIET ^property[4].valueCode = #LF 
* #DIET ^property[5].code = #child 
* #DIET ^property[5].valueCode = #LP 
* #DIET ^property[6].code = #child 
* #DIET ^property[6].valueCode = #LQ 
* #DIET ^property[7].code = #child 
* #DIET ^property[7].valueCode = #LS 
* #DIET ^property[8].code = #child 
* #DIET ^property[8].valueCode = #N 
* #DIET ^property[9].code = #child 
* #DIET ^property[9].valueCode = #NF 
* #DIET ^property[10].code = #child 
* #DIET ^property[10].valueCode = #PAF 
* #DIET ^property[11].code = #child 
* #DIET ^property[11].valueCode = #PAR 
* #DIET ^property[12].code = #child 
* #DIET ^property[12].valueCode = #RD 
* #DIET ^property[13].code = #child 
* #DIET ^property[13].valueCode = #SCH 
* #DIET ^property[14].code = #child 
* #DIET ^property[14].valueCode = #T 
* #DIET ^property[15].code = #child 
* #DIET ^property[15].valueCode = #VLI 
* #BR "breikost (GE)"
* #BR ^property[0].code = #parent 
* #BR ^property[0].valueCode = #DIET 
* #DM "diabetes mellitus diet"
* #DM ^property[0].code = #parent 
* #DM ^property[0].valueCode = #DIET 
* #FAST "fasting"
* #FAST ^property[0].code = #parent 
* #FAST ^property[0].valueCode = #DIET 
* #GF "gluten free"
* #GF ^property[0].code = #parent 
* #GF ^property[0].valueCode = #DIET 
* #LF "low fat"
* #LF ^property[0].code = #parent 
* #LF ^property[0].valueCode = #DIET 
* #LP "low protein"
* #LP ^property[0].code = #parent 
* #LP ^property[0].valueCode = #DIET 
* #LQ "liquid"
* #LQ ^property[0].code = #parent 
* #LQ ^property[0].valueCode = #DIET 
* #LS "low sodium"
* #LS ^property[0].code = #parent 
* #LS ^property[0].valueCode = #DIET 
* #N "normal diet"
* #N ^property[0].code = #parent 
* #N ^property[0].valueCode = #DIET 
* #NF "no fat"
* #NF ^property[0].code = #parent 
* #NF ^property[0].valueCode = #DIET 
* #PAF "phenylalanine free"
* #PAF ^property[0].code = #parent 
* #PAF ^property[0].valueCode = #DIET 
* #PAR "parenteral"
* #PAR ^property[0].code = #parent 
* #PAR ^property[0].valueCode = #DIET 
* #RD "reduction diet"
* #RD ^property[0].code = #parent 
* #RD ^property[0].valueCode = #DIET 
* #SCH "schonkost (GE)"
* #SCH ^property[0].code = #parent 
* #SCH ^property[0].valueCode = #DIET 
* #T "tea only"
* #T ^property[0].code = #parent 
* #T ^property[0].valueCode = #DIET 
* #VLI "low valin, leucin, isoleucin"
* #VLI ^property[0].code = #parent 
* #VLI ^property[0].valueCode = #DIET 
* #DRUGPRG "drug program"
* #F "final"
* #PRLMN "preliminary"
* #STORE "Storage"
* #SUBSIDFFS "subsidized fee for service program"
* #WRKCOMP "(workers compensation program"
* #_ActAccountCode "ActAccountCode"
* #_ActAccountCode ^property[0].code = #child 
* #_ActAccountCode ^property[0].valueCode = #ACCTRECEIVABLE 
* #_ActAccountCode ^property[1].code = #child 
* #_ActAccountCode ^property[1].valueCode = #CASH 
* #_ActAccountCode ^property[2].code = #child 
* #_ActAccountCode ^property[2].valueCode = #CC 
* #_ActAccountCode ^property[3].code = #child 
* #_ActAccountCode ^property[3].valueCode = #PBILLACCT 
* #ACCTRECEIVABLE "account receivable"
* #ACCTRECEIVABLE ^property[0].code = #parent 
* #ACCTRECEIVABLE ^property[0].valueCode = #_ActAccountCode 
* #CASH "Cash"
* #CASH ^property[0].code = #parent 
* #CASH ^property[0].valueCode = #_ActAccountCode 
* #CC "credit card"
* #CC ^property[0].code = #parent 
* #CC ^property[0].valueCode = #_ActAccountCode 
* #CC ^property[1].code = #child 
* #CC ^property[1].valueCode = #AE 
* #CC ^property[2].code = #child 
* #CC ^property[2].valueCode = #DN 
* #CC ^property[3].code = #child 
* #CC ^property[3].valueCode = #DV 
* #CC ^property[4].code = #child 
* #CC ^property[4].valueCode = #MC 
* #CC ^property[5].code = #child 
* #CC ^property[5].valueCode = #V 
* #AE "American Express"
* #AE ^property[0].code = #parent 
* #AE ^property[0].valueCode = #CC 
* #DN "Diner`s Club"
* #DN ^property[0].code = #parent 
* #DN ^property[0].valueCode = #CC 
* #DV "Discover Card"
* #DV ^property[0].code = #parent 
* #DV ^property[0].valueCode = #CC 
* #MC "Master Card"
* #MC ^property[0].code = #parent 
* #MC ^property[0].valueCode = #CC 
* #V "Visa"
* #V ^property[0].code = #parent 
* #V ^property[0].valueCode = #CC 
* #PBILLACCT "patient billing account"
* #PBILLACCT ^property[0].code = #parent 
* #PBILLACCT ^property[0].valueCode = #_ActAccountCode 
* #_ActAdjudicationCode "ActAdjudicationCode"
* #_ActAdjudicationCode ^property[0].code = #child 
* #_ActAdjudicationCode ^property[0].valueCode = #AA 
* #_ActAdjudicationCode ^property[1].code = #child 
* #_ActAdjudicationCode ^property[1].valueCode = #AR 
* #_ActAdjudicationCode ^property[2].code = #child 
* #_ActAdjudicationCode ^property[2].valueCode = #AS 
* #_ActAdjudicationCode ^property[3].code = #child 
* #_ActAdjudicationCode ^property[3].valueCode = #_ActAdjudicationGroupCode 
* #AA "adjudicated with adjustments"
* #AA ^property[0].code = #parent 
* #AA ^property[0].valueCode = #_ActAdjudicationCode 
* #AA ^property[1].code = #child 
* #AA ^property[1].valueCode = #ANF 
* #ANF "adjudicated with adjustments and no financial impact"
* #ANF ^property[0].code = #parent 
* #ANF ^property[0].valueCode = #AA 
* #AR "adjudicated as refused"
* #AR ^property[0].code = #parent 
* #AR ^property[0].valueCode = #_ActAdjudicationCode 
* #AS "adjudicated as submitted"
* #AS ^property[0].code = #parent 
* #AS ^property[0].valueCode = #_ActAdjudicationCode 
* #_ActAdjudicationGroupCode "ActAdjudicationGroupCode"
* #_ActAdjudicationGroupCode ^property[0].code = #parent 
* #_ActAdjudicationGroupCode ^property[0].valueCode = #_ActAdjudicationCode 
* #_ActAdjudicationGroupCode ^property[1].code = #child 
* #_ActAdjudicationGroupCode ^property[1].valueCode = #CONT 
* #_ActAdjudicationGroupCode ^property[2].code = #child 
* #_ActAdjudicationGroupCode ^property[2].valueCode = #DAY 
* #_ActAdjudicationGroupCode ^property[3].code = #child 
* #_ActAdjudicationGroupCode ^property[3].valueCode = #LOC 
* #_ActAdjudicationGroupCode ^property[4].code = #child 
* #_ActAdjudicationGroupCode ^property[4].valueCode = #MONTH 
* #_ActAdjudicationGroupCode ^property[5].code = #child 
* #_ActAdjudicationGroupCode ^property[5].valueCode = #PERIOD 
* #_ActAdjudicationGroupCode ^property[6].code = #child 
* #_ActAdjudicationGroupCode ^property[6].valueCode = #PROV 
* #_ActAdjudicationGroupCode ^property[7].code = #child 
* #_ActAdjudicationGroupCode ^property[7].valueCode = #WEEK 
* #_ActAdjudicationGroupCode ^property[8].code = #child 
* #_ActAdjudicationGroupCode ^property[8].valueCode = #YEAR 
* #CONT "contract"
* #CONT ^property[0].code = #parent 
* #CONT ^property[0].valueCode = #_ActAdjudicationGroupCode 
* #CONT ^property[1].code = #parent 
* #CONT ^property[1].valueCode = #_ActAdjudicationGroupCode 
* #DAY "day"
* #DAY ^property[0].code = #parent 
* #DAY ^property[0].valueCode = #_ActAdjudicationGroupCode 
* #DAY ^property[1].code = #parent 
* #DAY ^property[1].valueCode = #_ActAdjudicationGroupCode 
* #LOC "location"
* #LOC ^property[0].code = #parent 
* #LOC ^property[0].valueCode = #_ActAdjudicationGroupCode 
* #LOC ^property[1].code = #parent 
* #LOC ^property[1].valueCode = #_ActAdjudicationGroupCode 
* #MONTH "month"
* #MONTH ^property[0].code = #parent 
* #MONTH ^property[0].valueCode = #_ActAdjudicationGroupCode 
* #MONTH ^property[1].code = #parent 
* #MONTH ^property[1].valueCode = #_ActAdjudicationGroupCode 
* #PERIOD "period"
* #PERIOD ^property[0].code = #parent 
* #PERIOD ^property[0].valueCode = #_ActAdjudicationGroupCode 
* #PERIOD ^property[1].code = #parent 
* #PERIOD ^property[1].valueCode = #_ActAdjudicationGroupCode 
* #PROV "provider"
* #PROV ^property[0].code = #parent 
* #PROV ^property[0].valueCode = #_ActAdjudicationGroupCode 
* #PROV ^property[1].code = #parent 
* #PROV ^property[1].valueCode = #_ActAdjudicationGroupCode 
* #WEEK "week"
* #WEEK ^property[0].code = #parent 
* #WEEK ^property[0].valueCode = #_ActAdjudicationGroupCode 
* #WEEK ^property[1].code = #parent 
* #WEEK ^property[1].valueCode = #_ActAdjudicationGroupCode 
* #YEAR "year"
* #YEAR ^property[0].code = #parent 
* #YEAR ^property[0].valueCode = #_ActAdjudicationGroupCode 
* #YEAR ^property[1].code = #parent 
* #YEAR ^property[1].valueCode = #_ActAdjudicationGroupCode 
* #_ActAdjudicationResultActionCode "ActAdjudicationResultActionCode"
* #_ActAdjudicationResultActionCode ^property[0].code = #child 
* #_ActAdjudicationResultActionCode ^property[0].valueCode = #DISPLAY 
* #_ActAdjudicationResultActionCode ^property[1].code = #child 
* #_ActAdjudicationResultActionCode ^property[1].valueCode = #FORM 
* #DISPLAY "Display"
* #DISPLAY ^property[0].code = #parent 
* #DISPLAY ^property[0].valueCode = #_ActAdjudicationResultActionCode 
* #FORM "Print on Form"
* #FORM ^property[0].code = #parent 
* #FORM ^property[0].valueCode = #_ActAdjudicationResultActionCode 
* #_ActBillableModifierCode "ActBillableModifierCode"
* #_ActBillableModifierCode ^property[0].code = #child 
* #_ActBillableModifierCode ^property[0].valueCode = #CPTM 
* #_ActBillableModifierCode ^property[1].code = #child 
* #_ActBillableModifierCode ^property[1].valueCode = #HCPCSA 
* #CPTM "CPT modifier codes"
* #CPTM ^property[0].code = #parent 
* #CPTM ^property[0].valueCode = #_ActBillableModifierCode 
* #HCPCSA "HCPCS Level II and Carrier-assigned"
* #HCPCSA ^property[0].code = #parent 
* #HCPCSA ^property[0].valueCode = #_ActBillableModifierCode 
* #_ActBillingArrangementCode "ActBillingArrangementCode"
* #_ActBillingArrangementCode ^property[0].code = #child 
* #_ActBillingArrangementCode ^property[0].valueCode = #BLK 
* #_ActBillingArrangementCode ^property[1].code = #child 
* #_ActBillingArrangementCode ^property[1].valueCode = #CAP 
* #_ActBillingArrangementCode ^property[2].code = #child 
* #_ActBillingArrangementCode ^property[2].valueCode = #CONTF 
* #_ActBillingArrangementCode ^property[3].code = #child 
* #_ActBillingArrangementCode ^property[3].valueCode = #FINBILL 
* #_ActBillingArrangementCode ^property[4].code = #child 
* #_ActBillingArrangementCode ^property[4].valueCode = #ROST 
* #_ActBillingArrangementCode ^property[5].code = #child 
* #_ActBillingArrangementCode ^property[5].valueCode = #SESS 
* #BLK "block funding"
* #BLK ^property[0].code = #parent 
* #BLK ^property[0].valueCode = #_ActBillingArrangementCode 
* #CAP "capitation funding"
* #CAP ^property[0].code = #parent 
* #CAP ^property[0].valueCode = #_ActBillingArrangementCode 
* #CONTF "contract funding"
* #CONTF ^property[0].code = #parent 
* #CONTF ^property[0].valueCode = #_ActBillingArrangementCode 
* #FINBILL "financial"
* #FINBILL ^property[0].code = #parent 
* #FINBILL ^property[0].valueCode = #_ActBillingArrangementCode 
* #ROST "roster funding"
* #ROST ^property[0].code = #parent 
* #ROST ^property[0].valueCode = #_ActBillingArrangementCode 
* #SESS "sessional funding"
* #SESS ^property[0].code = #parent 
* #SESS ^property[0].valueCode = #_ActBillingArrangementCode 
* #_ActBoundedROICode "ActBoundedROICode"
* #_ActBoundedROICode ^property[0].code = #child 
* #_ActBoundedROICode ^property[0].valueCode = #ROIFS 
* #_ActBoundedROICode ^property[1].code = #child 
* #_ActBoundedROICode ^property[1].valueCode = #ROIPS 
* #ROIFS "fully specified ROI"
* #ROIFS ^property[0].code = #parent 
* #ROIFS ^property[0].valueCode = #_ActBoundedROICode 
* #ROIPS "partially specified ROI"
* #ROIPS ^property[0].code = #parent 
* #ROIPS ^property[0].valueCode = #_ActBoundedROICode 
* #_ActCareProvisionCode "act care provision"
* #_ActCareProvisionCode ^property[0].code = #child 
* #_ActCareProvisionCode ^property[0].valueCode = #_ActCredentialedCareCode 
* #_ActCareProvisionCode ^property[1].code = #child 
* #_ActCareProvisionCode ^property[1].valueCode = #_ActEncounterCode 
* #_ActCareProvisionCode ^property[2].code = #child 
* #_ActCareProvisionCode ^property[2].valueCode = #_ActMedicalServiceCode 
* #_ActCredentialedCareCode "act credentialed care"
* #_ActCredentialedCareCode ^property[0].code = #parent 
* #_ActCredentialedCareCode ^property[0].valueCode = #_ActCareProvisionCode 
* #_ActCredentialedCareCode ^property[1].code = #child 
* #_ActCredentialedCareCode ^property[1].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #_ActCredentialedCareCode ^property[2].code = #child 
* #_ActCredentialedCareCode ^property[2].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #_ActCredentialedCareProvisionPersonCode "act credentialed care provision peron"
* #_ActCredentialedCareProvisionPersonCode ^property[0].code = #parent 
* #_ActCredentialedCareProvisionPersonCode ^property[0].valueCode = #_ActCredentialedCareCode 
* #_ActCredentialedCareProvisionPersonCode ^property[1].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[1].valueCode = #CACC 
* #_ActCredentialedCareProvisionPersonCode ^property[2].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[2].valueCode = #CAIC 
* #_ActCredentialedCareProvisionPersonCode ^property[3].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[3].valueCode = #CAMC 
* #_ActCredentialedCareProvisionPersonCode ^property[4].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[4].valueCode = #CANC 
* #_ActCredentialedCareProvisionPersonCode ^property[5].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[5].valueCode = #CAPC 
* #_ActCredentialedCareProvisionPersonCode ^property[6].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[6].valueCode = #CBGC 
* #_ActCredentialedCareProvisionPersonCode ^property[7].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[7].valueCode = #CCCC 
* #_ActCredentialedCareProvisionPersonCode ^property[8].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[8].valueCode = #CCGC 
* #_ActCredentialedCareProvisionPersonCode ^property[9].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[9].valueCode = #CCPC 
* #_ActCredentialedCareProvisionPersonCode ^property[10].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[10].valueCode = #CCSC 
* #_ActCredentialedCareProvisionPersonCode ^property[11].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[11].valueCode = #CDEC 
* #_ActCredentialedCareProvisionPersonCode ^property[12].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[12].valueCode = #CDRC 
* #_ActCredentialedCareProvisionPersonCode ^property[13].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[13].valueCode = #CEMC 
* #_ActCredentialedCareProvisionPersonCode ^property[14].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[14].valueCode = #CFPC 
* #_ActCredentialedCareProvisionPersonCode ^property[15].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[15].valueCode = #CIMC 
* #_ActCredentialedCareProvisionPersonCode ^property[16].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[16].valueCode = #CMGC 
* #_ActCredentialedCareProvisionPersonCode ^property[17].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[17].valueCode = #CNEC 
* #_ActCredentialedCareProvisionPersonCode ^property[18].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[18].valueCode = #CNMC 
* #_ActCredentialedCareProvisionPersonCode ^property[19].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[19].valueCode = #CNQC 
* #_ActCredentialedCareProvisionPersonCode ^property[20].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[20].valueCode = #CNSC 
* #_ActCredentialedCareProvisionPersonCode ^property[21].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[21].valueCode = #COGC 
* #_ActCredentialedCareProvisionPersonCode ^property[22].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[22].valueCode = #COMC 
* #_ActCredentialedCareProvisionPersonCode ^property[23].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[23].valueCode = #COPC 
* #_ActCredentialedCareProvisionPersonCode ^property[24].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[24].valueCode = #COSC 
* #_ActCredentialedCareProvisionPersonCode ^property[25].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[25].valueCode = #COTC 
* #_ActCredentialedCareProvisionPersonCode ^property[26].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[26].valueCode = #CPEC 
* #_ActCredentialedCareProvisionPersonCode ^property[27].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[27].valueCode = #CPGC 
* #_ActCredentialedCareProvisionPersonCode ^property[28].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[28].valueCode = #CPHC 
* #_ActCredentialedCareProvisionPersonCode ^property[29].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[29].valueCode = #CPRC 
* #_ActCredentialedCareProvisionPersonCode ^property[30].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[30].valueCode = #CPSC 
* #_ActCredentialedCareProvisionPersonCode ^property[31].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[31].valueCode = #CPYC 
* #_ActCredentialedCareProvisionPersonCode ^property[32].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[32].valueCode = #CROC 
* #_ActCredentialedCareProvisionPersonCode ^property[33].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[33].valueCode = #CRPC 
* #_ActCredentialedCareProvisionPersonCode ^property[34].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[34].valueCode = #CSUC 
* #_ActCredentialedCareProvisionPersonCode ^property[35].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[35].valueCode = #CTSC 
* #_ActCredentialedCareProvisionPersonCode ^property[36].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[36].valueCode = #CURC 
* #_ActCredentialedCareProvisionPersonCode ^property[37].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[37].valueCode = #CVSC 
* #_ActCredentialedCareProvisionPersonCode ^property[38].code = #child 
* #_ActCredentialedCareProvisionPersonCode ^property[38].valueCode = #LGPC 
* #CACC "certified anatomic pathology and clinical pathology care"
* #CACC ^property[0].code = #parent 
* #CACC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CAIC "certified allergy and immunology care"
* #CAIC ^property[0].code = #parent 
* #CAIC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CAMC "certified aerospace medicine care"
* #CAMC ^property[0].code = #parent 
* #CAMC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CANC "certified anesthesiology care"
* #CANC ^property[0].code = #parent 
* #CANC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CAPC "certified anatomic pathology care"
* #CAPC ^property[0].code = #parent 
* #CAPC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CBGC "certified clinical biochemical genetics care"
* #CBGC ^property[0].code = #parent 
* #CBGC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CCCC "certified clinical cytogenetics care"
* #CCCC ^property[0].code = #parent 
* #CCCC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CCGC "certified clinical genetics (M.D.) care"
* #CCGC ^property[0].code = #parent 
* #CCGC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CCPC "certified clinical pathology care"
* #CCPC ^property[0].code = #parent 
* #CCPC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CCSC "certified colon and rectal surgery care"
* #CCSC ^property[0].code = #parent 
* #CCSC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CDEC "certified dermatology care"
* #CDEC ^property[0].code = #parent 
* #CDEC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CDRC "certified diagnostic radiology care"
* #CDRC ^property[0].code = #parent 
* #CDRC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CEMC "certified emergency medicine care"
* #CEMC ^property[0].code = #parent 
* #CEMC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CFPC "certified family practice care"
* #CFPC ^property[0].code = #parent 
* #CFPC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CIMC "certified internal medicine care"
* #CIMC ^property[0].code = #parent 
* #CIMC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CMGC "certified clinical molecular genetics care"
* #CMGC ^property[0].code = #parent 
* #CMGC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CNEC "certified neurology care"
* #CNEC ^property[0].code = #parent 
* #CNEC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CNMC "certified nuclear medicine care"
* #CNMC ^property[0].code = #parent 
* #CNMC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CNQC "certified neurology with special qualifications in child neurology care"
* #CNQC ^property[0].code = #parent 
* #CNQC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CNSC "certified neurological surgery care"
* #CNSC ^property[0].code = #parent 
* #CNSC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #COGC "certified obstetrics and gynecology care"
* #COGC ^property[0].code = #parent 
* #COGC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #COMC "certified occupational medicine care"
* #COMC ^property[0].code = #parent 
* #COMC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #COPC "certified ophthalmology care"
* #COPC ^property[0].code = #parent 
* #COPC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #COSC "certified orthopaedic surgery care"
* #COSC ^property[0].code = #parent 
* #COSC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #COTC "certified otolaryngology care"
* #COTC ^property[0].code = #parent 
* #COTC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CPEC "certified pediatrics care"
* #CPEC ^property[0].code = #parent 
* #CPEC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CPGC "certified Ph.D. medical genetics care"
* #CPGC ^property[0].code = #parent 
* #CPGC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CPHC "certified public health and general preventive medicine care"
* #CPHC ^property[0].code = #parent 
* #CPHC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CPRC "certified physical medicine and rehabilitation care"
* #CPRC ^property[0].code = #parent 
* #CPRC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CPSC "certified plastic surgery care"
* #CPSC ^property[0].code = #parent 
* #CPSC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CPYC "certified psychiatry care"
* #CPYC ^property[0].code = #parent 
* #CPYC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CROC "certified radiation oncology care"
* #CROC ^property[0].code = #parent 
* #CROC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CRPC "certified radiological physics care"
* #CRPC ^property[0].code = #parent 
* #CRPC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CSUC "certified surgery care"
* #CSUC ^property[0].code = #parent 
* #CSUC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CTSC "certified thoracic surgery care"
* #CTSC ^property[0].code = #parent 
* #CTSC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CURC "certified urology care"
* #CURC ^property[0].code = #parent 
* #CURC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #CVSC "certified vascular surgery care"
* #CVSC ^property[0].code = #parent 
* #CVSC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #LGPC "licensed general physician care"
* #LGPC ^property[0].code = #parent 
* #LGPC ^property[0].valueCode = #_ActCredentialedCareProvisionPersonCode 
* #_ActCredentialedCareProvisionProgramCode "act credentialed care provision program"
* #_ActCredentialedCareProvisionProgramCode ^property[0].code = #parent 
* #_ActCredentialedCareProvisionProgramCode ^property[0].valueCode = #_ActCredentialedCareCode 
* #_ActCredentialedCareProvisionProgramCode ^property[1].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[1].valueCode = #AALC 
* #_ActCredentialedCareProvisionProgramCode ^property[2].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[2].valueCode = #AAMC 
* #_ActCredentialedCareProvisionProgramCode ^property[3].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[3].valueCode = #ABHC 
* #_ActCredentialedCareProvisionProgramCode ^property[4].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[4].valueCode = #ACAC 
* #_ActCredentialedCareProvisionProgramCode ^property[5].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[5].valueCode = #ACHC 
* #_ActCredentialedCareProvisionProgramCode ^property[6].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[6].valueCode = #AHOC 
* #_ActCredentialedCareProvisionProgramCode ^property[7].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[7].valueCode = #ALTC 
* #_ActCredentialedCareProvisionProgramCode ^property[8].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[8].valueCode = #AOSC 
* #_ActCredentialedCareProvisionProgramCode ^property[9].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[9].valueCode = #CACS 
* #_ActCredentialedCareProvisionProgramCode ^property[10].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[10].valueCode = #CAMI 
* #_ActCredentialedCareProvisionProgramCode ^property[11].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[11].valueCode = #CAST 
* #_ActCredentialedCareProvisionProgramCode ^property[12].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[12].valueCode = #CBAR 
* #_ActCredentialedCareProvisionProgramCode ^property[13].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[13].valueCode = #CCAD 
* #_ActCredentialedCareProvisionProgramCode ^property[14].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[14].valueCode = #CCAR 
* #_ActCredentialedCareProvisionProgramCode ^property[15].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[15].valueCode = #CDEP 
* #_ActCredentialedCareProvisionProgramCode ^property[16].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[16].valueCode = #CDGD 
* #_ActCredentialedCareProvisionProgramCode ^property[17].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[17].valueCode = #CDIA 
* #_ActCredentialedCareProvisionProgramCode ^property[18].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[18].valueCode = #CEPI 
* #_ActCredentialedCareProvisionProgramCode ^property[19].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[19].valueCode = #CFEL 
* #_ActCredentialedCareProvisionProgramCode ^property[20].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[20].valueCode = #CHFC 
* #_ActCredentialedCareProvisionProgramCode ^property[21].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[21].valueCode = #CHRO 
* #_ActCredentialedCareProvisionProgramCode ^property[22].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[22].valueCode = #CHYP 
* #_ActCredentialedCareProvisionProgramCode ^property[23].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[23].valueCode = #CMIH 
* #_ActCredentialedCareProvisionProgramCode ^property[24].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[24].valueCode = #CMSC 
* #_ActCredentialedCareProvisionProgramCode ^property[25].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[25].valueCode = #COJR 
* #_ActCredentialedCareProvisionProgramCode ^property[26].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[26].valueCode = #CONC 
* #_ActCredentialedCareProvisionProgramCode ^property[27].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[27].valueCode = #COPD 
* #_ActCredentialedCareProvisionProgramCode ^property[28].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[28].valueCode = #CORT 
* #_ActCredentialedCareProvisionProgramCode ^property[29].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[29].valueCode = #CPAD 
* #_ActCredentialedCareProvisionProgramCode ^property[30].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[30].valueCode = #CPND 
* #_ActCredentialedCareProvisionProgramCode ^property[31].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[31].valueCode = #CPST 
* #_ActCredentialedCareProvisionProgramCode ^property[32].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[32].valueCode = #CSDM 
* #_ActCredentialedCareProvisionProgramCode ^property[33].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[33].valueCode = #CSIC 
* #_ActCredentialedCareProvisionProgramCode ^property[34].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[34].valueCode = #CSLD 
* #_ActCredentialedCareProvisionProgramCode ^property[35].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[35].valueCode = #CSPT 
* #_ActCredentialedCareProvisionProgramCode ^property[36].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[36].valueCode = #CTBU 
* #_ActCredentialedCareProvisionProgramCode ^property[37].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[37].valueCode = #CVDC 
* #_ActCredentialedCareProvisionProgramCode ^property[38].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[38].valueCode = #CWMA 
* #_ActCredentialedCareProvisionProgramCode ^property[39].code = #child 
* #_ActCredentialedCareProvisionProgramCode ^property[39].valueCode = #CWOH 
* #AALC "accredited assisted living care"
* #AALC ^property[0].code = #parent 
* #AALC ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #AAMC "accredited ambulatory care"
* #AAMC ^property[0].code = #parent 
* #AAMC ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #ABHC "accredited behavioral health care"
* #ABHC ^property[0].code = #parent 
* #ABHC ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #ACAC "accredited critical access hospital care"
* #ACAC ^property[0].code = #parent 
* #ACAC ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #ACHC "accredited hospital care"
* #ACHC ^property[0].code = #parent 
* #ACHC ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #AHOC "accredited home care"
* #AHOC ^property[0].code = #parent 
* #AHOC ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #ALTC "accredited long term care"
* #ALTC ^property[0].code = #parent 
* #ALTC ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #AOSC "accredited office-based surgery care"
* #AOSC ^property[0].code = #parent 
* #AOSC ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #CACS "certified acute coronary syndrome care"
* #CACS ^property[0].code = #parent 
* #CACS ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #CAMI "certified acute myocardial infarction care"
* #CAMI ^property[0].code = #parent 
* #CAMI ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #CAST "certified asthma care"
* #CAST ^property[0].code = #parent 
* #CAST ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #CBAR "certified bariatric surgery care"
* #CBAR ^property[0].code = #parent 
* #CBAR ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #CCAD "certified coronary artery disease care"
* #CCAD ^property[0].code = #parent 
* #CCAD ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #CCAR "certified cardiac care"
* #CCAR ^property[0].code = #parent 
* #CCAR ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #CDEP "certified depression care"
* #CDEP ^property[0].code = #parent 
* #CDEP ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #CDGD "certified digestive/gastrointestinal disorders care"
* #CDGD ^property[0].code = #parent 
* #CDGD ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #CDIA "certified diabetes care"
* #CDIA ^property[0].code = #parent 
* #CDIA ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #CEPI "certified epilepsy care"
* #CEPI ^property[0].code = #parent 
* #CEPI ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #CFEL "certified frail elderly care"
* #CFEL ^property[0].code = #parent 
* #CFEL ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #CHFC "certified heart failure care"
* #CHFC ^property[0].code = #parent 
* #CHFC ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #CHRO "certified high risk obstetrics care"
* #CHRO ^property[0].code = #parent 
* #CHRO ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #CHYP "certified hyperlipidemia care"
* #CHYP ^property[0].code = #parent 
* #CHYP ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #CMIH "certified migraine headache care"
* #CMIH ^property[0].code = #parent 
* #CMIH ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #CMSC "certified multiple sclerosis care"
* #CMSC ^property[0].code = #parent 
* #CMSC ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #COJR "certified orthopedic joint replacement care"
* #COJR ^property[0].code = #parent 
* #COJR ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #CONC "certified oncology care"
* #CONC ^property[0].code = #parent 
* #CONC ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #COPD "certified chronic obstructive pulmonary disease care"
* #COPD ^property[0].code = #parent 
* #COPD ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #CORT "certified organ transplant care"
* #CORT ^property[0].code = #parent 
* #CORT ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #CPAD "certified parkinsons disease care"
* #CPAD ^property[0].code = #parent 
* #CPAD ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #CPND "certified pneumonia disease care"
* #CPND ^property[0].code = #parent 
* #CPND ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #CPST "certified primary stroke center care"
* #CPST ^property[0].code = #parent 
* #CPST ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #CSDM "certified stroke disease management care"
* #CSDM ^property[0].code = #parent 
* #CSDM ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #CSIC "certified sickle cell care"
* #CSIC ^property[0].code = #parent 
* #CSIC ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #CSLD "certified sleep disorders care"
* #CSLD ^property[0].code = #parent 
* #CSLD ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #CSPT "certified spine treatment care"
* #CSPT ^property[0].code = #parent 
* #CSPT ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #CTBU "certified trauma/burn center care"
* #CTBU ^property[0].code = #parent 
* #CTBU ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #CVDC "certified vascular diseases care"
* #CVDC ^property[0].code = #parent 
* #CVDC ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #CWMA "certified wound management care"
* #CWMA ^property[0].code = #parent 
* #CWMA ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #CWOH "certified women`s health care"
* #CWOH ^property[0].code = #parent 
* #CWOH ^property[0].valueCode = #_ActCredentialedCareProvisionProgramCode 
* #_ActEncounterCode "ActEncounterCode"
* #_ActEncounterCode ^property[0].code = #parent 
* #_ActEncounterCode ^property[0].valueCode = #_ActCareProvisionCode 
* #_ActEncounterCode ^property[1].code = #child 
* #_ActEncounterCode ^property[1].valueCode = #AMB 
* #_ActEncounterCode ^property[2].code = #child 
* #_ActEncounterCode ^property[2].valueCode = #EMER 
* #_ActEncounterCode ^property[3].code = #child 
* #_ActEncounterCode ^property[3].valueCode = #FLD 
* #_ActEncounterCode ^property[4].code = #child 
* #_ActEncounterCode ^property[4].valueCode = #HH 
* #_ActEncounterCode ^property[5].code = #child 
* #_ActEncounterCode ^property[5].valueCode = #IMP 
* #_ActEncounterCode ^property[6].code = #child 
* #_ActEncounterCode ^property[6].valueCode = #PRENC 
* #_ActEncounterCode ^property[7].code = #child 
* #_ActEncounterCode ^property[7].valueCode = #SS 
* #_ActEncounterCode ^property[8].code = #child 
* #_ActEncounterCode ^property[8].valueCode = #VR 
* #AMB "ambulatory"
* #AMB ^property[0].code = #parent 
* #AMB ^property[0].valueCode = #_ActEncounterCode 
* #EMER "emergency"
* #EMER ^property[0].code = #parent 
* #EMER ^property[0].valueCode = #_ActEncounterCode 
* #FLD "field"
* #FLD ^property[0].code = #parent 
* #FLD ^property[0].valueCode = #_ActEncounterCode 
* #HH "home health"
* #HH ^property[0].code = #parent 
* #HH ^property[0].valueCode = #_ActEncounterCode 
* #IMP "inpatient encounter"
* #IMP ^property[0].code = #parent 
* #IMP ^property[0].valueCode = #_ActEncounterCode 
* #IMP ^property[1].code = #child 
* #IMP ^property[1].valueCode = #ACUTE 
* #IMP ^property[2].code = #child 
* #IMP ^property[2].valueCode = #NONAC 
* #ACUTE "inpatient acute"
* #ACUTE ^property[0].code = #parent 
* #ACUTE ^property[0].valueCode = #IMP 
* #NONAC "inpatient non-acute"
* #NONAC ^property[0].code = #parent 
* #NONAC ^property[0].valueCode = #IMP 
* #PRENC "pre-admission"
* #PRENC ^property[0].code = #parent 
* #PRENC ^property[0].valueCode = #_ActEncounterCode 
* #SS "short stay"
* #SS ^property[0].code = #parent 
* #SS ^property[0].valueCode = #_ActEncounterCode 
* #VR "virtual"
* #VR ^property[0].code = #parent 
* #VR ^property[0].valueCode = #_ActEncounterCode 
* #_ActMedicalServiceCode "ActMedicalServiceCode"
* #_ActMedicalServiceCode ^property[0].code = #parent 
* #_ActMedicalServiceCode ^property[0].valueCode = #_ActCareProvisionCode 
* #_ActMedicalServiceCode ^property[1].code = #child 
* #_ActMedicalServiceCode ^property[1].valueCode = #ALC 
* #_ActMedicalServiceCode ^property[2].code = #child 
* #_ActMedicalServiceCode ^property[2].valueCode = #CARD 
* #_ActMedicalServiceCode ^property[3].code = #child 
* #_ActMedicalServiceCode ^property[3].valueCode = #CHR 
* #_ActMedicalServiceCode ^property[4].code = #child 
* #_ActMedicalServiceCode ^property[4].valueCode = #DNTL 
* #_ActMedicalServiceCode ^property[5].code = #child 
* #_ActMedicalServiceCode ^property[5].valueCode = #DRGRHB 
* #_ActMedicalServiceCode ^property[6].code = #child 
* #_ActMedicalServiceCode ^property[6].valueCode = #GENRL 
* #_ActMedicalServiceCode ^property[7].code = #child 
* #_ActMedicalServiceCode ^property[7].valueCode = #MED 
* #_ActMedicalServiceCode ^property[8].code = #child 
* #_ActMedicalServiceCode ^property[8].valueCode = #OBS 
* #_ActMedicalServiceCode ^property[9].code = #child 
* #_ActMedicalServiceCode ^property[9].valueCode = #ONC 
* #_ActMedicalServiceCode ^property[10].code = #child 
* #_ActMedicalServiceCode ^property[10].valueCode = #PALL 
* #_ActMedicalServiceCode ^property[11].code = #child 
* #_ActMedicalServiceCode ^property[11].valueCode = #PED 
* #_ActMedicalServiceCode ^property[12].code = #child 
* #_ActMedicalServiceCode ^property[12].valueCode = #PHAR 
* #_ActMedicalServiceCode ^property[13].code = #child 
* #_ActMedicalServiceCode ^property[13].valueCode = #PHYRHB 
* #_ActMedicalServiceCode ^property[14].code = #child 
* #_ActMedicalServiceCode ^property[14].valueCode = #PSYCH 
* #_ActMedicalServiceCode ^property[15].code = #child 
* #_ActMedicalServiceCode ^property[15].valueCode = #SURG 
* #ALC "Alternative Level of Care"
* #ALC ^property[0].code = #parent 
* #ALC ^property[0].valueCode = #_ActMedicalServiceCode 
* #CARD "Cardiology"
* #CARD ^property[0].code = #parent 
* #CARD ^property[0].valueCode = #_ActMedicalServiceCode 
* #CHR "Chronic"
* #CHR ^property[0].code = #parent 
* #CHR ^property[0].valueCode = #_ActMedicalServiceCode 
* #DNTL "Dental"
* #DNTL ^property[0].code = #parent 
* #DNTL ^property[0].valueCode = #_ActMedicalServiceCode 
* #DRGRHB "Drug Rehab"
* #DRGRHB ^property[0].code = #parent 
* #DRGRHB ^property[0].valueCode = #_ActMedicalServiceCode 
* #GENRL "General"
* #GENRL ^property[0].code = #parent 
* #GENRL ^property[0].valueCode = #_ActMedicalServiceCode 
* #MED "Medical"
* #MED ^property[0].code = #parent 
* #MED ^property[0].valueCode = #_ActMedicalServiceCode 
* #OBS "Obstetrics"
* #OBS ^property[0].code = #parent 
* #OBS ^property[0].valueCode = #_ActMedicalServiceCode 
* #ONC "Oncology"
* #ONC ^property[0].code = #parent 
* #ONC ^property[0].valueCode = #_ActMedicalServiceCode 
* #PALL "Palliative"
* #PALL ^property[0].code = #parent 
* #PALL ^property[0].valueCode = #_ActMedicalServiceCode 
* #PED "Pediatrics"
* #PED ^property[0].code = #parent 
* #PED ^property[0].valueCode = #_ActMedicalServiceCode 
* #PHAR "Pharmaceutical"
* #PHAR ^property[0].code = #parent 
* #PHAR ^property[0].valueCode = #_ActMedicalServiceCode 
* #PHYRHB "Physical Rehab"
* #PHYRHB ^property[0].code = #parent 
* #PHYRHB ^property[0].valueCode = #_ActMedicalServiceCode 
* #PSYCH "Psychiatric"
* #PSYCH ^property[0].code = #parent 
* #PSYCH ^property[0].valueCode = #_ActMedicalServiceCode 
* #SURG "Surgical"
* #SURG ^property[0].code = #parent 
* #SURG ^property[0].valueCode = #_ActMedicalServiceCode 
* #_ActClaimAttachmentCategoryCode "ActClaimAttachmentCategoryCode"
* #_ActClaimAttachmentCategoryCode ^property[0].code = #child 
* #_ActClaimAttachmentCategoryCode ^property[0].valueCode = #AUTOATTCH 
* #_ActClaimAttachmentCategoryCode ^property[1].code = #child 
* #_ActClaimAttachmentCategoryCode ^property[1].valueCode = #DOCUMENT 
* #_ActClaimAttachmentCategoryCode ^property[2].code = #child 
* #_ActClaimAttachmentCategoryCode ^property[2].valueCode = #HEALTHREC 
* #_ActClaimAttachmentCategoryCode ^property[3].code = #child 
* #_ActClaimAttachmentCategoryCode ^property[3].valueCode = #IMG 
* #_ActClaimAttachmentCategoryCode ^property[4].code = #child 
* #_ActClaimAttachmentCategoryCode ^property[4].valueCode = #LABRESULTS 
* #_ActClaimAttachmentCategoryCode ^property[5].code = #child 
* #_ActClaimAttachmentCategoryCode ^property[5].valueCode = #MODEL 
* #_ActClaimAttachmentCategoryCode ^property[6].code = #child 
* #_ActClaimAttachmentCategoryCode ^property[6].valueCode = #WIATTCH 
* #_ActClaimAttachmentCategoryCode ^property[7].code = #child 
* #_ActClaimAttachmentCategoryCode ^property[7].valueCode = #XRAY 
* #AUTOATTCH "auto attachment"
* #AUTOATTCH ^property[0].code = #parent 
* #AUTOATTCH ^property[0].valueCode = #_ActClaimAttachmentCategoryCode 
* #DOCUMENT "document"
* #DOCUMENT ^property[0].code = #parent 
* #DOCUMENT ^property[0].valueCode = #_ActClaimAttachmentCategoryCode 
* #HEALTHREC "health record"
* #HEALTHREC ^property[0].code = #parent 
* #HEALTHREC ^property[0].valueCode = #_ActClaimAttachmentCategoryCode 
* #IMG "image"
* #IMG ^property[0].code = #parent 
* #IMG ^property[0].valueCode = #_ActClaimAttachmentCategoryCode 
* #LABRESULTS "lab results"
* #LABRESULTS ^property[0].code = #parent 
* #LABRESULTS ^property[0].valueCode = #_ActClaimAttachmentCategoryCode 
* #MODEL "model"
* #MODEL ^property[0].code = #parent 
* #MODEL ^property[0].valueCode = #_ActClaimAttachmentCategoryCode 
* #WIATTCH "work injury report attachment"
* #WIATTCH ^property[0].code = #parent 
* #WIATTCH ^property[0].valueCode = #_ActClaimAttachmentCategoryCode 
* #XRAY "x-ray"
* #XRAY ^property[0].code = #parent 
* #XRAY ^property[0].valueCode = #_ActClaimAttachmentCategoryCode 
* #_ActConsentType "ActConsentType"
* #_ActConsentType ^property[0].code = #child 
* #_ActConsentType ^property[0].valueCode = #ICOL 
* #_ActConsentType ^property[1].code = #child 
* #_ActConsentType ^property[1].valueCode = #IDSCL 
* #_ActConsentType ^property[2].code = #child 
* #_ActConsentType ^property[2].valueCode = #INFA 
* #_ActConsentType ^property[3].code = #child 
* #_ActConsentType ^property[3].valueCode = #IRDSCL 
* #_ActConsentType ^property[4].code = #child 
* #_ActConsentType ^property[4].valueCode = #RESEARCH 
* #ICOL "information collection"
* #ICOL ^property[0].code = #parent 
* #ICOL ^property[0].valueCode = #_ActConsentType 
* #IDSCL "information disclosure"
* #IDSCL ^property[0].code = #parent 
* #IDSCL ^property[0].valueCode = #_ActConsentType 
* #INFA "information access"
* #INFA ^property[0].code = #parent 
* #INFA ^property[0].valueCode = #_ActConsentType 
* #INFA ^property[1].code = #child 
* #INFA ^property[1].valueCode = #INFAO 
* #INFA ^property[2].code = #child 
* #INFA ^property[2].valueCode = #INFASO 
* #INFAO "access only"
* #INFAO ^property[0].code = #parent 
* #INFAO ^property[0].valueCode = #INFA 
* #INFASO "access and save only"
* #INFASO ^property[0].code = #parent 
* #INFASO ^property[0].valueCode = #INFA 
* #IRDSCL "information redisclosure"
* #IRDSCL ^property[0].code = #parent 
* #IRDSCL ^property[0].valueCode = #_ActConsentType 
* #RESEARCH "research information access"
* #RESEARCH ^property[0].code = #parent 
* #RESEARCH ^property[0].valueCode = #_ActConsentType 
* #RESEARCH ^property[1].code = #child 
* #RESEARCH ^property[1].valueCode = #RSDID 
* #RESEARCH ^property[2].code = #child 
* #RESEARCH ^property[2].valueCode = #RSREID 
* #RSDID "de-identified information access"
* #RSDID ^property[0].code = #parent 
* #RSDID ^property[0].valueCode = #RESEARCH 
* #RSREID "re-identifiable information access"
* #RSREID ^property[0].code = #parent 
* #RSREID ^property[0].valueCode = #RESEARCH 
* #_ActContainerRegistrationCode "ActContainerRegistrationCode"
* #_ActContainerRegistrationCode ^property[0].code = #child 
* #_ActContainerRegistrationCode ^property[0].valueCode = #ID 
* #_ActContainerRegistrationCode ^property[1].code = #child 
* #_ActContainerRegistrationCode ^property[1].valueCode = #IP 
* #_ActContainerRegistrationCode ^property[2].code = #child 
* #_ActContainerRegistrationCode ^property[2].valueCode = #L 
* #_ActContainerRegistrationCode ^property[3].code = #child 
* #_ActContainerRegistrationCode ^property[3].valueCode = #M 
* #_ActContainerRegistrationCode ^property[4].code = #child 
* #_ActContainerRegistrationCode ^property[4].valueCode = #O 
* #_ActContainerRegistrationCode ^property[5].code = #child 
* #_ActContainerRegistrationCode ^property[5].valueCode = #R 
* #_ActContainerRegistrationCode ^property[6].code = #child 
* #_ActContainerRegistrationCode ^property[6].valueCode = #X 
* #ID "Identified"
* #ID ^property[0].code = #parent 
* #ID ^property[0].valueCode = #_ActContainerRegistrationCode 
* #IP "In Position"
* #IP ^property[0].code = #parent 
* #IP ^property[0].valueCode = #_ActContainerRegistrationCode 
* #L "Left Equipment"
* #L ^property[0].code = #parent 
* #L ^property[0].valueCode = #_ActContainerRegistrationCode 
* #M "Missing"
* #M ^property[0].code = #parent 
* #M ^property[0].valueCode = #_ActContainerRegistrationCode 
* #O "In Process"
* #O ^property[0].code = #parent 
* #O ^property[0].valueCode = #_ActContainerRegistrationCode 
* #R "Process Completed"
* #R ^property[0].code = #parent 
* #R ^property[0].valueCode = #_ActContainerRegistrationCode 
* #X "Container Unavailable"
* #X ^property[0].code = #parent 
* #X ^property[0].valueCode = #_ActContainerRegistrationCode 
* #_ActControlVariable "ActControlVariable"
* #_ActControlVariable ^property[0].code = #child 
* #_ActControlVariable ^property[0].valueCode = #AUTO 
* #_ActControlVariable ^property[1].code = #child 
* #_ActControlVariable ^property[1].valueCode = #ENDC 
* #_ActControlVariable ^property[2].code = #child 
* #_ActControlVariable ^property[2].valueCode = #REFLEX 
* #AUTO "auto-repeat permission"
* #AUTO ^property[0].code = #parent 
* #AUTO ^property[0].valueCode = #_ActControlVariable 
* #ENDC "endogenous content"
* #ENDC ^property[0].code = #parent 
* #ENDC ^property[0].valueCode = #_ActControlVariable 
* #REFLEX "reflex permission"
* #REFLEX ^property[0].code = #parent 
* #REFLEX ^property[0].valueCode = #_ActControlVariable 
* #_ActCoverageConfirmationCode "ActCoverageConfirmationCode"
* #_ActCoverageConfirmationCode ^property[0].code = #child 
* #_ActCoverageConfirmationCode ^property[0].valueCode = #_ActCoverageAuthorizationConfirmationCode 
* #_ActCoverageAuthorizationConfirmationCode "ActCoverageAuthorizationConfirmationCode"
* #_ActCoverageAuthorizationConfirmationCode ^property[0].code = #parent 
* #_ActCoverageAuthorizationConfirmationCode ^property[0].valueCode = #_ActCoverageConfirmationCode 
* #_ActCoverageAuthorizationConfirmationCode ^property[1].code = #child 
* #_ActCoverageAuthorizationConfirmationCode ^property[1].valueCode = #AUTH 
* #_ActCoverageAuthorizationConfirmationCode ^property[2].code = #child 
* #_ActCoverageAuthorizationConfirmationCode ^property[2].valueCode = #NAUTH 
* #AUTH "Authorized"
* #AUTH ^property[0].code = #parent 
* #AUTH ^property[0].valueCode = #_ActCoverageAuthorizationConfirmationCode 
* #NAUTH "Not Authorized"
* #NAUTH ^property[0].code = #parent 
* #NAUTH ^property[0].valueCode = #_ActCoverageAuthorizationConfirmationCode 
* #_ActCoverageLimitCode "ActCoverageLimitCode"
* #_ActCoverageLimitCode ^property[0].code = #child 
* #_ActCoverageLimitCode ^property[0].valueCode = #COVMX 
* #_ActCoverageLimitCode ^property[1].code = #child 
* #_ActCoverageLimitCode ^property[1].valueCode = #_ActCoverageQuantityLimitCode 
* #COVMX "coverage maximum"
* #COVMX ^property[0].code = #parent 
* #COVMX ^property[0].valueCode = #_ActCoverageLimitCode 
* #COVMX ^property[1].code = #child 
* #COVMX ^property[1].valueCode = #LFEMX 
* #COVMX ^property[2].code = #child 
* #COVMX ^property[2].valueCode = #PRDMX 
* #LFEMX "life time maximum"
* #LFEMX ^property[0].code = #parent 
* #LFEMX ^property[0].valueCode = #COVMX 
* #PRDMX "period maximum"
* #PRDMX ^property[0].code = #parent 
* #PRDMX ^property[0].valueCode = #COVMX 
* #_ActCoverageQuantityLimitCode "ActCoverageQuantityLimitCode"
* #_ActCoverageQuantityLimitCode ^property[0].code = #parent 
* #_ActCoverageQuantityLimitCode ^property[0].valueCode = #_ActCoverageLimitCode 
* #_ActCoverageQuantityLimitCode ^property[1].code = #child 
* #_ActCoverageQuantityLimitCode ^property[1].valueCode = #COVPRD 
* #_ActCoverageQuantityLimitCode ^property[2].code = #child 
* #_ActCoverageQuantityLimitCode ^property[2].valueCode = #NETAMT 
* #_ActCoverageQuantityLimitCode ^property[3].code = #child 
* #_ActCoverageQuantityLimitCode ^property[3].valueCode = #UNITPRICE 
* #_ActCoverageQuantityLimitCode ^property[4].code = #child 
* #_ActCoverageQuantityLimitCode ^property[4].valueCode = #UNITQTY 
* #COVPRD "coverage period"
* #COVPRD ^property[0].code = #parent 
* #COVPRD ^property[0].valueCode = #_ActCoverageQuantityLimitCode 
* #NETAMT "Net Amount"
* #NETAMT ^property[0].code = #parent 
* #NETAMT ^property[0].valueCode = #_ActCoverageQuantityLimitCode 
* #UNITPRICE "Unit Price"
* #UNITPRICE ^property[0].code = #parent 
* #UNITPRICE ^property[0].valueCode = #_ActCoverageQuantityLimitCode 
* #UNITQTY "Unit Quantity"
* #UNITQTY ^property[0].code = #parent 
* #UNITQTY ^property[0].valueCode = #_ActCoverageQuantityLimitCode 
* #_ActCoverageTypeCode "ActCoverageTypeCode"
* #_ActCoverageTypeCode ^property[0].code = #child 
* #_ActCoverageTypeCode ^property[0].valueCode = #_ActInsurancePolicyCode 
* #_ActCoverageTypeCode ^property[1].code = #child 
* #_ActCoverageTypeCode ^property[1].valueCode = #_ActInsuranceTypeCode 
* #_ActCoverageTypeCode ^property[2].code = #child 
* #_ActCoverageTypeCode ^property[2].valueCode = #_ActProgramTypeCode 
* #_ActInsurancePolicyCode "ActInsurancePolicyCode"
* #_ActInsurancePolicyCode ^property[0].code = #parent 
* #_ActInsurancePolicyCode ^property[0].valueCode = #_ActCoverageTypeCode 
* #_ActInsurancePolicyCode ^property[1].code = #child 
* #_ActInsurancePolicyCode ^property[1].valueCode = #AUTOPOL 
* #_ActInsurancePolicyCode ^property[2].code = #child 
* #_ActInsurancePolicyCode ^property[2].valueCode = #EHCPOL 
* #_ActInsurancePolicyCode ^property[3].code = #child 
* #_ActInsurancePolicyCode ^property[3].valueCode = #HSAPOL 
* #_ActInsurancePolicyCode ^property[4].code = #child 
* #_ActInsurancePolicyCode ^property[4].valueCode = #PUBLICPOL 
* #AUTOPOL "automobile"
* #AUTOPOL ^property[0].code = #parent 
* #AUTOPOL ^property[0].valueCode = #_ActInsurancePolicyCode 
* #AUTOPOL ^property[1].code = #child 
* #AUTOPOL ^property[1].valueCode = #COL 
* #AUTOPOL ^property[2].code = #child 
* #AUTOPOL ^property[2].valueCode = #UNINSMOT 
* #COL "collision coverage policy"
* #COL ^property[0].code = #parent 
* #COL ^property[0].valueCode = #AUTOPOL 
* #UNINSMOT "uninsured motorist policy"
* #UNINSMOT ^property[0].code = #parent 
* #UNINSMOT ^property[0].valueCode = #AUTOPOL 
* #EHCPOL "extended healthcare"
* #EHCPOL ^property[0].code = #parent 
* #EHCPOL ^property[0].valueCode = #_ActInsurancePolicyCode 
* #EHCPOL ^property[1].code = #parent 
* #EHCPOL ^property[1].valueCode = #_ActInsurancePolicyCode 
* #HSAPOL "health spending account"
* #HSAPOL ^property[0].code = #parent 
* #HSAPOL ^property[0].valueCode = #_ActInsurancePolicyCode 
* #HSAPOL ^property[1].code = #parent 
* #HSAPOL ^property[1].valueCode = #_ActInsurancePolicyCode 
* #PUBLICPOL "public healthcare"
* #PUBLICPOL ^property[0].code = #parent 
* #PUBLICPOL ^property[0].valueCode = #_ActInsurancePolicyCode 
* #PUBLICPOL ^property[1].code = #child 
* #PUBLICPOL ^property[1].valueCode = #DENTPRG 
* #PUBLICPOL ^property[2].code = #child 
* #PUBLICPOL ^property[2].valueCode = #DISEASEPRG 
* #PUBLICPOL ^property[3].code = #child 
* #PUBLICPOL ^property[3].valueCode = #MANDPOL 
* #PUBLICPOL ^property[4].code = #child 
* #PUBLICPOL ^property[4].valueCode = #MENTPRG 
* #PUBLICPOL ^property[5].code = #child 
* #PUBLICPOL ^property[5].valueCode = #SAFNET 
* #PUBLICPOL ^property[6].code = #child 
* #PUBLICPOL ^property[6].valueCode = #SUBPRG 
* #PUBLICPOL ^property[7].code = #child 
* #PUBLICPOL ^property[7].valueCode = #SUBSIDIZ 
* #DENTPRG "dental program"
* #DENTPRG ^property[0].code = #parent 
* #DENTPRG ^property[0].valueCode = #PUBLICPOL 
* #DISEASEPRG "disease program"
* #DISEASEPRG ^property[0].code = #parent 
* #DISEASEPRG ^property[0].valueCode = #PUBLICPOL 
* #DISEASEPRG ^property[1].code = #child 
* #DISEASEPRG ^property[1].valueCode = #CANPRG 
* #DISEASEPRG ^property[2].code = #child 
* #DISEASEPRG ^property[2].valueCode = #ENDRENAL 
* #DISEASEPRG ^property[3].code = #child 
* #DISEASEPRG ^property[3].valueCode = #HIVAIDS 
* #CANPRG "disease program"
* #CANPRG ^property[0].code = #parent 
* #CANPRG ^property[0].valueCode = #DISEASEPRG 
* #ENDRENAL "end renal program"
* #ENDRENAL ^property[0].code = #parent 
* #ENDRENAL ^property[0].valueCode = #DISEASEPRG 
* #HIVAIDS "HIV-AIDS program"
* #HIVAIDS ^property[0].code = #parent 
* #HIVAIDS ^property[0].valueCode = #DISEASEPRG 
* #MANDPOL "mandatory health program"
* #MANDPOL ^property[0].code = #parent 
* #MANDPOL ^property[0].valueCode = #PUBLICPOL 
* #MENTPRG "mental health program"
* #MENTPRG ^property[0].code = #parent 
* #MENTPRG ^property[0].valueCode = #PUBLICPOL 
* #SAFNET "safety net clinic program"
* #SAFNET ^property[0].code = #parent 
* #SAFNET ^property[0].valueCode = #PUBLICPOL 
* #SUBPRG "substance use program"
* #SUBPRG ^property[0].code = #parent 
* #SUBPRG ^property[0].valueCode = #PUBLICPOL 
* #SUBSIDIZ "subsidized health program"
* #SUBSIDIZ ^property[0].code = #parent 
* #SUBSIDIZ ^property[0].valueCode = #PUBLICPOL 
* #SUBSIDIZ ^property[1].code = #child 
* #SUBSIDIZ ^property[1].valueCode = #SUBSIDMC 
* #SUBSIDIZ ^property[2].code = #child 
* #SUBSIDIZ ^property[2].valueCode = #SUBSUPP 
* #SUBSIDMC "subsidized managed care program"
* #SUBSIDMC ^property[0].code = #parent 
* #SUBSIDMC ^property[0].valueCode = #SUBSIDIZ 
* #SUBSUPP "subsidized supplemental health program"
* #SUBSUPP ^property[0].code = #parent 
* #SUBSUPP ^property[0].valueCode = #SUBSIDIZ 
* #_ActInsuranceTypeCode "ActInsuranceTypeCode"
* #_ActInsuranceTypeCode ^property[0].code = #parent 
* #_ActInsuranceTypeCode ^property[0].valueCode = #_ActCoverageTypeCode 
* #_ActInsuranceTypeCode ^property[1].code = #child 
* #_ActInsuranceTypeCode ^property[1].valueCode = #DIS 
* #_ActInsuranceTypeCode ^property[2].code = #child 
* #_ActInsuranceTypeCode ^property[2].valueCode = #EWB 
* #_ActInsuranceTypeCode ^property[3].code = #child 
* #_ActInsuranceTypeCode ^property[3].valueCode = #FLEXP 
* #_ActInsuranceTypeCode ^property[4].code = #child 
* #_ActInsuranceTypeCode ^property[4].valueCode = #LIFE 
* #_ActInsuranceTypeCode ^property[5].code = #child 
* #_ActInsuranceTypeCode ^property[5].valueCode = #PNC 
* #_ActInsuranceTypeCode ^property[6].code = #child 
* #_ActInsuranceTypeCode ^property[6].valueCode = #REI 
* #_ActInsuranceTypeCode ^property[7].code = #child 
* #_ActInsuranceTypeCode ^property[7].valueCode = #SURPL 
* #_ActInsuranceTypeCode ^property[8].code = #child 
* #_ActInsuranceTypeCode ^property[8].valueCode = #UMBRL 
* #_ActInsuranceTypeCode ^property[9].code = #child 
* #_ActInsuranceTypeCode ^property[9].valueCode = #_ActHealthInsuranceTypeCode 
* #DIS "disability insurance policy"
* #DIS ^property[0].code = #parent 
* #DIS ^property[0].valueCode = #_ActInsuranceTypeCode 
* #EWB "employee welfare benefit plan policy"
* #EWB ^property[0].code = #parent 
* #EWB ^property[0].valueCode = #_ActInsuranceTypeCode 
* #FLEXP "flexible benefit plan policy"
* #FLEXP ^property[0].code = #parent 
* #FLEXP ^property[0].valueCode = #_ActInsuranceTypeCode 
* #LIFE "life insurance policy"
* #LIFE ^property[0].code = #parent 
* #LIFE ^property[0].valueCode = #_ActInsuranceTypeCode 
* #LIFE ^property[1].code = #child 
* #LIFE ^property[1].valueCode = #ANNU 
* #LIFE ^property[2].code = #child 
* #LIFE ^property[2].valueCode = #TLIFE 
* #LIFE ^property[3].code = #child 
* #LIFE ^property[3].valueCode = #ULIFE 
* #ANNU "annuity policy"
* #ANNU ^property[0].code = #parent 
* #ANNU ^property[0].valueCode = #LIFE 
* #TLIFE "term life insurance policy"
* #TLIFE ^property[0].code = #parent 
* #TLIFE ^property[0].valueCode = #LIFE 
* #ULIFE "universal life insurance policy"
* #ULIFE ^property[0].code = #parent 
* #ULIFE ^property[0].valueCode = #LIFE 
* #PNC "property and casualty insurance policy"
* #PNC ^property[0].code = #parent 
* #PNC ^property[0].valueCode = #_ActInsuranceTypeCode 
* #REI "reinsurance policy"
* #REI ^property[0].code = #parent 
* #REI ^property[0].valueCode = #_ActInsuranceTypeCode 
* #SURPL "surplus line insurance policy"
* #SURPL ^property[0].code = #parent 
* #SURPL ^property[0].valueCode = #_ActInsuranceTypeCode 
* #UMBRL "umbrella liability insurance policy"
* #UMBRL ^property[0].code = #parent 
* #UMBRL ^property[0].valueCode = #_ActInsuranceTypeCode 
* #_ActHealthInsuranceTypeCode "ActHealthInsuranceTypeCode"
* #_ActHealthInsuranceTypeCode ^property[0].code = #parent 
* #_ActHealthInsuranceTypeCode ^property[0].valueCode = #_ActInsuranceTypeCode 
* #_ActHealthInsuranceTypeCode ^property[1].code = #child 
* #_ActHealthInsuranceTypeCode ^property[1].valueCode = #DENTAL 
* #_ActHealthInsuranceTypeCode ^property[2].code = #child 
* #_ActHealthInsuranceTypeCode ^property[2].valueCode = #DISEASE 
* #_ActHealthInsuranceTypeCode ^property[3].code = #child 
* #_ActHealthInsuranceTypeCode ^property[3].valueCode = #DRUGPOL 
* #_ActHealthInsuranceTypeCode ^property[4].code = #child 
* #_ActHealthInsuranceTypeCode ^property[4].valueCode = #HIP 
* #_ActHealthInsuranceTypeCode ^property[5].code = #child 
* #_ActHealthInsuranceTypeCode ^property[5].valueCode = #LTC 
* #_ActHealthInsuranceTypeCode ^property[6].code = #child 
* #_ActHealthInsuranceTypeCode ^property[6].valueCode = #MCPOL 
* #_ActHealthInsuranceTypeCode ^property[7].code = #child 
* #_ActHealthInsuranceTypeCode ^property[7].valueCode = #MENTPOL 
* #_ActHealthInsuranceTypeCode ^property[8].code = #child 
* #_ActHealthInsuranceTypeCode ^property[8].valueCode = #SUBPOL 
* #_ActHealthInsuranceTypeCode ^property[9].code = #child 
* #_ActHealthInsuranceTypeCode ^property[9].valueCode = #VISPOL 
* #DENTAL "dental care policy"
* #DENTAL ^property[0].code = #parent 
* #DENTAL ^property[0].valueCode = #_ActHealthInsuranceTypeCode 
* #DISEASE "disease specific policy"
* #DISEASE ^property[0].code = #parent 
* #DISEASE ^property[0].valueCode = #_ActHealthInsuranceTypeCode 
* #DRUGPOL "drug policy"
* #DRUGPOL ^property[0].code = #parent 
* #DRUGPOL ^property[0].valueCode = #_ActHealthInsuranceTypeCode 
* #HIP "health insurance plan policy"
* #HIP ^property[0].code = #parent 
* #HIP ^property[0].valueCode = #_ActHealthInsuranceTypeCode 
* #LTC "long term care policy"
* #LTC ^property[0].code = #parent 
* #LTC ^property[0].valueCode = #_ActHealthInsuranceTypeCode 
* #MCPOL "managed care policy"
* #MCPOL ^property[0].code = #parent 
* #MCPOL ^property[0].valueCode = #_ActHealthInsuranceTypeCode 
* #MCPOL ^property[1].code = #child 
* #MCPOL ^property[1].valueCode = #HMO 
* #MCPOL ^property[2].code = #child 
* #MCPOL ^property[2].valueCode = #POS 
* #MCPOL ^property[3].code = #child 
* #MCPOL ^property[3].valueCode = #PPO 
* #HMO "health maintenance organization policy"
* #HMO ^property[0].code = #parent 
* #HMO ^property[0].valueCode = #MCPOL 
* #POS "point of service policy"
* #POS ^property[0].code = #parent 
* #POS ^property[0].valueCode = #MCPOL 
* #POS ^property[1].code = #parent 
* #POS ^property[1].valueCode = #MCPOL 
* #PPO "preferred provider organization policy"
* #PPO ^property[0].code = #parent 
* #PPO ^property[0].valueCode = #MCPOL 
* #MENTPOL "mental health policy"
* #MENTPOL ^property[0].code = #parent 
* #MENTPOL ^property[0].valueCode = #_ActHealthInsuranceTypeCode 
* #SUBPOL "substance use policy"
* #SUBPOL ^property[0].code = #parent 
* #SUBPOL ^property[0].valueCode = #_ActHealthInsuranceTypeCode 
* #VISPOL "vision care policy"
* #VISPOL ^property[0].code = #parent 
* #VISPOL ^property[0].valueCode = #_ActHealthInsuranceTypeCode 
* #_ActProgramTypeCode "ActProgramTypeCode"
* #_ActProgramTypeCode ^property[0].code = #parent 
* #_ActProgramTypeCode ^property[0].valueCode = #_ActCoverageTypeCode 
* #_ActProgramTypeCode ^property[1].code = #child 
* #_ActProgramTypeCode ^property[1].valueCode = #CHAR 
* #_ActProgramTypeCode ^property[2].code = #child 
* #_ActProgramTypeCode ^property[2].valueCode = #CRIME 
* #_ActProgramTypeCode ^property[3].code = #child 
* #_ActProgramTypeCode ^property[3].valueCode = #EAP 
* #_ActProgramTypeCode ^property[4].code = #child 
* #_ActProgramTypeCode ^property[4].valueCode = #GOVEMP 
* #_ActProgramTypeCode ^property[5].code = #child 
* #_ActProgramTypeCode ^property[5].valueCode = #HIRISK 
* #_ActProgramTypeCode ^property[6].code = #child 
* #_ActProgramTypeCode ^property[6].valueCode = #IND 
* #_ActProgramTypeCode ^property[7].code = #child 
* #_ActProgramTypeCode ^property[7].valueCode = #MILITARY 
* #_ActProgramTypeCode ^property[8].code = #child 
* #_ActProgramTypeCode ^property[8].valueCode = #RETIRE 
* #_ActProgramTypeCode ^property[9].code = #child 
* #_ActProgramTypeCode ^property[9].valueCode = #SOCIAL 
* #_ActProgramTypeCode ^property[10].code = #child 
* #_ActProgramTypeCode ^property[10].valueCode = #VET 
* #_ActProgramTypeCode ^property[11].code = #child 
* #_ActProgramTypeCode ^property[11].valueCode = #WCBPOL 
* #CHAR "charity program"
* #CHAR ^property[0].code = #parent 
* #CHAR ^property[0].valueCode = #_ActProgramTypeCode 
* #CRIME "crime victim program"
* #CRIME ^property[0].code = #parent 
* #CRIME ^property[0].valueCode = #_ActProgramTypeCode 
* #EAP "employee assistance program"
* #EAP ^property[0].code = #parent 
* #EAP ^property[0].valueCode = #_ActProgramTypeCode 
* #GOVEMP "government employee health program"
* #GOVEMP ^property[0].code = #parent 
* #GOVEMP ^property[0].valueCode = #_ActProgramTypeCode 
* #HIRISK "high risk pool program"
* #HIRISK ^property[0].code = #parent 
* #HIRISK ^property[0].valueCode = #_ActProgramTypeCode 
* #IND "indigenous peoples health program"
* #IND ^property[0].code = #parent 
* #IND ^property[0].valueCode = #_ActProgramTypeCode 
* #MILITARY "military health program"
* #MILITARY ^property[0].code = #parent 
* #MILITARY ^property[0].valueCode = #_ActProgramTypeCode 
* #RETIRE "retiree health program"
* #RETIRE ^property[0].code = #parent 
* #RETIRE ^property[0].valueCode = #_ActProgramTypeCode 
* #SOCIAL "social service program"
* #SOCIAL ^property[0].code = #parent 
* #SOCIAL ^property[0].valueCode = #_ActProgramTypeCode 
* #VET "veteran health program"
* #VET ^property[0].code = #parent 
* #VET ^property[0].valueCode = #_ActProgramTypeCode 
* #WCBPOL "worker`s compensation"
* #WCBPOL ^property[0].code = #parent 
* #WCBPOL ^property[0].valueCode = #_ActProgramTypeCode 
* #_ActDetectedIssueManagementCode "ActDetectedIssueManagementCode"
* #_ActDetectedIssueManagementCode ^property[0].code = #child 
* #_ActDetectedIssueManagementCode ^property[0].valueCode = #1 
* #_ActDetectedIssueManagementCode ^property[1].code = #child 
* #_ActDetectedIssueManagementCode ^property[1].valueCode = #14 
* #_ActDetectedIssueManagementCode ^property[2].code = #child 
* #_ActDetectedIssueManagementCode ^property[2].valueCode = #8 
* #_ActDetectedIssueManagementCode ^property[3].code = #child 
* #_ActDetectedIssueManagementCode ^property[3].valueCode = #_ActAdministrativeDetectedIssueManagementCode 
* #1 "Therapy Appropriate"
* #1 ^property[0].code = #parent 
* #1 ^property[0].valueCode = #_ActDetectedIssueManagementCode 
* #1 ^property[1].code = #child 
* #1 ^property[1].valueCode = #19 
* #1 ^property[2].code = #child 
* #1 ^property[2].valueCode = #2 
* #1 ^property[3].code = #child 
* #1 ^property[3].valueCode = #22 
* #1 ^property[4].code = #child 
* #1 ^property[4].valueCode = #23 
* #1 ^property[5].code = #child 
* #1 ^property[5].valueCode = #3 
* #1 ^property[6].code = #child 
* #1 ^property[6].valueCode = #4 
* #1 ^property[7].code = #child 
* #1 ^property[7].valueCode = #5 
* #1 ^property[8].code = #child 
* #1 ^property[8].valueCode = #7 
* #19 "Consulted Supplier"
* #19 ^property[0].code = #parent 
* #19 ^property[0].valueCode = #1 
* #2 "Assessed Patient"
* #2 ^property[0].code = #parent 
* #2 ^property[0].valueCode = #1 
* #22 "appropriate indication or diagnosis"
* #22 ^property[0].code = #parent 
* #22 ^property[0].valueCode = #1 
* #23 "prior therapy documented"
* #23 ^property[0].code = #parent 
* #23 ^property[0].valueCode = #1 
* #3 "Patient Explanation"
* #3 ^property[0].code = #parent 
* #3 ^property[0].valueCode = #1 
* #4 "Consulted Other Source"
* #4 ^property[0].code = #parent 
* #4 ^property[0].valueCode = #1 
* #5 "Consulted Prescriber"
* #5 ^property[0].code = #parent 
* #5 ^property[0].valueCode = #1 
* #5 ^property[1].code = #child 
* #5 ^property[1].valueCode = #6 
* #6 "Prescriber Declined Change"
* #6 ^property[0].code = #parent 
* #6 ^property[0].valueCode = #5 
* #7 "Interacting Therapy No Longer Active/Planned"
* #7 ^property[0].code = #parent 
* #7 ^property[0].valueCode = #1 
* #14 "Supply Appropriate"
* #14 ^property[0].code = #parent 
* #14 ^property[0].valueCode = #_ActDetectedIssueManagementCode 
* #14 ^property[1].code = #child 
* #14 ^property[1].valueCode = #15 
* #14 ^property[2].code = #child 
* #14 ^property[2].valueCode = #16 
* #14 ^property[3].code = #child 
* #14 ^property[3].valueCode = #17 
* #14 ^property[4].code = #child 
* #14 ^property[4].valueCode = #18 
* #14 ^property[5].code = #child 
* #14 ^property[5].valueCode = #20 
* #15 "Replacement"
* #15 ^property[0].code = #parent 
* #15 ^property[0].valueCode = #14 
* #16 "Vacation Supply"
* #16 ^property[0].code = #parent 
* #16 ^property[0].valueCode = #14 
* #17 "Weekend Supply"
* #17 ^property[0].code = #parent 
* #17 ^property[0].valueCode = #14 
* #18 "Leave of Absence"
* #18 ^property[0].code = #parent 
* #18 ^property[0].valueCode = #14 
* #20 "additional quantity on separate dispense"
* #20 ^property[0].code = #parent 
* #20 ^property[0].valueCode = #14 
* #8 "Other Action Taken"
* #8 ^property[0].code = #parent 
* #8 ^property[0].valueCode = #_ActDetectedIssueManagementCode 
* #8 ^property[1].code = #child 
* #8 ^property[1].valueCode = #10 
* #8 ^property[2].code = #child 
* #8 ^property[2].valueCode = #11 
* #8 ^property[3].code = #child 
* #8 ^property[3].valueCode = #12 
* #8 ^property[4].code = #child 
* #8 ^property[4].valueCode = #13 
* #8 ^property[5].code = #child 
* #8 ^property[5].valueCode = #9 
* #10 "Provided Patient Education"
* #10 ^property[0].code = #parent 
* #10 ^property[0].valueCode = #8 
* #11 "Added Concurrent Therapy"
* #11 ^property[0].code = #parent 
* #11 ^property[0].valueCode = #8 
* #12 "Temporarily Suspended Concurrent Therapy"
* #12 ^property[0].code = #parent 
* #12 ^property[0].valueCode = #8 
* #13 "Stopped Concurrent Therapy"
* #13 ^property[0].code = #parent 
* #13 ^property[0].valueCode = #8 
* #9 "Instituted Ongoing Monitoring Program"
* #9 ^property[0].code = #parent 
* #9 ^property[0].valueCode = #8 
* #_ActAdministrativeDetectedIssueManagementCode "ActAdministrativeDetectedIssueManagementCode"
* #_ActAdministrativeDetectedIssueManagementCode ^property[0].code = #parent 
* #_ActAdministrativeDetectedIssueManagementCode ^property[0].valueCode = #_ActDetectedIssueManagementCode 
* #_ActAdministrativeDetectedIssueManagementCode ^property[1].code = #child 
* #_ActAdministrativeDetectedIssueManagementCode ^property[1].valueCode = #_AuthorizationIssueManagementCode 
* #_AuthorizationIssueManagementCode "Authorization Issue Management Code"
* #_AuthorizationIssueManagementCode ^property[0].code = #parent 
* #_AuthorizationIssueManagementCode ^property[0].valueCode = #_ActAdministrativeDetectedIssueManagementCode 
* #_AuthorizationIssueManagementCode ^property[1].code = #child 
* #_AuthorizationIssueManagementCode ^property[1].valueCode = #EMAUTH 
* #EMAUTH "emergency authorization override"
* #EMAUTH ^property[0].code = #parent 
* #EMAUTH ^property[0].valueCode = #_AuthorizationIssueManagementCode 
* #EMAUTH ^property[1].code = #child 
* #EMAUTH ^property[1].valueCode = #21 
* #21 "authorization confirmed"
* #21 ^property[0].code = #parent 
* #21 ^property[0].valueCode = #EMAUTH 
* #_ActExposureCode "ActExposureCode"
* #_ActExposureCode ^property[0].code = #child 
* #_ActExposureCode ^property[0].valueCode = #CHLDCARE 
* #_ActExposureCode ^property[1].code = #child 
* #_ActExposureCode ^property[1].valueCode = #CONVEYNC 
* #_ActExposureCode ^property[2].code = #child 
* #_ActExposureCode ^property[2].valueCode = #HLTHCARE 
* #_ActExposureCode ^property[3].code = #child 
* #_ActExposureCode ^property[3].valueCode = #HOMECARE 
* #_ActExposureCode ^property[4].code = #child 
* #_ActExposureCode ^property[4].valueCode = #HOSPPTNT 
* #_ActExposureCode ^property[5].code = #child 
* #_ActExposureCode ^property[5].valueCode = #HOSPVSTR 
* #_ActExposureCode ^property[6].code = #child 
* #_ActExposureCode ^property[6].valueCode = #HOUSEHLD 
* #_ActExposureCode ^property[7].code = #child 
* #_ActExposureCode ^property[7].valueCode = #INMATE 
* #_ActExposureCode ^property[8].code = #child 
* #_ActExposureCode ^property[8].valueCode = #INTIMATE 
* #_ActExposureCode ^property[9].code = #child 
* #_ActExposureCode ^property[9].valueCode = #LTRMCARE 
* #_ActExposureCode ^property[10].code = #child 
* #_ActExposureCode ^property[10].valueCode = #PLACE 
* #_ActExposureCode ^property[11].code = #child 
* #_ActExposureCode ^property[11].valueCode = #PTNTCARE 
* #_ActExposureCode ^property[12].code = #child 
* #_ActExposureCode ^property[12].valueCode = #SCHOOL2 
* #_ActExposureCode ^property[13].code = #child 
* #_ActExposureCode ^property[13].valueCode = #SOCIAL2 
* #_ActExposureCode ^property[14].code = #child 
* #_ActExposureCode ^property[14].valueCode = #SUBSTNCE 
* #_ActExposureCode ^property[15].code = #child 
* #_ActExposureCode ^property[15].valueCode = #TRAVINT 
* #_ActExposureCode ^property[16].code = #child 
* #_ActExposureCode ^property[16].valueCode = #WORK2 
* #CHLDCARE "Day care - Child care Interaction"
* #CHLDCARE ^property[0].code = #parent 
* #CHLDCARE ^property[0].valueCode = #_ActExposureCode 
* #CONVEYNC "Common Conveyance Interaction"
* #CONVEYNC ^property[0].code = #parent 
* #CONVEYNC ^property[0].valueCode = #_ActExposureCode 
* #HLTHCARE "Health Care Interaction - Not Patient Care"
* #HLTHCARE ^property[0].code = #parent 
* #HLTHCARE ^property[0].valueCode = #_ActExposureCode 
* #HOMECARE "Care Giver Interaction"
* #HOMECARE ^property[0].code = #parent 
* #HOMECARE ^property[0].valueCode = #_ActExposureCode 
* #HOSPPTNT "Hospital Patient Interaction"
* #HOSPPTNT ^property[0].code = #parent 
* #HOSPPTNT ^property[0].valueCode = #_ActExposureCode 
* #HOSPVSTR "Hospital Visitor Interaction"
* #HOSPVSTR ^property[0].code = #parent 
* #HOSPVSTR ^property[0].valueCode = #_ActExposureCode 
* #HOUSEHLD "Household Interaction"
* #HOUSEHLD ^property[0].code = #parent 
* #HOUSEHLD ^property[0].valueCode = #_ActExposureCode 
* #INMATE "Inmate Interaction"
* #INMATE ^property[0].code = #parent 
* #INMATE ^property[0].valueCode = #_ActExposureCode 
* #INTIMATE "Intimate Interaction"
* #INTIMATE ^property[0].code = #parent 
* #INTIMATE ^property[0].valueCode = #_ActExposureCode 
* #LTRMCARE "Long Term Care Facility Interaction"
* #LTRMCARE ^property[0].code = #parent 
* #LTRMCARE ^property[0].valueCode = #_ActExposureCode 
* #PLACE "Common Space Interaction"
* #PLACE ^property[0].code = #parent 
* #PLACE ^property[0].valueCode = #_ActExposureCode 
* #PTNTCARE "Health Care Interaction - Patient Care"
* #PTNTCARE ^property[0].code = #parent 
* #PTNTCARE ^property[0].valueCode = #_ActExposureCode 
* #SCHOOL2 "School Interaction"
* #SCHOOL2 ^property[0].code = #parent 
* #SCHOOL2 ^property[0].valueCode = #_ActExposureCode 
* #SOCIAL2 "Social/Extended Family Interaction"
* #SOCIAL2 ^property[0].code = #parent 
* #SOCIAL2 ^property[0].valueCode = #_ActExposureCode 
* #SUBSTNCE "Common Substance Interaction"
* #SUBSTNCE ^property[0].code = #parent 
* #SUBSTNCE ^property[0].valueCode = #_ActExposureCode 
* #TRAVINT "Common Travel Interaction"
* #TRAVINT ^property[0].code = #parent 
* #TRAVINT ^property[0].valueCode = #_ActExposureCode 
* #WORK2 "Work Interaction"
* #WORK2 ^property[0].code = #parent 
* #WORK2 ^property[0].valueCode = #_ActExposureCode 
* #_ActFinancialTransactionCode "ActFinancialTransactionCode"
* #_ActFinancialTransactionCode ^property[0].code = #child 
* #_ActFinancialTransactionCode ^property[0].valueCode = #CHRG 
* #_ActFinancialTransactionCode ^property[1].code = #child 
* #_ActFinancialTransactionCode ^property[1].valueCode = #REV 
* #CHRG "Standard Charge"
* #CHRG ^property[0].code = #parent 
* #CHRG ^property[0].valueCode = #_ActFinancialTransactionCode 
* #REV "Standard Charge Reversal"
* #REV ^property[0].code = #parent 
* #REV ^property[0].valueCode = #_ActFinancialTransactionCode 
* #_ActIncidentCode "ActIncidentCode"
* #_ActIncidentCode ^property[0].code = #child 
* #_ActIncidentCode ^property[0].valueCode = #MVA 
* #_ActIncidentCode ^property[1].code = #child 
* #_ActIncidentCode ^property[1].valueCode = #SCHOOL 
* #_ActIncidentCode ^property[2].code = #child 
* #_ActIncidentCode ^property[2].valueCode = #SPT 
* #_ActIncidentCode ^property[3].code = #child 
* #_ActIncidentCode ^property[3].valueCode = #WPA 
* #MVA "Motor vehicle accident"
* #MVA ^property[0].code = #parent 
* #MVA ^property[0].valueCode = #_ActIncidentCode 
* #SCHOOL "School Accident"
* #SCHOOL ^property[0].code = #parent 
* #SCHOOL ^property[0].valueCode = #_ActIncidentCode 
* #SPT "Sporting Accident"
* #SPT ^property[0].code = #parent 
* #SPT ^property[0].valueCode = #_ActIncidentCode 
* #WPA "Workplace accident"
* #WPA ^property[0].code = #parent 
* #WPA ^property[0].valueCode = #_ActIncidentCode 
* #_ActInformationAccessCode "ActInformationAccessCode"
* #_ActInformationAccessCode ^property[0].code = #child 
* #_ActInformationAccessCode ^property[0].valueCode = #ACADR 
* #_ActInformationAccessCode ^property[1].code = #child 
* #_ActInformationAccessCode ^property[1].valueCode = #ACALL 
* #_ActInformationAccessCode ^property[2].code = #child 
* #_ActInformationAccessCode ^property[2].valueCode = #ACALLG 
* #_ActInformationAccessCode ^property[3].code = #child 
* #_ActInformationAccessCode ^property[3].valueCode = #ACCONS 
* #_ActInformationAccessCode ^property[4].code = #child 
* #_ActInformationAccessCode ^property[4].valueCode = #ACDEMO 
* #_ActInformationAccessCode ^property[5].code = #child 
* #_ActInformationAccessCode ^property[5].valueCode = #ACDI 
* #_ActInformationAccessCode ^property[6].code = #child 
* #_ActInformationAccessCode ^property[6].valueCode = #ACIMMUN 
* #_ActInformationAccessCode ^property[7].code = #child 
* #_ActInformationAccessCode ^property[7].valueCode = #ACLAB 
* #_ActInformationAccessCode ^property[8].code = #child 
* #_ActInformationAccessCode ^property[8].valueCode = #ACMED 
* #_ActInformationAccessCode ^property[9].code = #child 
* #_ActInformationAccessCode ^property[9].valueCode = #ACMEDC 
* #_ActInformationAccessCode ^property[10].code = #child 
* #_ActInformationAccessCode ^property[10].valueCode = #ACMEN 
* #_ActInformationAccessCode ^property[11].code = #child 
* #_ActInformationAccessCode ^property[11].valueCode = #ACOBS 
* #_ActInformationAccessCode ^property[12].code = #child 
* #_ActInformationAccessCode ^property[12].valueCode = #ACPOLPRG 
* #_ActInformationAccessCode ^property[13].code = #child 
* #_ActInformationAccessCode ^property[13].valueCode = #ACPROV 
* #_ActInformationAccessCode ^property[14].code = #child 
* #_ActInformationAccessCode ^property[14].valueCode = #ACPSERV 
* #_ActInformationAccessCode ^property[15].code = #child 
* #_ActInformationAccessCode ^property[15].valueCode = #ACSUBSTAB 
* #ACADR "adverse drug reaction access"
* #ACADR ^property[0].code = #parent 
* #ACADR ^property[0].valueCode = #_ActInformationAccessCode 
* #ACALL "all access"
* #ACALL ^property[0].code = #parent 
* #ACALL ^property[0].valueCode = #_ActInformationAccessCode 
* #ACALLG "allergy access"
* #ACALLG ^property[0].code = #parent 
* #ACALLG ^property[0].valueCode = #_ActInformationAccessCode 
* #ACCONS "informational consent access"
* #ACCONS ^property[0].code = #parent 
* #ACCONS ^property[0].valueCode = #_ActInformationAccessCode 
* #ACDEMO "demographics access"
* #ACDEMO ^property[0].code = #parent 
* #ACDEMO ^property[0].valueCode = #_ActInformationAccessCode 
* #ACDI "diagnostic imaging access"
* #ACDI ^property[0].code = #parent 
* #ACDI ^property[0].valueCode = #_ActInformationAccessCode 
* #ACIMMUN "immunization access"
* #ACIMMUN ^property[0].code = #parent 
* #ACIMMUN ^property[0].valueCode = #_ActInformationAccessCode 
* #ACLAB "lab test result access"
* #ACLAB ^property[0].code = #parent 
* #ACLAB ^property[0].valueCode = #_ActInformationAccessCode 
* #ACMED "medication access"
* #ACMED ^property[0].code = #parent 
* #ACMED ^property[0].valueCode = #_ActInformationAccessCode 
* #ACMEDC "medical condition access"
* #ACMEDC ^property[0].code = #parent 
* #ACMEDC ^property[0].valueCode = #_ActInformationAccessCode 
* #ACMEN "mental health access"
* #ACMEN ^property[0].code = #parent 
* #ACMEN ^property[0].valueCode = #_ActInformationAccessCode 
* #ACOBS "common observations access"
* #ACOBS ^property[0].code = #parent 
* #ACOBS ^property[0].valueCode = #_ActInformationAccessCode 
* #ACPOLPRG "policy or program information access"
* #ACPOLPRG ^property[0].code = #parent 
* #ACPOLPRG ^property[0].valueCode = #_ActInformationAccessCode 
* #ACPROV "provider information access"
* #ACPROV ^property[0].code = #parent 
* #ACPROV ^property[0].valueCode = #_ActInformationAccessCode 
* #ACPSERV "professional service access"
* #ACPSERV ^property[0].code = #parent 
* #ACPSERV ^property[0].valueCode = #_ActInformationAccessCode 
* #ACSUBSTAB "substance abuse access"
* #ACSUBSTAB ^property[0].code = #parent 
* #ACSUBSTAB ^property[0].valueCode = #_ActInformationAccessCode 
* #_ActInformationAccessContextCode "ActInformationAccessContextCode"
* #_ActInformationAccessContextCode ^property[0].code = #child 
* #_ActInformationAccessContextCode ^property[0].valueCode = #INFAUT 
* #_ActInformationAccessContextCode ^property[1].code = #child 
* #_ActInformationAccessContextCode ^property[1].valueCode = #INFCON 
* #_ActInformationAccessContextCode ^property[2].code = #child 
* #_ActInformationAccessContextCode ^property[2].valueCode = #INFCRT 
* #_ActInformationAccessContextCode ^property[3].code = #child 
* #_ActInformationAccessContextCode ^property[3].valueCode = #INFDNG 
* #_ActInformationAccessContextCode ^property[4].code = #child 
* #_ActInformationAccessContextCode ^property[4].valueCode = #INFEMER 
* #_ActInformationAccessContextCode ^property[5].code = #child 
* #_ActInformationAccessContextCode ^property[5].valueCode = #INFPWR 
* #_ActInformationAccessContextCode ^property[6].code = #child 
* #_ActInformationAccessContextCode ^property[6].valueCode = #INFREG 
* #INFAUT "authorized information transfer"
* #INFAUT ^property[0].code = #parent 
* #INFAUT ^property[0].valueCode = #_ActInformationAccessContextCode 
* #INFCON "after explicit consent"
* #INFCON ^property[0].code = #parent 
* #INFCON ^property[0].valueCode = #_ActInformationAccessContextCode 
* #INFCRT "only on court order"
* #INFCRT ^property[0].code = #parent 
* #INFCRT ^property[0].valueCode = #_ActInformationAccessContextCode 
* #INFDNG "only if danger to others"
* #INFDNG ^property[0].code = #parent 
* #INFDNG ^property[0].valueCode = #_ActInformationAccessContextCode 
* #INFEMER "only in an emergency"
* #INFEMER ^property[0].code = #parent 
* #INFEMER ^property[0].valueCode = #_ActInformationAccessContextCode 
* #INFPWR "only if public welfare risk"
* #INFPWR ^property[0].code = #parent 
* #INFPWR ^property[0].valueCode = #_ActInformationAccessContextCode 
* #INFREG "regulatory information transfer"
* #INFREG ^property[0].code = #parent 
* #INFREG ^property[0].valueCode = #_ActInformationAccessContextCode 
* #_ActInformationCategoryCode "ActInformationCategoryCode"
* #_ActInformationCategoryCode ^property[0].code = #child 
* #_ActInformationCategoryCode ^property[0].valueCode = #ALLCAT 
* #_ActInformationCategoryCode ^property[1].code = #child 
* #_ActInformationCategoryCode ^property[1].valueCode = #ALLGCAT 
* #_ActInformationCategoryCode ^property[2].code = #child 
* #_ActInformationCategoryCode ^property[2].valueCode = #ARCAT 
* #_ActInformationCategoryCode ^property[3].code = #child 
* #_ActInformationCategoryCode ^property[3].valueCode = #COBSCAT 
* #_ActInformationCategoryCode ^property[4].code = #child 
* #_ActInformationCategoryCode ^property[4].valueCode = #DEMOCAT 
* #_ActInformationCategoryCode ^property[5].code = #child 
* #_ActInformationCategoryCode ^property[5].valueCode = #DICAT 
* #_ActInformationCategoryCode ^property[6].code = #child 
* #_ActInformationCategoryCode ^property[6].valueCode = #IMMUCAT 
* #_ActInformationCategoryCode ^property[7].code = #child 
* #_ActInformationCategoryCode ^property[7].valueCode = #LABCAT 
* #_ActInformationCategoryCode ^property[8].code = #child 
* #_ActInformationCategoryCode ^property[8].valueCode = #MEDCCAT 
* #_ActInformationCategoryCode ^property[9].code = #child 
* #_ActInformationCategoryCode ^property[9].valueCode = #MENCAT 
* #_ActInformationCategoryCode ^property[10].code = #child 
* #_ActInformationCategoryCode ^property[10].valueCode = #PSVCCAT 
* #_ActInformationCategoryCode ^property[11].code = #child 
* #_ActInformationCategoryCode ^property[11].valueCode = #RXCAT 
* #ALLCAT "all categories"
* #ALLCAT ^property[0].code = #parent 
* #ALLCAT ^property[0].valueCode = #_ActInformationCategoryCode 
* #ALLGCAT "allergy category"
* #ALLGCAT ^property[0].code = #parent 
* #ALLGCAT ^property[0].valueCode = #_ActInformationCategoryCode 
* #ARCAT "adverse drug reaction category"
* #ARCAT ^property[0].code = #parent 
* #ARCAT ^property[0].valueCode = #_ActInformationCategoryCode 
* #COBSCAT "common observation category"
* #COBSCAT ^property[0].code = #parent 
* #COBSCAT ^property[0].valueCode = #_ActInformationCategoryCode 
* #DEMOCAT "demographics category"
* #DEMOCAT ^property[0].code = #parent 
* #DEMOCAT ^property[0].valueCode = #_ActInformationCategoryCode 
* #DICAT "diagnostic image category"
* #DICAT ^property[0].code = #parent 
* #DICAT ^property[0].valueCode = #_ActInformationCategoryCode 
* #IMMUCAT "immunization category"
* #IMMUCAT ^property[0].code = #parent 
* #IMMUCAT ^property[0].valueCode = #_ActInformationCategoryCode 
* #LABCAT "lab test category"
* #LABCAT ^property[0].code = #parent 
* #LABCAT ^property[0].valueCode = #_ActInformationCategoryCode 
* #MEDCCAT "medical condition category"
* #MEDCCAT ^property[0].code = #parent 
* #MEDCCAT ^property[0].valueCode = #_ActInformationCategoryCode 
* #MENCAT "mental health category"
* #MENCAT ^property[0].code = #parent 
* #MENCAT ^property[0].valueCode = #_ActInformationCategoryCode 
* #PSVCCAT "professional service category"
* #PSVCCAT ^property[0].code = #parent 
* #PSVCCAT ^property[0].valueCode = #_ActInformationCategoryCode 
* #RXCAT "medication category"
* #RXCAT ^property[0].code = #parent 
* #RXCAT ^property[0].valueCode = #_ActInformationCategoryCode 
* #_ActInvoiceElementCode "ActInvoiceElementCode"
* #_ActInvoiceElementCode ^property[0].code = #child 
* #_ActInvoiceElementCode ^property[0].valueCode = #_ActInvoiceAdjudicationPaymentCode 
* #_ActInvoiceElementCode ^property[1].code = #child 
* #_ActInvoiceElementCode ^property[1].valueCode = #_ActInvoiceDetailCode 
* #_ActInvoiceElementCode ^property[2].code = #child 
* #_ActInvoiceElementCode ^property[2].valueCode = #_ActInvoiceGroupCode 
* #_ActInvoiceAdjudicationPaymentCode "ActInvoiceAdjudicationPaymentCode"
* #_ActInvoiceAdjudicationPaymentCode ^property[0].code = #parent 
* #_ActInvoiceAdjudicationPaymentCode ^property[0].valueCode = #_ActInvoiceElementCode 
* #_ActInvoiceAdjudicationPaymentCode ^property[1].code = #child 
* #_ActInvoiceAdjudicationPaymentCode ^property[1].valueCode = #_ActInvoiceAdjudicationPaymentGroupCode 
* #_ActInvoiceAdjudicationPaymentCode ^property[2].code = #child 
* #_ActInvoiceAdjudicationPaymentCode ^property[2].valueCode = #_ActInvoiceAdjudicationPaymentSummaryCode 
* #_ActInvoiceAdjudicationPaymentGroupCode "ActInvoiceAdjudicationPaymentGroupCode"
* #_ActInvoiceAdjudicationPaymentGroupCode ^property[0].code = #parent 
* #_ActInvoiceAdjudicationPaymentGroupCode ^property[0].valueCode = #_ActInvoiceAdjudicationPaymentCode 
* #_ActInvoiceAdjudicationPaymentGroupCode ^property[1].code = #child 
* #_ActInvoiceAdjudicationPaymentGroupCode ^property[1].valueCode = #ALEC 
* #_ActInvoiceAdjudicationPaymentGroupCode ^property[2].code = #child 
* #_ActInvoiceAdjudicationPaymentGroupCode ^property[2].valueCode = #BONUS 
* #_ActInvoiceAdjudicationPaymentGroupCode ^property[3].code = #child 
* #_ActInvoiceAdjudicationPaymentGroupCode ^property[3].valueCode = #CFWD 
* #_ActInvoiceAdjudicationPaymentGroupCode ^property[4].code = #child 
* #_ActInvoiceAdjudicationPaymentGroupCode ^property[4].valueCode = #EDU 
* #_ActInvoiceAdjudicationPaymentGroupCode ^property[5].code = #child 
* #_ActInvoiceAdjudicationPaymentGroupCode ^property[5].valueCode = #EPYMT 
* #_ActInvoiceAdjudicationPaymentGroupCode ^property[6].code = #child 
* #_ActInvoiceAdjudicationPaymentGroupCode ^property[6].valueCode = #GARN 
* #_ActInvoiceAdjudicationPaymentGroupCode ^property[7].code = #child 
* #_ActInvoiceAdjudicationPaymentGroupCode ^property[7].valueCode = #INVOICE 
* #_ActInvoiceAdjudicationPaymentGroupCode ^property[8].code = #child 
* #_ActInvoiceAdjudicationPaymentGroupCode ^property[8].valueCode = #PINV 
* #_ActInvoiceAdjudicationPaymentGroupCode ^property[9].code = #child 
* #_ActInvoiceAdjudicationPaymentGroupCode ^property[9].valueCode = #PPRD 
* #_ActInvoiceAdjudicationPaymentGroupCode ^property[10].code = #child 
* #_ActInvoiceAdjudicationPaymentGroupCode ^property[10].valueCode = #PROA 
* #_ActInvoiceAdjudicationPaymentGroupCode ^property[11].code = #child 
* #_ActInvoiceAdjudicationPaymentGroupCode ^property[11].valueCode = #RECOV 
* #_ActInvoiceAdjudicationPaymentGroupCode ^property[12].code = #child 
* #_ActInvoiceAdjudicationPaymentGroupCode ^property[12].valueCode = #RETRO 
* #_ActInvoiceAdjudicationPaymentGroupCode ^property[13].code = #child 
* #_ActInvoiceAdjudicationPaymentGroupCode ^property[13].valueCode = #TRAN 
* #ALEC "alternate electronic"
* #ALEC ^property[0].code = #parent 
* #ALEC ^property[0].valueCode = #_ActInvoiceAdjudicationPaymentGroupCode 
* #BONUS "bonus"
* #BONUS ^property[0].code = #parent 
* #BONUS ^property[0].valueCode = #_ActInvoiceAdjudicationPaymentGroupCode 
* #CFWD "carry forward adjusment"
* #CFWD ^property[0].code = #parent 
* #CFWD ^property[0].valueCode = #_ActInvoiceAdjudicationPaymentGroupCode 
* #EDU "education fees"
* #EDU ^property[0].code = #parent 
* #EDU ^property[0].valueCode = #_ActInvoiceAdjudicationPaymentGroupCode 
* #EPYMT "early payment fee"
* #EPYMT ^property[0].code = #parent 
* #EPYMT ^property[0].valueCode = #_ActInvoiceAdjudicationPaymentGroupCode 
* #GARN "garnishee"
* #GARN ^property[0].code = #parent 
* #GARN ^property[0].valueCode = #_ActInvoiceAdjudicationPaymentGroupCode 
* #INVOICE "submitted invoice"
* #INVOICE ^property[0].code = #parent 
* #INVOICE ^property[0].valueCode = #_ActInvoiceAdjudicationPaymentGroupCode 
* #PINV "paper invoice"
* #PINV ^property[0].code = #parent 
* #PINV ^property[0].valueCode = #_ActInvoiceAdjudicationPaymentGroupCode 
* #PPRD "prior period adjustment"
* #PPRD ^property[0].code = #parent 
* #PPRD ^property[0].valueCode = #_ActInvoiceAdjudicationPaymentGroupCode 
* #PROA "professional association deduction"
* #PROA ^property[0].code = #parent 
* #PROA ^property[0].valueCode = #_ActInvoiceAdjudicationPaymentGroupCode 
* #RECOV "recovery"
* #RECOV ^property[0].code = #parent 
* #RECOV ^property[0].valueCode = #_ActInvoiceAdjudicationPaymentGroupCode 
* #RETRO "retro adjustment"
* #RETRO ^property[0].code = #parent 
* #RETRO ^property[0].valueCode = #_ActInvoiceAdjudicationPaymentGroupCode 
* #TRAN "transaction fee"
* #TRAN ^property[0].code = #parent 
* #TRAN ^property[0].valueCode = #_ActInvoiceAdjudicationPaymentGroupCode 
* #_ActInvoiceAdjudicationPaymentSummaryCode "ActInvoiceAdjudicationPaymentSummaryCode"
* #_ActInvoiceAdjudicationPaymentSummaryCode ^property[0].code = #parent 
* #_ActInvoiceAdjudicationPaymentSummaryCode ^property[0].valueCode = #_ActInvoiceAdjudicationPaymentCode 
* #_ActInvoiceAdjudicationPaymentSummaryCode ^property[1].code = #child 
* #_ActInvoiceAdjudicationPaymentSummaryCode ^property[1].valueCode = #INVTYPE 
* #_ActInvoiceAdjudicationPaymentSummaryCode ^property[2].code = #child 
* #_ActInvoiceAdjudicationPaymentSummaryCode ^property[2].valueCode = #PAYEE 
* #_ActInvoiceAdjudicationPaymentSummaryCode ^property[3].code = #child 
* #_ActInvoiceAdjudicationPaymentSummaryCode ^property[3].valueCode = #PAYOR 
* #_ActInvoiceAdjudicationPaymentSummaryCode ^property[4].code = #child 
* #_ActInvoiceAdjudicationPaymentSummaryCode ^property[4].valueCode = #SENDAPP 
* #INVTYPE "invoice type"
* #INVTYPE ^property[0].code = #parent 
* #INVTYPE ^property[0].valueCode = #_ActInvoiceAdjudicationPaymentSummaryCode 
* #PAYEE "payee"
* #PAYEE ^property[0].code = #parent 
* #PAYEE ^property[0].valueCode = #_ActInvoiceAdjudicationPaymentSummaryCode 
* #PAYOR "payor"
* #PAYOR ^property[0].code = #parent 
* #PAYOR ^property[0].valueCode = #_ActInvoiceAdjudicationPaymentSummaryCode 
* #SENDAPP "sending application"
* #SENDAPP ^property[0].code = #parent 
* #SENDAPP ^property[0].valueCode = #_ActInvoiceAdjudicationPaymentSummaryCode 
* #_ActInvoiceDetailCode "ActInvoiceDetailCode"
* #_ActInvoiceDetailCode ^property[0].code = #parent 
* #_ActInvoiceDetailCode ^property[0].valueCode = #_ActInvoiceElementCode 
* #_ActInvoiceDetailCode ^property[1].code = #child 
* #_ActInvoiceDetailCode ^property[1].valueCode = #_ActInvoiceDetailClinicalProductCode 
* #_ActInvoiceDetailCode ^property[2].code = #child 
* #_ActInvoiceDetailCode ^property[2].valueCode = #_ActInvoiceDetailDrugProductCode 
* #_ActInvoiceDetailCode ^property[3].code = #child 
* #_ActInvoiceDetailCode ^property[3].valueCode = #_ActInvoiceDetailGenericCode 
* #_ActInvoiceDetailCode ^property[4].code = #child 
* #_ActInvoiceDetailCode ^property[4].valueCode = #_ActInvoiceDetailPreferredAccommodationCode 
* #_ActInvoiceDetailClinicalProductCode "ActInvoiceDetailClinicalProductCode"
* #_ActInvoiceDetailClinicalProductCode ^property[0].code = #parent 
* #_ActInvoiceDetailClinicalProductCode ^property[0].valueCode = #_ActInvoiceDetailCode 
* #_ActInvoiceDetailClinicalProductCode ^property[1].code = #child 
* #_ActInvoiceDetailClinicalProductCode ^property[1].valueCode = #UNSPSC 
* #UNSPSC "United Nations Standard Products and Services Classification"
* #UNSPSC ^property[0].code = #parent 
* #UNSPSC ^property[0].valueCode = #_ActInvoiceDetailClinicalProductCode 
* #_ActInvoiceDetailDrugProductCode "ActInvoiceDetailDrugProductCode"
* #_ActInvoiceDetailDrugProductCode ^property[0].code = #parent 
* #_ActInvoiceDetailDrugProductCode ^property[0].valueCode = #_ActInvoiceDetailCode 
* #_ActInvoiceDetailDrugProductCode ^property[1].code = #child 
* #_ActInvoiceDetailDrugProductCode ^property[1].valueCode = #GTIN 
* #_ActInvoiceDetailDrugProductCode ^property[2].code = #child 
* #_ActInvoiceDetailDrugProductCode ^property[2].valueCode = #UPC 
* #GTIN "Global Trade Item Number"
* #GTIN ^property[0].code = #parent 
* #GTIN ^property[0].valueCode = #_ActInvoiceDetailDrugProductCode 
* #UPC "Universal Product Code"
* #UPC ^property[0].code = #parent 
* #UPC ^property[0].valueCode = #_ActInvoiceDetailDrugProductCode 
* #_ActInvoiceDetailGenericCode "ActInvoiceDetailGenericCode"
* #_ActInvoiceDetailGenericCode ^property[0].code = #parent 
* #_ActInvoiceDetailGenericCode ^property[0].valueCode = #_ActInvoiceDetailCode 
* #_ActInvoiceDetailGenericCode ^property[1].code = #child 
* #_ActInvoiceDetailGenericCode ^property[1].valueCode = #_ActInvoiceDetailGenericAdjudicatorCode 
* #_ActInvoiceDetailGenericCode ^property[2].code = #child 
* #_ActInvoiceDetailGenericCode ^property[2].valueCode = #_ActInvoiceDetailGenericModifierCode 
* #_ActInvoiceDetailGenericCode ^property[3].code = #child 
* #_ActInvoiceDetailGenericCode ^property[3].valueCode = #_ActInvoiceDetailGenericProviderCode 
* #_ActInvoiceDetailGenericCode ^property[4].code = #child 
* #_ActInvoiceDetailGenericCode ^property[4].valueCode = #_ActInvoiceDetailTaxCode 
* #_ActInvoiceDetailGenericAdjudicatorCode "ActInvoiceDetailGenericAdjudicatorCode"
* #_ActInvoiceDetailGenericAdjudicatorCode ^property[0].code = #parent 
* #_ActInvoiceDetailGenericAdjudicatorCode ^property[0].valueCode = #_ActInvoiceDetailGenericCode 
* #_ActInvoiceDetailGenericAdjudicatorCode ^property[1].code = #child 
* #_ActInvoiceDetailGenericAdjudicatorCode ^property[1].valueCode = #COIN 
* #_ActInvoiceDetailGenericAdjudicatorCode ^property[2].code = #child 
* #_ActInvoiceDetailGenericAdjudicatorCode ^property[2].valueCode = #COPAYMENT 
* #_ActInvoiceDetailGenericAdjudicatorCode ^property[3].code = #child 
* #_ActInvoiceDetailGenericAdjudicatorCode ^property[3].valueCode = #DEDUCTIBLE 
* #_ActInvoiceDetailGenericAdjudicatorCode ^property[4].code = #child 
* #_ActInvoiceDetailGenericAdjudicatorCode ^property[4].valueCode = #PAY 
* #_ActInvoiceDetailGenericAdjudicatorCode ^property[5].code = #child 
* #_ActInvoiceDetailGenericAdjudicatorCode ^property[5].valueCode = #SPEND 
* #COIN "coinsurance"
* #COIN ^property[0].code = #parent 
* #COIN ^property[0].valueCode = #_ActInvoiceDetailGenericAdjudicatorCode 
* #COPAYMENT "patient co-pay"
* #COPAYMENT ^property[0].code = #parent 
* #COPAYMENT ^property[0].valueCode = #_ActInvoiceDetailGenericAdjudicatorCode 
* #DEDUCTIBLE "deductible"
* #DEDUCTIBLE ^property[0].code = #parent 
* #DEDUCTIBLE ^property[0].valueCode = #_ActInvoiceDetailGenericAdjudicatorCode 
* #PAY "payment"
* #PAY ^property[0].code = #parent 
* #PAY ^property[0].valueCode = #_ActInvoiceDetailGenericAdjudicatorCode 
* #SPEND "spend down"
* #SPEND ^property[0].code = #parent 
* #SPEND ^property[0].valueCode = #_ActInvoiceDetailGenericAdjudicatorCode 
* #_ActInvoiceDetailGenericModifierCode "ActInvoiceDetailGenericModifierCode"
* #_ActInvoiceDetailGenericModifierCode ^property[0].code = #parent 
* #_ActInvoiceDetailGenericModifierCode ^property[0].valueCode = #_ActInvoiceDetailGenericCode 
* #_ActInvoiceDetailGenericModifierCode ^property[1].code = #child 
* #_ActInvoiceDetailGenericModifierCode ^property[1].valueCode = #AFTHRS 
* #_ActInvoiceDetailGenericModifierCode ^property[2].code = #child 
* #_ActInvoiceDetailGenericModifierCode ^property[2].valueCode = #ISOL 
* #_ActInvoiceDetailGenericModifierCode ^property[3].code = #child 
* #_ActInvoiceDetailGenericModifierCode ^property[3].valueCode = #OOO 
* #AFTHRS "non-normal hours"
* #AFTHRS ^property[0].code = #parent 
* #AFTHRS ^property[0].valueCode = #_ActInvoiceDetailGenericModifierCode 
* #ISOL "isolation allowance"
* #ISOL ^property[0].code = #parent 
* #ISOL ^property[0].valueCode = #_ActInvoiceDetailGenericModifierCode 
* #OOO "out of office"
* #OOO ^property[0].code = #parent 
* #OOO ^property[0].valueCode = #_ActInvoiceDetailGenericModifierCode 
* #_ActInvoiceDetailGenericProviderCode "ActInvoiceDetailGenericProviderCode"
* #_ActInvoiceDetailGenericProviderCode ^property[0].code = #parent 
* #_ActInvoiceDetailGenericProviderCode ^property[0].valueCode = #_ActInvoiceDetailGenericCode 
* #_ActInvoiceDetailGenericProviderCode ^property[1].code = #child 
* #_ActInvoiceDetailGenericProviderCode ^property[1].valueCode = #CANCAPT 
* #_ActInvoiceDetailGenericProviderCode ^property[2].code = #child 
* #_ActInvoiceDetailGenericProviderCode ^property[2].valueCode = #DSC 
* #_ActInvoiceDetailGenericProviderCode ^property[3].code = #child 
* #_ActInvoiceDetailGenericProviderCode ^property[3].valueCode = #ESA 
* #_ActInvoiceDetailGenericProviderCode ^property[4].code = #child 
* #_ActInvoiceDetailGenericProviderCode ^property[4].valueCode = #FFSTOP 
* #_ActInvoiceDetailGenericProviderCode ^property[5].code = #child 
* #_ActInvoiceDetailGenericProviderCode ^property[5].valueCode = #FNLFEE 
* #_ActInvoiceDetailGenericProviderCode ^property[6].code = #child 
* #_ActInvoiceDetailGenericProviderCode ^property[6].valueCode = #FRSTFEE 
* #_ActInvoiceDetailGenericProviderCode ^property[7].code = #child 
* #_ActInvoiceDetailGenericProviderCode ^property[7].valueCode = #MARKUP 
* #_ActInvoiceDetailGenericProviderCode ^property[8].code = #child 
* #_ActInvoiceDetailGenericProviderCode ^property[8].valueCode = #MISSAPT 
* #_ActInvoiceDetailGenericProviderCode ^property[9].code = #child 
* #_ActInvoiceDetailGenericProviderCode ^property[9].valueCode = #PERFEE 
* #_ActInvoiceDetailGenericProviderCode ^property[10].code = #child 
* #_ActInvoiceDetailGenericProviderCode ^property[10].valueCode = #PERMBNS 
* #_ActInvoiceDetailGenericProviderCode ^property[11].code = #child 
* #_ActInvoiceDetailGenericProviderCode ^property[11].valueCode = #RESTOCK 
* #_ActInvoiceDetailGenericProviderCode ^property[12].code = #child 
* #_ActInvoiceDetailGenericProviderCode ^property[12].valueCode = #TRAVEL 
* #_ActInvoiceDetailGenericProviderCode ^property[13].code = #child 
* #_ActInvoiceDetailGenericProviderCode ^property[13].valueCode = #URGENT 
* #CANCAPT "cancelled appointment"
* #CANCAPT ^property[0].code = #parent 
* #CANCAPT ^property[0].valueCode = #_ActInvoiceDetailGenericProviderCode 
* #DSC "discount"
* #DSC ^property[0].code = #parent 
* #DSC ^property[0].valueCode = #_ActInvoiceDetailGenericProviderCode 
* #ESA "extraordinary service assessment"
* #ESA ^property[0].code = #parent 
* #ESA ^property[0].valueCode = #_ActInvoiceDetailGenericProviderCode 
* #FFSTOP "fee for service top off"
* #FFSTOP ^property[0].code = #parent 
* #FFSTOP ^property[0].valueCode = #_ActInvoiceDetailGenericProviderCode 
* #FNLFEE "final fee"
* #FNLFEE ^property[0].code = #parent 
* #FNLFEE ^property[0].valueCode = #_ActInvoiceDetailGenericProviderCode 
* #FRSTFEE "first fee"
* #FRSTFEE ^property[0].code = #parent 
* #FRSTFEE ^property[0].valueCode = #_ActInvoiceDetailGenericProviderCode 
* #MARKUP "markup or up-charge"
* #MARKUP ^property[0].code = #parent 
* #MARKUP ^property[0].valueCode = #_ActInvoiceDetailGenericProviderCode 
* #MISSAPT "missed appointment"
* #MISSAPT ^property[0].code = #parent 
* #MISSAPT ^property[0].valueCode = #_ActInvoiceDetailGenericProviderCode 
* #PERFEE "periodic fee"
* #PERFEE ^property[0].code = #parent 
* #PERFEE ^property[0].valueCode = #_ActInvoiceDetailGenericProviderCode 
* #PERMBNS "performance bonus"
* #PERMBNS ^property[0].code = #parent 
* #PERMBNS ^property[0].valueCode = #_ActInvoiceDetailGenericProviderCode 
* #RESTOCK "restocking fee"
* #RESTOCK ^property[0].code = #parent 
* #RESTOCK ^property[0].valueCode = #_ActInvoiceDetailGenericProviderCode 
* #TRAVEL "travel"
* #TRAVEL ^property[0].code = #parent 
* #TRAVEL ^property[0].valueCode = #_ActInvoiceDetailGenericProviderCode 
* #URGENT "urgent"
* #URGENT ^property[0].code = #parent 
* #URGENT ^property[0].valueCode = #_ActInvoiceDetailGenericProviderCode 
* #_ActInvoiceDetailTaxCode "ActInvoiceDetailTaxCode"
* #_ActInvoiceDetailTaxCode ^property[0].code = #parent 
* #_ActInvoiceDetailTaxCode ^property[0].valueCode = #_ActInvoiceDetailGenericCode 
* #_ActInvoiceDetailTaxCode ^property[1].code = #child 
* #_ActInvoiceDetailTaxCode ^property[1].valueCode = #FST 
* #_ActInvoiceDetailTaxCode ^property[2].code = #child 
* #_ActInvoiceDetailTaxCode ^property[2].valueCode = #HST 
* #_ActInvoiceDetailTaxCode ^property[3].code = #child 
* #_ActInvoiceDetailTaxCode ^property[3].valueCode = #PST 
* #FST "federal sales tax"
* #FST ^property[0].code = #parent 
* #FST ^property[0].valueCode = #_ActInvoiceDetailTaxCode 
* #HST "harmonized sales Tax"
* #HST ^property[0].code = #parent 
* #HST ^property[0].valueCode = #_ActInvoiceDetailTaxCode 
* #PST "provincial/state sales tax"
* #PST ^property[0].code = #parent 
* #PST ^property[0].valueCode = #_ActInvoiceDetailTaxCode 
* #_ActInvoiceDetailPreferredAccommodationCode "ActInvoiceDetailPreferredAccommodationCode"
* #_ActInvoiceDetailPreferredAccommodationCode ^property[0].code = #parent 
* #_ActInvoiceDetailPreferredAccommodationCode ^property[0].valueCode = #_ActInvoiceDetailCode 
* #_ActInvoiceDetailPreferredAccommodationCode ^property[1].code = #child 
* #_ActInvoiceDetailPreferredAccommodationCode ^property[1].valueCode = #_ActEncounterAccommodationCode 
* #_ActEncounterAccommodationCode "ActEncounterAccommodationCode"
* #_ActEncounterAccommodationCode ^property[0].code = #parent 
* #_ActEncounterAccommodationCode ^property[0].valueCode = #_ActInvoiceDetailPreferredAccommodationCode 
* #_ActEncounterAccommodationCode ^property[1].code = #child 
* #_ActEncounterAccommodationCode ^property[1].valueCode = #_HL7AccommodationCode 
* #_HL7AccommodationCode "HL7AccommodationCode"
* #_HL7AccommodationCode ^property[0].code = #parent 
* #_HL7AccommodationCode ^property[0].valueCode = #_ActEncounterAccommodationCode 
* #_HL7AccommodationCode ^property[1].code = #child 
* #_HL7AccommodationCode ^property[1].valueCode = #I 
* #_HL7AccommodationCode ^property[2].code = #child 
* #_HL7AccommodationCode ^property[2].valueCode = #P 
* #_HL7AccommodationCode ^property[3].code = #child 
* #_HL7AccommodationCode ^property[3].valueCode = #S 
* #_HL7AccommodationCode ^property[4].code = #child 
* #_HL7AccommodationCode ^property[4].valueCode = #SP 
* #_HL7AccommodationCode ^property[5].code = #child 
* #_HL7AccommodationCode ^property[5].valueCode = #W 
* #I "Isolation"
* #I ^property[0].code = #parent 
* #I ^property[0].valueCode = #_HL7AccommodationCode 
* #P "Private"
* #P ^property[0].code = #parent 
* #P ^property[0].valueCode = #_HL7AccommodationCode 
* #S "Suite"
* #S ^property[0].code = #parent 
* #S ^property[0].valueCode = #_HL7AccommodationCode 
* #SP "Semi-private"
* #SP ^property[0].code = #parent 
* #SP ^property[0].valueCode = #_HL7AccommodationCode 
* #W "Ward"
* #W ^property[0].code = #parent 
* #W ^property[0].valueCode = #_HL7AccommodationCode 
* #_ActInvoiceGroupCode "ActInvoiceGroupCode"
* #_ActInvoiceGroupCode ^property[0].code = #parent 
* #_ActInvoiceGroupCode ^property[0].valueCode = #_ActInvoiceElementCode 
* #_ActInvoiceGroupCode ^property[1].code = #child 
* #_ActInvoiceGroupCode ^property[1].valueCode = #_ActInvoiceInterGroupCode 
* #_ActInvoiceGroupCode ^property[2].code = #child 
* #_ActInvoiceGroupCode ^property[2].valueCode = #_ActInvoiceRootGroupCode 
* #_ActInvoiceInterGroupCode "ActInvoiceInterGroupCode"
* #_ActInvoiceInterGroupCode ^property[0].code = #parent 
* #_ActInvoiceInterGroupCode ^property[0].valueCode = #_ActInvoiceGroupCode 
* #_ActInvoiceInterGroupCode ^property[1].code = #child 
* #_ActInvoiceInterGroupCode ^property[1].valueCode = #CPNDDRGING 
* #_ActInvoiceInterGroupCode ^property[2].code = #child 
* #_ActInvoiceInterGroupCode ^property[2].valueCode = #CPNDINDING 
* #_ActInvoiceInterGroupCode ^property[3].code = #child 
* #_ActInvoiceInterGroupCode ^property[3].valueCode = #CPNDSUPING 
* #_ActInvoiceInterGroupCode ^property[4].code = #child 
* #_ActInvoiceInterGroupCode ^property[4].valueCode = #DRUGING 
* #_ActInvoiceInterGroupCode ^property[5].code = #child 
* #_ActInvoiceInterGroupCode ^property[5].valueCode = #FRAMEING 
* #_ActInvoiceInterGroupCode ^property[6].code = #child 
* #_ActInvoiceInterGroupCode ^property[6].valueCode = #LENSING 
* #_ActInvoiceInterGroupCode ^property[7].code = #child 
* #_ActInvoiceInterGroupCode ^property[7].valueCode = #PRDING 
* #CPNDDRGING "compound drug invoice group"
* #CPNDDRGING ^property[0].code = #parent 
* #CPNDDRGING ^property[0].valueCode = #_ActInvoiceInterGroupCode 
* #CPNDINDING "compound ingredient invoice group"
* #CPNDINDING ^property[0].code = #parent 
* #CPNDINDING ^property[0].valueCode = #_ActInvoiceInterGroupCode 
* #CPNDSUPING "compound supply invoice group"
* #CPNDSUPING ^property[0].code = #parent 
* #CPNDSUPING ^property[0].valueCode = #_ActInvoiceInterGroupCode 
* #DRUGING "drug invoice group"
* #DRUGING ^property[0].code = #parent 
* #DRUGING ^property[0].valueCode = #_ActInvoiceInterGroupCode 
* #FRAMEING "frame invoice group"
* #FRAMEING ^property[0].code = #parent 
* #FRAMEING ^property[0].valueCode = #_ActInvoiceInterGroupCode 
* #LENSING "lens invoice group"
* #LENSING ^property[0].code = #parent 
* #LENSING ^property[0].valueCode = #_ActInvoiceInterGroupCode 
* #PRDING "product invoice group"
* #PRDING ^property[0].code = #parent 
* #PRDING ^property[0].valueCode = #_ActInvoiceInterGroupCode 
* #_ActInvoiceRootGroupCode "ActInvoiceRootGroupCode"
* #_ActInvoiceRootGroupCode ^property[0].code = #parent 
* #_ActInvoiceRootGroupCode ^property[0].valueCode = #_ActInvoiceGroupCode 
* #_ActInvoiceRootGroupCode ^property[1].code = #child 
* #_ActInvoiceRootGroupCode ^property[1].valueCode = #CPINV 
* #_ActInvoiceRootGroupCode ^property[2].code = #child 
* #_ActInvoiceRootGroupCode ^property[2].valueCode = #CSINV 
* #_ActInvoiceRootGroupCode ^property[3].code = #child 
* #_ActInvoiceRootGroupCode ^property[3].valueCode = #CSPINV 
* #_ActInvoiceRootGroupCode ^property[4].code = #child 
* #_ActInvoiceRootGroupCode ^property[4].valueCode = #FININV 
* #_ActInvoiceRootGroupCode ^property[5].code = #child 
* #_ActInvoiceRootGroupCode ^property[5].valueCode = #OHSINV 
* #_ActInvoiceRootGroupCode ^property[6].code = #child 
* #_ActInvoiceRootGroupCode ^property[6].valueCode = #PAINV 
* #_ActInvoiceRootGroupCode ^property[7].code = #child 
* #_ActInvoiceRootGroupCode ^property[7].valueCode = #RXCINV 
* #_ActInvoiceRootGroupCode ^property[8].code = #child 
* #_ActInvoiceRootGroupCode ^property[8].valueCode = #RXDINV 
* #_ActInvoiceRootGroupCode ^property[9].code = #child 
* #_ActInvoiceRootGroupCode ^property[9].valueCode = #SBFINV 
* #_ActInvoiceRootGroupCode ^property[10].code = #child 
* #_ActInvoiceRootGroupCode ^property[10].valueCode = #VRXINV 
* #CPINV "clinical product invoice"
* #CPINV ^property[0].code = #parent 
* #CPINV ^property[0].valueCode = #_ActInvoiceRootGroupCode 
* #CSINV "clinical service invoice"
* #CSINV ^property[0].code = #parent 
* #CSINV ^property[0].valueCode = #_ActInvoiceRootGroupCode 
* #CSPINV "clinical service and product"
* #CSPINV ^property[0].code = #parent 
* #CSPINV ^property[0].valueCode = #_ActInvoiceRootGroupCode 
* #FININV "financial invoice"
* #FININV ^property[0].code = #parent 
* #FININV ^property[0].valueCode = #_ActInvoiceRootGroupCode 
* #OHSINV "oral health service"
* #OHSINV ^property[0].code = #parent 
* #OHSINV ^property[0].valueCode = #_ActInvoiceRootGroupCode 
* #PAINV "preferred accommodation invoice"
* #PAINV ^property[0].code = #parent 
* #PAINV ^property[0].valueCode = #_ActInvoiceRootGroupCode 
* #RXCINV "Rx compound invoice"
* #RXCINV ^property[0].code = #parent 
* #RXCINV ^property[0].valueCode = #_ActInvoiceRootGroupCode 
* #RXDINV "Rx dispense invoice"
* #RXDINV ^property[0].code = #parent 
* #RXDINV ^property[0].valueCode = #_ActInvoiceRootGroupCode 
* #SBFINV "sessional or block fee invoice"
* #SBFINV ^property[0].code = #parent 
* #SBFINV ^property[0].valueCode = #_ActInvoiceRootGroupCode 
* #VRXINV "vision dispense invoice"
* #VRXINV ^property[0].code = #parent 
* #VRXINV ^property[0].valueCode = #_ActInvoiceRootGroupCode 
* #_ActInvoiceElementSummaryCode "ActInvoiceElementSummaryCode"
* #_ActInvoiceElementSummaryCode ^property[0].code = #child 
* #_ActInvoiceElementSummaryCode ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #_ActInvoiceElementSummaryCode ^property[1].code = #child 
* #_ActInvoiceElementSummaryCode ^property[1].valueCode = #_InvoiceElementPaid 
* #_ActInvoiceElementSummaryCode ^property[2].code = #child 
* #_ActInvoiceElementSummaryCode ^property[2].valueCode = #_InvoiceElementSubmitted 
* #_InvoiceElementAdjudicated "InvoiceElementAdjudicated"
* #_InvoiceElementAdjudicated ^property[0].code = #parent 
* #_InvoiceElementAdjudicated ^property[0].valueCode = #_ActInvoiceElementSummaryCode 
* #_InvoiceElementAdjudicated ^property[1].code = #child 
* #_InvoiceElementAdjudicated ^property[1].valueCode = #ADNFPPELAT 
* #_InvoiceElementAdjudicated ^property[2].code = #child 
* #_InvoiceElementAdjudicated ^property[2].valueCode = #ADNFPPELCT 
* #_InvoiceElementAdjudicated ^property[3].code = #child 
* #_InvoiceElementAdjudicated ^property[3].valueCode = #ADNFPPMNAT 
* #_InvoiceElementAdjudicated ^property[4].code = #child 
* #_InvoiceElementAdjudicated ^property[4].valueCode = #ADNFPPMNCT 
* #_InvoiceElementAdjudicated ^property[5].code = #child 
* #_InvoiceElementAdjudicated ^property[5].valueCode = #ADNFSPELAT 
* #_InvoiceElementAdjudicated ^property[6].code = #child 
* #_InvoiceElementAdjudicated ^property[6].valueCode = #ADNFSPELCT 
* #_InvoiceElementAdjudicated ^property[7].code = #child 
* #_InvoiceElementAdjudicated ^property[7].valueCode = #ADNFSPMNAT 
* #_InvoiceElementAdjudicated ^property[8].code = #child 
* #_InvoiceElementAdjudicated ^property[8].valueCode = #ADNFSPMNCT 
* #_InvoiceElementAdjudicated ^property[9].code = #child 
* #_InvoiceElementAdjudicated ^property[9].valueCode = #ADNPPPELAT 
* #_InvoiceElementAdjudicated ^property[10].code = #child 
* #_InvoiceElementAdjudicated ^property[10].valueCode = #ADNPPPELCT 
* #_InvoiceElementAdjudicated ^property[11].code = #child 
* #_InvoiceElementAdjudicated ^property[11].valueCode = #ADNPPPMNAT 
* #_InvoiceElementAdjudicated ^property[12].code = #child 
* #_InvoiceElementAdjudicated ^property[12].valueCode = #ADNPPPMNCT 
* #_InvoiceElementAdjudicated ^property[13].code = #child 
* #_InvoiceElementAdjudicated ^property[13].valueCode = #ADNPSPELAT 
* #_InvoiceElementAdjudicated ^property[14].code = #child 
* #_InvoiceElementAdjudicated ^property[14].valueCode = #ADNPSPELCT 
* #_InvoiceElementAdjudicated ^property[15].code = #child 
* #_InvoiceElementAdjudicated ^property[15].valueCode = #ADNPSPMNAT 
* #_InvoiceElementAdjudicated ^property[16].code = #child 
* #_InvoiceElementAdjudicated ^property[16].valueCode = #ADNPSPMNCT 
* #_InvoiceElementAdjudicated ^property[17].code = #child 
* #_InvoiceElementAdjudicated ^property[17].valueCode = #ADPPPPELAT 
* #_InvoiceElementAdjudicated ^property[18].code = #child 
* #_InvoiceElementAdjudicated ^property[18].valueCode = #ADPPPPELCT 
* #_InvoiceElementAdjudicated ^property[19].code = #child 
* #_InvoiceElementAdjudicated ^property[19].valueCode = #ADPPPPMNAT 
* #_InvoiceElementAdjudicated ^property[20].code = #child 
* #_InvoiceElementAdjudicated ^property[20].valueCode = #ADPPPPMNCT 
* #_InvoiceElementAdjudicated ^property[21].code = #child 
* #_InvoiceElementAdjudicated ^property[21].valueCode = #ADPPSPELAT 
* #_InvoiceElementAdjudicated ^property[22].code = #child 
* #_InvoiceElementAdjudicated ^property[22].valueCode = #ADPPSPELCT 
* #_InvoiceElementAdjudicated ^property[23].code = #child 
* #_InvoiceElementAdjudicated ^property[23].valueCode = #ADPPSPMNAT 
* #_InvoiceElementAdjudicated ^property[24].code = #child 
* #_InvoiceElementAdjudicated ^property[24].valueCode = #ADPPSPMNCT 
* #_InvoiceElementAdjudicated ^property[25].code = #child 
* #_InvoiceElementAdjudicated ^property[25].valueCode = #ADRFPPELAT 
* #_InvoiceElementAdjudicated ^property[26].code = #child 
* #_InvoiceElementAdjudicated ^property[26].valueCode = #ADRFPPELCT 
* #_InvoiceElementAdjudicated ^property[27].code = #child 
* #_InvoiceElementAdjudicated ^property[27].valueCode = #ADRFPPMNAT 
* #_InvoiceElementAdjudicated ^property[28].code = #child 
* #_InvoiceElementAdjudicated ^property[28].valueCode = #ADRFPPMNCT 
* #_InvoiceElementAdjudicated ^property[29].code = #child 
* #_InvoiceElementAdjudicated ^property[29].valueCode = #ADRFSPELAT 
* #_InvoiceElementAdjudicated ^property[30].code = #child 
* #_InvoiceElementAdjudicated ^property[30].valueCode = #ADRFSPELCT 
* #_InvoiceElementAdjudicated ^property[31].code = #child 
* #_InvoiceElementAdjudicated ^property[31].valueCode = #ADRFSPMNAT 
* #_InvoiceElementAdjudicated ^property[32].code = #child 
* #_InvoiceElementAdjudicated ^property[32].valueCode = #ADRFSPMNCT 
* #ADNFPPELAT "adjud. nullified prior-period electronic amount"
* #ADNFPPELAT ^property[0].code = #parent 
* #ADNFPPELAT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADNFPPELCT "adjud. nullified prior-period electronic count"
* #ADNFPPELCT ^property[0].code = #parent 
* #ADNFPPELCT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADNFPPMNAT "adjud. nullified prior-period manual amount"
* #ADNFPPMNAT ^property[0].code = #parent 
* #ADNFPPMNAT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADNFPPMNCT "adjud. nullified prior-period manual count"
* #ADNFPPMNCT ^property[0].code = #parent 
* #ADNFPPMNCT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADNFSPELAT "adjud. nullified same-period electronic amount"
* #ADNFSPELAT ^property[0].code = #parent 
* #ADNFSPELAT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADNFSPELCT "adjud. nullified same-period electronic count"
* #ADNFSPELCT ^property[0].code = #parent 
* #ADNFSPELCT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADNFSPMNAT "adjud. nullified same-period manual amount"
* #ADNFSPMNAT ^property[0].code = #parent 
* #ADNFSPMNAT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADNFSPMNCT "adjud. nullified same-period manual count"
* #ADNFSPMNCT ^property[0].code = #parent 
* #ADNFSPMNCT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADNPPPELAT "adjud. non-payee payable prior-period electronic amount"
* #ADNPPPELAT ^property[0].code = #parent 
* #ADNPPPELAT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADNPPPELCT "adjud. non-payee payable prior-period electronic count"
* #ADNPPPELCT ^property[0].code = #parent 
* #ADNPPPELCT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADNPPPMNAT "adjud. non-payee payable prior-period manual amount"
* #ADNPPPMNAT ^property[0].code = #parent 
* #ADNPPPMNAT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADNPPPMNCT "adjud. non-payee payable prior-period manual count"
* #ADNPPPMNCT ^property[0].code = #parent 
* #ADNPPPMNCT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADNPSPELAT "adjud. non-payee payable same-period electronic amount"
* #ADNPSPELAT ^property[0].code = #parent 
* #ADNPSPELAT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADNPSPELCT "adjud. non-payee payable same-period electronic count"
* #ADNPSPELCT ^property[0].code = #parent 
* #ADNPSPELCT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADNPSPMNAT "adjud. non-payee payable same-period manual amount"
* #ADNPSPMNAT ^property[0].code = #parent 
* #ADNPSPMNAT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADNPSPMNCT "adjud. non-payee payable same-period manual count"
* #ADNPSPMNCT ^property[0].code = #parent 
* #ADNPSPMNCT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADPPPPELAT "adjud. payee payable prior-period electronic amount"
* #ADPPPPELAT ^property[0].code = #parent 
* #ADPPPPELAT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADPPPPELCT "adjud. payee payable prior-period electronic count"
* #ADPPPPELCT ^property[0].code = #parent 
* #ADPPPPELCT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADPPPPMNAT "adjud. payee payable prior-period manual amout"
* #ADPPPPMNAT ^property[0].code = #parent 
* #ADPPPPMNAT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADPPPPMNCT "adjud. payee payable prior-period manual count"
* #ADPPPPMNCT ^property[0].code = #parent 
* #ADPPPPMNCT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADPPSPELAT "adjud. payee payable same-period electronic amount"
* #ADPPSPELAT ^property[0].code = #parent 
* #ADPPSPELAT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADPPSPELCT "adjud. payee payable same-period electronic count"
* #ADPPSPELCT ^property[0].code = #parent 
* #ADPPSPELCT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADPPSPMNAT "adjud. payee payable same-period manual amount"
* #ADPPSPMNAT ^property[0].code = #parent 
* #ADPPSPMNAT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADPPSPMNCT "adjud. payee payable same-period manual count"
* #ADPPSPMNCT ^property[0].code = #parent 
* #ADPPSPMNCT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADRFPPELAT "adjud. refused prior-period electronic amount"
* #ADRFPPELAT ^property[0].code = #parent 
* #ADRFPPELAT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADRFPPELCT "adjud. refused prior-period electronic count"
* #ADRFPPELCT ^property[0].code = #parent 
* #ADRFPPELCT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADRFPPMNAT "adjud. refused prior-period manual amount"
* #ADRFPPMNAT ^property[0].code = #parent 
* #ADRFPPMNAT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADRFPPMNCT "adjud. refused prior-period manual count"
* #ADRFPPMNCT ^property[0].code = #parent 
* #ADRFPPMNCT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADRFSPELAT "adjud. refused same-period electronic amount"
* #ADRFSPELAT ^property[0].code = #parent 
* #ADRFSPELAT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADRFSPELCT "adjud. refused same-period electronic count"
* #ADRFSPELCT ^property[0].code = #parent 
* #ADRFSPELCT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADRFSPMNAT "adjud. refused same-period manual amount"
* #ADRFSPMNAT ^property[0].code = #parent 
* #ADRFSPMNAT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #ADRFSPMNCT "adjud. refused same-period manual count"
* #ADRFSPMNCT ^property[0].code = #parent 
* #ADRFSPMNCT ^property[0].valueCode = #_InvoiceElementAdjudicated 
* #_InvoiceElementPaid "InvoiceElementPaid"
* #_InvoiceElementPaid ^property[0].code = #parent 
* #_InvoiceElementPaid ^property[0].valueCode = #_ActInvoiceElementSummaryCode 
* #_InvoiceElementPaid ^property[1].code = #child 
* #_InvoiceElementPaid ^property[1].valueCode = #PDNFPPELAT 
* #_InvoiceElementPaid ^property[2].code = #child 
* #_InvoiceElementPaid ^property[2].valueCode = #PDNFPPELCT 
* #_InvoiceElementPaid ^property[3].code = #child 
* #_InvoiceElementPaid ^property[3].valueCode = #PDNFPPMNAT 
* #_InvoiceElementPaid ^property[4].code = #child 
* #_InvoiceElementPaid ^property[4].valueCode = #PDNFPPMNCT 
* #_InvoiceElementPaid ^property[5].code = #child 
* #_InvoiceElementPaid ^property[5].valueCode = #PDNFSPELAT 
* #_InvoiceElementPaid ^property[6].code = #child 
* #_InvoiceElementPaid ^property[6].valueCode = #PDNFSPELCT 
* #_InvoiceElementPaid ^property[7].code = #child 
* #_InvoiceElementPaid ^property[7].valueCode = #PDNFSPMNAT 
* #_InvoiceElementPaid ^property[8].code = #child 
* #_InvoiceElementPaid ^property[8].valueCode = #PDNFSPMNCT 
* #_InvoiceElementPaid ^property[9].code = #child 
* #_InvoiceElementPaid ^property[9].valueCode = #PDNPPPELAT 
* #_InvoiceElementPaid ^property[10].code = #child 
* #_InvoiceElementPaid ^property[10].valueCode = #PDNPPPELCT 
* #_InvoiceElementPaid ^property[11].code = #child 
* #_InvoiceElementPaid ^property[11].valueCode = #PDNPPPMNAT 
* #_InvoiceElementPaid ^property[12].code = #child 
* #_InvoiceElementPaid ^property[12].valueCode = #PDNPPPMNCT 
* #_InvoiceElementPaid ^property[13].code = #child 
* #_InvoiceElementPaid ^property[13].valueCode = #PDNPSPELAT 
* #_InvoiceElementPaid ^property[14].code = #child 
* #_InvoiceElementPaid ^property[14].valueCode = #PDNPSPELCT 
* #_InvoiceElementPaid ^property[15].code = #child 
* #_InvoiceElementPaid ^property[15].valueCode = #PDNPSPMNAT 
* #_InvoiceElementPaid ^property[16].code = #child 
* #_InvoiceElementPaid ^property[16].valueCode = #PDNPSPMNCT 
* #_InvoiceElementPaid ^property[17].code = #child 
* #_InvoiceElementPaid ^property[17].valueCode = #PDPPPPELAT 
* #_InvoiceElementPaid ^property[18].code = #child 
* #_InvoiceElementPaid ^property[18].valueCode = #PDPPPPELCT 
* #_InvoiceElementPaid ^property[19].code = #child 
* #_InvoiceElementPaid ^property[19].valueCode = #PDPPPPMNAT 
* #_InvoiceElementPaid ^property[20].code = #child 
* #_InvoiceElementPaid ^property[20].valueCode = #PDPPPPMNCT 
* #_InvoiceElementPaid ^property[21].code = #child 
* #_InvoiceElementPaid ^property[21].valueCode = #PDPPSPELAT 
* #_InvoiceElementPaid ^property[22].code = #child 
* #_InvoiceElementPaid ^property[22].valueCode = #PDPPSPELCT 
* #_InvoiceElementPaid ^property[23].code = #child 
* #_InvoiceElementPaid ^property[23].valueCode = #PDPPSPMNAT 
* #_InvoiceElementPaid ^property[24].code = #child 
* #_InvoiceElementPaid ^property[24].valueCode = #PDPPSPMNCT 
* #PDNFPPELAT "paid nullified prior-period electronic amount"
* #PDNFPPELAT ^property[0].code = #parent 
* #PDNFPPELAT ^property[0].valueCode = #_InvoiceElementPaid 
* #PDNFPPELCT "paid nullified prior-period electronic count"
* #PDNFPPELCT ^property[0].code = #parent 
* #PDNFPPELCT ^property[0].valueCode = #_InvoiceElementPaid 
* #PDNFPPMNAT "paid nullified prior-period manual amount"
* #PDNFPPMNAT ^property[0].code = #parent 
* #PDNFPPMNAT ^property[0].valueCode = #_InvoiceElementPaid 
* #PDNFPPMNCT "paid nullified prior-period manual count"
* #PDNFPPMNCT ^property[0].code = #parent 
* #PDNFPPMNCT ^property[0].valueCode = #_InvoiceElementPaid 
* #PDNFSPELAT "paid nullified same-period electronic amount"
* #PDNFSPELAT ^property[0].code = #parent 
* #PDNFSPELAT ^property[0].valueCode = #_InvoiceElementPaid 
* #PDNFSPELCT "paid nullified same-period electronic count"
* #PDNFSPELCT ^property[0].code = #parent 
* #PDNFSPELCT ^property[0].valueCode = #_InvoiceElementPaid 
* #PDNFSPMNAT "paid nullified same-period manual amount"
* #PDNFSPMNAT ^property[0].code = #parent 
* #PDNFSPMNAT ^property[0].valueCode = #_InvoiceElementPaid 
* #PDNFSPMNCT "paid nullified same-period manual count"
* #PDNFSPMNCT ^property[0].code = #parent 
* #PDNFSPMNCT ^property[0].valueCode = #_InvoiceElementPaid 
* #PDNPPPELAT "paid non-payee payable prior-period electronic amount"
* #PDNPPPELAT ^property[0].code = #parent 
* #PDNPPPELAT ^property[0].valueCode = #_InvoiceElementPaid 
* #PDNPPPELCT "paid non-payee payable prior-period electronic count"
* #PDNPPPELCT ^property[0].code = #parent 
* #PDNPPPELCT ^property[0].valueCode = #_InvoiceElementPaid 
* #PDNPPPMNAT "paid non-payee payable prior-period manual amount"
* #PDNPPPMNAT ^property[0].code = #parent 
* #PDNPPPMNAT ^property[0].valueCode = #_InvoiceElementPaid 
* #PDNPPPMNCT "paid non-payee payable prior-period manual count"
* #PDNPPPMNCT ^property[0].code = #parent 
* #PDNPPPMNCT ^property[0].valueCode = #_InvoiceElementPaid 
* #PDNPSPELAT "paid non-payee payable same-period electronic amount"
* #PDNPSPELAT ^property[0].code = #parent 
* #PDNPSPELAT ^property[0].valueCode = #_InvoiceElementPaid 
* #PDNPSPELCT "paid non-payee payable same-period electronic count"
* #PDNPSPELCT ^property[0].code = #parent 
* #PDNPSPELCT ^property[0].valueCode = #_InvoiceElementPaid 
* #PDNPSPMNAT "paid non-payee payable same-period manual amount"
* #PDNPSPMNAT ^property[0].code = #parent 
* #PDNPSPMNAT ^property[0].valueCode = #_InvoiceElementPaid 
* #PDNPSPMNCT "paid non-payee payable same-period manual count"
* #PDNPSPMNCT ^property[0].code = #parent 
* #PDNPSPMNCT ^property[0].valueCode = #_InvoiceElementPaid 
* #PDPPPPELAT "paid payee payable prior-period electronic amount"
* #PDPPPPELAT ^property[0].code = #parent 
* #PDPPPPELAT ^property[0].valueCode = #_InvoiceElementPaid 
* #PDPPPPELCT "paid payee payable prior-period electronic count"
* #PDPPPPELCT ^property[0].code = #parent 
* #PDPPPPELCT ^property[0].valueCode = #_InvoiceElementPaid 
* #PDPPPPMNAT "paid payee payable prior-period manual amount"
* #PDPPPPMNAT ^property[0].code = #parent 
* #PDPPPPMNAT ^property[0].valueCode = #_InvoiceElementPaid 
* #PDPPPPMNCT "paid payee payable prior-period manual count"
* #PDPPPPMNCT ^property[0].code = #parent 
* #PDPPPPMNCT ^property[0].valueCode = #_InvoiceElementPaid 
* #PDPPSPELAT "paid payee payable same-period electronic amount"
* #PDPPSPELAT ^property[0].code = #parent 
* #PDPPSPELAT ^property[0].valueCode = #_InvoiceElementPaid 
* #PDPPSPELCT "paid payee payable same-period electronic count"
* #PDPPSPELCT ^property[0].code = #parent 
* #PDPPSPELCT ^property[0].valueCode = #_InvoiceElementPaid 
* #PDPPSPMNAT "paid payee payable same-period manual amount"
* #PDPPSPMNAT ^property[0].code = #parent 
* #PDPPSPMNAT ^property[0].valueCode = #_InvoiceElementPaid 
* #PDPPSPMNCT "paid payee payable same-period manual count"
* #PDPPSPMNCT ^property[0].code = #parent 
* #PDPPSPMNCT ^property[0].valueCode = #_InvoiceElementPaid 
* #_InvoiceElementSubmitted "InvoiceElementSubmitted"
* #_InvoiceElementSubmitted ^property[0].code = #parent 
* #_InvoiceElementSubmitted ^property[0].valueCode = #_ActInvoiceElementSummaryCode 
* #_InvoiceElementSubmitted ^property[1].code = #child 
* #_InvoiceElementSubmitted ^property[1].valueCode = #SBBLELAT 
* #_InvoiceElementSubmitted ^property[2].code = #child 
* #_InvoiceElementSubmitted ^property[2].valueCode = #SBBLELCT 
* #_InvoiceElementSubmitted ^property[3].code = #child 
* #_InvoiceElementSubmitted ^property[3].valueCode = #SBNFELAT 
* #_InvoiceElementSubmitted ^property[4].code = #child 
* #_InvoiceElementSubmitted ^property[4].valueCode = #SBNFELCT 
* #_InvoiceElementSubmitted ^property[5].code = #child 
* #_InvoiceElementSubmitted ^property[5].valueCode = #SBPDELAT 
* #_InvoiceElementSubmitted ^property[6].code = #child 
* #_InvoiceElementSubmitted ^property[6].valueCode = #SBPDELCT 
* #SBBLELAT "submitted billed electronic amount"
* #SBBLELAT ^property[0].code = #parent 
* #SBBLELAT ^property[0].valueCode = #_InvoiceElementSubmitted 
* #SBBLELCT "submitted billed electronic count"
* #SBBLELCT ^property[0].code = #parent 
* #SBBLELCT ^property[0].valueCode = #_InvoiceElementSubmitted 
* #SBNFELAT "submitted nullified electronic amount"
* #SBNFELAT ^property[0].code = #parent 
* #SBNFELAT ^property[0].valueCode = #_InvoiceElementSubmitted 
* #SBNFELCT "submitted cancelled electronic count"
* #SBNFELCT ^property[0].code = #parent 
* #SBNFELCT ^property[0].valueCode = #_InvoiceElementSubmitted 
* #SBPDELAT "submitted pending electronic amount"
* #SBPDELAT ^property[0].code = #parent 
* #SBPDELAT ^property[0].valueCode = #_InvoiceElementSubmitted 
* #SBPDELCT "submitted pending electronic count"
* #SBPDELCT ^property[0].code = #parent 
* #SBPDELCT ^property[0].valueCode = #_InvoiceElementSubmitted 
* #_ActInvoiceOverrideCode "ActInvoiceOverrideCode"
* #_ActInvoiceOverrideCode ^property[0].code = #child 
* #_ActInvoiceOverrideCode ^property[0].valueCode = #COVGE 
* #_ActInvoiceOverrideCode ^property[1].code = #child 
* #_ActInvoiceOverrideCode ^property[1].valueCode = #EFORM 
* #_ActInvoiceOverrideCode ^property[2].code = #child 
* #_ActInvoiceOverrideCode ^property[2].valueCode = #FAX 
* #_ActInvoiceOverrideCode ^property[3].code = #child 
* #_ActInvoiceOverrideCode ^property[3].valueCode = #GFTH 
* #_ActInvoiceOverrideCode ^property[4].code = #child 
* #_ActInvoiceOverrideCode ^property[4].valueCode = #LATE 
* #_ActInvoiceOverrideCode ^property[5].code = #child 
* #_ActInvoiceOverrideCode ^property[5].valueCode = #MANUAL 
* #_ActInvoiceOverrideCode ^property[6].code = #child 
* #_ActInvoiceOverrideCode ^property[6].valueCode = #OOJ 
* #_ActInvoiceOverrideCode ^property[7].code = #child 
* #_ActInvoiceOverrideCode ^property[7].valueCode = #ORTHO 
* #_ActInvoiceOverrideCode ^property[8].code = #child 
* #_ActInvoiceOverrideCode ^property[8].valueCode = #PAPER 
* #_ActInvoiceOverrideCode ^property[9].code = #child 
* #_ActInvoiceOverrideCode ^property[9].valueCode = #PIE 
* #_ActInvoiceOverrideCode ^property[10].code = #child 
* #_ActInvoiceOverrideCode ^property[10].valueCode = #PYRDELAY 
* #_ActInvoiceOverrideCode ^property[11].code = #child 
* #_ActInvoiceOverrideCode ^property[11].valueCode = #REFNR 
* #_ActInvoiceOverrideCode ^property[12].code = #child 
* #_ActInvoiceOverrideCode ^property[12].valueCode = #REPSERV 
* #_ActInvoiceOverrideCode ^property[13].code = #child 
* #_ActInvoiceOverrideCode ^property[13].valueCode = #UNRELAT 
* #_ActInvoiceOverrideCode ^property[14].code = #child 
* #_ActInvoiceOverrideCode ^property[14].valueCode = #VERBAUTH 
* #COVGE "coverage problem"
* #COVGE ^property[0].code = #parent 
* #COVGE ^property[0].valueCode = #_ActInvoiceOverrideCode 
* #EFORM "electronic form to follow"
* #EFORM ^property[0].code = #parent 
* #EFORM ^property[0].valueCode = #_ActInvoiceOverrideCode 
* #FAX "fax to follow"
* #FAX ^property[0].code = #parent 
* #FAX ^property[0].valueCode = #_ActInvoiceOverrideCode 
* #GFTH "good faith indicator"
* #GFTH ^property[0].code = #parent 
* #GFTH ^property[0].valueCode = #_ActInvoiceOverrideCode 
* #LATE "late invoice"
* #LATE ^property[0].code = #parent 
* #LATE ^property[0].valueCode = #_ActInvoiceOverrideCode 
* #MANUAL "manual review"
* #MANUAL ^property[0].code = #parent 
* #MANUAL ^property[0].valueCode = #_ActInvoiceOverrideCode 
* #OOJ "out of jurisdiction"
* #OOJ ^property[0].code = #parent 
* #OOJ ^property[0].valueCode = #_ActInvoiceOverrideCode 
* #ORTHO "orthodontic service"
* #ORTHO ^property[0].code = #parent 
* #ORTHO ^property[0].valueCode = #_ActInvoiceOverrideCode 
* #PAPER "paper documentation to follow"
* #PAPER ^property[0].code = #parent 
* #PAPER ^property[0].valueCode = #_ActInvoiceOverrideCode 
* #PIE "public insurance exhausted"
* #PIE ^property[0].code = #parent 
* #PIE ^property[0].valueCode = #_ActInvoiceOverrideCode 
* #PYRDELAY "delayed by a previous payor"
* #PYRDELAY ^property[0].code = #parent 
* #PYRDELAY ^property[0].valueCode = #_ActInvoiceOverrideCode 
* #REFNR "referral not required"
* #REFNR ^property[0].code = #parent 
* #REFNR ^property[0].valueCode = #_ActInvoiceOverrideCode 
* #REPSERV "repeated service"
* #REPSERV ^property[0].code = #parent 
* #REPSERV ^property[0].valueCode = #_ActInvoiceOverrideCode 
* #UNRELAT "unrelated service"
* #UNRELAT ^property[0].code = #parent 
* #UNRELAT ^property[0].valueCode = #_ActInvoiceOverrideCode 
* #VERBAUTH "verbal authorization"
* #VERBAUTH ^property[0].code = #parent 
* #VERBAUTH ^property[0].valueCode = #_ActInvoiceOverrideCode 
* #_ActListCode "ActListCode"
* #_ActListCode ^property[0].code = #child 
* #_ActListCode ^property[0].valueCode = #MEDLIST 
* #_ActListCode ^property[1].code = #child 
* #_ActListCode ^property[1].valueCode = #_ActObservationList 
* #_ActListCode ^property[2].code = #child 
* #_ActListCode ^property[2].valueCode = #_ActTherapyDurationWorkingListCode 
* #MEDLIST "medication list"
* #MEDLIST ^property[0].code = #parent 
* #MEDLIST ^property[0].valueCode = #_ActListCode 
* #MEDLIST ^property[1].code = #child 
* #MEDLIST ^property[1].valueCode = #CURMEDLIST 
* #MEDLIST ^property[2].code = #child 
* #MEDLIST ^property[2].valueCode = #DISCMEDLIST 
* #MEDLIST ^property[3].code = #child 
* #MEDLIST ^property[3].valueCode = #HISTMEDLIST 
* #CURMEDLIST "current medication list"
* #CURMEDLIST ^property[0].code = #parent 
* #CURMEDLIST ^property[0].valueCode = #MEDLIST 
* #DISCMEDLIST "discharge medication list"
* #DISCMEDLIST ^property[0].code = #parent 
* #DISCMEDLIST ^property[0].valueCode = #MEDLIST 
* #HISTMEDLIST "medication history"
* #HISTMEDLIST ^property[0].code = #parent 
* #HISTMEDLIST ^property[0].valueCode = #MEDLIST 
* #_ActObservationList "ActObservationList"
* #_ActObservationList ^property[0].code = #parent 
* #_ActObservationList ^property[0].valueCode = #_ActListCode 
* #_ActObservationList ^property[1].code = #child 
* #_ActObservationList ^property[1].valueCode = #CARELIST 
* #_ActObservationList ^property[2].code = #child 
* #_ActObservationList ^property[2].valueCode = #CONDLIST 
* #_ActObservationList ^property[3].code = #child 
* #_ActObservationList ^property[3].valueCode = #GOALLIST 
* #CARELIST "care plan"
* #CARELIST ^property[0].code = #parent 
* #CARELIST ^property[0].valueCode = #_ActObservationList 
* #CONDLIST "condition list"
* #CONDLIST ^property[0].code = #parent 
* #CONDLIST ^property[0].valueCode = #_ActObservationList 
* #CONDLIST ^property[1].code = #child 
* #CONDLIST ^property[1].valueCode = #INTOLIST 
* #CONDLIST ^property[2].code = #child 
* #CONDLIST ^property[2].valueCode = #PROBLIST 
* #CONDLIST ^property[3].code = #child 
* #CONDLIST ^property[3].valueCode = #RISKLIST 
* #INTOLIST "intolerance list"
* #INTOLIST ^property[0].code = #parent 
* #INTOLIST ^property[0].valueCode = #CONDLIST 
* #PROBLIST "problem list"
* #PROBLIST ^property[0].code = #parent 
* #PROBLIST ^property[0].valueCode = #CONDLIST 
* #RISKLIST "risk factors"
* #RISKLIST ^property[0].code = #parent 
* #RISKLIST ^property[0].valueCode = #CONDLIST 
* #GOALLIST "goal list"
* #GOALLIST ^property[0].code = #parent 
* #GOALLIST ^property[0].valueCode = #_ActObservationList 
* #_ActTherapyDurationWorkingListCode "ActTherapyDurationWorkingListCode"
* #_ActTherapyDurationWorkingListCode ^property[0].code = #parent 
* #_ActTherapyDurationWorkingListCode ^property[0].valueCode = #_ActListCode 
* #_ActTherapyDurationWorkingListCode ^property[1].code = #child 
* #_ActTherapyDurationWorkingListCode ^property[1].valueCode = #_ActMedicationTherapyDurationWorkingListCode 
* #_ActMedicationTherapyDurationWorkingListCode "act medication therapy duration working list"
* #_ActMedicationTherapyDurationWorkingListCode ^property[0].code = #parent 
* #_ActMedicationTherapyDurationWorkingListCode ^property[0].valueCode = #_ActTherapyDurationWorkingListCode 
* #_ActMedicationTherapyDurationWorkingListCode ^property[1].code = #child 
* #_ActMedicationTherapyDurationWorkingListCode ^property[1].valueCode = #ACU 
* #_ActMedicationTherapyDurationWorkingListCode ^property[2].code = #child 
* #_ActMedicationTherapyDurationWorkingListCode ^property[2].valueCode = #CHRON 
* #_ActMedicationTherapyDurationWorkingListCode ^property[3].code = #child 
* #_ActMedicationTherapyDurationWorkingListCode ^property[3].valueCode = #ONET 
* #_ActMedicationTherapyDurationWorkingListCode ^property[4].code = #child 
* #_ActMedicationTherapyDurationWorkingListCode ^property[4].valueCode = #PRN 
* #ACU "short term/acute"
* #ACU ^property[0].code = #parent 
* #ACU ^property[0].valueCode = #_ActMedicationTherapyDurationWorkingListCode 
* #CHRON "continuous/chronic"
* #CHRON ^property[0].code = #parent 
* #CHRON ^property[0].valueCode = #_ActMedicationTherapyDurationWorkingListCode 
* #ONET "one time"
* #ONET ^property[0].code = #parent 
* #ONET ^property[0].valueCode = #_ActMedicationTherapyDurationWorkingListCode 
* #PRN "as needed"
* #PRN ^property[0].code = #parent 
* #PRN ^property[0].valueCode = #_ActMedicationTherapyDurationWorkingListCode 
* #_ActMonitoringProtocolCode "ActMonitoringProtocolCode"
* #_ActMonitoringProtocolCode ^property[0].code = #child 
* #_ActMonitoringProtocolCode ^property[0].valueCode = #CTLSUB 
* #_ActMonitoringProtocolCode ^property[1].code = #child 
* #_ActMonitoringProtocolCode ^property[1].valueCode = #INV 
* #_ActMonitoringProtocolCode ^property[2].code = #child 
* #_ActMonitoringProtocolCode ^property[2].valueCode = #LU 
* #_ActMonitoringProtocolCode ^property[3].code = #child 
* #_ActMonitoringProtocolCode ^property[3].valueCode = #OTC 
* #_ActMonitoringProtocolCode ^property[4].code = #child 
* #_ActMonitoringProtocolCode ^property[4].valueCode = #RX 
* #_ActMonitoringProtocolCode ^property[5].code = #child 
* #_ActMonitoringProtocolCode ^property[5].valueCode = #SA 
* #_ActMonitoringProtocolCode ^property[6].code = #child 
* #_ActMonitoringProtocolCode ^property[6].valueCode = #SAC 
* #CTLSUB "Controlled Substance"
* #CTLSUB ^property[0].code = #parent 
* #CTLSUB ^property[0].valueCode = #_ActMonitoringProtocolCode 
* #INV "investigational"
* #INV ^property[0].code = #parent 
* #INV ^property[0].valueCode = #_ActMonitoringProtocolCode 
* #LU "limited use"
* #LU ^property[0].code = #parent 
* #LU ^property[0].valueCode = #_ActMonitoringProtocolCode 
* #OTC "non prescription medicine"
* #OTC ^property[0].code = #parent 
* #OTC ^property[0].valueCode = #_ActMonitoringProtocolCode 
* #RX "prescription only medicine"
* #RX ^property[0].code = #parent 
* #RX ^property[0].valueCode = #_ActMonitoringProtocolCode 
* #SA "special authorization"
* #SA ^property[0].code = #parent 
* #SA ^property[0].valueCode = #_ActMonitoringProtocolCode 
* #SAC "special access"
* #SAC ^property[0].code = #parent 
* #SAC ^property[0].valueCode = #_ActMonitoringProtocolCode 
* #_ActNonObservationIndicationCode "ActNonObservationIndicationCode"
* #_ActNonObservationIndicationCode ^property[0].code = #child 
* #_ActNonObservationIndicationCode ^property[0].valueCode = #IND01 
* #_ActNonObservationIndicationCode ^property[1].code = #child 
* #_ActNonObservationIndicationCode ^property[1].valueCode = #IND02 
* #_ActNonObservationIndicationCode ^property[2].code = #child 
* #_ActNonObservationIndicationCode ^property[2].valueCode = #IND03 
* #_ActNonObservationIndicationCode ^property[3].code = #child 
* #_ActNonObservationIndicationCode ^property[3].valueCode = #IND04 
* #_ActNonObservationIndicationCode ^property[4].code = #child 
* #_ActNonObservationIndicationCode ^property[4].valueCode = #IND05 
* #IND01 "imaging study requiring contrast"
* #IND01 ^property[0].code = #parent 
* #IND01 ^property[0].valueCode = #_ActNonObservationIndicationCode 
* #IND02 "colonoscopy prep"
* #IND02 ^property[0].code = #parent 
* #IND02 ^property[0].valueCode = #_ActNonObservationIndicationCode 
* #IND03 "prophylaxis"
* #IND03 ^property[0].code = #parent 
* #IND03 ^property[0].valueCode = #_ActNonObservationIndicationCode 
* #IND04 "surgical prophylaxis"
* #IND04 ^property[0].code = #parent 
* #IND04 ^property[0].valueCode = #_ActNonObservationIndicationCode 
* #IND05 "pregnancy prophylaxis"
* #IND05 ^property[0].code = #parent 
* #IND05 ^property[0].valueCode = #_ActNonObservationIndicationCode 
* #_ActObservationVerificationType "act observation verification"
* #_ActObservationVerificationType ^property[0].code = #child 
* #_ActObservationVerificationType ^property[0].valueCode = #VFPAPER 
* #_ActObservationVerificationType ^property[1].code = #child 
* #_ActObservationVerificationType ^property[1].valueCode = #VRFPAPER 
* #VFPAPER "verify paper"
* #VFPAPER ^property[0].code = #parent 
* #VFPAPER ^property[0].valueCode = #_ActObservationVerificationType 
* #VRFPAPER "verify paper"
* #VRFPAPER ^property[0].code = #parent 
* #VRFPAPER ^property[0].valueCode = #_ActObservationVerificationType 
* #_ActPaymentCode "ActPaymentCode"
* #_ActPaymentCode ^property[0].code = #child 
* #_ActPaymentCode ^property[0].valueCode = #ACH 
* #_ActPaymentCode ^property[1].code = #child 
* #_ActPaymentCode ^property[1].valueCode = #CHK 
* #_ActPaymentCode ^property[2].code = #child 
* #_ActPaymentCode ^property[2].valueCode = #DDP 
* #_ActPaymentCode ^property[3].code = #child 
* #_ActPaymentCode ^property[3].valueCode = #NON 
* #ACH "Automated Clearing House"
* #ACH ^property[0].code = #parent 
* #ACH ^property[0].valueCode = #_ActPaymentCode 
* #CHK "Cheque"
* #CHK ^property[0].code = #parent 
* #CHK ^property[0].valueCode = #_ActPaymentCode 
* #DDP "Direct Deposit"
* #DDP ^property[0].code = #parent 
* #DDP ^property[0].valueCode = #_ActPaymentCode 
* #NON "Non-Payment Data"
* #NON ^property[0].code = #parent 
* #NON ^property[0].valueCode = #_ActPaymentCode 
* #_ActPharmacySupplyType "ActPharmacySupplyType"
* #_ActPharmacySupplyType ^property[0].code = #child 
* #_ActPharmacySupplyType ^property[0].valueCode = #DF 
* #_ActPharmacySupplyType ^property[1].code = #child 
* #_ActPharmacySupplyType ^property[1].valueCode = #EM 
* #_ActPharmacySupplyType ^property[2].code = #child 
* #_ActPharmacySupplyType ^property[2].valueCode = #FF 
* #_ActPharmacySupplyType ^property[3].code = #child 
* #_ActPharmacySupplyType ^property[3].valueCode = #FS 
* #_ActPharmacySupplyType ^property[4].code = #child 
* #_ActPharmacySupplyType ^property[4].valueCode = #MS 
* #_ActPharmacySupplyType ^property[5].code = #child 
* #_ActPharmacySupplyType ^property[5].valueCode = #RF 
* #_ActPharmacySupplyType ^property[6].code = #child 
* #_ActPharmacySupplyType ^property[6].valueCode = #UDE 
* #DF "Daily Fill"
* #DF ^property[0].code = #parent 
* #DF ^property[0].valueCode = #_ActPharmacySupplyType 
* #DF ^property[1].code = #parent 
* #DF ^property[1].valueCode = #_ActPharmacySupplyType 
* #EM "Emergency Supply"
* #EM ^property[0].code = #parent 
* #EM ^property[0].valueCode = #_ActPharmacySupplyType 
* #EM ^property[1].code = #child 
* #EM ^property[1].valueCode = #SO 
* #SO "Script Owing"
* #SO ^property[0].code = #parent 
* #SO ^property[0].valueCode = #EM 
* #FF "First Fill"
* #FF ^property[0].code = #parent 
* #FF ^property[0].valueCode = #_ActPharmacySupplyType 
* #FF ^property[1].code = #child 
* #FF ^property[1].valueCode = #FFC 
* #FF ^property[2].code = #child 
* #FF ^property[2].valueCode = #FFP 
* #FF ^property[3].code = #child 
* #FF ^property[3].valueCode = #FFPS 
* #FF ^property[4].code = #child 
* #FF ^property[4].valueCode = #FFSS 
* #FF ^property[5].code = #child 
* #FF ^property[5].valueCode = #TF 
* #FFC "First Fill - Complete"
* #FFC ^property[0].code = #parent 
* #FFC ^property[0].valueCode = #FF 
* #FFC ^property[1].code = #child 
* #FFC ^property[1].valueCode = #FFCS 
* #FFCS "first fill complete, partial strength"
* #FFCS ^property[0].code = #parent 
* #FFCS ^property[0].valueCode = #FFC 
* #FFP "First Fill - Part Fill"
* #FFP ^property[0].code = #parent 
* #FFP ^property[0].valueCode = #FF 
* #FFPS "first fill, part fill, partial strength"
* #FFPS ^property[0].code = #parent 
* #FFPS ^property[0].valueCode = #FF 
* #FFPS ^property[1].code = #parent 
* #FFPS ^property[1].valueCode = #FF 
* #FFSS "first fill, partial strength"
* #FFSS ^property[0].code = #parent 
* #FFSS ^property[0].valueCode = #FF 
* #TF "Trial Fill"
* #TF ^property[0].code = #parent 
* #TF ^property[0].valueCode = #FF 
* #TF ^property[1].code = #child 
* #TF ^property[1].valueCode = #TFS 
* #TFS "trial fill partial strength"
* #TFS ^property[0].code = #parent 
* #TFS ^property[0].valueCode = #TF 
* #FS "Floor stock"
* #FS ^property[0].code = #parent 
* #FS ^property[0].valueCode = #_ActPharmacySupplyType 
* #MS "Manufacturer Sample"
* #MS ^property[0].code = #parent 
* #MS ^property[0].valueCode = #_ActPharmacySupplyType 
* #RF "Refill"
* #RF ^property[0].code = #parent 
* #RF ^property[0].valueCode = #_ActPharmacySupplyType 
* #RF ^property[1].code = #child 
* #RF ^property[1].valueCode = #RFC 
* #RF ^property[2].code = #child 
* #RF ^property[2].valueCode = #RFF 
* #RF ^property[3].code = #child 
* #RF ^property[3].valueCode = #RFP 
* #RF ^property[4].code = #child 
* #RF ^property[4].valueCode = #RFS 
* #RF ^property[5].code = #child 
* #RF ^property[5].valueCode = #TB 
* #RF ^property[6].code = #child 
* #RF ^property[6].valueCode = #UD 
* #RFC "Refill - Complete"
* #RFC ^property[0].code = #parent 
* #RFC ^property[0].valueCode = #RF 
* #RFC ^property[1].code = #child 
* #RFC ^property[1].valueCode = #RFCS 
* #RFCS "refill complete partial strength"
* #RFCS ^property[0].code = #parent 
* #RFCS ^property[0].valueCode = #RFC 
* #RFF "Refill (First fill this facility)"
* #RFF ^property[0].code = #parent 
* #RFF ^property[0].valueCode = #RF 
* #RFF ^property[1].code = #child 
* #RFF ^property[1].valueCode = #RFFS 
* #RFFS "refill partial strength (first fill this facility)"
* #RFFS ^property[0].code = #parent 
* #RFFS ^property[0].valueCode = #RFF 
* #RFP "Refill - Part Fill"
* #RFP ^property[0].code = #parent 
* #RFP ^property[0].valueCode = #RF 
* #RFP ^property[1].code = #child 
* #RFP ^property[1].valueCode = #RFPS 
* #RFPS "refill part fill partial strength"
* #RFPS ^property[0].code = #parent 
* #RFPS ^property[0].valueCode = #RFP 
* #RFS "refill partial strength"
* #RFS ^property[0].code = #parent 
* #RFS ^property[0].valueCode = #RF 
* #TB "Trial Balance"
* #TB ^property[0].code = #parent 
* #TB ^property[0].valueCode = #RF 
* #TB ^property[1].code = #child 
* #TB ^property[1].valueCode = #TBS 
* #TBS "trial balance partial strength"
* #TBS ^property[0].code = #parent 
* #TBS ^property[0].valueCode = #TB 
* #UD "Unit Dose"
* #UD ^property[0].code = #parent 
* #UD ^property[0].valueCode = #RF 
* #UDE "unit dose equivalent"
* #UDE ^property[0].code = #parent 
* #UDE ^property[0].valueCode = #_ActPharmacySupplyType 
* #_ActPolicyType "ActPolicyType"
* #_ActPolicyType ^property[0].code = #child 
* #_ActPolicyType ^property[0].valueCode = #COVPOL 
* #COVPOL "benefit policy"
* #COVPOL ^property[0].code = #parent 
* #COVPOL ^property[0].valueCode = #_ActPolicyType 
* #_ActProductAcquisitionCode "ActProductAcquisitionCode"
* #_ActProductAcquisitionCode ^property[0].code = #child 
* #_ActProductAcquisitionCode ^property[0].valueCode = #LOAN 
* #_ActProductAcquisitionCode ^property[1].code = #child 
* #_ActProductAcquisitionCode ^property[1].valueCode = #TRANSFER 
* #LOAN "Loan"
* #LOAN ^property[0].code = #parent 
* #LOAN ^property[0].valueCode = #_ActProductAcquisitionCode 
* #LOAN ^property[1].code = #child 
* #LOAN ^property[1].valueCode = #RENT 
* #RENT "Rent"
* #RENT ^property[0].code = #parent 
* #RENT ^property[0].valueCode = #LOAN 
* #TRANSFER "Transfer"
* #TRANSFER ^property[0].code = #parent 
* #TRANSFER ^property[0].valueCode = #_ActProductAcquisitionCode 
* #TRANSFER ^property[1].code = #child 
* #TRANSFER ^property[1].valueCode = #SALE 
* #SALE "Sale"
* #SALE ^property[0].code = #parent 
* #SALE ^property[0].valueCode = #TRANSFER 
* #_ActSpecimenTransportCode "ActSpecimenTransportCode"
* #_ActSpecimenTransportCode ^property[0].code = #child 
* #_ActSpecimenTransportCode ^property[0].valueCode = #SREC 
* #_ActSpecimenTransportCode ^property[1].code = #child 
* #_ActSpecimenTransportCode ^property[1].valueCode = #SSTOR 
* #_ActSpecimenTransportCode ^property[2].code = #child 
* #_ActSpecimenTransportCode ^property[2].valueCode = #STRAN 
* #SREC "specimen received"
* #SREC ^property[0].code = #parent 
* #SREC ^property[0].valueCode = #_ActSpecimenTransportCode 
* #SSTOR "specimen in storage"
* #SSTOR ^property[0].code = #parent 
* #SSTOR ^property[0].valueCode = #_ActSpecimenTransportCode 
* #STRAN "specimen in transit"
* #STRAN ^property[0].code = #parent 
* #STRAN ^property[0].valueCode = #_ActSpecimenTransportCode 
* #_ActSpecimenTreatmentCode "ActSpecimenTreatmentCode"
* #_ActSpecimenTreatmentCode ^property[0].code = #child 
* #_ActSpecimenTreatmentCode ^property[0].valueCode = #ACID 
* #_ActSpecimenTreatmentCode ^property[1].code = #child 
* #_ActSpecimenTreatmentCode ^property[1].valueCode = #ALK 
* #_ActSpecimenTreatmentCode ^property[2].code = #child 
* #_ActSpecimenTreatmentCode ^property[2].valueCode = #DEFB 
* #_ActSpecimenTreatmentCode ^property[3].code = #child 
* #_ActSpecimenTreatmentCode ^property[3].valueCode = #FILT 
* #_ActSpecimenTreatmentCode ^property[4].code = #child 
* #_ActSpecimenTreatmentCode ^property[4].valueCode = #LDLP 
* #_ActSpecimenTreatmentCode ^property[5].code = #child 
* #_ActSpecimenTreatmentCode ^property[5].valueCode = #NEUT 
* #_ActSpecimenTreatmentCode ^property[6].code = #child 
* #_ActSpecimenTreatmentCode ^property[6].valueCode = #RECA 
* #_ActSpecimenTreatmentCode ^property[7].code = #child 
* #_ActSpecimenTreatmentCode ^property[7].valueCode = #UFIL 
* #ACID "Acidification"
* #ACID ^property[0].code = #parent 
* #ACID ^property[0].valueCode = #_ActSpecimenTreatmentCode 
* #ALK "Alkalization"
* #ALK ^property[0].code = #parent 
* #ALK ^property[0].valueCode = #_ActSpecimenTreatmentCode 
* #DEFB "Defibrination"
* #DEFB ^property[0].code = #parent 
* #DEFB ^property[0].valueCode = #_ActSpecimenTreatmentCode 
* #FILT "Filtration"
* #FILT ^property[0].code = #parent 
* #FILT ^property[0].valueCode = #_ActSpecimenTreatmentCode 
* #LDLP "LDL Precipitation"
* #LDLP ^property[0].code = #parent 
* #LDLP ^property[0].valueCode = #_ActSpecimenTreatmentCode 
* #NEUT "Neutralization"
* #NEUT ^property[0].code = #parent 
* #NEUT ^property[0].valueCode = #_ActSpecimenTreatmentCode 
* #RECA "Recalcification"
* #RECA ^property[0].code = #parent 
* #RECA ^property[0].valueCode = #_ActSpecimenTreatmentCode 
* #UFIL "Ultrafiltration"
* #UFIL ^property[0].code = #parent 
* #UFIL ^property[0].valueCode = #_ActSpecimenTreatmentCode 
* #_ActSubstanceAdministrationCode "ActSubstanceAdministrationCode"
* #_ActSubstanceAdministrationCode ^property[0].code = #child 
* #_ActSubstanceAdministrationCode ^property[0].valueCode = #DRUG 
* #_ActSubstanceAdministrationCode ^property[1].code = #child 
* #_ActSubstanceAdministrationCode ^property[1].valueCode = #FD 
* #_ActSubstanceAdministrationCode ^property[2].code = #child 
* #_ActSubstanceAdministrationCode ^property[2].valueCode = #IMMUNIZ 
* #DRUG "Drug therapy"
* #DRUG ^property[0].code = #parent 
* #DRUG ^property[0].valueCode = #_ActSubstanceAdministrationCode 
* #FD "food"
* #FD ^property[0].code = #parent 
* #FD ^property[0].valueCode = #_ActSubstanceAdministrationCode 
* #IMMUNIZ "Immunization"
* #IMMUNIZ ^property[0].code = #parent 
* #IMMUNIZ ^property[0].valueCode = #_ActSubstanceAdministrationCode 
* #_ActTaskCode "ActTaskCode"
* #_ActTaskCode ^property[0].code = #child 
* #_ActTaskCode ^property[0].valueCode = #OE 
* #_ActTaskCode ^property[1].code = #child 
* #_ActTaskCode ^property[1].valueCode = #PATDOC 
* #_ActTaskCode ^property[2].code = #child 
* #_ActTaskCode ^property[2].valueCode = #PATINFO 
* #OE "order entry task"
* #OE ^property[0].code = #parent 
* #OE ^property[0].valueCode = #_ActTaskCode 
* #OE ^property[1].code = #child 
* #OE ^property[1].valueCode = #LABOE 
* #OE ^property[2].code = #child 
* #OE ^property[2].valueCode = #MEDOE 
* #LABOE "laboratory test order entry task"
* #LABOE ^property[0].code = #parent 
* #LABOE ^property[0].valueCode = #OE 
* #MEDOE "medication order entry task"
* #MEDOE ^property[0].code = #parent 
* #MEDOE ^property[0].valueCode = #OE 
* #PATDOC "patient documentation task"
* #PATDOC ^property[0].code = #parent 
* #PATDOC ^property[0].valueCode = #_ActTaskCode 
* #PATDOC ^property[1].code = #child 
* #PATDOC ^property[1].valueCode = #ALLERLREV 
* #PATDOC ^property[2].code = #child 
* #PATDOC ^property[2].valueCode = #CLINNOTEE 
* #PATDOC ^property[3].code = #child 
* #PATDOC ^property[3].valueCode = #IMMLREV 
* #PATDOC ^property[4].code = #child 
* #PATDOC ^property[4].valueCode = #REMLREV 
* #ALLERLREV "allergy list review"
* #ALLERLREV ^property[0].code = #parent 
* #ALLERLREV ^property[0].valueCode = #PATDOC 
* #CLINNOTEE "clinical note entry task"
* #CLINNOTEE ^property[0].code = #parent 
* #CLINNOTEE ^property[0].valueCode = #PATDOC 
* #CLINNOTEE ^property[1].code = #child 
* #CLINNOTEE ^property[1].valueCode = #DIAGLISTE 
* #CLINNOTEE ^property[2].code = #child 
* #CLINNOTEE ^property[2].valueCode = #DISCHSUME 
* #CLINNOTEE ^property[3].code = #child 
* #CLINNOTEE ^property[3].valueCode = #PATREPE 
* #CLINNOTEE ^property[4].code = #child 
* #CLINNOTEE ^property[4].valueCode = #PROBLISTE 
* #CLINNOTEE ^property[5].code = #child 
* #CLINNOTEE ^property[5].valueCode = #RADREPE 
* #DIAGLISTE "diagnosis list entry task"
* #DIAGLISTE ^property[0].code = #parent 
* #DIAGLISTE ^property[0].valueCode = #CLINNOTEE 
* #DISCHSUME "discharge summary entry task"
* #DISCHSUME ^property[0].code = #parent 
* #DISCHSUME ^property[0].valueCode = #CLINNOTEE 
* #PATREPE "pathology report entry task"
* #PATREPE ^property[0].code = #parent 
* #PATREPE ^property[0].valueCode = #CLINNOTEE 
* #PROBLISTE "problem list entry task"
* #PROBLISTE ^property[0].code = #parent 
* #PROBLISTE ^property[0].valueCode = #CLINNOTEE 
* #RADREPE "radiology report entry task"
* #RADREPE ^property[0].code = #parent 
* #RADREPE ^property[0].valueCode = #CLINNOTEE 
* #IMMLREV "immunization list review"
* #IMMLREV ^property[0].code = #parent 
* #IMMLREV ^property[0].valueCode = #PATDOC 
* #REMLREV "reminder list review"
* #REMLREV ^property[0].code = #parent 
* #REMLREV ^property[0].valueCode = #PATDOC 
* #REMLREV ^property[1].code = #child 
* #REMLREV ^property[1].valueCode = #WELLREMLREV 
* #WELLREMLREV "wellness reminder list review"
* #WELLREMLREV ^property[0].code = #parent 
* #WELLREMLREV ^property[0].valueCode = #REMLREV 
* #PATINFO "patient information review task"
* #PATINFO ^property[0].code = #parent 
* #PATINFO ^property[0].valueCode = #_ActTaskCode 
* #PATINFO ^property[1].code = #child 
* #PATINFO ^property[1].valueCode = #ALLERLE 
* #PATINFO ^property[2].code = #child 
* #PATINFO ^property[2].valueCode = #CLINNOTEREV 
* #PATINFO ^property[3].code = #child 
* #PATINFO ^property[3].valueCode = #DIAGLISTREV 
* #PATINFO ^property[4].code = #child 
* #PATINFO ^property[4].valueCode = #IMMLE 
* #PATINFO ^property[5].code = #child 
* #PATINFO ^property[5].valueCode = #LABRREV 
* #PATINFO ^property[6].code = #child 
* #PATINFO ^property[6].valueCode = #MICRORREV 
* #PATINFO ^property[7].code = #child 
* #PATINFO ^property[7].valueCode = #MLREV 
* #PATINFO ^property[8].code = #child 
* #PATINFO ^property[8].valueCode = #OREV 
* #PATINFO ^property[9].code = #child 
* #PATINFO ^property[9].valueCode = #PATREPREV 
* #PATINFO ^property[10].code = #child 
* #PATINFO ^property[10].valueCode = #PROBLISTREV 
* #PATINFO ^property[11].code = #child 
* #PATINFO ^property[11].valueCode = #RADREPREV 
* #PATINFO ^property[12].code = #child 
* #PATINFO ^property[12].valueCode = #REMLE 
* #PATINFO ^property[13].code = #child 
* #PATINFO ^property[13].valueCode = #RISKASSESS 
* #ALLERLE "allergy list entry"
* #ALLERLE ^property[0].code = #parent 
* #ALLERLE ^property[0].valueCode = #PATINFO 
* #CLINNOTEREV "clinical note review task"
* #CLINNOTEREV ^property[0].code = #parent 
* #CLINNOTEREV ^property[0].valueCode = #PATINFO 
* #CLINNOTEREV ^property[1].code = #child 
* #CLINNOTEREV ^property[1].valueCode = #DISCHSUMREV 
* #DISCHSUMREV "discharge summary review task"
* #DISCHSUMREV ^property[0].code = #parent 
* #DISCHSUMREV ^property[0].valueCode = #CLINNOTEREV 
* #DIAGLISTREV "diagnosis list review task"
* #DIAGLISTREV ^property[0].code = #parent 
* #DIAGLISTREV ^property[0].valueCode = #PATINFO 
* #IMMLE "immunization list entry"
* #IMMLE ^property[0].code = #parent 
* #IMMLE ^property[0].valueCode = #PATINFO 
* #LABRREV "laboratory results review task"
* #LABRREV ^property[0].code = #parent 
* #LABRREV ^property[0].valueCode = #PATINFO 
* #MICRORREV "microbiology results review task"
* #MICRORREV ^property[0].code = #parent 
* #MICRORREV ^property[0].valueCode = #PATINFO 
* #MICRORREV ^property[1].code = #child 
* #MICRORREV ^property[1].valueCode = #MICROORGRREV 
* #MICRORREV ^property[2].code = #child 
* #MICRORREV ^property[2].valueCode = #MICROSENSRREV 
* #MICROORGRREV "microbiology organisms results review task"
* #MICROORGRREV ^property[0].code = #parent 
* #MICROORGRREV ^property[0].valueCode = #MICRORREV 
* #MICROSENSRREV "microbiology sensitivity test results review task"
* #MICROSENSRREV ^property[0].code = #parent 
* #MICROSENSRREV ^property[0].valueCode = #MICRORREV 
* #MLREV "medication list review task"
* #MLREV ^property[0].code = #parent 
* #MLREV ^property[0].valueCode = #PATINFO 
* #MLREV ^property[1].code = #child 
* #MLREV ^property[1].valueCode = #MARWLREV 
* #MARWLREV "medication administration record work list review task"
* #MARWLREV ^property[0].code = #parent 
* #MARWLREV ^property[0].valueCode = #MLREV 
* #OREV "orders review task"
* #OREV ^property[0].code = #parent 
* #OREV ^property[0].valueCode = #PATINFO 
* #PATREPREV "pathology report review task"
* #PATREPREV ^property[0].code = #parent 
* #PATREPREV ^property[0].valueCode = #PATINFO 
* #PROBLISTREV "problem list review task"
* #PROBLISTREV ^property[0].code = #parent 
* #PROBLISTREV ^property[0].valueCode = #PATINFO 
* #RADREPREV "radiology report review task"
* #RADREPREV ^property[0].code = #parent 
* #RADREPREV ^property[0].valueCode = #PATINFO 
* #REMLE "reminder list entry"
* #REMLE ^property[0].code = #parent 
* #REMLE ^property[0].valueCode = #PATINFO 
* #REMLE ^property[1].code = #child 
* #REMLE ^property[1].valueCode = #WELLREMLE 
* #WELLREMLE "wellness reminder list entry"
* #WELLREMLE ^property[0].code = #parent 
* #WELLREMLE ^property[0].valueCode = #REMLE 
* #RISKASSESS "risk assessment instrument task"
* #RISKASSESS ^property[0].code = #parent 
* #RISKASSESS ^property[0].valueCode = #PATINFO 
* #RISKASSESS ^property[1].code = #child 
* #RISKASSESS ^property[1].valueCode = #FALLRISK 
* #FALLRISK "falls risk assessment instrument task"
* #FALLRISK ^property[0].code = #parent 
* #FALLRISK ^property[0].valueCode = #RISKASSESS 
* #_ActTransportationModeCode "ActTransportationModeCode"
* #_ActTransportationModeCode ^property[0].code = #child 
* #_ActTransportationModeCode ^property[0].valueCode = #_ActPatientTransportationModeCode 
* #_ActPatientTransportationModeCode "ActPatientTransportationModeCode"
* #_ActPatientTransportationModeCode ^property[0].code = #parent 
* #_ActPatientTransportationModeCode ^property[0].valueCode = #_ActTransportationModeCode 
* #_ActPatientTransportationModeCode ^property[1].code = #child 
* #_ActPatientTransportationModeCode ^property[1].valueCode = #AFOOT 
* #_ActPatientTransportationModeCode ^property[2].code = #child 
* #_ActPatientTransportationModeCode ^property[2].valueCode = #AMBT 
* #_ActPatientTransportationModeCode ^property[3].code = #child 
* #_ActPatientTransportationModeCode ^property[3].valueCode = #LAWENF 
* #_ActPatientTransportationModeCode ^property[4].code = #child 
* #_ActPatientTransportationModeCode ^property[4].valueCode = #PRVTRN 
* #_ActPatientTransportationModeCode ^property[5].code = #child 
* #_ActPatientTransportationModeCode ^property[5].valueCode = #PUBTRN 
* #AFOOT "pedestrian transport"
* #AFOOT ^property[0].code = #parent 
* #AFOOT ^property[0].valueCode = #_ActPatientTransportationModeCode 
* #AMBT "ambulance transport"
* #AMBT ^property[0].code = #parent 
* #AMBT ^property[0].valueCode = #_ActPatientTransportationModeCode 
* #AMBT ^property[1].code = #child 
* #AMBT ^property[1].valueCode = #AMBAIR 
* #AMBT ^property[2].code = #child 
* #AMBT ^property[2].valueCode = #AMBGRND 
* #AMBT ^property[3].code = #child 
* #AMBT ^property[3].valueCode = #AMBHELO 
* #AMBAIR "fixed-wing ambulance transport"
* #AMBAIR ^property[0].code = #parent 
* #AMBAIR ^property[0].valueCode = #AMBT 
* #AMBGRND "ground ambulance transport"
* #AMBGRND ^property[0].code = #parent 
* #AMBGRND ^property[0].valueCode = #AMBT 
* #AMBHELO "helicopter ambulance transport"
* #AMBHELO ^property[0].code = #parent 
* #AMBHELO ^property[0].valueCode = #AMBT 
* #LAWENF "law enforcement transport"
* #LAWENF ^property[0].code = #parent 
* #LAWENF ^property[0].valueCode = #_ActPatientTransportationModeCode 
* #PRVTRN "private transport"
* #PRVTRN ^property[0].code = #parent 
* #PRVTRN ^property[0].valueCode = #_ActPatientTransportationModeCode 
* #PUBTRN "public transport"
* #PUBTRN ^property[0].code = #parent 
* #PUBTRN ^property[0].valueCode = #_ActPatientTransportationModeCode 
* #_ObservationType "ObservationType"
* #_ObservationType ^property[0].code = #child 
* #_ObservationType ^property[0].valueCode = #ADVERSE_REACTION 
* #_ObservationType ^property[1].code = #child 
* #_ObservationType ^property[1].valueCode = #ASSERTION 
* #_ObservationType ^property[2].code = #child 
* #_ObservationType ^property[2].valueCode = #CASESER 
* #_ObservationType ^property[3].code = #child 
* #_ObservationType ^property[3].valueCode = #DX 
* #_ObservationType ^property[4].code = #child 
* #_ObservationType ^property[4].valueCode = #GISTIER 
* #_ObservationType ^property[5].code = #child 
* #_ObservationType ^property[5].valueCode = #HHOBS 
* #_ObservationType ^property[6].code = #child 
* #_ObservationType ^property[6].valueCode = #ISSUE 
* #_ObservationType ^property[7].code = #child 
* #_ObservationType ^property[7].valueCode = #IndividualCaseSafetyReportType 
* #_ObservationType ^property[8].code = #child 
* #_ObservationType ^property[8].valueCode = #KSUBJ 
* #_ObservationType ^property[9].code = #child 
* #_ObservationType ^property[9].valueCode = #KSUBT 
* #_ObservationType ^property[10].code = #child 
* #_ObservationType ^property[10].valueCode = #OINT 
* #_ObservationType ^property[11].code = #child 
* #_ObservationType ^property[11].valueCode = #SEV 
* #_ObservationType ^property[12].code = #child 
* #_ObservationType ^property[12].valueCode = #_ActSpecObsCode 
* #_ObservationType ^property[13].code = #child 
* #_ObservationType ^property[13].valueCode = #_AnnotationType 
* #_ObservationType ^property[14].code = #child 
* #_ObservationType ^property[14].valueCode = #_GeneticObservationType 
* #_ObservationType ^property[15].code = #child 
* #_ObservationType ^property[15].valueCode = #_ImmunizationObservationType 
* #_ObservationType ^property[16].code = #child 
* #_ObservationType ^property[16].valueCode = #_LOINCObservationActContextAgeType 
* #_ObservationType ^property[17].code = #child 
* #_ObservationType ^property[17].valueCode = #_MedicationObservationType 
* #_ObservationType ^property[18].code = #child 
* #_ObservationType ^property[18].valueCode = #_ObservationIssueTriggerCodedObservationType 
* #_ObservationType ^property[19].code = #child 
* #_ObservationType ^property[19].valueCode = #_ObservationQualityMeasureAttribute 
* #_ObservationType ^property[20].code = #child 
* #_ObservationType ^property[20].valueCode = #_ObservationSequenceType 
* #_ObservationType ^property[21].code = #child 
* #_ObservationType ^property[21].valueCode = #_ObservationSeriesType 
* #_ObservationType ^property[22].code = #child 
* #_ObservationType ^property[22].valueCode = #_PatientImmunizationRelatedObservationType 
* #ADVERSE_REACTION "Adverse Reaction"
* #ADVERSE_REACTION ^property[0].code = #parent 
* #ADVERSE_REACTION ^property[0].valueCode = #_ObservationType 
* #ASSERTION "Assertion"
* #ASSERTION ^property[0].code = #parent 
* #ASSERTION ^property[0].valueCode = #_ObservationType 
* #CASESER "case seriousness criteria"
* #CASESER ^property[0].code = #parent 
* #CASESER ^property[0].valueCode = #_ObservationType 
* #DX "ObservationDiagnosisTypes"
* #DX ^property[0].code = #parent 
* #DX ^property[0].valueCode = #_ObservationType 
* #DX ^property[1].code = #child 
* #DX ^property[1].valueCode = #ADMDX 
* #DX ^property[2].code = #child 
* #DX ^property[2].valueCode = #DISDX 
* #DX ^property[3].code = #child 
* #DX ^property[3].valueCode = #INTDX 
* #DX ^property[4].code = #child 
* #DX ^property[4].valueCode = #NOI 
* #ADMDX "admitting diagnosis"
* #ADMDX ^property[0].code = #parent 
* #ADMDX ^property[0].valueCode = #DX 
* #DISDX "discharge diagnosis"
* #DISDX ^property[0].code = #parent 
* #DISDX ^property[0].valueCode = #DX 
* #INTDX "intermediate diagnosis"
* #INTDX ^property[0].code = #parent 
* #INTDX ^property[0].valueCode = #DX 
* #NOI "nature of injury"
* #NOI ^property[0].code = #parent 
* #NOI ^property[0].valueCode = #DX 
* #GISTIER "GIS tier"
* #GISTIER ^property[0].code = #parent 
* #GISTIER ^property[0].valueCode = #_ObservationType 
* #HHOBS "household situation observation"
* #HHOBS ^property[0].code = #parent 
* #HHOBS ^property[0].valueCode = #_ObservationType 
* #ISSUE "detected issue"
* #ISSUE ^property[0].code = #parent 
* #ISSUE ^property[0].valueCode = #_ObservationType 
* #ISSUE ^property[1].code = #child 
* #ISSUE ^property[1].valueCode = #_ActAdministrativeDetectedIssueCode 
* #ISSUE ^property[2].code = #child 
* #ISSUE ^property[2].valueCode = #_ActSuppliedItemDetectedIssueCode 
* #_ActAdministrativeDetectedIssueCode "ActAdministrativeDetectedIssueCode"
* #_ActAdministrativeDetectedIssueCode ^property[0].code = #parent 
* #_ActAdministrativeDetectedIssueCode ^property[0].valueCode = #ISSUE 
* #_ActAdministrativeDetectedIssueCode ^property[1].code = #child 
* #_ActAdministrativeDetectedIssueCode ^property[1].valueCode = #_ActAdministrativeAuthorizationDetectedIssueCode 
* #_ActAdministrativeDetectedIssueCode ^property[2].code = #child 
* #_ActAdministrativeDetectedIssueCode ^property[2].valueCode = #_ActAdministrativeRuleDetectedIssueCode 
* #_ActAdministrativeAuthorizationDetectedIssueCode "ActAdministrativeAuthorizationDetectedIssueCode"
* #_ActAdministrativeAuthorizationDetectedIssueCode ^property[0].code = #parent 
* #_ActAdministrativeAuthorizationDetectedIssueCode ^property[0].valueCode = #_ActAdministrativeDetectedIssueCode 
* #_ActAdministrativeAuthorizationDetectedIssueCode ^property[1].code = #child 
* #_ActAdministrativeAuthorizationDetectedIssueCode ^property[1].valueCode = #NAT 
* #_ActAdministrativeAuthorizationDetectedIssueCode ^property[2].code = #child 
* #_ActAdministrativeAuthorizationDetectedIssueCode ^property[2].valueCode = #SUPPRESSED 
* #_ActAdministrativeAuthorizationDetectedIssueCode ^property[3].code = #child 
* #_ActAdministrativeAuthorizationDetectedIssueCode ^property[3].valueCode = #VALIDAT 
* #NAT "Insufficient authorization"
* #NAT ^property[0].code = #parent 
* #NAT ^property[0].valueCode = #_ActAdministrativeAuthorizationDetectedIssueCode 
* #SUPPRESSED "record suppressed"
* #SUPPRESSED ^property[0].code = #parent 
* #SUPPRESSED ^property[0].valueCode = #_ActAdministrativeAuthorizationDetectedIssueCode 
* #VALIDAT "validation issue"
* #VALIDAT ^property[0].code = #parent 
* #VALIDAT ^property[0].valueCode = #_ActAdministrativeAuthorizationDetectedIssueCode 
* #VALIDAT ^property[1].code = #child 
* #VALIDAT ^property[1].valueCode = #BUS 
* #VALIDAT ^property[2].code = #child 
* #VALIDAT ^property[2].valueCode = #CODE_INVAL 
* #VALIDAT ^property[3].code = #child 
* #VALIDAT ^property[3].valueCode = #COMPLY 
* #VALIDAT ^property[4].code = #child 
* #VALIDAT ^property[4].valueCode = #DOSE 
* #VALIDAT ^property[5].code = #child 
* #VALIDAT ^property[5].valueCode = #FORMAT 
* #VALIDAT ^property[6].code = #child 
* #VALIDAT ^property[6].valueCode = #ILLEGAL 
* #VALIDAT ^property[7].code = #child 
* #VALIDAT ^property[7].valueCode = #KEY204 
* #VALIDAT ^property[8].code = #child 
* #VALIDAT ^property[8].valueCode = #KEY205 
* #VALIDAT ^property[9].code = #child 
* #VALIDAT ^property[9].valueCode = #LEN_RANGE 
* #VALIDAT ^property[10].code = #child 
* #VALIDAT ^property[10].valueCode = #MISSCOND 
* #VALIDAT ^property[11].code = #child 
* #VALIDAT ^property[11].valueCode = #MISSMAND 
* #VALIDAT ^property[12].code = #child 
* #VALIDAT ^property[12].valueCode = #NODUPS 
* #VALIDAT ^property[13].code = #child 
* #VALIDAT ^property[13].valueCode = #NOPERSIST 
* #VALIDAT ^property[14].code = #child 
* #VALIDAT ^property[14].valueCode = #OBSA 
* #VALIDAT ^property[15].code = #child 
* #VALIDAT ^property[15].valueCode = #REP_RANGE 
* #BUS "business constraint violation"
* #BUS ^property[0].code = #parent 
* #BUS ^property[0].valueCode = #VALIDAT 
* #CODE_INVAL "code is not valid"
* #CODE_INVAL ^property[0].code = #parent 
* #CODE_INVAL ^property[0].valueCode = #VALIDAT 
* #CODE_INVAL ^property[1].code = #child 
* #CODE_INVAL ^property[1].valueCode = #CODE_DEPREC 
* #CODE_DEPREC "code has been deprecated"
* #CODE_DEPREC ^property[0].code = #parent 
* #CODE_DEPREC ^property[0].valueCode = #CODE_INVAL 
* #COMPLY "Compliance Alert"
* #COMPLY ^property[0].code = #parent 
* #COMPLY ^property[0].valueCode = #VALIDAT 
* #COMPLY ^property[1].code = #child 
* #COMPLY ^property[1].valueCode = #ABUSE 
* #COMPLY ^property[2].code = #child 
* #COMPLY ^property[2].valueCode = #DUPTHPY 
* #COMPLY ^property[3].code = #child 
* #COMPLY ^property[3].valueCode = #FRAUD 
* #COMPLY ^property[4].code = #child 
* #COMPLY ^property[4].valueCode = #PLYDOC 
* #COMPLY ^property[5].code = #child 
* #COMPLY ^property[5].valueCode = #PLYPHRM 
* #ABUSE "commonly abused/misused alert"
* #ABUSE ^property[0].code = #parent 
* #ABUSE ^property[0].valueCode = #COMPLY 
* #DUPTHPY "Duplicate Therapy Alert"
* #DUPTHPY ^property[0].code = #parent 
* #DUPTHPY ^property[0].valueCode = #COMPLY 
* #DUPTHPY ^property[1].code = #parent 
* #DUPTHPY ^property[1].valueCode = #COMPLY 
* #DUPTHPY ^property[2].code = #parent 
* #DUPTHPY ^property[2].valueCode = #COMPLY 
* #DUPTHPY ^property[3].code = #child 
* #DUPTHPY ^property[3].valueCode = #DUPTHPCLS 
* #DUPTHPY ^property[4].code = #child 
* #DUPTHPY ^property[4].valueCode = #DUPTHPGEN 
* #DUPTHPCLS "duplicate therapeutic alass alert"
* #DUPTHPCLS ^property[0].code = #parent 
* #DUPTHPCLS ^property[0].valueCode = #DUPTHPY 
* #DUPTHPCLS ^property[1].code = #parent 
* #DUPTHPCLS ^property[1].valueCode = #DUPTHPY 
* #DUPTHPCLS ^property[2].code = #parent 
* #DUPTHPCLS ^property[2].valueCode = #DUPTHPY 
* #DUPTHPGEN "duplicate generic alert"
* #DUPTHPGEN ^property[0].code = #parent 
* #DUPTHPGEN ^property[0].valueCode = #DUPTHPY 
* #DUPTHPGEN ^property[1].code = #parent 
* #DUPTHPGEN ^property[1].valueCode = #DUPTHPY 
* #DUPTHPGEN ^property[2].code = #parent 
* #DUPTHPGEN ^property[2].valueCode = #DUPTHPY 
* #FRAUD "potential fraud"
* #FRAUD ^property[0].code = #parent 
* #FRAUD ^property[0].valueCode = #COMPLY 
* #FRAUD ^property[1].code = #parent 
* #FRAUD ^property[1].valueCode = #COMPLY 
* #PLYDOC "Poly-orderer Alert"
* #PLYDOC ^property[0].code = #parent 
* #PLYDOC ^property[0].valueCode = #COMPLY 
* #PLYDOC ^property[1].code = #parent 
* #PLYDOC ^property[1].valueCode = #COMPLY 
* #PLYPHRM "Poly-supplier Alert"
* #PLYPHRM ^property[0].code = #parent 
* #PLYPHRM ^property[0].valueCode = #COMPLY 
* #PLYPHRM ^property[1].code = #parent 
* #PLYPHRM ^property[1].valueCode = #COMPLY 
* #DOSE "Dosage problem"
* #DOSE ^property[0].code = #parent 
* #DOSE ^property[0].valueCode = #VALIDAT 
* #DOSE ^property[1].code = #parent 
* #DOSE ^property[1].valueCode = #VALIDAT 
* #DOSE ^property[2].code = #child 
* #DOSE ^property[2].valueCode = #DOSECOND 
* #DOSE ^property[3].code = #child 
* #DOSE ^property[3].valueCode = #DOSEDUR 
* #DOSE ^property[4].code = #child 
* #DOSE ^property[4].valueCode = #DOSEH 
* #DOSE ^property[5].code = #child 
* #DOSE ^property[5].valueCode = #DOSEIVL 
* #DOSE ^property[6].code = #child 
* #DOSE ^property[6].valueCode = #DOSEL 
* #DOSE ^property[7].code = #child 
* #DOSE ^property[7].valueCode = #MDOSE 
* #DOSECOND "dosage-condition alert"
* #DOSECOND ^property[0].code = #parent 
* #DOSECOND ^property[0].valueCode = #DOSE 
* #DOSECOND ^property[1].code = #parent 
* #DOSECOND ^property[1].valueCode = #DOSE 
* #DOSEDUR "Dose-Duration Alert"
* #DOSEDUR ^property[0].code = #parent 
* #DOSEDUR ^property[0].valueCode = #DOSE 
* #DOSEDUR ^property[1].code = #parent 
* #DOSEDUR ^property[1].valueCode = #DOSE 
* #DOSEDUR ^property[2].code = #child 
* #DOSEDUR ^property[2].valueCode = #DOSEDURH 
* #DOSEDUR ^property[3].code = #child 
* #DOSEDUR ^property[3].valueCode = #DOSEDURL 
* #DOSEDURH "Dose-Duration High Alert"
* #DOSEDURH ^property[0].code = #parent 
* #DOSEDURH ^property[0].valueCode = #DOSEDUR 
* #DOSEDURH ^property[1].code = #child 
* #DOSEDURH ^property[1].valueCode = #DOSEDURHIND 
* #DOSEDURHIND "Dose-Duration High for Indication Alert"
* #DOSEDURHIND ^property[0].code = #parent 
* #DOSEDURHIND ^property[0].valueCode = #DOSEDURH 
* #DOSEDURHIND ^property[1].code = #parent 
* #DOSEDURHIND ^property[1].valueCode = #DOSEDURH 
* #DOSEDURL "Dose-Duration Low Alert"
* #DOSEDURL ^property[0].code = #parent 
* #DOSEDURL ^property[0].valueCode = #DOSEDUR 
* #DOSEDURL ^property[1].code = #child 
* #DOSEDURL ^property[1].valueCode = #DOSEDURLIND 
* #DOSEDURLIND "Dose-Duration Low for Indication Alert"
* #DOSEDURLIND ^property[0].code = #parent 
* #DOSEDURLIND ^property[0].valueCode = #DOSEDURL 
* #DOSEDURLIND ^property[1].code = #parent 
* #DOSEDURLIND ^property[1].valueCode = #DOSEDURL 
* #DOSEH "High Dose Alert"
* #DOSEH ^property[0].code = #parent 
* #DOSEH ^property[0].valueCode = #DOSE 
* #DOSEH ^property[1].code = #child 
* #DOSEH ^property[1].valueCode = #DOSEHIND 
* #DOSEH ^property[2].code = #child 
* #DOSEH ^property[2].valueCode = #DOSEHINDA 
* #DOSEH ^property[3].code = #child 
* #DOSEH ^property[3].valueCode = #DOSEHINDSA 
* #DOSEH ^property[4].code = #child 
* #DOSEH ^property[4].valueCode = #DOSEHINDW 
* #DOSEHIND "High Dose for Indication Alert"
* #DOSEHIND ^property[0].code = #parent 
* #DOSEHIND ^property[0].valueCode = #DOSEH 
* #DOSEHIND ^property[1].code = #parent 
* #DOSEHIND ^property[1].valueCode = #DOSEH 
* #DOSEHINDA "High Dose for Age Alert"
* #DOSEHINDA ^property[0].code = #parent 
* #DOSEHINDA ^property[0].valueCode = #DOSEH 
* #DOSEHINDA ^property[1].code = #parent 
* #DOSEHINDA ^property[1].valueCode = #DOSEH 
* #DOSEHINDA ^property[2].code = #parent 
* #DOSEHINDA ^property[2].valueCode = #DOSEH 
* #DOSEHINDSA "High Dose for Height/Surface Area Alert"
* #DOSEHINDSA ^property[0].code = #parent 
* #DOSEHINDSA ^property[0].valueCode = #DOSEH 
* #DOSEHINDSA ^property[1].code = #parent 
* #DOSEHINDSA ^property[1].valueCode = #DOSEH 
* #DOSEHINDW "High Dose for Weight Alert"
* #DOSEHINDW ^property[0].code = #parent 
* #DOSEHINDW ^property[0].valueCode = #DOSEH 
* #DOSEHINDW ^property[1].code = #parent 
* #DOSEHINDW ^property[1].valueCode = #DOSEH 
* #DOSEIVL "Dose-Interval Alert"
* #DOSEIVL ^property[0].code = #parent 
* #DOSEIVL ^property[0].valueCode = #DOSE 
* #DOSEIVL ^property[1].code = #child 
* #DOSEIVL ^property[1].valueCode = #DOSEIVLIND 
* #DOSEIVLIND "Dose-Interval for Indication Alert"
* #DOSEIVLIND ^property[0].code = #parent 
* #DOSEIVLIND ^property[0].valueCode = #DOSEIVL 
* #DOSEIVLIND ^property[1].code = #parent 
* #DOSEIVLIND ^property[1].valueCode = #DOSEIVL 
* #DOSEL "Low Dose Alert"
* #DOSEL ^property[0].code = #parent 
* #DOSEL ^property[0].valueCode = #DOSE 
* #DOSEL ^property[1].code = #parent 
* #DOSEL ^property[1].valueCode = #DOSE 
* #DOSEL ^property[2].code = #child 
* #DOSEL ^property[2].valueCode = #DOSELIND 
* #DOSEL ^property[3].code = #child 
* #DOSEL ^property[3].valueCode = #DOSELINDA 
* #DOSEL ^property[4].code = #child 
* #DOSEL ^property[4].valueCode = #DOSELINDSA 
* #DOSEL ^property[5].code = #child 
* #DOSEL ^property[5].valueCode = #DOSELINDW 
* #DOSELIND "Low Dose for Indication Alert"
* #DOSELIND ^property[0].code = #parent 
* #DOSELIND ^property[0].valueCode = #DOSEL 
* #DOSELIND ^property[1].code = #parent 
* #DOSELIND ^property[1].valueCode = #DOSEL 
* #DOSELINDA "Low Dose for Age Alert"
* #DOSELINDA ^property[0].code = #parent 
* #DOSELINDA ^property[0].valueCode = #DOSEL 
* #DOSELINDA ^property[1].code = #parent 
* #DOSELINDA ^property[1].valueCode = #DOSEL 
* #DOSELINDSA "Low Dose for Height/Surface Area Alert"
* #DOSELINDSA ^property[0].code = #parent 
* #DOSELINDSA ^property[0].valueCode = #DOSEL 
* #DOSELINDW "Low Dose for Weight Alert"
* #DOSELINDW ^property[0].code = #parent 
* #DOSELINDW ^property[0].valueCode = #DOSEL 
* #DOSELINDW ^property[1].code = #parent 
* #DOSELINDW ^property[1].valueCode = #DOSEL 
* #MDOSE "maximum dosage reached"
* #MDOSE ^property[0].code = #parent 
* #MDOSE ^property[0].valueCode = #DOSE 
* #MDOSE ^property[1].code = #parent 
* #MDOSE ^property[1].valueCode = #DOSE 
* #FORMAT "invalid format"
* #FORMAT ^property[0].code = #parent 
* #FORMAT ^property[0].valueCode = #VALIDAT 
* #ILLEGAL "illegal"
* #ILLEGAL ^property[0].code = #parent 
* #ILLEGAL ^property[0].valueCode = #VALIDAT 
* #KEY204 "Unknown key identifier"
* #KEY204 ^property[0].code = #parent 
* #KEY204 ^property[0].valueCode = #VALIDAT 
* #KEY204 ^property[1].code = #parent 
* #KEY204 ^property[1].valueCode = #VALIDAT 
* #KEY205 "Duplicate key identifier"
* #KEY205 ^property[0].code = #parent 
* #KEY205 ^property[0].valueCode = #VALIDAT 
* #KEY205 ^property[1].code = #parent 
* #KEY205 ^property[1].valueCode = #VALIDAT 
* #LEN_RANGE "length out of range"
* #LEN_RANGE ^property[0].code = #parent 
* #LEN_RANGE ^property[0].valueCode = #VALIDAT 
* #LEN_RANGE ^property[1].code = #child 
* #LEN_RANGE ^property[1].valueCode = #LEN_LONG 
* #LEN_RANGE ^property[2].code = #child 
* #LEN_RANGE ^property[2].valueCode = #LEN_SHORT 
* #LEN_LONG "length is too long"
* #LEN_LONG ^property[0].code = #parent 
* #LEN_LONG ^property[0].valueCode = #LEN_RANGE 
* #LEN_SHORT "length is too short"
* #LEN_SHORT ^property[0].code = #parent 
* #LEN_SHORT ^property[0].valueCode = #LEN_RANGE 
* #MISSCOND "conditional element missing"
* #MISSCOND ^property[0].code = #parent 
* #MISSCOND ^property[0].valueCode = #VALIDAT 
* #MISSMAND "mandatory element missing"
* #MISSMAND ^property[0].code = #parent 
* #MISSMAND ^property[0].valueCode = #VALIDAT 
* #NODUPS "duplicate values are not permitted"
* #NODUPS ^property[0].code = #parent 
* #NODUPS ^property[0].valueCode = #VALIDAT 
* #NOPERSIST "element will not be persisted"
* #NOPERSIST ^property[0].code = #parent 
* #NOPERSIST ^property[0].valueCode = #VALIDAT 
* #OBSA "Observation Alert"
* #OBSA ^property[0].code = #parent 
* #OBSA ^property[0].valueCode = #VALIDAT 
* #OBSA ^property[1].code = #child 
* #OBSA ^property[1].valueCode = #AGE 
* #OBSA ^property[2].code = #child 
* #OBSA ^property[2].valueCode = #COND 
* #OBSA ^property[3].code = #child 
* #OBSA ^property[3].valueCode = #CREACT 
* #OBSA ^property[4].code = #child 
* #OBSA ^property[4].valueCode = #GEN 
* #OBSA ^property[5].code = #child 
* #OBSA ^property[5].valueCode = #GEND 
* #OBSA ^property[6].code = #child 
* #OBSA ^property[6].valueCode = #LAB 
* #OBSA ^property[7].code = #child 
* #OBSA ^property[7].valueCode = #REACT 
* #OBSA ^property[8].code = #child 
* #OBSA ^property[8].valueCode = #RREACT 
* #AGE "Age Alert"
* #AGE ^property[0].code = #parent 
* #AGE ^property[0].valueCode = #OBSA 
* #COND "Condition Alert"
* #COND ^property[0].code = #parent 
* #COND ^property[0].valueCode = #OBSA 
* #COND ^property[1].code = #child 
* #COND ^property[1].valueCode = #HGHT 
* #COND ^property[2].code = #child 
* #COND ^property[2].valueCode = #LACT 
* #COND ^property[3].code = #child 
* #COND ^property[3].valueCode = #PREG 
* #COND ^property[4].code = #child 
* #COND ^property[4].valueCode = #WGHT 
* #HGHT "HGHT"
* #HGHT ^property[0].code = #parent 
* #HGHT ^property[0].valueCode = #COND 
* #LACT "Lactation Alert"
* #LACT ^property[0].code = #parent 
* #LACT ^property[0].valueCode = #COND 
* #PREG "Pregnancy Alert"
* #PREG ^property[0].code = #parent 
* #PREG ^property[0].valueCode = #COND 
* #WGHT "WGHT"
* #WGHT ^property[0].code = #parent 
* #WGHT ^property[0].valueCode = #COND 
* #CREACT "common reaction alert"
* #CREACT ^property[0].code = #parent 
* #CREACT ^property[0].valueCode = #OBSA 
* #GEN "Genetic Alert"
* #GEN ^property[0].code = #parent 
* #GEN ^property[0].valueCode = #OBSA 
* #GEND "Gender Alert"
* #GEND ^property[0].code = #parent 
* #GEND ^property[0].valueCode = #OBSA 
* #LAB "Lab Alert"
* #LAB ^property[0].code = #parent 
* #LAB ^property[0].valueCode = #OBSA 
* #REACT "Reaction Alert"
* #REACT ^property[0].code = #parent 
* #REACT ^property[0].valueCode = #OBSA 
* #REACT ^property[1].code = #child 
* #REACT ^property[1].valueCode = #ALGY 
* #REACT ^property[2].code = #child 
* #REACT ^property[2].valueCode = #INT 
* #ALGY "Allergy Alert"
* #ALGY ^property[0].code = #parent 
* #ALGY ^property[0].valueCode = #REACT 
* #INT "Intolerance Alert"
* #INT ^property[0].code = #parent 
* #INT ^property[0].valueCode = #REACT 
* #RREACT "Related Reaction Alert"
* #RREACT ^property[0].code = #parent 
* #RREACT ^property[0].valueCode = #OBSA 
* #RREACT ^property[1].code = #child 
* #RREACT ^property[1].valueCode = #RALG 
* #RREACT ^property[2].code = #child 
* #RREACT ^property[2].valueCode = #RAR 
* #RREACT ^property[3].code = #child 
* #RREACT ^property[3].valueCode = #RINT 
* #RALG "Related Allergy Alert"
* #RALG ^property[0].code = #parent 
* #RALG ^property[0].valueCode = #RREACT 
* #RAR "Related Prior Reaction Alert"
* #RAR ^property[0].code = #parent 
* #RAR ^property[0].valueCode = #RREACT 
* #RINT "Related Intolerance Alert"
* #RINT ^property[0].code = #parent 
* #RINT ^property[0].valueCode = #RREACT 
* #REP_RANGE "repetitions out of range"
* #REP_RANGE ^property[0].code = #parent 
* #REP_RANGE ^property[0].valueCode = #VALIDAT 
* #REP_RANGE ^property[1].code = #child 
* #REP_RANGE ^property[1].valueCode = #MAXOCCURS 
* #REP_RANGE ^property[2].code = #child 
* #REP_RANGE ^property[2].valueCode = #MINOCCURS 
* #MAXOCCURS "repetitions above maximum"
* #MAXOCCURS ^property[0].code = #parent 
* #MAXOCCURS ^property[0].valueCode = #REP_RANGE 
* #MINOCCURS "repetitions below minimum"
* #MINOCCURS ^property[0].code = #parent 
* #MINOCCURS ^property[0].valueCode = #REP_RANGE 
* #_ActAdministrativeRuleDetectedIssueCode "ActAdministrativeRuleDetectedIssueCode"
* #_ActAdministrativeRuleDetectedIssueCode ^property[0].code = #parent 
* #_ActAdministrativeRuleDetectedIssueCode ^property[0].valueCode = #_ActAdministrativeDetectedIssueCode 
* #_ActAdministrativeRuleDetectedIssueCode ^property[1].code = #child 
* #_ActAdministrativeRuleDetectedIssueCode ^property[1].valueCode = #KEY206 
* #_ActAdministrativeRuleDetectedIssueCode ^property[2].code = #child 
* #_ActAdministrativeRuleDetectedIssueCode ^property[2].valueCode = #OBSOLETE 
* #KEY206 "non-matching identification"
* #KEY206 ^property[0].code = #parent 
* #KEY206 ^property[0].valueCode = #_ActAdministrativeRuleDetectedIssueCode 
* #OBSOLETE "obsolete record returned"
* #OBSOLETE ^property[0].code = #parent 
* #OBSOLETE ^property[0].valueCode = #_ActAdministrativeRuleDetectedIssueCode 
* #_ActSuppliedItemDetectedIssueCode "ActSuppliedItemDetectedIssueCode"
* #_ActSuppliedItemDetectedIssueCode ^property[0].code = #parent 
* #_ActSuppliedItemDetectedIssueCode ^property[0].valueCode = #ISSUE 
* #_ActSuppliedItemDetectedIssueCode ^property[1].code = #child 
* #_ActSuppliedItemDetectedIssueCode ^property[1].valueCode = #HISTORIC 
* #_ActSuppliedItemDetectedIssueCode ^property[2].code = #child 
* #_ActSuppliedItemDetectedIssueCode ^property[2].valueCode = #PATPREF 
* #_ActSuppliedItemDetectedIssueCode ^property[3].code = #child 
* #_ActSuppliedItemDetectedIssueCode ^property[3].valueCode = #_AdministrationDetectedIssueCode 
* #_ActSuppliedItemDetectedIssueCode ^property[4].code = #child 
* #_ActSuppliedItemDetectedIssueCode ^property[4].valueCode = #_SupplyDetectedIssueCode 
* #HISTORIC "record recorded as historical"
* #HISTORIC ^property[0].code = #parent 
* #HISTORIC ^property[0].valueCode = #_ActSuppliedItemDetectedIssueCode 
* #PATPREF "violates stated preferences"
* #PATPREF ^property[0].code = #parent 
* #PATPREF ^property[0].valueCode = #_ActSuppliedItemDetectedIssueCode 
* #PATPREF ^property[1].code = #child 
* #PATPREF ^property[1].valueCode = #PATPREFALT 
* #PATPREFALT "violates stated preferences, alternate available"
* #PATPREFALT ^property[0].code = #parent 
* #PATPREFALT ^property[0].valueCode = #PATPREF 
* #_AdministrationDetectedIssueCode "AdministrationDetectedIssueCode"
* #_AdministrationDetectedIssueCode ^property[0].code = #parent 
* #_AdministrationDetectedIssueCode ^property[0].valueCode = #_ActSuppliedItemDetectedIssueCode 
* #_AdministrationDetectedIssueCode ^property[1].code = #child 
* #_AdministrationDetectedIssueCode ^property[1].valueCode = #DACT 
* #_AdministrationDetectedIssueCode ^property[2].code = #child 
* #_AdministrationDetectedIssueCode ^property[2].valueCode = #TIME 
* #_AdministrationDetectedIssueCode ^property[3].code = #child 
* #_AdministrationDetectedIssueCode ^property[3].valueCode = #_AppropriatenessDetectedIssueCode 
* #DACT "drug action detected issue"
* #DACT ^property[0].code = #parent 
* #DACT ^property[0].valueCode = #_AdministrationDetectedIssueCode 
* #TIME "timing detected issue"
* #TIME ^property[0].code = #parent 
* #TIME ^property[0].valueCode = #_AdministrationDetectedIssueCode 
* #TIME ^property[1].code = #child 
* #TIME ^property[1].valueCode = #ALRTENDLATE 
* #TIME ^property[2].code = #child 
* #TIME ^property[2].valueCode = #ALRTSTRTLATE 
* #ALRTENDLATE "end too late alert"
* #ALRTENDLATE ^property[0].code = #parent 
* #ALRTENDLATE ^property[0].valueCode = #TIME 
* #ALRTSTRTLATE "start too late alert"
* #ALRTSTRTLATE ^property[0].code = #parent 
* #ALRTSTRTLATE ^property[0].valueCode = #TIME 
* #_AppropriatenessDetectedIssueCode "AppropriatenessDetectedIssueCode"
* #_AppropriatenessDetectedIssueCode ^property[0].code = #parent 
* #_AppropriatenessDetectedIssueCode ^property[0].valueCode = #_AdministrationDetectedIssueCode 
* #_AppropriatenessDetectedIssueCode ^property[1].code = #child 
* #_AppropriatenessDetectedIssueCode ^property[1].valueCode = #PREVINEF 
* #_AppropriatenessDetectedIssueCode ^property[2].code = #child 
* #_AppropriatenessDetectedIssueCode ^property[2].valueCode = #_InteractionDetectedIssueCode 
* #PREVINEF "previously ineffective"
* #PREVINEF ^property[0].code = #parent 
* #PREVINEF ^property[0].valueCode = #_AppropriatenessDetectedIssueCode 
* #_InteractionDetectedIssueCode "InteractionDetectedIssueCode"
* #_InteractionDetectedIssueCode ^property[0].code = #parent 
* #_InteractionDetectedIssueCode ^property[0].valueCode = #_AppropriatenessDetectedIssueCode 
* #_InteractionDetectedIssueCode ^property[1].code = #child 
* #_InteractionDetectedIssueCode ^property[1].valueCode = #FOOD 
* #_InteractionDetectedIssueCode ^property[2].code = #child 
* #_InteractionDetectedIssueCode ^property[2].valueCode = #TPROD 
* #FOOD "Food Interaction Alert"
* #FOOD ^property[0].code = #parent 
* #FOOD ^property[0].valueCode = #_InteractionDetectedIssueCode 
* #TPROD "Therapeutic Product Alert"
* #TPROD ^property[0].code = #parent 
* #TPROD ^property[0].valueCode = #_InteractionDetectedIssueCode 
* #TPROD ^property[1].code = #child 
* #TPROD ^property[1].valueCode = #DRG 
* #TPROD ^property[2].code = #child 
* #TPROD ^property[2].valueCode = #NHP 
* #TPROD ^property[3].code = #child 
* #TPROD ^property[3].valueCode = #NONRX 
* #DRG "Drug Interaction Alert"
* #DRG ^property[0].code = #parent 
* #DRG ^property[0].valueCode = #TPROD 
* #NHP "Natural Health Product Alert"
* #NHP ^property[0].code = #parent 
* #NHP ^property[0].valueCode = #TPROD 
* #NONRX "Non-Prescription Interaction Alert"
* #NONRX ^property[0].code = #parent 
* #NONRX ^property[0].valueCode = #TPROD 
* #_SupplyDetectedIssueCode "SupplyDetectedIssueCode"
* #_SupplyDetectedIssueCode ^property[0].code = #parent 
* #_SupplyDetectedIssueCode ^property[0].valueCode = #_ActSuppliedItemDetectedIssueCode 
* #_SupplyDetectedIssueCode ^property[1].code = #child 
* #_SupplyDetectedIssueCode ^property[1].valueCode = #ALLDONE 
* #_SupplyDetectedIssueCode ^property[2].code = #child 
* #_SupplyDetectedIssueCode ^property[2].valueCode = #FULFIL 
* #_SupplyDetectedIssueCode ^property[3].code = #child 
* #_SupplyDetectedIssueCode ^property[3].valueCode = #HELD 
* #_SupplyDetectedIssueCode ^property[4].code = #child 
* #_SupplyDetectedIssueCode ^property[4].valueCode = #TOOLATE 
* #_SupplyDetectedIssueCode ^property[5].code = #child 
* #_SupplyDetectedIssueCode ^property[5].valueCode = #TOOSOON 
* #ALLDONE "already performed"
* #ALLDONE ^property[0].code = #parent 
* #ALLDONE ^property[0].valueCode = #_SupplyDetectedIssueCode 
* #FULFIL "fulfillment alert"
* #FULFIL ^property[0].code = #parent 
* #FULFIL ^property[0].valueCode = #_SupplyDetectedIssueCode 
* #FULFIL ^property[1].code = #child 
* #FULFIL ^property[1].valueCode = #NOTACTN 
* #FULFIL ^property[2].code = #child 
* #FULFIL ^property[2].valueCode = #OTEQUIV 
* #FULFIL ^property[3].code = #child 
* #FULFIL ^property[3].valueCode = #TIMING 
* #NOTACTN "no longer actionable"
* #NOTACTN ^property[0].code = #parent 
* #NOTACTN ^property[0].valueCode = #FULFIL 
* #OTEQUIV "not equivalent alert"
* #OTEQUIV ^property[0].code = #parent 
* #OTEQUIV ^property[0].valueCode = #FULFIL 
* #OTEQUIV ^property[1].code = #child 
* #OTEQUIV ^property[1].valueCode = #NOTEQUIVGEN 
* #OTEQUIV ^property[2].code = #child 
* #OTEQUIV ^property[2].valueCode = #NOTEQUIVTHER 
* #NOTEQUIVGEN "not generically equivalent alert"
* #NOTEQUIVGEN ^property[0].code = #parent 
* #NOTEQUIVGEN ^property[0].valueCode = #OTEQUIV 
* #NOTEQUIVTHER "not therapeutically equivalent alert"
* #NOTEQUIVTHER ^property[0].code = #parent 
* #NOTEQUIVTHER ^property[0].valueCode = #OTEQUIV 
* #TIMING "event timing incorrect alert"
* #TIMING ^property[0].code = #parent 
* #TIMING ^property[0].valueCode = #FULFIL 
* #TIMING ^property[1].code = #child 
* #TIMING ^property[1].valueCode = #INTERVAL 
* #TIMING ^property[2].code = #child 
* #TIMING ^property[2].valueCode = #MINFREQ 
* #INTERVAL "outside requested time"
* #INTERVAL ^property[0].code = #parent 
* #INTERVAL ^property[0].valueCode = #TIMING 
* #MINFREQ "too soon within frequency based on the usage"
* #MINFREQ ^property[0].code = #parent 
* #MINFREQ ^property[0].valueCode = #TIMING 
* #HELD "held/suspended alert"
* #HELD ^property[0].code = #parent 
* #HELD ^property[0].valueCode = #_SupplyDetectedIssueCode 
* #TOOLATE "Refill Too Late Alert"
* #TOOLATE ^property[0].code = #parent 
* #TOOLATE ^property[0].valueCode = #_SupplyDetectedIssueCode 
* #TOOSOON "Refill Too Soon Alert"
* #TOOSOON ^property[0].code = #parent 
* #TOOSOON ^property[0].valueCode = #_SupplyDetectedIssueCode 
* #IndividualCaseSafetyReportType "Individual Case Safety Report Type"
* #IndividualCaseSafetyReportType ^property[0].code = #parent 
* #IndividualCaseSafetyReportType ^property[0].valueCode = #_ObservationType 
* #IndividualCaseSafetyReportType ^property[1].code = #child 
* #IndividualCaseSafetyReportType ^property[1].valueCode = #PAT_ADV_EVNT 
* #IndividualCaseSafetyReportType ^property[2].code = #child 
* #IndividualCaseSafetyReportType ^property[2].valueCode = #VAC_PROBLEM 
* #PAT_ADV_EVNT "patient adverse event"
* #PAT_ADV_EVNT ^property[0].code = #parent 
* #PAT_ADV_EVNT ^property[0].valueCode = #IndividualCaseSafetyReportType 
* #VAC_PROBLEM "vaccine product problem"
* #VAC_PROBLEM ^property[0].code = #parent 
* #VAC_PROBLEM ^property[0].valueCode = #IndividualCaseSafetyReportType 
* #KSUBJ "knowledge subject"
* #KSUBJ ^property[0].code = #parent 
* #KSUBJ ^property[0].valueCode = #_ObservationType 
* #KSUBT "knowledge subtopic"
* #KSUBT ^property[0].code = #parent 
* #KSUBT ^property[0].valueCode = #_ObservationType 
* #OINT "intolerance"
* #OINT ^property[0].code = #parent 
* #OINT ^property[0].valueCode = #_ObservationType 
* #OINT ^property[1].code = #child 
* #OINT ^property[1].valueCode = #ALG 
* #OINT ^property[2].code = #child 
* #OINT ^property[2].valueCode = #DINT 
* #OINT ^property[3].code = #child 
* #OINT ^property[3].valueCode = #EINT 
* #OINT ^property[4].code = #child 
* #OINT ^property[4].valueCode = #FINT 
* #OINT ^property[5].code = #child 
* #OINT ^property[5].valueCode = #NAINT 
* #ALG "Allergy"
* #ALG ^property[0].code = #parent 
* #ALG ^property[0].valueCode = #OINT 
* #ALG ^property[1].code = #child 
* #ALG ^property[1].valueCode = #DALG 
* #ALG ^property[2].code = #child 
* #ALG ^property[2].valueCode = #EALG 
* #ALG ^property[3].code = #child 
* #ALG ^property[3].valueCode = #FALG 
* #DALG "Drug Allergy"
* #DALG ^property[0].code = #parent 
* #DALG ^property[0].valueCode = #ALG 
* #EALG "Environmental Allergy"
* #EALG ^property[0].code = #parent 
* #EALG ^property[0].valueCode = #ALG 
* #FALG "Food Allergy"
* #FALG ^property[0].code = #parent 
* #FALG ^property[0].valueCode = #ALG 
* #DINT "Drug Intolerance"
* #DINT ^property[0].code = #parent 
* #DINT ^property[0].valueCode = #OINT 
* #DINT ^property[1].code = #child 
* #DINT ^property[1].valueCode = #DNAINT 
* #DNAINT "Drug Non-Allergy Intolerance"
* #DNAINT ^property[0].code = #parent 
* #DNAINT ^property[0].valueCode = #DINT 
* #EINT "Environmental Intolerance"
* #EINT ^property[0].code = #parent 
* #EINT ^property[0].valueCode = #OINT 
* #EINT ^property[1].code = #child 
* #EINT ^property[1].valueCode = #ENAINT 
* #ENAINT "Environmental Non-Allergy Intolerance"
* #ENAINT ^property[0].code = #parent 
* #ENAINT ^property[0].valueCode = #EINT 
* #FINT "Food Intolerance"
* #FINT ^property[0].code = #parent 
* #FINT ^property[0].valueCode = #OINT 
* #FINT ^property[1].code = #child 
* #FINT ^property[1].valueCode = #FNAINT 
* #FNAINT "Food Non-Allergy Intolerance"
* #FNAINT ^property[0].code = #parent 
* #FNAINT ^property[0].valueCode = #FINT 
* #NAINT "Non-Allergy Intolerance"
* #NAINT ^property[0].code = #parent 
* #NAINT ^property[0].valueCode = #OINT 
* #SEV "Severity Observation"
* #SEV ^property[0].code = #parent 
* #SEV ^property[0].valueCode = #_ObservationType 
* #_ActSpecObsCode "ActSpecObsCode"
* #_ActSpecObsCode ^property[0].code = #parent 
* #_ActSpecObsCode ^property[0].valueCode = #_ObservationType 
* #_ActSpecObsCode ^property[1].code = #child 
* #_ActSpecObsCode ^property[1].valueCode = #ARTBLD 
* #_ActSpecObsCode ^property[2].code = #child 
* #_ActSpecObsCode ^property[2].valueCode = #DILUTION 
* #_ActSpecObsCode ^property[3].code = #child 
* #_ActSpecObsCode ^property[3].valueCode = #EVNFCTS 
* #_ActSpecObsCode ^property[4].code = #child 
* #_ActSpecObsCode ^property[4].valueCode = #INTFR 
* #_ActSpecObsCode ^property[5].code = #child 
* #_ActSpecObsCode ^property[5].valueCode = #VOLUME 
* #ARTBLD "ActSpecObsArtBldCode"
* #ARTBLD ^property[0].code = #parent 
* #ARTBLD ^property[0].valueCode = #_ActSpecObsCode 
* #DILUTION "ActSpecObsDilutionCode"
* #DILUTION ^property[0].code = #parent 
* #DILUTION ^property[0].valueCode = #_ActSpecObsCode 
* #DILUTION ^property[1].code = #child 
* #DILUTION ^property[1].valueCode = #AUTO-HIGH 
* #DILUTION ^property[2].code = #child 
* #DILUTION ^property[2].valueCode = #AUTO-LOW 
* #DILUTION ^property[3].code = #child 
* #DILUTION ^property[3].valueCode = #PRE 
* #DILUTION ^property[4].code = #child 
* #DILUTION ^property[4].valueCode = #RERUN 
* #AUTO-HIGH "Auto-High Dilution"
* #AUTO-HIGH ^property[0].code = #parent 
* #AUTO-HIGH ^property[0].valueCode = #DILUTION 
* #AUTO-LOW "Auto-Low Dilution"
* #AUTO-LOW ^property[0].code = #parent 
* #AUTO-LOW ^property[0].valueCode = #DILUTION 
* #PRE "Pre-Dilution"
* #PRE ^property[0].code = #parent 
* #PRE ^property[0].valueCode = #DILUTION 
* #RERUN "Rerun Dilution"
* #RERUN ^property[0].code = #parent 
* #RERUN ^property[0].valueCode = #DILUTION 
* #EVNFCTS "ActSpecObsEvntfctsCode"
* #EVNFCTS ^property[0].code = #parent 
* #EVNFCTS ^property[0].valueCode = #_ActSpecObsCode 
* #INTFR "ActSpecObsInterferenceCode"
* #INTFR ^property[0].code = #parent 
* #INTFR ^property[0].valueCode = #_ActSpecObsCode 
* #INTFR ^property[1].code = #child 
* #INTFR ^property[1].valueCode = #FIBRIN 
* #INTFR ^property[2].code = #child 
* #INTFR ^property[2].valueCode = #HEMOLYSIS 
* #INTFR ^property[3].code = #child 
* #INTFR ^property[3].valueCode = #ICTERUS 
* #INTFR ^property[4].code = #child 
* #INTFR ^property[4].valueCode = #LIPEMIA 
* #FIBRIN "Fibrin"
* #FIBRIN ^property[0].code = #parent 
* #FIBRIN ^property[0].valueCode = #INTFR 
* #HEMOLYSIS "Hemolysis"
* #HEMOLYSIS ^property[0].code = #parent 
* #HEMOLYSIS ^property[0].valueCode = #INTFR 
* #ICTERUS "Icterus"
* #ICTERUS ^property[0].code = #parent 
* #ICTERUS ^property[0].valueCode = #INTFR 
* #LIPEMIA "Lipemia"
* #LIPEMIA ^property[0].code = #parent 
* #LIPEMIA ^property[0].valueCode = #INTFR 
* #VOLUME "ActSpecObsVolumeCode"
* #VOLUME ^property[0].code = #parent 
* #VOLUME ^property[0].valueCode = #_ActSpecObsCode 
* #VOLUME ^property[1].code = #child 
* #VOLUME ^property[1].valueCode = #AVAILABLE 
* #VOLUME ^property[2].code = #child 
* #VOLUME ^property[2].valueCode = #CONSUMPTION 
* #VOLUME ^property[3].code = #child 
* #VOLUME ^property[3].valueCode = #CURRENT 
* #VOLUME ^property[4].code = #child 
* #VOLUME ^property[4].valueCode = #INITIAL 
* #AVAILABLE "Available Volume"
* #AVAILABLE ^property[0].code = #parent 
* #AVAILABLE ^property[0].valueCode = #VOLUME 
* #CONSUMPTION "Consumption Volume"
* #CONSUMPTION ^property[0].code = #parent 
* #CONSUMPTION ^property[0].valueCode = #VOLUME 
* #CURRENT "Current Volume"
* #CURRENT ^property[0].code = #parent 
* #CURRENT ^property[0].valueCode = #VOLUME 
* #INITIAL "Initial Volume"
* #INITIAL ^property[0].code = #parent 
* #INITIAL ^property[0].valueCode = #VOLUME 
* #_AnnotationType "AnnotationType"
* #_AnnotationType ^property[0].code = #parent 
* #_AnnotationType ^property[0].valueCode = #_ObservationType 
* #_AnnotationType ^property[1].code = #child 
* #_AnnotationType ^property[1].valueCode = #_ActPatientAnnotationType 
* #_ActPatientAnnotationType "ActPatientAnnotationType"
* #_ActPatientAnnotationType ^property[0].code = #parent 
* #_ActPatientAnnotationType ^property[0].valueCode = #_AnnotationType 
* #_ActPatientAnnotationType ^property[1].code = #child 
* #_ActPatientAnnotationType ^property[1].valueCode = #ANNDI 
* #_ActPatientAnnotationType ^property[2].code = #child 
* #_ActPatientAnnotationType ^property[2].valueCode = #ANNGEN 
* #_ActPatientAnnotationType ^property[3].code = #child 
* #_ActPatientAnnotationType ^property[3].valueCode = #ANNIMM 
* #_ActPatientAnnotationType ^property[4].code = #child 
* #_ActPatientAnnotationType ^property[4].valueCode = #ANNLAB 
* #_ActPatientAnnotationType ^property[5].code = #child 
* #_ActPatientAnnotationType ^property[5].valueCode = #ANNMED 
* #ANNDI "diagnostic image note"
* #ANNDI ^property[0].code = #parent 
* #ANNDI ^property[0].valueCode = #_ActPatientAnnotationType 
* #ANNGEN "general note"
* #ANNGEN ^property[0].code = #parent 
* #ANNGEN ^property[0].valueCode = #_ActPatientAnnotationType 
* #ANNIMM "immunization note"
* #ANNIMM ^property[0].code = #parent 
* #ANNIMM ^property[0].valueCode = #_ActPatientAnnotationType 
* #ANNLAB "laboratory note"
* #ANNLAB ^property[0].code = #parent 
* #ANNLAB ^property[0].valueCode = #_ActPatientAnnotationType 
* #ANNMED "medication note"
* #ANNMED ^property[0].code = #parent 
* #ANNMED ^property[0].valueCode = #_ActPatientAnnotationType 
* #_GeneticObservationType "GeneticObservationType"
* #_GeneticObservationType ^property[0].code = #parent 
* #_GeneticObservationType ^property[0].valueCode = #_ObservationType 
* #_GeneticObservationType ^property[1].code = #child 
* #_GeneticObservationType ^property[1].valueCode = #GENE 
* #GENE "gene"
* #GENE ^property[0].code = #parent 
* #GENE ^property[0].valueCode = #_GeneticObservationType 
* #_ImmunizationObservationType "ImmunizationObservationType"
* #_ImmunizationObservationType ^property[0].code = #parent 
* #_ImmunizationObservationType ^property[0].valueCode = #_ObservationType 
* #_ImmunizationObservationType ^property[1].code = #child 
* #_ImmunizationObservationType ^property[1].valueCode = #OBSANTC 
* #_ImmunizationObservationType ^property[2].code = #child 
* #_ImmunizationObservationType ^property[2].valueCode = #OBSANTV 
* #OBSANTC "antigen count"
* #OBSANTC ^property[0].code = #parent 
* #OBSANTC ^property[0].valueCode = #_ImmunizationObservationType 
* #OBSANTV "antigen validity"
* #OBSANTV ^property[0].code = #parent 
* #OBSANTV ^property[0].valueCode = #_ImmunizationObservationType 
* #_LOINCObservationActContextAgeType "LOINCObservationActContextAgeType"
* #_LOINCObservationActContextAgeType ^property[0].code = #parent 
* #_LOINCObservationActContextAgeType ^property[0].valueCode = #_ObservationType 
* #_LOINCObservationActContextAgeType ^property[1].code = #child 
* #_LOINCObservationActContextAgeType ^property[1].valueCode = #21611-9 
* #_LOINCObservationActContextAgeType ^property[2].code = #child 
* #_LOINCObservationActContextAgeType ^property[2].valueCode = #21612-7 
* #_LOINCObservationActContextAgeType ^property[3].code = #child 
* #_LOINCObservationActContextAgeType ^property[3].valueCode = #29553-5 
* #_LOINCObservationActContextAgeType ^property[4].code = #child 
* #_LOINCObservationActContextAgeType ^property[4].valueCode = #30525-0 
* #_LOINCObservationActContextAgeType ^property[5].code = #child 
* #_LOINCObservationActContextAgeType ^property[5].valueCode = #30972-4 
* #21611-9 "age patient qn est"
* #21611-9 ^property[0].code = #parent 
* #21611-9 ^property[0].valueCode = #_LOINCObservationActContextAgeType 
* #21612-7 "age patient qn reported"
* #21612-7 ^property[0].code = #parent 
* #21612-7 ^property[0].valueCode = #_LOINCObservationActContextAgeType 
* #29553-5 "age patient qn calc"
* #29553-5 ^property[0].code = #parent 
* #29553-5 ^property[0].valueCode = #_LOINCObservationActContextAgeType 
* #30525-0 "age patient qn definition"
* #30525-0 ^property[0].code = #parent 
* #30525-0 ^property[0].valueCode = #_LOINCObservationActContextAgeType 
* #30972-4 "age at onset of adverse event"
* #30972-4 ^property[0].code = #parent 
* #30972-4 ^property[0].valueCode = #_LOINCObservationActContextAgeType 
* #_MedicationObservationType "MedicationObservationType"
* #_MedicationObservationType ^property[0].code = #parent 
* #_MedicationObservationType ^property[0].valueCode = #_ObservationType 
* #_MedicationObservationType ^property[1].code = #child 
* #_MedicationObservationType ^property[1].valueCode = #REP_HALF_LIFE 
* #_MedicationObservationType ^property[2].code = #child 
* #_MedicationObservationType ^property[2].valueCode = #SPLCOATING 
* #_MedicationObservationType ^property[3].code = #child 
* #_MedicationObservationType ^property[3].valueCode = #SPLCOLOR 
* #_MedicationObservationType ^property[4].code = #child 
* #_MedicationObservationType ^property[4].valueCode = #SPLIMAGE 
* #_MedicationObservationType ^property[5].code = #child 
* #_MedicationObservationType ^property[5].valueCode = #SPLIMPRINT 
* #_MedicationObservationType ^property[6].code = #child 
* #_MedicationObservationType ^property[6].valueCode = #SPLSCORING 
* #_MedicationObservationType ^property[7].code = #child 
* #_MedicationObservationType ^property[7].valueCode = #SPLSHAPE 
* #_MedicationObservationType ^property[8].code = #child 
* #_MedicationObservationType ^property[8].valueCode = #SPLSIZE 
* #_MedicationObservationType ^property[9].code = #child 
* #_MedicationObservationType ^property[9].valueCode = #SPLSYMBOL 
* #REP_HALF_LIFE "representative half-life"
* #REP_HALF_LIFE ^property[0].code = #parent 
* #REP_HALF_LIFE ^property[0].valueCode = #_MedicationObservationType 
* #SPLCOATING "coating"
* #SPLCOATING ^property[0].code = #parent 
* #SPLCOATING ^property[0].valueCode = #_MedicationObservationType 
* #SPLCOLOR "color"
* #SPLCOLOR ^property[0].code = #parent 
* #SPLCOLOR ^property[0].valueCode = #_MedicationObservationType 
* #SPLIMAGE "image"
* #SPLIMAGE ^property[0].code = #parent 
* #SPLIMAGE ^property[0].valueCode = #_MedicationObservationType 
* #SPLIMPRINT "imprint"
* #SPLIMPRINT ^property[0].code = #parent 
* #SPLIMPRINT ^property[0].valueCode = #_MedicationObservationType 
* #SPLSCORING "scoring"
* #SPLSCORING ^property[0].code = #parent 
* #SPLSCORING ^property[0].valueCode = #_MedicationObservationType 
* #SPLSHAPE "shape"
* #SPLSHAPE ^property[0].code = #parent 
* #SPLSHAPE ^property[0].valueCode = #_MedicationObservationType 
* #SPLSIZE "size"
* #SPLSIZE ^property[0].code = #parent 
* #SPLSIZE ^property[0].valueCode = #_MedicationObservationType 
* #SPLSYMBOL "symbol"
* #SPLSYMBOL ^property[0].code = #parent 
* #SPLSYMBOL ^property[0].valueCode = #_MedicationObservationType 
* #_ObservationIssueTriggerCodedObservationType "ObservationIssueTriggerCodedObservationType"
* #_ObservationIssueTriggerCodedObservationType ^property[0].code = #parent 
* #_ObservationIssueTriggerCodedObservationType ^property[0].valueCode = #_ObservationType 
* #_ObservationIssueTriggerCodedObservationType ^property[1].code = #child 
* #_ObservationIssueTriggerCodedObservationType ^property[1].valueCode = #_CaseTransmissionMode 
* #_CaseTransmissionMode "case transmission mode"
* #_CaseTransmissionMode ^property[0].code = #parent 
* #_CaseTransmissionMode ^property[0].valueCode = #_ObservationIssueTriggerCodedObservationType 
* #_CaseTransmissionMode ^property[1].code = #child 
* #_CaseTransmissionMode ^property[1].valueCode = #AIRTRNS 
* #_CaseTransmissionMode ^property[2].code = #child 
* #_CaseTransmissionMode ^property[2].valueCode = #ANANTRNS 
* #_CaseTransmissionMode ^property[3].code = #child 
* #_CaseTransmissionMode ^property[3].valueCode = #ANHUMTRNS 
* #_CaseTransmissionMode ^property[4].code = #child 
* #_CaseTransmissionMode ^property[4].valueCode = #BDYFLDTRNS 
* #_CaseTransmissionMode ^property[5].code = #child 
* #_CaseTransmissionMode ^property[5].valueCode = #BLDTRNS 
* #_CaseTransmissionMode ^property[6].code = #child 
* #_CaseTransmissionMode ^property[6].valueCode = #DERMTRNS 
* #_CaseTransmissionMode ^property[7].code = #child 
* #_CaseTransmissionMode ^property[7].valueCode = #ENVTRNS 
* #_CaseTransmissionMode ^property[8].code = #child 
* #_CaseTransmissionMode ^property[8].valueCode = #FECTRNS 
* #_CaseTransmissionMode ^property[9].code = #child 
* #_CaseTransmissionMode ^property[9].valueCode = #FOMTRNS 
* #_CaseTransmissionMode ^property[10].code = #child 
* #_CaseTransmissionMode ^property[10].valueCode = #FOODTRNS 
* #_CaseTransmissionMode ^property[11].code = #child 
* #_CaseTransmissionMode ^property[11].valueCode = #HUMHUMTRNS 
* #_CaseTransmissionMode ^property[12].code = #child 
* #_CaseTransmissionMode ^property[12].valueCode = #INDTRNS 
* #_CaseTransmissionMode ^property[13].code = #child 
* #_CaseTransmissionMode ^property[13].valueCode = #LACTTRNS 
* #_CaseTransmissionMode ^property[14].code = #child 
* #_CaseTransmissionMode ^property[14].valueCode = #NOSTRNS 
* #_CaseTransmissionMode ^property[15].code = #child 
* #_CaseTransmissionMode ^property[15].valueCode = #PARTRNS 
* #_CaseTransmissionMode ^property[16].code = #child 
* #_CaseTransmissionMode ^property[16].valueCode = #PLACTRNS 
* #_CaseTransmissionMode ^property[17].code = #child 
* #_CaseTransmissionMode ^property[17].valueCode = #SEXTRNS 
* #_CaseTransmissionMode ^property[18].code = #child 
* #_CaseTransmissionMode ^property[18].valueCode = #TRNSFTRNS 
* #_CaseTransmissionMode ^property[19].code = #child 
* #_CaseTransmissionMode ^property[19].valueCode = #VECTRNS 
* #_CaseTransmissionMode ^property[20].code = #child 
* #_CaseTransmissionMode ^property[20].valueCode = #WATTRNS 
* #AIRTRNS "airborne transmission"
* #AIRTRNS ^property[0].code = #parent 
* #AIRTRNS ^property[0].valueCode = #_CaseTransmissionMode 
* #ANANTRNS "animal to animal transmission"
* #ANANTRNS ^property[0].code = #parent 
* #ANANTRNS ^property[0].valueCode = #_CaseTransmissionMode 
* #ANHUMTRNS "animal to human transmission"
* #ANHUMTRNS ^property[0].code = #parent 
* #ANHUMTRNS ^property[0].valueCode = #_CaseTransmissionMode 
* #BDYFLDTRNS "body fluid contact transmission"
* #BDYFLDTRNS ^property[0].code = #parent 
* #BDYFLDTRNS ^property[0].valueCode = #_CaseTransmissionMode 
* #BLDTRNS "blood borne transmission"
* #BLDTRNS ^property[0].code = #parent 
* #BLDTRNS ^property[0].valueCode = #_CaseTransmissionMode 
* #DERMTRNS "transdermal transmission"
* #DERMTRNS ^property[0].code = #parent 
* #DERMTRNS ^property[0].valueCode = #_CaseTransmissionMode 
* #ENVTRNS "environmental exposure transmission"
* #ENVTRNS ^property[0].code = #parent 
* #ENVTRNS ^property[0].valueCode = #_CaseTransmissionMode 
* #FECTRNS "fecal-oral transmission"
* #FECTRNS ^property[0].code = #parent 
* #FECTRNS ^property[0].valueCode = #_CaseTransmissionMode 
* #FOMTRNS "fomite transmission"
* #FOMTRNS ^property[0].code = #parent 
* #FOMTRNS ^property[0].valueCode = #_CaseTransmissionMode 
* #FOODTRNS "food-borne transmission"
* #FOODTRNS ^property[0].code = #parent 
* #FOODTRNS ^property[0].valueCode = #_CaseTransmissionMode 
* #HUMHUMTRNS "human to human transmission"
* #HUMHUMTRNS ^property[0].code = #parent 
* #HUMHUMTRNS ^property[0].valueCode = #_CaseTransmissionMode 
* #INDTRNS "indeterminate disease transmission mode"
* #INDTRNS ^property[0].code = #parent 
* #INDTRNS ^property[0].valueCode = #_CaseTransmissionMode 
* #LACTTRNS "lactation transmission"
* #LACTTRNS ^property[0].code = #parent 
* #LACTTRNS ^property[0].valueCode = #_CaseTransmissionMode 
* #NOSTRNS "nosocomial transmission"
* #NOSTRNS ^property[0].code = #parent 
* #NOSTRNS ^property[0].valueCode = #_CaseTransmissionMode 
* #PARTRNS "parenteral transmission"
* #PARTRNS ^property[0].code = #parent 
* #PARTRNS ^property[0].valueCode = #_CaseTransmissionMode 
* #PLACTRNS "transplacental transmission"
* #PLACTRNS ^property[0].code = #parent 
* #PLACTRNS ^property[0].valueCode = #_CaseTransmissionMode 
* #SEXTRNS "sexual transmission"
* #SEXTRNS ^property[0].code = #parent 
* #SEXTRNS ^property[0].valueCode = #_CaseTransmissionMode 
* #TRNSFTRNS "transfusion transmission"
* #TRNSFTRNS ^property[0].code = #parent 
* #TRNSFTRNS ^property[0].valueCode = #_CaseTransmissionMode 
* #VECTRNS "vector-borne transmission"
* #VECTRNS ^property[0].code = #parent 
* #VECTRNS ^property[0].valueCode = #_CaseTransmissionMode 
* #WATTRNS "water-borne transmission"
* #WATTRNS ^property[0].code = #parent 
* #WATTRNS ^property[0].valueCode = #_CaseTransmissionMode 
* #_ObservationQualityMeasureAttribute "ObservationQualityMeasureAttribute"
* #_ObservationQualityMeasureAttribute ^property[0].code = #parent 
* #_ObservationQualityMeasureAttribute ^property[0].valueCode = #_ObservationType 
* #_ObservationQualityMeasureAttribute ^property[1].code = #child 
* #_ObservationQualityMeasureAttribute ^property[1].valueCode = #COPY 
* #_ObservationQualityMeasureAttribute ^property[2].code = #child 
* #_ObservationQualityMeasureAttribute ^property[2].valueCode = #DISC 
* #_ObservationQualityMeasureAttribute ^property[3].code = #child 
* #_ObservationQualityMeasureAttribute ^property[3].valueCode = #KEY 
* #_ObservationQualityMeasureAttribute ^property[4].code = #child 
* #_ObservationQualityMeasureAttribute ^property[4].valueCode = #MSRADJ 
* #_ObservationQualityMeasureAttribute ^property[5].code = #child 
* #_ObservationQualityMeasureAttribute ^property[5].valueCode = #MSRAGG 
* #_ObservationQualityMeasureAttribute ^property[6].code = #child 
* #_ObservationQualityMeasureAttribute ^property[6].valueCode = #MSRIMPROV 
* #_ObservationQualityMeasureAttribute ^property[7].code = #child 
* #_ObservationQualityMeasureAttribute ^property[7].valueCode = #MSRSCORE 
* #_ObservationQualityMeasureAttribute ^property[8].code = #child 
* #_ObservationQualityMeasureAttribute ^property[8].valueCode = #MSRSET 
* #_ObservationQualityMeasureAttribute ^property[9].code = #child 
* #_ObservationQualityMeasureAttribute ^property[9].valueCode = #MSRTOPIC 
* #_ObservationQualityMeasureAttribute ^property[10].code = #child 
* #_ObservationQualityMeasureAttribute ^property[10].valueCode = #MSRTYPE 
* #_ObservationQualityMeasureAttribute ^property[11].code = #child 
* #_ObservationQualityMeasureAttribute ^property[11].valueCode = #RAT 
* #_ObservationQualityMeasureAttribute ^property[12].code = #child 
* #_ObservationQualityMeasureAttribute ^property[12].valueCode = #REF 
* #_ObservationQualityMeasureAttribute ^property[13].code = #child 
* #_ObservationQualityMeasureAttribute ^property[13].valueCode = #USE 
* #COPY "copyright"
* #COPY ^property[0].code = #parent 
* #COPY ^property[0].valueCode = #_ObservationQualityMeasureAttribute 
* #DISC "disclaimer"
* #DISC ^property[0].code = #parent 
* #DISC ^property[0].valueCode = #_ObservationQualityMeasureAttribute 
* #KEY "keyword"
* #KEY ^property[0].code = #parent 
* #KEY ^property[0].valueCode = #_ObservationQualityMeasureAttribute 
* #MSRADJ "health quality measure risk adjustment"
* #MSRADJ ^property[0].code = #parent 
* #MSRADJ ^property[0].valueCode = #_ObservationQualityMeasureAttribute 
* #MSRAGG "health quality measure data aggregation"
* #MSRAGG ^property[0].code = #parent 
* #MSRAGG ^property[0].valueCode = #_ObservationQualityMeasureAttribute 
* #MSRIMPROV "health quality measure improvement notation"
* #MSRIMPROV ^property[0].code = #parent 
* #MSRIMPROV ^property[0].valueCode = #_ObservationQualityMeasureAttribute 
* #MSRSCORE "health quality measure scoring"
* #MSRSCORE ^property[0].code = #parent 
* #MSRSCORE ^property[0].valueCode = #_ObservationQualityMeasureAttribute 
* #MSRSET "health quality measure care setting"
* #MSRSET ^property[0].code = #parent 
* #MSRSET ^property[0].valueCode = #_ObservationQualityMeasureAttribute 
* #MSRTOPIC "health quality measure topic type"
* #MSRTOPIC ^property[0].code = #parent 
* #MSRTOPIC ^property[0].valueCode = #_ObservationQualityMeasureAttribute 
* #MSRTYPE "health quality measure type"
* #MSRTYPE ^property[0].code = #parent 
* #MSRTYPE ^property[0].valueCode = #_ObservationQualityMeasureAttribute 
* #RAT "rationale"
* #RAT ^property[0].code = #parent 
* #RAT ^property[0].valueCode = #_ObservationQualityMeasureAttribute 
* #REF "reference"
* #REF ^property[0].code = #parent 
* #REF ^property[0].valueCode = #_ObservationQualityMeasureAttribute 
* #USE "notice of use"
* #USE ^property[0].code = #parent 
* #USE ^property[0].valueCode = #_ObservationQualityMeasureAttribute 
* #_ObservationSequenceType "ObservationSequenceType"
* #_ObservationSequenceType ^property[0].code = #parent 
* #_ObservationSequenceType ^property[0].valueCode = #_ObservationType 
* #_ObservationSequenceType ^property[1].code = #child 
* #_ObservationSequenceType ^property[1].valueCode = #TIME_ABSOLUTE 
* #_ObservationSequenceType ^property[2].code = #child 
* #_ObservationSequenceType ^property[2].valueCode = #TIME_RELATIVE 
* #TIME_ABSOLUTE "absolute time sequence"
* #TIME_ABSOLUTE ^property[0].code = #parent 
* #TIME_ABSOLUTE ^property[0].valueCode = #_ObservationSequenceType 
* #TIME_RELATIVE "relative time sequence"
* #TIME_RELATIVE ^property[0].code = #parent 
* #TIME_RELATIVE ^property[0].valueCode = #_ObservationSequenceType 
* #_ObservationSeriesType "ObservationSeriesType"
* #_ObservationSeriesType ^property[0].code = #parent 
* #_ObservationSeriesType ^property[0].valueCode = #_ObservationType 
* #_ObservationSeriesType ^property[1].code = #child 
* #_ObservationSeriesType ^property[1].valueCode = #_ECGObservationSeriesType 
* #_ECGObservationSeriesType "ECGObservationSeriesType"
* #_ECGObservationSeriesType ^property[0].code = #parent 
* #_ECGObservationSeriesType ^property[0].valueCode = #_ObservationSeriesType 
* #_ECGObservationSeriesType ^property[1].code = #child 
* #_ECGObservationSeriesType ^property[1].valueCode = #REPRESENTATIVE_BEAT 
* #_ECGObservationSeriesType ^property[2].code = #child 
* #_ECGObservationSeriesType ^property[2].valueCode = #RHYTHM 
* #REPRESENTATIVE_BEAT "ECG representative beat waveforms"
* #REPRESENTATIVE_BEAT ^property[0].code = #parent 
* #REPRESENTATIVE_BEAT ^property[0].valueCode = #_ECGObservationSeriesType 
* #RHYTHM "ECG rhythm waveforms"
* #RHYTHM ^property[0].code = #parent 
* #RHYTHM ^property[0].valueCode = #_ECGObservationSeriesType 
* #_PatientImmunizationRelatedObservationType "PatientImmunizationRelatedObservationType"
* #_PatientImmunizationRelatedObservationType ^property[0].code = #parent 
* #_PatientImmunizationRelatedObservationType ^property[0].valueCode = #_ObservationType 
* #_PatientImmunizationRelatedObservationType ^property[1].code = #child 
* #_PatientImmunizationRelatedObservationType ^property[1].valueCode = #CLSSRM 
* #_PatientImmunizationRelatedObservationType ^property[2].code = #child 
* #_PatientImmunizationRelatedObservationType ^property[2].valueCode = #GRADE 
* #_PatientImmunizationRelatedObservationType ^property[3].code = #child 
* #_PatientImmunizationRelatedObservationType ^property[3].valueCode = #SCHL 
* #_PatientImmunizationRelatedObservationType ^property[4].code = #child 
* #_PatientImmunizationRelatedObservationType ^property[4].valueCode = #SCHLDIV 
* #_PatientImmunizationRelatedObservationType ^property[5].code = #child 
* #_PatientImmunizationRelatedObservationType ^property[5].valueCode = #TEACHER 
* #CLSSRM "classroom"
* #CLSSRM ^property[0].code = #parent 
* #CLSSRM ^property[0].valueCode = #_PatientImmunizationRelatedObservationType 
* #GRADE "grade"
* #GRADE ^property[0].code = #parent 
* #GRADE ^property[0].valueCode = #_PatientImmunizationRelatedObservationType 
* #SCHL "school"
* #SCHL ^property[0].code = #parent 
* #SCHL ^property[0].valueCode = #_PatientImmunizationRelatedObservationType 
* #SCHLDIV "school division"
* #SCHLDIV ^property[0].code = #parent 
* #SCHLDIV ^property[0].valueCode = #_PatientImmunizationRelatedObservationType 
* #TEACHER "teacher"
* #TEACHER ^property[0].code = #parent 
* #TEACHER ^property[0].valueCode = #_PatientImmunizationRelatedObservationType 
* #_ROIOverlayShape "ROIOverlayShape"
* #_ROIOverlayShape ^property[0].code = #child 
* #_ROIOverlayShape ^property[0].valueCode = #CIRCLE 
* #_ROIOverlayShape ^property[1].code = #child 
* #_ROIOverlayShape ^property[1].valueCode = #ELLIPSE 
* #_ROIOverlayShape ^property[2].code = #child 
* #_ROIOverlayShape ^property[2].valueCode = #POINT 
* #_ROIOverlayShape ^property[3].code = #child 
* #_ROIOverlayShape ^property[3].valueCode = #POLY 
* #CIRCLE "circle"
* #CIRCLE ^property[0].code = #parent 
* #CIRCLE ^property[0].valueCode = #_ROIOverlayShape 
* #ELLIPSE "ellipse"
* #ELLIPSE ^property[0].code = #parent 
* #ELLIPSE ^property[0].valueCode = #_ROIOverlayShape 
* #POINT "point"
* #POINT ^property[0].code = #parent 
* #POINT ^property[0].valueCode = #_ROIOverlayShape 
* #POLY "polyline"
* #POLY ^property[0].code = #parent 
* #POLY ^property[0].valueCode = #_ROIOverlayShape 
