Instance: elga-realmcode 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-realmcode" 
* name = "elga-realmcode" 
* title = "ELGA_RealmCode" 
* status = #active 
* version = "2.6" 
* description = "**Description:** Coding of the documents realm. In ELGA documents must use the code 'AT'

**Beschreibung:** Beschreibt den Hoheitsbereich des Dokuments. In ELGA fixiert mit AT." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.3" 
* date = "2013-01-10" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-iso-3166-alpha-2-code"
* compose.include[0].concept[0].code = #AT
* compose.include[0].concept[0].display = "Austria"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "Österreich" 
* compose.include[0].concept[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[0].designation[1].value = "Einziger erlaubter Wert" 

* expansion.timestamp = "2022-09-13T14:17:36.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem-iso-3166-alpha-2-code"
* expansion.contains[0].code = #AT
* expansion.contains[0].display = "Austria"
* expansion.contains[0].designation[0].language = #de-AT 
* expansion.contains[0].designation[0].value = "Österreich" 
* expansion.contains[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* expansion.contains[0].designation[1].value = "Einziger erlaubter Wert" 
