Instance: ems-hcv-rna 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-hcv-rna" 
* name = "ems-hcv-rna" 
* title = "EMS_HCV_RNA" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** Weitere Angabe zum HCV-RNA Test" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.106" 
* date = "2013-01-10" 
* count = 4 
* concept[0].code = #N
* concept[0].display = "Nachweisbar"
* concept[1].code = #NAW
* concept[1].display = "Invalid bzw. nicht auswertbar"
* concept[2].code = #NN
* concept[2].display = "nicht nachweisbar"
* concept[3].code = #NOTEST
* concept[3].display = "nicht durchgeführt"
