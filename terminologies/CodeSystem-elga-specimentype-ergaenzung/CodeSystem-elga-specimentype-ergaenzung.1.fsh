Instance: elga-specimentype-ergaenzung 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-specimentype-ergaenzung" 
* name = "elga-specimentype-ergaenzung" 
* title = "ELGA_SpecimenType_Ergaenzung" 
* status = #active 
* content = #complete 
* version = "202004" 
* description = "**Description:** Supplementary list for specimen codes

**Beschreibung:** Ergänzungsliste für Art des Specimens oder der Probe" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.173" 
* date = "2020-04-27" 
* count = 3 
* concept[0].code = #SWAB
* concept[0].display = "Swab"
* concept[0].designation[0].language = #de-AT 
* concept[0].designation[0].value = "Abstrich" 
* concept[1].code = #_HL7Specimen
* concept[1].display = "HL7 Specimen Type"
* concept[2].code = #_SNOMEDSpecimen
* concept[2].display = "SNOMED Specimen Type"
