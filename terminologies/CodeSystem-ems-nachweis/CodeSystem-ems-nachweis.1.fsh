Instance: ems-nachweis 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-nachweis" 
* name = "ems-nachweis" 
* title = "EMS_Nachweis" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Nachweis" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.81" 
* date = "2017-01-26" 
* count = 4 
* concept[0].code = #EA
* concept[0].display = "Ergebnis ausständig"
* concept[1].code = #NA
* concept[1].display = "nicht anwendbar"
* concept[2].code = #NE
* concept[2].display = "Nachweis erfolgt"
* concept[3].code = #NNE
* concept[3].display = "Nachweis nicht erfolgt"
