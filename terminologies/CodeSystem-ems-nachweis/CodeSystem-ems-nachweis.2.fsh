Instance: ems-nachweis 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-nachweis" 
* name = "ems-nachweis" 
* title = "EMS_Nachweis" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Nachweis" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.81" 
* date = "2017-01-26" 
* count = 4 
* #EA "Ergebnis ausständig"
* #NA "nicht anwendbar"
* #NE "Nachweis erfolgt"
* #NNE "Nachweis nicht erfolgt"
