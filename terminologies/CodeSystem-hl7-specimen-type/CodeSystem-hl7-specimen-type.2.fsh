Instance: hl7-specimen-type 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-specimen-type" 
* name = "hl7-specimen-type" 
* title = "HL7 Specimen Type" 
* status = #active 
* content = #complete 
* version = "HL7SpecimenType_3.3" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.5.129" 
* date = "2015-11-01" 
* count = 126 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #_SpecimenEntityType "SpecimenEntityType"
* #_SpecimenEntityType ^property[0].code = #child 
* #_SpecimenEntityType ^property[0].valueCode = #ABS 
* #_SpecimenEntityType ^property[1].code = #child 
* #_SpecimenEntityType ^property[1].valueCode = #AMN 
* #_SpecimenEntityType ^property[2].code = #child 
* #_SpecimenEntityType ^property[2].valueCode = #ASP 
* #_SpecimenEntityType ^property[3].code = #child 
* #_SpecimenEntityType ^property[3].valueCode = #BBL 
* #_SpecimenEntityType ^property[4].code = #child 
* #_SpecimenEntityType ^property[4].valueCode = #BDY 
* #_SpecimenEntityType ^property[5].code = #child 
* #_SpecimenEntityType ^property[5].valueCode = #BIFL 
* #_SpecimenEntityType ^property[6].code = #child 
* #_SpecimenEntityType ^property[6].valueCode = #BLD 
* #_SpecimenEntityType ^property[7].code = #child 
* #_SpecimenEntityType ^property[7].valueCode = #BLDA 
* #_SpecimenEntityType ^property[8].code = #child 
* #_SpecimenEntityType ^property[8].valueCode = #BLDC 
* #_SpecimenEntityType ^property[9].code = #child 
* #_SpecimenEntityType ^property[9].valueCode = #BLDCO 
* #_SpecimenEntityType ^property[10].code = #child 
* #_SpecimenEntityType ^property[10].valueCode = #BLDV 
* #_SpecimenEntityType ^property[11].code = #child 
* #_SpecimenEntityType ^property[11].valueCode = #BON 
* #_SpecimenEntityType ^property[12].code = #child 
* #_SpecimenEntityType ^property[12].valueCode = #BPH 
* #_SpecimenEntityType ^property[13].code = #child 
* #_SpecimenEntityType ^property[13].valueCode = #BPU 
* #_SpecimenEntityType ^property[14].code = #child 
* #_SpecimenEntityType ^property[14].valueCode = #BRN 
* #_SpecimenEntityType ^property[15].code = #child 
* #_SpecimenEntityType ^property[15].valueCode = #BRO 
* #_SpecimenEntityType ^property[16].code = #child 
* #_SpecimenEntityType ^property[16].valueCode = #BRTH 
* #_SpecimenEntityType ^property[17].code = #child 
* #_SpecimenEntityType ^property[17].valueCode = #CALC 
* #_SpecimenEntityType ^property[18].code = #child 
* #_SpecimenEntityType ^property[18].valueCode = #CDM 
* #_SpecimenEntityType ^property[19].code = #child 
* #_SpecimenEntityType ^property[19].valueCode = #CNJT 
* #_SpecimenEntityType ^property[20].code = #child 
* #_SpecimenEntityType ^property[20].valueCode = #CNL 
* #_SpecimenEntityType ^property[21].code = #child 
* #_SpecimenEntityType ^property[21].valueCode = #COL 
* #_SpecimenEntityType ^property[22].code = #child 
* #_SpecimenEntityType ^property[22].valueCode = #CRN 
* #_SpecimenEntityType ^property[23].code = #child 
* #_SpecimenEntityType ^property[23].valueCode = #CSF 
* #_SpecimenEntityType ^property[24].code = #child 
* #_SpecimenEntityType ^property[24].valueCode = #CTP 
* #_SpecimenEntityType ^property[25].code = #child 
* #_SpecimenEntityType ^property[25].valueCode = #CUR 
* #_SpecimenEntityType ^property[26].code = #child 
* #_SpecimenEntityType ^property[26].valueCode = #CVM 
* #_SpecimenEntityType ^property[27].code = #child 
* #_SpecimenEntityType ^property[27].valueCode = #CVX 
* #_SpecimenEntityType ^property[28].code = #child 
* #_SpecimenEntityType ^property[28].valueCode = #CYST 
* #_SpecimenEntityType ^property[29].code = #child 
* #_SpecimenEntityType ^property[29].valueCode = #DIAF 
* #_SpecimenEntityType ^property[30].code = #child 
* #_SpecimenEntityType ^property[30].valueCode = #DOSE 
* #_SpecimenEntityType ^property[31].code = #child 
* #_SpecimenEntityType ^property[31].valueCode = #DRN 
* #_SpecimenEntityType ^property[32].code = #child 
* #_SpecimenEntityType ^property[32].valueCode = #DUFL 
* #_SpecimenEntityType ^property[33].code = #child 
* #_SpecimenEntityType ^property[33].valueCode = #EAR 
* #_SpecimenEntityType ^property[34].code = #child 
* #_SpecimenEntityType ^property[34].valueCode = #EARW 
* #_SpecimenEntityType ^property[35].code = #child 
* #_SpecimenEntityType ^property[35].valueCode = #ELT 
* #_SpecimenEntityType ^property[36].code = #child 
* #_SpecimenEntityType ^property[36].valueCode = #ENDC 
* #_SpecimenEntityType ^property[37].code = #child 
* #_SpecimenEntityType ^property[37].valueCode = #ENDM 
* #_SpecimenEntityType ^property[38].code = #child 
* #_SpecimenEntityType ^property[38].valueCode = #EOS 
* #_SpecimenEntityType ^property[39].code = #child 
* #_SpecimenEntityType ^property[39].valueCode = #EXG 
* #_SpecimenEntityType ^property[40].code = #child 
* #_SpecimenEntityType ^property[40].valueCode = #EYE 
* #_SpecimenEntityType ^property[41].code = #child 
* #_SpecimenEntityType ^property[41].valueCode = #FIB 
* #_SpecimenEntityType ^property[42].code = #child 
* #_SpecimenEntityType ^property[42].valueCode = #FIST 
* #_SpecimenEntityType ^property[43].code = #child 
* #_SpecimenEntityType ^property[43].valueCode = #FLT 
* #_SpecimenEntityType ^property[44].code = #child 
* #_SpecimenEntityType ^property[44].valueCode = #FLU 
* #_SpecimenEntityType ^property[45].code = #child 
* #_SpecimenEntityType ^property[45].valueCode = #FOOD 
* #_SpecimenEntityType ^property[46].code = #child 
* #_SpecimenEntityType ^property[46].valueCode = #GAS 
* #_SpecimenEntityType ^property[47].code = #child 
* #_SpecimenEntityType ^property[47].valueCode = #GAST 
* #_SpecimenEntityType ^property[48].code = #child 
* #_SpecimenEntityType ^property[48].valueCode = #GEN 
* #_SpecimenEntityType ^property[49].code = #child 
* #_SpecimenEntityType ^property[49].valueCode = #GENC 
* #_SpecimenEntityType ^property[50].code = #child 
* #_SpecimenEntityType ^property[50].valueCode = #GENF 
* #_SpecimenEntityType ^property[51].code = #child 
* #_SpecimenEntityType ^property[51].valueCode = #GENL 
* #_SpecimenEntityType ^property[52].code = #child 
* #_SpecimenEntityType ^property[52].valueCode = #GENV 
* #_SpecimenEntityType ^property[53].code = #child 
* #_SpecimenEntityType ^property[53].valueCode = #HAR 
* #_SpecimenEntityType ^property[54].code = #child 
* #_SpecimenEntityType ^property[54].valueCode = #IHG 
* #_SpecimenEntityType ^property[55].code = #child 
* #_SpecimenEntityType ^property[55].valueCode = #ISLT 
* #_SpecimenEntityType ^property[56].code = #child 
* #_SpecimenEntityType ^property[56].valueCode = #IT 
* #_SpecimenEntityType ^property[57].code = #child 
* #_SpecimenEntityType ^property[57].valueCode = #LAM 
* #_SpecimenEntityType ^property[58].code = #child 
* #_SpecimenEntityType ^property[58].valueCode = #LIQ 
* #_SpecimenEntityType ^property[59].code = #child 
* #_SpecimenEntityType ^property[59].valueCode = #LN 
* #_SpecimenEntityType ^property[60].code = #child 
* #_SpecimenEntityType ^property[60].valueCode = #LNA 
* #_SpecimenEntityType ^property[61].code = #child 
* #_SpecimenEntityType ^property[61].valueCode = #LNV 
* #_SpecimenEntityType ^property[62].code = #child 
* #_SpecimenEntityType ^property[62].valueCode = #LYM 
* #_SpecimenEntityType ^property[63].code = #child 
* #_SpecimenEntityType ^property[63].valueCode = #MAC 
* #_SpecimenEntityType ^property[64].code = #child 
* #_SpecimenEntityType ^property[64].valueCode = #MAR 
* #_SpecimenEntityType ^property[65].code = #child 
* #_SpecimenEntityType ^property[65].valueCode = #MBLD 
* #_SpecimenEntityType ^property[66].code = #child 
* #_SpecimenEntityType ^property[66].valueCode = #MEC 
* #_SpecimenEntityType ^property[67].code = #child 
* #_SpecimenEntityType ^property[67].valueCode = #MILK 
* #_SpecimenEntityType ^property[68].code = #child 
* #_SpecimenEntityType ^property[68].valueCode = #MLK 
* #_SpecimenEntityType ^property[69].code = #child 
* #_SpecimenEntityType ^property[69].valueCode = #NAIL 
* #_SpecimenEntityType ^property[70].code = #child 
* #_SpecimenEntityType ^property[70].valueCode = #NOS 
* #_SpecimenEntityType ^property[71].code = #child 
* #_SpecimenEntityType ^property[71].valueCode = #PAFL 
* #_SpecimenEntityType ^property[72].code = #child 
* #_SpecimenEntityType ^property[72].valueCode = #PAT 
* #_SpecimenEntityType ^property[73].code = #child 
* #_SpecimenEntityType ^property[73].valueCode = #PLAS 
* #_SpecimenEntityType ^property[74].code = #child 
* #_SpecimenEntityType ^property[74].valueCode = #PLB 
* #_SpecimenEntityType ^property[75].code = #child 
* #_SpecimenEntityType ^property[75].valueCode = #PLC 
* #_SpecimenEntityType ^property[76].code = #child 
* #_SpecimenEntityType ^property[76].valueCode = #PLR 
* #_SpecimenEntityType ^property[77].code = #child 
* #_SpecimenEntityType ^property[77].valueCode = #PMN 
* #_SpecimenEntityType ^property[78].code = #child 
* #_SpecimenEntityType ^property[78].valueCode = #PPP 
* #_SpecimenEntityType ^property[79].code = #child 
* #_SpecimenEntityType ^property[79].valueCode = #PRP 
* #_SpecimenEntityType ^property[80].code = #child 
* #_SpecimenEntityType ^property[80].valueCode = #PRT 
* #_SpecimenEntityType ^property[81].code = #child 
* #_SpecimenEntityType ^property[81].valueCode = #PUS 
* #_SpecimenEntityType ^property[82].code = #child 
* #_SpecimenEntityType ^property[82].valueCode = #RBC 
* #_SpecimenEntityType ^property[83].code = #child 
* #_SpecimenEntityType ^property[83].valueCode = #SAL 
* #_SpecimenEntityType ^property[84].code = #child 
* #_SpecimenEntityType ^property[84].valueCode = #SER 
* #_SpecimenEntityType ^property[85].code = #child 
* #_SpecimenEntityType ^property[85].valueCode = #SKM 
* #_SpecimenEntityType ^property[86].code = #child 
* #_SpecimenEntityType ^property[86].valueCode = #SKN 
* #_SpecimenEntityType ^property[87].code = #child 
* #_SpecimenEntityType ^property[87].valueCode = #SMN 
* #_SpecimenEntityType ^property[88].code = #child 
* #_SpecimenEntityType ^property[88].valueCode = #SMPLS 
* #_SpecimenEntityType ^property[89].code = #child 
* #_SpecimenEntityType ^property[89].valueCode = #SNV 
* #_SpecimenEntityType ^property[90].code = #child 
* #_SpecimenEntityType ^property[90].valueCode = #SPRM 
* #_SpecimenEntityType ^property[91].code = #child 
* #_SpecimenEntityType ^property[91].valueCode = #SPT 
* #_SpecimenEntityType ^property[92].code = #child 
* #_SpecimenEntityType ^property[92].valueCode = #SPTC 
* #_SpecimenEntityType ^property[93].code = #child 
* #_SpecimenEntityType ^property[93].valueCode = #SPTT 
* #_SpecimenEntityType ^property[94].code = #child 
* #_SpecimenEntityType ^property[94].valueCode = #STL 
* #_SpecimenEntityType ^property[95].code = #child 
* #_SpecimenEntityType ^property[95].valueCode = #STON 
* #_SpecimenEntityType ^property[96].code = #child 
* #_SpecimenEntityType ^property[96].valueCode = #SWT 
* #_SpecimenEntityType ^property[97].code = #child 
* #_SpecimenEntityType ^property[97].valueCode = #TEAR 
* #_SpecimenEntityType ^property[98].code = #child 
* #_SpecimenEntityType ^property[98].valueCode = #THRB 
* #_SpecimenEntityType ^property[99].code = #child 
* #_SpecimenEntityType ^property[99].valueCode = #THRT 
* #_SpecimenEntityType ^property[100].code = #child 
* #_SpecimenEntityType ^property[100].valueCode = #TISG 
* #_SpecimenEntityType ^property[101].code = #child 
* #_SpecimenEntityType ^property[101].valueCode = #TISPL 
* #_SpecimenEntityType ^property[102].code = #child 
* #_SpecimenEntityType ^property[102].valueCode = #TISS 
* #_SpecimenEntityType ^property[103].code = #child 
* #_SpecimenEntityType ^property[103].valueCode = #TISU 
* #_SpecimenEntityType ^property[104].code = #child 
* #_SpecimenEntityType ^property[104].valueCode = #TLGI 
* #_SpecimenEntityType ^property[105].code = #child 
* #_SpecimenEntityType ^property[105].valueCode = #TLNG 
* #_SpecimenEntityType ^property[106].code = #child 
* #_SpecimenEntityType ^property[106].valueCode = #TSMI 
* #_SpecimenEntityType ^property[107].code = #child 
* #_SpecimenEntityType ^property[107].valueCode = #TUB 
* #_SpecimenEntityType ^property[108].code = #child 
* #_SpecimenEntityType ^property[108].valueCode = #ULC 
* #_SpecimenEntityType ^property[109].code = #child 
* #_SpecimenEntityType ^property[109].valueCode = #UMB 
* #_SpecimenEntityType ^property[110].code = #child 
* #_SpecimenEntityType ^property[110].valueCode = #UMED 
* #_SpecimenEntityType ^property[111].code = #child 
* #_SpecimenEntityType ^property[111].valueCode = #UR 
* #_SpecimenEntityType ^property[112].code = #child 
* #_SpecimenEntityType ^property[112].valueCode = #URC 
* #_SpecimenEntityType ^property[113].code = #child 
* #_SpecimenEntityType ^property[113].valueCode = #URNS 
* #_SpecimenEntityType ^property[114].code = #child 
* #_SpecimenEntityType ^property[114].valueCode = #URT 
* #_SpecimenEntityType ^property[115].code = #child 
* #_SpecimenEntityType ^property[115].valueCode = #URTH 
* #_SpecimenEntityType ^property[116].code = #child 
* #_SpecimenEntityType ^property[116].valueCode = #USUB 
* #_SpecimenEntityType ^property[117].code = #child 
* #_SpecimenEntityType ^property[117].valueCode = #VOM 
* #_SpecimenEntityType ^property[118].code = #child 
* #_SpecimenEntityType ^property[118].valueCode = #WAT 
* #_SpecimenEntityType ^property[119].code = #child 
* #_SpecimenEntityType ^property[119].valueCode = #WBC 
* #_SpecimenEntityType ^property[120].code = #child 
* #_SpecimenEntityType ^property[120].valueCode = #WICK 
* #_SpecimenEntityType ^property[121].code = #child 
* #_SpecimenEntityType ^property[121].valueCode = #WND 
* #_SpecimenEntityType ^property[122].code = #child 
* #_SpecimenEntityType ^property[122].valueCode = #WNDA 
* #_SpecimenEntityType ^property[123].code = #child 
* #_SpecimenEntityType ^property[123].valueCode = #WNDD 
* #_SpecimenEntityType ^property[124].code = #child 
* #_SpecimenEntityType ^property[124].valueCode = #WNDE 
* #ABS "Abcess"
* #ABS ^property[0].code = #parent 
* #ABS ^property[0].valueCode = #_SpecimenEntityType 
* #AMN "Amniotic fluid"
* #AMN ^property[0].code = #parent 
* #AMN ^property[0].valueCode = #_SpecimenEntityType 
* #ASP "Aspirate"
* #ASP ^property[0].code = #parent 
* #ASP ^property[0].valueCode = #_SpecimenEntityType 
* #BBL "Blood bag"
* #BBL ^property[0].code = #parent 
* #BBL ^property[0].valueCode = #_SpecimenEntityType 
* #BDY "Whole body"
* #BDY ^property[0].code = #parent 
* #BDY ^property[0].valueCode = #_SpecimenEntityType 
* #BIFL "Bile fluid"
* #BIFL ^property[0].code = #parent 
* #BIFL ^property[0].valueCode = #_SpecimenEntityType 
* #BLD "Whole blood"
* #BLD ^property[0].code = #parent 
* #BLD ^property[0].valueCode = #_SpecimenEntityType 
* #BLDA "Blood arterial"
* #BLDA ^property[0].code = #parent 
* #BLDA ^property[0].valueCode = #_SpecimenEntityType 
* #BLDC "Blood capillary"
* #BLDC ^property[0].code = #parent 
* #BLDC ^property[0].valueCode = #_SpecimenEntityType 
* #BLDCO "Blood - cord"
* #BLDCO ^property[0].code = #parent 
* #BLDCO ^property[0].valueCode = #_SpecimenEntityType 
* #BLDV "Blood venous"
* #BLDV ^property[0].code = #parent 
* #BLDV ^property[0].valueCode = #_SpecimenEntityType 
* #BON "Bone"
* #BON ^property[0].code = #parent 
* #BON ^property[0].valueCode = #_SpecimenEntityType 
* #BPH "Basophils"
* #BPH ^property[0].code = #parent 
* #BPH ^property[0].valueCode = #_SpecimenEntityType 
* #BPU "Blood product unit"
* #BPU ^property[0].code = #parent 
* #BPU ^property[0].valueCode = #_SpecimenEntityType 
* #BRN "Burn"
* #BRN ^property[0].code = #parent 
* #BRN ^property[0].valueCode = #_SpecimenEntityType 
* #BRO "Bronchial"
* #BRO ^property[0].code = #parent 
* #BRO ^property[0].valueCode = #_SpecimenEntityType 
* #BRTH "Exhaled gas (=breath) Breath (use EXG)"
* #BRTH ^property[0].code = #parent 
* #BRTH ^property[0].valueCode = #_SpecimenEntityType 
* #CALC "Calculus (=Stone) Stone (use CALC)"
* #CALC ^property[0].code = #parent 
* #CALC ^property[0].valueCode = #_SpecimenEntityType 
* #CDM "Cardiac muscle"
* #CDM ^property[0].code = #parent 
* #CDM ^property[0].valueCode = #_SpecimenEntityType 
* #CNJT "Conjunctiva"
* #CNJT ^property[0].code = #parent 
* #CNJT ^property[0].valueCode = #_SpecimenEntityType 
* #CNL "Cannula"
* #CNL ^property[0].code = #parent 
* #CNL ^property[0].valueCode = #_SpecimenEntityType 
* #COL "Colostrum"
* #COL ^property[0].code = #parent 
* #COL ^property[0].valueCode = #_SpecimenEntityType 
* #CRN "Cornea"
* #CRN ^property[0].code = #parent 
* #CRN ^property[0].valueCode = #_SpecimenEntityType 
* #CSF "Cerebral spinal fluid"
* #CSF ^property[0].code = #parent 
* #CSF ^property[0].valueCode = #_SpecimenEntityType 
* #CTP "Catheter tip"
* #CTP ^property[0].code = #parent 
* #CTP ^property[0].valueCode = #_SpecimenEntityType 
* #CUR "Curettage"
* #CUR ^property[0].code = #parent 
* #CUR ^property[0].valueCode = #_SpecimenEntityType 
* #CVM "Cervical mucus"
* #CVM ^property[0].code = #parent 
* #CVM ^property[0].valueCode = #_SpecimenEntityType 
* #CVX "Cervix"
* #CVX ^property[0].code = #parent 
* #CVX ^property[0].valueCode = #_SpecimenEntityType 
* #CYST "Cyst"
* #CYST ^property[0].code = #parent 
* #CYST ^property[0].valueCode = #_SpecimenEntityType 
* #DIAF "Dialysis fluid"
* #DIAF ^property[0].code = #parent 
* #DIAF ^property[0].valueCode = #_SpecimenEntityType 
* #DOSE "Dose med or substance"
* #DOSE ^property[0].code = #parent 
* #DOSE ^property[0].valueCode = #_SpecimenEntityType 
* #DRN "Drain"
* #DRN ^property[0].code = #parent 
* #DRN ^property[0].valueCode = #_SpecimenEntityType 
* #DUFL "Duodenal fluid"
* #DUFL ^property[0].code = #parent 
* #DUFL ^property[0].valueCode = #_SpecimenEntityType 
* #EAR "Ear"
* #EAR ^property[0].code = #parent 
* #EAR ^property[0].valueCode = #_SpecimenEntityType 
* #EARW "Ear wax (cerumen)"
* #EARW ^property[0].code = #parent 
* #EARW ^property[0].valueCode = #_SpecimenEntityType 
* #ELT "Electrode"
* #ELT ^property[0].code = #parent 
* #ELT ^property[0].valueCode = #_SpecimenEntityType 
* #ENDC "Endocardium"
* #ENDC ^property[0].code = #parent 
* #ENDC ^property[0].valueCode = #_SpecimenEntityType 
* #ENDM "Endometrium"
* #ENDM ^property[0].code = #parent 
* #ENDM ^property[0].valueCode = #_SpecimenEntityType 
* #EOS "Eosinophils"
* #EOS ^property[0].code = #parent 
* #EOS ^property[0].valueCode = #_SpecimenEntityType 
* #EXG "Exhaled gas (=breath) Breath (use EXG)"
* #EXG ^property[0].code = #parent 
* #EXG ^property[0].valueCode = #_SpecimenEntityType 
* #EYE "Eye"
* #EYE ^property[0].code = #parent 
* #EYE ^property[0].valueCode = #_SpecimenEntityType 
* #FIB "Fibroblasts"
* #FIB ^property[0].code = #parent 
* #FIB ^property[0].valueCode = #_SpecimenEntityType 
* #FIST "Fistula"
* #FIST ^property[0].code = #parent 
* #FIST ^property[0].valueCode = #_SpecimenEntityType 
* #FLT "Filter"
* #FLT ^property[0].code = #parent 
* #FLT ^property[0].valueCode = #_SpecimenEntityType 
* #FLU "Body fluid, unsp"
* #FLU ^property[0].code = #parent 
* #FLU ^property[0].valueCode = #_SpecimenEntityType 
* #FOOD "Food sample"
* #FOOD ^property[0].code = #parent 
* #FOOD ^property[0].valueCode = #_SpecimenEntityType 
* #GAS "Gas"
* #GAS ^property[0].code = #parent 
* #GAS ^property[0].valueCode = #_SpecimenEntityType 
* #GAST "Gastric fluid/contents"
* #GAST ^property[0].code = #parent 
* #GAST ^property[0].valueCode = #_SpecimenEntityType 
* #GEN "Genital"
* #GEN ^property[0].code = #parent 
* #GEN ^property[0].valueCode = #_SpecimenEntityType 
* #GENC "Genital cervix"
* #GENC ^property[0].code = #parent 
* #GENC ^property[0].valueCode = #_SpecimenEntityType 
* #GENF "Genital fluid"
* #GENF ^property[0].code = #parent 
* #GENF ^property[0].valueCode = #_SpecimenEntityType 
* #GENL "Genital lochia"
* #GENL ^property[0].code = #parent 
* #GENL ^property[0].valueCode = #_SpecimenEntityType 
* #GENV "Genital vaginal"
* #GENV ^property[0].code = #parent 
* #GENV ^property[0].valueCode = #_SpecimenEntityType 
* #HAR "Hair"
* #HAR ^property[0].code = #parent 
* #HAR ^property[0].valueCode = #_SpecimenEntityType 
* #IHG "Inhaled Gas"
* #IHG ^property[0].code = #parent 
* #IHG ^property[0].valueCode = #_SpecimenEntityType 
* #ISLT "Isolate"
* #ISLT ^property[0].code = #parent 
* #ISLT ^property[0].valueCode = #_SpecimenEntityType 
* #IT "Intubation tube"
* #IT ^property[0].code = #parent 
* #IT ^property[0].valueCode = #_SpecimenEntityType 
* #LAM "Lamella"
* #LAM ^property[0].code = #parent 
* #LAM ^property[0].valueCode = #_SpecimenEntityType 
* #LIQ "Liquid NOS"
* #LIQ ^property[0].code = #parent 
* #LIQ ^property[0].valueCode = #_SpecimenEntityType 
* #LN "Line"
* #LN ^property[0].code = #parent 
* #LN ^property[0].valueCode = #_SpecimenEntityType 
* #LNA "Line arterial"
* #LNA ^property[0].code = #parent 
* #LNA ^property[0].valueCode = #_SpecimenEntityType 
* #LNV "Line venous"
* #LNV ^property[0].code = #parent 
* #LNV ^property[0].valueCode = #_SpecimenEntityType 
* #LYM "Lymphocytes"
* #LYM ^property[0].code = #parent 
* #LYM ^property[0].valueCode = #_SpecimenEntityType 
* #MAC "Macrophages"
* #MAC ^property[0].code = #parent 
* #MAC ^property[0].valueCode = #_SpecimenEntityType 
* #MAR "Marrow (bone)"
* #MAR ^property[0].code = #parent 
* #MAR ^property[0].valueCode = #_SpecimenEntityType 
* #MBLD "Menstrual blood"
* #MBLD ^property[0].code = #parent 
* #MBLD ^property[0].valueCode = #_SpecimenEntityType 
* #MEC "Meconium"
* #MEC ^property[0].code = #parent 
* #MEC ^property[0].valueCode = #_SpecimenEntityType 
* #MILK "Breast milk"
* #MILK ^property[0].code = #parent 
* #MILK ^property[0].valueCode = #_SpecimenEntityType 
* #MLK "Milk"
* #MLK ^property[0].code = #parent 
* #MLK ^property[0].valueCode = #_SpecimenEntityType 
* #NAIL "Nail"
* #NAIL ^property[0].code = #parent 
* #NAIL ^property[0].valueCode = #_SpecimenEntityType 
* #NOS "Nose (nasal passage)"
* #NOS ^property[0].code = #parent 
* #NOS ^property[0].valueCode = #_SpecimenEntityType 
* #PAFL "Pancreatic fluid"
* #PAFL ^property[0].code = #parent 
* #PAFL ^property[0].valueCode = #_SpecimenEntityType 
* #PAT "Patient"
* #PAT ^property[0].code = #parent 
* #PAT ^property[0].valueCode = #_SpecimenEntityType 
* #PLAS "Plasma"
* #PLAS ^property[0].code = #parent 
* #PLAS ^property[0].valueCode = #_SpecimenEntityType 
* #PLB "Plasma bag"
* #PLB ^property[0].code = #parent 
* #PLB ^property[0].valueCode = #_SpecimenEntityType 
* #PLC "Placenta"
* #PLC ^property[0].code = #parent 
* #PLC ^property[0].valueCode = #_SpecimenEntityType 
* #PLR "Pleural fluid (thoracentesis fld)"
* #PLR ^property[0].code = #parent 
* #PLR ^property[0].valueCode = #_SpecimenEntityType 
* #PMN "Polymorphonuclear neutrophils"
* #PMN ^property[0].code = #parent 
* #PMN ^property[0].valueCode = #_SpecimenEntityType 
* #PPP "Platelet poor plasma"
* #PPP ^property[0].code = #parent 
* #PPP ^property[0].valueCode = #_SpecimenEntityType 
* #PRP "Platelet rich plasma"
* #PRP ^property[0].code = #parent 
* #PRP ^property[0].valueCode = #_SpecimenEntityType 
* #PRT "Peritoneal fluid /ascites"
* #PRT ^property[0].code = #parent 
* #PRT ^property[0].valueCode = #_SpecimenEntityType 
* #PUS "Pus"
* #PUS ^property[0].code = #parent 
* #PUS ^property[0].valueCode = #_SpecimenEntityType 
* #RBC "Erythrocytes"
* #RBC ^property[0].code = #parent 
* #RBC ^property[0].valueCode = #_SpecimenEntityType 
* #SAL "Saliva"
* #SAL ^property[0].code = #parent 
* #SAL ^property[0].valueCode = #_SpecimenEntityType 
* #SER "Serum"
* #SER ^property[0].code = #parent 
* #SER ^property[0].valueCode = #_SpecimenEntityType 
* #SKM "Skeletal muscle"
* #SKM ^property[0].code = #parent 
* #SKM ^property[0].valueCode = #_SpecimenEntityType 
* #SKN "Skin"
* #SKN ^property[0].code = #parent 
* #SKN ^property[0].valueCode = #_SpecimenEntityType 
* #SMN "Seminal fluid"
* #SMN ^property[0].code = #parent 
* #SMN ^property[0].valueCode = #_SpecimenEntityType 
* #SMPLS "Seminal plasma"
* #SMPLS ^property[0].code = #parent 
* #SMPLS ^property[0].valueCode = #_SpecimenEntityType 
* #SNV "Synovial fluid (Joint fluid)"
* #SNV ^property[0].code = #parent 
* #SNV ^property[0].valueCode = #_SpecimenEntityType 
* #SPRM "Spermatozoa"
* #SPRM ^property[0].code = #parent 
* #SPRM ^property[0].valueCode = #_SpecimenEntityType 
* #SPT "Sputum"
* #SPT ^property[0].code = #parent 
* #SPT ^property[0].valueCode = #_SpecimenEntityType 
* #SPTC "Sputum - coughed"
* #SPTC ^property[0].code = #parent 
* #SPTC ^property[0].valueCode = #_SpecimenEntityType 
* #SPTT "Sputum - tracheal aspirate"
* #SPTT ^property[0].code = #parent 
* #SPTT ^property[0].valueCode = #_SpecimenEntityType 
* #STL "Stool = Fecal"
* #STL ^property[0].code = #parent 
* #STL ^property[0].valueCode = #_SpecimenEntityType 
* #STON "Calculus (=Stone) Stone (use CALC)"
* #STON ^property[0].code = #parent 
* #STON ^property[0].valueCode = #_SpecimenEntityType 
* #SWT "Sweat"
* #SWT ^property[0].code = #parent 
* #SWT ^property[0].valueCode = #_SpecimenEntityType 
* #TEAR "Tears"
* #TEAR ^property[0].code = #parent 
* #TEAR ^property[0].valueCode = #_SpecimenEntityType 
* #THRB "Thrombocyte (platelet)"
* #THRB ^property[0].code = #parent 
* #THRB ^property[0].valueCode = #_SpecimenEntityType 
* #THRT "Throat"
* #THRT ^property[0].code = #parent 
* #THRT ^property[0].valueCode = #_SpecimenEntityType 
* #TISG "Tissue gall bladder"
* #TISG ^property[0].code = #parent 
* #TISG ^property[0].valueCode = #_SpecimenEntityType 
* #TISPL "Tissue placenta"
* #TISPL ^property[0].code = #parent 
* #TISPL ^property[0].valueCode = #_SpecimenEntityType 
* #TISS "Tissue, unspecified"
* #TISS ^property[0].code = #parent 
* #TISS ^property[0].valueCode = #_SpecimenEntityType 
* #TISU "Tissue ulcer"
* #TISU ^property[0].code = #parent 
* #TISU ^property[0].valueCode = #_SpecimenEntityType 
* #TLGI "Tissue large intestine"
* #TLGI ^property[0].code = #parent 
* #TLGI ^property[0].valueCode = #_SpecimenEntityType 
* #TLNG "Tissue lung"
* #TLNG ^property[0].code = #parent 
* #TLNG ^property[0].valueCode = #_SpecimenEntityType 
* #TSMI "Tissue small intestine Tissue ulcer"
* #TSMI ^property[0].code = #parent 
* #TSMI ^property[0].valueCode = #_SpecimenEntityType 
* #TUB "Tube, unspecified"
* #TUB ^property[0].code = #parent 
* #TUB ^property[0].valueCode = #_SpecimenEntityType 
* #ULC "Ulcer"
* #ULC ^property[0].code = #parent 
* #ULC ^property[0].valueCode = #_SpecimenEntityType 
* #UMB "Umbilical blood"
* #UMB ^property[0].code = #parent 
* #UMB ^property[0].valueCode = #_SpecimenEntityType 
* #UMED "Unknown medicine"
* #UMED ^property[0].code = #parent 
* #UMED ^property[0].valueCode = #_SpecimenEntityType 
* #UR "Urine"
* #UR ^property[0].code = #parent 
* #UR ^property[0].valueCode = #_SpecimenEntityType 
* #URC "Urine clean catch"
* #URC ^property[0].code = #parent 
* #URC ^property[0].valueCode = #_SpecimenEntityType 
* #URNS "Urine sediment"
* #URNS ^property[0].code = #parent 
* #URNS ^property[0].valueCode = #_SpecimenEntityType 
* #URT "Urine catheter"
* #URT ^property[0].code = #parent 
* #URT ^property[0].valueCode = #_SpecimenEntityType 
* #URTH "Urethra"
* #URTH ^property[0].code = #parent 
* #URTH ^property[0].valueCode = #_SpecimenEntityType 
* #USUB "Unknown substance"
* #USUB ^property[0].code = #parent 
* #USUB ^property[0].valueCode = #_SpecimenEntityType 
* #VOM "Vomitus"
* #VOM ^property[0].code = #parent 
* #VOM ^property[0].valueCode = #_SpecimenEntityType 
* #WAT "Water"
* #WAT ^property[0].code = #parent 
* #WAT ^property[0].valueCode = #_SpecimenEntityType 
* #WBC "Leukocytes"
* #WBC ^property[0].code = #parent 
* #WBC ^property[0].valueCode = #_SpecimenEntityType 
* #WICK "Wick"
* #WICK ^property[0].code = #parent 
* #WICK ^property[0].valueCode = #_SpecimenEntityType 
* #WND "Wound"
* #WND ^property[0].code = #parent 
* #WND ^property[0].valueCode = #_SpecimenEntityType 
* #WNDA "Wound abscess"
* #WNDA ^property[0].code = #parent 
* #WNDA ^property[0].valueCode = #_SpecimenEntityType 
* #WNDD "Wound drainage"
* #WNDD ^property[0].code = #parent 
* #WNDD ^property[0].valueCode = #_SpecimenEntityType 
* #WNDE "Wound exudate"
* #WNDE ^property[0].code = #parent 
* #WNDE ^property[0].valueCode = #_SpecimenEntityType 
