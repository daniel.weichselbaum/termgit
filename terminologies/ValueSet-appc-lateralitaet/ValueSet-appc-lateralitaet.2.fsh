Instance: appc-lateralitaet 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-appc-lateralitaet" 
* name = "appc-lateralitaet" 
* title = "APPC_Lateralitaet" 
* status = #active 
* version = "1.0.8" 
* description = "**Description:** Value Set for all APPC-Codes for 2nd Axis 'Laterality'

**Beschreibung:** Value Set aller Codes der 2. APPC-Achse 'Lateralität'" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.63" 
* date = "2014-09-01" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.bura.at/appc/" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-appc-lateralitaet"
* compose.include[0].concept[0].code = "0"
* compose.include[0].concept[0].display = "Lateralitaet unbestimmt"
* compose.include[0].concept[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[0].designation[0].value = "APPC: 1.2.40.0.34.5.38" 
* compose.include[0].concept[1].code = "1"
* compose.include[0].concept[1].display = "rechts"
* compose.include[0].concept[1].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[1].designation[0].value = "APPC: 1.2.40.0.34.5.38" 
* compose.include[0].concept[2].code = "2"
* compose.include[0].concept[2].display = "links"
* compose.include[0].concept[2].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[2].designation[0].value = "APPC: 1.2.40.0.34.5.38" 
* compose.include[0].concept[3].code = "3"
* compose.include[0].concept[3].display = "beidseits"
* compose.include[0].concept[3].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[3].designation[0].value = "APPC: 1.2.40.0.34.5.38" 
* compose.include[0].concept[4].code = "4"
* compose.include[0].concept[4].display = "unpaariges Organ"
* compose.include[0].concept[4].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[4].designation[0].value = "APPC: 1.2.40.0.34.5.38" 
* compose.include[0].concept[5].code = "5"
* compose.include[0].concept[5].display = "Atypische Situation ( - Transplantat etc.)"
* compose.include[0].concept[5].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[5].designation[0].value = "APPC: 1.2.40.0.34.5.38" 
