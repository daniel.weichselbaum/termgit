Instance: appc-lateralitaet 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-appc-lateralitaet" 
* name = "appc-lateralitaet" 
* title = "APPC_Lateralitaet" 
* status = #active 
* version = "1.0.8" 
* description = "**Description:** Value Set for all APPC-Codes for 2nd Axis 'Laterality'

**Beschreibung:** Value Set aller Codes der 2. APPC-Achse 'Lateralität'" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.63" 
* date = "2014-09-01" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.bura.at/appc/" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-appc-lateralitaet"
* compose.include[0].concept[0].code = #0
* compose.include[0].concept[0].display = "Lateralitaet unbestimmt"
* compose.include[0].concept[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[0].designation[0].value = "APPC: 1.2.40.0.34.5.38" 
* compose.include[0].concept[1].code = #1
* compose.include[0].concept[1].display = "rechts"
* compose.include[0].concept[1].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[1].designation[0].value = "APPC: 1.2.40.0.34.5.38" 
* compose.include[0].concept[2].code = #2
* compose.include[0].concept[2].display = "links"
* compose.include[0].concept[2].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[2].designation[0].value = "APPC: 1.2.40.0.34.5.38" 
* compose.include[0].concept[3].code = #3
* compose.include[0].concept[3].display = "beidseits"
* compose.include[0].concept[3].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[3].designation[0].value = "APPC: 1.2.40.0.34.5.38" 
* compose.include[0].concept[4].code = #4
* compose.include[0].concept[4].display = "unpaariges Organ"
* compose.include[0].concept[4].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[4].designation[0].value = "APPC: 1.2.40.0.34.5.38" 
* compose.include[0].concept[5].code = #5
* compose.include[0].concept[5].display = "Atypische Situation ( - Transplantat etc.)"
* compose.include[0].concept[5].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[5].designation[0].value = "APPC: 1.2.40.0.34.5.38" 

* expansion.timestamp = "2022-09-13T14:15:26.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem-appc-lateralitaet"
* expansion.contains[0].code = #0
* expansion.contains[0].display = "Lateralitaet unbestimmt"
* expansion.contains[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* expansion.contains[0].designation[0].value = "APPC: 1.2.40.0.34.5.38" 
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem-appc-lateralitaet"
* expansion.contains[1].code = #1
* expansion.contains[1].display = "rechts"
* expansion.contains[1].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* expansion.contains[1].designation[0].value = "APPC: 1.2.40.0.34.5.38" 
* expansion.contains[2].system = "https://termgit.elga.gv.at/CodeSystem-appc-lateralitaet"
* expansion.contains[2].code = #2
* expansion.contains[2].display = "links"
* expansion.contains[2].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* expansion.contains[2].designation[0].value = "APPC: 1.2.40.0.34.5.38" 
* expansion.contains[3].system = "https://termgit.elga.gv.at/CodeSystem-appc-lateralitaet"
* expansion.contains[3].code = #3
* expansion.contains[3].display = "beidseits"
* expansion.contains[3].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* expansion.contains[3].designation[0].value = "APPC: 1.2.40.0.34.5.38" 
* expansion.contains[4].system = "https://termgit.elga.gv.at/CodeSystem-appc-lateralitaet"
* expansion.contains[4].code = #4
* expansion.contains[4].display = "unpaariges Organ"
* expansion.contains[4].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* expansion.contains[4].designation[0].value = "APPC: 1.2.40.0.34.5.38" 
* expansion.contains[5].system = "https://termgit.elga.gv.at/CodeSystem-appc-lateralitaet"
* expansion.contains[5].code = #5
* expansion.contains[5].display = "Atypische Situation ( - Transplantat etc.)"
* expansion.contains[5].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* expansion.contains[5].designation[0].value = "APPC: 1.2.40.0.34.5.38" 
