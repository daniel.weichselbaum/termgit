Instance: ems-hcv-core-ag-assay 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-ems-hcv-core-ag-assay" 
* name = "ems-hcv-core-ag-assay" 
* title = "EMS_HCV_core_Ag_Assay" 
* status = #active 
* version = "201902" 
* description = "**Beschreibung:** Dieses Valueset wird verwendet um das Ergebnis HCV core Ag Assays zu codieren. Verwendung im Zuge vom EMS" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.92" 
* date = "2019-01-01" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-ems-posneg"
* compose.include[0].concept[0].code = "NEG"
* compose.include[0].concept[0].display = "negativ"
* compose.include[0].concept[1].code = "NOTEST"
* compose.include[0].concept[1].display = "nicht durchgeführt"
* compose.include[0].concept[2].code = "POS"
* compose.include[0].concept[2].display = "positiv"
