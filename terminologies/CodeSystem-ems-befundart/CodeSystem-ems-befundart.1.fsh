Instance: ems-befundart 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-befundart" 
* name = "ems-befundart" 
* title = "EMS_Befundart" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Liste der möglichen Befundarten" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.64" 
* date = "2017-01-26" 
* count = 4 
* concept[0].code = #BBIRZ
* concept[0].display = "Bestätigungsbefund internationale Referenzzentrale"
* concept[1].code = #BBNRZ
* concept[1].display = "Bestätigungsbefund nationale Referenzzentrale"
* concept[2].code = #EB
* concept[2].display = "Erstbefund"
* concept[3].code = #FB
* concept[3].display = "Folgebefund"
