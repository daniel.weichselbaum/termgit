Instance: hl7-consent-status 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-consent-status" 
* name = "hl7-consent-status" 
* title = "HL7 Consent Status" 
* status = #active 
* content = #complete 
* description = "**Description:** Consent Status (Table 0498)

**Beschreibung:** Status der Einverständniserklärung (Tabelle 0498)

**Versions-Beschreibung:** Status der Einverständniserklärung (Tabelle 0498)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.12.498" 
* date = "2015-06-01" 
* count = 6 
* property[0].code = #hints 
* property[0].type = #string 
* concept[0].code = #A
* concept[0].display = "Active - Consent has been granted"
* concept[0].designation[0].language = #de-AT 
* concept[0].designation[0].value = "Aktiv - Zustimmung erteilt" 
* concept[0].property[0].code = #hints 
* concept[0].property[0].valueString = "Gültiger Wert für ELGA-SOO" 
* concept[1].code = #B
* concept[1].display = "Bypassed (Consent not sought)"
* concept[1].designation[0].language = #de-AT 
* concept[1].designation[0].value = "Umgangen (Zustimmung nicht eingeholt)" 
* concept[2].code = #L
* concept[2].display = "Limited - Consent has been granted with limitations"
* concept[2].property[0].code = #hints 
* concept[2].property[0].valueString = "Eingeschränkt - Zustimmung mit Einschränkungen erteilt" 
* concept[3].code = #P
* concept[3].display = "Pending - Consent has not yet been sought"
* concept[3].designation[0].language = #de-AT 
* concept[3].designation[0].value = "Offen - Zustimmung wirde nicht angefragt" 
* concept[3].property[0].code = #hints 
* concept[3].property[0].valueString = "Gültiger Wert für ELGA-SOO" 
* concept[4].code = #R
* concept[4].display = "Refused - Consent has been refused"
* concept[4].designation[0].language = #de-AT 
* concept[4].designation[0].value = "Verweigert - Zustimmung wurde verweigert" 
* concept[5].code = #X
* concept[5].display = "Rescinded - Consent was initially granted, but was subsequently revoked or ended."
* concept[5].designation[0].language = #de-AT 
* concept[5].designation[0].value = "Aufgehoben - Zustimmung wurde ursprünglich gewährt, aber wurde später aufgehoben oder beendet." 
