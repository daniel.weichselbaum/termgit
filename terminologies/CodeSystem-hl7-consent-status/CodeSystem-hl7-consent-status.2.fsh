Instance: hl7-consent-status 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-consent-status" 
* name = "hl7-consent-status" 
* title = "HL7 Consent Status" 
* status = #active 
* content = #complete 
* description = "**Description:** Consent Status (Table 0498)

**Beschreibung:** Status der Einverständniserklärung (Tabelle 0498)

**Versions-Beschreibung:** Status der Einverständniserklärung (Tabelle 0498)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.12.498" 
* date = "2015-06-01" 
* count = 6 
* property[0].code = #hints 
* property[0].type = #string 
* #A "Active - Consent has been granted"
* #A ^designation[0].language = #de-AT 
* #A ^designation[0].value = "Aktiv - Zustimmung erteilt" 
* #A ^property[0].code = #hints 
* #A ^property[0].valueString = "Gültiger Wert für ELGA-SOO" 
* #B "Bypassed (Consent not sought)"
* #B ^designation[0].language = #de-AT 
* #B ^designation[0].value = "Umgangen (Zustimmung nicht eingeholt)" 
* #L "Limited - Consent has been granted with limitations"
* #L ^property[0].code = #hints 
* #L ^property[0].valueString = "Eingeschränkt - Zustimmung mit Einschränkungen erteilt" 
* #P "Pending - Consent has not yet been sought"
* #P ^designation[0].language = #de-AT 
* #P ^designation[0].value = "Offen - Zustimmung wirde nicht angefragt" 
* #P ^property[0].code = #hints 
* #P ^property[0].valueString = "Gültiger Wert für ELGA-SOO" 
* #R "Refused - Consent has been refused"
* #R ^designation[0].language = #de-AT 
* #R ^designation[0].value = "Verweigert - Zustimmung wurde verweigert" 
* #X "Rescinded - Consent was initially granted, but was subsequently revoked or ended."
* #X ^designation[0].language = #de-AT 
* #X ^designation[0].value = "Aufgehoben - Zustimmung wurde ursprünglich gewährt, aber wurde später aufgehoben oder beendet." 
