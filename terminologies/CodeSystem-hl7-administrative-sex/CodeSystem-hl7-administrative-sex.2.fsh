Instance: hl7-administrative-sex 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-administrative-sex" 
* name = "hl7-administrative-sex" 
* title = "HL7 Administrative Sex" 
* status = #active 
* content = #complete 
* version = "201912" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.12.1" 
* date = "2019-12-05" 
* count = 6 
* property[0].code = #hints 
* property[0].type = #string 
* #A "Ambiguous"
* #A ^designation[0].language = #de-AT 
* #A ^designation[0].value = "Mehrdeutig" 
* #A ^property[0].code = #hints 
* #A ^property[0].valueString = "nur für Tiermedizin" 
* #F "Female"
* #F ^designation[0].language = #de-AT 
* #F ^designation[0].value = "Weiblich" 
* #M "Male"
* #M ^designation[0].language = #de-AT 
* #M ^designation[0].value = "Männlich" 
* #N "Not applicable"
* #N ^definition = nur für HL7 V2.x
* #N ^designation[0].language = #de-AT 
* #N ^designation[0].value = "Offen" 
* #O "Other"
* #O ^definition = nur für HL7 V2.x
* #O ^designation[0].language = #de-AT 
* #O ^designation[0].value = "Divers" 
* #U "Unknown"
* #U ^designation[0].language = #de-AT 
* #U ^designation[0].value = "Unbekannt" 
