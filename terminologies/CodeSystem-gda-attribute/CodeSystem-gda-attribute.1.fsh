Instance: gda-attribute 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-gda-attribute" 
* name = "gda-attribute" 
* title = "GDA_Attribute" 
* status = #active 
* content = #complete 
* version = "2.0" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.4" 
* date = "2015-11-25" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.elga.gv.at" 
* count = 3 
* concept[0].code = #SSCHG
* concept[0].display = "Ermächtigung gemäß § 35 Strahlenschutzgesetz"
* concept[0].designation[0].language = #de-AT 
* concept[0].designation[0].value = "Ermächtigung gemäß § 35 Strahlenschutzgesetz" 
* concept[1].code = #SUBUB
* concept[1].display = "Berechtigung zur umfassenden Substitutionsbehandlung"
* concept[1].designation[0].language = #de-AT 
* concept[1].designation[0].value = "Berechtigung zur umfassenden Substitutionsbehandlung" 
* concept[2].code = #WBSUB
* concept[2].display = "Berechtigung zur Weiterbehandlung Substitution"
* concept[2].designation[0].language = #de-AT 
* concept[2].designation[0].value = "Berechtigung zur Weiterbehandlung Substitution" 
