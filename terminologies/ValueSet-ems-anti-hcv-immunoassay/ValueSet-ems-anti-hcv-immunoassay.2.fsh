Instance: ems-anti-hcv-immunoassay 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-ems-anti-hcv-immunoassay" 
* name = "ems-anti-hcv-immunoassay" 
* title = "EMS_Anti-HCV-Immunoassay" 
* status = #active 
* version = "201902" 
* description = "**Beschreibung:** Dieses Valueset wird verwendet um das Ergebnis des Anti-HCV Immunoassays zu bewerten. Verwendung im Zuge vom EMS" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.90" 
* date = "2019-01-01" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-ems-posneg"
* compose.include[0].concept[0].code = "GW"
* compose.include[0].concept[0].display = "grenzwertig"
* compose.include[0].concept[1].code = "NEG"
* compose.include[0].concept[1].display = "negativ"
* compose.include[0].concept[2].code = "NOTEST"
* compose.include[0].concept[2].display = "nicht durchgeführt"
* compose.include[0].concept[3].code = "POS"
* compose.include[0].concept[3].display = "positiv"
