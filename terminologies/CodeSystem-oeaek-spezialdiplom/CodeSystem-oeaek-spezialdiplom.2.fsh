Instance: oeaek-spezialdiplom 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-oeaek-spezialdiplom" 
* name = "oeaek-spezialdiplom" 
* title = "OEAEK_Spezialdiplom" 
* status = #active 
* content = #complete 
* version = "1.0" 
* description = "**Description:** Code List of all special diplomas valid in Austria issued by the Medical Chamber of Austria

**Beschreibung:** Code Liste aller in Österreich gültigen Spezialdiplome" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.3.1.10.40" 
* date = "2018-07-01" 
* count = 30 
* #39 "Sexualmedizin"
* #39 ^designation[0].language = #de-AT 
* #39 ^designation[0].value = "Sexualmedizin" 
* #44 "Sportmedizin"
* #44 ^designation[0].language = #de-AT 
* #44 ^designation[0].value = "Sportmedizin" 
* #45 "Umweltmedizin"
* #45 ^designation[0].language = #de-AT 
* #45 ^designation[0].value = "Umweltmedizin" 
* #46 "Kur-, Präventivmedizin und Wellness"
* #46 ^designation[0].language = #de-AT 
* #46 ^designation[0].value = "Kur-, Präventivmedizin und Wellness" 
* #49 "Psychosoziale Medizin"
* #49 ^designation[0].language = #de-AT 
* #49 ^designation[0].value = "Psychosoziale Medizin" 
* #50 "Psychosomatische Medizin"
* #50 ^designation[0].language = #de-AT 
* #50 ^designation[0].value = "Psychosomatische Medizin" 
* #51 "Psychotherapeutische Medizin"
* #51 ^designation[0].language = #de-AT 
* #51 ^designation[0].value = "Psychotherapeutische Medizin" 
* #52 "Geriatrie"
* #52 ^designation[0].language = #de-AT 
* #52 ^designation[0].value = "Geriatrie" 
* #53 "Akupunktur"
* #53 ^designation[0].language = #de-AT 
* #53 ^designation[0].value = "Akupunktur" 
* #56 "Arbeitsmedizin"
* #56 ^designation[0].language = #de-AT 
* #56 ^designation[0].value = "Arbeitsmedizin" 
* #61 "Manuelle Medizin"
* #61 ^designation[0].language = #de-AT 
* #61 ^designation[0].value = "Manuelle Medizin" 
* #62 "Homöopathie"
* #62 ^designation[0].language = #de-AT 
* #62 ^designation[0].value = "Homöopathie" 
* #63 "Neuraltherapie"
* #63 ^designation[0].language = #de-AT 
* #63 ^designation[0].value = "Neuraltherapie" 
* #64 "Ernährungsmedizin"
* #64 ^designation[0].language = #de-AT 
* #64 ^designation[0].value = "Ernährungsmedizin" 
* #65 "Krankenhaushygiene"
* #65 ^designation[0].language = #de-AT 
* #65 ^designation[0].value = "Krankenhaushygiene" 
* #66 "Klinischer Prüfarzt"
* #66 ^designation[0].language = #de-AT 
* #66 ^designation[0].value = "Klinischer Prüfarzt" 
* #67 "Diagnostik und Therapie nach Dr. F.X. Mayr"
* #67 ^designation[0].language = #de-AT 
* #67 ^designation[0].value = "Diagnostik und Therapie nach Dr. F.X. Mayr" 
* #68 "Schularzt"
* #68 ^designation[0].language = #de-AT 
* #68 ^designation[0].value = "Schularzt" 
* #69 "Anthroposophische Medizin"
* #69 ^designation[0].language = #de-AT 
* #69 ^designation[0].value = "Anthroposophische Medizin" 
* #72 "Genetik"
* #72 ^designation[0].language = #de-AT 
* #72 ^designation[0].value = "Genetik" 
* #73 "Applied Kinesiology"
* #73 ^designation[0].language = #de-AT 
* #73 ^designation[0].value = "Applied Kinesiology" 
* #74 "Chinesische Diagnostik und Arzneitherapie"
* #74 ^designation[0].language = #de-AT 
* #74 ^designation[0].value = "Chinesische Diagnostik und Arzneitherapie" 
* #75 "Kneippmedizin"
* #75 ^designation[0].language = #de-AT 
* #75 ^designation[0].value = "Kneippmedizin" 
* #84 "Forensisch-psychiatrische Gutachten"
* #84 ^designation[0].language = #de-AT 
* #84 ^designation[0].value = "Forensisch-psychiatrische Gutachten" 
* #85 "Spezielle Schmerztherapie"
* #85 ^designation[0].language = #de-AT 
* #85 ^designation[0].value = "Spezielle Schmerztherapie" 
* #86 "Begleitende Krebsbehandlungen"
* #86 ^designation[0].language = #de-AT 
* #86 ^designation[0].value = "Begleitende Krebsbehandlungen" 
* #87 "Palliativmedizin"
* #87 ^designation[0].language = #de-AT 
* #87 ^designation[0].value = "Palliativmedizin" 
* #91 "Orthomolekulare Medizin"
* #91 ^designation[0].language = #de-AT 
* #91 ^designation[0].value = "Orthomolekulare Medizin" 
* #92 "Phytotherapie"
* #92 ^designation[0].language = #de-AT 
* #92 ^designation[0].value = "Phytotherapie" 
* #93 "Substitutionsbehandlung"
* #93 ^designation[0].language = #de-AT 
* #93 ^designation[0].value = "Substitutionsbehandlung" 
