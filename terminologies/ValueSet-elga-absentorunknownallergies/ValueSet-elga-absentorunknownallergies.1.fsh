Instance: elga-absentorunknownallergies 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-absentorunknownallergies" 
* name = "elga-absentorunknownallergies" 
* title = "ELGA_AbsentOrUnknownAllergies" 
* status = #active 
* version = "202002" 
* description = "**Description:** Absent Allergy Or Intolerance 

**Beschreibung:** Keine Allergie oder Intoleranz" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.178" 
* date = "2020-02-04" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-ips-absentorunknowndata"
* compose.include[0].concept[0].code = #no-allergy-info
* compose.include[0].concept[0].display = "keine Information über Überempfindlichkeiten verfügbar"
* compose.include[0].concept[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[0].designation[0].value = "IPS CodeSystem - Absent and Unknown Data" 
* compose.include[0].concept[1].code = #no-known-allergies
* compose.include[0].concept[1].display = "keine Überempfindlichkeiten"
* compose.include[0].concept[1].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[1].designation[0].value = "IPS CodeSystem - Absent and Unknown Data" 

* expansion.timestamp = "2022-09-13T14:16:43.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem-ips-absentorunknowndata"
* expansion.contains[0].code = #no-allergy-info
* expansion.contains[0].display = "keine Information über Überempfindlichkeiten verfügbar"
* expansion.contains[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[0].designation[0].value = "IPS CodeSystem - Absent and Unknown Data" 
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem-ips-absentorunknowndata"
* expansion.contains[1].code = #no-known-allergies
* expansion.contains[1].display = "keine Überempfindlichkeiten"
* expansion.contains[1].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[1].designation[0].value = "IPS CodeSystem - Absent and Unknown Data" 
