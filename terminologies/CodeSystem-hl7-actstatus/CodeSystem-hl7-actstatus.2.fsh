Instance: hl7-actstatus 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-actstatus" 
* name = "hl7-actstatus" 
* title = "HL7 ActStatus" 
* status = #active 
* content = #complete 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.5.14" 
* date = "2015-07-15" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://wiki.hl7.de/index.php/2.16.840.1.113883.5.14" 
* count = 10 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #normal "normal"
* #normal ^definition = Umfasst die erwarteten Zustände des Acts, jedoch nicht "nullified" und "obsolete", die spezielle Endzustände für den Lebenszyklus darstellen
* #normal ^designation[0].language = #de-AT 
* #normal ^designation[0].value = "Normal" 
* #normal ^property[0].code = #child 
* #normal ^property[0].valueCode = #aborted 
* #normal ^property[1].code = #child 
* #normal ^property[1].valueCode = #active 
* #normal ^property[2].code = #child 
* #normal ^property[2].valueCode = #cancelled 
* #normal ^property[3].code = #child 
* #normal ^property[3].valueCode = #completed 
* #normal ^property[4].code = #child 
* #normal ^property[4].valueCode = #held 
* #normal ^property[5].code = #child 
* #normal ^property[5].valueCode = #new 
* #normal ^property[6].code = #child 
* #normal ^property[6].valueCode = #suspended 
* #aborted "aborted"
* #aborted ^definition = Vor Abschluss abgebrochen, wird nicht mehr aufgenommen.
* #aborted ^designation[0].language = #de-AT 
* #aborted ^designation[0].value = "Abgebrochen" 
* #aborted ^property[0].code = #parent 
* #aborted ^property[0].valueCode = #normal 
* #active "active"
* #active ^definition = Bereits begonnen oder kann begonnen werden.
* #active ^designation[0].language = #de-AT 
* #active ^designation[0].value = "Aktiv" 
* #active ^property[0].code = #parent 
* #active ^property[0].valueCode = #normal 
* #cancelled "cancelled"
* #cancelled ^definition = Vor Beginn/Aktivierung storniert.
* #cancelled ^designation[0].language = #de-AT 
* #cancelled ^designation[0].value = "Storniert" 
* #cancelled ^property[0].code = #parent 
* #cancelled ^property[0].valueCode = #normal 
* #completed "completed"
* #completed ^definition = Normal fertiggestellt.
* #completed ^designation[0].language = #de-AT 
* #completed ^designation[0].value = "Abgeschlossen" 
* #completed ^property[0].code = #parent 
* #completed ^property[0].valueCode = #normal 
* #held "held"
* #held ^definition = Angehalten, Act muss vor Beginn/Aktivierung freigegeben werden.
* #held ^designation[0].language = #de-AT 
* #held ^designation[0].value = "Angehalten" 
* #held ^property[0].code = #parent 
* #held ^property[0].valueCode = #normal 
* #new "new"
* #new ^definition = Neu, Act hat noch nicht begonnen.
* #new ^designation[0].language = #de-AT 
* #new ^designation[0].value = "Neu " 
* #new ^property[0].code = #parent 
* #new ^property[0].valueCode = #normal 
* #suspended "suspended"
* #suspended ^definition = Nach Beginn/Aktivierung unterbrochen  - Act kann fortgesetzt werden.
* #suspended ^designation[0].language = #de-AT 
* #suspended ^designation[0].value = "Ausgesetzt" 
* #suspended ^property[0].code = #parent 
* #suspended ^property[0].valueCode = #normal 
* #nullified "nullified"
* #nullified ^definition = Act wurde irrtümlich angelegt. Act bleibt für Audit erhalten.
* #nullified ^designation[0].language = #de-AT 
* #nullified ^designation[0].value = "Annulliert" 
* #obsolete "obsolete"
* #obsolete ^definition = Ersetzt durch einen anderen Act.
* #obsolete ^designation[0].language = #de-AT 
* #obsolete ^designation[0].value = "Obsolet" 
