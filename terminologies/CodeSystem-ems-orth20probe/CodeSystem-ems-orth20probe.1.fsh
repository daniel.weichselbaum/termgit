Instance: ems-orth20probe 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-orth20probe" 
* name = "ems-orth20probe" 
* title = "EMS_OrtH20Probe" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** Ort der Probenentnahme (nur beantworten wenn Umweltuntersuchung = ja)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.95" 
* date = "2017-01-26" 
* count = 5 
* concept[0].code = #COOLTOWER
* concept[0].display = "Kühlturm"
* concept[1].code = #POOL
* concept[1].display = "Whirlpool / Spa pool"
* concept[2].code = #WSYS
* concept[2].display = "Wassersytem"
* concept[3].code = #WSYS_C
* concept[3].display = "Kaltwassersystem"
* concept[4].code = #WSYS_W
* concept[4].display = "Warmwassersytem"
