Instance: medikationtherapieart 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-medikationtherapieart" 
* name = "medikationtherapieart" 
* title = "MedikationTherapieart" 
* status = #active 
* content = #complete 
* version = "3.0" 
* description = "**Description:** ELGA Codelist for type of therapy

**Beschreibung:** ELGA Codeliste für Therapieart" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.10.1.4.3.4.3.6" 
* date = "2015-03-31" 
* count = 2 
* concept[0].code = #EINZEL
* concept[0].display = "Einzelverordnung"
* concept[0].designation[0].language = #de-AT 
* concept[0].designation[0].value = "Einzelverordnung" 
* concept[1].code = #NICHTEINZEL
* concept[1].display = "Nicht-Einzelverordnung"
* concept[1].designation[0].language = #de-AT 
* concept[1].designation[0].value = "Nicht-Einzelverordnung" 
