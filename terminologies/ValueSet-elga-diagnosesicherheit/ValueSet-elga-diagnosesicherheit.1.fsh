Instance: elga-diagnosesicherheit 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-diagnosesicherheit" 
* name = "elga-diagnosesicherheit" 
* title = "ELGA_Diagnosesicherheit" 
* status = #active 
* version = "4.1" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.168" 
* date = "2017-02-17" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-sciphox-diagnosenzusatz"
* compose.include[0].concept[0].code = #G
* compose.include[0].concept[0].display = "Gesichert"
* compose.include[0].concept[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[0].designation[0].value = "Gesicherte Diagnose " 
* compose.include[0].concept[1].code = #V
* compose.include[0].concept[1].display = "Verdacht auf"
* compose.include[0].concept[1].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[1].designation[0].value = "Verdachtsdiagnose " 
* compose.include[0].concept[1].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[1].designation[1].value = "uncertaintyCode = UN" 

* expansion.timestamp = "2022-09-13T14:18:16.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem-sciphox-diagnosenzusatz"
* expansion.contains[0].code = #G
* expansion.contains[0].display = "Gesichert"
* expansion.contains[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[0].designation[0].value = "Gesicherte Diagnose " 
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem-sciphox-diagnosenzusatz"
* expansion.contains[1].code = #V
* expansion.contains[1].display = "Verdacht auf"
* expansion.contains[1].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[1].designation[0].value = "Verdachtsdiagnose " 
* expansion.contains[1].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* expansion.contains[1].designation[1].value = "uncertaintyCode = UN" 
