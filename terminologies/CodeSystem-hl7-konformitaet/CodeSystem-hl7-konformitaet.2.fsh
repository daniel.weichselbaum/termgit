Instance: hl7-konformitaet 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-konformitaet" 
* name = "hl7-konformitaet" 
* title = "HL7 Konformitaet" 
* status = #active 
* content = #complete 
* date = "2015-06-02" 
* count = 7 
* property[0].code = #status 
* property[0].type = #code 
* #B "Backward Compatibility"
* #B ^designation[0].language = #de-AT 
* #B ^designation[0].value = "nur für Anwärtskompatibilität zu älteren HL7-Versionen benötigt" 
* #C "Conditional"
* #C ^designation[0].language = #de-AT 
* #C ^designation[0].value = "bedingt, abhängig von anderen Feldern oder best. Triggerevents" 
* #O "Optional"
* #O ^designation[0].language = #de-AT 
* #O ^designation[0].value = "OPTIONAL" 
* #R "Required"
* #R ^designation[0].language = #de-AT 
* #R ^designation[0].value = "Notwendig, d.h. MUSS unterstützt werden und darf nicht leer sein" 
* #RE "Required, but may be empty"
* #RE ^designation[0].language = #de-AT 
* #RE ^designation[0].value = "MUSS unterstützt werden, darf aber auch leer sein" 
* #W "withdrawn"
* #W ^designation[0].language = #de-AT 
* #W ^designation[0].value = "entfernt, nicht mehr benutzt" 
* #X "deprecated"
* #X ^designation[0].language = #de-AT 
* #X ^designation[0].value = "nicht (mehr) erlaubt" 
* #X ^property[0].code = #status 
* #X ^property[0].valueCode = #retired 
