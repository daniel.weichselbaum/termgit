Instance: ems-genotyppora-r2 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-genotyppora-r2" 
* name = "ems-genotyppora-r2" 
* title = "EMS_GenotypPorA_R2" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Liste Genotyp PorA variable region 2" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.73" 
* date = "2017-01-26" 
* count = 448 
* #1 "1"
* #10 "10"
* #10_1 "10-1"
* #10_10 "10-10"
* #10_11 "10-11"
* #10_12 "10-12"
* #10_13 "10-13"
* #10_14 "10-14"
* #10_15 "10-15"
* #10_16 "10-16"
* #10_17 "10-17"
* #10_18 "10-18"
* #10_19 "10-19"
* #10_2 "10-2"
* #10_20 "10-20"
* #10_21 "10-21"
* #10_22 "10-22"
* #10_23 "10-23"
* #10_24 "10-24"
* #10_25 "10-25"
* #10_26 "10-26"
* #10_27 "10-27"
* #10_28 "10-28"
* #10_29 "10-29"
* #10_3 "10-3"
* #10_30 "10-30"
* #10_31 "10-31"
* #10_32 "10-32"
* #10_33 "10-33"
* #10_34 "10-34"
* #10_35 "10-35"
* #10_36 "10-36"
* #10_37 "10-37"
* #10_38 "10-38"
* #10_39 "10-39"
* #10_4 "10-4"
* #10_40 "10-40"
* #10_41 "10-41"
* #10_42 "10-42"
* #10_43 "10-43"
* #10_44 "10-44"
* #10_45 "10-45"
* #10_46 "10-46"
* #10_47 "10-47"
* #10_48 "10-48"
* #10_49 "10-49"
* #10_5 "10-5"
* #10_50 "10-50"
* #10_51 "10-51"
* #10_52 "10-52"
* #10_53 "10-53"
* #10_54 "10-54"
* #10_55 "10-55"
* #10_56 "10-56"
* #10_57 "10-57"
* #10_58 "10-58"
* #10_59 "10-59"
* #10_6 "10-6"
* #10_60 "10-60"
* #10_61 "10-61"
* #10_62 "10-62"
* #10_63 "10-63"
* #10_64 "10-64"
* #10_65 "10-65"
* #10_66 "10-66"
* #10_7 "10-7"
* #10_8 "10-8"
* #10_9 "10-9"
* #13 "13"
* #13_1 "13-1"
* #13_10 "13-10"
* #13_11 "13-11"
* #13_12 "13-12"
* #13_13 "13-13"
* #13_14 "13-14"
* #13_15 "13-15"
* #13_16 "13-16"
* #13_17 "13-17"
* #13_18 "13-18"
* #13_19 "13-19"
* #13_2 "13-2"
* #13_20 "13-20"
* #13_21 "13-21"
* #13_22 "13-22"
* #13_23 "13-23"
* #13_24 "13-24"
* #13_25 "13-25"
* #13_26 "13-26"
* #13_27 "13-27"
* #13_28 "13-28"
* #13_29 "13-29"
* #13_3 "13-3"
* #13_30 "13-30"
* #13_31 "13-31"
* #13_32 "13-32"
* #13_33 "13-33"
* #13_34 "13-34"
* #13_4 "13-4"
* #13_5 "13-5"
* #13_6 "13-6"
* #13_7 "13-7"
* #13_8 "13-8"
* #13_9 "13-9"
* #14 "14"
* #14_1 "14-1"
* #14_10 "14-10"
* #14_11 "14-11"
* #14_12 "14-12"
* #14_13 "14-13"
* #14_14 "14-14"
* #14_15 "14-15"
* #14_16 "14-16"
* #14_17 "14-17"
* #14_18 "14-18"
* #14_19 "14-19"
* #14_2 "14-2"
* #14_20 "14-20"
* #14_21 "14-21"
* #14_22 "14-22"
* #14_3 "14-3"
* #14_4 "14-4"
* #14_5 "14-5"
* #14_6 "14-6"
* #14_7 "14-7"
* #14_8 "14-8"
* #14_9 "14-9"
* #15 "15"
* #15_1 "15-1"
* #15_10 "15-10"
* #15_11 "15-11"
* #15_12 "15-12"
* #15_13 "15-13"
* #15_14 "15-14"
* #15_15 "15-15"
* #15_16 "15-16"
* #15_17 "15-17"
* #15_18 "15-18"
* #15_19 "15-19"
* #15_2 "15-2"
* #15_20 "15-20"
* #15_21 "15-21"
* #15_22 "15-22"
* #15_23 "15-23"
* #15_24 "15-24"
* #15_25 "15-25"
* #15_26 "15-26"
* #15_27 "15-27"
* #15_28 "15-28"
* #15_29 "15-29"
* #15_3 "15-3"
* #15_30 "15-30"
* #15_31 "15-31"
* #15_32 "15-32"
* #15_33 "15-33"
* #15_34 "15-34"
* #15_35 "15-35"
* #15_36 "15-36"
* #15_37 "15-37"
* #15_38 "15-38"
* #15_4 "15-4"
* #15_5 "15-5"
* #15_6 "15-6"
* #15_7 "15-7"
* #15_8 "15-8"
* #15_9 "15-9"
* #16 "16"
* #16_1 "16-1"
* #16_10 "16-10"
* #16_11 "16-11"
* #16_12 "16-12"
* #16_13 "16-13"
* #16_14 "16-14"
* #16_15 "16-15"
* #16_16 "16-16"
* #16_17 "16-17"
* #16_18 "16-18"
* #16_19 "16-19"
* #16_2 "16-2"
* #16_20 "16-20"
* #16_21 "16-21"
* #16_22 "16-22"
* #16_23 "16-23"
* #16_24 "16-24"
* #16_25 "16-25"
* #16_26 "16-26"
* #16_27 "16-27"
* #16_28 "16-28"
* #16_29 "16-29"
* #16_3 "16-3"
* #16_30 "16-30"
* #16_31 "16-31"
* #16_32 "16-32"
* #16_33 "16-33"
* #16_34 "16-34"
* #16_35 "16-35"
* #16_36 "16-36"
* #16_37 "16-37"
* #16_38 "16-38"
* #16_39 "16-39"
* #16_4 "16-4"
* #16_40 "16-40"
* #16_41 "16-41"
* #16_42 "16-42"
* #16_43 "16-43"
* #16_44 "16-44"
* #16_45 "16-45"
* #16_46 "16-46"
* #16_47 "16-47"
* #16_48 "16-48"
* #16_49 "16-49"
* #16_5 "16-5"
* #16_50 "16-50"
* #16_51 "16-51"
* #16_52 "16-52"
* #16_53 "16-53"
* #16_54 "16-54"
* #16_55 "16-55"
* #16_56 "16-56"
* #16_57 "16-57"
* #16_58 "16-58"
* #16_59 "16-59"
* #16_6 "16-6"
* #16_60 "16-60"
* #16_61 "16-61"
* #16_62 "16-62"
* #16_63 "16-63"
* #16_64 "16-64"
* #16_65 "16-65"
* #16_66 "16-66"
* #16_67 "16-67"
* #16_68 "16-68"
* #16_69 "16-69"
* #16_7 "16-7"
* #16_70 "16-70"
* #16_71 "16-71"
* #16_72 "16-72"
* #16_73 "16-73"
* #16_74 "16-74"
* #16_75 "16-75"
* #16_76 "16-76"
* #16_77 "16-77"
* #16_78 "16-78"
* #16_79 "16-79"
* #16_8 "16-8"
* #16_80 "16-80"
* #16_81 "16-81"
* #16_82 "16-82"
* #16_83 "16-83"
* #16_84 "16-84"
* #16_85 "16-85"
* #16_86 "16-86"
* #16_87 "16-87"
* #16_88 "16-88"
* #16_89 "16-89"
* #16_9 "16-9"
* #16_90 "16-90"
* #16_91 "16-91"
* #1_1 "1-1"
* #1_2 "1-2"
* #1_3 "1-3"
* #1_4 "1-4"
* #1_5 "1-5"
* #1_6 "1-6"
* #1_7 "1-7"
* #1_8 "1-8"
* #2 "2"
* #23 "23"
* #23_1 "23-1"
* #23_10 "23-10"
* #23_11 "23-11"
* #23_12 "23-12"
* #23_13 "23-13"
* #23_14 "23-14"
* #23_15 "23-15"
* #23_16 "23-16"
* #23_17 "23-17"
* #23_2 "23-2"
* #23_3 "23-3"
* #23_4 "23-4"
* #23_5 "23-5"
* #23_6 "23-6"
* #23_7 "23-7"
* #23_8 "23-8"
* #23_9 "23-9"
* #25 "25"
* #25_1 "25-1"
* #25_10 "25-10"
* #25_11 "25-11"
* #25_12 "25-12"
* #25_13 "25-13"
* #25_14 "25-14"
* #25_15 "25-15"
* #25_16 "25-16"
* #25_17 "25-17"
* #25_18 "25-18"
* #25_19 "25-19"
* #25_2 "25-2"
* #25_20 "25-20"
* #25_21 "25-21"
* #25_22 "25-22"
* #25_23 "25-23"
* #25_24 "25-24"
* #25_3 "25-3"
* #25_4 "25-4"
* #25_5 "25-5"
* #25_6 "25-6"
* #25_7 "25-7"
* #25_8 "25-8"
* #25_9 "25-9"
* #26 "26"
* #26_1 "26-1"
* #26_2 "26-2"
* #26_3 "26-3"
* #26_4 "26-4"
* #26_5 "26-5"
* #28 "28"
* #28_1 "28-1"
* #28_2 "28-2"
* #2_1 "2-1"
* #2_10 "2-10"
* #2_11 "2-11"
* #2_12 "2-12"
* #2_13 "2-13"
* #2_14 "2-14"
* #2_15 "2-15"
* #2_16 "2-16"
* #2_17 "2-17"
* #2_18 "2-18"
* #2_19 "2-19"
* #2_2 "2-2"
* #2_20 "2-20"
* #2_21 "2-21"
* #2_22 "2-22"
* #2_23 "2-23"
* #2_24 "2-24"
* #2_25 "2-25"
* #2_26 "2-26"
* #2_27 "2-27"
* #2_28 "2-28"
* #2_29 "2-29"
* #2_3 "2-3"
* #2_30 "2-30"
* #2_31 "2-31"
* #2_32 "2-32"
* #2_33 "2-33"
* #2_34 "2-34"
* #2_35 "2-35"
* #2_36 "2-36"
* #2_37 "2-37"
* #2_38 "2-38"
* #2_39 "2-39"
* #2_4 "2-4"
* #2_40 "2-40"
* #2_41 "2-41"
* #2_42 "2-42"
* #2_43 "2-43"
* #2_44 "2-44"
* #2_45 "2-45"
* #2_46 "2-46"
* #2_47 "2-47"
* #2_48 "2-48"
* #2_49 "2-49"
* #2_5 "2-5"
* #2_50 "2-50"
* #2_51 "2-51"
* #2_52 "2-52"
* #2_53 "2-53"
* #2_54 "2-54"
* #2_55 "2-55"
* #2_6 "2-6"
* #2_7 "2-7"
* #2_8 "2-8"
* #2_9 "2-9"
* #3 "3"
* #30 "30"
* #30_1 "30-1"
* #30_10 "30-10"
* #30_11 "30-11"
* #30_12 "30-12"
* #30_13 "30-13"
* #30_2 "30-2"
* #30_3 "30-3"
* #30_4 "30-4"
* #30_5 "30-5"
* #30_6 "30-6"
* #30_7 "30-7"
* #30_8 "30-8"
* #30_9 "30-9"
* #34 "34"
* #34_1 "34-1"
* #34_2 "34-2"
* #3_1 "3-1"
* #3_2 "3-2"
* #3_3 "3-3"
* #3_4 "3-4"
* #3_5 "3-5"
* #3_6 "3-6"
* #3_7 "3-7"
* #3_8 "3-8"
* #3_9 "3-9"
* #4 "4"
* #42 "42"
* #42_1 "42-1"
* #42_2 "42-2"
* #42_3 "42-3"
* #43 "43"
* #4_1 "4-1"
* #4_10 "4-10"
* #4_11 "4-11"
* #4_12 "4-12"
* #4_13 "4-13"
* #4_14 "4-14"
* #4_15 "4-15"
* #4_16 "4-16"
* #4_17 "4-17"
* #4_18 "4-18"
* #4_19 "4-19"
* #4_2 "4-2"
* #4_20 "4-20"
* #4_21 "4-21"
* #4_22 "4-22"
* #4_23 "4-23"
* #4_24 "4-24"
* #4_25 "4-25"
* #4_26 "4-26"
* #4_27 "4-27"
* #4_3 "4-3"
* #4_4 "4-4"
* #4_5 "4-5"
* #4_6 "4-6"
* #4_7 "4-7"
* #4_8 "4-8"
* #4_9 "4-9"
* #9 "9"
* #9_1 "9-1"
* #9_10 "9-10"
* #9_11 "9-11"
* #9_12 "9-12"
* #9_2 "9-2"
* #9_3 "9-3"
* #9_4 "9-4"
* #9_5 "9-5"
* #9_6 "9-6"
* #9_7 "9-7"
* #9_8 "9-8"
* #9_9 "9-9"
* #NOTEST "Nicht durchgeführt"
* #NST "Not subtypeable"
