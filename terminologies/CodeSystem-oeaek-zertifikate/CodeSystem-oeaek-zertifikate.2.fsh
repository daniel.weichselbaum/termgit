Instance: oeaek-zertifikate 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-oeaek-zertifikate" 
* name = "oeaek-zertifikate" 
* title = "OEAEK_Zertifikate" 
* status = #active 
* content = #complete 
* version = "1.0" 
* description = "**Description:** Code List of all certificates valid in Austria issued by the Medical Chamber of Austria

**Beschreibung:** Code Liste aller in Österreich gültigen ÖÄK Zertifikate" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.3.1.10.50" 
* date = "2018-07-01" 
* count = 25 
* #104 "Reisemedizin"
* #104 ^designation[0].language = #de-AT 
* #104 ^designation[0].value = "Reisemedizin" 
* #3 "Angiologische Basisdiagnostik"
* #3 ^designation[0].language = #de-AT 
* #3 ^designation[0].value = "Angiologische Basisdiagnostik" 
* #4210 "Sonographie Abdomen"
* #4210 ^designation[0].language = #de-AT 
* #4210 ^designation[0].value = "Sonographie Abdomen" 
* #4212 "Sonographie Arterien"
* #4212 ^designation[0].language = #de-AT 
* #4212 ^designation[0].value = "Sonographie Arterien" 
* #4214 "Sonographie Bewegungsapparat"
* #4214 ^designation[0].language = #de-AT 
* #4214 ^designation[0].value = "Sonographie Bewegungsapparat" 
* #4216 "Sonographie Bulbus und Anhängsel"
* #4216 ^designation[0].language = #de-AT 
* #4216 ^designation[0].value = "Sonographie Bulbus und Anhängsel" 
* #4218 "Sonographie Echokardiographie"
* #4218 ^designation[0].language = #de-AT 
* #4218 ^designation[0].value = "Sonographie Echokardiographie" 
* #4220 "Sonographie Hirnversorgende Arterien"
* #4220 ^designation[0].language = #de-AT 
* #4220 ^designation[0].value = "Sonographie Hirnversorgende Arterien" 
* #4222 "Sonographie Hüftsonographie"
* #4222 ^designation[0].language = #de-AT 
* #4222 ^designation[0].value = "Sonographie Hüftsonographie" 
* #4224 "Sonographie Mammae"
* #4224 ^designation[0].language = #de-AT 
* #4224 ^designation[0].value = "Sonographie Mammae" 
* #4226 "Sonographie Orbita"
* #4226 ^designation[0].language = #de-AT 
* #4226 ^designation[0].value = "Sonographie Orbita" 
* #4228 "Sonographie Päd Echokardiographie"
* #4228 ^designation[0].language = #de-AT 
* #4228 ^designation[0].value = "Sonographie Päd Echokardiographie" 
* #4230 "Sonographie Pädiatrische Echokardiographie"
* #4230 ^designation[0].language = #de-AT 
* #4230 ^designation[0].value = "Sonographie Pädiatrische Echokardiographie" 
* #4232 "Sonographie Pädiatrische Sonographie"
* #4232 ^designation[0].language = #de-AT 
* #4232 ^designation[0].value = "Sonographie Pädiatrische Sonographie" 
* #4234 "Sonographie Pleura und Lunge"
* #4234 ^designation[0].language = #de-AT 
* #4234 ^designation[0].value = "Sonographie Pleura und Lunge" 
* #4236 "Sonographie Schilddrüse"
* #4236 ^designation[0].language = #de-AT 
* #4236 ^designation[0].value = "Sonographie Schilddrüse" 
* #4238 "Sonographie Smallpart"
* #4238 ^designation[0].language = #de-AT 
* #4238 ^designation[0].value = "Sonographie Smallpart" 
* #4240 "Sonographie TEE Fokussierte Echokardiographie"
* #4240 ^designation[0].language = #de-AT 
* #4240 ^designation[0].value = "Sonographie TEE Fokussierte Echokardiographie" 
* #4242 "Sonographie transkraniell"
* #4242 ^designation[0].language = #de-AT 
* #4242 ^designation[0].value = "Sonographie transkraniell" 
* #4244 "Sonographie Urogenital"
* #4244 ^designation[0].language = #de-AT 
* #4244 ^designation[0].value = "Sonographie Urogenital" 
* #4246 "Sonographie Venen"
* #4246 ^designation[0].language = #de-AT 
* #4246 ^designation[0].value = "Sonographie Venen" 
* #4248 "Sonographie Weiblicher Unterbauch"
* #4248 ^designation[0].language = #de-AT 
* #4248 ^designation[0].value = "Sonographie Weiblicher Unterbauch" 
* #54 "Elektroenzephalographie"
* #54 ^designation[0].language = #de-AT 
* #54 ^designation[0].value = "Elektroenzephalographie" 
* #7 "Ärztliche Wundbehandlung"
* #7 ^designation[0].language = #de-AT 
* #7 ^designation[0].value = "Ärztliche Wundbehandlung" 
* #8 "Basismodul Sexualmedizin"
* #8 ^designation[0].language = #de-AT 
* #8 ^designation[0].value = "Basismodul Sexualmedizin" 
