Instance: ems-hospitalisierung 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-ems-hospitalisierung" 
* name = "ems-hospitalisierung" 
* title = "EMS_Hospitalisierung" 
* status = #active 
* version = "202001" 
* description = "**Description:** Kind of hospitalization. Contains information protected by copyright of SNOMED International. Any use of SNOMED CT in Austria requires a valid affiliate license or sublicense. The corresponding license is free of charge, provided that the use only takes place in Austria and fulfills the conditions of the Affiliate License Agreement. Affiliate licenses can be requested directly from the respective NRC via the [Member Licensing and Distribution Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS).

**Beschreibung:** Valueset mit Arten der Hospitalisierung für EMS. Enthält durch SNOMED International urheberrechtlich geschützte Information. Jede Verwendung von SNOMED CT in Österreich erfordert eine aufrechte Affiliate Lizenz oder eine Sublizenz. Die entsprechende Lizenz ist kostenlos, vorausgesetzt die Verwendung findet nur in Österreich statt und erfüllt die Bedingungen des Affiliate License Agreements. Affiliate Lizenzen können über das [Mitglieder-Lizenzierungs- und Distributions-Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS) direkt beim jeweiligen NRC beantragt werden." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.49" 
* date = "2020-01-01" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* compose.include[0].concept[0].code = #305351004
* compose.include[0].concept[0].display = "Admission to intensive care unit (procedure)"
* compose.include[0].concept[1].code = #32485007
* compose.include[0].concept[1].display = "Hospital admission (procedure)"

* expansion.timestamp = "2022-09-13T14:15:19.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* expansion.contains[0].code = #305351004
* expansion.contains[0].display = "Admission to intensive care unit (procedure)"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* expansion.contains[1].code = #32485007
* expansion.contains[1].display = "Hospital admission (procedure)"
