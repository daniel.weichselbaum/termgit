Instance: elga-proficiencylevelcode 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-proficiencylevelcode" 
* name = "elga-proficiencylevelcode" 
* title = "ELGA_ProficiencyLevelCode" 
* status = #active 
* version = "4.1" 
* description = "**Description:** List of codes representing the level of proficiency in a language.

**Beschreibung:** Codes zur Angabe der Sprachfähigkeit von Patienten (Sprachkenntnisse)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.174" 
* date = "2017-02-17" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.hl7.org/documentcenter/public_temp_C0F0C5A1-1C23-BA17-0C878A83AC28B5A7/standards/vocabulary/vocabulary_tables/infrastructure/vocabulary/LanguageAbilityProficiency.html" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-hl7-proficiencylevelcode"
* compose.include[0].concept[0].code = "E"
* compose.include[0].concept[0].display = "Excellent"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "ausgezeichnet" 
* compose.include[0].concept[1].code = "F"
* compose.include[0].concept[1].display = "Fair"
* compose.include[0].concept[1].designation[0].language = #de-AT 
* compose.include[0].concept[1].designation[0].value = "ausreichend" 
* compose.include[0].concept[2].code = "G"
* compose.include[0].concept[2].display = "Good"
* compose.include[0].concept[2].designation[0].language = #de-AT 
* compose.include[0].concept[2].designation[0].value = "gut" 
* compose.include[0].concept[3].code = "P"
* compose.include[0].concept[3].display = "Poor"
* compose.include[0].concept[3].designation[0].language = #de-AT 
* compose.include[0].concept[3].designation[0].value = "mangelhaft" 
