Instance: elga-entlassungsmanagementart 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-entlassungsmanagementart" 
* name = "elga-entlassungsmanagementart" 
* title = "ELGA_EntlassungsmanagementArt" 
* status = #active 
* content = #complete 
* version = "2.8" 
* description = "**Description:** Describes the variety in a discharge management disposition (describes the kind of discharge date)

**Beschreibung:** Beschreibt die Möglichkeiten in einer Entlassungsmanagement-Disposition (Beschreibt die Art des Entlassungsdatums)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.28" 
* date = "2013-01-10" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* count = 1 
* concept[0].code = #GEPLENTLDAT
* concept[0].display = "Geplantes Entlassungsdatum"
* concept[0].designation[0].language = #de-AT 
* concept[0].designation[0].value = "Geplantes Entlassungsdatum" 
