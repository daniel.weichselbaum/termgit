Instance: hl7-proficiencylevelcode 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-proficiencylevelcode" 
* name = "hl7-proficiencylevelcode" 
* title = "HL7 ProficiencyLevelCode" 
* status = #active 
* content = #complete 
* description = "**Description:** List of codes representing the level of proficiency in a language.

**Beschreibung:** Codes zur Angabe der Sprachfähigkeit von Patienten (Sprachkenntnisse)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.5.61" 
* date = "2016-11-24" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.hl7.org/documentcenter/public_temp_C0F0C5A1-1C23-BA17-0C878A83AC28B5A7/standards/vocabulary/vocabulary_tables/infrastructure/vocabulary/LanguageAbilityProficiency.html" 
* count = 4 
* #E "Excellent"
* #F "Fair"
* #G "Good"
* #P "Poor"
