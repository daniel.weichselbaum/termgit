Instance: hl7-at-formatcodes 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-at-formatcodes" 
* name = "hl7-at-formatcodes" 
* title = "HL7 AT FormatCodes" 
* status = #active 
* content = #complete 
* version = "1.0" 
* description = "**Beschreibung:** Diese Formatcodes ergänzen die ELGA_FormatCodes" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.2.16.1.4.10" 
* date = "1899-12-31" 
* count = 9 
* concept[0].code = #urn:ehealth-austria:BMP
* concept[0].display = "BMP-Bilddaten (Windows Bitmap)"
* concept[1].code = #urn:ehealth-austria:DICOM
* concept[1].display = "DICOM Objekte"
* concept[2].code = #urn:ehealth-austria:JPG
* concept[2].display = "JPG-Bilddaten"
* concept[3].code = #urn:ehealth-austria:MP4
* concept[3].display = "MPEG-4-Videodateien gem. ISO/IEC 14496-12 und -14"
* concept[4].code = #urn:ehealth-austria:PDF
* concept[4].display = "PDF-Dokumente"
* concept[5].code = #urn:ehealth-austria:PDF/A-1
* concept[5].display = "PDF-Dokumente gem. ISO 19005-1:2005"
* concept[6].code = #urn:ehealth-austria:PDF/A-2
* concept[6].display = "PDF-Dokumente gem. ISO 19005-2:2011"
* concept[7].code = #urn:ehealth-austria:PDF/A-3
* concept[7].display = "PDF-Dokumente gem. ISO 19005-3:2012"
* concept[8].code = #urn:ehealth-austria:WMF
* concept[8].display = "WMF-Bilddaten (Windows Metafile)"
