Instance: hl7-at-formatcodes 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-at-formatcodes" 
* name = "hl7-at-formatcodes" 
* title = "HL7 AT FormatCodes" 
* status = #active 
* content = #complete 
* version = "1.0" 
* description = "**Beschreibung:** Diese Formatcodes ergänzen die ELGA_FormatCodes" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.2.16.1.4.10" 
* date = "1899-12-31" 
* count = 9 
* #urn:ehealth-austria:BMP "BMP-Bilddaten (Windows Bitmap)"
* #urn:ehealth-austria:DICOM "DICOM Objekte"
* #urn:ehealth-austria:JPG "JPG-Bilddaten"
* #urn:ehealth-austria:MP4 "MPEG-4-Videodateien gem. ISO/IEC 14496-12 und -14"
* #urn:ehealth-austria:PDF "PDF-Dokumente"
* #urn:ehealth-austria:PDF/A-1 "PDF-Dokumente gem. ISO 19005-1:2005"
* #urn:ehealth-austria:PDF/A-2 "PDF-Dokumente gem. ISO 19005-2:2011"
* #urn:ehealth-austria:PDF/A-3 "PDF-Dokumente gem. ISO 19005-3:2012"
* #urn:ehealth-austria:WMF "WMF-Bilddaten (Windows Metafile)"
