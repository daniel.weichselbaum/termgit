Instance: ems-monoclonalsub 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-monoclonalsub" 
* name = "ems-monoclonalsub" 
* title = "EMS_MonoclonalSub" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Monoclonal Subtyp: Verwendung bei Legionellose" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.83" 
* date = "2017-01-26" 
* count = 17 
* #ALLN "Allentown (serogroup 1)"
* #ALLNFRAN "Allentown/France (serogroup 1)"
* #BELI "Bellingham (serogroup 1)"
* #BENI "Benidorm (serogroup 1)"
* #CAMB "Cambridge (serogroup 5)"
* #CAMPER "Camperdown (serogroup 1)"
* #DALA "Dallas (serogroup 5)"
* #FRAN "France (serogroup 1)"
* #HEY "Heysham (serogroup 1)"
* #KNOW "Knoxville (serogroup 1)"
* #LA "Los Angeles (serogroup 4)"
* #NA "Not applicable"
* #OLDA "OLDA (serogroup 1)"
* #OXF "Oxford (serogroup 1)"
* #OXFOLDA "Oxford/OLDA (serogroup 1)"
* #PHIL "Philadelphia (serogroup 1)"
* #PORT4 "Portland 4 (serogroup 4)"
