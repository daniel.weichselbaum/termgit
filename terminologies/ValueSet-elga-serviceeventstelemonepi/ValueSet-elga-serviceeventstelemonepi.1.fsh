Instance: elga-serviceeventstelemonepi 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-serviceeventstelemonepi" 
* name = "elga-serviceeventstelemonepi" 
* title = "ELGA_ServiceEventsTelemonEpi" 
* status = #active 
* version = "202106" 
* description = "**Description:** Contains information protected by copyright of SNOMED International. Any use of SNOMED CT in Austria requires a valid affiliate license or sublicense. The corresponding license is free of charge, provided that the use only takes place in Austria and fulfills the conditions of the Affiliate License Agreement. Affiliate licenses can be requested directly from the respective NRC via the [Member Licensing and Distribution Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS).

**Beschreibung:** Enthält durch SNOMED International urheberrechtlich geschützte Information. Jede Verwendung von SNOMED CT in Österreich erfordert eine aufrechte Affiliate Lizenz oder eine Sublizenz. Die entsprechende Lizenz ist kostenlos, vorausgesetzt die Verwendung findet nur in Österreich statt und erfüllt die Bedingungen des Affiliate License Agreements. Affiliate Lizenzen können über das [Mitglieder-Lizenzierungs- und Distributions-Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS) direkt beim jeweiligen NRC beantragt werden." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.26" 
* date = "2021-06-07" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* compose.include[0].concept[0].code = #719858009
* compose.include[0].concept[0].display = "Telehealth monitoring (regime/therapy)"
* compose.include[0].concept[1].code = #715191006
* compose.include[0].concept[1].display = "Telehealth asthma monitoring (regime/therapy)"
* compose.include[0].concept[2].code = #715280009
* compose.include[0].concept[2].display = "Telehealth hypertension monitoring (regime/therapy)"
* compose.include[0].concept[3].code = #473199000
* compose.include[0].concept[3].display = "Telehealth monitoring for chronic disease (regime/therapy)"
* compose.include[0].concept[4].code = #716358000
* compose.include[0].concept[4].display = "Telehealth chronic obstructive pulmonary disease monitoring (regime/therapy)"
* compose.include[0].concept[5].code = #879780004
* compose.include[0].concept[5].display = "Telehealth monitoring for chronic heart failure (regime/therapy)"
* compose.include[0].concept[6].code = #879782007
* compose.include[0].concept[6].display = "Telehealth monitoring for diabetes mellitus (regime/therapy)"
* compose.include[0].concept[7].code = #879781000
* compose.include[0].concept[7].display = "Telehealth monitoring for ischemic heart disease (regime/therapy)"
* compose.include[0].concept[8].code = #879783002
* compose.include[0].concept[8].display = "Telehealth monitoring for psychological stress (regime/therapy)"
* compose.include[0].concept[9].code = #715279006
* compose.include[0].concept[9].display = "Telehealth obesity monitoring (regime/therapy)"
* compose.include[0].concept[10].code = #879784008
* compose.include[0].concept[10].display = "Telehealth rehabilitation monitoring (regime/therapy)"

* expansion.timestamp = "2022-09-13T14:15:12.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* expansion.contains[0].code = #719858009
* expansion.contains[0].display = "Telehealth monitoring (regime/therapy)"
* expansion.contains[0].contains[0].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* expansion.contains[0].contains[0].code = #715191006
* expansion.contains[0].contains[0].display = "Telehealth asthma monitoring (regime/therapy)"
* expansion.contains[0].contains[1].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* expansion.contains[0].contains[1].code = #715280009
* expansion.contains[0].contains[1].display = "Telehealth hypertension monitoring (regime/therapy)"
* expansion.contains[0].contains[2].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* expansion.contains[0].contains[2].code = #473199000
* expansion.contains[0].contains[2].display = "Telehealth monitoring for chronic disease (regime/therapy)"
* expansion.contains[0].contains[2].contains[0].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* expansion.contains[0].contains[2].contains[0].code = #716358000
* expansion.contains[0].contains[2].contains[0].display = "Telehealth chronic obstructive pulmonary disease monitoring (regime/therapy)"
* expansion.contains[0].contains[3].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* expansion.contains[0].contains[3].code = #879780004
* expansion.contains[0].contains[3].display = "Telehealth monitoring for chronic heart failure (regime/therapy)"
* expansion.contains[0].contains[4].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* expansion.contains[0].contains[4].code = #879782007
* expansion.contains[0].contains[4].display = "Telehealth monitoring for diabetes mellitus (regime/therapy)"
* expansion.contains[0].contains[5].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* expansion.contains[0].contains[5].code = #879781000
* expansion.contains[0].contains[5].display = "Telehealth monitoring for ischemic heart disease (regime/therapy)"
* expansion.contains[0].contains[6].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* expansion.contains[0].contains[6].code = #879783002
* expansion.contains[0].contains[6].display = "Telehealth monitoring for psychological stress (regime/therapy)"
* expansion.contains[0].contains[7].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* expansion.contains[0].contains[7].code = #715279006
* expansion.contains[0].contains[7].display = "Telehealth obesity monitoring (regime/therapy)"
* expansion.contains[0].contains[8].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* expansion.contains[0].contains[8].code = #879784008
* expansion.contains[0].contains[8].display = "Telehealth rehabilitation monitoring (regime/therapy)"
