Instance: ucum 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ucum" 
* name = "ucum" 
* title = "UCUM" 
* status = #active 
* content = #fragment 
* version = "2005_202111" 
* description = "**Description:** Codes for structuring and laboratory parameters which are used in Austria, but where no official LOINC-Codes are existent. This codes are used within the Value Sets ELGA_Laborparameter and ELGA_Laborstruktur

**Beschreibung:** Codes für die Gliederung und Laborparameter, die in Österreich verwendet werden und für die es keinen LOINC gibt. Verwendung im ValueSet ELGA_Laborparameter und  ELGA_Laborstruktur" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.6.8" 
* date = "2021-11-29" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* caseSensitive = "true" 
* count = 415 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #01 "Most Common Healthcare Units"
* #01 ^property[0].code = #child 
* #01 ^property[0].valueCode = #% 
* #01 ^property[1].code = #child 
* #01 ^property[1].valueCode = #/uL 
* #01 ^property[2].code = #child 
* #01 ^property[2].valueCode = #10*3/uL 
* #01 ^property[3].code = #child 
* #01 ^property[3].valueCode = #10*6/uL 
* #01 ^property[4].code = #child 
* #01 ^property[4].valueCode = #U/L 
* #01 ^property[5].code = #child 
* #01 ^property[5].valueCode = #[iU]/L 
* #01 ^property[6].code = #child 
* #01 ^property[6].valueCode = #fL 
* #01 ^property[7].code = #child 
* #01 ^property[7].valueCode = #g/L 
* #01 ^property[8].code = #child 
* #01 ^property[8].valueCode = #g/dL 
* #01 ^property[9].code = #child 
* #01 ^property[9].valueCode = #g/mL 
* #01 ^property[10].code = #child 
* #01 ^property[10].valueCode = #kPa 
* #01 ^property[11].code = #child 
* #01 ^property[11].valueCode = #m[iU]/mL 
* #01 ^property[12].code = #child 
* #01 ^property[12].valueCode = #meq/L 
* #01 ^property[13].code = #child 
* #01 ^property[13].valueCode = #mg/dL 
* #01 ^property[14].code = #child 
* #01 ^property[14].valueCode = #mm[Hg] 
* #01 ^property[15].code = #child 
* #01 ^property[15].valueCode = #mmol/L 
* #01 ^property[16].code = #child 
* #01 ^property[16].valueCode = #mmol/kg 
* #01 ^property[17].code = #child 
* #01 ^property[17].valueCode = #mosm/kg 
* #01 ^property[18].code = #child 
* #01 ^property[18].valueCode = #ng/mL 
* #01 ^property[19].code = #child 
* #01 ^property[19].valueCode = #nmol/L 
* #01 ^property[20].code = #child 
* #01 ^property[20].valueCode = #pg 
* #01 ^property[21].code = #child 
* #01 ^property[21].valueCode = #pg/mL 
* #01 ^property[22].code = #child 
* #01 ^property[22].valueCode = #pmol/L 
* #01 ^property[23].code = #child 
* #01 ^property[23].valueCode = #u[iU]/mL 
* #01 ^property[24].code = #child 
* #01 ^property[24].valueCode = #ug/L 
* #01 ^property[25].code = #child 
* #01 ^property[25].valueCode = #ug/dL 
* #01 ^property[26].code = #child 
* #01 ^property[26].valueCode = #ug/mL 
* #01 ^property[27].code = #child 
* #01 ^property[27].valueCode = #umol/L 
* #% "Percent"
* #% ^property[0].code = #parent 
* #% ^property[0].valueCode = #01 
* #% ^property[1].code = #parent 
* #% ^property[1].valueCode = #03 
* #/uL "PerMicroLiter"
* #/uL ^property[0].code = #parent 
* #/uL ^property[0].valueCode = #01 
* #/uL ^property[1].code = #parent 
* #/uL ^property[1].valueCode = #41 
* #10*3/uL "ThousandsPerMicroLiter"
* #10*3/uL ^property[0].code = #parent 
* #10*3/uL ^property[0].valueCode = #01 
* #10*3/uL ^property[1].code = #parent 
* #10*3/uL ^property[1].valueCode = #41 
* #10*6/uL "MillionsPerMicroLiter"
* #10*6/uL ^property[0].code = #parent 
* #10*6/uL ^property[0].valueCode = #01 
* #10*6/uL ^property[1].code = #parent 
* #10*6/uL ^property[1].valueCode = #41 
* #U/L "UnitsPerLiter"
* #U/L ^property[0].code = #parent 
* #U/L ^property[0].valueCode = #01 
* #[iU]/L "InternationalUnitsPerLiter"
* #[iU]/L ^property[0].code = #parent 
* #[iU]/L ^property[0].valueCode = #01 
* #[iU]/L ^property[1].code = #parent 
* #[iU]/L ^property[1].valueCode = #44 
* #fL "FemtoLiter"
* #fL ^property[0].code = #parent 
* #fL ^property[0].valueCode = #01 
* #fL ^property[1].code = #parent 
* #fL ^property[1].valueCode = #28 
* #g/L "GramsPerLiter"
* #g/L ^property[0].code = #parent 
* #g/L ^property[0].valueCode = #01 
* #g/L ^property[1].code = #parent 
* #g/L ^property[1].valueCode = #42 
* #g/dL "GramsPerDeciLiter"
* #g/dL ^property[0].code = #parent 
* #g/dL ^property[0].valueCode = #01 
* #g/dL ^property[1].code = #parent 
* #g/dL ^property[1].valueCode = #42 
* #g/mL "GramsPerMilliLiter"
* #g/mL ^property[0].code = #parent 
* #g/mL ^property[0].valueCode = #01 
* #g/mL ^property[1].code = #parent 
* #g/mL ^property[1].valueCode = #42 
* #kPa "KiloPascal"
* #kPa ^property[0].code = #parent 
* #kPa ^property[0].valueCode = #01 
* #m[iU]/mL "MilliInternationalUnitsPerMilliLiter"
* #m[iU]/mL ^property[0].code = #parent 
* #m[iU]/mL ^property[0].valueCode = #01 
* #m[iU]/mL ^property[1].code = #parent 
* #m[iU]/mL ^property[1].valueCode = #44 
* #meq/L "MilliEquivalentsPerLiter"
* #meq/L ^property[0].code = #parent 
* #meq/L ^property[0].valueCode = #01 
* #meq/L ^property[1].code = #parent 
* #meq/L ^property[1].valueCode = #43 
* #mg/dL "MilliGramsPerDeciLiter"
* #mg/dL ^property[0].code = #parent 
* #mg/dL ^property[0].valueCode = #01 
* #mg/dL ^property[1].code = #parent 
* #mg/dL ^property[1].valueCode = #42 
* #mm[Hg] "MilliMetersOfMercury"
* #mm[Hg] ^property[0].code = #parent 
* #mm[Hg] ^property[0].valueCode = #01 
* #mm[Hg] ^property[1].code = #parent 
* #mm[Hg] ^property[1].valueCode = #100 
* #mmol/L "MilliMolesPerLiter"
* #mmol/L ^property[0].code = #parent 
* #mmol/L ^property[0].valueCode = #01 
* #mmol/L ^property[1].code = #parent 
* #mmol/L ^property[1].valueCode = #43 
* #mmol/kg "MilliMolesPerKiloGram"
* #mmol/kg ^property[0].code = #parent 
* #mmol/kg ^property[0].valueCode = #01 
* #mmol/kg ^property[1].code = #parent 
* #mmol/kg ^property[1].valueCode = #31 
* #mosm/kg "MilliOsmolesPerKiloGram"
* #mosm/kg ^property[0].code = #parent 
* #mosm/kg ^property[0].valueCode = #01 
* #mosm/kg ^property[1].code = #parent 
* #mosm/kg ^property[1].valueCode = #32 
* #ng/mL "NanoGramsPerMilliLiter"
* #ng/mL ^property[0].code = #parent 
* #ng/mL ^property[0].valueCode = #01 
* #ng/mL ^property[1].code = #parent 
* #ng/mL ^property[1].valueCode = #42 
* #nmol/L "NanoMolesPerLiter"
* #nmol/L ^property[0].code = #parent 
* #nmol/L ^property[0].valueCode = #01 
* #nmol/L ^property[1].code = #parent 
* #nmol/L ^property[1].valueCode = #43 
* #pg "PicoGrams"
* #pg ^property[0].code = #parent 
* #pg ^property[0].valueCode = #01 
* #pg ^property[1].code = #parent 
* #pg ^property[1].valueCode = #18 
* #pg/mL "PicoGramsPerMilliLiter"
* #pg/mL ^property[0].code = #parent 
* #pg/mL ^property[0].valueCode = #01 
* #pg/mL ^property[1].code = #parent 
* #pg/mL ^property[1].valueCode = #42 
* #pmol/L "PicoMolesPerLiter"
* #pmol/L ^property[0].code = #parent 
* #pmol/L ^property[0].valueCode = #43 
* #u[iU]/mL "MicroInternationalUnitsPerMilliLiter"
* #u[iU]/mL ^property[0].code = #parent 
* #u[iU]/mL ^property[0].valueCode = #01 
* #u[iU]/mL ^property[1].code = #parent 
* #u[iU]/mL ^property[1].valueCode = #44 
* #ug/L "MicroGramsPerLiter"
* #ug/L ^property[0].code = #parent 
* #ug/L ^property[0].valueCode = #01 
* #ug/L ^property[1].code = #parent 
* #ug/L ^property[1].valueCode = #42 
* #ug/dL "MicroGramsPerDeciLiter"
* #ug/dL ^property[0].code = #parent 
* #ug/dL ^property[0].valueCode = #01 
* #ug/dL ^property[1].code = #parent 
* #ug/dL ^property[1].valueCode = #42 
* #ug/mL "MicroGramsPerMilliLiter"
* #ug/mL ^property[0].code = #parent 
* #ug/mL ^property[0].valueCode = #01 
* #ug/mL ^property[1].code = #parent 
* #ug/mL ^property[1].valueCode = #42 
* #umol/L "MicroMolesPerLiter"
* #umol/L ^property[0].code = #parent 
* #umol/L ^property[0].valueCode = #01 
* #umol/L ^property[1].code = #parent 
* #umol/L ^property[1].valueCode = #43 
* #02 "Unity"
* #02 ^property[0].code = #child 
* #02 ^property[0].valueCode = #/{tot} 
* #02 ^property[1].code = #child 
* #02 ^property[1].valueCode = #10*3 
* #02 ^property[2].code = #child 
* #02 ^property[2].valueCode = #10*3.{RBC} 
* #02 ^property[3].code = #child 
* #02 ^property[3].valueCode = #10*5 
* #02 ^property[4].code = #child 
* #02 ^property[4].valueCode = #10*6 
* #02 ^property[5].code = #child 
* #02 ^property[5].valueCode = #10*6/{Specimen} 
* #02 ^property[6].code = #child 
* #02 ^property[6].valueCode = #10*8 
* #02 ^property[7].code = #child 
* #02 ^property[7].valueCode = #[lg] 
* #/{tot} "PerTotalCount"
* #/{tot} ^property[0].code = #parent 
* #/{tot} ^property[0].valueCode = #02 
* #10*3 "Thousand"
* #10*3 ^property[0].code = #parent 
* #10*3 ^property[0].valueCode = #02 
* #10*3.{RBC} "ThousandRedBloodCells"
* #10*3.{RBC} ^property[0].code = #parent 
* #10*3.{RBC} ^property[0].valueCode = #02 
* #10*5 "OneHundredThousand"
* #10*5 ^property[0].code = #parent 
* #10*5 ^property[0].valueCode = #02 
* #10*6 "Million"
* #10*6 ^property[0].code = #parent 
* #10*6 ^property[0].valueCode = #02 
* #10*6/{Specimen} "MillionPerSpecimen"
* #10*6/{Specimen} ^property[0].code = #parent 
* #10*6/{Specimen} ^property[0].valueCode = #02 
* #10*8 "TenToEighth"
* #10*8 ^property[0].code = #parent 
* #10*8 ^property[0].valueCode = #02 
* #[lg] "Log10"
* #[lg] ^property[0].code = #parent 
* #[lg] ^property[0].valueCode = #02 
* #03 "General Fraction Unit"
* #03 ^property[0].code = #child 
* #03 ^property[0].valueCode = #% 
* #03 ^property[1].code = #child 
* #03 ^property[1].valueCode = #%{0to3Hours} 
* #03 ^property[2].code = #child 
* #03 ^property[2].valueCode = #%{Total} 
* #03 ^property[3].code = #child 
* #03 ^property[3].valueCode = #{Relative}% 
* #%{0to3Hours} "Percent0to3Hours"
* #%{0to3Hours} ^property[0].code = #parent 
* #%{0to3Hours} ^property[0].valueCode = #03 
* #%{Total} "PercentTotal"
* #%{Total} ^property[0].code = #parent 
* #%{Total} ^property[0].valueCode = #03 
* #{Relative}% "RelativePercent"
* #{Relative}% ^property[0].code = #parent 
* #{Relative}% ^property[0].valueCode = #03 
* #04 "Number Fraction Units"
* #04 ^property[0].code = #child 
* #04 ^property[0].valueCode = #%/100{WBC} 
* #04 ^property[1].code = #child 
* #04 ^property[1].valueCode = #%{Abnormal} 
* #04 ^property[2].code = #child 
* #04 ^property[2].valueCode = #%{Blockade} 
* #04 ^property[3].code = #child 
* #04 ^property[3].valueCode = #%{EosSeen} 
* #04 ^property[4].code = #child 
* #04 ^property[4].valueCode = #%{FetalErythrocytes} 
* #04 ^property[5].code = #child 
* #04 ^property[5].valueCode = #%{Hemolysis} 
* #04 ^property[6].code = #child 
* #04 ^property[6].valueCode = #%{Normal} 
* #04 ^property[7].code = #child 
* #04 ^property[7].valueCode = #%{OfLymphocytes} 
* #04 ^property[8].code = #child 
* #04 ^property[8].valueCode = #%{OfWBCs} 
* #04 ^property[9].code = #child 
* #04 ^property[9].valueCode = #%{Positive} 
* #04 ^property[10].code = #child 
* #04 ^property[10].valueCode = #%{SpermMotility} 
* #04 ^property[11].code = #child 
* #04 ^property[11].valueCode = #%{ofBacteria} 
* #04 ^property[12].code = #child 
* #04 ^property[12].valueCode = #/10*10 
* #04 ^property[13].code = #child 
* #04 ^property[13].valueCode = #/10*12 
* #04 ^property[14].code = #child 
* #04 ^property[14].valueCode = #/10*6 
* #04 ^property[15].code = #child 
* #04 ^property[15].valueCode = #/10*9 
* #%/100{WBC} "PercentPer100WBC"
* #%/100{WBC} ^property[0].code = #parent 
* #%/100{WBC} ^property[0].valueCode = #04 
* #%{Abnormal} "PercentAbnormal"
* #%{Abnormal} ^property[0].code = #parent 
* #%{Abnormal} ^property[0].valueCode = #04 
* #%{Blockade} "PercentBlockade"
* #%{Blockade} ^property[0].code = #parent 
* #%{Blockade} ^property[0].valueCode = #04 
* #%{EosSeen} "PercentEosinophilsSeen"
* #%{EosSeen} ^property[0].code = #parent 
* #%{EosSeen} ^property[0].valueCode = #04 
* #%{FetalErythrocytes} "PercentFetalErythrocytes"
* #%{FetalErythrocytes} ^property[0].code = #parent 
* #%{FetalErythrocytes} ^property[0].valueCode = #04 
* #%{Hemolysis} "PercentHemolysis"
* #%{Hemolysis} ^property[0].code = #parent 
* #%{Hemolysis} ^property[0].valueCode = #04 
* #%{Normal} "PercentNormal"
* #%{Normal} ^property[0].code = #parent 
* #%{Normal} ^property[0].valueCode = #04 
* #%{OfLymphocytes} "PercentOfLymphocytes"
* #%{OfLymphocytes} ^property[0].code = #parent 
* #%{OfLymphocytes} ^property[0].valueCode = #04 
* #%{OfWBCs} "PercentOfWBCs"
* #%{OfWBCs} ^property[0].code = #parent 
* #%{OfWBCs} ^property[0].valueCode = #04 
* #%{Positive} "PercentPositive"
* #%{Positive} ^property[0].code = #parent 
* #%{Positive} ^property[0].valueCode = #04 
* #%{SpermMotility} "PercentSpermMotility"
* #%{SpermMotility} ^property[0].code = #parent 
* #%{SpermMotility} ^property[0].valueCode = #04 
* #%{ofBacteria} "PercentofBacteria"
* #%{ofBacteria} ^property[0].code = #parent 
* #%{ofBacteria} ^property[0].valueCode = #04 
* #/10*10 "PerTenGiga"
* #/10*10 ^property[0].code = #parent 
* #/10*10 ^property[0].valueCode = #04 
* #/10*12 "PerTrillion"
* #/10*12 ^property[0].code = #parent 
* #/10*12 ^property[0].valueCode = #04 
* #/10*6 "PerMillion"
* #/10*6 ^property[0].code = #parent 
* #/10*6 ^property[0].valueCode = #04 
* #/10*9 "PerBillion"
* #/10*9 ^property[0].code = #parent 
* #/10*9 ^property[0].valueCode = #04 
* #05 "Mass Or Substance Fraction Units"
* #05 ^property[0].code = #child 
* #05 ^property[0].valueCode = #%{Binding} 
* #05 ^property[1].code = #child 
* #05 ^property[1].valueCode = #%{Bound} 
* #05 ^property[2].code = #child 
* #05 ^property[2].valueCode = #%{Carboxyhemoglobin} 
* #05 ^property[3].code = #child 
* #05 ^property[3].valueCode = #%{HemoglobinA1C} 
* #05 ^property[4].code = #child 
* #05 ^property[4].valueCode = #%{HemoglobinSaturation} 
* #05 ^property[5].code = #child 
* #05 ^property[5].valueCode = #%{Hemoglobin} 
* #05 ^property[6].code = #child 
* #05 ^property[6].valueCode = #%{TotalProtein} 
* #%{Binding} "PercentBinding"
* #%{Binding} ^property[0].code = #parent 
* #%{Binding} ^property[0].valueCode = #05 
* #%{Bound} "PercentBound"
* #%{Bound} ^property[0].code = #parent 
* #%{Bound} ^property[0].valueCode = #05 
* #%{Carboxyhemoglobin} "PercentCarboxyhemoglobin"
* #%{Carboxyhemoglobin} ^property[0].code = #parent 
* #%{Carboxyhemoglobin} ^property[0].valueCode = #05 
* #%{HemoglobinA1C} "PercentHemoglobinA1C"
* #%{HemoglobinA1C} ^property[0].code = #parent 
* #%{HemoglobinA1C} ^property[0].valueCode = #05 
* #%{HemoglobinSaturation} "PercentHemoglobinSaturation"
* #%{HemoglobinSaturation} ^property[0].code = #parent 
* #%{HemoglobinSaturation} ^property[0].valueCode = #05 
* #%{Hemoglobin} "PercentHemoglobin"
* #%{Hemoglobin} ^property[0].code = #parent 
* #%{Hemoglobin} ^property[0].valueCode = #05 
* #%{TotalProtein} "PercentTotalProtein"
* #%{TotalProtein} ^property[0].code = #parent 
* #%{TotalProtein} ^property[0].valueCode = #05 
* #06 "Mass Or Substance Rate Fraction Units"
* #06 ^property[0].code = #child 
* #06 ^property[0].valueCode = #%{Excretion} 
* #06 ^property[1].code = #child 
* #06 ^property[1].valueCode = #%{Uptake} 
* #%{Excretion} "PercentExcretion"
* #%{Excretion} ^property[0].code = #parent 
* #%{Excretion} ^property[0].valueCode = #06 
* #%{Uptake} "PercentUptake"
* #%{Uptake} ^property[0].code = #parent 
* #%{Uptake} ^property[0].valueCode = #06 
* #07 "Mass Ratio Or Mass Fraction Or Mass Content Units"
* #07 ^property[0].code = #child 
* #07 ^property[0].valueCode = #g/[100]g 
* #07 ^property[1].code = #child 
* #07 ^property[1].valueCode = #g/g 
* #07 ^property[2].code = #child 
* #07 ^property[2].valueCode = #g/g{Cre} 
* #07 ^property[3].code = #child 
* #07 ^property[3].valueCode = #g/kg 
* #07 ^property[4].code = #child 
* #07 ^property[4].valueCode = #mg/g 
* #07 ^property[5].code = #child 
* #07 ^property[5].valueCode = #mg/g{Cre} 
* #07 ^property[6].code = #child 
* #07 ^property[6].valueCode = #mg/kg 
* #07 ^property[7].code = #child 
* #07 ^property[7].valueCode = #mg/mg 
* #07 ^property[8].code = #child 
* #07 ^property[8].valueCode = #mg/mg{Cre} 
* #07 ^property[9].code = #child 
* #07 ^property[9].valueCode = #ng/g 
* #07 ^property[10].code = #child 
* #07 ^property[10].valueCode = #ng/g{Cre} 
* #07 ^property[11].code = #child 
* #07 ^property[11].valueCode = #ng/kg 
* #07 ^property[12].code = #child 
* #07 ^property[12].valueCode = #ng/mg 
* #07 ^property[13].code = #child 
* #07 ^property[13].valueCode = #ng/mg{Protein} 
* #07 ^property[14].code = #child 
* #07 ^property[14].valueCode = #ug/[100]g 
* #07 ^property[15].code = #child 
* #07 ^property[15].valueCode = #ug/g 
* #07 ^property[16].code = #child 
* #07 ^property[16].valueCode = #ug/g{Cre} 
* #07 ^property[17].code = #child 
* #07 ^property[17].valueCode = #ug/g{DryWeight} 
* #07 ^property[18].code = #child 
* #07 ^property[18].valueCode = #ug/g{Hgb} 
* #07 ^property[19].code = #child 
* #07 ^property[19].valueCode = #ug/kg 
* #07 ^property[20].code = #child 
* #07 ^property[20].valueCode = #ug/mg 
* #07 ^property[21].code = #child 
* #07 ^property[21].valueCode = #ug/mg{Cre} 
* #07 ^property[22].code = #child 
* #07 ^property[22].valueCode = #ug/ng 
* #g/[100]g "GramsPer100Gram"
* #g/[100]g ^property[0].code = #parent 
* #g/[100]g ^property[0].valueCode = #07 
* #g/g "GramsPerGram"
* #g/g ^property[0].code = #parent 
* #g/g ^property[0].valueCode = #07 
* #g/g{Cre} "GramsPerGramCreatinine"
* #g/g{Cre} ^property[0].code = #parent 
* #g/g{Cre} ^property[0].valueCode = #07 
* #g/kg "GramsPerKiloGram"
* #g/kg ^property[0].code = #parent 
* #g/kg ^property[0].valueCode = #07 
* #mg/g "MilliGramsPerGram"
* #mg/g ^property[0].code = #parent 
* #mg/g ^property[0].valueCode = #07 
* #mg/g{Cre} "MilliGramPerGramCreatinine"
* #mg/g{Cre} ^property[0].code = #parent 
* #mg/g{Cre} ^property[0].valueCode = #07 
* #mg/kg "MilliGramsPerKiloGram"
* #mg/kg ^property[0].code = #parent 
* #mg/kg ^property[0].valueCode = #07 
* #mg/mg "MilliGramsPerMilliGram"
* #mg/mg ^property[0].code = #parent 
* #mg/mg ^property[0].valueCode = #07 
* #mg/mg{Cre} "MilligramsPerMilligramCreatinine"
* #mg/mg{Cre} ^property[0].code = #parent 
* #mg/mg{Cre} ^property[0].valueCode = #07 
* #ng/g "NanoGramsPerGram"
* #ng/g ^property[0].code = #parent 
* #ng/g ^property[0].valueCode = #07 
* #ng/g{Cre} "NanoGramsPerGramCreatinine"
* #ng/g{Cre} ^property[0].code = #parent 
* #ng/g{Cre} ^property[0].valueCode = #07 
* #ng/kg "NanoGramsPerKiloGram"
* #ng/kg ^property[0].code = #parent 
* #ng/kg ^property[0].valueCode = #07 
* #ng/mg "NanoGramsPerMilliGram"
* #ng/mg ^property[0].code = #parent 
* #ng/mg ^property[0].valueCode = #07 
* #ng/mg{Protein} "NanoGramsPerMilliGramProtein"
* #ng/mg{Protein} ^property[0].code = #parent 
* #ng/mg{Protein} ^property[0].valueCode = #07 
* #ug/[100]g "MicroGramPer100Gram"
* #ug/[100]g ^property[0].code = #parent 
* #ug/[100]g ^property[0].valueCode = #07 
* #ug/g "MicroGramsPerGram"
* #ug/g ^property[0].code = #parent 
* #ug/g ^property[0].valueCode = #07 
* #ug/g{Cre} "MicroGramPerGramCreatinine"
* #ug/g{Cre} ^property[0].code = #parent 
* #ug/g{Cre} ^property[0].valueCode = #07 
* #ug/g{DryWeight} "MicroGramPerGramDryWeight"
* #ug/g{DryWeight} ^property[0].code = #parent 
* #ug/g{DryWeight} ^property[0].valueCode = #07 
* #ug/g{Hgb} "MicroGramsPerGramHemoglobin"
* #ug/g{Hgb} ^property[0].code = #parent 
* #ug/g{Hgb} ^property[0].valueCode = #07 
* #ug/kg "MicroGramsPerKiloGram"
* #ug/kg ^property[0].code = #parent 
* #ug/kg ^property[0].valueCode = #07 
* #ug/mg "MicroGramsPerMilliGram"
* #ug/mg ^property[0].code = #parent 
* #ug/mg ^property[0].valueCode = #07 
* #ug/mg{Cre} "MicroGramsPerMilliGramCreatinine"
* #ug/mg{Cre} ^property[0].code = #parent 
* #ug/mg{Cre} ^property[0].valueCode = #07 
* #ug/ng "MicroGramsPerNanoGram"
* #ug/ng ^property[0].code = #parent 
* #ug/ng ^property[0].valueCode = #07 
* #08 "Substance Ratio Or Substance Fraction Units"
* #08 ^property[0].code = #child 
* #08 ^property[0].valueCode = #eq/mmol 
* #08 ^property[1].code = #child 
* #08 ^property[1].valueCode = #eq/umol 
* #08 ^property[2].code = #child 
* #08 ^property[2].valueCode = #mmol/mol 
* #08 ^property[3].code = #child 
* #08 ^property[3].valueCode = #mmol/mol{Cre} 
* #08 ^property[4].code = #child 
* #08 ^property[4].valueCode = #nmol/mmol 
* #08 ^property[5].code = #child 
* #08 ^property[5].valueCode = #nmol/mmol{Cre} 
* #08 ^property[6].code = #child 
* #08 ^property[6].valueCode = #nmol/mol 
* #08 ^property[7].code = #child 
* #08 ^property[7].valueCode = #pmol/umol 
* #08 ^property[8].code = #child 
* #08 ^property[8].valueCode = #umol/mol 
* #08 ^property[9].code = #child 
* #08 ^property[9].valueCode = #umol/mol{Cre} 
* #08 ^property[10].code = #child 
* #08 ^property[10].valueCode = #{BoneCollagen}eq/mmol{Cre} 
* #08 ^property[11].code = #child 
* #08 ^property[11].valueCode = #{BoneCollagen}eq/umol{Cre} 
* #eq/mmol "EquivalentsPerMilliMole"
* #eq/mmol ^property[0].code = #parent 
* #eq/mmol ^property[0].valueCode = #08 
* #eq/umol "EquivalentsPerMicroMole"
* #eq/umol ^property[0].code = #parent 
* #eq/umol ^property[0].valueCode = #08 
* #mmol/mol "MilliMolesPerMole"
* #mmol/mol ^property[0].code = #parent 
* #mmol/mol ^property[0].valueCode = #08 
* #mmol/mol{Cre} "MilliMolesPerMoleCreatinine"
* #mmol/mol{Cre} ^property[0].code = #parent 
* #mmol/mol{Cre} ^property[0].valueCode = #08 
* #nmol/mmol "NanoMolesPerMilliMole"
* #nmol/mmol ^property[0].code = #parent 
* #nmol/mmol ^property[0].valueCode = #08 
* #nmol/mmol{Cre} "NanoMolesPerMilliMoleCreatinine"
* #nmol/mmol{Cre} ^property[0].code = #parent 
* #nmol/mmol{Cre} ^property[0].valueCode = #08 
* #nmol/mol "NanoMolesPerMole"
* #nmol/mol ^property[0].code = #parent 
* #nmol/mol ^property[0].valueCode = #08 
* #pmol/umol "PicoMolesPerMicroMole"
* #pmol/umol ^property[0].code = #parent 
* #pmol/umol ^property[0].valueCode = #08 
* #umol/mol "MicroMolesPerMole"
* #umol/mol ^property[0].code = #parent 
* #umol/mol ^property[0].valueCode = #08 
* #umol/mol{Cre} "MicroMolesPerMoleCreatinine"
* #umol/mol{Cre} ^property[0].code = #parent 
* #umol/mol{Cre} ^property[0].valueCode = #08 
* #{BoneCollagen}eq/mmol{Cre} "BoneCollagenEquivalentsPerMilliMoleCreatinine"
* #{BoneCollagen}eq/mmol{Cre} ^property[0].code = #parent 
* #{BoneCollagen}eq/mmol{Cre} ^property[0].valueCode = #08 
* #{BoneCollagen}eq/umol{Cre} "BoneCollagenEquivalentsPerMicroMoleCreatinine"
* #{BoneCollagen}eq/umol{Cre} ^property[0].code = #parent 
* #{BoneCollagen}eq/umol{Cre} ^property[0].valueCode = #08 
* #09 "Volume Fraction Units"
* #09 ^property[0].code = #child 
* #09 ^property[0].valueCode = #%{Oxygen} 
* #09 ^property[1].code = #child 
* #09 ^property[1].valueCode = #%{vol} 
* #09 ^property[2].code = #child 
* #09 ^property[2].valueCode = #mL/dL 
* #%{Oxygen} "PercentOxygen"
* #%{Oxygen} ^property[0].code = #parent 
* #%{Oxygen} ^property[0].valueCode = #09 
* #%{vol} "VolumePercent"
* #%{vol} ^property[0].code = #parent 
* #%{vol} ^property[0].valueCode = #09 
* #mL/dL "MilliLitersPerDeciLiter"
* #mL/dL ^property[0].code = #parent 
* #mL/dL ^property[0].valueCode = #09 
* #10 "Catalytic Fraction Or Arbitrary Fraction Units"
* #10 ^property[0].code = #child 
* #10 ^property[0].valueCode = #%{Activity} 
* #10 ^property[1].code = #child 
* #10 ^property[1].valueCode = #%{BasalActivity} 
* #10 ^property[2].code = #child 
* #10 ^property[2].valueCode = #%{Inhibition} 
* #10 ^property[3].code = #child 
* #10 ^property[3].valueCode = #%{NormalPooledPlasma} 
* #%{Activity} "PercentActivity"
* #%{Activity} ^property[0].code = #parent 
* #%{Activity} ^property[0].valueCode = #10 
* #%{BasalActivity} "PercentBasalActivity"
* #%{BasalActivity} ^property[0].code = #parent 
* #%{BasalActivity} ^property[0].valueCode = #10 
* #%{Inhibition} "PercentInhibition"
* #%{Inhibition} ^property[0].code = #parent 
* #%{Inhibition} ^property[0].valueCode = #10 
* #%{NormalPooledPlasma} "PercentNormalPooledPlasma"
* #%{NormalPooledPlasma} ^property[0].code = #parent 
* #%{NormalPooledPlasma} ^property[0].valueCode = #10 
* #100 "ELGA specific use"
* #100 ^definition = Einheiten, die fuer die Verwendung in ELGA benoetigt werden
* #100 ^property[0].code = #child 
* #100 ^property[0].valueCode = #/d 
* #100 ^property[1].code = #child 
* #100 ^property[1].valueCode = #/h 
* #100 ^property[2].code = #child 
* #100 ^property[2].valueCode = #/min 
* #100 ^property[3].code = #child 
* #100 ^property[3].valueCode = #/s 
* #100 ^property[4].code = #child 
* #100 ^property[4].valueCode = #1 
* #100 ^property[5].code = #child 
* #100 ^property[5].valueCode = #10*6[iU] 
* #100 ^property[6].code = #child 
* #100 ^property[6].valueCode = #GBq 
* #100 ^property[7].code = #child 
* #100 ^property[7].valueCode = #IE 
* #100 ^property[8].code = #child 
* #100 ^property[8].valueCode = #J 
* #100 ^property[9].code = #child 
* #100 ^property[9].valueCode = #L/s 
* #100 ^property[10].code = #child 
* #100 ^property[10].valueCode = #[iU]/{Broteinheiten} 
* #100 ^property[11].code = #child 
* #100 ^property[11].valueCode = #bar 
* #100 ^property[12].code = #child 
* #100 ^property[12].valueCode = #cal 
* #100 ^property[13].code = #child 
* #100 ^property[13].valueCode = #cal/d 
* #100 ^property[14].code = #child 
* #100 ^property[14].valueCode = #cal/g 
* #100 ^property[15].code = #child 
* #100 ^property[15].valueCode = #cal/h 
* #100 ^property[16].code = #child 
* #100 ^property[16].valueCode = #cal/mL 
* #100 ^property[17].code = #child 
* #100 ^property[17].valueCode = #cm3 
* #100 ^property[18].code = #child 
* #100 ^property[18].valueCode = #kJ 
* #100 ^property[19].code = #child 
* #100 ^property[19].valueCode = #k[iU] 
* #100 ^property[20].code = #child 
* #100 ^property[20].valueCode = #k[iU]/L 
* #100 ^property[21].code = #child 
* #100 ^property[21].valueCode = #kat 
* #100 ^property[22].code = #child 
* #100 ^property[22].valueCode = #kcal 
* #100 ^property[23].code = #child 
* #100 ^property[23].valueCode = #kcal/d 
* #100 ^property[24].code = #child 
* #100 ^property[24].valueCode = #kcal/g 
* #100 ^property[25].code = #child 
* #100 ^property[25].valueCode = #kcal/h 
* #100 ^property[26].code = #child 
* #100 ^property[26].valueCode = #kcal/mL 
* #100 ^property[27].code = #child 
* #100 ^property[27].valueCode = #mL/s 
* #100 ^property[28].code = #child 
* #100 ^property[28].valueCode = #mL{Konzentrat} 
* #100 ^property[29].code = #child 
* #100 ^property[29].valueCode = #m[iU] 
* #100 ^property[30].code = #child 
* #100 ^property[30].valueCode = #mbar 
* #100 ^property[31].code = #child 
* #100 ^property[31].valueCode = #mm[Hg] 
* #100 ^property[32].code = #child 
* #100 ^property[32].valueCode = #ug/(kg.min) 
* #100 ^property[33].code = #child 
* #100 ^property[33].valueCode = #ukat 
* #100 ^property[34].code = #child 
* #100 ^property[34].valueCode = #ukat/L 
* #100 ^property[35].code = #child 
* #100 ^property[35].valueCode = #ukat/mL 
* #100 ^property[36].code = #child 
* #100 ^property[36].valueCode = #{Ampulle} 
* #100 ^property[37].code = #child 
* #100 ^property[37].valueCode = #{Beutel/Aufgussbeutel} 
* #100 ^property[38].code = #child 
* #100 ^property[38].valueCode = #{Broteinheiten} 
* #100 ^property[39].code = #child 
* #100 ^property[39].valueCode = #{Essloeffel} 
* #100 ^property[40].code = #child 
* #100 ^property[40].valueCode = #{Flaschen} 
* #100 ^property[41].code = #child 
* #100 ^property[41].valueCode = #{Globuli} 
* #100 ^property[42].code = #child 
* #100 ^property[42].valueCode = #{Hub} 
* #100 ^property[43].code = #child 
* #100 ^property[43].valueCode = #{Messloeffel} 
* #100 ^property[44].code = #child 
* #100 ^property[44].valueCode = #{PackYears} 
* #100 ^property[45].code = #child 
* #100 ^property[45].valueCode = #{Packungsbeutel} 
* #100 ^property[46].code = #child 
* #100 ^property[46].valueCode = #{Packung} 
* #100 ^property[47].code = #child 
* #100 ^property[47].valueCode = #{Stueck} 
* #100 ^property[48].code = #child 
* #100 ^property[48].valueCode = #{Teeloeffel} 
* #100 ^property[49].code = #child 
* #100 ^property[49].valueCode = #{Tropfen} 
* #/d "PerDay"
* #/d ^property[0].code = #parent 
* #/d ^property[0].valueCode = #100 
* #/h "PerHour"
* #/h ^property[0].code = #parent 
* #/h ^property[0].valueCode = #100 
* #/min "PerMinute"
* #/min ^property[0].code = #parent 
* #/min ^property[0].valueCode = #100 
* #/s "PerSecond"
* #/s ^property[0].code = #parent 
* #/s ^property[0].valueCode = #100 
* #1 "Unit"
* #1 ^property[0].code = #parent 
* #1 ^property[0].valueCode = #100 
* #10*6[iU] "MillionInternationalUnits"
* #10*6[iU] ^property[0].code = #parent 
* #10*6[iU] ^property[0].valueCode = #100 
* #GBq "GBq"
* #GBq ^property[0].code = #parent 
* #GBq ^property[0].valueCode = #100 
* #IE "IE"
* #IE ^property[0].code = #parent 
* #IE ^property[0].valueCode = #100 
* #J "Joule"
* #J ^property[0].code = #parent 
* #J ^property[0].valueCode = #100 
* #L/s "LitersPerSecond"
* #L/s ^property[0].code = #parent 
* #L/s ^property[0].valueCode = #100 
* #[iU]/{Broteinheiten} "IE/BE"
* #[iU]/{Broteinheiten} ^property[0].code = #parent 
* #[iU]/{Broteinheiten} ^property[0].valueCode = #100 
* #bar "Bar"
* #bar ^property[0].code = #parent 
* #bar ^property[0].valueCode = #100 
* #cal "Calorie"
* #cal ^property[0].code = #parent 
* #cal ^property[0].valueCode = #100 
* #cal/d "CaloriesPerDay"
* #cal/d ^property[0].code = #parent 
* #cal/d ^property[0].valueCode = #100 
* #cal/g "CaloriesPerGram"
* #cal/g ^property[0].code = #parent 
* #cal/g ^property[0].valueCode = #100 
* #cal/h "CaloriesPerHour"
* #cal/h ^property[0].code = #parent 
* #cal/h ^property[0].valueCode = #100 
* #cal/mL "CaloriesPerMiliLiter"
* #cal/mL ^property[0].code = #parent 
* #cal/mL ^property[0].valueCode = #100 
* #cm3 "CubicCentimeter"
* #cm3 ^property[0].code = #parent 
* #cm3 ^property[0].valueCode = #100 
* #kJ "KiloJoule"
* #kJ ^property[0].code = #parent 
* #kJ ^property[0].valueCode = #100 
* #k[iU] "KiloInternationalUnits"
* #k[iU] ^property[0].code = #parent 
* #k[iU] ^property[0].valueCode = #100 
* #k[iU]/L "KiloInternationalUnitsPerLiter"
* #k[iU]/L ^property[0].code = #parent 
* #k[iU]/L ^property[0].valueCode = #100 
* #kat "Katal"
* #kat ^property[0].code = #parent 
* #kat ^property[0].valueCode = #100 
* #kcal "KiloCalorie"
* #kcal ^property[0].code = #parent 
* #kcal ^property[0].valueCode = #100 
* #kcal/d "KiloCaloriesPerDay"
* #kcal/d ^property[0].code = #parent 
* #kcal/d ^property[0].valueCode = #100 
* #kcal/g "KiloCaloriesPerGram"
* #kcal/g ^property[0].code = #parent 
* #kcal/g ^property[0].valueCode = #100 
* #kcal/h "KiloCaloriesPerHour"
* #kcal/h ^property[0].code = #parent 
* #kcal/h ^property[0].valueCode = #100 
* #kcal/mL "KiloCaloriesPerMililiter"
* #kcal/mL ^property[0].code = #parent 
* #kcal/mL ^property[0].valueCode = #100 
* #mL/s "MilliLitersPerSecond"
* #mL/s ^property[0].code = #parent 
* #mL/s ^property[0].valueCode = #100 
* #mL{Konzentrat} "MilliLiterKonzentrat"
* #mL{Konzentrat} ^property[0].code = #parent 
* #mL{Konzentrat} ^property[0].valueCode = #100 
* #m[iU] "MilliInternationalUnit"
* #m[iU] ^property[0].code = #parent 
* #m[iU] ^property[0].valueCode = #100 
* #mbar "MilliBar"
* #mbar ^property[0].code = #parent 
* #mbar ^property[0].valueCode = #100 
* #ug/(kg.min) "MicroGramsPerKiloGramAndMinute"
* #ug/(kg.min) ^property[0].code = #parent 
* #ug/(kg.min) ^property[0].valueCode = #100 
* #ukat "MicroKatal"
* #ukat ^property[0].code = #parent 
* #ukat ^property[0].valueCode = #100 
* #ukat/L "MicroKatalsPerLiter"
* #ukat/L ^property[0].code = #parent 
* #ukat/L ^property[0].valueCode = #100 
* #ukat/mL "MicroKatalsPerMilliLiter"
* #ukat/mL ^property[0].code = #parent 
* #ukat/mL ^property[0].valueCode = #100 
* #{Ampulle} "Ampulle(n)"
* #{Ampulle} ^property[0].code = #parent 
* #{Ampulle} ^property[0].valueCode = #100 
* #{Beutel/Aufgussbeutel} "Beutel/Aufgussbeutel"
* #{Beutel/Aufgussbeutel} ^property[0].code = #parent 
* #{Beutel/Aufgussbeutel} ^property[0].valueCode = #100 
* #{Broteinheiten} "Broteinheit(en)"
* #{Broteinheiten} ^property[0].code = #parent 
* #{Broteinheiten} ^property[0].valueCode = #100 
* #{Essloeffel} "Essloeffel"
* #{Essloeffel} ^property[0].code = #parent 
* #{Essloeffel} ^property[0].valueCode = #100 
* #{Flaschen} "Flasche(n)"
* #{Flaschen} ^property[0].code = #parent 
* #{Flaschen} ^property[0].valueCode = #100 
* #{Globuli} "Globuli"
* #{Globuli} ^property[0].code = #parent 
* #{Globuli} ^property[0].valueCode = #100 
* #{Hub} "Hub/Huebe"
* #{Hub} ^property[0].code = #parent 
* #{Hub} ^property[0].valueCode = #100 
* #{Messloeffel} "Messloeffel"
* #{Messloeffel} ^property[0].code = #parent 
* #{Messloeffel} ^property[0].valueCode = #100 
* #{PackYears} "Packungsjahre"
* #{PackYears} ^property[0].code = #parent 
* #{PackYears} ^property[0].valueCode = #100 
* #{Packungsbeutel} "Packungsbeutel"
* #{Packungsbeutel} ^property[0].code = #parent 
* #{Packungsbeutel} ^property[0].valueCode = #100 
* #{Packung} "Packung(en)"
* #{Packung} ^property[0].code = #parent 
* #{Packung} ^property[0].valueCode = #100 
* #{Stueck} "Stueck"
* #{Stueck} ^property[0].code = #parent 
* #{Stueck} ^property[0].valueCode = #100 
* #{Teeloeffel} "Teeloeffel"
* #{Teeloeffel} ^property[0].code = #parent 
* #{Teeloeffel} ^property[0].valueCode = #100 
* #{Tropfen} "Tropfen"
* #{Tropfen} ^property[0].code = #parent 
* #{Tropfen} ^property[0].valueCode = #100 
* #11 "Entitic Number Units"
* #11 ^property[0].code = #child 
* #11 ^property[0].valueCode = #/10*12{rbc} 
* #11 ^property[1].code = #child 
* #11 ^property[1].valueCode = #/100 
* #11 ^property[2].code = #child 
* #11 ^property[2].valueCode = #/100{Spermatozoa} 
* #11 ^property[3].code = #child 
* #11 ^property[3].valueCode = #/100{WBC} 
* #11 ^property[4].code = #child 
* #11 ^property[4].valueCode = #/{Entity} 
* #/10*12{rbc} "PerTrillionRedBloodCells"
* #/10*12{rbc} ^property[0].code = #parent 
* #/10*12{rbc} ^property[0].valueCode = #11 
* #/100 "Per100"
* #/100 ^property[0].code = #parent 
* #/100 ^property[0].valueCode = #11 
* #/100{Spermatozoa} "Per100Spermatozoa"
* #/100{Spermatozoa} ^property[0].code = #parent 
* #/100{Spermatozoa} ^property[0].valueCode = #11 
* #/100{WBC} "Per100WBC"
* #/100{WBC} ^property[0].code = #parent 
* #/100{WBC} ^property[0].valueCode = #11 
* #/{Entity} "PerEntity"
* #/{Entity} ^property[0].code = #parent 
* #/{Entity} ^property[0].valueCode = #11 
* #12 "Plane Angle Units"
* #12 ^property[0].code = #child 
* #12 ^property[0].valueCode = #deg 
* #deg "DegreesOfArc"
* #deg ^property[0].code = #parent 
* #deg ^property[0].valueCode = #12 
* #13 "Arbitrary Number Units"
* #13 ^property[0].code = #child 
* #13 ^property[0].valueCode = #/[arb`U] 
* #/[arb`U] "PerArbitraryUnit"
* #/[arb`U] ^property[0].code = #parent 
* #/[arb`U] ^property[0].valueCode = #13 
* #14 "Arbitrary Units"
* #14 ^property[0].code = #child 
* #14 ^property[0].valueCode = #10*6.[iU] 
* #14 ^property[1].code = #child 
* #14 ^property[1].valueCode = #[iU] 
* #14 ^property[2].code = #child 
* #14 ^property[2].valueCode = #u[iU] 
* #10*6.[iU] "MillionInternationalUnit"
* #10*6.[iU] ^property[0].code = #parent 
* #10*6.[iU] ^property[0].valueCode = #14 
* #[iU] "InternationalUnit"
* #[iU] ^property[0].code = #parent 
* #[iU] ^property[0].valueCode = #14 
* #u[iU] "MicroInternationalUnit"
* #u[iU] ^property[0].code = #parent 
* #u[iU] ^property[0].valueCode = #14 
* #15 "English Length Units"
* #15 ^property[0].code = #child 
* #15 ^property[0].valueCode = #[Ch] 
* #15 ^property[1].code = #child 
* #15 ^property[1].valueCode = #[ft_i] 
* #15 ^property[2].code = #child 
* #15 ^property[2].valueCode = #[fth_i] 
* #15 ^property[3].code = #child 
* #15 ^property[3].valueCode = #[in_i] 
* #15 ^property[4].code = #child 
* #15 ^property[4].valueCode = #[mi_i] 
* #15 ^property[5].code = #child 
* #15 ^property[5].valueCode = #[nmi_i] 
* #15 ^property[6].code = #child 
* #15 ^property[6].valueCode = #[yd_i] 
* #[Ch] "French"
* #[Ch] ^property[0].code = #parent 
* #[Ch] ^property[0].valueCode = #15 
* #[ft_i] "Feet"
* #[ft_i] ^property[0].code = #parent 
* #[ft_i] ^property[0].valueCode = #15 
* #[fth_i] "Fathom"
* #[fth_i] ^property[0].code = #parent 
* #[fth_i] ^property[0].valueCode = #15 
* #[in_i] "Inch"
* #[in_i] ^property[0].code = #parent 
* #[in_i] ^property[0].valueCode = #15 
* #[mi_i] "StatuteMile"
* #[mi_i] ^property[0].code = #parent 
* #[mi_i] ^property[0].valueCode = #15 
* #[nmi_i] "NauticalMile"
* #[nmi_i] ^property[0].code = #parent 
* #[nmi_i] ^property[0].valueCode = #15 
* #[yd_i] "Yard"
* #[yd_i] ^property[0].code = #parent 
* #[yd_i] ^property[0].valueCode = #15 
* #16 "SI Length Units"
* #16 ^property[0].code = #child 
* #16 ^property[0].valueCode = #cm 
* #16 ^property[1].code = #child 
* #16 ^property[1].valueCode = #dm 
* #16 ^property[2].code = #child 
* #16 ^property[2].valueCode = #fm 
* #16 ^property[3].code = #child 
* #16 ^property[3].valueCode = #km 
* #16 ^property[4].code = #child 
* #16 ^property[4].valueCode = #m 
* #16 ^property[5].code = #child 
* #16 ^property[5].valueCode = #mm 
* #16 ^property[6].code = #child 
* #16 ^property[6].valueCode = #nm 
* #16 ^property[7].code = #child 
* #16 ^property[7].valueCode = #pm 
* #16 ^property[8].code = #child 
* #16 ^property[8].valueCode = #um 
* #cm "CentiMeter"
* #cm ^property[0].code = #parent 
* #cm ^property[0].valueCode = #16 
* #dm "DeciMeter"
* #dm ^property[0].code = #parent 
* #dm ^property[0].valueCode = #16 
* #fm "FemtoMeter"
* #fm ^property[0].code = #parent 
* #fm ^property[0].valueCode = #16 
* #km "KiloMeter"
* #km ^property[0].code = #parent 
* #km ^property[0].valueCode = #16 
* #m "Meter"
* #m ^property[0].code = #parent 
* #m ^property[0].valueCode = #16 
* #mm "MilliMeter"
* #mm ^property[0].code = #parent 
* #mm ^property[0].valueCode = #16 
* #nm "NanoMeter"
* #nm ^property[0].code = #parent 
* #nm ^property[0].valueCode = #16 
* #pm "PicoMeter"
* #pm ^property[0].code = #parent 
* #pm ^property[0].valueCode = #16 
* #um "MicroMeter"
* #um ^property[0].code = #parent 
* #um ^property[0].valueCode = #16 
* #17 "English Mass Units"
* #17 ^property[0].code = #child 
* #17 ^property[0].valueCode = #[dr_av] 
* #17 ^property[1].code = #child 
* #17 ^property[1].valueCode = #[gr] 
* #17 ^property[2].code = #child 
* #17 ^property[2].valueCode = #[lb_av] 
* #17 ^property[3].code = #child 
* #17 ^property[3].valueCode = #[oz_av] 
* #17 ^property[4].code = #child 
* #17 ^property[4].valueCode = #[oz_tr] 
* #17 ^property[5].code = #child 
* #17 ^property[5].valueCode = #[ston_av] 
* #[dr_av] "Dram"
* #[dr_av] ^property[0].code = #parent 
* #[dr_av] ^property[0].valueCode = #17 
* #[gr] "Grain"
* #[gr] ^property[0].code = #parent 
* #[gr] ^property[0].valueCode = #17 
* #[lb_av] "Pound"
* #[lb_av] ^property[0].code = #parent 
* #[lb_av] ^property[0].valueCode = #17 
* #[oz_av] "Ounce"
* #[oz_av] ^property[0].code = #parent 
* #[oz_av] ^property[0].valueCode = #17 
* #[oz_tr] "TroyOunce"
* #[oz_tr] ^property[0].code = #parent 
* #[oz_tr] ^property[0].valueCode = #17 
* #[ston_av] "Ton"
* #[ston_av] ^property[0].code = #parent 
* #[ston_av] ^property[0].valueCode = #17 
* #18 "SI Mass Units"
* #18 ^property[0].code = #child 
* #18 ^property[0].valueCode = #cg 
* #18 ^property[1].code = #child 
* #18 ^property[1].valueCode = #dg 
* #18 ^property[2].code = #child 
* #18 ^property[2].valueCode = #fg 
* #18 ^property[3].code = #child 
* #18 ^property[3].valueCode = #g 
* #18 ^property[4].code = #child 
* #18 ^property[4].valueCode = #g/{TotalWeight} 
* #18 ^property[5].code = #child 
* #18 ^property[5].valueCode = #kg 
* #18 ^property[6].code = #child 
* #18 ^property[6].valueCode = #mg 
* #18 ^property[7].code = #child 
* #18 ^property[7].valueCode = #mg/{TotalVolume} 
* #18 ^property[8].code = #child 
* #18 ^property[8].valueCode = #mg/{Volume} 
* #18 ^property[9].code = #child 
* #18 ^property[9].valueCode = #ng 
* #18 ^property[10].code = #child 
* #18 ^property[10].valueCode = #pg 
* #18 ^property[11].code = #child 
* #18 ^property[11].valueCode = #t 
* #18 ^property[12].code = #child 
* #18 ^property[12].valueCode = #ug 
* #18 ^property[13].code = #child 
* #18 ^property[13].valueCode = #ug/{Specimen} 
* #18 ^property[14].code = #child 
* #18 ^property[14].valueCode = #ug/{TotalVolume} 
* #cg "CentiGram"
* #cg ^property[0].code = #parent 
* #cg ^property[0].valueCode = #18 
* #dg "DeciGram"
* #dg ^property[0].code = #parent 
* #dg ^property[0].valueCode = #18 
* #fg "FemtoGram"
* #fg ^property[0].code = #parent 
* #fg ^property[0].valueCode = #18 
* #g "Gram"
* #g ^property[0].code = #parent 
* #g ^property[0].valueCode = #18 
* #g/{TotalWeight} "GramsPerTotalWeight"
* #g/{TotalWeight} ^property[0].code = #parent 
* #g/{TotalWeight} ^property[0].valueCode = #18 
* #kg "KiloGram"
* #kg ^property[0].code = #parent 
* #kg ^property[0].valueCode = #18 
* #mg "MilliGram"
* #mg ^property[0].code = #parent 
* #mg ^property[0].valueCode = #18 
* #mg/{TotalVolume} "MilliGramPerTotalVolume"
* #mg/{TotalVolume} ^property[0].code = #parent 
* #mg/{TotalVolume} ^property[0].valueCode = #18 
* #mg/{Volume} "MilliGramsPerVolume"
* #mg/{Volume} ^property[0].code = #parent 
* #mg/{Volume} ^property[0].valueCode = #18 
* #ng "NanoGram"
* #ng ^property[0].code = #parent 
* #ng ^property[0].valueCode = #18 
* #t "MetricTon"
* #t ^property[0].code = #parent 
* #t ^property[0].valueCode = #18 
* #ug "MicroGram"
* #ug ^property[0].code = #parent 
* #ug ^property[0].valueCode = #18 
* #ug/{Specimen} "MicroGramsPerSpecimen"
* #ug/{Specimen} ^property[0].code = #parent 
* #ug/{Specimen} ^property[0].valueCode = #18 
* #ug/{TotalVolume} "MicroGramsPerTotalVolume"
* #ug/{TotalVolume} ^property[0].code = #parent 
* #ug/{TotalVolume} ^property[0].valueCode = #18 
* #19 "Lineic Mass Units"
* #19 ^property[0].code = #child 
* #19 ^property[0].valueCode = #g.m/({hb}.m2) 
* #19 ^property[1].code = #child 
* #19 ^property[1].valueCode = #pg/mm 
* #g.m/({hb}.m2) "GramMeterPerHeartbeatPerSquareMeter"
* #g.m/({hb}.m2) ^property[0].code = #parent 
* #g.m/({hb}.m2) ^property[0].valueCode = #19 
* #pg/mm "PicoGramsPerMilliMeter"
* #pg/mm ^property[0].code = #parent 
* #pg/mm ^property[0].valueCode = #19 
* #20 "Temperature Units"
* #20 ^property[0].code = #child 
* #20 ^property[0].valueCode = #Cel 
* #20 ^property[1].code = #child 
* #20 ^property[1].valueCode = #K 
* #20 ^property[2].code = #child 
* #20 ^property[2].valueCode = #[degF] 
* #Cel "DegreesCelsius"
* #Cel ^property[0].code = #parent 
* #Cel ^property[0].valueCode = #20 
* #K "DegreesKelvin"
* #K ^property[0].code = #parent 
* #K ^property[0].valueCode = #20 
* #[degF] "DegreesFahrenheit"
* #[degF] ^property[0].code = #parent 
* #[degF] ^property[0].valueCode = #20 
* #21 "Thermal Resistance Units"
* #21 ^property[0].code = #child 
* #21 ^property[0].valueCode = #K/W 
* #K/W "KelvinPerWatt"
* #K/W ^property[0].code = #parent 
* #K/W ^property[0].valueCode = #21 
* #22 "Time Units"
* #22 ^property[0].code = #child 
* #22 ^property[0].valueCode = #Ms 
* #22 ^property[1].code = #child 
* #22 ^property[1].valueCode = #a 
* #22 ^property[2].code = #child 
* #22 ^property[2].valueCode = #d 
* #22 ^property[3].code = #child 
* #22 ^property[3].valueCode = #h 
* #22 ^property[4].code = #child 
* #22 ^property[4].valueCode = #ks 
* #22 ^property[5].code = #child 
* #22 ^property[5].valueCode = #min 
* #22 ^property[6].code = #child 
* #22 ^property[6].valueCode = #mo 
* #22 ^property[7].code = #child 
* #22 ^property[7].valueCode = #ms 
* #22 ^property[8].code = #child 
* #22 ^property[8].valueCode = #ns 
* #22 ^property[9].code = #child 
* #22 ^property[9].valueCode = #ps 
* #22 ^property[10].code = #child 
* #22 ^property[10].valueCode = #s 
* #22 ^property[11].code = #child 
* #22 ^property[11].valueCode = #us 
* #22 ^property[12].code = #child 
* #22 ^property[12].valueCode = #wk 
* #Ms "Megasecond"
* #Ms ^property[0].code = #parent 
* #Ms ^property[0].valueCode = #22 
* #a "Year"
* #a ^property[0].code = #parent 
* #a ^property[0].valueCode = #22 
* #d "Day"
* #d ^property[0].code = #parent 
* #d ^property[0].valueCode = #22 
* #h "Hour"
* #h ^property[0].code = #parent 
* #h ^property[0].valueCode = #22 
* #ks "KiloSecond"
* #ks ^property[0].code = #parent 
* #ks ^property[0].valueCode = #22 
* #min "Minute"
* #min ^property[0].code = #parent 
* #min ^property[0].valueCode = #22 
* #mo "Month"
* #mo ^property[0].code = #parent 
* #mo ^property[0].valueCode = #22 
* #ms "MilliSecond"
* #ms ^property[0].code = #parent 
* #ms ^property[0].valueCode = #22 
* #ns "NanoSecond"
* #ns ^property[0].code = #parent 
* #ns ^property[0].valueCode = #22 
* #ps "PicoSecond"
* #ps ^property[0].code = #parent 
* #ps ^property[0].valueCode = #22 
* #s "Second"
* #s ^property[0].code = #parent 
* #s ^property[0].valueCode = #22 
* #us "MicroSecond"
* #us ^property[0].code = #parent 
* #us ^property[0].valueCode = #22 
* #wk "Week"
* #wk ^property[0].code = #parent 
* #wk ^property[0].valueCode = #22 
* #23 "Substance Units"
* #23 ^property[0].code = #child 
* #23 ^property[0].valueCode = #eq 
* #23 ^property[1].code = #child 
* #23 ^property[1].valueCode = #fmol 
* #23 ^property[2].code = #child 
* #23 ^property[2].valueCode = #meq 
* #23 ^property[3].code = #child 
* #23 ^property[3].valueCode = #meq/{Specimen} 
* #23 ^property[4].code = #child 
* #23 ^property[4].valueCode = #mmol 
* #23 ^property[5].code = #child 
* #23 ^property[5].valueCode = #mmol/{TotalVolume} 
* #23 ^property[6].code = #child 
* #23 ^property[6].valueCode = #mol 
* #23 ^property[7].code = #child 
* #23 ^property[7].valueCode = #mosm 
* #23 ^property[8].code = #child 
* #23 ^property[8].valueCode = #nmol 
* #23 ^property[9].code = #child 
* #23 ^property[9].valueCode = #pmol 
* #23 ^property[10].code = #child 
* #23 ^property[10].valueCode = #ueq 
* #23 ^property[11].code = #child 
* #23 ^property[11].valueCode = #umol 
* #eq "Equivalent"
* #eq ^property[0].code = #parent 
* #eq ^property[0].valueCode = #23 
* #fmol "Femtomole"
* #fmol ^property[0].code = #parent 
* #fmol ^property[0].valueCode = #23 
* #meq "MilliEquivalent"
* #meq ^property[0].code = #parent 
* #meq ^property[0].valueCode = #23 
* #meq/{Specimen} "MilliEquivalentsPerSpecimen"
* #meq/{Specimen} ^property[0].code = #parent 
* #meq/{Specimen} ^property[0].valueCode = #23 
* #mmol "MilliMole"
* #mmol ^property[0].code = #parent 
* #mmol ^property[0].valueCode = #23 
* #mmol/{TotalVolume} "MilliMolesPerTotalVolume"
* #mmol/{TotalVolume} ^property[0].code = #parent 
* #mmol/{TotalVolume} ^property[0].valueCode = #23 
* #mol "Mole"
* #mol ^property[0].code = #parent 
* #mol ^property[0].valueCode = #23 
* #mosm "MilliOsmole"
* #mosm ^property[0].code = #parent 
* #mosm ^property[0].valueCode = #23 
* #nmol "NanoMole"
* #nmol ^property[0].code = #parent 
* #nmol ^property[0].valueCode = #23 
* #pmol "PicoMole"
* #pmol ^property[0].code = #parent 
* #pmol ^property[0].valueCode = #23 
* #ueq "MicroEquivalent"
* #ueq ^property[0].code = #parent 
* #ueq ^property[0].valueCode = #23 
* #umol "MicroMole"
* #umol ^property[0].code = #parent 
* #umol ^property[0].valueCode = #23 
* #24 "Areic Substance Units"
* #24 ^property[0].code = #child 
* #24 ^property[0].valueCode = #meq/m2 
* #24 ^property[1].code = #child 
* #24 ^property[1].valueCode = #mmol/m2 
* #meq/m2 "MilliEquivalentsPerSquareMeter"
* #meq/m2 ^property[0].code = #parent 
* #meq/m2 ^property[0].valueCode = #24 
* #mmol/m2 "MilliMolesPerSquareMeter"
* #mmol/m2 ^property[0].code = #parent 
* #mmol/m2 ^property[0].valueCode = #24 
* #25 "English Area Units"
* #25 ^property[0].code = #child 
* #25 ^property[0].valueCode = #[sft_i] 
* #25 ^property[1].code = #child 
* #25 ^property[1].valueCode = #[sin_i] 
* #25 ^property[2].code = #child 
* #25 ^property[2].valueCode = #[syd_i] 
* #[sft_i] "SquareFeet"
* #[sft_i] ^property[0].code = #parent 
* #[sft_i] ^property[0].valueCode = #25 
* #[sin_i] "SquareInch"
* #[sin_i] ^property[0].code = #parent 
* #[sin_i] ^property[0].valueCode = #25 
* #[syd_i] "SquareYard"
* #[syd_i] ^property[0].code = #parent 
* #[syd_i] ^property[0].valueCode = #25 
* #26 "SI Area Units"
* #26 ^property[0].code = #child 
* #26 ^property[0].valueCode = #cm2 
* #26 ^property[1].code = #child 
* #26 ^property[1].valueCode = #m2 
* #26 ^property[2].code = #child 
* #26 ^property[2].valueCode = #mm2 
* #cm2 "SquareCentiMeter"
* #cm2 ^property[0].code = #parent 
* #cm2 ^property[0].valueCode = #26 
* #m2 "SquareMeter"
* #m2 ^property[0].code = #parent 
* #m2 ^property[0].valueCode = #26 
* #mm2 "SquareMilliMeter"
* #mm2 ^property[0].code = #parent 
* #mm2 ^property[0].valueCode = #26 
* #27 "English Volume Units"
* #27 ^property[0].code = #child 
* #27 ^property[0].valueCode = #[cin_i] 
* #27 ^property[1].code = #child 
* #27 ^property[1].valueCode = #[cup_us] 
* #27 ^property[2].code = #child 
* #27 ^property[2].valueCode = #[fdr_us] 
* #27 ^property[3].code = #child 
* #27 ^property[3].valueCode = #[foz_us] 
* #27 ^property[4].code = #child 
* #27 ^property[4].valueCode = #[gal_us] 
* #27 ^property[5].code = #child 
* #27 ^property[5].valueCode = #[pt_us] 
* #27 ^property[6].code = #child 
* #27 ^property[6].valueCode = #[qt_us] 
* #[cin_i] "CubicInch"
* #[cin_i] ^property[0].code = #parent 
* #[cin_i] ^property[0].valueCode = #27 
* #[cup_us] "Cup"
* #[cup_us] ^property[0].code = #parent 
* #[cup_us] ^property[0].valueCode = #27 
* #[fdr_us] "FluidDram"
* #[fdr_us] ^property[0].code = #parent 
* #[fdr_us] ^property[0].valueCode = #27 
* #[foz_us] "FluidOunce"
* #[foz_us] ^property[0].code = #parent 
* #[foz_us] ^property[0].valueCode = #27 
* #[gal_us] "Gallon"
* #[gal_us] ^property[0].code = #parent 
* #[gal_us] ^property[0].valueCode = #27 
* #[pt_us] "Pint"
* #[pt_us] ^property[0].code = #parent 
* #[pt_us] ^property[0].valueCode = #27 
* #[qt_us] "Quart"
* #[qt_us] ^property[0].code = #parent 
* #[qt_us] ^property[0].valueCode = #27 
* #28 "SI Volume Units"
* #28 ^property[0].code = #child 
* #28 ^property[0].valueCode = #L 
* #28 ^property[1].code = #child 
* #28 ^property[1].valueCode = #cL 
* #28 ^property[2].code = #child 
* #28 ^property[2].valueCode = #dL 
* #28 ^property[3].code = #child 
* #28 ^property[3].valueCode = #fL 
* #28 ^property[4].code = #child 
* #28 ^property[4].valueCode = #hL 
* #28 ^property[5].code = #child 
* #28 ^property[5].valueCode = #kL 
* #28 ^property[6].code = #child 
* #28 ^property[6].valueCode = #mL 
* #28 ^property[7].code = #child 
* #28 ^property[7].valueCode = #mL/{h`b} 
* #28 ^property[8].code = #child 
* #28 ^property[8].valueCode = #nL 
* #28 ^property[9].code = #child 
* #28 ^property[9].valueCode = #pL 
* #28 ^property[10].code = #child 
* #28 ^property[10].valueCode = #uL 
* #L "Liter"
* #L ^property[0].code = #parent 
* #L ^property[0].valueCode = #28 
* #cL "CentiLiter"
* #cL ^property[0].code = #parent 
* #cL ^property[0].valueCode = #28 
* #dL "DeciLiter"
* #dL ^property[0].code = #parent 
* #dL ^property[0].valueCode = #28 
* #hL "HectoLiter"
* #hL ^property[0].code = #parent 
* #hL ^property[0].valueCode = #28 
* #kL "KiloLiter"
* #kL ^property[0].code = #parent 
* #kL ^property[0].valueCode = #28 
* #mL "MilliLiter"
* #mL ^property[0].code = #parent 
* #mL ^property[0].valueCode = #28 
* #mL/{h`b} "MilliLitersPerHeartbeat"
* #mL/{h`b} ^property[0].code = #parent 
* #mL/{h`b} ^property[0].valueCode = #28 
* #nL "NanoLiter"
* #nL ^property[0].code = #parent 
* #nL ^property[0].valueCode = #28 
* #pL "PicoLiter"
* #pL ^property[0].code = #parent 
* #pL ^property[0].valueCode = #28 
* #uL "MicroLiter"
* #uL ^property[0].code = #parent 
* #uL ^property[0].valueCode = #28 
* #29 "Volume Duration Units"
* #29 ^property[0].code = #child 
* #29 ^property[0].valueCode = #L.s2/s 
* #L.s2/s "LiterSquareSecondPerSecond"
* #L.s2/s ^property[0].code = #parent 
* #L.s2/s ^property[0].valueCode = #29 
* #30 "Number Content Units"
* #30 ^property[0].code = #child 
* #30 ^property[0].valueCode = #/g 
* #30 ^property[1].code = #child 
* #30 ^property[1].valueCode = #/g{HGB} 
* #30 ^property[2].code = #child 
* #30 ^property[2].valueCode = #/g{creat} 
* #30 ^property[3].code = #child 
* #30 ^property[3].valueCode = #/g{tot`nit} 
* #30 ^property[4].code = #child 
* #30 ^property[4].valueCode = #/g{tot`prot} 
* #30 ^property[5].code = #child 
* #30 ^property[5].valueCode = #/g{wet`tis} 
* #30 ^property[6].code = #child 
* #30 ^property[6].valueCode = #/kg 
* #30 ^property[7].code = #child 
* #30 ^property[7].valueCode = #/kg{body`wt} 
* #30 ^property[8].code = #child 
* #30 ^property[8].valueCode = #/mg 
* #/g "PerGram"
* #/g ^property[0].code = #parent 
* #/g ^property[0].valueCode = #30 
* #/g{HGB} "PerGramHemoglobin"
* #/g{HGB} ^property[0].code = #parent 
* #/g{HGB} ^property[0].valueCode = #30 
* #/g{creat} "PerGramCreatinine"
* #/g{creat} ^property[0].code = #parent 
* #/g{creat} ^property[0].valueCode = #30 
* #/g{tot`nit} "PerGramTotalNitrogen"
* #/g{tot`nit} ^property[0].code = #parent 
* #/g{tot`nit} ^property[0].valueCode = #30 
* #/g{tot`prot} "PerGramTotalProtein"
* #/g{tot`prot} ^property[0].code = #parent 
* #/g{tot`prot} ^property[0].valueCode = #30 
* #/g{wet`tis} "PerGramWetTissue"
* #/g{wet`tis} ^property[0].code = #parent 
* #/g{wet`tis} ^property[0].valueCode = #30 
* #/kg "PerKiloGram"
* #/kg ^property[0].code = #parent 
* #/kg ^property[0].valueCode = #30 
* #/kg{body`wt} "PerKiloGramBodyWeight"
* #/kg{body`wt} ^property[0].code = #parent 
* #/kg{body`wt} ^property[0].valueCode = #30 
* #/mg "PerMilliGram"
* #/mg ^property[0].code = #parent 
* #/mg ^property[0].valueCode = #30 
* #31 "Substance Content Units"
* #31 ^property[0].code = #child 
* #31 ^property[0].valueCode = #fmol/g 
* #31 ^property[1].code = #child 
* #31 ^property[1].valueCode = #fmol/mg 
* #31 ^property[2].code = #child 
* #31 ^property[2].valueCode = #meq/g 
* #31 ^property[3].code = #child 
* #31 ^property[3].valueCode = #meq/g{Cre} 
* #31 ^property[4].code = #child 
* #31 ^property[4].valueCode = #meq/kg 
* #31 ^property[5].code = #child 
* #31 ^property[5].valueCode = #mmol/g 
* #31 ^property[6].code = #child 
* #31 ^property[6].valueCode = #mmol/kg 
* #31 ^property[7].code = #child 
* #31 ^property[7].valueCode = #mol/kg 
* #31 ^property[8].code = #child 
* #31 ^property[8].valueCode = #nmol/g 
* #31 ^property[9].code = #child 
* #31 ^property[9].valueCode = #nmol/g{Cre} 
* #31 ^property[10].code = #child 
* #31 ^property[10].valueCode = #nmol/mg 
* #31 ^property[11].code = #child 
* #31 ^property[11].valueCode = #osm/kg 
* #31 ^property[12].code = #child 
* #31 ^property[12].valueCode = #umol/g 
* #31 ^property[13].code = #child 
* #31 ^property[13].valueCode = #umol/g{Cre} 
* #31 ^property[14].code = #child 
* #31 ^property[14].valueCode = #umol/g{Hgb} 
* #31 ^property[15].code = #child 
* #31 ^property[15].valueCode = #umol/mg 
* #31 ^property[16].code = #child 
* #31 ^property[16].valueCode = #umol/mg{Cre} 
* #fmol/g "FemtoMolesPerGram"
* #fmol/g ^property[0].code = #parent 
* #fmol/g ^property[0].valueCode = #31 
* #fmol/mg "FemtoMolesPerMilliGram"
* #fmol/mg ^property[0].code = #parent 
* #fmol/mg ^property[0].valueCode = #31 
* #meq/g "MilliEquivalentsPerGram"
* #meq/g ^property[0].code = #parent 
* #meq/g ^property[0].valueCode = #31 
* #meq/g{Cre} "MilliEquivalentsPerGramCreatinine"
* #meq/g{Cre} ^property[0].code = #parent 
* #meq/g{Cre} ^property[0].valueCode = #31 
* #meq/kg "MilliEquivalentsPerKiloGram"
* #meq/kg ^property[0].code = #parent 
* #meq/kg ^property[0].valueCode = #31 
* #mmol/g "MilliMolesPerGram"
* #mmol/g ^property[0].code = #parent 
* #mmol/g ^property[0].valueCode = #31 
* #mol/kg "MolesPerKiloGram"
* #mol/kg ^property[0].code = #parent 
* #mol/kg ^property[0].valueCode = #31 
* #nmol/g "NanoMolesPerGram"
* #nmol/g ^property[0].code = #parent 
* #nmol/g ^property[0].valueCode = #31 
* #nmol/g{Cre} "NanoMolesPerGramCreatinine"
* #nmol/g{Cre} ^property[0].code = #parent 
* #nmol/g{Cre} ^property[0].valueCode = #31 
* #nmol/mg "NanoMolesPerMilliGram"
* #nmol/mg ^property[0].code = #parent 
* #nmol/mg ^property[0].valueCode = #31 
* #osm/kg "OsmolesPerKiloGram"
* #osm/kg ^property[0].code = #parent 
* #osm/kg ^property[0].valueCode = #31 
* #umol/g "MicroMolesPerGram"
* #umol/g ^property[0].code = #parent 
* #umol/g ^property[0].valueCode = #31 
* #umol/g{Cre} "MicroMolesPerGramCreatinine"
* #umol/g{Cre} ^property[0].code = #parent 
* #umol/g{Cre} ^property[0].valueCode = #31 
* #umol/g{Hgb} "MicroMolesPerGramHemoglobin"
* #umol/g{Hgb} ^property[0].code = #parent 
* #umol/g{Hgb} ^property[0].valueCode = #31 
* #umol/mg "MicroMolesPerMilliGram"
* #umol/mg ^property[0].code = #parent 
* #umol/mg ^property[0].valueCode = #31 
* #umol/mg{Cre} "MicroMolesPerMilliGramCreatinine"
* #umol/mg{Cre} ^property[0].code = #parent 
* #umol/mg{Cre} ^property[0].valueCode = #31 
* #32 "Substance Rate Content Units"
* #32 ^property[0].code = #child 
* #32 ^property[0].valueCode = #U/g 
* #32 ^property[1].code = #child 
* #32 ^property[1].valueCode = #U/g{Cre} 
* #32 ^property[2].code = #child 
* #32 ^property[2].valueCode = #U/g{Hgb} 
* #32 ^property[3].code = #child 
* #32 ^property[3].valueCode = #kU/g 
* #32 ^property[4].code = #child 
* #32 ^property[4].valueCode = #kat/kg 
* #32 ^property[5].code = #child 
* #32 ^property[5].valueCode = #mU/g 
* #32 ^property[6].code = #child 
* #32 ^property[6].valueCode = #mU/g{Hgb} 
* #32 ^property[7].code = #child 
* #32 ^property[7].valueCode = #mU/mg 
* #32 ^property[8].code = #child 
* #32 ^property[8].valueCode = #mU/mg{Cre} 
* #32 ^property[9].code = #child 
* #32 ^property[9].valueCode = #mosm/kg 
* #32 ^property[10].code = #child 
* #32 ^property[10].valueCode = #umol/min/g 
* #U/g "UnitsPerGram"
* #U/g ^property[0].code = #parent 
* #U/g ^property[0].valueCode = #32 
* #U/g{Cre} "UnitsPerGramCreatinine"
* #U/g{Cre} ^property[0].code = #parent 
* #U/g{Cre} ^property[0].valueCode = #32 
* #U/g{Hgb} "UnitsPerGramHemoglobin"
* #U/g{Hgb} ^property[0].code = #parent 
* #U/g{Hgb} ^property[0].valueCode = #32 
* #kU/g "KiloUnitsPerGram"
* #kU/g ^property[0].code = #parent 
* #kU/g ^property[0].valueCode = #32 
* #kat/kg "KatalPerKilogram"
* #kat/kg ^property[0].code = #parent 
* #kat/kg ^property[0].valueCode = #32 
* #mU/g "MilliUnitsPerGram"
* #mU/g ^property[0].code = #parent 
* #mU/g ^property[0].valueCode = #32 
* #mU/g{Hgb} "MilliUnitsPerGramHemoglobin"
* #mU/g{Hgb} ^property[0].code = #parent 
* #mU/g{Hgb} ^property[0].valueCode = #32 
* #mU/mg "MilliUnitsPerMilligram"
* #mU/mg ^property[0].code = #parent 
* #mU/mg ^property[0].valueCode = #32 
* #mU/mg{Cre} "MilliUnitsPerMilliGramCreatinine"
* #mU/mg{Cre} ^property[0].code = #parent 
* #mU/mg{Cre} ^property[0].valueCode = #32 
* #umol/min/g "MicroMolesPerMinutePerGram"
* #umol/min/g ^property[0].code = #parent 
* #umol/min/g ^property[0].valueCode = #32 
* #33 "Arbitrary Concentration Content Units"
* #33 ^property[0].code = #child 
* #33 ^property[0].valueCode = #[iU]/g 
* #33 ^property[1].code = #child 
* #33 ^property[1].valueCode = #[iU]/g{Hgb} 
* #33 ^property[2].code = #child 
* #33 ^property[2].valueCode = #[iU]/kg 
* #33 ^property[3].code = #child 
* #33 ^property[3].valueCode = #{Ehrlich_U}/100g 
* #[iU]/g "InternationalUnitsPerGram"
* #[iU]/g ^property[0].code = #parent 
* #[iU]/g ^property[0].valueCode = #33 
* #[iU]/g{Hgb} "InternationalUnitsPerGramHemoglobin"
* #[iU]/g{Hgb} ^property[0].code = #parent 
* #[iU]/g{Hgb} ^property[0].valueCode = #33 
* #[iU]/kg "InternationalUnitsPerKilogram"
* #[iU]/kg ^property[0].code = #parent 
* #[iU]/kg ^property[0].valueCode = #33 
* #{Ehrlich_U}/100g "EhrlichUnitsPer100Gram"
* #{Ehrlich_U}/100g ^property[0].code = #parent 
* #{Ehrlich_U}/100g ^property[0].valueCode = #33 
* #34 "Volume Content Units"
* #34 ^property[0].code = #child 
* #34 ^property[0].valueCode = #L/kg 
* #34 ^property[1].code = #child 
* #34 ^property[1].valueCode = #mL/kg 
* #L/kg "LitersPerKilogram"
* #L/kg ^property[0].code = #parent 
* #L/kg ^property[0].valueCode = #34 
* #mL/kg "MilliLitersPerKiloGram"
* #mL/kg ^property[0].code = #parent 
* #mL/kg ^property[0].valueCode = #34 
* #35 "Energy Content Units"
* #35 ^property[0].code = #child 
* #35 ^property[0].valueCode = #kCal/[oz_av] 
* #kCal/[oz_av] "KiloCaloriesPerOunce"
* #kCal/[oz_av] ^property[0].code = #parent 
* #kCal/[oz_av] ^property[0].valueCode = #35 
* #36 "Areic Number Units"
* #36 ^property[0].code = #child 
* #36 ^property[0].valueCode = #/m2 
* #/m2 "PerSquareMeter"
* #/m2 ^property[0].code = #parent 
* #/m2 ^property[0].valueCode = #36 
* #37 "Areic Mass Units"
* #37 ^property[0].code = #child 
* #37 ^property[0].valueCode = #g/m2 
* #37 ^property[1].code = #child 
* #37 ^property[1].valueCode = #kg/m2 
* #37 ^property[2].code = #child 
* #37 ^property[2].valueCode = #mg/m2 
* #37 ^property[3].code = #child 
* #37 ^property[3].valueCode = #ng/m2 
* #37 ^property[4].code = #child 
* #37 ^property[4].valueCode = #ug/m2 
* #g/m2 "GramsPerSquareMeter"
* #g/m2 ^property[0].code = #parent 
* #g/m2 ^property[0].valueCode = #37 
* #kg/m2 "KiloGramsPerSquareMeter"
* #kg/m2 ^property[0].code = #parent 
* #kg/m2 ^property[0].valueCode = #37 
* #mg/m2 "MilliGramsPerSquareMeter"
* #mg/m2 ^property[0].code = #parent 
* #mg/m2 ^property[0].valueCode = #37 
* #ng/m2 "NanoGramsPerSquareMeter"
* #ng/m2 ^property[0].code = #parent 
* #ng/m2 ^property[0].valueCode = #37 
* #ug/m2 "MicroGramsPerSquareMeter"
* #ug/m2 ^property[0].code = #parent 
* #ug/m2 ^property[0].valueCode = #37 
* #38 "Massive Distance Units"
* #38 ^property[0].code = #child 
* #38 ^property[0].valueCode = #g.m 
* #38 ^property[1].code = #child 
* #38 ^property[1].valueCode = #g.m/{hb} 
* #g.m "GramMeter"
* #g.m ^property[0].code = #parent 
* #g.m ^property[0].valueCode = #38 
* #g.m/{hb} "GramMeterPerHeartbeat"
* #g.m/{hb} ^property[0].code = #parent 
* #g.m/{hb} ^property[0].valueCode = #38 
* #40 "Molar Mass Units"
* #40 ^property[0].code = #child 
* #40 ^property[0].valueCode = #kg/mol 
* #kg/mol "KiloGramsPerMole"
* #kg/mol ^property[0].code = #parent 
* #kg/mol ^property[0].valueCode = #40 
* #41 "Number Concentration Units"
* #41 ^property[0].code = #child 
* #41 ^property[0].valueCode = #/L 
* #41 ^property[1].code = #child 
* #41 ^property[1].valueCode = #/dL 
* #41 ^property[2].code = #child 
* #41 ^property[2].valueCode = #/mL 
* #41 ^property[3].code = #child 
* #41 ^property[3].valueCode = #/uL 
* #41 ^property[4].code = #child 
* #41 ^property[4].valueCode = #10*12/L 
* #41 ^property[5].code = #child 
* #41 ^property[5].valueCode = #10*3/L 
* #41 ^property[6].code = #child 
* #41 ^property[6].valueCode = #10*3/mL 
* #41 ^property[7].code = #child 
* #41 ^property[7].valueCode = #10*3/uL 
* #41 ^property[8].code = #child 
* #41 ^property[8].valueCode = #10*3{Copies}/mL 
* #41 ^property[9].code = #child 
* #41 ^property[9].valueCode = #10*6/L 
* #41 ^property[10].code = #child 
* #41 ^property[10].valueCode = #10*6/mL 
* #41 ^property[11].code = #child 
* #41 ^property[11].valueCode = #10*6/uL 
* #41 ^property[12].code = #child 
* #41 ^property[12].valueCode = #10*9/L 
* #41 ^property[13].code = #child 
* #41 ^property[13].valueCode = #10*9/mL 
* #41 ^property[14].code = #child 
* #41 ^property[14].valueCode = #10*9/uL 
* #41 ^property[15].code = #child 
* #41 ^property[15].valueCode = #{Cells}/uL 
* #41 ^property[16].code = #child 
* #41 ^property[16].valueCode = #{Copies}/mL 
* #41 ^property[17].code = #child 
* #41 ^property[17].valueCode = #{Spermatozoa}/mL 
* #41 ^property[18].code = #child 
* #41 ^property[18].valueCode = #{cfu}/mL 
* #41 ^property[19].code = #child 
* #41 ^property[19].valueCode = #{rbc}/uL 
* #/L "PerLiter"
* #/L ^property[0].code = #parent 
* #/L ^property[0].valueCode = #41 
* #/dL "PerDeciLiter"
* #/dL ^property[0].code = #parent 
* #/dL ^property[0].valueCode = #41 
* #/mL "PerMilliLiter"
* #/mL ^property[0].code = #parent 
* #/mL ^property[0].valueCode = #41 
* #10*12/L "TrillionPerLiter"
* #10*12/L ^property[0].code = #parent 
* #10*12/L ^property[0].valueCode = #41 
* #10*3/L "ThousandPerLiter"
* #10*3/L ^property[0].code = #parent 
* #10*3/L ^property[0].valueCode = #41 
* #10*3/mL "ThousandPerMilliLiter"
* #10*3/mL ^property[0].code = #parent 
* #10*3/mL ^property[0].valueCode = #41 
* #10*3{Copies}/mL "ThousandCopiesPerMilliLiter"
* #10*3{Copies}/mL ^property[0].code = #parent 
* #10*3{Copies}/mL ^property[0].valueCode = #41 
* #10*6/L "MillionPerLiter"
* #10*6/L ^property[0].code = #parent 
* #10*6/L ^property[0].valueCode = #41 
* #10*6/mL "MillionPerMilliLiter"
* #10*6/mL ^property[0].code = #parent 
* #10*6/mL ^property[0].valueCode = #41 
* #10*9/L "BillionPerLiter"
* #10*9/L ^property[0].code = #parent 
* #10*9/L ^property[0].valueCode = #41 
* #10*9/mL "BillionsPerMilliliter"
* #10*9/mL ^property[0].code = #parent 
* #10*9/mL ^property[0].valueCode = #41 
* #10*9/uL "BillionsPerMicroLiter"
* #10*9/uL ^property[0].code = #parent 
* #10*9/uL ^property[0].valueCode = #41 
* #{Cells}/uL "CellsPerMicroLiter"
* #{Cells}/uL ^property[0].code = #parent 
* #{Cells}/uL ^property[0].valueCode = #41 
* #{Copies}/mL "CopiesPerMilliLiter"
* #{Copies}/mL ^property[0].code = #parent 
* #{Copies}/mL ^property[0].valueCode = #41 
* #{Spermatozoa}/mL "SpermatozoaPerMilliLiter"
* #{Spermatozoa}/mL ^property[0].code = #parent 
* #{Spermatozoa}/mL ^property[0].valueCode = #41 
* #{cfu}/mL "ColonyFormingUnitsPerMilliLiter"
* #{cfu}/mL ^property[0].code = #parent 
* #{cfu}/mL ^property[0].valueCode = #41 
* #{rbc}/uL "RedBloodCellsPerMicroLiter"
* #{rbc}/uL ^property[0].code = #parent 
* #{rbc}/uL ^property[0].valueCode = #41 
* #42 "Mass Concentration Units"
* #42 ^property[0].code = #child 
* #42 ^property[0].valueCode = #g/L 
* #42 ^property[1].code = #child 
* #42 ^property[1].valueCode = #g/dL 
* #42 ^property[2].code = #child 
* #42 ^property[2].valueCode = #g/mL 
* #42 ^property[3].code = #child 
* #42 ^property[3].valueCode = #kg/L 
* #42 ^property[4].code = #child 
* #42 ^property[4].valueCode = #kg/m3 
* #42 ^property[5].code = #child 
* #42 ^property[5].valueCode = #mg/L 
* #42 ^property[6].code = #child 
* #42 ^property[6].valueCode = #mg/dL 
* #42 ^property[7].code = #child 
* #42 ^property[7].valueCode = #mg/m3 
* #42 ^property[8].code = #child 
* #42 ^property[8].valueCode = #mg/mL 
* #42 ^property[9].code = #child 
* #42 ^property[9].valueCode = #mg{Phenylketones}/dL 
* #42 ^property[10].code = #child 
* #42 ^property[10].valueCode = #ng/L 
* #42 ^property[11].code = #child 
* #42 ^property[11].valueCode = #ng/dL 
* #42 ^property[12].code = #child 
* #42 ^property[12].valueCode = #ng/mL 
* #42 ^property[13].code = #child 
* #42 ^property[13].valueCode = #ng/mL{rbc} 
* #42 ^property[14].code = #child 
* #42 ^property[14].valueCode = #pg/L 
* #42 ^property[15].code = #child 
* #42 ^property[15].valueCode = #pg/dL 
* #42 ^property[16].code = #child 
* #42 ^property[16].valueCode = #pg/mL 
* #42 ^property[17].code = #child 
* #42 ^property[17].valueCode = #ug/L 
* #42 ^property[18].code = #child 
* #42 ^property[18].valueCode = #ug/dL 
* #42 ^property[19].code = #child 
* #42 ^property[19].valueCode = #ug/dL{rbc} 
* #42 ^property[20].code = #child 
* #42 ^property[20].valueCode = #ug/mL 
* #kg/L "KiloGramsPerLiter"
* #kg/L ^property[0].code = #parent 
* #kg/L ^property[0].valueCode = #42 
* #kg/m3 "KiloGramsPerCubicMeter"
* #kg/m3 ^property[0].code = #parent 
* #kg/m3 ^property[0].valueCode = #42 
* #mg/L "MilliGramsPerLiter"
* #mg/L ^property[0].code = #parent 
* #mg/L ^property[0].valueCode = #42 
* #mg/m3 "MilliGramsPerCubicMeter"
* #mg/m3 ^property[0].code = #parent 
* #mg/m3 ^property[0].valueCode = #42 
* #mg/mL "MilliGramsPerMilliLiter"
* #mg/mL ^property[0].code = #parent 
* #mg/mL ^property[0].valueCode = #42 
* #mg{Phenylketones}/dL "MilliGramsPhenylketonesPerDeciLiter"
* #mg{Phenylketones}/dL ^property[0].code = #parent 
* #mg{Phenylketones}/dL ^property[0].valueCode = #42 
* #ng/L "NanoGramsPerLiter"
* #ng/L ^property[0].code = #parent 
* #ng/L ^property[0].valueCode = #42 
* #ng/dL "NanoGramsPerDeciLiter"
* #ng/dL ^property[0].code = #parent 
* #ng/dL ^property[0].valueCode = #42 
* #ng/mL{rbc} "NanoGramsPerMilliLiterRedBloodCells"
* #ng/mL{rbc} ^property[0].code = #parent 
* #ng/mL{rbc} ^property[0].valueCode = #42 
* #pg/L "PicoGramsPerLiter"
* #pg/L ^property[0].code = #parent 
* #pg/L ^property[0].valueCode = #42 
* #pg/dL "PicoGramsPerDeciLiter"
* #pg/dL ^property[0].code = #parent 
* #pg/dL ^property[0].valueCode = #42 
* #ug/dL{rbc} "MicroGramsPerDeciLiterRedBloodCells"
* #ug/dL{rbc} ^property[0].code = #parent 
* #ug/dL{rbc} ^property[0].valueCode = #42 
* #43 "Substance Concentration Units"
* #43 ^property[0].code = #child 
* #43 ^property[0].valueCode = #10*6.eq/mL 
* #43 ^property[1].code = #child 
* #43 ^property[1].valueCode = #eq/L 
* #43 ^property[2].code = #child 
* #43 ^property[2].valueCode = #eq/mL 
* #43 ^property[3].code = #child 
* #43 ^property[3].valueCode = #fmol/mL 
* #43 ^property[4].code = #child 
* #43 ^property[4].valueCode = #meq/L 
* #43 ^property[5].code = #child 
* #43 ^property[5].valueCode = #meq/dL 
* #43 ^property[6].code = #child 
* #43 ^property[6].valueCode = #meq/mL 
* #43 ^property[7].code = #child 
* #43 ^property[7].valueCode = #mmol/L 
* #43 ^property[8].code = #child 
* #43 ^property[8].valueCode = #mmol/dL 
* #43 ^property[9].code = #child 
* #43 ^property[9].valueCode = #mol/L 
* #43 ^property[10].code = #child 
* #43 ^property[10].valueCode = #mol/m3 
* #43 ^property[11].code = #child 
* #43 ^property[11].valueCode = #mol/mL 
* #43 ^property[12].code = #child 
* #43 ^property[12].valueCode = #mosm/L 
* #43 ^property[13].code = #child 
* #43 ^property[13].valueCode = #nmol/L 
* #43 ^property[14].code = #child 
* #43 ^property[14].valueCode = #nmol/dL 
* #43 ^property[15].code = #child 
* #43 ^property[15].valueCode = #nmol/mL 
* #43 ^property[16].code = #child 
* #43 ^property[16].valueCode = #osm/L 
* #43 ^property[17].code = #child 
* #43 ^property[17].valueCode = #pmol/L 
* #43 ^property[18].code = #child 
* #43 ^property[18].valueCode = #pmol/dL 
* #43 ^property[19].code = #child 
* #43 ^property[19].valueCode = #pmol/mL 
* #43 ^property[20].code = #child 
* #43 ^property[20].valueCode = #ueq/L 
* #43 ^property[21].code = #child 
* #43 ^property[21].valueCode = #ueq/mL 
* #43 ^property[22].code = #child 
* #43 ^property[22].valueCode = #umol/L 
* #43 ^property[23].code = #child 
* #43 ^property[23].valueCode = #umol/dL 
* #43 ^property[24].code = #child 
* #43 ^property[24].valueCode = #umol/mL 
* #43 ^property[25].code = #child 
* #43 ^property[25].valueCode = #{AHG}eq/mL 
* #10*6.eq/mL "MillionEquivalentsPerMilliLiter"
* #10*6.eq/mL ^property[0].code = #parent 
* #10*6.eq/mL ^property[0].valueCode = #43 
* #eq/L "EquivalentsPerLiter"
* #eq/L ^property[0].code = #parent 
* #eq/L ^property[0].valueCode = #43 
* #eq/mL "EquivalentsPerMilliLiter"
* #eq/mL ^property[0].code = #parent 
* #eq/mL ^property[0].valueCode = #43 
* #fmol/mL "FemtoMolesPerMilliLiter"
* #fmol/mL ^property[0].code = #parent 
* #fmol/mL ^property[0].valueCode = #43 
* #meq/dL "MilliEquivalentsPerDeciLiter"
* #meq/dL ^property[0].code = #parent 
* #meq/dL ^property[0].valueCode = #43 
* #meq/mL "MilliEquivalentPerMilliLiter"
* #meq/mL ^property[0].code = #parent 
* #meq/mL ^property[0].valueCode = #43 
* #mmol/dL "MilliMolesPerDeciLiter"
* #mmol/dL ^property[0].code = #parent 
* #mmol/dL ^property[0].valueCode = #43 
* #mol/L "MolesPerLiter"
* #mol/L ^property[0].code = #parent 
* #mol/L ^property[0].valueCode = #43 
* #mol/m3 "MolesPerCubicMeter"
* #mol/m3 ^property[0].code = #parent 
* #mol/m3 ^property[0].valueCode = #43 
* #mol/mL "MolesPerMilliLiter"
* #mol/mL ^property[0].code = #parent 
* #mol/mL ^property[0].valueCode = #43 
* #mosm/L "MilliOsmolesPerLiter"
* #mosm/L ^property[0].code = #parent 
* #mosm/L ^property[0].valueCode = #43 
* #nmol/dL "NanoMolesPerDeciLiter"
* #nmol/dL ^property[0].code = #parent 
* #nmol/dL ^property[0].valueCode = #43 
* #nmol/mL "NanoMolesPerMilliLiter"
* #nmol/mL ^property[0].code = #parent 
* #nmol/mL ^property[0].valueCode = #43 
* #osm/L "OsmolesPerLiter"
* #osm/L ^property[0].code = #parent 
* #osm/L ^property[0].valueCode = #43 
* #pmol/dL "PicoMolesPerDeciLiter"
* #pmol/dL ^property[0].code = #parent 
* #pmol/dL ^property[0].valueCode = #43 
* #pmol/mL "PicoMolesPerMilliLiter"
* #pmol/mL ^property[0].code = #parent 
* #pmol/mL ^property[0].valueCode = #43 
* #ueq/L "MicroEquivalentsPerLiter"
* #ueq/L ^property[0].code = #parent 
* #ueq/L ^property[0].valueCode = #43 
* #ueq/mL "MicroEquivalentsPerMilliLiter"
* #ueq/mL ^property[0].code = #parent 
* #ueq/mL ^property[0].valueCode = #43 
* #umol/dL "MicroMolesPerDeciLiter"
* #umol/dL ^property[0].code = #parent 
* #umol/dL ^property[0].valueCode = #43 
* #umol/mL "MicroMolesPerMilliLiter"
* #umol/mL ^property[0].code = #parent 
* #umol/mL ^property[0].valueCode = #43 
* #{AHG}eq/mL "AHGEquivalentsPerMilliLiter"
* #{AHG}eq/mL ^property[0].code = #parent 
* #{AHG}eq/mL ^property[0].valueCode = #43 
* #44 "Arbitrary Concentration Units"
* #44 ^property[0].code = #child 
* #44 ^property[0].valueCode = #[iU]/L 
* #44 ^property[1].code = #child 
* #44 ^property[1].valueCode = #[iU]/dL 
* #44 ^property[2].code = #child 
* #44 ^property[2].valueCode = #[iU]/mL 
* #44 ^property[3].code = #child 
* #44 ^property[3].valueCode = #k[iU]/mL 
* #44 ^property[4].code = #child 
* #44 ^property[4].valueCode = #m[iU]/L 
* #44 ^property[5].code = #child 
* #44 ^property[5].valueCode = #m[iU]/mL 
* #44 ^property[6].code = #child 
* #44 ^property[6].valueCode = #u[iU]/mL 
* #44 ^property[7].code = #child 
* #44 ^property[7].valueCode = #{ComplementCh50}U/mL 
* #44 ^property[8].code = #child 
* #44 ^property[8].valueCode = #{Ehrlich_U}/dL 
* #44 ^property[9].code = #child 
* #44 ^property[9].valueCode = #{Elisa_U}/mL 
* #44 ^property[10].code = #child 
* #44 ^property[10].valueCode = #{IgAPhospholipid}U/mL 
* #44 ^property[11].code = #child 
* #44 ^property[11].valueCode = #{IgGPhospholipid}U/mL 
* #44 ^property[12].code = #child 
* #44 ^property[12].valueCode = #{IgMPhospholipid}U/mL 
* #[iU]/dL "InternationalUnitsPerDeciLiter"
* #[iU]/dL ^property[0].code = #parent 
* #[iU]/dL ^property[0].valueCode = #44 
* #[iU]/mL "InternationalUnitsPerMilliLiter"
* #[iU]/mL ^property[0].code = #parent 
* #[iU]/mL ^property[0].valueCode = #44 
* #k[iU]/mL "KiloInternationalUnitsPerMilliLiter"
* #k[iU]/mL ^property[0].code = #parent 
* #k[iU]/mL ^property[0].valueCode = #44 
* #m[iU]/L "MilliInternationalUnitsPerLiter"
* #m[iU]/L ^property[0].code = #parent 
* #m[iU]/L ^property[0].valueCode = #44 
* #{ComplementCh50}U/mL "ComplementCh50UnitsPerMilliLiter"
* #{ComplementCh50}U/mL ^property[0].code = #parent 
* #{ComplementCh50}U/mL ^property[0].valueCode = #44 
* #{Ehrlich_U}/dL "EhrlichUnitsPerDeciLiter"
* #{Ehrlich_U}/dL ^property[0].code = #parent 
* #{Ehrlich_U}/dL ^property[0].valueCode = #44 
* #{Elisa_U}/mL "ElisaUnitsPerMilliLiter"
* #{Elisa_U}/mL ^property[0].code = #parent 
* #{Elisa_U}/mL ^property[0].valueCode = #44 
* #{IgAPhospholipid}U/mL "IgAPhospholipidUnitsPerMilliLiter"
* #{IgAPhospholipid}U/mL ^property[0].code = #parent 
* #{IgAPhospholipid}U/mL ^property[0].valueCode = #44 
* #{IgGPhospholipid}U/mL "IgGPhospholipidUnitsPerMilliLiter"
* #{IgGPhospholipid}U/mL ^property[0].code = #parent 
* #{IgGPhospholipid}U/mL ^property[0].valueCode = #44 
* #{IgMPhospholipid}U/mL "IgMPhospholipidUnitsPerMilliLiter"
* #{IgMPhospholipid}U/mL ^property[0].code = #parent 
* #{IgMPhospholipid}U/mL ^property[0].valueCode = #44 
* #45 "pH Units"
* #45 ^property[0].code = #child 
* #45 ^property[0].valueCode = #[pH] 
* #[pH] "pH"
* #[pH] ^property[0].code = #parent 
* #[pH] ^property[0].valueCode = #45 
