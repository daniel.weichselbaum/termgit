Instance: elga-confidentiality 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-confidentiality" 
* name = "elga-confidentiality" 
* title = "ELGA_Confidentiality" 
* status = #active 
* version = "202008" 
* description = "**Description:** This Value Set is used to declare confidentiality for the entire CDA-Document. Due to the fact that all medical documents are confidential and the access rules within ELGA are controlled by the ''Access Control''- set of regulations, all documents are assigned ConfidentialityCode N (normal). This is a limited subset of the original codelist.

**Beschreibung:** Das Value Set wird verwendet, um die Vertraulichkeit für das gesamte CDA-Dokument anzugeben. Da alle medizinischen Dokumente vertraulich sind und die Zugriffsregeln in ELGA außerhalb des Dokuments durch das Berechtigungsregelwerk gesteuert werden, bekommen alle Dokumente den ConfidentialityCode N (normal).     Eingeschränktes Subset der originalen Codeliste." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.7" 
* date = "2020-08-20" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.hl7.org" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-hl7-confidentiality-code"
* compose.include[0].concept[0].code = "L"
* compose.include[0].concept[0].display = "low"
* compose.include[0].concept[1].code = "M"
* compose.include[0].concept[1].display = "moderate"
* compose.include[0].concept[2].code = "N"
* compose.include[0].concept[2].display = "normal"
* compose.include[0].concept[3].code = "R"
* compose.include[0].concept[3].display = "restricted"
* compose.include[0].concept[4].code = "U"
* compose.include[0].concept[4].display = "unrestricted"
* compose.include[0].concept[5].code = "V"
* compose.include[0].concept[5].display = "very restricted"
