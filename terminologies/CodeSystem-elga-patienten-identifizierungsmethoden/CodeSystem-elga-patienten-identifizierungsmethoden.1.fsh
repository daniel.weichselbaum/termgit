Instance: elga-patienten-identifizierungsmethoden 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-patienten-identifizierungsmethoden" 
* name = "elga-patienten-identifizierungsmethoden" 
* title = "ELGA_Patienten-Identifizierungsmethoden" 
* status = #active 
* content = #complete 
* version = "202105" 
* description = "**Beschreibung:** ELGA-Codeliste für Identifizierungsmethoden von Patienten" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.162" 
* date = "2021-05-19" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* count = 6 
* concept[0].code = #PIM101
* concept[0].display = "e-card wurde in den Kartenleser gesteckt"
* concept[1].code = #PIM102
* concept[1].display = "Bürgerkarte wurde in den Kartenleser gesteckt"
* concept[2].code = #PIM103
* concept[2].display = "Im lokalen Patientenindex via LPID identifiziert"
* concept[3].code = #PIM104
* concept[3].display = "Patient ist identifiziert via e-card System ohne gesteckte e-card"
* concept[4].code = #PIM105
* concept[4].display = "Identifikation anhand amtlichen Lichtbildausweis oder e-card für mobile e-Impfpass Anwendung"
* concept[5].code = #PIM106
* concept[5].display = "Zuweisung"
