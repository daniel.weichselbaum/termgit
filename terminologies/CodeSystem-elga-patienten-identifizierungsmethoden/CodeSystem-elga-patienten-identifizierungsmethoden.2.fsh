Instance: elga-patienten-identifizierungsmethoden 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-patienten-identifizierungsmethoden" 
* name = "elga-patienten-identifizierungsmethoden" 
* title = "ELGA_Patienten-Identifizierungsmethoden" 
* status = #active 
* content = #complete 
* version = "202105" 
* description = "**Beschreibung:** ELGA-Codeliste für Identifizierungsmethoden von Patienten" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.162" 
* date = "2021-05-19" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* count = 6 
* #PIM101 "e-card wurde in den Kartenleser gesteckt"
* #PIM102 "Bürgerkarte wurde in den Kartenleser gesteckt"
* #PIM103 "Im lokalen Patientenindex via LPID identifiziert"
* #PIM104 "Patient ist identifiziert via e-card System ohne gesteckte e-card"
* #PIM105 "Identifikation anhand amtlichen Lichtbildausweis oder e-card für mobile e-Impfpass Anwendung"
* #PIM106 "Zuweisung"
