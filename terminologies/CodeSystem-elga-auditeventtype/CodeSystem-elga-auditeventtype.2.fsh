Instance: elga-auditeventtype 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-auditeventtype" 
* name = "elga-auditeventtype" 
* title = "ELGA_AuditEventType" 
* status = #active 
* content = #complete 
* version = "202202" 
* description = "**Beschreibung:** Codeliste der ELGA spezifischen Audit Event Type Codes (Transaktionen). Verwendung in IHE konformen und ELGA BeS definierten Audit Events." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.151" 
* date = "2022-02-10" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* count = 45 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #0 "PAP Transaction"
* #0 ^definition = PAP spezifische Audit Events
* #0 ^property[0].code = #child 
* #0 ^property[0].valueCode = #1 
* #0 ^property[1].code = #child 
* #0 ^property[1].valueCode = #2 
* #0 ^property[2].code = #child 
* #0 ^property[2].valueCode = #3 
* #0 ^property[3].code = #child 
* #0 ^property[3].valueCode = #4 
* #0 ^property[4].code = #child 
* #0 ^property[4].valueCode = #5 
* #0 ^property[5].code = #child 
* #0 ^property[5].valueCode = #6 
* #1 "Submit Policy"
* #1 ^designation[0].language = #de-AT 
* #1 ^designation[0].value = "Generelle Policy einbringen" 
* #1 ^property[0].code = #parent 
* #1 ^property[0].valueCode = #0 
* #2 "Access Policy"
* #2 ^designation[0].language = #de-AT 
* #2 ^designation[0].value = "Generelle Policy verwenden" 
* #2 ^property[0].code = #parent 
* #2 ^property[0].valueCode = #0 
* #3 "Deprecate Policy"
* #3 ^designation[0].language = #de-AT 
* #3 ^designation[0].value = "Generelle Policy ungültig setzen" 
* #3 ^property[0].code = #parent 
* #3 ^property[0].valueCode = #0 
* #4 "Submit Consent"
* #4 ^designation[0].language = #de-AT 
* #4 ^designation[0].value = "Patientenzustimmung einbringen" 
* #4 ^property[0].code = #parent 
* #4 ^property[0].valueCode = #0 
* #5 "Query Consent"
* #5 ^designation[0].language = #de-AT 
* #5 ^designation[0].value = "Patientenzustimmung abfragen" 
* #5 ^property[0].code = #parent 
* #5 ^property[0].valueCode = #0 
* #6 "Consent Submission Failed"
* #6 ^designation[0].language = #de-AT 
* #6 ^designation[0].value = "Patientenzustimmung konnte nicht eingebracht werden" 
* #6 ^property[0].code = #parent 
* #6 ^property[0].valueCode = #0 
* #10 "KBS"
* #10 ^definition = ELGA Kontaktbestätigungsservice Audit Events
* #10 ^property[0].code = #child 
* #10 ^property[0].valueCode = #11 
* #10 ^property[1].code = #child 
* #10 ^property[1].valueCode = #12 
* #10 ^property[2].code = #child 
* #10 ^property[2].valueCode = #13 
* #10 ^property[3].code = #child 
* #10 ^property[3].valueCode = #14 
* #10 ^property[4].code = #child 
* #10 ^property[4].valueCode = #15 
* #11 "Add Contact"
* #11 ^designation[0].language = #de-AT 
* #11 ^designation[0].value = "Kontaktbestätigung einbringen" 
* #11 ^property[0].code = #parent 
* #11 ^property[0].valueCode = #10 
* #12 "List Contacts"
* #12 ^designation[0].language = #de-AT 
* #12 ^designation[0].value = "Kontaktbestätigungen anzeigen" 
* #12 ^property[0].code = #parent 
* #12 ^property[0].valueCode = #10 
* #13 "Get Active Contact"
* #13 ^designation[0].language = #de-AT 
* #13 ^designation[0].value = "Aktive Kontaktbestätigung holen" 
* #13 ^property[0].code = #parent 
* #13 ^property[0].valueCode = #10 
* #14 "Delegate Contact"
* #14 ^designation[0].language = #de-AT 
* #14 ^designation[0].value = "Kontaktbestätigung delegieren" 
* #14 ^property[0].code = #parent 
* #14 ^property[0].valueCode = #10 
* #15 "Invalidate Contact"
* #15 ^designation[0].language = #de-AT 
* #15 ^designation[0].value = "Kontaktbestätigung stornieren" 
* #15 ^property[0].code = #parent 
* #15 ^property[0].valueCode = #10 
* #20 "ETS"
* #20 ^definition = ELGA Token Service Audit Events
* #20 ^property[0].code = #child 
* #20 ^property[0].valueCode = #21 
* #20 ^property[1].code = #child 
* #20 ^property[1].valueCode = #22 
* #20 ^property[2].code = #child 
* #20 ^property[2].valueCode = #23 
* #20 ^property[3].code = #child 
* #20 ^property[3].valueCode = #24 
* #20 ^property[4].code = #child 
* #20 ^property[4].valueCode = #25 
* #20 ^property[5].code = #child 
* #20 ^property[5].valueCode = #26 
* #20 ^property[6].code = #child 
* #20 ^property[6].valueCode = #27 
* #21 "Issue Token"
* #21 ^designation[0].language = #de-AT 
* #21 ^designation[0].value = "Token ausstellen" 
* #21 ^property[0].code = #parent 
* #21 ^property[0].valueCode = #20 
* #22 "Invalidate Token"
* #22 ^designation[0].language = #de-AT 
* #22 ^designation[0].value = "Token ungültig setzen" 
* #22 ^property[0].code = #parent 
* #22 ^property[0].valueCode = #20 
* #23 "Validate Token"
* #23 ^designation[0].language = #de-AT 
* #23 ^designation[0].value = "Token validieren" 
* #23 ^property[0].code = #parent 
* #23 ^property[0].valueCode = #20 
* #24 "Renew Token"
* #24 ^designation[0].language = #de-AT 
* #24 ^designation[0].value = "Token erneuern" 
* #24 ^property[0].code = #parent 
* #24 ^property[0].valueCode = #20 
* #25 "BürgerIn Anmeldung"
* #25 ^designation[0].language = #de-AT 
* #25 ^designation[0].value = "BürgerIn Anmeldung" 
* #25 ^property[0].code = #parent 
* #25 ^property[0].valueCode = #20 
* #26 "BürgerIn Anmeldung in Vertretung"
* #26 ^designation[0].language = #de-AT 
* #26 ^designation[0].value = "BürgerIn Anmeldung in Vertretung" 
* #26 ^property[0].code = #parent 
* #26 ^property[0].valueCode = #20 
* #27 "BürgerIn Anmeldung der OBST in Vertretung"
* #27 ^designation[0].language = #de-AT 
* #27 ^designation[0].value = "BürgerIn Anmeldung der OBST in Vertretung" 
* #27 ^property[0].code = #parent 
* #27 ^property[0].valueCode = #20 
* #30 "Dokumentenverwaltung"
* #30 ^designation[0].language = #de-AT 
* #30 ^designation[0].value = "Dokumentenverwaltung" 
* #30 ^property[0].code = #child 
* #30 ^property[0].valueCode = #31 
* #30 ^property[1].code = #child 
* #30 ^property[1].valueCode = #33 
* #30 ^property[2].code = #child 
* #30 ^property[2].valueCode = #35 
* #30 ^property[3].code = #child 
* #30 ^property[3].valueCode = #36 
* #30 ^property[4].code = #child 
* #30 ^property[4].valueCode = #37 
* #30 ^property[5].code = #child 
* #30 ^property[5].valueCode = #38 
* #31 "Dokument einbringen"
* #31 ^designation[0].language = #de-AT 
* #31 ^designation[0].value = "Dokument einbringen" 
* #31 ^property[0].code = #parent 
* #31 ^property[0].valueCode = #30 
* #33 "Dokument Metadaten ändern"
* #33 ^designation[0].language = #de-AT 
* #33 ^designation[0].value = "Dokument Metadaten ändern" 
* #33 ^property[0].code = #parent 
* #33 ^property[0].valueCode = #30 
* #35 "Dokument suchen"
* #35 ^designation[0].language = #de-AT 
* #35 ^designation[0].value = "Dokument suchen" 
* #35 ^property[0].code = #parent 
* #35 ^property[0].valueCode = #30 
* #36 "Dokument abrufen"
* #36 ^designation[0].language = #de-AT 
* #36 ^designation[0].value = "Dokument abrufen" 
* #36 ^property[0].code = #parent 
* #36 ^property[0].valueCode = #30 
* #37 "Löschaufträge abfragen"
* #37 ^designation[0].language = #de-AT 
* #37 ^designation[0].value = "Löschaufträge abfragen" 
* #37 ^property[0].code = #parent 
* #37 ^property[0].valueCode = #30 
* #38 "Löschauftrag bestätigen"
* #38 ^designation[0].language = #de-AT 
* #38 ^designation[0].value = "Löschauftrag bestätigen" 
* #38 ^property[0].code = #parent 
* #38 ^property[0].valueCode = #30 
* #40 "eMed"
* #40 ^property[0].code = #child 
* #40 ^property[0].valueCode = #42 
* #40 ^property[1].code = #child 
* #40 ^property[1].valueCode = #43 
* #40 ^property[2].code = #child 
* #40 ^property[2].valueCode = #44 
* #40 ^property[3].code = #child 
* #40 ^property[3].valueCode = #45 
* #40 ^property[4].code = #child 
* #40 ^property[4].valueCode = #46 
* #42 "Medikationsdaten ändern"
* #42 ^designation[0].language = #de-AT 
* #42 ^designation[0].value = "Medikationsdaten ändern" 
* #42 ^property[0].code = #parent 
* #42 ^property[0].valueCode = #40 
* #43 "Medikationsdaten lesen"
* #43 ^designation[0].language = #de-AT 
* #43 ^designation[0].value = "Medikationsdaten lesen" 
* #43 ^property[0].code = #parent 
* #43 ^property[0].valueCode = #40 
* #44 "EMEDAT-1 Request Security Token"
* #44 ^designation[0].language = #de-AT 
* #44 ^designation[0].value = "Anfrage zur Ausstellung einer eMed-ID Assertion" 
* #44 ^property[0].code = #parent 
* #44 ^property[0].valueCode = #40 
* #45 "EMEDAT-1 Generate DocumentID"
* #45 ^designation[0].language = #de-AT 
* #45 ^designation[0].value = "Anfrage zur Ausstellung einer eMed-ID" 
* #45 ^property[0].code = #parent 
* #45 ^property[0].valueCode = #40 
* #46 "EMEDAT Bulk Expire Dokuments"
* #46 ^definition = Internes Löschen und Invalidieren von Dokumenten entsprechend gesetzlicher / fachlicher Anforderungen
* #46 ^designation[0].language = #de-AT 
* #46 ^designation[0].value = "Internes Löschen und Invalidieren von Dokumenten" 
* #46 ^property[0].code = #parent 
* #46 ^property[0].valueCode = #40 
* #50 "ELGA allgemein"
* #50 ^definition = Allgemeine ELGA Audit Events
* #50 ^designation[0].language = #de-AT 
* #50 ^designation[0].value = "ELGA allgemein" 
* #50 ^property[0].code = #child 
* #50 ^property[0].valueCode = #51 
* #50 ^property[1].code = #child 
* #50 ^property[1].valueCode = #52 
* #50 ^property[2].code = #child 
* #50 ^property[2].valueCode = #53 
* #50 ^property[3].code = #child 
* #50 ^property[3].valueCode = #54 
* #51 "Notify ELGA XAD-PID Link Change"
* #51 ^definition = Transaktion ELGA-1
* #51 ^designation[0].language = #de-AT 
* #51 ^designation[0].value = "Technische Datenkorrektur" 
* #51 ^property[0].code = #parent 
* #51 ^property[0].valueCode = #50 
* #52 "Registrieren von Dokumenten im Rahmen eines Clearings"
* #52 ^designation[0].language = #de-AT 
* #52 ^designation[0].value = "Registrieren von Dokumenten im Rahmen eines Clearings" 
* #52 ^property[0].code = #parent 
* #52 ^property[0].valueCode = #50 
* #53 "Stornieren von Dokumenten im Rahmen eines Clearings"
* #53 ^designation[0].language = #de-AT 
* #53 ^designation[0].value = "Stornieren von Dokumenten im Rahmen eines Clearings" 
* #53 ^property[0].code = #parent 
* #53 ^property[0].valueCode = #50 
* #54 "Dokumentenbereinigung im Auftrag des GDA"
* #54 ^designation[0].language = #de-AT 
* #54 ^designation[0].value = "Dokumentenbereinigung im Auftrag des GDA" 
* #54 ^property[0].code = #parent 
* #54 ^property[0].valueCode = #50 
* #60 "e-Impfpass"
* #60 ^property[0].code = #child 
* #60 ^property[0].valueCode = #61 
* #60 ^property[1].code = #child 
* #60 ^property[1].valueCode = #62 
* #61 "Immunisierungsdaten geändert (e-Impfpass)"
* #61 ^designation[0].language = #de-AT 
* #61 ^designation[0].value = "e-Impfpassdaten geändert" 
* #61 ^property[0].code = #parent 
* #61 ^property[0].valueCode = #60 
* #62 "Immunisierungsdaten aufgerufen (e-Impfpass)"
* #62 ^designation[0].language = #de-AT 
* #62 ^designation[0].value = "e-Impfpassdaten aufgerufen" 
* #62 ^property[0].code = #parent 
* #62 ^property[0].valueCode = #60 
* #70 "Benachrichtigungsservice"
* #70 ^property[0].code = #child 
* #70 ^property[0].valueCode = #71 
* #70 ^property[1].code = #child 
* #70 ^property[1].valueCode = #72 
* #71 "Benachrichtigungsvermerk eingetragen"
* #71 ^designation[0].language = #de-AT 
* #71 ^designation[0].value = "Benachrichtigungsvermerk eingetragen" 
* #71 ^property[0].code = #parent 
* #71 ^property[0].valueCode = #70 
* #72 "Benachrichtigungsvermerk entfernt"
* #72 ^designation[0].language = #de-AT 
* #72 ^designation[0].value = "Benachrichtigungsvermerk entfernt" 
* #72 ^property[0].code = #parent 
* #72 ^property[0].valueCode = #70 
