Instance: gda-org 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-gda-org" 
* name = "gda-org" 
* title = "GDA-Org" 
* status = #active 
* content = #complete 
* version = "gda-org" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.5" 
* date = "2013-01-10" 
* count = 15 
* #ABT "Abteilung"
* #ADM "Verwaltung"
* #AMB "Ambulanz"
* #DEV "Dislozierte ambulante Erstversorgung"
* #DPT "Departement"
* #DTK "Dislozierte Tagesklinik"
* #DWK "Dislozierte Wochenklinik"
* #DZE "Dislozierte zentrale Aufnahme und Erstversorgungseinrichtung"
* #EVA "Ambulante Erstversorgung"
* #FSP "Fachschwerpunkt"
* #IST "Institut"
* #KAP "Anstaltsapotheke"
* #LBH "Einrichtung für Langzeitbehandlung"
* #REZ "Referenzzentrum"
* #ZAE "Zentrale Aufnahme und Erstversorgungseinrichtung"
