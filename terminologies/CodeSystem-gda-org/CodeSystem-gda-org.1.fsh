Instance: gda-org 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-gda-org" 
* name = "gda-org" 
* title = "GDA-Org" 
* status = #active 
* content = #complete 
* version = "gda-org" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.5" 
* date = "2013-01-10" 
* count = 15 
* concept[0].code = #ABT
* concept[0].display = "Abteilung"
* concept[1].code = #ADM
* concept[1].display = "Verwaltung"
* concept[2].code = #AMB
* concept[2].display = "Ambulanz"
* concept[3].code = #DEV
* concept[3].display = "Dislozierte ambulante Erstversorgung"
* concept[4].code = #DPT
* concept[4].display = "Departement"
* concept[5].code = #DTK
* concept[5].display = "Dislozierte Tagesklinik"
* concept[6].code = #DWK
* concept[6].display = "Dislozierte Wochenklinik"
* concept[7].code = #DZE
* concept[7].display = "Dislozierte zentrale Aufnahme und Erstversorgungseinrichtung"
* concept[8].code = #EVA
* concept[8].display = "Ambulante Erstversorgung"
* concept[9].code = #FSP
* concept[9].display = "Fachschwerpunkt"
* concept[10].code = #IST
* concept[10].display = "Institut"
* concept[11].code = #KAP
* concept[11].display = "Anstaltsapotheke"
* concept[12].code = #LBH
* concept[12].display = "Einrichtung für Langzeitbehandlung"
* concept[13].code = #REZ
* concept[13].display = "Referenzzentrum"
* concept[14].code = #ZAE
* concept[14].display = "Zentrale Aufnahme und Erstversorgungseinrichtung"
