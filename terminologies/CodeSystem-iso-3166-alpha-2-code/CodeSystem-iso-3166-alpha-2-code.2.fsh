Instance: iso-3166-alpha-2-code 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-iso-3166-alpha-2-code" 
* name = "iso-3166-alpha-2-code" 
* title = "ISO 3166 Alpha-2-Code" 
* status = #active 
* content = #complete 
* version = "2016" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.0.3166.1.2.2" 
* date = "2016-07-11" 
* count = 251 
* property[0].code = #hints 
* property[0].type = #string 
* #AD "Andorra"
* #AE "Vereinigte Arab.Emirate"
* #AF "Afghanistan"
* #AG "Antigua und Barbuda"
* #AI "Anguilla"
* #AL "Albanien"
* #AM "Armenien"
* #AN "Niedl.Antillen"
* #AO "Angola"
* #AQ "Antarktis"
* #AR "Argentinien"
* #AS "Amerikan.Samoa"
* #AT "Österreich"
* #AU "Australien"
* #AW "Aruba"
* #AX "Alandinseln"
* #AZ "Aserbaidschan"
* #BA "Bosnien und Herzegowina"
* #BB "Barbados"
* #BD "Bangladesch"
* #BE "Belgien"
* #BF "Burkina Faso"
* #BG "Bulgarien"
* #BH "Bahrain"
* #BI "Burundi"
* #BJ "Benin"
* #BM "Bermuda"
* #BN "Brunei Darussalam"
* #BO "Bolivien"
* #BR "Brasilien"
* #BS "Bahamas"
* #BT "Bhutan"
* #BV "Bouvetinsel"
* #BW "Botsuana"
* #BY "Belarus"
* #BZ "Belize"
* #CA "Kanada"
* #CC "Kokosinseln(Au)"
* #CD "Kongo,Dem.Rep.(Kinshasa)"
* #CF "Zentralafrikan.Republik"
* #CG "Kongo,Rep. (Brazzaville)"
* #CH "Schweiz"
* #CI "Cote d'Ivoire"
* #CK "Cookinseln"
* #CL "Chile"
* #CM "Kamerun"
* #CN "China"
* #CO "Kolumbien"
* #CR "Costa Rica"
* #CS "Serbien und Montenegro"
* #CU "Kuba"
* #CV "Kap Verde"
* #CX "Weihnachtsinsel"
* #CY "Zypern"
* #CZ "Tschechische Republik"
* #DE "Deutschland"
* #DJ "Dschibuti"
* #DK "Dänemark"
* #DM "Dominica"
* #DO "Dominikanische Republik"
* #DZ "Algerien"
* #EC "Ecuador"
* #EE "Estland"
* #EG "Ägypten"
* #EH "Westsahara"
* #ER "Eritrea"
* #ES "Spanien"
* #ET "Äthiopien"
* #FI "Finnland"
* #FJ "Fidschi"
* #FK "Falklandinseln"
* #FM "Mikronesien,Föd.Staat.von"
* #FO "Färöer"
* #FR "Frankreich"
* #GA "Gabun"
* #GB "Vereinigtes Königreich"
* #GD "Grenada"
* #GE "Georgien"
* #GF "Franz.Guayana"
* #GG "Guernsey (nur SV)"
* #GH "Ghana"
* #GI "Gibraltar"
* #GL "Grönland"
* #GM "Gambia"
* #GN "Guinea"
* #GP "Guadeloupe"
* #GQ "Äquatorialguinea"
* #GR "Griechenland"
* #GT "Guatemala"
* #GU "Guam"
* #GW "Guinea-Bissau"
* #GY "Guyana"
* #HK "Hongkong"
* #HM "Heard/McDonaldi"
* #HN "Honduras"
* #HR "Kroatien"
* #HT "Haiti"
* #HU "Ungarn"
* #ID "Indonesien"
* #IE "Irland"
* #IL "Israel"
* #IM "Isle of Man (nur SV)"
* #IN "Indien"
* #IO "Br.Terr/Ind.Ozn"
* #IQ "Irak"
* #IR "Iran,Islamische Republik"
* #IS "Island"
* #IT "Italien"
* #JE "Jersey (nur SV)"
* #JM "Jamaika"
* #JO "Jordanien"
* #JP "Japan"
* #KE "Kenia"
* #KG "Kirgisistan"
* #KH "Kambodscha"
* #KI "Kiribati"
* #KM "Komoren"
* #KN "St.Kitts und Nevis"
* #KP "Korea,Dem.Volksrepublik"
* #KR "Korea,Republik"
* #KS "Kosovo, Republik"
* #KW "Kuwait"
* #KY "Kaimaninseln"
* #KZ "Kasachstan"
* #LA "Laos,Demokr.Volksrepublik"
* #LB "Libanon"
* #LC "St.Lucia"
* #LI "Liechtenstein"
* #LK "Sri Lanka"
* #LR "Liberia"
* #LS "Lesotho"
* #LT "Litauen"
* #LU "Luxemburg"
* #LV "Lettland"
* #LY "Libysch-Arab.Dschamahir."
* #MA "Marokko"
* #MC "Monaco"
* #MD "Moldau"
* #ME "Montenegro"
* #MG "Madagaskar"
* #MH "Marshallinseln"
* #MK "Mazedonien,ehem.jug.Rep."
* #ML "Mali"
* #MM "Myanmar (früher:Birma)"
* #MN "Mongolei"
* #MO "Macau"
* #MP "Marianen,Nördl."
* #MQ "Martinique"
* #MR "Mauretanien"
* #MS "Montserrat"
* #MT "Malta"
* #MU "Mauritius"
* #MV "Malediven"
* #MW "Malawi"
* #MX "Mexiko"
* #MY "Malaysia"
* #MZ "Mosambik"
* #NA "Namibia"
* #NC "Neukaledonien"
* #NE "Niger"
* #NF "Norfolkinsel"
* #NG "Nigeria"
* #NI "Nicaragua"
* #NL "Niederlande"
* #NO "Norwegen"
* #NP "Nepal"
* #NR "Nauru"
* #NU "Niue"
* #NZ "Neuseeland"
* #OM "Oman"
* #PA "Panama"
* #PE "Peru"
* #PF "Franz.Polynesien"
* #PG "Papua-Neuguinea"
* #PH "Philippinen"
* #PK "Pakistan"
* #PL "Polen"
* #PM "St.Pierre/Miquel"
* #PN "Pitcairninseln"
* #PR "Puerto Rico"
* #PS "Paläst/Wjld-Gaza"
* #PT "Portugal"
* #PW "Palau"
* #PY "Paraguay"
* #QA "Katar"
* #RE "Reunion"
* #RO "Rumänien"
* #RS "Serbien"
* #RU "Russische Föderation"
* #RW "Ruanda"
* #SA "Saudi-Arabien"
* #SB "Salomonen"
* #SC "Seychellen"
* #SD "Sudan"
* #SE "Schweden"
* #SG "Singapur"
* #SH "Sankt Helena"
* #SI "Slowenien"
* #SJ "Svalbard/JanMay"
* #SK "Slowakei"
* #SL "Sierra Leone"
* #SM "San Marino"
* #SN "Senegal"
* #SO "Somalia"
* #SR "Suriname"
* #ST "Sao Tome und Principe"
* #SU "eheml. Sowjetunion"
* #SV "El Salvador"
* #SY "Syrien,Arabische Republik"
* #SZ "Swasiland"
* #TC "Turks/Caicosin"
* #TD "Tschad"
* #TF "Fr.Südgebiete"
* #TG "Togo"
* #TH "Thailand"
* #TJ "Tadschikistan"
* #TK "Tokelau"
* #TL "Timor-Leste"
* #TM "Turkmenistan"
* #TN "Tunesien"
* #TO "Tonga"
* #TP "Osttimor (alt)"
* #TP ^property[0].code = #hints 
* #TP ^property[0].valueString = "Nicht offiziell" 
* #TR "Türkei"
* #TT "Trinidad und Tobago"
* #TV "Tuvalu"
* #TW "Taiwan"
* #TZ "Tansania,Vereinigte Rep."
* #UA "Ukraine"
* #UG "Uganda"
* #UM "Amerik.Ozeanien"
* #US "Vereinigte Staaten"
* #UY "Uruguay"
* #UZ "Usbekistan"
* #VA "Vatikanstadt"
* #VC "St.Vincent u.d.Grenadinen"
* #VE "Venezuela"
* #VG "Br.Jungfernin."
* #VI "Am.Jungfernin."
* #VN "Vietnam"
* #VU "Vanuatu"
* #WF "Wallis/Futuna"
* #WS "Samoa"
* #XK "Kosovo"
* #XK ^property[0].code = #hints 
* #XK ^property[0].valueString = "Nicht offiziell" 
* #XX "Staatenlos"
* #XX ^property[0].code = #hints 
* #XX ^property[0].valueString = "Österreichspezifische Erweiterung" 
* #YE "Jemen"
* #YT "Mayotte"
* #YU "eheml. Yugoslawien"
* #YY "Unbekannt"
* #YY ^property[0].code = #hints 
* #YY ^property[0].valueString = "Österreichspezifische Erweiterung" 
* #ZA "Südafrika"
* #ZM "Sambia"
* #ZW "Simbabwe"
