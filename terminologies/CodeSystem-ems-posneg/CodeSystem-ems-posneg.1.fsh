Instance: ems-posneg 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-posneg" 
* name = "ems-posneg" 
* title = "EMS_PosNeg" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Positiv, Negativ" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.88" 
* date = "2017-01-26" 
* count = 8 
* concept[0].code = #GW
* concept[0].display = "grenzwertig"
* concept[1].code = #NA
* concept[1].display = "nicht anwendbar"
* concept[2].code = #NAW
* concept[2].display = "nicht auswertbar"
* concept[3].code = #NEG
* concept[3].display = "negativ"
* concept[4].code = #NOTEST
* concept[4].display = "nicht durchgeführt"
* concept[5].code = #POS
* concept[5].display = "positiv"
* concept[6].code = #UB
* concept[6].display = "unbestimmt"
* concept[7].code = #UNK
* concept[7].display = "unbekannt"
