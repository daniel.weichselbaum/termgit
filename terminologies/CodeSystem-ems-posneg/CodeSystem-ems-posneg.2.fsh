Instance: ems-posneg 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-posneg" 
* name = "ems-posneg" 
* title = "EMS_PosNeg" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Positiv, Negativ" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.88" 
* date = "2017-01-26" 
* count = 8 
* #GW "grenzwertig"
* #NA "nicht anwendbar"
* #NAW "nicht auswertbar"
* #NEG "negativ"
* #NOTEST "nicht durchgeführt"
* #POS "positiv"
* #UB "unbestimmt"
* #UNK "unbekannt"
