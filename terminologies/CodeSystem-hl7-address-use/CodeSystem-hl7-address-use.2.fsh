Instance: hl7-address-use 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-address-use" 
* name = "hl7-address-use" 
* title = "HL7 Address Use" 
* status = #active 
* content = #complete 
* version = "HL7:AddressUse" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.5.1119" 
* date = "2013-01-10" 
* count = 18 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #_GeneralAddressUse "_GeneralAddressUse"
* #_GeneralAddressUse ^property[0].code = #child 
* #_GeneralAddressUse ^property[0].valueCode = #BAD 
* #_GeneralAddressUse ^property[1].code = #child 
* #_GeneralAddressUse ^property[1].valueCode = #CONF 
* #_GeneralAddressUse ^property[2].code = #child 
* #_GeneralAddressUse ^property[2].valueCode = #H 
* #_GeneralAddressUse ^property[3].code = #child 
* #_GeneralAddressUse ^property[3].valueCode = #TMP 
* #_GeneralAddressUse ^property[4].code = #child 
* #_GeneralAddressUse ^property[4].valueCode = #WP 
* #BAD "bad address"
* #BAD ^property[0].code = #parent 
* #BAD ^property[0].valueCode = #_GeneralAddressUse 
* #CONF "confidential address"
* #CONF ^property[0].code = #parent 
* #CONF ^property[0].valueCode = #_GeneralAddressUse 
* #H "home address"
* #H ^property[0].code = #parent 
* #H ^property[0].valueCode = #_GeneralAddressUse 
* #H ^property[1].code = #child 
* #H ^property[1].valueCode = #HP 
* #H ^property[2].code = #child 
* #H ^property[2].valueCode = #HV 
* #HP "primary home"
* #HP ^property[0].code = #parent 
* #HP ^property[0].valueCode = #H 
* #HV "vacation home"
* #HV ^property[0].code = #parent 
* #HV ^property[0].valueCode = #H 
* #TMP "temporary address"
* #TMP ^property[0].code = #parent 
* #TMP ^property[0].valueCode = #_GeneralAddressUse 
* #WP "work place"
* #WP ^property[0].code = #parent 
* #WP ^property[0].valueCode = #_GeneralAddressUse 
* #WP ^property[1].code = #child 
* #WP ^property[1].valueCode = #DIR 
* #WP ^property[2].code = #child 
* #WP ^property[2].valueCode = #PUB 
* #DIR "direct"
* #DIR ^property[0].code = #parent 
* #DIR ^property[0].valueCode = #WP 
* #PUB "public"
* #PUB ^property[0].code = #parent 
* #PUB ^property[0].valueCode = #WP 
* #_PostalAddressUse "_PostalAddressUse"
* #_PostalAddressUse ^property[0].code = #child 
* #_PostalAddressUse ^property[0].valueCode = #PHYS 
* #_PostalAddressUse ^property[1].code = #child 
* #_PostalAddressUse ^property[1].valueCode = #PST 
* #PHYS "physical visit address"
* #PHYS ^property[0].code = #parent 
* #PHYS ^property[0].valueCode = #_PostalAddressUse 
* #PST "postal address"
* #PST ^property[0].code = #parent 
* #PST ^property[0].valueCode = #_PostalAddressUse 
* #_TelecommunicationAddressUse "_TelecommunicationAddressUse"
* #_TelecommunicationAddressUse ^property[0].code = #child 
* #_TelecommunicationAddressUse ^property[0].valueCode = #AS 
* #_TelecommunicationAddressUse ^property[1].code = #child 
* #_TelecommunicationAddressUse ^property[1].valueCode = #EC 
* #_TelecommunicationAddressUse ^property[2].code = #child 
* #_TelecommunicationAddressUse ^property[2].valueCode = #MC 
* #_TelecommunicationAddressUse ^property[3].code = #child 
* #_TelecommunicationAddressUse ^property[3].valueCode = #PG 
* #AS "answering service"
* #AS ^property[0].code = #parent 
* #AS ^property[0].valueCode = #_TelecommunicationAddressUse 
* #EC "emergency contact"
* #EC ^property[0].code = #parent 
* #EC ^property[0].valueCode = #_TelecommunicationAddressUse 
* #MC "mobile contact)"
* #MC ^property[0].code = #parent 
* #MC ^property[0].valueCode = #_TelecommunicationAddressUse 
* #PG "pager"
* #PG ^property[0].code = #parent 
* #PG ^property[0].valueCode = #_TelecommunicationAddressUse 
