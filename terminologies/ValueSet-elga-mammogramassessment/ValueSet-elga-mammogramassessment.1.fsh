Instance: elga-mammogramassessment 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-mammogramassessment" 
* name = "elga-mammogramassessment" 
* title = "ELGA_MammogramAssessment" 
* status = #active 
* version = "3.3" 
* description = "**Description:** Definition of the values for a mammogram according DICOM Coding Scheme 'BI' from 'Digital Imaging and Communications in Medicine (DICOM) Part 16: Content Mapping Resource'

**Beschreibung:** Definition der Werte für ein Mammogramm entsprechend DICOM Coding Scheme 'BI' aus 'Digital Imaging and Communications in Medicine (DICOM) Part 16: Content Mapping Resource'" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.55" 
* date = "2015-10-05" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-elga-mammogramassessment"
* compose.include[0].concept[0].code = #II.AC.a
* compose.include[0].concept[0].display = "0 - Need additional imaging evaluation"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "0 - braucht zusätzliche Bildgebung" 
* compose.include[0].concept[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[0].designation[1].value = "0 -  braucht zusätzliche Bildgebung" 
* compose.include[0].concept[1].code = #II.AC.b.1
* compose.include[0].concept[1].display = "1 - Negative"
* compose.include[0].concept[1].designation[0].language = #de-AT 
* compose.include[0].concept[1].designation[0].value = "1 - Negativ" 
* compose.include[0].concept[1].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[1].designation[1].value = "1 - negativ (d.h. keine Masse, keine Architekturstörung, kein Kalk, keine Hautverdickung etc.)" 
* compose.include[0].concept[2].code = #II.AC.b.2
* compose.include[0].concept[2].display = "2 - Benign Finding"
* compose.include[0].concept[2].designation[0].language = #de-AT 
* compose.include[0].concept[2].designation[0].value = "2 - Benigner Befund" 
* compose.include[0].concept[2].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[2].designation[1].value = "2 - benigne Befunde (z.B. Zysten, intramammäre Lymphknoten, Brustimplantate, stationäre postoperative Befunde, stationäre Fibroadenome)" 
* compose.include[0].concept[3].code = #II.AC.b.3
* compose.include[0].concept[3].display = "3 - Probably Benign Finding - short interval followup"
* compose.include[0].concept[3].designation[0].language = #de-AT 
* compose.include[0].concept[3].designation[0].value = "3 - Wahrscheinlich benigner Befund, kurzfristige Verlaufskontrolle empfohlen" 
* compose.include[0].concept[3].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[3].designation[1].value = "3 - Wahrscheinlich benigne Befunde, kurzfristige Verlaufskontrolle empfohlen (Läsionen mit scharfen Rändern, ovalärer Form, horizontaler Orientierung, Malignitätsrisiko unter 2 %.)" 
* compose.include[0].concept[4].code = #II.AC.b.4
* compose.include[0].concept[4].display = "4 - Suspicious abnormality, biopsy should be considered"
* compose.include[0].concept[4].designation[0].language = #de-AT 
* compose.include[0].concept[4].designation[0].value = "4 - Suspekter Befund, Biopsie sollte erwogen werden" 
* compose.include[0].concept[4].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[4].designation[1].value = "4 - Verdächtig, Biopsie sollte erwogen werden (Läsionen mit intermediärer Wahrscheinlichkeit für Malignität)" 
* compose.include[0].concept[5].code = #MA.II.A.5.4A
* compose.include[0].concept[5].display = "4A - Low suspicion"
* compose.include[0].concept[5].designation[0].language = #de-AT 
* compose.include[0].concept[5].designation[0].value = "4A - Geringgradiger Verdacht" 
* compose.include[0].concept[5].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[5].designation[1].value = "4A - Geringgradig verdächtiger Befund, Biopsie sollte erwogen werden" 
* compose.include[0].concept[6].code = #MA.II.A.5.4B
* compose.include[0].concept[6].display = "4B - Intermediate suspicion"
* compose.include[0].concept[6].designation[0].language = #de-AT 
* compose.include[0].concept[6].designation[0].value = "4B - Zwischenstuflicher Verdacht" 
* compose.include[0].concept[6].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[6].designation[1].value = "4B - Zwischenstuflich verdächtiger Befund, Biopsie sollte erwogen werden" 
* compose.include[0].concept[7].code = #MA.II.A.5.4C
* compose.include[0].concept[7].display = "4C - Moderate suspicion"
* compose.include[0].concept[7].designation[0].language = #de-AT 
* compose.include[0].concept[7].designation[0].value = "4C - Mäßiger Verdacht" 
* compose.include[0].concept[7].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[7].designation[1].value = "4C - Mäßig verdächtiger Befund, Biopsie sollte erwogen werden" 
* compose.include[0].concept[8].code = #II.AC.b.5
* compose.include[0].concept[8].display = "5 - Highly suggestive of malignancy, take appropriate action"
* compose.include[0].concept[8].designation[0].language = #de-AT 
* compose.include[0].concept[8].designation[0].value = "5 - Hochsuspekt, fast sicher maligne, eine adäquate Aktion sollte folgen" 
* compose.include[0].concept[8].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[8].designation[1].value = "5 - hochsuspekt, fast sicher maligne, eine adäquate Aktion sollte folgen (Risiko auf Malignität von 95 % oder größer)" 
* compose.include[0].concept[9].code = #MA.II.A.5.6
* compose.include[0].concept[9].display = "6 - Known biopsy proven malignancy"
* compose.include[0].concept[9].designation[0].language = #de-AT 
* compose.include[0].concept[9].designation[0].value = "6 - Gesichertes Malignom" 
* compose.include[0].concept[9].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[9].designation[1].value = "6 - Bekannter maligner Befund, mit Biopsie bestätigt" 

* expansion.timestamp = "2022-09-13T14:16:34.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem-elga-mammogramassessment"
* expansion.contains[0].code = #II.AC.a
* expansion.contains[0].display = "0 - Need additional imaging evaluation"
* expansion.contains[0].designation[0].language = #de-AT 
* expansion.contains[0].designation[0].value = "0 - braucht zusätzliche Bildgebung" 
* expansion.contains[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[0].designation[1].value = "0 -  braucht zusätzliche Bildgebung" 
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem-elga-mammogramassessment"
* expansion.contains[1].code = #II.AC.b.1
* expansion.contains[1].display = "1 - Negative"
* expansion.contains[1].designation[0].language = #de-AT 
* expansion.contains[1].designation[0].value = "1 - Negativ" 
* expansion.contains[1].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[1].designation[1].value = "1 - negativ (d.h. keine Masse, keine Architekturstörung, kein Kalk, keine Hautverdickung etc.)" 
* expansion.contains[2].system = "https://termgit.elga.gv.at/CodeSystem-elga-mammogramassessment"
* expansion.contains[2].code = #II.AC.b.2
* expansion.contains[2].display = "2 - Benign Finding"
* expansion.contains[2].designation[0].language = #de-AT 
* expansion.contains[2].designation[0].value = "2 - Benigner Befund" 
* expansion.contains[2].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[2].designation[1].value = "2 - benigne Befunde (z.B. Zysten, intramammäre Lymphknoten, Brustimplantate, stationäre postoperative Befunde, stationäre Fibroadenome)" 
* expansion.contains[3].system = "https://termgit.elga.gv.at/CodeSystem-elga-mammogramassessment"
* expansion.contains[3].code = #II.AC.b.3
* expansion.contains[3].display = "3 - Probably Benign Finding - short interval followup"
* expansion.contains[3].designation[0].language = #de-AT 
* expansion.contains[3].designation[0].value = "3 - Wahrscheinlich benigner Befund, kurzfristige Verlaufskontrolle empfohlen" 
* expansion.contains[3].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[3].designation[1].value = "3 - Wahrscheinlich benigne Befunde, kurzfristige Verlaufskontrolle empfohlen (Läsionen mit scharfen Rändern, ovalärer Form, horizontaler Orientierung, Malignitätsrisiko unter 2 %.)" 
* expansion.contains[4].system = "https://termgit.elga.gv.at/CodeSystem-elga-mammogramassessment"
* expansion.contains[4].code = #II.AC.b.4
* expansion.contains[4].display = "4 - Suspicious abnormality, biopsy should be considered"
* expansion.contains[4].designation[0].language = #de-AT 
* expansion.contains[4].designation[0].value = "4 - Suspekter Befund, Biopsie sollte erwogen werden" 
* expansion.contains[4].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[4].designation[1].value = "4 - Verdächtig, Biopsie sollte erwogen werden (Läsionen mit intermediärer Wahrscheinlichkeit für Malignität)" 
* expansion.contains[4].contains[0].system = "https://termgit.elga.gv.at/CodeSystem-elga-mammogramassessment"
* expansion.contains[4].contains[0].code = #MA.II.A.5.4A
* expansion.contains[4].contains[0].display = "4A - Low suspicion"
* expansion.contains[4].contains[0].designation[0].language = #de-AT 
* expansion.contains[4].contains[0].designation[0].value = "4A - Geringgradiger Verdacht" 
* expansion.contains[4].contains[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[4].contains[0].designation[1].value = "4A - Geringgradig verdächtiger Befund, Biopsie sollte erwogen werden" 
* expansion.contains[4].contains[1].system = "https://termgit.elga.gv.at/CodeSystem-elga-mammogramassessment"
* expansion.contains[4].contains[1].code = #MA.II.A.5.4B
* expansion.contains[4].contains[1].display = "4B - Intermediate suspicion"
* expansion.contains[4].contains[1].designation[0].language = #de-AT 
* expansion.contains[4].contains[1].designation[0].value = "4B - Zwischenstuflicher Verdacht" 
* expansion.contains[4].contains[1].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[4].contains[1].designation[1].value = "4B - Zwischenstuflich verdächtiger Befund, Biopsie sollte erwogen werden" 
* expansion.contains[4].contains[2].system = "https://termgit.elga.gv.at/CodeSystem-elga-mammogramassessment"
* expansion.contains[4].contains[2].code = #MA.II.A.5.4C
* expansion.contains[4].contains[2].display = "4C - Moderate suspicion"
* expansion.contains[4].contains[2].designation[0].language = #de-AT 
* expansion.contains[4].contains[2].designation[0].value = "4C - Mäßiger Verdacht" 
* expansion.contains[4].contains[2].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[4].contains[2].designation[1].value = "4C - Mäßig verdächtiger Befund, Biopsie sollte erwogen werden" 
* expansion.contains[5].system = "https://termgit.elga.gv.at/CodeSystem-elga-mammogramassessment"
* expansion.contains[5].code = #II.AC.b.5
* expansion.contains[5].display = "5 - Highly suggestive of malignancy, take appropriate action"
* expansion.contains[5].designation[0].language = #de-AT 
* expansion.contains[5].designation[0].value = "5 - Hochsuspekt, fast sicher maligne, eine adäquate Aktion sollte folgen" 
* expansion.contains[5].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[5].designation[1].value = "5 - hochsuspekt, fast sicher maligne, eine adäquate Aktion sollte folgen (Risiko auf Malignität von 95 % oder größer)" 
* expansion.contains[6].system = "https://termgit.elga.gv.at/CodeSystem-elga-mammogramassessment"
* expansion.contains[6].code = #MA.II.A.5.6
* expansion.contains[6].display = "6 - Known biopsy proven malignancy"
* expansion.contains[6].designation[0].language = #de-AT 
* expansion.contains[6].designation[0].value = "6 - Gesichertes Malignom" 
* expansion.contains[6].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[6].designation[1].value = "6 - Bekannter maligner Befund, mit Biopsie bestätigt" 
