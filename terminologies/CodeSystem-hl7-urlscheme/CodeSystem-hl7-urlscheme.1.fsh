Instance: hl7-urlscheme 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-urlscheme" 
* name = "hl7-urlscheme" 
* title = "HL7 URLScheme" 
* status = #active 
* content = #complete 
* version = "HL7:URLScheme" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.5.143" 
* date = "2013-01-10" 
* count = 11 
* concept[0].code = #fax
* concept[0].display = "Fax"
* concept[1].code = #file
* concept[1].display = "File"
* concept[2].code = #ftp
* concept[2].display = "FTP"
* concept[3].code = #http
* concept[3].display = "HTTP"
* concept[4].code = #mailto
* concept[4].display = "Mailto"
* concept[5].code = #me
* concept[5].display = "ME-Nummer"
* concept[6].code = #mllp
* concept[6].display = "HL7 Minimal Lower Layer Protocol"
* concept[7].code = #modem
* concept[7].display = "Modem"
* concept[8].code = #nfs
* concept[8].display = "NFS"
* concept[9].code = #tel
* concept[9].display = "Telephone"
* concept[10].code = #telnet
* concept[10].display = "Telnet"
