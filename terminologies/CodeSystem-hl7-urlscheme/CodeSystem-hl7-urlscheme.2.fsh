Instance: hl7-urlscheme 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-urlscheme" 
* name = "hl7-urlscheme" 
* title = "HL7 URLScheme" 
* status = #active 
* content = #complete 
* version = "HL7:URLScheme" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.5.143" 
* date = "2013-01-10" 
* count = 11 
* #fax "Fax"
* #file "File"
* #ftp "FTP"
* #http "HTTP"
* #mailto "Mailto"
* #me "ME-Nummer"
* #mllp "HL7 Minimal Lower Layer Protocol"
* #modem "Modem"
* #nfs "NFS"
* #tel "Telephone"
* #telnet "Telnet"
