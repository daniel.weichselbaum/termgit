Instance: medikationrezeptart 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-medikationrezeptart" 
* name = "medikationrezeptart" 
* title = "MedikationRezeptart" 
* status = #active 
* content = #complete 
* version = "3.0" 
* description = "**Description:** ELGA Codelist for Prescription type

**Beschreibung:** ELGA Codeliste für RezeptArt" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.10.1.4.3.4.3.3" 
* date = "2015-03-31" 
* count = 3 
* #KASSEN "Kassenrezept"
* #KASSEN ^designation[0].language = #de-AT 
* #KASSEN ^designation[0].value = "Kassenrezept" 
* #PRIVAT "Privatrezept"
* #PRIVAT ^designation[0].language = #de-AT 
* #PRIVAT ^designation[0].value = "Privatrezept" 
* #SUBST "Substitutionsrezept"
* #SUBST ^designation[0].language = #de-AT 
* #SUBST ^designation[0].value = "Substitutionsrezept" 
