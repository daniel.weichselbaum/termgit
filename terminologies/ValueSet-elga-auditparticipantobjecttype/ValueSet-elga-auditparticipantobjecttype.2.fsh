Instance: elga-auditparticipantobjecttype 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-auditparticipantobjecttype" 
* name = "elga-auditparticipantobjecttype" 
* title = "ELGA_AuditParticipantObjectType" 
* status = #active 
* version = "3.0" 
* description = "**Beschreibung:** Codeliste der ELGA spezifischen Audit Participant Object Type Codes. Der Audit Participant Object Type beschreibt die Art eines Objekts, welches im Audit Event referenziert ist." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.154" 
* date = "2015-06-01" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-elga-auditparticipantobjecttype"
* compose.include[0].concept[0].code = "100"
* compose.include[0].concept[0].display = "Policy"
* compose.include[0].concept[1].code = "110"
* compose.include[0].concept[1].display = "Kontaktbestätigung"
* compose.include[0].concept[2].code = "120"
* compose.include[0].concept[2].display = "Security Token"
* compose.include[0].concept[3].code = "130"
* compose.include[0].concept[3].display = "Vollmacht"
