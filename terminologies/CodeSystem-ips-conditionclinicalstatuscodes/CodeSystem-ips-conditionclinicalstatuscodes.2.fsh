Instance: ips-conditionclinicalstatuscodes 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ips-conditionclinicalstatuscodes" 
* name = "ips-conditionclinicalstatuscodes" 
* title = "IPS_ConditionClinicalStatusCodes" 
* status = #active 
* content = #complete 
* version = "202005" 
* description = "**Description:** Condition or Problem Status Code

**Beschreibung:** Codeliste für den Status Code eines Problems" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.4.642.3.155" 
* date = "2020-05-25" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* count = 8 
* property[0].code = #hints 
* property[0].type = #string 
* property[1].code = #child 
* property[1].type = #code 
* property[2].code = #parent 
* property[2].type = #code 
* #active "Active"
* #active ^property[0].code = #hints 
* #active ^property[0].valueString = "The subject is currently experiencing the symptoms of the condition or there is evidence of the condition" 
* #active ^property[1].code = #child 
* #active ^property[1].valueCode = #poorly-controlled 
* #active ^property[2].code = #child 
* #active ^property[2].valueCode = #recurrence 
* #active ^property[3].code = #child 
* #active ^property[3].valueCode = #relapse 
* #active ^property[4].code = #child 
* #active ^property[4].valueCode = #well-controlled 
* #poorly-controlled "Poorly controlled"
* #poorly-controlled ^property[0].code = #parent 
* #poorly-controlled ^property[0].valueCode = #active 
* #poorly-controlled ^property[1].code = #hints 
* #poorly-controlled ^property[1].valueString = "The  subject's condition is inadequately/poorly managed such that the recommended evidence-based clinical outcome targets are not met" 
* #recurrence "Recurrence"
* #recurrence ^property[0].code = #parent 
* #recurrence ^property[0].valueCode = #active 
* #recurrence ^property[1].code = #hints 
* #recurrence ^property[1].valueString = "The subject is experiencing a re-occurrence or repeating of a previously resolved condition. Example: recurrence of (previously resolved) urinary tract infection, pancreatitis, cholangitis, conjunctivitis" 
* #relapse "Relapse"
* #relapse ^property[0].code = #parent 
* #relapse ^property[0].valueCode = #active 
* #relapse ^property[1].code = #hints 
* #relapse ^property[1].valueString = "The subject is experiencing a return of a condition, or signs and symptoms after a period of improvement or remission. Examples: relapse of cancer, multiple sclerosis, rheumatoid arthritis, systemic lupus erythematosus, bipolar disorder, [psychotic relapse of] schizophrenia, etc" 
* #well-controlled "Well controlled"
* #well-controlled ^property[0].code = #parent 
* #well-controlled ^property[0].valueCode = #active 
* #well-controlled ^property[1].code = #hints 
* #well-controlled ^property[1].valueString = "The subject's condition is adequately or well managed such that the recommended  evidence-based clinical outcome targets are met" 
* #inactive "Inactive"
* #inactive ^property[0].code = #hints 
* #inactive ^property[0].valueString = "The subject is no longer experiencing the symptoms of the condition or there is no longer evidence of the condition" 
* #inactive ^property[1].code = #child 
* #inactive ^property[1].valueCode = #remission 
* #inactive ^property[2].code = #child 
* #inactive ^property[2].valueCode = #resolved 
* #remission "Remission"
* #remission ^property[0].code = #parent 
* #remission ^property[0].valueCode = #inactive 
* #remission ^property[1].code = #hints 
* #remission ^property[1].valueString = "The subject is no longer experiencing the symptoms of the condition, but there is a risk of the symptoms or condition returning" 
* #resolved "Resolved"
* #resolved ^property[0].code = #parent 
* #resolved ^property[0].valueCode = #inactive 
* #resolved ^property[1].code = #hints 
* #resolved ^property[1].valueString = "The subject is no longer experiencing the symptoms of the condition and there is a negligible perceived risk of the symptoms returning" 
