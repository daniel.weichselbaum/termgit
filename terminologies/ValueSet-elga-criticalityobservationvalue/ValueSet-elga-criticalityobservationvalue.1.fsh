Instance: elga-criticalityobservationvalue 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-criticalityobservationvalue" 
* name = "elga-criticalityobservationvalue" 
* title = "ELGA_CriticalityObservationValue" 
* status = #active 
* version = "202002" 
* description = "**Description:** Criticality of Allergies Or Intolerances. Contains information protected by copyright of SNOMED International. Any use of SNOMED CT in Austria requires a valid affiliate license or sublicense. The corresponding license is free of charge, provided that the use only takes place in Austria and fulfills the conditions of the Affiliate License Agreement. Affiliate licenses can be requested directly from the respective NRC via the [Member Licensing and Distribution Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS).

**Beschreibung:** Kritikalität der Auswirkungen von Allergien und Intoleranzen. Enthält durch SNOMED International urheberrechtlich geschützte Information. Jede Verwendung von SNOMED CT in Österreich erfordert eine aufrechte Affiliate Lizenz oder eine Sublizenz. Die entsprechende Lizenz ist kostenlos, vorausgesetzt die Verwendung findet nur in Österreich statt und erfüllt die Bedingungen des Affiliate License Agreements. Affiliate Lizenzen können über das [Mitglieder-Lizenzierungs- und Distributions-Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS) direkt beim jeweiligen NRC beantragt werden." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.182" 
* date = "2020-02-04" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* compose.include[0].concept[0].code = #723505004
* compose.include[0].concept[0].display = "nicht lebensbedrohlich"
* compose.include[0].concept[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[0].designation[0].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[0].designation[1].value = "geringe Kritikalität " 
* compose.include[0].concept[1].code = #723507007
* compose.include[0].concept[1].display = "Kritikalität unbekannt"
* compose.include[0].concept[1].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[1].designation[0].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[1].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[1].designation[1].value = "Kritikalität unbekannt" 
* compose.include[0].concept[2].code = #723509005
* compose.include[0].concept[2].display = "lebensbedrohlich"
* compose.include[0].concept[2].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[2].designation[0].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[2].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[2].designation[1].value = "hohe Kritikalität " 

* expansion.timestamp = "2022-09-13T14:18:15.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* expansion.contains[0].code = #723505004
* expansion.contains[0].display = "nicht lebensbedrohlich"
* expansion.contains[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[0].designation[0].value = "SNOMED Clinical Terms" 
* expansion.contains[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* expansion.contains[0].designation[1].value = "geringe Kritikalität " 
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* expansion.contains[1].code = #723507007
* expansion.contains[1].display = "Kritikalität unbekannt"
* expansion.contains[1].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[1].designation[0].value = "SNOMED Clinical Terms" 
* expansion.contains[1].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* expansion.contains[1].designation[1].value = "Kritikalität unbekannt" 
* expansion.contains[2].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* expansion.contains[2].code = #723509005
* expansion.contains[2].display = "lebensbedrohlich"
* expansion.contains[2].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[2].designation[0].value = "SNOMED Clinical Terms" 
* expansion.contains[2].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* expansion.contains[2].designation[1].value = "hohe Kritikalität " 
