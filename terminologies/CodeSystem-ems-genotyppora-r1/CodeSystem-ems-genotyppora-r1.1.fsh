Instance: ems-genotyppora-r1 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-genotyppora-r1" 
* name = "ems-genotyppora-r1" 
* title = "EMS_GenotypPorA_R1" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Liste Genotyp PorA variable region 1" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.72" 
* date = "2017-01-26" 
* count = 166 
* concept[0].code = #12
* concept[0].display = "12"
* concept[1].code = #12_1
* concept[1].display = "12-1"
* concept[2].code = #12_10
* concept[2].display = "12-10"
* concept[3].code = #12_11
* concept[3].display = "12-11"
* concept[4].code = #12_12
* concept[4].display = "12-12"
* concept[5].code = #12_13
* concept[5].display = "12-13"
* concept[6].code = #12_14
* concept[6].display = "12-14"
* concept[7].code = #12_15
* concept[7].display = "12-15"
* concept[8].code = #12_16
* concept[8].display = "12-16"
* concept[9].code = #12_17
* concept[9].display = "12-17"
* concept[10].code = #12_18
* concept[10].display = "12-18"
* concept[11].code = #12_2
* concept[11].display = "12-2"
* concept[12].code = #12_3
* concept[12].display = "12-3"
* concept[13].code = #12_4
* concept[13].display = "12-4"
* concept[14].code = #12_5
* concept[14].display = "12-5"
* concept[15].code = #12_6
* concept[15].display = "12-6"
* concept[16].code = #12_7
* concept[16].display = "12-7"
* concept[17].code = #12_8
* concept[17].display = "12-8"
* concept[18].code = #12_9
* concept[18].display = "12-9"
* concept[19].code = #17
* concept[19].display = "17"
* concept[20].code = #17_1
* concept[20].display = "17-1"
* concept[21].code = #17_2
* concept[21].display = "17-2"
* concept[22].code = #17_3
* concept[22].display = "17-3"
* concept[23].code = #18
* concept[23].display = "18"
* concept[24].code = #18_1
* concept[24].display = "18-1"
* concept[25].code = #18_10
* concept[25].display = "18-10"
* concept[26].code = #18_11
* concept[26].display = "18-11"
* concept[27].code = #18_12
* concept[27].display = "18-12"
* concept[28].code = #18_13
* concept[28].display = "18-13"
* concept[29].code = #18_14
* concept[29].display = "18-14"
* concept[30].code = #18_15
* concept[30].display = "18-15"
* concept[31].code = #18_16
* concept[31].display = "18-16"
* concept[32].code = #18_17
* concept[32].display = "18-17"
* concept[33].code = #18_18
* concept[33].display = "18-18"
* concept[34].code = #18_19
* concept[34].display = "18-19"
* concept[35].code = #18_2
* concept[35].display = "18-2"
* concept[36].code = #18_20
* concept[36].display = "18-20"
* concept[37].code = #18_3
* concept[37].display = "18-3"
* concept[38].code = #18_4
* concept[38].display = "18-4"
* concept[39].code = #18_5
* concept[39].display = "18-5"
* concept[40].code = #18_6
* concept[40].display = "18-6"
* concept[41].code = #18_7
* concept[41].display = "18-7"
* concept[42].code = #18_8
* concept[42].display = "18-8"
* concept[43].code = #18_9
* concept[43].display = "18-9"
* concept[44].code = #19
* concept[44].display = "19"
* concept[45].code = #19_1
* concept[45].display = "19-1"
* concept[46].code = #19_10
* concept[46].display = "19-10"
* concept[47].code = #19_11
* concept[47].display = "19-11"
* concept[48].code = #19_12
* concept[48].display = "19-12"
* concept[49].code = #19_13
* concept[49].display = "19-13"
* concept[50].code = #19_14
* concept[50].display = "19-14"
* concept[51].code = #19_15
* concept[51].display = "19-15"
* concept[52].code = #19_16
* concept[52].display = "19-16"
* concept[53].code = #19_17
* concept[53].display = "19-17"
* concept[54].code = #19_18
* concept[54].display = "19-18"
* concept[55].code = #19_19
* concept[55].display = "19-19"
* concept[56].code = #19_2
* concept[56].display = "19-2"
* concept[57].code = #19_20
* concept[57].display = "19-20"
* concept[58].code = #19_21
* concept[58].display = "19-21"
* concept[59].code = #19_22
* concept[59].display = "19-22"
* concept[60].code = #19_23
* concept[60].display = "19-23"
* concept[61].code = #19_24
* concept[61].display = "19-24"
* concept[62].code = #19_25
* concept[62].display = "19-25"
* concept[63].code = #19_26
* concept[63].display = "19-26"
* concept[64].code = #19_27
* concept[64].display = "19-27"
* concept[65].code = #19_28
* concept[65].display = "19-28"
* concept[66].code = #19_29
* concept[66].display = "19-29"
* concept[67].code = #19_3
* concept[67].display = "19-3"
* concept[68].code = #19_4
* concept[68].display = "19-4"
* concept[69].code = #19_5
* concept[69].display = "19-5"
* concept[70].code = #19_6
* concept[70].display = "19-6"
* concept[71].code = #19_7
* concept[71].display = "19-7"
* concept[72].code = #19_8
* concept[72].display = "19-8"
* concept[73].code = #19_9
* concept[73].display = "19-9"
* concept[74].code = #20
* concept[74].display = "20"
* concept[75].code = #20_1
* concept[75].display = "20-1"
* concept[76].code = #20_2
* concept[76].display = "20-2"
* concept[77].code = #20_3
* concept[77].display = "20-3"
* concept[78].code = #20_4
* concept[78].display = "20-4"
* concept[79].code = #21
* concept[79].display = "21"
* concept[80].code = #21_1
* concept[80].display = "21-1"
* concept[81].code = #21_10
* concept[81].display = "21-10"
* concept[82].code = #21_11
* concept[82].display = "21-11"
* concept[83].code = #21_12
* concept[83].display = "21-12"
* concept[84].code = #21_13
* concept[84].display = "21-13"
* concept[85].code = #21_2
* concept[85].display = "21-2"
* concept[86].code = #21_3
* concept[86].display = "21-3"
* concept[87].code = #21_4
* concept[87].display = "21-4"
* concept[88].code = #21_5
* concept[88].display = "21-5"
* concept[89].code = #21_6
* concept[89].display = "21-6"
* concept[90].code = #21_7
* concept[90].display = "21-7"
* concept[91].code = #21_8
* concept[91].display = "21-8"
* concept[92].code = #21_9
* concept[92].display = "21-9"
* concept[93].code = #22
* concept[93].display = "22"
* concept[94].code = #22_1
* concept[94].display = "22-1"
* concept[95].code = #22_10
* concept[95].display = "22-10"
* concept[96].code = #22_11
* concept[96].display = "22-11"
* concept[97].code = #22_12
* concept[97].display = "22-12"
* concept[98].code = #22_13
* concept[98].display = "22-13"
* concept[99].code = #22_14
* concept[99].display = "22-14"
* concept[100].code = #22_15
* concept[100].display = "22-15"
* concept[101].code = #22_16
* concept[101].display = "22-16"
* concept[102].code = #22_17
* concept[102].display = "22-17"
* concept[103].code = #22_18
* concept[103].display = "22-18"
* concept[104].code = #22_19
* concept[104].display = "22-19"
* concept[105].code = #22_2
* concept[105].display = "22-2"
* concept[106].code = #22_3
* concept[106].display = "22-3"
* concept[107].code = #22_4
* concept[107].display = "22-4"
* concept[108].code = #22_5
* concept[108].display = "22-5"
* concept[109].code = #22_6
* concept[109].display = "22-6"
* concept[110].code = #22_7
* concept[110].display = "22-7"
* concept[111].code = #22_8
* concept[111].display = "22-8"
* concept[112].code = #22_9
* concept[112].display = "22-9"
* concept[113].code = #31
* concept[113].display = "31"
* concept[114].code = #5
* concept[114].display = "5"
* concept[115].code = #5_1
* concept[115].display = "5-1"
* concept[116].code = #5_10
* concept[116].display = "5-10"
* concept[117].code = #5_11
* concept[117].display = "5-11"
* concept[118].code = #5_12
* concept[118].display = "5-12"
* concept[119].code = #5_13
* concept[119].display = "5-13"
* concept[120].code = #5_14
* concept[120].display = "5-14"
* concept[121].code = #5_15
* concept[121].display = "5-15"
* concept[122].code = #5_16
* concept[122].display = "5-16"
* concept[123].code = #5_17
* concept[123].display = "5-17"
* concept[124].code = #5_18
* concept[124].display = "5-18"
* concept[125].code = #5_19
* concept[125].display = "5-19"
* concept[126].code = #5_2
* concept[126].display = "5-2"
* concept[127].code = #5_20
* concept[127].display = "5-20"
* concept[128].code = #5_3
* concept[128].display = "5-3"
* concept[129].code = #5_4
* concept[129].display = "5-4"
* concept[130].code = #5_5
* concept[130].display = "5-5"
* concept[131].code = #5_6
* concept[131].display = "5-6"
* concept[132].code = #5_7
* concept[132].display = "5-7"
* concept[133].code = #5_8
* concept[133].display = "5-8"
* concept[134].code = #5_9
* concept[134].display = "5-9"
* concept[135].code = #7
* concept[135].display = "7"
* concept[136].code = #7_1
* concept[136].display = "7-1"
* concept[137].code = #7_10
* concept[137].display = "7-10"
* concept[138].code = #7_11
* concept[138].display = "7-11"
* concept[139].code = #7_12
* concept[139].display = "7-12"
* concept[140].code = #7_13
* concept[140].display = "7-13"
* concept[141].code = #7_14
* concept[141].display = "7-14"
* concept[142].code = #7_15
* concept[142].display = "7-15"
* concept[143].code = #7_16
* concept[143].display = "7-16"
* concept[144].code = #7_17
* concept[144].display = "7-17"
* concept[145].code = #7_18
* concept[145].display = "7-18"
* concept[146].code = #7_19
* concept[146].display = "7-19"
* concept[147].code = #7_2
* concept[147].display = "7-2"
* concept[148].code = #7_20
* concept[148].display = "7-20"
* concept[149].code = #7_21
* concept[149].display = "7-21"
* concept[150].code = #7_22
* concept[150].display = "7-22"
* concept[151].code = #7_23
* concept[151].display = "7-23"
* concept[152].code = #7_24
* concept[152].display = "7-24"
* concept[153].code = #7_25
* concept[153].display = "7-25"
* concept[154].code = #7_26
* concept[154].display = "7-26"
* concept[155].code = #7_27
* concept[155].display = "7-27"
* concept[156].code = #7_28
* concept[156].display = "7-28"
* concept[157].code = #7_3
* concept[157].display = "7-3"
* concept[158].code = #7_4
* concept[158].display = "7-4"
* concept[159].code = #7_5
* concept[159].display = "7-5"
* concept[160].code = #7_6
* concept[160].display = "7-6"
* concept[161].code = #7_7
* concept[161].display = "7-7"
* concept[162].code = #7_8
* concept[162].display = "7-8"
* concept[163].code = #7_9
* concept[163].display = "7-9"
* concept[164].code = #NOTEST
* concept[164].display = "Nicht durchgeführt"
* concept[165].code = #NST
* concept[165].display = "Not subtypeable"
