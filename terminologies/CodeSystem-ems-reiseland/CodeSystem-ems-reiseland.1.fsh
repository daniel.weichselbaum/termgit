Instance: ems-reiseland 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-reiseland" 
* name = "ems-reiseland" 
* title = "EMS_Reiseland" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** Reiseland" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.96" 
* date = "2017-01-26" 
* count = 261 
* concept[0].code = #AD
* concept[0].display = "Andorra"
* concept[1].code = #AE
* concept[1].display = "Vereinigte Arabische Emirate"
* concept[2].code = #AF
* concept[2].display = "Afghanistan"
* concept[3].code = #AG
* concept[3].display = "Antigua & Barbuda"
* concept[4].code = #AI
* concept[4].display = "Anguilla"
* concept[5].code = #AL
* concept[5].display = "Albanien"
* concept[6].code = #AM
* concept[6].display = "Armenien"
* concept[7].code = #AN
* concept[7].display = "Niederländische Antillen"
* concept[8].code = #AO
* concept[8].display = "Angola"
* concept[9].code = #AQ
* concept[9].display = "Antarktis"
* concept[10].code = #AR
* concept[10].display = "Argentinien"
* concept[11].code = #AS
* concept[11].display = "Samoa-Amerikanisch"
* concept[12].code = #AT
* concept[12].display = "Österreich"
* concept[13].code = #AU
* concept[13].display = "Australien"
* concept[14].code = #AW
* concept[14].display = "Aruba"
* concept[15].code = #AX
* concept[15].display = "Aland Islands"
* concept[16].code = #AZ
* concept[16].display = "Aserbaidschan"
* concept[17].code = #BA
* concept[17].display = "Bosnien & Herzegowina"
* concept[18].code = #BB
* concept[18].display = "Barbados"
* concept[19].code = #BD
* concept[19].display = "Bangladesch"
* concept[20].code = #BE
* concept[20].display = "Belgien"
* concept[21].code = #BF
* concept[21].display = "Burkina Faso"
* concept[22].code = #BG
* concept[22].display = "Bulgarien"
* concept[23].code = #BH
* concept[23].display = "Bahrain"
* concept[24].code = #BI
* concept[24].display = "Burundi"
* concept[25].code = #BJ
* concept[25].display = "Benin"
* concept[26].code = #BL
* concept[26].display = "Saint Barthelemy"
* concept[27].code = #BM
* concept[27].display = "Bermuda"
* concept[28].code = #BN
* concept[28].display = "Brunei Darussalam"
* concept[29].code = #BO
* concept[29].display = "Bolivien"
* concept[30].code = #BQ
* concept[30].display = "Bonaire, Sint Eustatius und Saba"
* concept[31].code = #BR
* concept[31].display = "Brasilien"
* concept[32].code = #BS
* concept[32].display = "Bahamas"
* concept[33].code = #BT
* concept[33].display = "Bhutan"
* concept[34].code = #BV
* concept[34].display = "Bouvetinsel"
* concept[35].code = #BW
* concept[35].display = "Botsuana"
* concept[36].code = #BY
* concept[36].display = "Belarus"
* concept[37].code = #BZ
* concept[37].display = "Belize"
* concept[38].code = #CA
* concept[38].display = "Kanada"
* concept[39].code = #CC
* concept[39].display = "Kokosinseln"
* concept[40].code = #CD
* concept[40].display = "Kongo, Demokr. Republik"
* concept[41].code = #CF
* concept[41].display = "Zentralafrikanische Republik"
* concept[42].code = #CG
* concept[42].display = "Kongo"
* concept[43].code = #CH
* concept[43].display = "Schweiz"
* concept[44].code = #CI
* concept[44].display = "Elfenbeinküste"
* concept[45].code = #CK
* concept[45].display = "Cook-Inseln"
* concept[46].code = #CL
* concept[46].display = "Chile"
* concept[47].code = #CM
* concept[47].display = "Kamerun"
* concept[48].code = #CN
* concept[48].display = "China"
* concept[49].code = #CO
* concept[49].display = "Kolumbien"
* concept[50].code = #CR
* concept[50].display = "Costa Rica"
* concept[51].code = #CT
* concept[51].display = "Canton & Enderburyinseln"
* concept[52].code = #CU
* concept[52].display = "Kuba"
* concept[53].code = #CV
* concept[53].display = "Kap Verde"
* concept[54].code = #CW
* concept[54].display = "Curacao"
* concept[55].code = #CX
* concept[55].display = "Weihnachtsinsel"
* concept[56].code = #CY
* concept[56].display = "Zypern"
* concept[57].code = #CZ
* concept[57].display = "Tschechische Republik"
* concept[58].code = #DE
* concept[58].display = "Deutschland"
* concept[59].code = #DJ
* concept[59].display = "Dschibuti"
* concept[60].code = #DK
* concept[60].display = "Dänemark"
* concept[61].code = #DM
* concept[61].display = "Dominica"
* concept[62].code = #DO
* concept[62].display = "Dominikanische Republik"
* concept[63].code = #DZ
* concept[63].display = "Algerien"
* concept[64].code = #EC
* concept[64].display = "Ecuador"
* concept[65].code = #EE
* concept[65].display = "Estland"
* concept[66].code = #EG
* concept[66].display = "Ägypten"
* concept[67].code = #EH
* concept[67].display = "Westsahara Eigenständiger Staat"
* concept[68].code = #ER
* concept[68].display = "Eritrea"
* concept[69].code = #ES
* concept[69].display = "Spanien"
* concept[70].code = #ET
* concept[70].display = "Äthiopien"
* concept[71].code = #FI
* concept[71].display = "Finnland"
* concept[72].code = #FJ
* concept[72].display = "Fidschi"
* concept[73].code = #FK
* concept[73].display = "Falklandinseln"
* concept[74].code = #FM
* concept[74].display = "Mikronesien"
* concept[75].code = #FO
* concept[75].display = "Färoeer Inseln"
* concept[76].code = #FQ
* concept[76].display = "Französische Südpolargebiete"
* concept[77].code = #FR
* concept[77].display = "Frankreich"
* concept[78].code = #GA
* concept[78].display = "Gabun"
* concept[79].code = #GD
* concept[79].display = "Grenada"
* concept[80].code = #GE
* concept[80].display = "Georgien"
* concept[81].code = #GF
* concept[81].display = "Französisch Guayana"
* concept[82].code = #GG
* concept[82].display = "Guernsey Insel"
* concept[83].code = #GH
* concept[83].display = "Ghana"
* concept[84].code = #GI
* concept[84].display = "Gibraltar"
* concept[85].code = #GL
* concept[85].display = "Grönland"
* concept[86].code = #GM
* concept[86].display = "Gambia"
* concept[87].code = #GN
* concept[87].display = "Guinea"
* concept[88].code = #GP
* concept[88].display = "Guadeloupe"
* concept[89].code = #GQ
* concept[89].display = "Äquatorial Guinea"
* concept[90].code = #GR
* concept[90].display = "Griechenland"
* concept[91].code = #GS
* concept[91].display = "Südgeorgien & südl. Sandwich-I."
* concept[92].code = #GT
* concept[92].display = "Guatemala"
* concept[93].code = #GU
* concept[93].display = "Guam US-Verwaltung"
* concept[94].code = #GW
* concept[94].display = "Guinea-Bissau"
* concept[95].code = #GY
* concept[95].display = "Guyana"
* concept[96].code = #HK
* concept[96].display = "Hongkong"
* concept[97].code = #HM
* concept[97].display = "Heard & McDonaldinseln"
* concept[98].code = #HN
* concept[98].display = "Honduras"
* concept[99].code = #HR
* concept[99].display = "Kroatien"
* concept[100].code = #HT
* concept[100].display = "Haiti"
* concept[101].code = #HU
* concept[101].display = "Ungarn"
* concept[102].code = #IC
* concept[102].display = "Kanarische Inseln"
* concept[103].code = #ID
* concept[103].display = "Indonesien"
* concept[104].code = #IE
* concept[104].display = "Irland"
* concept[105].code = #IL
* concept[105].display = "Israel"
* concept[106].code = #IM
* concept[106].display = "Isle of Man"
* concept[107].code = #IN
* concept[107].display = "Indien"
* concept[108].code = #IO
* concept[108].display = "Brit. Territorium im Ind. Ozean"
* concept[109].code = #IQ
* concept[109].display = "Irak"
* concept[110].code = #IR
* concept[110].display = "Iran"
* concept[111].code = #IS
* concept[111].display = "Island"
* concept[112].code = #IT
* concept[112].display = "Italien"
* concept[113].code = #JE
* concept[113].display = "Jersey Insel"
* concept[114].code = #JM
* concept[114].display = "Jamaika"
* concept[115].code = #JO
* concept[115].display = "Jordanien"
* concept[116].code = #JP
* concept[116].display = "Japan"
* concept[117].code = #JT
* concept[117].display = "Johnston Insel"
* concept[118].code = #KE
* concept[118].display = "Kenia"
* concept[119].code = #KG
* concept[119].display = "Kirgisistan"
* concept[120].code = #KH
* concept[120].display = "Kambodscha"
* concept[121].code = #KI
* concept[121].display = "Kiribati"
* concept[122].code = #KM
* concept[122].display = "Komoren"
* concept[123].code = #KN
* concept[123].display = "St. Kitts & Nevis"
* concept[124].code = #KP
* concept[124].display = "Korea, Demokr. Volksrepublik"
* concept[125].code = #KR
* concept[125].display = "Korea, Republik von"
* concept[126].code = #KW
* concept[126].display = "Kuwait"
* concept[127].code = #KY
* concept[127].display = "Kaimaninseln"
* concept[128].code = #KZ
* concept[128].display = "Kasachstan"
* concept[129].code = #LA
* concept[129].display = "Laos"
* concept[130].code = #LB
* concept[130].display = "Libanon"
* concept[131].code = #LC
* concept[131].display = "St. Lucia"
* concept[132].code = #LI
* concept[132].display = "Liechtenstein"
* concept[133].code = #LK
* concept[133].display = "Sri Lanka"
* concept[134].code = #LR
* concept[134].display = "Liberien"
* concept[135].code = #LS
* concept[135].display = "Lesotho"
* concept[136].code = #LT
* concept[136].display = "Litauen"
* concept[137].code = #LU
* concept[137].display = "Luxemburg"
* concept[138].code = #LV
* concept[138].display = "Lettland"
* concept[139].code = #LY
* concept[139].display = "Libysch-Arab. Dschamahirija"
* concept[140].code = #MA
* concept[140].display = "Marokko"
* concept[141].code = #MC
* concept[141].display = "Monaco"
* concept[142].code = #MD
* concept[142].display = "Moldau, Republik"
* concept[143].code = #ME
* concept[143].display = "Montenegro"
* concept[144].code = #MF
* concept[144].display = "Saint Martin (französischer Teil)"
* concept[145].code = #MG
* concept[145].display = "Madagaskar"
* concept[146].code = #MH
* concept[146].display = "Marshall Inseln"
* concept[147].code = #MI
* concept[147].display = "Midway Inseln"
* concept[148].code = #MK
* concept[148].display = "Mazedonien"
* concept[149].code = #ML
* concept[149].display = "Mali"
* concept[150].code = #MM
* concept[150].display = "Myanmar"
* concept[151].code = #MN
* concept[151].display = "Mongolei"
* concept[152].code = #MO
* concept[152].display = "Macao"
* concept[153].code = #MP
* concept[153].display = "Nördliche Marianen"
* concept[154].code = #MQ
* concept[154].display = "Martinique"
* concept[155].code = #MR
* concept[155].display = "Mauretanien"
* concept[156].code = #MS
* concept[156].display = "Montserrat"
* concept[157].code = #MT
* concept[157].display = "Malta"
* concept[158].code = #MU
* concept[158].display = "Mauritius"
* concept[159].code = #MV
* concept[159].display = "Malediven"
* concept[160].code = #MW
* concept[160].display = "Malawi"
* concept[161].code = #MX
* concept[161].display = "Mexiko"
* concept[162].code = #MY
* concept[162].display = "Malaysia"
* concept[163].code = #MZ
* concept[163].display = "Mosambik"
* concept[164].code = #NA
* concept[164].display = "Namibia"
* concept[165].code = #NC
* concept[165].display = "Neukaledonien"
* concept[166].code = #NE
* concept[166].display = "Niger"
* concept[167].code = #NF
* concept[167].display = "Norfolkinsel"
* concept[168].code = #NG
* concept[168].display = "Nigeria"
* concept[169].code = #NI
* concept[169].display = "Nicaragua"
* concept[170].code = #NL
* concept[170].display = "Niederlande"
* concept[171].code = #NO
* concept[171].display = "Norwegen"
* concept[172].code = #NP
* concept[172].display = "Nepal"
* concept[173].code = #NR
* concept[173].display = "Nauru"
* concept[174].code = #NU
* concept[174].display = "Niue"
* concept[175].code = #NZ
* concept[175].display = "Neuseeland"
* concept[176].code = #OM
* concept[176].display = "Oman"
* concept[177].code = #PA
* concept[177].display = "Panama"
* concept[178].code = #PE
* concept[178].display = "Peru"
* concept[179].code = #PF
* concept[179].display = "Französisch Polynesien"
* concept[180].code = #PG
* concept[180].display = "Papua Neuguinea"
* concept[181].code = #PH
* concept[181].display = "Philippinen"
* concept[182].code = #PK
* concept[182].display = "Pakistan"
* concept[183].code = #PL
* concept[183].display = "Polen"
* concept[184].code = #PM
* concept[184].display = "St. Pierre & Miquelon"
* concept[185].code = #PN
* concept[185].display = "Pitcairn Inseln"
* concept[186].code = #PR
* concept[186].display = "Puerto Rico"
* concept[187].code = #PS
* concept[187].display = "Palästina, Besetzt"
* concept[188].code = #PT
* concept[188].display = "Portugal"
* concept[189].code = #PU
* concept[189].display = "Pazifische Inseln US"
* concept[190].code = #PW
* concept[190].display = "Palau"
* concept[191].code = #PY
* concept[191].display = "Paraguay"
* concept[192].code = #PZ
* concept[192].display = "Panama Kanalzone"
* concept[193].code = #QA
* concept[193].display = "Katar"
* concept[194].code = #RE
* concept[194].display = "Réunion"
* concept[195].code = #RO
* concept[195].display = "Rumänien"
* concept[196].code = #RS
* concept[196].display = "Serbien"
* concept[197].code = #RU
* concept[197].display = "Russische Föderation"
* concept[198].code = #RW
* concept[198].display = "Ruanda"
* concept[199].code = #SA
* concept[199].display = "Saudi-Arabien"
* concept[200].code = #SB
* concept[200].display = "Salomonen"
* concept[201].code = #SC
* concept[201].display = "Seychellen"
* concept[202].code = #SD
* concept[202].display = "Sudan"
* concept[203].code = #SE
* concept[203].display = "Schweden"
* concept[204].code = #SG
* concept[204].display = "Singapur"
* concept[205].code = #SH
* concept[205].display = "St. Helena"
* concept[206].code = #SI
* concept[206].display = "Slowenien"
* concept[207].code = #SJ
* concept[207].display = "Spitzbergen & Jan Mayen Inseln"
* concept[208].code = #SK
* concept[208].display = "Slowakei"
* concept[209].code = #SL
* concept[209].display = "Sierra Leone"
* concept[210].code = #SM
* concept[210].display = "San Marino"
* concept[211].code = #SN
* concept[211].display = "Senegal"
* concept[212].code = #SO
* concept[212].display = "Somalia"
* concept[213].code = #SR
* concept[213].display = "Suriname"
* concept[214].code = #ST
* concept[214].display = "São Tomé & Príncipe"
* concept[215].code = #SV
* concept[215].display = "El Salvador"
* concept[216].code = #SX
* concept[216].display = "Sint Maarten (hollländischer Teil)"
* concept[217].code = #SY
* concept[217].display = "Syrien"
* concept[218].code = #SZ
* concept[218].display = "Swasiland"
* concept[219].code = #TC
* concept[219].display = "Turks- & Caicosinseln"
* concept[220].code = #TD
* concept[220].display = "Tschad"
* concept[221].code = #TF
* concept[221].display = "Französische Südgebiete"
* concept[222].code = #TG
* concept[222].display = "Togo"
* concept[223].code = #TH
* concept[223].display = "Thailand"
* concept[224].code = #TJ
* concept[224].display = "Tadschikistan"
* concept[225].code = #TK
* concept[225].display = "Tokelau"
* concept[226].code = #TL
* concept[226].display = "Timor-Leste"
* concept[227].code = #TM
* concept[227].display = "Turkmenistan"
* concept[228].code = #TN
* concept[228].display = "Tunesien"
* concept[229].code = #TO
* concept[229].display = "Tonga"
* concept[230].code = #TR
* concept[230].display = "Türkei"
* concept[231].code = #TT
* concept[231].display = "Trinidad & Tobago"
* concept[232].code = #TV
* concept[232].display = "Tuvalu"
* concept[233].code = #TW
* concept[233].display = "Taiwan"
* concept[234].code = #TZ
* concept[234].display = "Tansania, Vereinigte Republik"
* concept[235].code = #UA
* concept[235].display = "Ukraine"
* concept[236].code = #UG
* concept[236].display = "Uganda"
* concept[237].code = #UK
* concept[237].display = "Großbritannien"
* concept[238].code = #UM
* concept[238].display = "USA (sonstige kleine Inseln)"
* concept[239].code = #US
* concept[239].display = "Vereinigte Staaten"
* concept[240].code = #UY
* concept[240].display = "Uruguay"
* concept[241].code = #UZ
* concept[241].display = "Usbekistan"
* concept[242].code = #VA
* concept[242].display = "Vatikan"
* concept[243].code = #VC
* concept[243].display = "St. Vincent & Grenadinen"
* concept[244].code = #VE
* concept[244].display = "Venezuela"
* concept[245].code = #VG
* concept[245].display = "Jungfern-Inseln Britisch"
* concept[246].code = #VI
* concept[246].display = "Jungfern-Inseln USA"
* concept[247].code = #VN
* concept[247].display = "Vietnam"
* concept[248].code = #VU
* concept[248].display = "Vanuatu"
* concept[249].code = #WF
* concept[249].display = "Wallis und Futuna"
* concept[250].code = #WK
* concept[250].display = "Wake Insel"
* concept[251].code = #WS
* concept[251].display = "Samoa-West"
* concept[252].code = #XC
* concept[252].display = "Ceuta"
* concept[253].code = #XK
* concept[253].display = "Kosovo"
* concept[254].code = #XL
* concept[254].display = "Mellila"
* concept[255].code = #YE
* concept[255].display = "Jemen"
* concept[256].code = #YT
* concept[256].display = "Mayotte"
* concept[257].code = #YU
* concept[257].display = "Ex-Jugoslawien"
* concept[258].code = #ZA
* concept[258].display = "Südafrika"
* concept[259].code = #ZM
* concept[259].display = "Sambia"
* concept[260].code = #ZW
* concept[260].display = "Simbabwe"
