Instance: elga-actcode-abginfo 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-actcode-abginfo" 
* name = "elga-actcode-abginfo" 
* title = "ELGA_ActCode_AbgInfo" 
* status = #active 
* version = "3.0" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.160" 
* date = "2015-03-31" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-elga-actcode"
* compose.include[0].concept[0].code = "ERGINFO"
* compose.include[0].concept[0].display = "Ergänzende Informationen zur Abgabe"
* compose.include[0].concept[1].code = "MAGZUB"
* compose.include[0].concept[1].display = "Ergänzende Informationen zur magistralen Zubereitung"
