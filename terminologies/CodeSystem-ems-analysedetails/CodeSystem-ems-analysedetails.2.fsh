Instance: ems-analysedetails 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-analysedetails" 
* name = "ems-analysedetails" 
* title = "EMS_Analysedetails" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Liste für Analysedetails " 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.60" 
* date = "2017-01-26" 
* count = 183 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #A "Serotyp A"
* #B "Serotyp B"
* #BRUCELLOSE "Brucellose"
* #BRUCELLOSE ^property[0].code = #child 
* #BRUCELLOSE ^property[0].valueCode = #BRUABO 
* #BRUCELLOSE ^property[1].code = #child 
* #BRUCELLOSE ^property[1].valueCode = #BRUCAN 
* #BRUCELLOSE ^property[2].code = #child 
* #BRUCELLOSE ^property[2].valueCode = #BRUMEL 
* #BRUCELLOSE ^property[3].code = #child 
* #BRUCELLOSE ^property[3].valueCode = #BRUOTHER 
* #BRUCELLOSE ^property[4].code = #child 
* #BRUCELLOSE ^property[4].valueCode = #BRUSPP 
* #BRUCELLOSE ^property[5].code = #child 
* #BRUCELLOSE ^property[5].valueCode = #BRUSUI 
* #BRUABO "Brucella abortus"
* #BRUABO ^property[0].code = #parent 
* #BRUABO ^property[0].valueCode = #BRUCELLOSE 
* #BRUCAN "Brucella canis"
* #BRUCAN ^property[0].code = #parent 
* #BRUCAN ^property[0].valueCode = #BRUCELLOSE 
* #BRUMEL "Brucella melitensis"
* #BRUMEL ^property[0].code = #parent 
* #BRUMEL ^property[0].valueCode = #BRUCELLOSE 
* #BRUOTHER "andere Brucelloseart"
* #BRUOTHER ^property[0].code = #parent 
* #BRUOTHER ^property[0].valueCode = #BRUCELLOSE 
* #BRUSPP "Brucelloseart nicht spezifiziert"
* #BRUSPP ^property[0].code = #parent 
* #BRUSPP ^property[0].valueCode = #BRUCELLOSE 
* #BRUSUI "Brucella suis"
* #BRUSUI ^property[0].code = #parent 
* #BRUSUI ^property[0].valueCode = #BRUCELLOSE 
* #CAMPYLOBAKTER "Campylobakter"
* #CAMPYLOBAKTER ^property[0].code = #child 
* #CAMPYLOBAKTER ^property[0].valueCode = #CAMCOL 
* #CAMPYLOBAKTER ^property[1].code = #child 
* #CAMPYLOBAKTER ^property[1].valueCode = #CAMCON 
* #CAMPYLOBAKTER ^property[2].code = #child 
* #CAMPYLOBAKTER ^property[2].valueCode = #CAMCURV 
* #CAMPYLOBAKTER ^property[3].code = #child 
* #CAMPYLOBAKTER ^property[3].valueCode = #CAMFET 
* #CAMPYLOBAKTER ^property[4].code = #child 
* #CAMPYLOBAKTER ^property[4].valueCode = #CAMGRAC 
* #CAMPYLOBAKTER ^property[5].code = #child 
* #CAMPYLOBAKTER ^property[5].valueCode = #CAMHELV 
* #CAMPYLOBAKTER ^property[6].code = #child 
* #CAMPYLOBAKTER ^property[6].valueCode = #CAMHOM 
* #CAMPYLOBAKTER ^property[7].code = #child 
* #CAMPYLOBAKTER ^property[7].valueCode = #CAMHYO 
* #CAMPYLOBAKTER ^property[8].code = #child 
* #CAMPYLOBAKTER ^property[8].valueCode = #CAMJEJ 
* #CAMPYLOBAKTER ^property[9].code = #child 
* #CAMPYLOBAKTER ^property[9].valueCode = #CAMLAN 
* #CAMPYLOBAKTER ^property[10].code = #child 
* #CAMPYLOBAKTER ^property[10].valueCode = #CAMLAR 
* #CAMPYLOBAKTER ^property[11].code = #child 
* #CAMPYLOBAKTER ^property[11].valueCode = #CAMMUCO 
* #CAMPYLOBAKTER ^property[12].code = #child 
* #CAMPYLOBAKTER ^property[12].valueCode = #CAMRECT 
* #CAMPYLOBAKTER ^property[13].code = #child 
* #CAMPYLOBAKTER ^property[13].valueCode = #CAMSHOW 
* #CAMPYLOBAKTER ^property[14].code = #child 
* #CAMPYLOBAKTER ^property[14].valueCode = #CAMSPP 
* #CAMPYLOBAKTER ^property[15].code = #child 
* #CAMPYLOBAKTER ^property[15].valueCode = #CAMSPUT 
* #CAMPYLOBAKTER ^property[16].code = #child 
* #CAMPYLOBAKTER ^property[16].valueCode = #CAMUPS 
* #CAMCOL "C.coli"
* #CAMCOL ^property[0].code = #parent 
* #CAMCOL ^property[0].valueCode = #CAMPYLOBAKTER 
* #CAMCON "C.concisus"
* #CAMCON ^property[0].code = #parent 
* #CAMCON ^property[0].valueCode = #CAMPYLOBAKTER 
* #CAMCURV "C.curvus"
* #CAMCURV ^property[0].code = #parent 
* #CAMCURV ^property[0].valueCode = #CAMPYLOBAKTER 
* #CAMFET "C.fetus"
* #CAMFET ^property[0].code = #parent 
* #CAMFET ^property[0].valueCode = #CAMPYLOBAKTER 
* #CAMGRAC "C.gracilis"
* #CAMGRAC ^property[0].code = #parent 
* #CAMGRAC ^property[0].valueCode = #CAMPYLOBAKTER 
* #CAMHELV "C.helveticus"
* #CAMHELV ^property[0].code = #parent 
* #CAMHELV ^property[0].valueCode = #CAMPYLOBAKTER 
* #CAMHOM "C.hominis"
* #CAMHOM ^property[0].code = #parent 
* #CAMHOM ^property[0].valueCode = #CAMPYLOBAKTER 
* #CAMHYO "C.hyointestinalis"
* #CAMHYO ^property[0].code = #parent 
* #CAMHYO ^property[0].valueCode = #CAMPYLOBAKTER 
* #CAMJEJ "C.jejuni"
* #CAMJEJ ^property[0].code = #parent 
* #CAMJEJ ^property[0].valueCode = #CAMPYLOBAKTER 
* #CAMLAN "C.lanienae"
* #CAMLAN ^property[0].code = #parent 
* #CAMLAN ^property[0].valueCode = #CAMPYLOBAKTER 
* #CAMLAR "C.lari"
* #CAMLAR ^property[0].code = #parent 
* #CAMLAR ^property[0].valueCode = #CAMPYLOBAKTER 
* #CAMMUCO "C.mucosalis"
* #CAMMUCO ^property[0].code = #parent 
* #CAMMUCO ^property[0].valueCode = #CAMPYLOBAKTER 
* #CAMRECT "C.rectus"
* #CAMRECT ^property[0].code = #parent 
* #CAMRECT ^property[0].valueCode = #CAMPYLOBAKTER 
* #CAMSHOW "C.showae"
* #CAMSHOW ^property[0].code = #parent 
* #CAMSHOW ^property[0].valueCode = #CAMPYLOBAKTER 
* #CAMSPP "Campylobactererreger nicht spezifiziert"
* #CAMSPP ^property[0].code = #parent 
* #CAMSPP ^property[0].valueCode = #CAMPYLOBAKTER 
* #CAMSPUT "C.sputorum"
* #CAMSPUT ^property[0].code = #parent 
* #CAMSPUT ^property[0].valueCode = #CAMPYLOBAKTER 
* #CAMUPS "C.upsaliensis"
* #CAMUPS ^property[0].code = #parent 
* #CAMUPS ^property[0].valueCode = #CAMPYLOBAKTER 
* #CLOSTRIDIUM "Clostridium"
* #CLOSTRIDIUM ^property[0].code = #child 
* #CLOSTRIDIUM ^property[0].valueCode = #RIBO_001 
* #CLOSTRIDIUM ^property[1].code = #child 
* #CLOSTRIDIUM ^property[1].valueCode = #RIBO_002 
* #CLOSTRIDIUM ^property[2].code = #child 
* #CLOSTRIDIUM ^property[2].valueCode = #RIBO_003 
* #CLOSTRIDIUM ^property[3].code = #child 
* #CLOSTRIDIUM ^property[3].valueCode = #RIBO_012 
* #CLOSTRIDIUM ^property[4].code = #child 
* #CLOSTRIDIUM ^property[4].valueCode = #RIBO_014 
* #CLOSTRIDIUM ^property[5].code = #child 
* #CLOSTRIDIUM ^property[5].valueCode = #RIBO_015 
* #CLOSTRIDIUM ^property[6].code = #child 
* #CLOSTRIDIUM ^property[6].valueCode = #RIBO_017 
* #CLOSTRIDIUM ^property[7].code = #child 
* #CLOSTRIDIUM ^property[7].valueCode = #RIBO_020 
* #CLOSTRIDIUM ^property[8].code = #child 
* #CLOSTRIDIUM ^property[8].valueCode = #RIBO_023 
* #CLOSTRIDIUM ^property[9].code = #child 
* #CLOSTRIDIUM ^property[9].valueCode = #RIBO_027 
* #CLOSTRIDIUM ^property[10].code = #child 
* #CLOSTRIDIUM ^property[10].valueCode = #RIBO_029 
* #CLOSTRIDIUM ^property[11].code = #child 
* #CLOSTRIDIUM ^property[11].valueCode = #RIBO_046 
* #CLOSTRIDIUM ^property[12].code = #child 
* #CLOSTRIDIUM ^property[12].valueCode = #RIBO_053 
* #CLOSTRIDIUM ^property[13].code = #child 
* #CLOSTRIDIUM ^property[13].valueCode = #RIBO_056 
* #CLOSTRIDIUM ^property[14].code = #child 
* #CLOSTRIDIUM ^property[14].valueCode = #RIBO_066 
* #CLOSTRIDIUM ^property[15].code = #child 
* #CLOSTRIDIUM ^property[15].valueCode = #RIBO_070 
* #CLOSTRIDIUM ^property[16].code = #child 
* #CLOSTRIDIUM ^property[16].valueCode = #RIBO_075 
* #CLOSTRIDIUM ^property[17].code = #child 
* #CLOSTRIDIUM ^property[17].valueCode = #RIBO_077 
* #CLOSTRIDIUM ^property[18].code = #child 
* #CLOSTRIDIUM ^property[18].valueCode = #RIBO_078 
* #CLOSTRIDIUM ^property[19].code = #child 
* #CLOSTRIDIUM ^property[19].valueCode = #RIBO_081 
* #CLOSTRIDIUM ^property[20].code = #child 
* #CLOSTRIDIUM ^property[20].valueCode = #RIBO_087 
* #CLOSTRIDIUM ^property[21].code = #child 
* #CLOSTRIDIUM ^property[21].valueCode = #RIBO_095 
* #CLOSTRIDIUM ^property[22].code = #child 
* #CLOSTRIDIUM ^property[22].valueCode = #RIBO_106 
* #CLOSTRIDIUM ^property[23].code = #child 
* #CLOSTRIDIUM ^property[23].valueCode = #RIBO_117 
* #CLOSTRIDIUM ^property[24].code = #child 
* #CLOSTRIDIUM ^property[24].valueCode = #RIBO_126 
* #CLOSTRIDIUM ^property[25].code = #child 
* #CLOSTRIDIUM ^property[25].valueCode = #RIBO_131 
* #CLOSTRIDIUM ^property[26].code = #child 
* #CLOSTRIDIUM ^property[26].valueCode = #RIBO_AI_1 
* #CLOSTRIDIUM ^property[27].code = #child 
* #CLOSTRIDIUM ^property[27].valueCode = #RIBO_AI_10 
* #CLOSTRIDIUM ^property[28].code = #child 
* #CLOSTRIDIUM ^property[28].valueCode = #RIBO_AI_12 
* #CLOSTRIDIUM ^property[29].code = #child 
* #CLOSTRIDIUM ^property[29].valueCode = #RIBO_AI_14 
* #CLOSTRIDIUM ^property[30].code = #child 
* #CLOSTRIDIUM ^property[30].valueCode = #RIBO_AI_17 
* #CLOSTRIDIUM ^property[31].code = #child 
* #CLOSTRIDIUM ^property[31].valueCode = #RIBO_AI_19 
* #CLOSTRIDIUM ^property[32].code = #child 
* #CLOSTRIDIUM ^property[32].valueCode = #RIBO_AI_2 
* #CLOSTRIDIUM ^property[33].code = #child 
* #CLOSTRIDIUM ^property[33].valueCode = #RIBO_AI_20 
* #CLOSTRIDIUM ^property[34].code = #child 
* #CLOSTRIDIUM ^property[34].valueCode = #RIBO_AI_23 
* #CLOSTRIDIUM ^property[35].code = #child 
* #CLOSTRIDIUM ^property[35].valueCode = #RIBO_AI_27 
* #CLOSTRIDIUM ^property[36].code = #child 
* #CLOSTRIDIUM ^property[36].valueCode = #RIBO_AI_29 
* #CLOSTRIDIUM ^property[37].code = #child 
* #CLOSTRIDIUM ^property[37].valueCode = #RIBO_AI_3 
* #CLOSTRIDIUM ^property[38].code = #child 
* #CLOSTRIDIUM ^property[38].valueCode = #RIBO_AI_50 
* #CLOSTRIDIUM ^property[39].code = #child 
* #CLOSTRIDIUM ^property[39].valueCode = #RIBO_AI_52 
* #CLOSTRIDIUM ^property[40].code = #child 
* #CLOSTRIDIUM ^property[40].valueCode = #RIBO_AI_6 
* #CLOSTRIDIUM ^property[41].code = #child 
* #CLOSTRIDIUM ^property[41].valueCode = #RIBO_AI_8 
* #CLOSTRIDIUM ^property[42].code = #child 
* #CLOSTRIDIUM ^property[42].valueCode = #RIBO_AI_9 
* #CLOSTRIDIUM ^property[43].code = #child 
* #CLOSTRIDIUM ^property[43].valueCode = #RIBO_IHMT_EL_1 
* #CLOSTRIDIUM ^property[44].code = #child 
* #CLOSTRIDIUM ^property[44].valueCode = #RIBO_IHMT_EL_10 
* #CLOSTRIDIUM ^property[45].code = #child 
* #CLOSTRIDIUM ^property[45].valueCode = #RIBO_IHMT_EL_11 
* #CLOSTRIDIUM ^property[46].code = #child 
* #CLOSTRIDIUM ^property[46].valueCode = #RIBO_IHMT_EL_12 
* #CLOSTRIDIUM ^property[47].code = #child 
* #CLOSTRIDIUM ^property[47].valueCode = #RIBO_IHMT_EL_13 
* #CLOSTRIDIUM ^property[48].code = #child 
* #CLOSTRIDIUM ^property[48].valueCode = #RIBO_IHMT_EL_2 
* #CLOSTRIDIUM ^property[49].code = #child 
* #CLOSTRIDIUM ^property[49].valueCode = #RIBO_IHMT_EL_3 
* #CLOSTRIDIUM ^property[50].code = #child 
* #CLOSTRIDIUM ^property[50].valueCode = #RIBO_IHMT_EL_4 
* #CLOSTRIDIUM ^property[51].code = #child 
* #CLOSTRIDIUM ^property[51].valueCode = #RIBO_IHMT_EL_5 
* #CLOSTRIDIUM ^property[52].code = #child 
* #CLOSTRIDIUM ^property[52].valueCode = #RIBO_IHMT_EL_6 
* #CLOSTRIDIUM ^property[53].code = #child 
* #CLOSTRIDIUM ^property[53].valueCode = #RIBO_IHMT_EL_7 
* #CLOSTRIDIUM ^property[54].code = #child 
* #CLOSTRIDIUM ^property[54].valueCode = #RIBO_IHMT_EL_8 
* #CLOSTRIDIUM ^property[55].code = #child 
* #CLOSTRIDIUM ^property[55].valueCode = #RIBO_IHMT_EL_9 
* #RIBO_001 "Ribotyp 001"
* #RIBO_001 ^property[0].code = #parent 
* #RIBO_001 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_002 "Ribotyp 002"
* #RIBO_002 ^property[0].code = #parent 
* #RIBO_002 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_003 "Ribotyp 003"
* #RIBO_003 ^property[0].code = #parent 
* #RIBO_003 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_012 "Ribotyp 012"
* #RIBO_012 ^property[0].code = #parent 
* #RIBO_012 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_014 "Ribotyp 014"
* #RIBO_014 ^property[0].code = #parent 
* #RIBO_014 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_015 "Ribotyp 015"
* #RIBO_015 ^property[0].code = #parent 
* #RIBO_015 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_017 "Ribotyp 017"
* #RIBO_017 ^property[0].code = #parent 
* #RIBO_017 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_020 "Ribotyp 020"
* #RIBO_020 ^property[0].code = #parent 
* #RIBO_020 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_023 "Ribotyp 023"
* #RIBO_023 ^property[0].code = #parent 
* #RIBO_023 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_027 "Ribotyp 027"
* #RIBO_027 ^property[0].code = #parent 
* #RIBO_027 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_029 "Ribotyp 029"
* #RIBO_029 ^property[0].code = #parent 
* #RIBO_029 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_046 "Ribotyp 046"
* #RIBO_046 ^property[0].code = #parent 
* #RIBO_046 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_053 "Ribotyp 053"
* #RIBO_053 ^property[0].code = #parent 
* #RIBO_053 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_056 "Ribotyp 056"
* #RIBO_056 ^property[0].code = #parent 
* #RIBO_056 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_066 "Ribotyp 066"
* #RIBO_066 ^property[0].code = #parent 
* #RIBO_066 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_070 "Ribotyp 070"
* #RIBO_070 ^property[0].code = #parent 
* #RIBO_070 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_075 "Ribotyp 075"
* #RIBO_075 ^property[0].code = #parent 
* #RIBO_075 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_077 "Ribotyp 077"
* #RIBO_077 ^property[0].code = #parent 
* #RIBO_077 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_078 "Ribotyp 078"
* #RIBO_078 ^property[0].code = #parent 
* #RIBO_078 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_081 "Ribotyp 081"
* #RIBO_081 ^property[0].code = #parent 
* #RIBO_081 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_087 "Ribotyp 087"
* #RIBO_087 ^property[0].code = #parent 
* #RIBO_087 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_095 "Ribotyp 095"
* #RIBO_095 ^property[0].code = #parent 
* #RIBO_095 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_106 "Ribotyp 106"
* #RIBO_106 ^property[0].code = #parent 
* #RIBO_106 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_117 "Ribotyp 117"
* #RIBO_117 ^property[0].code = #parent 
* #RIBO_117 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_126 "Ribotyp 126"
* #RIBO_126 ^property[0].code = #parent 
* #RIBO_126 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_131 "Ribotyp 131"
* #RIBO_131 ^property[0].code = #parent 
* #RIBO_131 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_AI_1 "Ribotyp AI-1"
* #RIBO_AI_1 ^property[0].code = #parent 
* #RIBO_AI_1 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_AI_10 "Ribotyp AI-10"
* #RIBO_AI_10 ^property[0].code = #parent 
* #RIBO_AI_10 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_AI_12 "Ribotyp AI-12"
* #RIBO_AI_12 ^property[0].code = #parent 
* #RIBO_AI_12 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_AI_14 "Ribotyp AI-14"
* #RIBO_AI_14 ^property[0].code = #parent 
* #RIBO_AI_14 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_AI_17 "Ribotyp AI-17"
* #RIBO_AI_17 ^property[0].code = #parent 
* #RIBO_AI_17 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_AI_19 "Ribotyp AI-19"
* #RIBO_AI_19 ^property[0].code = #parent 
* #RIBO_AI_19 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_AI_2 "Ribotyp AI-2"
* #RIBO_AI_2 ^property[0].code = #parent 
* #RIBO_AI_2 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_AI_20 "Ribotyp AI-20"
* #RIBO_AI_20 ^property[0].code = #parent 
* #RIBO_AI_20 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_AI_23 "Ribotyp AI-23"
* #RIBO_AI_23 ^property[0].code = #parent 
* #RIBO_AI_23 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_AI_27 "Ribotyp AI-27"
* #RIBO_AI_27 ^property[0].code = #parent 
* #RIBO_AI_27 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_AI_29 "Ribotyp AI-29"
* #RIBO_AI_29 ^property[0].code = #parent 
* #RIBO_AI_29 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_AI_3 "Ribotyp AI-3"
* #RIBO_AI_3 ^property[0].code = #parent 
* #RIBO_AI_3 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_AI_50 "Ribotyp AI-50"
* #RIBO_AI_50 ^property[0].code = #parent 
* #RIBO_AI_50 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_AI_52 "Ribotyp AI-52"
* #RIBO_AI_52 ^property[0].code = #parent 
* #RIBO_AI_52 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_AI_6 "Ribotyp AI-6"
* #RIBO_AI_6 ^property[0].code = #parent 
* #RIBO_AI_6 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_AI_8 "Ribotyp AI-8"
* #RIBO_AI_8 ^property[0].code = #parent 
* #RIBO_AI_8 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_AI_9 "Ribotyp AI-9"
* #RIBO_AI_9 ^property[0].code = #parent 
* #RIBO_AI_9 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_IHMT_EL_1 "Ribotyp IHMT-EL 1"
* #RIBO_IHMT_EL_1 ^property[0].code = #parent 
* #RIBO_IHMT_EL_1 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_IHMT_EL_10 "Ribotyp IHMT-EL 10"
* #RIBO_IHMT_EL_10 ^property[0].code = #parent 
* #RIBO_IHMT_EL_10 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_IHMT_EL_11 "Ribotyp IHMT-EL 11"
* #RIBO_IHMT_EL_11 ^property[0].code = #parent 
* #RIBO_IHMT_EL_11 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_IHMT_EL_12 "Ribotyp IHMT-EL 12"
* #RIBO_IHMT_EL_12 ^property[0].code = #parent 
* #RIBO_IHMT_EL_12 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_IHMT_EL_13 "Ribotyp IHMT-EL 13"
* #RIBO_IHMT_EL_13 ^property[0].code = #parent 
* #RIBO_IHMT_EL_13 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_IHMT_EL_2 "Ribotyp IHMT-EL 2"
* #RIBO_IHMT_EL_2 ^property[0].code = #parent 
* #RIBO_IHMT_EL_2 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_IHMT_EL_3 "Ribotyp IHMT-EL 3"
* #RIBO_IHMT_EL_3 ^property[0].code = #parent 
* #RIBO_IHMT_EL_3 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_IHMT_EL_4 "Ribotyp IHMT-EL 4"
* #RIBO_IHMT_EL_4 ^property[0].code = #parent 
* #RIBO_IHMT_EL_4 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_IHMT_EL_5 "Ribotyp IHMT-EL 5"
* #RIBO_IHMT_EL_5 ^property[0].code = #parent 
* #RIBO_IHMT_EL_5 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_IHMT_EL_6 "Ribotyp IHMT-EL 6"
* #RIBO_IHMT_EL_6 ^property[0].code = #parent 
* #RIBO_IHMT_EL_6 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_IHMT_EL_7 "Ribotyp IHMT-EL 7"
* #RIBO_IHMT_EL_7 ^property[0].code = #parent 
* #RIBO_IHMT_EL_7 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_IHMT_EL_8 "Ribotyp IHMT-EL 8"
* #RIBO_IHMT_EL_8 ^property[0].code = #parent 
* #RIBO_IHMT_EL_8 ^property[0].valueCode = #CLOSTRIDIUM 
* #RIBO_IHMT_EL_9 "Ribotyp IHMT-EL 9"
* #RIBO_IHMT_EL_9 ^property[0].code = #parent 
* #RIBO_IHMT_EL_9 ^property[0].valueCode = #CLOSTRIDIUM 
* #DENGUEFIEBER "Denguefieber"
* #DENGUEFIEBER ^property[0].code = #child 
* #DENGUEFIEBER ^property[0].valueCode = #DEN1 
* #DENGUEFIEBER ^property[1].code = #child 
* #DENGUEFIEBER ^property[1].valueCode = #DEN2 
* #DENGUEFIEBER ^property[2].code = #child 
* #DENGUEFIEBER ^property[2].valueCode = #DEN3 
* #DENGUEFIEBER ^property[3].code = #child 
* #DENGUEFIEBER ^property[3].valueCode = #DEN4 
* #DEN1 "DEN-1"
* #DEN1 ^property[0].code = #parent 
* #DEN1 ^property[0].valueCode = #DENGUEFIEBER 
* #DEN2 "DEN-2"
* #DEN2 ^property[0].code = #parent 
* #DEN2 ^property[0].valueCode = #DENGUEFIEBER 
* #DEN3 "DEN-3"
* #DEN3 ^property[0].code = #parent 
* #DEN3 ^property[0].valueCode = #DENGUEFIEBER 
* #DEN4 "DEN-4"
* #DEN4 ^property[0].code = #parent 
* #DEN4 ^property[0].valueCode = #DENGUEFIEBER 
* #DIPHTHERIE "Diphtherie"
* #DIPHTHERIE ^property[0].code = #child 
* #DIPHTHERIE ^property[0].valueCode = #DIP 
* #DIPHTHERIE ^property[1].code = #child 
* #DIPHTHERIE ^property[1].valueCode = #PTBC 
* #DIPHTHERIE ^property[2].code = #child 
* #DIPHTHERIE ^property[2].valueCode = #ULC 
* #DIP "C.diphtheriae"
* #DIP ^property[0].code = #parent 
* #DIP ^property[0].valueCode = #DIPHTHERIE 
* #PTBC "C.pseudotuberculosis"
* #PTBC ^property[0].code = #parent 
* #PTBC ^property[0].valueCode = #DIPHTHERIE 
* #ULC "C.ulcerans"
* #ULC ^property[0].code = #parent 
* #ULC ^property[0].valueCode = #DIPHTHERIE 
* #E "Serotyp E"
* #ECOLI "E.-coli-Enteritits"
* #ECOLI ^property[0].code = #child 
* #ECOLI ^property[0].valueCode = #DAEC 
* #ECOLI ^property[1].code = #child 
* #ECOLI ^property[1].valueCode = #EAGGEC 
* #ECOLI ^property[2].code = #child 
* #ECOLI ^property[2].valueCode = #EIEC 
* #ECOLI ^property[3].code = #child 
* #ECOLI ^property[3].valueCode = #EPEC 
* #ECOLI ^property[4].code = #child 
* #ECOLI ^property[4].valueCode = #ETEC 
* #DAEC "diffus-adhärente E. coli (DAEC)"
* #DAEC ^property[0].code = #parent 
* #DAEC ^property[0].valueCode = #ECOLI 
* #EAGGEC "enteroaggregative E. coli (EAggEC)"
* #EAGGEC ^property[0].code = #parent 
* #EAGGEC ^property[0].valueCode = #ECOLI 
* #EIEC "enteroinvasive E. coli (EIEC)"
* #EIEC ^property[0].code = #parent 
* #EIEC ^property[0].valueCode = #ECOLI 
* #EPEC "enteropathogene E. coli (EPEC)"
* #EPEC ^property[0].code = #parent 
* #EPEC ^property[0].valueCode = #ECOLI 
* #ETEC "enterotoxische E. coli (ETEC)"
* #ETEC ^property[0].code = #parent 
* #ETEC ^property[0].valueCode = #ECOLI 
* #F "Serotyp F"
* #HANTAVIRUS "Hantavirus"
* #HANTAVIRUS ^property[0].code = #child 
* #HANTAVIRUS ^property[0].valueCode = #ANDV 
* #HANTAVIRUS ^property[1].code = #child 
* #HANTAVIRUS ^property[1].valueCode = #DOBV 
* #HANTAVIRUS ^property[2].code = #child 
* #HANTAVIRUS ^property[2].valueCode = #HTNV 
* #HANTAVIRUS ^property[3].code = #child 
* #HANTAVIRUS ^property[3].valueCode = #KORV 
* #HANTAVIRUS ^property[4].code = #child 
* #HANTAVIRUS ^property[4].valueCode = #PUUV 
* #HANTAVIRUS ^property[5].code = #child 
* #HANTAVIRUS ^property[5].valueCode = #SAAV 
* #HANTAVIRUS ^property[6].code = #child 
* #HANTAVIRUS ^property[6].valueCode = #SEOV 
* #HANTAVIRUS ^property[7].code = #child 
* #HANTAVIRUS ^property[7].valueCode = #SINNV 
* #ANDV "Andes-Virus"
* #ANDV ^property[0].code = #parent 
* #ANDV ^property[0].valueCode = #HANTAVIRUS 
* #DOBV "Dobrava-Belgrad-Virus"
* #DOBV ^property[0].code = #parent 
* #DOBV ^property[0].valueCode = #HANTAVIRUS 
* #HTNV "Hantaan-Virus"
* #HTNV ^property[0].code = #parent 
* #HTNV ^property[0].valueCode = #HANTAVIRUS 
* #KORV "Korea-Fieber-Virus"
* #KORV ^property[0].code = #parent 
* #KORV ^property[0].valueCode = #HANTAVIRUS 
* #PUUV "Puumala-Virus"
* #PUUV ^property[0].code = #parent 
* #PUUV ^property[0].valueCode = #HANTAVIRUS 
* #SAAV "Saaremaa-Virus"
* #SAAV ^property[0].code = #parent 
* #SAAV ^property[0].valueCode = #HANTAVIRUS 
* #SEOV "Seoul-Virus"
* #SEOV ^property[0].code = #parent 
* #SEOV ^property[0].valueCode = #HANTAVIRUS 
* #SINNV "Sin-Nombre-Virus"
* #SINNV ^property[0].code = #parent 
* #SINNV ^property[0].valueCode = #HANTAVIRUS 
* #LASSAFIEBER "Lassafieber"
* #LASSAFIEBER ^property[0].code = #child 
* #LASSAFIEBER ^property[0].valueCode = #CF 
* #LASSAFIEBER ^property[1].code = #child 
* #LASSAFIEBER ^property[1].valueCode = #LR 
* #LASSAFIEBER ^property[2].code = #child 
* #LASSAFIEBER ^property[2].valueCode = #NG 
* #LASSAFIEBER ^property[3].code = #child 
* #LASSAFIEBER ^property[3].valueCode = #SL 
* #CF "Zentralafrikanische Republik"
* #CF ^property[0].code = #parent 
* #CF ^property[0].valueCode = #LASSAFIEBER 
* #LR "Liberia"
* #LR ^property[0].code = #parent 
* #LR ^property[0].valueCode = #LASSAFIEBER 
* #NG "Nigeria"
* #NG ^property[0].code = #parent 
* #NG ^property[0].valueCode = #LASSAFIEBER 
* #SL "Sierra Leone"
* #SL ^property[0].code = #parent 
* #SL ^property[0].valueCode = #LASSAFIEBER 
* #LEGIONELLOSE "Legionellose"
* #LEGIONELLOSE ^property[0].code = #child 
* #LEGIONELLOSE ^property[0].valueCode = #LANISA 
* #LEGIONELLOSE ^property[1].code = #child 
* #LEGIONELLOSE ^property[1].valueCode = #LBOZ 
* #LEGIONELLOSE ^property[2].code = #child 
* #LEGIONELLOSE ^property[2].valueCode = #LBRU 
* #LEGIONELLOSE ^property[3].code = #child 
* #LEGIONELLOSE ^property[3].valueCode = #LCIN 
* #LEGIONELLOSE ^property[4].code = #child 
* #LEGIONELLOSE ^property[4].valueCode = #LDUM 
* #LEGIONELLOSE ^property[5].code = #child 
* #LEGIONELLOSE ^property[5].valueCode = #LFEE 
* #LEGIONELLOSE ^property[6].code = #child 
* #LEGIONELLOSE ^property[6].valueCode = #LGOR 
* #LEGIONELLOSE ^property[7].code = #child 
* #LEGIONELLOSE ^property[7].valueCode = #LJOR 
* #LEGIONELLOSE ^property[8].code = #child 
* #LEGIONELLOSE ^property[8].valueCode = #LLONG 
* #LEGIONELLOSE ^property[9].code = #child 
* #LEGIONELLOSE ^property[9].valueCode = #LMAC 
* #LEGIONELLOSE ^property[10].code = #child 
* #LEGIONELLOSE ^property[10].valueCode = #LMIC 
* #LEGIONELLOSE ^property[11].code = #child 
* #LEGIONELLOSE ^property[11].valueCode = #LO 
* #LEGIONELLOSE ^property[12].code = #child 
* #LEGIONELLOSE ^property[12].valueCode = #LSG1 
* #LEGIONELLOSE ^property[13].code = #child 
* #LEGIONELLOSE ^property[13].valueCode = #LSG10 
* #LEGIONELLOSE ^property[14].code = #child 
* #LEGIONELLOSE ^property[14].valueCode = #LSG11 
* #LEGIONELLOSE ^property[15].code = #child 
* #LEGIONELLOSE ^property[15].valueCode = #LSG12 
* #LEGIONELLOSE ^property[16].code = #child 
* #LEGIONELLOSE ^property[16].valueCode = #LSG13 
* #LEGIONELLOSE ^property[17].code = #child 
* #LEGIONELLOSE ^property[17].valueCode = #LSG14 
* #LEGIONELLOSE ^property[18].code = #child 
* #LEGIONELLOSE ^property[18].valueCode = #LSG15 
* #LEGIONELLOSE ^property[19].code = #child 
* #LEGIONELLOSE ^property[19].valueCode = #LSG16 
* #LEGIONELLOSE ^property[20].code = #child 
* #LEGIONELLOSE ^property[20].valueCode = #LSG2 
* #LEGIONELLOSE ^property[21].code = #child 
* #LEGIONELLOSE ^property[21].valueCode = #LSG3 
* #LEGIONELLOSE ^property[22].code = #child 
* #LEGIONELLOSE ^property[22].valueCode = #LSG4 
* #LEGIONELLOSE ^property[23].code = #child 
* #LEGIONELLOSE ^property[23].valueCode = #LSG5 
* #LEGIONELLOSE ^property[24].code = #child 
* #LEGIONELLOSE ^property[24].valueCode = #LSG6 
* #LEGIONELLOSE ^property[25].code = #child 
* #LEGIONELLOSE ^property[25].valueCode = #LSG7 
* #LEGIONELLOSE ^property[26].code = #child 
* #LEGIONELLOSE ^property[26].valueCode = #LSG8 
* #LEGIONELLOSE ^property[27].code = #child 
* #LEGIONELLOSE ^property[27].valueCode = #LSG9 
* #LEGIONELLOSE ^property[28].code = #child 
* #LEGIONELLOSE ^property[28].valueCode = #LSGMIX 
* #LEGIONELLOSE ^property[29].code = #child 
* #LEGIONELLOSE ^property[29].valueCode = #LSGUNK 
* #LEGIONELLOSE ^property[30].code = #child 
* #LEGIONELLOSE ^property[30].valueCode = #LWAD 
* #LANISA "L. anisa"
* #LANISA ^property[0].code = #parent 
* #LANISA ^property[0].valueCode = #LEGIONELLOSE 
* #LBOZ "L. bozemanii"
* #LBOZ ^property[0].code = #parent 
* #LBOZ ^property[0].valueCode = #LEGIONELLOSE 
* #LBRU "L. brunensis"
* #LBRU ^property[0].code = #parent 
* #LBRU ^property[0].valueCode = #LEGIONELLOSE 
* #LCIN "L. cincinnatiensis"
* #LCIN ^property[0].code = #parent 
* #LCIN ^property[0].valueCode = #LEGIONELLOSE 
* #LDUM "L. dumoffii"
* #LDUM ^property[0].code = #parent 
* #LDUM ^property[0].valueCode = #LEGIONELLOSE 
* #LFEE "L. feeleii"
* #LFEE ^property[0].code = #parent 
* #LFEE ^property[0].valueCode = #LEGIONELLOSE 
* #LGOR "L. gormanii"
* #LGOR ^property[0].code = #parent 
* #LGOR ^property[0].valueCode = #LEGIONELLOSE 
* #LJOR "L. jordanis"
* #LJOR ^property[0].code = #parent 
* #LJOR ^property[0].valueCode = #LEGIONELLOSE 
* #LLONG "L. longbeachae"
* #LLONG ^property[0].code = #parent 
* #LLONG ^property[0].valueCode = #LEGIONELLOSE 
* #LMAC "L. macaechernii"
* #LMAC ^property[0].code = #parent 
* #LMAC ^property[0].valueCode = #LEGIONELLOSE 
* #LMIC "L. micdadei"
* #LMIC ^property[0].code = #parent 
* #LMIC ^property[0].valueCode = #LEGIONELLOSE 
* #LO "L. other species"
* #LO ^property[0].code = #parent 
* #LO ^property[0].valueCode = #LEGIONELLOSE 
* #LSG1 "L. pneumophila serogroup 1"
* #LSG1 ^property[0].code = #parent 
* #LSG1 ^property[0].valueCode = #LEGIONELLOSE 
* #LSG10 "L. pneumophila serogroup 10"
* #LSG10 ^property[0].code = #parent 
* #LSG10 ^property[0].valueCode = #LEGIONELLOSE 
* #LSG11 "L. pneumophila serogroup 11"
* #LSG11 ^property[0].code = #parent 
* #LSG11 ^property[0].valueCode = #LEGIONELLOSE 
* #LSG12 "L. pneumophila serogroup 12"
* #LSG12 ^property[0].code = #parent 
* #LSG12 ^property[0].valueCode = #LEGIONELLOSE 
* #LSG13 "L. pneumophila serogroup 13"
* #LSG13 ^property[0].code = #parent 
* #LSG13 ^property[0].valueCode = #LEGIONELLOSE 
* #LSG14 "L. pneumophila serogroup 14"
* #LSG14 ^property[0].code = #parent 
* #LSG14 ^property[0].valueCode = #LEGIONELLOSE 
* #LSG15 "L. pneumophila serogroup 15"
* #LSG15 ^property[0].code = #parent 
* #LSG15 ^property[0].valueCode = #LEGIONELLOSE 
* #LSG16 "L. pneumophila serogroup 16"
* #LSG16 ^property[0].code = #parent 
* #LSG16 ^property[0].valueCode = #LEGIONELLOSE 
* #LSG2 "L. pneumophila serogroup 2"
* #LSG2 ^property[0].code = #parent 
* #LSG2 ^property[0].valueCode = #LEGIONELLOSE 
* #LSG3 "L. pneumophila serogroup 3"
* #LSG3 ^property[0].code = #parent 
* #LSG3 ^property[0].valueCode = #LEGIONELLOSE 
* #LSG4 "L. pneumophila serogroup 4"
* #LSG4 ^property[0].code = #parent 
* #LSG4 ^property[0].valueCode = #LEGIONELLOSE 
* #LSG5 "L. pneumophila serogroup 5"
* #LSG5 ^property[0].code = #parent 
* #LSG5 ^property[0].valueCode = #LEGIONELLOSE 
* #LSG6 "L. pneumophila serogroup 6"
* #LSG6 ^property[0].code = #parent 
* #LSG6 ^property[0].valueCode = #LEGIONELLOSE 
* #LSG7 "L. pneumophila serogroup 7"
* #LSG7 ^property[0].code = #parent 
* #LSG7 ^property[0].valueCode = #LEGIONELLOSE 
* #LSG8 "L. pneumophila serogroup 8"
* #LSG8 ^property[0].code = #parent 
* #LSG8 ^property[0].valueCode = #LEGIONELLOSE 
* #LSG9 "L. pneumophila serogroup 9"
* #LSG9 ^property[0].code = #parent 
* #LSG9 ^property[0].valueCode = #LEGIONELLOSE 
* #LSGMIX "L. pneumophila serogroup mixed"
* #LSGMIX ^property[0].code = #parent 
* #LSGMIX ^property[0].valueCode = #LEGIONELLOSE 
* #LSGUNK "L. pneumophila serogroup unknown"
* #LSGUNK ^property[0].code = #parent 
* #LSGUNK ^property[0].valueCode = #LEGIONELLOSE 
* #LWAD "L. wadsworthii"
* #LWAD ^property[0].code = #parent 
* #LWAD ^property[0].valueCode = #LEGIONELLOSE 
* #LEPTOSPIROSE "Leptospirose"
* #LEPTOSPIROSE ^property[0].code = #child 
* #LEPTOSPIROSE ^property[0].valueCode = #LAUS 
* #LEPTOSPIROSE ^property[1].code = #child 
* #LEPTOSPIROSE ^property[1].valueCode = #LAUT 
* #LEPTOSPIROSE ^property[2].code = #child 
* #LEPTOSPIROSE ^property[2].valueCode = #LBALL 
* #LEPTOSPIROSE ^property[3].code = #child 
* #LEPTOSPIROSE ^property[3].valueCode = #LBAT 
* #LEPTOSPIROSE ^property[4].code = #child 
* #LEPTOSPIROSE ^property[4].valueCode = #LBRAT 
* #LEPTOSPIROSE ^property[5].code = #child 
* #LEPTOSPIROSE ^property[5].valueCode = #LCAN 
* #LEPTOSPIROSE ^property[6].code = #child 
* #LEPTOSPIROSE ^property[6].valueCode = #LCOP 
* #LEPTOSPIROSE ^property[7].code = #child 
* #LEPTOSPIROSE ^property[7].valueCode = #LGRIP 
* #LEPTOSPIROSE ^property[8].code = #child 
* #LEPTOSPIROSE ^property[8].valueCode = #LHAR 
* #LEPTOSPIROSE ^property[9].code = #child 
* #LEPTOSPIROSE ^property[9].valueCode = #LHEB 
* #LEPTOSPIROSE ^property[10].code = #child 
* #LEPTOSPIROSE ^property[10].valueCode = #LPOM 
* #LEPTOSPIROSE ^property[11].code = #child 
* #LEPTOSPIROSE ^property[11].valueCode = #LPYRO 
* #LEPTOSPIROSE ^property[12].code = #child 
* #LEPTOSPIROSE ^property[12].valueCode = #LSAX 
* #LEPTOSPIROSE ^property[13].code = #child 
* #LEPTOSPIROSE ^property[13].valueCode = #LSEJ 
* #LEPTOSPIROSE ^property[14].code = #child 
* #LEPTOSPIROSE ^property[14].valueCode = #LTAR 
* #LEPTOSPIROSE ^property[15].code = #child 
* #LEPTOSPIROSE ^property[15].valueCode = #LWOLF 
* #LAUS "L. australis ballico"
* #LAUS ^property[0].code = #parent 
* #LAUS ^property[0].valueCode = #LEPTOSPIROSE 
* #LAUT "L. autumnalis Akyani"
* #LAUT ^property[0].code = #parent 
* #LAUT ^property[0].valueCode = #LEPTOSPIROSE 
* #LBALL "L. ballum"
* #LBALL ^property[0].code = #parent 
* #LBALL ^property[0].valueCode = #LEPTOSPIROSE 
* #LBAT "L. bataviae"
* #LBAT ^property[0].code = #parent 
* #LBAT ^property[0].valueCode = #LEPTOSPIROSE 
* #LBRAT "L. bratislava"
* #LBRAT ^property[0].code = #parent 
* #LBRAT ^property[0].valueCode = #LEPTOSPIROSE 
* #LCAN "L. canicola"
* #LCAN ^property[0].code = #parent 
* #LCAN ^property[0].valueCode = #LEPTOSPIROSE 
* #LCOP "L. copenhageni"
* #LCOP ^property[0].code = #parent 
* #LCOP ^property[0].valueCode = #LEPTOSPIROSE 
* #LGRIP "L. grippotyphosa"
* #LGRIP ^property[0].code = #parent 
* #LGRIP ^property[0].valueCode = #LEPTOSPIROSE 
* #LHAR "L. hardjö"
* #LHAR ^property[0].code = #parent 
* #LHAR ^property[0].valueCode = #LEPTOSPIROSE 
* #LHEB "L. hebdomadis"
* #LHEB ^property[0].code = #parent 
* #LHEB ^property[0].valueCode = #LEPTOSPIROSE 
* #LPOM "L. pomona"
* #LPOM ^property[0].code = #parent 
* #LPOM ^property[0].valueCode = #LEPTOSPIROSE 
* #LPYRO "L. pyrogenes"
* #LPYRO ^property[0].code = #parent 
* #LPYRO ^property[0].valueCode = #LEPTOSPIROSE 
* #LSAX "L. saxköbing"
* #LSAX ^property[0].code = #parent 
* #LSAX ^property[0].valueCode = #LEPTOSPIROSE 
* #LSEJ "L. sejrö"
* #LSEJ ^property[0].code = #parent 
* #LSEJ ^property[0].valueCode = #LEPTOSPIROSE 
* #LTAR "L. tarrasovi"
* #LTAR ^property[0].code = #parent 
* #LTAR ^property[0].valueCode = #LEPTOSPIROSE 
* #LWOLF "L. wolfii"
* #LWOLF ^property[0].code = #parent 
* #LWOLF ^property[0].valueCode = #LEPTOSPIROSE 
* #PERTUSSIS "Pertussis"
* #PERTUSSIS ^property[0].code = #child 
* #PERTUSSIS ^property[0].valueCode = #BBRONCH 
* #PERTUSSIS ^property[1].code = #child 
* #PERTUSSIS ^property[1].valueCode = #BPARA 
* #PERTUSSIS ^property[2].code = #child 
* #PERTUSSIS ^property[2].valueCode = #BPERT 
* #BBRONCH "Bordetella bronchiseptica"
* #BBRONCH ^property[0].code = #parent 
* #BBRONCH ^property[0].valueCode = #PERTUSSIS 
* #BPARA "Bordetella parapertussis"
* #BPARA ^property[0].code = #parent 
* #BPARA ^property[0].valueCode = #PERTUSSIS 
* #BPERT "Bordetella pertussis"
* #BPERT ^property[0].code = #parent 
* #BPERT ^property[0].valueCode = #PERTUSSIS 
* #POLIOMYELITIS "Poliomyelitis"
* #POLIOMYELITIS ^property[0].code = #child 
* #POLIOMYELITIS ^property[0].valueCode = #I 
* #POLIOMYELITIS ^property[1].code = #child 
* #POLIOMYELITIS ^property[1].valueCode = #II 
* #POLIOMYELITIS ^property[2].code = #child 
* #POLIOMYELITIS ^property[2].valueCode = #III 
* #I "Typ I (Brunhilde)"
* #I ^property[0].code = #parent 
* #I ^property[0].valueCode = #POLIOMYELITIS 
* #II "Typ II (Lansing)"
* #II ^property[0].code = #parent 
* #II ^property[0].valueCode = #POLIOMYELITIS 
* #III "Typ III (Leon)"
* #III ^property[0].code = #parent 
* #III ^property[0].valueCode = #POLIOMYELITIS 
* #SHIGELLOSE "Shigellose"
* #SHIGELLOSE ^property[0].code = #child 
* #SHIGELLOSE ^property[0].valueCode = #SHIBOY 
* #SHIGELLOSE ^property[1].code = #child 
* #SHIGELLOSE ^property[1].valueCode = #SHIDYS 
* #SHIGELLOSE ^property[2].code = #child 
* #SHIGELLOSE ^property[2].valueCode = #SHIFLE 
* #SHIGELLOSE ^property[3].code = #child 
* #SHIGELLOSE ^property[3].valueCode = #SHISON 
* #SHIGELLOSE ^property[4].code = #child 
* #SHIGELLOSE ^property[4].valueCode = #SHISPP 
* #SHIBOY "Shigella boydii"
* #SHIBOY ^property[0].code = #parent 
* #SHIBOY ^property[0].valueCode = #SHIGELLOSE 
* #SHIDYS "Shigella dysenteriae"
* #SHIDYS ^property[0].code = #parent 
* #SHIDYS ^property[0].valueCode = #SHIGELLOSE 
* #SHIFLE "Shigella flexneri"
* #SHIFLE ^property[0].code = #parent 
* #SHIFLE ^property[0].valueCode = #SHIGELLOSE 
* #SHISON "Shigella sonnei"
* #SHISON ^property[0].code = #parent 
* #SHISON ^property[0].valueCode = #SHIGELLOSE 
* #SHISPP "Shigella nicht spezifiziert"
* #SHISPP ^property[0].code = #parent 
* #SHISPP ^property[0].valueCode = #SHIGELLOSE 
* #TRICHINELLOSE "Trichinellose"
* #TRICHINELLOSE ^property[0].code = #child 
* #TRICHINELLOSE ^property[0].valueCode = #TRCOTHER 
* #TRICHINELLOSE ^property[1].code = #child 
* #TRICHINELLOSE ^property[1].valueCode = #TRCPSE 
* #TRICHINELLOSE ^property[2].code = #child 
* #TRICHINELLOSE ^property[2].valueCode = #TRCSPI 
* #TRICHINELLOSE ^property[3].code = #child 
* #TRICHINELLOSE ^property[3].valueCode = #TRCSSP 
* #TRCOTHER "andere Trichinellose"
* #TRCOTHER ^property[0].code = #parent 
* #TRCOTHER ^property[0].valueCode = #TRICHINELLOSE 
* #TRCPSE "Trichinella pseudospiralis"
* #TRCPSE ^property[0].code = #parent 
* #TRCPSE ^property[0].valueCode = #TRICHINELLOSE 
* #TRCSPI "Trichinella spiralis"
* #TRCSPI ^property[0].code = #parent 
* #TRCSPI ^property[0].valueCode = #TRICHINELLOSE 
* #TRCSSP "Trichinellose unspezifiziert"
* #TRCSSP ^property[0].code = #parent 
* #TRCSSP ^property[0].valueCode = #TRICHINELLOSE 
