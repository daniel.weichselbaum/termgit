Instance: exnds-concepts 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-exnds-concepts" 
* name = "exnds-concepts" 
* title = "EXNDS_Concepts" 
* status = #active 
* content = #complete 
* version = "201301" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.195" 
* date = "2013-01-10" 
* count = 179 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #ABR "Abrechnungs-Kennzeichen"
* #ABR ^property[0].code = #child 
* #ABR ^property[0].valueCode = #ABR_k 
* #ABR ^property[1].code = #child 
* #ABR ^property[1].valueCode = #ABR_o 
* #ABR ^property[2].code = #child 
* #ABR ^property[2].valueCode = #ABR_p 
* #ABR ^property[3].code = #child 
* #ABR ^property[3].valueCode = #ABR_x 
* #ABR_k "Kassen"
* #ABR_k ^property[0].code = #parent 
* #ABR_k ^property[0].valueCode = #ABR 
* #ABR_o "offen"
* #ABR_o ^property[0].code = #parent 
* #ABR_o ^property[0].valueCode = #ABR 
* #ABR_p "privat"
* #ABR_p ^property[0].code = #parent 
* #ABR_p ^property[0].valueCode = #ABR 
* #ABR_x "nicht verrechnen"
* #ABR_x ^property[0].code = #parent 
* #ABR_x ^property[0].valueCode = #ABR 
* #ABS-Daten "ABS Daten"
* #ArnMittelAbgabe "Arzneimittel Abgabeart"
* #ArnMittelAbgabe ^property[0].code = #child 
* #ArnMittelAbgabe ^property[0].valueCode = #ArnMittelAbgabe_B 
* #ArnMittelAbgabe ^property[1].code = #child 
* #ArnMittelAbgabe ^property[1].valueCode = #ArnMittelAbgabe_K 
* #ArnMittelAbgabe ^property[2].code = #child 
* #ArnMittelAbgabe ^property[2].valueCode = #ArnMittelAbgabe_oeA 
* #ArnMittelAbgabe_B "Barverkauf"
* #ArnMittelAbgabe_B ^property[0].code = #parent 
* #ArnMittelAbgabe_B ^property[0].valueCode = #ArnMittelAbgabe 
* #ArnMittelAbgabe_K "Kasse"
* #ArnMittelAbgabe_K ^property[0].code = #parent 
* #ArnMittelAbgabe_K ^property[0].valueCode = #ArnMittelAbgabe 
* #ArnMittelAbgabe_oeA "oeffentliche Apotheke"
* #ArnMittelAbgabe_oeA ^property[0].code = #parent 
* #ArnMittelAbgabe_oeA ^property[0].valueCode = #ArnMittelAbgabe 
* #ArnMittelBez "Arzneimittel Bezahlt"
* #ArnMittelMagBest "Bestandteil einer magistralen Zubereiten"
* #ArnMittelMagOrganizer "Organizer für magistrale Zubereitungen"
* #ArnMittelOrganizer "Arzneimittel-Organizer"
* #ArnMittelPackArt "Arzneimittelpackungsart"
* #ArnMittelPackArt ^property[0].code = #child 
* #ArnMittelPackArt ^property[0].valueCode = #ArnMittelPackArt_AEM 
* #ArnMittelPackArt ^property[1].code = #child 
* #ArnMittelPackArt ^property[1].valueCode = #ArnMittelPackArt_OP 
* #ArnMittelPackArt_AEM "Ärztemuster"
* #ArnMittelPackArt_AEM ^property[0].code = #parent 
* #ArnMittelPackArt_AEM ^property[0].valueCode = #ArnMittelPackArt 
* #ArnMittelPackArt_OP "Originalpackung"
* #ArnMittelPackArt_OP ^property[0].code = #parent 
* #ArnMittelPackArt_OP ^property[0].valueCode = #ArnMittelPackArt 
* #ArnMittelRezGebFrei "Arzneimittel rezeptgebührenbefreit"
* #ArzMittelPreis "Arzneimittelpreis"
* #ArzMittelPreis ^property[0].code = #child 
* #ArzMittelPreis ^property[0].valueCode = #ArzMittelPreisApo 
* #ArzMittelPreis ^property[1].code = #child 
* #ArzMittelPreis ^property[1].valueCode = #ArzMittelPreisTAX 
* #ArzMittelPreisApo "Apotheken-Verkaufspreis des Arzneimittels"
* #ArzMittelPreisApo ^property[0].code = #parent 
* #ArzMittelPreisApo ^property[0].valueCode = #ArzMittelPreis 
* #ArzMittelPreisTAX "TAX-Preis des Arzneimittels"
* #ArzMittelPreisTAX ^property[0].code = #parent 
* #ArzMittelPreisTAX ^property[0].valueCode = #ArzMittelPreis 
* #Attachment "Attachment"
* #BER "Beruf"
* #BLD "Bundeslandcode"
* #BLD ^property[0].code = #child 
* #BLD ^property[0].valueCode = #BLD1 
* #BLD ^property[1].code = #child 
* #BLD ^property[1].valueCode = #BLD2 
* #BLD ^property[2].code = #child 
* #BLD ^property[2].valueCode = #BLD3 
* #BLD ^property[3].code = #child 
* #BLD ^property[3].valueCode = #BLD4 
* #BLD ^property[4].code = #child 
* #BLD ^property[4].valueCode = #BLD5 
* #BLD ^property[5].code = #child 
* #BLD ^property[5].valueCode = #BLD6 
* #BLD ^property[6].code = #child 
* #BLD ^property[6].valueCode = #BLD7 
* #BLD ^property[7].code = #child 
* #BLD ^property[7].valueCode = #BLD8 
* #BLD ^property[8].code = #child 
* #BLD ^property[8].valueCode = #BLD9 
* #BLD1 "Wien"
* #BLD1 ^property[0].code = #parent 
* #BLD1 ^property[0].valueCode = #BLD 
* #BLD2 "Niederösterreich"
* #BLD2 ^property[0].code = #parent 
* #BLD2 ^property[0].valueCode = #BLD 
* #BLD3 "Burgenland"
* #BLD3 ^property[0].code = #parent 
* #BLD3 ^property[0].valueCode = #BLD 
* #BLD4 "Oberösterreich"
* #BLD4 ^property[0].code = #parent 
* #BLD4 ^property[0].valueCode = #BLD 
* #BLD5 "Steiermark"
* #BLD5 ^property[0].code = #parent 
* #BLD5 ^property[0].valueCode = #BLD 
* #BLD6 "Kärnten"
* #BLD6 ^property[0].code = #parent 
* #BLD6 ^property[0].valueCode = #BLD 
* #BLD7 "Salzburg"
* #BLD7 ^property[0].code = #parent 
* #BLD7 ^property[0].valueCode = #BLD 
* #BLD8 "Tirol"
* #BLD8 ^property[0].code = #parent 
* #BLD8 ^property[0].valueCode = #BLD 
* #BLD9 "Vorarlberg"
* #BLD9 ^property[0].code = #parent 
* #BLD9 ^property[0].valueCode = #BLD 
* #BSART "Behandlungsschein"
* #BSART ^definition = Art des Behandlungscheins
* #BSART ^property[0].code = #child 
* #BSART ^property[0].valueCode = #BSART1 
* #BSART ^property[1].code = #child 
* #BSART ^property[1].valueCode = #BSART2 
* #BSART ^property[2].code = #child 
* #BSART ^property[2].valueCode = #BSART3 
* #BSART ^property[3].code = #child 
* #BSART ^property[3].valueCode = #BSART4 
* #BSART ^property[4].code = #child 
* #BSART ^property[4].valueCode = #BSART5 
* #BSART ^property[5].code = #child 
* #BSART ^property[5].valueCode = #BSART6 
* #BSART ^property[6].code = #child 
* #BSART ^property[6].valueCode = #BSART7 
* #BSART ^property[7].code = #child 
* #BSART ^property[7].valueCode = #BSART8 
* #BSART ^property[8].code = #child 
* #BSART ^property[8].valueCode = #BSART9 
* #BSART1 "Krankenschein"
* #BSART1 ^definition = Krankenschein, Hebammengebührenrechnung (nur gültig bei Fachgebiet 70), Abrechnungsformular für MKP-Leistungen
* #BSART1 ^property[0].code = #parent 
* #BSART1 ^property[0].valueCode = #BSART 
* #BSART2 "Überweisungsschein"
* #BSART2 ^property[0].code = #parent 
* #BSART2 ^property[0].valueCode = #BSART 
* #BSART3 "Erste-Hilfe-Schein"
* #BSART3 ^property[0].code = #parent 
* #BSART3 ^property[0].valueCode = #BSART 
* #BSART4 "Vertretungsschein"
* #BSART4 ^property[0].code = #parent 
* #BSART4 ^property[0].valueCode = #BSART 
* #BSART5 "Bereitschaftsdienstschein"
* #BSART5 ^property[0].code = #parent 
* #BSART5 ^property[0].valueCode = #BSART 
* #BSART6 "Vorsorgeuntersuchungsschein"
* #BSART6 ^property[0].code = #parent 
* #BSART6 ^property[0].valueCode = #BSART 
* #BSART7 "Ambulanzschein"
* #BSART7 ^property[0].code = #parent 
* #BSART7 ^property[0].valueCode = #BSART 
* #BSART8 "Ersatzbehandlungsschein"
* #BSART8 ^property[0].code = #parent 
* #BSART8 ^property[0].valueCode = #BSART 
* #BSART9 "Verordnungsschein"
* #BSART9 ^property[0].code = #parent 
* #BSART9 ^property[0].valueCode = #BSART 
* #BefErstDat "Befunderstellungsdatum"
* #BefundArt "Befund Art"
* #BefundArt ^property[0].code = #child 
* #BefundArt ^property[0].valueCode = #BefundDokument 
* #BefundArt ^property[1].code = #child 
* #BefundArt ^property[1].valueCode = #BefundKartEintrag 
* #BefundDokument "Befund Dokument"
* #BefundDokument ^property[0].code = #parent 
* #BefundDokument ^property[0].valueCode = #BefundArt 
* #BefundKartEintrag "Befund Karteieintrages"
* #BefundKartEintrag ^property[0].code = #parent 
* #BefundKartEintrag ^property[0].valueCode = #BefundArt 
* #Behandlungen "Behandlungen"
* #BesKennzeichenHautfarbe "Besondere Kennzeichen / Hautfarbe"
* #CHKZ "Chefarztkennzeichen"
* #CHKZ ^property[0].code = #child 
* #CHKZ ^property[0].valueCode = #CHKZ_J 
* #CHKZ ^property[1].code = #child 
* #CHKZ ^property[1].valueCode = #CHKZ_N 
* #CHKZ ^property[2].code = #child 
* #CHKZ ^property[2].valueCode = #CHKZ_blank 
* #CHKZ_J "ja"
* #CHKZ_J ^property[0].code = #parent 
* #CHKZ_J ^property[0].valueCode = #CHKZ 
* #CHKZ_N "nein"
* #CHKZ_N ^property[0].code = #parent 
* #CHKZ_N ^property[0].valueCode = #CHKZ 
* #CHKZ_blank "Trägerspezifisch"
* #CHKZ_blank ^property[0].code = #parent 
* #CHKZ_blank ^property[0].valueCode = #CHKZ 
* #DatenbankexportEXNDS "Datenbankexport EXNDS"
* #DatenbankexportEXNDS ^property[0].code = #child 
* #DatenbankexportEXNDS ^property[0].valueCode = #EXNDS_Patientendaten 
* #DatenbankexportEXNDS ^property[1].code = #child 
* #DatenbankexportEXNDS ^property[1].valueCode = #EXNDS_SystemdatenONLY 
* #EXNDS_Patientendaten "Datenbankexport EXNDS - Patientendaten"
* #EXNDS_Patientendaten ^property[0].code = #parent 
* #EXNDS_Patientendaten ^property[0].valueCode = #DatenbankexportEXNDS 
* #EXNDS_SystemdatenONLY "Datenbankexport EXNDS - Systemdaten ONLY"
* #EXNDS_SystemdatenONLY ^property[0].code = #parent 
* #EXNDS_SystemdatenONLY ^property[0].valueCode = #DatenbankexportEXNDS 
* #Dienstgeber "Dienstgeber"
* #EKM "EntfernungInKM"
* #ERSTZUW "Erstzuweiser"
* #FremdStaatKenn "Fremdstaaten-Kennzeichen"
* #GRUVU "Grund für Behandlungsschein"
* #GRUVU ^property[0].code = #child 
* #GRUVU ^property[0].valueCode = #GRUVU_Bereitschaftsdienstschein 
* #GRUVU ^property[1].code = #child 
* #GRUVU ^property[1].valueCode = #GRUVU_Verordnungsschein 
* #GRUVU ^property[2].code = #child 
* #GRUVU ^property[2].valueCode = #GRUVU_Vertretungsschein 
* #GRUVU ^property[3].code = #child 
* #GRUVU ^property[3].valueCode = #GRUVU_Vorsorgeunterschungsschein 
* #GRUVU ^property[4].code = #child 
* #GRUVU ^property[4].valueCode = #GRUVU_Überweisungsschein 
* #GRUVU_Bereitschaftsdienstschein "Grund für Bereitschaftsdienstschein"
* #GRUVU_Bereitschaftsdienstschein ^property[0].code = #parent 
* #GRUVU_Bereitschaftsdienstschein ^property[0].valueCode = #GRUVU 
* #GRUVU_Bereitschaftsdienstschein ^property[1].code = #child 
* #GRUVU_Bereitschaftsdienstschein ^property[1].valueCode = #GRUVU51 
* #GRUVU51 "Wochentagsnachtdienst"
* #GRUVU51 ^property[0].code = #parent 
* #GRUVU51 ^property[0].valueCode = #GRUVU_Bereitschaftsdienstschein 
* #GRUVU_Verordnungsschein "Grund für Verordnungsschein"
* #GRUVU_Verordnungsschein ^property[0].code = #parent 
* #GRUVU_Verordnungsschein ^property[0].valueCode = #GRUVU 
* #GRUVU_Verordnungsschein ^property[1].code = #child 
* #GRUVU_Verordnungsschein ^property[1].valueCode = #GRUVU91 
* #GRUVU_Verordnungsschein ^property[2].code = #child 
* #GRUVU_Verordnungsschein ^property[2].valueCode = #GRUVU92 
* #GRUVU_Verordnungsschein ^property[3].code = #child 
* #GRUVU_Verordnungsschein ^property[3].valueCode = #GRUVU93 
* #GRUVU_Verordnungsschein ^property[4].code = #child 
* #GRUVU_Verordnungsschein ^property[4].valueCode = #GRUVU94 
* #GRUVU_Verordnungsschein ^property[5].code = #child 
* #GRUVU_Verordnungsschein ^property[5].valueCode = #GRUVU95 
* #GRUVU91 "bestimmte Leistungen"
* #GRUVU91 ^property[0].code = #parent 
* #GRUVU91 ^property[0].valueCode = #GRUVU_Verordnungsschein 
* #GRUVU92 "Röntgenbefund"
* #GRUVU92 ^property[0].code = #parent 
* #GRUVU92 ^property[0].valueCode = #GRUVU_Verordnungsschein 
* #GRUVU93 "Laborbefund"
* #GRUVU93 ^property[0].code = #parent 
* #GRUVU93 ^property[0].valueCode = #GRUVU_Verordnungsschein 
* #GRUVU94 "Verordnung zur Therapie"
* #GRUVU94 ^property[0].code = #parent 
* #GRUVU94 ^property[0].valueCode = #GRUVU_Verordnungsschein 
* #GRUVU95 "Akupunktur"
* #GRUVU95 ^property[0].code = #parent 
* #GRUVU95 ^property[0].valueCode = #GRUVU_Verordnungsschein 
* #GRUVU_Vertretungsschein "Grund für Vertretungsschein"
* #GRUVU_Vertretungsschein ^property[0].code = #parent 
* #GRUVU_Vertretungsschein ^property[0].valueCode = #GRUVU 
* #GRUVU_Vertretungsschein ^property[1].code = #child 
* #GRUVU_Vertretungsschein ^property[1].valueCode = #GRUVU41 
* #GRUVU_Vertretungsschein ^property[2].code = #child 
* #GRUVU_Vertretungsschein ^property[2].valueCode = #GRUVU42 
* #GRUVU_Vertretungsschein ^property[3].code = #child 
* #GRUVU_Vertretungsschein ^property[3].valueCode = #GRUVU43 
* #GRUVU_Vertretungsschein ^property[4].code = #child 
* #GRUVU_Vertretungsschein ^property[4].valueCode = #GRUVU44 
* #GRUVU_Vertretungsschein ^property[5].code = #child 
* #GRUVU_Vertretungsschein ^property[5].valueCode = #GRUVU45 
* #GRUVU_Vertretungsschein ^property[6].code = #child 
* #GRUVU_Vertretungsschein ^property[6].valueCode = #GRUVU46 
* #GRUVU_Vertretungsschein ^property[7].code = #child 
* #GRUVU_Vertretungsschein ^property[7].valueCode = #GRUVU47 
* #GRUVU41 "Krankheit"
* #GRUVU41 ^property[0].code = #parent 
* #GRUVU41 ^property[0].valueCode = #GRUVU_Vertretungsschein 
* #GRUVU42 "Urlaub"
* #GRUVU42 ^property[0].code = #parent 
* #GRUVU42 ^property[0].valueCode = #GRUVU_Vertretungsschein 
* #GRUVU43 "Fortbildung"
* #GRUVU43 ^property[0].code = #parent 
* #GRUVU43 ^property[0].valueCode = #GRUVU_Vertretungsschein 
* #GRUVU44 "Rücküberweisung nach Krankenstandsvertretung"
* #GRUVU44 ^property[0].code = #parent 
* #GRUVU44 ^property[0].valueCode = #GRUVU_Vertretungsschein 
* #GRUVU45 "Rücküberweisung nach Urlaubsvertretung"
* #GRUVU45 ^property[0].code = #parent 
* #GRUVU45 ^property[0].valueCode = #GRUVU_Vertretungsschein 
* #GRUVU46 "Rücküberweisung nach Fortbildungsvertretung"
* #GRUVU46 ^property[0].code = #parent 
* #GRUVU46 ^property[0].valueCode = #GRUVU_Vertretungsschein 
* #GRUVU47 "Nichterreichbarkeit"
* #GRUVU47 ^property[0].code = #parent 
* #GRUVU47 ^property[0].valueCode = #GRUVU_Vertretungsschein 
* #GRUVU_Vorsorgeunterschungsschein "Grund für Vorsorgeuntersuchungsschein"
* #GRUVU_Vorsorgeunterschungsschein ^property[0].code = #parent 
* #GRUVU_Vorsorgeunterschungsschein ^property[0].valueCode = #GRUVU 
* #GRUVU_Vorsorgeunterschungsschein ^property[1].code = #child 
* #GRUVU_Vorsorgeunterschungsschein ^property[1].valueCode = #GRUVU61 
* #GRUVU_Vorsorgeunterschungsschein ^property[2].code = #child 
* #GRUVU_Vorsorgeunterschungsschein ^property[2].valueCode = #GRUVU62 
* #GRUVU_Vorsorgeunterschungsschein ^property[3].code = #child 
* #GRUVU_Vorsorgeunterschungsschein ^property[3].valueCode = #GRUVU63 
* #GRUVU_Vorsorgeunterschungsschein ^property[4].code = #child 
* #GRUVU_Vorsorgeunterschungsschein ^property[4].valueCode = #GRUVU64 
* #GRUVU61 "chefärztlicher Dienst"
* #GRUVU61 ^property[0].code = #parent 
* #GRUVU61 ^property[0].valueCode = #GRUVU_Vorsorgeunterschungsschein 
* #GRUVU62 "Reihen-, Lehrlingsuntersuchung"
* #GRUVU62 ^property[0].code = #parent 
* #GRUVU62 ^property[0].valueCode = #GRUVU_Vorsorgeunterschungsschein 
* #GRUVU63 "Gesundenuntersuchung ohne kurativer Behandlung"
* #GRUVU63 ^property[0].code = #parent 
* #GRUVU63 ^property[0].valueCode = #GRUVU_Vorsorgeunterschungsschein 
* #GRUVU64 "Gesundenuntersuchung mit kurativer Behandlung"
* #GRUVU64 ^property[0].code = #parent 
* #GRUVU64 ^property[0].valueCode = #GRUVU_Vorsorgeunterschungsschein 
* #GRUVU_Überweisungsschein "Grund für Überweisungsschein"
* #GRUVU_Überweisungsschein ^property[0].code = #parent 
* #GRUVU_Überweisungsschein ^property[0].valueCode = #GRUVU 
* #GRUVU_Überweisungsschein ^property[1].code = #child 
* #GRUVU_Überweisungsschein ^property[1].valueCode = #GRUVU21 
* #GRUVU_Überweisungsschein ^property[2].code = #child 
* #GRUVU_Überweisungsschein ^property[2].valueCode = #GRUVU22 
* #GRUVU_Überweisungsschein ^property[3].code = #child 
* #GRUVU_Überweisungsschein ^property[3].valueCode = #GRUVU23 
* #GRUVU_Überweisungsschein ^property[4].code = #child 
* #GRUVU_Überweisungsschein ^property[4].valueCode = #GRUVU24 
* #GRUVU_Überweisungsschein ^property[5].code = #child 
* #GRUVU_Überweisungsschein ^property[5].valueCode = #GRUVU25 
* #GRUVU_Überweisungsschein ^property[6].code = #child 
* #GRUVU_Überweisungsschein ^property[6].valueCode = #GRUVU26 
* #GRUVU_Überweisungsschein ^property[7].code = #child 
* #GRUVU_Überweisungsschein ^property[7].valueCode = #GRUVU27 
* #GRUVU21 "fachärztliche Untersuchung"
* #GRUVU21 ^property[0].code = #parent 
* #GRUVU21 ^property[0].valueCode = #GRUVU_Überweisungsschein 
* #GRUVU22 "Erste-Hilfe-Leistung wegen Nichterreichbarkeit"
* #GRUVU22 ^property[0].code = #parent 
* #GRUVU22 ^property[0].valueCode = #GRUVU_Überweisungsschein 
* #GRUVU23 "Erste-Hilfe-Leistung im Bereitschaftsdienst"
* #GRUVU23 ^property[0].code = #parent 
* #GRUVU23 ^property[0].valueCode = #GRUVU_Überweisungsschein 
* #GRUVU24 "Röntgenbefund"
* #GRUVU24 ^property[0].code = #parent 
* #GRUVU24 ^property[0].valueCode = #GRUVU_Überweisungsschein 
* #GRUVU25 "Laborbefund"
* #GRUVU25 ^property[0].code = #parent 
* #GRUVU25 ^property[0].valueCode = #GRUVU_Überweisungsschein 
* #GRUVU26 "Ortswechsel"
* #GRUVU26 ^property[0].code = #parent 
* #GRUVU26 ^property[0].valueCode = #GRUVU_Überweisungsschein 
* #GRUVU27 "Übeweisung zur Therapie"
* #GRUVU27 ^property[0].code = #parent 
* #GRUVU27 ^property[0].valueCode = #GRUVU_Überweisungsschein 
* #KOERPERLICHEMERKMALE "Körperliche Merkmale"
* #Karteieintragungen "Karteieintragungen"
* #Karteineintrag "Karteineintrag"
* #Kassenleistung "Kassenleistung"
* #Krankenstand "Krankenstand"
* #Krankenstand ^property[0].code = #child 
* #Krankenstand ^property[0].valueCode = #KrankenstandGrund 
* #Krankenstand ^property[1].code = #child 
* #Krankenstand ^property[1].valueCode = #VorausEnde 
* #KrankenstandGrund "Grund für den Krankenstand"
* #KrankenstandGrund ^property[0].code = #parent 
* #KrankenstandGrund ^property[0].valueCode = #Krankenstand 
* #VorausEnde "Voraussichtliches Ende des Krankenstandes"
* #VorausEnde ^property[0].code = #parent 
* #VorausEnde ^property[0].valueCode = #Krankenstand 
* #MWST "Mehrwertsteuersatz"
* #PatInfoAdOrg "Patienteninformation Administrativ Organizer"
* #RGZ "Regiezuschlag"
* #RZG "Rezeptgebührenbefreit"
* #Saldo "Saldo"
* #Tarif "Tarif"
* #Tarif ^property[0].code = #child 
* #Tarif ^property[0].valueCode = #MNG 
* #Tarif ^property[1].code = #child 
* #Tarif ^property[1].valueCode = #TFG 
* #Tarif ^property[2].code = #child 
* #Tarif ^property[2].valueCode = #Zusatzkennzeichen 
* #MNG "Tarif Menge"
* #MNG ^property[0].code = #parent 
* #MNG ^property[0].valueCode = #Tarif 
* #TFG "Tariffeld"
* #TFG ^property[0].code = #parent 
* #TFG ^property[0].valueCode = #Tarif 
* #Zusatzkennzeichen "Zusatzkennzeichen"
* #Zusatzkennzeichen ^property[0].code = #parent 
* #Zusatzkennzeichen ^property[0].valueCode = #Tarif 
* #TherapieMenge "Therapie Menge"
* #VKT "Versichertenkategorie"
* #VKT ^property[0].code = #child 
* #VKT ^property[0].valueCode = #VKT01 
* #VKT ^property[1].code = #child 
* #VKT ^property[1].valueCode = #VKT05 
* #VKT ^property[2].code = #child 
* #VKT ^property[2].valueCode = #VKT07 
* #VKT ^property[3].code = #child 
* #VKT ^property[3].valueCode = #VKT20 
* #VKT ^property[4].code = #child 
* #VKT ^property[4].valueCode = #VKT21 
* #VKT ^property[5].code = #child 
* #VKT ^property[5].valueCode = #VKT22 
* #VKT ^property[6].code = #child 
* #VKT ^property[6].valueCode = #VKT23 
* #VKT ^property[7].code = #child 
* #VKT ^property[7].valueCode = #VKT24 
* #VKT ^property[8].code = #child 
* #VKT ^property[8].valueCode = #VKT25 
* #VKT ^property[9].code = #child 
* #VKT ^property[9].valueCode = #VKT26 
* #VKT ^property[10].code = #child 
* #VKT ^property[10].valueCode = #VKT29 
* #VKT01 "Erwerbstätige, Arbeitslose, Selbstversicherte, Zivildiener, Asylanten, Flüchtlinge, Mindestsicherungsempfänger"
* #VKT01 ^property[0].code = #parent 
* #VKT01 ^property[0].valueCode = #VKT 
* #VKT05 "Pensionist, Ruhegenussempfänger"
* #VKT05 ^property[0].code = #parent 
* #VKT05 ^property[0].valueCode = #VKT 
* #VKT07 "Kriegshinterbliebener"
* #VKT07 ^property[0].code = #parent 
* #VKT07 ^property[0].valueCode = #VKT 
* #VKT20 "Zugeteilte nach OFG"
* #VKT20 ^property[0].code = #parent 
* #VKT20 ^property[0].valueCode = #VKT 
* #VKT21 "Zugeteilte nach KOVG, HVG"
* #VKT21 ^property[0].code = #parent 
* #VKT21 ^property[0].valueCode = #VKT 
* #VKT22 "Zugeteilte nach KOVG-D"
* #VKT22 ^property[0].code = #parent 
* #VKT22 ^property[0].valueCode = #VKT 
* #VKT23 "Zugeteilte nach STVG, Impfschadengesetz"
* #VKT23 ^property[0].code = #parent 
* #VKT23 ^property[0].valueCode = #VKT 
* #VKT24 "Zugeteilte nach VOG"
* #VKT24 ^property[0].code = #parent 
* #VKT24 ^property[0].valueCode = #VKT 
* #VKT25 "MKP - Nichtversicherte"
* #VKT25 ^property[0].code = #parent 
* #VKT25 ^property[0].valueCode = #VKT 
* #VKT26 "VU- Nichtversicherte"
* #VKT26 ^property[0].code = #parent 
* #VKT26 ^property[0].valueCode = #VKT 
* #VKT29 "Fremdstaaten (De-facto-Versicherte)"
* #VKT29 ^property[0].code = #parent 
* #VKT29 ^property[0].valueCode = #VKT 
* #VSTRL "Leistungszuständiger Versicherungsträger"
* #VSTRL ^property[0].code = #child 
* #VSTRL ^property[0].valueCode = #Betriebskrankenkassen 
* #VSTRL ^property[1].code = #child 
* #VSTRL ^property[1].valueCode = #Krankenfürsorgeanstalten 
* #VSTRL ^property[2].code = #child 
* #VSTRL ^property[2].valueCode = #Pensionsversicherung 
* #VSTRL ^property[3].code = #child 
* #VSTRL ^property[3].valueCode = #Sonderversicherungsträger 
* #VSTRL ^property[4].code = #child 
* #VSTRL ^property[4].valueCode = #Sonstige 
* #VSTRL ^property[5].code = #child 
* #VSTRL ^property[5].valueCode = #Österreichische_Gesundheitskasse 
* #Betriebskrankenkassen "Betriebskrankenkassen"
* #Betriebskrankenkassen ^property[0].code = #parent 
* #Betriebskrankenkassen ^property[0].valueCode = #VSTRL 
* #Betriebskrankenkassen ^property[1].code = #child 
* #Betriebskrankenkassen ^property[1].valueCode = #VSTRL24 
* #Betriebskrankenkassen ^property[2].code = #child 
* #Betriebskrankenkassen ^property[2].valueCode = #VSTRL25 
* #Betriebskrankenkassen ^property[3].code = #child 
* #Betriebskrankenkassen ^property[3].valueCode = #VSTRL26 
* #Betriebskrankenkassen ^property[4].code = #child 
* #Betriebskrankenkassen ^property[4].valueCode = #VSTRL28 
* #VSTRL24 "Mondi"
* #VSTRL24 ^property[0].code = #parent 
* #VSTRL24 ^property[0].valueCode = #Betriebskrankenkassen 
* #VSTRL25 "Voestalpine Bahnsysteme"
* #VSTRL25 ^property[0].code = #parent 
* #VSTRL25 ^property[0].valueCode = #Betriebskrankenkassen 
* #VSTRL26 "Zeltweg"
* #VSTRL26 ^property[0].code = #parent 
* #VSTRL26 ^property[0].valueCode = #Betriebskrankenkassen 
* #VSTRL28 "Kapfenberg"
* #VSTRL28 ^property[0].code = #parent 
* #VSTRL28 ^property[0].valueCode = #Betriebskrankenkassen 
* #Krankenfürsorgeanstalten "Krankenfürsorgeanstalten"
* #Krankenfürsorgeanstalten ^property[0].code = #parent 
* #Krankenfürsorgeanstalten ^property[0].valueCode = #VSTRL 
* #Krankenfürsorgeanstalten ^property[1].code = #child 
* #Krankenfürsorgeanstalten ^property[1].valueCode = #VSTRL1A 
* #Krankenfürsorgeanstalten ^property[2].code = #child 
* #Krankenfürsorgeanstalten ^property[2].valueCode = #VSTRL2A 
* #Krankenfürsorgeanstalten ^property[3].code = #child 
* #Krankenfürsorgeanstalten ^property[3].valueCode = #VSTRL4A 
* #Krankenfürsorgeanstalten ^property[4].code = #child 
* #Krankenfürsorgeanstalten ^property[4].valueCode = #VSTRL4B 
* #Krankenfürsorgeanstalten ^property[5].code = #child 
* #Krankenfürsorgeanstalten ^property[5].valueCode = #VSTRL4C 
* #Krankenfürsorgeanstalten ^property[6].code = #child 
* #Krankenfürsorgeanstalten ^property[6].valueCode = #VSTRL4D 
* #Krankenfürsorgeanstalten ^property[7].code = #child 
* #Krankenfürsorgeanstalten ^property[7].valueCode = #VSTRL4E 
* #Krankenfürsorgeanstalten ^property[8].code = #child 
* #Krankenfürsorgeanstalten ^property[8].valueCode = #VSTRL4F 
* #Krankenfürsorgeanstalten ^property[9].code = #child 
* #Krankenfürsorgeanstalten ^property[9].valueCode = #VSTRL5A 
* #Krankenfürsorgeanstalten ^property[10].code = #child 
* #Krankenfürsorgeanstalten ^property[10].valueCode = #VSTRL6A 
* #Krankenfürsorgeanstalten ^property[11].code = #child 
* #Krankenfürsorgeanstalten ^property[11].valueCode = #VSTRL7A 
* #Krankenfürsorgeanstalten ^property[12].code = #child 
* #Krankenfürsorgeanstalten ^property[12].valueCode = #VSTRL7B 
* #Krankenfürsorgeanstalten ^property[13].code = #child 
* #Krankenfürsorgeanstalten ^property[13].valueCode = #VSTRL8B 
* #Krankenfürsorgeanstalten ^property[14].code = #child 
* #Krankenfürsorgeanstalten ^property[14].valueCode = #VSTRL8C 
* #Krankenfürsorgeanstalten ^property[15].code = #child 
* #Krankenfürsorgeanstalten ^property[15].valueCode = #VSTRL8D 
* #VSTRL1A "Krankenfürsorgeanstalt der Bediensteten der Stadt Wien"
* #VSTRL1A ^property[0].code = #parent 
* #VSTRL1A ^property[0].valueCode = #Krankenfürsorgeanstalten 
* #VSTRL2A "Krankenfürsorgeanstalt der Beamten der Stadtgemeinde Baden"
* #VSTRL2A ^property[0].code = #parent 
* #VSTRL2A ^property[0].valueCode = #Krankenfürsorgeanstalten 
* #VSTRL4A "Krankenfürsorge für die Beamten der Landeshauptstadt Linz"
* #VSTRL4A ^property[0].code = #parent 
* #VSTRL4A ^property[0].valueCode = #Krankenfürsorgeanstalten 
* #VSTRL4B "Kranken? und Unfallfürsorge für oberösterreichische Gemeindebeamte"
* #VSTRL4B ^property[0].code = #parent 
* #VSTRL4B ^property[0].valueCode = #Krankenfürsorgeanstalten 
* #VSTRL4C "Krankenfürsorgeanstalt für oberösterreichische Landesbeamte"
* #VSTRL4C ^property[0].code = #parent 
* #VSTRL4C ^property[0].valueCode = #Krankenfürsorgeanstalten 
* #VSTRL4D "Oberösterreichische Lehrer?Kranken? und Unfallfürsorge"
* #VSTRL4D ^property[0].code = #parent 
* #VSTRL4D ^property[0].valueCode = #Krankenfürsorgeanstalten 
* #VSTRL4E "Krankenfürsorge für die Beamten des Magistrates Steyr"
* #VSTRL4E ^property[0].code = #parent 
* #VSTRL4E ^property[0].valueCode = #Krankenfürsorgeanstalten 
* #VSTRL4F "Krankenfürsorge für oberösterreichische Beamten der Stadt Wels"
* #VSTRL4F ^property[0].code = #parent 
* #VSTRL4F ^property[0].valueCode = #Krankenfürsorgeanstalten 
* #VSTRL5A "Krankenfürsorgeanstalt für die Beamten der Landeshauptstadt Graz"
* #VSTRL5A ^property[0].code = #parent 
* #VSTRL5A ^property[0].valueCode = #Krankenfürsorgeanstalten 
* #VSTRL6A "Krankenfürsorgeanstalt für die Beamten der Stadt Villach"
* #VSTRL6A ^property[0].code = #parent 
* #VSTRL6A ^property[0].valueCode = #Krankenfürsorgeanstalten 
* #VSTRL7A "Krankenfürsorgeanstalt der Magistratsbeamten der Landeshauptstadt Salzburg"
* #VSTRL7A ^property[0].code = #parent 
* #VSTRL7A ^property[0].valueCode = #Krankenfürsorgeanstalten 
* #VSTRL7B "Krankenfürsorgeeinrichtung der Beamten der Stadtgemeinde Hallein"
* #VSTRL7B ^property[0].code = #parent 
* #VSTRL7B ^property[0].valueCode = #Krankenfürsorgeanstalten 
* #VSTRL8B "Kranken? und Unfallfürsorge der Tiroler Gemeindebeamten"
* #VSTRL8B ^property[0].code = #parent 
* #VSTRL8B ^property[0].valueCode = #Krankenfürsorgeanstalten 
* #VSTRL8C "Kranken? und Unfallfürsorge der Tiroler Landesbeamten"
* #VSTRL8C ^property[0].code = #parent 
* #VSTRL8C ^property[0].valueCode = #Krankenfürsorgeanstalten 
* #VSTRL8D "Kranken? und Unfallfürsorge der Tiroler Landeslehrer"
* #VSTRL8D ^property[0].code = #parent 
* #VSTRL8D ^property[0].valueCode = #Krankenfürsorgeanstalten 
* #Pensionsversicherung "Pensionsversicherung"
* #Pensionsversicherung ^property[0].code = #parent 
* #Pensionsversicherung ^property[0].valueCode = #VSTRL 
* #Pensionsversicherung ^property[1].code = #child 
* #Pensionsversicherung ^property[1].valueCode = #VSTRL02 
* #VSTRL02 "Pensionsversicherung"
* #VSTRL02 ^property[0].code = #parent 
* #VSTRL02 ^property[0].valueCode = #Pensionsversicherung 
* #Sonderversicherungsträger "Sonderversicherungsträger"
* #Sonderversicherungsträger ^property[0].code = #parent 
* #Sonderversicherungsträger ^property[0].valueCode = #VSTRL 
* #Sonderversicherungsträger ^property[1].code = #child 
* #Sonderversicherungsträger ^property[1].valueCode = #VSTRL05 
* #Sonderversicherungsträger ^property[2].code = #child 
* #Sonderversicherungsträger ^property[2].valueCode = #VSTRL07 
* #Sonderversicherungsträger ^property[3].code = #child 
* #Sonderversicherungsträger ^property[3].valueCode = #VSTRL40 
* #Sonderversicherungsträger ^property[4].code = #child 
* #Sonderversicherungsträger ^property[4].valueCode = #VSTRL41 
* #Sonderversicherungsträger ^property[5].code = #child 
* #Sonderversicherungsträger ^property[5].valueCode = #VSTRL42 
* #Sonderversicherungsträger ^property[6].code = #child 
* #Sonderversicherungsträger ^property[6].valueCode = #VSTRL43 
* #Sonderversicherungsträger ^property[7].code = #child 
* #Sonderversicherungsträger ^property[7].valueCode = #VSTRL44 
* #Sonderversicherungsträger ^property[8].code = #child 
* #Sonderversicherungsträger ^property[8].valueCode = #VSTRL45 
* #Sonderversicherungsträger ^property[9].code = #child 
* #Sonderversicherungsträger ^property[9].valueCode = #VSTRL46 
* #Sonderversicherungsträger ^property[10].code = #child 
* #Sonderversicherungsträger ^property[10].valueCode = #VSTRL47 
* #Sonderversicherungsträger ^property[11].code = #child 
* #Sonderversicherungsträger ^property[11].valueCode = #VSTRL48 
* #Sonderversicherungsträger ^property[12].code = #child 
* #Sonderversicherungsträger ^property[12].valueCode = #VSTRL49 
* #Sonderversicherungsträger ^property[13].code = #child 
* #Sonderversicherungsträger ^property[13].valueCode = #VSTRL50 
* #Sonderversicherungsträger ^property[14].code = #child 
* #Sonderversicherungsträger ^property[14].valueCode = #VSTRL51 
* #Sonderversicherungsträger ^property[15].code = #child 
* #Sonderversicherungsträger ^property[15].valueCode = #VSTRL52 
* #Sonderversicherungsträger ^property[16].code = #child 
* #Sonderversicherungsträger ^property[16].valueCode = #VSTRL53 
* #Sonderversicherungsträger ^property[17].code = #child 
* #Sonderversicherungsträger ^property[17].valueCode = #VSTRL54 
* #Sonderversicherungsträger ^property[18].code = #child 
* #Sonderversicherungsträger ^property[18].valueCode = #VSTRL55 
* #Sonderversicherungsträger ^property[19].code = #child 
* #Sonderversicherungsträger ^property[19].valueCode = #VSTRL56 
* #Sonderversicherungsträger ^property[20].code = #child 
* #Sonderversicherungsträger ^property[20].valueCode = #VSTRL57 
* #Sonderversicherungsträger ^property[21].code = #child 
* #Sonderversicherungsträger ^property[21].valueCode = #VSTRL58 
* #Sonderversicherungsträger ^property[22].code = #child 
* #Sonderversicherungsträger ^property[22].valueCode = #VSTRL59 
* #VSTRL05 "Versicherungsanstalt öffentlich Bediensteter, Eisenbahnen und Bergbau ? Eisenbahn Bergbau"
* #VSTRL05 ^property[0].code = #parent 
* #VSTRL05 ^property[0].valueCode = #Sonderversicherungsträger 
* #VSTRL07 "Versicherungsanstalt öffentlich Bediensteter, Eisenbahnen und Bergbau ? Öffentlich Bedienstete"
* #VSTRL07 ^property[0].code = #parent 
* #VSTRL07 ^property[0].valueCode = #Sonderversicherungsträger 
* #VSTRL40 "Sozialversicherungsanstalt der Selbständigen ? gewerbliche Wirtschaft"
* #VSTRL40 ^property[0].code = #parent 
* #VSTRL40 ^property[0].valueCode = #Sonderversicherungsträger 
* #VSTRL41 "Sozialversicherungsanstalt der Selbständigen ? gewerbliche Wirtschaft Landesstelle Wien"
* #VSTRL41 ^property[0].code = #parent 
* #VSTRL41 ^property[0].valueCode = #Sonderversicherungsträger 
* #VSTRL42 "Sozialversicherungsanstalt der Selbständigen ? gewerbliche Wirtschaft Landesstelle Niederösterreich"
* #VSTRL42 ^property[0].code = #parent 
* #VSTRL42 ^property[0].valueCode = #Sonderversicherungsträger 
* #VSTRL43 "Sozialversicherungsanstalt der Selbständigen ? gewerbliche Wirtschaft Landesstelle Burgenland"
* #VSTRL43 ^property[0].code = #parent 
* #VSTRL43 ^property[0].valueCode = #Sonderversicherungsträger 
* #VSTRL44 "Sozialversicherungsanstalt der Selbständigen ? gewerbliche Wirtschaft Landesstelle Oberösterreich"
* #VSTRL44 ^property[0].code = #parent 
* #VSTRL44 ^property[0].valueCode = #Sonderversicherungsträger 
* #VSTRL45 "Sozialversicherungsanstalt der Selbständigen ? gewerbliche Wirtschaft Landesstelle Steiermark"
* #VSTRL45 ^property[0].code = #parent 
* #VSTRL45 ^property[0].valueCode = #Sonderversicherungsträger 
* #VSTRL46 "Sozialversicherungsanstalt der Selbständigen ? gewerbliche Wirtschaft Landesstelle Kärnten"
* #VSTRL46 ^property[0].code = #parent 
* #VSTRL46 ^property[0].valueCode = #Sonderversicherungsträger 
* #VSTRL47 "Sozialversicherungsanstalt der Selbständigen ? gewerbliche Wirtschaft Landesstelle Salzburg"
* #VSTRL47 ^property[0].code = #parent 
* #VSTRL47 ^property[0].valueCode = #Sonderversicherungsträger 
* #VSTRL48 "Sozialversicherungsanstalt der Selbständigen ? gewerbliche Wirtschaft Landesstelle Tirol"
* #VSTRL48 ^property[0].code = #parent 
* #VSTRL48 ^property[0].valueCode = #Sonderversicherungsträger 
* #VSTRL49 "Sozialversicherungsanstalt der Selbständigen ? gewerbliche Wirtschaft Landesstelle Vorarlberg"
* #VSTRL49 ^property[0].code = #parent 
* #VSTRL49 ^property[0].valueCode = #Sonderversicherungsträger 
* #VSTRL50 "Sozialversicherungsanstalt der Selbständigen ? Landwirtschaft"
* #VSTRL50 ^property[0].code = #parent 
* #VSTRL50 ^property[0].valueCode = #Sonderversicherungsträger 
* #VSTRL51 "Sozialversicherungsanstalt der Selbständigen ? Landwirtschaft Landesstelle Wien"
* #VSTRL51 ^property[0].code = #parent 
* #VSTRL51 ^property[0].valueCode = #Sonderversicherungsträger 
* #VSTRL52 "Sozialversicherungsanstalt der Selbständigen ? Landwirtschaft Landesstelle Niederösterreich"
* #VSTRL52 ^property[0].code = #parent 
* #VSTRL52 ^property[0].valueCode = #Sonderversicherungsträger 
* #VSTRL53 "Sozialversicherungsanstalt der Selbständigen ? Landwirtschaft Landesstelle Burgenland"
* #VSTRL53 ^property[0].code = #parent 
* #VSTRL53 ^property[0].valueCode = #Sonderversicherungsträger 
* #VSTRL54 "Sozialversicherungsanstalt der Selbständigen ? Landwirtschaft Landesstelle Oberösterreich"
* #VSTRL54 ^property[0].code = #parent 
* #VSTRL54 ^property[0].valueCode = #Sonderversicherungsträger 
* #VSTRL55 "Sozialversicherungsanstalt der Selbständigen ? Landwirtschaft Landesstelle Steiermark"
* #VSTRL55 ^property[0].code = #parent 
* #VSTRL55 ^property[0].valueCode = #Sonderversicherungsträger 
* #VSTRL56 "Sozialversicherungsanstalt der Selbständigen ? Landwirtschaft Landesstelle Kärnten"
* #VSTRL56 ^property[0].code = #parent 
* #VSTRL56 ^property[0].valueCode = #Sonderversicherungsträger 
* #VSTRL57 "Sozialversicherungsanstalt der Selbständigen ? Landwirtschaft Landesstelle Salzburg"
* #VSTRL57 ^property[0].code = #parent 
* #VSTRL57 ^property[0].valueCode = #Sonderversicherungsträger 
* #VSTRL58 "Sozialversicherungsanstalt der Selbständigen ? Landwirtschaft Landesstelle Tirol"
* #VSTRL58 ^property[0].code = #parent 
* #VSTRL58 ^property[0].valueCode = #Sonderversicherungsträger 
* #VSTRL59 "Sozialversicherungsanstalt der Selbständigen ? Landwirtschaft Landesstelle Vorarlberg"
* #VSTRL59 ^property[0].code = #parent 
* #VSTRL59 ^property[0].valueCode = #Sonderversicherungsträger 
* #Sonstige "Sonstige"
* #Sonstige ^property[0].code = #parent 
* #Sonstige ^property[0].valueCode = #VSTRL 
* #Sonstige ^property[1].code = #child 
* #Sonstige ^property[1].valueCode = #VSTRL1L 
* #Sonstige ^property[2].code = #child 
* #Sonstige ^property[2].valueCode = #VSTRL4G 
* #Sonstige ^property[3].code = #child 
* #Sonstige ^property[3].valueCode = #VSTRL4L 
* #VSTRL1L "Magistrat der Stadt Wien"
* #VSTRL1L ^property[0].code = #parent 
* #VSTRL1L ^property[0].valueCode = #Sonstige 
* #VSTRL4G "Magistrat der Stadt Steyr"
* #VSTRL4G ^property[0].code = #parent 
* #VSTRL4G ^property[0].valueCode = #Sonstige 
* #VSTRL4L "Landesregierung Oberösterreich"
* #VSTRL4L ^property[0].code = #parent 
* #VSTRL4L ^property[0].valueCode = #Sonstige 
* #Österreichische_Gesundheitskasse "Österreichische Gesundheitskasse"
* #Österreichische_Gesundheitskasse ^property[0].code = #parent 
* #Österreichische_Gesundheitskasse ^property[0].valueCode = #VSTRL 
* #Österreichische_Gesundheitskasse ^property[1].code = #child 
* #Österreichische_Gesundheitskasse ^property[1].valueCode = #VSTRL11 
* #VSTRL11 "Österreichische Gesundheitskasse"
* #VSTRL11 ^property[0].code = #parent 
* #VSTRL11 ^property[0].valueCode = #Österreichische_Gesundheitskasse 
* #VisitenKMTyp "Visiten KM Typ"
* #VisitenKMTyp ^property[0].code = #child 
* #VisitenKMTyp ^property[0].valueCode = #KMA 
* #VisitenKMTyp ^property[1].code = #child 
* #VisitenKMTyp ^property[1].valueCode = #KMB 
* #VisitenKMTyp ^property[2].code = #child 
* #VisitenKMTyp ^property[2].valueCode = #KMN 
* #KMA "Visitenadresse-km Alpin"
* #KMA ^property[0].code = #parent 
* #KMA ^property[0].valueCode = #VisitenKMTyp 
* #KMB "Visitenadresse-km Berg"
* #KMB ^property[0].code = #parent 
* #KMB ^property[0].valueCode = #VisitenKMTyp 
* #KMN "Visitenadresse-km Normal"
* #KMN ^property[0].code = #parent 
* #KMN ^property[0].valueCode = #VisitenKMTyp 
* #Visitenadresse "Visitenadresse"
* #Visiteninformation "Visientinformation"
* #eCardKonsDaten "eCard Konsultationsdaten"
