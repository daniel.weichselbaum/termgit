Instance: elga-pflegestufen 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-pflegestufen" 
* name = "elga-pflegestufen" 
* title = "ELGA_Pflegestufen" 
* status = #active 
* content = #complete 
* version = "202005" 
* description = "**Description:** This code system represents the care levels possible in Austria for calculating the care allowance. To assign the care levels, the need for care must be determined in the course of an assessment by a doctor or a qualified nurse.

**Beschreibung:** Dieses Codesystem stellt die in Österreich möglichen Pflegestufen zur Berechnung des Pflegegelds dar. Für die Zuerkennung der Pflegestufen ist die Feststellung des Hilfe- und Betreuungsbedarfes im Rahmen einer Begutachtung durch einen Arzt oder eine diplomierte Pflegekraft notwendig." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.190" 
* date = "2020-05-07" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.svs.at/cdscontent/?contentid=10007.816625" 
* count = 7 
* property[0].code = #hints 
* property[0].type = #string 
* concept[0].code = #Pflegestufe_1
* concept[0].display = "Pflegestufe 1"
* concept[0].definition = "durchschnittlicher monatlicher Pflegebedarf von mehr als 65 Stunden (Wert 2020)"
* concept[1].code = #Pflegestufe_2
* concept[1].display = "Pflegestufe 2"
* concept[1].definition = "durchschnittlicher monatlicher Pflegebedarf von mehr als 95 Stunden (Wert 2020)"
* concept[2].code = #Pflegestufe_3
* concept[2].display = "Pflegestufe 3"
* concept[2].definition = "durchschnittlicher monatlicher Pflegebedarf von mehr als 120 Stunden (Wert 2020)"
* concept[3].code = #Pflegestufe_4
* concept[3].display = "Pflegestufe 4"
* concept[3].definition = "durchschnittlicher monatlicher Pflegebedarf von mehr als 160 Stunden (Wert 2020)"
* concept[4].code = #Pflegestufe_5
* concept[4].display = "Pflegestufe 5"
* concept[4].definition = "durchschnittlicher monatlicher Pflegebedarf von mehr als 180 Stunden und zusätzlich ein außergewöhnlicher Pflegeaufwand (Wert 2020)"
* concept[4].property[0].code = #hints 
* concept[4].property[0].valueString = "insbesondere Notwendigkeit der dauernden Bereitschaft einer Pflegeperson; der regelmäßigen Nachschau durch eine Pflegeperson in relativ kurzen, jedoch planbaren Zeitabständen (mindestens auch eine einmalige Nachschau in den Nachtstunden) oder von mehr als fünf Pflegeeinheiten, davon auch eine in den Nachtstunden" 
* concept[5].code = #Pflegestufe_6
* concept[5].display = "Pflegestufe 6"
* concept[5].definition = "durchschnittlicher monatlicher Pflegebedarf von mehr als 180 Stunden und zeitlich nicht planbare Betreuungsmaßnahmen oder dauernde Anwesenheit einer Pflegeperson (Wert 2020)"
* concept[5].property[0].code = #hints 
* concept[5].property[0].valueString = "zeitlich nicht planbare Betreuungsmaßnahmen regelmäßig während des Tages und der Nacht oder dauernde Anwesenheit einer Pflegeperson während des Tages und der Nacht auf Grund von Eigen- oder Fremdgefährdung" 
* concept[6].code = #Pflegestufe_7
* concept[6].display = "Pflegestufe 7"
* concept[6].definition = "durchschnittlicher monatlicher Pflegebedarf von mehr als 180 Stunden und keine zielgerichteten Bewegungen aller vier Extremitäten oder ein gleichzuachtender Zustand (Wert 2020)"
