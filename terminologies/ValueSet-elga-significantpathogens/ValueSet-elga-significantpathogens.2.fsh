Instance: elga-significantpathogens 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-significantpathogens" 
* name = "elga-significantpathogens" 
* title = "ELGA_SignificantPathogens" 
* status = #active 
* version = "3.0" 
* description = "**Description:** Value Set of significant pathogens listed in ''Epidemiologisches Meldesystem Benutzerhandbuch Fachlicher Teil'', 1. edition, October 2008, BMGFJ

**Beschreibung:** Value Set für Erreger meldepflichtiger Infektionskrankheiten laut ''Epidemiologisches Meldesystem Benutzerhandbuch Fachlicher Teil'', 1. Auflage Oktober 2008, des BMGFJ" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.58" 
* date = "2015-03-31" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-elga-significantpathogens"
* compose.include[0].concept[0].code = "SP001"
* compose.include[0].concept[0].display = "Adenovirus im Konjunktivalabstrich (*)"
* compose.include[0].concept[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[0].designation[0].value = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* compose.include[0].concept[1].code = "SP002"
* compose.include[0].concept[1].display = "HIV"
* compose.include[0].concept[2].code = "SP003"
* compose.include[0].concept[2].display = "Bacillus anthracis"
* compose.include[0].concept[3].code = "SP004"
* compose.include[0].concept[3].display = "Influenza A/H5"
* compose.include[0].concept[4].code = "SP005"
* compose.include[0].concept[4].display = "Influenza A/H5N1"
* compose.include[0].concept[5].code = "SP006"
* compose.include[0].concept[5].display = "Clostridium botulinum"
* compose.include[0].concept[6].code = "SP007"
* compose.include[0].concept[6].display = "Brucella spp."
* compose.include[0].concept[7].code = "SP008"
* compose.include[0].concept[7].display = "Campylobacter spp."
* compose.include[0].concept[8].code = "SP009"
* compose.include[0].concept[8].display = "Chlamydia trachomatis"
* compose.include[0].concept[9].code = "SP010"
* compose.include[0].concept[9].display = "Vibrio cholerae"
* compose.include[0].concept[10].code = "SP011"
* compose.include[0].concept[10].display = "Denguevirus"
* compose.include[0].concept[11].code = "SP012"
* compose.include[0].concept[11].display = "Corynebacterium diphtheriae"
* compose.include[0].concept[12].code = "SP013"
* compose.include[0].concept[12].display = "Corynebacterium ulcerans"
* compose.include[0].concept[13].code = "SP014"
* compose.include[0].concept[13].display = "Ebolavirus"
* compose.include[0].concept[13].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[13].designation[0].value = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* compose.include[0].concept[14].code = "SP015"
* compose.include[0].concept[14].display = "Escherichia coli, sonstige darmpathogene Stämme"
* compose.include[0].concept[14].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[14].designation[0].value = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* compose.include[0].concept[15].code = "SP016"
* compose.include[0].concept[15].display = "Echinococcus spp."
* compose.include[0].concept[16].code = "SP017"
* compose.include[0].concept[16].display = "Rickettsia prowazekii"
* compose.include[0].concept[16].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[16].designation[0].value = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* compose.include[0].concept[17].code = "SP018"
* compose.include[0].concept[17].display = "FSME-Virus"
* compose.include[0].concept[17].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[17].designation[0].value = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* compose.include[0].concept[18].code = "SP019"
* compose.include[0].concept[18].display = "Gelbfieber-Virus"
* compose.include[0].concept[19].code = "SP020"
* compose.include[0].concept[19].display = "Giardia lamblia"
* compose.include[0].concept[20].code = "SP021"
* compose.include[0].concept[20].display = "Neisseria gonorrhoeae"
* compose.include[0].concept[21].code = "SP022"
* compose.include[0].concept[21].display = "Haemophilus influenzae"
* compose.include[0].concept[22].code = "SP023"
* compose.include[0].concept[22].display = "Hantavirus"
* compose.include[0].concept[22].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[22].designation[0].value = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* compose.include[0].concept[23].code = "SP024"
* compose.include[0].concept[23].display = "Hepatitis-A-Virus"
* compose.include[0].concept[24].code = "SP025"
* compose.include[0].concept[24].display = "Hepatitis-B-Virus"
* compose.include[0].concept[25].code = "SP026"
* compose.include[0].concept[25].display = "Hepatitis-C-Virus"
* compose.include[0].concept[26].code = "SP027"
* compose.include[0].concept[26].display = "HDV - Hepatitis-D-Virus akut"
* compose.include[0].concept[26].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[26].designation[0].value = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* compose.include[0].concept[27].code = "SP028"
* compose.include[0].concept[27].display = "HEV - akute Virushepatitis E"
* compose.include[0].concept[27].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[27].designation[0].value = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* compose.include[0].concept[28].code = "SP029"
* compose.include[0].concept[28].display = "Influenzavirus"
* compose.include[0].concept[29].code = "SP030"
* compose.include[0].concept[29].display = "Bordetella pertussis"
* compose.include[0].concept[30].code = "SP031"
* compose.include[0].concept[30].display = "Cryptosporidium spp."
* compose.include[0].concept[31].code = "SP032"
* compose.include[0].concept[31].display = "Lassavirus"
* compose.include[0].concept[31].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[31].designation[0].value = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* compose.include[0].concept[32].code = "SP033"
* compose.include[0].concept[32].display = "Borrelia recurrentis"
* compose.include[0].concept[33].code = "SP034"
* compose.include[0].concept[33].display = "Legionella spp."
* compose.include[0].concept[34].code = "SP035"
* compose.include[0].concept[34].display = "Mycobacterium leprae"
* compose.include[0].concept[34].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[34].designation[0].value = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* compose.include[0].concept[35].code = "SP036"
* compose.include[0].concept[35].display = "Leptospira interrogans"
* compose.include[0].concept[36].code = "SP037"
* compose.include[0].concept[36].display = "Listeria monocytogenes"
* compose.include[0].concept[37].code = "SP038"
* compose.include[0].concept[37].display = "Plasmodium spp."
* compose.include[0].concept[38].code = "SP039"
* compose.include[0].concept[38].display = "Marburgvirus"
* compose.include[0].concept[38].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[38].designation[0].value = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* compose.include[0].concept[39].code = "SP040"
* compose.include[0].concept[39].display = "Masernvirus"
* compose.include[0].concept[40].code = "SP041"
* compose.include[0].concept[40].display = "Neisseria meningitidis"
* compose.include[0].concept[41].code = "SP042"
* compose.include[0].concept[41].display = "Mumpsvirus"
* compose.include[0].concept[42].code = "SP043"
* compose.include[0].concept[42].display = "Norovirus"
* compose.include[0].concept[42].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[42].designation[0].value = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* compose.include[0].concept[43].code = "SP044"
* compose.include[0].concept[43].display = "Chlamydophila psittaci"
* compose.include[0].concept[43].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[43].designation[0].value = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* compose.include[0].concept[44].code = "SP045"
* compose.include[0].concept[44].display = "Yersinia pestis"
* compose.include[0].concept[45].code = "SP046"
* compose.include[0].concept[45].display = "Streptococcus pneumoniae"
* compose.include[0].concept[46].code = "SP047"
* compose.include[0].concept[46].display = "Variola-Virus"
* compose.include[0].concept[47].code = "SP048"
* compose.include[0].concept[47].display = "Poliovirus"
* compose.include[0].concept[48].code = "SP049"
* compose.include[0].concept[48].display = "Coxiella burnetii"
* compose.include[0].concept[49].code = "SP050"
* compose.include[0].concept[49].display = "Rotavirus"
* compose.include[0].concept[49].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[49].designation[0].value = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* compose.include[0].concept[50].code = "SP051"
* compose.include[0].concept[50].display = "Rubella-Virus"
* compose.include[0].concept[51].code = "SP053"
* compose.include[0].concept[51].display = "Salmonella spp. außer S. Typhi und S. Paratyphi"
* compose.include[0].concept[52].code = "SP054"
* compose.include[0].concept[52].display = "SARS-Coronavirus, SARS-CoV"
* compose.include[0].concept[53].code = "SP055"
* compose.include[0].concept[53].display = "Shigella spp."
* compose.include[0].concept[54].code = "SP056"
* compose.include[0].concept[54].display = "Treponema pallidum"
* compose.include[0].concept[55].code = "SP057"
* compose.include[0].concept[55].display = "Clostridium tetani"
* compose.include[0].concept[56].code = "SP058"
* compose.include[0].concept[56].display = "Lyssa-Virus"
* compose.include[0].concept[57].code = "SP059"
* compose.include[0].concept[57].display = "Toxoplasma gondii"
* compose.include[0].concept[58].code = "SP060"
* compose.include[0].concept[58].display = "Trichinella spp."
* compose.include[0].concept[59].code = "SP061"
* compose.include[0].concept[59].display = "Mycobacterium-tuberculosis-Komplex"
* compose.include[0].concept[60].code = "SP062"
* compose.include[0].concept[60].display = "Francisella tularensis"
* compose.include[0].concept[61].code = "SP063"
* compose.include[0].concept[61].display = "Salmonella typhi"
* compose.include[0].concept[62].code = "SP064"
* compose.include[0].concept[62].display = "Salmonella paratyphi"
* compose.include[0].concept[63].code = "SP065"
* compose.include[0].concept[63].display = "West-Nil-Virus"
* compose.include[0].concept[64].code = "SP066"
* compose.include[0].concept[64].display = "Yersinia enterocolitica"
* compose.include[0].concept[65].code = "SP067"
* compose.include[0].concept[65].display = "Yersinia pseudotuberculosis"
