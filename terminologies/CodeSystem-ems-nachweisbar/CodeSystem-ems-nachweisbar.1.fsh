Instance: ems-nachweisbar 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-nachweisbar" 
* name = "ems-nachweisbar" 
* title = "EMS_Nachweisbar" 
* status = #active 
* content = #complete 
* version = "2.0.0" 
* description = "**Beschreibung:** EMS Liste Nachweisbar: Verwendung bei TBC 'INH-Resistenzgen (KatG/inhA PCR)' und 'RMP-Resistenzgen (PCR)'" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.78" 
* date = "2020-11-06" 
* count = 4 
* concept[0].code = #N
* concept[0].display = "nachweisbar"
* concept[1].code = #NB
* concept[1].display = "nicht bestimmbar"
* concept[2].code = #NN
* concept[2].display = "nicht nachweisbar"
* concept[3].code = #NOTEST
* concept[3].display = "nicht durchgeführt"
