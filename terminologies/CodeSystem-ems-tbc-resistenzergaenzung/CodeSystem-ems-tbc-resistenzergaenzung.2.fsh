Instance: ems-tbc-resistenzergaenzung 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-tbc-resistenzergaenzung" 
* name = "ems-tbc-resistenzergaenzung" 
* title = "EMS_TBC_ResistenzErgaenzung" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** Resistenzprüfung TBC für INH, RMP, EMB, SM, AMK, KM, CM, CIP, OFX, PZA, MFX, PAS, CS, LZD, PTO, RFB" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.67" 
* date = "2017-01-26" 
* count = 3 
* #GET_BEFNOTPOS "Getestet - Befundung nicht möglich"
* #GET_ERGMISS "Getestet - Ergebnis noch ausständig"
* #NICHTGET "nicht getestet"
