Instance: hl7-participation-function 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-participation-function" 
* name = "hl7-participation-function" 
* title = "HL7 Participation Function" 
* status = #active 
* content = #complete 
* version = "HL7:ParticipationFunction" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.5.88" 
* date = "2013-01-10" 
* count = 46 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #ADMPHYS "admitting physician"
* #ANEST "anesthesist"
* #ANRS "anesthesia nurse"
* #ATTPHYS "attending physician"
* #DISPHYS "discharging physician"
* #FASST "first assistant surgeon"
* #MDWF "midwife"
* #NASST "nurse assistant"
* #PCP "primary care physician"
* #PRISURG "primary surgeon"
* #RNDPHYS "rounding physician"
* #SASST "second assistant surgeon"
* #SNRS "scrub nurse"
* #TASST "third assistant"
* #_AuthorizedParticipationFunction "AuthorizedParticipationFunction"
* #_AuthorizedParticipationFunction ^property[0].code = #child 
* #_AuthorizedParticipationFunction ^property[0].valueCode = #_AuthorizedReceiverParticipationFunction 
* #_AuthorizedParticipationFunction ^property[1].code = #child 
* #_AuthorizedParticipationFunction ^property[1].valueCode = #_ConsenterParticipationFunction 
* #_AuthorizedParticipationFunction ^property[2].code = #child 
* #_AuthorizedParticipationFunction ^property[2].valueCode = #_OverriderParticipationFunction 
* #_AuthorizedReceiverParticipationFunction "AuthorizedReceiverParticipationFunction"
* #_AuthorizedReceiverParticipationFunction ^property[0].code = #parent 
* #_AuthorizedReceiverParticipationFunction ^property[0].valueCode = #_AuthorizedParticipationFunction 
* #_AuthorizedReceiverParticipationFunction ^property[1].code = #child 
* #_AuthorizedReceiverParticipationFunction ^property[1].valueCode = #AUCG 
* #_AuthorizedReceiverParticipationFunction ^property[2].code = #child 
* #_AuthorizedReceiverParticipationFunction ^property[2].valueCode = #AULR 
* #_AuthorizedReceiverParticipationFunction ^property[3].code = #child 
* #_AuthorizedReceiverParticipationFunction ^property[3].valueCode = #AUTM 
* #_AuthorizedReceiverParticipationFunction ^property[4].code = #child 
* #_AuthorizedReceiverParticipationFunction ^property[4].valueCode = #AUWA 
* #AUCG "caregiver information receiver"
* #AUCG ^property[0].code = #parent 
* #AUCG ^property[0].valueCode = #_AuthorizedReceiverParticipationFunction 
* #AULR "legitimate relationship information receiver"
* #AULR ^property[0].code = #parent 
* #AULR ^property[0].valueCode = #_AuthorizedReceiverParticipationFunction 
* #AUTM "care team information receiver"
* #AUTM ^property[0].code = #parent 
* #AUTM ^property[0].valueCode = #_AuthorizedReceiverParticipationFunction 
* #AUWA "work area information receiver"
* #AUWA ^property[0].code = #parent 
* #AUWA ^property[0].valueCode = #_AuthorizedReceiverParticipationFunction 
* #_ConsenterParticipationFunction "ConsenterParticipationFunction"
* #_ConsenterParticipationFunction ^property[0].code = #parent 
* #_ConsenterParticipationFunction ^property[0].valueCode = #_AuthorizedParticipationFunction 
* #_ConsenterParticipationFunction ^property[1].code = #child 
* #_ConsenterParticipationFunction ^property[1].valueCode = #GRDCON 
* #_ConsenterParticipationFunction ^property[2].code = #child 
* #_ConsenterParticipationFunction ^property[2].valueCode = #POACON 
* #_ConsenterParticipationFunction ^property[3].code = #child 
* #_ConsenterParticipationFunction ^property[3].valueCode = #PRCON 
* #_ConsenterParticipationFunction ^property[4].code = #child 
* #_ConsenterParticipationFunction ^property[4].valueCode = #PROMSK 
* #_ConsenterParticipationFunction ^property[5].code = #child 
* #_ConsenterParticipationFunction ^property[5].valueCode = #SUBCON 
* #GRDCON "legal guardian consent author"
* #GRDCON ^property[0].code = #parent 
* #GRDCON ^property[0].valueCode = #_ConsenterParticipationFunction 
* #POACON "healthcare power of attorney consent author"
* #POACON ^property[0].code = #parent 
* #POACON ^property[0].valueCode = #_ConsenterParticipationFunction 
* #PRCON "personal representative consent author"
* #PRCON ^property[0].code = #parent 
* #PRCON ^property[0].valueCode = #_ConsenterParticipationFunction 
* #PROMSK "authorized provider masking author"
* #PROMSK ^property[0].code = #parent 
* #PROMSK ^property[0].valueCode = #_ConsenterParticipationFunction 
* #SUBCON "subject of consent author"
* #SUBCON ^property[0].code = #parent 
* #SUBCON ^property[0].valueCode = #_ConsenterParticipationFunction 
* #_OverriderParticipationFunction "OverriderParticipationFunction"
* #_OverriderParticipationFunction ^property[0].code = #parent 
* #_OverriderParticipationFunction ^property[0].valueCode = #_AuthorizedParticipationFunction 
* #_OverriderParticipationFunction ^property[1].code = #child 
* #_OverriderParticipationFunction ^property[1].valueCode = #AUCOV 
* #_OverriderParticipationFunction ^property[2].code = #child 
* #_OverriderParticipationFunction ^property[2].valueCode = #AUEMROV 
* #AUCOV "consent overrider"
* #AUCOV ^property[0].code = #parent 
* #AUCOV ^property[0].valueCode = #_OverriderParticipationFunction 
* #AUEMROV "emergency overrider"
* #AUEMROV ^property[0].code = #parent 
* #AUEMROV ^property[0].valueCode = #_OverriderParticipationFunction 
* #_CoverageParticipationFunction "CoverageParticipationFunction"
* #_CoverageParticipationFunction ^property[0].code = #child 
* #_CoverageParticipationFunction ^property[0].valueCode = #_PayorParticipationFunction 
* #_CoverageParticipationFunction ^property[1].code = #child 
* #_CoverageParticipationFunction ^property[1].valueCode = #_SponsorParticipationFunction 
* #_CoverageParticipationFunction ^property[2].code = #child 
* #_CoverageParticipationFunction ^property[2].valueCode = #_UnderwriterParticipationFunction 
* #_PayorParticipationFunction "PayorParticipationFunction"
* #_PayorParticipationFunction ^property[0].code = #parent 
* #_PayorParticipationFunction ^property[0].valueCode = #_CoverageParticipationFunction 
* #_PayorParticipationFunction ^property[1].code = #child 
* #_PayorParticipationFunction ^property[1].valueCode = #CLMADJ 
* #_PayorParticipationFunction ^property[2].code = #child 
* #_PayorParticipationFunction ^property[2].valueCode = #ENROLL 
* #_PayorParticipationFunction ^property[3].code = #child 
* #_PayorParticipationFunction ^property[3].valueCode = #FFSMGT 
* #_PayorParticipationFunction ^property[4].code = #child 
* #_PayorParticipationFunction ^property[4].valueCode = #MCMGT 
* #_PayorParticipationFunction ^property[5].code = #child 
* #_PayorParticipationFunction ^property[5].valueCode = #PROVMGT 
* #_PayorParticipationFunction ^property[6].code = #child 
* #_PayorParticipationFunction ^property[6].valueCode = #UMGT 
* #CLMADJ "claims adjudication"
* #CLMADJ ^property[0].code = #parent 
* #CLMADJ ^property[0].valueCode = #_PayorParticipationFunction 
* #ENROLL "enrollment broker"
* #ENROLL ^property[0].code = #parent 
* #ENROLL ^property[0].valueCode = #_PayorParticipationFunction 
* #FFSMGT "ffs management"
* #FFSMGT ^property[0].code = #parent 
* #FFSMGT ^property[0].valueCode = #_PayorParticipationFunction 
* #MCMGT "managed care management"
* #MCMGT ^property[0].code = #parent 
* #MCMGT ^property[0].valueCode = #_PayorParticipationFunction 
* #PROVMGT "provider management"
* #PROVMGT ^property[0].code = #parent 
* #PROVMGT ^property[0].valueCode = #_PayorParticipationFunction 
* #UMGT "utilization management"
* #UMGT ^property[0].code = #parent 
* #UMGT ^property[0].valueCode = #_PayorParticipationFunction 
* #_SponsorParticipationFunction "SponsorParticipationFunction"
* #_SponsorParticipationFunction ^property[0].code = #parent 
* #_SponsorParticipationFunction ^property[0].valueCode = #_CoverageParticipationFunction 
* #_SponsorParticipationFunction ^property[1].code = #child 
* #_SponsorParticipationFunction ^property[1].valueCode = #FULINRD 
* #_SponsorParticipationFunction ^property[2].code = #child 
* #_SponsorParticipationFunction ^property[2].valueCode = #SELFINRD 
* #FULINRD "fully insured"
* #FULINRD ^property[0].code = #parent 
* #FULINRD ^property[0].valueCode = #_SponsorParticipationFunction 
* #SELFINRD "self insured"
* #SELFINRD ^property[0].code = #parent 
* #SELFINRD ^property[0].valueCode = #_SponsorParticipationFunction 
* #_UnderwriterParticipationFunction "UnderwriterParticipationFunction"
* #_UnderwriterParticipationFunction ^property[0].code = #parent 
* #_UnderwriterParticipationFunction ^property[0].valueCode = #_CoverageParticipationFunction 
* #_UnderwriterParticipationFunction ^property[1].code = #child 
* #_UnderwriterParticipationFunction ^property[1].valueCode = #PAYORCNTR 
* #_UnderwriterParticipationFunction ^property[2].code = #child 
* #_UnderwriterParticipationFunction ^property[2].valueCode = #REINS 
* #_UnderwriterParticipationFunction ^property[3].code = #child 
* #_UnderwriterParticipationFunction ^property[3].valueCode = #RETROCES 
* #_UnderwriterParticipationFunction ^property[4].code = #child 
* #_UnderwriterParticipationFunction ^property[4].valueCode = #SUBCTRT 
* #_UnderwriterParticipationFunction ^property[5].code = #child 
* #_UnderwriterParticipationFunction ^property[5].valueCode = #UNDERWRTNG 
* #PAYORCNTR "payor contracting"
* #PAYORCNTR ^property[0].code = #parent 
* #PAYORCNTR ^property[0].valueCode = #_UnderwriterParticipationFunction 
* #REINS "reinsures"
* #REINS ^property[0].code = #parent 
* #REINS ^property[0].valueCode = #_UnderwriterParticipationFunction 
* #RETROCES "retrocessionaires"
* #RETROCES ^property[0].code = #parent 
* #RETROCES ^property[0].valueCode = #_UnderwriterParticipationFunction 
* #SUBCTRT "subcontracting risk"
* #SUBCTRT ^property[0].code = #parent 
* #SUBCTRT ^property[0].valueCode = #_UnderwriterParticipationFunction 
* #UNDERWRTNG "underwriting"
* #UNDERWRTNG ^property[0].code = #parent 
* #UNDERWRTNG ^property[0].valueCode = #_UnderwriterParticipationFunction 
