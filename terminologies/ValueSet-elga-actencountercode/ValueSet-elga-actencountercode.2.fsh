Instance: elga-actencountercode 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-actencountercode" 
* name = "elga-actencountercode" 
* title = "ELGA_ActEncounterCode" 
* status = #active 
* version = "3.1" 
* description = "**Description:** Coding of patient encounter (patient stay)

**Beschreibung:** Codierung des Patientenkontakts (Aufenthalt)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.5" 
* date = "2015-09-01" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.hl7.org" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-hl7-act-code"
* compose.include[0].concept[0].code = "_ActEncounterCode"
* compose.include[0].concept[0].display = "ActEncounterCode"
* compose.include[0].concept[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[0].designation[0].value = "(Wird nicht verwendet)" 
* compose.include[0].concept[1].code = "AMB"
* compose.include[0].concept[1].display = "ambulatory"
* compose.include[0].concept[1].designation[0].language = #de-AT 
* compose.include[0].concept[1].designation[0].value = "Ambulante Aufnahme / Ordinationsbesuch" 
* compose.include[0].concept[1].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[1].designation[1].value = "'Ambulant von  bis'" 
* compose.include[0].concept[1].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[1].designation[2].value = "Ambulante Aufnahme / Ordinationsbesuch" 
* compose.include[0].concept[2].code = "EMER"
* compose.include[0].concept[2].display = "emergency"
* compose.include[0].concept[2].designation[0].language = #de-AT 
* compose.include[0].concept[2].designation[0].value = "Notfallkontakt (in den Räumlichkeiten des GDA)" 
* compose.include[0].concept[2].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[2].designation[1].value = "'Akutbehandlung von .. bis'" 
* compose.include[0].concept[2].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[2].designation[2].value = "Notfallkontakt (in den Räumlichkeiten des GDA)" 
* compose.include[0].concept[3].code = "FLD"
* compose.include[0].concept[3].display = "field"
* compose.include[0].concept[3].designation[0].language = #de-AT 
* compose.include[0].concept[3].designation[0].value = "Patientenkontakt außerhalb der Räumlichkeit eines GDA" 
* compose.include[0].concept[3].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[3].designation[1].value = "'Notfall, Rettung, erste Hilfe'" 
* compose.include[0].concept[3].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[3].designation[2].value = "Patientenkontakt außerhalb der Räumlichkeit eines GDA" 
* compose.include[0].concept[4].code = "HH"
* compose.include[0].concept[4].display = "home health"
* compose.include[0].concept[4].designation[0].language = #de-AT 
* compose.include[0].concept[4].designation[0].value = "Hausbesuche, Hausbetreuung" 
* compose.include[0].concept[4].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[4].designation[1].value = "'Hausbesuch(e) von  bis'" 
* compose.include[0].concept[4].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[4].designation[2].value = "Hausbesuch, Hausbetreuung, mobile Betreuung" 
* compose.include[0].concept[5].code = "IMP"
* compose.include[0].concept[5].display = "inpatient encounter"
* compose.include[0].concept[5].designation[0].language = #de-AT 
* compose.include[0].concept[5].designation[0].value = "Stationäre Aufnahme" 
* compose.include[0].concept[5].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[5].designation[1].value = "'Stationär von  bis'" 
* compose.include[0].concept[5].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[5].designation[2].value = "Stationäre Aufnahme" 
* compose.include[0].concept[6].code = "ACUTE"
* compose.include[0].concept[6].display = "inpatient acute"
* compose.include[0].concept[6].designation[0].language = #de-AT 
* compose.include[0].concept[6].designation[0].value = "Akute stationäre Aufnahme" 
* compose.include[0].concept[6].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[6].designation[1].value = "'Stationär von  bis'" 
* compose.include[0].concept[6].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[6].designation[2].value = "Akute stationäre Aufnahme" 
* compose.include[0].concept[7].code = "NONAC"
* compose.include[0].concept[7].display = "inpatient non-acute"
* compose.include[0].concept[7].designation[0].language = #de-AT 
* compose.include[0].concept[7].designation[0].value = "Nicht-akute (geplante) stationäre Aufnahme" 
* compose.include[0].concept[7].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[7].designation[1].value = "'Stationär von  bis'" 
* compose.include[0].concept[7].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[7].designation[2].value = "Nicht-akute (geplante) stationäre Aufnahme" 
* compose.include[0].concept[8].code = "PRENC"
* compose.include[0].concept[8].display = "pre-admission"
* compose.include[0].concept[8].designation[0].language = #de-AT 
* compose.include[0].concept[8].designation[0].value = "Prästationäre Aufnahme" 
* compose.include[0].concept[8].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[8].designation[1].value = "'Prästationär von  bis'" 
* compose.include[0].concept[8].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[8].designation[2].value = "Prästationäre Aufnahme" 
* compose.include[0].concept[9].code = "SS"
* compose.include[0].concept[9].display = "short stay"
* compose.include[0].concept[9].designation[0].language = #de-AT 
* compose.include[0].concept[9].designation[0].value = "Tagesbetreuung" 
* compose.include[0].concept[9].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[9].designation[1].value = "'Tagesklinisch von  bis'" 
* compose.include[0].concept[9].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[9].designation[2].value = "Tagesbetreuung, teilstationäre / tagesklinische Aufnahmen ('Null-Tagesfälle'),  Tageszentrum" 
* compose.include[0].concept[10].code = "VR"
* compose.include[0].concept[10].display = "virtual"
* compose.include[0].concept[10].designation[0].language = #de-AT 
* compose.include[0].concept[10].designation[0].value = "Virtueller Kontakt (Telemedizin, Telefonkontakt, )" 
* compose.include[0].concept[10].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[10].designation[1].value = "'Behandlung von  bis'" 
* compose.include[0].concept[10].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[10].designation[2].value = "Virtueller Kontakt (Telemedizin, Telefonkontakt, )" 
