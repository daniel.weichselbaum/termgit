Instance: hl7-marital-status 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-marital-status" 
* name = "hl7-marital-status" 
* title = "HL7 Marital Status" 
* status = #active 
* content = #complete 
* version = "HL7:MaritalStatus" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.5.2" 
* date = "2013-01-10" 
* count = 9 
* concept[0].code = #A
* concept[0].display = "Annulled"
* concept[1].code = #D
* concept[1].display = "Divorced"
* concept[2].code = #I
* concept[2].display = "Interlocutory"
* concept[3].code = #L
* concept[3].display = "Legally Separated"
* concept[4].code = #M
* concept[4].display = "Married"
* concept[5].code = #P
* concept[5].display = "Polygamous"
* concept[6].code = #S
* concept[6].display = "Never Married"
* concept[7].code = #T
* concept[7].display = "Domestic partner"
* concept[8].code = #W
* concept[8].display = "Widowed"
