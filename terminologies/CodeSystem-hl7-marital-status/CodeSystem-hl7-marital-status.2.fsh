Instance: hl7-marital-status 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-marital-status" 
* name = "hl7-marital-status" 
* title = "HL7 Marital Status" 
* status = #active 
* content = #complete 
* version = "HL7:MaritalStatus" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.5.2" 
* date = "2013-01-10" 
* count = 9 
* #A "Annulled"
* #D "Divorced"
* #I "Interlocutory"
* #L "Legally Separated"
* #M "Married"
* #P "Polygamous"
* #S "Never Married"
* #T "Domestic partner"
* #W "Widowed"
