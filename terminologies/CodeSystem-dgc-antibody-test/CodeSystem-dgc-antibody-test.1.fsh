Instance: dgc-antibody-test 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-dgc-antibody-test" 
* name = "dgc-antibody-test" 
* title = "DGC_Antibody-Test" 
* status = #active 
* content = #complete 
* version = "202106" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.203" 
* date = "2021-06-01" 
* count = 3 
* property[0].code = #Relationships 
* property[0].type = #string 
* concept[0].code = #701
* concept[0].display = "Abbott, SARS-CoV-2 IgG"
* concept[0].definition = "Abbott, SARS-CoV-2 IgG"
* concept[0].designation[0].language = #de-AT 
* concept[0].designation[0].value = "Abbott, SARS-CoV-2 IgG" 
* concept[0].property[0].code = #Relationships 
* concept[0].property[0].valueString = "$.t..ma" 
* concept[1].code = #754
* concept[1].display = "Abbott, COVID-19 LAB TEST TO DETECT ANTIBODIES"
* concept[1].definition = "Abbott, COVID-19 LAB TEST TO DETECT ANTIBODIES"
* concept[1].designation[0].language = #de-AT 
* concept[1].designation[0].value = "Abbott, COVID-19 LAB TEST TO DETECT ANTIBODIES" 
* concept[1].property[0].code = #Relationships 
* concept[1].property[0].valueString = "$.t..ma" 
* concept[2].code = #882
* concept[2].display = "ROCHE Diagnostics, Elecsys Anti-SARS-CoV-2"
* concept[2].definition = "ROCHE Diagnostics, Elecsys Anti-SARS-CoV-2"
* concept[2].designation[0].language = #de-AT 
* concept[2].designation[0].value = "ROCHE Diagnostics, Elecsys Anti-SARS-CoV-2" 
* concept[2].property[0].code = #Relationships 
* concept[2].property[0].valueString = "$.t..ma" 
