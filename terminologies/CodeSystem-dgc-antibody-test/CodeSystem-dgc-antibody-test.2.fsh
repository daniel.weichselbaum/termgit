Instance: dgc-antibody-test 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-dgc-antibody-test" 
* name = "dgc-antibody-test" 
* title = "DGC_Antibody-Test" 
* status = #active 
* content = #complete 
* version = "202106" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.203" 
* date = "2021-06-01" 
* count = 3 
* property[0].code = #Relationships 
* property[0].type = #string 
* #701 "Abbott, SARS-CoV-2 IgG"
* #701 ^definition = Abbott, SARS-CoV-2 IgG
* #701 ^designation[0].language = #de-AT 
* #701 ^designation[0].value = "Abbott, SARS-CoV-2 IgG" 
* #701 ^property[0].code = #Relationships 
* #701 ^property[0].valueString = "$.t..ma" 
* #754 "Abbott, COVID-19 LAB TEST TO DETECT ANTIBODIES"
* #754 ^definition = Abbott, COVID-19 LAB TEST TO DETECT ANTIBODIES
* #754 ^designation[0].language = #de-AT 
* #754 ^designation[0].value = "Abbott, COVID-19 LAB TEST TO DETECT ANTIBODIES" 
* #754 ^property[0].code = #Relationships 
* #754 ^property[0].valueString = "$.t..ma" 
* #882 "ROCHE Diagnostics, Elecsys Anti-SARS-CoV-2"
* #882 ^definition = ROCHE Diagnostics, Elecsys Anti-SARS-CoV-2
* #882 ^designation[0].language = #de-AT 
* #882 ^designation[0].value = "ROCHE Diagnostics, Elecsys Anti-SARS-CoV-2" 
* #882 ^property[0].code = #Relationships 
* #882 ^property[0].valueString = "$.t..ma" 
