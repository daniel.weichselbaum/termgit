Instance: elga-e-health-anwendungen 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-e-health-anwendungen" 
* name = "elga-e-health-anwendungen" 
* title = "ELGA_e-Health_Anwendungen" 
* status = #active 
* content = #complete 
* version = "202105" 
* description = "**Beschreibung:** ELGA Codeliste für autonome ELGA-Anwendungen und Services (relevant für das Berechtigungssystem)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.159" 
* date = "2021-05-19" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* count = 10 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #100 "ELGA Anwendungen"
* #100 ^designation[0].language = #de-AT 
* #100 ^designation[0].value = "ELGA Anwendungen" 
* #100 ^property[0].code = #child 
* #100 ^property[0].valueCode = #101 
* #100 ^property[1].code = #child 
* #100 ^property[1].valueCode = #102 
* #101 "E-Befunde"
* #101 ^designation[0].language = #de-AT 
* #101 ^designation[0].value = "E-Befunde" 
* #101 ^property[0].code = #parent 
* #101 ^property[0].valueCode = #100 
* #102 "E-Medikation"
* #102 ^designation[0].language = #de-AT 
* #102 ^designation[0].value = "E-Medikation" 
* #102 ^property[0].code = #parent 
* #102 ^property[0].valueCode = #100 
* #103 "e-Impfpass Anwendung"
* #103 ^designation[0].language = #de-AT 
* #103 ^designation[0].value = "e-Impfpass Anwendung" 
* #104 "Patientenverfügung Anwendung"
* #104 ^designation[0].language = #de-AT 
* #104 ^designation[0].value = "Patientenverfügung Anwendung" 
* #105 "EMS Epidemiologisches Meldesystem/EPI-Service"
* #105 ^designation[0].language = #de-AT 
* #105 ^designation[0].value = "EMS Epidemiologisches Meldesystem/EPI-Service" 
* #150 "Virtuelle Organisation für Testzwecke"
* #150 ^designation[0].language = #de-AT 
* #150 ^designation[0].value = "Virtuelle Organisation für Testzwecke" 
* #151 "PVN Gesunder Tennengau Hallein"
* #151 ^designation[0].language = #de-AT 
* #151 ^designation[0].value = "PVN Gesunder Tennengau Hallein" 
* #152 "PVN Gesunder Tennengau Lammertal"
* #152 ^designation[0].language = #de-AT 
* #152 ^designation[0].value = "PVN Gesunder Tennengau Lammertal" 
* #153 "PVN Gesunder Tennengau-Süd"
* #153 ^designation[0].language = #de-AT 
* #153 ^designation[0].value = "PVN Gesunder Tennengau-Süd" 
