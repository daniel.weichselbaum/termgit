Instance: elga-allergietyp 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-allergietyp" 
* name = "elga-allergietyp" 
* title = "ELGA_AllergieTyp" 
* status = #active 
* content = #complete 
* version = "202007" 
* description = "**Description:** Type of observed intolerance or allergy

**Beschreibung:** Art der beobachteten Intoleranz oder Allergie" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.180" 
* date = "2020-07-27" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* count = 5 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #Hypersens "Hypersensititivity"
* #Hypersens ^designation[0].language = #de-AT 
* #Hypersens ^designation[0].value = "Überempfindlichkeit" 
* #Hypersens ^property[0].code = #child 
* #Hypersens ^property[0].valueCode = #Allergy 
* #Hypersens ^property[1].code = #child 
* #Hypersens ^property[1].valueCode = #Intolerance 
* #Allergy "Allergy to substance"
* #Allergy ^designation[0].language = #de-AT 
* #Allergy ^designation[0].value = "Allergie" 
* #Allergy ^property[0].code = #parent 
* #Allergy ^property[0].valueCode = #Hypersens 
* #Allergy ^property[1].code = #child 
* #Allergy ^property[1].valueCode = #Allergy1 
* #Allergy ^property[2].code = #child 
* #Allergy ^property[2].valueCode = #Allergy4 
* #Allergy1 "Allergy: Type 1 hypersensitivity"
* #Allergy1 ^designation[0].language = #de-AT 
* #Allergy1 ^designation[0].value = "Allergie Typ I" 
* #Allergy1 ^property[0].code = #parent 
* #Allergy1 ^property[0].valueCode = #Allergy 
* #Allergy4 "Allergy: Type 4 hypersensitivity"
* #Allergy4 ^designation[0].language = #de-AT 
* #Allergy4 ^designation[0].value = "Allergie Typ VI" 
* #Allergy4 ^property[0].code = #parent 
* #Allergy4 ^property[0].valueCode = #Allergy 
* #Intolerance "Intolerance to substance"
* #Intolerance ^designation[0].language = #de-AT 
* #Intolerance ^designation[0].value = "Intoleranz" 
* #Intolerance ^property[0].code = #parent 
* #Intolerance ^property[0].valueCode = #Hypersens 
