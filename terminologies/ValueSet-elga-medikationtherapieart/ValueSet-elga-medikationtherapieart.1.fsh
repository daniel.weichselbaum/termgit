Instance: elga-medikationtherapieart 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-medikationtherapieart" 
* name = "elga-medikationtherapieart" 
* title = "ELGA_MedikationTherapieArt" 
* status = #active 
* version = "3.0" 
* description = "**Description:** ELGA Valueset for type of therapy

**Beschreibung:** ELGA Valueset für Therapieart" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.30" 
* date = "2015-03-31" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-medikationtherapieart"
* compose.include[0].concept[0].code = #EINZEL
* compose.include[0].concept[0].display = "Einzelverordnung"
* compose.include[0].concept[1].code = #NICHTEINZEL
* compose.include[0].concept[1].display = "Nicht-Einzelverordnung"

* expansion.timestamp = "2022-09-13T14:18:13.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem-medikationtherapieart"
* expansion.contains[0].code = #EINZEL
* expansion.contains[0].display = "Einzelverordnung"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem-medikationtherapieart"
* expansion.contains[1].code = #NICHTEINZEL
* expansion.contains[1].display = "Nicht-Einzelverordnung"
