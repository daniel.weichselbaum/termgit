Instance: dgc-diseaseoragent 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-dgc-diseaseoragent" 
* name = "dgc-diseaseoragent" 
* title = "DGC_DiseaseOrAgent" 
* status = #active 
* version = "202104" 
* description = "**Description:** To be used in certificate 2. Contains information protected by copyright of SNOMED International. Any use of SNOMED CT in Austria requires a valid affiliate license or sublicense. The corresponding license is free of charge, provided that the use only takes place in Austria and fulfills the conditions of the Affiliate License Agreement. Affiliate licenses can be requested directly from the respective NRC via the [Member Licensing and Distribution Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS).

**Beschreibung:** Code = SNOMED Codes, die in Österreich codiert werden können      DisplayName = Der DisplayName zum Code, der aus SNOMED gezogen wird     Anwendungsbeschreibung = DisplayName aus dem DGC     Deutsche Sprachvariante = Übersetzung     Relationship = kein richtiges Mapping, da es auf sich selbst zeigt. Enthält durch SNOMED International urheberrechtlich geschützte Information. Jede Verwendung von SNOMED CT in Österreich erfordert eine aufrechte Affiliate Lizenz oder eine Sublizenz. Die entsprechende Lizenz ist kostenlos, vorausgesetzt die Verwendung findet nur in Österreich statt und erfüllt die Bedingungen des Affiliate License Agreements. Affiliate Lizenzen können über das [Mitglieder-Lizenzierungs- und Distributions-Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS) direkt beim jeweiligen NRC beantragt werden." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.55" 
* date = "2021-04-29" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* compose.include[0].concept[0].code = #840539006
* compose.include[0].concept[0].display = "COVID-19"
* compose.include[0].concept[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[0].designation[0].value = "COVID-19" 
* compose.include[0].concept[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#relationships "relationships" 
* compose.include[0].concept[0].designation[1].value = "2.16.840.1.113883.6.96:840539006" 

* expansion.timestamp = "2022-09-13T14:17:37.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* expansion.contains[0].code = #840539006
* expansion.contains[0].display = "COVID-19"
* expansion.contains[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[0].designation[0].value = "COVID-19" 
* expansion.contains[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#relationships "relationships" 
* expansion.contains[0].designation[1].value = "2.16.840.1.113883.6.96:840539006" 
