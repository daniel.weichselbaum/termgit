Instance: ems-durchgefuehrt 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-durchgefuehrt" 
* name = "ems-durchgefuehrt" 
* title = "EMS_Durchgefuehrt" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Liste Durchgeführt: Verwendung bei TBC" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.74" 
* date = "2017-01-26" 
* count = 2 
* concept[0].code = #D
* concept[0].display = "Durchgeführt"
* concept[1].code = #ND
* concept[1].display = "nicht durchgeführt"
