Instance: exnds-formatcodes 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-exnds-formatcodes" 
* name = "exnds-formatcodes" 
* title = "EXNDS_FormatCodes" 
* status = #active 
* content = #complete 
* version = "201301" 
* description = "**Beschreibung:** EXNDS Codes für Dateiformate" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.199" 
* date = "2013-01-10" 
* count = 8 
* concept[0].code = #datev
* concept[0].display = "Buchhaltungsdaten"
* concept[1].code = #dep
* concept[1].display = "Datenerfassungsprotokoll"
* concept[2].code = #ebi
* concept[2].display = "ebInterface"
* concept[3].code = #edi
* concept[3].display = "EDIVKA-Datenaustausch"
* concept[4].code = #ical
* concept[4].display = "iCalendar"
* concept[5].code = #json
* concept[5].display = "JSON"
* concept[6].code = #pep
* concept[6].display = "Peppol-UBL"
* concept[7].code = #wah
* concept[7].display = "WAHonline"
