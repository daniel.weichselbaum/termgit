Instance: exnds-formatcodes 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-exnds-formatcodes" 
* name = "exnds-formatcodes" 
* title = "EXNDS_FormatCodes" 
* status = #active 
* content = #complete 
* version = "201301" 
* description = "**Beschreibung:** EXNDS Codes für Dateiformate" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.199" 
* date = "2013-01-10" 
* count = 8 
* #datev "Buchhaltungsdaten"
* #dep "Datenerfassungsprotokoll"
* #ebi "ebInterface"
* #edi "EDIVKA-Datenaustausch"
* #ical "iCalendar"
* #json "JSON"
* #pep "Peppol-UBL"
* #wah "WAHonline"
