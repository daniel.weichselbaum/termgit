Instance: eimpf-impfgrund 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-eimpf-impfgrund" 
* name = "eimpf-impfgrund" 
* title = "eImpf_Impfgrund" 
* status = #active 
* version = "201908" 
* description = "**Description:** Billing-related reasons for vaccinations

**Beschreibung:** Abrechnungsrelevante Gründe für Impfungen" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.7" 
* date = "2019-08-01" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-eimpf-ergaenzung"
* compose.include[0].concept[0].code = "IG1"
* compose.include[0].concept[0].display = "Indikationsimpfung für Risikogruppe"
* compose.include[0].concept[1].code = "IG2"
* compose.include[0].concept[1].display = "Wiederholungsimpfung aufgrund medizinischer Indikation"
