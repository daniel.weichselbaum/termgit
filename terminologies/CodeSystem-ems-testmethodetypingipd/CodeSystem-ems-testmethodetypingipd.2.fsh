Instance: ems-testmethodetypingipd 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-testmethodetypingipd" 
* name = "ems-testmethodetypingipd" 
* title = "EMS_TestMethodeTypingIPD" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Liste Testmethoden Typing IPD: Verwendung bei Pneumokokken" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.80" 
* date = "2017-01-26" 
* count = 6 
* #AGGLUGEL "Geldiffusionstest"
* #AGGLUKO "Koagglutination"
* #AGGLUOBJEKT "Objektträgeragglutination"
* #AGGLUPNEUMOTEST "Pneumotest©"
* #AGGLUQUELL "Quellungsreaktion"
* #NUCLACIDMULTI "Multiplex PCR"
