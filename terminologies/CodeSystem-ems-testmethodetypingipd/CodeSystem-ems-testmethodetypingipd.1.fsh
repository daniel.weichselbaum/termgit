Instance: ems-testmethodetypingipd 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-testmethodetypingipd" 
* name = "ems-testmethodetypingipd" 
* title = "EMS_TestMethodeTypingIPD" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Liste Testmethoden Typing IPD: Verwendung bei Pneumokokken" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.80" 
* date = "2017-01-26" 
* count = 6 
* concept[0].code = #AGGLUGEL
* concept[0].display = "Geldiffusionstest"
* concept[1].code = #AGGLUKO
* concept[1].display = "Koagglutination"
* concept[2].code = #AGGLUOBJEKT
* concept[2].display = "Objektträgeragglutination"
* concept[3].code = #AGGLUPNEUMOTEST
* concept[3].display = "Pneumotest©"
* concept[4].code = #AGGLUQUELL
* concept[4].display = "Quellungsreaktion"
* concept[5].code = #NUCLACIDMULTI
* concept[5].display = "Multiplex PCR"
