Instance: hl7-language-identification 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-hl7-language-identification" 
* name = "hl7-language-identification" 
* title = "HL7 Language Identification" 
* status = #active 
* content = #complete 
* version = "202105" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.6.121" 
* date = "2020-08-20" 
* count = 516 
* #aa "Danakil-Sprache"
* #aa ^designation[0].language = #de-AT 
* #aa ^designation[0].value = "Danakil-Sprache" 
* #ab "Abchasisch"
* #ab ^designation[0].language = #de-AT 
* #ab ^designation[0].value = "Abchasisch" 
* #ace "Aceh-Sprache"
* #ace ^designation[0].language = #de-AT 
* #ace ^designation[0].value = "Aceh-Sprache" 
* #ach "Acholi-Sprache"
* #ach ^designation[0].language = #de-AT 
* #ach ^designation[0].value = "Acholi-Sprache" 
* #ada "Adangme-Sprache"
* #ada ^designation[0].language = #de-AT 
* #ada ^designation[0].value = "Adangme-Sprache" 
* #ady "Adygisch"
* #ady ^designation[0].language = #de-AT 
* #ady ^designation[0].value = "Adygisch" 
* #ae "Avestisch"
* #ae ^designation[0].language = #de-AT 
* #ae ^designation[0].value = "Avestisch" 
* #af "Afrikaans"
* #af ^designation[0].language = #de-AT 
* #af ^designation[0].value = "Afrikaans" 
* #afa "Hamitosemitische Sprachen (Andere)"
* #afa ^designation[0].language = #de-AT 
* #afa ^designation[0].value = "Hamitosemitische Sprachen (Andere)" 
* #afh "Afrihili"
* #afh ^designation[0].language = #de-AT 
* #afh ^designation[0].value = "Afrihili" 
* #ain "Ainu-Sprache"
* #ain ^designation[0].language = #de-AT 
* #ain ^designation[0].value = "Ainu-Sprache" 
* #ak "Akan-Sprache"
* #ak ^designation[0].language = #de-AT 
* #ak ^designation[0].value = "Akan-Sprache" 
* #akk "Akkadisch"
* #akk ^designation[0].language = #de-AT 
* #akk ^designation[0].value = "Akkadisch" 
* #ale "Aleutisch"
* #ale ^designation[0].language = #de-AT 
* #ale ^designation[0].value = "Aleutisch" 
* #alg "Algonkin-Sprachen (Andere)"
* #alg ^designation[0].language = #de-AT 
* #alg ^designation[0].value = "Algonkin-Sprachen (Andere)" 
* #alt "Altaisch"
* #alt ^designation[0].language = #de-AT 
* #alt ^designation[0].value = "Altaisch" 
* #am "Amharisch"
* #am ^designation[0].language = #de-AT 
* #am ^designation[0].value = "Amharisch" 
* #an "Aragonesisch"
* #an ^designation[0].language = #de-AT 
* #an ^designation[0].value = "Aragonesisch" 
* #ang "Altenglisch"
* #ang ^designation[0].language = #de-AT 
* #ang ^designation[0].value = "Altenglisch" 
* #anp "Anga-Sprache"
* #anp ^designation[0].language = #de-AT 
* #anp ^designation[0].value = "Anga-Sprache" 
* #apa "Apachen-Sprachen"
* #apa ^designation[0].language = #de-AT 
* #apa ^designation[0].value = "Apachen-Sprachen" 
* #ar "Arabisch"
* #ar ^designation[0].language = #de-AT 
* #ar ^designation[0].value = "Arabisch" 
* #arc "Aramäisch"
* #arc ^designation[0].language = #de-AT 
* #arc ^designation[0].value = "Aramäisch" 
* #arn "Arauka-Sprachen"
* #arn ^designation[0].language = #de-AT 
* #arn ^designation[0].value = "Arauka-Sprachen" 
* #arp "Arapaho-Sprache"
* #arp ^designation[0].language = #de-AT 
* #arp ^designation[0].value = "Arapaho-Sprache" 
* #art "Kunstsprachen (Andere)"
* #art ^designation[0].language = #de-AT 
* #art ^designation[0].value = "Kunstsprachen (Andere)" 
* #arw "Arawak-Sprachen"
* #arw ^designation[0].language = #de-AT 
* #arw ^designation[0].value = "Arawak-Sprachen" 
* #as "Assamesisch"
* #as ^designation[0].language = #de-AT 
* #as ^designation[0].value = "Assamesisch" 
* #ast "Asturisch"
* #ast ^designation[0].language = #de-AT 
* #ast ^designation[0].value = "Asturisch" 
* #ath "Athapaskische Sprachen (Andere)"
* #ath ^designation[0].language = #de-AT 
* #ath ^designation[0].value = "Athapaskische Sprachen (Andere)" 
* #aus "Australische Sprachen"
* #aus ^designation[0].language = #de-AT 
* #aus ^designation[0].value = "Australische Sprachen" 
* #av "Awarisch"
* #av ^designation[0].language = #de-AT 
* #av ^designation[0].value = "Awarisch" 
* #awa "Awadhi"
* #awa ^designation[0].language = #de-AT 
* #awa ^designation[0].value = "Awadhi" 
* #ay "Aymará-Sprache"
* #ay ^designation[0].language = #de-AT 
* #ay ^designation[0].value = "Aymará-Sprache" 
* #az "Aserbeidschanisch"
* #az ^designation[0].language = #de-AT 
* #az ^designation[0].value = "Aserbeidschanisch" 
* #ba "Baschkirisch"
* #ba ^designation[0].language = #de-AT 
* #ba ^designation[0].value = "Baschkirisch" 
* #bad "Banda-Sprachen (Ubangi-Sprachen)"
* #bad ^designation[0].language = #de-AT 
* #bad ^designation[0].value = "Banda-Sprachen (Ubangi-Sprachen)" 
* #bai "Bamileke-Sprachen"
* #bai ^designation[0].language = #de-AT 
* #bai ^designation[0].value = "Bamileke-Sprachen" 
* #bal "Belutschisch"
* #bal ^designation[0].language = #de-AT 
* #bal ^designation[0].value = "Belutschisch" 
* #ban "Balinesisch"
* #ban ^designation[0].language = #de-AT 
* #ban ^designation[0].value = "Balinesisch" 
* #bas "Basaa-Sprache"
* #bas ^designation[0].language = #de-AT 
* #bas ^designation[0].value = "Basaa-Sprache" 
* #bat "Baltische Sprachen (Andere)"
* #bat ^designation[0].language = #de-AT 
* #bat ^designation[0].value = "Baltische Sprachen (Andere)" 
* #be "Weißrussisch"
* #be ^designation[0].language = #de-AT 
* #be ^designation[0].value = "Weißrussisch" 
* #bej "Bedauye"
* #bej ^designation[0].language = #de-AT 
* #bej ^designation[0].value = "Bedauye" 
* #bem "Bemba-Sprache"
* #bem ^designation[0].language = #de-AT 
* #bem ^designation[0].value = "Bemba-Sprache" 
* #ber "Berbersprachen (Andere)"
* #ber ^designation[0].language = #de-AT 
* #ber ^designation[0].value = "Berbersprachen (Andere)" 
* #bg "Bulgarisch"
* #bg ^designation[0].language = #de-AT 
* #bg ^designation[0].value = "Bulgarisch" 
* #bh "Bihari (Andere)"
* #bh ^designation[0].language = #de-AT 
* #bh ^designation[0].value = "Bihari (Andere)" 
* #bho "Bhojpuri"
* #bho ^designation[0].language = #de-AT 
* #bho ^designation[0].value = "Bhojpuri" 
* #bi "Beach-la-mar"
* #bi ^designation[0].language = #de-AT 
* #bi ^designation[0].value = "Beach-la-mar" 
* #bik "Bikol-Sprache"
* #bik ^designation[0].language = #de-AT 
* #bik ^designation[0].value = "Bikol-Sprache" 
* #bin "Edo-Sprache"
* #bin ^designation[0].language = #de-AT 
* #bin ^designation[0].value = "Edo-Sprache" 
* #bla "Blackfoot-Sprache"
* #bla ^designation[0].language = #de-AT 
* #bla ^designation[0].value = "Blackfoot-Sprache" 
* #bm "Bambara-Sprache"
* #bm ^designation[0].language = #de-AT 
* #bm ^designation[0].value = "Bambara-Sprache" 
* #bn "Bengali"
* #bn ^designation[0].language = #de-AT 
* #bn ^designation[0].value = "Bengali" 
* #bnt "Bantusprachen (Andere)"
* #bnt ^designation[0].language = #de-AT 
* #bnt ^designation[0].value = "Bantusprachen (Andere)" 
* #bo "Tibetisch"
* #bo ^designation[0].language = #de-AT 
* #bo ^designation[0].value = "Tibetisch" 
* #br "Bretonisch"
* #br ^designation[0].language = #de-AT 
* #br ^designation[0].value = "Bretonisch" 
* #bra "Braj-Bhakha"
* #bra ^designation[0].language = #de-AT 
* #bra ^designation[0].value = "Braj-Bhakha" 
* #bs "Bosnisch"
* #bs ^designation[0].language = #de-AT 
* #bs ^designation[0].value = "Bosnisch" 
* #btk "Batak-Sprache"
* #btk ^designation[0].language = #de-AT 
* #btk ^designation[0].value = "Batak-Sprache" 
* #bua "Burjatisch"
* #bua ^designation[0].language = #de-AT 
* #bua ^designation[0].value = "Burjatisch" 
* #bug "Bugi-Sprache"
* #bug ^designation[0].language = #de-AT 
* #bug ^designation[0].value = "Bugi-Sprache" 
* #byn "Bilin-Sprache"
* #byn ^designation[0].language = #de-AT 
* #byn ^designation[0].value = "Bilin-Sprache" 
* #ca "Katalanisch"
* #ca ^designation[0].language = #de-AT 
* #ca ^designation[0].value = "Katalanisch" 
* #cad "Caddo-Sprachen"
* #cad ^designation[0].language = #de-AT 
* #cad ^designation[0].value = "Caddo-Sprachen" 
* #cai "Indianersprachen, Zentralamerika (Andere)"
* #cai ^designation[0].language = #de-AT 
* #cai ^designation[0].value = "Indianersprachen, Zentralamerika (Andere)" 
* #car "Karibische Sprachen"
* #car ^designation[0].language = #de-AT 
* #car ^designation[0].value = "Karibische Sprachen" 
* #cau "Kaukasische Sprachen (Andere)"
* #cau ^designation[0].language = #de-AT 
* #cau ^designation[0].value = "Kaukasische Sprachen (Andere)" 
* #ce "Tschetschenisch"
* #ce ^designation[0].language = #de-AT 
* #ce ^designation[0].value = "Tschetschenisch" 
* #ceb "Cebuano"
* #ceb ^designation[0].language = #de-AT 
* #ceb ^designation[0].value = "Cebuano" 
* #cel "Keltische Sprachen (Andere)"
* #cel ^designation[0].language = #de-AT 
* #cel ^designation[0].value = "Keltische Sprachen (Andere)" 
* #ch "Chamorro-Sprache"
* #ch ^designation[0].language = #de-AT 
* #ch ^designation[0].value = "Chamorro-Sprache" 
* #chb "Chibcha-Sprachen"
* #chb ^designation[0].language = #de-AT 
* #chb ^designation[0].value = "Chibcha-Sprachen" 
* #chg "Tschagataisch"
* #chg ^designation[0].language = #de-AT 
* #chg ^designation[0].value = "Tschagataisch" 
* #chk "Trukesisch"
* #chk ^designation[0].language = #de-AT 
* #chk ^designation[0].value = "Trukesisch" 
* #chm "Tscheremissisch"
* #chm ^designation[0].language = #de-AT 
* #chm ^designation[0].value = "Tscheremissisch" 
* #chn "Chinook-Jargon"
* #chn ^designation[0].language = #de-AT 
* #chn ^designation[0].value = "Chinook-Jargon" 
* #cho "Choctaw-Sprache"
* #cho ^designation[0].language = #de-AT 
* #cho ^designation[0].value = "Choctaw-Sprache" 
* #chp "Chipewyan-Sprache"
* #chp ^designation[0].language = #de-AT 
* #chp ^designation[0].value = "Chipewyan-Sprache" 
* #chr "Cherokee-Sprache"
* #chr ^designation[0].language = #de-AT 
* #chr ^designation[0].value = "Cherokee-Sprache" 
* #chy "Cheyenne-Sprache"
* #chy ^designation[0].language = #de-AT 
* #chy ^designation[0].value = "Cheyenne-Sprache" 
* #cmc "Cham-Sprachen"
* #cmc ^designation[0].language = #de-AT 
* #cmc ^designation[0].value = "Cham-Sprachen" 
* #co "Korsisch"
* #co ^designation[0].language = #de-AT 
* #co ^designation[0].value = "Korsisch" 
* #cop "Koptisch"
* #cop ^designation[0].language = #de-AT 
* #cop ^designation[0].value = "Koptisch" 
* #cpe "Kreolisch-Englisch (Andere)"
* #cpe ^designation[0].language = #de-AT 
* #cpe ^designation[0].value = "Kreolisch-Englisch (Andere)" 
* #cpf "Kreolisch-Französisch (Andere)"
* #cpf ^designation[0].language = #de-AT 
* #cpf ^designation[0].value = "Kreolisch-Französisch (Andere)" 
* #cpp "Kreolisch-Portugiesisch (Andere)"
* #cpp ^designation[0].language = #de-AT 
* #cpp ^designation[0].value = "Kreolisch-Portugiesisch (Andere)" 
* #cr "Cree-Sprache"
* #cr ^designation[0].language = #de-AT 
* #cr ^designation[0].value = "Cree-Sprache" 
* #crh "Krimtatarisch"
* #crh ^designation[0].language = #de-AT 
* #crh ^designation[0].value = "Krimtatarisch" 
* #crp "Kreolische Sprachen; Pidginsprachen (Andere)"
* #crp ^designation[0].language = #de-AT 
* #crp ^designation[0].value = "Kreolische Sprachen; Pidginsprachen (Andere)" 
* #cs "Tschechisch"
* #cs ^designation[0].language = #de-AT 
* #cs ^designation[0].value = "Tschechisch" 
* #csb "Kaschubisch"
* #csb ^designation[0].language = #de-AT 
* #csb ^designation[0].value = "Kaschubisch" 
* #cu "Kirchenslawisch"
* #cu ^designation[0].language = #de-AT 
* #cu ^designation[0].value = "Kirchenslawisch" 
* #cus "Kuschitische Sprachen (Andere)"
* #cus ^designation[0].language = #de-AT 
* #cus ^designation[0].value = "Kuschitische Sprachen (Andere)" 
* #cv "Tschuwaschisch"
* #cv ^designation[0].language = #de-AT 
* #cv ^designation[0].value = "Tschuwaschisch" 
* #cy "Kymrisch"
* #cy ^designation[0].language = #de-AT 
* #cy ^designation[0].value = "Kymrisch" 
* #da "Dänisch"
* #da ^designation[0].language = #de-AT 
* #da ^designation[0].value = "Dänisch" 
* #dak "Dakota-Sprache"
* #dak ^designation[0].language = #de-AT 
* #dak ^designation[0].value = "Dakota-Sprache" 
* #dar "Darginisch"
* #dar ^designation[0].language = #de-AT 
* #dar ^designation[0].value = "Darginisch" 
* #day "Dajakisch"
* #day ^designation[0].language = #de-AT 
* #day ^designation[0].value = "Dajakisch" 
* #de "Deutsch"
* #de ^designation[0].language = #de-AT 
* #de ^designation[0].value = "Deutsch" 
* #de-AT "Österreichisches Deutsch"
* #de-CH "Deutsch (Schweiz)"
* #de-CH ^definition = HL7 Tags for the Identification of Languages
* #de-DE "Deutsch (Deutschland)"
* #de-DE ^definition = HL7 Tags for the Identification of Languages
* #del "Delaware-Sprache"
* #del ^designation[0].language = #de-AT 
* #del ^designation[0].value = "Delaware-Sprache" 
* #den "Slave-Sprache"
* #den ^designation[0].language = #de-AT 
* #den ^designation[0].value = "Slave-Sprache" 
* #dgr "Dogrib-Sprache"
* #dgr ^designation[0].language = #de-AT 
* #dgr ^designation[0].value = "Dogrib-Sprache" 
* #din "Dinka-Sprache"
* #din ^designation[0].language = #de-AT 
* #din ^designation[0].value = "Dinka-Sprache" 
* #doi "Dogri"
* #doi ^designation[0].language = #de-AT 
* #doi ^designation[0].value = "Dogri" 
* #dra "Drawidische Sprachen (Andere)"
* #dra ^designation[0].language = #de-AT 
* #dra ^designation[0].value = "Drawidische Sprachen (Andere)" 
* #dsb "Niedersorbisch"
* #dsb ^designation[0].language = #de-AT 
* #dsb ^designation[0].value = "Niedersorbisch" 
* #dua "Duala-Sprachen"
* #dua ^designation[0].language = #de-AT 
* #dua ^designation[0].value = "Duala-Sprachen" 
* #dum "Mittelniederländisch"
* #dum ^designation[0].language = #de-AT 
* #dum ^designation[0].value = "Mittelniederländisch" 
* #dv "Maledivisch"
* #dv ^designation[0].language = #de-AT 
* #dv ^designation[0].value = "Maledivisch" 
* #dyu "Dyula-Sprache"
* #dyu ^designation[0].language = #de-AT 
* #dyu ^designation[0].value = "Dyula-Sprache" 
* #dz "Dzongkha"
* #dz ^designation[0].language = #de-AT 
* #dz ^designation[0].value = "Dzongkha" 
* #ee "Ewe-Sprache"
* #ee ^designation[0].language = #de-AT 
* #ee ^designation[0].value = "Ewe-Sprache" 
* #efi "Efik"
* #efi ^designation[0].language = #de-AT 
* #efi ^designation[0].value = "Efik" 
* #egy "Ägyptisch"
* #egy ^designation[0].language = #de-AT 
* #egy ^designation[0].value = "Ägyptisch" 
* #eka "Ekajuk"
* #eka ^designation[0].language = #de-AT 
* #eka ^designation[0].value = "Ekajuk" 
* #el "Neugriechisch"
* #el ^designation[0].language = #de-AT 
* #el ^designation[0].value = "Neugriechisch" 
* #elx "Elamisch"
* #elx ^designation[0].language = #de-AT 
* #elx ^designation[0].value = "Elamisch" 
* #en "Englisch"
* #en ^designation[0].language = #de-AT 
* #en ^designation[0].value = "Englisch" 
* #en-AU "Englisch (Australien)"
* #en-AU ^definition = HL7 Tags for the Identification of Languages
* #en-CA "Englisch (Kanada)"
* #en-CA ^definition = HL7 Tags for the Identification of Languages
* #en-GB "Englisch (GB)"
* #en-GB ^definition = HL7 Tags for the Identification of Languages
* #en-IN "Englisch (Indien)"
* #en-IN ^definition = HL7 Tags for the Identification of Languages
* #en-NZ "Englisch (Neuseeland)"
* #en-NZ ^definition = HL7 Tags for the Identification of Languages
* #en-SG "Englisch (Singapur)"
* #en-SG ^definition = HL7 Tags for the Identification of Languages
* #en-US "Englisch (US)"
* #en-US ^definition = HL7 Tags for the Identification of Languages
* #enm "Mittelenglisch"
* #enm ^designation[0].language = #de-AT 
* #enm ^designation[0].value = "Mittelenglisch" 
* #eo "Esperanto"
* #eo ^designation[0].language = #de-AT 
* #eo ^designation[0].value = "Esperanto" 
* #es "Spanisch"
* #es ^designation[0].language = #de-AT 
* #es ^designation[0].value = "Spanisch" 
* #es-AR "Spanisch (Argentinien)"
* #es-AR ^definition = HL7 Tags for the Identification of Languages
* #es-ES "Spanish (Spanien)"
* #es-ES ^definition = HL7 Tags for the Identification of Languages
* #es-UY "Spanish (Uruguay)"
* #es-UY ^definition = HL7 Tags for the Identification of Languages
* #et "Estnisch"
* #et ^designation[0].language = #de-AT 
* #et ^designation[0].value = "Estnisch" 
* #eu "Baskisch"
* #eu ^designation[0].language = #de-AT 
* #eu ^designation[0].value = "Baskisch" 
* #ewo "Ewondo"
* #ewo ^designation[0].language = #de-AT 
* #ewo ^designation[0].value = "Ewondo" 
* #fa "Persisch"
* #fa ^designation[0].language = #de-AT 
* #fa ^designation[0].value = "Persisch" 
* #fan "Pangwe-Sprache"
* #fan ^designation[0].language = #de-AT 
* #fan ^designation[0].value = "Pangwe-Sprache" 
* #fat "Fante-Sprache"
* #fat ^designation[0].language = #de-AT 
* #fat ^designation[0].value = "Fante-Sprache" 
* #ff "Ful"
* #ff ^designation[0].language = #de-AT 
* #ff ^designation[0].value = "Ful" 
* #fi "Finnisch"
* #fi ^designation[0].language = #de-AT 
* #fi ^designation[0].value = "Finnisch" 
* #fil "Pilipino"
* #fil ^designation[0].language = #de-AT 
* #fil ^designation[0].value = "Pilipino" 
* #fiu "Finnougrische Sprachen (Andere)"
* #fiu ^designation[0].language = #de-AT 
* #fiu ^designation[0].value = "Finnougrische Sprachen (Andere)" 
* #fj "Fidschi-Sprache"
* #fj ^designation[0].language = #de-AT 
* #fj ^designation[0].value = "Fidschi-Sprache" 
* #fo "Färöisch"
* #fo ^designation[0].language = #de-AT 
* #fo ^designation[0].value = "Färöisch" 
* #fon "Fon-Sprache"
* #fon ^designation[0].language = #de-AT 
* #fon ^designation[0].value = "Fon-Sprache" 
* #fr "Französisch"
* #fr ^designation[0].language = #de-AT 
* #fr ^designation[0].value = "Französisch" 
* #fr-BE "Französisch (Belgien)"
* #fr-BE ^definition = HL7 Tags for the Identification of Languages
* #fr-CH "Französisch (Schweiz)"
* #fr-CH ^definition = HL7 Tags for the Identification of Languages
* #fr-FR "Französisch (Frankreich)"
* #fr-FR ^definition = HL7 Tags for the Identification of Languages
* #frm "Mittelfranzösisch"
* #frm ^designation[0].language = #de-AT 
* #frm ^designation[0].value = "Mittelfranzösisch" 
* #fro "Altfranzösisch"
* #fro ^designation[0].language = #de-AT 
* #fro ^designation[0].value = "Altfranzösisch" 
* #frr "Nordfriesisch"
* #frr ^designation[0].language = #de-AT 
* #frr ^designation[0].value = "Nordfriesisch" 
* #frs "Ostfriesisch"
* #frs ^designation[0].language = #de-AT 
* #frs ^designation[0].value = "Ostfriesisch" 
* #fur "Friulisch"
* #fur ^designation[0].language = #de-AT 
* #fur ^designation[0].value = "Friulisch" 
* #fy "Friesisch"
* #fy ^designation[0].language = #de-AT 
* #fy ^designation[0].value = "Friesisch" 
* #fy-NL "Friesisch (Niederlande)"
* #fy-NL ^definition = HL7 Tags for the Identification of Languages
* #ga "Irisch"
* #ga ^designation[0].language = #de-AT 
* #ga ^designation[0].value = "Irisch" 
* #gaa "Ga-Sprache"
* #gaa ^designation[0].language = #de-AT 
* #gaa ^designation[0].value = "Ga-Sprache" 
* #gay "Gayo-Sprache"
* #gay ^designation[0].language = #de-AT 
* #gay ^designation[0].value = "Gayo-Sprache" 
* #gba "Gbaya-Sprache"
* #gba ^designation[0].language = #de-AT 
* #gba ^designation[0].value = "Gbaya-Sprache" 
* #gd "Gälisch-Schottisch"
* #gd ^designation[0].language = #de-AT 
* #gd ^designation[0].value = "Gälisch-Schottisch" 
* #gem "Germanische Sprachen (Andere)"
* #gem ^designation[0].language = #de-AT 
* #gem ^designation[0].value = "Germanische Sprachen (Andere)" 
* #gez "Altäthiopisch"
* #gez ^designation[0].language = #de-AT 
* #gez ^designation[0].value = "Altäthiopisch" 
* #gil "Gilbertesisch"
* #gil ^designation[0].language = #de-AT 
* #gil ^designation[0].value = "Gilbertesisch" 
* #gl "Galicisch"
* #gl ^designation[0].language = #de-AT 
* #gl ^designation[0].value = "Galicisch" 
* #gmh "Mittelhochdeutsch"
* #gmh ^designation[0].language = #de-AT 
* #gmh ^designation[0].value = "Mittelhochdeutsch" 
* #gn "Guaraní-Sprache"
* #gn ^designation[0].language = #de-AT 
* #gn ^designation[0].value = "Guaraní-Sprache" 
* #goh "Althochdeutsch"
* #goh ^designation[0].language = #de-AT 
* #goh ^designation[0].value = "Althochdeutsch" 
* #gon "Gondi-Sprache"
* #gon ^designation[0].language = #de-AT 
* #gon ^designation[0].value = "Gondi-Sprache" 
* #gor "Gorontalesisch"
* #gor ^designation[0].language = #de-AT 
* #gor ^designation[0].value = "Gorontalesisch" 
* #got "Gotisch"
* #got ^designation[0].language = #de-AT 
* #got ^designation[0].value = "Gotisch" 
* #grb "Grebo-Sprache"
* #grb ^designation[0].language = #de-AT 
* #grb ^designation[0].value = "Grebo-Sprache" 
* #grc "Griechisch"
* #grc ^designation[0].language = #de-AT 
* #grc ^designation[0].value = "Griechisch" 
* #gsw "Schweizerdeutsch"
* #gsw ^designation[0].language = #de-AT 
* #gsw ^designation[0].value = "Schweizerdeutsch" 
* #gu "Gujarati-Sprache"
* #gu ^designation[0].language = #de-AT 
* #gu ^designation[0].value = "Gujarati-Sprache" 
* #gv "Manx"
* #gv ^designation[0].language = #de-AT 
* #gv ^designation[0].value = "Manx" 
* #gwi "Kutchin-Sprache"
* #gwi ^designation[0].language = #de-AT 
* #gwi ^designation[0].value = "Kutchin-Sprache" 
* #ha "Haussa-Sprache"
* #ha ^designation[0].language = #de-AT 
* #ha ^designation[0].value = "Haussa-Sprache" 
* #hai "Haida-Sprache"
* #hai ^designation[0].language = #de-AT 
* #hai ^designation[0].value = "Haida-Sprache" 
* #haw "Hawaiisch"
* #haw ^designation[0].language = #de-AT 
* #haw ^designation[0].value = "Hawaiisch" 
* #he "Hebräisch"
* #he ^designation[0].language = #de-AT 
* #he ^designation[0].value = "Hebräisch" 
* #hi "Hindi"
* #hi ^designation[0].language = #de-AT 
* #hi ^designation[0].value = "Hindi" 
* #hil "Hiligaynon-Sprache"
* #hil ^designation[0].language = #de-AT 
* #hil ^designation[0].value = "Hiligaynon-Sprache" 
* #him "Himachali"
* #him ^designation[0].language = #de-AT 
* #him ^designation[0].value = "Himachali" 
* #hit "Hethitisch"
* #hit ^designation[0].language = #de-AT 
* #hit ^designation[0].value = "Hethitisch" 
* #hmn "Miao-Sprachen"
* #hmn ^designation[0].language = #de-AT 
* #hmn ^designation[0].value = "Miao-Sprachen" 
* #ho "Hiri-Motu"
* #ho ^designation[0].language = #de-AT 
* #ho ^designation[0].value = "Hiri-Motu" 
* #hr "Kroatisch"
* #hr ^designation[0].language = #de-AT 
* #hr ^designation[0].value = "Kroatisch" 
* #hsb "Obersorbisch"
* #hsb ^designation[0].language = #de-AT 
* #hsb ^designation[0].value = "Obersorbisch" 
* #ht "Haïtien (Haiti-Kreolisch)"
* #ht ^designation[0].language = #de-AT 
* #ht ^designation[0].value = "Haïtien (Haiti-Kreolisch)" 
* #hu "Ungarisch"
* #hu ^designation[0].language = #de-AT 
* #hu ^designation[0].value = "Ungarisch" 
* #hup "Hupa-Sprache"
* #hup ^designation[0].language = #de-AT 
* #hup ^designation[0].value = "Hupa-Sprache" 
* #hy "Armenisch"
* #hy ^designation[0].language = #de-AT 
* #hy ^designation[0].value = "Armenisch" 
* #hz "Herero-Sprache"
* #hz ^designation[0].language = #de-AT 
* #hz ^designation[0].value = "Herero-Sprache" 
* #ia "Interlingua"
* #ia ^designation[0].language = #de-AT 
* #ia ^designation[0].value = "Interlingua" 
* #iba "Iban-Sprache"
* #iba ^designation[0].language = #de-AT 
* #iba ^designation[0].value = "Iban-Sprache" 
* #id "Bahasa Indonesia"
* #id ^designation[0].language = #de-AT 
* #id ^designation[0].value = "Bahasa Indonesia" 
* #ie "Interlingue"
* #ie ^designation[0].language = #de-AT 
* #ie ^designation[0].value = "Interlingue" 
* #ig "Ibo-Sprache"
* #ig ^designation[0].language = #de-AT 
* #ig ^designation[0].value = "Ibo-Sprache" 
* #ii "Lalo-Sprache"
* #ii ^designation[0].language = #de-AT 
* #ii ^designation[0].value = "Lalo-Sprache" 
* #ijo "Ijo-Sprache"
* #ijo ^designation[0].language = #de-AT 
* #ijo ^designation[0].value = "Ijo-Sprache" 
* #ik "Inupik"
* #ik ^designation[0].language = #de-AT 
* #ik ^designation[0].value = "Inupik" 
* #ilo "Ilokano-Sprache"
* #ilo ^designation[0].language = #de-AT 
* #ilo ^designation[0].value = "Ilokano-Sprache" 
* #inc "Indoarische Sprachen (Andere)"
* #inc ^designation[0].language = #de-AT 
* #inc ^designation[0].value = "Indoarische Sprachen (Andere)" 
* #ine "Indogermanische Sprachen (Andere)"
* #ine ^designation[0].language = #de-AT 
* #ine ^designation[0].value = "Indogermanische Sprachen (Andere)" 
* #inh "Inguschisch"
* #inh ^designation[0].language = #de-AT 
* #inh ^designation[0].value = "Inguschisch" 
* #io "Ido"
* #io ^designation[0].language = #de-AT 
* #io ^designation[0].value = "Ido" 
* #ira "Iranische Sprachen (Andere)"
* #ira ^designation[0].language = #de-AT 
* #ira ^designation[0].value = "Iranische Sprachen (Andere)" 
* #iro "Irokesische Sprachen"
* #iro ^designation[0].language = #de-AT 
* #iro ^designation[0].value = "Irokesische Sprachen" 
* #is "Isländisch"
* #is ^designation[0].language = #de-AT 
* #is ^designation[0].value = "Isländisch" 
* #it "Italienisch"
* #it ^designation[0].language = #de-AT 
* #it ^designation[0].value = "Italienisch" 
* #it-CH "Italienisch (Schweiz)"
* #it-CH ^definition = HL7 Tags for the Identification of Languages
* #it-IT "Italienisch (Italien)"
* #it-IT ^definition = HL7 Tags for the Identification of Languages
* #iu "Inuktitut"
* #iu ^designation[0].language = #de-AT 
* #iu ^designation[0].value = "Inuktitut" 
* #ja "Japanisch"
* #ja ^designation[0].language = #de-AT 
* #ja ^designation[0].value = "Japanisch" 
* #jbo "Lojban"
* #jbo ^designation[0].language = #de-AT 
* #jbo ^designation[0].value = "Lojban" 
* #jpr "Jüdisch-Persisch"
* #jpr ^designation[0].language = #de-AT 
* #jpr ^designation[0].value = "Jüdisch-Persisch" 
* #jrb "Jüdisch-Arabisch"
* #jrb ^designation[0].language = #de-AT 
* #jrb ^designation[0].value = "Jüdisch-Arabisch" 
* #jv "Javanisch"
* #jv ^designation[0].language = #de-AT 
* #jv ^designation[0].value = "Javanisch" 
* #ka "Georgisch"
* #ka ^designation[0].language = #de-AT 
* #ka ^designation[0].value = "Georgisch" 
* #kaa "Karakalpakisch"
* #kaa ^designation[0].language = #de-AT 
* #kaa ^designation[0].value = "Karakalpakisch" 
* #kab "Kabylisch"
* #kab ^designation[0].language = #de-AT 
* #kab ^designation[0].value = "Kabylisch" 
* #kac "Kachin-Sprache"
* #kac ^designation[0].language = #de-AT 
* #kac ^designation[0].value = "Kachin-Sprache" 
* #kam "Kamba-Sprache"
* #kam ^designation[0].language = #de-AT 
* #kam ^designation[0].value = "Kamba-Sprache" 
* #kar "Karenisch"
* #kar ^designation[0].language = #de-AT 
* #kar ^designation[0].value = "Karenisch" 
* #kaw "Kawi"
* #kaw ^designation[0].language = #de-AT 
* #kaw ^designation[0].value = "Kawi" 
* #kbd "Kabardinisch"
* #kbd ^designation[0].language = #de-AT 
* #kbd ^designation[0].value = "Kabardinisch" 
* #kg "Kongo-Sprache"
* #kg ^designation[0].language = #de-AT 
* #kg ^designation[0].value = "Kongo-Sprache" 
* #kha "Khasi-Sprache"
* #kha ^designation[0].language = #de-AT 
* #kha ^designation[0].value = "Khasi-Sprache" 
* #khi "Khoisan-Sprachen (Andere)"
* #khi ^designation[0].language = #de-AT 
* #khi ^designation[0].value = "Khoisan-Sprachen (Andere)" 
* #kho "Sakisch"
* #kho ^designation[0].language = #de-AT 
* #kho ^designation[0].value = "Sakisch" 
* #ki "Kikuyu-Sprache"
* #ki ^designation[0].language = #de-AT 
* #ki ^designation[0].value = "Kikuyu-Sprache" 
* #kj "Kwanyama-Sprache"
* #kj ^designation[0].language = #de-AT 
* #kj ^designation[0].value = "Kwanyama-Sprache" 
* #kk "Kasachisch"
* #kk ^designation[0].language = #de-AT 
* #kk ^designation[0].value = "Kasachisch" 
* #kl "Grönländisch"
* #kl ^designation[0].language = #de-AT 
* #kl ^designation[0].value = "Grönländisch" 
* #km "Kambodschanisch"
* #km ^designation[0].language = #de-AT 
* #km ^designation[0].value = "Kambodschanisch" 
* #kmb "Kimbundu-Sprache"
* #kmb ^designation[0].language = #de-AT 
* #kmb ^designation[0].value = "Kimbundu-Sprache" 
* #kn "Kannada"
* #kn ^designation[0].language = #de-AT 
* #kn ^designation[0].value = "Kannada" 
* #ko "Koreanisch"
* #ko ^designation[0].language = #de-AT 
* #ko ^designation[0].value = "Koreanisch" 
* #kok "Konkani"
* #kok ^designation[0].language = #de-AT 
* #kok ^designation[0].value = "Konkani" 
* #kos "Kosraeanisch"
* #kos ^designation[0].language = #de-AT 
* #kos ^designation[0].value = "Kosraeanisch" 
* #kpe "Kpelle-Sprache"
* #kpe ^designation[0].language = #de-AT 
* #kpe ^designation[0].value = "Kpelle-Sprache" 
* #kr "Kanuri-Sprache"
* #kr ^designation[0].language = #de-AT 
* #kr ^designation[0].value = "Kanuri-Sprache" 
* #krc "Karatschaiisch-Balkarisch"
* #krc ^designation[0].language = #de-AT 
* #krc ^designation[0].value = "Karatschaiisch-Balkarisch" 
* #krl "Karelisch"
* #krl ^designation[0].language = #de-AT 
* #krl ^designation[0].value = "Karelisch" 
* #kro "Kru-Sprachen (Andere)"
* #kro ^designation[0].language = #de-AT 
* #kro ^designation[0].value = "Kru-Sprachen (Andere)" 
* #kru "Oraon-Sprache"
* #kru ^designation[0].language = #de-AT 
* #kru ^designation[0].value = "Oraon-Sprache" 
* #ks "Kaschmiri"
* #ks ^designation[0].language = #de-AT 
* #ks ^designation[0].value = "Kaschmiri" 
* #ku "Kurdisch"
* #ku ^designation[0].language = #de-AT 
* #ku ^designation[0].value = "Kurdisch" 
* #kum "Kumükisch"
* #kum ^designation[0].language = #de-AT 
* #kum ^designation[0].value = "Kumükisch" 
* #kut "Kutenai-Sprache"
* #kut ^designation[0].language = #de-AT 
* #kut ^designation[0].value = "Kutenai-Sprache" 
* #kv "Komi-Sprache"
* #kv ^designation[0].language = #de-AT 
* #kv ^designation[0].value = "Komi-Sprache" 
* #kw "Kornisch"
* #kw ^designation[0].language = #de-AT 
* #kw ^designation[0].value = "Kornisch" 
* #ky "Kirgisisch"
* #ky ^designation[0].language = #de-AT 
* #ky ^designation[0].value = "Kirgisisch" 
* #la "Latein"
* #la ^designation[0].language = #de-AT 
* #la ^designation[0].value = "Latein" 
* #lad "Judenspanisch"
* #lad ^designation[0].language = #de-AT 
* #lad ^designation[0].value = "Judenspanisch" 
* #lah "Lahnda"
* #lah ^designation[0].language = #de-AT 
* #lah ^designation[0].value = "Lahnda" 
* #lam "Lamba-Sprache (Bantusprache)"
* #lam ^designation[0].language = #de-AT 
* #lam ^designation[0].value = "Lamba-Sprache (Bantusprache)" 
* #lb "Luxemburgisch"
* #lb ^designation[0].language = #de-AT 
* #lb ^designation[0].value = "Luxemburgisch" 
* #lez "Lesgisch"
* #lez ^designation[0].language = #de-AT 
* #lez ^designation[0].value = "Lesgisch" 
* #lg "Ganda-Sprache"
* #lg ^designation[0].language = #de-AT 
* #lg ^designation[0].value = "Ganda-Sprache" 
* #li "Limburgisch"
* #li ^designation[0].language = #de-AT 
* #li ^designation[0].value = "Limburgisch" 
* #ln "Lingala"
* #ln ^designation[0].language = #de-AT 
* #ln ^designation[0].value = "Lingala" 
* #lo "Laotisch"
* #lo ^designation[0].language = #de-AT 
* #lo ^designation[0].value = "Laotisch" 
* #lol "Mongo-Sprache"
* #lol ^designation[0].language = #de-AT 
* #lol ^designation[0].value = "Mongo-Sprache" 
* #loz "Rotse-Sprache"
* #loz ^designation[0].language = #de-AT 
* #loz ^designation[0].value = "Rotse-Sprache" 
* #lt "Litauisch"
* #lt ^designation[0].language = #de-AT 
* #lt ^designation[0].value = "Litauisch" 
* #lu "Luba-Katanga-Sprache"
* #lu ^designation[0].language = #de-AT 
* #lu ^designation[0].value = "Luba-Katanga-Sprache" 
* #lua "Lulua-Sprache"
* #lua ^designation[0].language = #de-AT 
* #lua ^designation[0].value = "Lulua-Sprache" 
* #lui "Luiseño-Sprache"
* #lui ^designation[0].language = #de-AT 
* #lui ^designation[0].value = "Luiseño-Sprache" 
* #lun "Lunda-Sprache"
* #lun ^designation[0].language = #de-AT 
* #lun ^designation[0].value = "Lunda-Sprache" 
* #luo "Luo-Sprache"
* #luo ^designation[0].language = #de-AT 
* #luo ^designation[0].value = "Luo-Sprache" 
* #lus "Lushai-Sprache"
* #lus ^designation[0].language = #de-AT 
* #lus ^designation[0].value = "Lushai-Sprache" 
* #lv "Lettisch"
* #lv ^designation[0].language = #de-AT 
* #lv ^designation[0].value = "Lettisch" 
* #mad "Maduresisch"
* #mad ^designation[0].language = #de-AT 
* #mad ^designation[0].value = "Maduresisch" 
* #mag "Khotta"
* #mag ^designation[0].language = #de-AT 
* #mag ^designation[0].value = "Khotta" 
* #mai "Maithili"
* #mai ^designation[0].language = #de-AT 
* #mai ^designation[0].value = "Maithili" 
* #mak "Makassarisch"
* #mak ^designation[0].language = #de-AT 
* #mak ^designation[0].value = "Makassarisch" 
* #man "Malinke-Sprache"
* #man ^designation[0].language = #de-AT 
* #man ^designation[0].value = "Malinke-Sprache" 
* #map "Austronesische Sprachen (Andere)"
* #map ^designation[0].language = #de-AT 
* #map ^designation[0].value = "Austronesische Sprachen (Andere)" 
* #mas "Massai-Sprache"
* #mas ^designation[0].language = #de-AT 
* #mas ^designation[0].value = "Massai-Sprache" 
* #mdf "Mokscha-Sprache"
* #mdf ^designation[0].language = #de-AT 
* #mdf ^designation[0].value = "Mokscha-Sprache" 
* #mdr "Mandaresisch"
* #mdr ^designation[0].language = #de-AT 
* #mdr ^designation[0].value = "Mandaresisch" 
* #men "Mende-Sprache"
* #men ^designation[0].language = #de-AT 
* #men ^designation[0].value = "Mende-Sprache" 
* #mg "Malagassi-Sprache"
* #mg ^designation[0].language = #de-AT 
* #mg ^designation[0].value = "Malagassi-Sprache" 
* #mga "Mittelirisch"
* #mga ^designation[0].language = #de-AT 
* #mga ^designation[0].value = "Mittelirisch" 
* #mh "Marschallesisch"
* #mh ^designation[0].language = #de-AT 
* #mh ^designation[0].value = "Marschallesisch" 
* #mi "Maori-Sprache"
* #mi ^designation[0].language = #de-AT 
* #mi ^designation[0].value = "Maori-Sprache" 
* #mic "Micmac-Sprache"
* #mic ^designation[0].language = #de-AT 
* #mic ^designation[0].value = "Micmac-Sprache" 
* #min "Minangkabau-Sprache"
* #min ^designation[0].language = #de-AT 
* #min ^designation[0].value = "Minangkabau-Sprache" 
* #mis "Einzelne andere Sprachen"
* #mis ^designation[0].language = #de-AT 
* #mis ^designation[0].value = "Einzelne andere Sprachen" 
* #mk "Makedonisch"
* #mk ^designation[0].language = #de-AT 
* #mk ^designation[0].value = "Makedonisch" 
* #mkh "Mon-Khmer-Sprachen (Andere)"
* #mkh ^designation[0].language = #de-AT 
* #mkh ^designation[0].value = "Mon-Khmer-Sprachen (Andere)" 
* #ml "Malayalam"
* #ml ^designation[0].language = #de-AT 
* #ml ^designation[0].value = "Malayalam" 
* #mn "Mongolisch"
* #mn ^designation[0].language = #de-AT 
* #mn ^designation[0].value = "Mongolisch" 
* #mnc "Mandschurisch"
* #mnc ^designation[0].language = #de-AT 
* #mnc ^designation[0].value = "Mandschurisch" 
* #mni "Meithei-Sprache"
* #mni ^designation[0].language = #de-AT 
* #mni ^designation[0].value = "Meithei-Sprache" 
* #mno "Manobo-Sprachen"
* #mno ^designation[0].language = #de-AT 
* #mno ^designation[0].value = "Manobo-Sprachen" 
* #moh "Mohawk-Sprache"
* #moh ^designation[0].language = #de-AT 
* #moh ^designation[0].value = "Mohawk-Sprache" 
* #mos "Mossi-Sprache"
* #mos ^designation[0].language = #de-AT 
* #mos ^designation[0].value = "Mossi-Sprache" 
* #mr "Marathi"
* #mr ^designation[0].language = #de-AT 
* #mr ^designation[0].value = "Marathi" 
* #ms "Malaiisch"
* #ms ^designation[0].language = #de-AT 
* #ms ^designation[0].value = "Malaiisch" 
* #mt "Maltesisch"
* #mt ^designation[0].language = #de-AT 
* #mt ^designation[0].value = "Maltesisch" 
* #mul "Mehrere Sprachen"
* #mul ^designation[0].language = #de-AT 
* #mul ^designation[0].value = "Mehrere Sprachen" 
* #mun "Mundasprachen (Andere)"
* #mun ^designation[0].language = #de-AT 
* #mun ^designation[0].value = "Mundasprachen (Andere)" 
* #mus "Muskogisch"
* #mus ^designation[0].language = #de-AT 
* #mus ^designation[0].value = "Muskogisch" 
* #mwl "Mirandesisch"
* #mwl ^designation[0].language = #de-AT 
* #mwl ^designation[0].value = "Mirandesisch" 
* #mwr "Marwari"
* #mwr ^designation[0].language = #de-AT 
* #mwr ^designation[0].value = "Marwari" 
* #my "Birmanisch"
* #my ^designation[0].language = #de-AT 
* #my ^designation[0].value = "Birmanisch" 
* #myn "Maya-Sprachen"
* #myn ^designation[0].language = #de-AT 
* #myn ^designation[0].value = "Maya-Sprachen" 
* #myv "Erza-Mordwinisch"
* #myv ^designation[0].language = #de-AT 
* #myv ^designation[0].value = "Erza-Mordwinisch" 
* #na "Nauruanisch"
* #na ^designation[0].language = #de-AT 
* #na ^designation[0].value = "Nauruanisch" 
* #nah "Nahuatl"
* #nah ^designation[0].language = #de-AT 
* #nah ^designation[0].value = "Nahuatl" 
* #nai "Indianersprachen, Nordamerika (Andere)"
* #nai ^designation[0].language = #de-AT 
* #nai ^designation[0].value = "Indianersprachen, Nordamerika (Andere)" 
* #nap "Neapel / Mundart"
* #nap ^designation[0].language = #de-AT 
* #nap ^designation[0].value = "Neapel / Mundart" 
* #nb "Bokmål"
* #nb ^designation[0].language = #de-AT 
* #nb ^designation[0].value = "Bokmål" 
* #nd "Ndebele-Sprache (Simbabwe)"
* #nd ^designation[0].language = #de-AT 
* #nd ^designation[0].value = "Ndebele-Sprache (Simbabwe)" 
* #nds "Niederdeutsch"
* #nds ^designation[0].language = #de-AT 
* #nds ^designation[0].value = "Niederdeutsch" 
* #ne "Nepali"
* #ne ^designation[0].language = #de-AT 
* #ne ^designation[0].value = "Nepali" 
* #new "Newari"
* #new ^designation[0].language = #de-AT 
* #new ^designation[0].value = "Newari" 
* #ng "Ndonga"
* #ng ^designation[0].language = #de-AT 
* #ng ^designation[0].value = "Ndonga" 
* #nia "Nias-Sprache"
* #nia ^designation[0].language = #de-AT 
* #nia ^designation[0].value = "Nias-Sprache" 
* #nic "Nigerkordofanische Sprachen (Andere)"
* #nic ^designation[0].language = #de-AT 
* #nic ^designation[0].value = "Nigerkordofanische Sprachen (Andere)" 
* #niu "Niue-Sprache"
* #niu ^designation[0].language = #de-AT 
* #niu ^designation[0].value = "Niue-Sprache" 
* #nl "Niederländisch"
* #nl ^designation[0].language = #de-AT 
* #nl ^designation[0].value = "Niederländisch" 
* #nl-BE "Niederländisch (Belgien)"
* #nl-BE ^definition = HL7 Tags for the Identification of Languages
* #nl-NL "Niederländisch (Niederlande)"
* #nl-NL ^definition = HL7 Tags for the Identification of Languages
* #nn "Nynorsk"
* #nn ^designation[0].language = #de-AT 
* #nn ^designation[0].value = "Nynorsk" 
* #no "Norwegisch"
* #no ^designation[0].language = #de-AT 
* #no ^designation[0].value = "Norwegisch" 
* #no-NO "Norwegisch (Norwegen)"
* #no-NO ^definition = HL7 Tags for the Identification of Languages
* #nog "Nogaisch"
* #nog ^designation[0].language = #de-AT 
* #nog ^designation[0].value = "Nogaisch" 
* #non "Altnorwegisch"
* #non ^designation[0].language = #de-AT 
* #non ^designation[0].value = "Altnorwegisch" 
* #nqo "N'Ko"
* #nqo ^designation[0].language = #de-AT 
* #nqo ^designation[0].value = "N'Ko" 
* #nr "Ndebele-Sprache (Transvaal)"
* #nr ^designation[0].language = #de-AT 
* #nr ^designation[0].value = "Ndebele-Sprache (Transvaal)" 
* #nso "Pedi-Sprache"
* #nso ^designation[0].language = #de-AT 
* #nso ^designation[0].value = "Pedi-Sprache" 
* #nub "Nubische Sprachen"
* #nub ^designation[0].language = #de-AT 
* #nub ^designation[0].value = "Nubische Sprachen" 
* #nv "Navajo-Sprache"
* #nv ^designation[0].language = #de-AT 
* #nv ^designation[0].value = "Navajo-Sprache" 
* #nwc "Alt-Newari"
* #nwc ^designation[0].language = #de-AT 
* #nwc ^designation[0].value = "Alt-Newari" 
* #ny "Nyanja-Sprache"
* #ny ^designation[0].language = #de-AT 
* #ny ^designation[0].value = "Nyanja-Sprache" 
* #nym "Nyamwezi-Sprache"
* #nym ^designation[0].language = #de-AT 
* #nym ^designation[0].value = "Nyamwezi-Sprache" 
* #nyn "Nkole-Sprache"
* #nyn ^designation[0].language = #de-AT 
* #nyn ^designation[0].value = "Nkole-Sprache" 
* #nyo "Nyoro-Sprache"
* #nyo ^designation[0].language = #de-AT 
* #nyo ^designation[0].value = "Nyoro-Sprache" 
* #nzi "Nzima-Sprache"
* #nzi ^designation[0].language = #de-AT 
* #nzi ^designation[0].value = "Nzima-Sprache" 
* #oc "Okzitanisch"
* #oc ^designation[0].language = #de-AT 
* #oc ^designation[0].value = "Okzitanisch" 
* #oj "Ojibwa-Sprache"
* #oj ^designation[0].language = #de-AT 
* #oj ^designation[0].value = "Ojibwa-Sprache" 
* #om "Galla-Sprache"
* #om ^designation[0].language = #de-AT 
* #om ^designation[0].value = "Galla-Sprache" 
* #or "Oriya-Sprache"
* #or ^designation[0].language = #de-AT 
* #or ^designation[0].value = "Oriya-Sprache" 
* #os "Ossetisch"
* #os ^designation[0].language = #de-AT 
* #os ^designation[0].value = "Ossetisch" 
* #osa "Osage-Sprache"
* #osa ^designation[0].language = #de-AT 
* #osa ^designation[0].value = "Osage-Sprache" 
* #ota "Osmanisch"
* #ota ^designation[0].language = #de-AT 
* #ota ^designation[0].value = "Osmanisch" 
* #oto "Otomangue-Sprachen"
* #oto ^designation[0].language = #de-AT 
* #oto ^designation[0].value = "Otomangue-Sprachen" 
* #pa "Pandschabi-Sprache"
* #pa ^designation[0].language = #de-AT 
* #pa ^designation[0].value = "Pandschabi-Sprache" 
* #paa "Papuasprachen (Andere)"
* #paa ^designation[0].language = #de-AT 
* #paa ^designation[0].value = "Papuasprachen (Andere)" 
* #pag "Pangasinan-Sprache"
* #pag ^designation[0].language = #de-AT 
* #pag ^designation[0].value = "Pangasinan-Sprache" 
* #pal "Mittelpersisch"
* #pal ^designation[0].language = #de-AT 
* #pal ^designation[0].value = "Mittelpersisch" 
* #pam "Pampanggan-Sprache"
* #pam ^designation[0].language = #de-AT 
* #pam ^designation[0].value = "Pampanggan-Sprache" 
* #pap "Papiamento"
* #pap ^designation[0].language = #de-AT 
* #pap ^designation[0].value = "Papiamento" 
* #pau "Palau-Sprache"
* #pau ^designation[0].language = #de-AT 
* #pau ^designation[0].value = "Palau-Sprache" 
* #peo "Altpersisch"
* #peo ^designation[0].language = #de-AT 
* #peo ^designation[0].value = "Altpersisch" 
* #phi "Philippinisch-Austronesisch (Andere)"
* #phi ^designation[0].language = #de-AT 
* #phi ^designation[0].value = "Philippinisch-Austronesisch (Andere)" 
* #phn "Phönikisch"
* #phn ^designation[0].language = #de-AT 
* #phn ^designation[0].value = "Phönikisch" 
* #pi "Pali"
* #pi ^designation[0].language = #de-AT 
* #pi ^designation[0].value = "Pali" 
* #pl "Polnisch"
* #pl ^designation[0].language = #de-AT 
* #pl ^designation[0].value = "Polnisch" 
* #pon "Ponapeanisch"
* #pon ^designation[0].language = #de-AT 
* #pon ^designation[0].value = "Ponapeanisch" 
* #pra "Prakrit"
* #pra ^designation[0].language = #de-AT 
* #pra ^designation[0].value = "Prakrit" 
* #pro "Altokzitanisch"
* #pro ^designation[0].language = #de-AT 
* #pro ^designation[0].value = "Altokzitanisch" 
* #ps "Paschtu"
* #ps ^designation[0].language = #de-AT 
* #ps ^designation[0].value = "Paschtu" 
* #pt "Portugiesisch"
* #pt ^designation[0].language = #de-AT 
* #pt ^designation[0].value = "Portugiesisch" 
* #pt-BR "Portugiesisch (Brasilien)"
* #pt-BR ^definition = HL7 Tags for the Identification of Languages
* #qaa "Reserviert für lokale Verwendung"
* #qaa ^designation[0].language = #de-AT 
* #qaa ^designation[0].value = "Reserviert für lokale Verwendung" 
* #qu "Quechua-Sprache"
* #qu ^designation[0].language = #de-AT 
* #qu ^designation[0].value = "Quechua-Sprache" 
* #raj "Rajasthani"
* #raj ^designation[0].language = #de-AT 
* #raj ^designation[0].value = "Rajasthani" 
* #rap "Osterinsel-Sprache"
* #rap ^designation[0].language = #de-AT 
* #rap ^designation[0].value = "Osterinsel-Sprache" 
* #rar "Rarotonganisch"
* #rar ^designation[0].language = #de-AT 
* #rar ^designation[0].value = "Rarotonganisch" 
* #rm "Rätoromanisch"
* #rm ^designation[0].language = #de-AT 
* #rm ^designation[0].value = "Rätoromanisch" 
* #rn "Rundi-Sprache"
* #rn ^designation[0].language = #de-AT 
* #rn ^designation[0].value = "Rundi-Sprache" 
* #ro "Rumänisch"
* #ro ^designation[0].language = #de-AT 
* #ro ^designation[0].value = "Rumänisch" 
* #roa "Romanische Sprachen (Andere)"
* #roa ^designation[0].language = #de-AT 
* #roa ^designation[0].value = "Romanische Sprachen (Andere)" 
* #rom "Romani (Sprache)"
* #rom ^designation[0].language = #de-AT 
* #rom ^designation[0].value = "Romani (Sprache)" 
* #ru "Russisch"
* #ru ^designation[0].language = #de-AT 
* #ru ^designation[0].value = "Russisch" 
* #ru-RU "Russisch (Russland)"
* #ru-RU ^definition = HL7 Tags for the Identification of Languages
* #rup "Aromunisch"
* #rup ^designation[0].language = #de-AT 
* #rup ^designation[0].value = "Aromunisch" 
* #rw "Rwanda-Sprache"
* #rw ^designation[0].language = #de-AT 
* #rw ^designation[0].value = "Rwanda-Sprache" 
* #sa "Sanskrit"
* #sa ^designation[0].language = #de-AT 
* #sa ^designation[0].value = "Sanskrit" 
* #sad "Sandawe-Sprache"
* #sad ^designation[0].language = #de-AT 
* #sad ^designation[0].value = "Sandawe-Sprache" 
* #sah "Jakutisch"
* #sah ^designation[0].language = #de-AT 
* #sah ^designation[0].value = "Jakutisch" 
* #sai "Indianersprachen, Südamerika (Andere)"
* #sai ^designation[0].language = #de-AT 
* #sai ^designation[0].value = "Indianersprachen, Südamerika (Andere)" 
* #sal "Salish-Sprache"
* #sal ^designation[0].language = #de-AT 
* #sal ^designation[0].value = "Salish-Sprache" 
* #sam "Samaritanisch"
* #sam ^designation[0].language = #de-AT 
* #sam ^designation[0].value = "Samaritanisch" 
* #sas "Sasak"
* #sas ^designation[0].language = #de-AT 
* #sas ^designation[0].value = "Sasak" 
* #sat "Santali"
* #sat ^designation[0].language = #de-AT 
* #sat ^designation[0].value = "Santali" 
* #sc "Sardisch"
* #sc ^designation[0].language = #de-AT 
* #sc ^designation[0].value = "Sardisch" 
* #scn "Sizilianisch"
* #scn ^designation[0].language = #de-AT 
* #scn ^designation[0].value = "Sizilianisch" 
* #sco "Schottisch"
* #sco ^designation[0].language = #de-AT 
* #sco ^designation[0].value = "Schottisch" 
* #sd "Sindhi-Sprache"
* #sd ^designation[0].language = #de-AT 
* #sd ^designation[0].value = "Sindhi-Sprache" 
* #se "Nordsaamisch"
* #se ^designation[0].language = #de-AT 
* #se ^designation[0].value = "Nordsaamisch" 
* #sel "Selkupisch"
* #sel ^designation[0].language = #de-AT 
* #sel ^designation[0].value = "Selkupisch" 
* #sem "Semitische Sprachen (Andere)"
* #sem ^designation[0].language = #de-AT 
* #sem ^designation[0].value = "Semitische Sprachen (Andere)" 
* #sg "Sango-Sprache"
* #sg ^designation[0].language = #de-AT 
* #sg ^designation[0].value = "Sango-Sprache" 
* #sga "Altirisch"
* #sga ^designation[0].language = #de-AT 
* #sga ^designation[0].value = "Altirisch" 
* #sgn "Zeichensprachen"
* #sgn ^designation[0].language = #de-AT 
* #sgn ^designation[0].value = "Zeichensprachen" 
* #shn "Schan-Sprache"
* #shn ^designation[0].language = #de-AT 
* #shn ^designation[0].value = "Schan-Sprache" 
* #si "Singhalesisch"
* #si ^designation[0].language = #de-AT 
* #si ^designation[0].value = "Singhalesisch" 
* #sid "Sidamo-Sprache"
* #sid ^designation[0].language = #de-AT 
* #sid ^designation[0].value = "Sidamo-Sprache" 
* #sio "Sioux-Sprachen (Andere)"
* #sio ^designation[0].language = #de-AT 
* #sio ^designation[0].value = "Sioux-Sprachen (Andere)" 
* #sit "Sinotibetische Sprachen (Andere)"
* #sit ^designation[0].language = #de-AT 
* #sit ^designation[0].value = "Sinotibetische Sprachen (Andere)" 
* #sk "Slowakisch"
* #sk ^designation[0].language = #de-AT 
* #sk ^designation[0].value = "Slowakisch" 
* #sl "Slowenisch"
* #sl ^designation[0].language = #de-AT 
* #sl ^designation[0].value = "Slowenisch" 
* #sla "Slawische Sprachen (Andere)"
* #sla ^designation[0].language = #de-AT 
* #sla ^designation[0].value = "Slawische Sprachen (Andere)" 
* #sm "Samoanisch"
* #sm ^designation[0].language = #de-AT 
* #sm ^designation[0].value = "Samoanisch" 
* #sma "Südsaamisch"
* #sma ^designation[0].language = #de-AT 
* #sma ^designation[0].value = "Südsaamisch" 
* #smi "Saamisch"
* #smi ^designation[0].language = #de-AT 
* #smi ^designation[0].value = "Saamisch" 
* #smj "Lulesaamisch"
* #smj ^designation[0].language = #de-AT 
* #smj ^designation[0].value = "Lulesaamisch" 
* #smn "Inarisaamisch"
* #smn ^designation[0].language = #de-AT 
* #smn ^designation[0].value = "Inarisaamisch" 
* #sms "Skoltsaamisch"
* #sms ^designation[0].language = #de-AT 
* #sms ^designation[0].value = "Skoltsaamisch" 
* #sn "Schona-Sprache"
* #sn ^designation[0].language = #de-AT 
* #sn ^designation[0].value = "Schona-Sprache" 
* #snk "Soninke-Sprache"
* #snk ^designation[0].language = #de-AT 
* #snk ^designation[0].value = "Soninke-Sprache" 
* #so "Somali"
* #so ^designation[0].language = #de-AT 
* #so ^designation[0].value = "Somali" 
* #sog "Sogdisch"
* #sog ^designation[0].language = #de-AT 
* #sog ^designation[0].value = "Sogdisch" 
* #son "Songhai-Sprache"
* #son ^designation[0].language = #de-AT 
* #son ^designation[0].value = "Songhai-Sprache" 
* #sq "Albanisch"
* #sq ^designation[0].language = #de-AT 
* #sq ^designation[0].value = "Albanisch" 
* #sr "Serbisch"
* #sr ^designation[0].language = #de-AT 
* #sr ^designation[0].value = "Serbisch" 
* #sr-RS "Serbisch (Serbien)"
* #sr-RS ^definition = HL7 Tags for the Identification of Languages
* #srn "Sranantongo"
* #srn ^designation[0].language = #de-AT 
* #srn ^designation[0].value = "Sranantongo" 
* #srr "Serer-Sprache"
* #srr ^designation[0].language = #de-AT 
* #srr ^designation[0].value = "Serer-Sprache" 
* #ss "Swasi-Sprache"
* #ss ^designation[0].language = #de-AT 
* #ss ^designation[0].value = "Swasi-Sprache" 
* #ssa "Nilosaharanische Sprachen (Andere)"
* #ssa ^designation[0].language = #de-AT 
* #ssa ^designation[0].value = "Nilosaharanische Sprachen (Andere)" 
* #st "Süd-Sotho-Sprache"
* #st ^designation[0].language = #de-AT 
* #st ^designation[0].value = "Süd-Sotho-Sprache" 
* #su "Sundanesisch"
* #su ^designation[0].language = #de-AT 
* #su ^designation[0].value = "Sundanesisch" 
* #suk "Sukuma-Sprache"
* #suk ^designation[0].language = #de-AT 
* #suk ^designation[0].value = "Sukuma-Sprache" 
* #sus "Susu"
* #sus ^designation[0].language = #de-AT 
* #sus ^designation[0].value = "Susu" 
* #sux "Sumerisch"
* #sux ^designation[0].language = #de-AT 
* #sux ^designation[0].value = "Sumerisch" 
* #sv "Schwedisch"
* #sv ^designation[0].language = #de-AT 
* #sv ^designation[0].value = "Schwedisch" 
* #sv-SE "Schwedisch (Schweden)"
* #sv-SE ^definition = HL7 Tags for the Identification of Languages
* #sw "Swahili"
* #sw ^designation[0].language = #de-AT 
* #sw ^designation[0].value = "Swahili" 
* #syc "Syrisch"
* #syc ^designation[0].language = #de-AT 
* #syc ^designation[0].value = "Syrisch" 
* #syr "Neuostaramäisch"
* #syr ^designation[0].language = #de-AT 
* #syr ^designation[0].value = "Neuostaramäisch" 
* #ta "Tamil"
* #ta ^designation[0].language = #de-AT 
* #ta ^designation[0].value = "Tamil" 
* #tai "Thaisprachen (Andere)"
* #tai ^designation[0].language = #de-AT 
* #tai ^designation[0].value = "Thaisprachen (Andere)" 
* #te "Telugu-Sprache"
* #te ^designation[0].language = #de-AT 
* #te ^designation[0].value = "Telugu-Sprache" 
* #tem "Temne-Sprache"
* #tem ^designation[0].language = #de-AT 
* #tem ^designation[0].value = "Temne-Sprache" 
* #ter "Tereno-Sprache"
* #ter ^designation[0].language = #de-AT 
* #ter ^designation[0].value = "Tereno-Sprache" 
* #tet "Tetum-Sprache"
* #tet ^designation[0].language = #de-AT 
* #tet ^designation[0].value = "Tetum-Sprache" 
* #tg "Tadschikisch"
* #tg ^designation[0].language = #de-AT 
* #tg ^designation[0].value = "Tadschikisch" 
* #th "Thailändisch"
* #th ^designation[0].language = #de-AT 
* #th ^designation[0].value = "Thailändisch" 
* #ti "Tigrinja-Sprache"
* #ti ^designation[0].language = #de-AT 
* #ti ^designation[0].value = "Tigrinja-Sprache" 
* #tig "Tigre-Sprache"
* #tig ^designation[0].language = #de-AT 
* #tig ^designation[0].value = "Tigre-Sprache" 
* #tiv "Tiv-Sprache"
* #tiv ^designation[0].language = #de-AT 
* #tiv ^designation[0].value = "Tiv-Sprache" 
* #tk "Turkmenisch"
* #tk ^designation[0].language = #de-AT 
* #tk ^designation[0].value = "Turkmenisch" 
* #tkl "Tokelauanisch"
* #tkl ^designation[0].language = #de-AT 
* #tkl ^designation[0].value = "Tokelauanisch" 
* #tl "Tagalog"
* #tl ^designation[0].language = #de-AT 
* #tl ^designation[0].value = "Tagalog" 
* #tlh "Klingonisch"
* #tlh ^designation[0].language = #de-AT 
* #tlh ^designation[0].value = "Klingonisch" 
* #tli "Tlingit-Sprache"
* #tli ^designation[0].language = #de-AT 
* #tli ^designation[0].value = "Tlingit-Sprache" 
* #tmh "Tamaseq"
* #tmh ^designation[0].language = #de-AT 
* #tmh ^designation[0].value = "Tamaseq" 
* #tn "Tswana-Sprache"
* #tn ^designation[0].language = #de-AT 
* #tn ^designation[0].value = "Tswana-Sprache" 
* #to "Tongaisch"
* #to ^designation[0].language = #de-AT 
* #to ^designation[0].value = "Tongaisch" 
* #tog "Tonga (Bantusprache, Sambia)"
* #tog ^designation[0].language = #de-AT 
* #tog ^designation[0].value = "Tonga (Bantusprache, Sambia)" 
* #tpi "Neumelanesisch"
* #tpi ^designation[0].language = #de-AT 
* #tpi ^designation[0].value = "Neumelanesisch" 
* #tr "Türkisch"
* #tr ^designation[0].language = #de-AT 
* #tr ^designation[0].value = "Türkisch" 
* #ts "Tsonga-Sprache"
* #ts ^designation[0].language = #de-AT 
* #ts ^designation[0].value = "Tsonga-Sprache" 
* #tsi "Tsimshian-Sprache"
* #tsi ^designation[0].language = #de-AT 
* #tsi ^designation[0].value = "Tsimshian-Sprache" 
* #tt "Tatarisch"
* #tt ^designation[0].language = #de-AT 
* #tt ^designation[0].value = "Tatarisch" 
* #tum "Tumbuka-Sprache"
* #tum ^designation[0].language = #de-AT 
* #tum ^designation[0].value = "Tumbuka-Sprache" 
* #tup "Tupi-Sprache"
* #tup ^designation[0].language = #de-AT 
* #tup ^designation[0].value = "Tupi-Sprache" 
* #tut "Altaische Sprachen (Andere)"
* #tut ^designation[0].language = #de-AT 
* #tut ^designation[0].value = "Altaische Sprachen (Andere)" 
* #tvl "Elliceanisch"
* #tvl ^designation[0].language = #de-AT 
* #tvl ^designation[0].value = "Elliceanisch" 
* #tw "Twi-Sprache"
* #tw ^designation[0].language = #de-AT 
* #tw ^designation[0].value = "Twi-Sprache" 
* #ty "Tahitisch"
* #ty ^designation[0].language = #de-AT 
* #ty ^designation[0].value = "Tahitisch" 
* #tyv "Tuwinisch"
* #tyv ^designation[0].language = #de-AT 
* #tyv ^designation[0].value = "Tuwinisch" 
* #udm "Udmurtisch"
* #udm ^designation[0].language = #de-AT 
* #udm ^designation[0].value = "Udmurtisch" 
* #ug "Uigurisch"
* #ug ^designation[0].language = #de-AT 
* #ug ^designation[0].value = "Uigurisch" 
* #uga "Ugaritisch"
* #uga ^designation[0].language = #de-AT 
* #uga ^designation[0].value = "Ugaritisch" 
* #uk "Ukrainisch"
* #uk ^designation[0].language = #de-AT 
* #uk ^designation[0].value = "Ukrainisch" 
* #umb "Mbundu-Sprache"
* #umb ^designation[0].language = #de-AT 
* #umb ^designation[0].value = "Mbundu-Sprache" 
* #und "Nicht zu entscheiden"
* #und ^designation[0].language = #de-AT 
* #und ^designation[0].value = "Nicht zu entscheiden" 
* #ur "Urdu"
* #ur ^designation[0].language = #de-AT 
* #ur ^designation[0].value = "Urdu" 
* #uz "Usbekisch"
* #uz ^designation[0].language = #de-AT 
* #uz ^designation[0].value = "Usbekisch" 
* #vai "Vai-Sprache"
* #vai ^designation[0].language = #de-AT 
* #vai ^designation[0].value = "Vai-Sprache" 
* #ve "Venda-Sprache"
* #ve ^designation[0].language = #de-AT 
* #ve ^designation[0].value = "Venda-Sprache" 
* #vi "Vietnamesisch"
* #vi ^designation[0].language = #de-AT 
* #vi ^designation[0].value = "Vietnamesisch" 
* #vo "Volapük"
* #vo ^designation[0].language = #de-AT 
* #vo ^designation[0].value = "Volapük" 
* #vot "Wotisch"
* #vot ^designation[0].language = #de-AT 
* #vot ^designation[0].value = "Wotisch" 
* #wa "Wallonisch"
* #wa ^designation[0].language = #de-AT 
* #wa ^designation[0].value = "Wallonisch" 
* #wak "Wakash-Sprachen"
* #wak ^designation[0].language = #de-AT 
* #wak ^designation[0].value = "Wakash-Sprachen" 
* #wal "Walamo-Sprache"
* #wal ^designation[0].language = #de-AT 
* #wal ^designation[0].value = "Walamo-Sprache" 
* #war "Waray"
* #war ^designation[0].language = #de-AT 
* #war ^designation[0].value = "Waray" 
* #was "Washo-Sprache"
* #was ^designation[0].language = #de-AT 
* #was ^designation[0].value = "Washo-Sprache" 
* #wen "Sorbisch (Andere)"
* #wen ^designation[0].language = #de-AT 
* #wen ^designation[0].value = "Sorbisch (Andere)" 
* #wo "Wolof-Sprache"
* #wo ^designation[0].language = #de-AT 
* #wo ^designation[0].value = "Wolof-Sprache" 
* #xal "Kalmückisch"
* #xal ^designation[0].language = #de-AT 
* #xal ^designation[0].value = "Kalmückisch" 
* #xh "Xhosa-Sprache"
* #xh ^designation[0].language = #de-AT 
* #xh ^designation[0].value = "Xhosa-Sprache" 
* #yao "Yao-Sprache (Bantusprache)"
* #yao ^designation[0].language = #de-AT 
* #yao ^designation[0].value = "Yao-Sprache (Bantusprache)" 
* #yap "Yapesisch"
* #yap ^designation[0].language = #de-AT 
* #yap ^designation[0].value = "Yapesisch" 
* #yi "Jiddisch"
* #yi ^designation[0].language = #de-AT 
* #yi ^designation[0].value = "Jiddisch" 
* #yo "Yoruba-Sprache"
* #yo ^designation[0].language = #de-AT 
* #yo ^designation[0].value = "Yoruba-Sprache" 
* #ypk "Ypik-Sprachen"
* #ypk ^designation[0].language = #de-AT 
* #ypk ^designation[0].value = "Ypik-Sprachen" 
* #za "Zhuang"
* #za ^designation[0].language = #de-AT 
* #za ^designation[0].value = "Zhuang" 
* #zap "Zapotekisch"
* #zap ^designation[0].language = #de-AT 
* #zap ^designation[0].value = "Zapotekisch" 
* #zbl "Bliss-Symbol"
* #zbl ^designation[0].language = #de-AT 
* #zbl ^designation[0].value = "Bliss-Symbol" 
* #zen "Zenaga"
* #zen ^designation[0].language = #de-AT 
* #zen ^designation[0].value = "Zenaga" 
* #zgh "Tamazight"
* #zgh ^designation[0].language = #de-AT 
* #zgh ^designation[0].value = "Tamazight" 
* #zh "Chinesisch"
* #zh ^designation[0].language = #de-AT 
* #zh ^designation[0].value = "Chinesisch" 
* #zh-CN "Chinesisch (China)"
* #zh-CN ^definition = HL7 Tags for the Identification of Languages
* #zh-HK "Chinesisch (Hong Kong)"
* #zh-HK ^definition = HL7 Tags for the Identification of Languages
* #zh-SG "Chinesisch (Singapur)"
* #zh-SG ^definition = HL7 Tags for the Identification of Languages
* #zh-TW "Chinesisch (Taiwan)"
* #zh-TW ^definition = HL7 Tags for the Identification of Languages
* #znd "Zande-Sprachen"
* #znd ^designation[0].language = #de-AT 
* #znd ^designation[0].value = "Zande-Sprachen" 
* #zu "Zulu-Sprache"
* #zu ^designation[0].language = #de-AT 
* #zu ^designation[0].value = "Zulu-Sprache" 
* #zun "Zuñi-Sprache"
* #zun ^designation[0].language = #de-AT 
* #zun ^designation[0].value = "Zuñi-Sprache" 
* #zxx "Kein linguistischer Inhalt"
* #zxx ^designation[0].language = #de-AT 
* #zxx ^designation[0].value = "Kein linguistischer Inhalt" 
* #zza "Zazaki"
* #zza ^designation[0].language = #de-AT 
* #zza ^designation[0].value = "Zazaki" 
