Instance: ems-artquartier 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-artquartier" 
* name = "ems-artquartier" 
* title = "EMS_ArtQuartier" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** EMS Liste der Quartierart" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.63" 
* date = "2017-01-26" 
* count = 4 
* #CAMPING "Campingplatz"
* #HOTEL "Hotel"
* #PRIV "privat"
* #SHIP "Schiff"
