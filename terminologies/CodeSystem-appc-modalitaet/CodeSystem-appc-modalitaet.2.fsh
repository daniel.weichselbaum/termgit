Instance: appc-modalitaet 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-appc-modalitaet" 
* name = "appc-modalitaet" 
* title = "APPC_Modalitaet" 
* status = #active 
* content = #complete 
* version = "202008" 
* description = "**Description:** Code List for all APPC-Codes for 1st Axis: Modality

**Beschreibung:** Code Liste aller Codes der 1. APPC-Achse: Modalität" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.38.1" 
* date = "2020-08-18" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.bura.at/appc/" 
* count = 16 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #0 "Modalitaet unbestimmt"
* #0 ^designation[0].language = #de-AT 
* #0 ^designation[0].value = "Modalitaet unbestimmt" 
* #1 "Röntgen"
* #1 ^designation[0].language = #de-AT 
* #1 ^designation[0].value = "Röntgenaufnahme" 
* #10 "Fotografische Dokumentation"
* #10 ^designation[0].language = #de-AT 
* #10 ^designation[0].value = "Fotografische Dokumentation" 
* #11 "Video Dokumentation"
* #11 ^designation[0].language = #de-AT 
* #11 ^designation[0].value = "Video Dokumentation" 
* #2 "CT"
* #2 ^designation[0].language = #de-AT 
* #2 ^designation[0].value = "Computer-Tomographie" 
* #3 "MRT"
* #3 ^designation[0].language = #de-AT 
* #3 ^designation[0].value = "Magnetresonanz-Tomographie" 
* #4 "US"
* #4 ^designation[0].language = #de-AT 
* #4 ^designation[0].value = "Ultraschalluntersuchung (Sonographie)" 
* #5 "NUK"
* #5 ^designation[0].language = #de-AT 
* #5 ^designation[0].value = "Nuklearmedizinische Untersuchung" 
* #6 "PET"
* #6 ^designation[0].language = #de-AT 
* #6 ^designation[0].value = "Positronen-Emissions-Tomographie" 
* #7 "Endoskopie"
* #7 ^designation[0].language = #de-AT 
* #7 ^designation[0].value = "Endoskopie" 
* #7 ^property[0].code = #child 
* #7 ^property[0].valueCode = #7-1 
* #7 ^property[1].code = #child 
* #7 ^property[1].valueCode = #7-2 
* #7-1 "Kapselendoskopie"
* #7-1 ^designation[0].language = #de-AT 
* #7-1 ^designation[0].value = "Kapselendoskopie" 
* #7-1 ^property[0].code = #parent 
* #7-1 ^property[0].valueCode = #7 
* #7-2 "Konfokale Laserendomikroskopie"
* #7-2 ^designation[0].language = #de-AT 
* #7-2 ^designation[0].value = "Konfokale Laserendomikroskopie" 
* #7-2 ^property[0].code = #parent 
* #7-2 ^property[0].valueCode = #7 
* #8 "Mikroskopische Untersuchung"
* #8 ^designation[0].language = #de-AT 
* #8 ^designation[0].value = "Mikroskopische Untersuchung" 
* #9 "Optische Kohärenztomografie"
* #9 ^designation[0].language = #de-AT 
* #9 ^designation[0].value = "Optische Kohärenztomografie" 
* #999 "Keine Modalität"
* #999 ^designation[0].language = #de-AT 
* #999 ^designation[0].value = "Keine Modalität" 
* #999 ^property[0].code = #child 
* #999 ^property[0].valueCode = #999-1 
* #999-1 "Keine Modalität - Studie entfernt"
* #999-1 ^designation[0].language = #de-AT 
* #999-1 ^designation[0].value = "Keine Modalität - Studie entfernt" 
* #999-1 ^property[0].code = #parent 
* #999-1 ^property[0].valueCode = #999 
