Instance: appc-modalitaet 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-appc-modalitaet" 
* name = "appc-modalitaet" 
* title = "APPC_Modalitaet" 
* status = #active 
* content = #complete 
* version = "202008" 
* description = "**Description:** Code List for all APPC-Codes for 1st Axis: Modality

**Beschreibung:** Code Liste aller Codes der 1. APPC-Achse: Modalität" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.38.1" 
* date = "2020-08-18" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.bura.at/appc/" 
* count = 16 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* concept[0].code = #0
* concept[0].display = "Modalitaet unbestimmt"
* concept[0].designation[0].language = #de-AT 
* concept[0].designation[0].value = "Modalitaet unbestimmt" 
* concept[1].code = #1
* concept[1].display = "Röntgen"
* concept[1].designation[0].language = #de-AT 
* concept[1].designation[0].value = "Röntgenaufnahme" 
* concept[2].code = #10
* concept[2].display = "Fotografische Dokumentation"
* concept[2].designation[0].language = #de-AT 
* concept[2].designation[0].value = "Fotografische Dokumentation" 
* concept[3].code = #11
* concept[3].display = "Video Dokumentation"
* concept[3].designation[0].language = #de-AT 
* concept[3].designation[0].value = "Video Dokumentation" 
* concept[4].code = #2
* concept[4].display = "CT"
* concept[4].designation[0].language = #de-AT 
* concept[4].designation[0].value = "Computer-Tomographie" 
* concept[5].code = #3
* concept[5].display = "MRT"
* concept[5].designation[0].language = #de-AT 
* concept[5].designation[0].value = "Magnetresonanz-Tomographie" 
* concept[6].code = #4
* concept[6].display = "US"
* concept[6].designation[0].language = #de-AT 
* concept[6].designation[0].value = "Ultraschalluntersuchung (Sonographie)" 
* concept[7].code = #5
* concept[7].display = "NUK"
* concept[7].designation[0].language = #de-AT 
* concept[7].designation[0].value = "Nuklearmedizinische Untersuchung" 
* concept[8].code = #6
* concept[8].display = "PET"
* concept[8].designation[0].language = #de-AT 
* concept[8].designation[0].value = "Positronen-Emissions-Tomographie" 
* concept[9].code = #7
* concept[9].display = "Endoskopie"
* concept[9].designation[0].language = #de-AT 
* concept[9].designation[0].value = "Endoskopie" 
* concept[9].property[0].code = #child 
* concept[9].property[0].valueCode = #7-1 
* concept[9].property[1].code = #child 
* concept[9].property[1].valueCode = #7-2 
* concept[10].code = #7-1
* concept[10].display = "Kapselendoskopie"
* concept[10].designation[0].language = #de-AT 
* concept[10].designation[0].value = "Kapselendoskopie" 
* concept[10].property[0].code = #parent 
* concept[10].property[0].valueCode = #7 
* concept[11].code = #7-2
* concept[11].display = "Konfokale Laserendomikroskopie"
* concept[11].designation[0].language = #de-AT 
* concept[11].designation[0].value = "Konfokale Laserendomikroskopie" 
* concept[11].property[0].code = #parent 
* concept[11].property[0].valueCode = #7 
* concept[12].code = #8
* concept[12].display = "Mikroskopische Untersuchung"
* concept[12].designation[0].language = #de-AT 
* concept[12].designation[0].value = "Mikroskopische Untersuchung" 
* concept[13].code = #9
* concept[13].display = "Optische Kohärenztomografie"
* concept[13].designation[0].language = #de-AT 
* concept[13].designation[0].value = "Optische Kohärenztomografie" 
* concept[14].code = #999
* concept[14].display = "Keine Modalität"
* concept[14].designation[0].language = #de-AT 
* concept[14].designation[0].value = "Keine Modalität" 
* concept[14].property[0].code = #child 
* concept[14].property[0].valueCode = #999-1 
* concept[15].code = #999-1
* concept[15].display = "Keine Modalität - Studie entfernt"
* concept[15].designation[0].language = #de-AT 
* concept[15].designation[0].value = "Keine Modalität - Studie entfernt" 
* concept[15].property[0].code = #parent 
* concept[15].property[0].valueCode = #999 
