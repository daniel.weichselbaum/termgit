Instance: ems-verotoxin-1-subtyp 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-verotoxin-1-subtyp" 
* name = "ems-verotoxin-1-subtyp" 
* title = "EMS_Verotoxin_1_Subtyp" 
* status = #active 
* content = #complete 
* version = "1.0.0" 
* description = "**Beschreibung:** Verotoxin 1 Subtyp" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.98" 
* date = "2017-01-26" 
* count = 4 
* #NOTEST "nicht durchgeführt"
* #VT1A "VT1a"
* #VT1C "VT1c"
* #VT1D "VT1d"
