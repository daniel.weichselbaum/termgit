Instance: elga-allergystatuscode 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-allergystatuscode" 
* name = "elga-allergystatuscode" 
* title = "ELGA_AllergyStatusCode" 
* status = #active 
* version = "202002" 
* description = "**Description:** Clinical Status of Allergies Or Intolerances. Contains information protected by copyright of SNOMED International. Any use of SNOMED CT in Austria requires a valid affiliate license or sublicense. The corresponding license is free of charge, provided that the use only takes place in Austria and fulfills the conditions of the Affiliate License Agreement. Affiliate licenses can be requested directly from the respective NRC via the [Member Licensing and Distribution Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS).

**Beschreibung:** Klinischer Status von Allergien und Intoleranzen. Enthält durch SNOMED International urheberrechtlich geschützte Information. Jede Verwendung von SNOMED CT in Österreich erfordert eine aufrechte Affiliate Lizenz oder eine Sublizenz. Die entsprechende Lizenz ist kostenlos, vorausgesetzt die Verwendung findet nur in Österreich statt und erfüllt die Bedingungen des Affiliate License Agreements. Affiliate Lizenzen können über das [Mitglieder-Lizenzierungs- und Distributions-Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS) direkt beim jeweiligen NRC beantragt werden." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.183" 
* date = "2020-02-04" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* compose.include[0].concept[0].code = "55561003"
* compose.include[0].concept[0].display = "bestehend"
* compose.include[0].concept[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[0].designation[0].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[0].designation[1].value = "The subject is currently experiencing the symptoms of the condition or there is evidence of the condition" 
* compose.include[0].concept[1].code = "73425007"
* compose.include[0].concept[1].display = "nicht mehr bestehend"
* compose.include[0].concept[1].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[1].designation[0].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[1].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[1].designation[1].value = "The subject is no longer experiencing the symptoms of the condition or there is no longer evidence of the condition" 
* compose.include[0].concept[2].code = "723506003"
* compose.include[0].concept[2].display = "abgeklungen"
* compose.include[0].concept[2].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[2].designation[0].value = "SNOMED Clinical Terms" 
* compose.include[0].concept[2].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[2].designation[1].value = "The subject is no longer experiencing the symptoms of the condition and there is a negligible perceived risk of the symptoms returning" 
