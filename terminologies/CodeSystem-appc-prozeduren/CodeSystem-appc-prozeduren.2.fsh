Instance: appc-prozeduren 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-appc-prozeduren" 
* name = "appc-prozeduren" 
* title = "APPC_Prozeduren" 
* status = #active 
* content = #complete 
* version = "201907" 
* description = "**Description:** Code List for all APPC-Codes for 3rd Axis: Procedures

**Beschreibung:** Code Liste aller Codes der 3. APPC-Achse: Prozeduren" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.38.3" 
* date = "2019-07-01" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.bura.at/appc/" 
* count = 109 
* property[0].code = #hints 
* property[0].type = #string 
* property[1].code = #child 
* property[1].type = #code 
* property[2].code = #parent 
* property[2].type = #code 
* #0 "Prozedur nicht näher bestimmt"
* #0 ^designation[0].language = #de-AT 
* #0 ^designation[0].value = "Prozedur nicht näher bestimmt" 
* #0 ^property[0].code = #hints 
* #0 ^property[0].valueString = "APPC: 1.2.40.0.34.5.38" 
* #0 ^property[1].code = #child 
* #0 ^property[1].valueCode = #0-1 
* #0 ^property[2].code = #child 
* #0 ^property[2].valueCode = #0-2 
* #0-1 "Tomographie"
* #0-1 ^designation[0].language = #de-AT 
* #0-1 ^designation[0].value = "Tomographie" 
* #0-1 ^property[0].code = #parent 
* #0-1 ^property[0].valueCode = #0 
* #0-1 ^property[1].code = #hints 
* #0-1 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #0-1 ^property[2].code = #child 
* #0-1 ^property[2].valueCode = #0-1-1 
* #0-1-1 "Tomosynthese"
* #0-1-1 ^designation[0].language = #de-AT 
* #0-1-1 ^designation[0].value = "Tomosynthese" 
* #0-1-1 ^property[0].code = #parent 
* #0-1-1 ^property[0].valueCode = #0-1 
* #0-1-1 ^property[1].code = #hints 
* #0-1-1 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #0-2 "Dual-energy X-ray absorptiometry (Dual-Energy)"
* #0-2 ^designation[0].language = #de-AT 
* #0-2 ^designation[0].value = "Dual-energy X-ray absorptiometry (Dual-Energy)" 
* #0-2 ^property[0].code = #parent 
* #0-2 ^property[0].valueCode = #0 
* #0-2 ^property[1].code = #hints 
* #0-2 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #1 "Füllung präformierter Gangsysteme"
* #1 ^designation[0].language = #de-AT 
* #1 ^designation[0].value = "Füllung präformierter Gangsysteme" 
* #1 ^property[0].code = #hints 
* #1 ^property[0].valueString = "APPC: 1.2.40.0.34.5.38" 
* #1 ^property[1].code = #child 
* #1 ^property[1].valueCode = #1-1 
* #1 ^property[2].code = #child 
* #1 ^property[2].valueCode = #1-2 
* #1 ^property[3].code = #child 
* #1 ^property[3].valueCode = #1-3 
* #1 ^property[4].code = #child 
* #1 ^property[4].valueCode = #1-4 
* #1 ^property[5].code = #child 
* #1 ^property[5].valueCode = #1-5 
* #1 ^property[6].code = #child 
* #1 ^property[6].valueCode = #1-6 
* #1 ^property[7].code = #child 
* #1 ^property[7].valueCode = #1-7 
* #1-1 "Enteroklysma"
* #1-1 ^designation[0].language = #de-AT 
* #1-1 ^designation[0].value = "Enteroklysma" 
* #1-1 ^property[0].code = #parent 
* #1-1 ^property[0].valueCode = #1 
* #1-1 ^property[1].code = #hints 
* #1-1 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #1-2 "Gefäßdarstellung"
* #1-2 ^designation[0].language = #de-AT 
* #1-2 ^designation[0].value = "Gefäßdarstellung" 
* #1-2 ^property[0].code = #parent 
* #1-2 ^property[0].valueCode = #1 
* #1-2 ^property[1].code = #hints 
* #1-2 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #1-2 ^property[2].code = #child 
* #1-2 ^property[2].valueCode = #1-2-1 
* #1-2 ^property[3].code = #child 
* #1-2 ^property[3].valueCode = #1-2-2 
* #1-2 ^property[4].code = #child 
* #1-2 ^property[4].valueCode = #1-2-3 
* #1-2-1 "Angiographie"
* #1-2-1 ^designation[0].language = #de-AT 
* #1-2-1 ^designation[0].value = "Angiographie" 
* #1-2-1 ^property[0].code = #parent 
* #1-2-1 ^property[0].valueCode = #1-2 
* #1-2-1 ^property[1].code = #hints 
* #1-2-1 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #1-2-1 ^property[2].code = #child 
* #1-2-1 ^property[2].valueCode = #1-2-1-1 
* #1-2-1 ^property[3].code = #child 
* #1-2-1 ^property[3].valueCode = #1-2-1-2 
* #1-2-1 ^property[4].code = #child 
* #1-2-1 ^property[4].valueCode = #1-2-1-3 
* #1-2-1-1 "Angiographie nativ (FD, MR Phasenkontrast etc.)"
* #1-2-1-1 ^designation[0].language = #de-AT 
* #1-2-1-1 ^designation[0].value = "Angiographie nativ (FD, MR Phasenkontrast etc.)" 
* #1-2-1-1 ^property[0].code = #parent 
* #1-2-1-1 ^property[0].valueCode = #1-2-1 
* #1-2-1-1 ^property[1].code = #hints 
* #1-2-1-1 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #1-2-1-2 "Angiographie KM (i.v. CT Angio, MR Angio, iv. DSA etc.)"
* #1-2-1-2 ^designation[0].language = #de-AT 
* #1-2-1-2 ^designation[0].value = "Angiographie KM (i.v. CT Angio, MR Angio, iv. DSA etc.)" 
* #1-2-1-2 ^property[0].code = #parent 
* #1-2-1-2 ^property[0].valueCode = #1-2-1 
* #1-2-1-2 ^property[1].code = #hints 
* #1-2-1-2 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #1-2-1-3 "Angiographie Katheter (invasiv, selektiv etc.)"
* #1-2-1-3 ^designation[0].language = #de-AT 
* #1-2-1-3 ^designation[0].value = "Angiographie Katheter (invasiv, selektiv etc.)" 
* #1-2-1-3 ^property[0].code = #parent 
* #1-2-1-3 ^property[0].valueCode = #1-2-1 
* #1-2-1-3 ^property[1].code = #hints 
* #1-2-1-3 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #1-2-2 "Phlebographie"
* #1-2-2 ^designation[0].language = #de-AT 
* #1-2-2 ^designation[0].value = "Phlebographie" 
* #1-2-2 ^property[0].code = #parent 
* #1-2-2 ^property[0].valueCode = #1-2 
* #1-2-2 ^property[1].code = #hints 
* #1-2-2 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #1-2-2 ^property[2].code = #child 
* #1-2-2 ^property[2].valueCode = #1-2-2-1 
* #1-2-2 ^property[3].code = #child 
* #1-2-2 ^property[3].valueCode = #1-2-2-2 
* #1-2-2 ^property[4].code = #child 
* #1-2-2 ^property[4].valueCode = #1-2-2-3 
* #1-2-2-1 "Phlebographie nativ"
* #1-2-2-1 ^designation[0].language = #de-AT 
* #1-2-2-1 ^designation[0].value = "Phlebographie nativ" 
* #1-2-2-1 ^property[0].code = #parent 
* #1-2-2-1 ^property[0].valueCode = #1-2-2 
* #1-2-2-1 ^property[1].code = #hints 
* #1-2-2-1 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #1-2-2-2 "Phlebographie KM"
* #1-2-2-2 ^designation[0].language = #de-AT 
* #1-2-2-2 ^designation[0].value = "Phlebographie KM" 
* #1-2-2-2 ^property[0].code = #parent 
* #1-2-2-2 ^property[0].valueCode = #1-2-2 
* #1-2-2-2 ^property[1].code = #hints 
* #1-2-2-2 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #1-2-2-3 "Phlebographie Katheter"
* #1-2-2-3 ^designation[0].language = #de-AT 
* #1-2-2-3 ^designation[0].value = "Phlebographie Katheter" 
* #1-2-2-3 ^property[0].code = #parent 
* #1-2-2-3 ^property[0].valueCode = #1-2-2 
* #1-2-2-3 ^property[1].code = #hints 
* #1-2-2-3 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #1-2-3 "Lymphographie"
* #1-2-3 ^designation[0].language = #de-AT 
* #1-2-3 ^designation[0].value = "Lymphographie" 
* #1-2-3 ^property[0].code = #parent 
* #1-2-3 ^property[0].valueCode = #1-2 
* #1-2-3 ^property[1].code = #hints 
* #1-2-3 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #1-2-3 ^property[2].code = #child 
* #1-2-3 ^property[2].valueCode = #1-2-3-1 
* #1-2-3 ^property[3].code = #child 
* #1-2-3 ^property[3].valueCode = #1-2-3-2 
* #1-2-3 ^property[4].code = #child 
* #1-2-3 ^property[4].valueCode = #1-2-3-3 
* #1-2-3-1 "Lymphographie nativ"
* #1-2-3-1 ^designation[0].language = #de-AT 
* #1-2-3-1 ^designation[0].value = "Lymphographie nativ" 
* #1-2-3-1 ^property[0].code = #parent 
* #1-2-3-1 ^property[0].valueCode = #1-2-3 
* #1-2-3-1 ^property[1].code = #hints 
* #1-2-3-1 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #1-2-3-2 "Lymphographie KM"
* #1-2-3-2 ^designation[0].language = #de-AT 
* #1-2-3-2 ^designation[0].value = "Lymphographie KM" 
* #1-2-3-2 ^property[0].code = #parent 
* #1-2-3-2 ^property[0].valueCode = #1-2-3 
* #1-2-3-2 ^property[1].code = #hints 
* #1-2-3-2 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #1-2-3-3 "Lymphographie Katheter"
* #1-2-3-3 ^designation[0].language = #de-AT 
* #1-2-3-3 ^designation[0].value = "Lymphographie Katheter" 
* #1-2-3-3 ^property[0].code = #parent 
* #1-2-3-3 ^property[0].valueCode = #1-2-3 
* #1-2-3-3 ^property[1].code = #hints 
* #1-2-3-3 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #1-3 "Fistulographie"
* #1-3 ^designation[0].language = #de-AT 
* #1-3 ^designation[0].value = "Fistulographie" 
* #1-3 ^property[0].code = #parent 
* #1-3 ^property[0].valueCode = #1 
* #1-3 ^property[1].code = #hints 
* #1-3 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #1-4 "Myelographie"
* #1-4 ^designation[0].language = #de-AT 
* #1-4 ^designation[0].value = "Myelographie" 
* #1-4 ^property[0].code = #parent 
* #1-4 ^property[0].valueCode = #1 
* #1-4 ^property[1].code = #hints 
* #1-4 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #1-5 "Arthrographie"
* #1-5 ^designation[0].language = #de-AT 
* #1-5 ^designation[0].value = "Arthrographie" 
* #1-5 ^property[0].code = #parent 
* #1-5 ^property[0].valueCode = #1 
* #1-5 ^property[1].code = #hints 
* #1-5 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #1-6 "retrograde KM Füllung"
* #1-6 ^designation[0].language = #de-AT 
* #1-6 ^designation[0].value = "retrograde KM Füllung (Pyelo, Uretro, Vesico, Urethro, HSG, ERCP, PTC...)" 
* #1-6 ^property[0].code = #parent 
* #1-6 ^property[0].valueCode = #1 
* #1-6 ^property[1].code = #hints 
* #1-6 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #1-7 "endocavitäre Applikation"
* #1-7 ^designation[0].language = #de-AT 
* #1-7 ^designation[0].value = "endocavitäre Applikation" 
* #1-7 ^property[0].code = #parent 
* #1-7 ^property[0].valueCode = #1 
* #1-7 ^property[1].code = #hints 
* #1-7 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #2 "Quantitative Analysen/Rekonstruktionen aus Datensätzen"
* #2 ^designation[0].language = #de-AT 
* #2 ^designation[0].value = "Quantitative Analysen/Rekonstruktionen aus Datensätzen" 
* #2 ^property[0].code = #hints 
* #2 ^property[0].valueString = "APPC: 1.2.40.0.34.5.38" 
* #2 ^property[1].code = #child 
* #2 ^property[1].valueCode = #2-1 
* #2 ^property[2].code = #child 
* #2 ^property[2].valueCode = #2-2 
* #2 ^property[3].code = #child 
* #2 ^property[3].valueCode = #2-3 
* #2 ^property[4].code = #child 
* #2 ^property[4].valueCode = #2-4 
* #2 ^property[5].code = #child 
* #2 ^property[5].valueCode = #2-5 
* #2 ^property[6].code = #child 
* #2 ^property[6].valueCode = #2-6 
* #2 ^property[7].code = #child 
* #2 ^property[7].valueCode = #2-7 
* #2 ^property[8].code = #child 
* #2 ^property[8].valueCode = #2-8 
* #2-1 "Osteo Densitometrie"
* #2-1 ^designation[0].language = #de-AT 
* #2-1 ^designation[0].value = "Osteo Densitometrie" 
* #2-1 ^property[0].code = #parent 
* #2-1 ^property[0].valueCode = #2 
* #2-1 ^property[1].code = #hints 
* #2-1 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #2-2 "Dental CT"
* #2-2 ^designation[0].language = #de-AT 
* #2-2 ^designation[0].value = "Dental CT" 
* #2-2 ^property[0].code = #parent 
* #2-2 ^property[0].valueCode = #2 
* #2-2 ^property[1].code = #hints 
* #2-2 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #2-3 "Berechnungen aus Datensätzen"
* #2-3 ^designation[0].language = #de-AT 
* #2-3 ^designation[0].value = "Berechnungen aus Datensätzen" 
* #2-3 ^property[0].code = #parent 
* #2-3 ^property[0].valueCode = #2 
* #2-3 ^property[1].code = #hints 
* #2-3 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #2-3 ^property[2].code = #child 
* #2-3 ^property[2].valueCode = #2-3-1 
* #2-3 ^property[3].code = #child 
* #2-3 ^property[3].valueCode = #2-3-2 
* #2-3 ^property[4].code = #child 
* #2-3 ^property[4].valueCode = #2-3-3 
* #2-3 ^property[5].code = #child 
* #2-3 ^property[5].valueCode = #2-3-4 
* #2-3 ^property[6].code = #child 
* #2-3 ^property[6].valueCode = #2-3-5 
* #2-3 ^property[7].code = #child 
* #2-3 ^property[7].valueCode = #2-3-6 
* #2-3 ^property[8].code = #child 
* #2-3 ^property[8].valueCode = #2-3-7 
* #2-3 ^property[9].code = #child 
* #2-3 ^property[9].valueCode = #2-3-8 
* #2-3-1 "Volumetrie"
* #2-3-1 ^designation[0].language = #de-AT 
* #2-3-1 ^designation[0].value = "Volumetrie" 
* #2-3-1 ^property[0].code = #parent 
* #2-3-1 ^property[0].valueCode = #2-3 
* #2-3-1 ^property[1].code = #hints 
* #2-3-1 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #2-3-2 "Fluss-/Durchfluss-Messung"
* #2-3-2 ^designation[0].language = #de-AT 
* #2-3-2 ^designation[0].value = "Flussmessung / Farbcodierung (FD) / KM-Anflutung / Intensitätsprofile / Perfusion" 
* #2-3-2 ^property[0].code = #parent 
* #2-3-2 ^property[0].valueCode = #2-3 
* #2-3-2 ^property[1].code = #hints 
* #2-3-2 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #2-3-3 "MPR"
* #2-3-3 ^designation[0].language = #de-AT 
* #2-3-3 ^designation[0].value = "MPR" 
* #2-3-3 ^property[0].code = #parent 
* #2-3-3 ^property[0].valueCode = #2-3 
* #2-3-3 ^property[1].code = #hints 
* #2-3-3 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #2-3-4 "3D / 4D"
* #2-3-4 ^designation[0].language = #de-AT 
* #2-3-4 ^designation[0].value = "3D / 4D" 
* #2-3-4 ^property[0].code = #parent 
* #2-3-4 ^property[0].valueCode = #2-3 
* #2-3-4 ^property[1].code = #hints 
* #2-3-4 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #2-3-5 "HR"
* #2-3-5 ^designation[0].language = #de-AT 
* #2-3-5 ^designation[0].value = "HR" 
* #2-3-5 ^property[0].code = #parent 
* #2-3-5 ^property[0].valueCode = #2-3 
* #2-3-5 ^property[1].code = #hints 
* #2-3-5 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #2-3-6 "errechnete Gefäß / Gang Darstellung"
* #2-3-6 ^designation[0].language = #de-AT 
* #2-3-6 ^designation[0].value = "errechnete Gefäß / Gang Darstellung" 
* #2-3-6 ^property[0].code = #parent 
* #2-3-6 ^property[0].valueCode = #2-3 
* #2-3-6 ^property[1].code = #hints 
* #2-3-6 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #2-3-7 "Virtuelle Endoskopie"
* #2-3-7 ^designation[0].language = #de-AT 
* #2-3-7 ^designation[0].value = "Virtuelle Endoskopie" 
* #2-3-7 ^property[0].code = #parent 
* #2-3-7 ^property[0].valueCode = #2-3 
* #2-3-7 ^property[1].code = #hints 
* #2-3-7 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #2-3-8 "Winkel- und Achsenmessungen"
* #2-3-8 ^designation[0].language = #de-AT 
* #2-3-8 ^designation[0].value = "Winkel- und Achsenmessungen" 
* #2-3-8 ^property[0].code = #parent 
* #2-3-8 ^property[0].valueCode = #2-3 
* #2-3-8 ^property[1].code = #hints 
* #2-3-8 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #2-4 "Spektroskopie"
* #2-4 ^designation[0].language = #de-AT 
* #2-4 ^designation[0].value = "Spektroskopie" 
* #2-4 ^property[0].code = #parent 
* #2-4 ^property[0].valueCode = #2 
* #2-4 ^property[1].code = #hints 
* #2-4 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #2-5 "Videoauswertung von Bewegungsstudien"
* #2-5 ^designation[0].language = #de-AT 
* #2-5 ^designation[0].value = "Videoauswertung von Bewegungsstudien" 
* #2-5 ^property[0].code = #parent 
* #2-5 ^property[0].valueCode = #2 
* #2-5 ^property[1].code = #hints 
* #2-5 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #2-6 "Bildfusion"
* #2-6 ^designation[0].language = #de-AT 
* #2-6 ^designation[0].value = "Bildfusion" 
* #2-6 ^property[0].code = #parent 
* #2-6 ^property[0].valueCode = #2 
* #2-6 ^property[1].code = #hints 
* #2-6 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #2-7 "Navigation"
* #2-7 ^designation[0].language = #de-AT 
* #2-7 ^designation[0].value = "Navigation" 
* #2-7 ^property[0].code = #parent 
* #2-7 ^property[0].valueCode = #2 
* #2-7 ^property[1].code = #hints 
* #2-7 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #2-8 "US-Kontrastmittel"
* #2-8 ^designation[0].language = #de-AT 
* #2-8 ^designation[0].value = "US-Kontrastmittel" 
* #2-8 ^property[0].code = #parent 
* #2-8 ^property[0].valueCode = #2 
* #2-8 ^property[1].code = #hints 
* #2-8 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3 "Dokumentation von Interventionen über künstlichen Zugang"
* #3 ^designation[0].language = #de-AT 
* #3 ^designation[0].value = "Dokumentation von Interventionen über künstlichen Zugang" 
* #3 ^property[0].code = #hints 
* #3 ^property[0].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3 ^property[1].code = #child 
* #3 ^property[1].valueCode = #3-1 
* #3 ^property[2].code = #child 
* #3 ^property[2].valueCode = #3-2 
* #3 ^property[3].code = #child 
* #3 ^property[3].valueCode = #3-3 
* #3 ^property[4].code = #child 
* #3 ^property[4].valueCode = #3-4 
* #3 ^property[5].code = #child 
* #3 ^property[5].valueCode = #3-5 
* #3 ^property[6].code = #child 
* #3 ^property[6].valueCode = #3-6 
* #3 ^property[7].code = #child 
* #3 ^property[7].valueCode = #3-7 
* #3 ^property[8].code = #child 
* #3 ^property[8].valueCode = #3-8 
* #3 ^property[9].code = #child 
* #3 ^property[9].valueCode = #3-9 
* #3-1 "Nadel Biopsie / Punktion"
* #3-1 ^designation[0].language = #de-AT 
* #3-1 ^designation[0].value = "Nadel Biopsie / Punktion" 
* #3-1 ^property[0].code = #parent 
* #3-1 ^property[0].valueCode = #3 
* #3-1 ^property[1].code = #hints 
* #3-1 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-2 "Vakuum Nadel Biopsie"
* #3-2 ^designation[0].language = #de-AT 
* #3-2 ^designation[0].value = "Vakuum Nadel Biopsie" 
* #3-2 ^property[0].code = #parent 
* #3-2 ^property[0].valueCode = #3 
* #3-2 ^property[1].code = #hints 
* #3-2 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-3 "(Verweil-) Katheterplacierung / Drainage / Stoma"
* #3-3 ^designation[0].language = #de-AT 
* #3-3 ^designation[0].value = "(Verweil-) Katheterplacierung / Drainage / Stoma" 
* #3-3 ^property[0].code = #parent 
* #3-3 ^property[0].valueCode = #3 
* #3-3 ^property[1].code = #hints 
* #3-3 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-3 ^property[2].code = #child 
* #3-3 ^property[2].valueCode = #3-3-1 
* #3-3 ^property[3].code = #child 
* #3-3 ^property[3].valueCode = #3-3-2 
* #3-3 ^property[4].code = #child 
* #3-3 ^property[4].valueCode = #3-3-3 
* #3-3 ^property[5].code = #child 
* #3-3 ^property[5].valueCode = #3-3-4 
* #3-3 ^property[6].code = #child 
* #3-3 ^property[6].valueCode = #3-3-5 
* #3-3 ^property[7].code = #child 
* #3-3 ^property[7].valueCode = #3-3-6 
* #3-3 ^property[8].code = #child 
* #3-3 ^property[8].valueCode = #3-3-7 
* #3-3 ^property[9].code = #child 
* #3-3 ^property[9].valueCode = #3-3-8 
* #3-3-1 "Permacat"
* #3-3-1 ^designation[0].language = #de-AT 
* #3-3-1 ^designation[0].value = "Permacat" 
* #3-3-1 ^property[0].code = #parent 
* #3-3-1 ^property[0].valueCode = #3-3 
* #3-3-1 ^property[1].code = #hints 
* #3-3-1 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-3-2 "Portacat"
* #3-3-2 ^designation[0].language = #de-AT 
* #3-3-2 ^designation[0].value = "Portacat" 
* #3-3-2 ^property[0].code = #parent 
* #3-3-2 ^property[0].valueCode = #3-3 
* #3-3-2 ^property[1].code = #hints 
* #3-3-2 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-3-3 "Katheter - Drainage"
* #3-3-3 ^designation[0].language = #de-AT 
* #3-3-3 ^designation[0].value = "Katheter - Drainage" 
* #3-3-3 ^property[0].code = #parent 
* #3-3-3 ^property[0].valueCode = #3-3 
* #3-3-3 ^property[1].code = #hints 
* #3-3-3 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-3-4 "TIPPS"
* #3-3-4 ^designation[0].language = #de-AT 
* #3-3-4 ^designation[0].value = "TIPPS" 
* #3-3-4 ^property[0].code = #parent 
* #3-3-4 ^property[0].valueCode = #3-3 
* #3-3-4 ^property[1].code = #hints 
* #3-3-4 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-3-5 "PEG Sonde"
* #3-3-5 ^designation[0].language = #de-AT 
* #3-3-5 ^designation[0].value = "PEG Sonde" 
* #3-3-5 ^property[0].code = #parent 
* #3-3-5 ^property[0].valueCode = #3-3 
* #3-3-5 ^property[1].code = #hints 
* #3-3-5 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-3-6 "Nephrostomie"
* #3-3-6 ^designation[0].language = #de-AT 
* #3-3-6 ^designation[0].value = "Nephrostomie" 
* #3-3-6 ^property[0].code = #parent 
* #3-3-6 ^property[0].valueCode = #3-3 
* #3-3-6 ^property[1].code = #hints 
* #3-3-6 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-3-7 "Pacemaker"
* #3-3-7 ^designation[0].language = #de-AT 
* #3-3-7 ^designation[0].value = "Pacemaker" 
* #3-3-7 ^property[0].code = #parent 
* #3-3-7 ^property[0].valueCode = #3-3 
* #3-3-7 ^property[1].code = #hints 
* #3-3-7 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-3-8 "US Sonde endovasculär"
* #3-3-8 ^designation[0].language = #de-AT 
* #3-3-8 ^designation[0].value = "US Sonde endovasculär" 
* #3-3-8 ^property[0].code = #parent 
* #3-3-8 ^property[0].valueCode = #3-3 
* #3-3-8 ^property[1].code = #hints 
* #3-3-8 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-4 "Bougierung"
* #3-4 ^designation[0].language = #de-AT 
* #3-4 ^designation[0].value = "Bougierung" 
* #3-4 ^property[0].code = #parent 
* #3-4 ^property[0].valueCode = #3 
* #3-4 ^property[1].code = #hints 
* #3-4 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-5 "Dilatation / Rekanalisation / Stein- / Fremdkörperentfernung"
* #3-5 ^designation[0].language = #de-AT 
* #3-5 ^designation[0].value = "Dilatation / Rekanalisation / Stein- / Fremdkörperentfernung" 
* #3-5 ^property[0].code = #parent 
* #3-5 ^property[0].valueCode = #3 
* #3-5 ^property[1].code = #hints 
* #3-5 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-5 ^property[2].code = #child 
* #3-5 ^property[2].valueCode = #3-5-1 
* #3-5 ^property[3].code = #child 
* #3-5 ^property[3].valueCode = #3-5-2 
* #3-5 ^property[4].code = #child 
* #3-5 ^property[4].valueCode = #3-5-3 
* #3-5 ^property[5].code = #child 
* #3-5 ^property[5].valueCode = #3-5-4 
* #3-5 ^property[6].code = #child 
* #3-5 ^property[6].valueCode = #3-5-5 
* #3-5 ^property[7].code = #child 
* #3-5 ^property[7].valueCode = #3-5-6 
* #3-5 ^property[8].code = #child 
* #3-5 ^property[8].valueCode = #3-5-7 
* #3-5-1 "Lyse"
* #3-5-1 ^designation[0].language = #de-AT 
* #3-5-1 ^designation[0].value = "Lyse" 
* #3-5-1 ^property[0].code = #parent 
* #3-5-1 ^property[0].valueCode = #3-5 
* #3-5-1 ^property[1].code = #hints 
* #3-5-1 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-5-2 "Embolektomie / Thrombektomie / Rekanalisation"
* #3-5-2 ^designation[0].language = #de-AT 
* #3-5-2 ^designation[0].value = "Embolektomie / Thrombektomie / Rekanalisation" 
* #3-5-2 ^property[0].code = #parent 
* #3-5-2 ^property[0].valueCode = #3-5 
* #3-5-2 ^property[1].code = #hints 
* #3-5-2 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-5-3 "Ballondilatation"
* #3-5-3 ^designation[0].language = #de-AT 
* #3-5-3 ^designation[0].value = "Ballondilatation" 
* #3-5-3 ^property[0].code = #parent 
* #3-5-3 ^property[0].valueCode = #3-5 
* #3-5-3 ^property[1].code = #hints 
* #3-5-3 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-5-4 "Stent"
* #3-5-4 ^designation[0].language = #de-AT 
* #3-5-4 ^designation[0].value = "Stent" 
* #3-5-4 ^property[0].code = #parent 
* #3-5-4 ^property[0].valueCode = #3-5 
* #3-5-4 ^property[1].code = #hints 
* #3-5-4 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-5-5 "Stentgraft"
* #3-5-5 ^designation[0].language = #de-AT 
* #3-5-5 ^designation[0].value = "Stentgraft" 
* #3-5-5 ^property[0].code = #parent 
* #3-5-5 ^property[0].valueCode = #3-5 
* #3-5-5 ^property[1].code = #hints 
* #3-5-5 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-5-6 "Filter (Cavaschirm)"
* #3-5-6 ^designation[0].language = #de-AT 
* #3-5-6 ^designation[0].value = "Filter (Cavaschirm)" 
* #3-5-6 ^property[0].code = #parent 
* #3-5-6 ^property[0].valueCode = #3-5 
* #3-5-6 ^property[1].code = #hints 
* #3-5-6 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-5-7 "Stein / Fremdkörperentfernung"
* #3-5-7 ^designation[0].language = #de-AT 
* #3-5-7 ^designation[0].value = "Stein / Fremdkörperentfernung" 
* #3-5-7 ^property[0].code = #parent 
* #3-5-7 ^property[0].valueCode = #3-5 
* #3-5-7 ^property[1].code = #hints 
* #3-5-7 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-6 "Embolisation"
* #3-6 ^designation[0].language = #de-AT 
* #3-6 ^designation[0].value = "Embolisation" 
* #3-6 ^property[0].code = #parent 
* #3-6 ^property[0].valueCode = #3 
* #3-6 ^property[1].code = #hints 
* #3-6 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-6 ^property[2].code = #child 
* #3-6 ^property[2].valueCode = #3-6-1 
* #3-6 ^property[3].code = #child 
* #3-6 ^property[3].valueCode = #3-6-2 
* #3-6 ^property[4].code = #child 
* #3-6 ^property[4].valueCode = #3-6-3 
* #3-6-1 "Organ"
* #3-6-1 ^designation[0].language = #de-AT 
* #3-6-1 ^designation[0].value = "Organ" 
* #3-6-1 ^property[0].code = #parent 
* #3-6-1 ^property[0].valueCode = #3-6 
* #3-6-1 ^property[1].code = #hints 
* #3-6-1 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-6-2 "Aneurysma"
* #3-6-2 ^designation[0].language = #de-AT 
* #3-6-2 ^designation[0].value = "Aneurysma" 
* #3-6-2 ^property[0].code = #parent 
* #3-6-2 ^property[0].valueCode = #3-6 
* #3-6-2 ^property[1].code = #hints 
* #3-6-2 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-6-3 "Arterio-Venöse Malformation"
* #3-6-3 ^designation[0].language = #de-AT 
* #3-6-3 ^designation[0].value = "Arterio-Venöse Malformation" 
* #3-6-3 ^property[0].code = #parent 
* #3-6-3 ^property[0].valueCode = #3-6 
* #3-6-3 ^property[1].code = #hints 
* #3-6-3 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-7 "lokale Prozeduren"
* #3-7 ^designation[0].language = #de-AT 
* #3-7 ^designation[0].value = "lokale Prozeduren" 
* #3-7 ^property[0].code = #parent 
* #3-7 ^property[0].valueCode = #3 
* #3-7 ^property[1].code = #hints 
* #3-7 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-7 ^property[2].code = #child 
* #3-7 ^property[2].valueCode = #3-7-1 
* #3-7 ^property[3].code = #child 
* #3-7 ^property[3].valueCode = #3-7-10 
* #3-7 ^property[4].code = #child 
* #3-7 ^property[4].valueCode = #3-7-11 
* #3-7 ^property[5].code = #child 
* #3-7 ^property[5].valueCode = #3-7-2 
* #3-7 ^property[6].code = #child 
* #3-7 ^property[6].valueCode = #3-7-3 
* #3-7 ^property[7].code = #child 
* #3-7 ^property[7].valueCode = #3-7-4 
* #3-7 ^property[8].code = #child 
* #3-7 ^property[8].valueCode = #3-7-5 
* #3-7 ^property[9].code = #child 
* #3-7 ^property[9].valueCode = #3-7-6 
* #3-7 ^property[10].code = #child 
* #3-7 ^property[10].valueCode = #3-7-7 
* #3-7 ^property[11].code = #child 
* #3-7 ^property[11].valueCode = #3-7-8 
* #3-7 ^property[12].code = #child 
* #3-7 ^property[12].valueCode = #3-7-9 
* #3-7-1 "Nukleolyse"
* #3-7-1 ^designation[0].language = #de-AT 
* #3-7-1 ^designation[0].value = "Nukleolyse" 
* #3-7-1 ^property[0].code = #parent 
* #3-7-1 ^property[0].valueCode = #3-7 
* #3-7-1 ^property[1].code = #hints 
* #3-7-1 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-7-10 "Probenentnahme"
* #3-7-10 ^designation[0].language = #de-AT 
* #3-7-10 ^designation[0].value = "Probenentnahme" 
* #3-7-10 ^property[0].code = #parent 
* #3-7-10 ^property[0].valueCode = #3-7 
* #3-7-10 ^property[1].code = #hints 
* #3-7-10 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-7-11 "Präparat"
* #3-7-11 ^designation[0].language = #de-AT 
* #3-7-11 ^designation[0].value = "Präparat" 
* #3-7-11 ^property[0].code = #parent 
* #3-7-11 ^property[0].valueCode = #3-7 
* #3-7-11 ^property[1].code = #hints 
* #3-7-11 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-7-2 "Ganglionblockade"
* #3-7-2 ^designation[0].language = #de-AT 
* #3-7-2 ^designation[0].value = "Ganglionblockade" 
* #3-7-2 ^property[0].code = #parent 
* #3-7-2 ^property[0].valueCode = #3-7 
* #3-7-2 ^property[1].code = #hints 
* #3-7-2 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-7-3 "Intervertebralgelenk"
* #3-7-3 ^designation[0].language = #de-AT 
* #3-7-3 ^designation[0].value = "Intervertebralgelenk" 
* #3-7-3 ^property[0].code = #parent 
* #3-7-3 ^property[0].valueCode = #3-7 
* #3-7-3 ^property[1].code = #hints 
* #3-7-3 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-7-4 "Radiofrequenzablation / Thermoablation / Markierung"
* #3-7-4 ^designation[0].language = #de-AT 
* #3-7-4 ^designation[0].value = "Radiofrequenzablation / Thermoablation / Markierung" 
* #3-7-4 ^property[0].code = #parent 
* #3-7-4 ^property[0].valueCode = #3-7 
* #3-7-4 ^property[1].code = #hints 
* #3-7-4 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-7-5 "Kryoablation"
* #3-7-5 ^designation[0].language = #de-AT 
* #3-7-5 ^designation[0].value = "Kryoablation" 
* #3-7-5 ^property[0].code = #parent 
* #3-7-5 ^property[0].valueCode = #3-7 
* #3-7-5 ^property[1].code = #hints 
* #3-7-5 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-7-6 "Laserablation"
* #3-7-6 ^designation[0].language = #de-AT 
* #3-7-6 ^designation[0].value = "Laserablation" 
* #3-7-6 ^property[0].code = #parent 
* #3-7-6 ^property[0].valueCode = #3-7 
* #3-7-6 ^property[1].code = #hints 
* #3-7-6 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-7-7 "Alkoholablation"
* #3-7-7 ^designation[0].language = #de-AT 
* #3-7-7 ^designation[0].value = "Alkoholablation" 
* #3-7-7 ^property[0].code = #parent 
* #3-7-7 ^property[0].valueCode = #3-7 
* #3-7-7 ^property[1].code = #hints 
* #3-7-7 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-7-8 "Venensampling"
* #3-7-8 ^designation[0].language = #de-AT 
* #3-7-8 ^designation[0].value = "Venensampling" 
* #3-7-8 ^property[0].code = #parent 
* #3-7-8 ^property[0].valueCode = #3-7 
* #3-7-8 ^property[1].code = #hints 
* #3-7-8 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-7-9 "Infiltration/Sklerosierung"
* #3-7-9 ^designation[0].language = #de-AT 
* #3-7-9 ^designation[0].value = "Infiltration/Sklerosierung" 
* #3-7-9 ^property[0].code = #parent 
* #3-7-9 ^property[0].valueCode = #3-7 
* #3-7-9 ^property[1].code = #hints 
* #3-7-9 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-8 "Vertebroplastie / Zementauffüllung"
* #3-8 ^designation[0].language = #de-AT 
* #3-8 ^designation[0].value = "Vertebroplastie / Zementauffüllung" 
* #3-8 ^property[0].code = #parent 
* #3-8 ^property[0].valueCode = #3 
* #3-8 ^property[1].code = #hints 
* #3-8 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-9 "Markierung"
* #3-9 ^designation[0].language = #de-AT 
* #3-9 ^designation[0].value = "Markierung" 
* #3-9 ^property[0].code = #parent 
* #3-9 ^property[0].valueCode = #3 
* #3-9 ^property[1].code = #hints 
* #3-9 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-9 ^property[2].code = #child 
* #3-9 ^property[2].valueCode = #3-9-1 
* #3-9 ^property[3].code = #child 
* #3-9 ^property[3].valueCode = #3-9-2 
* #3-9 ^property[4].code = #child 
* #3-9 ^property[4].valueCode = #3-9-3 
* #3-9 ^property[5].code = #child 
* #3-9 ^property[5].valueCode = #3-9-4 
* #3-9-1 "Draht"
* #3-9-1 ^designation[0].language = #de-AT 
* #3-9-1 ^designation[0].value = "Draht" 
* #3-9-1 ^property[0].code = #parent 
* #3-9-1 ^property[0].valueCode = #3-9 
* #3-9-1 ^property[1].code = #hints 
* #3-9-1 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-9-2 "Farbstoff"
* #3-9-2 ^designation[0].language = #de-AT 
* #3-9-2 ^designation[0].value = "Farbstoff" 
* #3-9-2 ^property[0].code = #parent 
* #3-9-2 ^property[0].valueCode = #3-9 
* #3-9-2 ^property[1].code = #hints 
* #3-9-2 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-9-3 "radioaktive Substanz"
* #3-9-3 ^designation[0].language = #de-AT 
* #3-9-3 ^designation[0].value = "radioaktive Substanz" 
* #3-9-3 ^property[0].code = #parent 
* #3-9-3 ^property[0].valueCode = #3-9 
* #3-9-3 ^property[1].code = #hints 
* #3-9-3 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #3-9-4 "Clipmarkierung"
* #3-9-4 ^designation[0].language = #de-AT 
* #3-9-4 ^designation[0].value = "Clipmarkierung" 
* #3-9-4 ^property[0].code = #parent 
* #3-9-4 ^property[0].valueCode = #3-9 
* #3-9-4 ^property[1].code = #hints 
* #3-9-4 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #4 "NUK Diagnostik mit offenen radioaktiven Stoffen"
* #4 ^designation[0].language = #de-AT 
* #4 ^designation[0].value = "NUK Diagnostik mit offenen radioaktiven Stoffen" 
* #4 ^property[0].code = #hints 
* #4 ^property[0].valueString = "APPC: 1.2.40.0.34.5.38" 
* #4 ^property[1].code = #child 
* #4 ^property[1].valueCode = #4-1 
* #4 ^property[2].code = #child 
* #4 ^property[2].valueCode = #4-10 
* #4 ^property[3].code = #child 
* #4 ^property[3].valueCode = #4-11 
* #4 ^property[4].code = #child 
* #4 ^property[4].valueCode = #4-12 
* #4 ^property[5].code = #child 
* #4 ^property[5].valueCode = #4-13 
* #4 ^property[6].code = #child 
* #4 ^property[6].valueCode = #4-14 
* #4 ^property[7].code = #child 
* #4 ^property[7].valueCode = #4-15 
* #4 ^property[8].code = #child 
* #4 ^property[8].valueCode = #4-16 
* #4 ^property[9].code = #child 
* #4 ^property[9].valueCode = #4-2 
* #4 ^property[10].code = #child 
* #4 ^property[10].valueCode = #4-3 
* #4 ^property[11].code = #child 
* #4 ^property[11].valueCode = #4-4 
* #4 ^property[12].code = #child 
* #4 ^property[12].valueCode = #4-5 
* #4 ^property[13].code = #child 
* #4 ^property[13].valueCode = #4-6 
* #4 ^property[14].code = #child 
* #4 ^property[14].valueCode = #4-7 
* #4 ^property[15].code = #child 
* #4 ^property[15].valueCode = #4-8 
* #4 ^property[16].code = #child 
* #4 ^property[16].valueCode = #4-9 
* #4-1 "Perfusion"
* #4-1 ^designation[0].language = #de-AT 
* #4-1 ^designation[0].value = "Perfusion" 
* #4-1 ^property[0].code = #parent 
* #4-1 ^property[0].valueCode = #4 
* #4-1 ^property[1].code = #hints 
* #4-1 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #4-10 "Liquorszintigrafie"
* #4-10 ^designation[0].language = #de-AT 
* #4-10 ^designation[0].value = "Liquorszintigrafie" 
* #4-10 ^property[0].code = #parent 
* #4-10 ^property[0].valueCode = #4 
* #4-10 ^property[1].code = #hints 
* #4-10 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #4-11 "Galliumscan"
* #4-11 ^designation[0].language = #de-AT 
* #4-11 ^designation[0].value = "Galliumscan" 
* #4-11 ^property[0].code = #parent 
* #4-11 ^property[0].valueCode = #4 
* #4-11 ^property[1].code = #hints 
* #4-11 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #4-12 "Meckelscan"
* #4-12 ^designation[0].language = #de-AT 
* #4-12 ^designation[0].value = "Meckelscan" 
* #4-12 ^property[0].code = #parent 
* #4-12 ^property[0].valueCode = #4 
* #4-12 ^property[1].code = #hints 
* #4-12 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #4-13 "markierte Blutzellen"
* #4-13 ^designation[0].language = #de-AT 
* #4-13 ^designation[0].value = "markierte Blutzellen (Blutungsscan, Leukozytenscan, Thrombozytenscan)" 
* #4-13 ^property[0].code = #parent 
* #4-13 ^property[0].valueCode = #4 
* #4-13 ^property[1].code = #hints 
* #4-13 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #4-14 "Direkte Miktionszytourethrografie"
* #4-14 ^designation[0].language = #de-AT 
* #4-14 ^designation[0].value = "Direkte Miktionszytourethrografie" 
* #4-14 ^property[0].code = #parent 
* #4-14 ^property[0].valueCode = #4 
* #4-14 ^property[1].code = #hints 
* #4-14 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #4-15 "Jodganzkörperscan"
* #4-15 ^designation[0].language = #de-AT 
* #4-15 ^designation[0].value = "Jodganzkörperscan" 
* #4-15 ^property[0].code = #parent 
* #4-15 ^property[0].valueCode = #4 
* #4-15 ^property[1].code = #hints 
* #4-15 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #4-16 "Herzszintigraphie"
* #4-16 ^designation[0].language = #de-AT 
* #4-16 ^designation[0].value = "Herzszintigraphie" 
* #4-16 ^property[0].code = #parent 
* #4-16 ^property[0].valueCode = #4 
* #4-16 ^property[1].code = #hints 
* #4-16 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #4-16 ^property[2].code = #child 
* #4-16 ^property[2].valueCode = #4-16-1 
* #4-16 ^property[3].code = #child 
* #4-16 ^property[3].valueCode = #4-16-2 
* #4-16 ^property[4].code = #child 
* #4-16 ^property[4].valueCode = #4-16-3 
* #4-16-1 "Myocardszintigraphie"
* #4-16-1 ^designation[0].language = #de-AT 
* #4-16-1 ^designation[0].value = "Myocardszintigraphie" 
* #4-16-1 ^property[0].code = #parent 
* #4-16-1 ^property[0].valueCode = #4-16 
* #4-16-1 ^property[1].code = #hints 
* #4-16-1 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #4-16-2 "Radionuklidventrikulografie"
* #4-16-2 ^designation[0].language = #de-AT 
* #4-16-2 ^designation[0].value = "Radionuklidventrikulografie" 
* #4-16-2 ^property[0].code = #parent 
* #4-16-2 ^property[0].valueCode = #4-16 
* #4-16-2 ^property[1].code = #hints 
* #4-16-2 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #4-16-3 "Gated Myokard SPECT"
* #4-16-3 ^designation[0].language = #de-AT 
* #4-16-3 ^designation[0].value = "Gated Myokard SPECT" 
* #4-16-3 ^property[0].code = #parent 
* #4-16-3 ^property[0].valueCode = #4-16 
* #4-16-3 ^property[1].code = #hints 
* #4-16-3 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #4-2 "Metabolismus / Organfunktion"
* #4-2 ^designation[0].language = #de-AT 
* #4-2 ^designation[0].value = "Metabolismus / Organfunktion" 
* #4-2 ^property[0].code = #parent 
* #4-2 ^property[0].valueCode = #4 
* #4-2 ^property[1].code = #hints 
* #4-2 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #4-3 "Knochenszintigrafie"
* #4-3 ^designation[0].language = #de-AT 
* #4-3 ^designation[0].value = "Knochenszintigrafie" 
* #4-3 ^property[0].code = #parent 
* #4-3 ^property[0].valueCode = #4 
* #4-3 ^property[1].code = #hints 
* #4-3 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #4-3 ^property[2].code = #child 
* #4-3 ^property[2].valueCode = #4-3-1 
* #4-3 ^property[3].code = #child 
* #4-3 ^property[3].valueCode = #4-3-2 
* #4-3-1 "Knochenszintigrafie Mehrphasen"
* #4-3-1 ^designation[0].language = #de-AT 
* #4-3-1 ^designation[0].value = "Knochenszintigrafie Mehrphasen" 
* #4-3-1 ^property[0].code = #parent 
* #4-3-1 ^property[0].valueCode = #4-3 
* #4-3-1 ^property[1].code = #hints 
* #4-3-1 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #4-3-2 "Knochenszintigrafie statisch"
* #4-3-2 ^designation[0].language = #de-AT 
* #4-3-2 ^designation[0].value = "Knochenszintigrafie statisch" 
* #4-3-2 ^property[0].code = #parent 
* #4-3-2 ^property[0].valueCode = #4-3 
* #4-3-2 ^property[1].code = #hints 
* #4-3-2 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #4-4 "Rezeptorszintigrafie"
* #4-4 ^designation[0].language = #de-AT 
* #4-4 ^designation[0].value = "Rezeptorszintigrafie" 
* #4-4 ^property[0].code = #parent 
* #4-4 ^property[0].valueCode = #4 
* #4-4 ^property[1].code = #hints 
* #4-4 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #4-5 "Nierenszintigrafie"
* #4-5 ^designation[0].language = #de-AT 
* #4-5 ^designation[0].value = "Nierenszintigrafie" 
* #4-5 ^property[0].code = #parent 
* #4-5 ^property[0].valueCode = #4 
* #4-5 ^property[1].code = #hints 
* #4-5 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #4-5 ^property[2].code = #child 
* #4-5 ^property[2].valueCode = #4-5-1 
* #4-5 ^property[3].code = #child 
* #4-5 ^property[3].valueCode = #4-5-2 
* #4-5 ^property[4].code = #child 
* #4-5 ^property[4].valueCode = #4-5-3 
* #4-5-1 "Nierenszintigrafie Dynamisch (ING,CNG,DNG)"
* #4-5-1 ^designation[0].language = #de-AT 
* #4-5-1 ^designation[0].value = "Nierenszintigrafie Dynamisch (ING,CNG,DNG)" 
* #4-5-1 ^property[0].code = #parent 
* #4-5-1 ^property[0].valueCode = #4-5 
* #4-5-1 ^property[1].code = #hints 
* #4-5-1 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #4-5-2 "Nierenszintigrafie Statisch (DMSA)"
* #4-5-2 ^designation[0].language = #de-AT 
* #4-5-2 ^designation[0].value = "Nierenszintigrafie Statisch (DMSA)" 
* #4-5-2 ^property[0].code = #parent 
* #4-5-2 ^property[0].valueCode = #4-5 
* #4-5-2 ^property[1].code = #hints 
* #4-5-2 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #4-5-3 "Nierenszintigrafie Indirekte Miktionsszintigrafie"
* #4-5-3 ^designation[0].language = #de-AT 
* #4-5-3 ^designation[0].value = "Nierenszintigrafie Indirekte Miktionsszintigrafie" 
* #4-5-3 ^property[0].code = #parent 
* #4-5-3 ^property[0].valueCode = #4-5 
* #4-5-3 ^property[1].code = #hints 
* #4-5-3 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #4-6 "Ventilation"
* #4-6 ^designation[0].language = #de-AT 
* #4-6 ^designation[0].value = "Ventilation" 
* #4-6 ^property[0].code = #parent 
* #4-6 ^property[0].valueCode = #4 
* #4-6 ^property[1].code = #hints 
* #4-6 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #4-7 "GI Transit"
* #4-7 ^designation[0].language = #de-AT 
* #4-7 ^designation[0].value = "GI Transit" 
* #4-7 ^property[0].code = #parent 
* #4-7 ^property[0].valueCode = #4 
* #4-7 ^property[1].code = #hints 
* #4-7 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #4-8 "Knochenmarks-Szintigraphie"
* #4-8 ^designation[0].language = #de-AT 
* #4-8 ^designation[0].value = "Knochenmarks-Szintigraphie" 
* #4-8 ^property[0].code = #parent 
* #4-8 ^property[0].valueCode = #4 
* #4-8 ^property[1].code = #hints 
* #4-8 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
* #4-9 "Lymphszintigrafie"
* #4-9 ^designation[0].language = #de-AT 
* #4-9 ^designation[0].value = "Lymphszintigrafie" 
* #4-9 ^property[0].code = #parent 
* #4-9 ^property[0].valueCode = #4 
* #4-9 ^property[1].code = #hints 
* #4-9 ^property[1].valueString = "APPC: 1.2.40.0.34.5.38" 
