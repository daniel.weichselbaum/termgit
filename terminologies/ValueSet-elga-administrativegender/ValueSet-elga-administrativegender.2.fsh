Instance: elga-administrativegender 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-administrativegender" 
* name = "elga-administrativegender" 
* title = "ELGA_AdministrativeGender" 
* status = #active 
* version = "201907" 
* description = "**Description:** The gender of a person used for adminstrative purposes (as opposed to clinical gender).

**Beschreibung:** Das Geschlecht einer Person zur administrativen Nutzung (entspricht nicht notwendigerweise dem biologischen oder medizinischen Geschlecht)." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.4" 
* date = "2019-07-08" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.hl7.org" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-hl7-administrative-gender"
* compose.include[0].concept[0].code = "F"
* compose.include[0].concept[0].display = "Female"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "Weibliche Person" 
* compose.include[0].concept[1].code = "M"
* compose.include[0].concept[1].display = "Male"
* compose.include[0].concept[1].designation[0].language = #de-AT 
* compose.include[0].concept[1].designation[0].value = "Männliche Person" 
* compose.include[0].concept[2].code = "UN"
* compose.include[0].concept[2].display = "Undifferentiated"
* compose.include[0].concept[2].designation[0].language = #de-AT 
* compose.include[0].concept[2].designation[0].value = "Divers" 
* compose.include[0].concept[2].designation[1].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[2].designation[1].value = "Menschen mit einer Variante der Geschlechtsentwicklung, deren medizinische Zuordnung zum männlichen oder weiblichen Geschlecht aufgrund einer atypischen Entwicklung des biologischen (chromosomalen, anatomischen und/oder hormonellen) Geschlechts nicht eindeutig möglich ist." 
* compose.include[0].concept[2].designation[2].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[2].designation[2].value = "Personen mit einer Variante der Geschlechtsentwicklung gegenüber männlich oder weiblich" 
