Instance: ems-klinischemanifestation 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-klinischemanifestation" 
* name = "ems-klinischemanifestation" 
* title = "EMS_KlinischeManifestation" 
* status = #active 
* content = #complete 
* version = "202101" 
* description = "**Description:** Code list for coding of the clinical symptoms (if applicable) (beside SNOMED CT concepts)

**Beschreibung:** Codeliste für die Codierung der (etwaigen) klinischen Manifestation welche nicht durch SNOMED CT gedeckt werden" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.109" 
* date = "2021-01-01" 
* count = 7 
* #DengueSchwerVerlauf "Dengue-Fieber, schwere Verlaufsform"
* #Hautdiphterie "Hautdiphterie"
* #Osteomyelitis/septischeArthritis "Osteomyelitis/septische Arthritis"
* #ars_exHuster_exFieber "akute respiratorische Symptome ohne Husten und ohne Fieber"
* #ars_exHuster_inkFieber "akute respiratorische Symptome mit Fieber 38°C oder mehr und ohne Husten"
* #ars_inkHusten_exFieber "akute respiratorische Symptome inkl. Husten ohne Fieber"
* #ars_inkHusten_inkFieber "akute respiratorische Symptome inkl. Husten und Fieber 38°C oder mehr"
