Instance: ems-klinischemanifestation 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-ems-klinischemanifestation" 
* name = "ems-klinischemanifestation" 
* title = "EMS_KlinischeManifestation" 
* status = #active 
* content = #complete 
* version = "202101" 
* description = "**Description:** Code list for coding of the clinical symptoms (if applicable) (beside SNOMED CT concepts)

**Beschreibung:** Codeliste für die Codierung der (etwaigen) klinischen Manifestation welche nicht durch SNOMED CT gedeckt werden" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.109" 
* date = "2021-01-01" 
* count = 7 
* concept[0].code = #DengueSchwerVerlauf
* concept[0].display = "Dengue-Fieber, schwere Verlaufsform"
* concept[1].code = #Hautdiphterie
* concept[1].display = "Hautdiphterie"
* concept[2].code = #Osteomyelitis/septischeArthritis
* concept[2].display = "Osteomyelitis/septische Arthritis"
* concept[3].code = #ars_exHuster_exFieber
* concept[3].display = "akute respiratorische Symptome ohne Husten und ohne Fieber"
* concept[4].code = #ars_exHuster_inkFieber
* concept[4].display = "akute respiratorische Symptome mit Fieber 38°C oder mehr und ohne Husten"
* concept[5].code = #ars_inkHusten_exFieber
* concept[5].display = "akute respiratorische Symptome inkl. Husten ohne Fieber"
* concept[6].code = #ars_inkHusten_inkFieber
* concept[6].display = "akute respiratorische Symptome inkl. Husten und Fieber 38°C oder mehr"
