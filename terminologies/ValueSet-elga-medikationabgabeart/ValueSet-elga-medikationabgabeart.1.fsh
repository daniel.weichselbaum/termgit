Instance: elga-medikationabgabeart 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-medikationabgabeart" 
* name = "elga-medikationabgabeart" 
* title = "ELGA_MedikationAbgabeArt" 
* status = #active 
* version = "3.0" 
* description = "**Beschreibung:** ELGA Value Set für die Medikations-Abgabeart" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.159" 
* date = "2015-03-31" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-hl7-act-code"
* compose.include[0].concept[0].code = #FFC
* compose.include[0].concept[0].display = "First Fill - Complete"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "Vollständige Abgabe" 
* compose.include[0].concept[1].code = #FFP
* compose.include[0].concept[1].display = "First Fill - Part Fill"
* compose.include[0].concept[1].designation[0].language = #de-AT 
* compose.include[0].concept[1].designation[0].value = "Partielle Abgabe (erste Abgabe)" 
* compose.include[0].concept[2].code = #RFC
* compose.include[0].concept[2].display = "Refill - Complete"
* compose.include[0].concept[2].designation[0].language = #de-AT 
* compose.include[0].concept[2].designation[0].value = "Partielle Abgabe (letzte Abgabe)" 
* compose.include[0].concept[3].code = #RFP
* compose.include[0].concept[3].display = "Refill - Part Fill"
* compose.include[0].concept[3].designation[0].language = #de-AT 
* compose.include[0].concept[3].designation[0].value = "Partielle Abgabe (weitere Teilabgabe)" 

* expansion.timestamp = "2022-09-13T14:17:35.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem-hl7-act-code"
* expansion.contains[0].code = #FFC
* expansion.contains[0].display = "First Fill - Complete"
* expansion.contains[0].designation[0].language = #de-AT 
* expansion.contains[0].designation[0].value = "Vollständige Abgabe" 
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem-hl7-act-code"
* expansion.contains[1].code = #FFP
* expansion.contains[1].display = "First Fill - Part Fill"
* expansion.contains[1].designation[0].language = #de-AT 
* expansion.contains[1].designation[0].value = "Partielle Abgabe (erste Abgabe)" 
* expansion.contains[2].system = "https://termgit.elga.gv.at/CodeSystem-hl7-act-code"
* expansion.contains[2].code = #RFC
* expansion.contains[2].display = "Refill - Complete"
* expansion.contains[2].designation[0].language = #de-AT 
* expansion.contains[2].designation[0].value = "Partielle Abgabe (letzte Abgabe)" 
* expansion.contains[3].system = "https://termgit.elga.gv.at/CodeSystem-hl7-act-code"
* expansion.contains[3].code = #RFP
* expansion.contains[3].display = "Refill - Part Fill"
* expansion.contains[3].designation[0].language = #de-AT 
* expansion.contains[3].designation[0].value = "Partielle Abgabe (weitere Teilabgabe)" 
