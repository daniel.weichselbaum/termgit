Instance: eimpf-impfrelevanteerkrankung 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-eimpf-impfrelevanteerkrankung" 
* name = "eimpf-impfrelevanteerkrankung" 
* title = "eImpf_ImpfrelevanteErkrankung" 
* status = #active 
* version = "202010.1" 
* description = "**Description:** Infectious diseases that result in long-term immunisation and therefore do not require further vaccination. Contains information protected by copyright of SNOMED International. Any use of SNOMED CT in Austria requires a valid affiliate license or sublicense. The corresponding license is free of charge, provided that the use only takes place in Austria and fulfills the conditions of the Affiliate License Agreement. Affiliate licenses can be requested directly from the respective NRC via the Member Licensing and Distribution Service (MLDS).https://wiki.hl7.at/index.php?title=SCT:SNOMED_CT

**Beschreibung:** Infektionskrankheiten, die eine langfristige Immunisierung nach sich ziehen und daher eine weitere Impfung nicht notwendig machen. Enthält durch SNOMED International urheberrechtlich geschützte Information. Jede Verwendung von SNOMED CT in Österreich erfordert eine aufrechte Affiliate Lizenz oder eine Sublizenz. Die entsprechende Lizenz ist kostenlos, vorausgesetzt die Verwendung findet nur in Österreich statt und erfüllt die Bedingungen des Affiliate License Agreements. Affiliate Lizenzen können über das Member Licensing and Distribution Service (MLDS) direkt beim jeweiligen NRC beantragt werden.https://wiki.hl7.at/index.php?title=SCT:SNOMED_CT" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.2" 
* date = "2020-10-29" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* compose.include[0].concept[0].code = #14189004
* compose.include[0].concept[0].display = "Masern"
* compose.include[0].concept[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#relationships "relationships" 
* compose.include[0].concept[0].designation[0].value = "1.2.40.0.34.5.184:B05~2.16.840.1.113883.6.96:836382004" 
* compose.include[0].concept[1].code = #16541001
* compose.include[0].concept[1].display = "Gelbfieber"
* compose.include[0].concept[1].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#relationships "relationships" 
* compose.include[0].concept[1].designation[0].value = "1.2.40.0.34.5.184:A95~2.16.840.1.113883.6.96:836385002" 
* compose.include[0].concept[2].code = #36653000
* compose.include[0].concept[2].display = "Röteln"
* compose.include[0].concept[2].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#relationships "relationships" 
* compose.include[0].concept[2].designation[0].value = "1.2.40.0.34.5.184:B06~2.16.840.1.113883.6.96:836388000" 
* compose.include[0].concept[3].code = #36989005
* compose.include[0].concept[3].display = "Mumps"
* compose.include[0].concept[3].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#relationships "relationships" 
* compose.include[0].concept[3].designation[0].value = "1.2.40.0.34.5.184:B26~2.16.840.1.113883.6.96:871738001" 
* compose.include[0].concept[4].code = #38907003
* compose.include[0].concept[4].display = "Varizellen"
* compose.include[0].concept[4].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#relationships "relationships" 
* compose.include[0].concept[4].designation[0].value = "1.2.40.0.34.5.184:B01~2.16.840.1.113883.6.96:836495005" 
* compose.include[0].concept[5].code = #40468003
* compose.include[0].concept[5].display = "Hepatitis A"
* compose.include[0].concept[5].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#relationships "relationships" 
* compose.include[0].concept[5].designation[0].value = "1.2.40.0.34.5.184:B15~2.16.840.1.113883.6.96:836375003" 
* compose.include[0].concept[6].code = #52947006
* compose.include[0].concept[6].display = "Japanische Encephalitis"
* compose.include[0].concept[6].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#relationships "relationships" 
* compose.include[0].concept[6].designation[0].value = "1.2.40.0.34.5.184:A83.0~2.16.840.1.113883.6.96:836378001" 
* compose.include[0].concept[7].code = #66071002
* compose.include[0].concept[7].display = "Hepatitis B"
* compose.include[0].concept[7].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#relationships "relationships" 
* compose.include[0].concept[7].designation[0].value = "1.2.40.0.34.5.184:B16~2.16.840.1.113883.6.96:836374004" 
* compose.include[0].concept[8].code = #712986001
* compose.include[0].concept[8].display = "Frühsommermeningoencephalitis"
* compose.include[0].concept[8].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#relationships "relationships" 
* compose.include[0].concept[8].designation[0].value = "1.2.40.0.34.5.184:A84~2.16.840.1.113883.6.96:836403007" 
* compose.include[0].concept[9].code = #840539006
* compose.include[0].concept[9].display = "COVID-19"
* compose.include[0].concept[9].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#relationships "relationships" 
* compose.include[0].concept[9].designation[0].value = "1.2.40.0.34.5.184:U07.1~2.16.840.1.113883.6.96:840534001" 

* expansion.timestamp = "2022-09-13T14:15:18.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* expansion.contains[0].code = #14189004
* expansion.contains[0].display = "Masern"
* expansion.contains[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#relationships "relationships" 
* expansion.contains[0].designation[0].value = "1.2.40.0.34.5.184:B05~2.16.840.1.113883.6.96:836382004" 
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* expansion.contains[1].code = #16541001
* expansion.contains[1].display = "Gelbfieber"
* expansion.contains[1].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#relationships "relationships" 
* expansion.contains[1].designation[0].value = "1.2.40.0.34.5.184:A95~2.16.840.1.113883.6.96:836385002" 
* expansion.contains[2].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* expansion.contains[2].code = #36653000
* expansion.contains[2].display = "Röteln"
* expansion.contains[2].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#relationships "relationships" 
* expansion.contains[2].designation[0].value = "1.2.40.0.34.5.184:B06~2.16.840.1.113883.6.96:836388000" 
* expansion.contains[3].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* expansion.contains[3].code = #36989005
* expansion.contains[3].display = "Mumps"
* expansion.contains[3].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#relationships "relationships" 
* expansion.contains[3].designation[0].value = "1.2.40.0.34.5.184:B26~2.16.840.1.113883.6.96:871738001" 
* expansion.contains[4].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* expansion.contains[4].code = #38907003
* expansion.contains[4].display = "Varizellen"
* expansion.contains[4].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#relationships "relationships" 
* expansion.contains[4].designation[0].value = "1.2.40.0.34.5.184:B01~2.16.840.1.113883.6.96:836495005" 
* expansion.contains[5].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* expansion.contains[5].code = #40468003
* expansion.contains[5].display = "Hepatitis A"
* expansion.contains[5].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#relationships "relationships" 
* expansion.contains[5].designation[0].value = "1.2.40.0.34.5.184:B15~2.16.840.1.113883.6.96:836375003" 
* expansion.contains[6].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* expansion.contains[6].code = #52947006
* expansion.contains[6].display = "Japanische Encephalitis"
* expansion.contains[6].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#relationships "relationships" 
* expansion.contains[6].designation[0].value = "1.2.40.0.34.5.184:A83.0~2.16.840.1.113883.6.96:836378001" 
* expansion.contains[7].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* expansion.contains[7].code = #66071002
* expansion.contains[7].display = "Hepatitis B"
* expansion.contains[7].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#relationships "relationships" 
* expansion.contains[7].designation[0].value = "1.2.40.0.34.5.184:B16~2.16.840.1.113883.6.96:836374004" 
* expansion.contains[8].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* expansion.contains[8].code = #712986001
* expansion.contains[8].display = "Frühsommermeningoencephalitis"
* expansion.contains[8].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#relationships "relationships" 
* expansion.contains[8].designation[0].value = "1.2.40.0.34.5.184:A84~2.16.840.1.113883.6.96:836403007" 
* expansion.contains[9].system = "https://termgit.elga.gv.at/CodeSystem-snomed-ct-auszug"
* expansion.contains[9].code = #840539006
* expansion.contains[9].display = "COVID-19"
* expansion.contains[9].designation[0].use = https://termgit.elga.gv.at/CodeSystem-austrian-designation-use#relationships "relationships" 
* expansion.contains[9].designation[0].value = "1.2.40.0.34.5.184:U07.1~2.16.840.1.113883.6.96:840534001" 
