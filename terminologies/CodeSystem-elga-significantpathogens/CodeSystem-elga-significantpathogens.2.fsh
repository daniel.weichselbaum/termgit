Instance: elga-significantpathogens 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-significantpathogens" 
* name = "elga-significantpathogens" 
* title = "ELGA_SignificantPathogens" 
* status = #active 
* content = #complete 
* version = "3.0" 
* description = "**Description:** Set of significant pathogens listed in ''Epidemiologisches Meldesystem Benutzerhandbuch Fachlicher Teil'', 1. edition, October 2008, BMGFJ     

**Beschreibung:** Erreger meldepflichtiger Infektionskrankheiten laut ''Epidemiologisches Meldesystem Benutzerhandbuch Fachlicher Teil'', 1. Auflage Oktober 2008, des BMGFJ" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.45" 
* date = "2015-03-31" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* count = 67 
* property[0].code = #hints 
* property[0].type = #string 
* property[1].code = #status 
* property[1].type = #code 
* #SP001 "Adenovirus im Konjunktivalabstrich (*)"
* #SP001 ^designation[0].language = #de-AT 
* #SP001 ^designation[0].value = "Adenovirus im Konjunktivalabstrich (*)" 
* #SP001 ^property[0].code = #hints 
* #SP001 ^property[0].valueString = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* #SP002 "HIV"
* #SP002 ^designation[0].language = #de-AT 
* #SP002 ^designation[0].value = "HIV" 
* #SP003 "Bacillus anthracis"
* #SP003 ^designation[0].language = #de-AT 
* #SP003 ^designation[0].value = "Bacillus anthracis" 
* #SP004 "Influenza A/H5"
* #SP004 ^designation[0].language = #de-AT 
* #SP004 ^designation[0].value = "Influenza A/H5" 
* #SP005 "Influenza A/H5N1"
* #SP005 ^designation[0].language = #de-AT 
* #SP005 ^designation[0].value = "Influenza A/H5N1" 
* #SP006 "Clostridium botulinum"
* #SP006 ^designation[0].language = #de-AT 
* #SP006 ^designation[0].value = "Clostridium botulinum" 
* #SP007 "Brucella spp."
* #SP007 ^designation[0].language = #de-AT 
* #SP007 ^designation[0].value = "Brucella spp." 
* #SP008 "Campylobacter spp."
* #SP008 ^designation[0].language = #de-AT 
* #SP008 ^designation[0].value = "Campylobacter spp." 
* #SP009 "Chlamydia trachomatis"
* #SP009 ^designation[0].language = #de-AT 
* #SP009 ^designation[0].value = "Chlamydia trachomatis" 
* #SP010 "Vibrio cholerae"
* #SP010 ^designation[0].language = #de-AT 
* #SP010 ^designation[0].value = "Vibrio cholerae" 
* #SP011 "Denguevirus"
* #SP011 ^designation[0].language = #de-AT 
* #SP011 ^designation[0].value = "Denguevirus" 
* #SP012 "Corynebacterium diphtheriae"
* #SP012 ^designation[0].language = #de-AT 
* #SP012 ^designation[0].value = "Corynebacterium diphtheriae" 
* #SP013 "Corynebacterium ulcerans"
* #SP013 ^designation[0].language = #de-AT 
* #SP013 ^designation[0].value = "Corynebacterium ulcerans" 
* #SP014 "Ebolavirus"
* #SP014 ^designation[0].language = #de-AT 
* #SP014 ^designation[0].value = "Ebolavirus" 
* #SP014 ^property[0].code = #hints 
* #SP014 ^property[0].valueString = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* #SP015 "Escherichia coli, sonstige darmpathogene Stämme"
* #SP015 ^designation[0].language = #de-AT 
* #SP015 ^designation[0].value = "Escherichia coli, sonstige darmpathogene Stämme" 
* #SP015 ^property[0].code = #hints 
* #SP015 ^property[0].valueString = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* #SP016 "Echinococcus spp."
* #SP016 ^designation[0].language = #de-AT 
* #SP016 ^designation[0].value = "Echinococcus spp." 
* #SP017 "Rickettsia prowazekii"
* #SP017 ^designation[0].language = #de-AT 
* #SP017 ^designation[0].value = "Rickettsia prowazekii" 
* #SP017 ^property[0].code = #hints 
* #SP017 ^property[0].valueString = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* #SP018 "FSME-Virus"
* #SP018 ^designation[0].language = #de-AT 
* #SP018 ^designation[0].value = "FSME-Virus" 
* #SP018 ^property[0].code = #hints 
* #SP018 ^property[0].valueString = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* #SP019 "Gelbfieber-Virus"
* #SP019 ^designation[0].language = #de-AT 
* #SP019 ^designation[0].value = "Gelbfieber-Virus" 
* #SP020 "Giardia lamblia"
* #SP020 ^designation[0].language = #de-AT 
* #SP020 ^designation[0].value = "Giardia lamblia" 
* #SP021 "Neisseria gonorrhoeae"
* #SP021 ^designation[0].language = #de-AT 
* #SP021 ^designation[0].value = "Neisseria gonorrhoeae" 
* #SP022 "Haemophilus influenzae"
* #SP022 ^designation[0].language = #de-AT 
* #SP022 ^designation[0].value = "Haemophilus influenzae" 
* #SP023 "Hantavirus"
* #SP023 ^designation[0].language = #de-AT 
* #SP023 ^designation[0].value = "Hantavirus" 
* #SP023 ^property[0].code = #hints 
* #SP023 ^property[0].valueString = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* #SP024 "Hepatitis-A-Virus"
* #SP024 ^designation[0].language = #de-AT 
* #SP024 ^designation[0].value = "Hepatitis-A-Virus" 
* #SP025 "Hepatitis-B-Virus"
* #SP025 ^designation[0].language = #de-AT 
* #SP025 ^designation[0].value = "Hepatitis-B-Virus" 
* #SP026 "Hepatitis-C-Virus"
* #SP026 ^designation[0].language = #de-AT 
* #SP026 ^designation[0].value = "Hepatitis-C-Virus" 
* #SP027 "HDV - Hepatitis-D-Virus akut"
* #SP027 ^designation[0].language = #de-AT 
* #SP027 ^designation[0].value = "HDV - Hepatitis-D-Virus akut" 
* #SP027 ^property[0].code = #hints 
* #SP027 ^property[0].valueString = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* #SP028 "HEV - akute Virushepatitis E"
* #SP028 ^designation[0].language = #de-AT 
* #SP028 ^designation[0].value = "HEV - akute Virushepatitis E" 
* #SP028 ^property[0].code = #hints 
* #SP028 ^property[0].valueString = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* #SP029 "Influenzavirus"
* #SP029 ^designation[0].language = #de-AT 
* #SP029 ^designation[0].value = "Influenzavirus" 
* #SP030 "Bordetella pertussis"
* #SP030 ^designation[0].language = #de-AT 
* #SP030 ^designation[0].value = "Bordetella pertussis" 
* #SP031 "Cryptosporidium spp."
* #SP031 ^designation[0].language = #de-AT 
* #SP031 ^designation[0].value = "Cryptosporidium spp." 
* #SP032 "Lassavirus"
* #SP032 ^designation[0].language = #de-AT 
* #SP032 ^designation[0].value = "Lassavirus" 
* #SP032 ^property[0].code = #hints 
* #SP032 ^property[0].valueString = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* #SP033 "Borrelia recurrentis"
* #SP033 ^designation[0].language = #de-AT 
* #SP033 ^designation[0].value = "Borrelia recurrentis" 
* #SP034 "Legionella spp."
* #SP034 ^designation[0].language = #de-AT 
* #SP034 ^designation[0].value = "Legionella spp." 
* #SP035 "Mycobacterium leprae"
* #SP035 ^designation[0].language = #de-AT 
* #SP035 ^designation[0].value = "Mycobacterium leprae" 
* #SP035 ^property[0].code = #hints 
* #SP035 ^property[0].valueString = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* #SP036 "Leptospira interrogans"
* #SP036 ^designation[0].language = #de-AT 
* #SP036 ^designation[0].value = "Leptospira interrogans" 
* #SP037 "Listeria monocytogenes"
* #SP037 ^designation[0].language = #de-AT 
* #SP037 ^designation[0].value = "Listeria monocytogenes" 
* #SP038 "Plasmodium spp."
* #SP038 ^designation[0].language = #de-AT 
* #SP038 ^designation[0].value = "Plasmodium spp." 
* #SP039 "Marburgvirus"
* #SP039 ^designation[0].language = #de-AT 
* #SP039 ^designation[0].value = "Marburgvirus" 
* #SP039 ^property[0].code = #hints 
* #SP039 ^property[0].valueString = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* #SP040 "Masernvirus"
* #SP040 ^designation[0].language = #de-AT 
* #SP040 ^designation[0].value = "Masernvirus" 
* #SP041 "Neisseria meningitidis"
* #SP041 ^designation[0].language = #de-AT 
* #SP041 ^designation[0].value = "Neisseria meningitidis" 
* #SP042 "Mumpsvirus"
* #SP042 ^designation[0].language = #de-AT 
* #SP042 ^designation[0].value = "Mumpsvirus" 
* #SP043 "Norovirus"
* #SP043 ^designation[0].language = #de-AT 
* #SP043 ^designation[0].value = "Norovirus" 
* #SP043 ^property[0].code = #hints 
* #SP043 ^property[0].valueString = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* #SP044 "Chlamydophila psittaci"
* #SP044 ^designation[0].language = #de-AT 
* #SP044 ^designation[0].value = "Chlamydophila psittaci" 
* #SP044 ^property[0].code = #hints 
* #SP044 ^property[0].valueString = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* #SP045 "Yersinia pestis"
* #SP045 ^designation[0].language = #de-AT 
* #SP045 ^designation[0].value = "Yersinia pestis" 
* #SP046 "Streptococcus pneumoniae"
* #SP046 ^designation[0].language = #de-AT 
* #SP046 ^designation[0].value = "Streptococcus pneumoniae" 
* #SP047 "Variola-Virus"
* #SP047 ^designation[0].language = #de-AT 
* #SP047 ^designation[0].value = "Variola-Virus" 
* #SP048 "Poliovirus"
* #SP048 ^designation[0].language = #de-AT 
* #SP048 ^designation[0].value = "Poliovirus" 
* #SP049 "Coxiella burnetii"
* #SP049 ^designation[0].language = #de-AT 
* #SP049 ^designation[0].value = "Coxiella burnetii" 
* #SP050 "Rotavirus"
* #SP050 ^designation[0].language = #de-AT 
* #SP050 ^designation[0].value = "Rotavirus" 
* #SP050 ^property[0].code = #hints 
* #SP050 ^property[0].valueString = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* #SP051 "Rubella-Virus"
* #SP051 ^designation[0].language = #de-AT 
* #SP051 ^designation[0].value = "Rubella-Virus" 
* #SP052 "DEPRECATED: Rubella-Virus"
* #SP052 ^designation[0].language = #de-AT 
* #SP052 ^designation[0].value = "DEPRECATED: Rubella-Virus" 
* #SP052 ^property[0].code = #status 
* #SP052 ^property[0].valueCode = #retired 
* #SP052 ^property[1].code = #hints 
* #SP052 ^property[1].valueString = "Duplikat, daher auf Ungültig (DEPRECATED) gesetzt" 
* #SP053 "Salmonella spp. außer S. Typhi und S. Paratyphi"
* #SP053 ^designation[0].language = #de-AT 
* #SP053 ^designation[0].value = "Salmonella spp. außer S. Typhi und S. Paratyphi" 
* #SP054 "SARS-Coronavirus, SARS-CoV"
* #SP054 ^designation[0].language = #de-AT 
* #SP054 ^designation[0].value = "SARS-Coronavirus, SARS-CoV" 
* #SP055 "Shigella spp."
* #SP055 ^designation[0].language = #de-AT 
* #SP055 ^designation[0].value = "Shigella spp." 
* #SP056 "Treponema pallidum"
* #SP056 ^designation[0].language = #de-AT 
* #SP056 ^designation[0].value = "Treponema pallidum" 
* #SP057 "Clostridium tetani"
* #SP057 ^designation[0].language = #de-AT 
* #SP057 ^designation[0].value = "Clostridium tetani" 
* #SP058 "Lyssa-Virus"
* #SP058 ^designation[0].language = #de-AT 
* #SP058 ^designation[0].value = "Lyssa-Virus" 
* #SP059 "Toxoplasma gondii"
* #SP059 ^designation[0].language = #de-AT 
* #SP059 ^designation[0].value = "Toxoplasma gondii" 
* #SP060 "Trichinella spp."
* #SP060 ^designation[0].language = #de-AT 
* #SP060 ^designation[0].value = "Trichinella spp." 
* #SP061 "Mycobacterium-tuberculosis-Komplex"
* #SP061 ^designation[0].language = #de-AT 
* #SP061 ^designation[0].value = "Mycobacterium-tuberculosis-Komplex" 
* #SP062 "Francisella tularensis"
* #SP062 ^designation[0].language = #de-AT 
* #SP062 ^designation[0].value = "Francisella tularensis" 
* #SP063 "Salmonella typhi"
* #SP063 ^designation[0].language = #de-AT 
* #SP063 ^designation[0].value = "Salmonella typhi" 
* #SP064 "Salmonella paratyphi"
* #SP064 ^designation[0].language = #de-AT 
* #SP064 ^designation[0].value = "Salmonella paratyphi" 
* #SP065 "West-Nil-Virus"
* #SP065 ^designation[0].language = #de-AT 
* #SP065 ^designation[0].value = "West-Nil-Virus" 
* #SP066 "Yersinia enterocolitica"
* #SP066 ^designation[0].language = #de-AT 
* #SP066 ^designation[0].value = "Yersinia enterocolitica" 
* #SP067 "Yersinia pseudotuberculosis"
* #SP067 ^designation[0].language = #de-AT 
* #SP067 ^designation[0].value = "Yersinia pseudotuberculosis" 
