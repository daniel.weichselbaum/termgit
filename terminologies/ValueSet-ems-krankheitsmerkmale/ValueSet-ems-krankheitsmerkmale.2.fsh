Instance: ems-krankheitsmerkmale 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-ems-krankheitsmerkmale" 
* name = "ems-krankheitsmerkmale" 
* title = "EMS_Krankheitsmerkmale" 
* status = #active 
* version = "202101" 
* description = "**Description:** Concepts for symptoms based on the EMS Requirements

**Beschreibung:** Konzepte Krankheitsmerkmale laut EMS Anforderungen" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.18" 
* date = "2021-01-27" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-ems-krankheitsmerkmale"
* compose.include[0].concept[0].code = "Verstorben"
* compose.include[0].concept[0].display = "Verstorben"
* compose.include[0].concept[1].code = "TOD"
* compose.include[0].concept[1].display = "Tod"
* compose.include[0].concept[2].code = "TOD_KRANKHEIT"
* compose.include[0].concept[2].display = "Krankheitsbezogener Tod"
* compose.include[0].concept[3].code = "AUSSCHEIDER"
* compose.include[0].concept[3].display = "Ausscheider"
* compose.include[0].concept[4].code = "ASYMPTOMATISCH"
* compose.include[0].concept[4].display = "Asymptomatisch"
