Instance: elga-medikationfrequenz 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-medikationfrequenz" 
* name = "elga-medikationfrequenz" 
* title = "ELGA_MedikationFrequenz" 
* status = #active 
* version = "3.0" 
* description = "**Description:** ELGA ValueSet for frequency

**Beschreibung:** ELGA ValueSet für Frequenz" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.69" 
* date = "2015-03-31" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-ucum"
* compose.include[0].concept[0].code = "d"
* compose.include[0].concept[0].display = "Day"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "Tag" 
* compose.include[0].concept[1].code = "mo"
* compose.include[0].concept[1].display = "Month"
* compose.include[0].concept[1].designation[0].language = #de-AT 
* compose.include[0].concept[1].designation[0].value = "Monat" 
* compose.include[0].concept[2].code = "wk"
* compose.include[0].concept[2].display = "Week"
* compose.include[0].concept[2].designation[0].language = #de-AT 
* compose.include[0].concept[2].designation[0].value = "Woche" 
