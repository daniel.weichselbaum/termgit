Instance: elga-fachaerzte 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-fachaerzte" 
* name = "elga-fachaerzte" 
* title = "ELGA_Fachaerzte" 
* status = #active 
* content = #complete 
* version = "3.0" 
* description = "**Beschreibung:** Berufsbezeichnungen für Fachärzte lt. Ärztinnen-/Ärzte-Ausbildungsordnung" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.160" 
* date = "2015-03-31" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.elga.gv.at" 
* count = 53 
* #102 "Fachärztin/Facharzt für Anästhesiologie und Intensivmedizin"
* #102 ^designation[0].language = #de-AT 
* #102 ^designation[0].value = "Fachärztin/Facharzt für Anästhesiologie und Intensivmedizin" 
* #103 "Fachärztin/Facharzt für Anatomie"
* #103 ^designation[0].language = #de-AT 
* #103 ^designation[0].value = "Fachärztin/Facharzt für Anatomie" 
* #104 "Fachärztin/Facharzt für Arbeitsmedizin"
* #104 ^designation[0].language = #de-AT 
* #104 ^designation[0].value = "Fachärztin/Facharzt für Arbeitsmedizin" 
* #105 "Fachärztin/Facharzt für Augenheilkunde und Optometrie"
* #105 ^designation[0].language = #de-AT 
* #105 ^designation[0].value = "Fachärztin/Facharzt für Augenheilkunde und Optometrie" 
* #106 "Fachärztin/Facharzt für Blutgruppenserologie und Transfusionsmedizin"
* #106 ^designation[0].language = #de-AT 
* #106 ^designation[0].value = "Fachärztin/Facharzt für Blutgruppenserologie und Transfusionsmedizin" 
* #107 "Fachärztin/Facharzt für Chirurgie"
* #107 ^designation[0].language = #de-AT 
* #107 ^designation[0].value = "Fachärztin/Facharzt für Chirurgie" 
* #108 "Fachärztin/Facharzt für Frauenheilkunde und Geburtshilfe"
* #108 ^designation[0].language = #de-AT 
* #108 ^designation[0].value = "Fachärztin/Facharzt für Frauenheilkunde und Geburtshilfe" 
* #109 "Fachärztin/Facharzt für Gerichtsmedizin"
* #109 ^designation[0].language = #de-AT 
* #109 ^designation[0].value = "Fachärztin/Facharzt für Gerichtsmedizin" 
* #110 "Fachärztin/Facharzt für Hals-, Nasen- und Ohrenkrankheiten"
* #110 ^designation[0].language = #de-AT 
* #110 ^designation[0].value = "Fachärztin/Facharzt für Hals-, Nasen- und Ohrenkrankheiten" 
* #111 "Fachärztin/Facharzt für Haut- und Geschlechtskrankheiten"
* #111 ^designation[0].language = #de-AT 
* #111 ^designation[0].value = "Fachärztin/Facharzt für Haut- und Geschlechtskrankheiten" 
* #112 "Fachärztin/Facharzt für Herzchirurgie"
* #112 ^designation[0].language = #de-AT 
* #112 ^designation[0].value = "Fachärztin/Facharzt für Herzchirurgie" 
* #113 "Fachärztin/Facharzt für Histologie und Embryologie"
* #113 ^designation[0].language = #de-AT 
* #113 ^designation[0].value = "Fachärztin/Facharzt für Histologie und Embryologie" 
* #114 "Fachärztin/Facharzt für Hygiene und Mikrobiologie"
* #114 ^designation[0].language = #de-AT 
* #114 ^designation[0].value = "Fachärztin/Facharzt für Hygiene und Mikrobiologie" 
* #115 "Fachärztin/Facharzt für Immunologie"
* #115 ^designation[0].language = #de-AT 
* #115 ^designation[0].value = "Fachärztin/Facharzt für Immunologie" 
* #116 "Fachärztin/Facharzt für Innere Medizin"
* #116 ^designation[0].language = #de-AT 
* #116 ^designation[0].value = "Fachärztin/Facharzt für Innere Medizin" 
* #117 "Fachärztin/Facharzt für Kinder- und Jugendchirurgie"
* #117 ^designation[0].language = #de-AT 
* #117 ^designation[0].value = "Fachärztin/Facharzt für Kinder- und Jugendchirurgie" 
* #118 "Fachärztin/Facharzt für Kinder- und Jugendheilkunde"
* #118 ^designation[0].language = #de-AT 
* #118 ^designation[0].value = "Fachärztin/Facharzt für Kinder- und Jugendheilkunde" 
* #119 "Fachärztin/Facharzt für Kinder- und Jugendpsychiatrie"
* #119 ^designation[0].language = #de-AT 
* #119 ^designation[0].value = "Fachärztin/Facharzt für Kinder- und Jugendpsychiatrie" 
* #120 "Fachärztin/Facharzt für Lungenkrankheiten"
* #120 ^designation[0].language = #de-AT 
* #120 ^designation[0].value = "Fachärztin/Facharzt für Lungenkrankheiten" 
* #121 "Fachärztin/Facharzt für Medizinische Biologie"
* #121 ^designation[0].language = #de-AT 
* #121 ^designation[0].value = "Fachärztin/Facharzt für Medizinische Biologie" 
* #122 "Fachärztin/Facharzt für Medizinische Biophysik"
* #122 ^designation[0].language = #de-AT 
* #122 ^designation[0].value = "Fachärztin/Facharzt für Medizinische Biophysik" 
* #123 "Fachärztin/Facharzt für Medizinische Genetik"
* #123 ^designation[0].language = #de-AT 
* #123 ^designation[0].value = "Fachärztin/Facharzt für Medizinische Genetik" 
* #124 "Fachärztin/Facharzt für Medizinische und Chemische Labordiagnostik"
* #124 ^designation[0].language = #de-AT 
* #124 ^designation[0].value = "Fachärztin/Facharzt für Medizinische und Chemische Labordiagnostik" 
* #125 "Fachärztin/Facharzt für Medizinische Leistungsphysiologie"
* #125 ^designation[0].language = #de-AT 
* #125 ^designation[0].value = "Fachärztin/Facharzt für Medizinische Leistungsphysiologie" 
* #126 "Fachärztin/Facharzt für Mikrobiologisch-Serologische Labordiagnostik"
* #126 ^designation[0].language = #de-AT 
* #126 ^designation[0].value = "Fachärztin/Facharzt für Mikrobiologisch-Serologische Labordiagnostik" 
* #127 "Fachärztin/Facharzt für Mund-, Kiefer- und Gesichtschirurgie"
* #127 ^designation[0].language = #de-AT 
* #127 ^designation[0].value = "Fachärztin/Facharzt für Mund-, Kiefer- und Gesichtschirurgie" 
* #128 "Fachärztin/Facharzt für Neurobiologie"
* #128 ^designation[0].language = #de-AT 
* #128 ^designation[0].value = "Fachärztin/Facharzt für Neurobiologie" 
* #129 "Fachärztin/Facharzt für Neurochirurgie"
* #129 ^designation[0].language = #de-AT 
* #129 ^designation[0].value = "Fachärztin/Facharzt für Neurochirurgie" 
* #130 "Fachärztin/Facharzt für Neurologie"
* #130 ^designation[0].language = #de-AT 
* #130 ^designation[0].value = "Fachärztin/Facharzt für Neurologie" 
* #131 "Fachärztin/Facharzt für Neurologie und Psychiatrie"
* #131 ^designation[0].language = #de-AT 
* #131 ^designation[0].value = "Fachärztin/Facharzt für Neurologie und Psychiatrie" 
* #132 "Fachärztin/Facharzt für Neuropathologie"
* #132 ^designation[0].language = #de-AT 
* #132 ^designation[0].value = "Fachärztin/Facharzt für Neuropathologie" 
* #133 "Fachärztin/Facharzt für Nuklearmedizin"
* #133 ^designation[0].language = #de-AT 
* #133 ^designation[0].value = "Fachärztin/Facharzt für Nuklearmedizin" 
* #134 "Fachärztin/Facharzt für Orthopädie und Orthopädische Chirurgie"
* #134 ^designation[0].language = #de-AT 
* #134 ^designation[0].value = "Fachärztin/Facharzt für Orthopädie und Orthopädische Chirurgie" 
* #135 "Fachärztin/Facharzt für Pathologie"
* #135 ^designation[0].language = #de-AT 
* #135 ^designation[0].value = "Fachärztin/Facharzt für Pathologie" 
* #136 "Fachärztin/Facharzt für Pathophysiologie"
* #136 ^designation[0].language = #de-AT 
* #136 ^designation[0].value = "Fachärztin/Facharzt für Pathophysiologie" 
* #137 "Fachärztin/Facharzt für Pharmakologie und Toxikologie"
* #137 ^designation[0].language = #de-AT 
* #137 ^designation[0].value = "Fachärztin/Facharzt für Pharmakologie und Toxikologie" 
* #138 "Fachärztin/Facharzt für Physikalische Medizin und Allgemeine Rehabilitation"
* #138 ^designation[0].language = #de-AT 
* #138 ^designation[0].value = "Fachärztin/Facharzt für Physikalische Medizin und Allgemeine Rehabilitation" 
* #139 "Fachärztin/Facharzt für Physiologie"
* #139 ^designation[0].language = #de-AT 
* #139 ^designation[0].value = "Fachärztin/Facharzt für Physiologie" 
* #140 "Fachärztin/Facharzt für Plastische, Ästhetische und Rekonstruktive Chirurgie"
* #140 ^designation[0].language = #de-AT 
* #140 ^designation[0].value = "Fachärztin/Facharzt für Plastische, Ästhetische und Rekonstruktive Chirurgie" 
* #141 "Fachärztin/Facharzt für Psychiatrie"
* #141 ^designation[0].language = #de-AT 
* #141 ^designation[0].value = "Fachärztin/Facharzt für Psychiatrie" 
* #142 "Fachärztin/Facharzt für Psychiatrie und Neurologie"
* #142 ^designation[0].language = #de-AT 
* #142 ^designation[0].value = "Fachärztin/Facharzt für Psychiatrie und Neurologie" 
* #143 "Fachärztin/Facharzt für Psychiatrie und Psychotherapeutische Medizin"
* #143 ^designation[0].language = #de-AT 
* #143 ^designation[0].value = "Fachärztin/Facharzt für Psychiatrie und Psychotherapeutische Medizin" 
* #144 "Fachärztin/Facharzt für Radiologie"
* #144 ^designation[0].language = #de-AT 
* #144 ^designation[0].value = "Fachärztin/Facharzt für Radiologie" 
* #145 "Fachärztin/Facharzt für Sozialmedizin"
* #145 ^designation[0].language = #de-AT 
* #145 ^designation[0].value = "Fachärztin/Facharzt für Sozialmedizin" 
* #146 "Fachärztin/Facharzt für Spezifische Prophylaxe und Tropenmedizin"
* #146 ^designation[0].language = #de-AT 
* #146 ^designation[0].value = "Fachärztin/Facharzt für Spezifische Prophylaxe und Tropenmedizin" 
* #147 "Fachärztin/Facharzt für Strahlentherapie-Radioonkologie"
* #147 ^designation[0].language = #de-AT 
* #147 ^designation[0].value = "Fachärztin/Facharzt für Strahlentherapie-Radioonkologie" 
* #148 "Fachärztin/Facharzt für Theoretische Sonderfächer"
* #148 ^designation[0].language = #de-AT 
* #148 ^designation[0].value = "Fachärztin/Facharzt für Theoretische Sonderfächer" 
* #149 "Fachärztin/Facharzt für Thoraxchirurgie"
* #149 ^designation[0].language = #de-AT 
* #149 ^designation[0].value = "Fachärztin/Facharzt für Thoraxchirurgie" 
* #150 "Fachärztin/Facharzt für Tumorbiologie"
* #150 ^designation[0].language = #de-AT 
* #150 ^designation[0].value = "Fachärztin/Facharzt für Tumorbiologie" 
* #151 "Fachärztin/Facharzt für Unfallchirurgie"
* #151 ^designation[0].language = #de-AT 
* #151 ^designation[0].value = "Fachärztin/Facharzt für Unfallchirurgie" 
* #152 "Fachärztin/Facharzt für Urologie"
* #152 ^designation[0].language = #de-AT 
* #152 ^designation[0].value = "Fachärztin/Facharzt für Urologie" 
* #153 "Fachärztin/Facharzt für Virologie"
* #153 ^designation[0].language = #de-AT 
* #153 ^designation[0].value = "Fachärztin/Facharzt für Virologie" 
* #154 "Fachärztin/Facharzt für Zahn-, Mund- und Kieferheilkunde"
* #154 ^designation[0].language = #de-AT 
* #154 ^designation[0].value = "Fachärztin/Facharzt für Zahn-, Mund- und Kieferheilkunde" 
