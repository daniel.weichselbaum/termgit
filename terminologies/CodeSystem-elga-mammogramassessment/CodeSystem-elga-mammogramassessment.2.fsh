Instance: elga-mammogramassessment 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-elga-mammogramassessment" 
* name = "elga-mammogramassessment" 
* title = "ELGA_MammogramAssessment" 
* status = #active 
* content = #complete 
* version = "3.3" 
* description = "**Description:** Definition of the values for a mammogram according DICOM Coding Scheme 'BI' from 'Digital Imaging and Communications in Medicine (DICOM) Part 16: Content Mapping Resource'

**Beschreibung:** Definition der Werte für ein Mammogramm entsprechend DICOM Coding Scheme 'BI' aus 'Digital Imaging and Communications in Medicine (DICOM) Part 16: Content Mapping Resource'" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.49" 
* date = "2015-10-05" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* count = 10 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #II.AC.a "0 - Need additional imaging evaluation"
* #II.AC.a ^definition = 0   braucht zusätzliche Bildgebung
* #II.AC.a ^designation[0].language = #de-AT 
* #II.AC.a ^designation[0].value = "0  braucht zusätzliche Bildgebung" 
* #II.AC.b.1 "1  Negative"
* #II.AC.b.1 ^definition = 1  negativ (d.h. keine Masse, keine Architekturstörung, kein Kalk, keine Hautverdickung etc.)
* #II.AC.b.1 ^designation[0].language = #de-AT 
* #II.AC.b.1 ^designation[0].value = "1  Negativ" 
* #II.AC.b.2 "2  Benign Finding"
* #II.AC.b.2 ^definition = 2  benigne Befunde (z.B. Zysten, intramammäre Lymphknoten, Brustimplantate, stationäre postoperative Befunde, stationäre Fibroadenome)
* #II.AC.b.2 ^designation[0].language = #de-AT 
* #II.AC.b.2 ^designation[0].value = "2  Benigner Befund" 
* #II.AC.b.3 "3 - Probably Benign Finding  short interval followup"
* #II.AC.b.3 ^definition = 3  Wahrscheinlich benigne Befunde, kurzfristige Verlaufskontrolle empfohlen (Läsionen mit scharfen Rändern, ovalärer Form, horizontaler Orientierung, Malignitätsrisiko unter 2 %.)
* #II.AC.b.3 ^designation[0].language = #de-AT 
* #II.AC.b.3 ^designation[0].value = "3  Wahrscheinlich benigner Befund, kurzfristige Verlaufskontrolle empfohlen" 
* #II.AC.b.4 "4 - Suspicious abnormality, biopsy should be considered"
* #II.AC.b.4 ^definition = 4  Verdächtig, Biopsie sollte erwogen werden (Läsionen mit intermediärer Wahrscheinlichkeit für Malignität)
* #II.AC.b.4 ^designation[0].language = #de-AT 
* #II.AC.b.4 ^designation[0].value = "4  Suspekter Befund, Biopsie sollte erwogen werden" 
* #II.AC.b.4 ^property[0].code = #child 
* #II.AC.b.4 ^property[0].valueCode = #MA.II.A.5.4A 
* #II.AC.b.4 ^property[1].code = #child 
* #II.AC.b.4 ^property[1].valueCode = #MA.II.A.5.4B 
* #II.AC.b.4 ^property[2].code = #child 
* #II.AC.b.4 ^property[2].valueCode = #MA.II.A.5.4C 
* #MA.II.A.5.4A "4A  Low suspicion"
* #MA.II.A.5.4A ^definition = 4A  Geringgradig verdächtiger Befund, Biopsie sollte erwogen werden
* #MA.II.A.5.4A ^designation[0].language = #de-AT 
* #MA.II.A.5.4A ^designation[0].value = "4A  Geringgradiger Verdacht" 
* #MA.II.A.5.4A ^property[0].code = #parent 
* #MA.II.A.5.4A ^property[0].valueCode = #II.AC.b.4 
* #MA.II.A.5.4B "4B  Intermediate suspicion"
* #MA.II.A.5.4B ^definition = 4B  Zwischenstuflich verdächtiger Befund, Biopsie sollte erwogen werden
* #MA.II.A.5.4B ^designation[0].language = #de-AT 
* #MA.II.A.5.4B ^designation[0].value = "4B  Zwischenstuflicher Verdacht" 
* #MA.II.A.5.4B ^property[0].code = #parent 
* #MA.II.A.5.4B ^property[0].valueCode = #II.AC.b.4 
* #MA.II.A.5.4C "4C  Moderate suspicion"
* #MA.II.A.5.4C ^definition = 4C  Mäßig verdächtiger Befund, Biopsie sollte erwogen werden
* #MA.II.A.5.4C ^designation[0].language = #de-AT 
* #MA.II.A.5.4C ^designation[0].value = "4C  Mäßiger Verdacht" 
* #MA.II.A.5.4C ^property[0].code = #parent 
* #MA.II.A.5.4C ^property[0].valueCode = #II.AC.b.4 
* #II.AC.b.5 "5 - Highly suggestive of malignancy, take appropriate action"
* #II.AC.b.5 ^definition = 5  hochsuspekt, fast sicher maligne, eine adäquate Aktion sollte folgen (Risiko auf Malignität von 95 % oder größer)
* #II.AC.b.5 ^designation[0].language = #de-AT 
* #II.AC.b.5 ^designation[0].value = "5  Hochsuspekt, fast sicher maligne, eine adäquate Aktion sollte folgen" 
* #MA.II.A.5.6 "6 - Known biopsy proven malignancy"
* #MA.II.A.5.6 ^definition = 6  Bekannter maligner Befund, mit Biopsie bestätigt
* #MA.II.A.5.6 ^designation[0].language = #de-AT 
* #MA.II.A.5.6 ^designation[0].value = "6  Gesichertes Malignom" 
