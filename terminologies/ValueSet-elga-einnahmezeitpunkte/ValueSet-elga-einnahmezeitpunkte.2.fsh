Instance: elga-einnahmezeitpunkte 
InstanceOf: ValueSet 
Usage: #definition 
* url = "https://termgit.elga.gv.at/ValueSet-elga-einnahmezeitpunkte" 
* name = "elga-einnahmezeitpunkte" 
* title = "ELGA_Einnahmezeitpunkte" 
* status = #active 
* version = "3.1" 
* description = "**Description:** Timing Events for drug administration

**Beschreibung:** Einnahmezeitpunkte für Medikamente" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.59" 
* date = "2015-07-31" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.hl7.org" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem-hl7-timing-event"
* compose.include[0].concept[0].code = "ACD"
* compose.include[0].concept[0].display = "ACD"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "Mittags" 
* compose.include[0].concept[1].code = "ACM"
* compose.include[0].concept[1].display = "ACM"
* compose.include[0].concept[1].designation[0].language = #de-AT 
* compose.include[0].concept[1].designation[0].value = "Morgens" 
* compose.include[0].concept[2].code = "ACV"
* compose.include[0].concept[2].display = "ACV"
* compose.include[0].concept[2].designation[0].language = #de-AT 
* compose.include[0].concept[2].designation[0].value = "Abends" 
* compose.include[0].concept[3].code = "HS"
* compose.include[0].concept[3].display = "HS"
* compose.include[0].concept[3].designation[0].language = #de-AT 
* compose.include[0].concept[3].designation[0].value = "Nachts" 
