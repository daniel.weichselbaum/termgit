Instance: icpc2 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://termgit.elga.gv.at/CodeSystem-icpc2" 
* name = "icpc2" 
* title = "ICPC2" 
* status = #active 
* content = #complete 
* version = "Version 2017" 
* description = "**Description:** International Classification of Primary Care

**Beschreibung:** International Classification of Primary Care" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.175" 
* date = "2019-07-03" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.elga.gv.at" 
* count = 1370 
* property[0].code = #hints 
* property[0].type = #string 
* property[1].code = #child 
* property[1].type = #code 
* property[2].code = #parent 
* property[2].type = #code 
* property[3].code = #Relationships 
* property[3].type = #string 
* #A "Allgemein und unspezifisch"
* #A ^property[0].code = #hints 
* #A ^property[0].valueString = "Beachte:  Hinweise: In dieser Klassifikation beziehen sich die Begriffe 'allgemein' oder 'mehrfach' auf drei oder mehr Körperregionen, Systeme oder Organe. Beschwerden, die nur ein oder zwei Regionen betreffen, sollten den betreffenden Regionen zugeordnet werden." 
* #A ^property[1].code = #child 
* #A ^property[1].valueCode = #A01 
* #A ^property[2].code = #child 
* #A ^property[2].valueCode = #A02 
* #A ^property[3].code = #child 
* #A ^property[3].valueCode = #A03 
* #A ^property[4].code = #child 
* #A ^property[4].valueCode = #A04 
* #A ^property[5].code = #child 
* #A ^property[5].valueCode = #A05 
* #A ^property[6].code = #child 
* #A ^property[6].valueCode = #A06 
* #A ^property[7].code = #child 
* #A ^property[7].valueCode = #A07 
* #A ^property[8].code = #child 
* #A ^property[8].valueCode = #A08 
* #A ^property[9].code = #child 
* #A ^property[9].valueCode = #A09 
* #A ^property[10].code = #child 
* #A ^property[10].valueCode = #A10 
* #A ^property[11].code = #child 
* #A ^property[11].valueCode = #A11 
* #A ^property[12].code = #child 
* #A ^property[12].valueCode = #A13 
* #A ^property[13].code = #child 
* #A ^property[13].valueCode = #A16 
* #A ^property[14].code = #child 
* #A ^property[14].valueCode = #A18 
* #A ^property[15].code = #child 
* #A ^property[15].valueCode = #A20 
* #A ^property[16].code = #child 
* #A ^property[16].valueCode = #A21 
* #A ^property[17].code = #child 
* #A ^property[17].valueCode = #A23 
* #A ^property[18].code = #child 
* #A ^property[18].valueCode = #A25 
* #A ^property[19].code = #child 
* #A ^property[19].valueCode = #A26 
* #A ^property[20].code = #child 
* #A ^property[20].valueCode = #A27 
* #A ^property[21].code = #child 
* #A ^property[21].valueCode = #A28 
* #A ^property[22].code = #child 
* #A ^property[22].valueCode = #A29 
* #A ^property[23].code = #child 
* #A ^property[23].valueCode = #A30 
* #A ^property[24].code = #child 
* #A ^property[24].valueCode = #A31 
* #A ^property[25].code = #child 
* #A ^property[25].valueCode = #A32 
* #A ^property[26].code = #child 
* #A ^property[26].valueCode = #A33 
* #A ^property[27].code = #child 
* #A ^property[27].valueCode = #A34 
* #A ^property[28].code = #child 
* #A ^property[28].valueCode = #A35 
* #A ^property[29].code = #child 
* #A ^property[29].valueCode = #A36 
* #A ^property[30].code = #child 
* #A ^property[30].valueCode = #A37 
* #A ^property[31].code = #child 
* #A ^property[31].valueCode = #A38 
* #A ^property[32].code = #child 
* #A ^property[32].valueCode = #A39 
* #A ^property[33].code = #child 
* #A ^property[33].valueCode = #A40 
* #A ^property[34].code = #child 
* #A ^property[34].valueCode = #A41 
* #A ^property[35].code = #child 
* #A ^property[35].valueCode = #A42 
* #A ^property[36].code = #child 
* #A ^property[36].valueCode = #A43 
* #A ^property[37].code = #child 
* #A ^property[37].valueCode = #A44 
* #A ^property[38].code = #child 
* #A ^property[38].valueCode = #A45 
* #A ^property[39].code = #child 
* #A ^property[39].valueCode = #A46 
* #A ^property[40].code = #child 
* #A ^property[40].valueCode = #A47 
* #A ^property[41].code = #child 
* #A ^property[41].valueCode = #A48 
* #A ^property[42].code = #child 
* #A ^property[42].valueCode = #A49 
* #A ^property[43].code = #child 
* #A ^property[43].valueCode = #A50 
* #A ^property[44].code = #child 
* #A ^property[44].valueCode = #A51 
* #A ^property[45].code = #child 
* #A ^property[45].valueCode = #A52 
* #A ^property[46].code = #child 
* #A ^property[46].valueCode = #A53 
* #A ^property[47].code = #child 
* #A ^property[47].valueCode = #A54 
* #A ^property[48].code = #child 
* #A ^property[48].valueCode = #A55 
* #A ^property[49].code = #child 
* #A ^property[49].valueCode = #A56 
* #A ^property[50].code = #child 
* #A ^property[50].valueCode = #A57 
* #A ^property[51].code = #child 
* #A ^property[51].valueCode = #A58 
* #A ^property[52].code = #child 
* #A ^property[52].valueCode = #A59 
* #A ^property[53].code = #child 
* #A ^property[53].valueCode = #A60 
* #A ^property[54].code = #child 
* #A ^property[54].valueCode = #A61 
* #A ^property[55].code = #child 
* #A ^property[55].valueCode = #A62 
* #A ^property[56].code = #child 
* #A ^property[56].valueCode = #A63 
* #A ^property[57].code = #child 
* #A ^property[57].valueCode = #A64 
* #A ^property[58].code = #child 
* #A ^property[58].valueCode = #A65 
* #A ^property[59].code = #child 
* #A ^property[59].valueCode = #A66 
* #A ^property[60].code = #child 
* #A ^property[60].valueCode = #A67 
* #A ^property[61].code = #child 
* #A ^property[61].valueCode = #A68 
* #A ^property[62].code = #child 
* #A ^property[62].valueCode = #A69 
* #A ^property[63].code = #child 
* #A ^property[63].valueCode = #A70 
* #A ^property[64].code = #child 
* #A ^property[64].valueCode = #A71 
* #A ^property[65].code = #child 
* #A ^property[65].valueCode = #A72 
* #A ^property[66].code = #child 
* #A ^property[66].valueCode = #A73 
* #A ^property[67].code = #child 
* #A ^property[67].valueCode = #A74 
* #A ^property[68].code = #child 
* #A ^property[68].valueCode = #A75 
* #A ^property[69].code = #child 
* #A ^property[69].valueCode = #A76 
* #A ^property[70].code = #child 
* #A ^property[70].valueCode = #A77 
* #A ^property[71].code = #child 
* #A ^property[71].valueCode = #A78 
* #A ^property[72].code = #child 
* #A ^property[72].valueCode = #A79 
* #A ^property[73].code = #child 
* #A ^property[73].valueCode = #A80 
* #A ^property[74].code = #child 
* #A ^property[74].valueCode = #A81 
* #A ^property[75].code = #child 
* #A ^property[75].valueCode = #A82 
* #A ^property[76].code = #child 
* #A ^property[76].valueCode = #A84 
* #A ^property[77].code = #child 
* #A ^property[77].valueCode = #A85 
* #A ^property[78].code = #child 
* #A ^property[78].valueCode = #A86 
* #A ^property[79].code = #child 
* #A ^property[79].valueCode = #A87 
* #A ^property[80].code = #child 
* #A ^property[80].valueCode = #A88 
* #A ^property[81].code = #child 
* #A ^property[81].valueCode = #A89 
* #A ^property[82].code = #child 
* #A ^property[82].valueCode = #A90 
* #A ^property[83].code = #child 
* #A ^property[83].valueCode = #A91 
* #A ^property[84].code = #child 
* #A ^property[84].valueCode = #A92 
* #A ^property[85].code = #child 
* #A ^property[85].valueCode = #A93 
* #A ^property[86].code = #child 
* #A ^property[86].valueCode = #A94 
* #A ^property[87].code = #child 
* #A ^property[87].valueCode = #A95 
* #A ^property[88].code = #child 
* #A ^property[88].valueCode = #A96 
* #A ^property[89].code = #child 
* #A ^property[89].valueCode = #A97 
* #A ^property[90].code = #child 
* #A ^property[90].valueCode = #A98 
* #A ^property[91].code = #child 
* #A ^property[91].valueCode = #A99 
* #A01 "Schmerz generalisiert/an mehreren Stellen"
* #A01 ^definition = Inklusive: generalisierte chronische Schmerzen allgemeiner Art~ anhaltende Schmerzen an mehreren Stellen Exklusive: nicht näher bezeichneter Schmerz A29 Kriterien:
* #A01 ^property[0].code = #parent 
* #A01 ^property[0].valueCode = #A 
* #A01 ^property[1].code = #Relationships 
* #A01 ^property[1].valueString = "ICD-10 2017:R52.0~R52.1~R52.2~R52.9" 
* #A02 "Frösteln"
* #A02 ^definition = Inklusive: Schüttelfrost~ kalte Schauer Exklusive: Fieber A03 Kriterien:
* #A02 ^property[0].code = #parent 
* #A02 ^property[0].valueCode = #A 
* #A02 ^property[1].code = #Relationships 
* #A02 ^property[1].valueString = "ICD-10 2017:R68.8" 
* #A03 "Fieber"
* #A03 ^definition = Inklusive: Pyrexie Exklusive: Exanthem A76~ Erschöpfung durch Hitze oder Hitzschlag A88 Kriterien:
* #A03 ^property[0].code = #parent 
* #A03 ^property[0].valueCode = #A 
* #A03 ^property[1].code = #Relationships 
* #A03 ^property[1].valueString = "ICD-10 2017:R50.8~R50.9" 
* #A04 "Schwäche/allgemeine Müdigkeit"
* #A04 ^definition = Inklusive: Erschöpfungssysndrom~ Mattigkeit~ Lethargie~ postvirales Müdigkeitssyndrom Exklusive: Unwohlsein~ Unpäßlichkeit A05~ Schläfrigkeit A29~ Erschöpfung durch Hitze A88~ Jetlag A88~ Schlafsucht P06 Kriterien:
* #A04 ^property[0].code = #parent 
* #A04 ^property[0].valueCode = #A 
* #A04 ^property[1].code = #Relationships 
* #A04 ^property[1].valueString = "ICD-10 2017:G93.3~R53" 
* #A05 "Unwohlsein"
* #A05 ^definition = Inklusive: Unpässlichkeit/Malaise Exklusive: Seneszenz~ Senilität P05~ Gewichtsverlust~ Kachexie T08~ Fehl-/Unterernährung T91 Kriterien:
* #A05 ^property[0].code = #parent 
* #A05 ^property[0].valueCode = #A 
* #A05 ^property[1].code = #Relationships 
* #A05 ^property[1].valueString = "ICD-10 2017:R53" 
* #A06 "Ohnmacht/Synkope"
* #A06 ^definition = Inklusive: Blackout~ Kollaps~ vasovagale Synkope Exklusive: Koma A07~ Schwächegefühl/Schwindel/Benommenheit N17 Kriterien:
* #A06 ^property[0].code = #parent 
* #A06 ^property[0].valueCode = #A 
* #A06 ^property[1].code = #Relationships 
* #A06 ^property[1].valueString = "ICD-10 2017:R55" 
* #A07 "Koma"
* #A07 ^definition = Inklusive: Stupor Exklusive: Synkope A06 Kriterien:
* #A07 ^property[0].code = #parent 
* #A07 ^property[0].valueCode = #A 
* #A07 ^property[1].code = #Relationships 
* #A07 ^property[1].valueString = "ICD-10 2017:R40.0~R40.1~R40.2" 
* #A08 "Schwellung"
* #A08 ^definition = Inklusive: Knoten/Gewebemasse NNB Exklusive: Schwellung der Lymphknoten B02~ Ödem K07~ Gelenk L20~ Mamma X19~ Y16 Kriterien:
* #A08 ^property[0].code = #parent 
* #A08 ^property[0].valueCode = #A 
* #A08 ^property[1].code = #Relationships 
* #A08 ^property[1].valueString = "ICD-10 2017:R68.8" 
* #A09 "Übermäßiges Schwitzen"
* #A09 ^definition = Inklusive: Hyperhidrosis~ Nachtschweiß~ Probleme mit Schwitzen Exklusive: Schweißdrüsenerkrankung S92 Kriterien:
* #A09 ^property[0].code = #parent 
* #A09 ^property[0].valueCode = #A 
* #A09 ^property[1].code = #Relationships 
* #A09 ^property[1].valueString = "ICD-10 2017:R61.0~R61.1~R61.9" 
* #A10 "Blutung/Hämorrhagie NNB"
* #A10 ^property[0].code = #parent 
* #A10 ^property[0].valueCode = #A 
* #A10 ^property[1].code = #Relationships 
* #A10 ^property[1].valueString = "ICD-10 2017:R58" 
* #A11 "Brustschmerz NNB"
* #A11 ^definition = Inklusive:  Exklusive: dem Herzen zugeordnete Schmerzen K01~ dem Brustkorb L04~ den Atemwegen R01 Kriterien:
* #A11 ^property[0].code = #parent 
* #A11 ^property[0].valueCode = #A 
* #A11 ^property[1].code = #Relationships 
* #A11 ^property[1].valueString = "ICD-10 2017:R07.4" 
* #A13 "Besorgnis über/Angst vor medizinischer Behandlung"
* #A13 ^definition = Inklusive: Besorgnis wegen/Angst vor Folgen eines Medikamentes/einer medizinischen Behandlung Exklusive: Nebenwirkungen von Medikamenten A85~ Komplikationen bei medikamentöser oder chirurgischer Behandlung A87 Kriterien:
* #A13 ^property[0].code = #parent 
* #A13 ^property[0].valueCode = #A 
* #A13 ^property[1].code = #Relationships 
* #A13 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #A16 "Unruhiges Kleinkind"
* #A16 ^definition = Inklusive: übermäßig weinendes/unruhiges Kind Exklusive: Kolik bei Kindern D01~ Unruhe bei Kindern oder Erwachsenen P04 Kriterien:
* #A16 ^property[0].code = #parent 
* #A16 ^property[0].valueCode = #A 
* #A16 ^property[1].code = #Relationships 
* #A16 ^property[1].valueString = "ICD-10 2017:R68.1" 
* #A18 "Besorgnis über äußere Erscheinung"
* #A18 ^definition = Inklusive:  Exklusive: Besorgnis wegen Aussehens der Ohren H15~ wegen Schwangerschaft W21~ Aussehen der Brüste X22 Kriterien:
* #A18 ^property[0].code = #parent 
* #A18 ^property[0].valueCode = #A 
* #A18 ^property[1].code = #Relationships 
* #A18 ^property[1].valueString = "ICD-10 2017:R46.8" 
* #A20 "Gespräch über/Bitte um Sterbehilfe"
* #A20 ^property[0].code = #parent 
* #A20 ^property[0].valueCode = #A 
* #A20 ^property[1].code = #Relationships 
* #A20 ^property[1].valueString = "ICD-10 2017:Z71.8" 
* #A21 "Risikofaktoren für bösartige Neubildungen"
* #A21 ^definition = Inklusive: Eigen-/Familienanamnese für bösartige Neubildung~ vorangegangene Behandlung~ andere Risikofaktoren für bösartige Neubildung Exklusive:  Kriterien:
* #A21 ^property[0].code = #parent 
* #A21 ^property[0].valueCode = #A 
* #A21 ^property[1].code = #Relationships 
* #A21 ^property[1].valueString = "ICD-10 2017:Z80.0~Z80.1~Z80.2~Z80.3~Z80.4~Z80.5~Z80.6~Z80.7~Z80.8~Z80.9~Z85.0~Z85.1~Z85.2~Z85.3~Z85.4~Z85.5~Z85.6~Z85.7~Z85.8~Z85.9~Z92.6" 
* #A23 "Risikofaktoren NNB"
* #A23 ^definition = Inklusive: Kontakt mit infektiöser Erkrankung~ Eigen-/Familienanamnese für Infektionskrankheit~ vorangegangene Behandlung~ andere Risikofaktoren für Infektionskrankheit Exklusive: Risikofaktor für maligne Erkrankung A21~ für kardiovaskuläre Erkrankung K22 Kriterien:
* #A23 ^property[0].code = #parent 
* #A23 ^property[0].valueCode = #A 
* #A23 ^property[1].code = #Relationships 
* #A23 ^property[1].valueString = "ICD-10 2017:Z20.0~Z20.1~Z20.2~Z20.3~Z20.4~Z20.5~Z20.6~Z20.7~Z20.8~Z20.9~Z28.0~Z28.1~Z28.2~Z28.8~Z28.9~Z72.0~Z72.1~Z72.2~Z72.3~Z72.4~Z72.5~Z73.2~Z81.0~Z81.1~Z81.2~Z81.3~Z81.4~Z81.8~Z82.0~Z82.1~Z82.2~Z82.5~Z82.6~Z82.7~Z82.8~Z83.0~Z83.1~Z83.2~Z83.3~Z83.4~Z83.5~Z83.6~Z83.7~Z84.0~Z84.1~Z84.2~Z84.3~Z84.8~Z86.0~Z86.1~Z86.2~Z86.3~Z86.4~Z86.5~Z86.6~Z87.0~Z87.1~Z87.2~Z87.3~Z87.4~Z87.5~Z87.6~Z87.7~Z87.8~Z88.0~Z88.1~Z88.2~Z88.3~Z88.4~Z88.5~Z88.6~Z88.7~Z88.8~Z88.9~Z91.0~Z91.1~Z91.2~Z91.3~Z91.4~Z91.5~Z91.6~Z91.7~Z91.8~Z92.0~Z92.1~Z92.2~Z92.3~Z92.4~Z92.5~Z92.8~Z92.9" 
* #A25 "Angst vor dem Tod/Sterben"
* #A25 ^property[0].code = #parent 
* #A25 ^property[0].valueCode = #A 
* #A25 ^property[1].code = #Relationships 
* #A25 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #A26 "Angst vor Krebserkrankung NNB"
* #A26 ^definition = Inklusive:  Exklusive: Wenn der Patient Krebs hat ist die Erkrankung zu kodieren Kriterien: Besorgnis oder Angst vor Krebs, der nicht einem bestimmten Kapitel zugeordnet ist, bei einem Patienten, der die Krankheit nicht hat oder bei dem die Diagnose noch nicht gesichert ist
* #A26 ^property[0].code = #parent 
* #A26 ^property[0].valueCode = #A 
* #A26 ^property[1].code = #Relationships 
* #A26 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #A27 "Angst vor anderer Krankheit NNB"
* #A27 ^definition = Inklusive:  Exklusive: Angst vor Krebs NNB A26~ wenn der Patient die Erkrankung hat~ ist die Erkrankung zu kodieren Kriterien: Besorgnis oder Angst vor einer anderen Krankheit, die nicht einem bestimmten Kapitel zugeordnet ist, bei einem Patienten, der die Krankheit nicht hat oder bei dem die Diagnose noch nicht gesichert ist
* #A27 ^property[0].code = #parent 
* #A27 ^property[0].valueCode = #A 
* #A27 ^property[1].code = #Relationships 
* #A27 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #A28 "Funktionseinschränkung/Behinderung NNB (A)"
* #A28 ^definition = Inklusive:  Exklusive: Sturz A29 Kriterien: Funktionseinschränkung oder Behinderung die keinem Problem in einem anderen Kapitel zugeordnet ist
* #A28 ^property[0].code = #parent 
* #A28 ^property[0].valueCode = #A 
* #A28 ^property[1].code = #Relationships 
* #A28 ^property[1].valueString = "ICD-10 2017:R26.3~R29.6~Z73.6~Z74.0~Z74.1~Z74.2~Z74.3~Z74.8~Z74.9~Z99.0~Z99.3~Z99.8~Z99.9" 
* #A28 ^property[2].code = #hints 
* #A28 ^property[2].valueString = "Beachte:  Hinweise: COOP/WONCA Charts eignen sich zur Dokumentation des funktionellen Status des Patienten (siehe Kapitel 8)" 
* #A29 "Andere Allgemeinsymptome/Beschwerden"
* #A29 ^definition = Inklusive: Schwerfälligkeit~ Benommenheit~ Stürze~ unspezifische Schmerzen Exklusive:  Kriterien:
* #A29 ^property[0].code = #parent 
* #A29 ^property[0].valueCode = #A 
* #A29 ^property[1].code = #Relationships 
* #A29 ^property[1].valueString = "ICD-10 2017:R68.0~R68.8" 
* #A30 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? vollständig"
* #A30 ^property[0].code = #parent 
* #A30 ^property[0].valueCode = #A 
* #A31 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? teilweise"
* #A31 ^property[0].code = #parent 
* #A31 ^property[0].valueCode = #A 
* #A32 "Allergie-/Sensitivitätstestung"
* #A32 ^property[0].code = #parent 
* #A32 ^property[0].valueCode = #A 
* #A33 "Mikrobiologische/immunologische Untersuchung"
* #A33 ^property[0].code = #parent 
* #A33 ^property[0].valueCode = #A 
* #A34 "Blutuntersuchung"
* #A34 ^property[0].code = #parent 
* #A34 ^property[0].valueCode = #A 
* #A35 "Urinuntersuchung"
* #A35 ^property[0].code = #parent 
* #A35 ^property[0].valueCode = #A 
* #A36 "Stuhluntersuchung"
* #A36 ^property[0].code = #parent 
* #A36 ^property[0].valueCode = #A 
* #A37 "Histologische Untersuchung/zytologischer Abstrich"
* #A37 ^property[0].code = #parent 
* #A37 ^property[0].valueCode = #A 
* #A38 "Andere Laboruntersuchung NAK"
* #A38 ^property[0].code = #parent 
* #A38 ^property[0].valueCode = #A 
* #A39 "Körperliche Funktionsprüfung"
* #A39 ^property[0].code = #parent 
* #A39 ^property[0].valueCode = #A 
* #A40 "Diagnostische Endoskopie"
* #A40 ^property[0].code = #parent 
* #A40 ^property[0].valueCode = #A 
* #A41 "Diagnostisches Röntgen/Bildgebung"
* #A41 ^property[0].code = #parent 
* #A41 ^property[0].valueCode = #A 
* #A42 "Elektrische Aufzeichnungsverfahren"
* #A42 ^property[0].code = #parent 
* #A42 ^property[0].valueCode = #A 
* #A43 "Andere diagnostische Untersuchung"
* #A43 ^property[0].code = #parent 
* #A43 ^property[0].valueCode = #A 
* #A44 "Präventive Impfung/Medikation"
* #A44 ^property[0].code = #parent 
* #A44 ^property[0].valueCode = #A 
* #A45 "Beobachtung/Gesundheitsschulung/Beratung/Diät"
* #A45 ^property[0].code = #parent 
* #A45 ^property[0].valueCode = #A 
* #A46 "Konsultation eines anderen Primärversorgers"
* #A46 ^property[0].code = #parent 
* #A46 ^property[0].valueCode = #A 
* #A47 "Konsultation eines Facharztes"
* #A47 ^property[0].code = #parent 
* #A47 ^property[0].valueCode = #A 
* #A48 "Klärung/Besprechung des Beratungsanlasses mit dem Patienten"
* #A48 ^property[0].code = #parent 
* #A48 ^property[0].valueCode = #A 
* #A49 "Andere Vorsorgemaßnahme"
* #A49 ^property[0].code = #parent 
* #A49 ^property[0].valueCode = #A 
* #A50 "Medikation/Verschreibung/Erneuerung/Injektion"
* #A50 ^property[0].code = #parent 
* #A50 ^property[0].valueCode = #A 
* #A51 "Inzision/Drainage/Spülung/Absaugung/Entfernung von Körperflüssigkeit"
* #A51 ^property[0].code = #parent 
* #A51 ^property[0].valueCode = #A 
* #A52 "Exzision/Entfernung von Gewebe/Biopsie/Destruktion/Debridement/Verkauterung"
* #A52 ^property[0].code = #parent 
* #A52 ^property[0].valueCode = #A 
* #A53 "Instrumentelle Manipulation/Katheterisierung/Intubation/Dilatation"
* #A53 ^property[0].code = #parent 
* #A53 ^property[0].valueCode = #A 
* #A54 "Reposition/Fixierung/Naht/Gips/prothetische Versorgung"
* #A54 ^property[0].code = #parent 
* #A54 ^property[0].valueCode = #A 
* #A55 "Lokale Injektion/Infiltration"
* #A55 ^property[0].code = #parent 
* #A55 ^property[0].valueCode = #A 
* #A56 "Verband/Druck/Kompression/Tamponade"
* #A56 ^property[0].code = #parent 
* #A56 ^property[0].valueCode = #A 
* #A57 "Physikalische Therapie/Rehabilitation"
* #A57 ^property[0].code = #parent 
* #A57 ^property[0].valueCode = #A 
* #A58 "Therapeutische Beratung/Zuhören"
* #A58 ^property[0].code = #parent 
* #A58 ^property[0].valueCode = #A 
* #A59 "Andere therapeutische Maßnahme/kleine Chirurgie NAK"
* #A59 ^property[0].code = #parent 
* #A59 ^property[0].valueCode = #A 
* #A60 "Testresultat/Ergebnis eigene Maßnahme"
* #A60 ^property[0].code = #parent 
* #A60 ^property[0].valueCode = #A 
* #A61 "Ergebnis Untersuchung/Test/Brief von anderem Leistungserbringer"
* #A61 ^property[0].code = #parent 
* #A61 ^property[0].valueCode = #A 
* #A62 "Adminstrative Maßnahme"
* #A62 ^property[0].code = #parent 
* #A62 ^property[0].valueCode = #A 
* #A63 "Folgekonsultation unspezifiziert"
* #A63 ^property[0].code = #parent 
* #A63 ^property[0].valueCode = #A 
* #A64 "Konsultation/Problembehandlung auf Initiative des Leistungserbringers"
* #A64 ^property[0].code = #parent 
* #A64 ^property[0].valueCode = #A 
* #A65 "Konsultation/Problembehandlung auf Initiative Dritter"
* #A65 ^property[0].code = #parent 
* #A65 ^property[0].valueCode = #A 
* #A66 "Überweisung an andere/n Leistungserbringer (nicht ärztl.)/Pflegekraft/Therapeuten/Sozialarbeiter"
* #A66 ^property[0].code = #parent 
* #A66 ^property[0].valueCode = #A 
* #A67 "Überweisung an einen Arzt/Spezialisten/eine Klinik/ein Krankenhaus"
* #A67 ^property[0].code = #parent 
* #A67 ^property[0].valueCode = #A 
* #A68 "Andere Überweisung NAK"
* #A68 ^property[0].code = #parent 
* #A68 ^property[0].valueCode = #A 
* #A69 "Anderer Beratungsanlass NAK"
* #A69 ^property[0].code = #parent 
* #A69 ^property[0].valueCode = #A 
* #A70 "Tuberkulose"
* #A70 ^definition = Inklusive: Tuberkuloseinfektion an jeglicher Körperstelle~ Spätfolgen einer Tuberkulose Exklusive:  Kriterien: Konversion zu positivem Tuberkulintest, oder Nachweis von Mycobacterium tuberculosis bzw. M. bovis bei Mikroskopie bzw. Kultur, oder charakteristisches Erscheinungsbild bei Thoraxröntgen, oder charakteristisches histologisches Erscheinungsbild bei Biopsie
* #A70 ^property[0].code = #parent 
* #A70 ^property[0].valueCode = #A 
* #A70 ^property[1].code = #Relationships 
* #A70 ^property[1].valueString = "ICD-10 2017:A15.0~A15.1~A15.2~A15.3~A15.4~A15.5~A15.6~A15.7~A15.8~A15.9~A16.0~A16.1~A16.2~A16.3~A16.4~A16.5~A16.7~A16.8~A16.9~A17.0~A17.1~A17.8~A17.9~A18.0~A18.1~A18.2~A18.3~A18.4~A18.5~A18.6~A18.7~A18.8~A19.0~A19.1~A19.2~A19.8~A19.9~B90.0~B90.1~B90.2~B90.8~B90.9~N74.0~N74.1" 
* #A70 ^property[2].code = #hints 
* #A70 ^property[2].valueString = "Beachte: Fieber A03~ Husten R05 Hinweise: " 
* #A71 "Masern"
* #A71 ^definition = Inklusive: Spätfolgen von Masern Exklusive:  Kriterien: Prodromalstadium mit Bindehautentzündung, Fieber und Husten, plus weiße Flecken auf rotem Grund in der Wangenschleimhaut (Koplick-Flecken), oder konfluierender makulopapulöser Ausschlag, der sich über Gesicht und Körper ausbreitet, oder ein atypisches Exanthem bei einer teilweise immunisierten Person während einer akuten Masernerkrankung
* #A71 ^property[0].code = #parent 
* #A71 ^property[0].valueCode = #A 
* #A71 ^property[1].code = #Relationships 
* #A71 ^property[1].valueString = "ICD-10 2017:B05.0~B05.1~B05.2~B05.3~B05.4~B05.8~B05.9" 
* #A71 ^property[2].code = #hints 
* #A71 ^property[2].valueString = "Beachte: Fieber A03~ anderes virales Exanthem A76~ generalisierter Ausschlag S07 Hinweise: " 
* #A72 "Windpocken"
* #A72 ^definition = Inklusive: Spätfolgen von Windpocken Exklusive: Herpes Zoster A70 Kriterien: Ein vesikuläres Exanthem, das in aufeinanderfolgenden Herden auftritt, wobei die Läsion sich rasch von Papeln zu Bläschen und schließlich zu Schorf entwickelt
* #A72 ^property[0].code = #parent 
* #A72 ^property[0].valueCode = #A 
* #A72 ^property[1].code = #Relationships 
* #A72 ^property[1].valueString = "ICD-10 2017:B01.0~B01.1~B01.2~B01.8~B01.9" 
* #A72 ^property[2].code = #hints 
* #A72 ^property[2].valueString = "Beachte: Fieber A03~ anderes virales Exanthem A76~ generalisierter Ausschlag S07 Hinweise: " 
* #A73 "Malaria"
* #A73 ^definition = Inklusive: Spätfolgen von Malaria Exklusive:  Kriterien: Intermittierendes Fieber mit Kälteschauern und Schüttelfrost bei einem Bewohner eines Malariagebietes oder einer Person, die vor kurzem ein Malariagebiet besucht hat, oder Nachweis von Formen des Malariaerregers im peripheren Blut
* #A73 ^property[0].code = #parent 
* #A73 ^property[0].valueCode = #A 
* #A73 ^property[1].code = #Relationships 
* #A73 ^property[1].valueString = "ICD-10 2017:B50.0~B50.8~B50.9~B51.0~B51.8~B51.9~B52.0~B52.8~B52.9~B53.0~B53.1~B53.8~B54" 
* #A73 ^property[2].code = #hints 
* #A73 ^property[2].valueString = "Beachte: Fieber A03 Hinweise: " 
* #A74 "Röteln"
* #A74 ^definition = Inklusive: Spätfolgen von Röteln~ kongenitale Röteln Exklusive: Dreitagefieber (roseola infantum) A76~ kongenitale Röteln A94 Kriterien: Akutes Exanthem mit vergrößerten Lymphknoten, meist subokzipital sowie retroaurikulär, mit fleckigem Ausschlag im Gesicht, der sich auf den Rumpf und die angrenzenden Teile der Gliedmaßen ausdehnt, oder serologischer Nachweis einer Rötelninfektion
* #A74 ^property[0].code = #parent 
* #A74 ^property[0].valueCode = #A 
* #A74 ^property[1].code = #Relationships 
* #A74 ^property[1].valueString = "ICD-10 2017:B06.0~B06.8~B06.9" 
* #A74 ^property[2].code = #hints 
* #A74 ^property[2].valueString = "Beachte: Fieber A03~ anderes virales Exanthem A76~ generalisierter Ausschlag S07 Hinweise: " 
* #A75 "Infektiöse Mononukleose"
* #A75 ^definition = Inklusive: Drüsenfieber~ M. Pfeiffer Exklusive:  Kriterien: Mandel- oder Rachenentzündung, wobei die betroffenen Lymphknoten nicht auf den vorderen Zervikalbereich beschränkt bleiben und entweder atypische Lymphozyten im Blutausstrich oder Milzvergrößerung, oder abnorme heterophile Antikörpertiter oder Epstein-Barr Virustiter
* #A75 ^property[0].code = #parent 
* #A75 ^property[0].valueCode = #A 
* #A75 ^property[1].code = #Relationships 
* #A75 ^property[1].valueString = "ICD-10 2017:B27.0~B27.1~B27.8~B27.9" 
* #A75 ^property[2].code = #hints 
* #A75 ^property[2].valueString = "Beachte: Fieber A03~ vergrößerte Lymphknoten B02~ akute Infektion der oberen Atemwege R74 Hinweise: " 
* #A76 "Virales Exanthem, anderes"
* #A76 ^definition = Inklusive: Fieber mit Hautausschlag~ Exklusive: Erkrankungen~ die unter A71~ A72~ A74~ A75 aufgeführt sind Kriterien:
* #A76 ^property[0].code = #parent 
* #A76 ^property[0].valueCode = #A 
* #A76 ^property[1].code = #Relationships 
* #A76 ^property[1].valueString = "ICD-10 2017:A88.0~B03~B04~B08.0~B08.2~B08.3~B08.4~B08.8~B09" 
* #A77 "Virale Erkrankung, andere NNB"
* #A77 ^definition = Inklusive: Adenovirus~ Kuhpocken~ Ringelröteln~ Exanthema subitum Exklusive: Virus-Exantheme A76~ Influenza R80 Kriterien:
* #A77 ^property[0].code = #parent 
* #A77 ^property[0].valueCode = #A 
* #A77 ^property[1].code = #Relationships 
* #A77 ^property[1].valueString = "ICD-10 2017:A82.0~A82.1~A82.9~A92.0~A92.1~A92.2~A92.3~A92.4~A92.8~A92.9~A93.0~A93.1~A93.2~A93.8~A94~A95.0~A95.1~A95.9~A96.0~A96.1~A96.2~A96.8~A96.9~A97.0~A97.1~A97.2~A97.9~A98.0~A98.1~A98.2~A98.3~A98.4~A98.5~A98.8~A99~B00.7~B25.0~B25.1~B25.2~B25.8~B25.9~B33.0~B33.1~B33.3~B33.8~B34.0~B34.1~B34.2~B34.3~B34.4~B34.8~B34.9~B97.0~B97.1~B97.2~B97.3~B97.4~B97.5~B97.6~B97.7~B97.8" 
* #A78 "Infektiöse Erkrankung, andere NNB"
* #A78 ^definition = Inklusive: Brucellose~ Infektion an nicht spezifizierter Stelle~ Lyme Disease~ Mycoplasmen~ Q-Fieber~ Rickettsiose~ Scharlach~ sexuell übertragbare Krankheit NNB~ Soor NNB~ Toxoplasmose Exklusive: Meningokokken-Meningitis N71 Kriterien:
* #A78 ^property[0].code = #parent 
* #A78 ^property[0].valueCode = #A 
* #A78 ^property[1].code = #Relationships 
* #A78 ^property[1].valueString = "ICD-10 2017:A20.0~A20.1~A20.2~A20.3~A20.7~A20.8~A20.9~A21.0~A21.1~A21.2~A21.3~A21.7~A21.8~A21.9~A22.0~A22.1~A22.2~A22.7~A22.8~A22.9~A23.0~A23.1~A23.2~A23.3~A23.8~A23.9~A24.0~A24.1~A24.2~A24.3~A24.4~A25.0~A25.1~A25.9~A26.0~A26.7~A26.8~A26.9~A27.0~A27.8~A27.9~A28.0~A28.1~A28.2~A28.8~A28.9~A30.0~A30.1~A30.2~A30.3~A30.4~A30.5~A30.8~A30.9~A31.0~A31.1~A31.8~A31.9~A32.0~A32.7~A32.8~A32.9~A38~A39.1~A39.2~A39.3~A39.4~A39.8~A39.9~A40.0~A40.1~A40.2~A40.3~A40.8~A40.9~A41.0~A41.1~A41.2~A41.3~A41.4~A41.5~A41.8~A41.9~A42.0~A42.1~A42.2~A42.7~A42.8~A42.9~A43.0~A43.1~A43.8~A43.9~A44.0~A44.1~A44.8~A44.9~A48.0~A48.2~A48.3~A48.4~A48.8~A49.0~A49.1~A49.2~A49.3~A49.8~A49.9~A59.8~A59.9~A64~A68.0~A68.1~A68.9~A69.2~A69.8~A69.9~A70~A74.8~A74.9~A75.0~A75.1~A75.2~A75.3~A75.9~A77.0~A77.1~A77.2~A77.3~A77.8~A77.9~A78~A79.0~A79.1~A79.8~A79.9~B37.7~B37.8~B37.9~B38.0~B38.1~B38.2~B38.3~B38.4~B38.7~B38.8~B38.9~B39.0~B39.1~B39.2~B39.3~B39.4~B39.5~B39.9~B40.0~B40.1~B40.2~B40.3~B40.7~B40.8~B40.9~B41.0~B41.7~B41.8~B41.9~B42.0~B42.1~B42.7~B42.8~B42.9~B43.0~B43.1~B43.2~B43.8~B43.9~B45.0~B45.1~B45.2~B45.3~B45.7~B45.8~B45.9~B46.0~B46.1~B46.2~B46.3~B46.4~B46.5~B46.8~B46.9~B47.0~B47.1~B47.9~B48.0~B48.1~B48.2~B48.3~B48.4~B48.7~B48.8~B49~B55.0~B55.1~B55.2~B55.9~B56.0~B56.1~B56.9~B57.0~B57.1~B57.2~B57.3~B57.4~B57.5~B58.8~B58.9~B59~B60.0~B60.1~B60.2~B60.8~B64~B89~B92~B94.8~B94.9~B95.0~B95.1~B95.2~B95.3~B95.4~B95.5~B95.6~B95.7~B95.8~B96.0~B96.1~B96.2~B96.3~B96.4~B96.5~B96.6~B96.7~B96.8~B98.1~B99~R57.2~R65.0~R65.1~R65.2~R65.3~R65.9" 
* #A79 "Malignom NNB"
* #A79 ^definition = Inklusive: Sekundäre/metastatische Neubildung bei unbekannter Lokalisation des Primärtumors~ Karzinomatose Exklusive:  Kriterien: Histologischer Nachweis eine bösartigen Neubildung
* #A79 ^property[0].code = #parent 
* #A79 ^property[0].valueCode = #A 
* #A79 ^property[1].code = #Relationships 
* #A79 ^property[1].valueString = "ICD-10 2017:C38.8~C45.9~C46.7~C46.8~C46.9~C76.0~C76.1~C76.2~C76.3~C76.4~C76.5~C76.7~C76.8~C78.0~C78.1~C78.2~C78.3~C78.4~C78.5~C78.6~C78.7~C78.8~C79.0~C79.1~C79.2~C79.3~C79.4~C79.5~C79.6~C79.7~C79.8~C80.0~C80.9~C97~D09.7~D09.9" 
* #A79 ^property[2].code = #hints 
* #A79 ^property[2].valueString = "Beachte: Krankheit/Zustand unbekannter Ursache/Lokalisation A99 Hinweise: " 
* #A80 "Trauma/Verletzung NNB"
* #A80 ^definition = Inklusive: Verkehrsunfall Exklusive: Polytrauma A81~ Spätfolgen eines Traumas A82 Kriterien:
* #A80 ^property[0].code = #parent 
* #A80 ^property[0].valueCode = #A 
* #A80 ^property[1].code = #Relationships 
* #A80 ^property[1].valueString = "ICD-10 2017:S11.0~S11.1~S11.2~S11.7~S11.8~S11.9~S15.0~S15.1~S15.2~S15.3~S15.7~S15.8~S15.9~S21.0~S21.1~S21.2~S21.7~S21.8~S21.9~S25.0~S25.1~S25.2~S25.3~S25.4~S25.5~S25.7~S25.8~S25.9~S26.0~S26.8~S26.9~S27.8~S27.9~S35.0~S35.1~S35.2~S35.3~S35.4~S35.5~S35.7~S35.8~S35.9~S37.9~S38.1~S38.3~S39.0~S39.6~S39.7~S39.8~S39.9~S45.0~S45.1~S45.2~S45.3~S45.7~S45.8~S45.9~S55.0~S55.1~S55.2~S55.7~S55.8~S55.9~S65.0~S65.1~S65.2~S65.3~S65.4~S65.5~S65.7~S65.8~S65.9~S75.0~S75.1~S75.2~S75.7~S75.8~S75.9~S85.0~S85.1~S85.2~S85.3~S85.4~S85.5~S85.7~S85.8~S85.9~S95.0~S95.1~S95.2~S95.7~S95.8~S95.9~T14.5~T14.7~T14.8~T14.9~T28.4~T28.9" 
* #A81 "Multiple Traumen/Verletzungen"
* #A81 ^definition = Inklusive: Multiple innere Verletzungen NNB Exklusive:  Kriterien:
* #A81 ^property[0].code = #parent 
* #A81 ^property[0].valueCode = #A 
* #A81 ^property[1].code = #Relationships 
* #A81 ^property[1].valueString = "ICD-10 2017:S17.0~S17.8~S17.9~S18~S19.7~S19.8~S19.9~S28.0~S28.1~S29.0~S29.7~S29.8~S29.9~S31.7~S36.7~S36.8~S37.7~S37.8~S39.0~S39.6~S39.7~S39.8~S39.9~T00.0~T00.1~T00.2~T00.3~T00.6~T00.8~T00.9~T01.0~T01.1~T01.2~T01.3~T01.6~T01.8~T01.9~T02.0~T02.1~T02.2~T02.3~T02.4~T02.5~T02.6~T02.7~T02.8~T02.9~T03.0~T03.1~T03.2~T03.3~T03.4~T03.8~T03.9~T04.0~T04.1~T04.2~T04.3~T04.4~T04.7~T04.8~T04.9~T05.0~T05.1~T05.2~T05.3~T05.4~T05.5~T05.6~T05.8~T05.9~T06.5~T06.8~T07~T29.0~T29.1~T29.2~T29.3~T29.4~T29.5~T29.6~T29.7" 
* #A81 ^property[2].code = #hints 
* #A81 ^property[2].valueString = "Beachte:  Hinweise: In dieser Klassifikation beziehen sich die Begriffe 'allgemein' oder 'mehrfach' auf drei oder mehr Körperregionen, Systeme oder Organe. Beschwerden, die nur ein oder zwei Regionen betreffen, sollten den betreffenden Regionen zugeordnet werden." 
* #A82 "Sekundärfolgen eines Traumas"
* #A82 ^definition = Inklusive: Deformität/Narben als Resultat einer vorangegangenen Verletzung~ alte Amputation Exklusive: Auswirkungen auf bestimmte Körperorgane (nach dem zugeordneten Kapitel kodieren)~ psychologische Auswirkungen P02~ akute Stressreaktion P02~ posttraumatischer Stress P02~ Wundinfektion S11~ Hautnarbe S99 Kriterien:
* #A82 ^property[0].code = #parent 
* #A82 ^property[0].valueCode = #A 
* #A82 ^property[1].code = #Relationships 
* #A82 ^property[1].valueString = "ICD-10 2017:T79.0~T79.1~T79.2~T79.4~T79.5~T79.7~T79.8~T79.9~T90.0~T90.1~T90.2~T90.3~T90.4~T90.5~T90.8~T90.9~T91.0~T91.1~T91.2~T91.3~T91.4~T91.5~T91.8~T91.9~T92.0~T92.1~T92.2~T92.3~T92.4~T92.5~T92.6~T92.8~T92.9~T93.0~T93.1~T93.2~T93.3~T93.4~T93.5~T93.6~T93.8~T93.9~T94.0~T94.1~T95.0~T95.1~T95.2~T95.3~T95.4~T95.8~T95.9~T96~T97~T98.0~T98.1~T98.2~T98.3" 
* #A84 "Vergiftung durch medizinische Substanz"
* #A84 ^definition = Inklusive: Toxischer Effekt bei Überdosierung eines Medikaments Exklusive: Medikamentemissbrauch P18~ Selbstmordversuch P77~ Insulinschock T87 Kriterien: Vergiftung/Schädigung durch akzidentelle oder absichtliche Überdosis einer Substanz, die in üblicher Dosierung heilende Wirkung hat
* #A84 ^property[0].code = #parent 
* #A84 ^property[0].valueCode = #A 
* #A84 ^property[1].code = #Relationships 
* #A84 ^property[1].valueString = "ICD-10 2017:T36.0~T36.1~T36.2~T36.3~T36.4~T36.5~T36.6~T36.7~T36.8~T36.9~T37.0~T37.1~T37.2~T37.3~T37.4~T37.5~T37.8~T37.9~T38.0~T38.1~T38.2~T38.3~T38.4~T38.5~T38.6~T38.7~T38.8~T38.9~T39.0~T39.1~T39.2~T39.3~T39.4~T39.8~T39.9~T40.0~T40.1~T40.2~T40.3~T40.4~T40.5~T40.6~T40.7~T40.8~T40.9~T41.0~T41.1~T41.2~T41.3~T41.4~T41.5~T42.0~T42.1~T42.2~T42.3~T42.4~T42.5~T42.6~T42.7~T42.8~T43.0~T43.1~T43.2~T43.3~T43.4~T43.5~T43.6~T43.8~T43.9~T44.0~T44.1~T44.2~T44.3~T44.4~T44.5~T44.6~T44.7~T44.8~T44.9~T45.0~T45.1~T45.2~T45.3~T45.4~T45.5~T45.6~T45.7~T45.8~T45.9~T46.0~T46.1~T46.2~T46.3~T46.4~T46.5~T46.6~T46.7~T46.8~T46.9~T47.0~T47.1~T47.2~T47.3~T47.4~T47.5~T47.6~T47.7~T47.8~T47.9~T48.0~T48.1~T48.2~T48.3~T48.4~T48.5~T48.6~T48.7~T49.0~T49.1~T49.2~T49.3~T49.4~T49.5~T49.6~T49.7~T49.8~T49.9~T50.8~T50.9" 
* #A85 "Unerwünschte Wirkung eines Medikaments"
* #A85 ^definition = Inklusive: Nebenwirkung/Allergie/Anaphylaxie bei Medikation in korrekter Dosierung Exklusive: Medikamentenvergiftung A84~ Reaktion auf Impfung und Bluttransfusion A87~ Parkinsonismus N87~ Medikamentenmissbrauch P18~ Kontaktdermatitis S88~ Insulinschock T87~ Analgetikanephropathie U88 Kriterien: Symptom/Beschwerden, die eher einem korrektem Medikamentengebrauch, als einer Krankheit oder Verletzung zugeordnet werden
* #A85 ^property[0].code = #parent 
* #A85 ^property[0].valueCode = #A 
* #A85 ^property[1].code = #Relationships 
* #A85 ^property[1].valueString = "ICD-10 2017:D61.1~D64.2~G44.4~I95.2~J70.2~J70.3~J70.4~L27.0~L27.1~P93~R50.2~T88.6~T88.7" 
* #A85 ^property[2].code = #hints 
* #A85 ^property[2].valueString = "Beachte:  Hinweise: Kann auch die Art der unerwünschten Wirkung bezeichnen" 
* #A86 "Toxischer Effekt einer nichtmedizinischen Substanz"
* #A86 ^definition = Inklusive: Generalisierter/lokaler toxischer Effekt von Kohlenmonoxid~ industrielle Materialien~ Blei~ giftigen Tieren/Insekten/Pflanzen/Schlangen Exklusive: durch Medikamente A84~ 85~ durch Alkohol~ Tabak~ Drogen P15 bis P19~ toxische Wirkung in den Atemwegen R99~ äußerliche chemische Verbrennungen S14~ nicht toxische Bisse/Stiche S12~ S13~ Kontaktdermatitis S88 Kriterien:
* #A86 ^property[0].code = #parent 
* #A86 ^property[0].valueCode = #A 
* #A86 ^property[1].code = #Relationships 
* #A86 ^property[1].valueString = "ICD-10 2017:D61.2~D64.2~T51.0~T51.1~T51.2~T51.3~T51.8~T51.9~T52.0~T52.1~T52.2~T52.3~T52.4~T52.8~T52.9~T53.0~T53.1~T53.2~T53.3~T53.4~T53.5~T53.6~T53.7~T53.9~T54.0~T54.1~T54.2~T54.3~T54.9~T55~T56.0~T56.1~T56.2~T56.3~T56.4~T56.5~T56.6~T56.7~T56.8~T56.9~T57.0~T57.1~T57.2~T57.3~T57.8~T57.9~T58~T59.0~T59.1~T59.2~T59.3~T59.4~T59.5~T59.6~T59.7~T59.8~T59.9~T60.0~T60.1~T60.2~T60.3~T60.4~T60.8~T60.9~T61.0~T61.1~T61.2~T61.8~T61.9~T62.0~T62.1~T62.2~T62.8~T62.9~T63.0~T63.1~T63.2~T63.3~T63.4~T63.5~T63.6~T63.8~T63.9~T64~T65.0~T65.1~T65.2~T65.3~T65.4~T65.5~T65.6~T65.8~T65.9" 
* #A87 "Komplikation einer medizinischen Behandlung"
* #A87 ^definition = Inklusive: Anästhetischer Schock~ Immunisierung/Transfusionsreaktion~ postoperative Infektion/Hämorrhagie/Wundheilungsstörung~ Probleme wegen diagnostischer/therapeutischer Bestrahlung Exklusive: Überdosis Medikamente A84~ Schädliche Wirkungen von Medikamenten A85~ Sonstige Hernien im Abdomen D91~ Hypoglykämie T87 Kriterien: eine unerwartete Störung aufgrund einer chirurgischen/medikamentösen/radiologischen oder anderen medizinischen Behandlung
* #A87 ^property[0].code = #parent 
* #A87 ^property[0].valueCode = #A 
* #A87 ^property[1].code = #Relationships 
* #A87 ^property[1].valueString = "ICD-10 2017:E89.0~E89.1~E89.2~E89.3~E89.4~E89.5~E89.6~E89.8~E89.9~G97.0~G97.1~G97.2~G97.8~G97.9~H59.0~H59.8~H59.9~H95.0~H95.1~H95.8~H95.9~I97.0~I97.1~I97.2~I97.8~I97.9~J70.0~J70.1~J70.8~J70.9~J95.0~J95.1~J95.2~J95.3~J95.4~J95.5~J95.8~J95.9~K43.2~K43.3~K43.4~K43.5~K52.0~K91.0~K91.3~M96.0~M96.1~M96.2~M96.3~M96.4~M96.5~M96.6~M96.8~M96.9~N98.0~N98.1~N98.2~N98.3~N98.8~N98.9~N99.0~N99.1~N99.2~N99.3~N99.4~N99.5~N99.8~N99.9~O29.0~O29.1~O29.2~O29.3~O29.4~O29.5~O29.6~O29.8~O29.9~O74.0~O74.1~O74.2~O74.3~O74.4~O74.5~O74.6~O74.7~O74.8~O74.9~O86.0~O89.0~O89.1~O89.2~O89.3~O89.4~O89.5~O89.6~O89.8~O89.9~O90.0~O90.1~O90.2~T80.0~T80.1~T80.2~T80.3~T80.4~T80.5~T80.6~T80.8~T80.9~T81.0~T81.1~T81.2~T81.3~T81.4~T81.5~T81.6~T81.7~T81.8~T81.9~T86.0~T86.1~T86.2~T86.3~T86.4~T86.8~T86.9~T87.0~T87.1~T87.2~T87.3~T87.4~T87.5~T87.6~T88.0~T88.1~T88.2~T88.3~T88.4~T88.5~T88.8~T88.9" 
* #A88 "Schädliche Folge physikalischer Einwirkung"
* #A88 ^definition = Inklusive: Folge von Kälte/Hitze/Blitzschlag/Bewegung/Druck/Strahlung~ Frostbeule~ Ertrinken~ Jetlag Exklusive: Auswirkungen von ärztlich verordneter Bestrahlung A87~ Schneeblindheit F79~ Auswirkungen von Alkohol P15~ P16~ Auswirkungen von Tabak P17~ Verbrennungen durch Strahlung S14~ Sonnenbrand S80 Kriterien:
* #A88 ^property[0].code = #parent 
* #A88 ^property[0].valueCode = #A 
* #A88 ^property[1].code = #Relationships 
* #A88 ^property[1].valueString = "ICD-10 2017:T33.0~T33.1~T33.2~T33.3~T33.4~T33.5~T33.6~T33.7~T33.8~T33.9~T34.0~T34.1~T34.2~T34.3~T34.4~T34.5~T34.6~T34.7~T34.8~T34.9~T35.0~T35.1~T35.2~T35.3~T35.4~T35.5~T35.6~T35.7~T66~T67.0~T67.1~T67.2~T67.3~T67.4~T67.5~T67.6~T67.7~T67.8~T67.9~T68~T69.0~T69.1~T69.8~T69.9~T70.2~T70.3~T70.4~T70.8~T70.9~T71~T73.0~T73.1~T73.2~T73.3~T73.8~T73.9~T75.0~T75.1~T75.2~T75.3~T75.4~T75.8~T78.8~T78.9" 
* #A89 "Auswirkung/Komplikationen einer Prothesenversorgung"
* #A89 ^definition = Inklusive: Unbehagen/Behinderung/Schmerz/Funktionseinschränkung als Ergebnis der Anlage/des Tragens eines Heil-/Hilfsmittels: Katheter~ Kolostoma~ Gastrostoma~ Herzklappenersatz~ Gelenkersatz~ Organtransplantat~ Schrittmacher Exklusive: Zahnersatz~ falsche Zähne D19 Kriterien:
* #A89 ^property[0].code = #parent 
* #A89 ^property[0].valueCode = #A 
* #A89 ^property[1].code = #Relationships 
* #A89 ^property[1].valueString = "ICD-10 2017:K91.4~T82.0~T82.1~T82.2~T82.3~T82.4~T82.5~T82.6~T82.7~T82.8~T82.9~T83.0~T83.1~T83.2~T83.3~T83.4~T83.5~T83.6~T83.8~T83.9~T84.0~T84.1~T84.2~T84.3~T84.4~T84.5~T84.6~T84.7~T84.8~T84.9~T85.0~T85.1~T85.2~T85.3~T85.4~T85.5~T85.6~T85.7~T85.8~T85.9~Z43.0~Z43.1~Z43.2~Z43.3~Z43.4~Z43.5~Z43.6~Z43.7~Z43.8~Z43.9~Z44.0~Z44.1~Z44.2~Z44.3~Z44.8~Z44.9~Z45.0~Z45.1~Z45.2~Z45.3~Z45.8~Z45.9~Z46.1~Z46.2~Z46.3~Z46.4~Z46.5~Z46.6~Z46.7~Z46.8~Z46.9~Z93.0~Z93.1~Z93.2~Z93.3~Z93.4~Z93.5~Z93.6~Z93.8~Z93.9~Z94.0~Z94.1~Z94.2~Z94.3~Z94.4~Z94.5~Z94.6~Z94.7~Z94.8~Z94.9~Z95.0~Z95.1~Z95.2~Z95.3~Z95.4~Z95.5~Z95.8~Z95.9~Z96.0~Z96.1~Z96.2~Z96.3~Z96.4~Z96.5~Z96.6~Z96.7~Z96.8~Z96.9~Z97.0~Z97.1~Z97.2~Z97.3~Z97.4~Z97.5~Z97.8" 
* #A90 "Angeborene Anomalie/NNB/mehrfach"
* #A90 ^definition = Inklusive: Trisomie 21~ Marfan Syndrom~ andere chromosomale Abweichung~ Neurofibromatose Exklusive: Anomalien~ die einem bestimmten Organ zugeordnet sind (nach dem entsprechenden Kapitel kodieren)~ kongenitale Röteln A74 Kriterien:
* #A90 ^property[0].code = #parent 
* #A90 ^property[0].valueCode = #A 
* #A90 ^property[1].code = #Relationships 
* #A90 ^property[1].valueString = "ICD-10 2017:Q85.0~Q85.1~Q85.8~Q85.9~Q86.0~Q86.1~Q86.2~Q86.8~Q87.0~Q87.1~Q87.2~Q87.3~Q87.4~Q87.5~Q87.8~Q89.3~Q89.4~Q89.7~Q89.9~Q90.0~Q90.1~Q90.2~Q90.9~Q91.0~Q91.1~Q91.2~Q91.3~Q91.4~Q91.5~Q91.6~Q91.7~Q92.0~Q92.1~Q92.2~Q92.3~Q92.4~Q92.5~Q92.6~Q92.7~Q92.8~Q92.9~Q93.0~Q93.1~Q93.2~Q93.3~Q93.4~Q93.5~Q93.6~Q93.7~Q93.8~Q93.9~Q95.0~Q95.1~Q95.2~Q95.3~Q95.4~Q95.5~Q95.8~Q95.9~Q96.0~Q96.1~Q96.2~Q96.3~Q96.4~Q96.8~Q96.9~Q97.0~Q97.1~Q97.2~Q97.3~Q97.8~Q97.9~Q98.0~Q98.1~Q98.2~Q98.3~Q98.4~Q98.5~Q98.6~Q98.7~Q98.8~Q98.9~Q99.0~Q99.1~Q99.2~Q99.8~Q99.9" 
* #A91 "Pathologischer Befund einer Untersuchung/NNB"
* #A91 ^definition = Inklusive: Abnorme unklare Pathologie/Bildgebung~ Elektrolytverschiebung~ Hyperglykämie Exklusive: Abnormer Befund der weißen Blutzellen B84~ sonstige hämatologische Anomalien B99~ Vitaminmangel/Mangelernährung T91~ abnormer Urintest U98~ abnormer Papanicolau-Abstrich X86 Kriterien: unerwartetes Ergebnis, das nicht einer bekannten Krankheit zugeordnet wird
* #A91 ^property[0].code = #parent 
* #A91 ^property[0].valueCode = #A 
* #A91 ^property[1].code = #Relationships 
* #A91 ^property[1].valueString = "ICD-10 2017:R73.0~R73.9~R74.0~R74.8~R74.9~R76.0~R76.1~R76.2~R76.8~R76.9~R77.0~R77.1~R77.2~R77.8~R77.9~R78.0~R78.1~R78.2~R78.3~R78.4~R78.5~R78.6~R78.7~R78.8~R78.9~R79.0~R79.8~R79.9~R83.0~R83.1~R83.2~R83.3~R83.4~R83.5~R83.6~R83.7~R83.8~R83.9~R84.0~R84.1~R84.2~R84.3~R84.4~R84.5~R84.6~R84.7~R84.8~R84.9~R85.0~R85.1~R85.2~R85.3~R85.4~R85.5~R85.6~R85.7~R85.8~R85.9~R86.0~R86.1~R86.2~R86.3~R86.4~R86.5~R86.6~R86.7~R86.8~R86.9~R87.0~R87.1~R87.2~R87.3~R87.4~R87.5~R87.7~R87.8~R87.9~R89.0~R89.1~R89.2~R89.3~R89.4~R89.5~R89.6~R89.7~R89.8~R89.9~R90.0~R90.8~R91~R92~R93.0~R93.1~R93.2~R93.3~R93.4~R93.5~R93.6~R93.7~R93.8~R94.0~R94.1~R94.2~R94.3~R94.4~R94.5~R94.6~R94.7~R94.8" 
* #A92 "Allergie/allergische Reaktion NNB"
* #A92 ^definition = Inklusive: Allergisches Ödem~ anaphylaktischer Schock~ angioneurotisches Ödem~ Lebensmittelallergie Exklusive: Nesselsucht S98~ als Folge von Medikamenten A85~ allergische Rhinitis R97 Kriterien:
* #A92 ^property[0].code = #parent 
* #A92 ^property[0].valueCode = #A 
* #A92 ^property[1].code = #Relationships 
* #A92 ^property[1].valueString = "ICD-10 2017:T78.0~T78.1~T78.2~T78.3~T78.4" 
* #A93 "Unreifes Neugeborenes"
* #A93 ^definition = Inklusive:  Exklusive:  Kriterien: Lebendgeburt unter einem Gestationsalter von 37 Wochen
* #A93 ^property[0].code = #parent 
* #A93 ^property[0].valueCode = #A 
* #A93 ^property[1].code = #Relationships 
* #A93 ^property[1].valueString = "ICD-10 2017:P07.0~P07.1~P07.2~P07.3" 
* #A94 "Perinatale Erkrankung, andere"
* #A94 ^definition = Inklusive:  Exklusive: angeborene Erkrankungen A90~ Frühgeburt A93~ Gedeihstörung T10 Kriterien: Krankheit, die intrauterin oder innerhalb von 7 Tagen nach Geburt  entsteht
* #A94 ^property[0].code = #parent 
* #A94 ^property[0].valueCode = #A 
* #A94 ^property[1].code = #Relationships 
* #A94 ^property[1].valueString = "ICD-10 2017:P00.0~P00.1~P00.2~P00.3~P00.4~P00.5~P00.6~P00.7~P00.8~P00.9~P01.0~P01.1~P01.2~P01.3~P01.4~P01.5~P01.6~P01.7~P01.8~P01.9~P02.0~P02.1~P02.2~P02.3~P02.4~P02.5~P02.6~P02.7~P02.8~P02.9~P03.0~P03.1~P03.2~P03.3~P03.4~P03.5~P03.6~P03.8~P03.9~P04.0~P04.1~P04.2~P04.3~P04.4~P04.5~P04.6~P04.8~P04.9~P05.0~P05.1~P05.2~P05.9~P08.0~P08.1~P08.2~P10.0~P10.1~P10.2~P10.3~P10.4~P10.8~P10.9~P11.0~P11.1~P11.2~P11.3~P11.4~P11.5~P11.9~P12.0~P12.1~P12.2~P12.3~P12.4~P12.8~P12.9~P13.0~P13.1~P13.2~P13.3~P13.4~P13.8~P13.9~P14.0~P14.1~P14.2~P14.3~P14.8~P14.9~P15.0~P15.1~P15.2~P15.3~P15.4~P15.5~P15.6~P15.8~P15.9~P20.0~P20.1~P20.9~P21.0~P21.1~P21.9~P22.0~P22.1~P22.8~P22.9~P23.0~P23.1~P23.2~P23.3~P23.4~P23.5~P23.6~P23.8~P23.9~P24.0~P24.1~P24.2~P24.3~P24.8~P24.9~P25.0~P25.1~P25.2~P25.3~P25.8~P26.0~P26.1~P26.8~P26.9~P27.0~P27.1~P27.8~P27.9~P28.0~P28.1~P28.2~P28.3~P28.4~P28.5~P28.8~P28.9~P29.0~P29.1~P29.2~P29.3~P29.4~P29.8~P29.9~P35.0~P35.1~P35.2~P35.3~P35.8~P35.9~P36.0~P36.1~P36.2~P36.3~P36.4~P36.5~P36.8~P36.9~P37.0~P37.1~P37.2~P37.3~P37.4~P37.5~P37.8~P37.9~P38~P39.0~P39.1~P39.2~P39.3~P39.4~P39.8~P39.9~P50.0~P50.1~P50.2~P50.3~P50.4~P50.5~P50.8~P50.9~P51.0~P51.8~P51.9~P52.0~P52.1~P52.2~P52.3~P52.4~P52.5~P52.6~P52.8~P52.9~P53~P54.0~P54.1~P54.2~P54.3~P54.4~P54.5~P54.6~P54.8~P54.9~P55.0~P55.1~P55.8~P55.9~P56.0~P56.9~P57.0~P57.8~P57.9~P58.0~P58.1~P58.2~P58.3~P58.4~P58.5~P58.8~P58.9~P59.0~P59.1~P59.2~P59.3~P59.8~P59.9~P60~P61.0~P61.1~P61.2~P61.3~P61.4~P61.5~P61.6~P61.8~P61.9~P70.0~P70.1~P70.2~P70.3~P70.4~P70.8~P70.9~P71.0~P71.1~P71.2~P71.3~P71.4~P71.8~P71.9~P72.0~P72.1~P72.2~P72.8~P72.9~P74.0~P74.1~P74.2~P74.3~P74.4~P74.5~P74.8~P74.9~P75~P76.0~P76.1~P76.2~P76.8~P76.9~P77~P78.0~P78.1~P78.2~P78.3~P78.8~P78.9~P80.0~P80.8~P80.9~P81.0~P81.8~P81.9~P83.0~P83.1~P83.2~P83.3~P83.4~P83.5~P83.6~P83.8~P83.9~P90~P91.0~P91.1~P91.2~P91.3~P91.4~P91.5~P91.6~P91.7~P91.8~P91.9~P92.0~P92.1~P92.2~P92.3~P92.4~P92.5~P92.8~P92.9~P93~P94.0~P94.1~P94.2~P94.8~P94.9~P96.0~P96.1~P96.2~P96.3~P96.4~P96.5~P96.8~P96.9" 
* #A95 "Perinataler Tod"
* #A95 ^definition = Inklusive:  Exklusive:  Kriterien: Intrauteriner Tod oder Tod innerhalb von 7 Tagen nach Geburt
* #A95 ^property[0].code = #parent 
* #A95 ^property[0].valueCode = #A 
* #A95 ^property[1].code = #Relationships 
* #A95 ^property[1].valueString = "ICD-10 2017:P95~R95.0~R95.9" 
* #A96 "Tod"
* #A96 ^definition = Inklusive:  Exklusive: Perinataler Tod A95 Kriterien:
* #A96 ^property[0].code = #parent 
* #A96 ^property[0].valueCode = #A 
* #A96 ^property[1].code = #Relationships 
* #A96 ^property[1].valueString = "ICD-10 2017:O95~O97.0~O97.1~O97.2~R95~R95.0~R95.9~R96.0~R96.1~R98~R99" 
* #A97 "Keine Erkrankung"
* #A97 ^definition = Inklusive: bei Beratung mit keiner Erkrankung beschäftigt Exklusive: Gesundheitsförderung~ präventive Medizin A98 Kriterien:
* #A97 ^property[0].code = #parent 
* #A97 ^property[0].valueCode = #A 
* #A97 ^property[1].code = #Relationships 
* #A97 ^property[1].valueString = "ICD-10 2017:Z00.0~Z00.1~Z00.2~Z00.3~Z00.4~Z00.5~Z00.6~Z00.8~Z02.0~Z02.1~Z02.2~Z02.3~Z02.4~Z02.5~Z02.6~Z02.7~Z02.8~Z02.9" 
* #A97 ^property[2].code = #hints 
* #A97 ^property[2].valueString = "Beachte:  Hinweise: Manchmal hat der Patient ein Anliegen, das der Hausarzt nicht einfach als Diagnose im Rahmen der Allgemeinmedizin interpretieren kann. In diesen Fällen wird der Arzt den Code A97 verwenden, womit er zum Ausdruck bringt, dass er auf das Anliegen des Patienten nicht auf professionelle Weise reagieren kann, außer durch Klarstellung dieser Tatsache." 
* #A98 "Gesundheitsförderung/präventive Medizin"
* #A98 ^definition = Inklusive: Medizinische Maßnahme/Beratung mit primär/sekundär präventiver Absicht~ einschließlich genetischer Beratung Exklusive: Keine Erkrankung A97 Kriterien:
* #A98 ^property[0].code = #parent 
* #A98 ^property[0].valueCode = #A 
* #A98 ^property[1].code = #Relationships 
* #A98 ^property[1].valueString = "ICD-10 2017:Z01.0~Z01.1~Z01.2~Z01.3~Z01.4~Z01.5~Z01.6~Z01.7~Z01.8~Z01.9~Z10.0~Z10.1~Z10.2~Z10.3~Z10.8~Z11.0~Z11.1~Z11.2~Z11.3~Z11.4~Z11.5~Z11.6~Z11.8~Z11.9~Z12.0~Z12.1~Z12.2~Z12.3~Z12.4~Z12.5~Z12.6~Z12.8~Z12.9~Z13.0~Z13.1~Z13.2~Z13.3~Z13.4~Z13.5~Z13.6~Z13.7~Z13.8~Z13.9~Z23.0~Z23.1~Z23.2~Z23.3~Z23.4~Z23.5~Z23.6~Z23.7~Z23.8~Z24.0~Z24.1~Z24.2~Z24.3~Z24.4~Z24.5~Z24.6~Z25.0~Z25.1~Z25.8~Z26.0~Z26.8~Z26.9~Z27.0~Z27.1~Z27.2~Z27.3~Z27.4~Z27.8~Z27.9~Z29.0~Z29.1~Z29.2~Z29.8~Z29.9~Z31.5~Z40.0~Z40.8~Z40.9~Z70.0~Z70.1~Z70.2~Z70.3~Z70.8~Z70.9" 
* #A99 "Erkrankung/Zustand ohne bekannte Ursache/Lokalisation"
* #A99 ^definition = Inklusive: Krankheitsträger NNB~ Überwachung eines fortbestehenden Problems NNB Exklusive:  Kriterien:
* #A99 ^property[0].code = #parent 
* #A99 ^property[0].valueCode = #A 
* #A99 ^property[1].code = #Relationships 
* #A99 ^property[1].valueString = "ICD-10 2017:D15.7~D15.9~D19.7~D19.9~D36.7~D36.9~D48.9~R57.8~R57.9~R69~Z03.0~Z03.1~Z03.2~Z03.3~Z03.4~Z03.5~Z03.6~Z03.8~Z03.9~Z04.0~Z04.1~Z04.2~Z04.3~Z04.4~Z04.5~Z04.6~Z04.8~Z04.9~Z08.0~Z08.1~Z08.2~Z08.7~Z08.8~Z08.9~Z09.0~Z09.1~Z09.2~Z09.3~Z09.4~Z09.7~Z09.8~Z09.9~Z22.0~Z22.1~Z22.2~Z22.3~Z22.4~Z22.6~Z22.8~Z22.9~Z41.0~Z41.1~Z41.3~Z41.8~Z41.9~Z42.0~Z42.1~Z42.2~Z42.3~Z42.4~Z42.8~Z42.9~Z47.0~Z47.8~Z47.9~Z48.0~Z48.8~Z48.9~Z49.0~Z49.1~Z49.2~Z50.0~Z50.1~Z50.2~Z50.3~Z50.4~Z50.5~Z50.6~Z50.7~Z50.8~Z50.9~Z51.0~Z51.1~Z51.2~Z51.3~Z51.4~Z51.5~Z51.6~Z51.8~Z51.9~Z52.0~Z52.1~Z52.2~Z52.3~Z52.4~Z52.5~Z52.6~Z52.7~Z52.8~Z52.9~Z53.0~Z53.1~Z53.2~Z53.8~Z53.9~Z54.0~Z54.1~Z54.2~Z54.3~Z54.4~Z54.7~Z54.8~Z54.9~Z71.0~Z71.2~Z71.3~Z71.4~Z71.5~Z71.6~Z71.7~Z71.8~Z71.9~Z76.0~Z76.1~Z76.2~Z76.3~Z76.4~Z76.8~Z76.9~Z90.0~Z90.8~Z98.8" 
* #B "Blut, blutbildende Organe, Immunsystem"
* #B ^property[0].code = #child 
* #B ^property[0].valueCode = #B02 
* #B ^property[1].code = #child 
* #B ^property[1].valueCode = #B04 
* #B ^property[2].code = #child 
* #B ^property[2].valueCode = #B25 
* #B ^property[3].code = #child 
* #B ^property[3].valueCode = #B26 
* #B ^property[4].code = #child 
* #B ^property[4].valueCode = #B27 
* #B ^property[5].code = #child 
* #B ^property[5].valueCode = #B28 
* #B ^property[6].code = #child 
* #B ^property[6].valueCode = #B29 
* #B ^property[7].code = #child 
* #B ^property[7].valueCode = #B30 
* #B ^property[8].code = #child 
* #B ^property[8].valueCode = #B31 
* #B ^property[9].code = #child 
* #B ^property[9].valueCode = #B32 
* #B ^property[10].code = #child 
* #B ^property[10].valueCode = #B33 
* #B ^property[11].code = #child 
* #B ^property[11].valueCode = #B34 
* #B ^property[12].code = #child 
* #B ^property[12].valueCode = #B35 
* #B ^property[13].code = #child 
* #B ^property[13].valueCode = #B36 
* #B ^property[14].code = #child 
* #B ^property[14].valueCode = #B37 
* #B ^property[15].code = #child 
* #B ^property[15].valueCode = #B38 
* #B ^property[16].code = #child 
* #B ^property[16].valueCode = #B39 
* #B ^property[17].code = #child 
* #B ^property[17].valueCode = #B40 
* #B ^property[18].code = #child 
* #B ^property[18].valueCode = #B41 
* #B ^property[19].code = #child 
* #B ^property[19].valueCode = #B42 
* #B ^property[20].code = #child 
* #B ^property[20].valueCode = #B43 
* #B ^property[21].code = #child 
* #B ^property[21].valueCode = #B44 
* #B ^property[22].code = #child 
* #B ^property[22].valueCode = #B45 
* #B ^property[23].code = #child 
* #B ^property[23].valueCode = #B46 
* #B ^property[24].code = #child 
* #B ^property[24].valueCode = #B47 
* #B ^property[25].code = #child 
* #B ^property[25].valueCode = #B48 
* #B ^property[26].code = #child 
* #B ^property[26].valueCode = #B49 
* #B ^property[27].code = #child 
* #B ^property[27].valueCode = #B50 
* #B ^property[28].code = #child 
* #B ^property[28].valueCode = #B51 
* #B ^property[29].code = #child 
* #B ^property[29].valueCode = #B52 
* #B ^property[30].code = #child 
* #B ^property[30].valueCode = #B53 
* #B ^property[31].code = #child 
* #B ^property[31].valueCode = #B58 
* #B ^property[32].code = #child 
* #B ^property[32].valueCode = #B59 
* #B ^property[33].code = #child 
* #B ^property[33].valueCode = #B60 
* #B ^property[34].code = #child 
* #B ^property[34].valueCode = #B61 
* #B ^property[35].code = #child 
* #B ^property[35].valueCode = #B62 
* #B ^property[36].code = #child 
* #B ^property[36].valueCode = #B63 
* #B ^property[37].code = #child 
* #B ^property[37].valueCode = #B64 
* #B ^property[38].code = #child 
* #B ^property[38].valueCode = #B65 
* #B ^property[39].code = #child 
* #B ^property[39].valueCode = #B66 
* #B ^property[40].code = #child 
* #B ^property[40].valueCode = #B67 
* #B ^property[41].code = #child 
* #B ^property[41].valueCode = #B68 
* #B ^property[42].code = #child 
* #B ^property[42].valueCode = #B69 
* #B ^property[43].code = #child 
* #B ^property[43].valueCode = #B70 
* #B ^property[44].code = #child 
* #B ^property[44].valueCode = #B71 
* #B ^property[45].code = #child 
* #B ^property[45].valueCode = #B72 
* #B ^property[46].code = #child 
* #B ^property[46].valueCode = #B73 
* #B ^property[47].code = #child 
* #B ^property[47].valueCode = #B74 
* #B ^property[48].code = #child 
* #B ^property[48].valueCode = #B75 
* #B ^property[49].code = #child 
* #B ^property[49].valueCode = #B76 
* #B ^property[50].code = #child 
* #B ^property[50].valueCode = #B77 
* #B ^property[51].code = #child 
* #B ^property[51].valueCode = #B78 
* #B ^property[52].code = #child 
* #B ^property[52].valueCode = #B79 
* #B ^property[53].code = #child 
* #B ^property[53].valueCode = #B80 
* #B ^property[54].code = #child 
* #B ^property[54].valueCode = #B81 
* #B ^property[55].code = #child 
* #B ^property[55].valueCode = #B82 
* #B ^property[56].code = #child 
* #B ^property[56].valueCode = #B83 
* #B ^property[57].code = #child 
* #B ^property[57].valueCode = #B84 
* #B ^property[58].code = #child 
* #B ^property[58].valueCode = #B87 
* #B ^property[59].code = #child 
* #B ^property[59].valueCode = #B90 
* #B ^property[60].code = #child 
* #B ^property[60].valueCode = #B99 
* #B02 "Lymphknoten vergrößert/schmerzend"
* #B02 ^definition = Inklusive: Lymphadenopathie mit/ohne Schmerz/Überempfindlichkeit~ andere Symptome/Beschwerden Lymphknoten Exklusive: akute Lymphadenitis B70~ chronische/nicht spezifizierte Lymphadenitis B71 Kriterien:
* #B02 ^property[0].code = #parent 
* #B02 ^property[0].valueCode = #B 
* #B02 ^property[1].code = #Relationships 
* #B02 ^property[1].valueString = "ICD-10 2017:R59.0~R59.1~R59.9" 
* #B04 "Blutsymptome/-beschwerden"
* #B04 ^definition = Inklusive:  Exklusive: Anämie B82~ Blässe S08 Kriterien:
* #B04 ^property[0].code = #parent 
* #B04 ^property[0].valueCode = #B 
* #B04 ^property[1].code = #Relationships 
* #B04 ^property[1].valueString = "ICD-10 2017:R68.8" 
* #B25 "Angst vor HIV/AIDS"
* #B25 ^definition = Inklusive:  Exklusive: Wenn der Patient die Krankheit hat~ ist die Krankheit zu kodieren Kriterien: Besorgnis oder Angst vor HIV/AIDS bei einem Patienten, der die Krankheit nicht hat oder bei dem die Diagnose noch nicht gesichert ist
* #B25 ^property[0].code = #parent 
* #B25 ^property[0].valueCode = #B 
* #B25 ^property[1].code = #Relationships 
* #B25 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #B26 "Angst vor bösartiger Blut-/Lympherkrankung"
* #B26 ^definition = Inklusive:  Exklusive: Wenn der Patient die Krankheit hat~ ist die Krankheit zu kodieren Kriterien: Besorgnis oder Angst vor Blut-/Lymphkrebs bei einem Patienten, der die Krankheit nicht hat oder bei dem die Diagnose noch nicht gesichert ist
* #B26 ^property[0].code = #parent 
* #B26 ^property[0].valueCode = #B 
* #B26 ^property[1].code = #Relationships 
* #B26 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #B27 "Angst vor anderer Blut-/Lympherkrankung"
* #B27 ^definition = Inklusive:  Exklusive: Angst vor bösartiger Blut-/ Lympherkrankung B26~ wenn der Patient die Krankheit hat~ ist die Krankheit zu kodieren Kriterien: Besorgnis oder Angst vor anderer Blut-/Lympherkrankung bei einem Patienten, der die Krankheit nicht hat oder bei dem die Diagnose noch nicht gesichert ist
* #B27 ^property[0].code = #parent 
* #B27 ^property[0].valueCode = #B 
* #B27 ^property[1].code = #Relationships 
* #B27 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #B28 "Funktionseinschränkung/Behinderung (B)"
* #B28 ^definition = Inklusive: Behinderung durch Blutungsneigung Exklusive:  Kriterien: Funktionseinschränkung/Behinderung durch eine Problem des Blutes/der blutbildenden Organe
* #B28 ^property[0].code = #parent 
* #B28 ^property[0].valueCode = #B 
* #B28 ^property[1].code = #Relationships 
* #B28 ^property[1].valueString = "ICD-10 2017:Z73.6" 
* #B28 ^property[2].code = #hints 
* #B28 ^property[2].valueString = "Beachte:  Hinweise: COOP/WONCA Charts eignen sich zur Dokumentation des funktionellen Status des Patienten (siehe Kapitel 8)" 
* #B29 "Andere Symptome/Beschwerden des Lymph-/Immunsystems"
* #B29 ^definition = Inklusive:  Exklusive: Splenomegalie B87 Kriterien:
* #B29 ^property[0].code = #parent 
* #B29 ^property[0].valueCode = #B 
* #B29 ^property[1].code = #Relationships 
* #B29 ^property[1].valueString = "ICD-10 2017:R68.8" 
* #B30 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? vollständig"
* #B30 ^property[0].code = #parent 
* #B30 ^property[0].valueCode = #B 
* #B31 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? teilweise"
* #B31 ^property[0].code = #parent 
* #B31 ^property[0].valueCode = #B 
* #B32 "Allergie-/Sensitivitätstestung"
* #B32 ^property[0].code = #parent 
* #B32 ^property[0].valueCode = #B 
* #B33 "Mikrobiologische/immunologische Untersuchung"
* #B33 ^property[0].code = #parent 
* #B33 ^property[0].valueCode = #B 
* #B34 "Blutuntersuchung"
* #B34 ^property[0].code = #parent 
* #B34 ^property[0].valueCode = #B 
* #B35 "Urinuntersuchung"
* #B35 ^property[0].code = #parent 
* #B35 ^property[0].valueCode = #B 
* #B36 "Stuhluntersuchung"
* #B36 ^property[0].code = #parent 
* #B36 ^property[0].valueCode = #B 
* #B37 "Histologische Untersuchung/zytologischer Abstrich"
* #B37 ^property[0].code = #parent 
* #B37 ^property[0].valueCode = #B 
* #B38 "Andere Laboruntersuchung NAK"
* #B38 ^property[0].code = #parent 
* #B38 ^property[0].valueCode = #B 
* #B39 "Körperliche Funktionsprüfung"
* #B39 ^property[0].code = #parent 
* #B39 ^property[0].valueCode = #B 
* #B40 "Diagnostische Endoskopie"
* #B40 ^property[0].code = #parent 
* #B40 ^property[0].valueCode = #B 
* #B41 "Diagnostisches Röntgen/Bildgebung"
* #B41 ^property[0].code = #parent 
* #B41 ^property[0].valueCode = #B 
* #B42 "Elektrische Aufzeichnungsverfahren"
* #B42 ^property[0].code = #parent 
* #B42 ^property[0].valueCode = #B 
* #B43 "Andere diagnostische Untersuchung"
* #B43 ^property[0].code = #parent 
* #B43 ^property[0].valueCode = #B 
* #B44 "Präventive Impfung/Medikation"
* #B44 ^property[0].code = #parent 
* #B44 ^property[0].valueCode = #B 
* #B45 "Beobachtung/Gesundheitsschulung/Beratung/Diät"
* #B45 ^property[0].code = #parent 
* #B45 ^property[0].valueCode = #B 
* #B46 "Konsultation eines anderen Primärversorgers"
* #B46 ^property[0].code = #parent 
* #B46 ^property[0].valueCode = #B 
* #B47 "Konsultation eines Facharztes"
* #B47 ^property[0].code = #parent 
* #B47 ^property[0].valueCode = #B 
* #B48 "Klärung/Besprechung des Beratungsanlasses mit dem Patienten"
* #B48 ^property[0].code = #parent 
* #B48 ^property[0].valueCode = #B 
* #B49 "Andere Vorsorgemaßnahme"
* #B49 ^property[0].code = #parent 
* #B49 ^property[0].valueCode = #B 
* #B50 "Medikation/Verschreibung/Erneuerung/Injektion"
* #B50 ^property[0].code = #parent 
* #B50 ^property[0].valueCode = #B 
* #B51 "Inzision/Drainage/Spülung/Absaugung/Entfernung von Körperflüssigkeit"
* #B51 ^property[0].code = #parent 
* #B51 ^property[0].valueCode = #B 
* #B52 "Exzision/Entfernung von Gewebe/Biopsie/Destruktion/Debridement/Verkauterung"
* #B52 ^property[0].code = #parent 
* #B52 ^property[0].valueCode = #B 
* #B53 "Instrumentelle Manipulation/Katheterisierung/Intubation/Dilatation"
* #B53 ^property[0].code = #parent 
* #B53 ^property[0].valueCode = #B 
* #B58 "Therapeutische Beratung/Zuhören"
* #B58 ^property[0].code = #parent 
* #B58 ^property[0].valueCode = #B 
* #B59 "Andere therapeutische Maßnahme/kleine Chirurgie NAK"
* #B59 ^property[0].code = #parent 
* #B59 ^property[0].valueCode = #B 
* #B60 "Testresultat/Ergebnis eigene Maßnahme"
* #B60 ^property[0].code = #parent 
* #B60 ^property[0].valueCode = #B 
* #B61 "Ergebnis Untersuchung/Test/Brief von anderem Leistungserbringer"
* #B61 ^property[0].code = #parent 
* #B61 ^property[0].valueCode = #B 
* #B62 "Adminstrative Maßnahme"
* #B62 ^property[0].code = #parent 
* #B62 ^property[0].valueCode = #B 
* #B63 "Folgekonsultation unspezifiziert"
* #B63 ^property[0].code = #parent 
* #B63 ^property[0].valueCode = #B 
* #B64 "Konsultation/Problembehandlung auf Initiative des Leistungserbringers"
* #B64 ^property[0].code = #parent 
* #B64 ^property[0].valueCode = #B 
* #B65 "Konsultation/Problembehandlung auf Initiative Dritter"
* #B65 ^property[0].code = #parent 
* #B65 ^property[0].valueCode = #B 
* #B66 "Überweisung an andere/n Leistungserbringer (nicht ärztl.)/Pflegekraft/Therapeuten/Sozialarbeiter"
* #B66 ^property[0].code = #parent 
* #B66 ^property[0].valueCode = #B 
* #B67 "Überweisung an einen Arzt/Spezialisten/eine Klinik/ein Krankenhaus"
* #B67 ^property[0].code = #parent 
* #B67 ^property[0].valueCode = #B 
* #B68 "Andere Überweisung NAK"
* #B68 ^property[0].code = #parent 
* #B68 ^property[0].valueCode = #B 
* #B69 "Anderer Beratungsanlass NAK"
* #B69 ^property[0].code = #parent 
* #B69 ^property[0].valueCode = #B 
* #B70 "Lymphadenitis, akute"
* #B70 ^definition = Inklusive: Lymphknotenabszess Exklusive: chronische/nicht spezifizierte Lymphadenitis~ Lymphadenitis mesenterialis B71~ akute Lymphangitis S76 Kriterien: einer/mehrere entzündete/vergrößerte und berührungsempfindliche/schmerzhafte Lymphknoten derselben anatomischen Region mit zeitlich nah zurückliegender Entstehung (weniger als 6 Wochen) und mit unbekannter Primärinfektion
* #B70 ^property[0].code = #parent 
* #B70 ^property[0].valueCode = #B 
* #B70 ^property[1].code = #Relationships 
* #B70 ^property[1].valueString = "ICD-10 2017:L04.0~L04.1~L04.2~L04.3~L04.8~L04.9" 
* #B70 ^property[2].code = #hints 
* #B70 ^property[2].valueString = "Beachte: vergrößerter Lymphknoten B02 Hinweise: " 
* #B71 "Lymphadenitis, chronische/unspezifische"
* #B71 ^definition = Inklusive: Mesenteriale Lymphadenitis Exklusive: akute Lymphadenitis (ausgenommen L. mesenterialis) B70~ akute Lymphangitis S76 Kriterien: vergrößerte und berührungsempfindliche Lymphknoten, die seit über 6 Wochen bestehenen; oder Nachweis entzündeter/vergrößerter mesenterialer Lymphknoten durch Chirurgie/Ultraschall/Lymphographie/andere Methode
* #B71 ^property[0].code = #parent 
* #B71 ^property[0].valueCode = #B 
* #B71 ^property[1].code = #Relationships 
* #B71 ^property[1].valueString = "ICD-10 2017:I88.0~I88.1~I88.8~I88.9" 
* #B71 ^property[2].code = #hints 
* #B71 ^property[2].valueString = "Beachte: vergrößerter Lymphknoten B02 Hinweise: " 
* #B72 "Morbus Hodgkin/Lmphom"
* #B72 ^definition = Inklusive:  Exklusive:  Kriterien: characteristische histologische Erscheinung
* #B72 ^property[0].code = #parent 
* #B72 ^property[0].valueCode = #B 
* #B72 ^property[1].code = #Relationships 
* #B72 ^property[1].valueString = "ICD-10 2017:C81.0~C81.1~C81.2~C81.3~C81.7~C81.9~C82.0~C82.1~C82.2~C82.7~C82.9~C83.0~C83.1~C83.3~C83.5~C83.7~C83.8~C83.9~C84.0~C84.1~C84.4~C84.5~C85.1~C85.7~C85.9~C86.0~C86.1~C86.2~C86.3~C86.4~C86.5~C86.6" 
* #B72 ^property[2].code = #hints 
* #B72 ^property[2].valueString = "Beachte: andere bösartige Neubildung des Blutes/der Lymphe B74~ gutartige/nicht spezifizierte Neubildung des Butes/der Lymphe B75 Hinweise: " 
* #B73 "Leukämie"
* #B73 ^definition = Inklusive: Alle Formen der Leukämie Exklusive:  Kriterien: characteristische histologische Erscheinung
* #B73 ^property[0].code = #parent 
* #B73 ^property[0].valueCode = #B 
* #B73 ^property[1].code = #Relationships 
* #B73 ^property[1].valueString = "ICD-10 2017:C91.0~C91.1~C91.3~C91.4~C91.5~C91.6~C91.7~C91.8~C91.9~C92.0~C92.1~C92.2~C92.3~C92.4~C92.5~C92.6~C92.7~C92.8~C92.9~C93.0~C93.1~C93.3~C93.7~C93.9~C94.0~C94.2~C94.3~C94.4~C94.7~C95.0~C95.1~C95.7~C95.9" 
* #B73 ^property[2].code = #hints 
* #B73 ^property[2].valueString = "Beachte: gutartige/nicht spezifizierte Neubildung des Butes/der Lymphe B75 Hinweise: " 
* #B74 "Maligne Bluterkrankung, andere"
* #B74 ^definition = Inklusive: myeloproliferative Erkrankungen~ multiples Myelom Exklusive: Morbus Hodgkin~ Lymphome B72 Kriterien:
* #B74 ^property[0].code = #parent 
* #B74 ^property[0].valueCode = #B 
* #B74 ^property[1].code = #Relationships 
* #B74 ^property[1].valueString = "ICD-10 2017:C26.1~C37~C46.3~C77.0~C77.1~C77.2~C77.3~C77.4~C77.5~C77.8~C77.9~C88.0~C88.2~C88.3~C88.4~C88.7~C88.9~C90.0~C90.1~C90.2~C90.3~C94.6~C96.0~C96.2~C96.4~C96.5~C96.6~C96.7~C96.8~C96.9" 
* #B75 "Benigne/unspezifische Blutneubildung"
* #B75 ^definition = Inklusive: Gutartige Neubildung Blut~ Neubildung Blut nicht als gut- oder bösartig spezifiziert/Testergebnis nicht verfügbar~ Polycythaemia rubra vera Exklusive:  Kriterien:
* #B75 ^property[0].code = #parent 
* #B75 ^property[0].valueCode = #B 
* #B75 ^property[1].code = #Relationships 
* #B75 ^property[1].valueString = "ICD-10 2017:D15.0~D36.0~D45~D47.0~D47.1~D47.2~D47.3~D47.4~D47.5~D47.7~D47.9" 
* #B76 "Milzruptur, traumatische"
* #B76 ^property[0].code = #parent 
* #B76 ^property[0].valueCode = #B 
* #B76 ^property[1].code = #Relationships 
* #B76 ^property[1].valueString = "ICD-10 2017:S36.0" 
* #B77 "Verletzung Blut/Lymphe/Milz, andere"
* #B77 ^definition = Inklusive:  Exklusive: Milzriß~ traumatischer B76 Kriterien:
* #B77 ^property[0].code = #parent 
* #B77 ^property[0].valueCode = #B 
* #B77 ^property[1].code = #Relationships 
* #B77 ^property[1].valueString = "ICD-10 2017:T11.4~T13.4~T14.9" 
* #B78 "Vererbliche hämolytische Anämie"
* #B78 ^definition = Inklusive: Sichelzellanämie~ Sichelzellträger~ Spherocytose~ Thalassaemie Exklusive:  Kriterien: Characteristische Befunde bei Tests wie Hämoglobinelektrophorese, Blutausstrich, oder verstärkte osmotische Brüchigkeit der roten Blutkörperchen
* #B78 ^property[0].code = #parent 
* #B78 ^property[0].valueCode = #B 
* #B78 ^property[1].code = #Relationships 
* #B78 ^property[1].valueString = "ICD-10 2017:D56.0~D56.1~D56.2~D56.3~D56.4~D56.8~D56.9~D57.0~D57.1~D57.2~D57.3~D57.8~D58.0~D58.1~D58.2~D58.8~D58.9" 
* #B78 ^property[2].code = #hints 
* #B78 ^property[2].valueString = "Beachte: andere angeborende Anomalie des Blutes/der Lymphe B79 Hinweise: " 
* #B79 "Angeborene Anomalie Blut/Lymphsystem, andere"
* #B79 ^definition = Inklusive: Angeborene Anämie Exklusive: Vererbbare hämolytische Anämie B78~ Hämophilie B83~ Hämangiom S81~ Lymphangiom S81 Kriterien:
* #B79 ^property[0].code = #parent 
* #B79 ^property[0].valueCode = #B 
* #B79 ^property[1].code = #Relationships 
* #B79 ^property[1].valueString = "ICD-10 2017:D61.0~D64.0~D64.4~Q89.0~Q89.8" 
* #B80 "Eisenmangelanämie"
* #B80 ^definition = Inklusive: Blutungsanämie Exklusive: Eisenmangel ohne Anämie T91 Kriterien: Absinken der Hämoglobin- oder Hämatokritwerte unter deie Normalwerte für Alter und Geschlecht; plus Nachweis von Blutverlust oder mikrozytärer, hypochromer Erythrozyten im Nativbild oder im gefärbten Ausstrich bei Nichtvorhandensein einer Thalassämie, oder vermindertes Eisen im Serum und erhöhte Eisenbindungskapazität, oder vermindertes Ferritin im Serum, oder vermindertes Hämosiderin im Knochenmark, oder gutes Ansprechen auf Verabreichung von Eisen
* #B80 ^property[0].code = #parent 
* #B80 ^property[0].valueCode = #B 
* #B80 ^property[1].code = #Relationships 
* #B80 ^property[1].valueString = "ICD-10 2017:D50.0~D50.1~D50.8~D50.9" 
* #B80 ^property[2].code = #hints 
* #B80 ^property[2].valueString = "Beachte: andere/nicht spezifizierte Anämie B82 Hinweise: " 
* #B81 "Anämie Vitamin B12/Folsäuremangel"
* #B81 ^definition = Inklusive: macrozytäre Anämie~ perniziöse Anämie Exklusive: Vitamin B 12-Mangel ohne Anämie T91 Kriterien: Makrozytäre Anämie im Nativbild oder gefärbten Ausstrich; plus verminderter Vitamin B12-Spiegel, Folsäurespiegel, oder positiver Schilling-Test
* #B81 ^property[0].code = #parent 
* #B81 ^property[0].valueCode = #B 
* #B81 ^property[1].code = #Relationships 
* #B81 ^property[1].valueString = "ICD-10 2017:D51.0~D51.1~D51.2~D51.3~D51.8~D51.9~D52.0~D52.1~D52.8~D52.9" 
* #B82 "Anämie, andere/unspezifisch"
* #B82 ^definition = Inklusive: erworbene hämolytische Anämie~ aplastische Anämie~ megaloblastische Anämie NNB~ Eiweißmangelanämie Exklusive: Eisenmangel-Anämie B80~ Vitamin B12-Folsäurenmangel-Anämie B81~ Schwangerschafts-Anämie W84 Kriterien:
* #B82 ^property[0].code = #parent 
* #B82 ^property[0].valueCode = #B 
* #B82 ^property[1].code = #Relationships 
* #B82 ^property[1].valueString = "ICD-10 2017:D46.0~D46.1~D46.2~D46.4~D46.5~D46.6~D46.7~D46.9~D53.0~D53.1~D53.2~D53.8~D53.9~D55.0~D55.1~D55.2~D55.3~D55.8~D55.9~D59.0~D59.1~D59.2~D59.3~D59.4~D59.5~D59.6~D59.8~D59.9~D60.0~D60.1~D60.8~D60.9~D61.3~D61.8~D61.9~D62~D63.0~D63.8~D64.1~D64.3~D64.8~D64.9" 
* #B83 "Purpura/Gerinnungsstörung"
* #B83 ^definition = Inklusive: abnorme Plättchen~ Hämophilie~ Thrombozytopenie Exklusive:  Kriterien:
* #B83 ^property[0].code = #parent 
* #B83 ^property[0].valueCode = #B 
* #B83 ^property[1].code = #Relationships 
* #B83 ^property[1].valueString = "ICD-10 2017:D65~D66~D67~D68.0~D68.1~D68.2~D68.3~D68.4~D68.5~D68.6~D68.8~D68.9~D69.0~D69.1~D69.2~D69.3~D69.4~D69.5~D69.6~D69.8~D69.9" 
* #B84 "Ungeklärte abnorme Leukozyten"
* #B84 ^definition = Inklusive: unklare Agranulozytose~ unklare Eosinophilie~ unklare Leukozytose~ unklare Lymphozytosis~ unklare Neutropenie Exklusive: Leukämie B73 Kriterien:
* #B84 ^property[0].code = #parent 
* #B84 ^property[0].valueCode = #B 
* #B84 ^property[1].code = #Relationships 
* #B84 ^property[1].valueString = "ICD-10 2017:D70~D71~D72.0~D72.1~D72.8~D72.9~R72" 
* #B87 "Splenomegalie"
* #B87 ^definition = Inklusive:  Exklusive: Hypersplenie B99 Kriterien:
* #B87 ^property[0].code = #parent 
* #B87 ^property[0].valueCode = #B 
* #B87 ^property[1].code = #Relationships 
* #B87 ^property[1].valueString = "ICD-10 2017:R16.1~R16.2" 
* #B90 "HIV-Infektion/AIDS"
* #B90 ^definition = Inklusive:  Exklusive:  Kriterien: serologisch nachgewiesene HIV-Infektion bei einem asymptomatischen Patienten
* #B90 ^property[0].code = #parent 
* #B90 ^property[0].valueCode = #B 
* #B90 ^property[1].code = #Relationships 
* #B90 ^property[1].valueString = "ICD-10 2017:B20.0~B20.1~B20.2~B20.3~B20.4~B20.5~B20.6~B20.7~B20.8~B20.9~B21.0~B21.1~B21.2~B21.3~B21.7~B21.8~B21.9~B22.0~B22.1~B22.2~B22.7~B23.0~B23.1~B23.2~B23.8~B24~R75~Z21" 
* #B99 "Blut-/Lymph-/Milzerkrankung, andere"
* #B99 ^definition = Inklusive: Autoimmunerkrankung Blut~ Komplementdefekt~ Hypersplenismus~ Immundefekt~ andere/unklare hämatologische Abnormalität~ erhöhte BSG~ Erythrozytenabnormalität~ Sarkoidose~ sekundäre Polyzythämie Exklusive: Lymphadenitis B70~ B71~ primäre Polyzythämie B75~ HIV/AIDS B90~ Lymphödem K99 Kriterien:
* #B99 ^property[0].code = #parent 
* #B99 ^property[0].valueCode = #B 
* #B99 ^property[1].code = #Relationships 
* #B99 ^property[1].valueString = "ICD-10 2017:D73.0~D73.1~D73.2~D73.3~D73.4~D73.5~D73.8~D73.9~D74.0~D74.8~D74.9~D75.0~D75.1~D75.8~D75.9~D76.1~D76.2~D76.3~D77~D80.0~D80.1~D80.2~D80.3~D80.4~D80.5~D80.6~D80.7~D80.8~D80.9~D81.0~D81.1~D81.2~D81.3~D81.4~D81.5~D81.6~D81.7~D81.8~D81.9~D82.0~D82.1~D82.2~D82.3~D82.4~D82.8~D82.9~D83.0~D83.1~D83.2~D83.8~D83.9~D84.0~D84.1~D84.8~D84.9~D86.0~D86.1~D86.2~D86.3~D86.8~D86.9~D89.0~D89.1~D89.2~D89.3~D89.8~D89.9~I89.1~I89.8~I89.9~R70.0~R70.1~R71" 
* #D "Verdauungssystem"
* #D ^property[0].code = #child 
* #D ^property[0].valueCode = #D01 
* #D ^property[1].code = #child 
* #D ^property[1].valueCode = #D02 
* #D ^property[2].code = #child 
* #D ^property[2].valueCode = #D03 
* #D ^property[3].code = #child 
* #D ^property[3].valueCode = #D04 
* #D ^property[4].code = #child 
* #D ^property[4].valueCode = #D05 
* #D ^property[5].code = #child 
* #D ^property[5].valueCode = #D06 
* #D ^property[6].code = #child 
* #D ^property[6].valueCode = #D07 
* #D ^property[7].code = #child 
* #D ^property[7].valueCode = #D08 
* #D ^property[8].code = #child 
* #D ^property[8].valueCode = #D09 
* #D ^property[9].code = #child 
* #D ^property[9].valueCode = #D10 
* #D ^property[10].code = #child 
* #D ^property[10].valueCode = #D11 
* #D ^property[11].code = #child 
* #D ^property[11].valueCode = #D12 
* #D ^property[12].code = #child 
* #D ^property[12].valueCode = #D13 
* #D ^property[13].code = #child 
* #D ^property[13].valueCode = #D14 
* #D ^property[14].code = #child 
* #D ^property[14].valueCode = #D15 
* #D ^property[15].code = #child 
* #D ^property[15].valueCode = #D16 
* #D ^property[16].code = #child 
* #D ^property[16].valueCode = #D17 
* #D ^property[17].code = #child 
* #D ^property[17].valueCode = #D18 
* #D ^property[18].code = #child 
* #D ^property[18].valueCode = #D19 
* #D ^property[19].code = #child 
* #D ^property[19].valueCode = #D20 
* #D ^property[20].code = #child 
* #D ^property[20].valueCode = #D21 
* #D ^property[21].code = #child 
* #D ^property[21].valueCode = #D23 
* #D ^property[22].code = #child 
* #D ^property[22].valueCode = #D24 
* #D ^property[23].code = #child 
* #D ^property[23].valueCode = #D25 
* #D ^property[24].code = #child 
* #D ^property[24].valueCode = #D26 
* #D ^property[25].code = #child 
* #D ^property[25].valueCode = #D27 
* #D ^property[26].code = #child 
* #D ^property[26].valueCode = #D28 
* #D ^property[27].code = #child 
* #D ^property[27].valueCode = #D29 
* #D ^property[28].code = #child 
* #D ^property[28].valueCode = #D30 
* #D ^property[29].code = #child 
* #D ^property[29].valueCode = #D31 
* #D ^property[30].code = #child 
* #D ^property[30].valueCode = #D32 
* #D ^property[31].code = #child 
* #D ^property[31].valueCode = #D33 
* #D ^property[32].code = #child 
* #D ^property[32].valueCode = #D34 
* #D ^property[33].code = #child 
* #D ^property[33].valueCode = #D35 
* #D ^property[34].code = #child 
* #D ^property[34].valueCode = #D36 
* #D ^property[35].code = #child 
* #D ^property[35].valueCode = #D37 
* #D ^property[36].code = #child 
* #D ^property[36].valueCode = #D38 
* #D ^property[37].code = #child 
* #D ^property[37].valueCode = #D39 
* #D ^property[38].code = #child 
* #D ^property[38].valueCode = #D40 
* #D ^property[39].code = #child 
* #D ^property[39].valueCode = #D41 
* #D ^property[40].code = #child 
* #D ^property[40].valueCode = #D42 
* #D ^property[41].code = #child 
* #D ^property[41].valueCode = #D43 
* #D ^property[42].code = #child 
* #D ^property[42].valueCode = #D44 
* #D ^property[43].code = #child 
* #D ^property[43].valueCode = #D45 
* #D ^property[44].code = #child 
* #D ^property[44].valueCode = #D46 
* #D ^property[45].code = #child 
* #D ^property[45].valueCode = #D47 
* #D ^property[46].code = #child 
* #D ^property[46].valueCode = #D48 
* #D ^property[47].code = #child 
* #D ^property[47].valueCode = #D49 
* #D ^property[48].code = #child 
* #D ^property[48].valueCode = #D50 
* #D ^property[49].code = #child 
* #D ^property[49].valueCode = #D51 
* #D ^property[50].code = #child 
* #D ^property[50].valueCode = #D52 
* #D ^property[51].code = #child 
* #D ^property[51].valueCode = #D53 
* #D ^property[52].code = #child 
* #D ^property[52].valueCode = #D54 
* #D ^property[53].code = #child 
* #D ^property[53].valueCode = #D55 
* #D ^property[54].code = #child 
* #D ^property[54].valueCode = #D56 
* #D ^property[55].code = #child 
* #D ^property[55].valueCode = #D57 
* #D ^property[56].code = #child 
* #D ^property[56].valueCode = #D58 
* #D ^property[57].code = #child 
* #D ^property[57].valueCode = #D59 
* #D ^property[58].code = #child 
* #D ^property[58].valueCode = #D60 
* #D ^property[59].code = #child 
* #D ^property[59].valueCode = #D61 
* #D ^property[60].code = #child 
* #D ^property[60].valueCode = #D62 
* #D ^property[61].code = #child 
* #D ^property[61].valueCode = #D63 
* #D ^property[62].code = #child 
* #D ^property[62].valueCode = #D64 
* #D ^property[63].code = #child 
* #D ^property[63].valueCode = #D65 
* #D ^property[64].code = #child 
* #D ^property[64].valueCode = #D66 
* #D ^property[65].code = #child 
* #D ^property[65].valueCode = #D67 
* #D ^property[66].code = #child 
* #D ^property[66].valueCode = #D68 
* #D ^property[67].code = #child 
* #D ^property[67].valueCode = #D69 
* #D ^property[68].code = #child 
* #D ^property[68].valueCode = #D70 
* #D ^property[69].code = #child 
* #D ^property[69].valueCode = #D71 
* #D ^property[70].code = #child 
* #D ^property[70].valueCode = #D72 
* #D ^property[71].code = #child 
* #D ^property[71].valueCode = #D73 
* #D ^property[72].code = #child 
* #D ^property[72].valueCode = #D74 
* #D ^property[73].code = #child 
* #D ^property[73].valueCode = #D75 
* #D ^property[74].code = #child 
* #D ^property[74].valueCode = #D76 
* #D ^property[75].code = #child 
* #D ^property[75].valueCode = #D77 
* #D ^property[76].code = #child 
* #D ^property[76].valueCode = #D78 
* #D ^property[77].code = #child 
* #D ^property[77].valueCode = #D79 
* #D ^property[78].code = #child 
* #D ^property[78].valueCode = #D80 
* #D ^property[79].code = #child 
* #D ^property[79].valueCode = #D81 
* #D ^property[80].code = #child 
* #D ^property[80].valueCode = #D82 
* #D ^property[81].code = #child 
* #D ^property[81].valueCode = #D83 
* #D ^property[82].code = #child 
* #D ^property[82].valueCode = #D84 
* #D ^property[83].code = #child 
* #D ^property[83].valueCode = #D85 
* #D ^property[84].code = #child 
* #D ^property[84].valueCode = #D86 
* #D ^property[85].code = #child 
* #D ^property[85].valueCode = #D87 
* #D ^property[86].code = #child 
* #D ^property[86].valueCode = #D88 
* #D ^property[87].code = #child 
* #D ^property[87].valueCode = #D89 
* #D ^property[88].code = #child 
* #D ^property[88].valueCode = #D90 
* #D ^property[89].code = #child 
* #D ^property[89].valueCode = #D91 
* #D ^property[90].code = #child 
* #D ^property[90].valueCode = #D92 
* #D ^property[91].code = #child 
* #D ^property[91].valueCode = #D93 
* #D ^property[92].code = #child 
* #D ^property[92].valueCode = #D94 
* #D ^property[93].code = #child 
* #D ^property[93].valueCode = #D95 
* #D ^property[94].code = #child 
* #D ^property[94].valueCode = #D96 
* #D ^property[95].code = #child 
* #D ^property[95].valueCode = #D97 
* #D ^property[96].code = #child 
* #D ^property[96].valueCode = #D98 
* #D ^property[97].code = #child 
* #D ^property[97].valueCode = #D99 
* #D01 "Bauchschmerzen/-krämpfe, generalisiert"
* #D01 ^definition = Inklusive: Koliken~ Bauchkrämpfe/Unbehagen/Schmerz NNB~ kindliche Koliken Exklusive: Magenschmerzen D02~ Sodbrennen D03~ lokalisierter Schmerz im Abdomen D06~ Magenverstimmung D07~ Blähungen D08~ Gallenkolik D98~ Nierenkolik U14~ Dysmenorrhoe X02 Kriterien:
* #D01 ^property[0].code = #parent 
* #D01 ^property[0].valueCode = #D 
* #D01 ^property[1].code = #Relationships 
* #D01 ^property[1].valueString = "ICD-10 2017:R10.0~R10.4" 
* #D02 "Bauchschmerzen, epigastrische"
* #D02 ^definition = Inklusive: epigastrische Beschwerden~ Völlegefühl~ Bauchweh/-schmerz Exklusive: Dyspepsie/Magenverstimmung D07~ Flatulenz~ Blähungen~ Winde D08 Kriterien:
* #D02 ^property[0].code = #parent 
* #D02 ^property[0].valueCode = #D 
* #D02 ^property[1].code = #Relationships 
* #D02 ^property[1].valueString = "ICD-10 2017:R10.1" 
* #D03 "Sodbrennen"
* #D03 ^definition = Inklusive: Hyperazidität~ Pyrosis Exklusive: Schmerzen im Oberbauch D02~ Magenverstimmung D07~ Ösophagitis~ Reflux D84 Kriterien:
* #D03 ^property[0].code = #parent 
* #D03 ^property[0].valueCode = #D 
* #D03 ^property[1].code = #Relationships 
* #D03 ^property[1].valueString = "ICD-10 2017:R12" 
* #D04 "Rektale/anale Schmerzen"
* #D04 ^definition = Inklusive: analer Spasmus~ Proctalgia fugax Exklusive: Kotimpaktion D12 Kriterien:
* #D04 ^property[0].code = #parent 
* #D04 ^property[0].valueCode = #D 
* #D04 ^property[1].code = #Relationships 
* #D04 ^property[1].valueString = "ICD-10 2017:K59.4~R10.2~R10.3" 
* #D05 "Perianaler Juckreiz"
* #D05 ^definition = Inklusive: Pruritus ani Exklusive: Pruritus vulvae X16 Kriterien:
* #D05 ^property[0].code = #parent 
* #D05 ^property[0].valueCode = #D 
* #D05 ^property[1].code = #Relationships 
* #D05 ^property[1].valueString = "ICD-10 2017:L29.0~L29.3" 
* #D06 "Bauchschmerzen lokalisiert, andere"
* #D06 ^definition = Inklusive: Dickdarmschmerzen Exklusive: Schmerzen im Oberbauch~ Magenschmerzen D02~ Sodbrennen D03~ lokalisierter Schmerz im Unterleib D06~ Magenverstimmung D07~ Blähungen D08~ Reizdarmsyndrom D93~ Gallenkolik D98~ Nierenkolik U14~ Dysmenorrhoe X02 Kriterien:
* #D06 ^property[0].code = #parent 
* #D06 ^property[0].valueCode = #D 
* #D06 ^property[1].code = #Relationships 
* #D06 ^property[1].valueString = "ICD-10 2017:R10.1~R10.2~R10.3" 
* #D07 "Magenverstimmung/Verdauungsstörung"
* #D07 ^definition = Inklusive:  Exklusive: Schmerzen im Oberbauch D02~ Sodbrennen D03~ Aufstoßen/Rülpsen D08 Kriterien:
* #D07 ^property[0].code = #parent 
* #D07 ^property[0].valueCode = #D 
* #D07 ^property[1].code = #Relationships 
* #D07 ^property[1].valueString = "ICD-10 2017:K30" 
* #D08 "Flatulenz/Blähungen/Aufstoßen"
* #D08 ^definition = Inklusive: Meteorismus~ Rülpsen~ Blähungsschmerz~ aufgetriebenes Abdomen~ verschlagene Winde Exklusive: Magenverstimmung D07~ Größenveränderung des Abdomens D25 Kriterien:
* #D08 ^property[0].code = #parent 
* #D08 ^property[0].valueCode = #D 
* #D08 ^property[1].code = #Relationships 
* #D08 ^property[1].valueString = "ICD-10 2017:R14" 
* #D09 "Übelkeit"
* #D09 ^definition = Inklusive:  Exklusive: Übersättigung D02~ Erbrechen D10~ durch Alkoholgenuß P16~ Appetitmangel T03~ Schwangerschaft W05 Kriterien:
* #D09 ^property[0].code = #parent 
* #D09 ^property[0].valueCode = #D 
* #D09 ^property[1].code = #Relationships 
* #D09 ^property[1].valueString = "ICD-10 2017:R11" 
* #D09 ^property[2].code = #hints 
* #D09 ^property[2].valueString = "Beachte:  Hinweise: Kodiere als Diagnose für Übelkeit und Erbrechen: D10" 
* #D10 "Erbrechen"
* #D10 ^definition = Inklusive: Emesis~ Hyperemesis~ Würgen/Brechreiz Exklusive: Hämatemesis D14~ in der Schwangerschaft W05 Kriterien:
* #D10 ^property[0].code = #parent 
* #D10 ^property[0].valueCode = #D 
* #D10 ^property[1].code = #Relationships 
* #D10 ^property[1].valueString = "ICD-10 2017:F50.5~R11" 
* #D10 ^property[2].code = #hints 
* #D10 ^property[2].valueString = "Beachte:  Hinweise: Kodiere als Diagnose für Diarrhoe und Erbrechen: D11" 
* #D11 "Durchfall"
* #D11 ^definition = Inklusive: Häufige oder flüssige Darmentleerungen~ wässrige Stühle Exklusive: Melaena D15~ sonstige intestinale Veränderungen D18 Kriterien:
* #D11 ^property[0].code = #parent 
* #D11 ^property[0].valueCode = #D 
* #D11 ^property[1].code = #Relationships 
* #D11 ^property[1].valueString = "ICD-10 2017:K52.9~K59.1" 
* #D12 "Verstopfung"
* #D12 ^definition = Inklusive: Kotimpaktation Exklusive: Darmverschluß D99 Kriterien:
* #D12 ^property[0].code = #parent 
* #D12 ^property[0].valueCode = #D 
* #D12 ^property[1].code = #Relationships 
* #D12 ^property[1].valueString = "ICD-10 2017:K56.4~K59.0" 
* #D13 "Gelbsucht"
* #D13 ^definition = Inklusive: Ikterus Exklusive:  Kriterien:
* #D13 ^property[0].code = #parent 
* #D13 ^property[0].valueCode = #D 
* #D13 ^property[1].code = #Relationships 
* #D13 ^property[1].valueString = "ICD-10 2017:R17" 
* #D14 "Bluterbrechen/Hämatemesis"
* #D14 ^definition = Inklusive:  Exklusive: Hämoptyse/Bluthusten R24 Kriterien:
* #D14 ^property[0].code = #parent 
* #D14 ^property[0].valueCode = #D 
* #D14 ^property[1].code = #Relationships 
* #D14 ^property[1].valueString = "ICD-10 2017:K92.0" 
* #D15 "Melaena/Teerstuhl"
* #D15 ^definition = Inklusive: schwarzer/teerartiger Stuhl Exklusive: frisches Blut im Stuhl D16 Kriterien:
* #D15 ^property[0].code = #parent 
* #D15 ^property[0].valueCode = #D 
* #D15 ^property[1].code = #Relationships 
* #D15 ^property[1].valueString = "ICD-10 2017:K92.1" 
* #D16 "Rektale Blutung"
* #D16 ^definition = Inklusive: frisches Blut im Stuhl Exklusive: Melaena D15 Kriterien:
* #D16 ^property[0].code = #parent 
* #D16 ^property[0].valueCode = #D 
* #D16 ^property[1].code = #Relationships 
* #D16 ^property[1].valueString = "ICD-10 2017:K62.5" 
* #D17 "Stuhlinkontinenz"
* #D17 ^definition = Inklusive: Stuhlinkontinenz Exklusive: Enkopresis P13 Kriterien:
* #D17 ^property[0].code = #parent 
* #D17 ^property[0].valueCode = #D 
* #D17 ^property[1].code = #Relationships 
* #D17 ^property[1].valueString = "ICD-10 2017:R15" 
* #D18 "Veränderung des Stuhlgangs/der Verdauung"
* #D18 ^definition = Inklusive:  Exklusive: Durchfall D11~ Verstopfung D12~ Inkontinenz D17 Kriterien:
* #D18 ^property[0].code = #parent 
* #D18 ^property[0].valueCode = #D 
* #D18 ^property[1].code = #Relationships 
* #D18 ^property[1].valueString = "ICD-10 2017:R19.4~R19.5" 
* #D19 "Zahn-/Zahnfleischsymptome/-beschwerden"
* #D19 ^definition = Inklusive: Probleme mit Zahnersatz~ Zahnfleischentzündung/-bluten~ Zahnen~ Zahnschmerzen Exklusive: Karies D82 Kriterien:
* #D19 ^property[0].code = #parent 
* #D19 ^property[0].valueCode = #D 
* #D19 ^property[1].code = #Relationships 
* #D19 ^property[1].valueString = "ICD-10 2017:K00.7~K03.6~K03.7~K08.8" 
* #D20 "Mund-/Zungen-/Lippensymptome/-beschwerden"
* #D20 ^definition = Inklusive: Mundgeruch~ belegte Zunge~ aufgsprungene Lippen~ Speichelfluß~ Mundtrockenheit~ Halitosis~ wunder Mund~ geschwollene Lippen Exklusive: Cheilosis D83~ Probleme mit Zähnen und Zahnfleisch D19~ Geschmacksstörung N16~ Dehydration T11 Kriterien:
* #D20 ^property[0].code = #parent 
* #D20 ^property[0].valueCode = #D 
* #D20 ^property[1].code = #Relationships 
* #D20 ^property[1].valueString = "ICD-10 2017:K13.1~K13.7~K14.1~K14.2~K14.5~K14.6~R19.6~R68.2" 
* #D21 "Schluckbeschwerden"
* #D21 ^definition = Inklusive: Erstickungsgefühl~ Dysphagie Exklusive:  Kriterien:
* #D21 ^property[0].code = #parent 
* #D21 ^property[0].valueCode = #D 
* #D21 ^property[1].code = #Relationships 
* #D21 ^property[1].valueString = "ICD-10 2017:R13" 
* #D23 "Hepatomegalie"
* #D23 ^property[0].code = #parent 
* #D23 ^property[0].valueCode = #D 
* #D23 ^property[1].code = #Relationships 
* #D23 ^property[1].valueString = "ICD-10 2017:R16.0~R16.2" 
* #D24 "Abdomineller Tastbefund/Resistenz NNB"
* #D24 ^definition = Inklusive: Geschwulst im Abdomen Exklusive: Splenomegalie B87~ Hepatomegalie D23~ Schwellung der Niere U14 Kriterien:
* #D24 ^property[0].code = #parent 
* #D24 ^property[0].valueCode = #D 
* #D24 ^property[1].code = #Relationships 
* #D24 ^property[1].valueString = "ICD-10 2017:R19.0" 
* #D25 "Aufgetriebenes Abdomen/Spannung"
* #D25 ^definition = Inklusive: aufgetriebenes Abdomen ohne Gewebsmasse Exklusive: Aufgedunsenheit~ Blähungen D08~ Gewebsmasse im Abdomen D24~ Aszites D29 Kriterien:
* #D25 ^property[0].code = #parent 
* #D25 ^property[0].valueCode = #D 
* #D25 ^property[1].code = #Relationships 
* #D25 ^property[1].valueString = "ICD-10 2017:R19.0" 
* #D26 "Angst vor Krebs des Verdauungssystems"
* #D26 ^definition = Inklusive:  Exklusive: Wenn der Patient die Krankheit hat~ ist die Krankheit zu kodieren Kriterien: Besorgnis oder Angst vor Krebs im Verdauungstrakt bei einem Patienten, der die Krankheit nicht hat oder bei dem die Diagnose noch nicht gesichert ist
* #D26 ^property[0].code = #parent 
* #D26 ^property[0].valueCode = #D 
* #D26 ^property[1].code = #Relationships 
* #D26 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #D27 "Angst vor Erkrankung Verdauungssystem, andere"
* #D27 ^definition = Inklusive:  Exklusive: Angst vor Krebs Verdauungsorgane D26~ wenn der Patient die Erkrankung hat~ kodiere die Erkrankung Kriterien: Besorgnis oder Angst vor anderer Erkrankung im Verdauungstrakt bei einem Patienten, der die Krankheit nicht hat oder bei dem die Diagnose noch nicht gesichert ist
* #D27 ^property[0].code = #parent 
* #D27 ^property[0].valueCode = #D 
* #D27 ^property[1].code = #Relationships 
* #D27 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #D28 "Funktionseinschränkung/Behinderung (D)"
* #D28 ^definition = Inklusive:  Exklusive: Kolostomie~ Gastronomie A89~ postoperative Störungen D99~ Dumpingsyndrom D99 Kriterien: Funktionseinschränkung/Behinderung durch eine Problem des Verdauungstraktes
* #D28 ^property[0].code = #parent 
* #D28 ^property[0].valueCode = #D 
* #D28 ^property[1].code = #Relationships 
* #D28 ^property[1].valueString = "ICD-10 2017:Z73.6" 
* #D28 ^property[2].code = #hints 
* #D28 ^property[2].valueString = "Beachte:  Hinweise: COOP/WONCA Charts eignen sich zur Dokumentation des funktionellen Status des Patienten (siehe Kapitel 8)" 
* #D29 "Andere Symptome/Beschwerden Verdauungssystem"
* #D29 ^definition = Inklusive: Aszites~ Zähneknirschen Exklusive:  Kriterien:
* #D29 ^property[0].code = #parent 
* #D29 ^property[0].valueCode = #D 
* #D29 ^property[1].code = #Relationships 
* #D29 ^property[1].valueString = "ICD-10 2017:K03.8~R18~R19.1~R19.2~R19.3~R19.8" 
* #D30 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? vollständig"
* #D30 ^property[0].code = #parent 
* #D30 ^property[0].valueCode = #D 
* #D31 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? teilweise"
* #D31 ^property[0].code = #parent 
* #D31 ^property[0].valueCode = #D 
* #D32 "Allergie-/Sensitivitätstestung"
* #D32 ^property[0].code = #parent 
* #D32 ^property[0].valueCode = #D 
* #D33 "Mikrobiologische/immunologische Untersuchung"
* #D33 ^property[0].code = #parent 
* #D33 ^property[0].valueCode = #D 
* #D34 "Blutuntersuchung"
* #D34 ^property[0].code = #parent 
* #D34 ^property[0].valueCode = #D 
* #D35 "Urinuntersuchung"
* #D35 ^property[0].code = #parent 
* #D35 ^property[0].valueCode = #D 
* #D36 "Stuhluntersuchung"
* #D36 ^property[0].code = #parent 
* #D36 ^property[0].valueCode = #D 
* #D37 "Histologische Untersuchung/zytologischer Abstrich"
* #D37 ^property[0].code = #parent 
* #D37 ^property[0].valueCode = #D 
* #D38 "Andere Laboruntersuchung NAK"
* #D38 ^property[0].code = #parent 
* #D38 ^property[0].valueCode = #D 
* #D39 "Körperliche Funktionsprüfung"
* #D39 ^property[0].code = #parent 
* #D39 ^property[0].valueCode = #D 
* #D40 "Diagnostische Endoskopie"
* #D40 ^property[0].code = #parent 
* #D40 ^property[0].valueCode = #D 
* #D41 "Diagnostisches Röntgen/Bildgebung"
* #D41 ^property[0].code = #parent 
* #D41 ^property[0].valueCode = #D 
* #D42 "Elektrische Aufzeichnungsverfahren"
* #D42 ^property[0].code = #parent 
* #D42 ^property[0].valueCode = #D 
* #D43 "Andere diagnostische Untersuchung"
* #D43 ^property[0].code = #parent 
* #D43 ^property[0].valueCode = #D 
* #D44 "Präventive Impfung/Medikation"
* #D44 ^property[0].code = #parent 
* #D44 ^property[0].valueCode = #D 
* #D45 "Beobachtung/Gesundheitsschulung/Beratung/Diät"
* #D45 ^property[0].code = #parent 
* #D45 ^property[0].valueCode = #D 
* #D46 "Konsultation eines anderen Primärversorgers"
* #D46 ^property[0].code = #parent 
* #D46 ^property[0].valueCode = #D 
* #D47 "Konsultation eines Facharztes"
* #D47 ^property[0].code = #parent 
* #D47 ^property[0].valueCode = #D 
* #D48 "Klärung/Besprechung des Beratungsanlasses mit dem Patienten"
* #D48 ^property[0].code = #parent 
* #D48 ^property[0].valueCode = #D 
* #D49 "Andere Vorsorgemaßnahme"
* #D49 ^property[0].code = #parent 
* #D49 ^property[0].valueCode = #D 
* #D50 "Medikation/Verschreibung/Erneuerung/Injektion"
* #D50 ^property[0].code = #parent 
* #D50 ^property[0].valueCode = #D 
* #D51 "Inzision/Drainage/Spülung/Absaugung/Entfernung von Körperflüssigkeit"
* #D51 ^property[0].code = #parent 
* #D51 ^property[0].valueCode = #D 
* #D52 "Exzision/Entfernung von Gewebe/Biopsie/Destruktion/Debridement/Verkauterung"
* #D52 ^property[0].code = #parent 
* #D52 ^property[0].valueCode = #D 
* #D53 "Instrumentelle Manipulation/Katheterisierung/Intubation/Dilatation"
* #D53 ^property[0].code = #parent 
* #D53 ^property[0].valueCode = #D 
* #D54 "Reposition/Fixierung/Naht/Gips/prothetische Versorgung"
* #D54 ^property[0].code = #parent 
* #D54 ^property[0].valueCode = #D 
* #D55 "Lokale Injektion/Infiltration"
* #D55 ^property[0].code = #parent 
* #D55 ^property[0].valueCode = #D 
* #D56 "Verband/Druck/Kompression/Tamponade"
* #D56 ^property[0].code = #parent 
* #D56 ^property[0].valueCode = #D 
* #D57 "Physikalische Therapie/Rehabilitation"
* #D57 ^property[0].code = #parent 
* #D57 ^property[0].valueCode = #D 
* #D58 "Therapeutische Beratung/Zuhören"
* #D58 ^property[0].code = #parent 
* #D58 ^property[0].valueCode = #D 
* #D59 "Andere therapeutische Maßnahme/kleine Chirurgie NAK"
* #D59 ^property[0].code = #parent 
* #D59 ^property[0].valueCode = #D 
* #D60 "Testresultat/Ergebnis eigene Maßnahme"
* #D60 ^property[0].code = #parent 
* #D60 ^property[0].valueCode = #D 
* #D61 "Ergebnis Untersuchung/Test/Brief von anderem Leistungserbringer"
* #D61 ^property[0].code = #parent 
* #D61 ^property[0].valueCode = #D 
* #D62 "Adminstrative Maßnahme"
* #D62 ^property[0].code = #parent 
* #D62 ^property[0].valueCode = #D 
* #D63 "Folgekonsultation unspezifiziert"
* #D63 ^property[0].code = #parent 
* #D63 ^property[0].valueCode = #D 
* #D64 "Konsultation/Problembehandlung auf Initiative des Leistungserbringers"
* #D64 ^property[0].code = #parent 
* #D64 ^property[0].valueCode = #D 
* #D65 "Konsultation/Problembehandlung auf Initiative Dritter"
* #D65 ^property[0].code = #parent 
* #D65 ^property[0].valueCode = #D 
* #D66 "Überweisung an andere/n Leistungserbringer (nicht ärztl.)/Pflegekraft/Therapeuten/Sozialarbeiter"
* #D66 ^property[0].code = #parent 
* #D66 ^property[0].valueCode = #D 
* #D67 "Überweisung an einen Arzt/Spezialisten/eine Klinik/ein Krankenhaus"
* #D67 ^property[0].code = #parent 
* #D67 ^property[0].valueCode = #D 
* #D68 "Andere Überweisung NAK"
* #D68 ^property[0].code = #parent 
* #D68 ^property[0].valueCode = #D 
* #D69 "Anderer Beratungsanlass NAK"
* #D69 ^property[0].code = #parent 
* #D69 ^property[0].valueCode = #D 
* #D70 "Gastrointestinale Infektion, Erreger gesichert"
* #D70 ^definition = Inklusive: gastrointestinale Infektion mit spezifischen Mikroorganismen wie Campylobacter~ Giardia~ Salmonellen~ Shigellen~ Typhus~ Cholera Exklusive: Kontakte mit/Träger von ansteckenden/parasitären Krankheiten A99~ vermutete oder nicht spezifizierte Infektionen des Verdauungstraktes D73 Kriterien: Ein Patientmit Symptomen und Isolation oder serologischem Nachweis pathogener Bakterien
* #D70 ^property[0].code = #parent 
* #D70 ^property[0].valueCode = #D 
* #D70 ^property[1].code = #Relationships 
* #D70 ^property[1].valueString = "ICD-10 2017:A00.0~A00.1~A00.9~A01.0~A01.1~A01.2~A01.3~A01.4~A02.0~A02.1~A02.2~A02.8~A02.9~A03.0~A03.1~A03.2~A03.3~A03.8~A03.9~A04.0~A04.1~A04.2~A04.3~A04.4~A04.5~A04.6~A04.7~A04.8~A04.9~A05.0~A05.1~A05.2~A05.3~A05.4~A05.8~A05.9~A06.0~A06.1~A06.2~A06.3~A06.4~A06.5~A06.6~A06.7~A06.8~A06.9~A07.0~A07.1~A07.2~A07.3~A07.8~A07.9~A08.0~A08.1~A08.2~A08.3~A08.4~A08.5" 
* #D70 ^property[2].code = #hints 
* #D70 ^property[2].valueString = "Beachte: Gastroenteritis angenommene Infektion D73 Hinweise: " 
* #D71 "Mumps"
* #D71 ^definition = Inklusive: Mumps mit Meningitis/Orchitis/Pancreatitis Exklusive:  Kriterien: Akute, nicht-eintrige, nicht erythematöse, diffuse, druckempfindliche Entzündung einer oder mehrerer Speicheldrüsen; oder Nachweis einer akuten Mumpsinfektion mit Kultur oder Serologie; oder Orchitis bei einer Person, die einem Kontakt mit Mumps ausgesetzt war, nach Verstreichen der entsprechenden Inkubationszeit
* #D71 ^property[0].code = #parent 
* #D71 ^property[0].valueCode = #D 
* #D71 ^property[1].code = #Relationships 
* #D71 ^property[1].valueString = "ICD-10 2017:B26.0~B26.1~B26.2~B26.3~B26.8~B26.9" 
* #D71 ^property[2].code = #hints 
* #D71 ^property[2].valueString = "Beachte: Schwellung A08 Hinweise: " 
* #D72 "Virushepatitis"
* #D72 ^definition = Inklusive: alle Hepatitiden mit angenommener viraler Genese~ chronisch aktive Hepatitis Exklusive: Hepatitis NNB D97 Kriterien: Hinweise auf virale Infektion mit Leberentzündung, mit oder ohne Gelbsucht; oder serologischer Nachweis einer Infektion mit einem Hepatitis-Virus
* #D72 ^property[0].code = #parent 
* #D72 ^property[0].valueCode = #D 
* #D72 ^property[1].code = #Relationships 
* #D72 ^property[1].valueString = "ICD-10 2017:B15.0~B15.9~B16.0~B16.1~B16.2~B16.9~B17.0~B17.1~B17.2~B17.8~B17.9~B18.0~B18.1~B18.2~B18.8~B18.9~B19.0~B19.9" 
* #D72 ^property[2].code = #hints 
* #D72 ^property[2].valueString = "Beachte: Gelbsucht D13~ Lebervergrößerung D23 Hinweise: " 
* #D73 "Gastroenteritis, vermutlich infektiös"
* #D73 ^definition = Inklusive: Diarrhoe/Erbrechen mit angenommener infektiöser Genese~ Dysenterie NNB~ Lebensmittelvergiftung~ Magen-Darm-Grippe Exklusive: Reizdarmsyndrom D93~ nicht-infektiöse Enteritis und Gastroenteritis D94~ D99 Kriterien:
* #D73 ^property[0].code = #parent 
* #D73 ^property[0].valueCode = #D 
* #D73 ^property[1].code = #Relationships 
* #D73 ^property[1].valueString = "ICD-10 2017:A09.0~A09.9" 
* #D74 "Bösartige Neubildung Magen"
* #D74 ^definition = Inklusive: Magenkrebs Exklusive:  Kriterien: characteristische histologische Erscheinung
* #D74 ^property[0].code = #parent 
* #D74 ^property[0].valueCode = #D 
* #D74 ^property[1].code = #Relationships 
* #D74 ^property[1].valueString = "ICD-10 2017:C16.0~C16.1~C16.2~C16.3~C16.4~C16.5~C16.6~C16.8~C16.9" 
* #D74 ^property[2].code = #hints 
* #D74 ^property[2].valueString = "Beachte: andere bösartige Neubildung der Verdauungsorgane (wenn Primärtumor unsicher) D77~ gutartige/nicht spezifizierte Neubildung der Verdauungsorgane D78 Hinweise: " 
* #D75 "Bösartige Neubildung Colon/Rektum"
* #D75 ^definition = Inklusive:  Exklusive:  Kriterien: characteristische histologische Erscheinung
* #D75 ^property[0].code = #parent 
* #D75 ^property[0].valueCode = #D 
* #D75 ^property[1].code = #Relationships 
* #D75 ^property[1].valueString = "ICD-10 2017:C18.0~C18.1~C18.2~C18.3~C18.4~C18.5~C18.6~C18.7~C18.8~C18.9~C19~C20~C21.0~C21.1~C21.2~C21.8" 
* #D75 ^property[2].code = #hints 
* #D75 ^property[2].valueString = "Beachte: andere bösartige Neubildung der Verdauungsorgane (wenn Primärtumor unsicher) D77~ gutartige/nicht spezifizierte Neubildung der Verdauungsorgane D78 Hinweise: " 
* #D76 "Bösartige Neubildung Pankreas"
* #D76 ^definition = Inklusive: Pankreaskarzinom Exklusive:  Kriterien: characteristische histologische Erscheinung
* #D76 ^property[0].code = #parent 
* #D76 ^property[0].valueCode = #D 
* #D76 ^property[1].code = #Relationships 
* #D76 ^property[1].valueString = "ICD-10 2017:C25.0~C25.1~C25.2~C25.3~C25.4~C25.7~C25.8~C25.9" 
* #D76 ^property[2].code = #hints 
* #D76 ^property[2].valueString = "Beachte: andere bösartige Neubildung der Verdauungsorgane (wenn Primärtumor unbekannt) D77~ gutartige/nicht spezifizierte Neubildung der Verdauungsorgane D78 Hinweise: " 
* #D77 "Bösartige Neubildung Verdauungssystem, andere/NNB"
* #D77 ^definition = Inklusive: alle anderen primären bösartigen Erkrankungen des Verdauungssystems~ der Gallenblase~ Leberkrebs Exklusive: Sekundäre bösartige Neubildungen von bekannter Lokalisation (nach dem Sitz kodieren)~ von unbekannter Lokalisation A79 Kriterien:
* #D77 ^property[0].code = #parent 
* #D77 ^property[0].valueCode = #D 
* #D77 ^property[1].code = #Relationships 
* #D77 ^property[1].valueString = "ICD-10 2017:C00.0~C00.1~C00.2~C00.3~C00.4~C00.5~C00.6~C00.8~C00.9~C01~C02.0~C02.1~C02.2~C02.3~C02.4~C02.8~C02.9~C03.0~C03.1~C03.9~C04.0~C04.1~C04.8~C04.9~C05.0~C05.1~C05.2~C05.8~C05.9~C06.0~C06.1~C06.2~C06.8~C06.9~C07~C08.0~C08.1~C08.8~C08.9~C14.8~C15.0~C15.1~C15.2~C15.3~C15.4~C15.5~C15.8~C15.9~C17.0~C17.1~C17.2~C17.3~C17.8~C17.9~C22.0~C22.1~C22.2~C22.3~C22.4~C22.7~C22.9~C23~C24.0~C24.1~C24.8~C24.9~C26.0~C26.8~C26.9~C45.1~C45.7~C46.2~C48.0~C48.1~C48.2~C48.8" 
* #D77 ^property[2].code = #hints 
* #D77 ^property[2].valueString = "Beachte: gutartige/nicht spezifizierte Neubildung der Verdauungsorgane D78 Hinweise: " 
* #D78 "Neubildung im Verdauungssystem, benigne/unspezifische"
* #D78 ^definition = Inklusive: gutartige Neubildungen der Verdauungsorgane/Neubildungen der Verdauungsorgane~ die nicht als gut- oder bösartig spezifiziert wurden/Histologie nicht verfügbar~ Polypen des Magens~ Duodenums~ Kolon~ Rektum Exklusive:  Kriterien:
* #D78 ^property[0].code = #parent 
* #D78 ^property[0].valueCode = #D 
* #D78 ^property[1].code = #Relationships 
* #D78 ^property[1].valueString = "ICD-10 2017:D00.0~D00.1~D00.2~D01.0~D01.1~D01.2~D01.3~D01.4~D01.5~D01.7~D01.9~D10.0~D10.1~D10.2~D10.3~D10.4~D10.5~D10.6~D10.7~D10.9~D11.0~D11.7~D11.9~D12.0~D12.1~D12.2~D12.3~D12.4~D12.5~D12.6~D12.7~D12.8~D12.9~D13.0~D13.1~D13.2~D13.3~D13.4~D13.5~D13.6~D13.7~D13.9~D19.1~D20.0~D20.1~D37.0~D37.1~D37.2~D37.3~D37.4~D37.5~D37.6~D37.7~D37.9~D48.3~D48.4~K31.7~K62.0~K62.1~K63.5" 
* #D79 "Fremdkörper im Verdauungssystem"
* #D79 ^definition = Inklusive: Fremdkörper verschluckt/im Verdauungstrakt~ einschließlich Mund~ Ösophagus~ Rektum Exklusive: Fremdkörper im Rachen R78~ Aspiration R87 Kriterien:
* #D79 ^property[0].code = #parent 
* #D79 ^property[0].valueCode = #D 
* #D79 ^property[1].code = #Relationships 
* #D79 ^property[1].valueString = "ICD-10 2017:T18.0~T18.1~T18.2~T18.3~T18.4~T18.5~T18.8~T18.9" 
* #D80 "Verletzung des Verdauungssystems, andere"
* #D80 ^definition = Inklusive: Verletzung eines Organs im Abdomen~ der Zähne~ Zunge Exklusive: mehrfache Organverletzungen A81~ der Organe im Beckenbereich X82~ Y80 Kriterien:
* #D80 ^property[0].code = #parent 
* #D80 ^property[0].valueCode = #D 
* #D80 ^property[1].code = #Relationships 
* #D80 ^property[1].valueString = "ICD-10 2017:S00.5~S01.5~S02.5~S03.2~S10.0~S36.1~S36.2~S36.3~S36.4~S36.5~S36.6~T28.0~T28.1~T28.2~T28.5~T28.6~T28.7" 
* #D81 "Angeborene Anomalie des Verdauungssystems"
* #D81 ^definition = Inklusive: Anomalie der Gallenwege~ Lippenkiefergaumenspalte~ Meckelsches Divertikel~ Megacolon~ Morbus Hirschprung~ Ösophagealatresie~ Pylorusstenose~ Zungenlähmung Exklusive: Hämangiom~ Lymphangiom S81~ angeborene Stoffwechselstörungen T80 Kriterien:
* #D81 ^property[0].code = #parent 
* #D81 ^property[0].valueCode = #D 
* #D81 ^property[1].code = #Relationships 
* #D81 ^property[1].valueString = "ICD-10 2017:K00.5~Q18.0~Q18.1~Q18.2~Q18.3~Q18.4~Q18.5~Q18.6~Q18.7~Q18.8~Q18.9~Q35.1~Q35.3~Q35.5~Q35.7~Q35.9~Q36.0~Q36.1~Q36.9~Q37.0~Q37.1~Q37.2~Q37.3~Q37.4~Q37.5~Q37.8~Q37.9~Q38.0~Q38.1~Q38.2~Q38.3~Q38.4~Q38.5~Q38.6~Q38.7~Q38.8~Q39.0~Q39.1~Q39.2~Q39.3~Q39.4~Q39.5~Q39.6~Q39.8~Q39.9~Q40.0~Q40.1~Q40.2~Q40.3~Q40.8~Q40.9~Q41.0~Q41.1~Q41.2~Q41.8~Q41.9~Q42.0~Q42.1~Q42.2~Q42.3~Q42.8~Q42.9~Q43.0~Q43.1~Q43.2~Q43.3~Q43.4~Q43.5~Q43.6~Q43.7~Q43.8~Q43.9~Q44.0~Q44.1~Q44.2~Q44.3~Q44.4~Q44.5~Q44.6~Q44.7~Q45.0~Q45.1~Q45.2~Q45.3~Q45.8~Q45.9" 
* #D82 "Zahn-/Zahnfleischerkrankung"
* #D82 ^definition = Inklusive: Karies~ Zahnabszess~ Gingivitis~ Malocclusion~ temporomandibuläre Gelenkstörung Exklusive: Zahnen D19~ Probleme mit Zahnersatz D19~ Verletzungen der Zähne und des Zahnfleisches D80~ Plaut-Vincent-Angina D83 Kriterien:
* #D82 ^property[0].code = #parent 
* #D82 ^property[0].valueCode = #D 
* #D82 ^property[1].code = #Relationships 
* #D82 ^property[1].valueString = "ICD-10 2017:K00.0~K00.1~K00.2~K00.3~K00.4~K00.6~K00.8~K00.9~K01.0~K01.1~K02.0~K02.1~K02.2~K02.3~K02.4~K02.5~K02.8~K02.9~K03.0~K03.1~K03.2~K03.3~K03.4~K03.5~K03.9~K04.0~K04.1~K04.2~K04.3~K04.4~K04.5~K04.6~K04.7~K04.8~K04.9~K05.0~K05.1~K05.2~K05.3~K05.4~K05.5~K05.6~K06.0~K06.1~K06.2~K06.8~K06.9~K07.0~K07.1~K07.2~K07.3~K07.4~K07.5~K07.8~K07.9~K08.0~K08.1~K08.2~K08.3~K08.8~K08.9~K09.0~K09.1~K09.8~K09.9~K10.0~K10.2~K10.3~K10.8" 
* #D83 "Mund-/Zungen-/Lippenerkrankung"
* #D83 ^definition = Inklusive: Aphten~ Cheilosis~ Glossitis~ Mukozele~ Soor~ Parotitis~ Speichelstein~ Stomatitis~ Angina ulcerosa Exklusive: Mumps D71~ Verletzungen D80~ Herpes simplex S71 Kriterien:
* #D83 ^property[0].code = #parent 
* #D83 ^property[0].valueCode = #D 
* #D83 ^property[1].code = #Relationships 
* #D83 ^property[1].valueString = "ICD-10 2017:A69.0~A69.1~B00.2~B37.0~K11.0~K11.1~K11.2~K11.3~K11.4~K11.5~K11.6~K11.7~K11.8~K11.9~K12.0~K12.1~K12.2~K12.3~K13.0~K13.2~K13.3~K13.4~K13.5~K13.6~K13.7~K14.0~K14.2~K14.3~K14.4~K14.8~K14.9" 
* #D84 "Speiseröhrenerkrankung"
* #D84 ^definition = Inklusive: Achalasie~ Ösophagusdivertikel~ Mallory-Weiss Syndrom~ Ösophagitis~ Ösophagusulzeration~ Reflux Exklusive: Speiseröhrenkrebs D77~ Hiatushernie D90~ Ösophagusvarizen K99 Kriterien:
* #D84 ^property[0].code = #parent 
* #D84 ^property[0].valueCode = #D 
* #D84 ^property[1].code = #Relationships 
* #D84 ^property[1].valueString = "ICD-10 2017:K20~K21.0~K21.9~K22.0~K22.1~K22.2~K22.3~K22.4~K22.5~K22.6~K22.7~K22.8~K22.9~K23.0~K23.1~K23.8" 
* #D85 "Duodenalulkus"
* #D85 ^definition = Inklusive: blutendes/obstruierendes/perforiertes Ulcus Exklusive:  Kriterien: charakteristische Bildgebung; oder charakteristischer endoskopischer Befund; oder Symptomzunahme bei einem Patienten mit zuvor nachgewiesenem Duodenalulcus
* #D85 ^property[0].code = #parent 
* #D85 ^property[0].valueCode = #D 
* #D85 ^property[1].code = #Relationships 
* #D85 ^property[1].valueString = "ICD-10 2017:K26.0~K26.1~K26.2~K26.3~K26.4~K26.5~K26.6~K26.7~K26.9" 
* #D85 ^property[2].code = #hints 
* #D85 ^property[2].valueString = "Beachte: Sodbrennen D03~ Dyspepsie/Magenverstimmung D07 Hinweise: " 
* #D86 "Peptisches Ulkus, anderes"
* #D86 ^definition = Inklusive: Magen-/Gastrojejunal-/Anastomosenulcus~ akute Erosion Exklusive: Speiseröhrenerkrankung D84~ Duodenalulkus D85 Kriterien: charakteristische Bildgebung; oder charakteristischer endoskopischer Befund; oder Symptomzunahme bei einem Patienten mit zuvor nachgewiesenem Ulcus
* #D86 ^property[0].code = #parent 
* #D86 ^property[0].valueCode = #D 
* #D86 ^property[1].code = #Relationships 
* #D86 ^property[1].valueString = "ICD-10 2017:E16.4~K25.0~K25.1~K25.2~K25.3~K25.4~K25.5~K25.6~K25.7~K25.9~K27.0~K27.1~K27.2~K27.3~K27.4~K27.5~K27.6~K27.7~K27.9~K28.0~K28.1~K28.2~K28.3~K28.4~K28.5~K28.6~K28.7~K28.9" 
* #D86 ^property[2].code = #hints 
* #D86 ^property[2].valueString = "Beachte: Sodbrennen D03~ Dyspepsie/Magenverstimmung D07 Hinweise: " 
* #D87 "Magenfunktionsstörung"
* #D87 ^definition = Inklusive: Akute Magendilatation~ Duodenitis~ Gastritis Exklusive: infektiöse Gastritis oder Enteritis D70~ D73 Kriterien: Durch Untersuchung nachgewiesene Magenfunktionsstörung
* #D87 ^property[0].code = #parent 
* #D87 ^property[0].valueCode = #D 
* #D87 ^property[1].code = #Relationships 
* #D87 ^property[1].valueString = "ICD-10 2017:B98.0~K29.0~K29.1~K29.2~K29.3~K29.4~K29.5~K29.6~K29.7~K29.8~K29.9~K31.0~K31.1~K31.2~K31.3~K31.4~K31.5~K31.6~K31.8~K31.9" 
* #D87 ^property[2].code = #hints 
* #D87 ^property[2].valueString = "Beachte: abdomineller Schmerz D01, D06~ epigastrischer Schmerz D02~ Sodbrennen D03~ Dyspepsie/Magenverstimmung D07~ gas problems (wind) D08~ nausea D09~ vomiting D10~ oesophagitis D84 Hinweise: " 
* #D88 "Appendizitis"
* #D88 ^definition = Inklusive: Appendixabszess/-perforation Exklusive:  Kriterien: Objektiv nachgewiesene Blinddarmentzündung, z.B. durch Operation oder pathologische Untersuchung
* #D88 ^property[0].code = #parent 
* #D88 ^property[0].valueCode = #D 
* #D88 ^property[1].code = #Relationships 
* #D88 ^property[1].valueString = "ICD-10 2017:K35.2~K35.3~K35.8~K36~K37" 
* #D88 ^property[2].code = #hints 
* #D88 ^property[2].valueString = "Beachte: abdomineller Schmerz D01, D06~ Erbrechen D10 Hinweise: " 
* #D89 "Leistenhernie"
* #D89 ^definition = Inklusive:  Exklusive: Hernia femoralis D91 Kriterien: Schwellung/Vorwölbung in der Leistenregion, durch Husten verstärkt; oder Vergrößerung bei Anstrengung; oder in das Abdomen reponierbar; oder Dünndarmileus
* #D89 ^property[0].code = #parent 
* #D89 ^property[0].valueCode = #D 
* #D89 ^property[1].code = #Relationships 
* #D89 ^property[1].valueString = "ICD-10 2017:K40.0~K40.1~K40.2~K40.3~K40.4~K40.9" 
* #D89 ^property[2].code = #hints 
* #D89 ^property[2].valueString = "Beachte: abdomineller Tumor D24 Hinweise: " 
* #D90 "Hiatushernie"
* #D90 ^definition = Inklusive: Zwerchfellhernie Exklusive: Öophagitis~ Reflux D84 Kriterien: charakteristische Bildgebung; oder charakteristischer endoskopischer Befund
* #D90 ^property[0].code = #parent 
* #D90 ^property[0].valueCode = #D 
* #D90 ^property[1].code = #Relationships 
* #D90 ^property[1].valueString = "ICD-10 2017:K44.0~K44.1~K44.9" 
* #D90 ^property[2].code = #hints 
* #D90 ^property[2].valueString = "Beachte: epigastrischer Schmerz D02~ Sodbrennen D03~ Dyspepsie/Magenverstimmung D07 Hinweise: " 
* #D91 "Abdominelle Hernie, andere"
* #D91 ^definition = Inklusive: Schenkel-/Narben-/Nabel-/Bauchwandhernie Exklusive: Inguinalhernie D89~ Hiatushernie D90 Kriterien: Intraoperativer Befund; oder Schwellung/Vorwölbung in der angegebenen Region, durch Husten verstärkt; oder Vergrößerung bei Anstrengung; oder in das Abdomen reponierbar; oder Dünndarmileus
* #D91 ^property[0].code = #parent 
* #D91 ^property[0].valueCode = #D 
* #D91 ^property[1].code = #Relationships 
* #D91 ^property[1].valueString = "ICD-10 2017:K41.0~K41.1~K41.2~K41.3~K41.4~K41.9~K42.0~K42.1~K42.9~K43.0~K43.1~K43.2~K43.3~K43.4~K43.5~K43.6~K43.7~K43.9~K45.0~K45.1~K45.8~K46.0~K46.1~K46.9" 
* #D91 ^property[2].code = #hints 
* #D91 ^property[2].valueString = "Beachte: abdomineller Tumor D24 Hinweise: " 
* #D92 "Divertikulose/Divertikulitis"
* #D92 ^definition = Inklusive: Darmdivertikulitis/-divertikulose Exklusive: Meckel`sches Divertikel D81~ Divertikel der Speiseröhre D84 Kriterien: Divertikelnachweis durch bildgebendes Verfahren; oder Demonstration eines Divertikels intraoperativ; oder akute abdominelle Schmerzen mit Fieber und druckempfindlich tastbares Colon descendens/sigmoideum
* #D92 ^property[0].code = #parent 
* #D92 ^property[0].valueCode = #D 
* #D92 ^property[1].code = #Relationships 
* #D92 ^property[1].valueString = "ICD-10 2017:K57.0~K57.1~K57.2~K57.3~K57.4~K57.5~K57.8~K57.9" 
* #D92 ^property[2].code = #hints 
* #D92 ^property[2].valueString = "Beachte: abdomineller Schmerz D01~ D06 Hinweise: " 
* #D93 "Reizdarmsyndrom"
* #D93 ^definition = Inklusive: Schleimhautcolitis~ spastisches Colon Exklusive: Darmerkrankung infektiösen Ursprungs D70~ D73~ regionale Enteritis D94~ allergische diätetische und toxische Gastroenteritis und Colitis D99~ vaskuläre Insuffizienz des Darms D99~ psychogene Diarrhoe P75 Kriterien: Ständige oder intermittierende Schmerzen im Abdomen und diffuse Darmbeschwerden über einen längeren Zeitraum; vermehrte Gasbildung, oder druckempfindlich tastbares Colon, oder Anamnese von Schleim ohne Blut im Stuhl
* #D93 ^property[0].code = #parent 
* #D93 ^property[0].valueCode = #D 
* #D93 ^property[1].code = #Relationships 
* #D93 ^property[1].valueString = "ICD-10 2017:K58.0~K58.9" 
* #D93 ^property[2].code = #hints 
* #D93 ^property[2].valueString = "Beachte: abdomineller Schmerz D01~ D06~ Blähungen D08~ Diarrhoe D11~ Verstopfung D12 Hinweise: " 
* #D94 "Chronisch entzündliche Darmerkrankung/Colitis ulcerosa"
* #D94 ^definition = Inklusive: Morbus Crohn~ regionale Enteritis~ Colitis ulcerosa Exklusive:  Kriterien: characteristischer endoskopischer/radiologischer/histologischer Befund
* #D94 ^property[0].code = #parent 
* #D94 ^property[0].valueCode = #D 
* #D94 ^property[1].code = #Relationships 
* #D94 ^property[1].valueString = "ICD-10 2017:K50.0~K50.1~K50.8~K50.9~K51.0~K51.2~K51.3~K51.4~K51.5~K51.8~K51.9" 
* #D94 ^property[2].code = #hints 
* #D94 ^property[2].valueString = "Beachte: abdomineller Schmerz D01~ D06~ Diarrhoe D11~ Colica mucosa D93 Hinweise: " 
* #D95 "Analfissur/perianaler Abszess"
* #D95 ^definition = Inklusive: Analfistel~ ischiorektaler Abszess Exklusive: Pilonidalabszess S85 Kriterien:
* #D95 ^property[0].code = #parent 
* #D95 ^property[0].valueCode = #D 
* #D95 ^property[1].code = #Relationships 
* #D95 ^property[1].valueString = "ICD-10 2017:K60.0~K60.1~K60.2~K60.3~K60.4~K60.5~K61.0~K61.1~K61.2~K61.3~K61.4" 
* #D96 "Würmer/andere Parasiten"
* #D96 ^definition = Inklusive: Wurmbefall~ kutane Larva migrans~ nicht spezifizierte intestinale Parasiten~ Trichinenbefall~ Echinococcuszysten Exklusive:  Kriterien: Nachweis von Helminthen in adulter Form, Larven oder Eiern; oder positiver Hauttest; oder positive Serologie
* #D96 ^property[0].code = #parent 
* #D96 ^property[0].valueCode = #D 
* #D96 ^property[1].code = #Relationships 
* #D96 ^property[1].valueString = "ICD-10 2017:B65.0~B65.1~B65.2~B65.3~B65.8~B65.9~B66.0~B66.1~B66.2~B66.3~B66.4~B66.5~B66.8~B66.9~B67.0~B67.1~B67.2~B67.3~B67.4~B67.5~B67.6~B67.7~B67.8~B67.9~B68.0~B68.1~B68.9~B69.0~B69.1~B69.8~B69.9~B70.0~B70.1~B71.0~B71.1~B71.8~B71.9~B72~B73~B74.0~B74.1~B74.2~B74.3~B74.4~B74.8~B74.9~B75~B76.0~B76.1~B76.8~B76.9~B77.0~B77.8~B77.9~B78.0~B78.1~B78.7~B78.9~B79~B80~B81.0~B81.1~B81.2~B81.3~B81.4~B81.8~B82.0~B82.9~B83.0~B83.1~B83.2~B83.3~B83.4~B83.8~B83.9" 
* #D97 "Lebererkrankung NNB"
* #D97 ^definition = Inklusive: alkoholbedingte Hepatitis~ Leberzirrhose~ Fettleber~ Hepatitis NNB~ Leberversagen~ portale Hypertension Exklusive: virale Hepatitis D72~ Hydatidenbefall D96 Kriterien:
* #D97 ^property[0].code = #parent 
* #D97 ^property[0].valueCode = #D 
* #D97 ^property[1].code = #Relationships 
* #D97 ^property[1].valueString = "ICD-10 2017:B58.1~B94.2~K70.0~K70.1~K70.2~K70.3~K70.4~K70.9~K71.0~K71.1~K71.2~K71.3~K71.4~K71.5~K71.6~K71.7~K71.8~K71.9~K72.0~K72.1~K72.9~K73.0~K73.1~K73.2~K73.8~K73.9~K74.0~K74.1~K74.2~K74.3~K74.4~K74.5~K74.6~K75.0~K75.1~K75.2~K75.3~K75.4~K75.8~K75.9~K76.0~K76.1~K76.2~K76.3~K76.4~K76.5~K76.6~K76.7~K76.8~K76.9~K77.0~K77.8" 
* #D98 "Cholezystitis/Cholelithiasis"
* #D98 ^definition = Inklusive: Gallenkolik~ Cholangitis~ Gallensteine Exklusive:  Kriterien: Cholezystitis: Nachweis eines typischen Befundes durch Ultraschall oder intraoperativ; oder lokalisierte Druckempfindlichkeit im rechten oberen Quadranten und Gelbsucht oder Fieber oder Gallensteine in der Anamnese, Cholelithiasis: Gallensteinnachweis intraoperativ oder in der Bildgebung, akute Gallenkolik: akute kolikartige Abdominalschmerzen im rechten oberen Quadranten ohne Fieber; und Gelbsucht oder Abwehrspannung im rechten oberen abdominellen Quadranten, oder Gallensteine in der Anamnese
* #D98 ^property[0].code = #parent 
* #D98 ^property[0].valueCode = #D 
* #D98 ^property[1].code = #Relationships 
* #D98 ^property[1].valueString = "ICD-10 2017:K80.0~K80.1~K80.2~K80.3~K80.4~K80.5~K80.8~K81.0~K81.1~K81.8~K81.9~K82.0~K82.1~K82.2~K82.3~K82.4~K82.8~K82.9~K83.0~K83.1~K83.2~K83.3~K83.4~K83.5~K83.8~K83.9~K87.0" 
* #D98 ^property[2].code = #hints 
* #D98 ^property[2].valueString = "Beachte: umschriebener abdomineller Schmerz D06 Hinweise: " 
* #D99 "Andere Erkrankung des Verdauungssystems"
* #D99 ^definition = Inklusive: Adhäsionen~ Zöliakie~ Sprue~ Dumping Syndrom~ Lebensmittelintoleranz~ allergische/toxische/diätetische Gastroenteropathie~ Ileus~ Darmobstruktion~ Invagination~ Laktoseintoleranz~ Malabsorptionssyndrom~ mesenteriale Gefäßerkrankung~ Pankreaserkrankung~ Pankreatitis Exklusive: durch Antibiotika verursachte Colitis A85~ bösartige Erkrankungen D74-D77 Kriterien:
* #D99 ^property[0].code = #parent 
* #D99 ^property[0].valueCode = #D 
* #D99 ^property[1].code = #Relationships 
* #D99 ^property[1].valueString = "ICD-10 2017:K38.0~K38.1~K38.2~K38.3~K38.8~K38.9~K52.1~K52.2~K52.3~K52.8~K52.9~K55.0~K55.1~K55.2~K55.8~K55.9~K56.0~K56.1~K56.2~K56.3~K56.5~K56.6~K56.7~K59.2~K59.3~K59.8~K59.9~K62.2~K62.3~K62.4~K62.6~K62.7~K62.8~K62.9~K63.0~K63.1~K63.2~K63.3~K63.4~K63.8~K63.9~K65.0~K65.8~K65.9~K66.0~K66.1~K66.8~K66.9~K67.0~K67.1~K67.2~K67.3~K67.8~K85.0~K85.1~K85.2~K85.3~K85.8~K85.9~K86.0~K86.1~K86.2~K86.3~K86.8~K86.9~K87.1~K90.0~K90.1~K90.2~K90.3~K90.4~K90.8~K90.9~K91.1~K91.2~K91.5~K91.8~K91.9~K92.2~K92.8~K92.9~K93.0~K93.1~K93.8~Z90.3~Z90.4~Z98.0" 
* #F "Auge"
* #F ^property[0].code = #child 
* #F ^property[0].valueCode = #F01 
* #F ^property[1].code = #child 
* #F ^property[1].valueCode = #F02 
* #F ^property[2].code = #child 
* #F ^property[2].valueCode = #F03 
* #F ^property[3].code = #child 
* #F ^property[3].valueCode = #F04 
* #F ^property[4].code = #child 
* #F ^property[4].valueCode = #F05 
* #F ^property[5].code = #child 
* #F ^property[5].valueCode = #F13 
* #F ^property[6].code = #child 
* #F ^property[6].valueCode = #F14 
* #F ^property[7].code = #child 
* #F ^property[7].valueCode = #F15 
* #F ^property[8].code = #child 
* #F ^property[8].valueCode = #F16 
* #F ^property[9].code = #child 
* #F ^property[9].valueCode = #F17 
* #F ^property[10].code = #child 
* #F ^property[10].valueCode = #F18 
* #F ^property[11].code = #child 
* #F ^property[11].valueCode = #F27 
* #F ^property[12].code = #child 
* #F ^property[12].valueCode = #F28 
* #F ^property[13].code = #child 
* #F ^property[13].valueCode = #F29 
* #F ^property[14].code = #child 
* #F ^property[14].valueCode = #F30 
* #F ^property[15].code = #child 
* #F ^property[15].valueCode = #F31 
* #F ^property[16].code = #child 
* #F ^property[16].valueCode = #F32 
* #F ^property[17].code = #child 
* #F ^property[17].valueCode = #F33 
* #F ^property[18].code = #child 
* #F ^property[18].valueCode = #F34 
* #F ^property[19].code = #child 
* #F ^property[19].valueCode = #F35 
* #F ^property[20].code = #child 
* #F ^property[20].valueCode = #F36 
* #F ^property[21].code = #child 
* #F ^property[21].valueCode = #F37 
* #F ^property[22].code = #child 
* #F ^property[22].valueCode = #F38 
* #F ^property[23].code = #child 
* #F ^property[23].valueCode = #F39 
* #F ^property[24].code = #child 
* #F ^property[24].valueCode = #F40 
* #F ^property[25].code = #child 
* #F ^property[25].valueCode = #F41 
* #F ^property[26].code = #child 
* #F ^property[26].valueCode = #F42 
* #F ^property[27].code = #child 
* #F ^property[27].valueCode = #F43 
* #F ^property[28].code = #child 
* #F ^property[28].valueCode = #F44 
* #F ^property[29].code = #child 
* #F ^property[29].valueCode = #F45 
* #F ^property[30].code = #child 
* #F ^property[30].valueCode = #F46 
* #F ^property[31].code = #child 
* #F ^property[31].valueCode = #F47 
* #F ^property[32].code = #child 
* #F ^property[32].valueCode = #F48 
* #F ^property[33].code = #child 
* #F ^property[33].valueCode = #F49 
* #F ^property[34].code = #child 
* #F ^property[34].valueCode = #F50 
* #F ^property[35].code = #child 
* #F ^property[35].valueCode = #F51 
* #F ^property[36].code = #child 
* #F ^property[36].valueCode = #F52 
* #F ^property[37].code = #child 
* #F ^property[37].valueCode = #F53 
* #F ^property[38].code = #child 
* #F ^property[38].valueCode = #F54 
* #F ^property[39].code = #child 
* #F ^property[39].valueCode = #F55 
* #F ^property[40].code = #child 
* #F ^property[40].valueCode = #F56 
* #F ^property[41].code = #child 
* #F ^property[41].valueCode = #F57 
* #F ^property[42].code = #child 
* #F ^property[42].valueCode = #F58 
* #F ^property[43].code = #child 
* #F ^property[43].valueCode = #F59 
* #F ^property[44].code = #child 
* #F ^property[44].valueCode = #F60 
* #F ^property[45].code = #child 
* #F ^property[45].valueCode = #F61 
* #F ^property[46].code = #child 
* #F ^property[46].valueCode = #F62 
* #F ^property[47].code = #child 
* #F ^property[47].valueCode = #F63 
* #F ^property[48].code = #child 
* #F ^property[48].valueCode = #F64 
* #F ^property[49].code = #child 
* #F ^property[49].valueCode = #F65 
* #F ^property[50].code = #child 
* #F ^property[50].valueCode = #F66 
* #F ^property[51].code = #child 
* #F ^property[51].valueCode = #F67 
* #F ^property[52].code = #child 
* #F ^property[52].valueCode = #F68 
* #F ^property[53].code = #child 
* #F ^property[53].valueCode = #F69 
* #F ^property[54].code = #child 
* #F ^property[54].valueCode = #F70 
* #F ^property[55].code = #child 
* #F ^property[55].valueCode = #F71 
* #F ^property[56].code = #child 
* #F ^property[56].valueCode = #F72 
* #F ^property[57].code = #child 
* #F ^property[57].valueCode = #F73 
* #F ^property[58].code = #child 
* #F ^property[58].valueCode = #F74 
* #F ^property[59].code = #child 
* #F ^property[59].valueCode = #F75 
* #F ^property[60].code = #child 
* #F ^property[60].valueCode = #F76 
* #F ^property[61].code = #child 
* #F ^property[61].valueCode = #F79 
* #F ^property[62].code = #child 
* #F ^property[62].valueCode = #F80 
* #F ^property[63].code = #child 
* #F ^property[63].valueCode = #F81 
* #F ^property[64].code = #child 
* #F ^property[64].valueCode = #F82 
* #F ^property[65].code = #child 
* #F ^property[65].valueCode = #F83 
* #F ^property[66].code = #child 
* #F ^property[66].valueCode = #F84 
* #F ^property[67].code = #child 
* #F ^property[67].valueCode = #F85 
* #F ^property[68].code = #child 
* #F ^property[68].valueCode = #F86 
* #F ^property[69].code = #child 
* #F ^property[69].valueCode = #F91 
* #F ^property[70].code = #child 
* #F ^property[70].valueCode = #F92 
* #F ^property[71].code = #child 
* #F ^property[71].valueCode = #F93 
* #F ^property[72].code = #child 
* #F ^property[72].valueCode = #F94 
* #F ^property[73].code = #child 
* #F ^property[73].valueCode = #F95 
* #F ^property[74].code = #child 
* #F ^property[74].valueCode = #F99 
* #F01 "Augenschmerz"
* #F01 ^definition = Inklusive:  Exklusive: Abnorme/ungewöhnliche Empfindungen im Auge F13 Kriterien:
* #F01 ^property[0].code = #parent 
* #F01 ^property[0].valueCode = #F 
* #F01 ^property[1].code = #Relationships 
* #F01 ^property[1].valueString = "ICD-10 2017:H57.1" 
* #F02 "Augenrötung"
* #F02 ^definition = Inklusive: blutunterlaufenes/entzündetes Auge Exklusive:  Kriterien:
* #F02 ^property[0].code = #parent 
* #F02 ^property[0].valueCode = #F 
* #F02 ^property[1].code = #Relationships 
* #F02 ^property[1].valueString = "ICD-10 2017:H57.8" 
* #F03 "Augenausfluss/-absonderung"
* #F03 ^definition = Inklusive: Tränenfluss~ eitrige Absonderung~ wässriges Auge Exklusive:  Kriterien:
* #F03 ^property[0].code = #parent 
* #F03 ^property[0].valueCode = #F 
* #F03 ^property[1].code = #Relationships 
* #F03 ^property[1].valueString = "ICD-10 2017:H04.2" 
* #F04 "Mouches volantes"
* #F04 ^definition = Inklusive: fixierte/schwimmende Flecken im Gesichtsfeld Exklusive: Sehstörungen F05 Kriterien:
* #F04 ^property[0].code = #parent 
* #F04 ^property[0].valueCode = #F 
* #F04 ^property[1].code = #Relationships 
* #F04 ^property[1].valueString = "ICD-10 2017:H53.1" 
* #F05 "Sehstörung, andere"
* #F05 ^definition = Inklusive: Gesichtsfeldtrübung~ Leseschwierigkeiten~ Doppelbilder~ Augenschmerz~ Photophobie~ Skotom und Blendung bei auf das Auge beschränkter Symptomatik~ temporäre Blindheit NNB~ Sehverlust~ Sehschwäche Exklusive: Blindheit in einem Auge F28~ Schneeblindheit F79~ Refraktionsfehler F91~ permanente Blindheit F94~ Farbenblindheit F99~ Nachtblindheit F99 Kriterien:
* #F05 ^property[0].code = #parent 
* #F05 ^property[0].valueCode = #F 
* #F05 ^property[1].code = #Relationships 
* #F05 ^property[1].valueString = "ICD-10 2017:H53.1~H53.2~H53.3~H53.8~H53.9" 
* #F13 "Auge Empfindungsstörung"
* #F13 ^definition = Inklusive: brennendes/trockenes/juckendes Auge Exklusive: Augenschmerzen F01 Kriterien:
* #F13 ^property[0].code = #parent 
* #F13 ^property[0].valueCode = #F 
* #F13 ^property[1].code = #Relationships 
* #F13 ^property[1].valueString = "ICD-10 2017:H57.8" 
* #F14 "Auge abnorme Bewegung"
* #F14 ^definition = Inklusive: abnormes Blinzeln~ träges Auge~ Nystagmus Exklusive: Schielen F95~ Zucken N08~ Tic am Auge P10 Kriterien:
* #F14 ^property[0].code = #parent 
* #F14 ^property[0].valueCode = #F 
* #F14 ^property[1].code = #Relationships 
* #F14 ^property[1].valueString = "ICD-10 2017:H55" 
* #F15 "Auge abnormes Aussehen"
* #F15 ^definition = Inklusive: veränderte Augenfarbe~ geschwollenes Auge Exklusive: rote Augen F02 Kriterien:
* #F15 ^property[0].code = #parent 
* #F15 ^property[0].valueCode = #F 
* #F15 ^property[1].code = #Relationships 
* #F15 ^property[1].valueString = "ICD-10 2017:H57.8" 
* #F16 "Augenlid Symptome/Beschwerden"
* #F16 ^definition = Inklusive: Ptosis des Augenlids Exklusive: entzündete Augenlider F72 Kriterien:
* #F16 ^property[0].code = #parent 
* #F16 ^property[0].valueCode = #F 
* #F16 ^property[1].code = #Relationships 
* #F16 ^property[1].valueString = "ICD-10 2017:H02.2~H02.3~H02.4~H02.5~H02.6~H02.7" 
* #F17 "Brille Symptome/Beschwerden"
* #F17 ^definition = Inklusive: Probleme wegen Brille~ die Struktur~ Funktion oder Empfinden des Auges beeinflussen Exklusive: Probleme mit Kontaktlinsen F18 Kriterien:
* #F17 ^property[0].code = #parent 
* #F17 ^property[0].valueCode = #F 
* #F17 ^property[1].code = #Relationships 
* #F17 ^property[1].valueString = "ICD-10 2017:Z46.0" 
* #F18 "Kontaktlinsen Symptome/Beschwerden"
* #F18 ^definition = Inklusive: Probleme durch Kontaktlinse~ die die Struktur~ Funktion oder Empfindung des(r) Auges(n) betreffen Exklusive:  Kriterien:
* #F18 ^property[0].code = #parent 
* #F18 ^property[0].valueCode = #F 
* #F18 ^property[1].code = #Relationships 
* #F18 ^property[1].valueString = "ICD-10 2017:Z46.0" 
* #F27 "Angst vor Augenerkrankung"
* #F27 ^definition = Inklusive: Angst vor Erblindung Exklusive: wenn der Patient die Erkrankung hat~ kodiere die Erkrankung Kriterien: Besorgnis über/Angst vor Augenerkrankung bei einem Patienten, der die Krankheit nicht hat/bevor die Diagnose gesichert ist
* #F27 ^property[0].code = #parent 
* #F27 ^property[0].valueCode = #F 
* #F27 ^property[1].code = #Relationships 
* #F27 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #F28 "Funktionseinschränkung/Behinderung (F)"
* #F28 ^definition = Inklusive: Erblindung eines Auges Exklusive: Blindheit F94 Kriterien: Funktionseinschränkung/Behinderung durch Visusproblem der Augen/eines Auges
* #F28 ^property[0].code = #parent 
* #F28 ^property[0].valueCode = #F 
* #F28 ^property[1].code = #Relationships 
* #F28 ^property[1].valueString = "ICD-10 2017:H54.4~H54.5~H54.6~H54.9~Z73.6" 
* #F28 ^property[2].code = #hints 
* #F28 ^property[2].valueString = "Beachte:  Hinweise: COOP/WONCA Charts eignen sich zur Dokumentation des funktionellen Status des Patienten (siehe Kapitel 8)" 
* #F29 "Andere Augensymptome/-beschwerden"
* #F29 ^property[0].code = #parent 
* #F29 ^property[0].valueCode = #F 
* #F29 ^property[1].code = #Relationships 
* #F29 ^property[1].valueString = "ICD-10 2017:H57.8~H57.9" 
* #F30 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? vollständig"
* #F30 ^property[0].code = #parent 
* #F30 ^property[0].valueCode = #F 
* #F31 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? teilweise"
* #F31 ^property[0].code = #parent 
* #F31 ^property[0].valueCode = #F 
* #F32 "Allergie-/Sensitivitätstestung"
* #F32 ^property[0].code = #parent 
* #F32 ^property[0].valueCode = #F 
* #F33 "Mikrobiologische/immunologische Untersuchung"
* #F33 ^property[0].code = #parent 
* #F33 ^property[0].valueCode = #F 
* #F34 "Blutuntersuchung"
* #F34 ^property[0].code = #parent 
* #F34 ^property[0].valueCode = #F 
* #F35 "Urinuntersuchung"
* #F35 ^property[0].code = #parent 
* #F35 ^property[0].valueCode = #F 
* #F36 "Stuhluntersuchung"
* #F36 ^property[0].code = #parent 
* #F36 ^property[0].valueCode = #F 
* #F37 "Histologische Untersuchung/zytologischer Abstrich"
* #F37 ^property[0].code = #parent 
* #F37 ^property[0].valueCode = #F 
* #F38 "Andere Laboruntersuchung NAK"
* #F38 ^property[0].code = #parent 
* #F38 ^property[0].valueCode = #F 
* #F39 "Körperliche Funktionsprüfung"
* #F39 ^property[0].code = #parent 
* #F39 ^property[0].valueCode = #F 
* #F40 "Diagnostische Endoskopie"
* #F40 ^property[0].code = #parent 
* #F40 ^property[0].valueCode = #F 
* #F41 "Diagnostisches Röntgen/Bildgebung"
* #F41 ^property[0].code = #parent 
* #F41 ^property[0].valueCode = #F 
* #F42 "Elektrische Aufzeichnungsverfahren"
* #F42 ^property[0].code = #parent 
* #F42 ^property[0].valueCode = #F 
* #F43 "Andere diagnostische Untersuchung"
* #F43 ^property[0].code = #parent 
* #F43 ^property[0].valueCode = #F 
* #F44 "Präventive Impfung/Medikation"
* #F44 ^property[0].code = #parent 
* #F44 ^property[0].valueCode = #F 
* #F45 "Beobachtung/Gesundheitsschulung/Beratung/Diät"
* #F45 ^property[0].code = #parent 
* #F45 ^property[0].valueCode = #F 
* #F46 "Konsultation eines anderen  Primärersorgers"
* #F46 ^property[0].code = #parent 
* #F46 ^property[0].valueCode = #F 
* #F47 "Konsultation eines Facharztes"
* #F47 ^property[0].code = #parent 
* #F47 ^property[0].valueCode = #F 
* #F48 "Klärung/Besprechung des Beratungsanlasses mit dem Patienten"
* #F48 ^property[0].code = #parent 
* #F48 ^property[0].valueCode = #F 
* #F49 "Andere Vorsorgemaßnahme"
* #F49 ^property[0].code = #parent 
* #F49 ^property[0].valueCode = #F 
* #F50 "Medikation/Verschreibung/Erneuerung/Injektion"
* #F50 ^property[0].code = #parent 
* #F50 ^property[0].valueCode = #F 
* #F51 "Inzision/Drainage/Spülung/Absaugung/Entfernung von Körperflüssigkeit"
* #F51 ^property[0].code = #parent 
* #F51 ^property[0].valueCode = #F 
* #F52 "Exzision/Entfernung von Gewebe/Biopsie/Destruktion/Debridement/Verkauterung"
* #F52 ^property[0].code = #parent 
* #F52 ^property[0].valueCode = #F 
* #F53 "Instrumentelle Manipulation/Katheterisierung/Intubation/Dilatation"
* #F53 ^property[0].code = #parent 
* #F53 ^property[0].valueCode = #F 
* #F54 "Reposition/Fixierung/Naht/Gips/prothetische Versorgung"
* #F54 ^property[0].code = #parent 
* #F54 ^property[0].valueCode = #F 
* #F55 "Lokale Injektion/Infiltration"
* #F55 ^property[0].code = #parent 
* #F55 ^property[0].valueCode = #F 
* #F56 "Verband/Druck/Kompression/Tamponade"
* #F56 ^property[0].code = #parent 
* #F56 ^property[0].valueCode = #F 
* #F57 "Physikalische Therapie/Rehabilitation"
* #F57 ^property[0].code = #parent 
* #F57 ^property[0].valueCode = #F 
* #F58 "Therapeutische Beratung/Zuhören"
* #F58 ^property[0].code = #parent 
* #F58 ^property[0].valueCode = #F 
* #F59 "Andere therapeutische Maßnahme/kleine Chirurgie NAK"
* #F59 ^property[0].code = #parent 
* #F59 ^property[0].valueCode = #F 
* #F60 "Testresultat/Ergebnis eigene Maßnahme"
* #F60 ^property[0].code = #parent 
* #F60 ^property[0].valueCode = #F 
* #F61 "Ergebnis Untersuchung/Test/Brief von anderem Leistungserbringer"
* #F61 ^property[0].code = #parent 
* #F61 ^property[0].valueCode = #F 
* #F62 "Adminstrative Maßnahme"
* #F62 ^property[0].code = #parent 
* #F62 ^property[0].valueCode = #F 
* #F63 "Folgekonsultation unspezifiziert"
* #F63 ^property[0].code = #parent 
* #F63 ^property[0].valueCode = #F 
* #F64 "Konsultation/Problembehandlung auf Initiative des Leistungserbringers"
* #F64 ^property[0].code = #parent 
* #F64 ^property[0].valueCode = #F 
* #F65 "Konsultation/Problembehandlung auf Initiative Dritter"
* #F65 ^property[0].code = #parent 
* #F65 ^property[0].valueCode = #F 
* #F66 "Überweisung an andere/n Leistungserbringer (nicht ärztl.)/Pflegekraft/Therapeuten/Sozialarbeiter"
* #F66 ^property[0].code = #parent 
* #F66 ^property[0].valueCode = #F 
* #F67 "Überweisung an einen Arzt/Spezialisten/eine Klinik/ein Krankenhaus"
* #F67 ^property[0].code = #parent 
* #F67 ^property[0].valueCode = #F 
* #F68 "Andere Überweisung NAK"
* #F68 ^property[0].code = #parent 
* #F68 ^property[0].valueCode = #F 
* #F69 "Anderer Beratungsanlass NAK"
* #F69 ^property[0].code = #parent 
* #F69 ^property[0].valueCode = #F 
* #F70 "Konjunktivitis, infektiöse"
* #F70 ^definition = Inklusive: Bakterielle/virale Konjunktivitis~ Konjunktivitis NNB Exklusive: allergisch mit und ohne Rhinorrhoe F71~ Verbrennung durch Verblitzen F79~ Trachom F86 Kriterien: angenommene oder nachgewiesene bakterielle Konjunktivitis
* #F70 ^property[0].code = #parent 
* #F70 ^property[0].valueCode = #F 
* #F70 ^property[1].code = #Relationships 
* #F70 ^property[1].valueString = "ICD-10 2017:A74.0~B30.0~B30.1~B30.2~B30.3~B30.8~B30.9~H10.0~H10.2~H10.3~H10.4~H10.5~H10.8~H10.9~H13.0~H13.1~H13.2~H13.3~H13.8" 
* #F71 "Konjunktivitis, allergische"
* #F71 ^definition = Inklusive: allergische Konjunktivitis mit/ohne Schnupfen Exklusive: bakterielle und virale Konjunktivitis F70~ Verbrennung durch Verblitzen F79~ Trachom F86 Kriterien: angenommene oder nachgewiesene allergische Hyperämie der Bindehaut, überschießendes Augentränen, Jucken/ödem der Konjunktiven
* #F71 ^property[0].code = #parent 
* #F71 ^property[0].valueCode = #F 
* #F71 ^property[1].code = #Relationships 
* #F71 ^property[1].valueString = "ICD-10 2017:H10.1" 
* #F72 "Blepharitis/Hagelkorn/Gerstenkorn"
* #F72 ^definition = Inklusive: Dermatitis/Dermatose der Augenlider~ Augenlidinfektion~ Hordeolum~ Meibomsche Zyste~ Tarsalzyste Exklusive: Dakryozystitis F73 Kriterien: Generalisierte oder lokalisierte Entzündung und/oder Schwellung der Augenlider einschließlichder Meibomschen Drüsen
* #F72 ^property[0].code = #parent 
* #F72 ^property[0].valueCode = #F 
* #F72 ^property[1].code = #Relationships 
* #F72 ^property[1].valueString = "ICD-10 2017:H00.0~H00.1~H01.0~H01.1~H01.8~H01.9" 
* #F73 "Augeninfektion/-entzündung, andere"
* #F73 ^definition = Inklusive: Tränendrüsenentzündung~ Herpes simplex Infektion des Auges ohne Cornealulkus~ Orbitaentzündung~ Iritis~ Iridozyclitis~ Keratitis Exklusive: Keratitis bei Masern A71~ Ulcus Corneae (Herpes) F85~ Trachom F86~ Zoster S70 Kriterien:
* #F73 ^property[0].code = #parent 
* #F73 ^property[0].valueCode = #F 
* #F73 ^property[1].code = #Relationships 
* #F73 ^property[1].valueString = "ICD-10 2017:B00.5~B58.0~H03.0~H03.1~H03.8~H04.3~H04.4~H05.0~H05.1~H16.1~H16.2~H16.3~H16.4~H16.8~H16.9~H20.0~H20.1~H20.2~H20.8~H20.9~H21.0~H21.1~H21.2~H21.3~H21.4~H21.5~H21.8~H21.9~H22.0~H22.1~H22.8~H30.0~H30.1~H30.2~H30.8~H30.9~H32.0~H32.8~H44.0~H44.1" 
* #F74 "Neubildung Auge/Anhangsgebilde"
* #F74 ^definition = Inklusive: Gutartige/bösartige Neubildung des Auges/der Adnexen Exklusive:  Kriterien:
* #F74 ^property[0].code = #parent 
* #F74 ^property[0].valueCode = #F 
* #F74 ^property[1].code = #Relationships 
* #F74 ^property[1].valueString = "ICD-10 2017:C69.0~C69.1~C69.2~C69.3~C69.4~C69.5~C69.6~C69.8~C69.9~D09.2~D31.0~D31.1~D31.2~D31.3~D31.4~D31.5~D31.6~D31.9~D48.7" 
* #F75 "Kontusion/Blutung im Auge"
* #F75 ^definition = Inklusive: blaues Auge~ Hyphäma~ subkonjunktivale Blutung Exklusive: Ulcus corneae F85 Kriterien:
* #F75 ^property[0].code = #parent 
* #F75 ^property[0].valueCode = #F 
* #F75 ^property[1].code = #Relationships 
* #F75 ^property[1].valueString = "ICD-10 2017:H11.3~S00.1~S05.1" 
* #F76 "Fremdkörper im Auge"
* #F76 ^definition = Inklusive:  Exklusive: Hornhautabrasion F 79 Kriterien:
* #F76 ^property[0].code = #parent 
* #F76 ^property[0].valueCode = #F 
* #F76 ^property[1].code = #Relationships 
* #F76 ^property[1].valueString = "ICD-10 2017:T15.0~T15.1~T15.8~T15.9" 
* #F79 "Augenverletzung, andere"
* #F79 ^definition = Inklusive: Hornhautabrasion~ Verbrennung durch Verblitzen~ Schneeblindheit Exklusive: Kontusion/Blutung F75~ Fremdkörper F76 Kriterien:
* #F79 ^property[0].code = #parent 
* #F79 ^property[0].valueCode = #F 
* #F79 ^property[1].code = #Relationships 
* #F79 ^property[1].valueString = "ICD-10 2017:H16.1~H44.6~H44.7~S00.2~S01.1~S05.0~S05.2~S05.3~S05.4~S05.5~S05.6~S05.7~S05.8~S05.9~S09.9~T26.0~T26.1~T26.2~T26.3~T26.4~T26.5~T26.6~T26.7~T26.8~T26.9" 
* #F80 "Tränenkanalverschluss beim Kleinkind"
* #F80 ^definition = Inklusive:  Exklusive: Dakryozystitis F73~ bei älteren Patienten F99 Kriterien: Tränenausfluß ohne Weinen, der vor dem 3. Lebensmonat besteht
* #F80 ^property[0].code = #parent 
* #F80 ^property[0].valueCode = #F 
* #F80 ^property[1].code = #Relationships 
* #F80 ^property[1].valueString = "ICD-10 2017:Q10.5" 
* #F81 "Angeborene Anomalie Auge, andere"
* #F81 ^property[0].code = #parent 
* #F81 ^property[0].valueCode = #F 
* #F81 ^property[1].code = #Relationships 
* #F81 ^property[1].valueString = "ICD-10 2017:Q10.0~Q10.1~Q10.2~Q10.3~Q10.4~Q10.6~Q10.7~Q11.0~Q11.1~Q11.2~Q11.3~Q12.0~Q12.1~Q12.2~Q12.3~Q12.4~Q12.8~Q12.9~Q13.0~Q13.1~Q13.2~Q13.3~Q13.4~Q13.5~Q13.8~Q13.9~Q14.0~Q14.1~Q14.2~Q14.3~Q14.8~Q14.9~Q15.0~Q15.8~Q15.9" 
* #F82 "Netzhautablösung"
* #F82 ^property[0].code = #parent 
* #F82 ^property[0].valueCode = #F 
* #F82 ^property[1].code = #Relationships 
* #F82 ^property[1].valueString = "ICD-10 2017:H33.0~H33.1~H33.2~H33.3~H33.4~H33.5" 
* #F83 "Retinopathie"
* #F83 ^definition = Inklusive: Rehinopathie infolge Diabetes oder Hypertonie~ sonstige Exklusive:  Kriterien:
* #F83 ^property[0].code = #parent 
* #F83 ^property[0].valueCode = #F 
* #F83 ^property[1].code = #Relationships 
* #F83 ^property[1].valueString = "ICD-10 2017:H35.0~H35.1~H35.2~H35.4~H36.0~H36.8" 
* #F83 ^property[2].code = #hints 
* #F83 ^property[2].valueString = "Beachte:  Hinweise: Die auslösende Erkrankung ist zusätzlich zu kodieren, z.B. Diabetes T89, T90 oder Bluthochdruck K87" 
* #F84 "Maculadegeneration"
* #F84 ^definition = Inklusive:  Exklusive: Retinopathie F83 Kriterien:
* #F84 ^property[0].code = #parent 
* #F84 ^property[0].valueCode = #F 
* #F84 ^property[1].code = #Relationships 
* #F84 ^property[1].valueString = "ICD-10 2017:H35.3" 
* #F85 "Cornealulcus"
* #F85 ^definition = Inklusive: Ulcus dendriticum~ virale Keratitis Exklusive: Hornhautabrasion F79~ sonstige Verletzung F79 Kriterien:
* #F85 ^property[0].code = #parent 
* #F85 ^property[0].valueCode = #F 
* #F85 ^property[1].code = #Relationships 
* #F85 ^property[1].valueString = "ICD-10 2017:H16.0~H19.0~H19.1~H19.2~H19.3~H19.8" 
* #F86 "Trachom"
* #F86 ^definition = Inklusive:  Exklusive:  Kriterien: Entweder nachgewiesene Infektion mit Chlamydia trachomatis oder typisches klinisches Erscheinungsbild einschließlich chronischer Entzündung und Hypertrophie der Bindehaut mit Bildung gelblicher oder grauer Körnchen
* #F86 ^property[0].code = #parent 
* #F86 ^property[0].valueCode = #F 
* #F86 ^property[1].code = #Relationships 
* #F86 ^property[1].valueString = "ICD-10 2017:A71.0~A71.1~A71.9~B94.0" 
* #F86 ^property[2].code = #hints 
* #F86 ^property[2].valueString = "Beachte: rotes Auge F02~ Ausfluß aus dem Auge F03 Hinweise: " 
* #F91 "Refraktionsfehler"
* #F91 ^definition = Inklusive: Astigmatismus~ Hypermetropie~ Weitsichtigkeit~ Kurzsichtigkeit~ Presbyopie Exklusive: Sehschwäche/Blindheit F94 Kriterien: Sehfehler, der durch entsprechende Linsen behoben werden kann
* #F91 ^property[0].code = #parent 
* #F91 ^property[0].valueCode = #F 
* #F91 ^property[1].code = #Relationships 
* #F91 ^property[1].valueString = "ICD-10 2017:H44.2~H52.0~H52.1~H52.2~H52.3~H52.4~H52.5~H52.6~H52.7" 
* #F92 "Katarakt"
* #F92 ^definition = Inklusive:  Exklusive: angeboren F81 Kriterien: Teilweise oder völlige Trübung des Linsensystems, die die Sehfähigkeit vermindert oder beeinträchtigt
* #F92 ^property[0].code = #parent 
* #F92 ^property[0].valueCode = #F 
* #F92 ^property[1].code = #Relationships 
* #F92 ^property[1].valueString = "ICD-10 2017:H25.0~H25.1~H25.2~H25.8~H25.9~H26.0~H26.1~H26.2~H26.3~H26.4~H26.8~H26.9~H28.0~H28.1~H28.2~H28.8" 
* #F93 "Glaukom"
* #F93 ^definition = Inklusive: erhöhter Augeninnendruck Exklusive: angeboren F81 Kriterien:
* #F93 ^property[0].code = #parent 
* #F93 ^property[0].valueCode = #F 
* #F93 ^property[1].code = #Relationships 
* #F93 ^property[1].valueString = "ICD-10 2017:H40.0~H40.1~H40.2~H40.3~H40.4~H40.5~H40.6~H40.8~H40.9~H42.0~H42.8" 
* #F94 "Blindheit"
* #F94 ^definition = Inklusive: teilweise oder völlige Erblindung auf beiden Augen Exklusive: verschwommenes Sehen F05~ vorübergehende Erblindung F05~ Blindheit auf einem Auge F28~ Schneeblindheit F79~ Refraktionsfehler F91~ Farbenblindheit F99~ Nachtblindheit F99 Kriterien:
* #F94 ^property[0].code = #parent 
* #F94 ^property[0].valueCode = #F 
* #F94 ^property[1].code = #Relationships 
* #F94 ^property[1].valueString = "ICD-10 2017:H54.0~H54.1~H54.2~H54.3" 
* #F95 "Strabismus"
* #F95 ^definition = Inklusive: Schielen~ Esophorie Exklusive:  Kriterien: Mangelnde Parallelität der Sehachsen der Augen, die bei einer ärztlichen Untersuchung festgestellt wurde
* #F95 ^property[0].code = #parent 
* #F95 ^property[0].valueCode = #F 
* #F95 ^property[1].code = #Relationships 
* #F95 ^property[1].valueString = "ICD-10 2017:H49.0~H49.1~H49.2~H49.3~H49.4~H49.8~H49.9~H50.0~H50.1~H50.2~H50.3~H50.4~H50.5~H50.6~H50.8~H50.9~H51.0~H51.1~H51.2~H51.8~H51.9" 
* #F95 ^property[2].code = #hints 
* #F95 ^property[2].valueString = "Beachte: abnorme Augenbewegung F14 Hinweise: " 
* #F99 "Andere Erkrankung Auge/Anhangsgebilde"
* #F99 ^definition = Inklusive: Amblyopie~ Arcus senilis~ Farbenblindheit~ Hornhouttrübung~ Störung der Augenhöhle~ Ektropium~ Entropium~ Episkleritis~ Trichiasis~ Nachtblindheit~ Papillenödem~ Pterygium~ Skleritis Exklusive:  Kriterien:
* #F99 ^property[0].code = #parent 
* #F99 ^property[0].valueCode = #F 
* #F99 ^property[1].code = #Relationships 
* #F99 ^property[1].valueString = "ICD-10 2017:H02.0~H02.1~H02.8~H02.9~H04.0~H04.1~H04.5~H04.6~H04.8~H04.9~H05.2~H05.3~H05.4~H05.5~H05.8~H05.9~H06.0~H06.1~H06.2~H06.3~H11.0~H11.1~H11.2~H11.4~H11.8~H11.9~H15.0~H15.1~H15.8~H15.9~H17.0~H17.1~H17.8~H17.9~H18.0~H18.1~H18.2~H18.3~H18.4~H18.5~H18.6~H18.7~H18.8~H18.9~H27.0~H27.1~H27.8~H27.9~H31.0~H31.1~H31.2~H31.3~H31.4~H31.8~H31.9~H34.0~H34.1~H34.2~H34.8~H34.9~H35.5~H35.6~H35.7~H35.8~H35.9~H43.0~H43.1~H43.2~H43.3~H43.8~H43.9~H44.3~H44.4~H44.5~H44.8~H44.9~H45.0~H45.1~H45.8~H46~H47.0~H47.1~H47.2~H47.3~H47.4~H47.5~H47.6~H47.7~H48.0~H48.1~H48.8~H53.0~H53.4~H53.5~H53.6~H53.8~H57.0~H57.8~H58.0~H58.1~H58.8" 
* #H "Ohr"
* #H ^property[0].code = #child 
* #H ^property[0].valueCode = #H01 
* #H ^property[1].code = #child 
* #H ^property[1].valueCode = #H02 
* #H ^property[2].code = #child 
* #H ^property[2].valueCode = #H03 
* #H ^property[3].code = #child 
* #H ^property[3].valueCode = #H04 
* #H ^property[4].code = #child 
* #H ^property[4].valueCode = #H05 
* #H ^property[5].code = #child 
* #H ^property[5].valueCode = #H13 
* #H ^property[6].code = #child 
* #H ^property[6].valueCode = #H15 
* #H ^property[7].code = #child 
* #H ^property[7].valueCode = #H27 
* #H ^property[8].code = #child 
* #H ^property[8].valueCode = #H28 
* #H ^property[9].code = #child 
* #H ^property[9].valueCode = #H29 
* #H ^property[10].code = #child 
* #H ^property[10].valueCode = #H30 
* #H ^property[11].code = #child 
* #H ^property[11].valueCode = #H31 
* #H ^property[12].code = #child 
* #H ^property[12].valueCode = #H32 
* #H ^property[13].code = #child 
* #H ^property[13].valueCode = #H33 
* #H ^property[14].code = #child 
* #H ^property[14].valueCode = #H34 
* #H ^property[15].code = #child 
* #H ^property[15].valueCode = #H35 
* #H ^property[16].code = #child 
* #H ^property[16].valueCode = #H36 
* #H ^property[17].code = #child 
* #H ^property[17].valueCode = #H37 
* #H ^property[18].code = #child 
* #H ^property[18].valueCode = #H38 
* #H ^property[19].code = #child 
* #H ^property[19].valueCode = #H39 
* #H ^property[20].code = #child 
* #H ^property[20].valueCode = #H40 
* #H ^property[21].code = #child 
* #H ^property[21].valueCode = #H41 
* #H ^property[22].code = #child 
* #H ^property[22].valueCode = #H42 
* #H ^property[23].code = #child 
* #H ^property[23].valueCode = #H43 
* #H ^property[24].code = #child 
* #H ^property[24].valueCode = #H44 
* #H ^property[25].code = #child 
* #H ^property[25].valueCode = #H45 
* #H ^property[26].code = #child 
* #H ^property[26].valueCode = #H46 
* #H ^property[27].code = #child 
* #H ^property[27].valueCode = #H47 
* #H ^property[28].code = #child 
* #H ^property[28].valueCode = #H48 
* #H ^property[29].code = #child 
* #H ^property[29].valueCode = #H49 
* #H ^property[30].code = #child 
* #H ^property[30].valueCode = #H50 
* #H ^property[31].code = #child 
* #H ^property[31].valueCode = #H51 
* #H ^property[32].code = #child 
* #H ^property[32].valueCode = #H52 
* #H ^property[33].code = #child 
* #H ^property[33].valueCode = #H53 
* #H ^property[34].code = #child 
* #H ^property[34].valueCode = #H54 
* #H ^property[35].code = #child 
* #H ^property[35].valueCode = #H55 
* #H ^property[36].code = #child 
* #H ^property[36].valueCode = #H56 
* #H ^property[37].code = #child 
* #H ^property[37].valueCode = #H57 
* #H ^property[38].code = #child 
* #H ^property[38].valueCode = #H58 
* #H ^property[39].code = #child 
* #H ^property[39].valueCode = #H59 
* #H ^property[40].code = #child 
* #H ^property[40].valueCode = #H60 
* #H ^property[41].code = #child 
* #H ^property[41].valueCode = #H61 
* #H ^property[42].code = #child 
* #H ^property[42].valueCode = #H62 
* #H ^property[43].code = #child 
* #H ^property[43].valueCode = #H63 
* #H ^property[44].code = #child 
* #H ^property[44].valueCode = #H64 
* #H ^property[45].code = #child 
* #H ^property[45].valueCode = #H65 
* #H ^property[46].code = #child 
* #H ^property[46].valueCode = #H66 
* #H ^property[47].code = #child 
* #H ^property[47].valueCode = #H67 
* #H ^property[48].code = #child 
* #H ^property[48].valueCode = #H68 
* #H ^property[49].code = #child 
* #H ^property[49].valueCode = #H69 
* #H ^property[50].code = #child 
* #H ^property[50].valueCode = #H70 
* #H ^property[51].code = #child 
* #H ^property[51].valueCode = #H71 
* #H ^property[52].code = #child 
* #H ^property[52].valueCode = #H72 
* #H ^property[53].code = #child 
* #H ^property[53].valueCode = #H73 
* #H ^property[54].code = #child 
* #H ^property[54].valueCode = #H74 
* #H ^property[55].code = #child 
* #H ^property[55].valueCode = #H75 
* #H ^property[56].code = #child 
* #H ^property[56].valueCode = #H76 
* #H ^property[57].code = #child 
* #H ^property[57].valueCode = #H77 
* #H ^property[58].code = #child 
* #H ^property[58].valueCode = #H78 
* #H ^property[59].code = #child 
* #H ^property[59].valueCode = #H79 
* #H ^property[60].code = #child 
* #H ^property[60].valueCode = #H80 
* #H ^property[61].code = #child 
* #H ^property[61].valueCode = #H81 
* #H ^property[62].code = #child 
* #H ^property[62].valueCode = #H82 
* #H ^property[63].code = #child 
* #H ^property[63].valueCode = #H83 
* #H ^property[64].code = #child 
* #H ^property[64].valueCode = #H84 
* #H ^property[65].code = #child 
* #H ^property[65].valueCode = #H85 
* #H ^property[66].code = #child 
* #H ^property[66].valueCode = #H86 
* #H ^property[67].code = #child 
* #H ^property[67].valueCode = #H99 
* #H01 "Ohrschmerz"
* #H01 ^property[0].code = #parent 
* #H01 ^property[0].valueCode = #H 
* #H01 ^property[1].code = #Relationships 
* #H01 ^property[1].valueString = "ICD-10 2017:H92.0" 
* #H02 "Hörstörung"
* #H02 ^definition = Inklusive:  Exklusive: Taubheit eines Ohres H28~ Taubheit beider Ohren H86 Kriterien:
* #H02 ^property[0].code = #parent 
* #H02 ^property[0].valueCode = #H 
* #H02 ^property[1].code = #Relationships 
* #H02 ^property[1].valueString = "ICD-10 2017:H93.2" 
* #H03 "Tinnitus, Klingeln, Brummen"
* #H03 ^definition = Inklusive: Widerhall im Ohr Exklusive: Knistern/Knallen im Ohr H29 Kriterien:
* #H03 ^property[0].code = #parent 
* #H03 ^property[0].valueCode = #H 
* #H03 ^property[1].code = #Relationships 
* #H03 ^property[1].valueString = "ICD-10 2017:H93.1" 
* #H04 "Ausfluss Ohr"
* #H04 ^definition = Inklusive:  Exklusive: Blut im/aus dem Ohr H05 Kriterien:
* #H04 ^property[0].code = #parent 
* #H04 ^property[0].valueCode = #H 
* #H04 ^property[1].code = #Relationships 
* #H04 ^property[1].valueString = "ICD-10 2017:H92.1" 
* #H05 "Blutung Ohr"
* #H05 ^definition = Inklusive: Blut im/aus dem Ohr Exklusive:  Kriterien:
* #H05 ^property[0].code = #parent 
* #H05 ^property[0].valueCode = #H 
* #H05 ^property[1].code = #Relationships 
* #H05 ^property[1].valueString = "ICD-10 2017:H92.2" 
* #H13 "Verstopfungsgefühl Ohr"
* #H13 ^definition = Inklusive: verstopftes Ohr Exklusive: übermäßiges Ohrenschmalz H81 Kriterien:
* #H13 ^property[0].code = #parent 
* #H13 ^property[0].valueCode = #H 
* #H13 ^property[1].code = #Relationships 
* #H13 ^property[1].valueString = "ICD-10 2017:H93.8" 
* #H15 "Besorgnis über äußere Erscheinung der Ohren"
* #H15 ^definition = Inklusive:  Exklusive: abstehende Ohren/angeborene Fehlbildung des Ohres H80 Kriterien:
* #H15 ^property[0].code = #parent 
* #H15 ^property[0].valueCode = #H 
* #H15 ^property[1].code = #Relationships 
* #H15 ^property[1].valueString = "ICD-10 2017:R46.8" 
* #H27 "Angst vor Ohrenerkrankung"
* #H27 ^definition = Inklusive: Angst vor Taubheit Exklusive: wenn der Patient die Erkrankung hat~ kodiere die Erkrankung Kriterien: Besorgnis über/Angst vor Ohrenerkrankung oder Ertauben bei einem Patienten, der die Krankheit nicht hat/bevor die Diagnose gesichert ist
* #H27 ^property[0].code = #parent 
* #H27 ^property[0].valueCode = #H 
* #H27 ^property[1].code = #Relationships 
* #H27 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #H28 "Funktionseinschränkung/Behinderung (H)"
* #H28 ^definition = Inklusive: vorrübergehende Schwerhörigkeit/ Taubheit Exklusive: Altersschwerhörigkeit H84~ akustisches Trauma H85~ Taubheit/ Schwerhörigkeit H86~ Schwindel/ Benommenheit N17 Kriterien: Funktionseinschränkung/Behinderung durch ein Problem mit den Ohren/dem Gehör
* #H28 ^property[0].code = #parent 
* #H28 ^property[0].valueCode = #H 
* #H28 ^property[1].code = #Relationships 
* #H28 ^property[1].valueString = "ICD-10 2017:Z73.6" 
* #H28 ^property[2].code = #hints 
* #H28 ^property[2].valueString = "Beachte:  Hinweise: COOP/WONCA Charts eignen sich zur Dokumentation des funktionellen Status des Patienten (siehe Kapitel 8)" 
* #H29 "Andere Ohrensymptome/-beschwerden"
* #H29 ^definition = Inklusive: Knistern oder Knallen im Ohr~ juckendes Ohr~ Ziehen an den Ohren Exklusive: Benommenheit/Gleichgewichtsverlust/Schwindel N17 Kriterien:
* #H29 ^property[0].code = #parent 
* #H29 ^property[0].valueCode = #H 
* #H29 ^property[1].code = #Relationships 
* #H29 ^property[1].valueString = "ICD-10 2017:H93.8~H93.9" 
* #H30 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? vollständig"
* #H30 ^property[0].code = #parent 
* #H30 ^property[0].valueCode = #H 
* #H31 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? teilweise"
* #H31 ^property[0].code = #parent 
* #H31 ^property[0].valueCode = #H 
* #H32 "Allergie-/Sensitivitätstestung"
* #H32 ^property[0].code = #parent 
* #H32 ^property[0].valueCode = #H 
* #H33 "Mikrobiologische/immunologische Untersuchung"
* #H33 ^property[0].code = #parent 
* #H33 ^property[0].valueCode = #H 
* #H34 "Blutuntersuchung"
* #H34 ^property[0].code = #parent 
* #H34 ^property[0].valueCode = #H 
* #H35 "Urinuntersuchung"
* #H35 ^property[0].code = #parent 
* #H35 ^property[0].valueCode = #H 
* #H36 "Stuhluntersuchung"
* #H36 ^property[0].code = #parent 
* #H36 ^property[0].valueCode = #H 
* #H37 "Histologische Untersuchung/zytologischer Abstrich"
* #H37 ^property[0].code = #parent 
* #H37 ^property[0].valueCode = #H 
* #H38 "Andere Laboruntersuchung NAK"
* #H38 ^property[0].code = #parent 
* #H38 ^property[0].valueCode = #H 
* #H39 "Körperliche Funktionsprüfung"
* #H39 ^property[0].code = #parent 
* #H39 ^property[0].valueCode = #H 
* #H40 "Diagnostische Endoskopie"
* #H40 ^property[0].code = #parent 
* #H40 ^property[0].valueCode = #H 
* #H41 "Diagnostisches Röntgen/Bildgebung"
* #H41 ^property[0].code = #parent 
* #H41 ^property[0].valueCode = #H 
* #H42 "Elektrische Aufzeichnungsverfahren"
* #H42 ^property[0].code = #parent 
* #H42 ^property[0].valueCode = #H 
* #H43 "Andere diagnostische Untersuchung"
* #H43 ^property[0].code = #parent 
* #H43 ^property[0].valueCode = #H 
* #H44 "Präventive Impfung/Medikation"
* #H44 ^property[0].code = #parent 
* #H44 ^property[0].valueCode = #H 
* #H45 "Beobachtung/Gesundheitsschulung/Beratung/Diät"
* #H45 ^property[0].code = #parent 
* #H45 ^property[0].valueCode = #H 
* #H46 "Konsultation eines anderen Primärversorgers"
* #H46 ^property[0].code = #parent 
* #H46 ^property[0].valueCode = #H 
* #H47 "Konsultation eines Facharztes"
* #H47 ^property[0].code = #parent 
* #H47 ^property[0].valueCode = #H 
* #H48 "Klärung/Besprechung des Beratungsanlasses mit dem Patienten"
* #H48 ^property[0].code = #parent 
* #H48 ^property[0].valueCode = #H 
* #H49 "Andere Vorsorgemaßnahme"
* #H49 ^property[0].code = #parent 
* #H49 ^property[0].valueCode = #H 
* #H50 "Medikation/Verschreibung/Erneuerung/Injektion"
* #H50 ^property[0].code = #parent 
* #H50 ^property[0].valueCode = #H 
* #H51 "Inzision/Drainage/Spülung/Absaugung/Entfernung von Körperflüssigkeit"
* #H51 ^property[0].code = #parent 
* #H51 ^property[0].valueCode = #H 
* #H52 "Exzision/Entfernung von Gewebe/Biopsie/Destruktion/Debridement/Verkauterung"
* #H52 ^property[0].code = #parent 
* #H52 ^property[0].valueCode = #H 
* #H53 "Instrumentelle Manipulation/Katheterisierung/Intubation/Dilatation"
* #H53 ^property[0].code = #parent 
* #H53 ^property[0].valueCode = #H 
* #H54 "Reposition/Fixierung/Naht/Gips/prothetische Versorgung"
* #H54 ^property[0].code = #parent 
* #H54 ^property[0].valueCode = #H 
* #H55 "Lokale Injektion/Infiltration"
* #H55 ^property[0].code = #parent 
* #H55 ^property[0].valueCode = #H 
* #H56 "Verband/Druck/Kompression/Tamponade"
* #H56 ^property[0].code = #parent 
* #H56 ^property[0].valueCode = #H 
* #H57 "Physikalische Therapie/Rehabilitation"
* #H57 ^property[0].code = #parent 
* #H57 ^property[0].valueCode = #H 
* #H58 "Therapeutische Beratung/Zuhören"
* #H58 ^property[0].code = #parent 
* #H58 ^property[0].valueCode = #H 
* #H59 "Andere therapeutische Maßnahme/kleine Chirurgie NAK"
* #H59 ^property[0].code = #parent 
* #H59 ^property[0].valueCode = #H 
* #H60 "Testresultat/Ergebnis eigene Maßnahme"
* #H60 ^property[0].code = #parent 
* #H60 ^property[0].valueCode = #H 
* #H61 "Ergebnis Untersuchung/Test/Brief von anderem Leistungserbringer"
* #H61 ^property[0].code = #parent 
* #H61 ^property[0].valueCode = #H 
* #H62 "Adminstrative Maßnahme"
* #H62 ^property[0].code = #parent 
* #H62 ^property[0].valueCode = #H 
* #H63 "Folgekonsultation unspezifiziert"
* #H63 ^property[0].code = #parent 
* #H63 ^property[0].valueCode = #H 
* #H64 "Konsultation/Problembehandlung auf Initiative des Leistungserbringers"
* #H64 ^property[0].code = #parent 
* #H64 ^property[0].valueCode = #H 
* #H65 "Konsultation/Problembehandlung auf Initiative Dritter"
* #H65 ^property[0].code = #parent 
* #H65 ^property[0].valueCode = #H 
* #H66 "Überweisung an andere/n Leistungserbringer (nicht ärztl.)/Pflegekraft/Therapeuten/Sozialarbeiter"
* #H66 ^property[0].code = #parent 
* #H66 ^property[0].valueCode = #H 
* #H67 "Überweisung an einen Arzt/Spezialisten/eine Klinik/ein Krankenhaus"
* #H67 ^property[0].code = #parent 
* #H67 ^property[0].valueCode = #H 
* #H68 "Andere Überweisung NAK"
* #H68 ^property[0].code = #parent 
* #H68 ^property[0].valueCode = #H 
* #H69 "Anderer Beratungsanlass NAK"
* #H69 ^property[0].code = #parent 
* #H69 ^property[0].valueCode = #H 
* #H70 "Otitis externa"
* #H70 ^definition = Inklusive: Ekzem/Abzeß/Furunkel im äußeren Gehörgang~ Badeotitis Exklusive:  Kriterien: Entzündung oder Abschuppung des äußeren Gehörgangs
* #H70 ^property[0].code = #parent 
* #H70 ^property[0].valueCode = #H 
* #H70 ^property[1].code = #Relationships 
* #H70 ^property[1].valueString = "ICD-10 2017:H60.0~H60.1~H60.2~H60.3~H60.4~H60.5~H60.8~H60.9~H62.0~H62.1~H62.2~H62.3~H62.4~H62.8" 
* #H71 "Akute Mittelohrentzündung/Myringitis"
* #H71 ^definition = Inklusive: Akute eitrige Mittelohrentzündung~ Mittelohrentzündung NNB~ akute Tympanitis Exklusive: seröse Mittelohrentzündung H72~ chronische Mittelohrentzündung H74 Kriterien: Frische Perforation des Trommelfells mit eitrigem Ausfluß; oder entzündetes und vorgewölbtes Trommelfell; oder ein Trommelfell ist stärker gerötet als das andere; oder gerötetes Trommelfell mit Ohrenschmerzen; oder Bläschen auf dem Trommelfell
* #H71 ^property[0].code = #parent 
* #H71 ^property[0].valueCode = #H 
* #H71 ^property[1].code = #Relationships 
* #H71 ^property[1].valueString = "ICD-10 2017:H66.0~H66.4~H66.9~H67.0~H67.1~H67.8~H70.0~H73.0" 
* #H71 ^property[2].code = #hints 
* #H71 ^property[2].valueString = "Beachte: Ohrenschmerz H01~ Ausfluß aus dem Ohr H04 Hinweise: " 
* #H72 "Muko-/Serotympanon/Paukenerguss"
* #H72 ^definition = Inklusive: Glue ear~ Mittelohrentzündung mit Erguß Exklusive: akute Mittelohrentzündung H71~ chronische Mittelohrentzündung H74 Kriterien: sichtbare Sekretansammlung hinter dem Trommelfell ohne Entzündung; oder fehlender Trommelfellreflex mit daraus resultierenden Hörbeschwerden
* #H72 ^property[0].code = #parent 
* #H72 ^property[0].valueCode = #H 
* #H72 ^property[1].code = #Relationships 
* #H72 ^property[1].valueString = "ICD-10 2017:H65.0~H65.1~H65.2~H65.3~H65.4~H65.9" 
* #H72 ^property[2].code = #hints 
* #H72 ^property[2].valueString = "Beachte: Gefühl der Ohrenverstopfung H13~ Entzündung oder Verschluß der Eustachischen Röhre H73 Hinweise: " 
* #H73 "Tubenkatarrh/-ventilationsstörung"
* #H73 ^definition = Inklusive: Verschluß/Katarrh/Funktionsstörung der Eustachischen Röhre Exklusive: seröse Mittelohrentzündung H72 Kriterien:
* #H73 ^property[0].code = #parent 
* #H73 ^property[0].valueCode = #H 
* #H73 ^property[1].code = #Relationships 
* #H73 ^property[1].valueString = "ICD-10 2017:H68.0~H68.1~H69.0~H69.8~H69.9" 
* #H73 ^property[2].code = #hints 
* #H73 ^property[2].valueString = "Beachte: Gefühl der Ohrenverstopfung H13 Hinweise: " 
* #H74 "Chronische Mittelohrentzündung"
* #H74 ^definition = Inklusive: Cholesteatom~ chronische eitrige Mittelohrentzündung~ Mastoiditis Exklusive: seröse Mittelohrentzündung H72 Kriterien:
* #H74 ^property[0].code = #parent 
* #H74 ^property[0].valueCode = #H 
* #H74 ^property[1].code = #Relationships 
* #H74 ^property[1].valueString = "ICD-10 2017:H66.1~H66.2~H66.3~H66.9~H70.1~H70.2~H70.8~H70.9~H71~H73.1~H75.0~H75.8" 
* #H75 "Neubildung im/am Ohr"
* #H75 ^definition = Inklusive: gutartige/bösartige Neubildung am/im Ohr Exklusive: Ohrpolyp H99~ Akustikusneurinom N75 Kriterien:
* #H75 ^property[0].code = #parent 
* #H75 ^property[0].valueCode = #H 
* #H75 ^property[1].code = #Relationships 
* #H75 ^property[1].valueString = "ICD-10 2017:C30.1~C44.2~C49.0~D02.3~D04.2~D14.0~D21.0~D23.2" 
* #H76 "Fremdkörper im Ohr"
* #H76 ^property[0].code = #parent 
* #H76 ^property[0].valueCode = #H 
* #H76 ^property[1].code = #Relationships 
* #H76 ^property[1].valueString = "ICD-10 2017:T16" 
* #H77 "Perforation Trommelfell"
* #H77 ^definition = Inklusive:  Exklusive: Trommelfellperforation mit Infektion H71~ H74~ traumatische/druckbedingte Trommelfellruptur H79 Kriterien:
* #H77 ^property[0].code = #parent 
* #H77 ^property[0].valueCode = #H 
* #H77 ^property[1].code = #Relationships 
* #H77 ^property[1].valueString = "ICD-10 2017:H72.0~H72.1~H72.2~H72.8~H72.9" 
* #H78 "Oberflächliche Verletzung Ohr"
* #H78 ^definition = Inklusive: Verletzung am äußeren Gehörgang~ Ohrmuschel Exklusive: Verletzung des Trommelfells H79 Kriterien:
* #H78 ^property[0].code = #parent 
* #H78 ^property[0].valueCode = #H 
* #H78 ^property[1].code = #Relationships 
* #H78 ^property[1].valueString = "ICD-10 2017:S00.4" 
* #H79 "Ohrenverletzung, andere"
* #H79 ^definition = Inklusive: traumatisch/druckbedingter Trommelfellriss Exklusive:  Kriterien:
* #H79 ^property[0].code = #parent 
* #H79 ^property[0].valueCode = #H 
* #H79 ^property[1].code = #Relationships 
* #H79 ^property[1].valueString = "ICD-10 2017:S01.3~S07.8~S08.1~S09.2~T70.0" 
* #H80 "Angeborene Anomalie des Ohrs"
* #H80 ^definition = Inklusive: überzählige Ohrmuschel~ abstehende Ohren Exklusive: angeborene Taubheit H86 Kriterien:
* #H80 ^property[0].code = #parent 
* #H80 ^property[0].valueCode = #H 
* #H80 ^property[1].code = #Relationships 
* #H80 ^property[1].valueString = "ICD-10 2017:Q16.0~Q16.1~Q16.2~Q16.3~Q16.4~Q16.5~Q16.9~Q17.0~Q17.1~Q17.2~Q17.3~Q17.4~Q17.5~Q17.8~Q17.9" 
* #H81 "Übermäßige Ohrschmalzbildung"
* #H81 ^definition = Inklusive:  Exklusive:  Kriterien: Symptome/Beschwerden durch Cerumen/Ohrenschmalz im Gehörgang
* #H81 ^property[0].code = #parent 
* #H81 ^property[0].valueCode = #H 
* #H81 ^property[1].code = #Relationships 
* #H81 ^property[1].valueString = "ICD-10 2017:H61.2" 
* #H82 "Schwindelsyndrom"
* #H82 ^definition = Inklusive: benigner paroxysmaler/Lagerungsschwindel~ Labyrinthitis~ Morbus Ménière~ Neuritis vestibularis Exklusive:  Kriterien: Nachweisbarer Drehschwindel
* #H82 ^property[0].code = #parent 
* #H82 ^property[0].valueCode = #H 
* #H82 ^property[1].code = #Relationships 
* #H82 ^property[1].valueString = "ICD-10 2017:A88.1~H81.0~H81.1~H81.2~H81.3~H81.4~H81.8~H81.9~H82~H83.0~H83.1~H83.2" 
* #H82 ^property[2].code = #hints 
* #H82 ^property[2].valueString = "Beachte: Schwindel/Benommenheit N17 Hinweise: " 
* #H83 "Otosklerose"
* #H83 ^property[0].code = #parent 
* #H83 ^property[0].valueCode = #H 
* #H83 ^property[1].code = #Relationships 
* #H83 ^property[1].valueString = "ICD-10 2017:H80.0~H80.1~H80.2~H80.8~H80.9" 
* #H84 "Altersschwerhörigkeit"
* #H84 ^definition = Inklusive:  Exklusive: Taubheit H86 Kriterien: Altersbedingtes graduelles Einsetzen symmetrischer bilateraler Schwerhörigkeit, insbesondere bei Hochfrequenztönen
* #H84 ^property[0].code = #parent 
* #H84 ^property[0].valueCode = #H 
* #H84 ^property[1].code = #Relationships 
* #H84 ^property[1].valueString = "ICD-10 2017:H91.1" 
* #H84 ^property[2].code = #hints 
* #H84 ^property[2].valueString = "Beachte: Hörminderung H28~ Taubheit H86 Hinweise: " 
* #H85 "Akustisches Trauma"
* #H85 ^definition = Inklusive: lärmbedingte Schwerhörigkeit Exklusive: Perforation eines Trommelfells H77 Kriterien: Schwerhörigkeit im Hochfrequenzbereich, bei definitiver Exposition gegenüber großem Lärm in der Anamnese des Patienten
* #H85 ^property[0].code = #parent 
* #H85 ^property[0].valueCode = #H 
* #H85 ^property[1].code = #Relationships 
* #H85 ^property[1].valueString = "ICD-10 2017:H83.3" 
* #H85 ^property[2].code = #hints 
* #H85 ^property[2].valueString = "Beachte: Hörminderung H28~ Taubheit H86 Hinweise: " 
* #H86 "Taubheit/Schwerhörigkeit"
* #H86 ^definition = Inklusive: angeborene Taubheit~ partielle/vollständige Taubheit beider Ohren Exklusive: vorübergehende Taubheit/Taubheit eins Ohrs H28~ Otosklerose H83~ Presbyakusis H84~ Lärmtaubheit H85 Kriterien:
* #H86 ^property[0].code = #parent 
* #H86 ^property[0].valueCode = #H 
* #H86 ^property[1].code = #Relationships 
* #H86 ^property[1].valueString = "ICD-10 2017:H90.0~H90.1~H90.2~H90.3~H90.4~H90.5~H90.6~H90.7~H90.8~H91.0~H91.2~H91.3~H91.8~H91.9" 
* #H99 "Andere Ohr-/Mastoiderkrankung"
* #H99 ^definition = Inklusive: Polypen des Mittelohres Exklusive: Mastoiditis H74 Kriterien:
* #H99 ^property[0].code = #parent 
* #H99 ^property[0].valueCode = #H 
* #H99 ^property[1].code = #Relationships 
* #H99 ^property[1].valueString = "ICD-10 2017:H61.0~H61.1~H61.3~H61.8~H61.9~H73.8~H73.9~H74.0~H74.1~H74.2~H74.3~H74.4~H74.8~H74.9~H83.8~H83.9~H93.0~H93.3~H93.8~H93.9~H94.0~H94.8" 
* #K "Kreislauf"
* #K ^property[0].code = #child 
* #K ^property[0].valueCode = #K01 
* #K ^property[1].code = #child 
* #K ^property[1].valueCode = #K02 
* #K ^property[2].code = #child 
* #K ^property[2].valueCode = #K03 
* #K ^property[3].code = #child 
* #K ^property[3].valueCode = #K04 
* #K ^property[4].code = #child 
* #K ^property[4].valueCode = #K05 
* #K ^property[5].code = #child 
* #K ^property[5].valueCode = #K06 
* #K ^property[6].code = #child 
* #K ^property[6].valueCode = #K07 
* #K ^property[7].code = #child 
* #K ^property[7].valueCode = #K22 
* #K ^property[8].code = #child 
* #K ^property[8].valueCode = #K24 
* #K ^property[9].code = #child 
* #K ^property[9].valueCode = #K25 
* #K ^property[10].code = #child 
* #K ^property[10].valueCode = #K27 
* #K ^property[11].code = #child 
* #K ^property[11].valueCode = #K28 
* #K ^property[12].code = #child 
* #K ^property[12].valueCode = #K29 
* #K ^property[13].code = #child 
* #K ^property[13].valueCode = #K30 
* #K ^property[14].code = #child 
* #K ^property[14].valueCode = #K31 
* #K ^property[15].code = #child 
* #K ^property[15].valueCode = #K32 
* #K ^property[16].code = #child 
* #K ^property[16].valueCode = #K33 
* #K ^property[17].code = #child 
* #K ^property[17].valueCode = #K34 
* #K ^property[18].code = #child 
* #K ^property[18].valueCode = #K35 
* #K ^property[19].code = #child 
* #K ^property[19].valueCode = #K36 
* #K ^property[20].code = #child 
* #K ^property[20].valueCode = #K37 
* #K ^property[21].code = #child 
* #K ^property[21].valueCode = #K38 
* #K ^property[22].code = #child 
* #K ^property[22].valueCode = #K39 
* #K ^property[23].code = #child 
* #K ^property[23].valueCode = #K40 
* #K ^property[24].code = #child 
* #K ^property[24].valueCode = #K41 
* #K ^property[25].code = #child 
* #K ^property[25].valueCode = #K42 
* #K ^property[26].code = #child 
* #K ^property[26].valueCode = #K43 
* #K ^property[27].code = #child 
* #K ^property[27].valueCode = #K44 
* #K ^property[28].code = #child 
* #K ^property[28].valueCode = #K45 
* #K ^property[29].code = #child 
* #K ^property[29].valueCode = #K46 
* #K ^property[30].code = #child 
* #K ^property[30].valueCode = #K47 
* #K ^property[31].code = #child 
* #K ^property[31].valueCode = #K48 
* #K ^property[32].code = #child 
* #K ^property[32].valueCode = #K49 
* #K ^property[33].code = #child 
* #K ^property[33].valueCode = #K50 
* #K ^property[34].code = #child 
* #K ^property[34].valueCode = #K51 
* #K ^property[35].code = #child 
* #K ^property[35].valueCode = #K52 
* #K ^property[36].code = #child 
* #K ^property[36].valueCode = #K53 
* #K ^property[37].code = #child 
* #K ^property[37].valueCode = #K54 
* #K ^property[38].code = #child 
* #K ^property[38].valueCode = #K55 
* #K ^property[39].code = #child 
* #K ^property[39].valueCode = #K56 
* #K ^property[40].code = #child 
* #K ^property[40].valueCode = #K57 
* #K ^property[41].code = #child 
* #K ^property[41].valueCode = #K58 
* #K ^property[42].code = #child 
* #K ^property[42].valueCode = #K59 
* #K ^property[43].code = #child 
* #K ^property[43].valueCode = #K60 
* #K ^property[44].code = #child 
* #K ^property[44].valueCode = #K61 
* #K ^property[45].code = #child 
* #K ^property[45].valueCode = #K62 
* #K ^property[46].code = #child 
* #K ^property[46].valueCode = #K63 
* #K ^property[47].code = #child 
* #K ^property[47].valueCode = #K64 
* #K ^property[48].code = #child 
* #K ^property[48].valueCode = #K65 
* #K ^property[49].code = #child 
* #K ^property[49].valueCode = #K66 
* #K ^property[50].code = #child 
* #K ^property[50].valueCode = #K67 
* #K ^property[51].code = #child 
* #K ^property[51].valueCode = #K68 
* #K ^property[52].code = #child 
* #K ^property[52].valueCode = #K69 
* #K ^property[53].code = #child 
* #K ^property[53].valueCode = #K70 
* #K ^property[54].code = #child 
* #K ^property[54].valueCode = #K71 
* #K ^property[55].code = #child 
* #K ^property[55].valueCode = #K72 
* #K ^property[56].code = #child 
* #K ^property[56].valueCode = #K73 
* #K ^property[57].code = #child 
* #K ^property[57].valueCode = #K74 
* #K ^property[58].code = #child 
* #K ^property[58].valueCode = #K75 
* #K ^property[59].code = #child 
* #K ^property[59].valueCode = #K76 
* #K ^property[60].code = #child 
* #K ^property[60].valueCode = #K77 
* #K ^property[61].code = #child 
* #K ^property[61].valueCode = #K78 
* #K ^property[62].code = #child 
* #K ^property[62].valueCode = #K79 
* #K ^property[63].code = #child 
* #K ^property[63].valueCode = #K80 
* #K ^property[64].code = #child 
* #K ^property[64].valueCode = #K81 
* #K ^property[65].code = #child 
* #K ^property[65].valueCode = #K82 
* #K ^property[66].code = #child 
* #K ^property[66].valueCode = #K83 
* #K ^property[67].code = #child 
* #K ^property[67].valueCode = #K84 
* #K ^property[68].code = #child 
* #K ^property[68].valueCode = #K85 
* #K ^property[69].code = #child 
* #K ^property[69].valueCode = #K86 
* #K ^property[70].code = #child 
* #K ^property[70].valueCode = #K87 
* #K ^property[71].code = #child 
* #K ^property[71].valueCode = #K88 
* #K ^property[72].code = #child 
* #K ^property[72].valueCode = #K89 
* #K ^property[73].code = #child 
* #K ^property[73].valueCode = #K90 
* #K ^property[74].code = #child 
* #K ^property[74].valueCode = #K91 
* #K ^property[75].code = #child 
* #K ^property[75].valueCode = #K92 
* #K ^property[76].code = #child 
* #K ^property[76].valueCode = #K93 
* #K ^property[77].code = #child 
* #K ^property[77].valueCode = #K94 
* #K ^property[78].code = #child 
* #K ^property[78].valueCode = #K95 
* #K ^property[79].code = #child 
* #K ^property[79].valueCode = #K96 
* #K ^property[80].code = #child 
* #K ^property[80].valueCode = #K99 
* #K01 "Herzschmerz"
* #K01 ^definition = Inklusive: Schmerzen~ die dem Herzen zugeschrieben werden Exklusive: Brustschmerzen NNB A11~ Angst vor Herzanfall K24~ Angina pectoris K74~ Brustbeklemmung R29 Kriterien:
* #K01 ^property[0].code = #parent 
* #K01 ^property[0].valueCode = #K 
* #K01 ^property[1].code = #Relationships 
* #K01 ^property[1].valueString = "ICD-10 2017:R07.2" 
* #K02 "Druck/Engegefühl des Herzens"
* #K02 ^definition = Inklusive: Gefühl von Herzschwere Exklusive: Brustschmerzen NNB A11~ Angst vor Herzanfall K24~ Angina pectoris K74~ Kurzatmigkeit/Dyspnoe R02 Kriterien:
* #K02 ^property[0].code = #parent 
* #K02 ^property[0].valueCode = #K 
* #K02 ^property[1].code = #Relationships 
* #K02 ^property[1].valueString = "ICD-10 2017:R07.2" 
* #K03 "Herz/Gefäßsystem Schmerzen, NNB"
* #K03 ^definition = Inklusive:  Exklusive: Schmerzen die dem Herz zugewiesen werden K01~ Claudicatio K92~ Migräne N89 Kriterien:
* #K03 ^property[0].code = #parent 
* #K03 ^property[0].valueCode = #K 
* #K03 ^property[1].code = #Relationships 
* #K03 ^property[1].valueString = "ICD-10 2017:R09.8" 
* #K04 "Palpitation/Bewusste Wahrnehmung des Herzens"
* #K04 ^definition = Inklusive: Tachykardie Exklusive: paroxysmale Tachykardie K79 Kriterien:
* #K04 ^property[0].code = #parent 
* #K04 ^property[0].valueCode = #K 
* #K04 ^property[1].code = #Relationships 
* #K04 ^property[1].valueString = "ICD-10 2017:R00.0~R00.1~R00.2" 
* #K05 "Unregelmäßigkeiten Herzschlag, andere"
* #K05 ^definition = Inklusive:  Exklusive: Herzklopfen K04 Kriterien:
* #K05 ^property[0].code = #parent 
* #K05 ^property[0].valueCode = #K 
* #K05 ^property[1].code = #Relationships 
* #K05 ^property[1].valueString = "ICD-10 2017:R00.8" 
* #K06 "Auffälligkeiten venös/kapillär"
* #K06 ^definition = Inklusive: ungewöhnlich hervortretende Venen~ Spider naevus Exklusive: Varizen K95~ Hämangiom S81 Kriterien:
* #K06 ^property[0].code = #parent 
* #K06 ^property[0].valueCode = #K 
* #K06 ^property[1].code = #Relationships 
* #K06 ^property[1].valueString = "ICD-10 2017:I78.1~I87.8" 
* #K07 "Geschwollene Knöchel/Ödeme"
* #K07 ^definition = Inklusive: Wassersucht~ Flüssigkeitsretention~ geschwollene Füße/Beine Exklusive: Knöchelsymptom L16~ umschriebene Schwellung S04 Kriterien:
* #K07 ^property[0].code = #parent 
* #K07 ^property[0].valueCode = #K 
* #K07 ^property[1].code = #Relationships 
* #K07 ^property[1].valueString = "ICD-10 2017:R60.0~R60.1~R60.9" 
* #K22 "Risiko Herz-/Gefäßerkrankung"
* #K22 ^definition = Inklusive: persönliche- oder Familienvorgeschichte~ frühere Episode~ anderer Risikofaktor für kardiovaskuläre Erkrankung Exklusive:  Kriterien:
* #K22 ^property[0].code = #parent 
* #K22 ^property[0].valueCode = #K 
* #K22 ^property[1].code = #Relationships 
* #K22 ^property[1].valueString = "ICD-10 2017:Z82.3~Z82.4~Z86.7" 
* #K24 "Angst vor Herzerkrankung"
* #K24 ^definition = Inklusive: Angst vor Herzanfall Exklusive: wenn der Patient die Erkrankung hat~ ist die Erkrankung zu kodieren Kriterien: Besorgnis über/Angst vor Herzanfall/-erkrankung bei einem Patienten, der die Krankheit nicht hat/bevor die Diagnose gesichert ist
* #K24 ^property[0].code = #parent 
* #K24 ^property[0].valueCode = #K 
* #K24 ^property[1].code = #Relationships 
* #K24 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #K25 "Angst vor Bluthochdruck"
* #K25 ^definition = Inklusive:  Exklusive: wenn der Patient die Erkrankung hat~ ist die Erkrankung zu kodieren Kriterien: Besorgnis über/Angst vor Bluthochdruck bei einem Patienten, der die Krankheit nicht hat/bevor die Diagnose gesichert ist
* #K25 ^property[0].code = #parent 
* #K25 ^property[0].valueCode = #K 
* #K25 ^property[1].code = #Relationships 
* #K25 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #K27 "Angst vor anderer Herz-/Gefäßerkrankung"
* #K27 ^definition = Inklusive:  Exklusive: Angst vor kardivaskulärer Erkrankungs K24~ K25~ wenn der Patient die Erkrankung hat~ ist die Erkrankung zu kodieren Kriterien: Besorgnis über/Angst vor Kreislauferkrankung bei einem Patienten, der die Krankheit nicht hat/bevor die Diagnose gesichert ist
* #K27 ^property[0].code = #parent 
* #K27 ^property[0].valueCode = #K 
* #K27 ^property[1].code = #Relationships 
* #K27 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #K28 "Funktionseinschränkung/Behinderung (K)"
* #K28 ^definition = Inklusive:  Exklusive:  Kriterien: Funktionseinschränkung/Behinderung durch kardiovaskuläres Problem
* #K28 ^property[0].code = #parent 
* #K28 ^property[0].valueCode = #K 
* #K28 ^property[1].code = #Relationships 
* #K28 ^property[1].valueString = "ICD-10 2017:Z73.6~Z99.4" 
* #K28 ^property[2].code = #hints 
* #K28 ^property[2].valueString = "Beachte:  Hinweise: COOP/WONCA Charts eignen sich zur Dokumentation des funktionellen Status des Patienten (siehe Kapitel 8)" 
* #K29 "Andere Herz-/Gefäßsymptome/-beschwerden"
* #K29 ^definition = Inklusive: Herzbeschwerden~ niedriger Blutdruck~ schwaches Herz Exklusive: Flüssigkeit im Brustraum R82~ Zyanose S08 Kriterien:
* #K29 ^property[0].code = #parent 
* #K29 ^property[0].valueCode = #K 
* #K29 ^property[1].code = #Relationships 
* #K29 ^property[1].valueString = "ICD-10 2017:R03.1~R09.8" 
* #K30 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? vollständig"
* #K30 ^property[0].code = #parent 
* #K30 ^property[0].valueCode = #K 
* #K31 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? teilweise"
* #K31 ^property[0].code = #parent 
* #K31 ^property[0].valueCode = #K 
* #K32 "Allergie-/Sensitivitätstestung"
* #K32 ^property[0].code = #parent 
* #K32 ^property[0].valueCode = #K 
* #K33 "Mikrobiologische/immunologische Untersuchung"
* #K33 ^property[0].code = #parent 
* #K33 ^property[0].valueCode = #K 
* #K34 "Blutuntersuchung"
* #K34 ^property[0].code = #parent 
* #K34 ^property[0].valueCode = #K 
* #K35 "Urinuntersuchung"
* #K35 ^property[0].code = #parent 
* #K35 ^property[0].valueCode = #K 
* #K36 "Stuhluntersuchung"
* #K36 ^property[0].code = #parent 
* #K36 ^property[0].valueCode = #K 
* #K37 "Histologische Untersuchung/zytologischer Abstrich"
* #K37 ^property[0].code = #parent 
* #K37 ^property[0].valueCode = #K 
* #K38 "Andere Laboruntersuchung NAK"
* #K38 ^property[0].code = #parent 
* #K38 ^property[0].valueCode = #K 
* #K39 "Körperliche Funktionsprüfung"
* #K39 ^property[0].code = #parent 
* #K39 ^property[0].valueCode = #K 
* #K40 "Diagnostische Endoskopie"
* #K40 ^property[0].code = #parent 
* #K40 ^property[0].valueCode = #K 
* #K41 "Diagnostisches Röntgen/Bildgebung"
* #K41 ^property[0].code = #parent 
* #K41 ^property[0].valueCode = #K 
* #K42 "Elektrische Aufzeichnungsverfahren"
* #K42 ^property[0].code = #parent 
* #K42 ^property[0].valueCode = #K 
* #K43 "Andere diagnostische Untersuchung"
* #K43 ^property[0].code = #parent 
* #K43 ^property[0].valueCode = #K 
* #K44 "Präventive Impfung/Medikation"
* #K44 ^property[0].code = #parent 
* #K44 ^property[0].valueCode = #K 
* #K45 "Beobachtung/Gesundheitsschulung/Beratung/Diät"
* #K45 ^property[0].code = #parent 
* #K45 ^property[0].valueCode = #K 
* #K46 "Konsultation eines anderen Primärversorgers"
* #K46 ^property[0].code = #parent 
* #K46 ^property[0].valueCode = #K 
* #K47 "Konsultation eines Facharztes"
* #K47 ^property[0].code = #parent 
* #K47 ^property[0].valueCode = #K 
* #K48 "Klärung/Besprechung des Beratungsanlasses mit dem Patienten"
* #K48 ^property[0].code = #parent 
* #K48 ^property[0].valueCode = #K 
* #K49 "Andere Vorsorgemaßnahme"
* #K49 ^property[0].code = #parent 
* #K49 ^property[0].valueCode = #K 
* #K50 "Medikation/Verschreibung/Erneuerung/Injektion"
* #K50 ^property[0].code = #parent 
* #K50 ^property[0].valueCode = #K 
* #K51 "Inzision/Drainage/Spülung/Absaugung/Entfernung von Körperflüssigkeit"
* #K51 ^property[0].code = #parent 
* #K51 ^property[0].valueCode = #K 
* #K52 "Exzision/Entfernung von Gewebe/Biopsie/Destruktion/Debridement/Verkauterung"
* #K52 ^property[0].code = #parent 
* #K52 ^property[0].valueCode = #K 
* #K53 "Instrumentelle Manipulation/Katheterisierung/Intubation/Dilatation"
* #K53 ^property[0].code = #parent 
* #K53 ^property[0].valueCode = #K 
* #K54 "Reposition/Fixierung/Naht/Gips/prothetische Versorgung"
* #K54 ^property[0].code = #parent 
* #K54 ^property[0].valueCode = #K 
* #K55 "Lokale Injektion/Infiltration"
* #K55 ^property[0].code = #parent 
* #K55 ^property[0].valueCode = #K 
* #K56 "Verband/Druck/Kompression/Tamponade"
* #K56 ^property[0].code = #parent 
* #K56 ^property[0].valueCode = #K 
* #K57 "Physikalische Therapie/Rehabilitation"
* #K57 ^property[0].code = #parent 
* #K57 ^property[0].valueCode = #K 
* #K58 "Therapeutische Beratung/Zuhören"
* #K58 ^property[0].code = #parent 
* #K58 ^property[0].valueCode = #K 
* #K59 "Andere therapeutische Maßnahme/kleine Chirurgie NAK"
* #K59 ^property[0].code = #parent 
* #K59 ^property[0].valueCode = #K 
* #K60 "Testresultat/Ergebnis eigene Maßnahme"
* #K60 ^property[0].code = #parent 
* #K60 ^property[0].valueCode = #K 
* #K61 "Ergebnis Untersuchung/Test/Brief von anderem Leistungserbringer"
* #K61 ^property[0].code = #parent 
* #K61 ^property[0].valueCode = #K 
* #K62 "Adminstrative Maßnahme"
* #K62 ^property[0].code = #parent 
* #K62 ^property[0].valueCode = #K 
* #K63 "Folgekonsultation unspezifiziert"
* #K63 ^property[0].code = #parent 
* #K63 ^property[0].valueCode = #K 
* #K64 "Konsultation/Problembehandlung auf Initiative des Leistungserbringers"
* #K64 ^property[0].code = #parent 
* #K64 ^property[0].valueCode = #K 
* #K65 "Konsultation/Problembehandlung auf Initiative Dritter"
* #K65 ^property[0].code = #parent 
* #K65 ^property[0].valueCode = #K 
* #K66 "Überweisung an andere/n Leistungserbringer (nicht ärztl.)/Pflegekraft/Therapeuten/Sozialarbeiter"
* #K66 ^property[0].code = #parent 
* #K66 ^property[0].valueCode = #K 
* #K67 "Überweisung an einen Arzt/Spezialisten/eine Klinik/ein Krankenhaus"
* #K67 ^property[0].code = #parent 
* #K67 ^property[0].valueCode = #K 
* #K68 "Andere Überweisung NAK"
* #K68 ^property[0].code = #parent 
* #K68 ^property[0].valueCode = #K 
* #K69 "Anderer Beratungsanlass NAK"
* #K69 ^property[0].code = #parent 
* #K69 ^property[0].valueCode = #K 
* #K70 "Infektion des Herz-/Kreislaufsystems"
* #K70 ^definition = Inklusive: akute/subakute Endokarditis~ bakterielle Endokarditis~ Myokarditis~ Perikarditis (nicht rheumatische) Exklusive: rheumatische Herzerkrankung K71~ Phlebitis und Thrombophlebitis K94~ Arteritis K99 Kriterien:
* #K70 ^property[0].code = #parent 
* #K70 ^property[0].valueCode = #K 
* #K70 ^property[1].code = #Relationships 
* #K70 ^property[1].valueString = "ICD-10 2017:A39.5~B33.2~B37.6~I30.1~I32.0~I32.1~I33.0~I33.9~I38~I40.0~I40.1~I40.8~I40.9~I41.0~I41.1~I41.2~I41.8" 
* #K71 "Rheumatisches Fieber/Herzerkrankung"
* #K71 ^definition = Inklusive: Chorea Sydenham~ Mitralstenose Exklusive:  Kriterien: - Für akutes rheumatisches Fieber: zwei Symptome erster Ordnung; oder ein Symptom erster Ordnung und zwei Symptome zweiter Ordnung, plus Nachweis einer vorangegangenen Streptokokkeninfektion; Symptome erster Ordnung: Polyarthritis migrans; Karditis; Chorea Sydenham; Erythema marginatum; subkutane Knotenbildung jüngeren Datums/Erythemna  nodosum; Symptome zweiter Ordnung: Fieber, Gelenkschmerzen, erhöhte BSR oder Nachweis von C-reaktivem Protein, verlängertes P-R Intervall im EKG. Für chronisch rheumatische Herzerkrankung: entweder körperliche Befunde, die für eine krankhafte Veränderung der Herzklappen oder des Endokards sprechen, bei einem Patienten mit rheumatischem Fieber in der Anamnese; oder körperliche Befunde, die für eine Mitralstenose sprechen, auch bei Fehlen von rheumatischem Fieber in der Anamnese, wenn keine andere Ursache nachweisbar ist
* #K71 ^property[0].code = #parent 
* #K71 ^property[0].valueCode = #K 
* #K71 ^property[1].code = #Relationships 
* #K71 ^property[1].valueString = "ICD-10 2017:I00~I01.0~I01.1~I01.2~I01.8~I01.9~I02.0~I02.9~I05.0~I05.1~I05.2~I05.8~I05.9~I06.0~I06.1~I06.2~I06.8~I06.9~I07.0~I07.1~I07.2~I07.8~I07.9~I08.0~I08.1~I08.2~I08.3~I08.8~I08.9~I09.0~I09.1~I09.2~I09.8~I09.9" 
* #K71 ^property[2].code = #hints 
* #K71 ^property[2].valueString = "Beachte: Herzklappenerkrankung K83~ andere Herzerkrankung K84 Hinweise: " 
* #K72 "Neubildung Herz/Gefäßsystem"
* #K72 ^definition = Inklusive: gutartige/bösartige kardiovaskuläre Neubildung Exklusive: Hämangiom S81 Kriterien:
* #K72 ^property[0].code = #parent 
* #K72 ^property[0].valueCode = #K 
* #K72 ^property[1].code = #Relationships 
* #K72 ^property[1].valueString = "ICD-10 2017:C38.0~C45.2~D15.1~D15.2~D48.7" 
* #K73 "Angeborene Anomalie Herz/Gefäßsystem"
* #K73 ^definition = Inklusive: atrialer/ventrikulärer Septumdefekt~ Fallotsche Tetralogie~ persistierender Ductus Arteriosus Exklusive: Hämangiom S81 Kriterien:
* #K73 ^property[0].code = #parent 
* #K73 ^property[0].valueCode = #K 
* #K73 ^property[1].code = #Relationships 
* #K73 ^property[1].valueString = "ICD-10 2017:I42.4~Q20.0~Q20.1~Q20.2~Q20.3~Q20.4~Q20.5~Q20.6~Q20.8~Q20.9~Q21.0~Q21.1~Q21.2~Q21.3~Q21.4~Q21.8~Q21.9~Q22.0~Q22.1~Q22.2~Q22.3~Q22.4~Q22.5~Q22.6~Q22.8~Q22.9~Q23.0~Q23.1~Q23.2~Q23.3~Q23.4~Q23.8~Q23.9~Q24.0~Q24.1~Q24.2~Q24.3~Q24.4~Q24.5~Q24.6~Q24.8~Q24.9~Q25.0~Q25.1~Q25.2~Q25.3~Q25.4~Q25.5~Q25.6~Q25.7~Q25.8~Q25.9~Q26.0~Q26.1~Q26.2~Q26.3~Q26.4~Q26.5~Q26.6~Q26.8~Q26.9~Q27.0~Q27.1~Q27.2~Q27.3~Q27.4~Q27.8~Q27.9~Q28.0~Q28.1~Q28.2~Q28.3~Q28.8~Q28.9" 
* #K74 "Ischämische Herzerkrankung mit Angina"
* #K74 ^definition = Inklusive: Belastungsangina~ Angina pectoris~ Angina mit Spasmus~ ischämischer Brustschmerz~ instabile Angina Exklusive: ischämische Herzerkrankung ohne Angina K76 Kriterien: Vorgeschichte plus Nachweis eines alten Myokardinfarktes im EKG oder bildgebenden Verfahren, oder Nachweis einer Myokardischämie im Ruhe- oder Belastungs-EKG, oder Nachweis durch bildgebende und invasive Verfahren von Verengungen der Koronararterien oder ventrikulärem Aneurysma
* #K74 ^property[0].code = #parent 
* #K74 ^property[0].valueCode = #K 
* #K74 ^property[1].code = #Relationships 
* #K74 ^property[1].valueString = "ICD-10 2017:I20.0~I20.1~I20.8~I20.9~I24.0~I24.8~I24.9" 
* #K74 ^property[2].code = #hints 
* #K74 ^property[2].valueString = "Beachte: Herzschmerz K01 Hinweise: " 
* #K75 "Akuter Myokardinfarkt"
* #K75 ^definition = Inklusive: Als akut klassifizierter Myokardinfakrt oder innerhalb von vier Wochen nach dem Ereignis (28 Tage) Exklusive: alter oder abgeheilter Myokardinfarkt K74~ K76 Kriterien: Brustschmerzen, die für Myokardischämie typisch sind und länger als 15 Minuten andauern, und/oder abnorme ST-T-Veränderungen, oder neue Q-Zacken im EKG, oder charakteristisches Ansteigen der Herzenzyme
* #K75 ^property[0].code = #parent 
* #K75 ^property[0].valueCode = #K 
* #K75 ^property[1].code = #Relationships 
* #K75 ^property[1].valueString = "ICD-10 2017:I21.0~I21.1~I21.2~I21.3~I21.4~I21.9~I22.0~I22.1~I22.8~I22.9~I23.0~I23.1~I23.2~I23.3~I23.4~I23.5~I23.6~I23.8~I24.1" 
* #K75 ^property[2].code = #hints 
* #K75 ^property[2].valueString = "Beachte: Herzschmerz KO1~ Angina pectoris K74~ chronisch ischämische Herzerkrankung K76 Hinweise: zusätzlich auch K74 oder K76 kodieren" 
* #K76 "Ischämische Herzerkrankung ohne Angina"
* #K76 ^definition = Inklusive: Herzwandaneurysma~ arteriosklerotische/atherosklerotische Herzerkrankung~ Koronararterienerkrankung~ ischäemische Kardiomyopathie~ alter Myokardinfarkt~ stumme Myokardischämie Exklusive: ischämische Herzerkrankung mit Angina K74 Kriterien: Vorgeschichte plus Nachweis eines alten Myokardinfarktes im EKG oder bildgebenden Verfahren, oder Nachweis einer Myokardischämie im Ruhe- oder Belastungs-EKG, oder Nachweis durch bildgebende und invasive Verfahren von Verengungen der Koronararterien oder ventrikulärem Aneurysma
* #K76 ^property[0].code = #parent 
* #K76 ^property[0].valueCode = #K 
* #K76 ^property[1].code = #Relationships 
* #K76 ^property[1].valueString = "ICD-10 2017:I25.0~I25.1~I25.2~I25.3~I25.4~I25.5~I25.6~I25.8~I25.9" 
* #K77 "Herzinsuffizienz"
* #K77 ^definition = Inklusive: Asthma cardiale~ kongestives Herzversagen~ Herzversagen NNB~ Linkherzschwäche~ Pulmonarödem~ Rechtsherzschwäche Exklusive: Cor pulmonale K82 Kriterien: mehrfache Anzeichen inkl. Ödem der abhängigen Körperpartien, erhöhter Druck in der Vena jugularis, Lebervergrößerung bei Fehlen einer Lebererkrankung, Lungenstauung, Pleuraerguß, vergrößertes Herz
* #K77 ^property[0].code = #parent 
* #K77 ^property[0].valueCode = #K 
* #K77 ^property[1].code = #Relationships 
* #K77 ^property[1].valueString = "ICD-10 2017:I50.0~I50.19~I50.9" 
* #K78 "Vorhofflimmern/-flattern"
* #K78 ^definition = Inklusive:  Exklusive: paroxysmale Tachykardie K79 Kriterien: Nachweis charakteristischer Befunde im EKG, oder völlig unregelmäßiger Herzschlag mit Pulsdefizit
* #K78 ^property[0].code = #parent 
* #K78 ^property[0].valueCode = #K 
* #K78 ^property[1].code = #Relationships 
* #K78 ^property[1].valueString = "ICD-10 2017:I48.0~I48.1~I48.2~I48.3~I48.4~I48.9" 
* #K78 ^property[2].code = #hints 
* #K78 ^property[2].valueString = "Beachte: Herzklopfen K04~ unregelmäßiger Herzschlag K05 Hinweise: " 
* #K79 "Paroxysmale Tachykardie"
* #K79 ^definition = Inklusive: supraventrikuläre/ventrikuläre Tachykardie Exklusive: Tahchykardie NNB K04~ Vorhofflimmern K78 Kriterien: Vorgeschichte mit wiederholtem Auftreten von Herzrasen (über 140 Schläge/min.) das abrupt beginnt und endet
* #K79 ^property[0].code = #parent 
* #K79 ^property[0].valueCode = #K 
* #K79 ^property[1].code = #Relationships 
* #K79 ^property[1].valueString = "ICD-10 2017:I47.0~I47.1~I47.2~I47.9" 
* #K79 ^property[2].code = #hints 
* #K79 ^property[2].valueString = "Beachte: Herzklopfen K04~ unregelmäßiger Herzschlag K05 Hinweise: " 
* #K80 "Herzrhythmusstörung NNB"
* #K80 ^definition = Inklusive: atriale/junctionale/ventrikuläre Extrasystolen~ Bradykardie~ Bigemismus~ ektopische Schläge~ Sick sinus Syndrom~ Kammerflimmern/-flattern Exklusive: paroxysmale Tachykardie K79 Kriterien: ein oder mehrere Herzschläge, die außerhaqlb der für den zugrundeliegenden Rhythmus typischen Schlagintervalle auftreten
* #K80 ^property[0].code = #parent 
* #K80 ^property[0].valueCode = #K 
* #K80 ^property[1].code = #Relationships 
* #K80 ^property[1].valueString = "ICD-10 2017:I49.0~I49.1~I49.2~I49.3~I49.4~I49.5~I49.8~I49.9" 
* #K80 ^property[2].code = #hints 
* #K80 ^property[2].valueString = "Beachte: Herzklopfen KO4~ unregelmäßiger Herzschlag K05 Hinweise: " 
* #K81 "Herz-/arterielles Geräusch NNB"
* #K81 ^definition = Inklusive: kardiales/Karotiden-/renales Arteriengeräusch~ akzidentelles Herzgeräusch bei Kindern Exklusive: rheumatische Herzerkrankung K71~ Herzklappenerkrankung K83~ Hirngefäßerkrankung K90 Kriterien:
* #K81 ^property[0].code = #parent 
* #K81 ^property[0].valueCode = #K 
* #K81 ^property[1].code = #Relationships 
* #K81 ^property[1].valueString = "ICD-10 2017:R01.0~R01.1~R01.2~R09.8" 
* #K82 "Pulmonale Herzerkrankung"
* #K82 ^definition = Inklusive: chronisches Cor pulmonale~ Erkrankungen der Lungengefäße~ primäre pulmonale Hypertonie Exklusive: Lungenembolie K93 Kriterien: Vorliegen einer chronischen Erkrankung der Lungen, des Lungengefäßsystems oder Stöurng des respiratorischen Gasaustausches, plus Vergrößerung des rechten Ventrikels oder Rechtsherzinsuffizienz
* #K82 ^property[0].code = #parent 
* #K82 ^property[0].valueCode = #K 
* #K82 ^property[1].code = #Relationships 
* #K82 ^property[1].valueString = "ICD-10 2017:I27.0~I27.1~I27.2~I27.8~I27.9~I28.0~I28.1~I28.8~I28.9" 
* #K82 ^property[2].code = #hints 
* #K82 ^property[2].valueString = "Beachte: Rechtsherversagen K77 Hinweise: " 
* #K83 "Herzklappenerkrankung NNB"
* #K83 ^definition = Inklusive: chronische Endokarditis~ Mitralklappenprolaps~ nichtrheumatische Störungen der Aorten-~ Mitral~ Pulmonal-~ Trikuspidalklappe Exklusive: rheumatische Klappenerkrankungen K71 Kriterien: Fehlen von Kriterien für chronisch rheumatische Herzerkrankungen (K71), plus Nachweis von Herzklappenfehlern, entweder durch charakteristische Herzgeräusche oder in der Echokardiographie
* #K83 ^property[0].code = #parent 
* #K83 ^property[0].valueCode = #K 
* #K83 ^property[1].code = #Relationships 
* #K83 ^property[1].valueString = "ICD-10 2017:I34.0~I34.1~I34.2~I34.8~I34.9~I35.0~I35.1~I35.2~I35.8~I35.9~I36.0~I36.1~I36.2~I36.8~I36.9~I37.0~I37.1~I37.2~I37.8~I37.9~I39.0~I39.1~I39.2~I39.3~I39.4~I39.8" 
* #K83 ^property[2].code = #hints 
* #K83 ^property[2].valueString = "Beachte: hypertensive Herzerkrankung K87~ Herzgeräusche NNB K81 Hinweise: " 
* #K84 "Herzerkrankung, andere"
* #K84 ^definition = Inklusive: Schenkelblock~ Herzstillstand~ Kardiomegalie~ Perikarderkrankung~ Linksschenkelblock~ andere Reizleitungsstörungen Exklusive: Herzrhytmusstörungen K80 Kriterien:
* #K84 ^property[0].code = #parent 
* #K84 ^property[0].valueCode = #K 
* #K84 ^property[1].code = #Relationships 
* #K84 ^property[1].valueString = "ICD-10 2017:I30.0~I30.8~I30.9~I31.0~I31.1~I31.2~I31.3~I31.8~I31.9~I32.8~I42.0~I42.1~I42.2~I42.3~I42.5~I42.6~I42.7~I42.8~I42.9~I43.0~I43.1~I43.2~I43.8~I44.0~I44.1~I44.2~I44.3~I44.4~I44.5~I44.6~I44.7~I45.0~I45.1~I45.2~I45.3~I45.4~I45.5~I45.6~I45.8~I45.9~I46.0~I46.1~I46.9~I51.0~I51.1~I51.2~I51.3~I51.4~I51.5~I51.6~I51.7~I51.8~I51.9~I52.0~I52.1~I52.8~O90.3" 
* #K85 "Erhöhter Blutdruck"
* #K85 ^definition = Inklusive: erhöhter Blutdruck ohne Erfüllung der Kriterien für K86 und K87~ vorübergehender oder labiler Bluthochdruck Exklusive:  Kriterien:
* #K85 ^property[0].code = #parent 
* #K85 ^property[0].valueCode = #K 
* #K85 ^property[1].code = #Relationships 
* #K85 ^property[1].valueString = "ICD-10 2017:R03.0" 
* #K86 "Bluthochdruck, unkomplizierter"
* #K86 ^definition = Inklusive: essentielle Hypertonie~ Hypertonie NNB~ idiopathische Hypertonie Exklusive: Hypertonie mit Komplikationen K87~ in der Schwangerschaft W81 Kriterien: entweder zwei oder mehr Messungen pro Konsultation, bei zwei oder mehr Konsultationen vorgenommen, mit Blutdruckwerten von durchschnittlich über 95 mmHg diastolisch oder über 160 mmHg systolisch beim Erwachsenen; oder zwei oder mehr Messungen bei einer einzelnen Konsultation mit durchschnittlichen diastolischen Blutdruckwerten von 120 mmHg oder mehr; plus Fehlen von Anzeichen für sekundäre Auswirkungen auf Herz, Nieren, Augen oder Gehirn
* #K86 ^property[0].code = #parent 
* #K86 ^property[0].valueCode = #K 
* #K86 ^property[1].code = #Relationships 
* #K86 ^property[1].valueString = "ICD-10 2017:I10" 
* #K86 ^property[2].code = #hints 
* #K86 ^property[2].valueString = "Beachte: erhöhter Blutdruck K85 Hinweise: 1: Bei Kindern geeignete pädiatrische Blutdrucktabellen verwenden  2:Falls sekundärer Hochdruck, ebenfalls zugrundeliegende Ursache kodieren" 
* #K87 "Bluthochdruck, komplizierter"
* #K87 ^definition = Inklusive: Maligne Hypertonie Exklusive: unkomplizierte Hypertonie K86 Kriterien: entweder zwei oder mehr Messungen pro Konsultation, bei zwei oder mehr Konsultationen vorgenommen, mit Blutdruckwerten von durchschnittlich über 95 mmHg diastolisch oder über 160 mmHg systolisch beim Erwachsenen; oder zwei oder mehr Messungen bei einer einzelnen Konsultation mit durchschnittlichen diastolischen Blutdruckwerten von 120 mmHg oder mehr; plus Anomalien des Herzens (Vergrößerung, Insuffizienz), der Nieren (Albuminämie, Azotämie), der Augen oder des Gehirns, die auf Hypertonie zurückgeführt werden können
* #K87 ^property[0].code = #parent 
* #K87 ^property[0].valueCode = #K 
* #K87 ^property[1].code = #Relationships 
* #K87 ^property[1].valueString = "ICD-10 2017:I11.0~I11.9~I12.0~I12.9~I13.0~I13.1~I13.2~I13.9~I15.0~I15.1~I15.2~I15.8~I15.9~I67.4" 
* #K87 ^property[2].code = #hints 
* #K87 ^property[2].valueString = "Beachte:  Hinweise: 1: Bei Kindern geeignete pädiatrische Blutdrucktabellen verwenden  2:Falls sekundärer Hochdruck, ebenfalls zugrundeliegende Ursache kodieren" 
* #K88 "Orthostatische Dysregulation"
* #K88 ^definition = Inklusive: idiopathische/orthostatische Hypotonie Exklusive: durch Medikamente hervorgerufen A85 Kriterien: Anzeichen oder Symptome einer cerebrovaskulären Insuffizienz (Schwindel, Synkopen) beim Wechsel von liegender zu aufrechter Haltung; sowie ein durchschnittlicher Blutdruckabfall um 15 mmHg in zwei oder mehr Fällen beim Aufstehen bzw. Aufsetzen
* #K88 ^property[0].code = #parent 
* #K88 ^property[0].valueCode = #K 
* #K88 ^property[1].code = #Relationships 
* #K88 ^property[1].valueString = "ICD-10 2017:I95.0~I95.1~I95.8~I95.9" 
* #K88 ^property[2].code = #hints 
* #K88 ^property[2].valueString = "Beachte: niedriger Blutdruck K29 Hinweise: " 
* #K89 "Transiente zerebrale Ischämie"
* #K89 ^definition = Inklusive: Basilarisinsuffizienz~ Sturzattacken~ Transiente ischämische Attacken (TIA)~ vorübergehende globale Amnesie Exklusive: zerebrovaskulärer Insult K90~ Migräne N89~ Karotidengeräusche K81 Kriterien: Symptome einer flüchtigen Unterfunktion des Gehirns (unter 24 Stunden), die plötzlich auftraten, vermutlich vaskulären Ursprungs, ohne Folgeerscheinungen bleiben; unter ausschluß von Migräne, migräneähnlichen Zuständen oder Epilepsie
* #K89 ^property[0].code = #parent 
* #K89 ^property[0].valueCode = #K 
* #K89 ^property[1].code = #Relationships 
* #K89 ^property[1].valueString = "ICD-10 2017:G45.0~G45.1~G45.2~G45.3~G45.4~G45.8~G45.9" 
* #K89 ^property[2].code = #hints 
* #K89 ^property[2].valueString = "Beachte: Ohnmacht/Synkope A06 Hinweise: zusätzlich kodieren mit K91" 
* #K90 "Schlaganfall/zerebrovaskulärer Insult"
* #K90 ^definition = Inklusive: Apoplex~ zerebrale Embolie/Infarzierung/Thrombose/Occlusion/ Stenose/Hämorrhagie~ zerebrovaskulärer Unfall~ Insult~ subarachnoidale Blutung Exklusive: flüchtige zerebrale Ischämie K89~ traumatische intrakranielle Blutung N80 Kriterien: Anzeichen und Symptome einer Störung der Gehirnfunktion, vermutlich vaskulären Ursprungs, die mehr als 24 Stunden dauert oder zum Tod führt, innerhalb von vier Wochen (28 Tagen) nach dem Auftreten
* #K90 ^property[0].code = #parent 
* #K90 ^property[0].valueCode = #K 
* #K90 ^property[1].code = #Relationships 
* #K90 ^property[1].valueString = "ICD-10 2017:G46.0~G46.1~G46.2~G46.3~G46.4~G46.5~G46.6~G46.7~G46.8~I60.0~I60.1~I60.2~I60.3~I60.4~I60.5~I60.6~I60.7~I60.8~I60.9~I61.0~I61.1~I61.2~I61.3~I61.4~I61.5~I61.6~I61.8~I61.9~I62.0~I62.1~I62.9~I63.0~I63.1~I63.2~I63.3~I63.4~I63.5~I63.6~I63.8~I63.9~I64" 
* #K90 ^property[2].code = #hints 
* #K90 ^property[2].valueString = "Beachte:  Hinweise: zusätzlich kodieren mit K91" 
* #K91 "Zerebrovaskuläre Erkrankung"
* #K91 ^definition = Inklusive: zerebrales Aneurysma~ Residuen eines Schlaganfalls Exklusive:  Kriterien: voausgegangene flüchtige cerebrale Ischämie oder Schlaganfall; oder Nachweis einer cerebrovaskulären Erkrankung bei Untersuchung
* #K91 ^property[0].code = #parent 
* #K91 ^property[0].valueCode = #K 
* #K91 ^property[1].code = #Relationships 
* #K91 ^property[1].valueString = "ICD-10 2017:I65.0~I65.1~I65.2~I65.3~I65.8~I65.9~I66.0~I66.1~I66.2~I66.3~I66.4~I66.8~I66.9~I67.0~I67.1~I67.2~I67.3~I67.5~I67.6~I67.7~I67.8~I67.9~I68.0~I68.1~I68.2~I68.8~I69.0~I69.1~I69.2~I69.3~I69.4~I69.8" 
* #K92 "Atherosklerose/periphere Gefäßerkrankung"
* #K92 ^definition = Inklusive: Arteriosklerose~ arterielle Embolie/Thrombose/Stenose~ Atherome~ Endarteritis~ Gangrän~ Claudicatio intermittens~ Ischämie der Extremitäten~ Raynaud Syndrom~ Vasospasmus Exklusive: Mesenterialsklerose D99~ Atherosklerose der Augen/Retina F99~ Koronarsklerose K74 bis K76~ pulmonal K82~ Zerebralsklerose K89~ K90~ Aneurysma K99~ Nephrosklerose U99 Kriterien:
* #K92 ^property[0].code = #parent 
* #K92 ^property[0].valueCode = #K 
* #K92 ^property[1].code = #Relationships 
* #K92 ^property[1].valueString = "ICD-10 2017:I70.0~I70.1~I70.2~I70.8~I70.9~I73.0~I73.1~I73.8~I73.9~I74.0~I74.1~I74.2~I74.3~I74.4~I74.5~I74.8~I74.9~R02" 
* #K93 "Lungenembolie"
* #K93 ^definition = Inklusive: Pulmonarvenen-/arterieninfarkt~ Thromboembolie~ Thrombose Exklusive:  Kriterien: Plötzliches Auftreten von Dyspnoe/tachypnoe und der Nachweis eines Lungeninfarktes, entweder durch Untersuchung oder im bildbegenden Verfahren; oder Nachweis einer akuten rechtsventrikulären Belastung im EKG
* #K93 ^property[0].code = #parent 
* #K93 ^property[0].valueCode = #K 
* #K93 ^property[1].code = #Relationships 
* #K93 ^property[1].valueString = "ICD-10 2017:I26.0~I26.9" 
* #K93 ^property[2].code = #hints 
* #K93 ^property[2].valueString = "Beachte: Dyspnoe R02 Hinweise: " 
* #K94 "Phlebitis/Thrombose"
* #K94 ^definition = Inklusive: oberflächliche/tiefe Venenthrombose~ Phlebothrombose~ Pfortaderthrombose Exklusive: zerebrale Thrombose K89~ K90 Kriterien:
* #K94 ^property[0].code = #parent 
* #K94 ^property[0].valueCode = #K 
* #K94 ^property[1].code = #Relationships 
* #K94 ^property[1].valueString = "ICD-10 2017:I80.0~I80.1~I80.2~I80.3~I80.8~I80.9~I81~I82.0~I82.1~I82.2~I82.3~I82.8~I82.9~I87.0" 
* #K95 "Varikosis der Beine"
* #K95 ^definition = Inklusive: variköses Ekzem~ Venenklappeninsuffizienz~ venöse Stauung Exklusive: Ulcus varikosum S97 Kriterien: Vorhandensein erweiterter oberflächlicher Venen in der unteren Extremität; oder Nachweis einer Klappeninsuffizienz der Venen
* #K95 ^property[0].code = #parent 
* #K95 ^property[0].valueCode = #K 
* #K95 ^property[1].code = #Relationships 
* #K95 ^property[1].valueString = "ICD-10 2017:I83.1~I83.9~I87.2" 
* #K95 ^property[2].code = #hints 
* #K95 ^property[2].valueString = "Beachte: prominente Venen K06 Hinweise: " 
* #K96 "Hämorrhoiden"
* #K96 ^definition = Inklusive: innere Hämorrhoiden mit/ohne Komplikationen~ perianale Hämatome~ residuale Hautfalte nach Hämorrhoiden~ thrombosierte äußere Hämorrhoiden~ Krampfadern in Anus oder Rektum Exklusive:  Kriterien: sichtbare Varikosis des Venenplexus der Analregion oder des Analkanals; oder druckempfindliche, schmerzhafte, bläuliche lokalisierte Schwellungen, die plötzlich in der perianalen Region auftreten; oder perianale Hautfalten
* #K96 ^property[0].code = #parent 
* #K96 ^property[0].valueCode = #K 
* #K96 ^property[1].code = #Relationships 
* #K96 ^property[1].valueString = "ICD-10 2017:K64.0~K64.1~K64.2~K64.3~K64.4~K64.5~K64.8~K64.9" 
* #K96 ^property[2].code = #hints 
* #K96 ^property[2].valueString = "Beachte: analer Schmerz D04~ rektale Blutung D16~ analer Tumor D29 Hinweise: " 
* #K99 "Andere Erkrankung Herz/Gefäßsystem"
* #K99 ^definition = Inklusive: Aortenaneurysma~ arteriovenöse Fistel~ Arteritis~ Lymphödem~ Ösophaguvarizen~ andere Aneurysmen~ Polyarteritis nodosa~ Vasculitis~ Varizen an anderen Stellen als den Beinen Exklusive: chronische/unspezifische Lymphadenitis B71~ zerebrales Aneurysma K91~ Gangrän K92 Kriterien:
* #K99 ^property[0].code = #parent 
* #K99 ^property[0].valueCode = #K 
* #K99 ^property[1].code = #Relationships 
* #K99 ^property[1].valueString = "ICD-10 2017:I71.0~I71.1~I71.2~I71.3~I71.4~I71.5~I71.6~I71.8~I71.9~I72.0~I72.1~I72.2~I72.3~I72.4~I72.5~I72.6~I72.8~I72.9~I77.0~I77.1~I77.2~I77.3~I77.4~I77.5~I77.6~I77.8~I77.9~I78.0~I78.8~I78.9~I79.0~I79.1~I79.2~I79.8~I85.0~I85.9~I86.0~I86.1~I86.2~I86.3~I86.4~I86.8~I87.1~I87.8~I87.9~I89.0~I98.0~I98.1~I98.2~I98.3~I98.8~I99~M30.0~M30.1~M30.2~M30.3~M30.8~M31.0~M31.1~M31.2~M31.3~M31.4~M31.5~M31.6~M31.7~M31.8~M31.9~R57.0~R57.1~T06.3" 
* #L "Bewegungsapparat"
* #L ^property[0].code = #child 
* #L ^property[0].valueCode = #L01 
* #L ^property[1].code = #child 
* #L ^property[1].valueCode = #L02 
* #L ^property[2].code = #child 
* #L ^property[2].valueCode = #L03 
* #L ^property[3].code = #child 
* #L ^property[3].valueCode = #L04 
* #L ^property[4].code = #child 
* #L ^property[4].valueCode = #L05 
* #L ^property[5].code = #child 
* #L ^property[5].valueCode = #L07 
* #L ^property[6].code = #child 
* #L ^property[6].valueCode = #L08 
* #L ^property[7].code = #child 
* #L ^property[7].valueCode = #L09 
* #L ^property[8].code = #child 
* #L ^property[8].valueCode = #L10 
* #L ^property[9].code = #child 
* #L ^property[9].valueCode = #L11 
* #L ^property[10].code = #child 
* #L ^property[10].valueCode = #L12 
* #L ^property[11].code = #child 
* #L ^property[11].valueCode = #L13 
* #L ^property[12].code = #child 
* #L ^property[12].valueCode = #L14 
* #L ^property[13].code = #child 
* #L ^property[13].valueCode = #L15 
* #L ^property[14].code = #child 
* #L ^property[14].valueCode = #L16 
* #L ^property[15].code = #child 
* #L ^property[15].valueCode = #L17 
* #L ^property[16].code = #child 
* #L ^property[16].valueCode = #L18 
* #L ^property[17].code = #child 
* #L ^property[17].valueCode = #L19 
* #L ^property[18].code = #child 
* #L ^property[18].valueCode = #L20 
* #L ^property[19].code = #child 
* #L ^property[19].valueCode = #L26 
* #L ^property[20].code = #child 
* #L ^property[20].valueCode = #L27 
* #L ^property[21].code = #child 
* #L ^property[21].valueCode = #L28 
* #L ^property[22].code = #child 
* #L ^property[22].valueCode = #L29 
* #L ^property[23].code = #child 
* #L ^property[23].valueCode = #L30 
* #L ^property[24].code = #child 
* #L ^property[24].valueCode = #L31 
* #L ^property[25].code = #child 
* #L ^property[25].valueCode = #L32 
* #L ^property[26].code = #child 
* #L ^property[26].valueCode = #L33 
* #L ^property[27].code = #child 
* #L ^property[27].valueCode = #L34 
* #L ^property[28].code = #child 
* #L ^property[28].valueCode = #L35 
* #L ^property[29].code = #child 
* #L ^property[29].valueCode = #L36 
* #L ^property[30].code = #child 
* #L ^property[30].valueCode = #L37 
* #L ^property[31].code = #child 
* #L ^property[31].valueCode = #L38 
* #L ^property[32].code = #child 
* #L ^property[32].valueCode = #L39 
* #L ^property[33].code = #child 
* #L ^property[33].valueCode = #L40 
* #L ^property[34].code = #child 
* #L ^property[34].valueCode = #L41 
* #L ^property[35].code = #child 
* #L ^property[35].valueCode = #L42 
* #L ^property[36].code = #child 
* #L ^property[36].valueCode = #L43 
* #L ^property[37].code = #child 
* #L ^property[37].valueCode = #L44 
* #L ^property[38].code = #child 
* #L ^property[38].valueCode = #L45 
* #L ^property[39].code = #child 
* #L ^property[39].valueCode = #L46 
* #L ^property[40].code = #child 
* #L ^property[40].valueCode = #L47 
* #L ^property[41].code = #child 
* #L ^property[41].valueCode = #L48 
* #L ^property[42].code = #child 
* #L ^property[42].valueCode = #L49 
* #L ^property[43].code = #child 
* #L ^property[43].valueCode = #L50 
* #L ^property[44].code = #child 
* #L ^property[44].valueCode = #L51 
* #L ^property[45].code = #child 
* #L ^property[45].valueCode = #L52 
* #L ^property[46].code = #child 
* #L ^property[46].valueCode = #L53 
* #L ^property[47].code = #child 
* #L ^property[47].valueCode = #L54 
* #L ^property[48].code = #child 
* #L ^property[48].valueCode = #L55 
* #L ^property[49].code = #child 
* #L ^property[49].valueCode = #L56 
* #L ^property[50].code = #child 
* #L ^property[50].valueCode = #L57 
* #L ^property[51].code = #child 
* #L ^property[51].valueCode = #L58 
* #L ^property[52].code = #child 
* #L ^property[52].valueCode = #L59 
* #L ^property[53].code = #child 
* #L ^property[53].valueCode = #L60 
* #L ^property[54].code = #child 
* #L ^property[54].valueCode = #L61 
* #L ^property[55].code = #child 
* #L ^property[55].valueCode = #L62 
* #L ^property[56].code = #child 
* #L ^property[56].valueCode = #L63 
* #L ^property[57].code = #child 
* #L ^property[57].valueCode = #L64 
* #L ^property[58].code = #child 
* #L ^property[58].valueCode = #L65 
* #L ^property[59].code = #child 
* #L ^property[59].valueCode = #L66 
* #L ^property[60].code = #child 
* #L ^property[60].valueCode = #L67 
* #L ^property[61].code = #child 
* #L ^property[61].valueCode = #L68 
* #L ^property[62].code = #child 
* #L ^property[62].valueCode = #L69 
* #L ^property[63].code = #child 
* #L ^property[63].valueCode = #L70 
* #L ^property[64].code = #child 
* #L ^property[64].valueCode = #L71 
* #L ^property[65].code = #child 
* #L ^property[65].valueCode = #L72 
* #L ^property[66].code = #child 
* #L ^property[66].valueCode = #L73 
* #L ^property[67].code = #child 
* #L ^property[67].valueCode = #L74 
* #L ^property[68].code = #child 
* #L ^property[68].valueCode = #L75 
* #L ^property[69].code = #child 
* #L ^property[69].valueCode = #L76 
* #L ^property[70].code = #child 
* #L ^property[70].valueCode = #L77 
* #L ^property[71].code = #child 
* #L ^property[71].valueCode = #L78 
* #L ^property[72].code = #child 
* #L ^property[72].valueCode = #L79 
* #L ^property[73].code = #child 
* #L ^property[73].valueCode = #L80 
* #L ^property[74].code = #child 
* #L ^property[74].valueCode = #L81 
* #L ^property[75].code = #child 
* #L ^property[75].valueCode = #L82 
* #L ^property[76].code = #child 
* #L ^property[76].valueCode = #L83 
* #L ^property[77].code = #child 
* #L ^property[77].valueCode = #L84 
* #L ^property[78].code = #child 
* #L ^property[78].valueCode = #L85 
* #L ^property[79].code = #child 
* #L ^property[79].valueCode = #L86 
* #L ^property[80].code = #child 
* #L ^property[80].valueCode = #L87 
* #L ^property[81].code = #child 
* #L ^property[81].valueCode = #L88 
* #L ^property[82].code = #child 
* #L ^property[82].valueCode = #L89 
* #L ^property[83].code = #child 
* #L ^property[83].valueCode = #L90 
* #L ^property[84].code = #child 
* #L ^property[84].valueCode = #L91 
* #L ^property[85].code = #child 
* #L ^property[85].valueCode = #L92 
* #L ^property[86].code = #child 
* #L ^property[86].valueCode = #L93 
* #L ^property[87].code = #child 
* #L ^property[87].valueCode = #L94 
* #L ^property[88].code = #child 
* #L ^property[88].valueCode = #L95 
* #L ^property[89].code = #child 
* #L ^property[89].valueCode = #L96 
* #L ^property[90].code = #child 
* #L ^property[90].valueCode = #L97 
* #L ^property[91].code = #child 
* #L ^property[91].valueCode = #L98 
* #L ^property[92].code = #child 
* #L ^property[92].valueCode = #L99 
* #L01 "Nackensymptome/-beschwerden"
* #L01 ^definition = Inklusive: Schmerz~ der der Wirbelsäule/dem muskulären/knöchernen System zugeschrieben wird Exklusive: Kopfschmerz N01~ Gesichtsschmerz N03 Kriterien:
* #L01 ^property[0].code = #parent 
* #L01 ^property[0].valueCode = #L 
* #L01 ^property[1].code = #Relationships 
* #L01 ^property[1].valueString = "ICD-10 2017:M54.0~M54.2" 
* #L02 "Rückensymptome/-beschwerden"
* #L02 ^definition = Inklusive: Rückenschmerzen NNB~ Schmerzen der Brustwirbelsäule Exklusive: Schmerzen im unteren Rücken L03 Kriterien:
* #L02 ^property[0].code = #parent 
* #L02 ^property[0].valueCode = #L 
* #L02 ^property[1].code = #Relationships 
* #L02 ^property[1].valueString = "ICD-10 2017:M54.0~M54.6~M54.8~M54.9" 
* #L03 "Untere Rückensymptome/-beschwerden"
* #L03 ^definition = Inklusive: Rückenschmerz (lumal/sakroiliakal) Coccydynie~ Lumbago~ Lumbalgie Exklusive: Schmerzen in der Brustwirbelsäule L02~ Ischialgie L86 Kriterien:
* #L03 ^property[0].code = #parent 
* #L03 ^property[0].valueCode = #L 
* #L03 ^property[1].code = #Relationships 
* #L03 ^property[1].valueString = "ICD-10 2017:M53.3~M54.0~M54.5" 
* #L04 "Brustkorbsymptome/-beschwerden muskuloskeletal"
* #L04 ^definition = Inklusive: Brustschmerz~ der dem muskulären/knöchernen System zugeschrieben wird Exklusive: Brustschmerzen NNB A11~ Schmerzen~ die dem Herz zugewiesen werden K01~ schmerzhafte Atmung/pleuritische Schmerzen/Pleurodynie R01 Kriterien:
* #L04 ^property[0].code = #parent 
* #L04 ^property[0].valueCode = #L 
* #L04 ^property[1].code = #Relationships 
* #L04 ^property[1].valueString = "ICD-10 2017:R07.3~R29.8" 
* #L05 "Flanken/-Achselsymptome/-beschwerden"
* #L05 ^definition = Inklusive: Lendenschmerz Exklusive: Nierensymptome U14 Kriterien:
* #L05 ^property[0].code = #parent 
* #L05 ^property[0].valueCode = #L 
* #L05 ^property[1].code = #Relationships 
* #L05 ^property[1].valueString = "ICD-10 2017:R29.8" 
* #L07 "Kiefersymptome/-beschwerden"
* #L07 ^definition = Inklusive: Kiefergelenkssyndrom Exklusive: Zahn-/Zahnfleischsymptome/-beschwerden D19 Kriterien:
* #L07 ^property[0].code = #parent 
* #L07 ^property[0].valueCode = #L 
* #L07 ^property[1].code = #Relationships 
* #L07 ^property[1].valueString = "ICD-10 2017:K07.6~K10.8~M25.4~M25.5~M25.6~R29.8" 
* #L08 "Schultersymptome/-beschwerden"
* #L08 ^property[0].code = #parent 
* #L08 ^property[0].valueCode = #L 
* #L08 ^property[1].code = #Relationships 
* #L08 ^property[1].valueString = "ICD-10 2017:M25.4~M25.5~M25.6" 
* #L09 "Armsymptome/-beschwerden"
* #L09 ^definition = Inklusive:  Exklusive: Muskelschmerzen/Myalgie L18 Kriterien:
* #L09 ^property[0].code = #parent 
* #L09 ^property[0].valueCode = #L 
* #L09 ^property[1].code = #Relationships 
* #L09 ^property[1].valueString = "ICD-10 2017:M79.6~R29.8" 
* #L10 "Ellbogensymptome/-beschwerden"
* #L10 ^property[0].code = #parent 
* #L10 ^property[0].valueCode = #L 
* #L10 ^property[1].code = #Relationships 
* #L10 ^property[1].valueString = "ICD-10 2017:M25.4~M25.5~M25.6" 
* #L11 "Handgelenkssymptome/-beschwerden"
* #L11 ^property[0].code = #parent 
* #L11 ^property[0].valueCode = #L 
* #L11 ^property[1].code = #Relationships 
* #L11 ^property[1].valueString = "ICD-10 2017:M25.4~M25.5~M25.6" 
* #L12 "Hand-/Fingersymptome/-beschwerden"
* #L12 ^property[0].code = #parent 
* #L12 ^property[0].valueCode = #L 
* #L12 ^property[1].code = #Relationships 
* #L12 ^property[1].valueString = "ICD-10 2017:M25.4~M25.5~M25.6~M79.6~R29.8" 
* #L13 "Hüftsymptome/-beschwerden"
* #L13 ^property[0].code = #parent 
* #L13 ^property[0].valueCode = #L 
* #L13 ^property[1].code = #Relationships 
* #L13 ^property[1].valueString = "ICD-10 2017:M25.4~M25.5~M25.6~R29.4" 
* #L14 "Beinsymptome/-beschwerden"
* #L14 ^definition = Inklusive: Wadenkrämpfe Exklusive: Muskel Schmerzen/Myalgie L18~ Restless legs N04 Kriterien:
* #L14 ^property[0].code = #parent 
* #L14 ^property[0].valueCode = #L 
* #L14 ^property[1].code = #Relationships 
* #L14 ^property[1].valueString = "ICD-10 2017:M79.6~R29.8" 
* #L15 "Kniesymptome/-beschwerden"
* #L15 ^property[0].code = #parent 
* #L15 ^property[0].valueCode = #L 
* #L15 ^property[1].code = #Relationships 
* #L15 ^property[1].valueString = "ICD-10 2017:M25.4~M25.5~M25.6" 
* #L16 "Sprunggelenkssymptome/-beschwerden"
* #L16 ^property[0].code = #parent 
* #L16 ^property[0].valueCode = #L 
* #L16 ^property[1].code = #Relationships 
* #L16 ^property[1].valueString = "ICD-10 2017:M25.4~M25.5~M25.6" 
* #L17 "Fuß-/Zehensymptome/-beschwerden"
* #L17 ^definition = Inklusive: Metatarsalgie Exklusive:  Kriterien:
* #L17 ^property[0].code = #parent 
* #L17 ^property[0].valueCode = #L 
* #L17 ^property[1].code = #Relationships 
* #L17 ^property[1].valueString = "ICD-10 2017:M25.4~M25.5~M25.6~M77.4~M79.6~R29.8" 
* #L18 "Muskelschmerzen"
* #L18 ^definition = Inklusive: Fibromyalgie~ Fibrositis~ Maylgie~ Pannikulitis~ Rheumatismus Exklusive: Schmerzen der Wirbelsäule L01~ L02~ L03~ Bein- (Waden-)krämpfe L14 Kriterien:
* #L18 ^property[0].code = #parent 
* #L18 ^property[0].valueCode = #L 
* #L18 ^property[1].code = #Relationships 
* #L18 ^property[1].valueString = "ICD-10 2017:M60.1~M60.2~M60.8~M60.9~M79.0~M79.1~M79.3~M79.6~M79.7~R25.2" 
* #L19 "Muskelsymptome/-beschwerden NNB"
* #L19 ^definition = Inklusive: Muskelatrophie/-steifheit/-verspannung~ Muskelschwäche Exklusive: Schmerzen in der Wirbelsäule L01~ L02~ L03~ Beinkrämpfe L14~ "Wachstumsschmerzen" beim Kind L29~ Restless legs N04 Kriterien:
* #L19 ^property[0].code = #parent 
* #L19 ^property[0].valueCode = #L 
* #L19 ^property[1].code = #Relationships 
* #L19 ^property[1].valueString = "ICD-10 2017:M62.5~M62.6~M79.9" 
* #L20 "Gelenksymptome/-beschwerden NNB"
* #L20 ^definition = Inklusive: Arthralgie~ Gelenkschmerz/-erguß/-schwellung~ Schmerz/Versteifung/Schwäche im Gelenk Exklusive: Symptome/Beschwerden spezifiziert in L07~ L08~ L10-13~ L15-17 Kriterien:
* #L20 ^property[0].code = #parent 
* #L20 ^property[0].valueCode = #L 
* #L20 ^property[1].code = #Relationships 
* #L20 ^property[1].valueString = "ICD-10 2017:M25.4~M25.5~M25.6~M25.8~M25.9" 
* #L26 "Angst vor Krebs muskuloskeletal"
* #L26 ^definition = Inklusive:  Exklusive: wenn der Patient die Erkrankung hat~ ist die Erkrankung zu kodieren Kriterien: Besorgnis über/Angst vor Krebs am Bewegungsapparat bei einem Patienten, der die Krankheit nicht hat/bevor die Diagnose gesichert ist
* #L26 ^property[0].code = #parent 
* #L26 ^property[0].valueCode = #L 
* #L26 ^property[1].code = #Relationships 
* #L26 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #L27 "Angst vor muskuloskeletaler Erkrankung, andere"
* #L27 ^definition = Inklusive:  Exklusive: Angst vor Krebs~ muskuloskelettal L26~ wenn der Patient die Erkrankung hat~ kodiere die Erkrankung Kriterien: Besorgnis über/Angst vor einer anderen Erkrankung des Bewegungsapparates bei einem Patienten, der die Krankheit nicht hat/bevor die Diagnose gesichert ist
* #L27 ^property[0].code = #parent 
* #L27 ^property[0].valueCode = #L 
* #L27 ^property[1].code = #Relationships 
* #L27 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #L28 "Funktionseinschränkung/Behinderung (L)"
* #L28 ^definition = Inklusive:  Exklusive: Stürze A29~ Hinken/Gehbeschwerden/Gangstörungen N29 Kriterien: Funktionseinschränkung/Behinderung durch ein Problem des Bewegungsapparates
* #L28 ^property[0].code = #parent 
* #L28 ^property[0].valueCode = #L 
* #L28 ^property[1].code = #Relationships 
* #L28 ^property[1].valueString = "ICD-10 2017:R26.2~Z73.6" 
* #L28 ^property[2].code = #hints 
* #L28 ^property[2].valueString = "Beachte:  Hinweise: COOP/WONCA Charts eignen sich zur Dokumentation des funktionellen Status des Patienten (siehe Kapitel 8)" 
* #L29 "Andere Symptome/Beschwerden muskuloskeletal"
* #L29 ^definition = Inklusive: "Wachstumsschmerz" beim Kind Exklusive: Uhrglasnägel S22 Kriterien:
* #L29 ^property[0].code = #parent 
* #L29 ^property[0].valueCode = #L 
* #L29 ^property[1].code = #Relationships 
* #L29 ^property[1].valueString = "ICD-10 2017:R26.8~R29.3~R29.8" 
* #L30 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? vollständig"
* #L30 ^property[0].code = #parent 
* #L30 ^property[0].valueCode = #L 
* #L31 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? teilweise"
* #L31 ^property[0].code = #parent 
* #L31 ^property[0].valueCode = #L 
* #L32 "Allergie-/Sensitivitätstestung"
* #L32 ^property[0].code = #parent 
* #L32 ^property[0].valueCode = #L 
* #L33 "Mikrobiologische/immunologische Untersuchung"
* #L33 ^property[0].code = #parent 
* #L33 ^property[0].valueCode = #L 
* #L34 "Blutuntersuchung"
* #L34 ^property[0].code = #parent 
* #L34 ^property[0].valueCode = #L 
* #L35 "Urinuntersuchung"
* #L35 ^property[0].code = #parent 
* #L35 ^property[0].valueCode = #L 
* #L36 "Stuhluntersuchung"
* #L36 ^property[0].code = #parent 
* #L36 ^property[0].valueCode = #L 
* #L37 "Histologische Untersuchung/zytologischer Abstrich"
* #L37 ^property[0].code = #parent 
* #L37 ^property[0].valueCode = #L 
* #L38 "Andere Laboruntersuchung NAK"
* #L38 ^property[0].code = #parent 
* #L38 ^property[0].valueCode = #L 
* #L39 "Körperliche Funktionsprüfung"
* #L39 ^property[0].code = #parent 
* #L39 ^property[0].valueCode = #L 
* #L40 "Diagnostische Endoskopie"
* #L40 ^property[0].code = #parent 
* #L40 ^property[0].valueCode = #L 
* #L41 "Diagnostisches Röntgen/Bildgebung"
* #L41 ^property[0].code = #parent 
* #L41 ^property[0].valueCode = #L 
* #L42 "Elektrische Aufzeichnungsverfahren"
* #L42 ^property[0].code = #parent 
* #L42 ^property[0].valueCode = #L 
* #L43 "Andere diagnostische Untersuchung"
* #L43 ^property[0].code = #parent 
* #L43 ^property[0].valueCode = #L 
* #L44 "Präventive Impfung/Medikation"
* #L44 ^property[0].code = #parent 
* #L44 ^property[0].valueCode = #L 
* #L45 "Beobachtung/Gesundheitsschulung/Beratung/Diät"
* #L45 ^property[0].code = #parent 
* #L45 ^property[0].valueCode = #L 
* #L46 "Konsultation eines anderen Primärversorgers"
* #L46 ^property[0].code = #parent 
* #L46 ^property[0].valueCode = #L 
* #L47 "Konsultation eines Facharztes"
* #L47 ^property[0].code = #parent 
* #L47 ^property[0].valueCode = #L 
* #L48 "Klärung/Besprechung des Beratungsanlasses mit dem Patienten"
* #L48 ^property[0].code = #parent 
* #L48 ^property[0].valueCode = #L 
* #L49 "Andere Vorsorgemaßnahme"
* #L49 ^property[0].code = #parent 
* #L49 ^property[0].valueCode = #L 
* #L50 "Medikation/Verschreibung/Erneuerung/Injektion"
* #L50 ^property[0].code = #parent 
* #L50 ^property[0].valueCode = #L 
* #L51 "Inzision/Drainage/Spülung/Absaugung/Entfernung von Körperflüssigkeit"
* #L51 ^property[0].code = #parent 
* #L51 ^property[0].valueCode = #L 
* #L52 "Exzision/Entfernung von Gewebe/Biopsie/Destruktion/Debridement/Verkauterung"
* #L52 ^property[0].code = #parent 
* #L52 ^property[0].valueCode = #L 
* #L53 "Instrumentelle Manipulation/Katheterisierung/Intubation/Dilatation"
* #L53 ^property[0].code = #parent 
* #L53 ^property[0].valueCode = #L 
* #L54 "Reposition/Fixierung/Naht/Gips/prothetische Versorgung"
* #L54 ^property[0].code = #parent 
* #L54 ^property[0].valueCode = #L 
* #L55 "Lokale Injektion/Infiltration"
* #L55 ^property[0].code = #parent 
* #L55 ^property[0].valueCode = #L 
* #L56 "Verband/Druck/Kompression/Tamponade"
* #L56 ^property[0].code = #parent 
* #L56 ^property[0].valueCode = #L 
* #L57 "Physikalische Therapie/Rehabilitation"
* #L57 ^property[0].code = #parent 
* #L57 ^property[0].valueCode = #L 
* #L58 "Therapeutische Beratung/Zuhören"
* #L58 ^property[0].code = #parent 
* #L58 ^property[0].valueCode = #L 
* #L59 "Andere therapeutische Maßnahme/kleine Chirurgie NAK"
* #L59 ^property[0].code = #parent 
* #L59 ^property[0].valueCode = #L 
* #L60 "Testresultat/Ergebnis eigene Maßnahme"
* #L60 ^property[0].code = #parent 
* #L60 ^property[0].valueCode = #L 
* #L61 "Ergebnis Untersuchung/Test/Brief von anderem Leistungserbringer"
* #L61 ^property[0].code = #parent 
* #L61 ^property[0].valueCode = #L 
* #L62 "Adminstrative Maßnahme"
* #L62 ^property[0].code = #parent 
* #L62 ^property[0].valueCode = #L 
* #L63 "Folgekonsultation unspezifiziert"
* #L63 ^property[0].code = #parent 
* #L63 ^property[0].valueCode = #L 
* #L64 "Konsultation/Problembehandlung auf Initiative des Leistungserbringers"
* #L64 ^property[0].code = #parent 
* #L64 ^property[0].valueCode = #L 
* #L65 "Konsultation/Problembehandlung auf Initiative Dritter"
* #L65 ^property[0].code = #parent 
* #L65 ^property[0].valueCode = #L 
* #L66 "Überweisung an andere/n Leistungserbringer (nicht ärztl.)/Pflegekraft/Therapeuten/Sozialarbeiter"
* #L66 ^property[0].code = #parent 
* #L66 ^property[0].valueCode = #L 
* #L67 "Überweisung an einen Arzt/Spezialisten/eine Klinik/ein Krankenhaus"
* #L67 ^property[0].code = #parent 
* #L67 ^property[0].valueCode = #L 
* #L68 "Andere Überweisung NAK"
* #L68 ^property[0].code = #parent 
* #L68 ^property[0].valueCode = #L 
* #L69 "Anderer Beratungsanlass NAK"
* #L69 ^property[0].code = #parent 
* #L69 ^property[0].valueCode = #L 
* #L70 "Infektion des muskuloskelettalen Systems"
* #L70 ^definition = Inklusive: Infektiöse Sehnenscheidentzündung~ Osteomyelitis~ eitrige Arthritis Exklusive: Morbus Reiter L99~ Spätfolgen einer Polio N70 Kriterien: Infektion im Bereich des Bewegungsapparates
* #L70 ^property[0].code = #parent 
* #L70 ^property[0].valueCode = #L 
* #L70 ^property[1].code = #Relationships 
* #L70 ^property[1].valueString = "ICD-10 2017:M00.0~M00.1~M00.2~M00.8~M00.9~M01.0~M01.1~M01.2~M01.3~M01.4~M01.5~M01.6~M01.8~M46.2~M46.3~M46.4~M46.5~M60.0~M65.0~M65.1~M71.0~M71.1~M72.6~M72.8~M86.0~M86.1~M86.2~M86.3~M86.4~M86.5~M86.6~M86.8~M86.9" 
* #L71 "Bösartige Neubildung muskuloskelettal"
* #L71 ^definition = Inklusive: Fibrosarkom~ Osteosarkom Exklusive: Sekundäre Neubildung (kodieren an der ursprünglichen Lokalisation)~ gutartige/nicht spezifizierte Neubildung des Bewegungsapparates L97 Kriterien: characteristisches histologisches Erscheingsbild
* #L71 ^property[0].code = #parent 
* #L71 ^property[0].valueCode = #L 
* #L71 ^property[1].code = #Relationships 
* #L71 ^property[1].valueString = "ICD-10 2017:C40.0~C40.1~C40.2~C40.3~C40.8~C40.9~C41.0~C41.1~C41.2~C41.3~C41.4~C41.8~C41.9~C46.1~C49.0~C49.1~C49.2~C49.3~C49.4~C49.5~C49.6~C49.8~C49.9" 
* #L72 "Fraktur: Radius/Ulna"
* #L72 ^definition = Inklusive: typische Radiusfraktur Exklusive: pathologische Fraktur L95~ L99~ Pseudarthrose L99 Kriterien: Frakturnachweis in der Bildgebung; oder Trauma plus sichtbare/tastbare Deformität oder Krepitation am Knochen
* #L72 ^property[0].code = #parent 
* #L72 ^property[0].valueCode = #L 
* #L72 ^property[1].code = #Relationships 
* #L72 ^property[1].valueString = "ICD-10 2017:S52.0~S52.1~S52.2~S52.3~S52.4~S52.5~S52.6~S52.7~S52.8~S52.9" 
* #L72 ^property[2].code = #hints 
* #L72 ^property[2].valueString = "Beachte: Symptome am Arm L09~ muskuloskelettale Verletzung NNB L81 Hinweise: " 
* #L73 "Fraktur: Tibia/Fibula"
* #L73 ^definition = Inklusive: Pottsche Fraktur (komplizierter Schenkelhalsbruch) Exklusive: Patellafraktur L76~ pathologische Fraktur L95~ L99~ Pseudarthrose L99 Kriterien: Frakturnachweis in der Bildgebung; oder Trauma plus sichtbare/tastbare Deformität oder Krepitation am Knochen
* #L73 ^property[0].code = #parent 
* #L73 ^property[0].valueCode = #L 
* #L73 ^property[1].code = #Relationships 
* #L73 ^property[1].valueString = "ICD-10 2017:S82.1~S82.2~S82.3~S82.4~S82.5~S82.6~S82.7~S82.8~S82.9" 
* #L73 ^property[2].code = #hints 
* #L73 ^property[2].valueString = "Beachte: Symptom am Bein L14~ Symptom am Knöchel L16~ muskuloskelettale Verletzung NNB L81 Hinweise: " 
* #L74 "Fraktur: Hand-/Fußknochen"
* #L74 ^definition = Inklusive: Handwurzel-/ Mittelhandfraktur~ Fraktur der Phalangen an Hand/Fuß Exklusive: pathologische Fraktur L95~ L99~ Pseudarthrose L99 Kriterien: Frakturnachweis in der Bildgebung; oder Trauma plus sichtbare/tastbare Deformität oder Krepitation am Knochen
* #L74 ^property[0].code = #parent 
* #L74 ^property[0].valueCode = #L 
* #L74 ^property[1].code = #Relationships 
* #L74 ^property[1].valueString = "ICD-10 2017:S62.0~S62.1~S62.2~S62.3~S62.4~S62.5~S62.6~S62.7~S62.8~S92.0~S92.1~S92.2~S92.3~S92.4~S92.5~S92.7~S92.9" 
* #L74 ^property[2].code = #hints 
* #L74 ^property[2].valueString = "Beachte: Symptom am Arm L09~ Symptome am Bein L14~ muskuloskelettale Verletzung NNB L81 Hinweise: " 
* #L75 "Fraktur: Femur"
* #L75 ^definition = Inklusive: Schenkelhalsfraktur Exklusive: pathologische Fraktur L95~ L99~ Pseudarthrose L99 Kriterien: Frakturnachweis in der Bildgebung; oder Trauma plus sichtbare/tastbare Deformität oder Krepitation am Knochen
* #L75 ^property[0].code = #parent 
* #L75 ^property[0].valueCode = #L 
* #L75 ^property[1].code = #Relationships 
* #L75 ^property[1].valueString = "ICD-10 2017:S72.0~S72.1~S72.2~S72.3~S72.4~S72.7~S72.8~S72.9" 
* #L75 ^property[2].code = #hints 
* #L75 ^property[2].valueString = "Beachte: Symptom am Bein L14~ muskuloskelettale Verletzung NNB L81 Hinweise: " 
* #L76 "Fraktur: andere"
* #L76 ^definition = Inklusive:  Exklusive: Frakturs spezifiziert in L72~ L73~ L74~ und L75~ pathologische Fraktur L95~ L99~ Pseudarthrose L99~ Schädelfraktur N80 Kriterien: Frakturnachweis in der Bildgebung; oder Trauma plus sichtbare/tastbare Dislokation des Knochens
* #L76 ^property[0].code = #parent 
* #L76 ^property[0].valueCode = #L 
* #L76 ^property[1].code = #Relationships 
* #L76 ^property[1].valueString = "ICD-10 2017:S02.2~S02.3~S02.4~S02.6~S02.7~S02.8~S02.9~S12.0~S12.1~S12.2~S12.7~S12.8~S12.9~S22.0~S22.1~S22.2~S22.3~S22.4~S22.5~S22.8~S22.9~S32.0~S32.1~S32.2~S32.3~S32.4~S32.5~S32.7~S32.8~S42.0~S42.1~S42.2~S42.3~S42.4~S42.7~S42.8~S42.9~S82.0~T08~T10~T12~T14.2" 
* #L76 ^property[2].code = #hints 
* #L76 ^property[2].valueString = "Beachte: Symptome in Komponente 1 Hinweise: " 
* #L77 "Verstauchung/Zerrung des Sprunggelenks"
* #L77 ^definition = Inklusive:  Exklusive:  Kriterien: Verstauchung/Zerrung des betroffenen Bereichs plus Schmerzen, die beim Dehnen oder Strecken verstärkt werden
* #L77 ^property[0].code = #parent 
* #L77 ^property[0].valueCode = #L 
* #L77 ^property[1].code = #Relationships 
* #L77 ^property[1].valueString = "ICD-10 2017:S93.4" 
* #L77 ^property[2].code = #hints 
* #L77 ^property[2].valueString = "Beachte: Symptom am Knöchel L16 Hinweise: " 
* #L78 "Verstauchung/Zerrung des Knies"
* #L78 ^definition = Inklusive:  Exklusive: akute Schädigung des Meniskus/der Kreuzbänder L96 Kriterien: Verstauchung/Zerrung des betroffenen Bereichs plus Schmerzen, die beim Dehnen oder Strecken verstärkt werden
* #L78 ^property[0].code = #parent 
* #L78 ^property[0].valueCode = #L 
* #L78 ^property[1].code = #Relationships 
* #L78 ^property[1].valueString = "ICD-10 2017:S83.4~S83.6" 
* #L79 "Verstauchung/Zerrung eines Gelenks NNB"
* #L79 ^definition = Inklusive: Stauchung/Zerrung anderer Gelenke/Bänder~ Whiplash-Verletzung Exklusive: Sprunggelenksdistorsion/-stauchung L77~ Distorsion/Stauchung im Knie L78~ Rückentrauma L84 Kriterien: Verstauchung/Zerrung des betroffenen Bereichs plus Schmerzen, die beim Dehnen oder Strecken verstärkt werden
* #L79 ^property[0].code = #parent 
* #L79 ^property[0].valueCode = #L 
* #L79 ^property[1].code = #Relationships 
* #L79 ^property[1].valueString = "ICD-10 2017:S03.4~S03.5~S13.4~S13.5~S13.6~S23.3~S23.4~S23.5~S33.6~S43.4~S43.5~S43.6~S43.7~S53.2~S53.3~S53.4~S63.3~S63.4~S63.5~S63.6~S63.7~S73.1~S93.2~S93.5~S93.6~T09.2~T11.2~T13.2~T14.3" 
* #L79 ^property[2].code = #hints 
* #L79 ^property[2].valueString = "Beachte: Symptome in Komponente 1 Hinweise: " 
* #L80 "Luxation/Subluxation Gelenk"
* #L80 ^definition = Inklusive: Dislokation/Subluxation an jeglicher Stelle~ einschließlich Wirbelsäule Exklusive:  Kriterien: Gelenktrauma plus entweder Bildnachweis einer Dislokation/ Subluxation, oder palpable/sichtbare Deformität durch Dislokation
* #L80 ^property[0].code = #parent 
* #L80 ^property[0].valueCode = #L 
* #L80 ^property[1].code = #Relationships 
* #L80 ^property[1].valueString = "ICD-10 2017:M22.0~M22.1~S03.0~S03.3~S13.0~S13.1~S13.2~S13.3~S23.0~S23.1~S23.2~S33.0~S33.1~S33.2~S33.3~S43.0~S43.1~S43.2~S43.3~S53.0~S53.1~S63.0~S63.1~S63.2~S73.0~S83.0~S83.1~S93.0~S93.1~S93.3~T09.2~T11.2~T13.2~T14.3" 
* #L80 ^property[2].code = #hints 
* #L80 ^property[2].valueString = "Beachte: Symptome in Komponente 1 Hinweise: Luxationen mit Fraktur sind unter der entsprechenden Fraktur zu kodieren" 
* #L81 "Verletzung muskuloskelettal NNB"
* #L81 ^definition = Inklusive: tief eingedrungener Fremdkörper~ Hämarthros~ traumatische Amputation Exklusive: innere Thorax-/Abdomen-/Beckenverletzung~ Polytrauma A81~ Verletzungsfolgen Deformitäten/Behinderung/Narben A82~ Zahnverletzung D80~ Trommelfellverletzung H77~ traumatische Gelenkerkrankung L91~ Pseudarthrose/Knochenheilung in Fehlstellung L99~ Kopfverlet Kriterien:
* #L81 ^property[0].code = #parent 
* #L81 ^property[0].valueCode = #L 
* #L81 ^property[1].code = #Relationships 
* #L81 ^property[1].valueString = "ICD-10 2017:M79.5~S09.1~S16~S20.2~S30.0~S30.1~S33.4~S39.0~S39.8~S39.9~S40.0~S46.0~S46.1~S46.2~S46.3~S46.7~S46.8~S46.9~S47~S48.0~S48.1~S48.9~S49.7~S49.8~S49.9~S50.0~S50.1~S56.0~S56.1~S56.2~S56.3~S56.4~S56.5~S56.7~S56.8~S57.0~S57.8~S57.9~S58.0~S58.1~S58.9~S59.7~S59.8~S59.9~S60.0~S60.1~S60.2~S66.0~S66.1~S66.2~S66.3~S66.4~S66.5~S66.6~S66.7~S66.8~S66.9~S67.0~S67.8~S68.0~S68.1~S68.2~S68.3~S68.4~S68.8~S68.9~S69.7~S69.8~S69.9~S70.0~S70.1~S76.0~S76.1~S76.2~S76.3~S76.4~S76.7~S77.0~S77.1~S77.2~S78.0~S78.1~S78.9~S79.7~S79.8~S79.9~S80.0~S80.1~S86.0~S86.1~S86.2~S86.3~S86.7~S86.8~S86.9~S87.0~S87.8~S88.0~S88.1~S88.9~S89.7~S89.8~S89.9~S90.0~S90.1~S90.2~S90.3~S96.0~S96.1~S96.2~S96.7~S96.8~S96.9~S97.0~S97.1~S97.8~S98.0~S98.1~S98.2~S98.3~S98.4~S99.7~S99.8~S99.9~T06.4~T09.5~T09.6~T09.8~T09.9~T11.0~T11.5~T11.6~T11.8~T11.9~T13.0~T13.4~T13.5~T13.6~T13.8~T13.9~T14.3~T14.6~T14.7" 
* #L82 "Angeborene Anomalie muskuloskelettal"
* #L82 ^definition = Inklusive: O-Beine~ Klumpfuß~ angeborene Hüftluxation~ Genu recurvatum~ Fehlbildung am Schädel/im Gesicht~ andere angeborene Fehlbildung des Fußes Exklusive: Skoliose L85~ Plattfuß (erworben) L98~ Spina bifida N85 Kriterien:
* #L82 ^property[0].code = #parent 
* #L82 ^property[0].valueCode = #L 
* #L82 ^property[1].code = #Relationships 
* #L82 ^property[1].valueString = "ICD-10 2017:Q65.0~Q65.1~Q65.2~Q65.3~Q65.4~Q65.5~Q65.6~Q65.8~Q65.9~Q66.0~Q66.1~Q66.2~Q66.3~Q66.4~Q66.5~Q66.6~Q66.7~Q66.8~Q66.9~Q67.0~Q67.1~Q67.2~Q67.3~Q67.4~Q67.5~Q67.6~Q67.7~Q67.8~Q68.0~Q68.1~Q68.2~Q68.3~Q68.4~Q68.5~Q68.8~Q69.0~Q69.1~Q69.2~Q69.9~Q70.0~Q70.1~Q70.2~Q70.3~Q70.4~Q70.9~Q71.0~Q71.1~Q71.2~Q71.3~Q71.4~Q71.5~Q71.6~Q71.8~Q71.9~Q72.0~Q72.1~Q72.2~Q72.3~Q72.4~Q72.5~Q72.6~Q72.7~Q72.8~Q72.9~Q73.0~Q73.1~Q73.8~Q74.0~Q74.1~Q74.2~Q74.3~Q74.8~Q74.9~Q75.0~Q75.1~Q75.2~Q75.3~Q75.4~Q75.5~Q75.8~Q75.9~Q76.0~Q76.1~Q76.2~Q76.3~Q76.4~Q76.5~Q76.6~Q76.7~Q76.8~Q76.9~Q77.0~Q77.1~Q77.2~Q77.3~Q77.4~Q77.5~Q77.6~Q77.7~Q77.8~Q77.9~Q78.0~Q78.1~Q78.2~Q78.3~Q78.4~Q78.5~Q78.6~Q78.8~Q78.9~Q79.0~Q79.1~Q79.2~Q79.3~Q79.4~Q79.5~Q79.6~Q79.8~Q79.9" 
* #L83 "Halswirbelsäulensyndrom"
* #L83 ^definition = Inklusive: Syndrome mit/ohne Schmerzausstrahlung~ Bandscheibenschaden im Bereich der Halswirbelsäule~ Bandscheibenschaden~ zervikobrachiales Syndrom~ zervikaler Kopfschmerz~ Osteoarthritis des Halses~ Wurzelsyndrom der oberen Extremität~ Spondylose~ Torticollis Exklusive:  Kriterien:
* #L83 ^property[0].code = #parent 
* #L83 ^property[0].valueCode = #L 
* #L83 ^property[1].code = #Relationships 
* #L83 ^property[1].valueString = "ICD-10 2017:M43.0~M43.1~M43.3~M43.4~M43.6~M46.0~M47.1~M47.2~M47.8~M47.9~M48.0~M48.1~M48.2~M48.3~M48.4~M48.5~M48.8~M48.9~M50.0~M50.1~M50.2~M50.3~M50.8~M50.9~M53.0~M53.1" 
* #L84 "Rückensyndrom ohne Schmerzausstrahlung"
* #L84 ^definition = Inklusive: Rückenverspannung~ Wirbelkompression NNB~ Gelenkflächendegeneration~ Osteoarthrose/Osteoarthritis der Wirbelsäule~ Spondylolisthesis~ Spondylose Exklusive: Coccygodynie L03~ auf den Nacken bezogene Syndrome L83~ Rückenschmerzen mit Ausstrahlung/Ischialgie L86~ psychogene Rückenschmerzen P75 Kriterien: Rückenschmerz ohne Ausstrahlung plus durch körperliche Untersuchung bestätigte Bewegungseinschränkung
* #L84 ^property[0].code = #parent 
* #L84 ^property[0].valueCode = #L 
* #L84 ^property[1].code = #Relationships 
* #L84 ^property[1].valueString = "ICD-10 2017:M43.0~M43.1~M43.5~M46.0~M46.1~M46.8~M46.9~M47.0~M47.8~M47.9~M48.0~M48.1~M48.2~M48.3~M48.4~M48.5~M48.8~M48.9~M51.2~M51.3~M51.4~M51.8~M51.9~M53.2~M53.3~M53.8~M53.9~S33.5~S33.7" 
* #L84 ^property[2].code = #hints 
* #L84 ^property[2].valueString = "Beachte: Symptom/Beschwerden Rücken L02~ Symptom/Beschwerden des unteren Rückens L03 Hinweise: " 
* #L85 "Erworbene Deformität der Wirbelsäule"
* #L85 ^definition = Inklusive: Kyphoskoliose~ Kyphose~ Lordose~ Skoliose Exklusive: angeborene Deformität L82~ Spondylitis ankylosans L88~ Spondylolisthesis L84 Kriterien:
* #L85 ^property[0].code = #parent 
* #L85 ^property[0].valueCode = #L 
* #L85 ^property[1].code = #Relationships 
* #L85 ^property[1].valueString = "ICD-10 2017:M40.0~M40.1~M40.2~M40.3~M40.4~M40.5~M41.0~M41.1~M41.2~M41.3~M41.4~M41.5~M41.8~M41.9~M43.8~M43.9" 
* #L86 "Rückensyndrom mit Schmerzausstrahlung"
* #L86 ^definition = Inklusive: Bandscheibenprolaps/-degeneration~ Ischias Exklusive: Bandscheibenläsion L83~ Spondylolisthesis L84~ frisches Rückentrauma L84 Kriterien: Schmerz im lumbalen/thorakalen Bereich der Wirbelsäule, begleitet durch Ausstrahlung, oder einen neurologischen Ausfall in einem entsprechenden Bereich, oder Ischias, ausstrahlender Schmerz im Bein, der sich bei Bewegung, Husten oder Verlagerung verstärkt, oder Nachweis eines Bandscheibenvorfalls im Lenden- oder Brustwirbelbereich durch entsprechende bildgebende Verfahren oder Operation
* #L86 ^property[0].code = #parent 
* #L86 ^property[0].valueCode = #L 
* #L86 ^property[1].code = #Relationships 
* #L86 ^property[1].valueString = "ICD-10 2017:G55.0~G55.1~G55.2~G55.3~G55.8~M47.1~M47.2~M51.0~M51.1~M51.2~M51.3~M51.4~M51.8~M51.9~M54.3~M54.4" 
* #L86 ^property[2].code = #hints 
* #L86 ^property[2].valueString = "Beachte: Rückenschmerz L02~ Schmerzen im unteren Rückens L03 Hinweise: Vom Patienten berichtete diffuse Schmerzen sind hier nicht zu kodieren" 
* #L87 "Bursitis/Tendinitis/Synovitis NNB"
* #L87 ^definition = Inklusive: Knochensporn~ Bänderverkalkung~ Dupuytrensche Kontraktur~ Fasciitis~ Ganglion~ Synovialzyste~ Tendosynovitis~ schnellender Finger Exklusive: Bursitis/Tendinitis/Synovitis der Schulter L92~ Tennisellbogen/laterale Epikondylitis L93 Kriterien:
* #L87 ^property[0].code = #parent 
* #L87 ^property[0].valueCode = #L 
* #L87 ^property[1].code = #Relationships 
* #L87 ^property[1].valueString = "ICD-10 2017:M65.2~M65.3~M65.4~M65.8~M65.9~M67.3~M67.4~M70.0~M70.1~M70.2~M70.3~M70.4~M70.5~M70.6~M70.7~M70.8~M70.9~M71.2~M71.3~M71.4~M71.5~M71.8~M71.9~M72.0~M72.1~M72.2~M72.4~M72.9~M76.0~M76.1~M76.2~M76.3~M76.4~M76.5~M76.6~M76.7~M76.8~M76.9~M77.0~M77.2~M77.3~M77.5~M77.8~M77.9" 
* #L88 "Rheumatoide/seropositive Arthritis"
* #L88 ^definition = Inklusive: verwandte Zustände: Spondylitis ankylosans~ juvenile Arthritis Exklusive: Psoriatische Gelenkmanifestation L99 Kriterien:
* #L88 ^property[0].code = #parent 
* #L88 ^property[0].valueCode = #L 
* #L88 ^property[1].code = #Relationships 
* #L88 ^property[1].valueString = "ICD-10 2017:M05.0~M05.1~M05.2~M05.3~M05.8~M05.9~M06.0~M06.1~M06.2~M06.3~M06.4~M06.8~M06.9~M08.0~M08.1~M08.2~M08.3~M08.4~M08.8~M08.9~M45" 
* #L89 "Arthrose der Hüfte"
* #L89 ^definition = Inklusive: Sekundäre Coxarthrose nach Dysplasie/Trauma Exklusive:  Kriterien: entweder charakteristisches radiologisches Erscheinungsbild; oder eine mindestens drei Monate andauernde Gelenkstörung ohne konstitutionelle Symptome, begleitet von mindestens drei der folgenden Symptome: internittierende Schwellung, Krepitation, Steifheit oder Bewegungseinschränkung, normale BSG und Rheumatest sowie Harnsäure, Patient ist über 40 Jahre
* #L89 ^property[0].code = #parent 
* #L89 ^property[0].valueCode = #L 
* #L89 ^property[1].code = #Relationships 
* #L89 ^property[1].valueString = "ICD-10 2017:M16.0~M16.1~M16.2~M16.3~M16.4~M16.5~M16.6~M16.7~M16.9" 
* #L89 ^property[2].code = #hints 
* #L89 ^property[2].valueString = "Beachte: Gelenksymptom L20~ Arthritis NNB L91 Hinweise: " 
* #L90 "Arthrose des Knies"
* #L90 ^definition = Inklusive: Sekundäre Gonarthrose nach Dysplasie/Trauma Exklusive:  Kriterien: entweder charakteristisches radiologisches Erscheinungsbild; oder eine mindestens drei Monate andauernde Gelenkstörung ohne konstitutionelle Symptome, begleitet von mindestens drei der folgenden Symptome: internittierende Schwellung, Krepitation, Steifheit oder Bewegungseinschränkung, normale BSG und Rheumatest sowie Harnsäure, Patient ist über 40 Jahre
* #L90 ^property[0].code = #parent 
* #L90 ^property[0].valueCode = #L 
* #L90 ^property[1].code = #Relationships 
* #L90 ^property[1].valueString = "ICD-10 2017:M17.0~M17.1~M17.2~M17.3~M17.4~M17.5~M17.9" 
* #L90 ^property[2].code = #hints 
* #L90 ^property[2].valueString = "Beachte: Gelenksymptom L20~ Arthritis NNB L91 Hinweise: " 
* #L91 "Arthrose, andere"
* #L91 ^definition = Inklusive: Arthritis NNB~ Heberdensche Knötchen~ Osteoarthritis~ traumatische Arthropathie Exklusive: Arthrose im Halsbereich L83~ Arthrose der Wirbelsäule L84~ Coxarthrose L89~ Gonarthrose L90~ Arthrose der Schulter L92 Kriterien: entweder charakteristisches radiologisches Erscheinungsbild; oder Heberden-Knoten, oder eine mindestens drei Monate andauernde Gelenkstörung ohne konstitutionelle Symptome, begleitet von mindestens drei der folgenden Symptome: internittierende Schwellung, Krepitation, Steifheit oder Bewegungseinschränkung, normale BSG und Rheumatest sowie Harnsäure, Patient ist über 40 Jahre
* #L91 ^property[0].code = #parent 
* #L91 ^property[0].valueCode = #L 
* #L91 ^property[1].code = #Relationships 
* #L91 ^property[1].valueString = "ICD-10 2017:M13.0~M13.1~M13.8~M13.9~M15.0~M15.1~M15.2~M15.3~M15.4~M15.8~M15.9~M18.0~M18.1~M18.2~M18.3~M18.4~M18.5~M18.9~M19.0~M19.1~M19.2~M19.8~M19.9" 
* #L91 ^property[2].code = #hints 
* #L91 ^property[2].valueString = "Beachte: Gelenksymptom L20 Hinweise: " 
* #L92 "Schultersyndrom"
* #L92 ^definition = Inklusive: Bursitis der Schulter~ Schulterversteifung~ Osteoarthrose/Synovitis der Schulter~ Rotatorenmanschettensyndrom~ Tendinitis der Schulter Exklusive:  Kriterien: Schulterschmerz mit Bewegungseinschränkung oder lokaler Schmerzhäufigkeit oder Krepitation; oder Bildnachweis einer periartikulären Verkalkung
* #L92 ^property[0].code = #parent 
* #L92 ^property[0].valueCode = #L 
* #L92 ^property[1].code = #Relationships 
* #L92 ^property[1].valueString = "ICD-10 2017:M19.0~M19.1~M19.2~M19.8~M19.9~M75.0~M75.1~M75.2~M75.3~M75.4~M75.5~M75.8~M75.9" 
* #L93 "Tennisellbogen"
* #L93 ^definition = Inklusive: laterale Epikondylitis Exklusive: andere Tendinitis L87 Kriterien:
* #L93 ^property[0].code = #parent 
* #L93 ^property[0].valueCode = #L 
* #L93 ^property[1].code = #Relationships 
* #L93 ^property[1].valueString = "ICD-10 2017:M77.1" 
* #L94 "Osteochondrose"
* #L94 ^definition = Inklusive: Perthes-Legg-Calvé-Erkrankung~ Morbus Osgood-Schlatter~ Osteochondritis dissecans~ Morbus Scheuermann~ Epiphysenlösung des Oberschenkels Exklusive:  Kriterien:
* #L94 ^property[0].code = #parent 
* #L94 ^property[0].valueCode = #L 
* #L94 ^property[1].code = #Relationships 
* #L94 ^property[1].valueString = "ICD-10 2017:M42.0~M42.1~M42.9~M91.0~M91.1~M91.2~M91.3~M91.8~M91.9~M92.0~M92.1~M92.2~M92.3~M92.4~M92.5~M92.6~M92.7~M92.8~M92.9~M93.0~M93.1~M93.2~M93.8~M93.9" 
* #L95 "Osteoporose"
* #L95 ^definition = Inklusive: pathologische Fraktur durch Osteoporose Exklusive:  Kriterien: characteristisches radiologisches Erscheinungsbild
* #L95 ^property[0].code = #parent 
* #L95 ^property[0].valueCode = #L 
* #L95 ^property[1].code = #Relationships 
* #L95 ^property[1].valueString = "ICD-10 2017:M80.0~M80.1~M80.2~M80.3~M80.4~M80.5~M80.8~M80.9~M81.0~M81.1~M81.2~M81.3~M81.4~M81.5~M81.6~M81.8~M81.9~M82.0~M82.1~M82.8" 
* #L96 "Akuter Kniebinnenschaden"
* #L96 ^definition = Inklusive: akute Meniskus-/Kreuzbandschädigung Exklusive: akute Schädigung der Seitenbänder L78~ Dislokation der Patella L80~ chronischer Kniebinnenschaden L99 Kriterien: Eine auslösende Verletzung, die nicht mehr als einen Monat zurückliegt, sowie Nachweis eines Bänder- oder Meniskusrisses durch Operation, Arthroskopie oder bildgebende Verfahren; oder Auftreten von Streck- oder Beugehemmung, Schmerzen im Knie und Schwellung des Knies
* #L96 ^property[0].code = #parent 
* #L96 ^property[0].valueCode = #L 
* #L96 ^property[1].code = #Relationships 
* #L96 ^property[1].valueString = "ICD-10 2017:S83.2~S83.3~S83.5~S83.7" 
* #L96 ^property[2].code = #hints 
* #L96 ^property[2].valueString = "Beachte: Kniesymptom L15~ Kniezerrung L78 Hinweise: " 
* #L97 "Neubildung muskuloskelettal, gutartig/unspezifiziert"
* #L97 ^definition = Inklusive: gutartige muskuloskelettäre Neubildung~ nicht als gutartige oder bösartig klassifizierte muskuloskelettäre Neubildung wenn Histologie nicht verfügbar Exklusive: bösartige Neubildung des Bewegungsapparates L71 Kriterien:
* #L97 ^property[0].code = #parent 
* #L97 ^property[0].valueCode = #L 
* #L97 ^property[1].code = #Relationships 
* #L97 ^property[1].valueString = "ICD-10 2017:D16.0~D16.1~D16.2~D16.3~D16.4~D16.5~D16.6~D16.7~D16.8~D16.9~D21.0~D21.1~D21.2~D21.3~D21.4~D21.5~D21.6~D21.9~D48.0~D48.1~K10.1" 
* #L98 "Erworbene Deformität einer Extremität"
* #L98 ^definition = Inklusive: entzündeter Fußballen~ Genu valgum-varum~ Hallux valgus/varus~ Hammerzehe~ Senkfuß/Plattfuß Exklusive: allgemeine angeborene Deformitäten und Anomalien A90~ Deformitäten und Anomalien des Bewegungsapparates L82 Kriterien:
* #L98 ^property[0].code = #parent 
* #L98 ^property[0].valueCode = #L 
* #L98 ^property[1].code = #Relationships 
* #L98 ^property[1].valueString = "ICD-10 2017:M20.0~M20.1~M20.2~M20.3~M20.4~M20.5~M20.6~M21.0~M21.1~M21.2~M21.3~M21.4~M21.5~M21.6~M21.7~M21.8~M21.9" 
* #L99 "Andere muskuloskelettale Erkrankung"
* #L99 ^definition = Inklusive: Arthrodese~ chronischer Kniebinnenschaden~ Kontrakturen~ Tietze-Syndrom~ Dermatomyositis~ Störungen der Kniescheibe~ schlecht oder nicht verheilte Fraktur~ Myositis~ Morbus Paget~ Pannikulitis~ pathologische Fraktur NNB~ Polymyalgia rheumatica~ Psoriasisa Exklusive: Hyperurikämie A91~ pathologische Fraktur durch Osteoporose L95~ Lähmung als Folge von Kinderlähmung N70~ Lähmung nach Schlaganfall N81~ Gicht T92~ Chondrokalzinose/Kristallarthropathie~ Osteomalazie T99 Kriterien:
* #L99 ^property[0].code = #parent 
* #L99 ^property[0].valueCode = #L 
* #L99 ^property[1].code = #Relationships 
* #L99 ^property[1].valueString = "ICD-10 2017:K09.2~K10.9~M02.0~M02.1~M02.2~M02.3~M02.8~M02.9~M03.0~M03.1~M03.2~M03.6~M07.0~M07.1~M07.2~M07.3~M07.4~M07.5~M07.6~M09.0~M09.1~M09.2~M09.8~M12.0~M12.1~M12.2~M12.3~M12.4~M12.5~M12.8~M14.0~M14.1~M14.2~M14.3~M14.4~M14.5~M14.6~M14.8~M22.2~M22.3~M22.4~M22.8~M22.9~M23.0~M23.1~M23.2~M23.3~M23.4~M23.5~M23.6~M23.8~M23.9~M24.0~M24.1~M24.2~M24.3~M24.4~M24.5~M24.6~M24.7~M24.8~M24.9~M25.0~M25.1~M25.2~M25.3~M25.7~M25.8~M25.9~M32.0~M32.1~M32.8~M32.9~M33.0~M33.1~M33.2~M33.9~M34.0~M34.1~M34.2~M34.8~M34.9~M35.0~M35.1~M35.2~M35.3~M35.4~M35.5~M35.6~M35.7~M35.8~M35.9~M36.0~M36.1~M36.2~M36.3~M36.4~M36.8~M43.2~M49.0~M49.1~M49.2~M49.3~M49.4~M49.5~M49.8~M54.1~M61.0~M61.1~M61.2~M61.3~M61.4~M61.5~M61.9~M62.0~M62.1~M62.2~M62.3~M62.4~M62.8~M62.9~M63.0~M63.1~M63.2~M63.3~M63.8~M66.0~M66.1~M66.2~M66.3~M66.4~M66.5~M67.0~M67.1~M67.2~M67.8~M67.9~M68.0~M68.8~M73.0~M73.1~M73.8~M79.4~M79.8~M84.0~M84.1~M84.2~M84.3~M84.4~M84.8~M84.9~M85.0~M85.1~M85.2~M85.3~M85.4~M85.5~M85.6~M85.8~M85.9~M87.0~M87.1~M87.2~M87.3~M87.8~M87.9~M88.0~M88.8~M88.9~M89.0~M89.1~M89.2~M89.3~M89.4~M89.5~M89.6~M89.8~M89.9~M90.0~M90.1~M90.2~M90.3~M90.4~M90.5~M90.6~M90.7~M90.8~M94.0~M94.1~M94.2~M94.3~M94.8~M94.9~M95.0~M95.1~M95.2~M95.3~M95.4~M95.5~M95.8~M95.9~M99.0~M99.1~M99.2~M99.3~M99.4~M99.5~M99.6~M99.7~M99.8~M99.9~T79.6~Z89.0~Z89.1~Z89.2~Z89.3~Z89.4~Z89.5~Z89.6~Z89.7~Z89.8~Z89.9~Z98.1" 
* #N "Neurologisch"
* #N ^property[0].code = #child 
* #N ^property[0].valueCode = #N01 
* #N ^property[1].code = #child 
* #N ^property[1].valueCode = #N03 
* #N ^property[2].code = #child 
* #N ^property[2].valueCode = #N04 
* #N ^property[3].code = #child 
* #N ^property[3].valueCode = #N05 
* #N ^property[4].code = #child 
* #N ^property[4].valueCode = #N06 
* #N ^property[5].code = #child 
* #N ^property[5].valueCode = #N07 
* #N ^property[6].code = #child 
* #N ^property[6].valueCode = #N08 
* #N ^property[7].code = #child 
* #N ^property[7].valueCode = #N16 
* #N ^property[8].code = #child 
* #N ^property[8].valueCode = #N17 
* #N ^property[9].code = #child 
* #N ^property[9].valueCode = #N18 
* #N ^property[10].code = #child 
* #N ^property[10].valueCode = #N19 
* #N ^property[11].code = #child 
* #N ^property[11].valueCode = #N26 
* #N ^property[12].code = #child 
* #N ^property[12].valueCode = #N27 
* #N ^property[13].code = #child 
* #N ^property[13].valueCode = #N28 
* #N ^property[14].code = #child 
* #N ^property[14].valueCode = #N29 
* #N ^property[15].code = #child 
* #N ^property[15].valueCode = #N30 
* #N ^property[16].code = #child 
* #N ^property[16].valueCode = #N31 
* #N ^property[17].code = #child 
* #N ^property[17].valueCode = #N32 
* #N ^property[18].code = #child 
* #N ^property[18].valueCode = #N33 
* #N ^property[19].code = #child 
* #N ^property[19].valueCode = #N34 
* #N ^property[20].code = #child 
* #N ^property[20].valueCode = #N35 
* #N ^property[21].code = #child 
* #N ^property[21].valueCode = #N36 
* #N ^property[22].code = #child 
* #N ^property[22].valueCode = #N37 
* #N ^property[23].code = #child 
* #N ^property[23].valueCode = #N38 
* #N ^property[24].code = #child 
* #N ^property[24].valueCode = #N39 
* #N ^property[25].code = #child 
* #N ^property[25].valueCode = #N40 
* #N ^property[26].code = #child 
* #N ^property[26].valueCode = #N41 
* #N ^property[27].code = #child 
* #N ^property[27].valueCode = #N42 
* #N ^property[28].code = #child 
* #N ^property[28].valueCode = #N43 
* #N ^property[29].code = #child 
* #N ^property[29].valueCode = #N44 
* #N ^property[30].code = #child 
* #N ^property[30].valueCode = #N45 
* #N ^property[31].code = #child 
* #N ^property[31].valueCode = #N46 
* #N ^property[32].code = #child 
* #N ^property[32].valueCode = #N47 
* #N ^property[33].code = #child 
* #N ^property[33].valueCode = #N48 
* #N ^property[34].code = #child 
* #N ^property[34].valueCode = #N49 
* #N ^property[35].code = #child 
* #N ^property[35].valueCode = #N50 
* #N ^property[36].code = #child 
* #N ^property[36].valueCode = #N51 
* #N ^property[37].code = #child 
* #N ^property[37].valueCode = #N52 
* #N ^property[38].code = #child 
* #N ^property[38].valueCode = #N53 
* #N ^property[39].code = #child 
* #N ^property[39].valueCode = #N54 
* #N ^property[40].code = #child 
* #N ^property[40].valueCode = #N55 
* #N ^property[41].code = #child 
* #N ^property[41].valueCode = #N56 
* #N ^property[42].code = #child 
* #N ^property[42].valueCode = #N57 
* #N ^property[43].code = #child 
* #N ^property[43].valueCode = #N58 
* #N ^property[44].code = #child 
* #N ^property[44].valueCode = #N59 
* #N ^property[45].code = #child 
* #N ^property[45].valueCode = #N60 
* #N ^property[46].code = #child 
* #N ^property[46].valueCode = #N61 
* #N ^property[47].code = #child 
* #N ^property[47].valueCode = #N62 
* #N ^property[48].code = #child 
* #N ^property[48].valueCode = #N63 
* #N ^property[49].code = #child 
* #N ^property[49].valueCode = #N64 
* #N ^property[50].code = #child 
* #N ^property[50].valueCode = #N65 
* #N ^property[51].code = #child 
* #N ^property[51].valueCode = #N66 
* #N ^property[52].code = #child 
* #N ^property[52].valueCode = #N67 
* #N ^property[53].code = #child 
* #N ^property[53].valueCode = #N68 
* #N ^property[54].code = #child 
* #N ^property[54].valueCode = #N69 
* #N ^property[55].code = #child 
* #N ^property[55].valueCode = #N70 
* #N ^property[56].code = #child 
* #N ^property[56].valueCode = #N71 
* #N ^property[57].code = #child 
* #N ^property[57].valueCode = #N72 
* #N ^property[58].code = #child 
* #N ^property[58].valueCode = #N73 
* #N ^property[59].code = #child 
* #N ^property[59].valueCode = #N74 
* #N ^property[60].code = #child 
* #N ^property[60].valueCode = #N75 
* #N ^property[61].code = #child 
* #N ^property[61].valueCode = #N76 
* #N ^property[62].code = #child 
* #N ^property[62].valueCode = #N79 
* #N ^property[63].code = #child 
* #N ^property[63].valueCode = #N80 
* #N ^property[64].code = #child 
* #N ^property[64].valueCode = #N81 
* #N ^property[65].code = #child 
* #N ^property[65].valueCode = #N85 
* #N ^property[66].code = #child 
* #N ^property[66].valueCode = #N86 
* #N ^property[67].code = #child 
* #N ^property[67].valueCode = #N87 
* #N ^property[68].code = #child 
* #N ^property[68].valueCode = #N88 
* #N ^property[69].code = #child 
* #N ^property[69].valueCode = #N89 
* #N ^property[70].code = #child 
* #N ^property[70].valueCode = #N90 
* #N ^property[71].code = #child 
* #N ^property[71].valueCode = #N91 
* #N ^property[72].code = #child 
* #N ^property[72].valueCode = #N92 
* #N ^property[73].code = #child 
* #N ^property[73].valueCode = #N93 
* #N ^property[74].code = #child 
* #N ^property[74].valueCode = #N94 
* #N ^property[75].code = #child 
* #N ^property[75].valueCode = #N95 
* #N ^property[76].code = #child 
* #N ^property[76].valueCode = #N99 
* #N01 "Kopfschmerz"
* #N01 ^definition = Inklusive: posttraumatischer Kopfschmerz Exklusive: zervikaler Kopfschmerz L83~ Gesichtsschmerz N03~ Migräne N89~ Clusterkopfschmerz N90~ Spannungskopfschmerz N95~ atypische Gesichtsneuralgie N99~ Nebenhöhlenschmerzen R09~ Post-Zoster-Neuralgie S70 Kriterien:
* #N01 ^property[0].code = #parent 
* #N01 ^property[0].valueCode = #N 
* #N01 ^property[1].code = #Relationships 
* #N01 ^property[1].valueString = "ICD-10 2017:G44.3~G44.8~R51" 
* #N03 "Gesichtsschmerz"
* #N03 ^definition = Inklusive:  Exklusive: Zahnschmerz D19~ Kopfschmerz N01~ Migräne N89~ Trigeminusneuralgie N92~ Nebenhöhlenschmerzen R09~ Post-Zoster-Neuralgie S70 Kriterien:
* #N03 ^property[0].code = #parent 
* #N03 ^property[0].valueCode = #N 
* #N03 ^property[1].code = #Relationships 
* #N03 ^property[1].valueString = "ICD-10 2017:G50.1~R51" 
* #N04 "Restless legs"
* #N04 ^definition = Inklusive:  Exklusive: Beinkrämpfe L14~ Claudicatio intermittens K92 Kriterien:
* #N04 ^property[0].code = #parent 
* #N04 ^property[0].valueCode = #N 
* #N04 ^property[1].code = #Relationships 
* #N04 ^property[1].valueString = "ICD-10 2017:G25.8" 
* #N05 "Kribbeln Finger/Füße/Zehen"
* #N05 ^definition = Inklusive: brennendes/prickelndes Gefühl an Fingern/Füßen/Zehen~ Parästhesien Exklusive: Schmerzen/Berührungsempfindlichkeit der Haut S01 Kriterien:
* #N05 ^property[0].code = #parent 
* #N05 ^property[0].valueCode = #N 
* #N05 ^property[1].code = #Relationships 
* #N05 ^property[1].valueString = "ICD-10 2017:R20.2" 
* #N06 "Empfindungsstörung, andere"
* #N06 ^definition = Inklusive: Anästhesie~ Taubheitsgefühl Exklusive: Kribbelgefühl der Finger/Füße/Zehen N05~ Schmerzen/Berührungsempfindlichkeit der Haut S01 Kriterien:
* #N06 ^property[0].code = #parent 
* #N06 ^property[0].valueCode = #N 
* #N06 ^property[1].code = #Relationships 
* #N06 ^property[1].valueString = "ICD-10 2017:R20.0~R20.1~R20.3~R20.8" 
* #N07 "Krampfanfälle, neurologische Anfälle"
* #N07 ^definition = Inklusive: Fieberkrämpfe~ Anfälle Exklusive: Ohnmacht A06~ TIA K89 Kriterien:
* #N07 ^property[0].code = #parent 
* #N07 ^property[0].valueCode = #N 
* #N07 ^property[1].code = #Relationships 
* #N07 ^property[1].valueString = "ICD-10 2017:R56.0~R56.8" 
* #N08 "Abnorme unwillkürliche Bewegungen"
* #N08 ^definition = Inklusive: Dystonie~ ruckartige Bewegungen~ Myoklonie~ Schüttelkrämpfe~ Spasmus~ Lähmung~ Tremor~ Zuckungen Exklusive: Chorea K71~ Krämpfe L12~ L14~ L17~ L18~ Restless legs N04~ Krampfanfall N07~ Tic douloureux N92~ organische Tics N99~ psychogene Tics P10 Kriterien:
* #N08 ^property[0].code = #parent 
* #N08 ^property[0].valueCode = #N 
* #N08 ^property[1].code = #Relationships 
* #N08 ^property[1].valueString = "ICD-10 2017:G25.0~G25.1~G25.2~G25.3~G25.4~G25.5~G25.6~G25.8~G25.9~R25.0~R25.1~R25.3~R25.8~R29.0" 
* #N16 "Geruchs-/Geschmacksstörung"
* #N16 ^definition = Inklusive: Anosmie Exklusive: Mundgeruch D20 Kriterien:
* #N16 ^property[0].code = #parent 
* #N16 ^property[0].valueCode = #N 
* #N16 ^property[1].code = #Relationships 
* #N16 ^property[1].valueString = "ICD-10 2017:R43.0~R43.1~R43.2~R43.8" 
* #N17 "Schwindel/Benommenheit"
* #N17 ^definition = Inklusive: Schwindelgefühl~ Ohnmachtsgefühl/Benommenheit~ Gleichgewichtsverlust~ benebelt Exklusive: Synkope/Ohnmacht A06~ Reisekrankheit A88~ spezielle Schwindelsyndrome H82 Kriterien:
* #N17 ^property[0].code = #parent 
* #N17 ^property[0].valueCode = #N 
* #N17 ^property[1].code = #Relationships 
* #N17 ^property[1].valueString = "ICD-10 2017:R42" 
* #N18 "Lähmung/Schwäche"
* #N18 ^definition = Inklusive: Parese Exklusive: allgemeine Schwäche A04 Kriterien:
* #N18 ^property[0].code = #parent 
* #N18 ^property[0].valueCode = #N 
* #N18 ^property[1].code = #Relationships 
* #N18 ^property[1].valueString = "ICD-10 2017:G81.0~G81.1~G81.9~G82.0~G82.1~G82.2~G82.3~G82.4~G82.5~G83.0~G83.1~G83.2~G83.3~G83.4~G83.5~G83.8~G83.9~G98" 
* #N19 "Sprachstörung"
* #N19 ^definition = Inklusive: Aphasie~ Dysphasie~ Dysarthrie~ verwaschene Sprache Exklusive: Stammeln/ Stottern P10~Sprachentwicklungsverzögerung P22~Heiserkeit R23 Kriterien:
* #N19 ^property[0].code = #parent 
* #N19 ^property[0].valueCode = #N 
* #N19 ^property[1].code = #Relationships 
* #N19 ^property[1].valueString = "ICD-10 2017:R47.0~R47.1~R47.8" 
* #N26 "Angst vor Krebs des Nervensystems"
* #N26 ^definition = Inklusive:  Exklusive: wenn der Patient die Erkrankung hat~ kodiere die Erkrankung Kriterien: Besorgnis über/Angst vor Krebs des Nervensystem bei einem Patienten, der die Krankheit nicht hat/bevor die Diagnose gesichert ist
* #N26 ^property[0].code = #parent 
* #N26 ^property[0].valueCode = #N 
* #N26 ^property[1].code = #Relationships 
* #N26 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #N27 "Angst vor Nervenleiden, andere"
* #N27 ^definition = Inklusive:  Exklusive: Angst vor Krebs des Nervensystems N26~ wenn der Patient die Erkrankung hat~ kodiere die Erkrankung Kriterien: Besorgnis über/Angst vor einer anderen Erkrankung des Nervensystems bei einem Patienten, der die Krankheit nicht hat/bevor die Diagnose gesichert ist
* #N27 ^property[0].code = #parent 
* #N27 ^property[0].valueCode = #N 
* #N27 ^property[1].code = #Relationships 
* #N27 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #N28 "Funktionseinschränkung/Behinderung (N)"
* #N28 ^definition = Inklusive: Behinderung durch neurologische Erkrankungen~ Störungen Exklusive:  Kriterien: Funktionseinschränkung/Behinderung durch ein Problem des Nervensystems
* #N28 ^property[0].code = #parent 
* #N28 ^property[0].valueCode = #N 
* #N28 ^property[1].code = #Relationships 
* #N28 ^property[1].valueString = "ICD-10 2017:Z73.6" 
* #N28 ^property[2].code = #hints 
* #N28 ^property[2].valueString = "Beachte:  Hinweise: COOP/WONCA Charts eignen sich zur Dokumentation des funktionellen Status des Patienten (siehe Kapitel 8)" 
* #N29 "Andere neurologische Symptome/Beschwerden"
* #N29 ^definition = Inklusive: Ataxie~ Gangunsicherheit~ Hinken~ Meningismus Exklusive:  Kriterien:
* #N29 ^property[0].code = #parent 
* #N29 ^property[0].valueCode = #N 
* #N29 ^property[1].code = #Relationships 
* #N29 ^property[1].valueString = "ICD-10 2017:M79.2~R26.0~R26.1~R26.8~R27.0~R27.8~R29.0~R29.1~R29.2~R29.8" 
* #N30 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? vollständig"
* #N30 ^property[0].code = #parent 
* #N30 ^property[0].valueCode = #N 
* #N31 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? teilweise"
* #N31 ^property[0].code = #parent 
* #N31 ^property[0].valueCode = #N 
* #N32 "Allergie-/Sensitivitätstestung"
* #N32 ^property[0].code = #parent 
* #N32 ^property[0].valueCode = #N 
* #N33 "Mikrobiologische/immunologische Untersuchung"
* #N33 ^property[0].code = #parent 
* #N33 ^property[0].valueCode = #N 
* #N34 "Blutuntersuchung"
* #N34 ^property[0].code = #parent 
* #N34 ^property[0].valueCode = #N 
* #N35 "Urinuntersuchung"
* #N35 ^property[0].code = #parent 
* #N35 ^property[0].valueCode = #N 
* #N36 "Stuhluntersuchung"
* #N36 ^property[0].code = #parent 
* #N36 ^property[0].valueCode = #N 
* #N37 "Histologische Untersuchung/zytologischer Abstrich"
* #N37 ^property[0].code = #parent 
* #N37 ^property[0].valueCode = #N 
* #N38 "Andere Laboruntersuchung NAK"
* #N38 ^property[0].code = #parent 
* #N38 ^property[0].valueCode = #N 
* #N39 "Körperliche Funktionsprüfung"
* #N39 ^property[0].code = #parent 
* #N39 ^property[0].valueCode = #N 
* #N40 "Diagnostische Endoskopie"
* #N40 ^property[0].code = #parent 
* #N40 ^property[0].valueCode = #N 
* #N41 "Diagnostisches Röntgen/Bildgebung"
* #N41 ^property[0].code = #parent 
* #N41 ^property[0].valueCode = #N 
* #N42 "Elektrische Aufzeichnungsverfahren"
* #N42 ^property[0].code = #parent 
* #N42 ^property[0].valueCode = #N 
* #N43 "Andere diagnostische Untersuchung"
* #N43 ^property[0].code = #parent 
* #N43 ^property[0].valueCode = #N 
* #N44 "Präventive Impfung/Medikation"
* #N44 ^property[0].code = #parent 
* #N44 ^property[0].valueCode = #N 
* #N45 "Beobachtung/Gesundheitsschulung/Beratung/Diät"
* #N45 ^property[0].code = #parent 
* #N45 ^property[0].valueCode = #N 
* #N46 "Konsultation eines anderen Primärversorgers"
* #N46 ^property[0].code = #parent 
* #N46 ^property[0].valueCode = #N 
* #N47 "Konsultation eines Facharztes"
* #N47 ^property[0].code = #parent 
* #N47 ^property[0].valueCode = #N 
* #N48 "Klärung/Besprechung des Beratungsanlasses mit dem Patienten"
* #N48 ^property[0].code = #parent 
* #N48 ^property[0].valueCode = #N 
* #N49 "Andere Vorsorgemaßnahme"
* #N49 ^property[0].code = #parent 
* #N49 ^property[0].valueCode = #N 
* #N50 "Medikation/Verschreibung/Erneuerung/Injektion"
* #N50 ^property[0].code = #parent 
* #N50 ^property[0].valueCode = #N 
* #N51 "Inzision/Drainage/Spülung/Absaugung/Entfernung von Körperflüssigkeit"
* #N51 ^property[0].code = #parent 
* #N51 ^property[0].valueCode = #N 
* #N52 "Exzision/Entfernung von Gewebe/Biopsie/Destruktion/Debridement/Verkauterung"
* #N52 ^property[0].code = #parent 
* #N52 ^property[0].valueCode = #N 
* #N53 "Instrumentelle Manipulation/Katheterisierung/Intubation/Dilatation"
* #N53 ^property[0].code = #parent 
* #N53 ^property[0].valueCode = #N 
* #N54 "Reposition/Fixierung/Naht/Gips/prothetische Versorgung"
* #N54 ^property[0].code = #parent 
* #N54 ^property[0].valueCode = #N 
* #N55 "Lokale Injektion/Infiltration"
* #N55 ^property[0].code = #parent 
* #N55 ^property[0].valueCode = #N 
* #N56 "Verband/Druck/Kompression/Tamponade"
* #N56 ^property[0].code = #parent 
* #N56 ^property[0].valueCode = #N 
* #N57 "Physikalische Therapie/Rehabilitation"
* #N57 ^property[0].code = #parent 
* #N57 ^property[0].valueCode = #N 
* #N58 "Therapeutische Beratung/Zuhören"
* #N58 ^property[0].code = #parent 
* #N58 ^property[0].valueCode = #N 
* #N59 "Andere therapeutische Maßnahme/kleine Chirurgie NAK"
* #N59 ^property[0].code = #parent 
* #N59 ^property[0].valueCode = #N 
* #N60 "Testresultat/Ergebnis eigene Maßnahme"
* #N60 ^property[0].code = #parent 
* #N60 ^property[0].valueCode = #N 
* #N61 "Ergebnis Untersuchung/Test/Brief von anderem Leistungserbringer"
* #N61 ^property[0].code = #parent 
* #N61 ^property[0].valueCode = #N 
* #N62 "Adminstrative Maßnahme"
* #N62 ^property[0].code = #parent 
* #N62 ^property[0].valueCode = #N 
* #N63 "Folgekonsultation unspezifiziert"
* #N63 ^property[0].code = #parent 
* #N63 ^property[0].valueCode = #N 
* #N64 "Konsultation/Problembehandlung auf Initiative des Leistungserbringers"
* #N64 ^property[0].code = #parent 
* #N64 ^property[0].valueCode = #N 
* #N65 "Konsultation/Problembehandlung auf Initiative Dritter"
* #N65 ^property[0].code = #parent 
* #N65 ^property[0].valueCode = #N 
* #N66 "Überweisung an andere/n Leistungserbringer (nicht ärztl.)/Pflegekraft/Therapeuten/Sozialarbeiter"
* #N66 ^property[0].code = #parent 
* #N66 ^property[0].valueCode = #N 
* #N67 "Überweisung an einen Arzt/Spezialisten/eine Klinik/ein Krankenhaus"
* #N67 ^property[0].code = #parent 
* #N67 ^property[0].valueCode = #N 
* #N68 "Andere Überweisung NAK"
* #N68 ^property[0].code = #parent 
* #N68 ^property[0].valueCode = #N 
* #N69 "Anderer Beratungsanlass NAK"
* #N69 ^property[0].code = #parent 
* #N69 ^property[0].valueCode = #N 
* #N70 "Poliomyelitis"
* #N70 ^definition = Inklusive: Spätfolgen einer Kinderlähmung~ Post-Polio Syndrom~ andere Infektion mit Enteroviren Exklusive:  Kriterien:
* #N70 ^property[0].code = #parent 
* #N70 ^property[0].valueCode = #N 
* #N70 ^property[1].code = #Relationships 
* #N70 ^property[1].valueString = "ICD-10 2017:A80.0~A80.1~A80.2~A80.3~A80.4~A80.9~A85.0~B91~G14" 
* #N71 "Meningitis/Enzephalitis"
* #N71 ^definition = Inklusive:  Exklusive:  Kriterien: eine akute fieberhafte Erkrankung mit auffälligem Liquorbefund
* #N71 ^property[0].code = #parent 
* #N71 ^property[0].valueCode = #N 
* #N71 ^property[1].code = #Relationships 
* #N71 ^property[1].valueString = "ICD-10 2017:A32.1~A39.0~A83.0~A83.1~A83.2~A83.3~A83.4~A83.5~A83.6~A83.8~A83.9~A84.0~A84.1~A84.8~A84.9~A85.1~A85.2~A85.8~A86~A87.0~A87.1~A87.2~A87.8~A87.9~B00.3~B00.4~B37.5~B58.2~B94.1~G00.0~G00.1~G00.2~G00.3~G00.8~G00.9~G01~G02.0~G02.1~G02.8~G03.0~G03.1~G03.2~G03.8~G03.9~G04.0~G04.1~G04.2~G04.8~G04.9~G05.0~G05.1~G05.2~G05.8" 
* #N71 ^property[2].code = #hints 
* #N71 ^property[2].valueString = "Beachte: Fieber A03~ Meningismus N29 Hinweise: " 
* #N72 "Tetanus"
* #N72 ^definition = Inklusive:  Exklusive: Tetanie N08 Kriterien: Körperstarre mit hypertonen Kontraktionen oder tetanischen Spasmen und einer vorhergegangenen Verletzung in der Anamnese
* #N72 ^property[0].code = #parent 
* #N72 ^property[0].valueCode = #N 
* #N72 ^property[1].code = #Relationships 
* #N72 ^property[1].valueString = "ICD-10 2017:A33~A34~A35" 
* #N73 "Neurologische Infektion, andere"
* #N73 ^definition = Inklusive: zerebraler Abszess~ Slow Virus Infection Exklusive: Poliomyelitis N70~ Meningitis/Enzephalitis N71~ akute Polyneuritis N94 Kriterien:
* #N73 ^property[0].code = #parent 
* #N73 ^property[0].valueCode = #N 
* #N73 ^property[1].code = #Relationships 
* #N73 ^property[1].valueString = "ICD-10 2017:A81.0~A81.1~A81.2~A81.8~A81.9~A88.8~A89~G06.0~G06.1~G06.2~G07~G08~G09" 
* #N74 "Bösartige Neubildung Nervensystem"
* #N74 ^definition = Inklusive:  Exklusive:  Kriterien: charakteristisches histologisches Erscheinungsbild
* #N74 ^property[0].code = #parent 
* #N74 ^property[0].valueCode = #N 
* #N74 ^property[1].code = #Relationships 
* #N74 ^property[1].valueString = "ICD-10 2017:C47.0~C47.1~C47.2~C47.3~C47.4~C47.5~C47.6~C47.8~C47.9~C70.0~C70.1~C70.9~C71.0~C71.1~C71.2~C71.3~C71.4~C71.5~C71.6~C71.7~C71.8~C71.9~C72.0~C72.1~C72.2~C72.3~C72.4~C72.5~C72.8~C72.9" 
* #N74 ^property[2].code = #hints 
* #N74 ^property[2].valueString = "Beachte: nicht spezifizierte Neubildung des Nervensystems N76 Hinweise: " 
* #N75 "Gutartige Neubildung Nervensystem"
* #N75 ^definition = Inklusive: Akustikusneurinom~ Meningeom Exklusive:  Kriterien:
* #N75 ^property[0].code = #parent 
* #N75 ^property[0].valueCode = #N 
* #N75 ^property[1].code = #Relationships 
* #N75 ^property[1].valueString = "ICD-10 2017:D32.0~D32.1~D32.9~D33.0~D33.1~D33.2~D33.3~D33.4~D33.7~D33.9~D36.1" 
* #N76 "Neubildung Nervensystem nicht spezifiziert"
* #N76 ^definition = Inklusive: nicht als gutartige oder bösartig klassifizierte neurologische Neubildung wenn Histologie nicht verfügbar Exklusive: Neurofibromatose A90 Kriterien:
* #N76 ^property[0].code = #parent 
* #N76 ^property[0].valueCode = #N 
* #N76 ^property[1].code = #Relationships 
* #N76 ^property[1].valueString = "ICD-10 2017:D42.0~D42.1~D42.9~D43.0~D43.1~D43.2~D43.3~D43.4~D43.7~D43.9~D48.2" 
* #N79 "Gehirnerschütterung"
* #N79 ^definition = Inklusive: Spätfolge einer Gehirnerschütterung Exklusive: psychische Auswirkungen einer Gehirnerschütterung P02 Kriterien: Schädeltrauma mit Verlust des Bewußtseins und/oder neurologischen Folgeerscheinungen
* #N79 ^property[0].code = #parent 
* #N79 ^property[0].valueCode = #N 
* #N79 ^property[1].code = #Relationships 
* #N79 ^property[1].valueString = "ICD-10 2017:S06.0" 
* #N79 ^property[2].code = #hints 
* #N79 ^property[2].valueString = "Beachte: andere Kopfverletzung N80 Hinweise: " 
* #N80 "Kopfverletzung, andere"
* #N80 ^definition = Inklusive: zerebrale Kontusion~ zerebrale Verletzung mit/ohne Schädelfraktur~ epidurales Hämatom~ subdurales Hämatom~ traumatische intrakranielle Blutung Exklusive: Gehirnerschütterung N79 Kriterien: Schädeltrauma mit Komplikation durch Gehirnschaden
* #N80 ^property[0].code = #parent 
* #N80 ^property[0].valueCode = #N 
* #N80 ^property[1].code = #Relationships 
* #N80 ^property[1].valueString = "ICD-10 2017:S02.0~S02.1~S02.8~S02.9~S06.1~S06.2~S06.3~S06.4~S06.5~S06.6~S06.7~S06.8~S06.9~S07.0~S07.1~S07.8~S07.9~S08.0~S08.8~S08.9~S09.0~S09.7~S09.8~S09.9" 
* #N81 "Verletzung Nervensystem, andere"
* #N81 ^definition = Inklusive: Nervenverletzung~ Rückenmarksverletzung Exklusive:  Kriterien:
* #N81 ^property[0].code = #parent 
* #N81 ^property[0].valueCode = #N 
* #N81 ^property[1].code = #Relationships 
* #N81 ^property[1].valueString = "ICD-10 2017:S04.0~S04.1~S04.2~S04.3~S04.4~S04.5~S04.6~S04.7~S04.8~S04.9~S09.9~S14.0~S14.1~S14.2~S14.3~S14.4~S14.5~S14.6~S24.0~S24.1~S24.2~S24.3~S24.4~S24.5~S24.6~S34.0~S34.1~S34.2~S34.3~S34.4~S34.5~S34.6~S34.8~S44.0~S44.1~S44.2~S44.3~S44.4~S44.5~S44.7~S44.8~S44.9~S54.0~S54.1~S54.2~S54.3~S54.7~S54.8~S54.9~S64.0~S64.1~S64.2~S64.3~S64.4~S64.7~S64.8~S64.9~S74.0~S74.1~S74.2~S74.7~S74.8~S74.9~S84.0~S84.1~S84.2~S84.7~S84.8~S84.9~S94.0~S94.1~S94.2~S94.3~S94.7~S94.8~S94.9~T06.0~T06.1~T06.2~T09.3~T09.4~T11.3~T11.4~T13.3~T14.4" 
* #N85 "Angeborene Anomalie Nervensystem"
* #N85 ^definition = Inklusive: Hydrocephalus~ Spina bifida Exklusive:  Kriterien:
* #N85 ^property[0].code = #parent 
* #N85 ^property[0].valueCode = #N 
* #N85 ^property[1].code = #Relationships 
* #N85 ^property[1].valueString = "ICD-10 2017:Q00.0~Q00.1~Q00.2~Q01.0~Q01.1~Q01.2~Q01.8~Q01.9~Q02~Q03.0~Q03.1~Q03.8~Q03.9~Q04.0~Q04.1~Q04.2~Q04.3~Q04.4~Q04.5~Q04.6~Q04.8~Q04.9~Q05.0~Q05.1~Q05.2~Q05.3~Q05.4~Q05.5~Q05.6~Q05.7~Q05.8~Q05.9~Q06.0~Q06.1~Q06.2~Q06.3~Q06.4~Q06.8~Q06.9~Q07.0~Q07.8~Q07.9" 
* #N86 "Multiple Sklerose"
* #N86 ^definition = Inklusive: disseminierte Sklerose Exklusive:  Kriterien: Verschlimmerung/Remission mannigfaltiger neurologischer Manifestationen mit sowohl zeitlich als auch lokal disseminierten Ausfallserscheinungen (jede Kombination neurologischer Anzeichen und Symptome ist möglich)
* #N86 ^property[0].code = #parent 
* #N86 ^property[0].valueCode = #N 
* #N86 ^property[1].code = #Relationships 
* #N86 ^property[1].valueString = "ICD-10 2017:G35" 
* #N86 ^property[2].code = #hints 
* #N86 ^property[2].valueString = "Beachte: andere neurologische Symptome N29 Hinweise: " 
* #N87 "Morbus Parkinson"
* #N87 ^definition = Inklusive: Medikamenteninduzierter Parkisonismus~ Schüttellähmung~ Morbus Parkinson Exklusive:  Kriterien: Verarmung und Verlangsamung der Willkürbewegung, Ruhetremor, der sich bei zielgerichteter, aktiver Bewegung verbessert, sowie Muskelstarre
* #N87 ^property[0].code = #parent 
* #N87 ^property[0].valueCode = #N 
* #N87 ^property[1].code = #Relationships 
* #N87 ^property[1].valueString = "ICD-10 2017:G20~G21.0~G21.1~G21.2~G21.3~G21.4~G21.8~G21.9~G22" 
* #N87 ^property[2].code = #hints 
* #N87 ^property[2].valueString = "Beachte: abnormale unwillkürliche Bewegungen N08~ Sprachstörung N19 Hinweise: " 
* #N88 "Epilepsie"
* #N88 ^definition = Inklusive: alle Formen der Epilepsie: fokale Anfälle~ generalisierte Anfälle~ Grand mal Anfall~ Petit mal Anfall~ Status epilepticus Exklusive:  Kriterien: Wiederkehrende Episoden plötzlicher Bewußtseinsänderung mit oder ohne tonische und klonische Bewegungen/Krämpfe, plus entweder ein Augenzeugenbericht von einem Anfall oder charakteristische EEG-Zeichen
* #N88 ^property[0].code = #parent 
* #N88 ^property[0].valueCode = #N 
* #N88 ^property[1].code = #Relationships 
* #N88 ^property[1].valueString = "ICD-10 2017:G40.0~G40.1~G40.2~G40.3~G40.4~G40.5~G40.6~G40.7~G40.8~G40.9~G41.0~G41.1~G41.2~G41.8~G41.9" 
* #N88 ^property[2].code = #hints 
* #N88 ^property[2].valueString = "Beachte: Konvulsionen N07~ andere neurologische Symptome N29 Hinweise: " 
* #N89 "Migräne"
* #N89 ^definition = Inklusive: vaskulärer Kopfschmerz mit/ohne Aura Exklusive: zervikaler Kopfschmerz L83~ Clusterkopfschmerz N90~ Spannungskopfschmerz N95 Kriterien: Wiederkehrender Kopfschmerz mit mindestens drei der folgenden Symptome: einseitige Kopfschmerzen, Übelkeit oder Erbrechen, Aura, sonstige neurologischen Symptome, Migräne in der Familienanamnese
* #N89 ^property[0].code = #parent 
* #N89 ^property[0].valueCode = #N 
* #N89 ^property[1].code = #Relationships 
* #N89 ^property[1].valueString = "ICD-10 2017:G43.0~G43.1~G43.2~G43.3~G43.8~G43.9~G44.1" 
* #N89 ^property[2].code = #hints 
* #N89 ^property[2].valueString = "Beachte: Kopfschmerz N01 Hinweise: " 
* #N90 "Cluster-Kopfschmerz"
* #N90 ^definition = Inklusive:  Exklusive: Migräne N89 Kriterien: Anfälle von heftigen, oft quälenden einseitigen Schmerzen im periorbitalen und temporalen Bereich, die bis zu achtmal täglich auftreten, begleitet von konjunktivaler Injektion, Tränenbildung, Nasenverstopfung, Nasenrinnen, Schwitzen, Miose, Ptose oder periorbitalem Ödem. Die Anfälle treten gehäuft in Wochen oder Monate dauernden Perioden auf, zwischen denen Monate oder Jahre der Remission liegen
* #N90 ^property[0].code = #parent 
* #N90 ^property[0].valueCode = #N 
* #N90 ^property[1].code = #Relationships 
* #N90 ^property[1].valueString = "ICD-10 2017:G44.0" 
* #N91 "Fazialisparese"
* #N91 ^definition = Inklusive:  Exklusive:  Kriterien: Akut auftretende einseitige Lähmung der Gesichtsmuskeln ohne Verlust der Sensibilität
* #N91 ^property[0].code = #parent 
* #N91 ^property[0].valueCode = #N 
* #N91 ^property[1].code = #Relationships 
* #N91 ^property[1].valueString = "ICD-10 2017:G51.0~G51.1~G51.2~G51.3~G51.4~G51.8~G51.9~G53.0~G53.1~G53.2~G53.3~G53.8" 
* #N91 ^property[2].code = #hints 
* #N91 ^property[2].valueString = "Beachte: Lähmung/Schwäche N18 Hinweise: " 
* #N92 "Trigeminus-Neuralgie"
* #N92 ^definition = Inklusive: Tic douloureux Exklusive: Post-Zoster-Neuralgie S70 Kriterien: Einseitige Anfälle brennenden Gesichtsschmerzes, der sich beim Berühren von Triggerpunkten, beim Schneuzen oder Gähnen noch verschlimmet, oder sensorische oder motorische Lähmung
* #N92 ^property[0].code = #parent 
* #N92 ^property[0].valueCode = #N 
* #N92 ^property[1].code = #Relationships 
* #N92 ^property[1].valueString = "ICD-10 2017:G50.0~G50.8~G50.9" 
* #N92 ^property[2].code = #hints 
* #N92 ^property[2].valueString = "Beachte: Neuralgie NNB N99 Hinweise: " 
* #N93 "Karpaltunnel-Syndrom"
* #N93 ^definition = Inklusive:  Exklusive:  Kriterien: Verlust oder Verminderung der Sensibilität von Daumen, Zeigefinger und Mittelfinger, die sich auf den halben Ringfinger erstrecken können. Dysästhesie und Schmerz werden gewöhnlich nachts schlimmer und können in den Unterarm ausstrahlen
* #N93 ^property[0].code = #parent 
* #N93 ^property[0].valueCode = #N 
* #N93 ^property[1].code = #Relationships 
* #N93 ^property[1].valueString = "ICD-10 2017:G56.0" 
* #N93 ^property[2].code = #hints 
* #N93 ^property[2].valueString = "Beachte: Empfindungsstörung N06 Hinweise: " 
* #N94 "Periphere Neuritis/Neuropathie"
* #N94 ^definition = Inklusive: akute infektiöse Polyneuropathie~ diabetische Neuropathie (T89 und T90 doppelt kodieren)~ Guillain-Barré Syndrom~ Nervenläsion~ Neuropathie~ Phantomschmerz Exklusive: Post-Zoster-Neuralgie S70 Kriterien: Sensorische, reflektorische und motorische Veränderungen, die auf das Gebiet einzelner Nerven beschränkt sind, oft ohne ersichtlichen Grund, manchmal sekundär aus einer bestimmten Krankheit (z.B. Diabetes) entstanden
* #N94 ^property[0].code = #parent 
* #N94 ^property[0].valueCode = #N 
* #N94 ^property[1].code = #Relationships 
* #N94 ^property[1].valueString = "ICD-10 2017:G54.0~G54.1~G54.2~G54.3~G54.4~G54.5~G54.6~G54.7~G54.8~G54.9~G55.0~G55.1~G55.2~G55.3~G55.8~G56.1~G56.2~G56.3~G56.4~G56.8~G56.9~G57.0~G57.1~G57.2~G57.3~G57.4~G57.5~G57.6~G57.8~G57.9~G58.0~G58.7~G58.8~G58.9~G59.0~G59.8~G60.0~G60.1~G60.2~G60.3~G60.8~G60.9~G61.0~G61.1~G61.8~G61.9~G62.0~G62.1~G62.2~G62.8~G62.9~G63.0~G63.1~G63.2~G63.3~G63.4~G63.5~G63.6~G63.8~G64~M79.2" 
* #N95 "Spannungskopfschmerz"
* #N95 ^definition = Inklusive:  Exklusive: Migräne N89~ Clusterkopfschmerz N90 Kriterien: Drückende, generalisierte Kopfschmerzen, die mit Streß und Muskelverspannung einhergehen, mit oder ohne Schmerzhaftigkeit der Schädelmuskeln
* #N95 ^property[0].code = #parent 
* #N95 ^property[0].valueCode = #N 
* #N95 ^property[1].code = #Relationships 
* #N95 ^property[1].valueString = "ICD-10 2017:G44.2" 
* #N95 ^property[2].code = #hints 
* #N95 ^property[2].valueString = "Beachte: Kopfschmerz N01 Hinweise: " 
* #N99 "Andere neurologische Erkrankung"
* #N99 ^definition = Inklusive: Zerebrale Lähmung~ Erkrankung motorischer Neuronen~ Myasthenia gravis~ Neuralgie NNB Exklusive: Schlafapnoe P06 Kriterien:
* #N99 ^property[0].code = #parent 
* #N99 ^property[0].valueCode = #N 
* #N99 ^property[1].code = #Relationships 
* #N99 ^property[1].valueString = "ICD-10 2017:E51.2~G10~G11.0~G11.1~G11.2~G11.3~G11.4~G11.8~G11.9~G12.0~G12.1~G12.2~G12.8~G12.9~G13.0~G13.1~G13.2~G13.8~G23.0~G23.1~G23.2~G23.3~G23.8~G23.9~G24.0~G24.1~G24.2~G24.3~G24.4~G24.5~G24.8~G24.9~G26~G31.0~G31.1~G31.8~G31.9~G32.0~G32.8~G36.0~G36.1~G36.8~G36.9~G37.0~G37.1~G37.2~G37.3~G37.4~G37.5~G37.8~G37.9~G52.0~G52.1~G52.2~G52.3~G52.7~G52.8~G52.9~G70.0~G70.1~G70.2~G70.8~G70.9~G71.0~G71.1~G71.2~G71.3~G71.8~G71.9~G72.0~G72.1~G72.2~G72.3~G72.4~G72.8~G72.9~G73.0~G73.1~G73.2~G73.3~G73.4~G73.5~G73.6~G73.7~G80.0~G80.1~G80.2~G80.3~G80.4~G80.8~G80.9~G83.5~G90.0~G90.1~G90.2~G90.4~G90.8~G90.9~G91.0~G91.1~G91.2~G91.3~G91.8~G91.9~G92~G93.0~G93.1~G93.2~G93.4~G93.5~G93.6~G93.7~G93.8~G93.9~G94.0~G94.1~G94.2~G94.8~G95.0~G95.1~G95.2~G95.8~G95.9~G96.0~G96.1~G96.8~G96.9~G98~G99.0~G99.1~G99.2~G99.8~M79.2~Z98.2" 
* #P "Psychologisch"
* #P ^property[0].code = #child 
* #P ^property[0].valueCode = #P01 
* #P ^property[1].code = #child 
* #P ^property[1].valueCode = #P02 
* #P ^property[2].code = #child 
* #P ^property[2].valueCode = #P03 
* #P ^property[3].code = #child 
* #P ^property[3].valueCode = #P04 
* #P ^property[4].code = #child 
* #P ^property[4].valueCode = #P05 
* #P ^property[5].code = #child 
* #P ^property[5].valueCode = #P06 
* #P ^property[6].code = #child 
* #P ^property[6].valueCode = #P07 
* #P ^property[7].code = #child 
* #P ^property[7].valueCode = #P08 
* #P ^property[8].code = #child 
* #P ^property[8].valueCode = #P09 
* #P ^property[9].code = #child 
* #P ^property[9].valueCode = #P10 
* #P ^property[10].code = #child 
* #P ^property[10].valueCode = #P11 
* #P ^property[11].code = #child 
* #P ^property[11].valueCode = #P12 
* #P ^property[12].code = #child 
* #P ^property[12].valueCode = #P13 
* #P ^property[13].code = #child 
* #P ^property[13].valueCode = #P15 
* #P ^property[14].code = #child 
* #P ^property[14].valueCode = #P16 
* #P ^property[15].code = #child 
* #P ^property[15].valueCode = #P17 
* #P ^property[16].code = #child 
* #P ^property[16].valueCode = #P18 
* #P ^property[17].code = #child 
* #P ^property[17].valueCode = #P19 
* #P ^property[18].code = #child 
* #P ^property[18].valueCode = #P20 
* #P ^property[19].code = #child 
* #P ^property[19].valueCode = #P22 
* #P ^property[20].code = #child 
* #P ^property[20].valueCode = #P23 
* #P ^property[21].code = #child 
* #P ^property[21].valueCode = #P24 
* #P ^property[22].code = #child 
* #P ^property[22].valueCode = #P25 
* #P ^property[23].code = #child 
* #P ^property[23].valueCode = #P27 
* #P ^property[24].code = #child 
* #P ^property[24].valueCode = #P28 
* #P ^property[25].code = #child 
* #P ^property[25].valueCode = #P29 
* #P ^property[26].code = #child 
* #P ^property[26].valueCode = #P30 
* #P ^property[27].code = #child 
* #P ^property[27].valueCode = #P31 
* #P ^property[28].code = #child 
* #P ^property[28].valueCode = #P32 
* #P ^property[29].code = #child 
* #P ^property[29].valueCode = #P34 
* #P ^property[30].code = #child 
* #P ^property[30].valueCode = #P35 
* #P ^property[31].code = #child 
* #P ^property[31].valueCode = #P36 
* #P ^property[32].code = #child 
* #P ^property[32].valueCode = #P38 
* #P ^property[33].code = #child 
* #P ^property[33].valueCode = #P39 
* #P ^property[34].code = #child 
* #P ^property[34].valueCode = #P40 
* #P ^property[35].code = #child 
* #P ^property[35].valueCode = #P41 
* #P ^property[36].code = #child 
* #P ^property[36].valueCode = #P42 
* #P ^property[37].code = #child 
* #P ^property[37].valueCode = #P43 
* #P ^property[38].code = #child 
* #P ^property[38].valueCode = #P44 
* #P ^property[39].code = #child 
* #P ^property[39].valueCode = #P45 
* #P ^property[40].code = #child 
* #P ^property[40].valueCode = #P46 
* #P ^property[41].code = #child 
* #P ^property[41].valueCode = #P47 
* #P ^property[42].code = #child 
* #P ^property[42].valueCode = #P48 
* #P ^property[43].code = #child 
* #P ^property[43].valueCode = #P49 
* #P ^property[44].code = #child 
* #P ^property[44].valueCode = #P50 
* #P ^property[45].code = #child 
* #P ^property[45].valueCode = #P51 
* #P ^property[46].code = #child 
* #P ^property[46].valueCode = #P52 
* #P ^property[47].code = #child 
* #P ^property[47].valueCode = #P53 
* #P ^property[48].code = #child 
* #P ^property[48].valueCode = #P54 
* #P ^property[49].code = #child 
* #P ^property[49].valueCode = #P55 
* #P ^property[50].code = #child 
* #P ^property[50].valueCode = #P56 
* #P ^property[51].code = #child 
* #P ^property[51].valueCode = #P57 
* #P ^property[52].code = #child 
* #P ^property[52].valueCode = #P58 
* #P ^property[53].code = #child 
* #P ^property[53].valueCode = #P59 
* #P ^property[54].code = #child 
* #P ^property[54].valueCode = #P60 
* #P ^property[55].code = #child 
* #P ^property[55].valueCode = #P61 
* #P ^property[56].code = #child 
* #P ^property[56].valueCode = #P62 
* #P ^property[57].code = #child 
* #P ^property[57].valueCode = #P63 
* #P ^property[58].code = #child 
* #P ^property[58].valueCode = #P64 
* #P ^property[59].code = #child 
* #P ^property[59].valueCode = #P65 
* #P ^property[60].code = #child 
* #P ^property[60].valueCode = #P66 
* #P ^property[61].code = #child 
* #P ^property[61].valueCode = #P67 
* #P ^property[62].code = #child 
* #P ^property[62].valueCode = #P68 
* #P ^property[63].code = #child 
* #P ^property[63].valueCode = #P69 
* #P ^property[64].code = #child 
* #P ^property[64].valueCode = #P70 
* #P ^property[65].code = #child 
* #P ^property[65].valueCode = #P71 
* #P ^property[66].code = #child 
* #P ^property[66].valueCode = #P72 
* #P ^property[67].code = #child 
* #P ^property[67].valueCode = #P73 
* #P ^property[68].code = #child 
* #P ^property[68].valueCode = #P74 
* #P ^property[69].code = #child 
* #P ^property[69].valueCode = #P75 
* #P ^property[70].code = #child 
* #P ^property[70].valueCode = #P76 
* #P ^property[71].code = #child 
* #P ^property[71].valueCode = #P77 
* #P ^property[72].code = #child 
* #P ^property[72].valueCode = #P78 
* #P ^property[73].code = #child 
* #P ^property[73].valueCode = #P79 
* #P ^property[74].code = #child 
* #P ^property[74].valueCode = #P80 
* #P ^property[75].code = #child 
* #P ^property[75].valueCode = #P81 
* #P ^property[76].code = #child 
* #P ^property[76].valueCode = #P82 
* #P ^property[77].code = #child 
* #P ^property[77].valueCode = #P85 
* #P ^property[78].code = #child 
* #P ^property[78].valueCode = #P86 
* #P ^property[79].code = #child 
* #P ^property[79].valueCode = #P98 
* #P ^property[80].code = #child 
* #P ^property[80].valueCode = #P99 
* #P01 "Gefühl der Angst/Unruhe/Spannung"
* #P01 ^definition = Inklusive: Angstzustand NB~ Angstgefühl Exklusive: Angststörung P74 Kriterien: Vom Patienten als Folge einer emotionalen oder psychosozialen Erfahrung berichtete Gefühle, wobei die Erfahrung nicht auf das Vorhandensein einer psychischen Störung zurückgeführt wird. Es gibt einen allmählichen Übergang von Gefühlen, die nicht willkommen, aber ganz normal sind, zu Gefühlen, die den Patienten so beunruhigen, dass er professionelle Hilfe sucht
* #P01 ^property[0].code = #parent 
* #P01 ^property[0].valueCode = #P 
* #P01 ^property[1].code = #Relationships 
* #P01 ^property[1].valueString = "ICD-10 2017:R45.0" 
* #P02 "Akute Stressreaktion"
* #P02 ^definition = Inklusive: Anpassungsstörung~ Kulturschock~ Stress/Trauer/Heimweh~ unmittelbares post-traumatische Stresstörung~ Schock (psychisch) Exklusive: depressive Verstimmung P03~ depressive Störung P76~ post-traumatische Stresstörung P82 Kriterien: Eine Reaktion auf ein mit Streß verbundenes Ereignis im Leben oder auf eine bedeutende Veränderung des Lebens, die eine nachhaltige Anpassung erfordert; entweder als erwartete Reaktion auf das Ereignis oder als Resultat mkangelhafter Anpassung, so dass die Bewährung im Alltag behindert und die soziale Funktionalität eingeschränkt wird, mit Wiedergewinnung des seelischen Gleichgewichts innerhalb einer begrenzten Zeitspanne
* #P02 ^property[0].code = #parent 
* #P02 ^property[0].valueCode = #P 
* #P02 ^property[1].code = #Relationships 
* #P02 ^property[1].valueString = "ICD-10 2017:F43.0~F43.2~F43.8~F43.9" 
* #P03 "Depressives Gefühl"
* #P03 ^definition = Inklusive: Gefühl der Unzulänglichkeit~ Unglücklichsein~ Besorgnis Exklusive: geringes Selbstvertrauen P28~ depressive Störung P76 Kriterien: Vom Patienten als Folge einer emotionalen oder psychischen Erfahrung berichtete Gefühle, wobei die Erfahrung nicht auf eine psychische Störung zurückgeführt wird. Es gibt einen allmählichen Übergang von Gefühlen, die nicht willkommen, aber ganz normal sind, zu Gefühlen, die den Patienten so beunruhigen, dass er professionelle Hilfe sucht
* #P03 ^property[0].code = #parent 
* #P03 ^property[0].valueCode = #P 
* #P03 ^property[1].code = #Relationships 
* #P03 ^property[1].valueString = "ICD-10 2017:R45.2~R45.3" 
* #P04 "Reizbares/ärgerliches Gefühl/Verhalten"
* #P04 ^definition = Inklusive: Agitiertheit NNB~ Rastlosigkeit NNB Exklusive: hyperaktives Kind P22~ Reizbarkeit des Partners Z13 Kriterien: Vom Patienten als Folge einer emotionalen oder psychischen Erfahrung berichtete Gefühle, wobei die Erfahrung nicht auf eine psychische Störung zurückgeführt wird, bzw. Verhalten, das Gereiztheit oder Ärger erkennen lässt. Es gibt einen allmählichen Übergang von Gefühlen, die nicht willkommen, aber ganz normal sind, zu Gefühlen, die den Patienten so beunruhigen, dass er professionelle Hilfe sucht
* #P04 ^property[0].code = #parent 
* #P04 ^property[0].valueCode = #P 
* #P04 ^property[1].code = #Relationships 
* #P04 ^property[1].valueString = "ICD-10 2017:R45.1~R45.4~R45.5~R45.6" 
* #P05 "Senilität, sich alt fühlen/benehmen"
* #P05 ^definition = Inklusive: Sorgen wegen des Alterns~ Seneszenz Exklusive: Demenz P70 Kriterien: Vom Patienten als Folge einer emotionalen oder psychischen Erfahrung berichtete Gefühle, wobei die Erfahrung nicht auf eine psychische Störung zurückgeführt wird. Es gibt einen allmählichen Übergang von Gefühlen, die nicht willkommen, aber ganz normal sind, zu Gefühlen, die den Patienten so beunruhigen, dass er professionelle Hilfe sucht
* #P05 ^property[0].code = #parent 
* #P05 ^property[0].valueCode = #P 
* #P05 ^property[1].code = #Relationships 
* #P05 ^property[1].valueString = "ICD-10 2017:R54" 
* #P06 "Schlafstörung"
* #P06 ^definition = Inklusive: Schlaflosigkeit~ Albträume~ Schlafapnoe~ Schlafwandeln~ Somnolenz Exklusive: Jet lag A88 Kriterien: Schlafstörung als Diagnose setzt voraus, dass das Schlafproblem eine ernsthafte Beschwerde darstellt, die nach übereinstimmender Meinung von Arzt und Patient nicht von einer anderen Störung verursacht wird, sondern eine eigenständige Beschwerde darstellt. Schlaflosigkeit setzt einen quantitativen oder qualitativen Mangel an Schlaf voraus, der nach Ansicht des Patienten unzufriedenstellend ist und sich über einen längeren Zeitraum erstreckt. Eine Hypersomnie liegt dann vor, wenn der Patient durch übertriebene Schläfrigkeit und Schlafanfälle während des Tages in der Ausübung seiner Funktion behindert wird
* #P06 ^property[0].code = #parent 
* #P06 ^property[0].valueCode = #P 
* #P06 ^property[1].code = #Relationships 
* #P06 ^property[1].valueString = "ICD-10 2017:F51.0~F51.1~F51.2~F51.3~F51.4~F51.5~F51.8~F51.9~G47.0~G47.1~G47.2~G47.3~G47.4~G47.8~G47.9" 
* #P07 "Vermindertes sexuelles Verlangen"
* #P07 ^definition = Inklusive: Frigidität~ Libidoverlust Exklusive: nichtorganische Impotenz/Verlust der sexuellen Erfüllung P08~ Sorge wegen sexueller Präferenz P09 Kriterien: Probleme hinsichtlich des sexuellen Verlangens, die nicht durch eine organische Störung oder eine Krankheit verursacht werden, sondern die Unfähigkeit des Patienten/der Patientin widerspiegeln, Partner in einer sexuellen Beziehung der von ihm/ihr gewünschten Art zu sein, weil das Verlangen nachgelassen hat oder die Genitalorgane nicht ausreichend reagieren bzw. funktionieren
* #P07 ^property[0].code = #parent 
* #P07 ^property[0].valueCode = #P 
* #P07 ^property[1].code = #Relationships 
* #P07 ^property[1].valueString = "ICD-10 2017:F52.0" 
* #P08 "Verminderte sexuelle Erfüllung"
* #P08 ^definition = Inklusive: nichtorganische Impotenz oder Dyspareunie~ Ejaculatio praecox~ Vaginismus psychogenen Ursprungs Exklusive: Pobleme mit dem sexuellen Verlangen P07~ Sorge wegen sexueller Präferenz P09~ Vaginismus NNB X04~ organische Impotenz/sexuelle Pobleme Y07 Kriterien: Probleme hinsichtlich des sexuellen Verlangens, die nicht durch eine organische Störung oder eine Krankheit verursacht werden, sondern die Unfähigkeit des Patienten/der Patientin widerspiegeln, Partner in einer sexuellen Beziehung der von ihm/ihr gewünschten Art zu sein, weil das Verlangen nachgelassen hat oder die Genitalorgane nicht ausreichend reagieren bzw. funktionieren, oder als Folge von Problemen mit der sexuellen Entwicklung
* #P08 ^property[0].code = #parent 
* #P08 ^property[0].valueCode = #P 
* #P08 ^property[1].code = #Relationships 
* #P08 ^property[1].valueString = "ICD-10 2017:F52.1~F52.2~F52.3~F52.4~F52.5~F52.6~F52.7~F52.8~F52.9" 
* #P09 "Besorgnis wegen sexueller Neigungen"
* #P09 ^definition = Inklusive:  Exklusive: vermindertes sexuelles Verlangen P07~ verminderte sexuelle Erfüllung P08 Kriterien: Probleme hinsichtlich des sexuellen Verlangens, die nicht durch eine organische Störung oder eine Krankheit verursacht werden, sondern die Unfähigkeit des Patienten/der Patientin widerspiegeln, Partner in einer sexuellen Beziehung der von ihm/ihr gewünschten Art zu sein, weil er/sie Probleme hinsichtlich seiner/ihrer sexuellen Identität/Neigung/Orientierung hat
* #P09 ^property[0].code = #parent 
* #P09 ^property[0].valueCode = #P 
* #P09 ^property[1].code = #Relationships 
* #P09 ^property[1].valueString = "ICD-10 2017:F64.0~F64.1~F64.2~F64.8~F64.9~F65.0~F65.1~F65.2~F65.3~F65.4~F65.5~F65.6~F65.8~F65.9~F66.0~F66.1~F66.2~F66.8~F66.9" 
* #P10 "Stammeln/Stottern/Tic"
* #P10 ^definition = Inklusive:  Exklusive: tic douloureux N92 Kriterien: Stammeln/Stottern: eine Sprachstörung, die durch häufige Wiederholung und Verlängerung von Lauten gekennzeichnet ist, oder durch häufiges Ausdehnen von Pausen, die den Redefluß unterbrechen
* #P10 ^property[0].code = #parent 
* #P10 ^property[0].valueCode = #P 
* #P10 ^property[1].code = #Relationships 
* #P10 ^property[1].valueString = "ICD-10 2017:F95.0~F95.1~F95.2~F95.8~F95.9~F98.4~F98.5~F98.6" 
* #P11 "Essstörung beim Kind"
* #P11 ^definition = Inklusive: Probleme beim Füttern~ Probleme mit dem Eßverhalten von Kindern Exklusive: Anorexia nervosa P86~ Eßstörung beim Erwachsenen T05 Kriterien:
* #P11 ^property[0].code = #parent 
* #P11 ^property[0].valueCode = #P 
* #P11 ^property[1].code = #Relationships 
* #P11 ^property[1].valueString = "ICD-10 2017:F98.2~F98.3" 
* #P11 ^property[2].code = #hints 
* #P11 ^property[2].valueString = "Beachte:  Hinweise: Verhaltensprobleme bei Kindern sind besonders schwer zu klassifizieren, was dadurch illustriert wird, dass sie auf vier Kapitel des ICPC verteilt sind. Ob Eltern diese Probleme ihrem Hausarzt berichten oder nicht, wird ihren Eindruck wiederspiegeln, ob es sich um 'normales', wenn auch eventuell störendes Verhalten, oder um besorgniserregendes oder 'krankhaftes' Verhalten handelt" 
* #P12 "Bettnässen/Enuresis"
* #P12 ^definition = Inklusive:  Exklusive: durch organische Störung verursachtes Bettnässen U04 Kriterien: Unwillkürliches Urinieren bei Tag oder Nacht, das sich nicht auf eine organische Störung zurückführen lässt
* #P12 ^property[0].code = #parent 
* #P12 ^property[0].valueCode = #P 
* #P12 ^property[1].code = #Relationships 
* #P12 ^property[1].valueString = "ICD-10 2017:F98.0" 
* #P12 ^property[2].code = #hints 
* #P12 ^property[2].valueString = "Beachte:  Hinweise: Verhaltensprobleme bei Kindern sind besonders schwer zu klassifizieren, was dadurch illustriert wird, dass sie auf vier Kapitel des ICPC verteilt sind. Ob Eltern diese Probleme ihrem Hausarzt berichten oder nicht, wird ihren Eindruck wiederspiegeln, ob es sich um 'normales', wenn auch eventuell störendes Verhalten, oder um besorgniserregendes oder 'krankhaftes' Verhalten handelt" 
* #P13 "Enkopresis/Stuhlkontrollproblem"
* #P13 ^definition = Inklusive:  Exklusive:  Kriterien: Die Diagnose Enkompresis setzt voraus, dass wiederholt gewöhnlich wohlgeformte Fäkalien an unangebrachten Orten abgesetzt werden, was als abnormes Verhalten bezogen auf das Alter des Kindes angesehen wird und nicht auf Verstopfung, eine Störung des Schließmuskels oder eine andere Krankheit verursacht wird
* #P13 ^property[0].code = #parent 
* #P13 ^property[0].valueCode = #P 
* #P13 ^property[1].code = #Relationships 
* #P13 ^property[1].valueString = "ICD-10 2017:F98.1" 
* #P15 "Chronischer Alkoholmissbrauch"
* #P15 ^definition = Inklusive: alkoholisches Hirnsyndrom~ Alkoholpsychose~ Alkoholismus~ Delirium tremens Exklusive:  Kriterien: Eine durch Alkoholgenuss verursachte Störung, die zu mindestens einem der folgenden Symptome führt: übermäßiger Gebrauch mit klinisch signifikantem Schaden an der Gesundheit, ein Abhängigkeitssyndrom, Entzugserscheinungen, ein psychotisches Erscheinungsbild
* #P15 ^property[0].code = #parent 
* #P15 ^property[0].valueCode = #P 
* #P15 ^property[1].code = #Relationships 
* #P15 ^property[1].valueString = "ICD-10 2017:F10.1~F10.2~F10.3~F10.4~F10.5~F10.6~F10.7~F10.8~F10.9~G31.2" 
* #P15 ^property[2].code = #hints 
* #P15 ^property[2].valueString = "Beachte:  Hinweise: Bei Problemen durch Substanzmißbrauch sollten die erheblichen Unterschiede zwischen Ländern und Kulturen berücksichtigt werden. Der Arzt kann auch ohne Zustimmung des Patienten einen Fall als 'chronischen Alkoholmißbrauch' klassifizieren, also auch ohne die Bereitschaft des Patienten, therapeutischen Maßnahmen zuzustimmen" 
* #P16 "Akuter Alkoholmissbrauch"
* #P16 ^definition = Inklusive: Trunkenheit Exklusive:  Kriterien: Eine Störung, die auf Alkoholgenuß mit akuter Intoxikation zurückzuführen ist, mit oder ohne Vorgeschichte von chronischem Alkohomißbrauch
* #P16 ^property[0].code = #parent 
* #P16 ^property[0].valueCode = #P 
* #P16 ^property[1].code = #Relationships 
* #P16 ^property[1].valueString = "ICD-10 2017:F10.0" 
* #P16 ^property[2].code = #hints 
* #P16 ^property[2].valueString = "Beachte:  Hinweise: Ein Arzt kann entscheiden, eine Episode als 'akuten Alkoholmißbrauch' zu bezeichnen, ohne Zustimmung des Patienten und folglich ohne die Bereitschaft des Patienten, einer medizinischen Behandlung zuzustimmen." 
* #P17 "Tabakmissbrauch"
* #P17 ^definition = Inklusive: Übermäßiges Rauchen Exklusive:  Kriterien: Eine Störung, die auf Tabakgenuß zurückzuführen ist und zu mindestens einer der folgenden Erscheinungen führt: akute Intoxikation, übermäßiger Gebrauch mit klinisch signifikanter Schädigung der Gesundheit, Abhängigkeitssyndrom oder Entzugserscheinungen
* #P17 ^property[0].code = #parent 
* #P17 ^property[0].valueCode = #P 
* #P17 ^property[1].code = #Relationships 
* #P17 ^property[1].valueString = "ICD-10 2017:F17.0~F17.1~F17.2~F17.3~F17.4~F17.5~F17.6~F17.7~F17.8~F17.9" 
* #P17 ^property[2].code = #hints 
* #P17 ^property[2].valueString = "Beachte: Risikofaktor NNB A23 Hinweise: Bei Problemen durch Substanzmißbrauch sollten die erheblichen Unterschiede zwischen Ländern und Kulturen berücksichtigt werden. Der Arzt kann auch ohne Zustimmung des Patienten einen Fall als 'chronischen Alkoholmißbrauch' klassifizieren, also auch ohne die Bereitschaft des Patienten, therapeutischen Maßnahmen zuzustimmen" 
* #P18 "Medikamentenmissbrauch"
* #P18 ^definition = Inklusive: Missbrauch einer verschreibungspflichtigen Substanz Exklusive:  Kriterien:
* #P18 ^property[0].code = #parent 
* #P18 ^property[0].valueCode = #P 
* #P18 ^property[1].code = #Relationships 
* #P18 ^property[1].valueString = "ICD-10 2017:F13.0~F13.1~F13.2~F13.3~F13.4~F13.5~F13.6~F13.7~F13.8~F13.9~F19.0~F19.1~F19.2~F19.3~F19.4~F19.5~F19.6~F19.7~F19.8~F19.9~F55" 
* #P18 ^property[2].code = #hints 
* #P18 ^property[2].valueString = "Beachte:  Hinweise: Bei Problemen durch Substanzmißbrauch sollten die erheblichen Unterschiede zwischen Ländern und Kulturen berücksichtigt werden. Der Arzt kann auch ohne Zustimmung des Patienten einen Fall als 'chronischen Alkoholmißbrauch' klassifizieren, also auch ohne die Bereitschaft des Patienten, therapeutischen Maßnahmen zuzustimmen" 
* #P19 "Drogenmissbrauch"
* #P19 ^definition = Inklusive: Drogenabhängigkeit~ Drogenentzug Exklusive:  Kriterien: Eine Störung, die auf die Einnahme einer abhängig machenden psychoaktiven Substanz zurückzuführen ist und zu mindestens einem der folgenden Zustände führt: akute Intoxikation, übermäßiger Gebrauch mit klinisch signifikanter Schädigung der Gesundheit, Abhängigkeitssyndrom oder Entzugserscheinungen oder psychotisches Erscheinungsbild
* #P19 ^property[0].code = #parent 
* #P19 ^property[0].valueCode = #P 
* #P19 ^property[1].code = #Relationships 
* #P19 ^property[1].valueString = "ICD-10 2017:F11.0~F11.1~F11.2~F11.3~F11.4~F11.5~F11.6~F11.7~F11.8~F11.9~F12.0~F12.1~F12.2~F12.3~F12.4~F12.5~F12.6~F12.7~F12.8~F12.9~F13.0~F13.1~F13.2~F13.3~F13.4~F13.5~F13.6~F13.7~F13.8~F13.9~F14.0~F14.1~F14.2~F14.3~F14.4~F14.5~F14.6~F14.7~F14.8~F14.9~F15.0~F15.1~F15.2~F15.3~F15.4~F15.5~F15.6~F15.7~F15.8~F15.9~F16.0~F16.1~F16.2~F16.3~F16.4~F16.5~F16.6~F16.7~F16.8~F16.9~F18.0~F18.1~F18.2~F18.3~F18.4~F18.5~F18.6~F18.7~F18.8~F18.9~F19.0~F19.1~F19.2~F19.3~F19.4~F19.5~F19.6~F19.7~F19.8~F19.9" 
* #P19 ^property[2].code = #hints 
* #P19 ^property[2].valueString = "Beachte:  Hinweise: Bei Problemen durch Substanzmißbrauch sollten die erheblichen Unterschiede zwischen Ländern und Kulturen berücksichtigt werden. Der Arzt kann auch ohne Zustimmung des Patienten einen Fall als 'chronischen Alkoholmißbrauch' klassifizieren, also auch ohne die Bereitschaft des Patienten, therapeutischen Maßnahmen zuzustimmen" 
* #P20 "Gedächtnisstörung"
* #P20 ^definition = Inklusive: Amnesie~ Desorientiertheit~ Konzentrationsstörung Exklusive:  Kriterien:
* #P20 ^property[0].code = #parent 
* #P20 ^property[0].valueCode = #P 
* #P20 ^property[1].code = #Relationships 
* #P20 ^property[1].valueString = "ICD-10 2017:R41.0~R41.1~R41.2~R41.3~R41.8" 
* #P22 "Verhaltenauffälligkeit/Entwicklungsstörung Kind"
* #P22 ^definition = Inklusive: verzögerte Entwicklungsphasen~ Eifersucht~ hyperaktives Kind~ verzögerte Sprachentwicklung~ Wutausbrüche Exklusive: Verhaltenssymptome/-beschwerden beim Jugendlichen~ beim Erwachsenen P23~ P80~ Sorge um körperliche Entwicklung/Wachstumsverzögerung T10 Kriterien:
* #P22 ^property[0].code = #parent 
* #P22 ^property[0].valueCode = #P 
* #P22 ^property[1].code = #Relationships 
* #P22 ^property[1].valueString = "ICD-10 2017:F91.0~F91.1~F91.2~F91.3~F91.8~F91.9~F92.0~F92.8~F92.9~F93.0~F93.1~F93.2~F93.3~F93.8~F93.9~F94.0~F94.1~F94.2~F94.8~F94.9~F98.8~F98.9~R62.0" 
* #P23 "Verhaltenauffälligkeit/Entwicklungsstörung, Adoleszens"
* #P23 ^definition = Inklusive: Delinquenz Exklusive: Verhaltenssymptome/-beschwerden beim Kind~ beim Erwachsenen P22~ P80~ P81 Kriterien:
* #P23 ^property[0].code = #parent 
* #P23 ^property[0].valueCode = #P 
* #P23 ^property[1].code = #Relationships 
* #P23 ^property[1].valueString = "ICD-10 2017:F91.0~F91.1~F91.2~F91.3~F91.8~F91.9~F92.0~F92.8~F92.9~F94.0~F94.1~F94.2~F94.8~F94.9~F98.8~F98.9" 
* #P24 "Spezifische Lernstörung"
* #P24 ^definition = Inklusive: Dyslexie Exklusive: Aufmerksamkeits-Defizit-Störung P81~ mentale Retardierung P85 Kriterien: Spezifisches Problem im Bereich des Sprechens, des Spracherwerbs oder des Lernens, die im Kindesalter erstmals auftreten, zusammen mit einer Beeinträchtigung von Funktionen, die mit biologischen Reifungsprozessen im Zentralnervensystem zusammenhängen, wobei die Probleme über längere Zeit bestehen bleiben, ohne spontane Remissionen oder Rückfälle, auch wenn die Mängel mit dem Heranwachsen des Kindes abnehmen
* #P24 ^property[0].code = #parent 
* #P24 ^property[0].valueCode = #P 
* #P24 ^property[1].code = #Relationships 
* #P24 ^property[1].valueString = "ICD-10 2017:F80.0~F80.1~F80.2~F80.3~F80.8~F80.9~F81.0~F81.1~F81.2~F81.3~F81.8~F81.9~F82~F83~R48.0~R48.1~R48.2~R48.8" 
* #P25 "Lebensphasenproblem Erwachsener"
* #P25 ^definition = Inklusive: Syndrom des leeren Nests~ Midlife-Krise~ Ruhestandskrise Exklusive: Senilität~ sich alt fühlen/benehmen P05~ Menopause X11 Kriterien:
* #P25 ^property[0].code = #parent 
* #P25 ^property[0].valueCode = #P 
* #P27 "Angst vor Geisteskrankheit"
* #P27 ^definition = Inklusive: Besorgnis wegen Geisteskrankheit~ Angst vor Selbstmordversuch Exklusive: wenn der Patient die Erkrankung hat~ ist die Erkrankung zu kodieren Kriterien: Besorgnis über/Angst vor einer Geisteskrankheit bei einem Patienten, der die Krankheit nicht hat/bevor die Diagnose gesichert ist
* #P27 ^property[0].code = #parent 
* #P27 ^property[0].valueCode = #P 
* #P27 ^property[1].code = #Relationships 
* #P27 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #P28 "Funktionseinschränkung/Behinderung (P)"
* #P28 ^definition = Inklusive: geringes Selbstvertrauen Exklusive:  Kriterien: Funktionseinschränkung/Behinderung durch eine Geisteskrankheit
* #P28 ^property[0].code = #parent 
* #P28 ^property[0].valueCode = #P 
* #P28 ^property[1].code = #Relationships 
* #P28 ^property[1].valueString = "ICD-10 2017:Z73.6" 
* #P28 ^property[2].code = #hints 
* #P28 ^property[2].valueString = "Beachte:  Hinweise: COOP/WONCA Charts eignen sich zur Dokumentation des funktionellen Status des Patienten (siehe Kapitel 8)" 
* #P29 "Andere psychische Symptome/Beschwerden"
* #P29 ^definition = Inklusive: Wahnvorstellungen~ Essstörungen NNB~ Halluzinationen~ multiple psychische Symptome und Beschwerden~ mangelnde Hygiene~ eigenartiges Verhalten~ Misstrauen Exklusive: Spannungskopfschmerz N95 Kriterien:
* #P29 ^property[0].code = #parent 
* #P29 ^property[0].valueCode = #P 
* #P29 ^property[1].code = #Relationships 
* #P29 ^property[1].valueString = "ICD-10 2017:F50.8~F50.9~F63.3~F98.8~F98.9~R44.0~R44.1~R44.2~R44.3~R44.8~R45.7~R45.8~R46.0~R46.1~R46.2~R46.3~R46.4~R46.5~R46.6~R46.7~R46.8~R63.6~Z73.0~Z73.1~Z73.3" 
* #P30 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? vollständig"
* #P30 ^property[0].code = #parent 
* #P30 ^property[0].valueCode = #P 
* #P31 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? teilweise"
* #P31 ^property[0].code = #parent 
* #P31 ^property[0].valueCode = #P 
* #P32 "Allergie-/Sensitivitätstestung"
* #P32 ^property[0].code = #parent 
* #P32 ^property[0].valueCode = #P 
* #P34 "Blutuntersuchung"
* #P34 ^property[0].code = #parent 
* #P34 ^property[0].valueCode = #P 
* #P35 "Urinuntersuchung"
* #P35 ^property[0].code = #parent 
* #P35 ^property[0].valueCode = #P 
* #P36 "Stuhluntersuchung"
* #P36 ^property[0].code = #parent 
* #P36 ^property[0].valueCode = #P 
* #P38 "Andere Laboruntersuchung NAK"
* #P38 ^property[0].code = #parent 
* #P38 ^property[0].valueCode = #P 
* #P39 "Körperliche Funktionsprüfung"
* #P39 ^property[0].code = #parent 
* #P39 ^property[0].valueCode = #P 
* #P40 "Diagnostische Endoskopie"
* #P40 ^property[0].code = #parent 
* #P40 ^property[0].valueCode = #P 
* #P41 "Diagnostisches Röntgen/Bildgebung"
* #P41 ^property[0].code = #parent 
* #P41 ^property[0].valueCode = #P 
* #P42 "Elektrische Aufzeichnungsverfahren"
* #P42 ^property[0].code = #parent 
* #P42 ^property[0].valueCode = #P 
* #P43 "Andere diagnostische Untersuchung"
* #P43 ^property[0].code = #parent 
* #P43 ^property[0].valueCode = #P 
* #P44 "Präventive Impfung/Medikation"
* #P44 ^property[0].code = #parent 
* #P44 ^property[0].valueCode = #P 
* #P45 "Beobachtung/Gesundheitsschulung/Beratung/Diät"
* #P45 ^property[0].code = #parent 
* #P45 ^property[0].valueCode = #P 
* #P46 "Konsultation eines anderen Primärversorgers"
* #P46 ^property[0].code = #parent 
* #P46 ^property[0].valueCode = #P 
* #P47 "Konsultation eines Facharztes"
* #P47 ^property[0].code = #parent 
* #P47 ^property[0].valueCode = #P 
* #P48 "Klärung/Besprechung des Beratungsanlasses mit dem Patienten"
* #P48 ^property[0].code = #parent 
* #P48 ^property[0].valueCode = #P 
* #P49 "Andere Vorsorgemaßnahme"
* #P49 ^property[0].code = #parent 
* #P49 ^property[0].valueCode = #P 
* #P50 "Medikation/Verschreibung/Erneuerung/Injektion"
* #P50 ^property[0].code = #parent 
* #P50 ^property[0].valueCode = #P 
* #P51 "Inzision/Drainage/Spülung/Absaugung/Entfernung von Körperflüssigkeit"
* #P51 ^property[0].code = #parent 
* #P51 ^property[0].valueCode = #P 
* #P52 "Exzision/Entfernung von Gewebe/Biopsie/Destruktion/Debridement/Verkauterung"
* #P52 ^property[0].code = #parent 
* #P52 ^property[0].valueCode = #P 
* #P53 "Instrumentelle Manipulation/Katheterisierung/Intubation/Dilatation"
* #P53 ^property[0].code = #parent 
* #P53 ^property[0].valueCode = #P 
* #P54 "Reposition/Fixierung/Naht/Gips/prothetische Versorgung"
* #P54 ^property[0].code = #parent 
* #P54 ^property[0].valueCode = #P 
* #P55 "Lokale Injektion/Infiltration"
* #P55 ^property[0].code = #parent 
* #P55 ^property[0].valueCode = #P 
* #P56 "Verband/Druck/Kompression/Tamponade"
* #P56 ^property[0].code = #parent 
* #P56 ^property[0].valueCode = #P 
* #P57 "Physikalische Therapie/Rehabilitation"
* #P57 ^property[0].code = #parent 
* #P57 ^property[0].valueCode = #P 
* #P58 "Therapeutische Beratung/Zuhören"
* #P58 ^property[0].code = #parent 
* #P58 ^property[0].valueCode = #P 
* #P59 "Andere therapeutische Maßnahme/kleine Chirurgie NAK"
* #P59 ^property[0].code = #parent 
* #P59 ^property[0].valueCode = #P 
* #P60 "Testresultat/Ergebnis eigene Maßnahme"
* #P60 ^property[0].code = #parent 
* #P60 ^property[0].valueCode = #P 
* #P61 "Ergebnis Untersuchung/Test/Brief von anderem Leistungserbringer"
* #P61 ^property[0].code = #parent 
* #P61 ^property[0].valueCode = #P 
* #P62 "Adminstrative Maßnahme"
* #P62 ^property[0].code = #parent 
* #P62 ^property[0].valueCode = #P 
* #P63 "Folgekonsultation unspezifiziert"
* #P63 ^property[0].code = #parent 
* #P63 ^property[0].valueCode = #P 
* #P64 "Konsultation/Problembehandlung auf Initiative des Leistungserbringers"
* #P64 ^property[0].code = #parent 
* #P64 ^property[0].valueCode = #P 
* #P65 "Konsultation/Problembehandlung auf Initiative Dritter"
* #P65 ^property[0].code = #parent 
* #P65 ^property[0].valueCode = #P 
* #P66 "Überweisung an andere/n Leistungserbringer (nicht ärztl.)/Pflegekraft/Therapeuten/Sozialarbeiter"
* #P66 ^property[0].code = #parent 
* #P66 ^property[0].valueCode = #P 
* #P67 "Überweisung an einen Arzt/Spezialisten/eine Klinik/ein Krankenhaus"
* #P67 ^property[0].code = #parent 
* #P67 ^property[0].valueCode = #P 
* #P68 "Andere Überweisung NAK"
* #P68 ^property[0].code = #parent 
* #P68 ^property[0].valueCode = #P 
* #P69 "Anderer Beratungsanlass NAK"
* #P69 ^property[0].code = #parent 
* #P69 ^property[0].valueCode = #P 
* #P70 "Demenz"
* #P70 ^definition = Inklusive: Alzheimer-Krankheit~ senile Demenz Exklusive:  Kriterien: Ein Syndrom durch eine Hirnerkrankung, üblicherweise chronischer/progredienter Natur, mit klinisch bedeutsamer mehrfacher Störung höherer kortikaler Funktionen (Gedächtnis, Denken, Orientatierung, Verständnis), bei erhaltenem Bewußtsein
* #P70 ^property[0].code = #parent 
* #P70 ^property[0].valueCode = #P 
* #P70 ^property[1].code = #Relationships 
* #P70 ^property[1].valueString = "ICD-10 2017:F00.0~F00.1~F00.2~F00.9~F01.0~F01.1~F01.2~F01.3~F01.8~F01.9~F02.0~F02.1~F02.2~F02.3~F02.4~F02.8~F03~G30.0~G30.1~G30.8~G30.9" 
* #P70 ^property[2].code = #hints 
* #P70 ^property[2].valueString = "Beachte: Senilität P05~ andere psychologische Symptome P29 Hinweise: " 
* #P71 "Organisches Psychosyndrom, anderes"
* #P71 ^definition = Inklusive: Delirium Exklusive: Alkoholbedingte Psychose P15~ nicht spezifizierte Psychose P98 Kriterien: organische psychische Stöungen setzen voraus: Psychosyndrom, Verhaltensweisen aufgrund organischer Krankheit
* #P71 ^property[0].code = #parent 
* #P71 ^property[0].valueCode = #P 
* #P71 ^property[1].code = #Relationships 
* #P71 ^property[1].valueString = "ICD-10 2017:F04~F05.0~F05.1~F05.8~F05.9~F06.0~F06.1~F06.2~F06.3~F06.4~F06.5~F06.6~F06.7~F06.8~F06.9~F07.0~F07.1~F07.2~F07.8~F07.9~F09" 
* #P72 "Schizophrenie"
* #P72 ^definition = Inklusive: alle Formen der Schizophrenie~ Paranoia Exklusive: akute/vorübergehende Psychose P98 Kriterien: grundsätzliche und charakteristische Denkstörungen, Wahrnehmung und Affekte , die inadäquat oder verzerrt sind (z.B. Gedankenlautwerden, Gedankeneingebung, Gedankenentzug, Wahnwahrnehmung, Stimmhalluzinationen, Kontrollwahn), bei klarem Bewußtsein und ohne Einschränkung der intellektuellen Fähigkeit.
* #P72 ^property[0].code = #parent 
* #P72 ^property[0].valueCode = #P 
* #P72 ^property[1].code = #Relationships 
* #P72 ^property[1].valueString = "ICD-10 2017:F20.0~F20.1~F20.2~F20.3~F20.4~F20.5~F20.6~F20.8~F20.9~F21~F22.0~F22.8~F22.9~F24~F25.0~F25.1~F25.2~F25.8~F25.9~F28" 
* #P72 ^property[2].code = #hints 
* #P72 ^property[2].valueString = "Beachte: Psychose NNB P98 Hinweise: " 
* #P73 "Affektive Psychose"
* #P73 ^definition = Inklusive: bipolare Störung~ Hypomanie~ Manie~ manisch-depressive Störung Exklusive: Depression P76 Kriterien: Eine grundlegende Störung im Bereich  der Affekte und Stimmungen, mit Wechsel zwischen euphorischen und deprimierten Zuständen (mit und ohne Angstgefühle). Bei einer manischen Störung sind Stimmung, Energie und Aktivität zugleich erhöht. Bei einer bipolaren Störung treten zumindest zwei Perioden von Stimmungsstörungen auf, mit Wechsel von hoch zu niedrig.
* #P73 ^property[0].code = #parent 
* #P73 ^property[0].valueCode = #P 
* #P73 ^property[1].code = #Relationships 
* #P73 ^property[1].valueString = "ICD-10 2017:F30.0~F30.1~F30.2~F30.8~F30.9~F31.0~F31.1~F31.2~F31.3~F31.4~F31.5~F31.6~F31.7~F31.8~F31.9~F34.0" 
* #P73 ^property[2].code = #hints 
* #P73 ^property[2].valueString = "Beachte: Psychose NNB P98 Hinweise: " 
* #P74 "Angststörung/Panikattacke"
* #P74 ^definition = Inklusive: Angstneurose~ Panikstörung Exklusive: Angsstörung mit Depression P76~ Angst NNB P01 Kriterien: Klinisch bedeutsame Angststörung, die nicht auf eine bestimmte Umweltsituation beschränkt ist. Sie manifestiert sich als Panikattacken (wiederholte Anfälle schwerer Angst , die nicht auf eine bestimmte Situation beschränkt ist, mit oder ohne körperliche Symptome) oder als eine Störung mit generalisierter und andauernder Angst, die nicht auf eine bestimmte Situation bezogen ist und mit unterschiedlichen körperlichen Symptomen auftritt
* #P74 ^property[0].code = #parent 
* #P74 ^property[0].valueCode = #P 
* #P74 ^property[1].code = #Relationships 
* #P74 ^property[1].valueString = "ICD-10 2017:F41.0~F41.1~F41.3~F41.8~F41.9" 
* #P74 ^property[2].code = #hints 
* #P74 ^property[2].valueString = "Beachte: Gefühl von Angst/Nervosität/Anspannung P01 Hinweise: " 
* #P75 "Somatisierungsstörung"
* #P75 ^definition = Inklusive: Konversionsstörung~ hypochondrische Störung~ Hysterie~ Scheinschwangerschaft Exklusive:  Kriterien: Die Somatisierungs-/Konversionsstörung ist gekennzeichnet durch übermäßige Beschäftigung mit und wiederholtem Auftreten von körperlichen Symptomen und Beschwerden, zusammen mit fortwährender Einforderung medizinischer Untersuchung trotz wiederholter unauffälliger Befunde und beruhigender Versicherung der Ärzte.  Um diese Diagnose zu stellen, ist die vom Patienten vorgebrachte Darstellung von mehrfachen, immer wieder auftretenden und häufig wechselnden körperlichen Symptomen gegenüber dem Hausarzt über einen Zeitraum von mindestens einem Jahr erforderlich. Hypochondire setzt eine andauernde Beunruhigung voraus, die sich entweder  auf die äußere Erscheinung oder auf die Möglichkeit, eine ernste Krankheit zu haben, bezieht, verbunden mit ständigen somatischen Beschwerden über einen Zeitraum von mindestens einem Jahr, trotz wiederholter unauffälliger Befunde und beruhigender Versicherung der Ärzte.
* #P75 ^property[0].code = #parent 
* #P75 ^property[0].valueCode = #P 
* #P75 ^property[1].code = #Relationships 
* #P75 ^property[1].valueString = "ICD-10 2017:F44.0~F44.1~F44.2~F44.3~F44.4~F44.5~F44.6~F44.7~F44.8~F44.9~F45.0~F45.1~F45.2~F45.3~F45.4~F45.9" 
* #P75 ^property[2].code = #hints 
* #P75 ^property[2].valueString = "Beachte:  Hinweise: Somatisierung ist das wiederholte Vorbringen körperlicher Symptome und Beschwerden, die auf körperliche Störungen schließen lassen, für die keine nachweisbaren organischen Befunde vorliegen oder physiologischen Mechanismen erkenbar sind, wobei es positive Hinweise darauf gibt, daß sie mit psychischen Faktoren zusammenhängen, während der Patient nicht das Gefühl hat, beim Umgang mit diesen psychischen Faktoren die Hervorbringung der Symptome zu kontrollieren. Psychische Symptome und Beschwerden einschließlich Schmerzen, die vom Patienten als Folge einer körperlichen Störung eine Systems bzw. Organs unter Kontrolle des autonomen Nervensystems dargestellt werden, oder aus andauernden, schweren/quälenden Schmerzen bestehen, die nicht als Folge eines physiologischen Vorgangs bzw. einer physiologischen Störung erklärt werden können, sind unter dem Symptom/der Beschwerde zu kodieren, die den körperlichen Aspekt repräsentiert, und - wenn möglich - mit einem Code, der das emotionelle oder psychische Problem darstellt, mit dem sie verbunden sind. Die Definition der Konversionsstörung in der ICD-10 (mindestens zwei Jahre) ist zu streng für den Gebrauch in der hausärztlichen Praxis" 
* #P76 "Depressive Störung"
* #P76 ^definition = Inklusive: depressive Neurose/Psychose~ gemischte Angst und Depression~ reaktive Depression~ postnatale/Wochenbettdepression Exklusive: akute Stressreaktion P02 Kriterien: eine grundlegende Störung in Affekt und Stimmung in Richtung Depression. Stimmung und Aktivität sind zugleich herabgesetzt, dazu kommt eine Einschränkung der Fähigkeit, Freude, Interesse und Konzentration aufzubringen. Gewöhnlich treten Schlaf- und Appetitstörungen auf, Selbstwertgefühl und Selbstsicherheit sind herabgesetzt.
* #P76 ^property[0].code = #parent 
* #P76 ^property[0].valueCode = #P 
* #P76 ^property[1].code = #Relationships 
* #P76 ^property[1].valueString = "ICD-10 2017:F32.0~F32.1~F32.2~F32.3~F32.8~F32.9~F33.0~F33.1~F33.2~F33.3~F33.4~F33.8~F33.9~F34.1~F34.8~F34.9~F38.0~F38.1~F38.8~F39~F41.2~F53.0" 
* #P76 ^property[2].code = #hints 
* #P76 ^property[2].valueString = "Beachte: depressives Gefühl P03 Hinweise: " 
* #P77 "Suizid/Suizidversuch"
* #P77 ^definition = Inklusive: parasuizidale Handlung~ erfolgreicher Suizidversuch (mit A96 doppelt kodieren) Exklusive: Angst vor Suizid P27 Kriterien:
* #P77 ^property[0].code = #parent 
* #P77 ^property[0].valueCode = #P 
* #P77 ^property[1].code = #Relationships 
* #P77 ^property[1].valueString = "ICD-10 2017:Z91.5" 
* #P78 "Neurasthenie"
* #P78 ^definition = Inklusive:  Exklusive:  Kriterien: Erhöhte Ermüdbarkeit mit unangenehmen Empfindungen, Konzentrationsschwäche und einem ständigen Abnehmen der Leistungsfähigkeit und Lebenstüchtigkeit; ein Gefühl der körperlichen Schwäche und Erschöpfung nach geistiger Anstrengung oder nach geringer körperlicher Anstrengung, das oft von Muskelschmerzen und der Unfähigkeit, zu entspannen begleitet ist
* #P78 ^property[0].code = #parent 
* #P78 ^property[0].valueCode = #P 
* #P78 ^property[1].code = #Relationships 
* #P78 ^property[1].valueString = "ICD-10 2017:F48.0" 
* #P78 ^property[2].code = #hints 
* #P78 ^property[2].valueString = "Beachte: Abgeschlagenheit/postvirales Ermüdungssyndrom/chronic fatigue syndrome A04 Hinweise: " 
* #P79 "Phobie/Zwangsstörung"
* #P79 ^definition = Inklusive:  Exklusive:  Kriterien: die phobische Angststörung setzt voraus: Äußerung deutlicher Ängstlichkeit, die in genau definierten Situationen entsteht, die gewöhnlich nicht als gefährlich angesehen werden; der Patient versucht ie Situationen zu vermeiden oder durchlebt sie mit Angstgefühlen. Die Diagnose Zwangsstörung setzt voraus: das Vorhandensein von quälenden und immer wiederkehrenden Zwangsgedanken oder Zwangshandlungen, die vom Patienten als von ihm selbst stammend erkannt werden; stereotype Zwangshandlungen werden ständig wiederholt, um ein objektiv unwahrscheinliches Ereignis zu verhindern, obwohl sie vom Patienten als sinn- und wirkungslos erkannt werden
* #P79 ^property[0].code = #parent 
* #P79 ^property[0].valueCode = #P 
* #P79 ^property[1].code = #Relationships 
* #P79 ^property[1].valueString = "ICD-10 2017:F40.0~F40.1~F40.2~F40.8~F40.9~F42.0~F42.1~F42.2~F42.8~F42.9" 
* #P80 "Persönlichkeitsstörung"
* #P80 ^definition = Inklusive: Psychopathie~ Kompensationsneurose~ Münchhausen Syndrom~ Verhaltensstörung beim Erwachsenen Exklusive:  Kriterien: anhaltende und klinisch bedeutsame Zustände und Verhaltensmuster im persönlichen Lebensstil bzw. in den Beziehungen zu anderen, die signifikante oder extreme Abweichungen von dem anzeigen, was in einem bestimmten Kulturkreis als Norm für Wahrnehmung, Gefühle und Verhalten eines durchschnittlichen Individuums gilt. Diese Muster sind tief verwurzelt und dauerhaft.
* #P80 ^property[0].code = #parent 
* #P80 ^property[0].valueCode = #P 
* #P80 ^property[1].code = #Relationships 
* #P80 ^property[1].valueString = "ICD-10 2017:F60.0~F60.1~F60.2~F60.3~F60.4~F60.5~F60.6~F60.7~F60.8~F60.9~F61~F62.0~F62.1~F62.8~F62.9~F63.0~F63.1~F63.2~F63.8~F63.9~F68.0~F68.1~F68.8~F69" 
* #P81 "Hyperaktivität"
* #P81 ^definition = Inklusive: Aufmerksamkeitsstörung (ADD)~ Hyperaktivität Exklusive: hyperkinetische Störung mit Beginn in der Pubertät P23~ Lernstörung P24 Kriterien: Frühes Auftreten eine Mangels an Ausdauer in Aktivitäten, die kognitive Beteiligung erfordern, mit der Tendenz, von einer Aktivität zur nächsten zu wechseln, ohne eine davon zu vollenden, mit unorganisiertem und schlecht geregeltem Verhalten und übermäßiger Aktivität
* #P81 ^property[0].code = #parent 
* #P81 ^property[0].valueCode = #P 
* #P81 ^property[1].code = #Relationships 
* #P81 ^property[1].valueString = "ICD-10 2017:F90.0~F90.1~F90.8~F90.9" 
* #P81 ^property[2].code = #hints 
* #P81 ^property[2].valueString = "Beachte: hyperaktives Kind P22 Hinweise: " 
* #P82 "Posttraumatische Belastungsreaktion"
* #P82 ^definition = Inklusive: persistierende Anpassungsstörung Exklusive:  Kriterien: ein Stress verursachendes Ereignis, dem ein bedeutender Zustand von Unruhe und Erregung folgt, mit verzögerten oder verlängerten Reaktionen, Nachhallerinnerungen, Alpträumen, emotionaler Stumpfheit, und Anhedonie, die sich störend auf die sozialen Beziehungen und die Leistungsfähigkeit auswirken und mit dauerhaften Depressionen, Beklemmungen, Besorgnis und dem Gefühl, mit der Umwelt nicht zurechtzukommen, verbunden sind
* #P82 ^property[0].code = #parent 
* #P82 ^property[0].valueCode = #P 
* #P82 ^property[1].code = #Relationships 
* #P82 ^property[1].valueString = "ICD-10 2017:F43.1" 
* #P82 ^property[2].code = #hints 
* #P82 ^property[2].valueString = "Beachte: Angstgefühl P01~ akute Stressreaktion P02~ depressives Gefühl P03 Hinweise: " 
* #P85 "Mentale Retardierung"
* #P85 ^definition = Inklusive:  Exklusive: mentale Retardierung durch angeborene Fehlbildung A90 Kriterien: Mangelnde oder unvollständige Entfaltung der geistigen Fähigkeiten mit Beeinträchtigung der Fertigkeiten während der Entwicklungsphase und ein niedriges allgemeines Intelligenzniveau, mit oder ohne Beeinträchtigung der Fertigkeiten
* #P85 ^property[0].code = #parent 
* #P85 ^property[0].valueCode = #P 
* #P85 ^property[1].code = #Relationships 
* #P85 ^property[1].valueString = "ICD-10 2017:F70.0~F70.1~F70.8~F70.9~F71.0~F71.1~F71.8~F71.9~F72.0~F72.1~F72.8~F72.9~F73.0~F73.1~F73.8~F73.9~F78.0~F78.1~F78.8~F78.9~F79.0~F79.1~F79.8~F79.9" 
* #P86 "Anorexia nervosa/Bulimie"
* #P86 ^definition = Inklusive:  Exklusive:  Kriterien: Anorexia nervosa: vom Patienten bewußt herbeigeführter und unterhaltener Gewichtsverlust, verbunden mit intensiver und überbewerteter Furcht vor Übergewicht sowie vor Körperschemastörung. Bulimie: Wiederholte Anfälle von übermäßigem Essen und übertriebener Sorge um das Körpergewicht, die zu einem Verhaltensmuster führen, bei dem auf das Überessen ein absichtlich herbeigeführtes Erbrechen oder die Anwendung von Abführmitteln folgt
* #P86 ^property[0].code = #parent 
* #P86 ^property[0].valueCode = #P 
* #P86 ^property[1].code = #Relationships 
* #P86 ^property[1].valueString = "ICD-10 2017:F50.0~F50.1~F50.2~F50.3~F50.4" 
* #P86 ^property[2].code = #hints 
* #P86 ^property[2].valueString = "Beachte: Eßstörung~ Nahrungsverweigerung P11~ P29~ Problem beim Füttern T04, T05 Hinweise: " 
* #P98 "Psychose NNB/andere"
* #P98 ^definition = Inklusive: akute/ vorübergehende/puerperale Psychose Exklusive:  Kriterien:
* #P98 ^property[0].code = #parent 
* #P98 ^property[0].valueCode = #P 
* #P98 ^property[1].code = #Relationships 
* #P98 ^property[1].valueString = "ICD-10 2017:F23.0~F23.1~F23.2~F23.3~F23.8~F23.9~F29~F53.1" 
* #P99 "Andere psychische Störung/Erkrankung"
* #P99 ^definition = Inklusive: Autismus~ Neurose NNB Exklusive:  Kriterien:
* #P99 ^property[0].code = #parent 
* #P99 ^property[0].valueCode = #P 
* #P99 ^property[1].code = #Relationships 
* #P99 ^property[1].valueString = "ICD-10 2017:F48.1~F48.8~F48.9~F53.8~F53.9~F54~F59~F84.0~F84.1~F84.2~F84.3~F84.4~F84.5~F84.8~F84.9~F88~F89~F99" 
* #R "Atmungsorgane"
* #R ^property[0].code = #child 
* #R ^property[0].valueCode = #R01 
* #R ^property[1].code = #child 
* #R ^property[1].valueCode = #R02 
* #R ^property[2].code = #child 
* #R ^property[2].valueCode = #R03 
* #R ^property[3].code = #child 
* #R ^property[3].valueCode = #R04 
* #R ^property[4].code = #child 
* #R ^property[4].valueCode = #R05 
* #R ^property[5].code = #child 
* #R ^property[5].valueCode = #R06 
* #R ^property[6].code = #child 
* #R ^property[6].valueCode = #R07 
* #R ^property[7].code = #child 
* #R ^property[7].valueCode = #R08 
* #R ^property[8].code = #child 
* #R ^property[8].valueCode = #R09 
* #R ^property[9].code = #child 
* #R ^property[9].valueCode = #R21 
* #R ^property[10].code = #child 
* #R ^property[10].valueCode = #R23 
* #R ^property[11].code = #child 
* #R ^property[11].valueCode = #R24 
* #R ^property[12].code = #child 
* #R ^property[12].valueCode = #R25 
* #R ^property[13].code = #child 
* #R ^property[13].valueCode = #R26 
* #R ^property[14].code = #child 
* #R ^property[14].valueCode = #R27 
* #R ^property[15].code = #child 
* #R ^property[15].valueCode = #R28 
* #R ^property[16].code = #child 
* #R ^property[16].valueCode = #R29 
* #R ^property[17].code = #child 
* #R ^property[17].valueCode = #R30 
* #R ^property[18].code = #child 
* #R ^property[18].valueCode = #R31 
* #R ^property[19].code = #child 
* #R ^property[19].valueCode = #R32 
* #R ^property[20].code = #child 
* #R ^property[20].valueCode = #R33 
* #R ^property[21].code = #child 
* #R ^property[21].valueCode = #R34 
* #R ^property[22].code = #child 
* #R ^property[22].valueCode = #R35 
* #R ^property[23].code = #child 
* #R ^property[23].valueCode = #R36 
* #R ^property[24].code = #child 
* #R ^property[24].valueCode = #R37 
* #R ^property[25].code = #child 
* #R ^property[25].valueCode = #R38 
* #R ^property[26].code = #child 
* #R ^property[26].valueCode = #R39 
* #R ^property[27].code = #child 
* #R ^property[27].valueCode = #R40 
* #R ^property[28].code = #child 
* #R ^property[28].valueCode = #R41 
* #R ^property[29].code = #child 
* #R ^property[29].valueCode = #R42 
* #R ^property[30].code = #child 
* #R ^property[30].valueCode = #R43 
* #R ^property[31].code = #child 
* #R ^property[31].valueCode = #R44 
* #R ^property[32].code = #child 
* #R ^property[32].valueCode = #R45 
* #R ^property[33].code = #child 
* #R ^property[33].valueCode = #R46 
* #R ^property[34].code = #child 
* #R ^property[34].valueCode = #R47 
* #R ^property[35].code = #child 
* #R ^property[35].valueCode = #R48 
* #R ^property[36].code = #child 
* #R ^property[36].valueCode = #R49 
* #R ^property[37].code = #child 
* #R ^property[37].valueCode = #R50 
* #R ^property[38].code = #child 
* #R ^property[38].valueCode = #R51 
* #R ^property[39].code = #child 
* #R ^property[39].valueCode = #R52 
* #R ^property[40].code = #child 
* #R ^property[40].valueCode = #R53 
* #R ^property[41].code = #child 
* #R ^property[41].valueCode = #R54 
* #R ^property[42].code = #child 
* #R ^property[42].valueCode = #R55 
* #R ^property[43].code = #child 
* #R ^property[43].valueCode = #R56 
* #R ^property[44].code = #child 
* #R ^property[44].valueCode = #R57 
* #R ^property[45].code = #child 
* #R ^property[45].valueCode = #R58 
* #R ^property[46].code = #child 
* #R ^property[46].valueCode = #R59 
* #R ^property[47].code = #child 
* #R ^property[47].valueCode = #R60 
* #R ^property[48].code = #child 
* #R ^property[48].valueCode = #R61 
* #R ^property[49].code = #child 
* #R ^property[49].valueCode = #R62 
* #R ^property[50].code = #child 
* #R ^property[50].valueCode = #R63 
* #R ^property[51].code = #child 
* #R ^property[51].valueCode = #R64 
* #R ^property[52].code = #child 
* #R ^property[52].valueCode = #R65 
* #R ^property[53].code = #child 
* #R ^property[53].valueCode = #R66 
* #R ^property[54].code = #child 
* #R ^property[54].valueCode = #R67 
* #R ^property[55].code = #child 
* #R ^property[55].valueCode = #R68 
* #R ^property[56].code = #child 
* #R ^property[56].valueCode = #R69 
* #R ^property[57].code = #child 
* #R ^property[57].valueCode = #R71 
* #R ^property[58].code = #child 
* #R ^property[58].valueCode = #R72 
* #R ^property[59].code = #child 
* #R ^property[59].valueCode = #R73 
* #R ^property[60].code = #child 
* #R ^property[60].valueCode = #R74 
* #R ^property[61].code = #child 
* #R ^property[61].valueCode = #R75 
* #R ^property[62].code = #child 
* #R ^property[62].valueCode = #R76 
* #R ^property[63].code = #child 
* #R ^property[63].valueCode = #R77 
* #R ^property[64].code = #child 
* #R ^property[64].valueCode = #R78 
* #R ^property[65].code = #child 
* #R ^property[65].valueCode = #R79 
* #R ^property[66].code = #child 
* #R ^property[66].valueCode = #R80 
* #R ^property[67].code = #child 
* #R ^property[67].valueCode = #R81 
* #R ^property[68].code = #child 
* #R ^property[68].valueCode = #R82 
* #R ^property[69].code = #child 
* #R ^property[69].valueCode = #R83 
* #R ^property[70].code = #child 
* #R ^property[70].valueCode = #R84 
* #R ^property[71].code = #child 
* #R ^property[71].valueCode = #R85 
* #R ^property[72].code = #child 
* #R ^property[72].valueCode = #R86 
* #R ^property[73].code = #child 
* #R ^property[73].valueCode = #R87 
* #R ^property[74].code = #child 
* #R ^property[74].valueCode = #R88 
* #R ^property[75].code = #child 
* #R ^property[75].valueCode = #R89 
* #R ^property[76].code = #child 
* #R ^property[76].valueCode = #R90 
* #R ^property[77].code = #child 
* #R ^property[77].valueCode = #R92 
* #R ^property[78].code = #child 
* #R ^property[78].valueCode = #R95 
* #R ^property[79].code = #child 
* #R ^property[79].valueCode = #R96 
* #R ^property[80].code = #child 
* #R ^property[80].valueCode = #R97 
* #R ^property[81].code = #child 
* #R ^property[81].valueCode = #R98 
* #R ^property[82].code = #child 
* #R ^property[82].valueCode = #R99 
* #R01 "Schmerzen Atmungssystem"
* #R01 ^definition = Inklusive: Schmerzhaftes Durchatmen~ pleuraler Schmerz~ Pleurodynie Exklusive: Thoraxschmerzen A11~ bewegungsabhängige Thoraxschmerzen L04~ Nasenschmerzen R08~ Nebenhöhlenschmerzen R09~ Halsweh R21~ Engegefühl im Thorax R29~ Pleuritis R82 Kriterien:
* #R01 ^property[0].code = #parent 
* #R01 ^property[0].valueCode = #R 
* #R01 ^property[1].code = #Relationships 
* #R01 ^property[1].valueString = "ICD-10 2017:R07.1" 
* #R02 "Kurzatmigkeit/Dyspnoe"
* #R02 ^definition = Inklusive: Orthopnoea Exklusive: Keuchen R03~ Stridor R04~ Hyperventilation R98 Kriterien:
* #R02 ^property[0].code = #parent 
* #R02 ^property[0].valueCode = #R 
* #R02 ^property[1].code = #Relationships 
* #R02 ^property[1].valueString = "ICD-10 2017:R06.0" 
* #R03 "Atemgeräusch/Giemen/Brummen"
* #R03 ^definition = Inklusive: inspiratorischer Stridor~ Rhonchus Exklusive: Dyspnoe R02~ Stridor R04~ Hyperventilation R98 Kriterien:
* #R03 ^property[0].code = #parent 
* #R03 ^property[0].valueCode = #R 
* #R03 ^property[1].code = #Relationships 
* #R03 ^property[1].valueString = "ICD-10 2017:R06.2" 
* #R04 "Atemproblem, andere"
* #R04 ^definition = Inklusive: abnorme Atmung~ Apnoe~ Atempausen~ Respiratory Distress Syndrom~ Schnarchen~ Stridor~ Tachypnoe Exklusive: Schlafapnoe P06~ Atemschmerzen R01~ Dyspnoe R02~ Keuchen R03~ Husten R05~ Hyperventilation R98 Kriterien:
* #R04 ^property[0].code = #parent 
* #R04 ^property[0].valueCode = #R 
* #R04 ^property[1].code = #Relationships 
* #R04 ^property[1].valueString = "ICD-10 2017:R06.1~R06.3~R06.5~R06.8" 
* #R05 "Husten"
* #R05 ^definition = Inklusive: Husten (trocken oder feucht) Exklusive: abnormer Auswurf/Schleim R25 Kriterien:
* #R05 ^property[0].code = #parent 
* #R05 ^property[0].valueCode = #R 
* #R05 ^property[1].code = #Relationships 
* #R05 ^property[1].valueString = "ICD-10 2017:R05" 
* #R06 "Nasenbluten/Epistaxis"
* #R06 ^property[0].code = #parent 
* #R06 ^property[0].valueCode = #R 
* #R06 ^property[1].code = #Relationships 
* #R06 ^property[1].valueString = "ICD-10 2017:R04.0" 
* #R07 "Schnupfen/Niesen/verstopfte Nase"
* #R07 ^definition = Inklusive: verstopfte Nase~ Rhinorrhoe~ laufende Nase Exklusive:  Kriterien:
* #R07 ^property[0].code = #parent 
* #R07 ^property[0].valueCode = #R 
* #R07 ^property[1].code = #Relationships 
* #R07 ^property[1].valueString = "ICD-10 2017:R06.7" 
* #R08 "Nasensymptome/-beschwerden andere"
* #R08 ^definition = Inklusive: Schmerzen in der Nase~ post-nasal drip~ prominente Nase~ rote Nase Exklusive: Anosmie N16~ Epistaxis R06~ verstopfte Nase/Niesen R07~ Nebenhöhlenbeschwerden R09~ Rhinophym S99 Kriterien:
* #R08 ^property[0].code = #parent 
* #R08 ^property[0].valueCode = #R 
* #R08 ^property[1].code = #Relationships 
* #R08 ^property[1].valueString = "ICD-10 2017:J34.8" 
* #R09 "Nasennebenhöhlensymptome/-beschwerden"
* #R09 ^definition = Inklusive: verstopfte Nasennebenhöhlen~ Schmerz/Druckgefühl in einer Nasennebenhöhle Exklusive: Kopfschmerz N01~ Gesichtsschmerz N03~ verstopfte Nase R07 Kriterien:
* #R09 ^property[0].code = #parent 
* #R09 ^property[0].valueCode = #R 
* #R09 ^property[1].code = #Relationships 
* #R09 ^property[1].valueString = "ICD-10 2017:J34.8" 
* #R21 "Hals-/Rachensymptome/-beschwerden"
* #R21 ^definition = Inklusive: trockener/entzündeter/geröteter/gereizter Rachen~ vergrößerte Tonsillen~ Globusgefühl~ schmerzende Tonsillen Exklusive: Stimmsymptome R23~ Hypertrophie der Mandeln R90 Kriterien:
* #R21 ^property[0].code = #parent 
* #R21 ^property[0].valueCode = #R 
* #R21 ^property[1].code = #Relationships 
* #R21 ^property[1].valueString = "ICD-10 2017:R07.0~R09.8" 
* #R23 "Stimmsymptome/-beschwerden"
* #R23 ^definition = Inklusive: Stimmverlust~ Aphonie~ Heiserkeit Exklusive: neurologische Sprachstörung N19~ Stammeln/ Stottern/ Tic P10~ Halsschmerzen R21 Kriterien:
* #R23 ^property[0].code = #parent 
* #R23 ^property[0].valueCode = #R 
* #R23 ^property[1].code = #Relationships 
* #R23 ^property[1].valueString = "ICD-10 2017:R49.0~R49.1~R49.2~R49.8" 
* #R24 "Hämoptysis"
* #R24 ^definition = Inklusive: Bluthusten Exklusive:  Kriterien:
* #R24 ^property[0].code = #parent 
* #R24 ^property[0].valueCode = #R 
* #R24 ^property[1].code = #Relationships 
* #R24 ^property[1].valueString = "ICD-10 2017:R04.2" 
* #R25 "Abnormes Sputum/Schleim"
* #R25 ^definition = Inklusive:  Exklusive: Husten mit Auswurf R05~ Hämoptyse R24 Kriterien:
* #R25 ^property[0].code = #parent 
* #R25 ^property[0].valueCode = #R 
* #R25 ^property[1].code = #Relationships 
* #R25 ^property[1].valueString = "ICD-10 2017:R09.3" 
* #R26 "Angst vor Krebs des Atmungssystems"
* #R26 ^definition = Inklusive:  Exklusive: wenn der Patient die Erkrankung hat~ ist die Erkrankung zu kodieren Kriterien: Besorgnis über/Angst vor Krebs der Atemwege bei einem Patienten, der die Krankheit nicht hat/bevor die Diagnose gesichert ist
* #R26 ^property[0].code = #parent 
* #R26 ^property[0].valueCode = #R 
* #R26 ^property[1].code = #Relationships 
* #R26 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #R27 "Angst vor Atemwegserkrankung, anderer"
* #R27 ^definition = Inklusive:  Exklusive: Angst vor Krebs des Atmungssystems R26~ wenn der Patient die Erkrankung hat~ kodiere die Erkrankung Kriterien: Besorgnis über/Angst vor einer anderen Erkrankung der Atemwege bei einem Patienten, der die Krankheit nicht hat/bevor die Diagnose gesichert ist
* #R27 ^property[0].code = #parent 
* #R27 ^property[0].valueCode = #R 
* #R27 ^property[1].code = #Relationships 
* #R27 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #R28 "Funktionseinschränkung/Behinderung (R)"
* #R28 ^definition = Inklusive: Behinderung infolge Hypoxie~ Hyperkapnie~ eingeschränkter Lungenfunktion~ Erkrankungen der Atemwege oder als Folge von Erkrankungen der Nase/des Kehlkopfes/des Rachens Exklusive: Dyspnoe R02~ Keuchen R03 Kriterien: Funktionseinschränkung/Behinderung durch ein Problem der Atemwege
* #R28 ^property[0].code = #parent 
* #R28 ^property[0].valueCode = #R 
* #R28 ^property[1].code = #Relationships 
* #R28 ^property[1].valueString = "ICD-10 2017:Z73.6~Z99.1" 
* #R28 ^property[2].code = #hints 
* #R28 ^property[2].valueString = "Beachte:  Hinweise: COOP/WONCA Charts eignen sich zur Dokumentation des funktionellen Status des Patienten (siehe Kapitel 8)" 
* #R29 "Andere Symptome/Beschwerden der Atmungsorgane"
* #R29 ^definition = Inklusive: Beklemmung im Brustkorb~ Flüssigkeit in der Lunge~ Schluckauf~ Lungenstauung Exklusive:  Kriterien:
* #R29 ^property[0].code = #parent 
* #R29 ^property[0].valueCode = #R 
* #R29 ^property[1].code = #Relationships 
* #R29 ^property[1].valueString = "ICD-10 2017:R04.1~R04.8~R04.9~R06.6~R09.0~R09.2~R09.8" 
* #R30 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? vollständig"
* #R30 ^property[0].code = #parent 
* #R30 ^property[0].valueCode = #R 
* #R31 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? teilweise"
* #R31 ^property[0].code = #parent 
* #R31 ^property[0].valueCode = #R 
* #R32 "Allergie-/Sensitivitätstestung"
* #R32 ^property[0].code = #parent 
* #R32 ^property[0].valueCode = #R 
* #R33 "Mikrobiologische/immunologische Untersuchung"
* #R33 ^property[0].code = #parent 
* #R33 ^property[0].valueCode = #R 
* #R34 "Blutuntersuchung"
* #R34 ^property[0].code = #parent 
* #R34 ^property[0].valueCode = #R 
* #R35 "Urinuntersuchung"
* #R35 ^property[0].code = #parent 
* #R35 ^property[0].valueCode = #R 
* #R36 "Stuhluntersuchung"
* #R36 ^property[0].code = #parent 
* #R36 ^property[0].valueCode = #R 
* #R37 "Histologische Untersuchung/zytologischer Abstrich"
* #R37 ^property[0].code = #parent 
* #R37 ^property[0].valueCode = #R 
* #R38 "Andere Laboruntersuchung NAK"
* #R38 ^property[0].code = #parent 
* #R38 ^property[0].valueCode = #R 
* #R39 "Körperliche Funktionsprüfung"
* #R39 ^property[0].code = #parent 
* #R39 ^property[0].valueCode = #R 
* #R40 "Diagnostische Endoskopie"
* #R40 ^property[0].code = #parent 
* #R40 ^property[0].valueCode = #R 
* #R41 "Diagnostisches Röntgen/Bildgebung"
* #R41 ^property[0].code = #parent 
* #R41 ^property[0].valueCode = #R 
* #R42 "Elektrische Aufzeichnungsverfahren"
* #R42 ^property[0].code = #parent 
* #R42 ^property[0].valueCode = #R 
* #R43 "Andere diagnostische Untersuchung"
* #R43 ^property[0].code = #parent 
* #R43 ^property[0].valueCode = #R 
* #R44 "Präventive Impfung/Medikation"
* #R44 ^property[0].code = #parent 
* #R44 ^property[0].valueCode = #R 
* #R45 "Beobachtung/Gesundheitsschulung/Beratung/Diät"
* #R45 ^property[0].code = #parent 
* #R45 ^property[0].valueCode = #R 
* #R46 "Konsultation eines anderen Primärversorgers"
* #R46 ^property[0].code = #parent 
* #R46 ^property[0].valueCode = #R 
* #R47 "Konsultation eines Facharztes"
* #R47 ^property[0].code = #parent 
* #R47 ^property[0].valueCode = #R 
* #R48 "Klärung/Besprechung des Beratungsanlasses mit dem Patienten"
* #R48 ^property[0].code = #parent 
* #R48 ^property[0].valueCode = #R 
* #R49 "Andere Vorsorgemaßnahme"
* #R49 ^property[0].code = #parent 
* #R49 ^property[0].valueCode = #R 
* #R50 "Medikation/Verschreibung/Erneuerung/Injektion"
* #R50 ^property[0].code = #parent 
* #R50 ^property[0].valueCode = #R 
* #R51 "Inzision/Drainage/Spülung/Absaugung/Entfernung von Körperflüssigkeit"
* #R51 ^property[0].code = #parent 
* #R51 ^property[0].valueCode = #R 
* #R52 "Exzision/Entfernung von Gewebe/Biopsie/Destruktion/Debridement/Verkauterung"
* #R52 ^property[0].code = #parent 
* #R52 ^property[0].valueCode = #R 
* #R53 "Instrumentelle Manipulation/Katheterisierung/Intubation/Dilatation"
* #R53 ^property[0].code = #parent 
* #R53 ^property[0].valueCode = #R 
* #R54 "Reposition/Fixierung/Naht/Gips/prothetische Versorgung"
* #R54 ^property[0].code = #parent 
* #R54 ^property[0].valueCode = #R 
* #R55 "Lokale Injektion/Infiltration"
* #R55 ^property[0].code = #parent 
* #R55 ^property[0].valueCode = #R 
* #R56 "Verband/Druck/Kompression/Tamponade"
* #R56 ^property[0].code = #parent 
* #R56 ^property[0].valueCode = #R 
* #R57 "Physikalische Therapie/Rehabilitation"
* #R57 ^property[0].code = #parent 
* #R57 ^property[0].valueCode = #R 
* #R58 "Therapeutische Beratung/Zuhören"
* #R58 ^property[0].code = #parent 
* #R58 ^property[0].valueCode = #R 
* #R59 "Andere therapeutische Maßnahme/kleine Chirurgie NAK"
* #R59 ^property[0].code = #parent 
* #R59 ^property[0].valueCode = #R 
* #R60 "Testresultat/Ergebnis eigene Maßnahme"
* #R60 ^property[0].code = #parent 
* #R60 ^property[0].valueCode = #R 
* #R61 "Ergebnis Untersuchung/Test/Brief von anderem Leistungserbringer"
* #R61 ^property[0].code = #parent 
* #R61 ^property[0].valueCode = #R 
* #R62 "Adminstrative Maßnahme"
* #R62 ^property[0].code = #parent 
* #R62 ^property[0].valueCode = #R 
* #R63 "Folgekonsultation unspezifiziert"
* #R63 ^property[0].code = #parent 
* #R63 ^property[0].valueCode = #R 
* #R64 "Konsultation/Problembehandlung auf Initiative des Leistungserbringers"
* #R64 ^property[0].code = #parent 
* #R64 ^property[0].valueCode = #R 
* #R65 "Konsultation/Problembehandlung auf Initiative Dritter"
* #R65 ^property[0].code = #parent 
* #R65 ^property[0].valueCode = #R 
* #R66 "Überweisung an andere/n Leistungserbringer (nicht ärztl.)/Pflegekraft/Therapeuten/Sozialarbeiter"
* #R66 ^property[0].code = #parent 
* #R66 ^property[0].valueCode = #R 
* #R67 "Überweisung an einen Arzt/Spezialisten/eine Klinik/ein Krankenhaus"
* #R67 ^property[0].code = #parent 
* #R67 ^property[0].valueCode = #R 
* #R68 "Andere Überweisung NAK"
* #R68 ^property[0].code = #parent 
* #R68 ^property[0].valueCode = #R 
* #R69 "Anderer Beratungsanlass NAK"
* #R69 ^property[0].code = #parent 
* #R69 ^property[0].valueCode = #R 
* #R71 "Keuchhusten"
* #R71 ^definition = Inklusive: Parapertussis~ Pertussis Exklusive: Krupp R77 Kriterien: Infektion der Atemwege mit einem charakteristischem paroxysmalen Staccato-Husten, der mit einem hohen "Ziehen" beim Einatmen endet; oder eine Infektion der Atemwege mit mindestens drei Wochen anhaltendem Husten in zeitlichem Zusammenhang mit nachgewiesenem Keuchhusten; oder Nachweis von Bordetella pertussis oder parapertussis
* #R71 ^property[0].code = #parent 
* #R71 ^property[0].valueCode = #R 
* #R71 ^property[1].code = #Relationships 
* #R71 ^property[1].valueString = "ICD-10 2017:A37.0~A37.1~A37.8~A37.9" 
* #R71 ^property[2].code = #hints 
* #R71 ^property[2].valueString = "Beachte: Husten R05~ Infektion der oberen Atemwege R74 Hinweise: " 
* #R72 "Streptokokkeninfekt Hals"
* #R72 ^definition = Inklusive: nachgewiesene Streptokokkenpharyngitis/-tonsillitis Exklusive: Scharlach A78~ Erysipel/Streprokokkeninfektion der Haut S76 Kriterien: Akute Entzündung im Rachenraum plus Nachweis betahämolysierender Streptokokken
* #R72 ^property[0].code = #parent 
* #R72 ^property[0].valueCode = #R 
* #R72 ^property[1].code = #Relationships 
* #R72 ^property[1].valueString = "ICD-10 2017:J02.0~J03.0" 
* #R72 ^property[2].code = #hints 
* #R72 ^property[2].valueString = "Beachte: Tonsillitis R76 Hinweise: " 
* #R73 "Furunkel/Abszess Nase"
* #R73 ^definition = Inklusive: lokalisierte Naseninfektion Exklusive: akute Sinusitis R75 Kriterien:
* #R73 ^property[0].code = #parent 
* #R73 ^property[0].valueCode = #R 
* #R73 ^property[1].code = #Relationships 
* #R73 ^property[1].valueString = "ICD-10 2017:J34.0" 
* #R74 "Infektion obere Atemwege, akut"
* #R74 ^definition = Inklusive: akute Rhinitis~ Coryza~ Nasopharyngitis~ Pharyngitis~ Infektion der oberen Atemwege Exklusive: Masern A71~ infektiöse Mononukleose A75~ virale Pharyngokonjunktivitis F70~ Sinusitis R75~ Tonsillitis/Mandelentzündung R76~ Laryngitis/Krupp R77~ Influenza R80~ chronische Pharyngitis R83~ allergische Rhinitis R97 Kriterien: Nachweis einer akuten Entzündung der Nasen- oder Rachenschleimhaut bei fehlenden Kriterien für spezifische akute Infektion der Atemwege, die in diesem Abschnitt klassifiziert sind
* #R74 ^property[0].code = #parent 
* #R74 ^property[0].valueCode = #R 
* #R74 ^property[1].code = #Relationships 
* #R74 ^property[1].valueString = "ICD-10 2017:B00.2~B08.5~J00~J02.8~J02.9~J06.0~J06.8~J06.9" 
* #R75 "Sinusitis, akute/chronische"
* #R75 ^definition = Inklusive: Sinusitis~ die eine Nasennebenhöhle betrifft Exklusive:  Kriterien: eitriger nasaler oder postnasaler Ausfluß oder vorhergegangene ärztlich behandelte Fälle von Sinusitis, plus Druckempfindlichkeit in einer oder mehrerer Nasennebenhöhlen, oder ein tiefsitzender Gesichtsschmerz, der sich beim Senken des Kopfes verstärkt, oder Verschattung bei Durchleuchtung, oder Nachweis einer Sinusitis im bildgebenden Verfahren, oder eitrige Absonderung aus Nasennebenhöhlen
* #R75 ^property[0].code = #parent 
* #R75 ^property[0].valueCode = #R 
* #R75 ^property[1].code = #Relationships 
* #R75 ^property[1].valueString = "ICD-10 2017:J01.0~J01.1~J01.2~J01.3~J01.4~J01.8~J01.9~J32.0~J32.1~J32.2~J32.3~J32.4~J32.8~J32.9" 
* #R75 ^property[2].code = #hints 
* #R75 ^property[2].valueString = "Beachte: Kopfschmerz N01~ Gesichtsschmerz N03~ Infektion der oberen Atemwege R74 Hinweise: " 
* #R76 "Tonsillitis, akute"
* #R76 ^definition = Inklusive: Peritonsillarabszess~ Mandelentzündung Exklusive: infektöse Mononukleose A75~ Halsinfektion durch Streptokokken R72~ Diphtherie R83~ Hypertrophie/chronische Infektion der Tonsillen R90 Kriterien: Halsentzündung oder Fieber mit stärkerer Rötung der Tonsillen als der hinteren Rachenwand und entweder Eiter auf
* #R76 ^property[0].code = #parent 
* #R76 ^property[0].valueCode = #R 
* #R76 ^property[1].code = #Relationships 
* #R76 ^property[1].valueString = "ICD-10 2017:J03.8~J03.9~J36" 
* #R76 ^property[2].code = #hints 
* #R76 ^property[2].valueString = "Beachte: akute Infektion der oberen Atemwege R74 Hinweise: " 
* #R77 "Laryngitis/Tracheitis, akute"
* #R77 ^definition = Inklusive: Krupp Exklusive: Laryngotracheobronchitis R78~ Epiglottitis R83 Kriterien: Heiserkeit/Stridor mit oder ohne Atemnot, oder tiefer, trockener, schmerzhafter Husten (bei Kindern bellend) sowie normaler Lungenbefund
* #R77 ^property[0].code = #parent 
* #R77 ^property[0].valueCode = #R 
* #R77 ^property[1].code = #Relationships 
* #R77 ^property[1].valueString = "ICD-10 2017:J04.0~J04.1~J04.2~J05.0~J38.5" 
* #R77 ^property[2].code = #hints 
* #R77 ^property[2].valueString = "Beachte: Infektion der oberen Atemwege R74 Hinweise: " 
* #R78 "Akute Bronchitis/Bronchiolitis"
* #R78 ^definition = Inklusive: akute Infektion der unteren Atemwege NNB~ Bronchitis NNB~ Entzündung im Brustraum NNB~ Laryngotracheobronchitis~ Tracheobronchitis Exklusive: Influenza R80~ chronische Bronchitis R91~ allergic Bronchitis R96 Kriterien: Bei Kindern und Erwachsenen Husten und Fieber mit lokalen oder generalisierten Befunden an der Lunge wie z.B. pfeifendes Atmen, heisere Rasselgeräusche, pfeifende oder feuchte Rasselgeräusche. Bei Kindern (Brochiolotis): Dyspnoe und obstruktives Emphysem
* #R78 ^property[0].code = #parent 
* #R78 ^property[0].valueCode = #R 
* #R78 ^property[1].code = #Relationships 
* #R78 ^property[1].valueString = "ICD-10 2017:J20.0~J20.1~J20.2~J20.3~J20.4~J20.5~J20.6~J20.7~J20.8~J20.9~J21.0~J21.1~J21.8~J21.9~J22~J40" 
* #R78 ^property[2].code = #hints 
* #R78 ^property[2].valueString = "Beachte: Keuchen R03~ Husten R05~ Infektion der oberen Atemwege R74 Hinweise: " 
* #R79 "Chronische Bronchitis"
* #R79 ^definition = Inklusive:  Exklusive: Emphysem/chronisch obstruktive Lungen-/Atemwegserkrankung R95~ Bronchiektasie R99 Kriterien: während mindestens zwei Jahren ein über drei Monate dauernder, an den meisten Tagen auftretender Husten mit eitrigen Auswurf und vereinzelt Rasselgeräuschen oder Giemen bei Auskultation des Brustkorbs während dieser Perioden
* #R79 ^property[0].code = #parent 
* #R79 ^property[0].valueCode = #R 
* #R79 ^property[1].code = #Relationships 
* #R79 ^property[1].valueString = "ICD-10 2017:J41.0~J41.1~J41.8~J42" 
* #R79 ^property[2].code = #hints 
* #R79 ^property[2].valueString = "Beachte: Husten R05~ abnormes Sputum/abnormer Auswurf R25~ Bronchitis NNB R78 Hinweise: " 
* #R80 "Influenza"
* #R80 ^definition = Inklusive: grippeartige Erkrankungen~ Parainfluenza Exklusive: viraler Magen-Darm-Infekt D70~ Grippepneumonie R81 Kriterien: Muskelschmerz und Husten ohne besondere somatische Anzeichen außer Rötung der Nasenschleimhäute und des Rachens plus drei oder mehr der folgenden Punkte: plötzliches Auftreten (innerhalb von 12 Stunden), Schüttelfrost, Frösteln oder Fieber, Erschöpfung und Abgeschlagenheit, Influenza bei direkten Kontaktpersonen oder Grippeepidemie, oder Viruskultur oder serologischer Nachweis einer Influenza-Virusinfektion
* #R80 ^property[0].code = #parent 
* #R80 ^property[0].valueCode = #R 
* #R80 ^property[1].code = #Relationships 
* #R80 ^property[1].valueString = "ICD-10 2017:J09~J10.1~J10.8~J11.1~J11.8" 
* #R80 ^property[2].code = #hints 
* #R80 ^property[2].valueString = "Beachte: Fieber A03~ Virusinfektion NNB A77~ Infektion der oberen Atemwege R74 Hinweise: " 
* #R81 "Pneumonie"
* #R81 ^definition = Inklusive: bakterielle/virale Pneumonie~ Bronchopneumonie~ Grippepneumonie~ Legionärskrankheit~ Pneumonitis Exklusive: Aspirationspneumonie R99 Kriterien: Hinweis auf pulmonales Infiltrat
* #R81 ^property[0].code = #parent 
* #R81 ^property[0].valueCode = #R 
* #R81 ^property[1].code = #Relationships 
* #R81 ^property[1].valueString = "ICD-10 2017:A48.1~B33.4~J10.0~J11.0~J12.0~J12.1~J12.2~J12.3~J12.8~J12.9~J13~J14~J15.0~J15.1~J15.2~J15.3~J15.4~J15.5~J15.6~J15.7~J15.8~J15.9~J16.0~J16.8~J17.0~J17.1~J17.2~J17.3~J17.8~J18.0~J18.1~J18.2~J18.8~J18.9" 
* #R81 ^property[2].code = #hints 
* #R81 ^property[2].valueString = "Beachte: Husten R05~ akute Bronchitis R78 Hinweise: " 
* #R82 "Pleuritis/Pleuraerguss"
* #R82 ^definition = Inklusive: entzündliches pleurales Exsudar~ Pleuritis Exklusive: Tuberkulose A70~ Pneumonie R81~bösartige effusion to be kodierend to origin einer bösartige Neubildung Kriterien: klinischer Hinweis auf Pleuraerguß; oder pleuritische Schmerzen begleitet von Pleurareiben; oder zytologischer bzw. bakteriologischer Nachweis einer Entzündung in der Pleuraflüssigkeit
* #R82 ^property[0].code = #parent 
* #R82 ^property[0].valueCode = #R 
* #R82 ^property[1].code = #Relationships 
* #R82 ^property[1].valueString = "ICD-10 2017:J90~J91~J94.0~J94.1~J94.2~J94.8~J94.9~R09.1" 
* #R82 ^property[2].code = #hints 
* #R82 ^property[2].valueString = "Beachte: pleuritischer Schmerz R01 Hinweise: " 
* #R83 "Atemwegsinfekt, anderer"
* #R83 ^definition = Inklusive: chronische Nasopharyngitis~ chronische Pharyngitis~ chronische Rhinitis NNB~ Diphtherie~ Empyem~ Epiglottitis~ Pilzinfektion der Lunge~ Lungenabszess~ Protozoeninfektion der Lunge (ohne Pneumonie) Exklusive: Zystische Fibrose T99 Kriterien:
* #R83 ^property[0].code = #parent 
* #R83 ^property[0].valueCode = #R 
* #R83 ^property[1].code = #Relationships 
* #R83 ^property[1].valueString = "ICD-10 2017:A36.0~A36.1~A36.2~A36.3~A36.8~A36.9~B37.1~B44.0~B44.1~B44.2~B44.7~B44.8~B44.9~B58.3~J05.1~J31.0~J31.1~J31.2~J37.0~J37.1~J85.0~J85.1~J85.2~J85.3~J86.0~J86.9" 
* #R84 "Bösartige Neubildung Lunge/Bronchus"
* #R84 ^definition = Inklusive: Bösartige Neubildung der Trachea/des Bronchus/der Lunge Exklusive: bösartige Neubildung unbekannter Lokalisation A79~ eine sekundäre bösartige Neubildung bekannter Lokalisation ist bei der Lokalisation zu kodieren Kriterien: characteristisches histologisches Erscheinungsbild
* #R84 ^property[0].code = #parent 
* #R84 ^property[0].valueCode = #R 
* #R84 ^property[1].code = #Relationships 
* #R84 ^property[1].valueString = "ICD-10 2017:C33~C34.0~C34.1~C34.2~C34.3~C34.8~C34.9~C45.7" 
* #R84 ^property[2].code = #hints 
* #R84 ^property[2].valueString = "Beachte: nicht spezifizierte Neubildung der Atemwege R92 Hinweise: " 
* #R85 "Bösartige Neubildung Atemwege, andere"
* #R85 ^definition = Inklusive: Bösartige Neubildungmalignancy von Larynx/Mediastinum/Nase/Pharynx/Pleura/Sinus~ Mesotheliom Exklusive: Morbus Hodgkin B72~ bösartige Neubildung der Luftröhre/des Bronchus/der Lunge R84 Kriterien: characteristisches histologisches Erscheinungsbild
* #R85 ^property[0].code = #parent 
* #R85 ^property[0].valueCode = #R 
* #R85 ^property[1].code = #Relationships 
* #R85 ^property[1].valueString = "ICD-10 2017:C09.0~C09.1~C09.8~C09.9~C10.0~C10.1~C10.2~C10.3~C10.4~C10.8~C10.9~C11.0~C11.1~C11.2~C11.3~C11.8~C11.9~C12~C13.0~C13.1~C13.2~C13.8~C13.9~C14.0~C14.2~C30.0~C31.0~C31.1~C31.2~C31.3~C31.8~C31.9~C32.0~C32.1~C32.2~C32.3~C32.8~C32.9~C38.1~C38.2~C38.3~C38.4~C39.0~C39.8~C39.9~C45.0~C45.7" 
* #R85 ^property[2].code = #hints 
* #R85 ^property[2].valueString = "Beachte: nicht spezifizierte Neubildung der Atemwege R92 Hinweise: " 
* #R86 "Gutartige Neubildung Atemwege"
* #R86 ^definition = Inklusive:  Exklusive: unspezifizierte Neubildung der Atemwege R92~ Nasenpolyp R99 Kriterien: characteristisches klinisches oder histologisches Erscheinungsbild
* #R86 ^property[0].code = #parent 
* #R86 ^property[0].valueCode = #R 
* #R86 ^property[1].code = #Relationships 
* #R86 ^property[1].valueString = "ICD-10 2017:D14.0~D14.1~D14.2~D14.3~D14.4~D19.0" 
* #R86 ^property[2].code = #hints 
* #R86 ^property[2].valueString = "Beachte: nicht spezifizierte Neubildung der Atemwege R92 Hinweise: " 
* #R87 "Fremdkörper Nase/Larynx/Bronchus"
* #R87 ^definition = Inklusive: Fremdkörper in der Lunge Exklusive: Ertrinken A88~ Fremdkörper im Ösophagus D79~ Fremdkörper im Ohr H76~ Aspirationspneumonie R99 Kriterien: Fremdkörpernachweis direkt/endoskopisch/radiologisch
* #R87 ^property[0].code = #parent 
* #R87 ^property[0].valueCode = #R 
* #R87 ^property[1].code = #Relationships 
* #R87 ^property[1].valueString = "ICD-10 2017:T17.0~T17.1~T17.2~T17.3~T17.4~T17.5~T17.8~T17.9" 
* #R87 ^property[2].code = #hints 
* #R87 ^property[2].valueString = "Beachte: andere Beschwerden der Atemwege R29 Hinweise: " 
* #R88 "Verletzung Atemwege, andere"
* #R88 ^definition = Inklusive: Verletzung/Trauma von Nase/Atemwegen Exklusive: Ertrinken A88~ Nasenfraktur L76~ Fremdkörper in den Atemwegen R87 Kriterien:
* #R88 ^property[0].code = #parent 
* #R88 ^property[0].valueCode = #R 
* #R88 ^property[1].code = #Relationships 
* #R88 ^property[1].valueString = "ICD-10 2017:S00.3~S01.2~S03.1~S10.0~S17.0~S19.8~S27.0~S27.1~S27.2~S27.3~S27.4~S27.5~S27.6~S27.7~T27.0~T27.1~T27.2~T27.3~T27.4~T27.5~T27.6~T27.7~T70.1" 
* #R89 "Angeborene Anomalie Atemwege"
* #R89 ^definition = Inklusive: Angebohrene Fehlbildung von Nase/Pharynx/Trachea/Larynx/Bronchien/Lunge/Pleura Exklusive: Lippen-/Kiefer-/Gaumenspalte D81~ Zystische Fibrose T99 Kriterien:
* #R89 ^property[0].code = #parent 
* #R89 ^property[0].valueCode = #R 
* #R89 ^property[1].code = #Relationships 
* #R89 ^property[1].valueString = "ICD-10 2017:Q30.0~Q30.1~Q30.2~Q30.3~Q30.8~Q30.9~Q31.0~Q31.1~Q31.2~Q31.3~Q31.5~Q31.8~Q31.9~Q32.0~Q32.1~Q32.2~Q32.3~Q32.4~Q33.0~Q33.1~Q33.2~Q33.3~Q33.4~Q33.5~Q33.6~Q33.8~Q33.9~Q34.0~Q34.1~Q34.8~Q34.9" 
* #R90 "Hypertrophie der Gaumen-/Rachenmandeln"
* #R90 ^definition = Inklusive: chronische Tonsillitis Exklusive: akute Tonsillitis R76~ allergische Rhinitis R97 Kriterien:
* #R90 ^property[0].code = #parent 
* #R90 ^property[0].valueCode = #R 
* #R90 ^property[1].code = #Relationships 
* #R90 ^property[1].valueString = "ICD-10 2017:J34.8~J35.0~J35.1~J35.2~J35.3~J35.8~J35.9" 
* #R92 "Neubildung Atemwege nicht spezifiziert"
* #R92 ^definition = Inklusive: Neubildung der Atemwege~ nicht als gut- oder bösartig spezifiziert/ wenn Histologie nicht verfügbar Exklusive: sekundäre Neubildung unbekannter Lokalisation A79~ bösartige Neubildung der Atemwege R84~ R85~ gutartige Neubildung der Atemwege R86 Kriterien:
* #R92 ^property[0].code = #parent 
* #R92 ^property[0].valueCode = #R 
* #R92 ^property[1].code = #Relationships 
* #R92 ^property[1].valueString = "ICD-10 2017:D02.0~D02.1~D02.2~D02.3~D02.4~D38.0~D38.1~D38.2~D38.3~D38.4~D38.5~D38.6" 
* #R95 "COPD"
* #R95 ^definition = Inklusive: chronisch obstruktive Atemwege~ der Lunge (COPD)~ chronic airways limitation (CAL)~ Emphysem Exklusive: chronisch Bronchitis R79~ Asthma R96~ Bronchiektasie R99~ Zystische Fibrose T99 Kriterien: objektiver Nachweis eine Atemwegsobstruktion, nicht/nur teilweise durch Bronchodilatatoren aufhebbar
* #R95 ^property[0].code = #parent 
* #R95 ^property[0].valueCode = #R 
* #R95 ^property[1].code = #Relationships 
* #R95 ^property[1].valueString = "ICD-10 2017:J43.0~J43.1~J43.2~J43.8~J43.9~J44.09~J44.19~J44.89~J44.99" 
* #R95 ^property[2].code = #hints 
* #R95 ^property[2].valueString = "Beachte: andere Atmungsproblem R04 Hinweise: " 
* #R96 "Asthma"
* #R96 ^definition = Inklusive: Reaktive Atemwegserkrankung~ spastische Bronchitis Exklusive: Bronchiolitis R78~ chronische Bronchitis R79~ Emphysem R95 Kriterien: wiederholte Episoden reversibler akuter Bronchialobstruktion mit pfeifender Atmung und/oder trockenem Husten; oder diagnostische Tests, die die derzeit geltenden Kriterien für Asthma erfüllen
* #R96 ^property[0].code = #parent 
* #R96 ^property[0].valueCode = #R 
* #R96 ^property[1].code = #Relationships 
* #R96 ^property[1].valueString = "ICD-10 2017:J45.0~J45.1~J45.8~J45.9~J46" 
* #R96 ^property[2].code = #hints 
* #R96 ^property[2].valueString = "Beachte: Keuchen R03~ Husten R05 Hinweise: " 
* #R97 "Heuschnupfen"
* #R97 ^definition = Inklusive: Heuschnupfen~ Rhinits allergica~ vasomotorische Rhinitis Exklusive: Infektion der oberen Atemwege R74~ chronische Rhinitis NNB R83 Kriterien:
* #R97 ^property[0].code = #parent 
* #R97 ^property[0].valueCode = #R 
* #R97 ^property[1].code = #Relationships 
* #R97 ^property[1].valueString = "ICD-10 2017:J30.0~J30.1~J30.2~J30.3~J30.4" 
* #R98 "Hyperventilationssyndrom"
* #R98 ^definition = Inklusive:  Exklusive:  Kriterien: Symptome, die auf Hyperventilation zurückführbaqr sind und durch Rückatmung ausgeatmeter Luft gebessert werden
* #R98 ^property[0].code = #parent 
* #R98 ^property[0].valueCode = #R 
* #R98 ^property[1].code = #Relationships 
* #R98 ^property[1].valueString = "ICD-10 2017:R06.4" 
* #R98 ^property[2].code = #hints 
* #R98 ^property[2].valueString = "Beachte: andere Atmungsproblem R04 Hinweise: " 
* #R99 "Andere Atemwegserkrankung"
* #R99 ^definition = Inklusive: Aspirationspneumonie~ Bronchiektasie~ Nasenscheidewandverkrümmung~ Lungenkomplikation aufgrund anderer Erkrankung~ Mediastinalerkrankung~ nasale Polypen~ sonstige Kehlkopferkrankungen~ Pneumokoniose~ Pneumothorax~ Pneumonitis durch Allergie/Chemikalien/Staub/Dämpfe/Schimmel~ Atelektase~ respiratorische Insuffizienz Exklusive:  Kriterien:
* #R99 ^property[0].code = #parent 
* #R99 ^property[0].valueCode = #R 
* #R99 ^property[1].code = #Relationships 
* #R99 ^property[1].valueString = "ICD-10 2017:J33.0~J33.1~J33.8~J33.9~J34.1~J34.2~J34.3~J34.8~J38.0~J38.1~J38.2~J38.3~J38.4~J38.5~J38.6~J38.7~J39.0~J39.1~J39.2~J39.3~J39.8~J39.9~J47~J60~J61~J62.0~J62.8~J63.0~J63.1~J63.2~J63.3~J63.4~J63.5~J63.8~J64~J65~J66.0~J66.1~J66.2~J66.8~J67.0~J67.1~J67.2~J67.3~J67.4~J67.5~J67.6~J67.7~J67.8~J67.9~J68.0~J68.1~J68.2~J68.3~J68.4~J68.8~J68.9~J69.0~J69.1~J69.8~J80~J81~J82~J84.0~J84.1~J84.8~J84.9~J92.0~J92.9~J93.0~J93.1~J93.8~J93.9~J96.0~J96.1~J96.9~J98.0~J98.1~J98.2~J98.3~J98.4~J98.5~J98.6~J98.8~J98.9~J99.0~J99.1~J99.8~U04.9~Z90.2" 
* #S "Haut"
* #S ^property[0].code = #child 
* #S ^property[0].valueCode = #S01 
* #S ^property[1].code = #child 
* #S ^property[1].valueCode = #S02 
* #S ^property[2].code = #child 
* #S ^property[2].valueCode = #S03 
* #S ^property[3].code = #child 
* #S ^property[3].valueCode = #S04 
* #S ^property[4].code = #child 
* #S ^property[4].valueCode = #S05 
* #S ^property[5].code = #child 
* #S ^property[5].valueCode = #S06 
* #S ^property[6].code = #child 
* #S ^property[6].valueCode = #S07 
* #S ^property[7].code = #child 
* #S ^property[7].valueCode = #S08 
* #S ^property[8].code = #child 
* #S ^property[8].valueCode = #S09 
* #S ^property[9].code = #child 
* #S ^property[9].valueCode = #S10 
* #S ^property[10].code = #child 
* #S ^property[10].valueCode = #S11 
* #S ^property[11].code = #child 
* #S ^property[11].valueCode = #S12 
* #S ^property[12].code = #child 
* #S ^property[12].valueCode = #S13 
* #S ^property[13].code = #child 
* #S ^property[13].valueCode = #S14 
* #S ^property[14].code = #child 
* #S ^property[14].valueCode = #S15 
* #S ^property[15].code = #child 
* #S ^property[15].valueCode = #S16 
* #S ^property[16].code = #child 
* #S ^property[16].valueCode = #S17 
* #S ^property[17].code = #child 
* #S ^property[17].valueCode = #S18 
* #S ^property[18].code = #child 
* #S ^property[18].valueCode = #S19 
* #S ^property[19].code = #child 
* #S ^property[19].valueCode = #S20 
* #S ^property[20].code = #child 
* #S ^property[20].valueCode = #S21 
* #S ^property[21].code = #child 
* #S ^property[21].valueCode = #S22 
* #S ^property[22].code = #child 
* #S ^property[22].valueCode = #S23 
* #S ^property[23].code = #child 
* #S ^property[23].valueCode = #S24 
* #S ^property[24].code = #child 
* #S ^property[24].valueCode = #S26 
* #S ^property[25].code = #child 
* #S ^property[25].valueCode = #S27 
* #S ^property[26].code = #child 
* #S ^property[26].valueCode = #S28 
* #S ^property[27].code = #child 
* #S ^property[27].valueCode = #S29 
* #S ^property[28].code = #child 
* #S ^property[28].valueCode = #S30 
* #S ^property[29].code = #child 
* #S ^property[29].valueCode = #S31 
* #S ^property[30].code = #child 
* #S ^property[30].valueCode = #S32 
* #S ^property[31].code = #child 
* #S ^property[31].valueCode = #S33 
* #S ^property[32].code = #child 
* #S ^property[32].valueCode = #S34 
* #S ^property[33].code = #child 
* #S ^property[33].valueCode = #S35 
* #S ^property[34].code = #child 
* #S ^property[34].valueCode = #S36 
* #S ^property[35].code = #child 
* #S ^property[35].valueCode = #S37 
* #S ^property[36].code = #child 
* #S ^property[36].valueCode = #S38 
* #S ^property[37].code = #child 
* #S ^property[37].valueCode = #S39 
* #S ^property[38].code = #child 
* #S ^property[38].valueCode = #S40 
* #S ^property[39].code = #child 
* #S ^property[39].valueCode = #S41 
* #S ^property[40].code = #child 
* #S ^property[40].valueCode = #S42 
* #S ^property[41].code = #child 
* #S ^property[41].valueCode = #S43 
* #S ^property[42].code = #child 
* #S ^property[42].valueCode = #S44 
* #S ^property[43].code = #child 
* #S ^property[43].valueCode = #S45 
* #S ^property[44].code = #child 
* #S ^property[44].valueCode = #S46 
* #S ^property[45].code = #child 
* #S ^property[45].valueCode = #S47 
* #S ^property[46].code = #child 
* #S ^property[46].valueCode = #S48 
* #S ^property[47].code = #child 
* #S ^property[47].valueCode = #S49 
* #S ^property[48].code = #child 
* #S ^property[48].valueCode = #S50 
* #S ^property[49].code = #child 
* #S ^property[49].valueCode = #S51 
* #S ^property[50].code = #child 
* #S ^property[50].valueCode = #S52 
* #S ^property[51].code = #child 
* #S ^property[51].valueCode = #S53 
* #S ^property[52].code = #child 
* #S ^property[52].valueCode = #S54 
* #S ^property[53].code = #child 
* #S ^property[53].valueCode = #S55 
* #S ^property[54].code = #child 
* #S ^property[54].valueCode = #S56 
* #S ^property[55].code = #child 
* #S ^property[55].valueCode = #S57 
* #S ^property[56].code = #child 
* #S ^property[56].valueCode = #S58 
* #S ^property[57].code = #child 
* #S ^property[57].valueCode = #S59 
* #S ^property[58].code = #child 
* #S ^property[58].valueCode = #S60 
* #S ^property[59].code = #child 
* #S ^property[59].valueCode = #S61 
* #S ^property[60].code = #child 
* #S ^property[60].valueCode = #S62 
* #S ^property[61].code = #child 
* #S ^property[61].valueCode = #S63 
* #S ^property[62].code = #child 
* #S ^property[62].valueCode = #S64 
* #S ^property[63].code = #child 
* #S ^property[63].valueCode = #S65 
* #S ^property[64].code = #child 
* #S ^property[64].valueCode = #S66 
* #S ^property[65].code = #child 
* #S ^property[65].valueCode = #S67 
* #S ^property[66].code = #child 
* #S ^property[66].valueCode = #S68 
* #S ^property[67].code = #child 
* #S ^property[67].valueCode = #S69 
* #S ^property[68].code = #child 
* #S ^property[68].valueCode = #S70 
* #S ^property[69].code = #child 
* #S ^property[69].valueCode = #S71 
* #S ^property[70].code = #child 
* #S ^property[70].valueCode = #S72 
* #S ^property[71].code = #child 
* #S ^property[71].valueCode = #S73 
* #S ^property[72].code = #child 
* #S ^property[72].valueCode = #S74 
* #S ^property[73].code = #child 
* #S ^property[73].valueCode = #S75 
* #S ^property[74].code = #child 
* #S ^property[74].valueCode = #S76 
* #S ^property[75].code = #child 
* #S ^property[75].valueCode = #S77 
* #S ^property[76].code = #child 
* #S ^property[76].valueCode = #S78 
* #S ^property[77].code = #child 
* #S ^property[77].valueCode = #S79 
* #S ^property[78].code = #child 
* #S ^property[78].valueCode = #S80 
* #S ^property[79].code = #child 
* #S ^property[79].valueCode = #S81 
* #S ^property[80].code = #child 
* #S ^property[80].valueCode = #S82 
* #S ^property[81].code = #child 
* #S ^property[81].valueCode = #S83 
* #S ^property[82].code = #child 
* #S ^property[82].valueCode = #S84 
* #S ^property[83].code = #child 
* #S ^property[83].valueCode = #S85 
* #S ^property[84].code = #child 
* #S ^property[84].valueCode = #S86 
* #S ^property[85].code = #child 
* #S ^property[85].valueCode = #S87 
* #S ^property[86].code = #child 
* #S ^property[86].valueCode = #S88 
* #S ^property[87].code = #child 
* #S ^property[87].valueCode = #S89 
* #S ^property[88].code = #child 
* #S ^property[88].valueCode = #S90 
* #S ^property[89].code = #child 
* #S ^property[89].valueCode = #S91 
* #S ^property[90].code = #child 
* #S ^property[90].valueCode = #S92 
* #S ^property[91].code = #child 
* #S ^property[91].valueCode = #S93 
* #S ^property[92].code = #child 
* #S ^property[92].valueCode = #S94 
* #S ^property[93].code = #child 
* #S ^property[93].valueCode = #S95 
* #S ^property[94].code = #child 
* #S ^property[94].valueCode = #S96 
* #S ^property[95].code = #child 
* #S ^property[95].valueCode = #S97 
* #S ^property[96].code = #child 
* #S ^property[96].valueCode = #S98 
* #S ^property[97].code = #child 
* #S ^property[97].valueCode = #S99 
* #S01 "Schmerz/Überempfindlicheit der Haut"
* #S01 ^definition = Inklusive: Brennen~ schmerzhafte Läsion~ Ausschlag Exklusive: Kribbelgefühl der Finger/Füße/Zehen N05~ andere Empfindungsstörungen N06 Kriterien:
* #S01 ^property[0].code = #parent 
* #S01 ^property[0].valueCode = #S 
* #S01 ^property[1].code = #Relationships 
* #S01 ^property[1].valueString = "ICD-10 2017:R20.8" 
* #S02 "Juckreiz"
* #S02 ^definition = Inklusive: Hautreizung Exklusive: anogenitaler Juckreiz D05~ Dermatitis artefacta S99~ Juckreiz der Vulva X16~ Juckreiz der Brustwarzen X20 Kriterien:
* #S02 ^property[0].code = #parent 
* #S02 ^property[0].valueCode = #S 
* #S02 ^property[1].code = #Relationships 
* #S02 ^property[1].valueString = "ICD-10 2017:L29.8~L29.9" 
* #S03 "Warzen"
* #S03 ^definition = Inklusive: Verrucae Exklusive: Molluscum contagiosum S95~ Genitalwarzen X91~ Y76 Kriterien:
* #S03 ^property[0].code = #parent 
* #S03 ^property[0].valueCode = #S 
* #S03 ^property[1].code = #Relationships 
* #S03 ^property[1].valueString = "ICD-10 2017:B07" 
* #S04 "Papel/Hautschwellung, lokalisiert"
* #S04 ^definition = Inklusive: Papel Exklusive: Insektenbiss S12~ Knoten in der Brust X19~ Y16 Kriterien:
* #S04 ^property[0].code = #parent 
* #S04 ^property[0].valueCode = #S 
* #S04 ^property[1].code = #Relationships 
* #S04 ^property[1].valueString = "ICD-10 2017:R22.0~R22.1~R22.2~R22.3~R22.4~R22.9~R23.8" 
* #S05 "Papel/Hautschwellung, generalisiert"
* #S05 ^definition = Inklusive: Papeln/Knoten/Schwellungen an mehreren Stellen Exklusive: Knöchelschwellung/-ödem K07 Kriterien:
* #S05 ^property[0].code = #parent 
* #S05 ^property[0].valueCode = #S 
* #S05 ^property[1].code = #Relationships 
* #S05 ^property[1].valueString = "ICD-10 2017:R22.7~R23.8" 
* #S06 "Rötung/Ausschlag, lokalisiert"
* #S06 ^definition = Inklusive: Flecken~ Erythem~ Rötung Exklusive: umschriebene Knoten/Schwellung S04 Kriterien:
* #S06 ^property[0].code = #parent 
* #S06 ^property[0].valueCode = #S 
* #S06 ^property[1].code = #Relationships 
* #S06 ^property[1].valueString = "ICD-10 2017:L53.9~R21" 
* #S07 "Rötung/Ausschlag, generalisiert"
* #S07 ^definition = Inklusive: Flecken~ Erythem~ Rötung an mehreren Stellen Exklusive: andere virale Exantheme A76~ allgemeine Knoten/Schwellungen der Haut S05 Kriterien:
* #S07 ^property[0].code = #parent 
* #S07 ^property[0].valueCode = #S 
* #S07 ^property[1].code = #Relationships 
* #S07 ^property[1].valueString = "ICD-10 2017:L53.9~R21" 
* #S08 "Hautfarbe verändert"
* #S08 ^definition = Inklusive: Augenringe"~ Zyanose~ Erröten~ Sommersprossen~ Blässe~ Pigmentstörung Exklusive: Kontusion/Hämatom S16~ Vitiligo S99 Kriterien:
* #S08 ^property[0].code = #parent 
* #S08 ^property[0].valueCode = #S 
* #S08 ^property[1].code = #Relationships 
* #S08 ^property[1].valueString = "ICD-10 2017:L81.0~L81.1~L81.2~L81.3~R23.0~R23.1~R23.2" 
* #S09 "Infizierter Finger/Zeh"
* #S09 ^definition = Inklusive: Paronychie Exklusive: posttraumatische Infektion der Finger/Zehen S11~ Tinea S74~ Monilien/Candida-Erkrankungen S75 Kriterien:
* #S09 ^property[0].code = #parent 
* #S09 ^property[0].valueCode = #S 
* #S09 ^property[1].code = #Relationships 
* #S09 ^property[1].valueString = "ICD-10 2017:L03.0" 
* #S10 "Furunkel/Karbunkel"
* #S10 ^definition = Inklusive: Abszess~ Follikulitis~ Furunkel Exklusive: Lymphadenitis B70~ perianaler Furunkel D95~ äußerer Gehörgang H70~ Furunkel an der Nase R73~ infizierter Finger/Zeh S09~ Wundinfektion S11~ Erysipel S76~ Pilonidalabszess S85~ Schweißdrüsenentzündung S92~ Furunkel am weiblichen äußeren Genitale X99~ Furunkel am männlichen äußeren Genitale Y99 Kriterien:
* #S10 ^property[0].code = #parent 
* #S10 ^property[0].valueCode = #S 
* #S10 ^property[1].code = #Relationships 
* #S10 ^property[1].valueString = "ICD-10 2017:L02.0~L02.1~L02.2~L02.3~L02.4~L02.8~L02.9" 
* #S11 "Hautinfektion, posttraumatisch"
* #S11 ^definition = Inklusive: posttraumatische Wund-/Bißinfektion Exklusive: chirurgische Wundinfektion A87~ Erysipel~ Pyodermie S76~ Impetigo S84 Kriterien:
* #S11 ^property[0].code = #parent 
* #S11 ^property[0].valueCode = #S 
* #S11 ^property[1].code = #Relationships 
* #S11 ^property[1].valueString = "ICD-10 2017:T79.3" 
* #S12 "Insektenbiss/-stich"
* #S12 ^definition = Inklusive:  Exklusive: toxische Auswirkung nichtmedikamentöser Substanzen A86~ infizierte Bißwunde S11~ Krätze S72~ Pediculose S73 Kriterien:
* #S12 ^property[0].code = #parent 
* #S12 ^property[0].valueCode = #S 
* #S12 ^property[1].code = #Relationships 
* #S12 ^property[1].valueString = "ICD-10 2017:S00.0~S00.2~S00.3~S00.4~S00.5~S00.7~S00.8~S00.9~S10.1~S10.7~S10.8~S10.9~S20.1~S20.3~S20.4~S20.7~S20.8~S30.7~S30.8~S30.9~S40.7~S40.8~S50.7~S50.8~S60.7~S60.8~S70.7~S70.8~S80.7~S80.8~S90.7~S90.8~T09.0~T11.0~T13.0~T14.0" 
* #S13 "Tier-/Menschenbiss"
* #S13 ^definition = Inklusive:  Exklusive: toxische Auswirkung nichtmedikamentöser Substanzen A86~ infizierte Bißwunde S11 Kriterien:
* #S13 ^property[0].code = #parent 
* #S13 ^property[0].valueCode = #S 
* #S13 ^property[1].code = #Relationships 
* #S13 ^property[1].valueString = "ICD-10 2017:T14.1" 
* #S14 "Verbrennung/Verbrühung"
* #S14 ^definition = Inklusive: Verbrennung/Verbrühung alle Grade~ äußere chemische Verbrennung Exklusive: Sonnenbrand S80 Kriterien:
* #S14 ^property[0].code = #parent 
* #S14 ^property[0].valueCode = #S 
* #S14 ^property[1].code = #Relationships 
* #S14 ^property[1].valueString = "ICD-10 2017:T20.0~T20.1~T20.2~T20.3~T20.4~T20.5~T20.6~T20.7~T21.0~T21.1~T21.2~T21.3~T21.4~T21.5~T21.6~T21.7~T22.0~T22.1~T22.2~T22.3~T22.4~T22.5~T22.6~T22.7~T23.0~T23.1~T23.2~T23.3~T23.4~T23.5~T23.6~T23.7~T24.0~T24.1~T24.2~T24.3~T24.4~T24.5~T24.6~T24.7~T25.0~T25.1~T25.2~T25.3~T25.4~T25.5~T25.6~T25.7~T30.0~T30.1~T30.2~T30.3~T30.4~T30.5~T30.6~T30.7~T31.0~T31.1~T31.2~T31.3~T31.4~T31.5~T31.6~T31.7~T31.8~T31.9~T32.0~T32.1~T32.2~T32.3~T32.4~T32.5~T32.6~T32.7~T32.8~T32.9" 
* #S15 "Fremdkörper in der Haut"
* #S15 ^definition = Inklusive: Fremdkörper unter dem Nagel Exklusive:  Kriterien:
* #S15 ^property[0].code = #parent 
* #S15 ^property[0].valueCode = #S 
* #S15 ^property[1].code = #Relationships 
* #S15 ^property[1].valueString = "ICD-10 2017:S00.0~S00.2~S00.3~S00.4~S00.5~S00.7~S00.8~S00.9~S10.1~S10.7~S10.8~S10.9~S20.1~S20.3~S20.4~S20.7~S20.8~S30.7~S30.8~S30.9~S40.7~S40.8~S50.7~S50.8~S60.7~S60.8~S70.7~S70.8~S80.7~S80.8~S90.7~S90.8~T09.0~T11.0~T13.0~T14.0~T14.1" 
* #S16 "Prellung/Kontusion"
* #S16 ^definition = Inklusive: Ekchymose~ Hämatom Exklusive: Quetschung/Hämatom mit Hautriss S17 Kriterien: Prellung/Quetschung bei intakter Hautoberfläche
* #S16 ^property[0].code = #parent 
* #S16 ^property[0].valueCode = #S 
* #S16 ^property[1].code = #Relationships 
* #S16 ^property[1].valueString = "ICD-10 2017:S00.0~S00.7~S00.8~S00.9~S10.1~S10.7~S10.8~S10.9~S20.0~S20.1~S20.2~S20.3~S20.4~S20.7~S20.8~S30.0~S30.1~S40.0~S40.7~S40.8~S40.9~S50.0~S50.1~S50.7~S50.8~S50.9~S60.0~S60.1~S60.2~S60.7~S60.8~S60.9~S70.0~S70.1~S70.7~S70.8~S70.9~S80.0~S80.1~S80.7~S80.8~S80.9~S90.0~S90.1~S90.2~S90.3~S90.7~S90.8~S90.9~T09.0~T11.0~T13.0~T14.0" 
* #S17 "Abschürfung/Kratzer/Blase"
* #S17 ^definition = Inklusive: Quetschung mit Hauteinriß~ Schürfwunde/Schramme Exklusive:  Kriterien:
* #S17 ^property[0].code = #parent 
* #S17 ^property[0].valueCode = #S 
* #S17 ^property[1].code = #Relationships 
* #S17 ^property[1].valueString = "ICD-10 2017:S00.0~S00.7~S00.8~S00.9~S10.1~S10.7~S10.8~S10.9~S20.1~S20.3~S20.4~S20.7~S20.8~S30.7~S30.8~S30.9~S40.7~S40.8~S40.9~S50.7~S50.8~S50.9~S60.7~S60.8~S60.9~S70.7~S70.8~S70.9~S80.7~S80.8~S80.9~S90.3~S90.7~S90.8~S90.9~T09.0~T11.0~T13.0~T14.0" 
* #S18 "Risswunde/Schnittverletzung"
* #S18 ^definition = Inklusive: Lazeration/ Schnitt in Haut/ Unterhautgewebe Exklusive: Bißwunde S13~ Hämatom mit Hautriß S17 Kriterien:
* #S18 ^property[0].code = #parent 
* #S18 ^property[0].valueCode = #S 
* #S18 ^property[1].code = #Relationships 
* #S18 ^property[1].valueString = "ICD-10 2017:S01.0~S01.2~S01.4~S01.7~S01.8~S01.9~S11.0~S11.1~S11.2~S11.7~S11.8~S11.9~S21.0~S21.1~S21.2~S21.7~S21.8~S21.9~S31.0~S31.1~S31.8~S41.0~S41.1~S41.7~S41.8~S51.0~S51.7~S51.8~S51.9~S61.0~S61.1~S61.7~S61.8~S61.9~S71.0~S71.1~S71.7~S71.8~S81.0~S81.7~S81.8~S81.9~S91.0~S91.1~S91.2~S91.3~S91.7~T09.1~T11.1~T13.1~T14.1" 
* #S19 "Hautverletzung, andere"
* #S19 ^definition = Inklusive: Nagelausriß~ Nadelstich~ Einstich Exklusive: Bißwunde S13 Kriterien:
* #S19 ^property[0].code = #parent 
* #S19 ^property[0].valueCode = #S 
* #S19 ^property[1].code = #Relationships 
* #S19 ^property[1].valueString = "ICD-10 2017:S00.0~S00.7~S00.8~S00.9~S10.1~S10.1~S10.7~S10.7~S10.8~S10.8~S10.9~S10.9~S20.1~S20.3~S20.4~S20.7~S20.8~S30.7~S30.8~S30.9~S40.7~S40.8~S40.9~S50.7~S50.8~S50.9~S60.7~S60.8~S60.9~S70.7~S70.8~S70.9~S80.7~S80.8~S80.9~S90.7~S90.8~S90.9~T09.0~T11.0~T13.0~T14.0" 
* #S20 "Verhornung/Schwielenbildung"
* #S20 ^definition = Inklusive:  Exklusive: Hyperkeratose S80 Kriterien:
* #S20 ^property[0].code = #parent 
* #S20 ^property[0].valueCode = #S 
* #S20 ^property[1].code = #Relationships 
* #S20 ^property[1].valueString = "ICD-10 2017:L84" 
* #S21 "Hautbeschaffenheitssymptome/-beschwerden"
* #S21 ^definition = Inklusive: trockene Haut~ Epidermolyse~ Hautschuppung~ Falten Exklusive: Schweißprobleme A09~ Kopfhautsymptome/-beschwerden S24~ Ichthyose S83~ Schweißdrüsenerkrankung S92~ Symptome/Beschwerden an der Vulva X10 Kriterien:
* #S21 ^property[0].code = #parent 
* #S21 ^property[0].valueCode = #S 
* #S21 ^property[1].code = #Relationships 
* #S21 ^property[1].valueString = "ICD-10 2017:R23.4" 
* #S22 "Nagelsymptome/-beschwerden"
* #S22 ^definition = Inklusive: Uhrglasnägel Exklusive: Paronychie S09~ eingewachsener Nagel S94 Kriterien:
* #S22 ^property[0].code = #parent 
* #S22 ^property[0].valueCode = #S 
* #S22 ^property[1].code = #Relationships 
* #S22 ^property[1].valueString = "ICD-10 2017:L60.1~L60.4~L60.5~L60.9~L62.0~L62.8~R68.3" 
* #S23 "Haarausfall/Kahlheit"
* #S23 ^definition = Inklusive: Alopezie Exklusive:  Kriterien:
* #S23 ^property[0].code = #parent 
* #S23 ^property[0].valueCode = #S 
* #S23 ^property[1].code = #Relationships 
* #S23 ^property[1].valueString = "ICD-10 2017:L63.0~L63.1~L63.2~L63.8~L63.9~L64.0~L64.8~L64.9~L65.0~L65.1~L65.2~L65.8~L65.9~L66.0~L66.1~L66.2~L66.3~L66.4~L66.8~L66.9" 
* #S24 "Haar-/Kopfhautsymptome/-beschwerden, andere"
* #S24 ^definition = Inklusive: trockene Kopfhaut~ Hirsutismus Exklusive: Trichotillomanie P29~ Follikulitis S10~ Haarausfall/ Kahlheit S23~ Schuppen S86 Kriterien:
* #S24 ^property[0].code = #parent 
* #S24 ^property[0].valueCode = #S 
* #S24 ^property[1].code = #Relationships 
* #S24 ^property[1].valueString = "ICD-10 2017:L67.0~L67.1~L67.8~L67.9~L68.0~L68.1~L68.2~L68.3~L68.8~L68.9" 
* #S26 "Angst vor Hautkrebs"
* #S26 ^definition = Inklusive:  Exklusive: wenn der Patient die Erkrankung hat~ kodiere die Erkrankung Kriterien: Besorgnis über/Angst vor Hautkrebs bei einem Patienten, der die Krankheit nicht hat/bevor die Diagnose gesichert ist
* #S26 ^property[0].code = #parent 
* #S26 ^property[0].valueCode = #S 
* #S26 ^property[1].code = #Relationships 
* #S26 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #S27 "Angst vor Hauterkrankung, andere"
* #S27 ^definition = Inklusive:  Exklusive: Angst vor Hautkrebs S26~ wenn der Patient die Erkrankung hat~ ist die Erkrankung zu kodieren Kriterien: Besorgnis über/Angst vor einer anderen Hautkrankheit bei einem Patienten, der die Krankheit nicht hat/bevor die Diagnose gesichert ist
* #S27 ^property[0].code = #parent 
* #S27 ^property[0].valueCode = #S 
* #S27 ^property[1].code = #Relationships 
* #S27 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #S28 "Funktionseinschränkung/Behinderung (S)"
* #S28 ^definition = Inklusive:  Exklusive:  Kriterien: Funktionseinschränkung/Behinderung durch eine Hauterkrankung
* #S28 ^property[0].code = #parent 
* #S28 ^property[0].valueCode = #S 
* #S28 ^property[1].code = #Relationships 
* #S28 ^property[1].valueString = "ICD-10 2017:Z73.6" 
* #S28 ^property[2].code = #hints 
* #S28 ^property[2].valueString = "Beachte:  Hinweise: COOP/WONCA Charts eignen sich zur Dokumentation des funktionellen Status des Patienten (siehe Kapitel 8)" 
* #S29 "Andere Hautsymptome/-beschwerden"
* #S29 ^definition = Inklusive: Cellulitis~ Hautblutung~ Petechien~ Nagelprobleme~ Hautverletzung~ wunde Stelle(n) Exklusive: Narbe S99 Kriterien:
* #S29 ^property[0].code = #parent 
* #S29 ^property[0].valueCode = #S 
* #S29 ^property[1].code = #Relationships 
* #S29 ^property[1].valueString = "ICD-10 2017:R23.3~R23.8" 
* #S30 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? vollständig"
* #S30 ^property[0].code = #parent 
* #S30 ^property[0].valueCode = #S 
* #S31 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? teilweise"
* #S31 ^property[0].code = #parent 
* #S31 ^property[0].valueCode = #S 
* #S32 "Allergie-/Sensitivitätstestung"
* #S32 ^property[0].code = #parent 
* #S32 ^property[0].valueCode = #S 
* #S33 "Mikrobiologische/immunologische Untersuchung"
* #S33 ^property[0].code = #parent 
* #S33 ^property[0].valueCode = #S 
* #S34 "Blutuntersuchung"
* #S34 ^property[0].code = #parent 
* #S34 ^property[0].valueCode = #S 
* #S35 "Urinuntersuchung"
* #S35 ^property[0].code = #parent 
* #S35 ^property[0].valueCode = #S 
* #S36 "Stuhluntersuchung"
* #S36 ^property[0].code = #parent 
* #S36 ^property[0].valueCode = #S 
* #S37 "Histologische Untersuchung/zytologischer Abstrich"
* #S37 ^property[0].code = #parent 
* #S37 ^property[0].valueCode = #S 
* #S38 "Andere Laboruntersuchung NAK"
* #S38 ^property[0].code = #parent 
* #S38 ^property[0].valueCode = #S 
* #S39 "Körperliche Funktionsprüfung"
* #S39 ^property[0].code = #parent 
* #S39 ^property[0].valueCode = #S 
* #S40 "Diagnostische Endoskopie"
* #S40 ^property[0].code = #parent 
* #S40 ^property[0].valueCode = #S 
* #S41 "Diagnostisches Röntgen/Bildgebung"
* #S41 ^property[0].code = #parent 
* #S41 ^property[0].valueCode = #S 
* #S42 "Elektrische Aufzeichnungsverfahren"
* #S42 ^property[0].code = #parent 
* #S42 ^property[0].valueCode = #S 
* #S43 "Andere diagnostische Untersuchung"
* #S43 ^property[0].code = #parent 
* #S43 ^property[0].valueCode = #S 
* #S44 "Präventive Impfung/Medikation"
* #S44 ^property[0].code = #parent 
* #S44 ^property[0].valueCode = #S 
* #S45 "Beobachtung/Gesundheitsschulung/Beratung/Diät"
* #S45 ^property[0].code = #parent 
* #S45 ^property[0].valueCode = #S 
* #S46 "Konsultation eines anderen Primärversorgers"
* #S46 ^property[0].code = #parent 
* #S46 ^property[0].valueCode = #S 
* #S47 "Konsultation eines Facharztes"
* #S47 ^property[0].code = #parent 
* #S47 ^property[0].valueCode = #S 
* #S48 "Klärung/Besprechung des Beratungsanlasses mit dem Patienten"
* #S48 ^property[0].code = #parent 
* #S48 ^property[0].valueCode = #S 
* #S49 "Andere Vorsorgemaßnahme"
* #S49 ^property[0].code = #parent 
* #S49 ^property[0].valueCode = #S 
* #S50 "Medikation/Verschreibung/Erneuerung/Injektion"
* #S50 ^property[0].code = #parent 
* #S50 ^property[0].valueCode = #S 
* #S51 "Inzision/Drainage/Spülung/Absaugung/Entfernung von Körperflüssigkeit"
* #S51 ^property[0].code = #parent 
* #S51 ^property[0].valueCode = #S 
* #S52 "Exzision/Entfernung von Gewebe/Biopsie/Destruktion/Debridement/Verkauterung"
* #S52 ^property[0].code = #parent 
* #S52 ^property[0].valueCode = #S 
* #S53 "Instrumentelle Manipulation/Katheterisierung/Intubation/Dilatation"
* #S53 ^property[0].code = #parent 
* #S53 ^property[0].valueCode = #S 
* #S54 "Reposition/Fixierung/Naht/Gips/prothetische Versorgung"
* #S54 ^property[0].code = #parent 
* #S54 ^property[0].valueCode = #S 
* #S55 "Lokale Injektion/Infiltration"
* #S55 ^property[0].code = #parent 
* #S55 ^property[0].valueCode = #S 
* #S56 "Verband/Druck/Kompression/Tamponade"
* #S56 ^property[0].code = #parent 
* #S56 ^property[0].valueCode = #S 
* #S57 "Physikalische Therapie/Rehabilitation"
* #S57 ^property[0].code = #parent 
* #S57 ^property[0].valueCode = #S 
* #S58 "Therapeutische Beratung/Zuhören"
* #S58 ^property[0].code = #parent 
* #S58 ^property[0].valueCode = #S 
* #S59 "Andere therapeutische Maßnahme/kleine Chirurgie NAK"
* #S59 ^property[0].code = #parent 
* #S59 ^property[0].valueCode = #S 
* #S60 "Testresultat/Ergebnis eigene Maßnahme"
* #S60 ^property[0].code = #parent 
* #S60 ^property[0].valueCode = #S 
* #S61 "Ergebnis Untersuchung/Test/Brief von anderem Leistungserbringer"
* #S61 ^property[0].code = #parent 
* #S61 ^property[0].valueCode = #S 
* #S62 "Adminstrative Maßnahme"
* #S62 ^property[0].code = #parent 
* #S62 ^property[0].valueCode = #S 
* #S63 "Folgekonsultation unspezifiziert"
* #S63 ^property[0].code = #parent 
* #S63 ^property[0].valueCode = #S 
* #S64 "Konsultation/Problembehandlung auf Initiative des Leistungserbringers"
* #S64 ^property[0].code = #parent 
* #S64 ^property[0].valueCode = #S 
* #S65 "Konsultation/Problembehandlung auf Initiative Dritter"
* #S65 ^property[0].code = #parent 
* #S65 ^property[0].valueCode = #S 
* #S66 "Überweisung an andere/-n Leistungserbringer (nicht ärztl.)/Pflegekraft/Therapeuten/Sozialarbeiter"
* #S66 ^property[0].code = #parent 
* #S66 ^property[0].valueCode = #S 
* #S67 "Überweisung an einen Arzt/Spezialisten/eine Klinik/ein Krankenhaus"
* #S67 ^property[0].code = #parent 
* #S67 ^property[0].valueCode = #S 
* #S68 "Andere Überweisung NAK"
* #S68 ^property[0].code = #parent 
* #S68 ^property[0].valueCode = #S 
* #S69 "Anderer Beratungsanlass NAK"
* #S69 ^property[0].code = #parent 
* #S69 ^property[0].valueCode = #S 
* #S70 "Herpes zoster"
* #S70 ^definition = Inklusive: Post-zoster Neuralgie~ Gürtelrose~ Zoster ophthalmicus Exklusive:  Kriterien: gruppierte Bläschen mit halbseitiger Ausbreitung über dem Bereich eines einzelnen Dermatoms
* #S70 ^property[0].code = #parent 
* #S70 ^property[0].valueCode = #S 
* #S70 ^property[1].code = #Relationships 
* #S70 ^property[1].valueString = "ICD-10 2017:B02.0~B02.1~B02.2~B02.3~B02.7~B02.8~B02.9" 
* #S70 ^property[2].code = #hints 
* #S70 ^property[2].valueString = "Beachte: Hautschmerz S01~ Ausschlag S06 Hinweise: " 
* #S71 "Herpes simplex"
* #S71 ^definition = Inklusive: Herpes labialis~ Fieberblase Exklusive: Herpes simplex am Auge ohne Hornhautulkus F73~ Genitalinfektion X90~ Y72 Kriterien: lokalisierte Bläschen mit geröteter Basis plus Vorgeschichte ähnlicher Läsionen, oder serologischer bzw. virologischer Nachweis
* #S71 ^property[0].code = #parent 
* #S71 ^property[0].valueCode = #S 
* #S71 ^property[1].code = #Relationships 
* #S71 ^property[1].valueString = "ICD-10 2017:B00.0~B00.1~B00.8~B00.9" 
* #S71 ^property[2].code = #hints 
* #S71 ^property[2].valueString = "Beachte: Ausschlag S06 Hinweise: " 
* #S72 "Krätze/andere Askariasis"
* #S72 ^definition = Inklusive:  Exklusive:  Kriterien: intensiv juckende Hautläsionen plus Milbengänge an den Seiten der Handflächen, der Finger, des Penis oder in Hautfalten, oder Nachweis von Parasiten oder Eiern in den Läsionen
* #S72 ^property[0].code = #parent 
* #S72 ^property[0].valueCode = #S 
* #S72 ^property[1].code = #Relationships 
* #S72 ^property[1].valueString = "ICD-10 2017:B86~B88.0~B88.2" 
* #S72 ^property[2].code = #hints 
* #S72 ^property[2].valueString = "Beachte: Pruritus S02 Hinweise: " 
* #S73 "Pedikulose/Hautbefall, anderer"
* #S73 ^definition = Inklusive: Flöhe~ Läuse~ Milben~ Zecken Exklusive: infizierter Insektenbiss S11~ Insektenbiss S12 Kriterien: Nachweis von Nissen an den Haarschäften bzw. von Ungeziefer auf Haut oder Kleidern
* #S73 ^property[0].code = #parent 
* #S73 ^property[0].valueCode = #S 
* #S73 ^property[1].code = #Relationships 
* #S73 ^property[1].valueString = "ICD-10 2017:B85.0~B85.1~B85.2~B85.3~B85.4~B87.0~B87.1~B87.2~B87.3~B87.4~B87.8~B87.9~B88.1~B88.3~B88.8~B88.9" 
* #S73 ^property[2].code = #hints 
* #S73 ^property[2].valueString = "Beachte: Pruritus S02~ lokalisierter Ausschlag S06 Hinweise: " 
* #S74 "Dermatophytose"
* #S74 ^definition = Inklusive: Hautpilz~ Nagelmykose~ Onychomykose~ Pityriasis versicolor~ Flechtengrind~ Tinea Exklusive: Monilien-/Candidainfektion S75 Kriterien: eitrige schuppende Läsionen mit freier Mitten und ohne Bläschen am Rand, oder Nachweis von Pilzbefall
* #S74 ^property[0].code = #parent 
* #S74 ^property[0].valueCode = #S 
* #S74 ^property[1].code = #Relationships 
* #S74 ^property[1].valueString = "ICD-10 2017:B35.0~B35.1~B35.2~B35.3~B35.4~B35.5~B35.6~B35.8~B35.9~B36.0~B36.1~B36.2~B36.3~B36.8~B36.9" 
* #S75 "Candidose der Haut"
* #S75 ^definition = Inklusive: Monilia intertrigo~ Soor mit Einbeziehung der Nägel/Perianalregion/Haut Exklusive: oral Candidose D83~ Genitalinfektion X72~ Y75 Kriterien:
* #S75 ^property[0].code = #parent 
* #S75 ^property[0].valueCode = #S 
* #S75 ^property[1].code = #Relationships 
* #S75 ^property[1].valueString = "ICD-10 2017:B37.2" 
* #S76 "Hautinfektion andere"
* #S76 ^definition = Inklusive: Cellulitis~ Erysipel~ Pyodermie~ Streptokokkeninfektion der Haut Exklusive: Furunkel/Karbunkl/andere umschriebene Hautinfektion S11~ Impetigo S84~ Molluscum contagiosum S95~ Akne S96 Kriterien:
* #S76 ^property[0].code = #parent 
* #S76 ^property[0].valueCode = #S 
* #S76 ^property[1].code = #Relationships 
* #S76 ^property[1].valueString = "ICD-10 2017:A46~A66.0~A66.1~A66.2~A66.3~A66.4~A66.5~A66.6~A66.7~A66.8~A66.9~A67.0~A67.1~A67.2~A67.3~A67.9~L03.1~L03.2~L03.3~L03.8~L03.9~L08.0~L08.1~L08.8~L08.9~L98.0" 
* #S77 "Bösartige Neubildung Haut"
* #S77 ^definition = Inklusive: Basalzellkarzinom~ malignes Melanom~ Ulcus rodens~ Plattenepithelkarzinom der Haut Exklusive: Präkanzerose der Haut S79 Kriterien: characteristisches histologisches Erscheinungsbild
* #S77 ^property[0].code = #parent 
* #S77 ^property[0].valueCode = #S 
* #S77 ^property[1].code = #Relationships 
* #S77 ^property[1].valueString = "ICD-10 2017:C43.0~C43.1~C43.2~C43.3~C43.4~C43.5~C43.6~C43.7~C43.8~C43.9~C44.0~C44.1~C44.2~C44.3~C44.4~C44.5~C44.6~C44.7~C44.8~C44.9~C46.0" 
* #S77 ^property[2].code = #hints 
* #S77 ^property[2].valueString = "Beachte: andere bösartige Neubildung (wenn Primärtumor unbekannt) A79~ Neubildung der Haut nicht als gutartig oder bösartig spezifiziert/Histologie nicht verfügbar S79 Hinweise: " 
* #S78 "Lipom"
* #S78 ^property[0].code = #parent 
* #S78 ^property[0].valueCode = #S 
* #S78 ^property[1].code = #Relationships 
* #S78 ^property[1].valueString = "ICD-10 2017:D17.0~D17.1~D17.2~D17.3~D17.4~D17.5~D17.6~D17.7~D17.9" 
* #S79 "Benigne/unklare Neubildung Haut"
* #S79 ^definition = Inklusive: gutartige Neubildung der Haut~ Neubildung der Haut nicht als gut- oder bösartig spezifiziert/Histologie nicht verfügbar~ Dermoidzyste~ Präkanzerose Exklusive: Mariske K96~ Strahlenkeratose S80~ Hämangiom S81~ Leberfleck/Pigmentnävus S82~ Keloid~ Hyperkeratose~ seborrhoische/senile Warzen S99 Kriterien:
* #S79 ^property[0].code = #parent 
* #S79 ^property[0].valueCode = #S 
* #S79 ^property[1].code = #Relationships 
* #S79 ^property[1].valueString = "ICD-10 2017:D03.0~D03.1~D03.2~D03.3~D03.4~D03.5~D03.6~D03.7~D03.8~D03.9~D04.0~D04.1~D04.2~D04.3~D04.4~D04.5~D04.6~D04.7~D04.8~D04.9~D23.0~D23.1~D23.2~D23.3~D23.4~D23.5~D23.6~D23.7~D23.9~D48.5" 
* #S80 "Sonnenbedingte Keratose/Sonnenbrand"
* #S80 ^definition = Inklusive: Photosensitivität~ Strahlenschaden der Haut~ senile Keratose~ sonnenbedingte Hyperkeratose~ polymorpher Lichtausschlag Exklusive: Hautschäden durch künstliche Strahlung A87~ A88 Kriterien:
* #S80 ^property[0].code = #parent 
* #S80 ^property[0].valueCode = #S 
* #S80 ^property[1].code = #Relationships 
* #S80 ^property[1].valueString = "ICD-10 2017:L55.0~L55.1~L55.2~L55.8~L55.9~L56.0~L56.1~L56.2~L56.3~L56.4~L56.8~L56.9~L57.0~L57.1~L57.2~L57.3~L57.4~L57.5~L57.8~L57.9~L58.0~L58.1~L58.9~L59.0~L59.8~L59.9" 
* #S81 "Hämangiom/Lymphangiom"
* #S81 ^definition = Inklusive: Geburtsmal~ Naevus flammeus~ Feuermahl~ Storchenbiß Exklusive:  Kriterien: vaskulärer oder lymphatischer Tumor, der sich über die Haut erhebt und sich auf Druck entleert
* #S81 ^property[0].code = #parent 
* #S81 ^property[0].valueCode = #S 
* #S81 ^property[1].code = #Relationships 
* #S81 ^property[1].valueString = "ICD-10 2017:D18.0~D18.1" 
* #S81 ^property[2].code = #hints 
* #S81 ^property[2].valueString = "Beachte: lokalisierte Schwellung S04 Hinweise: " 
* #S82 "Nävus/Leberfleck"
* #S82 ^property[0].code = #parent 
* #S82 ^property[0].valueCode = #S 
* #S82 ^property[1].code = #Relationships 
* #S82 ^property[1].valueString = "ICD-10 2017:D22.0~D22.1~D22.2~D22.3~D22.4~D22.5~D22.6~D22.7~D22.9" 
* #S83 "Angeborene Hautanomalie, andere"
* #S83 ^definition = Inklusive: Geburtsmal~ Ichthyosis Exklusive: Hämangiom/Lymphangiom S81 Kriterien:
* #S83 ^property[0].code = #parent 
* #S83 ^property[0].valueCode = #S 
* #S83 ^property[1].code = #Relationships 
* #S83 ^property[1].valueString = "ICD-10 2017:Q80.0~Q80.1~Q80.2~Q80.3~Q80.4~Q80.8~Q80.9~Q81.0~Q81.1~Q81.2~Q81.8~Q81.9~Q82.0~Q82.1~Q82.2~Q82.3~Q82.4~Q82.5~Q82.8~Q82.9~Q84.0~Q84.1~Q84.2~Q84.3~Q84.4~Q84.5~Q84.6~Q84.8~Q84.9" 
* #S84 "Impetigo"
* #S84 ^definition = Inklusive: Impetigo als Folge anderer Hautkrankheiten Exklusive:  Kriterien: sich ausbreitende Hautläsion aus Maculae, Vesikulae, Pusteln oder einer Kruste auf gerötetem Grund
* #S84 ^property[0].code = #parent 
* #S84 ^property[0].valueCode = #S 
* #S84 ^property[1].code = #Relationships 
* #S84 ^property[1].valueString = "ICD-10 2017:L00~L01.0~L01.1" 
* #S84 ^property[2].code = #hints 
* #S84 ^property[2].valueString = "Beachte: andere lokalisierte Hautinfektion S11 Hinweise: " 
* #S85 "Pilonidalzyste/-fistel"
* #S85 ^definition = Inklusive: Pilonidalabszess Exklusive: Dermoidzyste S79 Kriterien:
* #S85 ^property[0].code = #parent 
* #S85 ^property[0].valueCode = #S 
* #S85 ^property[1].code = #Relationships 
* #S85 ^property[1].valueString = "ICD-10 2017:L05.0~L05.9" 
* #S86 "Seborrhoische Dermatitis"
* #S86 ^definition = Inklusive: Milchschorf~ Kopfschuppen Exklusive: Seborrhoische Warze S99 Kriterien: fettige, schuppige Läsionen auf gerötetem Grund in einem oder mehreren der folgenden Bereiche: Kopfhaut, Gesicht, Brustbein, zwischen den Schulterblättern, Nagelgegend und in Körperfalten, sofern die Läsionen nicht auf andere Hauterkrankungen zurückzuführen sind
* #S86 ^property[0].code = #parent 
* #S86 ^property[0].valueCode = #S 
* #S86 ^property[1].code = #Relationships 
* #S86 ^property[1].valueString = "ICD-10 2017:L21.0~L21.1~L21.8~L21.9" 
* #S86 ^property[2].code = #hints 
* #S86 ^property[2].valueString = "Beachte: lokalisierter Ausschlag S06~ generalisierter Ausschlag S07 Hinweise: " 
* #S87 "Dermatitis/atopisches Ekzem"
* #S87 ^definition = Inklusive: Dermatitis der Beugeseiten~ Ekzema infantum Exklusive: Dermatitis/atopisches Ekzem mit Einbeziehung des äußeren Gehörgangs H70~ allergische Dermatitis S88~ Windelausschlag S89 Kriterien: eitrige erythematöse Läsionen mit oder ohne Lichenifizierung in den Bereichen, die auf Kontakt mit chemischen Substanzen zurückzuführen sind
* #S87 ^property[0].code = #parent 
* #S87 ^property[0].valueCode = #S 
* #S87 ^property[1].code = #Relationships 
* #S87 ^property[1].valueString = "ICD-10 2017:L20.0~L20.8~L20.9" 
* #S87 ^property[2].code = #hints 
* #S87 ^property[2].valueString = "Beachte: Pruritus S02~ Ausschlag S06~ S07 Hinweise: " 
* #S88 "Allergische/Kontaktdermatitis"
* #S88 ^definition = Inklusive: Allergische Dermatitis~ chemische Dermatitis~ Dermatitis NNB~ Ekzem NNB~ Intertrigo~ Kontaktdermatitis auf Pflanzen~ Hautallergie Exklusive: allgemeine Allergie/allergische Reaktion A92~ Kontakt- und andere Dermatitis des Augenlids F71~ Kontakt-/andere Dermatitis des äußeren Gehörgangs H70~ atopisches Ekzem S87~ Windelausschlag S89~ Urtikaria S98 ~ Dermatitis artefacta/Neurodermatitis S99 Kriterien: eitrige erythematöse Läsionen, die auf Kontakt mit chemischen Substanzen zurückzuführen sind
* #S88 ^property[0].code = #parent 
* #S88 ^property[0].valueCode = #S 
* #S88 ^property[1].code = #Relationships 
* #S88 ^property[1].valueString = "ICD-10 2017:L23.0~L23.1~L23.2~L23.3~L23.4~L23.5~L23.6~L23.7~L23.8~L23.9~L24.0~L24.1~L24.2~L24.3~L24.4~L24.5~L24.6~L24.7~L24.8~L24.9~L25.0~L25.1~L25.2~L25.3~L25.4~L25.5~L25.8~L25.9~L27.2~L27.8~L27.9~L30.0~L30.3~L30.4~L30.8~L30.9" 
* #S88 ^property[2].code = #hints 
* #S88 ^property[2].valueString = "Beachte: Pruritus S02~ Ausschlag S06~ S07 Hinweise: " 
* #S89 "Windeldermatitis"
* #S89 ^definition = Inklusive:  Exklusive:  Kriterien: Dermatitis, insbesondere im Windelbereich, wobei Hautvertiefungen ausgespart bleiben
* #S89 ^property[0].code = #parent 
* #S89 ^property[0].valueCode = #S 
* #S89 ^property[1].code = #Relationships 
* #S89 ^property[1].valueString = "ICD-10 2017:L22" 
* #S90 "Pityriasis rosea"
* #S90 ^definition = Inklusive:  Exklusive:  Kriterien: ovale, schuppige Läsionen im Verlauf der Hautspalten des Oberkörpers mit einer einzelnen Läsion, die dem akuten Ausschlag vorausging in der Anamnese
* #S90 ^property[0].code = #parent 
* #S90 ^property[0].valueCode = #S 
* #S90 ^property[1].code = #Relationships 
* #S90 ^property[1].valueString = "ICD-10 2017:L42" 
* #S90 ^property[2].code = #hints 
* #S90 ^property[2].valueString = "Beachte: Ausschlag S06~ S07 Hinweise: " 
* #S91 "Psoriasis"
* #S91 ^definition = Inklusive:  Exklusive:  Kriterien: Plättchen aus silbrigen Schuppen an Knien, Ellbogen oder Kopfhaut und/oder Tüpfelnägel
* #S91 ^property[0].code = #parent 
* #S91 ^property[0].valueCode = #S 
* #S91 ^property[1].code = #Relationships 
* #S91 ^property[1].valueString = "ICD-10 2017:L40.0~L40.1~L40.2~L40.3~L40.4~L40.5~L40.8~L40.9" 
* #S91 ^property[2].code = #hints 
* #S91 ^property[2].valueString = "Beachte:  Hinweise: psoriatische Arthritis ist mit L99 doppelt zu kodieren" 
* #S92 "Schweißdrüsenerkrankung"
* #S92 ^definition = Inklusive: Dyshidrose~ Hitzeausschlag~ Hidradenitis~ Milien~ Hitzebläschen~ Schweißausschlag Exklusive: Hyperhidrose A09 Kriterien:
* #S92 ^property[0].code = #parent 
* #S92 ^property[0].valueCode = #S 
* #S92 ^property[1].code = #Relationships 
* #S92 ^property[1].valueString = "ICD-10 2017:L30.1~L73.2~L74.0~L74.1~L74.2~L74.3~L74.4~L74.8~L74.9~L75.0~L75.1~L75.2~L75.8~L75.9" 
* #S93 "Atherom"
* #S93 ^definition = Inklusive: Talgdrüsenzyste Exklusive:  Kriterien:
* #S93 ^property[0].code = #parent 
* #S93 ^property[0].valueCode = #S 
* #S93 ^property[1].code = #Relationships 
* #S93 ^property[1].valueString = "ICD-10 2017:L72.1" 
* #S94 "Eingewachsener Nagel"
* #S94 ^definition = Inklusive:  Exklusive: Paronychie S09 Kriterien:
* #S94 ^property[0].code = #parent 
* #S94 ^property[0].valueCode = #S 
* #S94 ^property[1].code = #Relationships 
* #S94 ^property[1].valueString = "ICD-10 2017:L60.0" 
* #S95 "Molluscum contagiosum"
* #S95 ^property[0].code = #parent 
* #S95 ^property[0].valueCode = #S 
* #S95 ^property[1].code = #Relationships 
* #S95 ^property[1].valueString = "ICD-10 2017:B08.1" 
* #S96 "Akne"
* #S96 ^definition = Inklusive: Mittesser~ Komedonen~ Pickel Exklusive: Medikamentenakne A85 Kriterien:
* #S96 ^property[0].code = #parent 
* #S96 ^property[0].valueCode = #S 
* #S96 ^property[1].code = #Relationships 
* #S96 ^property[1].valueString = "ICD-10 2017:L70.0~L70.1~L70.2~L70.3~L70.4~L70.5~L70.8~L70.9" 
* #S97 "Chronische Ulzeration Haut"
* #S97 ^definition = Inklusive: Wundliegen~ Druckstelle~ Ulcus varicosum~ Dekubitalgeschwür Exklusive: Gangrän K92 Kriterien:
* #S97 ^property[0].code = #parent 
* #S97 ^property[0].valueCode = #S 
* #S97 ^property[1].code = #Relationships 
* #S97 ^property[1].valueString = "ICD-10 2017:I83.0~I83.2~L89.0~L89.1~L89.2~L89.3~L89.9~L97~L98.4" 
* #S98 "Urtikaria"
* #S98 ^definition = Inklusive: Nesselausschlag~ Quaddeln Exklusive: Medikamentenallergie A85~ angioneurotisches Ödem/ allergisches Ödem A92 Kriterien:
* #S98 ^property[0].code = #parent 
* #S98 ^property[0].valueCode = #S 
* #S98 ^property[1].code = #Relationships 
* #S98 ^property[1].valueString = "ICD-10 2017:L50.0~L50.1~L50.2~L50.3~L50.4~L50.5~L50.6~L50.8~L50.9" 
* #S99 "Andere Hautkrankheit"
* #S99 ^definition = Inklusive: Dermatitis artefacta~ Lupus erythematodes discoides~ Erythema multiforme~ Erythema nodosum~ Granulom~ Granuloma annulare~ Hyperkeratose NNB~ Keloid~ Kerato-Akanthom~ Lichen planus~ Neurodermitis~ Onychogryphose~ Rosacea~ Rhinophym~ Narbe~ seborrhoische oder senile Warzen~ Striae atrophicae~ Vitiligo Exklusive:  Kriterien:
* #S99 ^property[0].code = #parent 
* #S99 ^property[0].valueCode = #S 
* #S99 ^property[1].code = #Relationships 
* #S99 ^property[1].valueString = "ICD-10 2017:L10.0~L10.1~L10.2~L10.3~L10.4~L10.5~L10.8~L10.9~L11.0~L11.1~L11.8~L11.9~L12.0~L12.1~L12.2~L12.3~L12.8~L12.9~L13.0~L13.1~L13.8~L13.9~L14~L26~L28.0~L28.1~L28.2~L30.2~L30.5~L41.0~L41.1~L41.3~L41.4~L41.5~L41.8~L41.9~L43.0~L43.1~L43.2~L43.3~L43.8~L43.9~L44.0~L44.1~L44.2~L44.3~L44.4~L44.8~L44.9~L45~L51.0~L51.1~L51.2~L51.8~L51.9~L52~L53.0~L53.1~L53.2~L53.3~L53.8~L54.0~L54.8~L60.2~L60.3~L60.8~L71.0~L71.1~L71.8~L71.9~L72.0~L72.2~L72.8~L72.9~L73.0~L73.1~L73.8~L73.9~L80~L81.4~L81.5~L81.6~L81.7~L81.8~L81.9~L82~L83~L85.0~L85.1~L85.2~L85.3~L85.8~L85.9~L86~L87.0~L87.1~L87.2~L87.8~L87.9~L88~L90.0~L90.1~L90.2~L90.3~L90.4~L90.5~L90.6~L90.8~L90.9~L91.0~L91.8~L91.9~L92.0~L92.1~L92.2~L92.3~L92.8~L92.9~L93.0~L93.1~L93.2~L94.0~L94.1~L94.2~L94.3~L94.4~L94.5~L94.6~L94.8~L94.9~L95.0~L95.1~L95.8~L95.9~L98.1~L98.2~L98.3~L98.5~L98.6~L98.7~L98.8~L98.9~L99.0~L99.8" 
* #T "Endokrin, metabolisch, Ernährung"
* #T ^property[0].code = #child 
* #T ^property[0].valueCode = #T01 
* #T ^property[1].code = #child 
* #T ^property[1].valueCode = #T02 
* #T ^property[2].code = #child 
* #T ^property[2].valueCode = #T03 
* #T ^property[3].code = #child 
* #T ^property[3].valueCode = #T04 
* #T ^property[4].code = #child 
* #T ^property[4].valueCode = #T05 
* #T ^property[5].code = #child 
* #T ^property[5].valueCode = #T07 
* #T ^property[6].code = #child 
* #T ^property[6].valueCode = #T08 
* #T ^property[7].code = #child 
* #T ^property[7].valueCode = #T10 
* #T ^property[8].code = #child 
* #T ^property[8].valueCode = #T11 
* #T ^property[9].code = #child 
* #T ^property[9].valueCode = #T26 
* #T ^property[10].code = #child 
* #T ^property[10].valueCode = #T27 
* #T ^property[11].code = #child 
* #T ^property[11].valueCode = #T28 
* #T ^property[12].code = #child 
* #T ^property[12].valueCode = #T29 
* #T ^property[13].code = #child 
* #T ^property[13].valueCode = #T30 
* #T ^property[14].code = #child 
* #T ^property[14].valueCode = #T31 
* #T ^property[15].code = #child 
* #T ^property[15].valueCode = #T32 
* #T ^property[16].code = #child 
* #T ^property[16].valueCode = #T33 
* #T ^property[17].code = #child 
* #T ^property[17].valueCode = #T34 
* #T ^property[18].code = #child 
* #T ^property[18].valueCode = #T35 
* #T ^property[19].code = #child 
* #T ^property[19].valueCode = #T36 
* #T ^property[20].code = #child 
* #T ^property[20].valueCode = #T37 
* #T ^property[21].code = #child 
* #T ^property[21].valueCode = #T38 
* #T ^property[22].code = #child 
* #T ^property[22].valueCode = #T39 
* #T ^property[23].code = #child 
* #T ^property[23].valueCode = #T40 
* #T ^property[24].code = #child 
* #T ^property[24].valueCode = #T41 
* #T ^property[25].code = #child 
* #T ^property[25].valueCode = #T42 
* #T ^property[26].code = #child 
* #T ^property[26].valueCode = #T43 
* #T ^property[27].code = #child 
* #T ^property[27].valueCode = #T44 
* #T ^property[28].code = #child 
* #T ^property[28].valueCode = #T45 
* #T ^property[29].code = #child 
* #T ^property[29].valueCode = #T46 
* #T ^property[30].code = #child 
* #T ^property[30].valueCode = #T47 
* #T ^property[31].code = #child 
* #T ^property[31].valueCode = #T48 
* #T ^property[32].code = #child 
* #T ^property[32].valueCode = #T49 
* #T ^property[33].code = #child 
* #T ^property[33].valueCode = #T50 
* #T ^property[34].code = #child 
* #T ^property[34].valueCode = #T51 
* #T ^property[35].code = #child 
* #T ^property[35].valueCode = #T56 
* #T ^property[36].code = #child 
* #T ^property[36].valueCode = #T57 
* #T ^property[37].code = #child 
* #T ^property[37].valueCode = #T58 
* #T ^property[38].code = #child 
* #T ^property[38].valueCode = #T59 
* #T ^property[39].code = #child 
* #T ^property[39].valueCode = #T60 
* #T ^property[40].code = #child 
* #T ^property[40].valueCode = #T61 
* #T ^property[41].code = #child 
* #T ^property[41].valueCode = #T62 
* #T ^property[42].code = #child 
* #T ^property[42].valueCode = #T63 
* #T ^property[43].code = #child 
* #T ^property[43].valueCode = #T64 
* #T ^property[44].code = #child 
* #T ^property[44].valueCode = #T65 
* #T ^property[45].code = #child 
* #T ^property[45].valueCode = #T66 
* #T ^property[46].code = #child 
* #T ^property[46].valueCode = #T67 
* #T ^property[47].code = #child 
* #T ^property[47].valueCode = #T68 
* #T ^property[48].code = #child 
* #T ^property[48].valueCode = #T69 
* #T ^property[49].code = #child 
* #T ^property[49].valueCode = #T70 
* #T ^property[50].code = #child 
* #T ^property[50].valueCode = #T71 
* #T ^property[51].code = #child 
* #T ^property[51].valueCode = #T72 
* #T ^property[52].code = #child 
* #T ^property[52].valueCode = #T73 
* #T ^property[53].code = #child 
* #T ^property[53].valueCode = #T78 
* #T ^property[54].code = #child 
* #T ^property[54].valueCode = #T80 
* #T ^property[55].code = #child 
* #T ^property[55].valueCode = #T81 
* #T ^property[56].code = #child 
* #T ^property[56].valueCode = #T82 
* #T ^property[57].code = #child 
* #T ^property[57].valueCode = #T83 
* #T ^property[58].code = #child 
* #T ^property[58].valueCode = #T85 
* #T ^property[59].code = #child 
* #T ^property[59].valueCode = #T86 
* #T ^property[60].code = #child 
* #T ^property[60].valueCode = #T87 
* #T ^property[61].code = #child 
* #T ^property[61].valueCode = #T89 
* #T ^property[62].code = #child 
* #T ^property[62].valueCode = #T90 
* #T ^property[63].code = #child 
* #T ^property[63].valueCode = #T91 
* #T ^property[64].code = #child 
* #T ^property[64].valueCode = #T92 
* #T ^property[65].code = #child 
* #T ^property[65].valueCode = #T93 
* #T ^property[66].code = #child 
* #T ^property[66].valueCode = #T99 
* #T01 "Übermäßiger Durst"
* #T01 ^definition = Inklusive: Polydipsie Exklusive:  Kriterien:
* #T01 ^property[0].code = #parent 
* #T01 ^property[0].valueCode = #T 
* #T01 ^property[1].code = #Relationships 
* #T01 ^property[1].valueString = "ICD-10 2017:R63.1" 
* #T02 "Übermäßiger Appetit"
* #T02 ^definition = Inklusive: Übermäßige Nahrungsaufnahme~ Polyphagie Exklusive: Bulimie P86 Kriterien:
* #T02 ^property[0].code = #parent 
* #T02 ^property[0].valueCode = #T 
* #T02 ^property[1].code = #Relationships 
* #T02 ^property[1].valueString = "ICD-10 2017:R63.2" 
* #T03 "Appetitverlust"
* #T03 ^definition = Inklusive: Anorexie Exklusive: Anorexia nervosa P86 Kriterien:
* #T03 ^property[0].code = #parent 
* #T03 ^property[0].valueCode = #T 
* #T03 ^property[1].code = #Relationships 
* #T03 ^property[1].valueString = "ICD-10 2017:R63.0" 
* #T04 "Ernährungsproblem Kleinkind/Kind"
* #T04 ^definition = Inklusive: Problem~ was und wie ein Kleinkind/Kind gefüttert/essen soll Exklusive: Nahrungsmittelallergie A92~ Nahrungsmittelintoleranz D99~ Eßstörung/Ernährungsstörung mit psychologischer Ursache P11 Kriterien:
* #T04 ^property[0].code = #parent 
* #T04 ^property[0].valueCode = #T 
* #T04 ^property[1].code = #Relationships 
* #T04 ^property[1].valueString = "ICD-10 2017:R63.3" 
* #T05 "Ernährungsproblem Erwachsene"
* #T05 ^definition = Inklusive: Problem~ was und wie ein Erwachsener essen/ ernährt werden soll Exklusive: nahrungsmittelallergie A92~ Dysphagie D2~ Nahrungsmittelintoleranz D99~ psychische Essstörung/ Nahrungsverweigerung P29~ Anorexia nervosa/ Bulimie P86~ Appetitverlust T03 Kriterien:
* #T05 ^property[0].code = #parent 
* #T05 ^property[0].valueCode = #T 
* #T05 ^property[1].code = #Relationships 
* #T05 ^property[1].valueString = "ICD-10 2017:R63.3" 
* #T07 "Gewichtszunahme"
* #T07 ^definition = Inklusive:  Exklusive: Fettleibigkeit T82~ Übergewicht T83 Kriterien:
* #T07 ^property[0].code = #parent 
* #T07 ^property[0].valueCode = #T 
* #T07 ^property[1].code = #Relationships 
* #T07 ^property[1].valueString = "ICD-10 2017:R63.5" 
* #T08 "Gewichtsverlust"
* #T08 ^definition = Inklusive: Kachexie Exklusive: Anorexia nervosa P86 Kriterien:
* #T08 ^property[0].code = #parent 
* #T08 ^property[0].valueCode = #T 
* #T08 ^property[1].code = #Relationships 
* #T08 ^property[1].valueString = "ICD-10 2017:R63.4~R64" 
* #T10 "Wachstumsverzögerung"
* #T10 ^definition = Inklusive: Gedeihstörung~ physiologisch bedingte Wachstumsverzögerung Exklusive: verzögerte Entwicklungsphasen P22~ Lernstörung P24~ mentale Retardierung P85~ verzögerte Pubertät T99 Kriterien:
* #T10 ^property[0].code = #parent 
* #T10 ^property[0].valueCode = #T 
* #T10 ^property[1].code = #Relationships 
* #T10 ^property[1].valueString = "ICD-10 2017:E34.3~R62.8~R62.9" 
* #T11 "Austrocknung"
* #T11 ^definition = Inklusive: Wassermangel/ Austrocknung Exklusive: Salzmangel/ Elektrolytstörung T99 Kriterien:
* #T11 ^property[0].code = #parent 
* #T11 ^property[0].valueCode = #T 
* #T11 ^property[1].code = #Relationships 
* #T11 ^property[1].valueString = "ICD-10 2017:E86" 
* #T26 "Angst vor Krebs des endokrinen Systems"
* #T26 ^definition = Inklusive:  Exklusive: wenn der Patient die Erkrankung hat~ ist die Erkrankung zu kodieren Kriterien: Besorgnis über/Angst vor Krebs des endokrinen Systems bei einem Patienten, der die Krankheit nicht hat/bevor die Diagnose gesichert ist
* #T26 ^property[0].code = #parent 
* #T26 ^property[0].valueCode = #T 
* #T26 ^property[1].code = #Relationships 
* #T26 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #T27 "Angst vor endokriner/metabolischer Erkrankung, andere"
* #T27 ^definition = Inklusive: Angst vor Diabetes Exklusive: Angst vor Krebs des Endokrinen Systems T26~ wenn der Patient die Erkrankung hat~ ist die Erkrankung zu kodieren Kriterien: Besorgnis über/Angst vor einer anderen Erkrankung des endokrinen Systems bei einem Patienten, der die Krankheit nicht hat/bevor die Diagnose gesichert ist
* #T27 ^property[0].code = #parent 
* #T27 ^property[0].valueCode = #T 
* #T27 ^property[1].code = #Relationships 
* #T27 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #T28 "Funktionseinschränkung (T)"
* #T28 ^definition = Inklusive:  Exklusive:  Kriterien: Funktionseinschränkung/Behinderung durch eine Erkrankung des endokrinen Systems
* #T28 ^property[0].code = #parent 
* #T28 ^property[0].valueCode = #T 
* #T28 ^property[1].code = #Relationships 
* #T28 ^property[1].valueString = "ICD-10 2017:Z73.6" 
* #T28 ^property[2].code = #hints 
* #T28 ^property[2].valueString = "Beachte:  Hinweise: COOP/WONCA Charts eignen sich zur Dokumentation des funktionellen Status des Patienten (siehe Kapitel 8)" 
* #T29 "Andere endokrinologische/metabolische/ernährungsbedingte Symptome/Beschwerden"
* #T29 ^definition = Inklusive: Heißhunger auf spezifische Nahrungsmittel~ Untergewicht Exklusive: Hyperglykämie A91~ Flüssigkeitsretention K07 Kriterien:
* #T29 ^property[0].code = #parent 
* #T29 ^property[0].valueCode = #T 
* #T29 ^property[1].code = #Relationships 
* #T29 ^property[1].valueString = "ICD-10 2017:R63.8" 
* #T30 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? vollständig"
* #T30 ^property[0].code = #parent 
* #T30 ^property[0].valueCode = #T 
* #T31 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? teilweise"
* #T31 ^property[0].code = #parent 
* #T31 ^property[0].valueCode = #T 
* #T32 "Allergie-/Sensitivitätstestung"
* #T32 ^property[0].code = #parent 
* #T32 ^property[0].valueCode = #T 
* #T33 "Mikrobiologische/immunologische Untersuchung"
* #T33 ^property[0].code = #parent 
* #T33 ^property[0].valueCode = #T 
* #T34 "Blutuntersuchung"
* #T34 ^property[0].code = #parent 
* #T34 ^property[0].valueCode = #T 
* #T35 "Urinuntersuchung"
* #T35 ^property[0].code = #parent 
* #T35 ^property[0].valueCode = #T 
* #T36 "Stuhluntersuchung"
* #T36 ^property[0].code = #parent 
* #T36 ^property[0].valueCode = #T 
* #T37 "Histologische Untersuchung/zytologischer Abstrich"
* #T37 ^property[0].code = #parent 
* #T37 ^property[0].valueCode = #T 
* #T38 "Andere Laboruntersuchung NAK"
* #T38 ^property[0].code = #parent 
* #T38 ^property[0].valueCode = #T 
* #T39 "Körperliche Funktionsprüfung"
* #T39 ^property[0].code = #parent 
* #T39 ^property[0].valueCode = #T 
* #T40 "Diagnostische Endoskopie"
* #T40 ^property[0].code = #parent 
* #T40 ^property[0].valueCode = #T 
* #T41 "Diagnostisches Röntgen/Bildgebung"
* #T41 ^property[0].code = #parent 
* #T41 ^property[0].valueCode = #T 
* #T42 "Elektrische Aufzeichnungsverfahren"
* #T42 ^property[0].code = #parent 
* #T42 ^property[0].valueCode = #T 
* #T43 "Andere diagnostische Untersuchung"
* #T43 ^property[0].code = #parent 
* #T43 ^property[0].valueCode = #T 
* #T44 "Präventive Impfung/Medikation"
* #T44 ^property[0].code = #parent 
* #T44 ^property[0].valueCode = #T 
* #T45 "Beobachtung/Gesundheitsschulung/Beratung/Diät"
* #T45 ^property[0].code = #parent 
* #T45 ^property[0].valueCode = #T 
* #T46 "Konsultation eines anderen Primärversorgers"
* #T46 ^property[0].code = #parent 
* #T46 ^property[0].valueCode = #T 
* #T47 "Konsultation eines Facharztes"
* #T47 ^property[0].code = #parent 
* #T47 ^property[0].valueCode = #T 
* #T48 "Klärung/Besprechung des Beratungsanlasses mit dem Patienten"
* #T48 ^property[0].code = #parent 
* #T48 ^property[0].valueCode = #T 
* #T49 "Andere Vorsorgemaßnahme"
* #T49 ^property[0].code = #parent 
* #T49 ^property[0].valueCode = #T 
* #T50 "Medikation/Verschreibung/Erneuerung/Injektion"
* #T50 ^property[0].code = #parent 
* #T50 ^property[0].valueCode = #T 
* #T51 "Inzision/Drainage/Spülung/Absaugung/Entfernung von Körperflüssigkeit"
* #T51 ^property[0].code = #parent 
* #T51 ^property[0].valueCode = #T 
* #T56 "Verband/Druck/Kompression/Tamponade"
* #T56 ^property[0].code = #parent 
* #T56 ^property[0].valueCode = #T 
* #T57 "Physikalische Therapie/Rehabilitation"
* #T57 ^property[0].code = #parent 
* #T57 ^property[0].valueCode = #T 
* #T58 "Therapeutische Beratung/Zuhören"
* #T58 ^property[0].code = #parent 
* #T58 ^property[0].valueCode = #T 
* #T59 "Andere therapeutische Maßnahme/kleine Chirurgie NAK"
* #T59 ^property[0].code = #parent 
* #T59 ^property[0].valueCode = #T 
* #T60 "Testresultat/Ergebnis eigene Maßnahme"
* #T60 ^property[0].code = #parent 
* #T60 ^property[0].valueCode = #T 
* #T61 "Ergebnis Untersuchung/Test/Brief von anderem Leistungserbringer"
* #T61 ^property[0].code = #parent 
* #T61 ^property[0].valueCode = #T 
* #T62 "Adminstrative Maßnahme"
* #T62 ^property[0].code = #parent 
* #T62 ^property[0].valueCode = #T 
* #T63 "Folgekonsultation unspezifiziert"
* #T63 ^property[0].code = #parent 
* #T63 ^property[0].valueCode = #T 
* #T64 "Konsultation/Problembehandlung auf Initiative des Leistungserbringers"
* #T64 ^property[0].code = #parent 
* #T64 ^property[0].valueCode = #T 
* #T65 "Konsultation/Problembehandlung auf Initiative Dritter"
* #T65 ^property[0].code = #parent 
* #T65 ^property[0].valueCode = #T 
* #T66 "Überweisung an andere/n Leistungserbringer (nicht ärztl.)/Pflegekraft/Therapeuten/Sozialarbeiter"
* #T66 ^property[0].code = #parent 
* #T66 ^property[0].valueCode = #T 
* #T67 "Überweisung an einen Arzt/Spezialisten/eine Klinik/ein Krankenhaus"
* #T67 ^property[0].code = #parent 
* #T67 ^property[0].valueCode = #T 
* #T68 "Andere Überweisung NAK"
* #T68 ^property[0].code = #parent 
* #T68 ^property[0].valueCode = #T 
* #T69 "Anderer Beratungsanlass NAK"
* #T69 ^property[0].code = #parent 
* #T69 ^property[0].valueCode = #T 
* #T70 "Endokrine Infektion"
* #T70 ^definition = Inklusive:  Exklusive: Thyreoiditis T99 Kriterien:
* #T70 ^property[0].code = #parent 
* #T70 ^property[0].valueCode = #T 
* #T70 ^property[1].code = #Relationships 
* #T70 ^property[1].valueString = "ICD-10 2017:E06.0" 
* #T71 "Bösartige Neubildung Schilddrüse"
* #T71 ^definition = Inklusive:  Exklusive:  Kriterien: characteristisches histologisches Erscheinungsbild
* #T71 ^property[0].code = #parent 
* #T71 ^property[0].valueCode = #T 
* #T71 ^property[1].code = #Relationships 
* #T71 ^property[1].valueString = "ICD-10 2017:C73" 
* #T71 ^property[2].code = #hints 
* #T71 ^property[2].valueString = "Beachte: andere/nicht spezifizierte endokrine Neubildung T73~ Kropf T81 Hinweise: " 
* #T72 "Gutartige Neubildung Schilddrüse"
* #T72 ^definition = Inklusive:  Exklusive: andere/unspezifizierte Neubildung des endokrinen Systems T73~ Kropf T81 Kriterien:
* #T72 ^property[0].code = #parent 
* #T72 ^property[0].valueCode = #T 
* #T72 ^property[1].code = #Relationships 
* #T72 ^property[1].valueString = "ICD-10 2017:D34" 
* #T73 "Neubildung endokrine, andere/unspezifiziert"
* #T73 ^definition = Inklusive: andere endokrine Neoplasie/ endokrine neoplasie~ nicht als gut- oder bösartig spezifiziert/ wenn keine Histologie verfügbar ist Exklusive:  Kriterien:
* #T73 ^property[0].code = #parent 
* #T73 ^property[0].valueCode = #T 
* #T73 ^property[1].code = #Relationships 
* #T73 ^property[1].valueString = "ICD-10 2017:C74.0~C74.1~C74.9~C75.0~C75.1~C75.2~C75.3~C75.4~C75.5~C75.8~C75.9~D09.3~D35.0~D35.1~D35.2~D35.3~D35.4~D35.5~D35.6~D35.7~D35.8~D35.9~D44.0~D44.1~D44.2~D44.3~D44.4~D44.5~D44.6~D44.7~D44.8~D44.9" 
* #T78 "Thyreoglossale Fistel/Zyste"
* #T78 ^definition = Inklusive:  Exklusive: Kropf T81 Kriterien:
* #T78 ^property[0].code = #parent 
* #T78 ^property[0].valueCode = #T 
* #T78 ^property[1].code = #Relationships 
* #T78 ^property[1].valueString = "ICD-10 2017:Q89.2" 
* #T80 "Angeborene Fehlbildung, endokrine/metabolische"
* #T80 ^definition = Inklusive: Kretinismus~ Zwergwuchs Exklusive: Persistierender Ductus thyroglossus T78 Kriterien:
* #T80 ^property[0].code = #parent 
* #T80 ^property[0].valueCode = #T 
* #T80 ^property[1].code = #Relationships 
* #T80 ^property[1].valueString = "ICD-10 2017:E00.0~E00.1~E00.2~E00.9~E25.0~Q89.1~Q89.2" 
* #T81 "Struma"
* #T81 ^definition = Inklusive: nichttoxischer Kropf~ Schilddrüsenknoten Exklusive: Neubildung der Schilddrüse T71-T73~ Persistierender Ductus thyroglossus T78~ toxischer Kropf T85~ Hypothyreose T86 Kriterien:
* #T81 ^property[0].code = #parent 
* #T81 ^property[0].valueCode = #T 
* #T81 ^property[1].code = #Relationships 
* #T81 ^property[1].valueString = "ICD-10 2017:E04.0~E04.1~E04.2~E04.8~E04.9" 
* #T82 "Adipositas"
* #T82 ^definition = Inklusive:  Exklusive: Übergewicht T83 Kriterien: Ein Body Mass Index über 30
* #T82 ^property[0].code = #parent 
* #T82 ^property[0].valueCode = #T 
* #T82 ^property[1].code = #Relationships 
* #T82 ^property[1].valueString = "ICD-10 2017:E66.0~E66.1~E66.2~E66.8~E66.9" 
* #T83 "Übergewicht"
* #T83 ^definition = Inklusive:  Exklusive: Fettleibigkeit T82 Kriterien: Ein Body Mass Index zwischen 25 und 30
* #T83 ^property[0].code = #parent 
* #T83 ^property[0].valueCode = #T 
* #T83 ^property[1].code = #Relationships 
* #T83 ^property[1].valueString = "ICD-10 2017:E66.0~E66.1~E66.2~E66.8~E66.9" 
* #T85 "Hyperthyreose/Thyreotoxische Krise"
* #T85 ^definition = Inklusive: Morbus Basedow~ toxischer Kropf Exklusive: nichttoxischer Kropf T81 Kriterien: Labornachweis einer übermäßigen Schilddrüsenhromonproduktion, oder Schilddrüsenknoten oder Struma plus Tremor, Gewichtsverlust und schneller Puls (>100/min in Ruhe) oder Augenzeichen (Exophthalmus, Graefe-Zeichen oder Augenmuskellähmung)
* #T85 ^property[0].code = #parent 
* #T85 ^property[0].valueCode = #T 
* #T85 ^property[1].code = #Relationships 
* #T85 ^property[1].valueString = "ICD-10 2017:E05.0~E05.1~E05.2~E05.3~E05.4~E05.5~E05.8~E05.9" 
* #T86 "Hypothyreose/Myxödem"
* #T86 ^definition = Inklusive:  Exklusive: Kretinismus T80 Kriterien: Labornachweis einer verminderten Schilddrüsenhromonproduktion und übermäßig vieler schilddrüsenstimulierender Hormone, oder mindestens vier der folgenden Symptome: Schwäche, Müdigkeit, geistige Veränderung: Apathie, schlechtes Gedächtnis, Verlangsamung; Stimmveränderung: rauheres, tiefers, langsameres Sprechen; übermäßige Kälteempfindlichkeit; Verstopfung; grobe, aufgedunsene Gesichtszüge; kalte, trockene, blasse Haut, vermindertes Schwitzen; peripheres Ödem
* #T86 ^property[0].code = #parent 
* #T86 ^property[0].valueCode = #T 
* #T86 ^property[1].code = #Relationships 
* #T86 ^property[1].valueString = "ICD-10 2017:E01.0~E01.1~E01.2~E01.8~E02~E03.0~E03.1~E03.2~E03.3~E03.4~E03.5~E03.8~E03.9" 
* #T86 ^property[2].code = #hints 
* #T86 ^property[2].valueString = "Beachte: andere Beschwerden des Stoffwechsels T29 Hinweise: " 
* #T87 "Hypoglykämie"
* #T87 ^definition = Inklusive: Hyperinsulismus~ Insulinkoma Exklusive:  Kriterien: Biochemischer Nachweis von Hyopglykömie; oder charakteristische Symptome bei einem Diabets-Patienten, di durch Einnahme oder Injektion von Zucker gelindert werden
* #T87 ^property[0].code = #parent 
* #T87 ^property[0].valueCode = #T 
* #T87 ^property[1].code = #Relationships 
* #T87 ^property[1].valueString = "ICD-10 2017:E15~E16.0~E16.1~E16.2~E16.3~E16.9" 
* #T89 "Diabetes mellitus, primär insulinabhängig"
* #T89 ^definition = Inklusive: in der Jugend einsetzender Diabetes~ Typ 1 Diabetes Exklusive: medikamenteninduzierte Hyperglykämie A85~ Hyperglykämie als isolierter Befund A91~ nichtinsulinabhängiger DiabetesT90~ Gestationsdiabetes W85 Kriterien: Der Patient bedarf der regelmäßigen, fortwährenden Behandlung mit Insulin, nachdem die Diagnose durch einen der folgenden Punkte erwisen wurde: a) die klassischen Dianbetes-Symptome wie Polyurie, Polydpsie und rascher Gewichtsverlust in Verbindung mit einer eindeutigen Erhöhung der Blutzuckerwerte b) Blutzucker-Nüchternwerte von mindestens 140mg/dl bei mindestens zwei Gelegenheiten c) Blutzuckerwerte von mindestens 200mg/dl bei mindestens zwei beliebigen Messungen d) ein oraler Glukosetoleranztest mit einem Blutzuckerwert von mindestens 200mg/dl nach zwei Stunden
* #T89 ^property[0].code = #parent 
* #T89 ^property[0].valueCode = #T 
* #T89 ^property[1].code = #Relationships 
* #T89 ^property[1].valueString = "ICD-10 2017:E10.0~E10.1~E10.2~E10.3~E10.4~E10.5~E10.6~E10.7~E10.8~E10.9" 
* #T89 ^property[2].code = #hints 
* #T89 ^property[2].valueString = "Beachte: Hyperglykämie A91~ Diabetes, nicht insulinabhängig T90 Hinweise: 1.Komplikationen wie z.B. Rethinopathie F83 oder Nephropathie U88 sind doppelt zu kodieren 2. Diabetes in der Schwangerschaft ist mit W84 doppelt zu kodieren" 
* #T90 "Diabetes mellitus, primär nicht insulinabhängig"
* #T90 ^definition = Inklusive: Diabetes NNB~ im Alter einsetzender Dianetes~ Typ II Diabetes Exklusive: medikamenteninduzierte Hyperglykämie A85~ Hyperglykämie als Einzelbefund A91~ insulinabhängiger Diabetes T89~ Gestationsdiabetes W85 Kriterien: Der Patient bedarf keiner regelmäßigen, fortwährenden Behandlung mit Insulin, nachdem die Diagnose durch einen der folgenden Punkte erwisen wurde: a) die klassischen Dianbetes-Symptome wie Polyurie, Polydpsie und rascher Gewichtsverlust in Verbindung mit einer eindeutigen Erhöhung der Blutzuckerwerte b) Blutzucker-Nüchternwerte von mindestens 140mg/dl bei mindestens zwei Gelegenheiten c) Blutzuckerwerte von mindestens 200mg/dl bei mindestens zwei beliebigen Messungen d) ein oraler Glukosetoleranztest mit einem Blutzuckerwert von mindestens 200mg/dl nach zwei Stunden
* #T90 ^property[0].code = #parent 
* #T90 ^property[0].valueCode = #T 
* #T90 ^property[1].code = #Relationships 
* #T90 ^property[1].valueString = "ICD-10 2017:E11.0~E11.1~E11.2~E11.3~E11.4~E11.5~E11.6~E11.7~E11.8~E11.9~E12.0~E12.1~E12.2~E12.3~E12.4~E12.5~E12.6~E12.7~E12.8~E12.9~E13.0~E13.1~E13.2~E13.3~E13.4~E13.5~E13.6~E13.7~E13.8~E13.9~E14.0~E14.1~E14.2~E14.3~E14.4~E14.5~E14.6~E14.7~E14.8~E14.9" 
* #T90 ^property[2].code = #hints 
* #T90 ^property[2].valueString = "Beachte: siehe auch: Hyperglykämie A91~ Diabetes, insulinabhängig T89 Hinweise: 1.Komplikationen wie z.B. Rethinopathie F83 oder Nephropathie U88 sind doppelt zu kodieren 2. Diabetes in der Schwangerschaft ist mit W84 doppelt zu kodieren" 
* #T91 "Vitamin-/Nährstoffmangel"
* #T91 ^definition = Inklusive: Beri-Beri~ ernährungsbedingter Mineralmangel~ Eisenmangel ohne Anämie~ Unterernährung~ Marasmus~ Skorbut Exklusive: Eisenmangelanämie B80~ perniziöse Anämie B81~ Malabsorptionssyndrom/Sprue D99 Kriterien:
* #T91 ^property[0].code = #parent 
* #T91 ^property[0].valueCode = #T 
* #T91 ^property[1].code = #Relationships 
* #T91 ^property[1].valueString = "ICD-10 2017:E40~E41~E42~E43~E44.0~E44.1~E45~E46~E50.0~E50.1~E50.2~E50.3~E50.4~E50.5~E50.6~E50.7~E50.8~E50.9~E51.1~E51.8~E51.9~E52~E53.0~E53.1~E53.8~E53.9~E54~E55.0~E55.9~E56.0~E56.1~E56.8~E56.9~E58~E59~E60~E61.0~E61.1~E61.2~E61.3~E61.4~E61.5~E61.6~E61.7~E61.8~E61.9~E63.0~E63.1~E63.8~E63.9~E64.0~E64.1~E64.2~E64.3~E64.8~E64.9" 
* #T92 "Gicht"
* #T92 ^definition = Inklusive:  Exklusive: durch Medikamente induzierte Gicht A85~erhöhte Harnsäurewerte A91~ Pseudogicht oder andere durch Kristalle induzierte Arthropathie T99 Kriterien:
* #T92 ^property[0].code = #parent 
* #T92 ^property[0].valueCode = #T 
* #T92 ^property[1].code = #Relationships 
* #T92 ^property[1].valueString = "ICD-10 2017:M10.0~M10.1~M10.2~M10.3~M10.4~M10.9" 
* #T93 "Fettstoffwechselstörung"
* #T93 ^definition = Inklusive: abnorme Lipoproteinwerte~ Hyperlipidämie~ erhöhte Werte für Cholesterin/Triglyzeride~ Xanthelasmen Exklusive:  Kriterien:
* #T93 ^property[0].code = #parent 
* #T93 ^property[0].valueCode = #T 
* #T93 ^property[1].code = #Relationships 
* #T93 ^property[1].valueString = "ICD-10 2017:E78.0~E78.1~E78.2~E78.3~E78.4~E78.5~E78.6~E78.8~E78.9" 
* #T99 "Andere endokrinologische/metabolische/ernährungsbedingte Erkrankung"
* #T99 ^definition = Inklusive: Akromegalie~ Funktionsstörung der Nebenniere/Eierstöcke/Hypophyse/Nebenschilddrüse/Hoden oder andere endokrine Funktionsstörung~ Amyloidose~ Kristallarthopathien~ Pseudogicht~ Cushing-Syndrom~ zystische Fibrose~ Diabetes insipidus~ Gilbert-Syndrom~ Hyperaldosteronismus~ Osteomalazie~ Porphyrie~ verfrühte oder verzögerte Pubertät~ renale Glykosurie~ Thyroiditis Exklusive: Lebenmittelallergie A92~ Lebensmittelintoleranz D99~ Osteoporose L95 Kriterien:
* #T99 ^property[0].code = #parent 
* #T99 ^property[0].valueCode = #T 
* #T99 ^property[1].code = #Relationships 
* #T99 ^property[1].valueString = "ICD-10 2017:E06.1~E06.2~E06.3~E06.4~E06.5~E06.9~E07.0~E07.1~E07.8~E07.9~E16.8~E20.0~E20.1~E20.8~E20.9~E21.0~E21.1~E21.2~E21.3~E21.4~E21.5~E22.0~E22.1~E22.2~E22.8~E22.9~E23.0~E23.1~E23.2~E23.3~E23.6~E23.7~E24.0~E24.1~E24.2~E24.3~E24.4~E24.8~E24.9~E25.8~E25.9~E26.0~E26.1~E26.8~E26.9~E27.0~E27.1~E27.2~E27.3~E27.4~E27.5~E27.8~E27.9~E28.0~E28.1~E28.2~E28.3~E28.8~E28.9~E29.0~E29.1~E29.8~E29.9~E30.0~E30.1~E30.8~E30.9~E31.0~E31.1~E31.8~E31.9~E32.0~E32.1~E32.8~E32.9~E34.0~E34.1~E34.2~E34.4~E34.5~E34.8~E34.9~E35.0~E35.1~E35.8~E65~E67.0~E67.1~E67.2~E67.3~E67.8~E68~E70.0~E70.1~E70.2~E70.3~E70.8~E70.9~E71.0~E71.1~E71.2~E71.3~E72.0~E72.1~E72.2~E72.3~E72.4~E72.5~E72.8~E72.9~E73.0~E73.1~E73.8~E73.9~E74.0~E74.1~E74.2~E74.3~E74.4~E74.8~E74.9~E75.0~E75.1~E75.2~E75.3~E75.4~E75.5~E75.6~E76.0~E76.1~E76.2~E76.3~E76.8~E76.9~E77.0~E77.1~E77.8~E77.9~E79.0~E79.1~E79.8~E79.9~E80.0~E80.1~E80.2~E80.3~E80.4~E80.5~E80.6~E80.7~E83.0~E83.1~E83.2~E83.3~E83.4~E83.5~E83.8~E83.9~E84.0~E84.1~E84.8~E84.9~E85.0~E85.1~E85.2~E85.3~E85.4~E85.8~E85.9~E87.0~E87.1~E87.2~E87.3~E87.4~E87.5~E87.6~E87.7~E87.8~E88.0~E88.1~E88.2~E88.3~E88.8~E88.9~E90~M11.0~M11.1~M11.2~M11.8~M11.9~M83.0~M83.1~M83.2~M83.3~M83.4~M83.5~M83.8~M83.9" 
* #U "Urologisch"
* #U ^property[0].code = #child 
* #U ^property[0].valueCode = #U01 
* #U ^property[1].code = #child 
* #U ^property[1].valueCode = #U02 
* #U ^property[2].code = #child 
* #U ^property[2].valueCode = #U04 
* #U ^property[3].code = #child 
* #U ^property[3].valueCode = #U05 
* #U ^property[4].code = #child 
* #U ^property[4].valueCode = #U06 
* #U ^property[5].code = #child 
* #U ^property[5].valueCode = #U07 
* #U ^property[6].code = #child 
* #U ^property[6].valueCode = #U08 
* #U ^property[7].code = #child 
* #U ^property[7].valueCode = #U13 
* #U ^property[8].code = #child 
* #U ^property[8].valueCode = #U14 
* #U ^property[9].code = #child 
* #U ^property[9].valueCode = #U26 
* #U ^property[10].code = #child 
* #U ^property[10].valueCode = #U27 
* #U ^property[11].code = #child 
* #U ^property[11].valueCode = #U28 
* #U ^property[12].code = #child 
* #U ^property[12].valueCode = #U29 
* #U ^property[13].code = #child 
* #U ^property[13].valueCode = #U30 
* #U ^property[14].code = #child 
* #U ^property[14].valueCode = #U31 
* #U ^property[15].code = #child 
* #U ^property[15].valueCode = #U32 
* #U ^property[16].code = #child 
* #U ^property[16].valueCode = #U33 
* #U ^property[17].code = #child 
* #U ^property[17].valueCode = #U34 
* #U ^property[18].code = #child 
* #U ^property[18].valueCode = #U35 
* #U ^property[19].code = #child 
* #U ^property[19].valueCode = #U36 
* #U ^property[20].code = #child 
* #U ^property[20].valueCode = #U37 
* #U ^property[21].code = #child 
* #U ^property[21].valueCode = #U38 
* #U ^property[22].code = #child 
* #U ^property[22].valueCode = #U39 
* #U ^property[23].code = #child 
* #U ^property[23].valueCode = #U40 
* #U ^property[24].code = #child 
* #U ^property[24].valueCode = #U41 
* #U ^property[25].code = #child 
* #U ^property[25].valueCode = #U42 
* #U ^property[26].code = #child 
* #U ^property[26].valueCode = #U43 
* #U ^property[27].code = #child 
* #U ^property[27].valueCode = #U44 
* #U ^property[28].code = #child 
* #U ^property[28].valueCode = #U45 
* #U ^property[29].code = #child 
* #U ^property[29].valueCode = #U46 
* #U ^property[30].code = #child 
* #U ^property[30].valueCode = #U47 
* #U ^property[31].code = #child 
* #U ^property[31].valueCode = #U48 
* #U ^property[32].code = #child 
* #U ^property[32].valueCode = #U49 
* #U ^property[33].code = #child 
* #U ^property[33].valueCode = #U50 
* #U ^property[34].code = #child 
* #U ^property[34].valueCode = #U51 
* #U ^property[35].code = #child 
* #U ^property[35].valueCode = #U52 
* #U ^property[36].code = #child 
* #U ^property[36].valueCode = #U53 
* #U ^property[37].code = #child 
* #U ^property[37].valueCode = #U54 
* #U ^property[38].code = #child 
* #U ^property[38].valueCode = #U55 
* #U ^property[39].code = #child 
* #U ^property[39].valueCode = #U56 
* #U ^property[40].code = #child 
* #U ^property[40].valueCode = #U57 
* #U ^property[41].code = #child 
* #U ^property[41].valueCode = #U58 
* #U ^property[42].code = #child 
* #U ^property[42].valueCode = #U59 
* #U ^property[43].code = #child 
* #U ^property[43].valueCode = #U60 
* #U ^property[44].code = #child 
* #U ^property[44].valueCode = #U61 
* #U ^property[45].code = #child 
* #U ^property[45].valueCode = #U62 
* #U ^property[46].code = #child 
* #U ^property[46].valueCode = #U63 
* #U ^property[47].code = #child 
* #U ^property[47].valueCode = #U64 
* #U ^property[48].code = #child 
* #U ^property[48].valueCode = #U65 
* #U ^property[49].code = #child 
* #U ^property[49].valueCode = #U66 
* #U ^property[50].code = #child 
* #U ^property[50].valueCode = #U67 
* #U ^property[51].code = #child 
* #U ^property[51].valueCode = #U68 
* #U ^property[52].code = #child 
* #U ^property[52].valueCode = #U69 
* #U ^property[53].code = #child 
* #U ^property[53].valueCode = #U70 
* #U ^property[54].code = #child 
* #U ^property[54].valueCode = #U71 
* #U ^property[55].code = #child 
* #U ^property[55].valueCode = #U72 
* #U ^property[56].code = #child 
* #U ^property[56].valueCode = #U75 
* #U ^property[57].code = #child 
* #U ^property[57].valueCode = #U76 
* #U ^property[58].code = #child 
* #U ^property[58].valueCode = #U77 
* #U ^property[59].code = #child 
* #U ^property[59].valueCode = #U78 
* #U ^property[60].code = #child 
* #U ^property[60].valueCode = #U79 
* #U ^property[61].code = #child 
* #U ^property[61].valueCode = #U80 
* #U ^property[62].code = #child 
* #U ^property[62].valueCode = #U85 
* #U ^property[63].code = #child 
* #U ^property[63].valueCode = #U88 
* #U ^property[64].code = #child 
* #U ^property[64].valueCode = #U90 
* #U ^property[65].code = #child 
* #U ^property[65].valueCode = #U95 
* #U ^property[66].code = #child 
* #U ^property[66].valueCode = #U98 
* #U ^property[67].code = #child 
* #U ^property[67].valueCode = #U99 
* #U01 "Schmerzhafte Miktion"
* #U01 ^definition = Inklusive: Brennen beim Wasserlassen Exklusive: gehäufte Miktion/ Harndrang U02~ Urethritis U72 Kriterien:
* #U01 ^property[0].code = #parent 
* #U01 ^property[0].valueCode = #U 
* #U01 ^property[1].code = #Relationships 
* #U01 ^property[1].valueString = "ICD-10 2017:R30.0~R30.1~R30.9" 
* #U02 "Häufige Miktion/Harndrang"
* #U02 ^definition = Inklusive: Nykturie~ Polyurie Exklusive:  Kriterien:
* #U02 ^property[0].code = #parent 
* #U02 ^property[0].valueCode = #U 
* #U02 ^property[1].code = #Relationships 
* #U02 ^property[1].valueString = "ICD-10 2017:R35" 
* #U04 "Harninkontinenz"
* #U04 ^definition = Inklusive: Enuresis organischen Ursprungs~ unwillkürliche Miktion~ Stressinkontinenz Exklusive: Harninkontinenz psychogenen Ursprungs P12 Kriterien:
* #U04 ^property[0].code = #parent 
* #U04 ^property[0].valueCode = #U 
* #U04 ^property[1].code = #Relationships 
* #U04 ^property[1].valueString = "ICD-10 2017:N39.3~N39.4~R32" 
* #U05 "Miktionsproblem, anderes"
* #U05 ^definition = Inklusive: Anurie~ Harntröpfeln~ Oligurie Exklusive: Harnverhalt U08 Kriterien:
* #U05 ^property[0].code = #parent 
* #U05 ^property[0].valueCode = #U 
* #U05 ^property[1].code = #Relationships 
* #U05 ^property[1].valueString = "ICD-10 2017:R34~R39.1" 
* #U06 "Hämaturie"
* #U06 ^definition = Inklusive: Blut im Urin Exklusive:  Kriterien: Blutnachweis im Urin mikroskopisch/makroskopisch/laborchemisch
* #U06 ^property[0].code = #parent 
* #U06 ^property[0].valueCode = #U 
* #U06 ^property[1].code = #Relationships 
* #U06 ^property[1].valueString = "ICD-10 2017:N02.0~N02.1~N02.2~N02.3~N02.4~N02.5~N02.6~N02.7~N02.8~N02.9~R31" 
* #U07 "Miktionssymptom/-beschwerden, andere"
* #U07 ^definition = Inklusive: schlecht riechender Urin~ dunkler Urin Exklusive: abnormer Urintest U98 Kriterien:
* #U07 ^property[0].code = #parent 
* #U07 ^property[0].valueCode = #U 
* #U07 ^property[1].code = #Relationships 
* #U07 ^property[1].valueString = "ICD-10 2017:R39.8" 
* #U08 "Harnverhalt"
* #U08 ^property[0].code = #parent 
* #U08 ^property[0].valueCode = #U 
* #U08 ^property[1].code = #Relationships 
* #U08 ^property[1].valueString = "ICD-10 2017:R33" 
* #U13 "Blasensymptom/-beschwerden, andere"
* #U13 ^definition = Inklusive: Blasenschmerz~ Reizblase Exklusive:  Kriterien:
* #U13 ^property[0].code = #parent 
* #U13 ^property[0].valueCode = #U 
* #U13 ^property[1].code = #Relationships 
* #U13 ^property[1].valueString = "ICD-10 2017:R39.8" 
* #U14 "Nierensymptom/-beschwerden"
* #U14 ^definition = Inklusive: Nierenschmerz~ Nierenprobleme~ Nierenkolik Exklusive: Leisten-/Flankenschmerzen L05 Kriterien:
* #U14 ^property[0].code = #parent 
* #U14 ^property[0].valueCode = #U 
* #U14 ^property[1].code = #Relationships 
* #U14 ^property[1].valueString = "ICD-10 2017:N23" 
* #U26 "Angst vor Krebs der Harnorgane"
* #U26 ^definition = Inklusive:  Exklusive: wenn der Patient die Erkrankung hat~ ist die Erkrankung zu kodieren Kriterien: Besorgnis über/Angst vor Krebs der Harnwege bei einem Patienten, der die Krankheit nicht hat/bevor die Diagnose gesichert ist
* #U26 ^property[0].code = #parent 
* #U26 ^property[0].valueCode = #U 
* #U26 ^property[1].code = #Relationships 
* #U26 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #U27 "Angst vor anderer Erkrankung der Harnorgane"
* #U27 ^definition = Inklusive:  Exklusive: Angst vor Krebs der Harnwege U26~ wenn der Patient die Erkrankung hat~ ist die Erkrankung zu kodieren Kriterien: Besorgnis über/Angst vor einer anderen Erkrankung der Harnwege bei einem Patienten, der die Krankheit nicht hat/bevor die Diagnose gesichert ist
* #U27 ^property[0].code = #parent 
* #U27 ^property[0].valueCode = #U 
* #U27 ^property[1].code = #Relationships 
* #U27 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #U28 "Funktionseinschränkung/Behinderung (U)"
* #U28 ^definition = Inklusive: Nierentransplantation~ geringer Durchfluß Exklusive: Harninkontinenz U04 Kriterien: Funktionseinschränkung/Behinderung durch eine Erkrankung der Harnwege
* #U28 ^property[0].code = #parent 
* #U28 ^property[0].valueCode = #U 
* #U28 ^property[1].code = #Relationships 
* #U28 ^property[1].valueString = "ICD-10 2017:Z73.6~Z99.2" 
* #U28 ^property[2].code = #hints 
* #U28 ^property[2].valueString = "Beachte:  Hinweise: COOP/WONCA Charts eignen sich zur Dokumentation des funktionellen Status des Patienten (siehe Kapitel 8)" 
* #U29 "Andere Harnorgansymptome/-beschwerden"
* #U29 ^definition = Inklusive:  Exklusive: reizbare Blase/Blasenschmerzen U13~ Nierenpobleme U14 Kriterien:
* #U29 ^property[0].code = #parent 
* #U29 ^property[0].valueCode = #U 
* #U29 ^property[1].code = #Relationships 
* #U29 ^property[1].valueString = "ICD-10 2017:R39.8" 
* #U30 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? vollständig"
* #U30 ^property[0].code = #parent 
* #U30 ^property[0].valueCode = #U 
* #U31 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? teilweise"
* #U31 ^property[0].code = #parent 
* #U31 ^property[0].valueCode = #U 
* #U32 "Allergie-/Sensitivitätstestung"
* #U32 ^property[0].code = #parent 
* #U32 ^property[0].valueCode = #U 
* #U33 "Mikrobiologische/immunologische Untersuchung"
* #U33 ^property[0].code = #parent 
* #U33 ^property[0].valueCode = #U 
* #U34 "Blutuntersuchung"
* #U34 ^property[0].code = #parent 
* #U34 ^property[0].valueCode = #U 
* #U35 "Urinuntersuchung"
* #U35 ^property[0].code = #parent 
* #U35 ^property[0].valueCode = #U 
* #U36 "Stuhluntersuchung"
* #U36 ^property[0].code = #parent 
* #U36 ^property[0].valueCode = #U 
* #U37 "Histologische Untersuchung/zytologischer Abstrich"
* #U37 ^property[0].code = #parent 
* #U37 ^property[0].valueCode = #U 
* #U38 "Andere Laboruntersuchung NAK"
* #U38 ^property[0].code = #parent 
* #U38 ^property[0].valueCode = #U 
* #U39 "Körperliche Funktionsprüfung"
* #U39 ^property[0].code = #parent 
* #U39 ^property[0].valueCode = #U 
* #U40 "Diagnostische Endoskopie"
* #U40 ^property[0].code = #parent 
* #U40 ^property[0].valueCode = #U 
* #U41 "Diagnostisches Röntgen/Bildgebung"
* #U41 ^property[0].code = #parent 
* #U41 ^property[0].valueCode = #U 
* #U42 "Elektrische Aufzeichnungsverfahren"
* #U42 ^property[0].code = #parent 
* #U42 ^property[0].valueCode = #U 
* #U43 "Andere diagnostische Untersuchung"
* #U43 ^property[0].code = #parent 
* #U43 ^property[0].valueCode = #U 
* #U44 "Präventive Impfung/Medikation"
* #U44 ^property[0].code = #parent 
* #U44 ^property[0].valueCode = #U 
* #U45 "Beobachtung/Gesundheitsschulung/Beratung/Diät"
* #U45 ^property[0].code = #parent 
* #U45 ^property[0].valueCode = #U 
* #U46 "Konsultation eines anderen Primärversorgers"
* #U46 ^property[0].code = #parent 
* #U46 ^property[0].valueCode = #U 
* #U47 "Konsultation eines Facharztes"
* #U47 ^property[0].code = #parent 
* #U47 ^property[0].valueCode = #U 
* #U48 "Klärung/Besprechung des Beratungsanlasses mit dem Patienten"
* #U48 ^property[0].code = #parent 
* #U48 ^property[0].valueCode = #U 
* #U49 "Andere Vorsorgemaßnahme"
* #U49 ^property[0].code = #parent 
* #U49 ^property[0].valueCode = #U 
* #U50 "Medikation/Verschreibung/Erneuerung/Injektion"
* #U50 ^property[0].code = #parent 
* #U50 ^property[0].valueCode = #U 
* #U51 "Inzision/Drainage/Spülung/Absaugung/Entfernung von Körperflüssigkeit"
* #U51 ^property[0].code = #parent 
* #U51 ^property[0].valueCode = #U 
* #U52 "Exzision/Entfernung von Gewebe/Biopsie/Destruktion/Debridement/Verkauterung"
* #U52 ^property[0].code = #parent 
* #U52 ^property[0].valueCode = #U 
* #U53 "Instrumentelle Manipulation/Katheterisierung/Intubation/Dilatation"
* #U53 ^property[0].code = #parent 
* #U53 ^property[0].valueCode = #U 
* #U54 "Reposition/Fixierung/Naht/Gips/prothetische Versorgung"
* #U54 ^property[0].code = #parent 
* #U54 ^property[0].valueCode = #U 
* #U55 "Lokale Injektion/Infiltration"
* #U55 ^property[0].code = #parent 
* #U55 ^property[0].valueCode = #U 
* #U56 "Verband/Druck/Kompression/Tamponade"
* #U56 ^property[0].code = #parent 
* #U56 ^property[0].valueCode = #U 
* #U57 "Physikalische Therapie/Rehabilitation"
* #U57 ^property[0].code = #parent 
* #U57 ^property[0].valueCode = #U 
* #U58 "Therapeutische Beratung/Zuhören"
* #U58 ^property[0].code = #parent 
* #U58 ^property[0].valueCode = #U 
* #U59 "Andere therapeutische Maßnahme/kleine Chirurgie NAK"
* #U59 ^property[0].code = #parent 
* #U59 ^property[0].valueCode = #U 
* #U60 "Testresultat/Ergebnis eigene Maßnahme"
* #U60 ^property[0].code = #parent 
* #U60 ^property[0].valueCode = #U 
* #U61 "Ergebnis Untersuchung/Test/Brief von anderem Leistungserbringer"
* #U61 ^property[0].code = #parent 
* #U61 ^property[0].valueCode = #U 
* #U62 "Adminstrative Maßnahme"
* #U62 ^property[0].code = #parent 
* #U62 ^property[0].valueCode = #U 
* #U63 "Folgekonsultation unspezifiziert"
* #U63 ^property[0].code = #parent 
* #U63 ^property[0].valueCode = #U 
* #U64 "Konsultation/Problembehandlung auf Initiative des Leistungserbringers"
* #U64 ^property[0].code = #parent 
* #U64 ^property[0].valueCode = #U 
* #U65 "Konsultation/Problembehandlung auf Initiative Dritter"
* #U65 ^property[0].code = #parent 
* #U65 ^property[0].valueCode = #U 
* #U66 "Überweisung an andere/n Leistungserbringer (nicht ärztl.)/Pflegekraft/Therapeuten/Sozialarbeiter"
* #U66 ^property[0].code = #parent 
* #U66 ^property[0].valueCode = #U 
* #U67 "Überweisung an einen Arzt/Spezialisten/eine Klinik/ein Krankenhaus"
* #U67 ^property[0].code = #parent 
* #U67 ^property[0].valueCode = #U 
* #U68 "Andere Überweisung NAK"
* #U68 ^property[0].code = #parent 
* #U68 ^property[0].valueCode = #U 
* #U69 "Anderer Beratungsanlass NAK"
* #U69 ^property[0].code = #parent 
* #U69 ^property[0].valueCode = #U 
* #U70 "Pyelonephritis/Pyelitis"
* #U70 ^definition = Inklusive: Niereninfektion~ Nierenabszess~ perinephritischer Abszess Exklusive:  Kriterien: Zwei der folgenden Symptome: Schmerzen in der Lendengegend, oder Druckempfindlichkeit der Nierenlager, oder Untersuchungshinweis auf chronische Nierenschädigung; plus klinischer Laborhinweis auf eine "Infektion der Harnorgane
* #U70 ^property[0].code = #parent 
* #U70 ^property[0].valueCode = #U 
* #U70 ^property[1].code = #Relationships 
* #U70 ^property[1].valueString = "ICD-10 2017:N10~N11.0~N11.1~N11.8~N11.9~N12~N15.1~N15.9" 
* #U70 ^property[2].code = #hints 
* #U70 ^property[2].valueString = "Beachte: Zystitis/andere Harnwegsinfektion U71 Hinweise: " 
* #U71 "Zystitis/Harnwegsinfekt, anderer"
* #U71 ^definition = Inklusive: akute/chronische Zystitis (nicht venerisch)~ asymptomatische Bakteriurie~ Infektion der unteren Harnwege~ Infektion der Harnwege NNB Exklusive: Pyelonephritis U70~ Urethritis U72~ Vaginitis X84~ Balanitis Y75 Kriterien:
* #U71 ^property[0].code = #parent 
* #U71 ^property[0].valueCode = #U 
* #U71 ^property[1].code = #Relationships 
* #U71 ^property[1].valueString = "ICD-10 2017:N30.0~N30.1~N30.2~N30.3~N30.4~N30.8~N30.9~N39.0" 
* #U71 ^property[2].code = #hints 
* #U71 ^property[2].valueString = "Beachte:  Hinweise: Bei Schwangerschaft auch W84 kodieren" 
* #U72 "Urethritis"
* #U72 ^definition = Inklusive: Chlamydienurethritis~ unspezifische Urethritis~ Urethralsyndrom~ Meatitis Exklusive: Gonokokkenurethritis bei der Frau X71~ Trichomonadenurethritis bei der Frau X73~ Gonokokkenurethritis beim Mann Y71 Kriterien: Ausfluß aus der Harnröhre mit häufigem, brennendem, schmerzhaftem Harnlassen oder Harndrang ohne Nachweis von Bakteriurie durch Mikroskopie oder Kultur; oder Entzündung an der äußeren Harnröhrenöffnung
* #U72 ^property[0].code = #parent 
* #U72 ^property[0].valueCode = #U 
* #U72 ^property[1].code = #Relationships 
* #U72 ^property[1].valueString = "ICD-10 2017:A56.0~A56.2~B37.4~N34.0~N34.1~N34.2~N34.3" 
* #U72 ^property[2].code = #hints 
* #U72 ^property[2].valueString = "Beachte: schmerzhaftes Wasserlassen U01~ häufiges/dringendes Wasserlassen U02~ Reizblase U13~ Harnröhrenausfluß X29, Y03 Hinweise: " 
* #U75 "Bösartige Neubildung der Niere"
* #U75 ^definition = Inklusive:  Exklusive:  Kriterien: charakteristisches histologisches Erscheinungsbild
* #U75 ^property[0].code = #parent 
* #U75 ^property[0].valueCode = #U 
* #U75 ^property[1].code = #Relationships 
* #U75 ^property[1].valueString = "ICD-10 2017:C64~C65" 
* #U75 ^property[2].code = #hints 
* #U75 ^property[2].valueString = "Beachte: Neubildung der Harnwege NNB U79 Hinweise: " 
* #U76 "Bösartige Neubildung der Blase"
* #U76 ^definition = Inklusive:  Exklusive:  Kriterien: charakteristisches histologisches Erscheinungsbild
* #U76 ^property[0].code = #parent 
* #U76 ^property[0].valueCode = #U 
* #U76 ^property[1].code = #Relationships 
* #U76 ^property[1].valueString = "ICD-10 2017:C67.0~C67.1~C67.2~C67.3~C67.4~C67.5~C67.6~C67.7~C67.8~C67.9" 
* #U76 ^property[2].code = #hints 
* #U76 ^property[2].valueString = "Beachte: Neubildung der Harnwege NNB U79 Hinweise: " 
* #U77 "Bösartige Neubildung der Harnorgane, andere"
* #U77 ^definition = Inklusive: Bösartige Neubildung an Harnleiter~ Harnröhre Exklusive: bösartige Neubildung der Prostata Y77 Kriterien: charakteristisches histologisches Erscheinungsbild
* #U77 ^property[0].code = #parent 
* #U77 ^property[0].valueCode = #U 
* #U77 ^property[1].code = #Relationships 
* #U77 ^property[1].valueString = "ICD-10 2017:C66~C68.0~C68.1~C68.8~C68.9" 
* #U77 ^property[2].code = #hints 
* #U77 ^property[2].valueString = "Beachte: Neubildung der Harnwege NNB U79 Hinweise: " 
* #U78 "Gutartige Neubildung der Harnorgane"
* #U78 ^definition = Inklusive: Blasenpapillom~ Polyp der Harnwege Exklusive: Prostatahypertrophie Y85 Kriterien: charakteristisches histologisches Erscheinungsbild
* #U78 ^property[0].code = #parent 
* #U78 ^property[0].valueCode = #U 
* #U78 ^property[1].code = #Relationships 
* #U78 ^property[1].valueString = "ICD-10 2017:D30.0~D30.1~D30.2~D30.3~D30.4~D30.7~D30.9" 
* #U78 ^property[2].code = #hints 
* #U78 ^property[2].valueString = "Beachte: Neubildung der Harnwege NNB U79 Hinweise: " 
* #U79 "Neubildung der Harnorgane NNB"
* #U79 ^definition = Inklusive: Neubildung NNB der Blase/Niere/Harnleiter/Harnröhre Exklusive: histologisch gesicherte Neubildung des Harntraktes U75~ U76~ U77~ U78 Kriterien:
* #U79 ^property[0].code = #parent 
* #U79 ^property[0].valueCode = #U 
* #U79 ^property[1].code = #Relationships 
* #U79 ^property[1].valueString = "ICD-10 2017:D09.0~D09.1~D41.0~D41.1~D41.2~D41.3~D41.4~D41.7~D41.9" 
* #U80 "Verletzung der Harnorgane"
* #U80 ^definition = Inklusive: Nierenquetschung~ Fremdkörper in den Harnorganen Exklusive:  Kriterien:
* #U80 ^property[0].code = #parent 
* #U80 ^property[0].valueCode = #U 
* #U80 ^property[1].code = #Relationships 
* #U80 ^property[1].valueString = "ICD-10 2017:S37.0~S37.1~S37.2~S37.3~T19.0~T19.1~T28.3~T28.8" 
* #U85 "Angeborene Anomalie der Harnorgane"
* #U85 ^definition = Inklusive: Doppeniere/-ureter~ angeborene Zystenniere Exklusive:  Kriterien:
* #U85 ^property[0].code = #parent 
* #U85 ^property[0].valueCode = #U 
* #U85 ^property[1].code = #Relationships 
* #U85 ^property[1].valueString = "ICD-10 2017:Q60.0~Q60.1~Q60.2~Q60.3~Q60.4~Q60.5~Q60.6~Q61.0~Q61.1~Q61.2~Q61.3~Q61.4~Q61.5~Q61.8~Q61.9~Q62.0~Q62.1~Q62.2~Q62.3~Q62.4~Q62.5~Q62.6~Q62.7~Q62.8~Q63.0~Q63.1~Q63.2~Q63.3~Q63.8~Q63.9~Q64.0~Q64.1~Q64.2~Q64.3~Q64.4~Q64.5~Q64.6~Q64.7~Q64.8~Q64.9" 
* #U88 "Glomerulonephritis/Nephrose"
* #U88 ^definition = Inklusive: akute Glomerulonephritis~ Analgetikanephropathie~ chronische Glomerulonephritis~ Nephritis~ Nephropathie~ Nephrosklerose~ nephrotisches Syndrom Exklusive: Niereninsuffizienz U99 Kriterien: drei oder mehr der folgenden Symptome: Hämaturie, Proteinurie, Salz- und Wasserretention in den Nieren, verminderte Nierenfunktion, anhaltende Anomalien des Harnsediments; oder Bestätigung durch Nierenbiopsie
* #U88 ^property[0].code = #parent 
* #U88 ^property[0].valueCode = #U 
* #U88 ^property[1].code = #Relationships 
* #U88 ^property[1].valueString = "ICD-10 2017:N00.0~N00.1~N00.2~N00.3~N00.4~N00.5~N00.6~N00.7~N00.8~N00.9~N01.0~N01.1~N01.2~N01.3~N01.4~N01.5~N01.6~N01.7~N01.8~N01.9~N03.0~N03.1~N03.2~N03.3~N03.4~N03.5~N03.6~N03.7~N03.8~N03.9~N04.0~N04.1~N04.2~N04.3~N04.4~N04.5~N04.6~N04.7~N04.8~N04.9~N05.0~N05.1~N05.2~N05.3~N05.4~N05.5~N05.6~N05.7~N05.8~N05.9~N07.0~N07.1~N07.2~N07.3~N07.4~N07.5~N07.6~N07.7~N07.8~N07.9~N08.0~N08.1~N08.2~N08.3~N08.4~N08.5~N08.8~N14.0~N14.1~N14.2~N14.3~N14.4~N15.0~N15.8~N16.0~N16.1~N16.2~N16.3~N16.4~N16.5~N16.8" 
* #U88 ^property[2].code = #hints 
* #U88 ^property[2].valueString = "Beachte: abnormer Urintest U98~ Nierenbeschwerden U14 Hinweise: " 
* #U90 "Orthostatische Albuminurie/Proteinurie"
* #U90 ^definition = Inklusive: lagebedingte Proteinurie Exklusive:  Kriterien: Albuminurie nach Gehen, keine Albuminurie nach nächtlichem Liegen und kein Hinweis auf eine Nierenerkrankung
* #U90 ^property[0].code = #parent 
* #U90 ^property[0].valueCode = #U 
* #U90 ^property[1].code = #Relationships 
* #U90 ^property[1].valueString = "ICD-10 2017:N39.2" 
* #U90 ^property[2].code = #hints 
* #U90 ^property[2].valueString = "Beachte: Proteinurie NNB U98 Hinweise: " 
* #U95 "Harnstein"
* #U95 ^definition = Inklusive: Steine oder Griess in Blase/ Niere/ Harnleiter~ Urolithiasis Exklusive:  Kriterien: Kolikartige Schmerzen und entweder Hämaturie oder Vorgeschichte von Harnsteinen; oder Abgehen von Harnsteinen; oder Nachweis von Harnsteinen in bildgebenden Verfahren
* #U95 ^property[0].code = #parent 
* #U95 ^property[0].valueCode = #U 
* #U95 ^property[1].code = #Relationships 
* #U95 ^property[1].valueString = "ICD-10 2017:N20.0~N20.1~N20.2~N20.9~N21.0~N21.1~N21.8~N21.9~N22.0~N22.8" 
* #U95 ^property[2].code = #hints 
* #U95 ^property[2].valueString = "Beachte: Blut im Urin U06~ Nierenkolik U14~ andere Harnwegsbeschwerden U29~ abnormer Urintest U98 Hinweise: " 
* #U98 "Auffälliger Urintest NNB"
* #U98 ^definition = Inklusive: Glykosurie~ Proteinurie~ Leukozyturie~ Pyurie Exklusive: Hämaturie/Blut im Urin U06~ orthostatische Albuminurie/Proteinurie U90 Kriterien:
* #U98 ^property[0].code = #parent 
* #U98 ^property[0].valueCode = #U 
* #U98 ^property[1].code = #Relationships 
* #U98 ^property[1].valueString = "ICD-10 2017:N39.1~R80~R81~R82.0~R82.1~R82.2~R82.3~R82.4~R82.5~R82.6~R82.7~R82.8~R82.9" 
* #U99 "Andere Erkrankung der Harnorgane"
* #U99 ^definition = Inklusive: Blasendivertikel~ Hydronephrose~ hypertrophe Niere~ Blasenhalsobstruktion~ Niereninsuffizienz~ Karbunkel an der Harnröhre~ Harnröhrenstriktur~ Uretralreflux~ Urämie Exklusive:  Kriterien:
* #U99 ^property[0].code = #parent 
* #U99 ^property[0].valueCode = #U 
* #U99 ^property[1].code = #Relationships 
* #U99 ^property[1].valueString = "ICD-10 2017:N06.0~N06.1~N06.2~N06.3~N06.4~N06.5~N06.6~N06.7~N06.8~N06.9~N13.0~N13.1~N13.2~N13.3~N13.4~N13.5~N13.6~N13.7~N13.8~N13.9~N17.0~N17.1~N17.2~N17.8~N17.9~N18.1~N18.2~N18.3~N18.4~N18.5~N18.9~N19~N25.0~N25.1~N25.8~N25.9~N26~N27.0~N27.1~N27.9~N28.0~N28.1~N28.8~N28.9~N29.0~N29.1~N29.8~N31.0~N31.1~N31.2~N31.8~N31.9~N32.0~N32.1~N32.2~N32.3~N32.4~N32.8~N32.9~N33.0~N33.8~N35.0~N35.1~N35.8~N35.9~N36.0~N36.1~N36.2~N36.3~N36.8~N36.9~N37.0~N37.8~N39.8~N39.9~R39.2~T19.8~T19.9~Z90.5~Z90.6" 
* #W "Schwangerschaft, Geburt, Familienplanung"
* #W ^property[0].code = #child 
* #W ^property[0].valueCode = #W01 
* #W ^property[1].code = #child 
* #W ^property[1].valueCode = #W02 
* #W ^property[2].code = #child 
* #W ^property[2].valueCode = #W03 
* #W ^property[3].code = #child 
* #W ^property[3].valueCode = #W05 
* #W ^property[4].code = #child 
* #W ^property[4].valueCode = #W10 
* #W ^property[5].code = #child 
* #W ^property[5].valueCode = #W11 
* #W ^property[6].code = #child 
* #W ^property[6].valueCode = #W12 
* #W ^property[7].code = #child 
* #W ^property[7].valueCode = #W13 
* #W ^property[8].code = #child 
* #W ^property[8].valueCode = #W14 
* #W ^property[9].code = #child 
* #W ^property[9].valueCode = #W15 
* #W ^property[10].code = #child 
* #W ^property[10].valueCode = #W17 
* #W ^property[11].code = #child 
* #W ^property[11].valueCode = #W18 
* #W ^property[12].code = #child 
* #W ^property[12].valueCode = #W19 
* #W ^property[13].code = #child 
* #W ^property[13].valueCode = #W21 
* #W ^property[14].code = #child 
* #W ^property[14].valueCode = #W27 
* #W ^property[15].code = #child 
* #W ^property[15].valueCode = #W28 
* #W ^property[16].code = #child 
* #W ^property[16].valueCode = #W29 
* #W ^property[17].code = #child 
* #W ^property[17].valueCode = #W30 
* #W ^property[18].code = #child 
* #W ^property[18].valueCode = #W31 
* #W ^property[19].code = #child 
* #W ^property[19].valueCode = #W32 
* #W ^property[20].code = #child 
* #W ^property[20].valueCode = #W33 
* #W ^property[21].code = #child 
* #W ^property[21].valueCode = #W34 
* #W ^property[22].code = #child 
* #W ^property[22].valueCode = #W35 
* #W ^property[23].code = #child 
* #W ^property[23].valueCode = #W36 
* #W ^property[24].code = #child 
* #W ^property[24].valueCode = #W37 
* #W ^property[25].code = #child 
* #W ^property[25].valueCode = #W38 
* #W ^property[26].code = #child 
* #W ^property[26].valueCode = #W39 
* #W ^property[27].code = #child 
* #W ^property[27].valueCode = #W40 
* #W ^property[28].code = #child 
* #W ^property[28].valueCode = #W41 
* #W ^property[29].code = #child 
* #W ^property[29].valueCode = #W43 
* #W ^property[30].code = #child 
* #W ^property[30].valueCode = #W44 
* #W ^property[31].code = #child 
* #W ^property[31].valueCode = #W45 
* #W ^property[32].code = #child 
* #W ^property[32].valueCode = #W46 
* #W ^property[33].code = #child 
* #W ^property[33].valueCode = #W47 
* #W ^property[34].code = #child 
* #W ^property[34].valueCode = #W48 
* #W ^property[35].code = #child 
* #W ^property[35].valueCode = #W49 
* #W ^property[36].code = #child 
* #W ^property[36].valueCode = #W50 
* #W ^property[37].code = #child 
* #W ^property[37].valueCode = #W51 
* #W ^property[38].code = #child 
* #W ^property[38].valueCode = #W52 
* #W ^property[39].code = #child 
* #W ^property[39].valueCode = #W53 
* #W ^property[40].code = #child 
* #W ^property[40].valueCode = #W54 
* #W ^property[41].code = #child 
* #W ^property[41].valueCode = #W55 
* #W ^property[42].code = #child 
* #W ^property[42].valueCode = #W56 
* #W ^property[43].code = #child 
* #W ^property[43].valueCode = #W57 
* #W ^property[44].code = #child 
* #W ^property[44].valueCode = #W58 
* #W ^property[45].code = #child 
* #W ^property[45].valueCode = #W59 
* #W ^property[46].code = #child 
* #W ^property[46].valueCode = #W60 
* #W ^property[47].code = #child 
* #W ^property[47].valueCode = #W61 
* #W ^property[48].code = #child 
* #W ^property[48].valueCode = #W62 
* #W ^property[49].code = #child 
* #W ^property[49].valueCode = #W63 
* #W ^property[50].code = #child 
* #W ^property[50].valueCode = #W64 
* #W ^property[51].code = #child 
* #W ^property[51].valueCode = #W65 
* #W ^property[52].code = #child 
* #W ^property[52].valueCode = #W66 
* #W ^property[53].code = #child 
* #W ^property[53].valueCode = #W67 
* #W ^property[54].code = #child 
* #W ^property[54].valueCode = #W68 
* #W ^property[55].code = #child 
* #W ^property[55].valueCode = #W69 
* #W ^property[56].code = #child 
* #W ^property[56].valueCode = #W70 
* #W ^property[57].code = #child 
* #W ^property[57].valueCode = #W71 
* #W ^property[58].code = #child 
* #W ^property[58].valueCode = #W72 
* #W ^property[59].code = #child 
* #W ^property[59].valueCode = #W73 
* #W ^property[60].code = #child 
* #W ^property[60].valueCode = #W75 
* #W ^property[61].code = #child 
* #W ^property[61].valueCode = #W76 
* #W ^property[62].code = #child 
* #W ^property[62].valueCode = #W78 
* #W ^property[63].code = #child 
* #W ^property[63].valueCode = #W79 
* #W ^property[64].code = #child 
* #W ^property[64].valueCode = #W80 
* #W ^property[65].code = #child 
* #W ^property[65].valueCode = #W81 
* #W ^property[66].code = #child 
* #W ^property[66].valueCode = #W82 
* #W ^property[67].code = #child 
* #W ^property[67].valueCode = #W83 
* #W ^property[68].code = #child 
* #W ^property[68].valueCode = #W84 
* #W ^property[69].code = #child 
* #W ^property[69].valueCode = #W85 
* #W ^property[70].code = #child 
* #W ^property[70].valueCode = #W90 
* #W ^property[71].code = #child 
* #W ^property[71].valueCode = #W91 
* #W ^property[72].code = #child 
* #W ^property[72].valueCode = #W92 
* #W ^property[73].code = #child 
* #W ^property[73].valueCode = #W93 
* #W ^property[74].code = #child 
* #W ^property[74].valueCode = #W94 
* #W ^property[75].code = #child 
* #W ^property[75].valueCode = #W95 
* #W ^property[76].code = #child 
* #W ^property[76].valueCode = #W96 
* #W ^property[77].code = #child 
* #W ^property[77].valueCode = #W99 
* #W01 "Fragliche Schwangerschaft"
* #W01 ^definition = Inklusive: verspätete Regelblutung~ Symptome~ die auf Schwangerschaft hinweisen Exklusive: Angst vor Schwangerschaft W02~ bestätigte Schwangerschaft W78~ W79 Kriterien:
* #W01 ^property[0].code = #parent 
* #W01 ^property[0].valueCode = #W 
* #W01 ^property[1].code = #Relationships 
* #W01 ^property[1].valueString = "ICD-10 2017:Z32.0" 
* #W02 "Angst vor Schwangerschaft"
* #W02 ^definition = Inklusive: Furcht vor Möglichkeit ungewollter Schwangerschaft Exklusive: Sorge/Furcht bei Bestätigung einer ungewollten Schwangerschaft W79 Kriterien:
* #W02 ^property[0].code = #parent 
* #W02 ^property[0].valueCode = #W 
* #W02 ^property[1].code = #Relationships 
* #W02 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #W03 "Blutung in der Schwangerschaft"
* #W03 ^definition = Inklusive:  Exklusive: intrapartale Blutung W17 Kriterien:
* #W03 ^property[0].code = #parent 
* #W03 ^property[0].valueCode = #W 
* #W03 ^property[1].code = #Relationships 
* #W03 ^property[1].valueString = "ICD-10 2017:O20.0~O20.8~O20.9~O46.0~O46.8~O46.9" 
* #W05 "Schwangerschaftsbedingte Übelkeit/Erbrechen"
* #W05 ^definition = Inklusive: Hyperemesis~ morgentliche Übelkeit bei bestätigter Schwangerschaft Exklusive:  Kriterien:
* #W05 ^property[0].code = #parent 
* #W05 ^property[0].valueCode = #W 
* #W05 ^property[1].code = #Relationships 
* #W05 ^property[1].valueString = "ICD-10 2017:O21.0~O21.1~O21.2~O21.8~O21.9" 
* #W10 "Postkoitale Empfängnisverhütung"
* #W10 ^definition = Inklusive: Pille danach Exklusive:  Kriterien:
* #W10 ^property[0].code = #parent 
* #W10 ^property[0].valueCode = #W 
* #W10 ^property[1].code = #Relationships 
* #W10 ^property[1].valueString = "ICD-10 2017:Z30.3" 
* #W11 "Orale Empfängnisverhütung"
* #W11 ^definition = Inklusive: Familienplanung bei Frauen mit oraler hormoneller Kontrazeption Exklusive:  Kriterien:
* #W11 ^property[0].code = #parent 
* #W11 ^property[0].valueCode = #W 
* #W11 ^property[1].code = #Relationships 
* #W11 ^property[1].valueString = "ICD-10 2017:Z30.4" 
* #W12 "Intrauterine Empfängnisverhütung"
* #W12 ^definition = Inklusive: Familienplanung mittels Intrauterinpessar/Spirale Exklusive:  Kriterien:
* #W12 ^property[0].code = #parent 
* #W12 ^property[0].valueCode = #W 
* #W12 ^property[1].code = #Relationships 
* #W12 ^property[1].valueString = "ICD-10 2017:Z30.1~Z30.5" 
* #W13 "Sterilisierung Frau"
* #W13 ^definition = Inklusive: Familienplanung durch Sterilisierung der Frau Exklusive:  Kriterien:
* #W13 ^property[0].code = #parent 
* #W13 ^property[0].valueCode = #W 
* #W13 ^property[1].code = #Relationships 
* #W13 ^property[1].valueString = "ICD-10 2017:Z30.2" 
* #W14 "Empfängnisverhütung Frau, andere"
* #W14 ^definition = Inklusive: Empfängnisverhütung NNB~ Familienplanung NNB Exklusive: genetische Beratung A98~ orale Empfängnisverhütung W11~ intrauterine Empfängnisverhütung W12~ Familienplanung durch Sterilisation der Frau W13 Kriterien:
* #W14 ^property[0].code = #parent 
* #W14 ^property[0].valueCode = #W 
* #W14 ^property[1].code = #Relationships 
* #W14 ^property[1].valueString = "ICD-10 2017:Z30.0~Z30.8~Z30.9" 
* #W15 "Infertilität/Subfertilität Frau"
* #W15 ^definition = Inklusive: primäre und sekundäre Sterilität Exklusive:  Kriterien: Keine Empfängnis trotz zweijährigen ungeschützten Verkehrs
* #W15 ^property[0].code = #parent 
* #W15 ^property[0].valueCode = #W 
* #W15 ^property[1].code = #Relationships 
* #W15 ^property[1].valueString = "ICD-10 2017:N97.0~N97.1~N97.2~N97.3~N97.4~N97.8~N97.9~Z31.0~Z31.1~Z31.2~Z31.3~Z31.4~Z31.6~Z31.8~Z31.9" 
* #W15 ^property[2].code = #hints 
* #W15 ^property[2].valueString = "Beachte: andere Symptome/Beschwerden hinsichtlich Schwangerschaft W29 Hinweise: " 
* #W17 "Postpartale Blutung"
* #W17 ^definition = Inklusive:  Exklusive:  Kriterien: starke Blutung bei oder innerhalb von 6 Wochen nach der Entbindung
* #W17 ^property[0].code = #parent 
* #W17 ^property[0].valueCode = #W 
* #W17 ^property[1].code = #Relationships 
* #W17 ^property[1].valueString = "ICD-10 2017:O72.0~O72.1~O72.2~O72.3" 
* #W17 ^property[2].code = #hints 
* #W17 ^property[2].valueString = "Beachte: andere post-partale Beschwerden W18 Hinweise: " 
* #W18 "Postpartale Symptome/Beschwerden, andere"
* #W18 ^definition = Inklusive:  Exklusive: Wochenbettdepression P76~ postpartale Blutung W17~ Stillbeschwerden W19~ Wochenbettkomplikationen W96 Kriterien: Beschwerden, die bei oder innerhalb von 6 Wochen nach der Entbindung auftreten
* #W18 ^property[0].code = #parent 
* #W18 ^property[0].valueCode = #W 
* #W18 ^property[1].code = #Relationships 
* #W18 ^property[1].valueString = "ICD-10 2017:O90.9" 
* #W19 "Brust-/Stillsymptome/-beschwerden"
* #W19 ^definition = Inklusive: Galactorrhoe~ Unterdrückung der Laktation~ Abstillen Exklusive: puerperale Mastitis W94~ aufgesprungene Brustwarzen W95 Kriterien:
* #W19 ^property[0].code = #parent 
* #W19 ^property[0].valueCode = #W 
* #W19 ^property[1].code = #Relationships 
* #W19 ^property[1].valueString = "ICD-10 2017:O92.5~O92.6~O92.7" 
* #W21 "Sorge um schwangerschaftbedingtes Aussehen"
* #W21 ^property[0].code = #parent 
* #W21 ^property[0].valueCode = #W 
* #W21 ^property[1].code = #Relationships 
* #W21 ^property[1].valueString = "ICD-10 2017:R46.8" 
* #W27 "Angst vor Schwangerschaftskomplikationen"
* #W27 ^definition = Inklusive: Angst vor angeborener Mißbildung des Neugeborenen Exklusive: wenn die Patienin die Komplikation hat~ ist die Erkrankung zu kodieren Kriterien: Besorgnis über/Angst vor Schwangerschaftkomplikationen bei einer Patientin, die keine Komplikationen hat/bevor Komplikationen gesichert sind
* #W27 ^property[0].code = #parent 
* #W27 ^property[0].valueCode = #W 
* #W27 ^property[1].code = #Relationships 
* #W27 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #W28 "Funktionseinschränkung/Behinderung (W)"
* #W28 ^definition = Inklusive: instabiles Becken Exklusive:  Kriterien: Funktionseinschränkung/Behinderung in Verbindung mit einer Schwangerschaft
* #W28 ^property[0].code = #parent 
* #W28 ^property[0].valueCode = #W 
* #W28 ^property[1].code = #Relationships 
* #W28 ^property[1].valueString = "ICD-10 2017:Z73.6" 
* #W28 ^property[2].code = #hints 
* #W28 ^property[2].valueString = "Beachte:  Hinweise: COOP/WONCA Charts eignen sich zur Dokumentation des funktionellen Status des Patienten (siehe Kapitel 8)" 
* #W29 "Andere Schwangerschaftssymptome/-beschwerden"
* #W29 ^definition = Inklusive: Symptome/Beschwerden im Bereich Familienplanung Exklusive:  Kriterien:
* #W29 ^property[0].code = #parent 
* #W29 ^property[0].valueCode = #W 
* #W29 ^property[1].code = #Relationships 
* #W29 ^property[1].valueString = "ICD-10 2017:O26.0~O26.1~O26.2~O26.3~O26.5" 
* #W30 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? vollständig"
* #W30 ^property[0].code = #parent 
* #W30 ^property[0].valueCode = #W 
* #W31 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? teilweise"
* #W31 ^property[0].code = #parent 
* #W31 ^property[0].valueCode = #W 
* #W32 "Allergie-/Sensitivitätstestung"
* #W32 ^property[0].code = #parent 
* #W32 ^property[0].valueCode = #W 
* #W33 "Mikrobiologische/immunologische Untersuchung"
* #W33 ^property[0].code = #parent 
* #W33 ^property[0].valueCode = #W 
* #W34 "Blutuntersuchung"
* #W34 ^property[0].code = #parent 
* #W34 ^property[0].valueCode = #W 
* #W35 "Urinuntersuchung"
* #W35 ^property[0].code = #parent 
* #W35 ^property[0].valueCode = #W 
* #W36 "Stuhluntersuchung"
* #W36 ^property[0].code = #parent 
* #W36 ^property[0].valueCode = #W 
* #W37 "Histologische Untersuchung/zytologischer Abstrich"
* #W37 ^property[0].code = #parent 
* #W37 ^property[0].valueCode = #W 
* #W38 "Andere Laboruntersuchung NAK"
* #W38 ^property[0].code = #parent 
* #W38 ^property[0].valueCode = #W 
* #W39 "Körperliche Funktionsprüfung"
* #W39 ^property[0].code = #parent 
* #W39 ^property[0].valueCode = #W 
* #W40 "Diagnostische Endoskopie"
* #W40 ^property[0].code = #parent 
* #W40 ^property[0].valueCode = #W 
* #W41 "Diagnostisches Röntgen/Bildgebung"
* #W41 ^property[0].code = #parent 
* #W41 ^property[0].valueCode = #W 
* #W43 "Andere diagnostische Untersuchung"
* #W43 ^property[0].code = #parent 
* #W43 ^property[0].valueCode = #W 
* #W44 "Präventive Impfung/Medikation"
* #W44 ^property[0].code = #parent 
* #W44 ^property[0].valueCode = #W 
* #W45 "Beobachtung/Gesundheitsschulung/Beratung/Diät"
* #W45 ^property[0].code = #parent 
* #W45 ^property[0].valueCode = #W 
* #W46 "Konsultation eines anderen Primärversorgers"
* #W46 ^property[0].code = #parent 
* #W46 ^property[0].valueCode = #W 
* #W47 "Konsultation eines Facharztes"
* #W47 ^property[0].code = #parent 
* #W47 ^property[0].valueCode = #W 
* #W48 "Klärung/Besprechung des Beratungsanlasses mit dem Patienten"
* #W48 ^property[0].code = #parent 
* #W48 ^property[0].valueCode = #W 
* #W49 "Andere Vorsorgemaßnahme"
* #W49 ^property[0].code = #parent 
* #W49 ^property[0].valueCode = #W 
* #W50 "Medikation/Verschreibung/Erneuerung/Injektion"
* #W50 ^property[0].code = #parent 
* #W50 ^property[0].valueCode = #W 
* #W51 "Inzision/Drainage/Spülung/Absaugung/Entfernung von Körperflüssigkeit"
* #W51 ^property[0].code = #parent 
* #W51 ^property[0].valueCode = #W 
* #W52 "Exzision/Entfernung von Gewebe/Biopsie/Destruktion/Debridement/Verkauterung"
* #W52 ^property[0].code = #parent 
* #W52 ^property[0].valueCode = #W 
* #W53 "Instrumentelle Manipulation/Katheterisierung/Intubation/Dilatation"
* #W53 ^property[0].code = #parent 
* #W53 ^property[0].valueCode = #W 
* #W54 "Reposition/Fixierung/Naht/Gips/prothetische Versorgung"
* #W54 ^property[0].code = #parent 
* #W54 ^property[0].valueCode = #W 
* #W55 "Lokale Injektion/Infiltration"
* #W55 ^property[0].code = #parent 
* #W55 ^property[0].valueCode = #W 
* #W56 "Verband/Druck/Kompression/Tamponade"
* #W56 ^property[0].code = #parent 
* #W56 ^property[0].valueCode = #W 
* #W57 "Physikalische Therapie/Rehabilitation"
* #W57 ^property[0].code = #parent 
* #W57 ^property[0].valueCode = #W 
* #W58 "Therapeutische Beratung/Zuhören"
* #W58 ^property[0].code = #parent 
* #W58 ^property[0].valueCode = #W 
* #W59 "Andere therapeutische Maßnahme/kleine Chirurgie NAK"
* #W59 ^property[0].code = #parent 
* #W59 ^property[0].valueCode = #W 
* #W60 "Testresultat/Ergebnis eigene Maßnahme"
* #W60 ^property[0].code = #parent 
* #W60 ^property[0].valueCode = #W 
* #W61 "Ergebnis Untersuchung/Test/Brief von anderem Leistungserbringer"
* #W61 ^property[0].code = #parent 
* #W61 ^property[0].valueCode = #W 
* #W62 "Adminstrative Maßnahme"
* #W62 ^property[0].code = #parent 
* #W62 ^property[0].valueCode = #W 
* #W63 "Folgekonsultation unspezifiziert"
* #W63 ^property[0].code = #parent 
* #W63 ^property[0].valueCode = #W 
* #W64 "Konsultation/Problembehandlung auf Initiative des Leistungserbringers"
* #W64 ^property[0].code = #parent 
* #W64 ^property[0].valueCode = #W 
* #W65 "Konsultation/Problembehandlung auf Initiative Dritter"
* #W65 ^property[0].code = #parent 
* #W65 ^property[0].valueCode = #W 
* #W66 "Überweisung an andere/n Leistungserbringer (nicht ärztl.)/Pflegekraft/Therapeuten/Sozialarbeiter"
* #W66 ^property[0].code = #parent 
* #W66 ^property[0].valueCode = #W 
* #W67 "Überweisung an einen Arzt/Spezialisten/eine Klinik/ein Krankenhaus"
* #W67 ^property[0].code = #parent 
* #W67 ^property[0].valueCode = #W 
* #W68 "Andere Überweisung NAK"
* #W68 ^property[0].code = #parent 
* #W68 ^property[0].valueCode = #W 
* #W69 "Anderer Beratungsanlass NAK"
* #W69 ^property[0].code = #parent 
* #W69 ^property[0].valueCode = #W 
* #W70 "Wochenbettinfektion/-sepsis"
* #W70 ^definition = Inklusive:  Exklusive: Neugeborenentetanus N72 Kriterien: Infektion des Geburtskanals oder der Fortpflanzungsorgane innerhalb von 6 Wochen nach Entbindung
* #W70 ^property[0].code = #parent 
* #W70 ^property[0].valueCode = #W 
* #W70 ^property[1].code = #Relationships 
* #W70 ^property[1].valueString = "ICD-10 2017:O85~O86.1~O86.3" 
* #W71 "Komplikation während Schwangerschaft/Wochenbett durch andere Infektion"
* #W71 ^definition = Inklusive:  Exklusive: Puerperalinfektion W70~ puerperale Mastitis W94 Kriterien:
* #W71 ^property[0].code = #parent 
* #W71 ^property[0].valueCode = #W 
* #W71 ^property[1].code = #Relationships 
* #W71 ^property[1].valueString = "ICD-10 2017:O23.0~O23.1~O23.2~O23.3~O23.4~O23.5~O23.9~O26.4~O41.1~O75.2~O75.3~O86.2~O86.4~O86.8~O98.0~O98.1~O98.2~O98.3~O98.4~O98.5~O98.6~O98.7~O98.8~O98.9" 
* #W72 "Bösartige Neubildung verbunden mit Schwangerschaft"
* #W72 ^definition = Inklusive: Chorionepitheliom~ Choriokarzinom Exklusive:  Kriterien:
* #W72 ^property[0].code = #parent 
* #W72 ^property[0].valueCode = #W 
* #W72 ^property[1].code = #Relationships 
* #W72 ^property[1].valueString = "ICD-10 2017:C58" 
* #W73 "Gutartige/nicht spezifizierte Neubildung verbunden mit Schwangerschaft"
* #W73 ^definition = Inklusive: Blasenmole Exklusive:  Kriterien:
* #W73 ^property[0].code = #parent 
* #W73 ^property[0].valueCode = #W 
* #W73 ^property[1].code = #Relationships 
* #W73 ^property[1].valueString = "ICD-10 2017:O01.0~O01.1~O01.9" 
* #W75 "Komplikation der Schwangerschaft durch Verletzung"
* #W75 ^definition = Inklusive: Verletzungsfolgen~ die mit der Schwangerschaft interferieren Exklusive: neue Verletzung durch die Geburt W92~ W93 Kriterien:
* #W75 ^property[0].code = #parent 
* #W75 ^property[0].valueCode = #W 
* #W75 ^property[1].code = #Relationships 
* #W75 ^property[1].valueString = "ICD-10 2017:T14.9" 
* #W76 "Komplikation der Schwangerschaft durch angeborene Anomalie"
* #W76 ^definition = Inklusive: Anomalien im Mutterleib~ die Schwangerschaft oder Geburt beeinträchtigen können Exklusive:  Kriterien:
* #W76 ^property[0].code = #parent 
* #W76 ^property[0].valueCode = #W 
* #W76 ^property[1].code = #Relationships 
* #W76 ^property[1].valueString = "ICD-10 2017:O99.8" 
* #W78 "Schwangerschaft"
* #W78 ^definition = Inklusive: bestätigte Schwangerschaft Exklusive: ungewollte Schwangerschaft W79~ ektopische Schwangerschaft W80~ Risikoschwangerschaft W84 Kriterien:
* #W78 ^property[0].code = #parent 
* #W78 ^property[0].valueCode = #W 
* #W78 ^property[1].code = #Relationships 
* #W78 ^property[1].valueString = "ICD-10 2017:Z32.1~Z33~Z34.0~Z34.8~Z34.9~Z36.0~Z36.1~Z36.2~Z36.3~Z36.4~Z36.5~Z36.8~Z36.9" 
* #W79 "Ungewollte Schwangerschaft"
* #W79 ^property[0].code = #parent 
* #W79 ^property[0].valueCode = #W 
* #W79 ^property[1].code = #Relationships 
* #W79 ^property[1].valueString = "ICD-10 2017:Z32.1" 
* #W80 "Ektopische Schwangerschaft"
* #W80 ^definition = Inklusive:  Exklusive:  Kriterien: Bestätigung durch Ultraschall, Laparoskopie, Kuldoskopie oder chirurgischen Eingriff
* #W80 ^property[0].code = #parent 
* #W80 ^property[0].valueCode = #W 
* #W80 ^property[1].code = #Relationships 
* #W80 ^property[1].valueString = "ICD-10 2017:O00.0~O00.1~O00.2~O00.8~O00.9" 
* #W80 ^property[2].code = #hints 
* #W80 ^property[2].valueString = "Beachte: vorgeburtliche Blutung W03~ andere Symptome/Beschwerden während der Schwangerschaft W29 Hinweise: " 
* #W81 "Schwangerschaftstoxikose"
* #W81 ^definition = Inklusive: Eklampsie~ Hypertonus~ Ödem und Proteinurie in der Schwangerschaft~ Präeklampsie Exklusive:  Kriterien:
* #W81 ^property[0].code = #parent 
* #W81 ^property[0].valueCode = #W 
* #W81 ^property[1].code = #Relationships 
* #W81 ^property[1].valueString = "ICD-10 2017:O10~O11~O12.0~O12.1~O12.2~O13~O14.0~O14.1~O14.2~O14.9~O15.0~O15.1~O15.2~O15.9~O16" 
* #W81 ^property[2].code = #hints 
* #W81 ^property[2].valueString = "Beachte: andere Symptom/Beschwerden während der Schwangerschaft W29 Hinweise: " 
* #W82 "Spontanabort"
* #W82 ^definition = Inklusive: drohender/kompletter/inkompletter/unbemerkter/habitueller Abort~ Fehlgeburt Exklusive: antepartale Blutung W03~ induzierter Abort W83~ vorzeitige Wehen nach der 28. Schwangerschaftswoche W92~ fötaler Tod/Todgeburt nach der 28. Schwangerschaftswoche W93 Kriterien:
* #W82 ^property[0].code = #parent 
* #W82 ^property[0].valueCode = #W 
* #W82 ^property[1].code = #Relationships 
* #W82 ^property[1].valueString = "ICD-10 2017:N96~O02.0~O02.1~O02.8~O02.9~O03.0~O03.1~O03.2~O03.3~O03.4~O03.5~O03.6~O03.7~O03.8~O03.9~O05.0~O05.1~O05.2~O05.3~O05.4~O05.5~O05.6~O05.7~O05.8~O05.9~O06.0~O06.1~O06.2~O06.3~O06.4~O06.5~O06.6~O06.7~O06.8~O06.9" 
* #W83 "Induzierter Abort"
* #W83 ^definition = Inklusive: Beendigung der Schwangerschaft~ alle Komplikationen Exklusive:  Kriterien:
* #W83 ^property[0].code = #parent 
* #W83 ^property[0].valueCode = #W 
* #W83 ^property[1].code = #Relationships 
* #W83 ^property[1].valueString = "ICD-10 2017:O04.0~O04.1~O04.2~O04.3~O04.4~O04.5~O04.6~O04.7~O04.8~O04.9~Z30.3" 
* #W84 "Hochrisikoschwangerschaft"
* #W84 ^definition = Inklusive: Erstgeburt in fortgeschrittenem Alter~ Schwangerschaftsanämie~ Diabetes oder andere vorbestehende Krankheiten~ die die Schwangerschaft beeinträchtigen können~ enges Becken~ Hydramnion~ anomale Kindslage~ Mehrlingsschwangerschaft~ Placenta praevia~ früher Kaiserschnitt~ zu kleiner Fötus für sein Entwicklungsstadium (small for date fetus) Exklusive: Infektionskomplikation einer Schwangerschaft W71~ ektopische Schwangerschaft W80~ Schwangerschaftstoxikämie W81~ Gestationsdiabetes W85 Kriterien:
* #W84 ^property[0].code = #parent 
* #W84 ^property[0].valueCode = #W 
* #W84 ^property[1].code = #Relationships 
* #W84 ^property[1].valueString = "ICD-10 2017:O24.0~O24.1~O24.2~O24.3~O24.9~O25~O30.0~O30.1~O30.2~O30.8~O30.9~O31.0~O31.1~O31.2~O31.8~O32.0~O32.1~O32.2~O32.3~O32.4~O32.5~O32.6~O32.8~O32.9~O33.0~O33.1~O33.2~O33.3~O33.4~O33.5~O33.6~O33.7~O33.8~O33.9~O34.0~O34.1~O34.2~O34.3~O34.4~O34.5~O34.6~O34.7~O34.8~O34.9~O35.0~O35.1~O35.2~O35.3~O35.4~O35.5~O35.6~O35.7~O35.8~O35.9~O36.0~O36.1~O36.2~O36.3~O36.4~O36.5~O36.6~O36.7~O36.8~O36.9~O40~O43.0~O43.1~O43.2~O43.8~O43.9~O44.0~O44.1~O60.0~O99.0~O99.1~O99.2~O99.3~O99.4~O99.5~O99.6~O99.7~O99.8~Z35.0~Z35.1~Z35.2~Z35.3~Z35.4~Z35.5~Z35.6~Z35.7~Z35.8~Z35.9" 
* #W85 "Schwangerschaftsdiabetes"
* #W85 ^definition = Inklusive: Diabetes mit Erstmanifestation während der Schwangerschaft Exklusive: vorbestehender Diabetes T89~T90 Kriterien: Blutzucker-Nüchternwerte über 100mg/dl und/oder Blutzuckerwerte über 180mg/dl - bei Verwenung venösen Vollblutes - nach zwei Stunden bei einem oralen Glukosetoleranztest (75g)
* #W85 ^property[0].code = #parent 
* #W85 ^property[0].valueCode = #W 
* #W85 ^property[1].code = #Relationships 
* #W85 ^property[1].valueString = "ICD-10 2017:O24.4" 
* #W85 ^property[2].code = #hints 
* #W85 ^property[2].valueString = "Beachte: Hyperglykämie A91 Hinweise: " 
* #W90 "Unkomplizierte Wehentätigkeit/Entbindung Lebendgeburt"
* #W90 ^property[0].code = #parent 
* #W90 ^property[0].valueCode = #W 
* #W90 ^property[1].code = #Relationships 
* #W90 ^property[1].valueString = "ICD-10 2017:O80.0~O80.1~O80.8~O80.9~Z37.0~Z37.9~Z38.0~Z38.1~Z38.2~Z38.3~Z38.4~Z38.5~Z38.6~Z38.7~Z38.8~Z39.0~Z39.1~Z39.2" 
* #W91 "Unkomplizierte Wehentätigkeit/Entbindung Totgeburt"
* #W91 ^property[0].code = #parent 
* #W91 ^property[0].valueCode = #W 
* #W91 ^property[1].code = #Relationships 
* #W91 ^property[1].valueString = "ICD-10 2017:Z37.1~Z37.9" 
* #W92 "Komplizierte Wehentätigkeit/Entbindung Lebendgeburt"
* #W92 ^definition = Inklusive: Lebendgeburt nach komplizierter Entbindung: manuell oder apparativ unterstützte Geburt~ Geburt aus Beckenendlage~ Kaiserschnitt~ Asphyxie~ Weheneinleitung~ Geburtsverletzung~ Placenta praevia~ innere Wendung Exklusive: postpartale Blutung W17~Eklampsie W81 Kriterien:
* #W92 ^property[0].code = #parent 
* #W92 ^property[0].valueCode = #W 
* #W92 ^property[1].code = #Relationships 
* #W92 ^property[1].valueString = "ICD-10 2017:O42.0~O42.1~O42.2~O42.9~O45.0~O45.8~O45.9~O60.1~O60.2~O60.3~O61.0~O61.1~O61.8~O61.9~O62.0~O62.1~O62.2~O62.3~O62.4~O62.8~O62.9~O63.0~O63.1~O63.2~O63.9~O64.0~O64.1~O64.2~O64.3~O64.4~O64.5~O64.8~O64.9~O65.0~O65.1~O65.2~O65.3~O65.4~O65.5~O65.8~O65.9~O66.0~O66.1~O66.2~O66.3~O66.4~O66.5~O66.8~O66.9~O67.0~O67.8~O67.9~O68.0~O68.1~O68.2~O68.3~O68.8~O68.9~O69.0~O69.1~O69.2~O69.3~O69.4~O69.5~O69.8~O69.9~O70.0~O70.1~O70.2~O70.3~O70.9~O71.0~O71.1~O71.2~O71.3~O71.4~O71.5~O71.6~O71.7~O71.8~O71.9~O73.0~O73.1~O75.0~O75.1~O75.4~O75.5~O75.6~O75.7~O75.8~O75.9~O81.0~O81.1~O81.2~O81.3~O81.4~O81.5~O82.0~O82.1~O82.2~O82.8~O82.9~O83.0~O83.1~O83.2~O83.3~O83.4~O83.8~O83.9~O84.0~O84.1~O84.2~O84.8~O84.9~Z37.2~Z37.5~Z37.9~Z38.0~Z38.1~Z38.2~Z38.3~Z38.4~Z38.5~Z38.6~Z38.7~Z38.8~Z39.0~Z39.1~Z39.2" 
* #W93 "Komplizierte Wehentätigkeit/Entbindung Totgeburt"
* #W93 ^definition = Inklusive: Totgeburt nach komplizierter Entbindung: manuell oder apparativ unterstützte Geburt~ Geburt aus Beckenendlage~ Kaiserschnitt~ Asphyxie~ Weheneinleitung~ Geburtsverletzung~ Placenta praevia~ innere Wendung Exklusive: postpartale Blutung W17~Eklampsie W81 Kriterien:
* #W93 ^property[0].code = #parent 
* #W93 ^property[0].valueCode = #W 
* #W93 ^property[1].code = #Relationships 
* #W93 ^property[1].valueString = "ICD-10 2017:O42.0~O42.1~O42.2~O42.9~O45.0~O45.8~O45.9~O60.1~O60.2~O60.3~O61.0~O61.1~O61.8~O61.9~O62.0~O62.1~O62.2~O62.3~O62.4~O62.8~O62.9~O63.0~O63.1~O63.2~O63.9~O64.0~O64.1~O64.2~O64.3~O64.4~O64.5~O64.8~O64.9~O65.0~O65.1~O65.2~O65.3~O65.4~O65.5~O65.8~O65.9~O66.0~O66.1~O66.2~O66.3~O66.4~O66.5~O66.8~O66.9~O67.0~O67.8~O67.9~O68.0~O68.1~O68.2~O68.3~O68.8~O68.9~O69.0~O69.1~O69.2~O69.3~O69.4~O69.5~O69.8~O69.9~O70.0~O70.1~O70.2~O70.3~O70.9~O71.0~O71.1~O71.2~O71.3~O71.4~O71.5~O71.6~O71.7~O71.8~O71.9~O73.0~O73.1~O75.0~O75.1~O75.4~O75.5~O75.6~O75.7~O75.8~O75.9~O81.0~O81.1~O81.2~O81.3~O81.4~O81.5~O82.0~O82.1~O82.2~O82.8~O82.9~O83.0~O83.1~O83.2~O83.3~O83.4~O83.8~O83.9~O84.0~O84.1~O84.2~O84.8~O84.9~Z37.1~Z37.3~Z37.4~Z37.6~Z37.7~Z37.9" 
* #W94 "Wochenbettmastitis"
* #W94 ^definition = Inklusive: Brustabszess Exklusive:  Kriterien: Schmerzen/Entzündung der Brust beim Stillen innerhalb von 6 Wochen nach Entbindung
* #W94 ^property[0].code = #parent 
* #W94 ^property[0].valueCode = #W 
* #W94 ^property[1].code = #Relationships 
* #W94 ^property[1].valueString = "ICD-10 2017:O91.0~O91.1~O91.2" 
* #W94 ^property[2].code = #hints 
* #W94 ^property[2].valueString = "Beachte: Lactationsstörungen W19 Hinweise: " 
* #W95 "Brusterkrankung während Schwangerschaft/Wochenbett, andere"
* #W95 ^definition = Inklusive: Brustbeschwerden im Wochenbett~ Brustwarzenrhagaden Exklusive: Stillbeschwerden W19~ Mastitis W94~ Brustprobleme ohne Bezug zu Schwangerschaft/Stillen X21 Kriterien:
* #W95 ^property[0].code = #parent 
* #W95 ^property[0].valueCode = #W 
* #W95 ^property[1].code = #Relationships 
* #W95 ^property[1].valueString = "ICD-10 2017:O92.0~O92.1~O92.2~O92.3~O92.4" 
* #W96 "Komplikation Wochenbett, andere"
* #W96 ^definition = Inklusive:  Exklusive: Wochenbettdepression P76~ Wochenbettpsychose P98~ Wochenbettinfektion W70~ Schwangerschaftstoxämie W81~ Brustbeschwerden in der Schwangerschaft W95 Kriterien:
* #W96 ^property[0].code = #parent 
* #W96 ^property[0].valueCode = #W 
* #W96 ^property[1].code = #Relationships 
* #W96 ^property[1].valueString = "ICD-10 2017:O87.0~O87.1~O87.2~O87.3~O87.8~O87.9~O90.4~O90.8~O90.9" 
* #W99 "Andere Störung Schwangerschaft/Entbindung"
* #W99 ^definition = Inklusive:  Exklusive: Scheinschwangerschaft P75 Kriterien:
* #W99 ^property[0].code = #parent 
* #W99 ^property[0].valueCode = #W 
* #W99 ^property[1].code = #Relationships 
* #W99 ^property[1].valueString = "ICD-10 2017:O07.0~O07.1~O07.2~O07.3~O07.4~O07.5~O07.6~O07.7~O07.8~O07.9~O08.0~O08.1~O08.2~O08.3~O08.4~O08.5~O08.6~O08.7~O08.8~O08.9~O22.0~O22.1~O22.2~O22.3~O22.4~O22.5~O22.8~O22.9~O26.6~O26.7~O26.8~O26.9~O28.0~O28.1~O28.2~O28.3~O28.4~O28.5~O28.8~O28.9~O41.0~O41.8~O41.9~O47.0~O47.1~O47.9~O48~O88.0~O88.1~O88.2~O88.3~O88.8~O90.5~O94~O96.0~O96.1~O96.9~O97" 
* #X "Weibliches Genitale"
* #X ^property[0].code = #child 
* #X ^property[0].valueCode = #X01 
* #X ^property[1].code = #child 
* #X ^property[1].valueCode = #X02 
* #X ^property[2].code = #child 
* #X ^property[2].valueCode = #X03 
* #X ^property[3].code = #child 
* #X ^property[3].valueCode = #X04 
* #X ^property[4].code = #child 
* #X ^property[4].valueCode = #X05 
* #X ^property[5].code = #child 
* #X ^property[5].valueCode = #X06 
* #X ^property[6].code = #child 
* #X ^property[6].valueCode = #X07 
* #X ^property[7].code = #child 
* #X ^property[7].valueCode = #X08 
* #X ^property[8].code = #child 
* #X ^property[8].valueCode = #X09 
* #X ^property[9].code = #child 
* #X ^property[9].valueCode = #X10 
* #X ^property[10].code = #child 
* #X ^property[10].valueCode = #X11 
* #X ^property[11].code = #child 
* #X ^property[11].valueCode = #X12 
* #X ^property[12].code = #child 
* #X ^property[12].valueCode = #X13 
* #X ^property[13].code = #child 
* #X ^property[13].valueCode = #X14 
* #X ^property[14].code = #child 
* #X ^property[14].valueCode = #X15 
* #X ^property[15].code = #child 
* #X ^property[15].valueCode = #X16 
* #X ^property[16].code = #child 
* #X ^property[16].valueCode = #X17 
* #X ^property[17].code = #child 
* #X ^property[17].valueCode = #X18 
* #X ^property[18].code = #child 
* #X ^property[18].valueCode = #X19 
* #X ^property[19].code = #child 
* #X ^property[19].valueCode = #X20 
* #X ^property[20].code = #child 
* #X ^property[20].valueCode = #X21 
* #X ^property[21].code = #child 
* #X ^property[21].valueCode = #X22 
* #X ^property[22].code = #child 
* #X ^property[22].valueCode = #X23 
* #X ^property[23].code = #child 
* #X ^property[23].valueCode = #X24 
* #X ^property[24].code = #child 
* #X ^property[24].valueCode = #X25 
* #X ^property[25].code = #child 
* #X ^property[25].valueCode = #X26 
* #X ^property[26].code = #child 
* #X ^property[26].valueCode = #X27 
* #X ^property[27].code = #child 
* #X ^property[27].valueCode = #X28 
* #X ^property[28].code = #child 
* #X ^property[28].valueCode = #X29 
* #X ^property[29].code = #child 
* #X ^property[29].valueCode = #X30 
* #X ^property[30].code = #child 
* #X ^property[30].valueCode = #X31 
* #X ^property[31].code = #child 
* #X ^property[31].valueCode = #X32 
* #X ^property[32].code = #child 
* #X ^property[32].valueCode = #X33 
* #X ^property[33].code = #child 
* #X ^property[33].valueCode = #X34 
* #X ^property[34].code = #child 
* #X ^property[34].valueCode = #X35 
* #X ^property[35].code = #child 
* #X ^property[35].valueCode = #X36 
* #X ^property[36].code = #child 
* #X ^property[36].valueCode = #X37 
* #X ^property[37].code = #child 
* #X ^property[37].valueCode = #X38 
* #X ^property[38].code = #child 
* #X ^property[38].valueCode = #X39 
* #X ^property[39].code = #child 
* #X ^property[39].valueCode = #X40 
* #X ^property[40].code = #child 
* #X ^property[40].valueCode = #X41 
* #X ^property[41].code = #child 
* #X ^property[41].valueCode = #X42 
* #X ^property[42].code = #child 
* #X ^property[42].valueCode = #X43 
* #X ^property[43].code = #child 
* #X ^property[43].valueCode = #X44 
* #X ^property[44].code = #child 
* #X ^property[44].valueCode = #X45 
* #X ^property[45].code = #child 
* #X ^property[45].valueCode = #X46 
* #X ^property[46].code = #child 
* #X ^property[46].valueCode = #X47 
* #X ^property[47].code = #child 
* #X ^property[47].valueCode = #X48 
* #X ^property[48].code = #child 
* #X ^property[48].valueCode = #X49 
* #X ^property[49].code = #child 
* #X ^property[49].valueCode = #X50 
* #X ^property[50].code = #child 
* #X ^property[50].valueCode = #X51 
* #X ^property[51].code = #child 
* #X ^property[51].valueCode = #X52 
* #X ^property[52].code = #child 
* #X ^property[52].valueCode = #X53 
* #X ^property[53].code = #child 
* #X ^property[53].valueCode = #X54 
* #X ^property[54].code = #child 
* #X ^property[54].valueCode = #X55 
* #X ^property[55].code = #child 
* #X ^property[55].valueCode = #X56 
* #X ^property[56].code = #child 
* #X ^property[56].valueCode = #X57 
* #X ^property[57].code = #child 
* #X ^property[57].valueCode = #X58 
* #X ^property[58].code = #child 
* #X ^property[58].valueCode = #X59 
* #X ^property[59].code = #child 
* #X ^property[59].valueCode = #X60 
* #X ^property[60].code = #child 
* #X ^property[60].valueCode = #X61 
* #X ^property[61].code = #child 
* #X ^property[61].valueCode = #X62 
* #X ^property[62].code = #child 
* #X ^property[62].valueCode = #X63 
* #X ^property[63].code = #child 
* #X ^property[63].valueCode = #X64 
* #X ^property[64].code = #child 
* #X ^property[64].valueCode = #X65 
* #X ^property[65].code = #child 
* #X ^property[65].valueCode = #X66 
* #X ^property[66].code = #child 
* #X ^property[66].valueCode = #X67 
* #X ^property[67].code = #child 
* #X ^property[67].valueCode = #X68 
* #X ^property[68].code = #child 
* #X ^property[68].valueCode = #X69 
* #X ^property[69].code = #child 
* #X ^property[69].valueCode = #X70 
* #X ^property[70].code = #child 
* #X ^property[70].valueCode = #X71 
* #X ^property[71].code = #child 
* #X ^property[71].valueCode = #X72 
* #X ^property[72].code = #child 
* #X ^property[72].valueCode = #X73 
* #X ^property[73].code = #child 
* #X ^property[73].valueCode = #X74 
* #X ^property[74].code = #child 
* #X ^property[74].valueCode = #X75 
* #X ^property[75].code = #child 
* #X ^property[75].valueCode = #X76 
* #X ^property[76].code = #child 
* #X ^property[76].valueCode = #X77 
* #X ^property[77].code = #child 
* #X ^property[77].valueCode = #X78 
* #X ^property[78].code = #child 
* #X ^property[78].valueCode = #X79 
* #X ^property[79].code = #child 
* #X ^property[79].valueCode = #X80 
* #X ^property[80].code = #child 
* #X ^property[80].valueCode = #X81 
* #X ^property[81].code = #child 
* #X ^property[81].valueCode = #X82 
* #X ^property[82].code = #child 
* #X ^property[82].valueCode = #X83 
* #X ^property[83].code = #child 
* #X ^property[83].valueCode = #X84 
* #X ^property[84].code = #child 
* #X ^property[84].valueCode = #X85 
* #X ^property[85].code = #child 
* #X ^property[85].valueCode = #X86 
* #X ^property[86].code = #child 
* #X ^property[86].valueCode = #X87 
* #X ^property[87].code = #child 
* #X ^property[87].valueCode = #X88 
* #X ^property[88].code = #child 
* #X ^property[88].valueCode = #X89 
* #X ^property[89].code = #child 
* #X ^property[89].valueCode = #X90 
* #X ^property[90].code = #child 
* #X ^property[90].valueCode = #X91 
* #X ^property[91].code = #child 
* #X ^property[91].valueCode = #X92 
* #X ^property[92].code = #child 
* #X ^property[92].valueCode = #X99 
* #X01 "Genitalschmerz, Frau"
* #X01 ^definition = Inklusive: Schmerzen im Becken/ in der Vulva Exklusive: Menstruationsschmerzen X02~ Dyspareunie bei der Frau X04~ Brustschmerzen bei der Frau X18 Kriterien:
* #X01 ^property[0].code = #parent 
* #X01 ^property[0].valueCode = #X 
* #X01 ^property[1].code = #Relationships 
* #X01 ^property[1].valueString = "ICD-10 2017:N94.8~R10.2" 
* #X02 "Menstruationsschmerz"
* #X02 ^definition = Inklusive: Dysmenorrhoe Exklusive:  Kriterien:
* #X02 ^property[0].code = #parent 
* #X02 ^property[0].valueCode = #X 
* #X02 ^property[1].code = #Relationships 
* #X02 ^property[1].valueString = "ICD-10 2017:N94.4~N94.5~N94.6" 
* #X03 "Intermenstruelle Schmerzen"
* #X03 ^definition = Inklusive: Mittelschmerz~ Ovulationsschmerz Exklusive:  Kriterien:
* #X03 ^property[0].code = #parent 
* #X03 ^property[0].valueCode = #X 
* #X03 ^property[1].code = #Relationships 
* #X03 ^property[1].valueString = "ICD-10 2017:N94.0" 
* #X04 "Schmerzen beim Geschlechtsverkehr, Frau"
* #X04 ^definition = Inklusive: Dyspareunie der Frau~ Vaginismus NNB Exklusive: psychogene sexuelle Pobleme P07~ P08 Kriterien:
* #X04 ^property[0].code = #parent 
* #X04 ^property[0].valueCode = #X 
* #X04 ^property[1].code = #Relationships 
* #X04 ^property[1].valueString = "ICD-10 2017:N94.1~N94.2" 
* #X05 "Fehlende/spärliche Menstruation"
* #X05 ^definition = Inklusive: Amenorrhoe~ verzögerte oder verspätete Regelblutung~ Oligomenorrhoe Exklusive: Frage einer Schwangerschaft W01~ Angst vor Schwangerschaft W02 Kriterien:
* #X05 ^property[0].code = #parent 
* #X05 ^property[0].valueCode = #X 
* #X05 ^property[1].code = #Relationships 
* #X05 ^property[1].valueString = "ICD-10 2017:N91.0~N91.1~N91.2~N91.3~N91.4~N91.5" 
* #X06 "Übermäßige Menstruation"
* #X06 ^definition = Inklusive: Menorrhagie~ pubertäre Blutung Exklusive:  Kriterien:
* #X06 ^property[0].code = #parent 
* #X06 ^property[0].valueCode = #X 
* #X06 ^property[1].code = #Relationships 
* #X06 ^property[1].valueString = "ICD-10 2017:N92.0~N92.2~N92.4" 
* #X07 "Unregelmäßige/häufige Menstruation"
* #X07 ^definition = Inklusive: Polymenorrhoe Exklusive: Menorrhagie/pubertäre Blutung X06 Kriterien:
* #X07 ^property[0].code = #parent 
* #X07 ^property[0].valueCode = #X 
* #X07 ^property[1].code = #Relationships 
* #X07 ^property[1].valueString = "ICD-10 2017:N92.0~N92.1~N92.5~N92.6" 
* #X08 "Zwischenblutung"
* #X08 ^definition = Inklusive: Durchbruchsblutung~ dysfunktionale Gebärmutterblutung~ Metrorrhagie~ Ovulationsblutung~ "spotting" Exklusive: postmenopausale Blutung X12~ postkoitale Blutung X13 Kriterien:
* #X08 ^property[0].code = #parent 
* #X08 ^property[0].valueCode = #X 
* #X08 ^property[1].code = #Relationships 
* #X08 ^property[1].valueString = "ICD-10 2017:N92.3~N93.8~N93.9" 
* #X09 "Prämenstruelle Symptome/Beschwerden"
* #X09 ^definition = Inklusive:  Exklusive: Prämenstrulle Beschwerden X89 Kriterien:
* #X09 ^property[0].code = #parent 
* #X09 ^property[0].valueCode = #X 
* #X09 ^property[1].code = #Relationships 
* #X09 ^property[1].valueString = "ICD-10 2017:N94.9" 
* #X10 "Hinausschieben der Menstruation"
* #X10 ^definition = Inklusive:  Exklusive:  Kriterien: Hinausschieben der erwarteten regelmäßigen Menstruation durch Hormonbehandlung
* #X10 ^property[0].code = #parent 
* #X10 ^property[0].valueCode = #X 
* #X10 ^property[1].code = #Relationships 
* #X10 ^property[1].valueString = "ICD-10 2017:Z30.9" 
* #X11 "Menopausale Symptome/Beschwerden"
* #X11 ^definition = Inklusive: atrophische Colpitis~ klimakterisches Syndrom~ Symptome/Beschwerden durch das Klimakterium~ senile Colpitis Exklusive: postmenopausale Blutung X12 Kriterien:
* #X11 ^property[0].code = #parent 
* #X11 ^property[0].valueCode = #X 
* #X11 ^property[1].code = #Relationships 
* #X11 ^property[1].valueString = "ICD-10 2017:N95.1~N95.2~N95.3~N95.8~N95.9" 
* #X12 "Postmenopausale Blutung"
* #X12 ^definition = Inklusive:  Exklusive:  Kriterien: Vaginale Blutung entweder nach einer mindestens 6 Monate andauernden Amenorrhoe oder nach Nachweis der Menopause durch entsprechende Labortests
* #X12 ^property[0].code = #parent 
* #X12 ^property[0].valueCode = #X 
* #X12 ^property[1].code = #Relationships 
* #X12 ^property[1].valueString = "ICD-10 2017:N95.0" 
* #X12 ^property[2].code = #hints 
* #X12 ^property[2].valueString = "Beachte: unregelmäßige Menstruation X07 Hinweise: " 
* #X13 "Postkoitale Blutung"
* #X13 ^definition = Inklusive: Blutung beim Verkehr Exklusive:  Kriterien:
* #X13 ^property[0].code = #parent 
* #X13 ^property[0].valueCode = #X 
* #X13 ^property[1].code = #Relationships 
* #X13 ^property[1].valueString = "ICD-10 2017:N93.0" 
* #X14 "Vaginaler Ausfluss"
* #X14 ^definition = Inklusive: Fluor vaginalis~ Leukorrhoe Exklusive: Blutung X06~ X07~ X08~ atrophische Vaginitis X11~ Gonorrhoea bei der Frau X71~ urogenitaler Soor bei der Frau X72~ urogenitale Trichomoniasis bei der Frau X73~ Chlamydieninfektion bei der Frau X92 Kriterien:
* #X14 ^property[0].code = #parent 
* #X14 ^property[0].valueCode = #X 
* #X14 ^property[1].code = #Relationships 
* #X14 ^property[1].valueString = "ICD-10 2017:N89.8" 
* #X15 "Vaginale Symptome/Beschwerden, andere"
* #X15 ^definition = Inklusive: vaginale Trockenheit Exklusive: weiblicher Genitalschmerz X01~ organischer Vaginismus X04~ atrophische Vaginitis X11 Kriterien:
* #X15 ^property[0].code = #parent 
* #X15 ^property[0].valueCode = #X 
* #X15 ^property[1].code = #Relationships 
* #X15 ^property[1].valueString = "ICD-10 2017:N89.9" 
* #X16 "Vulvasymptome/-beschwerden"
* #X16 ^definition = Inklusive: Jucken an der Vulva~ Trockenheit der Vulva Exklusive: Vulvaschmerzen X01~ Vulvaabszes X99 Kriterien:
* #X16 ^property[0].code = #parent 
* #X16 ^property[0].valueCode = #X 
* #X16 ^property[1].code = #Relationships 
* #X16 ^property[1].valueString = "ICD-10 2017:L29.2~N90.9" 
* #X17 "Unterbauch-/Unterleibsymptome/-beschwerden, weiblich"
* #X17 ^definition = Inklusive:  Exklusive: genitale Schmerzen bei der Frau X01 Kriterien:
* #X17 ^property[0].code = #parent 
* #X17 ^property[0].valueCode = #X 
* #X17 ^property[1].code = #Relationships 
* #X17 ^property[1].valueString = "ICD-10 2017:N94.9" 
* #X18 "Brustschmerz, Frau"
* #X18 ^definition = Inklusive: Mastodynie Exklusive: Brustschmerzen in Schwangerschaft/Stillzeit W19 Kriterien: Besorgnis über/Angst vor Krebs der Harnwege bei einem Patienten, der die Krankheit nicht hat/bevor die Diagnose gesichert ist
* #X18 ^property[0].code = #parent 
* #X18 ^property[0].valueCode = #X 
* #X18 ^property[1].code = #Relationships 
* #X18 ^property[1].valueString = "ICD-10 2017:N64.4" 
* #X19 "Knoten, Tastbefund weibliche Brust"
* #X19 ^definition = Inklusive: zystische Mastopathie Exklusive:  Kriterien: Besorgnis über/Angst vor einer anderen Erkrankung der Harnwege bei einem Patienten, der die Krankheit nicht hat/bevor die Diagnose gesichert ist
* #X19 ^property[0].code = #parent 
* #X19 ^property[0].valueCode = #X 
* #X19 ^property[1].code = #Relationships 
* #X19 ^property[1].valueString = "ICD-10 2017:N63" 
* #X20 "Brustwarzensymptome/-beschwerden, Frau"
* #X20 ^definition = Inklusive: Ausfluß aus den Brustwarzen~ Rissigkeit~ Schmerzen~ Jucken~ Retraktion Exklusive: Burstwarzensymptome/-beschwerden in Schwangerschaft/Stillzeiz W19 Kriterien: Funktionseinschränkung/Behinderung durch eine Erkrankung der Harnwege
* #X20 ^property[0].code = #parent 
* #X20 ^property[0].valueCode = #X 
* #X20 ^property[1].code = #Relationships 
* #X20 ^property[1].valueString = "ICD-10 2017:N64.0~N64.5" 
* #X21 "Brustsymptome/-beschwerden Frau, andere"
* #X21 ^definition = Inklusive: Mastitis (nicht in Laktationsphase)~ Mastopathie~ Galaktorrhoe Exklusive: Mastitis (in Lactationsphase) W94 Kriterien:
* #X21 ^property[0].code = #parent 
* #X21 ^property[0].valueCode = #X 
* #X21 ^property[1].code = #Relationships 
* #X21 ^property[1].valueString = "ICD-10 2017:N61~N62~N64.3~N64.5~N64.9" 
* #X22 "Sorge um Aussehen der weiblichen Brust"
* #X22 ^property[0].code = #parent 
* #X22 ^property[0].valueCode = #X 
* #X22 ^property[1].code = #Relationships 
* #X22 ^property[1].valueString = "ICD-10 2017:R46.8" 
* #X23 "Angst vor sexuell übertragbaren Krankheiten, Frau"
* #X23 ^definition = Inklusive:  Exklusive: Angst vor HIV/AIDS B25~ wenn der Patient die Erkrankung hat~ ist die Erkrankung zu kodieren Kriterien: Besorgnis über/Angst vor einer sexuell übertragbaren Erkrankung bei einer Patientin, die die Krankheit nicht hat/bevor die Diagnose gesichert ist
* #X23 ^property[0].code = #parent 
* #X23 ^property[0].valueCode = #X 
* #X23 ^property[1].code = #Relationships 
* #X23 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #X24 "Angst vor sexueller Funktionsstörung, Frau"
* #X24 ^definition = Inklusive:  Exklusive: sexuelle Funktionsstörung P07~ P08 Kriterien: Besorgnis über/Angst vor sexueller Dysfunktion bei einer Patientin, die keine sexuelle Dysfunktion hat
* #X24 ^property[0].code = #parent 
* #X24 ^property[0].valueCode = #X 
* #X24 ^property[1].code = #Relationships 
* #X24 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #X25 "Angst vor Genitalkrebs, Frau"
* #X25 ^definition = Inklusive:  Exklusive: wenn die Patientin die Erkrankung hat~ ist die Erkrankung zu kodieren Kriterien: Besorgnis über/Angst vor Krebs des weiblichen Genitales bei einer Patientin, die die Krankheit nicht hat/bevor die Diagnose gesichert ist
* #X25 ^property[0].code = #parent 
* #X25 ^property[0].valueCode = #X 
* #X25 ^property[1].code = #Relationships 
* #X25 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #X26 "Angst vor Brustkrebs, Frau"
* #X26 ^definition = Inklusive:  Exklusive: wenn die Patientin die Erkrankung hat~ ist die Erkrankung zu kodieren Kriterien: Besorgnis über/Angst vor Brustkrebs bei einer Patientin, die die Krankheit nicht hat/bevor die Diagnose gesichert ist
* #X26 ^property[0].code = #parent 
* #X26 ^property[0].valueCode = #X 
* #X26 ^property[1].code = #Relationships 
* #X26 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #X27 "Angst vor Genital-/Brusterkrankung Frau, andere"
* #X27 ^definition = Inklusive:  Exklusive: Angst vor Genitalkrebs bei der Frau X25~ Angst Brustkrebs bei der Frau X26~ wenn die Patientin die Erkrankung hat~ ist die Erkrankung zu kodieren Kriterien: Besorgnis über/Angst vor einer anderen Erkrankung des weiblichen Genitales/der Brust bei einer Patientin, die die Krankheit nicht hat/bevor die Diagnose gesichert ist
* #X27 ^property[0].code = #parent 
* #X27 ^property[0].valueCode = #X 
* #X27 ^property[1].code = #Relationships 
* #X27 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #X28 "Funktionseinschränkung/Behinderung (X)"
* #X28 ^definition = Inklusive:  Exklusive: sexuelle Funktionsstörung P07~ P08~ schmerzhafte Geschlechtsverkehr bei der Frau/ Vaginismus X04 Kriterien: Funktionseinschränkung/Behinderung durch eine Erkrankung des weiblichen Genitalsystems (einschließlich Brust)
* #X28 ^property[0].code = #parent 
* #X28 ^property[0].valueCode = #X 
* #X28 ^property[1].code = #Relationships 
* #X28 ^property[1].valueString = "ICD-10 2017:Z73.6~Z90.7" 
* #X28 ^property[2].code = #hints 
* #X28 ^property[2].valueString = "Beachte:  Hinweise: COOP/WONCA Charts eignen sich zur Dokumentation des funktionellen Status des Patienten (siehe Kapitel 8)" 
* #X29 "Andere Genitalsymptome/-beschwerden Frau"
* #X29 ^definition = Inklusive: Ausfluß aus der Harnröhre bei der Frau Exklusive:  Kriterien:
* #X29 ^property[0].code = #parent 
* #X29 ^property[0].valueCode = #X 
* #X29 ^property[1].code = #Relationships 
* #X29 ^property[1].valueString = "ICD-10 2017:N94.9~R36" 
* #X30 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? vollständig"
* #X30 ^property[0].code = #parent 
* #X30 ^property[0].valueCode = #X 
* #X31 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? teilweise"
* #X31 ^property[0].code = #parent 
* #X31 ^property[0].valueCode = #X 
* #X32 "Allergie-/Sensitivitätstestung"
* #X32 ^property[0].code = #parent 
* #X32 ^property[0].valueCode = #X 
* #X33 "Mikrobiologische/immunologische Untersuchung"
* #X33 ^property[0].code = #parent 
* #X33 ^property[0].valueCode = #X 
* #X34 "Blutuntersuchung"
* #X34 ^property[0].code = #parent 
* #X34 ^property[0].valueCode = #X 
* #X35 "Urinuntersuchung"
* #X35 ^property[0].code = #parent 
* #X35 ^property[0].valueCode = #X 
* #X36 "Stuhluntersuchung"
* #X36 ^property[0].code = #parent 
* #X36 ^property[0].valueCode = #X 
* #X37 "Histologische Untersuchung/zytologischer Abstrich"
* #X37 ^property[0].code = #parent 
* #X37 ^property[0].valueCode = #X 
* #X38 "Andere Laboruntersuchung NAK"
* #X38 ^property[0].code = #parent 
* #X38 ^property[0].valueCode = #X 
* #X39 "Körperliche Funktionsprüfung"
* #X39 ^property[0].code = #parent 
* #X39 ^property[0].valueCode = #X 
* #X40 "Diagnostische Endoskopie"
* #X40 ^property[0].code = #parent 
* #X40 ^property[0].valueCode = #X 
* #X41 "Diagnostisches Röntgen/Bildgebung"
* #X41 ^property[0].code = #parent 
* #X41 ^property[0].valueCode = #X 
* #X42 "Elektrische Aufzeichnungsverfahren"
* #X42 ^property[0].code = #parent 
* #X42 ^property[0].valueCode = #X 
* #X43 "Andere diagnostische Untersuchung"
* #X43 ^property[0].code = #parent 
* #X43 ^property[0].valueCode = #X 
* #X44 "Präventive Impfung/Medikation"
* #X44 ^property[0].code = #parent 
* #X44 ^property[0].valueCode = #X 
* #X45 "Beobachtung/Gesundheitsschulung/Beratung/Diät"
* #X45 ^property[0].code = #parent 
* #X45 ^property[0].valueCode = #X 
* #X46 "Konsultation eines anderen Primärversorgers"
* #X46 ^property[0].code = #parent 
* #X46 ^property[0].valueCode = #X 
* #X47 "Konsultation eines Facharztes"
* #X47 ^property[0].code = #parent 
* #X47 ^property[0].valueCode = #X 
* #X48 "Klärung/Besprechung des Beratungsanlasses mit dem Patienten"
* #X48 ^property[0].code = #parent 
* #X48 ^property[0].valueCode = #X 
* #X49 "Andere Vorsorgemaßnahme"
* #X49 ^property[0].code = #parent 
* #X49 ^property[0].valueCode = #X 
* #X50 "Medikation/Verschreibung/Erneuerung/Injektion"
* #X50 ^property[0].code = #parent 
* #X50 ^property[0].valueCode = #X 
* #X51 "Inzision/Drainage/Spülung/Absaugung/Entfernung von Körperflüssigkeit"
* #X51 ^property[0].code = #parent 
* #X51 ^property[0].valueCode = #X 
* #X52 "Exzision/Entfernung von Gewebe/Biopsie/Destruktion/Debridement/Verkauterung"
* #X52 ^property[0].code = #parent 
* #X52 ^property[0].valueCode = #X 
* #X53 "Instrumentelle Manipulation/Katheterisierung/Intubation/Dilatation"
* #X53 ^property[0].code = #parent 
* #X53 ^property[0].valueCode = #X 
* #X54 "Reposition/Fixierung/Naht/Gips/prothetische Versorgung"
* #X54 ^property[0].code = #parent 
* #X54 ^property[0].valueCode = #X 
* #X55 "Lokale Injektion/Infiltration"
* #X55 ^property[0].code = #parent 
* #X55 ^property[0].valueCode = #X 
* #X56 "Verband/Druck/Kompression/Tamponade"
* #X56 ^property[0].code = #parent 
* #X56 ^property[0].valueCode = #X 
* #X57 "Physikalische Therapie/Rehabilitation"
* #X57 ^property[0].code = #parent 
* #X57 ^property[0].valueCode = #X 
* #X58 "Therapeutische Beratung/Zuhören"
* #X58 ^property[0].code = #parent 
* #X58 ^property[0].valueCode = #X 
* #X59 "Andere therapeutische Maßnahme/kleine Chirurgie NAK"
* #X59 ^property[0].code = #parent 
* #X59 ^property[0].valueCode = #X 
* #X60 "Testresultat/Ergebnis eigene Maßnahme"
* #X60 ^property[0].code = #parent 
* #X60 ^property[0].valueCode = #X 
* #X61 "Ergebnis Untersuchung/Test/Brief von anderem Leistungserbringer"
* #X61 ^property[0].code = #parent 
* #X61 ^property[0].valueCode = #X 
* #X62 "Adminstrative Maßnahme"
* #X62 ^property[0].code = #parent 
* #X62 ^property[0].valueCode = #X 
* #X63 "Folgekonsultation unspezifiziert"
* #X63 ^property[0].code = #parent 
* #X63 ^property[0].valueCode = #X 
* #X64 "Konsultation/Problembehandlung auf Initiative des Leistungserbringers"
* #X64 ^property[0].code = #parent 
* #X64 ^property[0].valueCode = #X 
* #X65 "Konsultation/Problembehandlung auf Initiative Dritter"
* #X65 ^property[0].code = #parent 
* #X65 ^property[0].valueCode = #X 
* #X66 "Überweisung an andere/n Leistungserbringer (nicht ärztl.)/Pflegekraft/Therapeuten/Sozialarbeiter"
* #X66 ^property[0].code = #parent 
* #X66 ^property[0].valueCode = #X 
* #X67 "Überweisung an einen Arzt/Spezialisten/eine Klinik/ein Krankenhaus"
* #X67 ^property[0].code = #parent 
* #X67 ^property[0].valueCode = #X 
* #X68 "Andere Überweisung NAK"
* #X68 ^property[0].code = #parent 
* #X68 ^property[0].valueCode = #X 
* #X69 "Anderer Beratungsanlass NAK"
* #X69 ^property[0].code = #parent 
* #X69 ^property[0].valueCode = #X 
* #X70 "Syphilis, Frau"
* #X70 ^definition = Inklusive: Syphilis jeglicher Lokalisation Exklusive:  Kriterien: Nachweis von Treponema pallidum durch mikroskopische Untersuchung oder positiven serologischen Test auf Syphilis
* #X70 ^property[0].code = #parent 
* #X70 ^property[0].valueCode = #X 
* #X70 ^property[1].code = #Relationships 
* #X70 ^property[1].valueString = "ICD-10 2017:A50.0~A50.1~A50.2~A50.3~A50.4~A50.5~A50.6~A50.7~A50.9~A51.0~A51.1~A51.2~A51.3~A51.4~A51.5~A51.9~A52.0~A52.1~A52.2~A52.3~A52.7~A52.8~A52.9~A53.0~A53.9~A65~N74.2" 
* #X71 "Gonorrhoe, Frau"
* #X71 ^definition = Inklusive: Gonorrhoe jeglicher Lokalisation Exklusive:  Kriterien: eitriger Ausfluß aus Urethra oder Vagina bei einer Patientin nach Kontakt mit einem bekannten Fall; oder Nachweis von gramnegativen, intrazellulären Diplokokken; oder Kultur von Neisseria gonorrhoeae
* #X71 ^property[0].code = #parent 
* #X71 ^property[0].valueCode = #X 
* #X71 ^property[1].code = #Relationships 
* #X71 ^property[1].valueString = "ICD-10 2017:A54.0~A54.1~A54.2~A54.3~A54.4~A54.5~A54.6~A54.8~A54.9~N74.3" 
* #X71 ^property[2].code = #hints 
* #X71 ^property[2].valueString = "Beachte: Urethritis U72~ Harnröhrenausfluß X29 Hinweise: " 
* #X72 "Vaginalmykose/Candidiasis"
* #X72 ^definition = Inklusive: Monilieninfektion der Vagina~ Soor Exklusive:  Kriterien: Entzündung der Schleimhäute oder Haut im Urogenitalbereich mit charakteristischem weißen Belag oder Nachweis von Candida
* #X72 ^property[0].code = #parent 
* #X72 ^property[0].valueCode = #X 
* #X72 ^property[1].code = #Relationships 
* #X72 ^property[1].valueString = "ICD-10 2017:B37.3~B37.4" 
* #X72 ^property[2].code = #hints 
* #X72 ^property[2].valueString = "Beachte: vaginaler Ausfluß X14~ Vaginitis X84 Hinweise: " 
* #X73 "Trichomoniasis Genitalbereich, Frau"
* #X73 ^definition = Inklusive:  Exklusive:  Kriterien: charakteristischer übelriechender Ausfluß oder Nachweis von Trichomonaden durch mikroskopische Untersuchung
* #X73 ^property[0].code = #parent 
* #X73 ^property[0].valueCode = #X 
* #X73 ^property[1].code = #Relationships 
* #X73 ^property[1].valueString = "ICD-10 2017:A59.0" 
* #X73 ^property[2].code = #hints 
* #X73 ^property[2].valueString = "Beachte: vaginaler Ausfluß X14~ Vaginitis X84 Hinweise: " 
* #X74 "Entzündung im weiblichen Becken"
* #X74 ^definition = Inklusive: Endometritis~ Oophoritis~ Salpingitis Exklusive: sexuell übertragene Erkrankungs bei der Frau X70-X73~ Chlamydieninfektion bei der Frau X92 Kriterien: Schmerzen im Unterbauch mit starker Druckempfindlichkeit des Uterus oder der Adnexe bei Palpation, plus weitere Hinweise auf Entzündung
* #X74 ^property[0].code = #parent 
* #X74 ^property[0].valueCode = #X 
* #X74 ^property[1].code = #Relationships 
* #X74 ^property[1].valueString = "ICD-10 2017:N70.0~N70.1~N70.9~N71.0~N71.1~N71.9~N73.0~N73.1~N73.2~N73.3~N73.4~N73.5~N73.6~N73.8~N73.9~N74.8" 
* #X74 ^property[2].code = #hints 
* #X74 ^property[2].valueString = "Beachte: Pelvipathie X99 Hinweise: " 
* #X75 "Bösartige Neubildung Zervix Uteri"
* #X75 ^definition = Inklusive:  Exklusive: karzinoma-in-situ der Zervix X81~ intraepitheliale Neoplasie der Zervix (CIN) Grad 3 X81~ abnormer Zervixabstrich (CIN) Grad 1 und 2 X86 Kriterien: charakteristisches histologisches Erscheinungsbild
* #X75 ^property[0].code = #parent 
* #X75 ^property[0].valueCode = #X 
* #X75 ^property[1].code = #Relationships 
* #X75 ^property[1].valueString = "ICD-10 2017:C53.0~C53.1~C53.8~C53.9" 
* #X76 "Bösartige Neubildung Brust, Frau"
* #X76 ^definition = Inklusive: karzinoma-in-situ der weiblichen Brust~ intraduktales Karzinom Exklusive: carcinoma in situ X81 Kriterien: charakteristisches histologisches Erscheinungsbild
* #X76 ^property[0].code = #parent 
* #X76 ^property[0].valueCode = #X 
* #X76 ^property[1].code = #Relationships 
* #X76 ^property[1].valueString = "ICD-10 2017:C50.0~C50.1~C50.2~C50.3~C50.4~C50.5~C50.6~C50.8~C50.9" 
* #X76 ^property[2].code = #hints 
* #X76 ^property[2].valueString = "Beachte: Knoten in der Brust X19 Hinweise: " 
* #X77 "Bösartige Neubildung des weiblichen Genitales, andere"
* #X77 ^definition = Inklusive: Bösartige Neubildungen an Adnexen~ Ovarien~ Uterus~ Vagina~ Vulva Exklusive: carcinoma-in-situ der Zervix X75~ anderes carcinoma-in-situ der weiblichen Genitale X81 Kriterien: charakteristisches histologisches Erscheinungsbild
* #X77 ^property[0].code = #parent 
* #X77 ^property[0].valueCode = #X 
* #X77 ^property[1].code = #Relationships 
* #X77 ^property[1].valueString = "ICD-10 2017:C51.0~C51.1~C51.2~C51.8~C51.9~C52~C54.0~C54.1~C54.2~C54.3~C54.8~C54.9~C55~C56~C57.0~C57.1~C57.2~C57.3~C57.4~C57.7~C57.8~C57.9" 
* #X77 ^property[2].code = #hints 
* #X77 ^property[2].valueString = "Beachte: andere/nicht spezifizierte Neubildung des weiblichen Genitales X81 Hinweise: " 
* #X78 "Uterus myomatosus"
* #X78 ^definition = Inklusive: fibroider Uterus~ Fibromyom der Zervix~ Myom Exklusive:  Kriterien: Vergrößerung des Uterus, die nicht auf Schwangerschaft oder bösartige Neubildung zurückzuführen ist, mit einem oder mehreren Tumor(en) am Uterus
* #X78 ^property[0].code = #parent 
* #X78 ^property[0].valueCode = #X 
* #X78 ^property[1].code = #Relationships 
* #X78 ^property[1].valueString = "ICD-10 2017:D25.0~D25.1~D25.2~D25.9" 
* #X79 "Gutartige Neubildung, weibliche Brust"
* #X79 ^definition = Inklusive: Fibroadenom der weiblichen Brust Exklusive: Mastopathie X88 Kriterien: charakteristisches histologisches Erscheinungsbild
* #X79 ^property[0].code = #parent 
* #X79 ^property[0].valueCode = #X 
* #X79 ^property[1].code = #Relationships 
* #X79 ^property[1].valueString = "ICD-10 2017:D24" 
* #X79 ^property[2].code = #hints 
* #X79 ^property[2].valueString = "Beachte: Knoten in der Brust X19 Hinweise: " 
* #X80 "Gutartige Neubildung weibliches Genitale"
* #X80 ^definition = Inklusive:  Exklusive: Zervixpolyp X85~ physiologische Zyste eines Ovars X99 Kriterien:
* #X80 ^property[0].code = #parent 
* #X80 ^property[0].valueCode = #X 
* #X80 ^property[1].code = #Relationships 
* #X80 ^property[1].valueString = "ICD-10 2017:D26.0~D26.1~D26.7~D26.9~D27~D28.0~D28.1~D28.2~D28.7~D28.9" 
* #X81 "Neubildung weibliches Genitale andere/nicht spezifiziert"
* #X81 ^definition = Inklusive: Anderes carcinoma-in-situ Exklusive: carcinoma-in-situ der Zervix X75~ Endometriumpolyp X99 Kriterien:
* #X81 ^property[0].code = #parent 
* #X81 ^property[0].valueCode = #X 
* #X81 ^property[1].code = #Relationships 
* #X81 ^property[1].valueString = "ICD-10 2017:D05.0~D05.1~D05.7~D05.9~D06.0~D06.1~D06.7~D06.9~D07.0~D07.1~D07.2~D07.3~D39.0~D39.1~D39.2~D39.7~D39.9~D48.6" 
* #X82 "Verletzung weibliches Genitale"
* #X82 ^definition = Inklusive: Fremdkörper in der Vagina~ weibliche Beschneidung Exklusive: Genitalverletzung nach Trauma W92~ W93 Kriterien:
* #X82 ^property[0].code = #parent 
* #X82 ^property[0].valueCode = #X 
* #X82 ^property[1].code = #Relationships 
* #X82 ^property[1].valueString = "ICD-10 2017:S30.2~S31.4~S31.5~S37.4~S37.5~S37.6~S38.0~S38.2~S39.8~S39.9~T19.2~T19.3~T28.3~T28.8~Z41.2" 
* #X83 "Angeborene Anomalie weibliches Genitale"
* #X83 ^definition = Inklusive: Hermaphroditismus~ Hymen imperforatum Exklusive: andere angeborene Syndrome A90 Kriterien:
* #X83 ^property[0].code = #parent 
* #X83 ^property[0].valueCode = #X 
* #X83 ^property[1].code = #Relationships 
* #X83 ^property[1].valueString = "ICD-10 2017:Q50.0~Q50.1~Q50.2~Q50.3~Q50.4~Q50.5~Q50.6~Q51.0~Q51.1~Q51.2~Q51.3~Q51.4~Q51.5~Q51.6~Q51.7~Q51.8~Q51.9~Q52.0~Q52.1~Q52.2~Q52.3~Q52.4~Q52.5~Q52.6~Q52.7~Q52.8~Q52.9~Q56.0~Q56.1~Q56.2~Q56.3~Q56.4~Q83.0~Q83.1~Q83.2~Q83.3~Q83.8~Q83.9" 
* #X84 "Vaginitis/Vulvitis NNB"
* #X84 ^definition = Inklusive: Gardnerella-Infektion der Vagina (Aminkolpitis) Exklusive: atrophische Vaginitis X11~ genitaler Soor X72~ genitale Trichomoniasis X73 Kriterien:
* #X84 ^property[0].code = #parent 
* #X84 ^property[0].valueCode = #X 
* #X84 ^property[1].code = #Relationships 
* #X84 ^property[1].valueString = "ICD-10 2017:N76.0~N76.1~N76.2~N76.3~N76.4~N76.5~N76.6~N76.8~N77.0~N77.1~N77.8" 
* #X85 "Zervixerkrankung NNB"
* #X85 ^definition = Inklusive: Erosion der Portio~ zervikale Leukoplakie~ Zervizitis~ zervikaler Schleimhautpolyp~ alte Lazeration der Zervix Exklusive: Zervixanomalie in Schwangerschaft/Geburt/Wochenbett W77~ abnormer Papanicolaou-Abstrich X86 Kriterien:
* #X85 ^property[0].code = #parent 
* #X85 ^property[0].valueCode = #X 
* #X85 ^property[1].code = #Relationships 
* #X85 ^property[1].valueString = "ICD-10 2017:N72~N84.1~N86~N88.0~N88.1~N88.2~N88.3~N88.4~N88.8~N88.9" 
* #X86 "Auffälliger Zervix-Abstrich"
* #X86 ^definition = Inklusive: zervikale intraepitheliale Neubildung (CIN) Grad 1 und 2~ zervikale Dysplasie Exklusive: intraepitheliale Neubildung der Zervix (CIN) Grad 3 X75 Kriterien:
* #X86 ^property[0].code = #parent 
* #X86 ^property[0].valueCode = #X 
* #X86 ^property[1].code = #Relationships 
* #X86 ^property[1].valueString = "ICD-10 2017:N87.0~N87.1~N87.2~N87.9~R87.6" 
* #X87 "Uterovaginaler Prolaps"
* #X87 ^definition = Inklusive: Zystozele~ Rektozele~ Procidentia Exklusive: Stressinkontinenz U04 Kriterien:
* #X87 ^property[0].code = #parent 
* #X87 ^property[0].valueCode = #X 
* #X87 ^property[1].code = #Relationships 
* #X87 ^property[1].valueString = "ICD-10 2017:N81.0~N81.1~N81.2~N81.3~N81.4~N81.5~N81.6~N81.8~N81.9" 
* #X88 "Fibrozystische Erkrankung Brustdrüse"
* #X88 ^definition = Inklusive: chronische zystische Erkrankung der Brust~ zystische Fibroadenome der Brust~ Dysplasie der Brust~ vereinzelte Zyste der Brust Exklusive:  Kriterien:
* #X88 ^property[0].code = #parent 
* #X88 ^property[0].valueCode = #X 
* #X88 ^property[1].code = #Relationships 
* #X88 ^property[1].valueString = "ICD-10 2017:N60.0~N60.1~N60.2~N60.3~N60.4~N60.8~N60.9" 
* #X89 "Prämenstruelles Spannungssyndrom"
* #X89 ^definition = Inklusive:  Exklusive:  Kriterien: periodisches Auftreten während des Menstruationszyklus von mindestens zwei der folgenden Symptome: Ödem, Druckempfindlichkeit oder Anschwellen der Brust, Kopfschmerzen, Reizbarkeit, Stimmungsschwankungen
* #X89 ^property[0].code = #parent 
* #X89 ^property[0].valueCode = #X 
* #X89 ^property[1].code = #Relationships 
* #X89 ^property[1].valueString = "ICD-10 2017:N94.3" 
* #X89 ^property[2].code = #hints 
* #X89 ^property[2].valueString = "Beachte: prämenstruelle Beschwerden X09 Hinweise: " 
* #X90 "Herpes genitalis, Frau"
* #X90 ^definition = Inklusive: anogenitaler Herpes simplex der Frau Exklusive:  Kriterien: Kleine Bläschen mit typischem Erscheinungsbild und typischer Lokalisation, die sich zu schmerzhaften Geschwüren und Schorf entwickeln
* #X90 ^property[0].code = #parent 
* #X90 ^property[0].valueCode = #X 
* #X90 ^property[1].code = #Relationships 
* #X90 ^property[1].valueString = "ICD-10 2017:A60.0~A60.1~A60.9" 
* #X91 "Condylomata acuminata, Frau"
* #X91 ^definition = Inklusive: venerische Warzen~ humane Papilloma-Virusinfektion Exklusive:  Kriterien: charakteristische Erscheinung der Läsionen, oder charakteristisches histologisches Erscheinungsbild
* #X91 ^property[0].code = #parent 
* #X91 ^property[0].valueCode = #X 
* #X91 ^property[1].code = #Relationships 
* #X91 ^property[1].valueString = "ICD-10 2017:A63.0" 
* #X92 "Chlamydieninfektion weibliches Genitale"
* #X92 ^definition = Inklusive:  Exklusive:  Kriterien: nachgewiesene Chlamydieninfektion
* #X92 ^property[0].code = #parent 
* #X92 ^property[0].valueCode = #X 
* #X92 ^property[1].code = #Relationships 
* #X92 ^property[1].valueString = "ICD-10 2017:A56.0~A56.1~A56.2~A56.3~A56.4~A56.8~N74.4" 
* #X99 "Andere Erkrankung weibliches Genitale"
* #X99 ^definition = Inklusive: Bartholini-Zyste~ Endometriose~ Fistel im Genitalbereich~ Pelvipathie~ physiologische Ovarialzyste Exklusive: sexuell übertragene Erkrankung NNB A78 Kriterien:
* #X99 ^property[0].code = #parent 
* #X99 ^property[0].valueCode = #X 
* #X99 ^property[1].code = #Relationships 
* #X99 ^property[1].valueString = "ICD-10 2017:A55~A57~A58~A63.8~N61~N64.1~N64.2~N64.8~N75.0~N75.1~N75.8~N75.9~N80.0~N80.1~N80.2~N80.3~N80.4~N80.5~N80.6~N80.8~N80.9~N82.0~N82.1~N82.2~N82.3~N82.4~N82.5~N82.8~N82.9~N83.0~N83.1~N83.2~N83.3~N83.4~N83.5~N83.6~N83.7~N83.8~N83.9~N84.0~N84.2~N84.3~N84.8~N84.9~N85.0~N85.1~N85.2~N85.3~N85.4~N85.5~N85.6~N85.7~N85.8~N85.9~N89.0~N89.1~N89.2~N89.3~N89.4~N89.5~N89.6~N89.7~N89.8~N90.0~N90.1~N90.2~N90.3~N90.4~N90.5~N90.6~N90.7~N90.8~N94.8~N94.9~Z90.1~Z90.7" 
* #Y "Männliches Genitale"
* #Y ^property[0].code = #child 
* #Y ^property[0].valueCode = #Y01 
* #Y ^property[1].code = #child 
* #Y ^property[1].valueCode = #Y02 
* #Y ^property[2].code = #child 
* #Y ^property[2].valueCode = #Y03 
* #Y ^property[3].code = #child 
* #Y ^property[3].valueCode = #Y04 
* #Y ^property[4].code = #child 
* #Y ^property[4].valueCode = #Y05 
* #Y ^property[5].code = #child 
* #Y ^property[5].valueCode = #Y06 
* #Y ^property[6].code = #child 
* #Y ^property[6].valueCode = #Y07 
* #Y ^property[7].code = #child 
* #Y ^property[7].valueCode = #Y08 
* #Y ^property[8].code = #child 
* #Y ^property[8].valueCode = #Y10 
* #Y ^property[9].code = #child 
* #Y ^property[9].valueCode = #Y13 
* #Y ^property[10].code = #child 
* #Y ^property[10].valueCode = #Y14 
* #Y ^property[11].code = #child 
* #Y ^property[11].valueCode = #Y16 
* #Y ^property[12].code = #child 
* #Y ^property[12].valueCode = #Y24 
* #Y ^property[13].code = #child 
* #Y ^property[13].valueCode = #Y25 
* #Y ^property[14].code = #child 
* #Y ^property[14].valueCode = #Y26 
* #Y ^property[15].code = #child 
* #Y ^property[15].valueCode = #Y27 
* #Y ^property[16].code = #child 
* #Y ^property[16].valueCode = #Y28 
* #Y ^property[17].code = #child 
* #Y ^property[17].valueCode = #Y29 
* #Y ^property[18].code = #child 
* #Y ^property[18].valueCode = #Y30 
* #Y ^property[19].code = #child 
* #Y ^property[19].valueCode = #Y31 
* #Y ^property[20].code = #child 
* #Y ^property[20].valueCode = #Y32 
* #Y ^property[21].code = #child 
* #Y ^property[21].valueCode = #Y33 
* #Y ^property[22].code = #child 
* #Y ^property[22].valueCode = #Y34 
* #Y ^property[23].code = #child 
* #Y ^property[23].valueCode = #Y35 
* #Y ^property[24].code = #child 
* #Y ^property[24].valueCode = #Y36 
* #Y ^property[25].code = #child 
* #Y ^property[25].valueCode = #Y37 
* #Y ^property[26].code = #child 
* #Y ^property[26].valueCode = #Y38 
* #Y ^property[27].code = #child 
* #Y ^property[27].valueCode = #Y39 
* #Y ^property[28].code = #child 
* #Y ^property[28].valueCode = #Y40 
* #Y ^property[29].code = #child 
* #Y ^property[29].valueCode = #Y41 
* #Y ^property[30].code = #child 
* #Y ^property[30].valueCode = #Y42 
* #Y ^property[31].code = #child 
* #Y ^property[31].valueCode = #Y43 
* #Y ^property[32].code = #child 
* #Y ^property[32].valueCode = #Y44 
* #Y ^property[33].code = #child 
* #Y ^property[33].valueCode = #Y45 
* #Y ^property[34].code = #child 
* #Y ^property[34].valueCode = #Y46 
* #Y ^property[35].code = #child 
* #Y ^property[35].valueCode = #Y47 
* #Y ^property[36].code = #child 
* #Y ^property[36].valueCode = #Y48 
* #Y ^property[37].code = #child 
* #Y ^property[37].valueCode = #Y49 
* #Y ^property[38].code = #child 
* #Y ^property[38].valueCode = #Y50 
* #Y ^property[39].code = #child 
* #Y ^property[39].valueCode = #Y51 
* #Y ^property[40].code = #child 
* #Y ^property[40].valueCode = #Y52 
* #Y ^property[41].code = #child 
* #Y ^property[41].valueCode = #Y53 
* #Y ^property[42].code = #child 
* #Y ^property[42].valueCode = #Y54 
* #Y ^property[43].code = #child 
* #Y ^property[43].valueCode = #Y55 
* #Y ^property[44].code = #child 
* #Y ^property[44].valueCode = #Y56 
* #Y ^property[45].code = #child 
* #Y ^property[45].valueCode = #Y58 
* #Y ^property[46].code = #child 
* #Y ^property[46].valueCode = #Y59 
* #Y ^property[47].code = #child 
* #Y ^property[47].valueCode = #Y60 
* #Y ^property[48].code = #child 
* #Y ^property[48].valueCode = #Y61 
* #Y ^property[49].code = #child 
* #Y ^property[49].valueCode = #Y62 
* #Y ^property[50].code = #child 
* #Y ^property[50].valueCode = #Y63 
* #Y ^property[51].code = #child 
* #Y ^property[51].valueCode = #Y64 
* #Y ^property[52].code = #child 
* #Y ^property[52].valueCode = #Y65 
* #Y ^property[53].code = #child 
* #Y ^property[53].valueCode = #Y66 
* #Y ^property[54].code = #child 
* #Y ^property[54].valueCode = #Y67 
* #Y ^property[55].code = #child 
* #Y ^property[55].valueCode = #Y68 
* #Y ^property[56].code = #child 
* #Y ^property[56].valueCode = #Y69 
* #Y ^property[57].code = #child 
* #Y ^property[57].valueCode = #Y70 
* #Y ^property[58].code = #child 
* #Y ^property[58].valueCode = #Y71 
* #Y ^property[59].code = #child 
* #Y ^property[59].valueCode = #Y72 
* #Y ^property[60].code = #child 
* #Y ^property[60].valueCode = #Y73 
* #Y ^property[61].code = #child 
* #Y ^property[61].valueCode = #Y74 
* #Y ^property[62].code = #child 
* #Y ^property[62].valueCode = #Y75 
* #Y ^property[63].code = #child 
* #Y ^property[63].valueCode = #Y76 
* #Y ^property[64].code = #child 
* #Y ^property[64].valueCode = #Y77 
* #Y ^property[65].code = #child 
* #Y ^property[65].valueCode = #Y78 
* #Y ^property[66].code = #child 
* #Y ^property[66].valueCode = #Y79 
* #Y ^property[67].code = #child 
* #Y ^property[67].valueCode = #Y80 
* #Y ^property[68].code = #child 
* #Y ^property[68].valueCode = #Y81 
* #Y ^property[69].code = #child 
* #Y ^property[69].valueCode = #Y82 
* #Y ^property[70].code = #child 
* #Y ^property[70].valueCode = #Y83 
* #Y ^property[71].code = #child 
* #Y ^property[71].valueCode = #Y84 
* #Y ^property[72].code = #child 
* #Y ^property[72].valueCode = #Y85 
* #Y ^property[73].code = #child 
* #Y ^property[73].valueCode = #Y86 
* #Y ^property[74].code = #child 
* #Y ^property[74].valueCode = #Y99 
* #Y01 "Penisschmerz"
* #Y01 ^definition = Inklusive:  Exklusive: Priapismus/schmerzhafte Erektion Y08 Kriterien:
* #Y01 ^property[0].code = #parent 
* #Y01 ^property[0].valueCode = #Y 
* #Y01 ^property[1].code = #Relationships 
* #Y01 ^property[1].valueString = "ICD-10 2017:N48.8" 
* #Y02 "Schmerz in Hoden/Skrotum"
* #Y02 ^definition = Inklusive: Schmerzen im Perineum~ Beckenschmerzen Exklusive:  Kriterien:
* #Y02 ^property[0].code = #parent 
* #Y02 ^property[0].valueCode = #Y 
* #Y02 ^property[1].code = #Relationships 
* #Y02 ^property[1].valueString = "ICD-10 2017:N50.8~R10.2" 
* #Y03 "Harnröhrenausfluss"
* #Y03 ^property[0].code = #parent 
* #Y03 ^property[0].valueCode = #Y 
* #Y03 ^property[1].code = #Relationships 
* #Y03 ^property[1].valueString = "ICD-10 2017:R36" 
* #Y04 "Penissymptome/-beschwerden, andere"
* #Y04 ^definition = Inklusive: Symptome/ Beschwerden derVorhaut Exklusive: Penisschmerzen Y01~ schmerzhafte Erektion/ Priapismus Y08 Kriterien:
* #Y04 ^property[0].code = #parent 
* #Y04 ^property[0].valueCode = #Y 
* #Y04 ^property[1].code = #Relationships 
* #Y04 ^property[1].valueString = "ICD-10 2017:N48.9" 
* #Y05 "Skrotum-/Hodensymptome/-beschwerden, andere"
* #Y05 ^definition = Inklusive: Tumoröse Veränderung am Hoden Exklusive: Schmerzen in Hoden/Skrotum Y02 Kriterien:
* #Y05 ^property[0].code = #parent 
* #Y05 ^property[0].valueCode = #Y 
* #Y05 ^property[1].code = #Relationships 
* #Y05 ^property[1].valueString = "ICD-10 2017:L29.1~N50.9" 
* #Y06 "Prostatasymptome/-beschwerden"
* #Y06 ^definition = Inklusive: Prostatahypertrophie Exklusive: häufiges/dringendes Wasserlassen U02~ Harnverhalt U08 Kriterien:
* #Y06 ^property[0].code = #parent 
* #Y06 ^property[0].valueCode = #Y 
* #Y06 ^property[1].code = #Relationships 
* #Y06 ^property[1].valueString = "ICD-10 2017:N42.9" 
* #Y07 "Impotenz NNB"
* #Y07 ^definition = Inklusive: Impotenz organischen Ursprungs Exklusive: vermindertes sexuelles Verlangen P07~ psychogene Impotenz/verminderte sexuelle Erfüllung P08 Kriterien:
* #Y07 ^property[0].code = #parent 
* #Y07 ^property[0].valueCode = #Y 
* #Y07 ^property[1].code = #Relationships 
* #Y07 ^property[1].valueString = "ICD-10 2017:N48.4" 
* #Y08 "Sexualfunktion Symptome/Beschwerden, Mann"
* #Y08 ^definition = Inklusive: schmerzhafte Erektion/ Priapismus Exklusive: vermindertes sexuelles Verlangen P07~ psychogene Impotenz/verminderte sexuelle Erfüllung P08~ Impotenz organischen Ursprungs Y07 Kriterien:
* #Y08 ^property[0].code = #parent 
* #Y08 ^property[0].valueCode = #Y 
* #Y08 ^property[1].code = #Relationships 
* #Y08 ^property[1].valueString = "ICD-10 2017:N48.3~N48.9" 
* #Y10 "Infertilität/Subfertilität, Mann"
* #Y10 ^definition = Inklusive:  Exklusive:  Kriterien: keine Empfängnis trotz zweijähriger Bemühungen
* #Y10 ^property[0].code = #parent 
* #Y10 ^property[0].valueCode = #Y 
* #Y10 ^property[1].code = #Relationships 
* #Y10 ^property[1].valueString = "ICD-10 2017:N46~Z31.0~Z31.4~Z31.6~Z31.8~Z31.9" 
* #Y13 "Sterilisierung, Mann"
* #Y13 ^definition = Inklusive: Familienplanung durch Sterilisation des Mannes Exklusive:  Kriterien:
* #Y13 ^property[0].code = #parent 
* #Y13 ^property[0].valueCode = #Y 
* #Y13 ^property[1].code = #Relationships 
* #Y13 ^property[1].valueString = "ICD-10 2017:Z30.2" 
* #Y14 "Familienplanung, Mann andere"
* #Y14 ^definition = Inklusive: Kontrazeption NNB~ Familienplanung NNB Exklusive: genetische Beratung A98 Kriterien:
* #Y14 ^property[0].code = #parent 
* #Y14 ^property[0].valueCode = #Y 
* #Y14 ^property[1].code = #Relationships 
* #Y14 ^property[1].valueString = "ICD-10 2017:Z30.0~Z30.8~Z30.9" 
* #Y16 "Brustsymptome/-beschwerden, Mann"
* #Y16 ^definition = Inklusive: Knoten in der männlichen Brust~ Gynäkomastie Exklusive: Erkrankung einer männlichen Brust Y99 Kriterien:
* #Y16 ^property[0].code = #parent 
* #Y16 ^property[0].valueCode = #Y 
* #Y16 ^property[1].code = #Relationships 
* #Y16 ^property[1].valueString = "ICD-10 2017:N62~N63~N64.5" 
* #Y24 "Angst vor sexueller Dysfunktion, Mann"
* #Y24 ^definition = Inklusive:  Exklusive: Wenn der Patient eine sexuelle Funktionsstörung hat~ den Zustand kodieren Kriterien: Besorgnis über/Angst vor einer sexuell übertragbaren Erkrankung bei einem Patienten, der die Krankheit nicht hat/bevor die Diagnose gesichert ist
* #Y24 ^property[0].code = #parent 
* #Y24 ^property[0].valueCode = #Y 
* #Y24 ^property[1].code = #Relationships 
* #Y24 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #Y25 "Angst vor sexuell übertragbaren Krankheiten, Mann"
* #Y25 ^definition = Inklusive:  Exklusive: Angst vor HIV/AIDS B25~ bei einem Patienten mit der Erkrankung ist die Erkrankung zu kodieren Kriterien: Besorgnis über/Angst vor sexueller Dysfunktion bei einem Patienten, der keine sexuelle Dysfunktion hat
* #Y25 ^property[0].code = #parent 
* #Y25 ^property[0].valueCode = #Y 
* #Y25 ^property[1].code = #Relationships 
* #Y25 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #Y26 "Angst vor Genitalkrebs, Mann"
* #Y26 ^definition = Inklusive:  Exklusive: bei einem Patienten mit der Erkrankung ist die Erkrankung zu kodieren Kriterien: Besorgnis über/Angst vor Krebs bei einem Patienten, der die Krankheit nicht hat/bevor die Diagnose gesichert ist
* #Y26 ^property[0].code = #parent 
* #Y26 ^property[0].valueCode = #Y 
* #Y26 ^property[1].code = #Relationships 
* #Y26 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #Y27 "Angst vor Geschlechtskrankheit Mann, andere"
* #Y27 ^definition = Inklusive:  Exklusive: Angst vor sexuell übertragene Erkrankung Y25~ Angst vor Genitalkrebs Y26~ bei einem Patienten mit der Erkrankung ist die Erkrankung zu kodieren Kriterien: Besorgnis über/Angst vor einer anderen Erkrankung des Genitales bei einem Patienten, der die Krankheit nicht hat/bevor die Diagnose gesichert ist
* #Y27 ^property[0].code = #parent 
* #Y27 ^property[0].valueCode = #Y 
* #Y27 ^property[1].code = #Relationships 
* #Y27 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #Y28 "Funktionseinschränkung/Behinderung (Y)"
* #Y28 ^definition = Inklusive:  Exklusive: sexuelle Funktionsstörung P07~ P08~ Impotenz NNB Y07 Kriterien: Funktionseinschränkung/Behinderung durch eine Erkrankung des Genitalsystems (einschließlich Brust)
* #Y28 ^property[0].code = #parent 
* #Y28 ^property[0].valueCode = #Y 
* #Y28 ^property[1].code = #Relationships 
* #Y28 ^property[1].valueString = "ICD-10 2017:Z73.6~Z90.7" 
* #Y28 ^property[2].code = #hints 
* #Y28 ^property[2].valueString = "Beachte:  Hinweise: COOP/WONCA Charts eignen sich zur Dokumentation des funktionellen Status des Patienten (siehe Kapitel 8)" 
* #Y29 "Andere Genitalsymptome/-beschwerden Mann"
* #Y29 ^property[0].code = #parent 
* #Y29 ^property[0].valueCode = #Y 
* #Y29 ^property[1].code = #Relationships 
* #Y29 ^property[1].valueString = "ICD-10 2017:N50.9" 
* #Y30 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? vollständig"
* #Y30 ^property[0].code = #parent 
* #Y30 ^property[0].valueCode = #Y 
* #Y31 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? teilweise"
* #Y31 ^property[0].code = #parent 
* #Y31 ^property[0].valueCode = #Y 
* #Y32 "Allergie-/Sensitivitätstestung"
* #Y32 ^property[0].code = #parent 
* #Y32 ^property[0].valueCode = #Y 
* #Y33 "Mikrobiologische/immunologische Untersuchung"
* #Y33 ^property[0].code = #parent 
* #Y33 ^property[0].valueCode = #Y 
* #Y34 "Blutuntersuchung"
* #Y34 ^property[0].code = #parent 
* #Y34 ^property[0].valueCode = #Y 
* #Y35 "Urinuntersuchung"
* #Y35 ^property[0].code = #parent 
* #Y35 ^property[0].valueCode = #Y 
* #Y36 "Stuhluntersuchung"
* #Y36 ^property[0].code = #parent 
* #Y36 ^property[0].valueCode = #Y 
* #Y37 "Histologische Untersuchung/zytologischer Abstrich"
* #Y37 ^property[0].code = #parent 
* #Y37 ^property[0].valueCode = #Y 
* #Y38 "Andere Laboruntersuchung NAK"
* #Y38 ^property[0].code = #parent 
* #Y38 ^property[0].valueCode = #Y 
* #Y39 "Körperliche Funktionsprüfung"
* #Y39 ^property[0].code = #parent 
* #Y39 ^property[0].valueCode = #Y 
* #Y40 "Diagnostische Endoskopie"
* #Y40 ^property[0].code = #parent 
* #Y40 ^property[0].valueCode = #Y 
* #Y41 "Diagnostisches Röntgen/Bildgebung"
* #Y41 ^property[0].code = #parent 
* #Y41 ^property[0].valueCode = #Y 
* #Y42 "Elektrische Aufzeichnungsverfahren"
* #Y42 ^property[0].code = #parent 
* #Y42 ^property[0].valueCode = #Y 
* #Y43 "Andere diagnostische Untersuchung"
* #Y43 ^property[0].code = #parent 
* #Y43 ^property[0].valueCode = #Y 
* #Y44 "Präventive Impfung/Medikation"
* #Y44 ^property[0].code = #parent 
* #Y44 ^property[0].valueCode = #Y 
* #Y45 "Beobachtung/Gesundheitsschulung/Beratung/Diät"
* #Y45 ^property[0].code = #parent 
* #Y45 ^property[0].valueCode = #Y 
* #Y46 "Konsultation eines anderen Primärversorgers"
* #Y46 ^property[0].code = #parent 
* #Y46 ^property[0].valueCode = #Y 
* #Y47 "Konsultation eines Facharztes"
* #Y47 ^property[0].code = #parent 
* #Y47 ^property[0].valueCode = #Y 
* #Y48 "Klärung/Besprechung des Beratungsanlasses mit dem Patienten"
* #Y48 ^property[0].code = #parent 
* #Y48 ^property[0].valueCode = #Y 
* #Y49 "Andere Vorsorgemaßnahme"
* #Y49 ^property[0].code = #parent 
* #Y49 ^property[0].valueCode = #Y 
* #Y50 "Medikation/Verschreibung/Erneuerung/Injektion"
* #Y50 ^property[0].code = #parent 
* #Y50 ^property[0].valueCode = #Y 
* #Y51 "Inzision/Drainage/Spülung/Absaugung/Entfernung von Körperflüssigkeit"
* #Y51 ^property[0].code = #parent 
* #Y51 ^property[0].valueCode = #Y 
* #Y52 "Exzision/Entfernung von Gewebe/Biopsie/Destruktion/Debridement/Verkauterung"
* #Y52 ^property[0].code = #parent 
* #Y52 ^property[0].valueCode = #Y 
* #Y53 "Instrumentelle Manipulation/Katheterisierung/Intubation/Dilatation"
* #Y53 ^property[0].code = #parent 
* #Y53 ^property[0].valueCode = #Y 
* #Y54 "Reposition/Fixierung/Naht/Gips/prothetische Versorgung"
* #Y54 ^property[0].code = #parent 
* #Y54 ^property[0].valueCode = #Y 
* #Y55 "Lokale Injektion/Infiltration"
* #Y55 ^property[0].code = #parent 
* #Y55 ^property[0].valueCode = #Y 
* #Y56 "Verband/Druck/Kompression/Tamponade"
* #Y56 ^property[0].code = #parent 
* #Y56 ^property[0].valueCode = #Y 
* #Y58 "Therapeutische Beratung/Zuhören"
* #Y58 ^property[0].code = #parent 
* #Y58 ^property[0].valueCode = #Y 
* #Y59 "Andere therapeutische Maßnahme/kleine Chirurgie NAK"
* #Y59 ^property[0].code = #parent 
* #Y59 ^property[0].valueCode = #Y 
* #Y60 "Testresultat/Ergebnis eigene Maßnahme"
* #Y60 ^property[0].code = #parent 
* #Y60 ^property[0].valueCode = #Y 
* #Y61 "Ergebnis Untersuchung/Test/Brief von anderem Leistungserbringer"
* #Y61 ^property[0].code = #parent 
* #Y61 ^property[0].valueCode = #Y 
* #Y62 "Adminstrative Maßnahme"
* #Y62 ^property[0].code = #parent 
* #Y62 ^property[0].valueCode = #Y 
* #Y63 "Folgekonsultation unspezifiziert"
* #Y63 ^property[0].code = #parent 
* #Y63 ^property[0].valueCode = #Y 
* #Y64 "Konsultation/Problembehandlung auf Initiative des Leistungserbringers"
* #Y64 ^property[0].code = #parent 
* #Y64 ^property[0].valueCode = #Y 
* #Y65 "Konsultation/Problembehandlung auf Initiative Dritter"
* #Y65 ^property[0].code = #parent 
* #Y65 ^property[0].valueCode = #Y 
* #Y66 "Überweisung an andere/n Leistungserbringer (nicht ärztl.)/Pflegekraft/Therapeuten/Sozialarbeiter"
* #Y66 ^property[0].code = #parent 
* #Y66 ^property[0].valueCode = #Y 
* #Y67 "Überweisung an einen Arzt/Spezialisten/eine Klinik/ein Krankenhaus"
* #Y67 ^property[0].code = #parent 
* #Y67 ^property[0].valueCode = #Y 
* #Y68 "Andere Überweisung NAK"
* #Y68 ^property[0].code = #parent 
* #Y68 ^property[0].valueCode = #Y 
* #Y69 "Anderer Beratungsanlass NAK"
* #Y69 ^property[0].code = #parent 
* #Y69 ^property[0].valueCode = #Y 
* #Y70 "Syphilis, Mann"
* #Y70 ^definition = Inklusive: Syphilis jeglicher Lokalisation Exklusive:  Kriterien: Mikroskopischer Nachweis von Treponema pallidum oder positiver serologischer Test auf Syphilis
* #Y70 ^property[0].code = #parent 
* #Y70 ^property[0].valueCode = #Y 
* #Y70 ^property[1].code = #Relationships 
* #Y70 ^property[1].valueString = "ICD-10 2017:A50.0~A50.1~A50.2~A50.3~A50.4~A50.5~A50.6~A50.7~A50.9~A51.0~A51.1~A51.2~A51.3~A51.4~A51.5~A51.9~A52.0~A52.1~A52.2~A52.3~A52.7~A52.8~A52.9~A53.0~A53.9~A65" 
* #Y71 "Gonorrhoe, Mann"
* #Y71 ^definition = Inklusive: Gnorrhoe jeglicher Lokalisation Exklusive:  Kriterien: eitriger Ausfluß aus Urethra oder Rektum bei einem Patienten nach Kontakt mit einem bekannten Fall; oder Nachweis von gramnegativen, intrazellulären Diplokokken; oder Kultur von Neisseria gonorrhoeae
* #Y71 ^property[0].code = #parent 
* #Y71 ^property[0].valueCode = #Y 
* #Y71 ^property[1].code = #Relationships 
* #Y71 ^property[1].valueString = "ICD-10 2017:A54.0~A54.1~A54.2~A54.3~A54.4~A54.5~A54.6~A54.8~A54.9" 
* #Y71 ^property[2].code = #hints 
* #Y71 ^property[2].valueString = "Beachte: Urethritis U72~ Harnröhrenausfluß Y03 Hinweise: " 
* #Y72 "Genitalherpes, Mann"
* #Y72 ^definition = Inklusive: anogenitaler Herpes~ Herpes genitalis beim Mann Exklusive:  Kriterien: kleine Bläschen mit charakteristischem Erscheinungsbild und typischer Lokalisation, die sich zu schmerzhaften Ulzera und Schorf entwickeln
* #Y72 ^property[0].code = #parent 
* #Y72 ^property[0].valueCode = #Y 
* #Y72 ^property[1].code = #Relationships 
* #Y72 ^property[1].valueString = "ICD-10 2017:A60.0~A60.1~A60.9" 
* #Y73 "Prostatitis/Samenbläschenentzündung"
* #Y73 ^definition = Inklusive:  Exklusive:  Kriterien: Druckempfindlichkeit der Prostata/Samenblase bei Palpation und Hinweise auf Entzündung im Urintest
* #Y73 ^property[0].code = #parent 
* #Y73 ^property[0].valueCode = #Y 
* #Y73 ^property[1].code = #Relationships 
* #Y73 ^property[1].valueString = "ICD-10 2017:A59.0~N41.0~N41.1~N41.2~N41.3~N41.8~N41.9~N49.0" 
* #Y74 "Orchitis/Epididymitis"
* #Y74 ^definition = Inklusive:  Exklusive: Tuberkulose A70~ Mumps D71~ Gonokokkenorchitis Y71~ Hodentorsion Y99 Kriterien: Schwellung und Druckempfindlichkeit der Hoden/Nebenhoden und Abwesenheit eine spezifischen Äthiologie (Mumps, Gonokokken, Tuberkulose, Trauma oder Torsion)
* #Y74 ^property[0].code = #parent 
* #Y74 ^property[0].valueCode = #Y 
* #Y74 ^property[1].code = #Relationships 
* #Y74 ^property[1].valueString = "ICD-10 2017:A56.1~N45.0~N45.9" 
* #Y74 ^property[2].code = #hints 
* #Y74 ^property[2].valueString = "Beachte: Hodenbeschwerden Y05 Hinweise: " 
* #Y75 "Balanitis"
* #Y75 ^definition = Inklusive: Candidiasis der Glans penis Exklusive: Krätze S72~ Syphilis beim Mann Y70~ Gonorrhoe beim Mann Y71~ Herpes genitalis beim Mann Y72 Kriterien: Entzündungszeichen an Vorhaut/Glans penis
* #Y75 ^property[0].code = #parent 
* #Y75 ^property[0].valueCode = #Y 
* #Y75 ^property[1].code = #Relationships 
* #Y75 ^property[1].valueString = "ICD-10 2017:B37.4~N48.1" 
* #Y76 "Condylomata acuminata, Mann"
* #Y76 ^definition = Inklusive: venerische Warzen~ humane Papillomavirus-Infektion beim Mann Exklusive:  Kriterien: charakteristische Erscheinung der Läsionen, oder charakteristisches histologisches Erscheinungsbild
* #Y76 ^property[0].code = #parent 
* #Y76 ^property[0].valueCode = #Y 
* #Y76 ^property[1].code = #Relationships 
* #Y76 ^property[1].valueString = "ICD-10 2017:A63.0" 
* #Y77 "Bösartige Neubildung Prostata"
* #Y77 ^definition = Inklusive:  Exklusive:  Kriterien: characteristisches histologisches Erscheinungsbild
* #Y77 ^property[0].code = #parent 
* #Y77 ^property[0].valueCode = #Y 
* #Y77 ^property[1].code = #Relationships 
* #Y77 ^property[1].valueString = "ICD-10 2017:C61" 
* #Y77 ^property[2].code = #hints 
* #Y77 ^property[2].valueString = "Beachte: gutartige/nicht spezifizierte Neubildung des männlichen Genitals Y79 Hinweise: " 
* #Y78 "Bösartige Neubildung männliches Genitale, andere"
* #Y78 ^definition = Inklusive: Hodenkrebs/Seminom~ Brustkrebs beim Mann Exklusive: Carzinoma in situ Y79 Kriterien: characteristisches histologisches Erscheinungsbild
* #Y78 ^property[0].code = #parent 
* #Y78 ^property[0].valueCode = #Y 
* #Y78 ^property[1].code = #Relationships 
* #Y78 ^property[1].valueString = "ICD-10 2017:C50.0~C50.1~C50.2~C50.3~C50.4~C50.5~C50.6~C50.8~C50.9~C60.0~C60.1~C60.2~C60.8~C60.9~C62.0~C62.1~C62.9~C63.0~C63.1~C63.2~C63.7~C63.8~C63.9" 
* #Y78 ^property[2].code = #hints 
* #Y78 ^property[2].valueString = "Beachte: gutartige/nicht spezifizierte Neubildung des männlichen Genitals Y79 Hinweise: " 
* #Y79 "Gutartige/nicht spezifizierte Neubildung männliches Genitale, andere"
* #Y79 ^definition = Inklusive: gutartige Neubildung des männlichen Genitale~ nicht als gut- oder bösartig spezifizierte Neubildung des männlichen Genitale/Histologie nicht verfügbar~ guartige Neubildung der männlichen Brust Exklusive: Prostatahypertrophie Y85 Kriterien:
* #Y79 ^property[0].code = #parent 
* #Y79 ^property[0].valueCode = #Y 
* #Y79 ^property[1].code = #Relationships 
* #Y79 ^property[1].valueString = "ICD-10 2017:D05.0~D05.1~D05.7~D05.9~D07.4~D07.5~D07.6~D24~D29.0~D29.1~D29.2~D29.3~D29.4~D29.7~D29.9~D40.0~D40.1~D40.7~D40.9~D48.6~N42.3" 
* #Y80 "Verletzung männliches Genitale"
* #Y80 ^definition = Inklusive: Beschneidung Exklusive:  Kriterien:
* #Y80 ^property[0].code = #parent 
* #Y80 ^property[0].valueCode = #Y 
* #Y80 ^property[1].code = #Relationships 
* #Y80 ^property[1].valueString = "ICD-10 2017:S30.2~S31.2~S31.3~S31.5~S38.0~S38.2~T28.3~T28.8~Z41.2" 
* #Y81 "Phimose/überschüssige Vorhaut"
* #Y81 ^definition = Inklusive: Paraphimose Exklusive:  Kriterien: für überschüssige Vorhaut: übermäßig lange Vorhaut, die nicht über die Glans penis zurückgezogen werden kann; für Phimose: die Enge der Vorhaut verhindert ein Zurückziehen über die Glans penis
* #Y81 ^property[0].code = #parent 
* #Y81 ^property[0].valueCode = #Y 
* #Y81 ^property[1].code = #Relationships 
* #Y81 ^property[1].valueString = "ICD-10 2017:N47" 
* #Y82 "Hypospadie"
* #Y82 ^property[0].code = #parent 
* #Y82 ^property[0].valueCode = #Y 
* #Y82 ^property[1].code = #Relationships 
* #Y82 ^property[1].valueString = "ICD-10 2017:Q54.0~Q54.1~Q54.2~Q54.3~Q54.4~Q54.8~Q54.9" 
* #Y83 "Hodenhochstand"
* #Y83 ^definition = Inklusive: Kryptorchismus Exklusive: Retraktile Hoden Y84 Kriterien: Hoden wurde nie im Hodensack beobachtet und läßt sich nicht in den Hodensack transferieren
* #Y83 ^property[0].code = #parent 
* #Y83 ^property[0].valueCode = #Y 
* #Y83 ^property[1].code = #Relationships 
* #Y83 ^property[1].valueString = "ICD-10 2017:Q53.0~Q53.1~Q53.2~Q53.9" 
* #Y84 "Angeborene Anomalie des männlichen Genitale, andere"
* #Y84 ^definition = Inklusive: Hermaphroditismus~ retraktiler Hoden Exklusive:  Kriterien:
* #Y84 ^property[0].code = #parent 
* #Y84 ^property[0].valueCode = #Y 
* #Y84 ^property[1].code = #Relationships 
* #Y84 ^property[1].valueString = "ICD-10 2017:Q55.0~Q55.1~Q55.2~Q55.3~Q55.4~Q55.5~Q55.6~Q55.8~Q55.9~Q56.0~Q56.1~Q56.2~Q56.3~Q56.4~Q83.0~Q83.1~Q83.2~Q83.3~Q83.8~Q83.9" 
* #Y85 "Benigne Prostatahypertrophie"
* #Y85 ^definition = Inklusive: Fibroma~ Hyperplasie~ Mittellappen-syndrom~ Unwegsamkeit der Prostata~ Prostatamegalie Exklusive:  Kriterien: vergrößerte, glatte, pralle Prostata, nachgewiesen durch Palpation, Zystoskopie oder bildgebendes Verfahren, ohne Hinweis auf Prostatakarzinom
* #Y85 ^property[0].code = #parent 
* #Y85 ^property[0].valueCode = #Y 
* #Y85 ^property[1].code = #Relationships 
* #Y85 ^property[1].valueString = "ICD-10 2017:N40" 
* #Y85 ^property[2].code = #hints 
* #Y85 ^property[2].valueString = "Beachte: Symptome/Beschwerden beim Wasserlassen U01~ U02~ U03~ U04~ U05~ Harnverhalt U08 Hinweise: " 
* #Y86 "Hydrozele"
* #Y86 ^definition = Inklusive:  Exklusive:  Kriterien: nicht schmerzhafte, fluktuierende Schwellung im Bereich der Hoden oder des Samenstrangs mit Nachweis der Schwellung durch Diaphanoskopie oder im bildgebenden Verfahren
* #Y86 ^property[0].code = #parent 
* #Y86 ^property[0].valueCode = #Y 
* #Y86 ^property[1].code = #Relationships 
* #Y86 ^property[1].valueString = "ICD-10 2017:N43.0~N43.1~N43.2~N43.3" 
* #Y86 ^property[2].code = #hints 
* #Y86 ^property[2].valueString = "Beachte: ander Symptome/Beschwerden des Hodens/Skrotums Y05 Hinweise: " 
* #Y99 "Andere Erkrankung des Genitales, Mann"
* #Y99 ^definition = Inklusive: Erkrankung der männlichen Brust~ Epididymalzyste~ Spermatozele~ Hodentorsion Exklusive: sexuell übertragene Erkrankung NNB A78~ Gynäkomastie Y16~ Brustkrebs beim Mann Y78 Kriterien:
* #Y99 ^property[0].code = #parent 
* #Y99 ^property[0].valueCode = #Y 
* #Y99 ^property[1].code = #Relationships 
* #Y99 ^property[1].valueString = "ICD-10 2017:A55~A56.1~A56.3~A56.4~A56.8~A57~A58~A63.8~N42.0~N42.1~N42.2~N42.8~N42.9~N43.4~N44~N48.0~N48.2~N48.5~N48.6~N48.8~N48.9~N49.1~N49.2~N49.8~N49.9~N50.0~N50.1~N50.8~N50.9~N51.0~N51.1~N51.2~N51.8~N64.8~N64.9~Z90.7" 
* #Z "Soziale Probleme"
* #Z ^property[0].code = #child 
* #Z ^property[0].valueCode = #Z01 
* #Z ^property[1].code = #child 
* #Z ^property[1].valueCode = #Z02 
* #Z ^property[2].code = #child 
* #Z ^property[2].valueCode = #Z03 
* #Z ^property[3].code = #child 
* #Z ^property[3].valueCode = #Z04 
* #Z ^property[4].code = #child 
* #Z ^property[4].valueCode = #Z05 
* #Z ^property[5].code = #child 
* #Z ^property[5].valueCode = #Z06 
* #Z ^property[6].code = #child 
* #Z ^property[6].valueCode = #Z07 
* #Z ^property[7].code = #child 
* #Z ^property[7].valueCode = #Z08 
* #Z ^property[8].code = #child 
* #Z ^property[8].valueCode = #Z09 
* #Z ^property[9].code = #child 
* #Z ^property[9].valueCode = #Z10 
* #Z ^property[10].code = #child 
* #Z ^property[10].valueCode = #Z11 
* #Z ^property[11].code = #child 
* #Z ^property[11].valueCode = #Z12 
* #Z ^property[12].code = #child 
* #Z ^property[12].valueCode = #Z13 
* #Z ^property[13].code = #child 
* #Z ^property[13].valueCode = #Z14 
* #Z ^property[14].code = #child 
* #Z ^property[14].valueCode = #Z15 
* #Z ^property[15].code = #child 
* #Z ^property[15].valueCode = #Z16 
* #Z ^property[16].code = #child 
* #Z ^property[16].valueCode = #Z18 
* #Z ^property[17].code = #child 
* #Z ^property[17].valueCode = #Z19 
* #Z ^property[18].code = #child 
* #Z ^property[18].valueCode = #Z20 
* #Z ^property[19].code = #child 
* #Z ^property[19].valueCode = #Z21 
* #Z ^property[20].code = #child 
* #Z ^property[20].valueCode = #Z22 
* #Z ^property[21].code = #child 
* #Z ^property[21].valueCode = #Z23 
* #Z ^property[22].code = #child 
* #Z ^property[22].valueCode = #Z24 
* #Z ^property[23].code = #child 
* #Z ^property[23].valueCode = #Z25 
* #Z ^property[24].code = #child 
* #Z ^property[24].valueCode = #Z27 
* #Z ^property[25].code = #child 
* #Z ^property[25].valueCode = #Z28 
* #Z ^property[26].code = #child 
* #Z ^property[26].valueCode = #Z29 
* #Z ^property[27].code = #child 
* #Z ^property[27].valueCode = #Z30 
* #Z ^property[28].code = #child 
* #Z ^property[28].valueCode = #Z31 
* #Z ^property[29].code = #child 
* #Z ^property[29].valueCode = #Z32 
* #Z ^property[30].code = #child 
* #Z ^property[30].valueCode = #Z33 
* #Z ^property[31].code = #child 
* #Z ^property[31].valueCode = #Z34 
* #Z ^property[32].code = #child 
* #Z ^property[32].valueCode = #Z35 
* #Z ^property[33].code = #child 
* #Z ^property[33].valueCode = #Z36 
* #Z ^property[34].code = #child 
* #Z ^property[34].valueCode = #Z37 
* #Z ^property[35].code = #child 
* #Z ^property[35].valueCode = #Z38 
* #Z ^property[36].code = #child 
* #Z ^property[36].valueCode = #Z39 
* #Z ^property[37].code = #child 
* #Z ^property[37].valueCode = #Z40 
* #Z ^property[38].code = #child 
* #Z ^property[38].valueCode = #Z41 
* #Z ^property[39].code = #child 
* #Z ^property[39].valueCode = #Z42 
* #Z ^property[40].code = #child 
* #Z ^property[40].valueCode = #Z43 
* #Z ^property[41].code = #child 
* #Z ^property[41].valueCode = #Z44 
* #Z ^property[42].code = #child 
* #Z ^property[42].valueCode = #Z45 
* #Z ^property[43].code = #child 
* #Z ^property[43].valueCode = #Z46 
* #Z ^property[44].code = #child 
* #Z ^property[44].valueCode = #Z47 
* #Z ^property[45].code = #child 
* #Z ^property[45].valueCode = #Z48 
* #Z ^property[46].code = #child 
* #Z ^property[46].valueCode = #Z49 
* #Z ^property[47].code = #child 
* #Z ^property[47].valueCode = #Z50 
* #Z ^property[48].code = #child 
* #Z ^property[48].valueCode = #Z51 
* #Z ^property[49].code = #child 
* #Z ^property[49].valueCode = #Z52 
* #Z ^property[50].code = #child 
* #Z ^property[50].valueCode = #Z53 
* #Z ^property[51].code = #child 
* #Z ^property[51].valueCode = #Z54 
* #Z ^property[52].code = #child 
* #Z ^property[52].valueCode = #Z56 
* #Z ^property[53].code = #child 
* #Z ^property[53].valueCode = #Z57 
* #Z ^property[54].code = #child 
* #Z ^property[54].valueCode = #Z58 
* #Z ^property[55].code = #child 
* #Z ^property[55].valueCode = #Z59 
* #Z ^property[56].code = #child 
* #Z ^property[56].valueCode = #Z60 
* #Z ^property[57].code = #child 
* #Z ^property[57].valueCode = #Z61 
* #Z ^property[58].code = #child 
* #Z ^property[58].valueCode = #Z62 
* #Z ^property[59].code = #child 
* #Z ^property[59].valueCode = #Z63 
* #Z ^property[60].code = #child 
* #Z ^property[60].valueCode = #Z64 
* #Z ^property[61].code = #child 
* #Z ^property[61].valueCode = #Z65 
* #Z ^property[62].code = #child 
* #Z ^property[62].valueCode = #Z66 
* #Z ^property[63].code = #child 
* #Z ^property[63].valueCode = #Z67 
* #Z ^property[64].code = #child 
* #Z ^property[64].valueCode = #Z68 
* #Z ^property[65].code = #child 
* #Z ^property[65].valueCode = #Z69 
* #Z01 "Armut/finanzielle Probleme"
* #Z01 ^property[0].code = #parent 
* #Z01 ^property[0].valueCode = #Z 
* #Z01 ^property[1].code = #hints 
* #Z01 ^property[1].valueString = "Beachte:  Hinweise: Probleme, die die Lebensbedingungen betreffen, setzen voraus, daß der Patient sich besorgt darüber geäußert hat, die Existenz des Probleme anerkennt und Hilfe wünscht. Unabhängig von den objektiv vorhandenen Bedingungen kann ein Patient seine Situation als problematisch beurteilen. Eine korrekte Klassifizierung dieser Probleme erfordert sowohl die Anerkennung absoluter Unterschiede in bezug auf die Lebensbedingungen als auch die Beachtung der Wahrnehmung durch die Einzelperson." 
* #Z02 "Probleme mit Ernährung/Wasser"
* #Z02 ^property[0].code = #parent 
* #Z02 ^property[0].valueCode = #Z 
* #Z02 ^property[1].code = #hints 
* #Z02 ^property[1].valueString = "Beachte:  Hinweise: Probleme, die die Lebensbedingungen betreffen, setzen voraus, daß der Patient sich besorgt darüber geäußert hat, die Existenz des Probleme anerkennt und Hilfe wünscht. Unabhängig von den objektiv vorhandenen Bedingungen kann ein Patient seine Situation als problematisch beurteilen. Eine korrekte Klassifizierung dieser Probleme erfordert sowohl die Anerkennung absoluter Unterschiede in bezug auf die Lebensbedingungen als auch die Beachtung der Wahrnehmung durch die Einzelperson." 
* #Z03 "Wohnungs-/Nachbarschaftsprobleme"
* #Z03 ^property[0].code = #parent 
* #Z03 ^property[0].valueCode = #Z 
* #Z03 ^property[1].code = #hints 
* #Z03 ^property[1].valueString = "Beachte:  Hinweise: Probleme, die die Lebensbedingungen betreffen, setzen voraus, daß der Patient sich besorgt darüber geäußert hat, die Existenz des Probleme anerkennt und Hilfe wünscht. Unabhängig von den objektiv vorhandenen Bedingungen kann ein Patient seine Situation als problematisch beurteilen. Eine korrekte Klassifizierung dieser Probleme erfordert sowohl die Anerkennung absoluter Unterschiede in bezug auf die Lebensbedingungen als auch die Beachtung der Wahrnehmung durch die Einzelperson." 
* #Z04 "Soziokulturelle Probleme"
* #Z04 ^definition = Inklusive: illegitime Schwangerschaft Exklusive: ungewollte Schwangerschaft W79 Kriterien:
* #Z04 ^property[0].code = #parent 
* #Z04 ^property[0].valueCode = #Z 
* #Z04 ^property[1].code = #hints 
* #Z04 ^property[1].valueString = "Beachte:  Hinweise: Probleme, die die Lebensbedingungen betreffen, setzen voraus, daß der Patient sich besorgt darüber geäußert hat, die Existenz des Probleme anerkennt und Hilfe wünscht. Unabhängig von den objektiv vorhandenen Bedingungen kann ein Patient seine Situation als problematisch beurteilen. Eine korrekte Klassifizierung dieser Probleme erfordert sowohl die Anerkennung absoluter Unterschiede in bezug auf die Lebensbedingungen als auch die Beachtung der Wahrnehmung durch die Einzelperson." 
* #Z05 "Probleme am Arbeitsplatz"
* #Z05 ^property[0].code = #parent 
* #Z05 ^property[0].valueCode = #Z 
* #Z05 ^property[1].code = #hints 
* #Z05 ^property[1].valueString = "Beachte:  Hinweise: Probleme, die die Arbeitsbedingungen betreffen, setzen voraus, daß der Patient sich besorgt darüber geäußert hat, die Existenz des Probleme anerkennt und Hilfe wünscht. Unabhängig von den objektiv vorhandenen Bedingungen kann ein Patient seine Situation als problematisch beurteilen. Eine korrekte Klassifizierung dieser Probleme erfordert sowohl die Anerkennung absoluter Unterschiede in bezug auf die Arbeitsbedingungen als auch die Beachtung der Wahrnehmung durch die Einzelperson." 
* #Z06 "Problem mit Arbeitslosigkeit"
* #Z06 ^definition = Inklusive:  Exklusive: Problem mit Berentung P25 Kriterien:
* #Z06 ^property[0].code = #parent 
* #Z06 ^property[0].valueCode = #Z 
* #Z06 ^property[1].code = #hints 
* #Z06 ^property[1].valueString = "Beachte:  Hinweise: Probleme, die Arbeitslosigkeit betreffen, setzen voraus, daß der Patient sich besorgt darüber geäußert hat, die Existenz des Probleme anerkennt und Hilfe wünscht. Unabhängig von den objektiv vorhandenen Bedingungen kann ein Patient seine Situation als problematisch beurteilen. Eine korrekte Klassifizierung dieser Probleme erfordert sowohl die Anerkennung absoluter Unterschiede in bezug auf Arbeitslosigkeit als auch die Beachtung der Wahrnehmung durch die Einzelperson." 
* #Z07 "Ausbildungsproblem"
* #Z07 ^definition = Inklusive: Analphabetismus Exklusive:  Kriterien:
* #Z07 ^property[0].code = #parent 
* #Z07 ^property[0].valueCode = #Z 
* #Z07 ^property[1].code = #hints 
* #Z07 ^property[1].valueString = "Beachte:  Hinweise: Bildungsprobleme setzen voraus, daß der Patient sich besorgt darüber geäußert hat, die Existenz des Probleme anerkennt und Hilfe wünscht. Unabhängig von den objektiv vorhandenen Bedingungen kann ein Patient seine Situation als problematisch beurteilen. Eine korrekte Klassifizierung dieser Probleme erfordert sowohl die Anerkennung absoluter Unterschiede in bezug auf Bildung als auch die Beachtung der Wahrnehmung durch die Einzelperson." 
* #Z08 "Problem mit Sozialhilfe"
* #Z08 ^property[0].code = #parent 
* #Z08 ^property[0].valueCode = #Z 
* #Z08 ^property[1].code = #hints 
* #Z08 ^property[1].valueString = "Beachte:  Hinweise: Probleme, die die soziale Fürsorge betreffen, setzen voraus, daß der Patient sich besorgt darüber geäußert hat, die Existenz des Probleme anerkennt und Hilfe wünscht. Unabhängig von den objektiv vorhandenen Bedingungen kann ein Patient seine Situation als problematisch beurteilen. Eine korrekte Klassifizierung dieser Probleme erfordert sowohl die Anerkennung absoluter Unterschiede in bezug auf die sozialen Verhältnisse als auch die Beachtung der Wahrnehmung durch die Einzelperson." 
* #Z09 "Rechtliches Problem"
* #Z09 ^property[0].code = #parent 
* #Z09 ^property[0].valueCode = #Z 
* #Z09 ^property[1].code = #hints 
* #Z09 ^property[1].valueString = "Beachte:  Hinweise: Probleme, die Rechtsfragen betreffen, setzen voraus, daß der Patient sich besorgt darüber geäußert hat, die Existenz des Probleme anerkennt und Hilfe wünscht. Unabhängig von den objektiv vorhandenen Bedingungen kann ein Patient seine Situation als problematisch beurteilen. Eine korrekte Klassifizierung dieser Probleme erfordert sowohl die Anerkennung absoluter Unterschiede in bezug auf rechtliche Verhältnisse als auch die Beachtung der Wahrnehmung durch die Einzelperson." 
* #Z10 "Problem mit Gesundheitssystem"
* #Z10 ^property[0].code = #parent 
* #Z10 ^property[0].valueCode = #Z 
* #Z10 ^property[1].code = #Relationships 
* #Z10 ^property[1].valueString = "ICD-10 2017:Z75.0~Z75.1~Z75.2~Z75.3~Z75.4~Z75.5~Z75.8~Z75.9" 
* #Z10 ^property[2].code = #hints 
* #Z10 ^property[2].valueString = "Beachte:  Hinweise: Probleme, die das Gesundheitssystem betreffen, setzen voraus, daß der Patient sich besorgt darüber geäußert hat, die Existenz des Probleme anerkennt und Hilfe wünscht. Unabhängig von den objektiv vorhandenen Bedingungen kann ein Patient seine Situation als problematisch beurteilen. Eine korrekte Klassifizierung dieser Probleme erfordert sowohl die Anerkennung absoluter Unterschiede in bezug auf das Gesundheitssystem als auch die Beachtung der Wahrnehmung durch die Einzelperson." 
* #Z11 "Problem mit Compliance/Kranksein"
* #Z11 ^definition = Inklusive: mangelnde Mitarbeit (Compliance) des Patienten Exklusive:  Kriterien:
* #Z11 ^property[0].code = #parent 
* #Z11 ^property[0].valueCode = #Z 
* #Z11 ^property[1].code = #Relationships 
* #Z11 ^property[1].valueString = "ICD-10 2017:Z75.0~Z75.1~Z75.2~Z75.3~Z75.4~Z75.5~Z75.8~Z75.9" 
* #Z11 ^property[2].code = #hints 
* #Z11 ^property[2].valueString = "Beachte:  Hinweise: Die Diagnose sozialer Probleme aufgrund von Krankheit setzt voraus, dass der Patient die Existenz des Problems anerkennt und Hilfe wünscht." 
* #Z12 "Beziehungsproblem mit Partner/der Partnerin"
* #Z12 ^definition = Inklusive: emotionaler Missbrauch/ Misshandlung Exklusive: körperliche/ sexuelle Misshandlung Z25 Kriterien:
* #Z12 ^property[0].code = #parent 
* #Z12 ^property[0].valueCode = #Z 
* #Z12 ^property[1].code = #Relationships 
* #Z12 ^property[1].valueString = "ICD-10 2017:T74.0~T74.3" 
* #Z12 ^property[2].code = #hints 
* #Z12 ^property[2].valueString = "Beachte:  Hinweise: Die Diagnose Beziehungsprobleme zwischen Familienpartnern setzt voraus, dass der Patient die Existenz des Problems anerkennt und Hilfe wünscht." 
* #Z13 "Problem durch Verhalten des Partners"
* #Z13 ^definition = Inklusive: Untreue~ körperliche Misshandlung Exklusive:  Kriterien:
* #Z13 ^property[0].code = #parent 
* #Z13 ^property[0].valueCode = #Z 
* #Z13 ^property[1].code = #hints 
* #Z13 ^property[1].valueString = "Beachte:  Hinweise: Die Diagnose Probleme durch das Verhalten von Familienpartnern setzt voraus, dass der Patient die Existenz des Problems anerkennt und Hilfe wünscht." 
* #Z14 "Problem durch Erkrankung des Partners"
* #Z14 ^property[0].code = #parent 
* #Z14 ^property[0].valueCode = #Z 
* #Z14 ^property[1].code = #hints 
* #Z14 ^property[1].valueString = "Beachte:  Hinweise: Die Diagnose Probleme aufgrund der Krankheit eines oder beider Familienpartner setzt voraus, dass der Patient die Existenz des Problems anerkennt und Hilfe wünscht." 
* #Z15 "Problem durch Verlust/Tod des Partners"
* #Z15 ^definition = Inklusive: Trauer~ Scheidung~ Trennung Exklusive:  Kriterien:
* #Z15 ^property[0].code = #parent 
* #Z15 ^property[0].valueCode = #Z 
* #Z15 ^property[1].code = #hints 
* #Z15 ^property[1].valueString = "Beachte:  Hinweise: Die Diagnose Probleme aufgrund des Verlusts eines Familienpartners setzt voraus, dass der Patient die Existenz des Problems anerkennt und Hilfe wünscht." 
* #Z16 "Beziehungsproblem mit Kind"
* #Z16 ^definition = Inklusive: emotionale Misshandlung/ Missbrauch eines Kindes Exklusive: körperliche/ sexuelle Misshandlung Z25 Kriterien:
* #Z16 ^property[0].code = #parent 
* #Z16 ^property[0].valueCode = #Z 
* #Z16 ^property[1].code = #Relationships 
* #Z16 ^property[1].valueString = "ICD-10 2017:T74.0~T74.3" 
* #Z16 ^property[2].code = #hints 
* #Z16 ^property[2].valueString = "Beachte:  Hinweise: Die Diagnose Probleme in der Beziehung zu einem Kind setzt voraus, dass der Patient die Existenz des Problems anerkennt und Hilfe wünscht." 
* #Z18 "Problem durch Erkrankung des Kindes"
* #Z18 ^property[0].code = #parent 
* #Z18 ^property[0].valueCode = #Z 
* #Z18 ^property[1].code = #hints 
* #Z18 ^property[1].valueString = "Beachte:  Hinweise: Die Diagnose Probleme aufgrund der Krankheit eines Kindes setzt voraus, dass der Patient die Existenz des Problems anerkennt und Hilfe wünscht." 
* #Z19 "Problem durch Verlust/Tod des Kindes"
* #Z19 ^property[0].code = #parent 
* #Z19 ^property[0].valueCode = #Z 
* #Z19 ^property[1].code = #hints 
* #Z19 ^property[1].valueString = "Beachte:  Hinweise: Die Diagnose Probleme aufgrund des Verlustes eines Kindes in der Familie setzt voraus, dass der Patient die Existenz des Problems anerkennt und Hilfe wünscht." 
* #Z20 "Beziehungsproblem mit Elternteil/Familie"
* #Z20 ^definition = Inklusive: Beziehungsproblem mit Elternteil/ erwachsenem Geschwister/ anderem Familienmitglied Exklusive: Beziehungsproblem mit Partner Z12~ Beziehungsproblem mit Kind Z16~beziehungsproblem mit Freund(in) Z24 Kriterien:
* #Z20 ^property[0].code = #parent 
* #Z20 ^property[0].valueCode = #Z 
* #Z20 ^property[1].code = #Relationships 
* #Z20 ^property[1].valueString = "ICD-10 2017:T74.0" 
* #Z20 ^property[2].code = #hints 
* #Z20 ^property[2].valueString = "Beachte:  Hinweise: Die Diagnose Probleme in der Beziehung zwischen Familienmitgliedern setzt voraus, dass der Patient die Existenz des Problems anerkennt und Hilfe wünscht." 
* #Z21 "Problem durch Verhalten Elternteil/Familie"
* #Z21 ^definition = Inklusive:  Exklusive: Symptom/ Beschwerden bzgl. Verhalten Kind P22~Symptom/ Beschwerden bzgl. Verhalten Jugendlicher P23~ Problem durch Verhalten des Partners Z13 Kriterien:
* #Z21 ^property[0].code = #parent 
* #Z21 ^property[0].valueCode = #Z 
* #Z21 ^property[1].code = #hints 
* #Z21 ^property[1].valueString = "Beachte:  Hinweise: Die Diagnose Probleme durch das Verhalten von Familienmitgliedern voraus, dass der Patient die Existenz des Problems anerkennt und Hilfe wünscht." 
* #Z22 "Problem durch Erkrankung Elternteil/Familie"
* #Z22 ^definition = Inklusive:  Exklusive: Problem mit Erkrankung des Partners Z14 Kriterien:
* #Z22 ^property[0].code = #parent 
* #Z22 ^property[0].valueCode = #Z 
* #Z22 ^property[1].code = #hints 
* #Z22 ^property[1].valueString = "Beachte:  Hinweise: Die Diagnose Probleme aufgrund der Krankheit eines Famlilienmitgliedes setzt voraus, dass der Patient die Existenz des Problems anerkennt und Hilfe wünscht." 
* #Z23 "Problem durch Verlust/Tod Elternteil/Familienmitglied"
* #Z23 ^definition = Inklusive:  Exklusive: Verlust eines Partners Z15~ Verlust eines Kindes Z19 Kriterien:
* #Z23 ^property[0].code = #parent 
* #Z23 ^property[0].valueCode = #Z 
* #Z23 ^property[1].code = #hints 
* #Z23 ^property[1].valueString = "Beachte:  Hinweise: Die Diagnose Probleme durch den Verlust oder Tod eines Familienmitgliedes setzt voraus, dass der Patient die Existenz des Problems anerkennt und Hilfe wünscht." 
* #Z24 "Beziehungsproblem mit Freunden"
* #Z24 ^definition = Inklusive:  Exklusive: Beziehungsproblem mit einem Familienmitglied Z20 Kriterien:
* #Z24 ^property[0].code = #parent 
* #Z24 ^property[0].valueCode = #Z 
* #Z24 ^property[1].code = #hints 
* #Z24 ^property[1].valueString = "Beachte:  Hinweise: Die Diagnose Probleme in the relationship with friends setzt voraus, dass der Patient die Existenz des Problems anerkennt und Hilfe wünscht." 
* #Z25 "Körperliche Misshandlung/sexueller Missbrauch"
* #Z25 ^definition = Inklusive: Opfer körperlicher Gewalt~ Vergewaltigung~ sexueller Belästigung Exklusive: Körperliches Probleme in der entsprechenden Rubrik in anderen Kapiteln kodieren~ psychische Probleme in Kapitel P kodieren Kriterien:
* #Z25 ^property[0].code = #parent 
* #Z25 ^property[0].valueCode = #Z 
* #Z25 ^property[1].code = #Relationships 
* #Z25 ^property[1].valueString = "ICD-10 2017:T74.1~T74.2~T74.8~T74.9" 
* #Z25 ^property[2].code = #hints 
* #Z25 ^property[2].valueString = "Beachte:  Hinweise: Die Diagnose sozialer Probleme durch einen tätlichen Angriff oder andere schädigende Ereignisse setzt voraus, dass der Patient die Existenz des Problems anerkennt und Hilfe wünscht." 
* #Z27 "Angst vor sozialen Problemen"
* #Z27 ^definition = Inklusive: Sorge um/Angst vor einem sozialen Problem Exklusive: Wenn des Patient ein soziales Problem hat~ ist das Problem zu kodieren Kriterien: Furcht vor einem sozialen Problem bei einem Patienten ohne soziales Problem
* #Z27 ^property[0].code = #parent 
* #Z27 ^property[0].valueCode = #Z 
* #Z27 ^property[1].code = #Relationships 
* #Z27 ^property[1].valueString = "ICD-10 2017:Z71.1" 
* #Z28 "Funktionseinschränkung/Behinderung (Z)"
* #Z28 ^definition = Inklusive:  Exklusive:  Kriterien: Funktionseinschränkung/Behinderung durch ein soziales Problem, einschließlich Isolation/Alleinleben/Einsamkeit
* #Z28 ^property[0].code = #parent 
* #Z28 ^property[0].valueCode = #Z 
* #Z28 ^property[1].code = #Relationships 
* #Z28 ^property[1].valueString = "ICD-10 2017:Z73.4~Z73.6" 
* #Z28 ^property[2].code = #hints 
* #Z28 ^property[2].valueString = "Beachte:  Hinweise: COOP/WONCA Charts eignen sich zur Dokumentation des funktionellen Status des Patienten (siehe Kapitel 8)" 
* #Z29 "Soziale Probleme NNB"
* #Z29 ^definition = Inklusive: Umweltprobleme~ Umweltverschmutzung~ Simulantentum Exklusive:  Kriterien:
* #Z29 ^property[0].code = #parent 
* #Z29 ^property[0].valueCode = #Z 
* #Z29 ^property[1].code = #Relationships 
* #Z29 ^property[1].valueString = "ICD-10 2017:Z72.6~Z72.8~Z72.9~Z73.5~Z73.8~Z73.9~Z76.5" 
* #Z30 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? vollständig"
* #Z30 ^property[0].code = #parent 
* #Z30 ^property[0].valueCode = #Z 
* #Z31 "Ärztliche Untersuchung/Beurteilung des Gesundheitszustands ? teilweise"
* #Z31 ^property[0].code = #parent 
* #Z31 ^property[0].valueCode = #Z 
* #Z32 "Allergie-/Sensitivitätstestung"
* #Z32 ^property[0].code = #parent 
* #Z32 ^property[0].valueCode = #Z 
* #Z33 "Mikrobiologische/immunologische Untersuchung"
* #Z33 ^property[0].code = #parent 
* #Z33 ^property[0].valueCode = #Z 
* #Z34 "Blutuntersuchung"
* #Z34 ^property[0].code = #parent 
* #Z34 ^property[0].valueCode = #Z 
* #Z35 "Urinuntersuchung"
* #Z35 ^property[0].code = #parent 
* #Z35 ^property[0].valueCode = #Z 
* #Z36 "Stuhluntersuchung"
* #Z36 ^property[0].code = #parent 
* #Z36 ^property[0].valueCode = #Z 
* #Z37 "Histologische Untersuchung/zytologischer Abstrich"
* #Z37 ^property[0].code = #parent 
* #Z37 ^property[0].valueCode = #Z 
* #Z38 "Andere Laboruntersuchung NAK"
* #Z38 ^property[0].code = #parent 
* #Z38 ^property[0].valueCode = #Z 
* #Z39 "Körperliche Funktionsprüfung"
* #Z39 ^property[0].code = #parent 
* #Z39 ^property[0].valueCode = #Z 
* #Z40 "Diagnostische Endoskopie"
* #Z40 ^property[0].code = #parent 
* #Z40 ^property[0].valueCode = #Z 
* #Z41 "Diagnostisches Röntgen/Bildgebung"
* #Z41 ^property[0].code = #parent 
* #Z41 ^property[0].valueCode = #Z 
* #Z42 "Elektrische Aufzeichnungsverfahren"
* #Z42 ^property[0].code = #parent 
* #Z42 ^property[0].valueCode = #Z 
* #Z43 "Andere diagnostische Untersuchung"
* #Z43 ^property[0].code = #parent 
* #Z43 ^property[0].valueCode = #Z 
* #Z44 "Präventive Impfung/Medikation"
* #Z44 ^property[0].code = #parent 
* #Z44 ^property[0].valueCode = #Z 
* #Z45 "Beobachtung/Gesundheitsschulung/Beratung/Diät"
* #Z45 ^property[0].code = #parent 
* #Z45 ^property[0].valueCode = #Z 
* #Z46 "Konsultation eines anderen Primärversorgers"
* #Z46 ^property[0].code = #parent 
* #Z46 ^property[0].valueCode = #Z 
* #Z47 "Konsultation eines Facharztes"
* #Z47 ^property[0].code = #parent 
* #Z47 ^property[0].valueCode = #Z 
* #Z48 "Klärung/Besprechung des Beratungsanlasses mit dem Patienten"
* #Z48 ^property[0].code = #parent 
* #Z48 ^property[0].valueCode = #Z 
* #Z49 "Andere Vorsorgemaßnahme"
* #Z49 ^property[0].code = #parent 
* #Z49 ^property[0].valueCode = #Z 
* #Z50 "Medikation/Verschreibung/Erneuerung/Injektion"
* #Z50 ^property[0].code = #parent 
* #Z50 ^property[0].valueCode = #Z 
* #Z51 "Inzision/Drainage/Spülung/Absaugung/Entfernung von Körperflüssigkeit"
* #Z51 ^property[0].code = #parent 
* #Z51 ^property[0].valueCode = #Z 
* #Z52 "Exzision/Entfernung von Gewebe/Biopsie/Destruktion/Debridement/Verkauterung"
* #Z52 ^property[0].code = #parent 
* #Z52 ^property[0].valueCode = #Z 
* #Z53 "Instrumentelle Manipulation/Katheterisierung/Intubation/Dilatation"
* #Z53 ^property[0].code = #parent 
* #Z53 ^property[0].valueCode = #Z 
* #Z54 "Reposition/Fixierung/Naht/Gips/prothetische Versorgung"
* #Z54 ^property[0].code = #parent 
* #Z54 ^property[0].valueCode = #Z 
* #Z56 "Verband/Druck/Kompression/Tamponade"
* #Z56 ^property[0].code = #parent 
* #Z56 ^property[0].valueCode = #Z 
* #Z57 "Physikalische Therapie/Rehabilitation"
* #Z57 ^property[0].code = #parent 
* #Z57 ^property[0].valueCode = #Z 
* #Z58 "Therapeutische Beratung/Zuhören"
* #Z58 ^property[0].code = #parent 
* #Z58 ^property[0].valueCode = #Z 
* #Z59 "Andere therapeutische Maßnahme/kleine Chirurgie NAK"
* #Z59 ^property[0].code = #parent 
* #Z59 ^property[0].valueCode = #Z 
* #Z60 "Testresultat/Ergebnis eigene Maßnahme"
* #Z60 ^property[0].code = #parent 
* #Z60 ^property[0].valueCode = #Z 
* #Z61 "Ergebnis Untersuchung/Test/Brief von anderem Leistungserbringer"
* #Z61 ^property[0].code = #parent 
* #Z61 ^property[0].valueCode = #Z 
* #Z62 "Adminstrative Maßnahme"
* #Z62 ^property[0].code = #parent 
* #Z62 ^property[0].valueCode = #Z 
* #Z63 "Folgekonsultation unspezifiziert"
* #Z63 ^property[0].code = #parent 
* #Z63 ^property[0].valueCode = #Z 
* #Z64 "Konsultation/Problembehandlung auf Initiative des Leistungserbringers"
* #Z64 ^property[0].code = #parent 
* #Z64 ^property[0].valueCode = #Z 
* #Z65 "Konsultation/Problembehandlung auf Initiative Dritter"
* #Z65 ^property[0].code = #parent 
* #Z65 ^property[0].valueCode = #Z 
* #Z66 "Überweisung an andere/n Leistungserbringer (nicht ärztl.)/Pflegekraft/Therapeuten/Sozialarbeiter"
* #Z66 ^property[0].code = #parent 
* #Z66 ^property[0].valueCode = #Z 
* #Z67 "Überweisung an einen Arzt/Spezialisten/eine Klinik/ein Krankenhaus"
* #Z67 ^property[0].code = #parent 
* #Z67 ^property[0].valueCode = #Z 
* #Z68 "Andere Überweisung NAK"
* #Z68 ^property[0].code = #parent 
* #Z68 ^property[0].valueCode = #Z 
* #Z69 "Anderer Beratungsanlass NAK"
* #Z69 ^property[0].code = #parent 
* #Z69 ^property[0].valueCode = #Z 
