# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0+20221010]

### Added

- [TERGI-116](https://jira-neu.elga.gv.at/browse/TERGI-116) Force manual pipeline triggering for branches which are not the default branch.

### Changed

- [TERGI-87](https://jira-neu.elga.gv.at/browse/TERGI-87) and [TERGI-67](https://jira-neu.elga.gv.at/browse/TERGI-67) Improve creation and display of previous versions. From now on, previous versions will only be created if the business version of a terminology (`ValueSet.version`, `CodeSystem.version`, etc.) has been changed.

## [0.13.0+20220929]

### Added

- [TERGI-73](https://jira-neu.elga.gv.at/browse/TERGI-73) Add support for new MaLaC-CT parameter `tergiTunnelToken` which allows access to all API endpoints of a tergi.

### Changed

- Resources in `input/resources` will not be deleted by default prior to IG Publisher invocation. As a result, e.g. `ConceptMap` resources may be included.

## [0.12.2+20220831]

### Fixed

- Fix URLs to images.

## [0.12.1+20220818]

### Added

- Additional stage in pipeline checking if required environment variables have got a value.

### Fixed

- [TERGI-100](https://jira-neu.elga.gv.at/browse/TERGI-100) Support creation and editing of terminologies in branches including merging them into the default branch.

## [0.12.0+20220729]

### Changed

- [TERGI-65](https://jira-neu.elga.gv.at/browse/TERGI-65) Pipeline fails if user lacks permissions for pushing to default branch or if CI/CD variable for the user has not been configured.
- [TERGI-69](https://jira-neu.elga.gv.at/browse/TERGI-69) Let pipeline fail if certain commands within `create-terminologit.sh` fail. Generally improve pipeline execution.

### Fixed

- [TERGI-38](https://jira-neu.elga.gv.at/browse/TERGI-38) Check if directories required for execution exist.

## [0.11.0+20220708]

### Changed

- [TERGI-88](https://jira-neu.elga.gv.at/browse/TERGI-88) Pipeline will fail if a terminology cannot be successfully converted by MaLaC-CT.
- [TERGI-14](https://jira-neu.elga.gv.at/browse/TERGI-14) New documentation structure, splitting governance and tech, with automatic move of needed files

## [0.10.1+20220607]

### Fixed

- Fix memory issue by removing unused outputs

## [0.10.0+20220603]

### Added

- [TERGI-64](https://jira-neu.elga.gv.at/browse/TERGI-64) Add `.2.outdatedcsv.xml` and `.2.svsextelga.xml` both of which will provide all concept properties.

## [0.9.2+20220516]

### Fixed

- [TERGI-81](https://jira-neu.elga.gv.at/browse/TERGI-81) Fix using whole url for references to codesystems

## [0.9.1+20220504]

### Changed

- [CSD-1901](https://jira-neu.elga.gv.at/browse/CSD-1901) For `.1.svsextelga.xml` and for `.1.outdatedcsv.csv` it has been highlighted that these formats are deprecated.

## [0.9.0+20220503]

### Changed

- Prevent `output/*.zip`, `output/*.tgz`, `output/*.xlsx`, and `output/*.pack` from being pushed to the HTML project as that would bloat the repository unnecessarily.

## [0.8.1+20220503]

### Fixed

- Clone HTML project in `update_html_project` job as it is not possible to pass it on as artifacts (size limit in GitLab.com is 1G [https://docs.gitlab.com/ee/user/gitlab_com/index.html#gitlab-cicd](https://docs.gitlab.com/ee/user/gitlab_com/index.html#gitlab-cicd)).

## [0.8.0+20220503]

### Changed

- Provide separate job for updating HTML project.

## [0.7.2+20220422]

### Changed

- Improve performance by cloning termgit-html-project only once.

## [0.7.1+20220421]

### Fixed

- Copy "Zuppl" terminologies output directory in order to preserver functionality of versioned files.

## [0.7.0+20220420]

### Changed

- Remove obsolete MaLaC-CT CLI parameters.

## [0.6.1+20220411]

### Fixed

- Rules for creating `robots.txt`.
- Change contact for IG.

## [0.6.0+20220408]

### Added

- Add english pages (`index_en.md`, `faq_en.md`).

## [0.5.2+20220318]

### Fixed

- Touch `processedTerminologies.csv` after execution of MaLaC-CT because MaLaC-CT creates column headers only if file does not exist.

## [0.5.1+20220317]

### Fixed

- Create empty `processedTerminologies.csv` if no terminologies have been processed by MaLaC-CT.

## [0.5.0+20220315]

### Added

- [TERGI-12](https://jira-neu.elga.gv.at/browse/TERGI-12) Support legacy file formats.
- [TERGI-18](https://jira-neu.elga.gv.at/browse/TERGI-18) Create Git tags for OIDs of terminologies.
- Add job `skip_malac-ct` allowing to skip execution of MaLaC-CT.

### Changed

- Convert all CodeSystems before any ValueSet is processed by MaLaC-CT.

## [0.4.0+20220301]

### Added

- Log version of [elgagmbh/fsh-ing-grounds](https://hub.docker.com/r/elgagmbh/fsh-ing-grounds) in pipeline.

### Changed

- Due to a new version of [elgagmbh/fsh-ing-grounds](https://hub.docker.com/r/elgagmbh/fsh-ing-grounds) some of the additional installations of dependencies are no longer necessary. They are provided within the docker image.

## [0.3.0+20220223]

### Added

- Add pipeline job `run_malac-ct_integration_test` for enabling full integration test.

### Changed

- Explicitly state expiry period for artifacts.
- Refactor pipeline and related scripts.

## Fixed

- Make sure `input/includes` directory exists.

## [0.2.0+20220218]

### Added

- [TERGI-3](https://jira-neu.elga.gv.at/browse/TERGI-3) Filter certain files from being included into `sitemap.xml`.

### Fixed

- [TERGI-50](https://jira-neu.elga.gv.at/browse/TERGI-50) Correct versioning of files.

## [0.1.0+20220215]

### Added

- Create `*-download.xhtml` and `*-previous-versions.xhtml` for zuppl-files automatically.
- Add specification of pages to `sushi-config.yaml`.
- Support different converting priorities based on resource type (e.g. CodeSystem).
- Create `sitemap.xml` and `robots.txt` automatically.
- Make upload of terminologies by MaLaC-CT to FHIR server configurable.

### Changed

- Update title of IG.
- Move specification of menu from `menu.xml` to `sushi-config.yaml`.
- Make pipeline repository independent.

### Fixed

- Detect newly created zuppl-files correctly.
- Add check for changed resource type (e.g. CodeSystem).
- Create separate log files for extensive commands.
- Keep download links in versioned files unchanged.
- Keep "Defining URL" within versioned files unchanged.
- Set Xmx to 12g for execution of IG publisher in order to have enough non-heap memory.
- Set default expiry period of artifacts to 1 week.
- Create tags for non-zuppl-terminologies only.

## [0.0.1+20220101]

- Start of parallel operation of TerminoloGit **[https://termgit.elga.gv.at/](https://termgit.elga.gv.at/)** and [https://termpub.gesundheit.gv.at/TermBrowser/gui/main/main.zul](https://termpub.gesundheit.gv.at/TermBrowser/gui/main/main.zul)

