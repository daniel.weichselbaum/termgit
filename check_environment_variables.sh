#!/bin/bash

log () {
  echo " "
  echo "##############################"
  echo "ERROR: Environment variable $1 does not have a value. $2"
  echo "##############################"
  echo " "
}

exitcode=0

[[ -z "$FHIRServer" ]] && { log "FHIRServer" "Create a CI/CD variable having that name."; exitcode=1; }
[[ -z "$FHIRServerUser" ]] && { log "FHIRServerUser" "Create a CI/CD variable having that name."; exitcode=1; }
[[ -z "$FHIRServerUserPw" ]] && { log "FHIRServerUserPw" "Create a CI/CD variable having that name."; exitcode=1; }
[[ -z "$TERMGIT_CANONICAL" ]] && { log "TERMGIT_CANONICAL" "Create a CI/CD variable having that name."; exitcode=1; }
[[ -z "$TERMGIT_HTML_PROJECT" ]] && { log "TERMGIT_HTML_PROJECT" "Create a CI/CD variable having that name."; exitcode=1; }
[[ -z "$TERMGIT_HTML_PROJECT_DEFAULT_BRANCH" ]] && { log "TERMGIT_HTML_PROJECT_DEFAULT_BRANCH" "Create a CI/CD variable having that name."; exitcode=1; }
[[ -z "$GITLAB_CI_TOKEN" ]] && { log "GITLAB_CI_TOKEN" "Create a CI/CD variable for the user triggering this pipeline: [username]_GITLAB_CI_TOKEN"; exitcode=1; }

exit $exitcode
