> **For the english version please click [here](arch_and_setup_en.html).**

### Die ELGA & österreichische eHealth-Terminologien

In ELGA und den österreichischen eHealth-Systemen wird eine Vielzahl unterschiedlicher Terminologien mit unterschiedlicher Struktur von verschiedenen internationalen, nationalen und lokalen Quellen verwendet. Die aktuellen Terminologien und die Governance sind unter [termgit.elga.gv.at](https://termgit.elga.gv.at) zu finden. Technologisch basiert der österreichische e-Health Terminologie-Browser auf TerminoloGit.

Im Sinne des Open-Source-Gedankens würden sich die Entwickler sehr über die Nutzung des Terminologieservers in anderen Projekten, Feedback und noch mehr über eine aktive Beteiligung freuen. Verbesserungsvorschläge oder Fehlermeldungen, die bei TerminoloGit erkannt wurden, können über das GitLab-Ticketsystem unter [https://gitlab.com/elga-gmbh/termgit/-/issues/new](https://gitlab.com/elga-gmbh/termgit/-/issues/new) eingemeldet werden.

### Verwendung und Web Services

Im Folgenden werden die unterschiedliche Verwendung des Terminologie-Browsers und der WebServices aufgezeigt.

#### Browsing/Recherche
*Akteur:* Student

Der Browser eignet sich hervorragend für Recherchezwecke oder einfach nur zum Durchstöbern, da alle Listen in übersichtlichen HTML Seiten dargestellt werden. Der Anwender hat die Möglichkeit verschiedene Codesysteme und Value Sets einzeln in strukturierter Form abzurufen oder mit Hilfe der Suchfunktion ganz bestimmte Konzepte zu suchen. Zwischen den Codesystemen und Value Sets besteht auch immer eine Verknüpfung, um zusammen gehörige Listen leichter ausfindig zu machen.

#### Automatischer Import von Katalogen
*Akteur:* System

Damit Ihre Systeme immer auf dem aktuellsten Stand bleiben, gibt es die Möglichkeit des automatischen Imports von Katalogen. Es gibt mehrere Varianten wie Sie sicherstellen können, dass Sie immer die aktuellsten Listen in Ihrem System führen. Die hier dargestellten Varianten konzentrieren sich auf die Aktualisierung einer bestimmten Terminologie.

Grundsätzlich kann mit folgenden aufgebautem URL immer die aktuellste Version einer Terminologie abgerufen werden:

    https://gitlab.com/elga-gmbh/termgit/-/raw/main/terminologies/<NAME_OF_TERMINOLOGY>/<NAME_OF_TERMINOLOGY><FORMAT>

Dabei entspricht
- der `<NAME_OF_TERMINOLOGY>` dem Typ der Terminologie (`CodeSystem` oder `ValueSet`) sowie Namen der Terminologie (z.B. `iso-3166-1-alpha-3`). Der Typ und der Name werden mit `-` verknüpft: `CodeSystem-iso-3166-1-alpha-3`
- das `<FORMAT>` der Dateiendung eines der angebotenen Formate (siehe [Downloadformate](#downloadformate)), z.B. `.4.fhir.xml`

Das Ergebnis sieht dann wie folgt aus:

    https://gitlab.com/elga-gmbh/termgit/-/raw/main/terminologies/CodeSystem-iso-3166-1-alpha-3/CodeSystem-iso-3166-1-alpha-3.4.fhir.xml

Anhand eines bestimmten Formats einer Terminologie werden in den nächsten Punkten verschiedene Arten des Abrufs dargestellt:
1. Als stabiler https-Link zur Datei, ohne Versionsangaben:
   * Python:

         response = urllib2.urlopen('https://gitlab.com/elga-gmbh/termgit/-/raw/main/terminologies/CodeSystem-iso-3166-1-alpha-3/CodeSystem-iso-3166-1-alpha-3.2.claml.xml?inline=false')
   * Java:

         URL url = new URL("https://gitlab.com/elga-gmbh/termgit/-/raw/main/terminologies/CodeSystem-iso-3166-1-alpha-3/CodeSystem-iso-3166-1-alpha-3.2.claml.xml?inline=false");
         URLConnection connection = url.openConnection();
         InputStream is = connection.getInputStream();
         // .. then download the file
2. Mit Git über ssh in einem lokalen Repository und den Git-Tags für die jeweilige Terminologie:
   * Erstmaliges klonen des Repositories:

         git clone git@gitlab.com:elga-gmbh/termgit.git

   * Aktualisieren des lokalen Git-Reposistories (inkl. Git-Tags):

         git fetch --tags -f

   * Prüfen des Unterschieds zwischen dem aktuellen, lokalen Verzeichnisinhalt und dem des Git-Tags einer Terminologie nach dem Aktualisieren des Repositories:

         git log HEAD..tags/CodeSytem-iso-3166-1-alpha-3

     oder

         git log HEAD..tags/1.0.3166.1.2.3

   * Auschecken der aktuellsten Version einer Terminologie mittels des Git-Tags. **Hinweis:** Dabei wird das gesamte Repository auf den Stand des Git-Tags gesetzt. Andere Terminologien könnten einen aktuelleren Stand haben.

         git checkout tags/CodeSystem-iso-3166-1-alpha-3

     oder

         git checkout tags/1.0.3166.1.2.3

3. Mit git über ssh ohne lokalem Repository und den Git-Tags für die jeweiligen Terminologie-Order (einzelne Dateien sind hier nicht möglich):

       git archive -o C:/tmp/CS-ISO-3166-1-alpha-3.zip --remote git@gitlab.com:elga-gmbh/termgit.git CodeSystem-iso-3166-1-alpha-3:terminologies/CodeSystem-iso-3166-1-alpha-3

   oder

       git archive -o C:/tmp/CS-ISO-3166-1-alpha-3.zip --remote git@gitlab.com:elga-gmbh/termgit.git 1.0.3166.1.2.3:terminologies/CodeSystem-iso-3166-1-alpha-3

4. Mit GitLab API über REST für ein bestimmtes Format (hier .2.claml.xml; '/' in dem GitLab-Repo-Pfad müssen mit '%2f' escaped werden)

       curl https://gitlab.com/api/v4/projects/33179072/repository/files/terminologies%2fCodeSystem-iso-3166-1-alpha-3%2fCodeSystem-iso-3166-1-alpha-3.2.claml.xml?ref=main

5. Mit FHIR über REST (TerGi mit FHIR Server kommt mit TerGi Alpha Release im Q3 2022):
   * curl:

         curl https://<FHIR SERVER NAME>.at/CodeSystem/CodeSystem-iso-3166-1-alpha-3

   * mehr Informationen finden Sie hier: [http://www.hl7.org/fhir/overview-dev.html#Interactions](http://www.hl7.org/fhir/overview-dev.html#Interactions)
   * für Testzwecke kann beispielsweise der folgende FHIR Server verwendet werden

         curl http://hapi.fhir.org/baseR4/CodeSystem/2559241

#### Abruf von Metainformationen eines Git-Tags

Für die automatisierte Überprüfung, ob eine bestimmte Terminologie aktualisiert wurde, kann die [GitLab-API für Git-Tags](https://docs.gitlab.com/ee/api/tags.html) verwendet werden. Mit Hilfe des folgenden Befehls können die Metadaten eines Git-Tags abgerufen werden:

    curl https://gitlab.com/api/v4/projects/33179072/repository/tags/CodeSystem-iso-3166-1-alpha-3

In den Metadaten befindet sich unter anderem das Datum, an dem der Git-Tag erstellt wurde (`created_at`).

#### Manuelle Aktualisierung von Terminologien
*Akteur:* Stammdaten-/System-Admin

Neben dem automatischen Import von Katalogen, besteht ebenfalls die Möglichkeit, manuell alle von Ihnen verwendeten Terminologien zu aktualisieren. Dafür empfehlen wir die Tag-Neuigkeiten in GitLab zu aktivieren. Bei jeder Änderung erhalten Sie dann eine automatische Benachrichtigung.

[![tag-abonnieren](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/Tag-abonnieren.png "tag-Neuigkeiten-abonnieren"){: style="width: 100%"}](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/Tag-abonnieren.png)

[![manuelles-mapping](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/automatische-benachrichtigung.png "Manuelles-Mapping"){: style="width: 100%"}](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/automatische-benachrichtigung.png)

#### Validation von Codes
*Akteur:* System

Das System kann auch mit Hilfe des neuen österreichischen e-Health Terminologieservers prüfen, ob ein bestimmter Code Teil eines Codesystems oder eines Value Sets ist. Dies funktioniert mit der FHIR-Operation [`$validate-code` für Codesysteme](http://www.hl7.org/fhir/codesystem-operation-validate-code.html) bzw. [`$validate-code` für Value Sets](http://www.hl7.org/fhir/valueset-operation-validate-code.html).

[![validate-code](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/validate-code.png){: style="width: 100%"}](https://gitlab.com/elga-gmbh/termgit/-/raw/dev/input/images/validate-code.png)

### Tab "Download" mit allen Downloadformaten

Folgende Downloadformate werden aktuell angeboten:

| Name | Dateiendung | Hinweis |
| ----------- | ----------- | ----------- |
| FHIR R4 xml | `.4.fhir.xml` | |
| FHIR R4 json | `.4.fhir.json` | |
| fsh v1 | `.1.fsh` | |
| fsh v2 | `.2.fsh` | |
| ClaML v2 | `.2.claml.xml` | |
| ClaML v3 | `.3.claml.xml` | |
| propCSV v1 | `.1.propcsv.csv` | |
| SVSextELGA v1 | `.1.svsextelga.xml` | **Achtung! DEPRECATED!**<br/>Dieses Format kann technisch gesehen nicht alle Informationen enthalten,<br/>die in den anderen Formaten verfügbar sind.<br/>Dieses Format ist nur für Legacy-Zwecke verfügbar. |
| SVSextELGA v2 | `.2.svsextelga.xml`   | **Achtung! DEPRECATED!**<br/>Dieses Format kann technisch gesehen nicht alle Informationen enthalten,<br/>die in den anderen Formaten verfügbar sind.<br/>Dieses Format ist nur für Legacy-Zwecke verfügbar.<br/> Es sind hier jedoch alle Eigenschaften zu Konzepten verfügbar. |
| outdatedCSV v1 | `.1.outdatedcsv.csv` | **Achtung! DEPRECATED!**<br/>Dieses Format kann technisch gesehen nicht alle Informationen enthalten,<br/>die in den anderen Formaten verfügbar sind.<br/>Dieses Format ist nur für Legacy-Zwecke verfügbar. |
| outdatedCSV v2 | `.2.outdatedcsv.csv`   | **Achtung! DEPRECATED!**<br/>Dieses Format kann technisch gesehen nicht alle Informationen enthalten,<br/>die in den anderen Formaten verfügbar sind.<br/>Dieses Format ist nur für Legacy-Zwecke verfügbar.<br/> Es sind hier jedoch alle Eigenschaften zu Konzepten verfügbar. |


#### FHIR R4 XML
Die XML-Darstellung für eine Ressource wird in diesem Format beschrieben [^2]:

     <name xmlns="http://hl7.org/fhir" (attrA="value")>
       <!-- from Resource: id, meta, implicitRules, and language -->
       <nameA><!--  1..1 type description of content  --><nameA>
       <nameB[x]><!-- 0..1 type1|type1 description  --></nameB[x]>
       <nameC> <!--  1..* -->
         <nameD><!-- 1..1 type>Relevant elements  --></nameD>
       </nameC>
     <name>

#### FHIR R4 JSON
Die JSON-Darstellung für eine Ressource basiert auf dem [JSON-Format, beschrieben in STD 90 (RFC 8259)](https://www.rfc-editor.org/info/std90) und wird unter Verwendung dieses Formats beschrieben [^3]:

    {
      "resourceType" : "[Resource Type]",
      // from Source: property0
      "property1" : "<[primitive]>", // short description
      "property2" : { [Data Type] }, // short description
      "property3" : { // Short Description
        "propertyA" : { CodeableConcept }, // Short Description (Example)
      },
      "property4" : [{ // Short Description
        "propertyB" : { Reference(ResourceType) } // R!  Short Description
      }]
    }

#### fsh v1 und v2
FHIR Shorthand (fsh) ist eine domänenspezifische Sprache zur Definition des Inhalts von FHIR Implementation Guides (IG). Die Sprache wurde speziell für diesen Zweck entwickelt, ist einfach und kompakt und ermöglicht es dem Autor, seine Absicht mit weniger Bedenken bezüglich der zugrunde liegenden FHIR-Mechanik auszudrücken. fsh kann mit einem beliebigen Texteditor erstellt und aktualisiert werden. Da es sich um Text handelt, ermöglicht es eine verteilte, teambasierte Entwicklung mit Quellcode-Kontrolltools wie GitHub [^1]. Zur Zeit werden Version 1 wie auch Version 2 unterstützt.


#### Proprietäres CSV
Das proprietäre CSV dient den Terminologieverwaltern zur leichter Bearbeitung von Terminologien. Dieses Format besteht aus einer Reihen von FHIR Metadaten-Elementen und Konzept-Elementen. In der FHIR Spezifikation sind die verpflichtenden Elemente für Codesysteme (http://hl7.org/fhir/codesystem.html#resource) und Value Sets (http://hl7.org/fhir/valueset.html#resource) definiert - auch die Bezeichnungen sind bereits vordefiniert und dürfen nicht verändert werden.

Der Wert der Bezeichnung wird in den jeweiligen FHIR-Datentypen (uri, Identifier, string, code, ...) konvertiert und abgespeichert, falls die Konvertierung erfolgreich war. Es können keine weiteren Metadaten-Elemente oder Konzept-Elemente angeführt werden, außer es wird eine eigene Extension für CodeSystem/ValueSet erstellt.

Die Metadaten-Elemente und Konzepte-Elemente können in jeglicher Reihenfolge angegeben werden. Es wird nach dem key-value-prinzip immer der Key ausgelesen, welcher dem darunter stehenden Value die Bedeutung gibt.

Folgende Metadaten-Elemente sind für die **ELGA Codesysteme** gewünscht:

| Name | Kardinalität | Beschreibung |
| --- | --- | --- |
| resource | **1..1** | declares the existence of and describes a code system or code system supplement and its key properties, and optionally defines a part or all of its content. Also known as Ontology, Terminology, or Enumeration |
| url | 0..1 | Canonical identifier for this code system, represented as a URI (globally unique) (Coding.system) |
| identifier | 0..* | Additional identifier for the code system (business identifier) all attributes are combined by a pipe in the import file e.g.\|\|urn:oid:1.25.256.25.8.6\|\|\| |
| * use | 0..1 | usual, official, temp, secondary, old (If known) |
| * type | 0..1 | Description of identifier |
| * system | 0..1 | The namespace for the identifier value |
| * value | 0..1 | The value that is unique |
| * period | 0..1 | Time period when id is/was valid for use |
| * assigner | 0..1 | Organization that issued id (may be just text) |
| date | 0..1 | Date last changed. The format is YYYY, YYYY-MM, YYYY-MM-DD or YYYY-MM-DDThh:mm:ss+zz:zz, e.g. 2018, 1973-06, 1905-08-23, 2015-02-07T13:28:17-05:00 or 2017-01-01T00:00:00.000Z. |
| version | 0..1 | Business version of the code system (Coding.version) |
| name | 0..1 | Name for this code system (computer friendly) |
| title | 0..1 | Name for this code system (human friendly) |
| status | **1..1** | draft, active, retired, unknown |
| description | 0..1 | Natural language description of the code system |
| content | **1..1** | not-present, example, fragment, complete, supplement |
| copyright | 0..1 | Use and/or publishing restrictions |

Folgende Konzept-Elemente sind für die **ELGA Codesysteme** gewünscht:

| Name | Kardinalität | Beschreibung |
| --- | --- | --- |
| code | **1..1** | Code that identifies concept |
| display | 0..1 | Text to display to the user |
| designation | 0..* | Additional representations for the concept - all three attributes are combined by a pipe in the import file e.g. de\|Fully specified name\|Bronchitis |
| * language | 0..1 | Human language of the designation - Common Languages (Preferred: de) |
| * use | 0..1 | Details how this designation would be used - Designation Use (Fully specified name or Synonym) |
| * value | **1..1** | The text value for this designation |
| definition | 0..1 | Formal definition |
| property | 0..* | Property value for the concept - both attributes are combined by a pipe in the import file e.g. child\|101 |
| * code | 1..1 | Reference to CodeSystem.property.code |
| * value | 1..1 | Value of the property for this concept |

Folgende Metadaten-Elemente sind für die **ELGA Value Sets** gewünscht:

| Name | Kardinalität | Beschreibung |
| --- | --- | --- |
| resource | **1..1** | declares the existence of and describes a code system or code system supplement and its key properties, and optionally defines a part or all of its content. Also known as Ontology, Terminology, or Enumeration |
| url | 0..1 | Canonical identifier for this value set, represented as a URI (globally unique) |
| identifier | 0..* | Additional identifier for the value set (business identifier) all attributes are combined by a pipe in the import file e.g.\|\|urn:oid:1.25.256.25.8.6\|\|\| |
| * use | 0..1 | usual, official, temp, secondary, old (If known) |
| * type | 0..1 | Description of identifier |
| * system | 0..1 | The namespace for the identifier value |
| * value | 0..1 | The value that is unique |
| * period | 0..1 | Time period when id is/was valid for use |
| * assigner | 0..1 | Organization that issued id (may be just text) |
| date | 0..1 | Date last changed. The format is YYYY, YYYY-MM, YYYY-MM-DD or YYYY-MM-DDThh:mm:ss+zz:zz, e.g. 2018, 1973-06, 1905-08-23, 2015-02-07T13:28:17-05:00 or 2017-01-01T00:00:00.000Z. |
| version | 0..1 | Business version of the code system (Coding.version) |
| name | 0..1 | Name for this code system (computer friendly) |
| title | 0..1 | Name for this code system (human friendly) |
| status | **1..1** | draft, active, retired, unknown |
| description | 0..1 | Natural language description of the code system |
| copyright | 0..1 | Use and/or publishing restrictions |

Folgende Konzept-Elemente sind für die **ELGA Value Sets** gewünscht:

| Name | Kardinalität | Beschreibung |
| --- | --- | --- |
| system | 0..1 | The system the codes come from e.g. urn:oid:1.34.2.0.12.56.35 |
| code | **1..1** | Code or expression from system |
| display | 0..1 | Text to display for this code for this value set in this valueset |
| designation | 0..* | Additional representations for the concept - all three attributes are combined by a pipe in the import file e.g. de\|Fully specified name\|Bronchitis |
| * language | 0..1 | Human language of the designation - Common Languages (Preferred: de) |
| * use | 0..1 | Details how this designation would be used - Designation Use (Fully specified name or Synonym) |
| * value | 1..1 | The text value for this designation |
| filter | 0..* | Select codes/concepts by their properties (including relationships) - all three attributes are combined by a pipe in the import file e.g. child\|not-in\|101 |
| * property | 1..1 | A property/filter defined by the code system |
| * op | 1..1 | =, is-a, descendent-of, is-not-a, regex, in, not-in, generalizes, exists |
| * value | 1..1 | Code from the system, or regex criteria, or boolean value for exists |
| exclude | 0..1 | Explicitly exclude codes from a code system or other value sets - means that this concept should be excluded (false/0 oder true/1) |


#### ClaML v2 und v3
ClaML (Classification Markup Language) ist ein spezielles XML-Datenformat für Klassifikationen. Der ClaML Export steht nur für Codelisten zur Verfügung. Informationen zur Terminologie bzw. Versionen sind in den ersten Elementen enthalten. Die OID findet sich im „uid“-Attribut des Elements Identifier. Es wird ClaML in der Version 2 und in der Version 3 unterstützt. Weitere Informationen zur Terminologie finden sich in den ersten „Meta“-Elementen als name-value Paar:
Description: deutsche Beschreibung der Terminologie
* Description_eng: englische Beschreibung der Terminologie
* Website: Link zu Quelle etc.
* version_description: Beschreibung der Version
* insert_ts: Datum des Imports/Anlegen am Terminologieserver
* status_date: „Status geändert am“-Datum
* expiration_date: „gültig-bis“ Datum
* last_change_date: „geändert am“ Datum
* gueltigkeitsbereich
* statusCode: 0=>Vorschlag; 1=>Public; 2=>Obsolet
* unvollständig (bei Codelisten): Flag zur Kennzeichnung von Codelisten, die nur auszugsweise am Terminologieserver veröffentlicht sind
* verantw_Org (bei Codelisten): verantwortliche Organisation

Alle anderen Informationen zur Version befinden im title-Element:
* tite@date: „gültig-ab“ Datum
* tite@name: Name der Terminologie
* tite@version: Bezeichnung bzw. Nummer der Version

Die einzelnen Konzepte werden als class-Element abgebildet, jeweils mit name-value
Paaren:
* class/@code: Code
* \<Rubric kind='preferred'>/Label: Begriff
* \<Rubric kind='note'>/Label: nähere Beschreibung (Anwendungsbeschreibung) des Konzepts
* TS_ATTRIBUTE_HINTS: Hinweise zur Anwendung des Konzepts
* TS_ATTRIBUTE_MEANING: deutsche Sprachvariante, Bedeutung
* TS_ATTRIBUTE_STATUS/ TS_ATTRIBUTE_STATUSUPDATE: Informationen zum Status des einzelnen Konzepts
* Level/Type: Abbildung der Hierarchie
* Relationships: etwaige Verbindungen zu anderen Konzepten

Zu beachten ist hier, dass die Elemente in der Exportdatei nicht vorhanden sind, wenn sie am Terminologieserver nicht befüllt sind.

#### SVSextELGA
Das angebotene SVS ist ein erweitertes IHE Sharing Value Set Format. In diesem XML Format können Codelisten und Value Sets exportiert werden.
Diese enthält Attribute zur Terminologie bzw. der Version (Element „valueSet“):
* Name: Bezeichnung der Terminologie
* Beschreibung und description der Terminologie
* effectiveDate: „gültig-ab“ Datum der Version
* Id: OID der Version
* statusCode (derzeit nicht unterstützt)
* website: Link zu Quelle etc.
* version: Nummer/Bezeichnung der Version
* version-beschreibung (bei Codelisten): Beschreibung der Version
* gueltigkeitsbereich
* statusCode: 0=>Vorschlag: 1=>Public; 2=>Obsolet
* last_change_date: Datum der letzten Änderung

Alle Konzepte sind als „concept“-Elemente in der „conceptList“ vorhanden und enthalten folgende Attribute:
* code
* codeSystem: OID des Quell-Code Systems des Konzepts
* displayName: Begriff
* concept_beschreibung: nähere Beschreibung (Anwendungsbeschreibung) des Konzepts
* deutsch: deutsche Sprachvariante, Bedeutung
* einheit_codiert, einheit_print: nur relevant bei Laborparametern
* hinweise: Hinweise zur Anwendung des Konzepts
* level/type: Abbildung der Hierarchie
* relationships: etwaige Verbindungen zu anderen Konzepten
* orderNumber (nur bei Value Sets): Index zur Fixierung der Reihenfolge
* conceptStatus: 0=>Vorschlag; 1=>Public; 2=>Obsolet
* unvollständig (bei Codelisten): Flag zur Kennzeichnung von Codelisten, die nur auszugsweise am Terminologieserver veröffentlicht sind
* verantw_Org (bei Codelisten): verantwortliche Organisation

In der Version 2 von SVSextELGA können nicht definierte weitere Attribute enthalten sein.

#### outdatedcsv
Das Format "outdatedcsv" bildet das csv Exportformat des bisherigen Terminologieservers (termpub.gv.at) ab. Das Format enthält keine Metadaten zu Codesystemen oder Value Sets ab und kann auch nicht alle für FHIR erforderlichen Informationen abbilden, die in anderen Formaten vorhanden sind und ist somit nicht FHIR tauglich.
Zu jedem Konzept können in "outdatedcsv" lediglich folgende Informationen dargestellt werden:
* code
* codeSystem
* displayName
* parentCodeSystemName
* concept_Beschreibung
* meaning
* hints
* orderNumber
* level
* type
* relationships
* einheit print
* einheit codiert

**Dieses Format soll den Umstieg auf den neuen Terminologieserver (termgit.gv.at) erleichtern. Von einer weiteren Verwendung wird explizit abgeraten, da dieses Format nicht mehr weiterentwickelt wird.**

In der Version 2 von outdatedcsv können nicht definierte Attribute als weitere Spalten enthalten sein.

### Aktueller Entwicklungsstand

Die folgenden zwei Open-Source Projekte dienen der Weiterentwicklung der Prozesse und der Software, die für die Bereitstellung von TerminoloGit erforderlich sind.

| Projektname | Beschreibung | Repository-URL | GitLab-Projekt-ID | GitLab-Pages-URL |
| --- | --- | --- | --- | --- |
| TerminoloGit Dev | [https://gitlab.com/elga-gmbh/termgit-dev](https://gitlab.com/elga-gmbh/termgit-dev) | Repository für die technische Weiterentwicklung von TerminoloGit.<br/><br/>*Hinweis:* Unter dem angegebenen GitLab-Pages-URL werden die Inhalte der letzten erfolgreichen Pipeline eines beliebigen Git-Branches dieses Repositories dargestellt. | 21743825 | [https://elga-gmbh.gitlab.io/termgit-dev/](https://elga-gmbh.gitlab.io/termgit-dev/) |
| TerminoloGit Dev HTML | [https://gitlab.com/elga-gmbh/terminologit-dev-html](https://gitlab.com/elga-gmbh/terminologit-dev-html) | Repository für die statischen HTML-Seiten, die vom HL7® FHIR® IG Publisher auf Basis des `master`-Branches von [https://gitlab.com/elga-gmbh/termgit-dev](https://gitlab.com/elga-gmbh/termgit-dev) erstellt wurden. | 28239847 | [https://dev.termgit.elga.gv.at](https://dev.termgit.elga.gv.at) |


### Architektur

Grundlegende Kenntnisse von Git und GitLab werden vorausgesetzt. Speziell wichtig für TerminoloGit sind:
* Git als verteiltes Versionskontrollsystem (Jeder Entwickler hat eine vollständige Kopie des Repositories)
	* Branching/Merging (Parallele, unabhängige Entwicklung an unterschiedlichen Komponenten)
	* Tags
	* Logs
* GitLab als UI
	* Merge Requests
	* CI/CD mit GitLab Runner
	* Benutzer-/Rechteverwaltung

Grundlegende Kenntnisse von FHIR und seiner Werkzeuge werden vorausgesetzt. Speziell wichtig für TerminoloGit sind:
* FHIR kombiniert die Vorteile der etablierten HL7-Standard-Produktlinien Version 2, Version 3 und CDA mit aktuellen Web-Standards
	* Datenformate und Elemente als sogenannte „Ressourcen“
	* starker Fokus liegt dabei auf einer einfachen Implementierbarkeit
	* Web-basierte API Technologien, wie das HTTP-basierte Programmierparadigma REST, HTML, TLS und OAUTH2
	* Repräsentation der Daten als JSON als auch XML
	* direkten Zugriff auf einzelne Informationsfelder als Service
* FHIR IG Publisher als Tool für die Erstellung von statischen HTML-Seiten, die den gesamten Inhalt eines Implementation Guides darstellen
	* Benutzerdefinierter Inhalt sowie FHIR Ressourcen können verarbeitet werden
* FSH (FHIR Shorthand) + SUSHI (SUSHI Unshortens Short Hands Inputs)
	* FSH erlaubt die vereinfachte Definition von FHIR Ressourcen, primär für die Definition von FHIR IGs
	* SUSHI interpretiert/kompiliert FSH-Dateien und erstellt daraus FHIR Ressourcen.
* FHIR Server, wenn dieser nur für Terminologien genutzt wird, wird dieser FHIR tx (Terminology Exchange) genannt
	* REST API
	* Operationen (z.B. $validate-code)
	* Versionierung von Ressourcen

#### Fully dressed use case diagram

Das gesamte Konzept wird in einem fully dressed use case diagram dargestellt, darunter findet man die verschriftlichten Anwendungsfälle auf subfunction/fish level. Bitte beachten Sie, dass dies nicht vollständig dem Cockburn-Diagramm für fully dressed use case Diagramme entspricht. Es wurden ein paar Änderungen vorgenommen, um alle benötigten Informationen in einem Diagramm darzustellen.

[![fully dressed use case diagram](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/FullyDressedUseCaseDiagramm.png){: style="width: 100%"}](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/FullyDressedUseCaseDiagramm.png)

** Parameter für alle Anwendungsfälle:**

1. __Scope:__
    * eine Terminologie wie ein Codesystem oder ein Value Set

2. __Level:__
    * Subfunction/Fish

##### Konto erstellen, authentifizieren und autorisieren

1. __Akteure:__
    * Terminologie Benutzer (Mensch)
    * GitLab-Dienst (Maschine)

1. __Kurzbeschreibung:__ Der Terminologie-Benutzer benötigt ein Konto für spezielle Interaktionen wie das Abonnieren eines Tags einer Terminologie über GitLab und erstellt eines über GitLab.

1. __Nachbedingungen:__
    1. __Minimalgarantien:__
        * wenn die Benutzerautorisierung durch die ELGA GmbH nicht erfolgt ist
            * Warten, bis der Autorisierungsprozess abgeschlossen ist
    2. __Erfolgsgarantien:__
        * Es wurde ein Benutzerkonto mit individuellen Zugriffsrechten angelegt.

1. __Voraussetzungen:__
    * Die für TerminoloGit benötigten GitLab-Projekte wurden erstellt.

1. __Auslöser:__
    * Ein Terminologie-Benutzer möchte eine spezielle Interaktion durchführen

1. __Basisablauf:__
    1. Anlegen eines Kontos auf GitLab und ggf. Beantragung individueller Berechtigungen
    1. Anmeldung bei GitLab

1. __Erweiterungen:__ keine

1. __Software:__
    * Jeder Browser

1. __Hardware:__
    * Jeder Client mit einem Browser

1. __Dienste:__
    * GitLab.com oder eigenes GitLab CE Hosting

##### Abonnement

1. __Akteure:__
    * GitLab-Dienst (Maschine)

1. __Kurzbeschreibung:__ Die Abonnenten werden per Mail informiert, weil eine neue oder bearbeitete Terminologie-Datei gepusht wurde.

1. __Nachbedingungen:__
    1. __Minimale Garantien:__ keine
    2. __Erfolgsgarantien:__
        * Ein Abonnent wird über eine neue Terminologie informiert

1. __Voraussetzungen:__
    * Das GitLab-Projekt TerminoloGit wurde von GitLab-Benutzern abonniert

1. __Auslöser:__
    * Eine Terminologie wurde hinzugefügt oder aktualisiert

1. __Basisablauf:__
    1. GitLab sendet die Änderung an alle Abonnenten

1. __Erweiterungen:__ keine

1. __Software:__
    * Jedes Mailprogramm

1. __Hardware:__
    * Jeder Client mit einem Mailprogramm

1. __Dienste:__
    * GitLab zum Senden der Änderungen an die Abonnenten

##### Verwendung von FHIR IG und FHIR-tx-at

1. __Akteure:__
   * Terminologie-Benutzer (Mensch oder Maschine)
   * GitLab Service (Maschine)

1. __Kurzbeschreibung:__ Eine Terminologie wie ein Codesystem oder ein Valueset wird über die GitLab GUI oder TerminoloGit GUI oder GitLab REST-Schnittstelle oder FHIR TX REST-Schnittstelle abgerufen

1. __Nachbedingungen:__
    1. __Minimalgarantien:__
        * Erhalt einer Rückmeldung
    2. __Erfolgsgarantien:__
        * ob die vom Benutzer gesuchte Terminologie existiert
            * Erhalt einer Terminologie

1. __Voraussetzungen:__
    * Das TerminoloGit GitLab Projekt existiert
    * Das FHIR IG ist veröffentlicht
    * Der FHIR-tx-at ist auf dem neuesten Stand

1. __Auslöser:__
    * Eine Terminologie muss abgerufen werden

1. __Basisablauf:__
    1. Wenn Sie die FHIR IG-Webseite aufrufen, erhalten Sie ein durchsuchbares Format aller Terminologien, mit der Möglichkeit, sie in einem beliebigen Format herunterzuladen
	1. Wenn Sie die GitLab-Webseite aufrufen, erhalten Sie eine Liste aller Formate aller Terminologien, mit der Möglichkeit, sie herunterzuladen
    1. Durch den Betrieb mit weltweit standardisierten FHIR ValueSet oder CodeSystems REST Operationen werden die Terminologien von einer Maschine abgerufen
	1. Beim Betrieb mit der GitLab REST API werden die Terminologien von einer Maschine abgerufen

1. __Erweiterungen:__
    * Eine Validierung von Konzepten in Ressourcen ValueSet oder CodeSystem ist mit $validate-code möglich.
    * Ein Hierarchiestatus zwischen zwei Konzepten in einem CodeSystem ist mit $subsumes möglich.
    * Eine detaillierte Ansicht eines Konzepts in einem CodeSystem ist mit $lookup möglich.

1. __Software:__
    * Ein Browser oder Rest-Client

1. __Hardware:__
    * Client zum Browsen oder Senden von Rest-Operationen

1. __Dienste:__
    * GitLab für die Veröffentlichung von Terminologien
    * ein FHIR-Server für Rest-Operationen

##### CRUD und Push einer Terminologie zu GitLab

1. __Akteure:__
    * Terminologie-Verantwortlicher (Mensch)
    * GitLab-Dienst (Maschine)

1. __Kurzbeschreibung:__ Eine neue Terminologiedatei wird erstellt oder bearbeitet und an das GitLab-Projekt übertragen

1. __Nachbedingungen:__
    1. __Minimale Garantien:__
        * Wenn der Commit erfolgreich ist (z. B. keine Merge-Konflikte):
            * Die neue, bearbeitete oder als im Ruhestand gekennzeichnete Terminologiedatei wird im TerminoloGit-GitLab-Projekt veröffentlicht. Sie wird durch Git und durch das eigene IGVer-Programm versioniert.
            * Die Continuous Delivery wird gestartet und löst die nächsten Schritte aus
    2. __Erfolgsgarantien:__
        * Beim Pushen kommt eine Rückmeldung vom Git-Client zurück

1. __Voraussetzungen:__
    * Das TerminoloGit GitLab Projekt existiert
    * Ein lokaler Git-Client und ein Editor ist auf einem Client installiert oder eine WebIDE von GitLab wird verwendet

1. __Auslöser:__
    * Eine Terminologie muss erstellt oder aktualisiert werden

1. __Grundlegender Ablauf:__
    1. Mit einem Editor wie z.B. Notepad++ oder Visual Studio Code oder einer WebIDE wird eine Terminologie-Datei im lokalen Git-Repository aktualisiert oder erstellt bzw. durch Setzen des Status auf retired gelöscht.
    1. In Ihrem lokalen Git-Client wird ein neuer Commit erstellt und an das entfernte Repository TerminoloGit übertragen

1. __Erweiterungen:__
    * Ein Branch neben Master könnte darauf hinweisen, dass ein neuer Commit zu Test- oder Genehmigungszwecken gepusht wird. In diesem Fall ist die Continuous Delivery von GitLab die gleiche wie für Master, mit dem einzigen Unterschied, dass das Ergebnis nur auf einer eigenen Seite sichtbar ist

1. __Software:__
    * ein lokaler Git-Client, z.B. SourceTree (kostenlose Git-GUI) oder ein Browser
    * einen Editor, z. B. Notepad++ oder Visual Studio Code (kostenlose Code-Editoren) oder einen Browser

1. __Hardware:__
    * Client für die Bearbeitung von Textdateien und Git-Pushing oder Browsing

1. __Dienste:__
    * GitLab für die Veröffentlichung von Terminologien

##### Kontinuierliche Auslieferung an FHIR IG und FHIR-tx-at

1. __Akteure:__
    * GitLab Service (Maschine)

1. __Kurzbeschreibung:__ Eine neue oder bearbeitete Terminologiedatei wird vom GitLab Service durch einen Runner in andere Terminologieformate verarbeitet

1. __Nachbedingungen:__
    1. __Minimalgarantien:__
        * Wenn kein Fehler bei der kontinuierlichen Übermittlung auftritt:
            * Die neuen oder bearbeiteten Terminologie-Dateien werden von MaLaC-CT in alle anderen Formate konvertiert.
            * Das FHIR IG wird mit Links zu allen Formaten aufgebaut.
            * Das FHIR-tx-at wird mit den neuen Terminologien aktualisiert.
    2. __Erfolgsgarantien:__
        * Eine Nachricht wird vom GitLab-Runner zurückgegeben

1. __Voraussetzungen:__
    * Das TerminoloGit GitLab CI ist konfiguriert.

1. __Auslöser:__
    * Eine Terminologie wurde hinzugefügt oder aktualisiert

1. __Grundlegender Ablauf:__
    1. ein Runner für die kontinuierliche Bereitstellung wird gestartet, weil eine neue Übergabe erfolgt ist
    1. wenn die Konvertierung erfolgreich war, wird der IGVer und anschließend der IG Publisher gestartet
    1. wenn der IG Publisher bestanden hat, wird der Upload zum FHIR-tx-at gestartet

1. __Erweiterungen:__ keine

1. __Software:__ keine

1. __Hardware:__ keine

1. __Dienste:__
    * GitLab zum Ausführen des Runners

##### In Branches pushen, genehmigen und zu Master zusammenführen/mergen

1. __Akteure:__
    * Terminologie-Verantwortlicher (Mensch)
    * GitLab-Dienst (Maschine)

1. __Kurzbeschreibung:__ Eine neue oder bearbeitete Terminologiedatei wird in einen Zweig/Branch übertragen und für eine Zusammenführung mit Master angefordert.

1. __Postconditions:__
    1. __Minimalgarantien:__
        * Wenn eine Korrektur erforderlich ist:
            * Warten, dass der Terminologie-Verantwortliche mit einem neuen Commit korrigiert
    2. __Erfolgsgarantien:__
        * ein Zweig/Branch wurde nach Master zusammengeführt

1. __Voraussetzungen:__
    * Das TerminoloGit-Projekt ist für die Verzweigung konfiguriert

1. __Auslöser:__
    * Eine Terminologie wurde zu einem Zweig/Branch hinzugefügt oder aktualisiert

1. __Grundlegender Ablauf:__
    1. der Terminologie-Verantwortliche erstellt für seine Commits einen neuen Zweig/Branch
    1. der Terminologie-Verantwortliche erstellt einen Commit mit seinen Änderungen
    1. der Terminologie-Verantwortliche beantragt eine Zusammenführung/Merge mit dem Master/Main-Branch oder einem anderen geschützten Zweig/Branch
    1. ein Terminologieadministrator kommentiert den Antrag und verlangt Änderungen oder akzeptiert diesen Zweig/Branch und führt ihn zusammen

1. __Erweiterungen:__ keine

1. __Software:__
    * ein Browser

1. __Hardware:__
    * ein Rechner mit einem Browser

1. __Dienste:__
    * GitLab zum Ausführen des Runners


#### BigPicture

Mit dem Wissen der groben Architektur durch das fully dressed use case Diagramm zeigt das folgende BigPicture die verschiedenen Möglichkeiten TerminoloGit zu verwenden, darunter auch die mögliche kaskadierende Systemabhängigkeiten.

[![big-picture](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/BigPicture.png){: style="width: 100%"}](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/BigPicture.png)

#### CI/CD Ablauf

Im folgenden werden die drei häuftigsten Abläufe der CI/CD Pipeline visualisiert. Für weitere Details ist die .gitlab-ci.yml heranzuziehen.

##### Standardcase Terminologie-Wartung

[![CI-CD-normal-case](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/CICD_norm.png){: style="width: 100%"}](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/CICD_norm.png)
<br/>

##### Integrationstest

[![CI-CD-integration-test](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/CICD_inttest.png){: style="width: 100%"}](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/CICD_inttest.png)
<br/>

##### SKIP_MALAC

[![CI-CD-skip-malac](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/CICD_skip_malac.png){: style="width: 100%"}](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/CICD_skip_malac.png)
<br/>

#### Einrichtung/Installation

0. Erstellen Sie einen GitLab.com-Benutzer
1. Forken Sie von einem bestehenden TerminoloGit-Projekt
1.1. Wenn Sie ein neues Repository mit nur zwei Referenzterminologien benötigen, forken Sie von
https://gitlab.com/elga-gmbh/termgit-dev & https://gitlab.com/elga-gmbh/termgit-dev-html
1.2. Wenn Sie alle veröffentlichten Terminologien benötigen, forken Sie von https://gitlab.com/elga-gmbh/termgit & https://gitlab.com/elga-gmbh/termgit-html
2. Konfigurieren Sie die CI/CD
2.1. Aktivieren Sie unter Settings - CI/CD - Runner die öffentlichen Runner
2.1. Bestätigen Sie Ihren Gitlab.com-User mit einer Kreditkarte (es wird nichts abgebucht, das liegt nur daran, dass Crypto-Miner den kostenlosen Gitlab-Runner mit ihren Bots ausnutzen)
2.3. Unter Settings - CI/CD - variables folgendes anlegen
 * [username]_GITLAB_CI_TOKEN = [unter User - preferences - access tokens ein Token mit dem Namen: GITLAB_CI und dem Scope: api & read_api anlegen und dieses Token verwenden]
 * TERMGIT_CANONICAL = https://[Benutzername].gitlab.io/[Projektname]
 * TERMGIT_HTML_PROJECT = [benutzername]/[HTML-Projektname]
 * TERMGIT_HTML_PROJECT_DEFAULT_BRANCH = main
 * UPLOAD_ARTIFACTS = true
3. Löschen Sie alle nicht benötigten Terminologien und fügen Sie Ihre eigenen Terminologien hinzu.
Beobachten Sie den Lauf Ihrer Pipeline und genießen Sie Ihr erstes veröffentlichtes TerminoloGit unter https://[Benutzername].gitlab.io/[Projektname]

### Tab "Previous Versions" mit der Anzeige von alten Versionen
Alte Versionen einer Terminologie können über den Tab "Previous Versions" aufgerufen werden. Die dargestellte Tabelle enthält folgende Spalten:

- `Version Number`: Diese Spalte enthält die Versionsnummer der jeweiligen Version der Terminologie.
- `Current Version vs. Outdated Version`: Mittels des [W3C HTML Diff Tools](https://services.w3.org/htmldiff) können die Unterschiede zwischen der alten und aktuellen Version der Terminologie angezeigt werden.

Sollten für eine Terminologie keine alten Versionen vorhanden sein, ist die Tabelle leer.

In der Darstellung ist eine alte Version durch folgenden Banner erkennbar. Über diesen kann auch direkt zur aktuellen Version gewechselt werden.

[![old-version-banner](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/old-version-banner.png){: style="width: 100%"}](https://gitlab.com/elga-gmbh/termgit-dev/-/raw/dev/input/images/old-version-banner.png)

### Kaskadierende Synchronisation von eigenen Instanzen

Eine kaskadierende Synchronisation ist manuell mit der Git Funktion Submodule oder Subtree machbar. Weiters ist diese automatisiert per CI/CD im eigenen System möglich, wenn man selbst keine Adaptionen/Erweiterungen an den Terminologien durchführt.

Für die TerGi Implementierung im Q3 2022 werden weitere Wege für die kaskadierende Synchronisation zwischen TerminoloGit und TerGi Instanzen untersucht und implementiert. Diese Dokumentation wird entsprechend den Ergebnissen aktualisiert.

Zur Zeit exisitiert für interne Tests und Konvertierungsunterstützung ein eigener FHIR 4.0.1 (R4) Server https://hub.docker.com/r/ibmcom/ibm-fhir-server, basierend auf dem Open-Source Projekt https://github.com/LinuxForHealth/FHIR. Für TerGi Instanzen wird eine freie Wahl des FHIR Servers möglich sein.

### Referenzen

[^1]: FHIR Short Hand, beschrieben in [http://hl7.org/fhir/uv/shorthand/2020May/](http://hl7.org/fhir/uv/shorthand/2020May/)
[^2]: FHIR XML, beschrieben in [http://www.hl7.org/fhir/xml.html](http://www.hl7.org/fhir/xml.html)
[^3]: FHIR JSON, beschrieben in [http://www.hl7.org/fhir/json.html](http://www.hl7.org/fhir/json.html)
